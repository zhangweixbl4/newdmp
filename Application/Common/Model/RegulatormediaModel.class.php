<?php
namespace Common\Model;



class RegulatormediaModel{

	/*通过媒介获取监管机构ID*/
	public function get_regulatormedia($fmediaid){
		
		$fregulatorcode = M('tregulatormedia')->where(array('fmediaid'=>$fmediaid))->getField('fregulatorcode');//查询关联表获取监管机构代码
		$fregulatorcode = intval($fregulatorcode);//转为数字类型
		
		if($fregulatorcode == 0){
			$info = M('tmedia')->field('tregulator.fcode as regulator_code')
										->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构表，目的：获取媒介机构的地区
										->join('tregulator on tregulator.fregionid = tmediaowner.fregionid')//连接监管机构表，获取媒介机构code‘
										->where(array('tmedia.fid'=>$fmediaid))
										->find();
			$fregulatorcode	= intval($info['regulator_code']);	
			//var_dump($fregulatorcode);exit;			
		}

		return $fregulatorcode;//返回监管机构代码
		
		
	}
    

}