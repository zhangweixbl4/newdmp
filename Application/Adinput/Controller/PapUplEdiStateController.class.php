<?php
namespace Adinput\Controller;
use Think\Controller;
use Think\Exception;
import('Vendor.PHPExcel');
class PapUplEdiStateController extends Controller {


    /*
     * 渲染页面
     */
    public function index()
    {
        $regionTree = json_encode($this->getRegionTree());
        $this->assign(compact('regionTree'));
        $this->display();
    }

    public function apUplEdiState(){
        $export_excel   = I("export_excel",0);//导出excel
        $page_num       = I("page_num",1);//页码
        $page_size      = I("page_size",20) * $page_num;//每页数量
        $month          = I("month",date('Y-m'));//月份
        $region         = I("region");//地区id
        $is_include_sub = I("is_include_sub",0);//是否包含下级
        $media_name     = I("media_name");//媒体名称
        $label          = I("label");//标签
        $data = $this->apUplEdiState_exc($export_excel,$page_num,$page_size,$month,$region,$is_include_sub,$media_name,$label);
        if($export_excel == 1){
            // 导出
            $fileurl = $this->exportExcel($data);
            if(!empty($fileurl)){
                $this->ajaxReturn(['code'=>0,'msg'=>'导出成功！','data'=>$fileurl]);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'导出失败！']);
            }
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'获取成功！','data'=>$data]);
    }
    /**
     * @Des: 
     * @Date: 2019-11-18 14:59:06
     * @param {type} 
     * @return: 
     */
    public function apUplEdiState_exc($export_excel,$page_num,$page_size,$month,$region,$is_include_sub,$media_name,$label){
        $monthMap1 = '';
        $monthMap2 = '';
        $regionMap = '';
        $mediaNameMap = '';
        $labelMap = " and tlabel.flabelclass = '报纸'";
        $page_num = $page_num - 1;
        $limit = " limit ".$page_num.",".$page_size;
        if($export_excel == 1){
            $limit = '';
        }
        $s_date = $month.'-01';
        $e_date = $month.'-'.date('t', strtotime($month));
        if(!empty($month)){
            $monthMap1 = " and tpapersource.fissuedate between '".$s_date."' and '".$e_date."'";
            $monthMap2 = " and tpaperissue.fissuedate between '".$s_date."' and '".$e_date."'";
        }
        if(!empty($region)){
            $regionMap = " and tmedia.media_region_id = ".$region;
            if($is_include_sub != 0){
                $regionMap = " and tmedia.media_region_id like '".trim($region,0)."%'";
            }
        }
        if(!empty($media_name)){
            $mediaNameMap = " and tmedia.fmedianame like '%".$media_name."%'";
        }
        if(!empty($label)){
            $labelMap .= " and tlabel.flabel = '".$label."'";
        }
        $count_sql = "
        select count(*) as total
                from tmedia left join (
                select 
                tmedia.fid
                from tmedia,tpapersource,tmedialabel,tlabel
                where tmedia.fid = tmedialabel.fmediaid
                and tmedialabel.flabelid = tlabel.flabelid 
                and tpapersource.fmediaid = tmedia.fid
                ".$monthMap1.$mediaNameMap.$labelMap.$regionMap."
                group by tmedia.fmedianame
                ) a on tmedia.fid = a.fid
                left join (
                select 
                tmedia.fid
                from tmedia,tmedialabel,tlabel,tpaperissue 
                where tmedia.fid = tmedialabel.fmediaid
                and tmedialabel.flabelid = tlabel.flabelid 
                and tpaperissue.fmediaid = tmedia.fid
                ".$monthMap2.$labelMap."
                group by tmedia.fid) b on tmedia.fid = b.fid,tmedialabel,tlabel
                where tmedia.fid = tmedialabel.fmediaid
                and tmedialabel.flabelid = tlabel.flabelid 
                ".$mediaNameMap.$labelMap.$regionMap."
        ";
        $count = M("")->query($count_sql);
        $sql = "
        select tregion.fname1 as region_name,tmedia.fid,tmedia.fmedianame,'' fdescribe,
                ifnull(d1,0) as d1,ifnull(a1,0) as a1,
                ifnull(d2,0) as d2,ifnull(a2,0) as a2,
                ifnull(d3,0) as d3,ifnull(a3,0) as a3,
                ifnull(d4,0) as d4,ifnull(a4,0) as a4,
                ifnull(d5,0) as d5,ifnull(a5,0) as a5,
                ifnull(d6,0) as d6,ifnull(a6,0) as a6,
                ifnull(d7,0) as d7,ifnull(a7,0) as a7,
                ifnull(d8,0) as d8,ifnull(a8,0) as a8,
                ifnull(d9,0) as d9,ifnull(a9,0) as a9,
                ifnull(d10,0) as d10,ifnull(a10,0) as a10,
                ifnull(d11,0) as d11,ifnull(a11,0) as a11,
                ifnull(d12,0) as d12,ifnull(a12,0) as a12,
                ifnull(d13,0) as d13,ifnull(a13,0) as a13,
                ifnull(d14,0) as d14,ifnull(a14,0) as a14,
                ifnull(d15,0) as d15,ifnull(a15,0) as a15,
                ifnull(d16,0) as d16,ifnull(a16,0) as a16,
                ifnull(d17,0) as d17,ifnull(a17,0) as a17,
                ifnull(d18,0) as d18,ifnull(a18,0) as a18,
                ifnull(d19,0) as d19,ifnull(a19,0) as a19,
                ifnull(d20,0) as d20,ifnull(a20,0) as a20,
                ifnull(d21,0) as d21,ifnull(a21,0) as a21,
                ifnull(d22,0) as d22,ifnull(a22,0) as a22,
                ifnull(d23,0) as d23,ifnull(a23,0) as a23,
                ifnull(d24,0) as d24,ifnull(a24,0) as a24,
                ifnull(d25,0) as d25,ifnull(a25,0) as a25,
                ifnull(d26,0) as d26,ifnull(a26,0) as a26,
                ifnull(d27,0) as d27,ifnull(a27,0) as a27,
                ifnull(d28,0) as d28,ifnull(a28,0) as a28,
                ifnull(d29,0) as d29,ifnull(a29,0) as a29,
                ifnull(d30,0) as d30,ifnull(a30,0) as a30,
                ifnull(d31,0) as d31,ifnull(a31,0) as a31
                from tmedia
                JOIN tregion ON tregion.fid = tmedia.media_region_id
                left join (
                select 
                tmedia.fid,
                sum(case when day(tpapersource.fissuedate) = 1 then 1 else 0 end) d1,
                sum(case when day(tpapersource.fissuedate) = 2 then 1 else 0 end) d2,
                sum(case when day(tpapersource.fissuedate) = 3 then 1 else 0 end) d3,
                sum(case when day(tpapersource.fissuedate) = 4 then 1 else 0 end) d4,
                sum(case when day(tpapersource.fissuedate) = 5 then 1 else 0 end) d5,
                sum(case when day(tpapersource.fissuedate) = 6 then 1 else 0 end) d6,
                sum(case when day(tpapersource.fissuedate) = 7 then 1 else 0 end) d7,
                sum(case when day(tpapersource.fissuedate) = 8 then 1 else 0 end) d8,
                sum(case when day(tpapersource.fissuedate) = 9 then 1 else 0 end) d9,
                sum(case when day(tpapersource.fissuedate) = 10 then 1 else 0 end) d10,
                sum(case when day(tpapersource.fissuedate) = 11 then 1 else 0 end) d11,
                sum(case when day(tpapersource.fissuedate) = 12 then 1 else 0 end) d12,
                sum(case when day(tpapersource.fissuedate) = 13 then 1 else 0 end) d13,
                sum(case when day(tpapersource.fissuedate) = 14 then 1 else 0 end) d14,
                sum(case when day(tpapersource.fissuedate) = 15 then 1 else 0 end) d15,
                sum(case when day(tpapersource.fissuedate) = 16 then 1 else 0 end) d16,
                sum(case when day(tpapersource.fissuedate) = 17 then 1 else 0 end) d17,
                sum(case when day(tpapersource.fissuedate) = 18 then 1 else 0 end) d18,
                sum(case when day(tpapersource.fissuedate) = 19 then 1 else 0 end) d19,
                sum(case when day(tpapersource.fissuedate) = 20 then 1 else 0 end) d20,
                sum(case when day(tpapersource.fissuedate) = 21 then 1 else 0 end) d21,
                sum(case when day(tpapersource.fissuedate) = 22 then 1 else 0 end) d22,
                sum(case when day(tpapersource.fissuedate) = 23 then 1 else 0 end) d23,
                sum(case when day(tpapersource.fissuedate) = 24 then 1 else 0 end) d24,
                sum(case when day(tpapersource.fissuedate) = 25 then 1 else 0 end) d25,
                sum(case when day(tpapersource.fissuedate) = 26 then 1 else 0 end) d26,
                sum(case when day(tpapersource.fissuedate) = 27 then 1 else 0 end) d27,
                sum(case when day(tpapersource.fissuedate) = 28 then 1 else 0 end) d28,
                sum(case when day(tpapersource.fissuedate) = 29 then 1 else 0 end) d29,
                sum(case when day(tpapersource.fissuedate) = 30 then 1 else 0 end) d30,
                sum(case when day(tpapersource.fissuedate) = 31 then 1 else 0 end) d31
                from tmedia,tpapersource,tmedialabel,tlabel
                where tmedia.fid = tmedialabel.fmediaid
                and tmedialabel.flabelid = tlabel.flabelid 
                and tpapersource.fmediaid = tmedia.fid
                ".$monthMap1.$mediaNameMap.$labelMap.$regionMap."
                group by tmedia.fmedianame
                ) a on tmedia.fid = a.fid
                left join (
                select 
                tmedia.fid,
                sum(case when day(tpaperissue.fissuedate) = 1 then 1 else 0 end) a1,
                sum(case when day(tpaperissue.fissuedate) = 2 then 1 else 0 end) a2,
                sum(case when day(tpaperissue.fissuedate) = 3 then 1 else 0 end) a3,
                sum(case when day(tpaperissue.fissuedate) = 4 then 1 else 0 end) a4,
                sum(case when day(tpaperissue.fissuedate) = 5 then 1 else 0 end) a5,
                sum(case when day(tpaperissue.fissuedate) = 6 then 1 else 0 end) a6,
                sum(case when day(tpaperissue.fissuedate) = 7 then 1 else 0 end) a7,
                sum(case when day(tpaperissue.fissuedate) = 8 then 1 else 0 end) a8,
                sum(case when day(tpaperissue.fissuedate) = 9 then 1 else 0 end) a9,
                sum(case when day(tpaperissue.fissuedate) = 10 then 1 else 0 end) a10,
                sum(case when day(tpaperissue.fissuedate) = 11 then 1 else 0 end) a11,
                sum(case when day(tpaperissue.fissuedate) = 12 then 1 else 0 end) a12,
                sum(case when day(tpaperissue.fissuedate) = 13 then 1 else 0 end) a13,
                sum(case when day(tpaperissue.fissuedate) = 14 then 1 else 0 end) a14,
                sum(case when day(tpaperissue.fissuedate) = 15 then 1 else 0 end) a15,
                sum(case when day(tpaperissue.fissuedate) = 16 then 1 else 0 end) a16,
                sum(case when day(tpaperissue.fissuedate) = 17 then 1 else 0 end) a17,
                sum(case when day(tpaperissue.fissuedate) = 18 then 1 else 0 end) a18,
                sum(case when day(tpaperissue.fissuedate) = 19 then 1 else 0 end) a19,
                sum(case when day(tpaperissue.fissuedate) = 20 then 1 else 0 end) a20,
                sum(case when day(tpaperissue.fissuedate) = 21 then 1 else 0 end) a21,
                sum(case when day(tpaperissue.fissuedate) = 22 then 1 else 0 end) a22,
                sum(case when day(tpaperissue.fissuedate) = 23 then 1 else 0 end) a23,
                sum(case when day(tpaperissue.fissuedate) = 24 then 1 else 0 end) a24,
                sum(case when day(tpaperissue.fissuedate) = 25 then 1 else 0 end) a25,
                sum(case when day(tpaperissue.fissuedate) = 26 then 1 else 0 end) a26,
                sum(case when day(tpaperissue.fissuedate) = 27 then 1 else 0 end) a27,
                sum(case when day(tpaperissue.fissuedate) = 28 then 1 else 0 end) a28,
                sum(case when day(tpaperissue.fissuedate) = 29 then 1 else 0 end) a29,
                sum(case when day(tpaperissue.fissuedate) = 30 then 1 else 0 end) a30,
                sum(case when day(tpaperissue.fissuedate) = 31 then 1 else 0 end) a31
                from tmedia,tmedialabel,tlabel,tpaperissue 
                where tmedia.fid = tmedialabel.fmediaid
                and tmedialabel.flabelid = tlabel.flabelid 
                and tpaperissue.fmediaid = tmedia.fid
                ".$monthMap2.$labelMap."
                group by tmedia.fid) b on tmedia.fid = b.fid,tmedialabel,tlabel
                where tmedia.fid = tmedialabel.fmediaid
                and tmedialabel.flabelid = tlabel.flabelid 
                ".$mediaNameMap.$labelMap.$regionMap.$limit;
        $list = M("")->query($sql);
        $total = count($data);
        $data = [
            'total' => $count[0]['total'],
            'list' => $list
        ];
        return $data;
    }

    public function apUplEdiState_V20191118(){

        $monthMap1 = '';
        $monthMap2 = '';
        $regionMap = '';
        $mediaNameMap = '';
        $labelMap = " and tlabel.flabelclass = '报纸'";

        $export_excel = I("export_excel",0);//导出excel

        $page_num = I("page_num",1);//页码

        $page_size= I("page_size",20) * $page_num;//每页数量
        $page_num = $page_num - 1;
        $limit = " limit ".$page_num.",".$page_size;

        if($export_excel == 1){
            $limit = '';
        }

        $month = I("month",date('Y-m'));//月份
        $s_date = $month.'-01';
        $e_date = $month.'-'.date('t', strtotime($month));

        if(!empty($month)){
            $monthMap1 = " and tpapersource.fissuedate between '".$s_date."' and '".$e_date."'";
            $monthMap2 = " and tpaperissue.fissuedate between '".$s_date."' and '".$e_date."'";
        }

        $region = I("region");//地区id
        $is_include_sub = I("is_include_sub",0);//是否包含下级

        if(!empty($region)){
            $regionMap = " and tmedia.media_region_id = ".$region;
            if($is_include_sub != 0){
                $regionMap = " and tmedia.media_region_id like '".trim($region,0)."%'";
            }
        }

        $media_name = I("media_name");//媒体名称

        if(!empty($media_name)){
            $mediaNameMap = " and tmedia.fmedianame like '%".$media_name."%'";
        }

        $label = I("label");//标签

        if(!empty($label)){
            $labelMap .= " and tlabel.flabel = '".$label."'";
        }

        $count_sql = "
        select count(*) as total
                from tmedia left join (
                select 
                tmedia.fid
                from tmedia,tpapersource,tmedialabel,tlabel
                where tmedia.fid = tmedialabel.fmediaid
                and tmedialabel.flabelid = tlabel.flabelid 
                and tpapersource.fmediaid = tmedia.fid
                ".$monthMap1.$mediaNameMap.$labelMap.$regionMap."
                group by tmedia.fmedianame
                ) a on tmedia.fid = a.fid
                left join (
                select 
                tmedia.fid
                from tmedia,tmedialabel,tlabel,tpaperissue 
                where tmedia.fid = tmedialabel.fmediaid
                and tmedialabel.flabelid = tlabel.flabelid 
                and tpaperissue.fmediaid = tmedia.fid
                ".$monthMap2.$labelMap."
                group by tmedia.fid) b on tmedia.fid = b.fid,tmedialabel,tlabel
                where tmedia.fid = tmedialabel.fmediaid
                and tmedialabel.flabelid = tlabel.flabelid 
                ".$mediaNameMap.$labelMap.$regionMap."
        ";

        $count = M("")->query($count_sql);

        $sql = "
        select tregion.fname1 as region_name,tmedia.fid,tmedia.fmedianame,'' fdescribe,
                ifnull(d1,0) as d1,ifnull(a1,0) as a1,
                ifnull(d2,0) as d2,ifnull(a2,0) as a2,
                ifnull(d3,0) as d3,ifnull(a3,0) as a3,
                ifnull(d4,0) as d4,ifnull(a4,0) as a4,
                ifnull(d5,0) as d5,ifnull(a5,0) as a5,
                ifnull(d6,0) as d6,ifnull(a6,0) as a6,
                ifnull(d7,0) as d7,ifnull(a7,0) as a7,
                ifnull(d8,0) as d8,ifnull(a8,0) as a8,
                ifnull(d9,0) as d9,ifnull(a9,0) as a9,
                ifnull(d10,0) as d10,ifnull(a10,0) as a10,
                ifnull(d11,0) as d11,ifnull(a11,0) as a11,
                ifnull(d12,0) as d12,ifnull(a12,0) as a12,
                ifnull(d13,0) as d13,ifnull(a13,0) as a13,
                ifnull(d14,0) as d14,ifnull(a14,0) as a14,
                ifnull(d15,0) as d15,ifnull(a15,0) as a15,
                ifnull(d16,0) as d16,ifnull(a16,0) as a16,
                ifnull(d17,0) as d17,ifnull(a17,0) as a17,
                ifnull(d18,0) as d18,ifnull(a18,0) as a18,
                ifnull(d19,0) as d19,ifnull(a19,0) as a19,
                ifnull(d20,0) as d20,ifnull(a20,0) as a20,
                ifnull(d21,0) as d21,ifnull(a21,0) as a21,
                ifnull(d22,0) as d22,ifnull(a22,0) as a22,
                ifnull(d23,0) as d23,ifnull(a23,0) as a23,
                ifnull(d24,0) as d24,ifnull(a24,0) as a24,
                ifnull(d25,0) as d25,ifnull(a25,0) as a25,
                ifnull(d26,0) as d26,ifnull(a26,0) as a26,
                ifnull(d27,0) as d27,ifnull(a27,0) as a27,
                ifnull(d28,0) as d28,ifnull(a28,0) as a28,
                ifnull(d29,0) as d29,ifnull(a29,0) as a29,
                ifnull(d30,0) as d30,ifnull(a30,0) as a30,
                ifnull(d31,0) as d31,ifnull(a31,0) as a31
                from tmedia
                JOIN tregion ON tregion.fid = tmedia.media_region_id
                left join (
                select 
                tmedia.fid,
                sum(case when day(tpapersource.fissuedate) = 1 then 1 else 0 end) d1,
                sum(case when day(tpapersource.fissuedate) = 2 then 1 else 0 end) d2,
                sum(case when day(tpapersource.fissuedate) = 3 then 1 else 0 end) d3,
                sum(case when day(tpapersource.fissuedate) = 4 then 1 else 0 end) d4,
                sum(case when day(tpapersource.fissuedate) = 5 then 1 else 0 end) d5,
                sum(case when day(tpapersource.fissuedate) = 6 then 1 else 0 end) d6,
                sum(case when day(tpapersource.fissuedate) = 7 then 1 else 0 end) d7,
                sum(case when day(tpapersource.fissuedate) = 8 then 1 else 0 end) d8,
                sum(case when day(tpapersource.fissuedate) = 9 then 1 else 0 end) d9,
                sum(case when day(tpapersource.fissuedate) = 10 then 1 else 0 end) d10,
                sum(case when day(tpapersource.fissuedate) = 11 then 1 else 0 end) d11,
                sum(case when day(tpapersource.fissuedate) = 12 then 1 else 0 end) d12,
                sum(case when day(tpapersource.fissuedate) = 13 then 1 else 0 end) d13,
                sum(case when day(tpapersource.fissuedate) = 14 then 1 else 0 end) d14,
                sum(case when day(tpapersource.fissuedate) = 15 then 1 else 0 end) d15,
                sum(case when day(tpapersource.fissuedate) = 16 then 1 else 0 end) d16,
                sum(case when day(tpapersource.fissuedate) = 17 then 1 else 0 end) d17,
                sum(case when day(tpapersource.fissuedate) = 18 then 1 else 0 end) d18,
                sum(case when day(tpapersource.fissuedate) = 19 then 1 else 0 end) d19,
                sum(case when day(tpapersource.fissuedate) = 20 then 1 else 0 end) d20,
                sum(case when day(tpapersource.fissuedate) = 21 then 1 else 0 end) d21,
                sum(case when day(tpapersource.fissuedate) = 22 then 1 else 0 end) d22,
                sum(case when day(tpapersource.fissuedate) = 23 then 1 else 0 end) d23,
                sum(case when day(tpapersource.fissuedate) = 24 then 1 else 0 end) d24,
                sum(case when day(tpapersource.fissuedate) = 25 then 1 else 0 end) d25,
                sum(case when day(tpapersource.fissuedate) = 26 then 1 else 0 end) d26,
                sum(case when day(tpapersource.fissuedate) = 27 then 1 else 0 end) d27,
                sum(case when day(tpapersource.fissuedate) = 28 then 1 else 0 end) d28,
                sum(case when day(tpapersource.fissuedate) = 29 then 1 else 0 end) d29,
                sum(case when day(tpapersource.fissuedate) = 30 then 1 else 0 end) d30,
                sum(case when day(tpapersource.fissuedate) = 31 then 1 else 0 end) d31
                from tmedia,tpapersource,tmedialabel,tlabel
                where tmedia.fid = tmedialabel.fmediaid
                and tmedialabel.flabelid = tlabel.flabelid 
                and tpapersource.fmediaid = tmedia.fid
                ".$monthMap1.$mediaNameMap.$labelMap.$regionMap."
                group by tmedia.fmedianame
                ) a on tmedia.fid = a.fid
                left join (
                select 
                tmedia.fid,
                sum(case when day(tpaperissue.fissuedate) = 1 then 1 else 0 end) a1,
                sum(case when day(tpaperissue.fissuedate) = 2 then 1 else 0 end) a2,
                sum(case when day(tpaperissue.fissuedate) = 3 then 1 else 0 end) a3,
                sum(case when day(tpaperissue.fissuedate) = 4 then 1 else 0 end) a4,
                sum(case when day(tpaperissue.fissuedate) = 5 then 1 else 0 end) a5,
                sum(case when day(tpaperissue.fissuedate) = 6 then 1 else 0 end) a6,
                sum(case when day(tpaperissue.fissuedate) = 7 then 1 else 0 end) a7,
                sum(case when day(tpaperissue.fissuedate) = 8 then 1 else 0 end) a8,
                sum(case when day(tpaperissue.fissuedate) = 9 then 1 else 0 end) a9,
                sum(case when day(tpaperissue.fissuedate) = 10 then 1 else 0 end) a10,
                sum(case when day(tpaperissue.fissuedate) = 11 then 1 else 0 end) a11,
                sum(case when day(tpaperissue.fissuedate) = 12 then 1 else 0 end) a12,
                sum(case when day(tpaperissue.fissuedate) = 13 then 1 else 0 end) a13,
                sum(case when day(tpaperissue.fissuedate) = 14 then 1 else 0 end) a14,
                sum(case when day(tpaperissue.fissuedate) = 15 then 1 else 0 end) a15,
                sum(case when day(tpaperissue.fissuedate) = 16 then 1 else 0 end) a16,
                sum(case when day(tpaperissue.fissuedate) = 17 then 1 else 0 end) a17,
                sum(case when day(tpaperissue.fissuedate) = 18 then 1 else 0 end) a18,
                sum(case when day(tpaperissue.fissuedate) = 19 then 1 else 0 end) a19,
                sum(case when day(tpaperissue.fissuedate) = 20 then 1 else 0 end) a20,
                sum(case when day(tpaperissue.fissuedate) = 21 then 1 else 0 end) a21,
                sum(case when day(tpaperissue.fissuedate) = 22 then 1 else 0 end) a22,
                sum(case when day(tpaperissue.fissuedate) = 23 then 1 else 0 end) a23,
                sum(case when day(tpaperissue.fissuedate) = 24 then 1 else 0 end) a24,
                sum(case when day(tpaperissue.fissuedate) = 25 then 1 else 0 end) a25,
                sum(case when day(tpaperissue.fissuedate) = 26 then 1 else 0 end) a26,
                sum(case when day(tpaperissue.fissuedate) = 27 then 1 else 0 end) a27,
                sum(case when day(tpaperissue.fissuedate) = 28 then 1 else 0 end) a28,
                sum(case when day(tpaperissue.fissuedate) = 29 then 1 else 0 end) a29,
                sum(case when day(tpaperissue.fissuedate) = 30 then 1 else 0 end) a30,
                sum(case when day(tpaperissue.fissuedate) = 31 then 1 else 0 end) a31
                from tmedia,tmedialabel,tlabel,tpaperissue 
                where tmedia.fid = tmedialabel.fmediaid
                and tmedialabel.flabelid = tlabel.flabelid 
                and tpaperissue.fmediaid = tmedia.fid
                ".$monthMap2.$labelMap."
                group by tmedia.fid) b on tmedia.fid = b.fid,tmedialabel,tlabel
                where tmedia.fid = tmedialabel.fmediaid
                and tmedialabel.flabelid = tlabel.flabelid 
                ".$mediaNameMap.$labelMap.$regionMap.$limit;

        $list = M("")->query($sql);

        $total = count($data);

        $data = [
            'total' => $count[0]['total'],
            'list' => $list
        ];
        $this->ajaxReturn([
            'code' => 0,
            'msg' => '获取成功！',
            'data'=>$data
        ]);

    }
    /**
     * @Des: 
     * @Date: 2019-11-18 17:46:09
     * @param {type} 
     * @return: 
     */
    public function exportExcel($data = []){
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "DataGather")             //设置文件的创建者
            ->setLastModifiedBy( "DataGather")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别
        $col = ['','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ'];
        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        $sheet ->setCellValue('A1','地区');
        $sheet ->setCellValue('B1','媒体名称');
        $sheet ->setCellValue('C1','周期');
        $sheet ->setCellValue('D1','类别');
        for($i=1;$i<=31;$i++){
            $sheet ->setCellValue($col[$i].'1',$i.'日');
        }
        $data_count = count($data);
        $list = $data['list'];
        $rows = 2*count($list)+2;
        foreach ($list as $key => $value) {
            foreach($value as $k=>$row){
                $t = substr($k,0,1);//d-版面数 a-广告数
                $n = substr($k,1);//日期
                $type = ['版面','广告'];
                if($k == 'region_name'){
                    $sheet ->mergeCells("A".(2*($key+1)).":A".(2*($key+1)+1));
                    $sheet ->setCellValue('A'.(2*($key+1)),$row);
                }
                if($k == 'fmedianame'){
                    $sheet ->mergeCells("B".(2*($key+1)).":B".(2*($key+1)+1));
                    $sheet ->setCellValue('B'.(2*($key+1)),$row);
                }
                if($k == 'fdescribe'){
                    $sheet ->mergeCells("C".(2*($key+1)).":C".(2*($key+1)+1));
                    $sheet ->setCellValue('C'.(2*($key+1)),$row);
                }
                if($k == 'fid'){
                    $sheet ->setCellValue('D'.($key+2),$type[$key%2]);
                    $sheet ->setCellValue('D'.($rows-1),$type[($rows-1)%2]);
                    $rows--;
                }
                if($t == 'd'){
                    $n = (int)$n;
                    $sheet ->setCellValue($col[$n].(2*($key+1)),$row);
                }
                if($t == 'a'){
                    $n = (int)$n;
                    $sheet ->setCellValue($col[$n].(2*($key+1)+1),$row);
                }
            }
        }
        $objActSheet =$objPHPExcel->getActiveSheet();
        $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $date = date('Ymdhis');
        $savefile = './Public/Xls/'.$date.'.xlsx';
        $savefile2 = '/Public/Xls/'.$date.'.xlsx';
        $objWriter->save($savefile);
        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件
        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
    }

    public function export_excel($data = []){

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        $sheet ->mergeCells("A1");//序号
        $sheet ->mergeCells("B1:B3");//管辖机构
        $sheet ->mergeCells("C1:C3");//媒体数量
        $sheet ->mergeCells("D1:D3");//线索数量

        //线索查看
        $sheet ->mergeCells("E1:F1");
        $sheet ->mergeCells("E1:E2");
        $sheet ->mergeCells("E1:F2");

        //线索复核
        $sheet ->mergeCells("G1:H1");
        $sheet ->mergeCells("G1:G2");
        $sheet ->mergeCells("G1:H2");

        $sheet ->mergeCells("I1:P1");//线索处理情况
        $sheet ->mergeCells("I2:J2");//线索处理
        $sheet ->mergeCells("K2:L2");//立案
        $sheet ->mergeCells("M2:P2");//线索处理方式


        $sheet ->setCellValue('A1','地区');
        $sheet ->setCellValue('B1','媒体名称');
        $sheet ->setCellValue('C1','周期');

        

        $sheet ->setCellValue('D1','线索数量');
        $sheet ->setCellValue('E1','线索查看');
        $sheet ->setCellValue('E3','查看量');
        $sheet ->setCellValue('F3','查看率');

        $sheet ->setCellValue('G1','线索复核');
        $sheet ->setCellValue('G3','通过');
        $sheet ->setCellValue('H3','未通过');

        $sheet ->setCellValue('I1','线索处理情况');
        $sheet ->setCellValue('I2','线索处理');
        $sheet ->setCellValue('I3','处理量');
        $sheet ->setCellValue('j3','处理率');

        $sheet ->setCellValue('K2','立案');
        $sheet ->setCellValue('K3','已立案');
        $sheet ->setCellValue('L3','已结案');

        $sheet ->setCellValue('M2','线索处理方式');
        $sheet ->setCellValue('M3','责令停止发布');
        $sheet ->setCellValue('N3','立案');
        $sheet ->setCellValue('O3','移交司法机关');
        $sheet ->setCellValue('p3','其他');
        $data_count = count($res);
        foreach ($res as $key => $value) {

            $sheet ->setCellValue('A'.($key+4),($key+1));
            if($data_count == $key){
                $sheet ->setCellValue('B'.($key+4),'合计');
            }else{
                $sheet ->setCellValue('B'.($key+4),$value['region_name']);
            }
            $sheet ->setCellValue('C'.($key+4),$value['media_count']);
            $sheet ->setCellValue('D'.($key+4),$value['cule_count']);
            $sheet ->setCellValue('E'.($key+4),$value['ck_count']);
            $sheet ->setCellValue('F'.($key+4),$value['ckl']);
            $sheet ->setCellValue('G'.($key+4),$value['pass_count']);
            $sheet ->setCellValue('H'.($key+4),$value['unpass_count']);
            $sheet ->setCellValue('I'.($key+4),$value['cl_count']);
            $sheet ->setCellValue('J'.($key+4),$value['cll']);
            $sheet ->setCellValue('K'.($key+4),$value['yla']);
            $sheet ->setCellValue('L'.($key+4),$value['yja']);
            $sheet ->setCellValue('M'.($key+4),$value['tzfb']);
            $sheet ->setCellValue('N'.($key+4),$value['lian']);
            $sheet ->setCellValue('O'.($key+4),$value['yjsf']);
            $sheet ->setCellValue('P'.($key+4),$value['others']);

        }
        $objActSheet =$objPHPExcel->getActiveSheet();
        $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $date = date('Ymdhis');
        $savefile = './Public/Xls/'.$date.'.xlsx';
        $savefile2 = '/Public/Xls/'.$date.'.xlsx';
        $objWriter->save($savefile);
        //即时导出下载
        /*          header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition:attachment;filename="'.$date.'.xlsx"');
                    header('Cache-Control:max-age=0');
                    $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
                    $objWriter->save( 'php://output');*/
        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件
        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        //chr(61)
    }

    public function mergeCells($lu = '65',$ru = '66',$ld = '',$rd = ''){

    }

    /**
     * 获取行政区划树
     * @return array
     */
    protected function getRegionTree()
    {
        $data = M('tregion')->field('fid value, fpid, fname label')->cache(true, 60)->where(['fstate' => ['EQ', 1], 'fid' => ['NEQ', 100000]])->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpid']])){
                $items[$value['fpid']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }

}