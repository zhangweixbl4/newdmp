<?php
namespace Agp\Controller;
use Think\Controller;

import('Vendor.PHPExcel');

class FoutxlsController extends BaseController{
	 /**
	 * 导出信用数据，匹配Fxinyong控制器
	 * by zw
	 */
    public function xinyong_outxls(){
    	session_write_close();
    	$system_num = getconfig('system_num');
    	
	    $objPHPExcel = new \PHPExcel();  
	    $objPHPExcel
	      ->getProperties()  //获得文件属性对象，给下文提供设置资源
	      ->setCreator( "MaartenBalliauw")             //设置文件的创建者
	      ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
	      ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
	      ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
	      ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
	      ->setKeywords( "office 2007 openxmlphp")        //设置标记
	      ->setCategory( "Test resultfile");                //设置类别

		$medianame 	= I('medianame');//媒体名称
		$n 			= I('n');//年
		$y 			= I('y');//月
		$fmtype		= I('fmtype');//媒体类型
		$regionid	= I('area');//地区

		if(!empty($fmtype)){//媒体类型
			$where['fmtype'] = $fmtype;
		}
		if(!empty($medianame)){//媒体名称
			$where['tmedia.fmedianame'] = array('like','%'.$medianame.'%');
		}
		if(!empty($n)){//年份
			$where['n'] = $n;
		}
		if(!empty($y)){//月份
			$where['y'] = $y;
		}
		if(!empty($regionid)){
			$where['tregion.fid'] = $regionid;
		}

		$data = M('tmedia_credit')
			->field('tmedia_credit.*, (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tregion.fname1')
			->join('tmedia on tmedia_credit.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
			->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->join('tmediaowner on tmediaowner.fid=tmedia.fmediaownerid')
			->join('tregion on tmediaowner.fregionid=tregion.fid')
			->where($where)
			->order('fname1 asc,fmedianame asc,n desc, y desc')
			->select();

		$upname = session('regulatorpersonInfo.regulatorpname').'广告监管平台';

		$sheet = $objPHPExcel->setActiveSheetIndex(0);
		$sheet -> mergeCells('A1:I1');
		$sheet ->getStyle('A1:I1')->applyFromArray(array('font' => array ('bold' => true),'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
		$sheet ->setCellValue('A1',$upname.'-信用评价清单');

		$sheet ->setCellValue('A2','序号');
		$sheet ->setCellValue('B2','地区');
		$sheet ->setCellValue('C2','媒体名称');
		$sheet ->setCellValue('D2','年份');
		$sheet ->setCellValue('E2','月份');
		$sheet ->setCellValue('F2','信用评价期初分');
		$sheet ->setCellValue('G2','本月信用评价扣分');
		$sheet ->setCellValue('H2','奖惩分');
		$sheet ->setCellValue('I2','信用评价期末分');
		foreach ($data as $key => $value) {	
			$sheet ->setCellValue('A'.($key+3),$key+1);
			$sheet ->setCellValue('B'.($key+3),$value['fname1']);
			$sheet ->setCellValue('C'.($key+3),$value['fmedianame']);
			$sheet ->setCellValue('D'.($key+3),$value['n'].'年');
			$sheet ->setCellValue('E'.($key+3),$value['y'].'月');
			$sheet ->setCellValue('F'.($key+3),$value['xypjqcf']);
			$sheet ->setCellValue('G'.($key+3),$value['byxypjkf']);
			$sheet ->setCellValue('H'.($key+3),$value['jzf']);
			$sheet ->setCellValue('I'.($key+3),$value['xypjqmf']);
		}

		$objActSheet =$objPHPExcel->getActiveSheet();
		//给当前活动的表设置名称
		$objActSheet->setTitle('Sheet1');

		$objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$date = date('Ymdhis');
		$savefile = './Public/Xls/'.$date.'.xlsx';
		$objWriter->save($savefile);

		$ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

		D('Function')->write_log('信用数据',1,'生成成功');
		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
    }

    /**
	 * 导出统计分析情况-自定义查询结果
	 * by zw
	 */
    public function out_tjfx_zdycjg_list(){
    	session_write_close();
    	$ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $ischeck = $ALL_CONFIG['ischeck'];
    	
	    $objPHPExcel = new \PHPExcel();  
	    $objPHPExcel
	      ->getProperties()  //获得文件属性对象，给下文提供设置资源
	      ->setCreator( "MaartenBalliauw")             //设置文件的创建者
	      ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
	      ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
	      ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
	      ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
	      ->setKeywords( "office 2007 openxmlphp")        //设置标记
	      ->setCategory( "Test resultfile");                //设置类别

	    $regionids    = I('regionids');//行政区划组
	    $tadclass     = I('tadclass');//广告类别组
	    $mediaclass   = I('mediaclass');//媒体类别组
	    $mediaids     = I('mediaids');//媒体id组
	    $hstarttime   = I('hstarttime');//发布开始时间，天
	    $hendtime     = I('hendtime');//发布结束时间，天
	    $sstarttime   = I('sstarttime');//发布开始时间，秒
	    $sendtime     = I('sendtime');//发布结束时间，秒
	    $tadname      = I('tadname');//广告名称
	    $where_tia = '1=1';
	    //是否抽查模式
		if(!empty($ischeck)){
		    $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
		    //如果抽查表有数据
		    if(!empty($spot_check_data)){
		        $dates = [];//定义日期数组
		        foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
		            $year_month = '';
		            $date_str = [];
		            $year_month = substr($spot_check_data_val['fmonth'],0,7);
		            if(!empty($spot_check_data_val['condition'])){
		            	$date_str = explode(',',$spot_check_data_val['condition']);
		            }
		            foreach ($date_str as $date_str_val){
		                $dates[] = $year_month.'-'.$date_str_val;
		            }
		        }
				$where_tia 	.= ' and a.fissue_date in ("'.implode('","', $dates).'")';
		    }else{
		        $where_tia 	.= ' and 1=0';
		    }
		}

	    if(!empty($regionids)){
	      $where_tia .= ' and b.fregion_id in('.implode(',', $regionids).')';
	    }
	    if(!empty($tadclass)){
	      $where_tia .= ' and left(b.fad_class_code,2) in('.implode(',', $tadclass).')';
	    }
	    if(!empty($mediaclass)){
	      $where_tia .= ' and b.fmedia_class in('.implode(',', $mediaclass).')';
	    }
	    if(!empty($mediaids)){
	      $where_tia .= ' and b.fmedia_id in('.implode(',', $mediaids).')';
	    }
	    if(!empty($tadname)){
	      $where_tia .= ' and b.fad_name like "%'.$tadname.'%"';
	    }
	    if(!empty($hstarttime)||!empty($hendtime)){
	      $hstarttime = $hstarttime?$hstarttime:date('Y-m-d');
	      $hendtime   = $hendtime?$hendtime:date('Y-m-d');
	      $where_tia .= ' and a.fissue_date BETWEEN "'.$hstarttime.'" and "'.$hendtime.'"';
	    }
	    if(!empty($sstarttime)||!empty($sendtime)){
	      $sstarttime = $sstarttime?date('H:i:s',strtotime($sstarttime)):date('H:i:s');
	      $sendtime   = $sendtime?date('H:i:s',strtotime($sendtime)):date('H:i:s');
	      $where_tia .= ' and a.fstarttime>="'.$sstarttime.'" and a.fendtime<="'.$sendtime.'"';
	    }

	    $upname = session('regulatorpersonInfo.regulatorpname').'广告监管平台';

		$sheet = $objPHPExcel->setActiveSheetIndex(0);
		$sheet -> mergeCells('A1:K1');
		$sheet ->getStyle('A1:K1')->applyFromArray(array('font' => array ('bold' => true),'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
		$sheet ->setCellValue('A1',$upname.'-自定义查询结果');

	    $data = M('tbn_illegal_ad_issue')
	      ->alias('a')
	      ->field('b.fid,a.fissue_date,a.fstarttime,a.fendtime,b.fad_name,a.fpage,a.fmedia_class,(case when a.fmedia_class=1 then "电视" when a.fmedia_class=2 then "广播" when a.fmedia_class=3 then "报纸" else "" end) as fmedia_class2,d.ffullname as fadclass, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,b.fillegal,b.favifilename,b.fjpgfilename,(UNIX_TIMESTAMP(a.fendtime)-UNIX_TIMESTAMP(a.fstarttime)) as difftime')
	      ->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')
	      ->join('tmedia c on c.fid=a.fmedia_id and c.fid=c.main_media_id')
	      ->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on snd.illad_id=b.fid')
	      ->join('tadclass d on b.fad_class_code=d.fcode')
	      ->where($where_tia)
	      ->order('a.fid desc')
	      ->select();
		$sheet ->setCellValue('A2','序号');
		$sheet ->setCellValue('B2','媒体分类');
		$sheet ->setCellValue('C2','广告分类');
		$sheet ->setCellValue('D2','广告名称');
		$sheet ->setCellValue('E2','发布媒体');
		$sheet ->setCellValue('F2','播出时间');
		$sheet ->setCellValue('G2','开始时间');
		$sheet ->setCellValue('H2','结束时间');
		$sheet ->setCellValue('I2','时长/版面');
		$sheet ->setCellValue('J2','涉嫌违法原因');
		$sheet ->setCellValue('K2','证据下载地址');
		foreach ($data as $key => $value) {	
			$sheet ->setCellValue('A'.($key+3),$key+1);
			$sheet ->setCellValue('B'.($key+3),$value['fmedia_class2']);
			$sheet ->setCellValue('C'.($key+3),$value['fadclass']);
			$sheet ->setCellValue('D'.($key+3),$value['fad_name']);
			$sheet ->setCellValue('E'.($key+3),$value['fmedianame']);
			$sheet ->setCellValue('F'.($key+3),$value['fissue_date']);
			$sheet ->setCellValue('G'.($key+3),$value['fstarttime']);
			$sheet ->setCellValue('H'.($key+3),$value['fendtime']);
			if($value['fmedia_class']==3){
				$sheet ->setCellValue('I'.($key+3),$value['fpage']);
			}else{
				$sheet ->setCellValue('I'.($key+3),$value['difftime'].'秒');
			}
			$sheet ->setCellValue('J'.($key+3),$value['fillegal']);
			if($value['fmedia_class']==1||$value['fmedia_class']==2){
				$sheet ->setCellValue('K'.($key+3),$value['favifilename']);
			}else{
				$sheet ->setCellValue('K'.($key+3),$value['fjpgfilename']);
			}
		}

		$objActSheet =$objPHPExcel->getActiveSheet();
		//给当前活动的表设置名称
		$objActSheet->setTitle('Sheet1');

		$objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$date = date('Ymdhis');
		$savefile = './Public/Xls/'.$date.'.xlsx';
		$objWriter->save($savefile);

		$ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

		D('Function')->write_log('自定义查询',1,'生成成功');
		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		//即时导出下载
		// header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// header('Content-Disposition:attachment;filename="01simple.xlsx"');
		// header('Cache-Control:max-age=0');
		// $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
		// $objWriter->save( 'php://output');
    }

    /**
	 * 导出统计分析情况-广告名称查询结果
	 * by zw
	 */
    public function out_tjfx_zdycjg_list2(){
    	session_write_close();
    	$ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $ischeck = $ALL_CONFIG['ischeck'];
        $isrelease = $ALL_CONFIG['isrelease'];

    	//国家局系统只显示有打国家局标签媒体的数据
	    if($system_num == '100000'){
			if(session('regulatorpersonInfo.fregulatorlevel')!=30){
				$regionid = session('regulatorpersonInfo.regionid');//机构行政区划ID
				$regionid = substr($regionid , 0 , 2);
				$where['tia.fregion_id'] = array('like',$regionid.'%');
			}
	    }
	    if(!empty($isrelease)){
			$where['_string']   = ' tiai.fsend_status = 2';
	    }
    	
	    $objPHPExcel = new \PHPExcel();  
	    $objPHPExcel
	      ->getProperties()  //获得文件属性对象，给下文提供设置资源
	      ->setCreator( "MaartenBalliauw")             //设置文件的创建者
	      ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
	      ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
	      ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
	      ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
	      ->setKeywords( "office 2007 openxmlphp")        //设置标记
	      ->setCategory( "Test resultfile");                //设置类别

		$ad_name 	= I('name');//广告名
		$start_date = I('start_date');//开始日期
		$end_date 	= I('end_date');//结束日期
	    if($ad_name){
			$where['tia.fad_name'] = array('like','%'.$ad_name.'%');
		}
		if($start_date && $end_date){
			$where['tiai.fissue_date'] = array('between',array($start_date,$end_date));
		}
		//是否抽查模式
		if(!empty($ischeck)){
		    $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
		    //如果抽查表有数据
		    if(!empty($spot_check_data)){
		        $dates = [];//定义日期数组
		        foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
		            $year_month = '';
		            $date_str = [];
		            $year_month = substr($spot_check_data_val['fmonth'],0,7);
		            if(!empty($spot_check_data_val['condition'])){
		            	$date_str = explode(',',$spot_check_data_val['condition']);
		            }
		            foreach ($date_str as $date_str_val){
		                $dates[] = $year_month.'-'.$date_str_val;
		            }
		        }
				$where['_string'] 	.= ' tbn_illegal_ad_issue.fissue_date in ("'.implode('","', $dates).'")';
		    }else{
		        $where['_string'] 	.= ' 1=0';
		    }
		}
		
	    $upname = session('regulatorpersonInfo.regulatorpname').'广告监管平台';

		$sheet = $objPHPExcel->setActiveSheetIndex(0);
		$sheet -> mergeCells('A1:K1');
		$sheet ->getStyle('A1:K1')->applyFromArray(array('font' => array ('bold' => true),'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
		$sheet ->setCellValue('A1',$upname.'-广告名称查询结果');

	    $data = M('tbn_illegal_ad_issue')
			->alias('tiai')
			->field('
				tia.fid,
				tia.fmedia_class,
				(case when tiai.fmedia_class=1 then "电视" when tiai.fmedia_class=2 then "广播" when tiai.fmedia_class=3 then "报纸" else "" end) as fmedia_class2,
				tc.ffullname as fadclass,
				tia.fad_name,
				 (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,
				tiai.fissue_date,
				tiai.fstarttime,
				tiai.fendtime,
				tiai.fpage,
				tia.fillegal,
				tia.favifilename,
				tia.fjpgfilename,
				(UNIX_TIMESTAMP(tiai.fendtime)-UNIX_TIMESTAMP(tiai.fstarttime)) as difftime
			')
			->join('tbn_illegal_ad tia on tiai.fillegal_ad_id = tia.fid and fcustomer="'.$system_num.'"')
			->join('tmedia tm on tiai.fmedia_id = tm.fid and tm.fid=tm.main_media_id')
			->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on snd.illad_id=tia.fid')
			->join('tregion tn on tia.fregion_id=tn.fid'.$wherestr)
			->join('tadclass tc on tia.fad_class_code = tc.fcode')
			->where($where)
			->select();

		$sheet ->setCellValue('A2','序号');
		$sheet ->setCellValue('B2','媒体分类');
		$sheet ->setCellValue('C2','广告分类');
		$sheet ->setCellValue('D2','广告名称');
		$sheet ->setCellValue('E2','发布媒体');
		$sheet ->setCellValue('F2','播出时间');
		$sheet ->setCellValue('G2','开始时间');
		$sheet ->setCellValue('H2','结束时间');
		$sheet ->setCellValue('I2','时长/版面');
		$sheet ->setCellValue('J2','涉嫌违法原因');
		$sheet ->setCellValue('K2','证据下载地址');
		foreach ($data as $key => $value) {	
			$sheet ->setCellValue('A'.($key+3),$key+1);
			$sheet ->setCellValue('B'.($key+3),$value['fmedia_class2']);
			$sheet ->setCellValue('C'.($key+3),$value['fadclass']);
			$sheet ->setCellValue('D'.($key+3),$value['fad_name']);
			$sheet ->setCellValue('E'.($key+3),$value['fmedianame']);
			$sheet ->setCellValue('F'.($key+3),$value['fissue_date']);
			$sheet ->setCellValue('G'.($key+3),$value['fstarttime']);
			$sheet ->setCellValue('H'.($key+3),$value['fendtime']);
			if($value['fmedia_class']==3){
				$sheet ->setCellValue('I'.($key+3),$value['fpage']);
			}else{
				$sheet ->setCellValue('I'.($key+3),$value['difftime'].'秒');
			}
			$sheet ->setCellValue('J'.($key+3),$value['fillegal']);
			if($value['fmedia_class']==1||$value['fmedia_class']==2){
				$sheet ->setCellValue('K'.($key+3),$value['favifilename']);
			}else{
				$sheet ->setCellValue('K'.($key+3),$value['fjpgfilename']);
			}
		}

		$objActSheet =$objPHPExcel->getActiveSheet();
		//给当前活动的表设置名称
		$objActSheet->setTitle('Sheet1');

		$objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$date = date('Ymdhis');
		$savefile = './Public/Xls/'.$date.'.xlsx';
		$objWriter->save($savefile);

		$ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

		D('Function')->write_log('广告名称查询',1,'生成成功');
		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		//即时导出下载
		// header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// header('Content-Disposition:attachment;filename="01simple.xlsx"');
		// header('Cache-Control:max-age=0');
		// $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
		// $objWriter->save( 'php://output');
    }
}