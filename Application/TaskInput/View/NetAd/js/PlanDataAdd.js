Vue.prototype.$ELEMENT = {
    size: 'mini',
    zIndex: 3000
  }
  const vm = new Vue({
    el: "#app",
    data() {
      return {
        form: {
          fadname: '',
          fadowner: '',
          fstatus: '0',
          adilltype: '-1',
          limit: '',
          adtype: '',
        },
        adtype: [],
        date: [],
        loading: true,
        page: {
          pageIndex: 1,
          pageSize: 20,
        },
        show: false,
        tableData: [],
        allcount: 0,
        multipleSelection: [],
        tableHeight: '400px',
        fstatus: {
          '1': {
            label: '截图中',
            type: 'primary',
          },
          '2': {
            label: '截图完成',
            type: 'success',
          },
          '3': {
            label: '截图失败',
            type: 'danger',
          },
          '8': {
            label: '待检查',
            type: 'danger',
          },
          '4': {
            label: '待补录',
            type: 'danger',
          },
          '5': {
            label: '补录中',
            type: 'primary',
          },
          '6': {
            label: '通过',
            type: 'success',
          },
          '7': {
            label: '作废',
            type: 'danger',
          },
          '14': {
            label: '识别中',
            type: 'warning',
          },
        },
        fstatus2: {
          '1': {
            label: '截图中',
            type: 'primary',
          },
          '2': {
            label: '截图完成',
            type: 'danger',
          }
        },
        adilltype: {
          '0': '不违法',
          '30': '违法',
        },
        limitNumber: [
          {
            value: '100',
            label: '100条'
          },
          {
            value: '200',
            label: '200条'
          },
          {
            value: '500',
            label: '500条'
          },
          {
            value: '1000',
            label: '1000条'
          },
          {
            value: '5000',
            label: '5000条'
          },
        ],
        AdClassArr: []
      }
    },
    computed: {
      taskId() {
        return this.GetQueryString('id')
      },
      dataType() {
        return this.GetQueryParame('dataType')
      }
    },
    created() {
      this.getList()
      this.adtypeList()
    },
    mounted() {
      this.$nextTick(() => {
        this.getTableHeight()
        document.querySelector('#app').style.opacity = 1
      })
      window.onresize = () => {
        this.getTableHeight()
      }
    },
    methods: {
      getList(page) {
        if(page) this.page.pageIndex = page
        let [sttime, endtime ] = this.date || []
        let data = {
          taskid: this.taskId,
          dataType: this.dataType,
          sttime,
          endtime,
          ...this.form,
          ...this.page
        }
        this.loading = true
        $.post('/TaskInput/NetAd/PlanDataAddList',JSON.stringify(data))
        .then(res => {
          if (res.code === 0) {
            this.tableData = res.data
            this.allcount = res.count
          } else {
            this.$notify.error({
              title: '错误',
              message: res.msg,
              duration: 1000
            });
          }
          this.loading = false
        })
      },
      changePriority(addCheck) {
        let ids = addCheck ? this.multipleSelection.map(i => i.id) : []
        let length = ids.length
        let [sttime, endtime ] = this.date || []
        let str = !addCheck ? '确认添加所有搜索结果？' : `确认添加勾选的 ${length} 条数据？`
        let data = {
          sttime,
          endtime,
          taskid: this.taskId,
          dataType: this.dataType,
          selad: ids,
          ...this.form,
        }
        this.$confirm(str, '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          this.loading = true
          $.post('/TaskInput/NetAd/PlanDataAddAction', JSON.stringify(data))
            .then(res => {
              if (res.code === 0) {
                this.loading = false
                this.multipleSelection = []
                this.$notify({
                  message: res.msg,
                  type: 'success',
                  duration: 1000
                });
                this.getList()
              } else {
                this.$notify.error({
                  title: '错误',
                  message: res.msg,
                  duration: 1000
                });
                this.loading = false
              }
          })
        }).catch(() => {
        })
      },
      getTableHeight() {
        this.tableHeight = `${document.body.clientHeight - 146}px`
      },
      GetQueryString(name){
        let reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)")
        let r = window.location.search.substr(1).match(reg)
        if (r != null) return unescape(r[2])
        return null
      },
      GetQueryParame(dataType){
        let reg = new RegExp("(^|&)"+ dataType +"=([^&]*)(&|$)")
        let r = window.location.search.substr(1).match(reg)
        if (r != null) return unescape(r[2])
        return null
      },
      handleSelectionChange(val) {
        this.multipleSelection = val
      },
      // 广告分类列表
      adtypeList() {
        $.post('/TaskInput/NetAd/AdClassArr')
        .then((res) => {
          this.AdClassArr = res.data
        })
      },
      handleChange(value) {
        this.form.adtype = [...value].pop()
      }
    },
  })