<?php
//电视广告发行控制器
namespace Gongshang\Controller;
use Think\Controller;
class SpecialTvissueController extends BaseController {

    public function index(){
		$regulatorpersonInfo = M('tregulatorperson')->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
		$regulatorInfo = M('tregulator')->where(array('fid'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$keyword = I('keyword');//搜索关键词
		$fadname = I('fadname');// 广告名称
		$fversion = I('fversion');// 版本描述
		$fmediaid  = I('fmediaid');// 媒体ID
		$adclass_code = I('adclass_code');//广告类别
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID
		$fstarttime_s = I('fstarttime_s');// 发布日期

		if($fstarttime_s == ''){
			$fstarttime_s = date('Y-m-d');
			$_GET['fstarttime_s'] = $fstarttime_s;
		}	
		$fstarttime_e = I('fstarttime_e');// 发布日期
		if($fstarttime_e == ''){
			$fstarttime_e = date('Y-m-d');
			$_GET['fstarttime_e'] = $fstarttime_e;
		}

		$region_id = session('regulatorpersonInfo.regionid');//地区ID
		if(I('region_id') != '') $region_id = I('region_id');
		$this_region_id = I('this_region_id');//是否只查询本级地区

		$where = array();//查询条件

		if($fmediaid == ''){
			//是否有指定的媒体
			$media=$this->get_access_media();//获取权限判断后的最终媒体列表
			if($media){
				$where['ttvissue.fmediaid'] = array('in', $media);//发布媒介
			}
		}else{
			$where['ttvissue.fmediaid'] = $fmediaid;//发布媒介
		}

		//如果只查询当前级
		/* if($this_region_id == 'true'){
			$where['tmediaowner.fregionid'] = $region_id;//地区搜索条件
		}else{
			if($region_id !=100000 && (I('region_id') ||empty($media))){//不是国家工商局并且有地区检索或者没有media
				$region_id_rtrim = rtrim($region_id, '00');//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmediaowner.fregionid,' . $region_id_strlen . ')'] = $region_id_rtrim;//地区搜索条件
			}
		} */
		
		
		if($region_id > 0){
			if($this_region_id == 'true'){
				$where['tmediaowner.fregionid'] = $region_id;//地区搜索条件
			}elseif($region_id != 100000){
				$region_id_rtrim = A('Common/System','Model')->get_region_left($region_id);//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmediaowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}
			$media_owner_join = 'tmediaowner on tmediaowner.fid = tmedia.fmediaownerid';
			
		}
		
		
		
		
		
		
		$where['ttvissue.fstate'] = array('neq',-1);

		//如果没有检索$adclass_code 并且有special_adclass
		if(session('regulatorpersonInfo.special_adclass') &&empty($adclass_code)){

			//$where['tadclass.fcode|tadclass.fpcode'] = array('in',session('regulatorpersonInfo.special_adclass'));//code或者pcode 是special_adclass
			
			$adidList = array();
			foreach(explode(',',session('regulatorpersonInfo.special_adclass')) as $special_adclass){
				//var_dump($special_adclass);
				$adclass_code_strlen = strlen($special_adclass);//获取code长度
				$adidList = array_merge($adidList,M('tad')->cache(true,6000)->where(array('left(tad.fadclasscode,'.$adclass_code_strlen.')'=>$special_adclass))->getField('fadid',true));//广告id
				
				
			}
			if($adidList){
				$where['ttvsample.fadid'] = array('in',$adidList);
			}
			
			
		}
		//如果有检索$adclass_code
		if(!empty($adclass_code)){
			
			$adclass_code_strlen = strlen($adclass_code);//获取code长度
			
			$where['ttvsample.fadid'] = array('exp',"in (select fadid from tad where left(tad.fadclasscode,".$adclass_code_strlen.") = '".$adclass_code."')");//广告id
		}

		if($keyword != ''){
			//$where['ttvissue.fversion'] = array('like','%'.$keyword.'%');
		} 
	
		if($fadname != ''){
			//$where['tad.fadname'] = array('like','%'.$fadname.'%');//广告名称
			$where['ttvsample.fadid'] = array('exp',"in (select fadid from tad where fadname like '%".$fadname."%')");//广告名称
		}
 

		if($fversion != ''){
			$where['ttvsample.fversion'] = array('like','%'.$fversion.'%');//版本描述
		}

		if(is_array($fillegaltypecode)){
			$where['ttvsample.fillegaltypecode'] = array('in',$fillegaltypecode);//违法类型代码
		}
		if($fstarttime_s != '' || $fstarttime_e != ''){
			
			$where['ttvissue.fissuedate'] = array('between',$fstarttime_s.','.date('Y-m-d',strtotime($fstarttime_e)));
		}

		$count = M('ttvissue')->cache(true,6000)
								->join('ttvsample on ttvsample.fid = ttvissue.ftvsampleid')
								//->join('tad on tad.fadid = ttvsample.fadid')
								//->join('tadclass on tadclass.fcode = tad.fadclasscode')//广告内容列别
								->join('tmedia on tmedia.fid = ttvissue.fmediaid')
								->join($media_owner_join)
								//->join('tillegaltype on tillegaltype.fcode = ttvsample.fillegaltypecode')
								//->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数

		$tvissueList = M('ttvissue')->cache(true,6000)
										->field('
													ttvissue.*,
													
													(select fadname from tad where fadid = ttvsample.fadid) as fadname,
													ttvsample.fversion,
													ttvsample.favifilename,
													tmedia.fmedianame,
													
													(select fillegaltype from tillegaltype where fcode = ttvsample.fillegaltypecode) as fillegaltype,
													
													(select tadclass.ffullname from tadclass join tad on tad.fadclasscode = tadclass.fcode where tad.fadid = ttvsample.fadid) as adclass_code,
													
													
													ttvsample.fadmanuno,
													ttvsample.fmanuno,
													ttvsample.fadapprno,
													ttvsample.fapprno,
													ttvsample.fadent,
													ttvsample.fent,
													ttvsample.fentzone,
													ttvsample.fexpressioncodes,
													ttvsample.fexpressions
										')
										->join('ttvsample on ttvsample.fid = ttvissue.ftvsampleid')
										//->join('tad on tad.fadid = ttvsample.fadid')
										//->join('tadclass on tadclass.fcode = tad.fadclasscode')//广告内容列别
										->join('tmedia on tmedia.fid = ttvissue.fmediaid')
										->join($media_owner_join)
										//->join('tillegaltype on tillegaltype.fcode = ttvsample.fillegaltypecode')
										//->join('tregion on tregion.fid = tmediaowner.fregionid')
										->where($where)
										->order('ttvissue.fstarttime desc') 
										->limit($Page->firstRow.','.$Page->listRows)->select();//查询广告样本列表
		//var_dump(M('ttvissue')->getLastSql());								
		$tvissueList = list_k($tvissueList,$p,$pp);//为列表加上序号
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		//var_dump($tvissueList);
		$this->assign('regionid',session('regulatorpersonInfo.regionid'));
		$this->assign('tvissueList',$tvissueList);//广告发布列表
		$this->assign('illegaltype',$illegaltype);//违法类型
		$this->assign('regulatorInfo',$regulatorInfo);//监管机构信息
		$this->assign('page',$Page->show());//分页输出
		// $this->display();
	}

	
	
	/*广告发布详情*/
	public function ajax_tvissue_details(){
		
		$ftvissueid = I('ftvissueid');//获取发布ID
		$ftvissueid = intval($ftvissueid);
		$tvissueDetails = M('ttvissue')
					->field('ttvissue.*,tad.fadname,ttvsample.fversion,tmedia.fmedianame,tadowner.fname as adowner,
						ttvsample.fadmanuno,ttvsample.fmanuno,ttvsample.fadapprno,ttvsample.fapprno,ttvsample.fadent,ttvsample.fent,ttvsample.fentzone,
						tillegaltype.fillegaltype,
						ttvsample.fillegalcontent,ttvsample.fexpressioncodes,ttvsample.fexpressions
						')

					->join('ttvsample on ttvsample.fid = ttvissue.ftvsampleid')
					->join('tad on tad.fadid = ttvsample.fadid')
					->join('tadowner on tadowner.fid = tad.fadowner')
					->join('tillegaltype on tillegaltype.fcode = ttvsample.fillegaltypecode')
					->join('tmedia on tmedia.fid = ttvissue.fmediaid')
					->where(array('ttvissue.ftvissueid'=>$ftvissueid))
					->find();//查询样本详情
		//var_dump(M('ttvissue')->getLastSql());

		$fmp4url = strval($tvissueDetails['fmp4url']);

		$mp4url_state = strval($tvissueDetails['mp4url_state']);
		if($tvissueDetails['validity_time'] < time()){
			$mp4url_state = '0';
		}

		$ad_part = array(
			'fmp4url'=>$fmp4url,
			'mp4url_state'=>$mp4url_state,

		);
		$this->ajaxReturn(array('code'=>0,'tvissueDetails'=>$tvissueDetails,'ad_part'=>$ad_part));

	}
}