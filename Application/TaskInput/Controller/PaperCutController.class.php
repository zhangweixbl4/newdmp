<?php
namespace InputPc\Controller;
use Think\Controller;
class PaperCutController extends BaseController {
	

	

    public function index(){
		
		$wx_id = intval(session('wx_id'));	
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$keyword = I('keyword');
		$fstate = I('fstate');
		$fissuedate_s = I('fissuedate_s');
		if($fissuedate_s == ''){
			$fissuedate_s = date('Y-m-d',time()-7*86400);
			$_GET['fissuedate_s'] = $fissuedate_s;
		}	
		$fissuedate_s = date('Y-m-d',strtotime($fissuedate_s));
		
		$fissuedate_e = I('fissuedate_e');
		if($fissuedate_e == ''){
			$fissuedate_e = date('Y-m-d');
			$_GET['fissuedate_e'] = $fissuedate_e;
		}	
		$fissuedate_e = date('Y-m-d',strtotime($fissuedate_e));
		$fpage = I('fpage');//版面
		
		cookie('paper_cut_get_next_task',array('keyword'=>$keyword,'fissuedate_s'=>$fissuedate_s,'fissuedate_e'=>$fissuedate_e,'fpage'=>$fpage));

		$where = array();
		
		$where['_string'] .= '(tpapersource.fstate = 0 or ';//未剪辑的
		$where['_string'] .= 'tpapersource.fuserid = '.$wx_id.') ';//自己剪辑的
		$where['_string'] .= A('InputPc/Task','Model')->paper_cut_authority($wx_id);//数据权限控制
		$where['fissuedate'] = array('between',array($fissuedate_s,$fissuedate_e));
		if($keyword != ''){
			$where['tmedia.fmedianame'] = array('like','%'.$keyword.'%');
		}
		if($fstate != ''){
			$where['tpapersource.fstate'] = intval($fstate);
		}
		if($fpage != ''){
			$where['tpapersource.fpage'] = array('like','%'.$fpage.'%');
		}
		
		
		$count = M('tpapersource')
								->join('tmedia on tmedia.fid = tpapersource.fmediaid')
								//->join('tmedialabel on tmedialabel.fmediaid = tmedia.fid and tmedialabel.flabelid = 242')
								->where($where)->count();
		
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$sourceList = M('tpapersource')
								->field('
										tpapersource.*,
										tmedia.fmedianame
										
										
										')
								->join('tmedia on tmedia.fid = tpapersource.fmediaid')
								//->join('tmedialabel on tmedialabel.fmediaid = tmedia.fid and tmedialabel.flabelid = 242')
								->order('( case when tpapersource.fstate=6 then 1 ELSE 4 END),tpapersource.fissuedate desc,rand(),tmedia.fmedianame')
								->limit($Page->firstRow.','.$Page->listRows)
								->where($where)->select();
		
		//var_dump(M('tpapersource')->getLastSql());
		
		
		
		
		$sourceList = list_k($sourceList,$p,$pp);//为列表加上序号
		
		
		$todayTaskCount = A('InputPc/PaperCut','Model')->get_today_paper_task_count();
		
		$this->assign('todayTaskCount',$todayTaskCount);
		$this->assign('sourceList',$sourceList);
		
		$this->assign('wx_id',$wx_id);
		
		
		$this->assign('page',$Page->show());//分页输出
		//exit;
		$this->display();
	}
	
	
	public function ad_cut(){
		if(	!A('InputPc/Task','Model')->user_other_authority('1003')){
			exit('无权限');
		}
		$fid = I('fid');//获取素材ID
		$wx_id = intval(session('wx_id'));

		$papersourceDetails = M('tpapersource')
										->field('tpapersource.*,tmedia.fmedianame')
										->join('tmedia on tmedia.fid = tpapersource.fmediaid')
										->where(array('tpapersource.fid'=>$fid))
										->find();//查询样本详情
		if(($papersourceDetails['fuserid'] == $wx_id || $papersourceDetails['fuserid'] == 0) && $papersourceDetails['fstate'] == 0){
			M('tpapersource')->where(array('tpapersource.fid'=>$fid))->save(array('fuserid'=>$wx_id,'fstate'=>1,'operation_time'=>time()));//修改处理人
		}
			
		if($papersourceDetails['fuserid'] != $wx_id){
			$taskUser = M('ad_input_user')->where(array('wx_id'=>$papersourceDetails['fuserid']))->find();
		}								
		
		
		$illegaltypeList = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
		$papersizeList = M('tpapersize')->field('fcode,fsize')->select();//规格列表
		$issueList = M('tpaperissue')->field('
											tpaperissue.fpaperissueid,	
											tpaperissue.fsize,
											tpaperissue.fissuetype,
											tpapersample.fjpgfilename,
											tpapersample.fversion,
											tpapersample.cut_err_reason,
											tpapersample.finputstate,
											(select nickname from ad_input_user where wx_id = tpapersample.sub_cut_err_user) as sub_cut_err_username,
											
											
											tad.fadname
											
											
												')
									->join('tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
									->join('tad on tad.fadid = tpapersample.fadid')
									->where(array('tpaperissue.sourceid'=>$fid))
									->select();
		$this->assign('taskUser',$taskUser);//任务用户信息					
		$this->assign('issueList',$issueList);//广告列表	
		$this->assign('papersizeList',$papersizeList);//规格列表
		$this->assign('illegaltypeList',$illegaltypeList);//违法类型数据列表
		$this->assign('IllegalList',$IllegalList);//违法表现数据列表
		$this->assign('papersourceDetails',$papersourceDetails);
		$this->assign('qiniu_up_base64_img_token',A('Common/Qiniu','Model')->qiniu_up_base64_img_token());
		
		
		$this->display();
	}
	
	
	/*添加广告样本*/
	public function add_papersample(){
		//exit;
		$wx_id = intval(session('wx_id'));
		$wxInfo = M('ad_input_user')->field('wx_id,nickname,cutting_seniority')->where(array('wx_id'=>$wx_id))->find();
		$nickname = $wxInfo['nickname'];
		if(	!A('InputPc/Task','Model')->user_other_authority('1003')){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
		}
		
		$fmediaid = I('fmediaid');
		$fadname = I('fadname');//广告名
		if($fadname == ''){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'请输入广告名称'));
		}
		$fbrand = I('fbrand');// 广告品牌
		$adclass_code = I('adclass_code');//广告分类code
		$adowner_name = I('adowner_name');//广告主
		$fversion = I('fversion');//版本
		$fspokesman = I('fspokesman');//代言人
		$fapprovalid = I('fapprovalid');//审批号
		$fapprovalunit = I('fapprovalunit');//审批单位
		
		$fillegaltypecode = intval(I('fillegaltypecode','-1'));//违法类型代码
		$fexpressioncodes = I('fexpressioncodes');//违法表现代码，逗号分隔
		$fillegalcontent = I('fillegalcontent');//违法内容
		$sourceId = I('sourceId');//原始素材编号
		$fissuetype = I('fissuetype');//发布类型
		$fsize = I('fsize');//发布规格
		$fjpgfilename = I('fjpgfilename');
		$fadmanuno = I('fadmanuno');//广告中标识的生产批准文号
		$fmanuno = I('fmanuno');//生产批准文号
		$fadapprno = I('fadapprno');//广告中标识的广告批准文号
		$fapprno = I('fapprno');//广告批准文号
		$fadent = I('fadent');//广告中标识的生产企业（证件持有人）名称
		$fent = I('fent');//生产企业（证件持有人）名称
		$fentzone = I('fentzone');//生产企业（证件持有人）所在地区
		
		$sourceInfo = M('tpapersource')->where(array('fid'=>$sourceId))->find();//素材信息
		
		if($sourceInfo['fuserid'] != $wx_id){//判断该报纸版面是不是由自己处理
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,该报纸版面目前不是由您处理'));
		}
		if($sourceInfo['fstate'] != 1){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,当前状态不允许添加'));
		}
		
		
		if($adclass_code == '') $adclass_code = '2301';
		$fadid = A('Adinput/InputTask','Model')->get_ad_id($fadname,$fbrand,$adclass_code,$adowner_name);//获取广告ID
		
		
		$a_e_data['fmediaid'] = $sourceInfo['fmediaid'];//媒介id
		$a_e_data['fissuedate'] = $sourceInfo['fissuedate'];//发布日期
		
		$a_e_data['fadid'] = $fadid;//广告ID
		
		$a_e_data['fversion'] = $fversion;//版本说明
		$a_e_data['fspokesman'] = $fspokesman;//代言人

		$a_e_data['fapprovalid'] = $fapprovalid;//审批号
		$a_e_data['fapprovalunit'] = $fapprovalunit;//审批单位

		$a_e_data['fcreator'] = $nickname;//创建人
		$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$a_e_data['fmodifier'] = $nickname;//修改人
		$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$a_e_data['finspectstate'] = 1;//违法判定状态
		$a_e_data['fjpgfilename'] = $fjpgfilename;
		$a_e_data['sourceid'] = $sourceInfo['fid'];
		
		$a_e_data['fadmanuno'] = $fadmanuno;//广告中标识的生产批准文号
		$a_e_data['fmanuno'] = $fmanuno;//生产批准文号
		$a_e_data['fadapprno'] = $fadapprno;//广告中标识的广告批准文号
		$a_e_data['fapprno'] = $fapprno;//广告批准文号
		$a_e_data['fadent'] = $fadent;//广告中标识的生产企业（证件持有人）名称
		$a_e_data['fent'] = $fent;//生产企业（证件持有人）名称
		$a_e_data['fentzone'] = $fentzone;//生产企业（证件持有人）所在地区
		
		
		if(true || $fadname == '' || !A('InputPc/Task','Model')->user_other_authority('1001')){//如果广告名称为空 或者 没有广告录入权限
			$a_e_data['finputstate'] = 1;//需要众包录入广告信息
		}else{
			$a_e_data['finputstate'] = 2;
			$a_e_data['finputuser'] = $wx_id;
			A('InputPc/Task','Model')->change_task_count($wx_id,'paper_input',1);//
		}
		
		if(M('tadclass')->where(array('fcode'=>$adclass_code))->count() == 0){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'广告类别错误'));
		} 

		M()->startTrans();//开启事务方法
		$papersampleid = M('tpapersample')->add($a_e_data);//添加数据		
		
		if($papersampleid > 0){
			if(intval($fillegaltypecode) >= 0 && A('InputPc/Task','Model')->user_other_authority('1002')){//判断是否有违法判定 和 有广告初审权限
				$illegal = A('Open/Papersample','Model')->papersampleillegal($papersampleid,explode(',',$fexpressioncodes),$nickname);//添加电视广告违法表现对应表并获取冗余字段
				$illegal['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
				$illegal['fillegalcontent'] = $fillegalcontent;//违法内容
				$illegal['finspectstate'] = 2;//判定状态
				$illegal['finspectuser'] = $wx_id;//违法审查人
				$illegal['fmodifier'] = $wxInfo['nickname'];//修改人
				$illegal['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
				if($fillegaltypecode != '') $illegal['fillegaltypecode'] = $fillegaltypecode;//违法类型
				$rr_illegal = M('tpapersample')->where(array('fpapersampleid'=>$papersampleid))->save($illegal);//修改数据
				if(!$rr_illegal){
					M()->rollback();//事务回滚方法
					$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,违法数据写入失败'));
				}
				A('InputPc/Task','Model')->change_task_count($wx_id,'paper_inspect',1);//		
			}
			$issueId = M('tpaperissue')->add(array(
													'fpapersampleid' => $papersampleid,
													'fmediaid' => $sourceInfo['fmediaid'],
													'fissuedate' => $sourceInfo['fissuedate'],
													'fpage' => $sourceInfo['fpage'],
													'fpagetype' => $sourceInfo['fpagetype'],
													'fsize' => $fsize,//发布规格
													'fissuetype' => $fissuetype,//发布类型
													'famounts' => 0,//金额
													
													'fquantity' => 1,//数量
													'fissample' => 1,//是否样本
													'fcreator' => $nickname,//创建人
													'fcreatetime' => date('Y-m-d H:i:s'),//创建时间
													'fmodifier' => $nickname,//修改人
													'fmodifytime' => date('Y-m-d H:i:s'),//修改时间
													'sourceid' => $sourceInfo['fid'],
													
														));
			//var_dump($issueId);
			if(!$issueId){
				M()->rollback();//事务回滚方法
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,发布记录表写入失败'));
			}
			M()->commit();//事务提交方法
			A('InputPc/Task','Model')->change_task_count($wx_id,'paper_ad_cut',1);//
			if(intval($fillegaltypecode) >= 0){//判断是否有违法判定
				set_tillegalad_data($papersampleid,'paper');//加入AGP违法广告逻辑
			}
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
		}else{

			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));
		}

	}
	
	
	
	/*删除报纸发布*/
	public function del_issue(){
		$wx_id = intval(session('wx_id'));
		$fpaperissueid = intval(I('fpaperissueid'));//报纸样本ID

		
		$sourceUserId = M('tpaperissue')->join('tpapersource on tpapersource.fid = tpaperissue.sourceid')->where(array('tpaperissue.fpaperissueid'=>$fpaperissueid))->getField('tpapersource.fuserid');
		if($wx_id != $sourceUserId){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败,您无权删除该记录'));
		}
		
		M()->startTrans();//开启事务方法
		$fpapersampleid = M('tpaperissue')->where(array('fpaperissueid'=>$fpaperissueid))->getField('fpapersampleid');//查询样本id
		$del_sample_state = M('tpapersample')->where(array('fpapersampleid'=>$fpapersampleid))->delete();//删除样本
		if(intval($del_sample_state) == 0){
			M()->rollback();//事务回滚方法
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败,删除样本失败'));
		}
		$del_issue_state = M('tpaperissue')->where(array('fpaperissueid'=>$fpaperissueid))->delete();//删除发布记录
		if(intval($del_issue_state) == 0){
			M()->rollback();//事务回滚方法
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败,删除记录失败'));
		}
		M()->commit();//事务提交方法
		A('InputPc/Task','Model')->change_task_count($wx_id,'paper_ad_cut',-1);//
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		
		
	}
	
	/*退回重新剪辑*/
	public function back_papersource(){
		$wx_id = intval(session('wx_id'));
		$fid = I('fid');//广告样本ID
		$fid = intval($fid);//转为数字
		$where = array();
		$where['fuserid'] = $wx_id;
		$where['fid'] = $fid;
		
		if(M('tpapersource')->where($where)->getField('fstate') == 2){//判断报纸状态是否等于2
			A('InputPc/Task','Model')->change_task_count($wx_id,'paper_page_cut',-1);//剪辑版面减1
		}
		$e_data = array();
		$e_data['fuserid'] = 0;
		$e_data['operation_time'] = 0;
		$e_data['fstate'] = 0;
		
		
		$rr = M('tpapersource')->where($where)->save($e_data);
		if($rr > 0){
				
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败'));
		}
		
	}
	
	/*提交版面*/
	public function submit_papersource(){
		$wx_id = intval(session('wx_id'));
		$fid = I('fid');//广告样本ID
		$fid = intval($fid);//转为数字
		$where = array();
		$where['fuserid'] = $wx_id;
		$where['fid'] = $fid;
		$where['fstate'] = array('neq',2);
		
		$e_data = array();
		
		$e_data['operation_time'] = 0;
		$e_data['fstate'] = 2;
		$e_data['fmodifytime'] = date('Y-m-d H:i:s');
		
		
		
		$rr = M('tpapersource')->where($where)->save($e_data);
		//var_dump(M('tpapersource')->getLastSql());
		
		
		

		
		if($rr > 0){
			A('InputPc/Task','Model')->change_task_count($wx_id,'paper_page_cut',1);//剪辑版面加1
			$next_task = $this->get_next_task();
			if($next_task){
				$next_url = U('InputPc/PaperCut/ad_cut',array('fid'=>$next_task['fid']));
			}
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功','next_url'=>strval($next_url)));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败','next_url'=>''));
		}
	}
	
	
	/*获取下一个任务*/
    private function get_next_task(){
		
		$wx_id = intval(session('wx_id'));	
		
		$paper_cut_get_next_task = cookie('paper_cut_get_next_task');

		$keyword = $paper_cut_get_next_task['keyword'];

		$fissuedate_s = $paper_cut_get_next_task['fissuedate_s'];
		if($fissuedate_s == ''){
			$fissuedate_s = date('Y-m-d',time()-7*86400);
			$_GET['fissuedate_s'] = $fissuedate_s;
		}	
		$fissuedate_s = date('Y-m-d',strtotime($fissuedate_s));
		
		$fissuedate_e = $paper_cut_get_next_task['fissuedate_e'];
		if($fissuedate_e == ''){
			$fissuedate_e = date('Y-m-d');
			$_GET['fissuedate_e'] = $fissuedate_e;
		}	
		$fissuedate_e = date('Y-m-d',strtotime($fissuedate_e));
		$fpage = $paper_cut_get_next_task['fpage'];//版面
		

		$where = array();
		$where['_string'] .= '(tpapersource.fstate = 0 or ';//未剪辑的
		$where['_string'] .= 'tpapersource.fuserid = '.$wx_id.') ';//自己剪辑的
		$where['_string'] .= A('InputPc/Task','Model')->paper_cut_authority($wx_id);//数据权限控制
		$where['fissuedate'] = array('between',array($fissuedate_s,$fissuedate_e));
		if($keyword != ''){
			$where['tmedia.fmedianame'] = array('like','%'.$keyword.'%');
		}

		$where['tpapersource.fstate'] = 0;
		
		if($fpage != ''){
			$where['tpapersource.fpage'] = array('like','%'.$fpage.'%');
		}
		

		$sourceInfo = M('tpapersource')
								->field('
										tpapersource.fid
										')
								->join('tmedia on tmedia.fid = tpapersource.fmediaid')
								->where($where)->find();				
		return $sourceInfo;						
		

	}
	
	

	

}