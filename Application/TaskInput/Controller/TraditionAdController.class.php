<?php

/*传统广告任务*/
namespace TaskInput\Controller;
use TaskInput\Model\AdInputUserModel;
use TaskInput\Model\TaskInputLinkModel;
use TaskInput\Model\TaskInputModel;
use TaskInput\Model\TaskInputScoreModel;
use TaskInput\Model\NetAdModel;
use TaskInput\Model\OutAdModel;
use Think\Exception;
use Think\Page;

class TraditionAdController extends BaseController {
    const CUT_ERROR_COUNT_FIELD = 'v3_sub_cut_err';
    const TASK_BACK_COUNT_FIELD = 'v3_task_back';
    const QUALITY_COUNT_FIELD = 'v3_quality';
    const USER_AUTH_CODE = '1006';  // 录入权限
    const QUALITY_INSPECTOR_AUTH_CODE = '1007';     // 审核权限
    const PREVIEW_AUTH_CODE = '1008';     // 预览权限
    const ILLEGAL_REVIEW_COMMENT_AUTH_CODE = '3001';
    const ILLEGAL_REVIEW_COMMENT_CONFIRM_AUTH_CODE = '3002';

    protected $TASK_COUNT_FIELD_MAP = [
        '1' => 'v3_tv_task',
        '2' => 'v3_bc_task',
        '3' => 'v3_paper_task',
        '5' => 'v3_out_task',
        '13' => 'v3_net_task',
    ];
    private $isNetAd = 1;

	// 接收参数
    protected $P;
    
    public function _initialize()
    {
        // 如果taskType == 1 这个广告就是传统媒体的
        parent::_initialize();
        $this->isNetAd = I('taskType')?I('taskType'):1;
		$this->P = json_decode(file_get_contents('php://input'),true);
    }

    public function taskCheck()
    {
        $this -> display();
    }

    /**
     * 临时
     */
    public function standardization(){
        $illegalTypeArr = M('tillegaltype')->field('fcode value, fillegaltype text')->cache(true)->where(['fstate' => ['EQ', 1]])->select();
        $illegalTypeArr = json_encode($illegalTypeArr);
        $role = 'user';
        $type = 'review';
        $adClassArr = json_encode($this->getAdClassArr());
        $adClassTree = json_encode($this->getAdClassTree());
        $regionTree = json_encode($this->getRegionTree());
        $illegalArr = json_encode($this->getIllegalArr());
        $this->assign(compact('illegalTypeArr', 'role', 'adClassArr', 'adClassTree', 'type', 'regionTree', 'illegalArr'));
        $this->display();
    }

    /**
     * 录入员处理任务页面
     */
    public function index()
    {
        $reqUrl = $_SERVER['REQUEST_URI'];
        session_write_close();
        if(!A('InputPc/Task','Model')->user_other_authority(self::USER_AUTH_CODE) && !A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)){
            echo '<h1>无权限,请联系管理员</h1>';
            return false;
        }
        $wx_id = session('wx_id') ? session('wx_id') : 0;
        if (IS_POST){
            // 判断用户是做网络广告还是传统媒体广告
            if ($this->isNetAd == 2){
                // 暂停
                // $this->ajaxReturn(['code' => -1,'msg' => '网络媒体任务维护中，请处理传统媒体广告']);

                $netController = new NetAdController();
                $netController->index();
                return true;
            }elseif($this->isNetAd == 3){
                $outController = new OutAdController();
                $outController->index();
                return true;
            }

            // 暂停
            // $this->ajaxReturn(['code' => -1,'msg' => '传统媒体任务临时维护中，请处理网络媒体广告']);

            // 得到如果参数存在taskid, 则是重做任务, 否则是获取新任务
            if (I('taskid')){
                $taskid = I('taskid');
            }else{
                // 如果是历史数据组里面的, 就一定是查历史数据的
                if(M('ad_input_user')->where(['wx_id' => ['EQ', session('wx_id')]])->getField('group_id') == 38){
                    $taskid = A('Common/InputTask','Model')->get_task();
                }else{
                    // 随机分配有环节的或者没有环节的任务, 抽取的比例由系统变量决定, 如果为100, 就全部分配有环节任务
                    $rand = intval(sys_c('link_task_rate'));
                    $rand = $rand >= 100 ? 1 : mt_rand(0, $rand);
                    if ($rand){
                        $taskid = A('TaskInput/TaskInput','Model')->getTaskTrans();
                    }else{
                        $taskid = A('Common/InputTask','Model')->get_task();
                    }
                    // 没有找到任务就找另一个任务的任务
                    if (!$taskid){
                        if (!$rand){
                            $taskid = A('TaskInput/TaskInput','Model')->getTaskTrans();
                        }else{
                            $taskid = A('Common/InputTask','Model')->get_task();
                        }
                    }
                }
            }
            // 处理错误情况
            if (!$taskid){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '未找到任务'
                ]);
            }
            // 找到基本信息
            $taskData = M('task_input')
                ->field('
                    taskid, 
                    tem_ad_name, 
                    fadid, 
                    media_class, 
                    media_id, 
                    source_path, 
                    issue_date, 
                    fspokesman, 
                    fversion, 
                    fmanuno, 
                    fadmanuno, 
                    fapprno, 
                    fadapprno, 
                    fent, 
                    fadent, 
                    fentzone, 
                    task_state, 
                    back_reason, 
                    is_long_ad, 
                    sam_id,
                    sam_uuid,
                    fillegaltypecode, 
                    fexpressioncodes, 
                    fillegalcontent, 
                    quality_state, 
                    lasttime, 
                    cut_err_state, 
                    cut_err_reason
					')
                ->master(true)
                ->find($taskid);

            // 获取任务环节的进行程度
            $userAuthArr = (new AdInputUserModel())->getTaskLinkAuth($wx_id);
            if($userAuthArr){
                $taskData['linkTypeArr'] = M('task_input_link')
                    ->field('state, type')
                    ->where([
                        'media_class' => ['EQ', $taskData['media_class']],
                        'type' => ['IN', $userAuthArr],
                        'taskid' => ['EQ', $taskid],
                    ])
                    ->master(true)
                    ->select();
            }
            if (!$taskid){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '未找到任务'
                ]);
            }
            $mediaInfo = M('tmedia')->field('fmedianame, fjpgfilename')->find($taskData['media_id']);
            $taskData['media_name'] = $mediaInfo['fmedianame'];
            // 电视广告需要获取台标
            $taskData['tv_logo'] = $mediaInfo['fjpgfilename'];
            // 报纸需要再获取版面
            if ($taskData['media_class'] == 3){
                $taskData['paperUrl'] =  A('Common/Paper','Model')->get_slim_source($taskData['sam_id']);
            }elseif($taskData['media_class'] == 2){
                $taskData['fadlen'] = M('tbcsample')->where(['fid' => ['EQ', $taskData['sam_id']]])->getField('fadlen');
            }elseif ($taskData['media_class'] == 1){
                $taskData['fadlen'] = M('ttvsample')->where(['fid' => ['EQ', $taskData['sam_id']]])->getField('fadlen');
            }
            if (!$taskData['fadlen']){
                $taskData['fadlen'] = 0;
            }
            // 如果是剪辑错误, 显示错误原因
            if ($taskData['cut_err_state'] == 1){
                // 判断剪辑错误理由是新版还是旧版
                if (strpos($taskData['cut_err_reason'], ' ;; ') !== false){
                    $arr = explode(' ;; ', $taskData['cut_err_reason']);
                    $cutErrorType = M('cut_error_reason')->cache(true)->where(['id' => ['IN', $arr[0]]])->getField('reason', true);
                    $cutErrorType = implode(',', $cutErrorType);
                    $taskData['cut_err_reason'] = '错误类型:'.$cutErrorType . ' ; ' . $arr[1];
                }
            }

            $data = [];
            // 如果adid不是0, 则找到广告相关数据
            if ($taskData['fadid'] != 0){
                $data['ad'] = M('tad')->field('fadname, fbrand, fadclasscode, fadclasscode_v2, fadowner')->find($taskData['fadid']);
                $data['adOwner'] = M('tadowner')->field('fname')->find($data['ad']['fadowner']);
                $taskData['adCateText1'] = M('tadclass')->where(['fcode' => ['EQ', $data['ad']['fadclasscode']]])->cache(true)->getField('ffullname');
                $taskData['adCateText2'] = M('hz_ad_class')->where(['fcode' => ['EQ', $data['ad']['fadclasscode_v2']]])->cache(true)->getField('ffullname');
                $taskData['adCateText2'] = $taskData['adCateText2'] ? $taskData['adCateText2'] : '';
            }
            foreach ($data as $key => $value) {
                if ($value){
                    $taskData = array_merge($taskData, $value);
                }
            }
            // 如果有违法表现, 查出来
            if ($taskData['fexpressioncodes']){
                $taskData['checkedIllegalArr'] = M('tillegal')->field('fcode, fconfirmation, fexpression, fillegaltype, fpcode, fcode id, fexpression label')->cache(true)->where(['fcode' => ['IN', explode(';',$taskData['fexpressioncodes'])]])->select();
            }

            // 如果是电视广播, 就播放样本生成时的发布场景
            if ($taskData['media_class'] == 1 || $taskData['media_class'] == 2){
                $sampleTable = $taskData['media_class'] == 1 ? 'ttvsample' : 'tbcsample';
                $sampleTime = M($sampleTable)->field('sstarttime, sendtime')->find($taskData['sam_id']);
                if ($sampleTime['sstarttime'] && $sampleTime['sendtime']){
                    foreach ($sampleTime as $key => $value) {
                        $sampleTime[$key] = strtotime($value);
                    }
                    $sampleTime['sstarttime'] = intval($sampleTime['sstarttime']) - 11;//向前多播2个片段
                    $sampleTime['sendtime'] = intval($sampleTime['sendtime']) + 11;//向后多播2个片段
                    $taskData['sampleSource'] = A('Common/Media','Model')->get_m3u8($taskData['media_id'],$sampleTime['sstarttime'],$sampleTime['sendtime']);
                }else{
                    $taskData['sampleSource'] = 'null';
                }

                // 标记是否为转播且返回转播来源媒体名称
                $fromMediaName = '';
                $na_url = 'http://47.96.182.117/manage/channel/getChannelServiceTime?channel='.$taskData['media_id'].'&date='.$taskData['issue_date'];
                $ret = http($na_url);
                if(!empty($ret)){
                    $retArr = json_decode($ret,true);
                    if($retArr['code'] == 0 && !empty($retArr['dataList'])){
                        foreach($retArr['dataList'] as $key => $val){
                            if($val['type'] == 3){
                                $start = $val['start'];
                                $end = $val['end'];
                                $from = $val['from'];
                                if($sampleTime['sstarttime'] >= $start && $sampleTime['sendtime'] <= $end){
                                    $fromMediaName = $from;
                                }
                            }
                        }
                    }
                }else{
                    file_put_contents('LOG/media_na_time',date('Y-m-d H:i:s').'	接口超时'.$na_url."\n",FILE_APPEND);
                }
                $taskData['fromMediaName'] = $fromMediaName;
            }
			$taskData['sample_to_word'] = '';
			if($taskData['sam_uuid']) $taskData['sample_to_word'] = strval(A('Admin/Sample','Model')->sample_to_word($taskData['sam_uuid']));
            // 特例：来自DMP管理人员可直接修改任务 20191220
            if($_SERVER['REQUEST_URI'] == '/Admin/TaskInput/index'){
                $taskData['linkTypeArr'] = [[state=>"0", type=>"1"], [state=>"0", type=>"2"]];
                // $taskData['task_state'] = 0;
            }
            $data = [
                'code' => 0,
                'data'  => $taskData,
            ];
            $this->ajaxReturn($data);
        }else{
            $type = 'input';
            $illegalType = M('tillegaltype')->field('fcode value, fillegaltype text')->cache(true)->where(['fstate' => ['EQ', 1]])->select();
            $illegalTypeArr = json_encode($illegalType);
            $adClassArr = json_encode($this->getAdClassArr());
            $adClassTree = json_encode($this->getAdClassTree());
            $illegalArr = json_encode($this->getIllegalArr());
            $this->assign(compact('type', 'adClassTree', 'illegalTypeArr', 'adClassArr', 'illegalArr'));
            $this->display();
        }
    }

    protected function getIllegalTypeArr()
    {
        $illegalType = M('tillegaltype')->field('fcode value, fillegaltype text')->cache(true)->where(['fstate' => ['EQ', 1]])->select();
        return $illegalType;
    }

    /**
     * 处理提交任务
     */
    public function submitTask()
    {
        if(!A('InputPc/Task','Model')->user_other_authority(self::USER_AUTH_CODE) && !A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        $formData = I('post.data');
        // 判断用户是做网络广告还是传统媒体广告
        if ($formData['media_class'] == 13){
            $netController = new NetAdController();
            $netController->submitTask();
            return true;
        }elseif ($formData['media_class'] == 5){
            $outController = new OutAdController();
            $outController->submitTask();
            return true;
        }

        $taskInputTableField = ['taskid', 'fspokesman', 'fversion', 'fmanuno', 'fadmanuno', 'fapprno', 'fadapprno', 'fent', 'fadent' , 'fentzone', 'fillegaltypecode', 'fexpressioncodes', 'fillegalcontent', 'is_long_ad'];
        $fadname = $formData['fadname'];
        $fbrand = $formData['fbrand'];
        $adclass_code = $formData['fadclasscode'];
        if ($adclass_code == 2301){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '当前广告类别错误, 请选择另一个类别'
            ]);
        }
        $adclass_code_v2 = $formData['fadclasscode_v2'];
        $adowner_name = $formData['fname'];
        // 如果不违法,就直接过滤掉违法相关的2个字段
        if ($formData['fillegaltypecode'] == '0'){
            $formData['fexpressioncodes'] = '';
            $formData['fillegalcontent'] = '';
        }
        // 临时, 如果发布日期在8月1号之前, 就不允许修改违法判定
        if (strtotime($formData['issue_date']) < strtotime('2018-08-01')){
            unset($formData['fillegaltypecode']);
            unset($formData['fexpressioncodes']);
            unset($formData['fillegalcontent']);
        }


        // 提交条件的限制 1. 本人提交本人的任务,或者是质检员修改任务; 2. 任务状态只能是做任务中或被退回
        $wxId = session('wx_id');
        $taskInfo = M('task_input')->find($formData['taskid']);
        $taskState = $taskInfo['task_state'];
        $currentId = $taskInfo['wx_id'];
        // 整理task_input的修改数据
        $taskInputData = [];
        $requiredField = [
            'fadclasscode',
            'fadclasscode_v2',
            'fname',
            'fadname',
            'fbrand',
        ];
        foreach ($taskInputTableField as $key => $value) {
            if (isset($formData[$value])){
                $taskInputData[$value] = $formData[$value];
            }elseif (in_array($value, $requiredField)){

                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '有必填字段没有填写, 请重新填写',
                    'v' => $value
                ]);
            }
        }
        $taskInputData['lasttime'] = time();
        $taskInputData['task_state'] = 2;
        $taskInputData['syn_state'] = 1;
        $wxInfo = M('ad_input_user')->find($wxId);
        $taskInputData['fadid'] = A('Common/Ad','Model')->getAdId($fadname,$fbrand,$adclass_code,$adowner_name,$wxInfo['alias'], $adclass_code_v2);//获取广告ID
        $role = 'user'; // 用来标识是质检员修改任务还是录入员做任务
        // 录入员做任务
        if ($wxId == $currentId && $taskState != 2){
            $allowTaskState = [1,2,4];
            // task_input_flow处理
            $taskInputFlowData = [
                'taskid' => $taskInputData['taskid'],
                'flow_content' => '完成任务',
                'flow_time' => time(),
                'wx_id' => $wxId,
            ];
            if ($taskState == 4){
                $taskInputFlowData['task_info'] = json_encode($taskInputData);
            }
        }elseif (A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE) && $taskInfo['quality_state'] != 2){  // 质检员修改任务
            $allowTaskState = [2];
            // task_input_flow处理
            $taskInputFlowData = [
                'taskid' => $taskInputData['taskid'],
                'flow_content' => '质检员修改任务',
                'flow_time' => time(),
                'wx_id' => $wxId,
            ];
            $taskInputData['quality_state'] = 2;
            $taskInputData['quality_wx_id'] = session('wx_id');
            $role = 'inspector';
        }else{
            $this->ajaxReturn([
                'code' => -2,
                'msg' => '当前任务不是你的,开始下一个任务'
            ]);
        }

        if (!in_array($taskState, $allowTaskState)){
            $this->ajaxReturn([
                'code' => -3,
                'msg' => '当前任务状态非法',
            ]);
        }

        M()->startTrans();
        $taskInputUpdate = M('task_input')->where(['taskid' => ['EQ', $taskInputData['taskid']]])->save($taskInputData) === false ? false : true;
        $taskInputFlowUpdate = M('task_input_flow')->add($taskInputFlowData) === false ? false : true;
        if ($taskInputUpdate && $taskInputFlowUpdate){
            if ($role == 'user'){
                A('InputPc/Task','Model')->change_task_count($wxId, $this->TASK_COUNT_FIELD_MAP[$taskInfo['media_class']], 1);
            }elseif ($role == 'inspector'){
                A('InputPc/Task','Model')->change_task_count($currentId, self::TASK_BACK_COUNT_FIELD, 1);
                A('Adinput/Weixin','Model')->task_back($currentId,'质检员修改任务',$fadname);
                A('InputPc/Task','Model')->change_task_count($wxId, self::QUALITY_COUNT_FIELD, 1);
            }
            $scoreModel = new TaskInputScoreModel();
            $scoreModel->addScoreBySubmitTask($taskInfo['taskid'], 1);
            if (strtotime($taskInfo['issue_date']) >= strtotime('2018-08-01')){
                $scoreModel->addScoreBySubmitTask($taskInfo['taskid'], 2);
                A('InputPc/Task','Model')->change_task_count($wxId, $this->TASK_COUNT_FIELD_MAP[$taskInfo['media_class']], 1);
            }
            M()->commit();
            A('Common/AliyunLog','Model')->task_input_log([
                'type' => 'save',
                'data' => $taskInputData,
                'msg' => '提交任务成功;'
            ]);
            $code = 0;
            $msg = '提交成功';
        }else{
            M()->rollback();
            A('Common/AliyunLog','Model')->task_input_log([
                'type' => 'save',
                'data' => $taskInputData,
                'msg' => '提交任务失败;'
            ]);
            $code = -1;
            if (!$taskInputUpdate){
                $msg = '任务更新失败';
            }else{
                $msg = '任务记录更新失败';
            }
        }
        $this->ajaxReturn(compact('code', 'msg'));
    }

    /**
     * 提交广告信息录入任务
     * @return bool
     */
    public function submitAdInputTask()
    {
        $wx_id = session('wx_id');
        if(!A('InputPc/Task','Model')->user_other_authority(self::USER_AUTH_CODE) && !A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        $formData = I('post.data');
        // 判断用户是做网络广告还是传统媒体广告
        if ($formData['media_class'] == 13){
            $netController = new NetAdController();
            $netController->submitAdInputTask();
            return true;
        }elseif ($formData['media_class'] == 5){
            $outController = new OutAdController();
            $outController->submitAdInputTask();
            return true;
        }
        $taskInputTableField = [
            'taskid',
            'fspokesman',
            'fversion',
            'is_long_ad',
            'fadclasscode',
            'fadclasscode_v2',
            'fname',
            'fadname',
            'media_class',
            'fbrand',
            'fadid',
        ];
        $taskInputData = [];
        $requiredField = [
            'fadclasscode',
            'fadclasscode_v2',
            'fname',
            'fadname',
            'fbrand',
            'taskid'
        ];
        if ($formData['fadname'] == '社会公益'){
            $adInfo = M('tad')->where(['fadname' => ['EQ', '社会公益']])->cache(true, 60)->find();
            $formData['fadclasscode'] = $adInfo['fadclasscode'];
            $formData['fadclasscode_v2'] = $adInfo['fadclasscode_v2'];
            $formData['fname'] = '广告主不详';
            $formData['fbrand'] = $adInfo['fbrand'];
        }
        foreach ($taskInputTableField as $key => $value) {
            if (isset($formData[$value])){
                $taskInputData[$value] = $formData[$value];
            }elseif (in_array($value, $requiredField) && $formData['fadname'] !== '社会公益'){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '有必填字段没有填写, 请重新填写',
                    'v' => $value
                ]);
            }
        }
        $model = new TaskInputModel();
        M()->startTrans();
        $res = $model->submitAdInputTask($taskInputData, $wx_id);
        if ($res){
            $model->commit();
            $scoreRes = (new TaskInputScoreModel())->addScoreBySubmitTask($taskInputData['taskid'], 1);

            // 是否需要自动审核
            // $isAutoInspect = A('Common/InputTask','Model')->isAutoInspect($taskInputData['fadname']);
            // if(!$isAutoInspect){
            //     M('task_input_link')->where(['taskid'=>$taskInputData['taskid'],'media_class'=>$taskInputData['media_class'],'type'=>2])->save(['state'=>2,'wx_id'=>0,'update_time'=>time()]);
            //     $flowData = [
            //         'taskid'       => $taskInputData['taskid'],
            //         'flow_content' => '系统自动审核',
            //         'flow_time'    => time(),
            //         'wx_id'        => 0,
            //         'task_info'    => json_encode($taskInputData),
            //     ];
            //     $flowRes = M('task_input_flow')->add($flowData);
            //     // 将任务自动完成
            //     M('task_input')->where(['taskid'=>$taskInputData['taskid']])->save(['task_state'=>2,'syn_state'=>1]);
            //     $flowDataFinish = [
            //         'taskid'       => $taskInputData['taskid'],
            //         'flow_content' => '系统自动审核并完成',
            //         'flow_time'    => time(),
            //         'wx_id'        => 0,
            //         'task_info'    => json_encode($taskInputData),
            //     ];
            //     $flowResFinish = M('task_input_flow')->add($flowDataFinish);
            // }

            $adCateText1 = M('tadclass')->where(['fcode' => ['EQ', $taskInputData['fadclasscode']]])->cache(true)->getField('ffullname');
            $adCateText2 = M('hz_ad_class')->where(['fcode' => ['EQ', $taskInputData['fadclasscode_v2']]])->cache(true)->getField('ffullname');
            $adCateText2 = $adCateText2 ? $adCateText2 : '';
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '提交成功',
                'adCateText1' => $adCateText1,
                'adCateText2' => $adCateText2,
            ]);
        }else{
            M()->rollback();
            $this->ajaxReturn([
                'code' => -1,
                'msg' => $model->getError(),
            ]);
        }
    }

    public function submitJudgeTask()
    {
        $wx_id = session('wx_id');
        if(!A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE) && !A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        // 判断用户是做网络广告还是传统媒体广告
        if (I('post.data')['media_class'] == 13){
            $netController = new NetAdController();
            $netController->submitJudgeTask();
            return true;
        }elseif (I('post.data')['media_class'] == 5){
            $outController = new OutAdController();
            $outController->submitJudgeTask();
            return true;
        }
        $taskInputTableField = [
            'taskid',
            'fspokesman',
            'fversion',
            'is_long_ad',
            'fadclasscode',
            'fadclasscode_v2',
            'fname',
            'fadname',
            'fbrand',
            'fadid',
            'fillegaltypecode',
            'fillegalcontent',
            'fexpressioncodes',
            'fmanuno',
            'fadmanuno',
            'fapprno',
            'fadapprno',
            'fent',
            'fadent' ,
            'media_class',
            'fentzone',
        ];
        $formData = I('post.data');
        $taskInputData = [];
        $requiredField = [
            'taskid',
            'fillegaltypecode',
        ];
        foreach ($taskInputTableField as $key => $value) {
            if (isset($formData[$value])){
                $taskInputData[$value] = $formData[$value];
            }elseif (in_array($value, $requiredField)){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '有必填字段没有填写, 请重新填写',
                    'v' => $value
                ]);
            }
        }
        $model = new TaskInputModel();
        $model->startTrans();
        $res = $model->submitJudgeTask($taskInputData, $wx_id);
        if ($res){
            $model->commit();
            $taskInfo = M('task_input')->alias('a')->field('b.fadclasscode')->join('tad b ON b.fadid=a.fadid')->where(['a.taskid'=>$taskInputData['taskid']])->find();
            if(substr($taskInfo['fadclasscode'],0,2) != 22 && substr($taskInfo['fadclasscode'],0,2) != 23){
                // 公益、其它类广告判定不积分
                $scoreRes = (new TaskInputScoreModel())->addScoreBySubmitTask($taskInputData['taskid'], 2);
            }
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '提交成功'
            ]);
        }else{
            $model->rollback();
            $this->ajaxReturn([
                'code' => -1,
                'msg' => $model->getError(),
            ]);
        }
    }

    /**
     * 展示我的任务列表
     */
    public function myTaskList_V20190304()
    {
        if(  !A('InputPc/Task','Model')->user_other_authority(self::USER_AUTH_CODE)){
            echo '<h1>无权限, 请联系管理员</h1>';
            return false;
        }
        $isNetAd = I('isNetAd') == 1 ? 1 : 0;
        if (IS_POST){
            $where = ['task.wx_id' => ['EQ', session('wx_id')]];
            $linkSubQueryWhere = [
                'state' => ['NEQ', 0],
            ];
            if ($isNetAd){
                $linkSubQueryWhere['media_class'] = ['EQ', 13];
            }else{
                $linkSubQueryWhere['media_class'] = ['NEQ', 13];
            }
            $linkSubQuery = M('task_input_link')
                ->field('taskid')
                ->where($linkSubQueryWhere)
                ->group('taskid')
                ->buildSql();
            $where['_string'] = "task.taskid IN $linkSubQuery OR task_state <> 0";
            $formData = I('post.where');
            $page = [I('page'), I('pageSize')];
            if ($isNetAd){
                $netAdController = new NetAdController();
                $data = $netAdController->baseTaskInputSearch($formData, $where, $page);
            }else{
                $data = $this->baseTaskInputSearch($formData, $where, $page);
            }
            $this->ajaxReturn($data);
        }else{
            $illegalTypeArr = M('tillegaltype')->field('fcode value, fillegaltype text')->cache(true)->where(['fstate' => ['EQ', 1]])->select();
            $illegalTypeArr = json_encode($illegalTypeArr);
            $role = 'user';
            $type = 'review';
            $adClassArr = json_encode($this->getAdClassArr());
            $adClassTree = json_encode($this->getAdClassTree());
            $regionTree = json_encode($this->getRegionTree());
            $illegalArr = json_encode($this->getIllegalArr());
            $this->assign(compact('illegalTypeArr', 'role', 'adClassArr', 'adClassTree', 'type', 'regionTree', 'illegalArr'));
            $this->display('taskList');
        }
    }
    
    /**
     * 展示我的任务列表
     */
    public function myTaskList()
    {
        if(  !A('InputPc/Task','Model')->user_other_authority(self::USER_AUTH_CODE)){
            echo '<h1>无权限, 请联系管理员</h1>';
            return false;
        }
        $isNetAd = I('isNetAd') ? (int)I('isNetAd') : 1;
        if (IS_POST){
            $page = [I('page'), I('pageSize')];
            if ($isNetAd == 2){
                $flowSubQueryWhere = [
                    'wx_id' => session('wx_id'),
                    'RIGHT(logtype,1)' => 2 //完成状态的任务流程日志
                ];
                $flowSubQuery = M('tnettask_flowlog')
                    ->field('taskid')
                    ->where($flowSubQueryWhere)
                    ->group('taskid')
                    ->buildSql();
                $where['_string'] = "task.taskid IN $flowSubQuery";
                $formData = I('post.where');
                $netAdController = new NetAdController();
                $data = $netAdController->baseTaskInputSearch($formData, $where, $page);
            }elseif($isNetAd == 3){
                $formData = I('post.where');

                $limitstr = ' limit '.($page[0]-1)*$page[1].','.$page[1];
                $where_string = ' 1=1 ';
                if(!empty($formData['timeRangeArr'])){
                    foreach ($formData['timeRangeArr'] as $key => $value) {
                        $formData['timeRangeArr'][$key] = $value / 1000;
                    }
                    $formData['timeRangeArr'][1] = strtotime('+1 day', $formData['timeRangeArr'][1]);
                    $where_string .= ' and net_created_date between '.$formData['timeRangeArr'][0].' and '.$formData['timeRangeArr'][1];
                }

                //广告名
                if(!empty($formData['adName'])){
                    $where_string .= ' and fadname like "%'.$formData['adName'].'%"';
                }

                //广告主
                if ($formData['fadowner'] != ''){
                    $where_string .= ' and adOwnerName like "%'.$formData['fadowner'].'%"';
                }

                //发布企业
                if ($formData['mediaName'] != ''){
                    $where_string .= ' and fmedianame like "%'.$formData['mediaName'].'%"';
                }

                $countsql = '
                    select count(*) acount from (
                        (select a.taskid,a.wx_id,a.task_state,b.upunit_name fmedianame,b.name fadname,b.class_name ffullname,b.manage_name adOwnerName,a.net_created_date,from_unixtime(a.modifytime) lasttime,(case when `check`>10 or `check` = 2 then "预审通过" when `check` = 3 then "预审不通过" else "未处理" end) fillegaltype from todtask a,tod_ad_info b where a.fadsource_id = 0 and a.fadinfo_id = b.fid)
                        union all 
                        (select a.taskid,a.wx_id,a.task_state,b.upunit_name fmedianame,b.name fadname,b.class_name ffullname,b.manage_name adOwnerName,a.net_created_date,from_unixtime(a.modifytime) lasttime,(case when `check`<>12 then "审核未处理" when fillegaltypecode = 0 then "不违法" when fillegaltypecode = 30 then "违法" end) fillegaltype from todtask a,tod_ad_info b,tod_lat_and_lon c where a.fadsource_id > 0 and a.fadinfo_id = b.fid and a.fadsource_id = c.source_id)
                    ) task
                    inner join (select taskid from todtask_flowlog where wx_id = '.session('wx_id').' group by taskid) tasklog on task.taskid = tasklog.taskid
                    where '.$where_string;
                $count = M()->query($countsql);

                $datasql = '
                    select * from (
                        (select a.taskid,a.wx_id,a.task_state,b.upunit_name fmedianame,b.name fadname,b.class_name ffullname,b.manage_name adOwnerName,a.net_created_date,from_unixtime(a.modifytime) lasttime,(case when `check`>10 or `check` = 2 then "预审通过" when `check` = 3 then "预审不通过" else "未处理" end) fillegaltype,-1 fillegaltypecode from todtask a,tod_ad_info b where a.fadsource_id = 0 and a.fadinfo_id = b.fid)
                        union all 
                        (select a.taskid,a.wx_id,a.task_state,b.upunit_name fmedianame,b.name fadname,b.class_name ffullname,b.manage_name adOwnerName,a.net_created_date,from_unixtime(a.modifytime) lasttime,(case when `check`<>12 then "审核未处理" when fillegaltypecode = 0 then "不违法" when fillegaltypecode = 30 then "违法" end) fillegaltype,fillegaltypecode from todtask a,tod_ad_info b,tod_lat_and_lon c where a.fadsource_id > 0 and a.fadinfo_id = b.fid and a.fadsource_id = c.source_id)
                    ) task
                    inner join (select taskid from todtask_flowlog where wx_id = '.session('wx_id').' group by taskid) tasklog on task.taskid = tasklog.taskid
                    where '.$where_string.'
                    order by net_created_date desc '.$limitstr.'
                ';
                $res = M()->query($datasql);

                foreach ($res as $key => $value) {
                    $res[$key]['fbrand'] = '无突出品牌';
                    $res[$key]['cut_err_state'] = '0';
                    $res[$key]['quality_state'] = '0';
                    $res[$key]['alias'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $value['wx_id']]])->getField('alias');
                    $res[$key]['input_user'] = $res[$key]['alias'];
                }
                $data = [
                    'data' => $res,
                    'total' => $count[0]['acount'],
                    'page' => $page[0],
                    'where' => []
                ];
            }else{
                // $where = ['task.wx_id' => ['EQ', session('wx_id')]];
                $linkSubQueryWhere = [
                    'state' => ['GT',0],
                    'wx_id' => session('wx_id'),
                ];
                $linkSubQueryWhere['media_class'] = ['IN', [1,2,3]];
                $linkSubQuery = M('task_input_link')
                    ->field('taskid')
                    ->where($linkSubQueryWhere)
                    ->group('taskid')
                    ->buildSql();
                // $where['_string'] = "task.taskid IN $linkSubQuery OR task_state <> 0";
                $where['_string'] = "task.taskid IN $linkSubQuery";
                $formData = I('post.where');
                $data = $this->baseTaskInputSearch($formData, $where, $page);
            }
            $this->ajaxReturn($data);
        }else{
            $illegalTypeArr = M('tillegaltype')->field('fcode value, fillegaltype text')->cache(true)->where(['fstate' => ['EQ', 1]])->select();
            $illegalTypeArr = json_encode($illegalTypeArr);
            $role = 'user';
            $type = 'review';
            $adClassArr = json_encode($this->getAdClassArr());
            $adClassTree = json_encode($this->getAdClassTree());
            $regionTree = json_encode($this->getRegionTree());
            $illegalArr = json_encode($this->getIllegalArr());
            $this->assign(compact('illegalTypeArr', 'role', 'adClassArr', 'adClassTree', 'type', 'regionTree', 'illegalArr'));
            $this->display('taskList');
        }
    }

    /**
     * 展示审核员视角的任务列表
     */
    public function inspectorTaskList()

    {
        if(  !A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)){
            echo '<h1>无权限, 请联系管理员</h1>';
            return false;
        }
        if (IS_POST){
            $where = [
                // 'quality_state' => ['EQ', 0]
            ];
            $where['_string'] = "RIGHT(task_state,1) IN (2,3,5)";
            $formData = I('post.where');
            $page = [I('page'), I('pageSize')];
            $isNetAd = I('isNetAd') ? (int)I('isNetAd') : 0;
            if ($isNetAd == 2){
                $netAdController = new NetAdController();
                $data = $netAdController->inspectorTaskInputSearch($formData, $where, $page);
            }elseif($isNetAd == 3){
                $outAdController = new OutAdController();
                $data = $outAdController->baseTaskInputSearch($formData, $where, $page);
            }else{
                $data = $this->inspectorTaskInputSearch($formData, $where, $page);
            }
            $this->ajaxReturn($data);
        }else{
            $illegalTypeArr = M('tillegaltype')->field('fcode value, fillegaltype text')->cache(true)->where(['fstate' => ['EQ', 1]])->select();
            $illegalTypeArr = json_encode($illegalTypeArr);
            $role = 'inspector';
            $type = 'quality';
            $adClassArr = json_encode($this->getAdClassArr());
            $adClassTree = json_encode($this->getAdClassTree());
            $regionTree = json_encode($this->getRegionTree());
            $illegalArr = json_encode($this->getIllegalArr());
            $this->assign(compact('illegalTypeArr', 'role', 'adClassArr', 'adClassTree', 'type', 'regionTree', 'illegalArr'));
            $this->display();
        }
    }

    /**
     * 基础众包任务搜索方法
     * @param $formData array 前端传过来的搜索条件
     * @param $where array 自定义的搜索条件
     * @param int $page
     * @return array
     */
    protected function baseTaskInputSearch($formData, $where, $page = [1, 20])
    {
        if ($formData['timeRangeArr']){
            foreach ($formData['timeRangeArr'] as $key => $value) {
                $formData['timeRangeArr'][$key] = $value / 1000;
            }
            $formData['timeRangeArr'][1] = strtotime('+1 day', $formData['timeRangeArr'][1]);
        }
        if ($formData['media_class'] != ''){
            $where['task.media_class'] = ['EQ', $formData['media_class']];
        }
        if ($formData['fillegaltypecode'] != ''){
            $where['fillegaltypecode'] = ['EQ', $formData['fillegaltypecode']];
        }
        if ($formData['task_state'] != ''){
            $where['task_state'] = ['EQ', $formData['task_state']];
        }
        if ($formData['quality_state'] != ''){
            $where['quality_state'] = ['EQ', $formData['quality_state']];
        }
        if ($formData['taskid'] != ''){
            $where['task.taskid'] = ['EQ', $formData['taskid']];
        }
        if ($formData['timeRangeArr'] != ''){
            $where['lasttime'] = ['BETWEEN', $formData['timeRangeArr']];
        }

        $where['cut_err_state'] = ['EQ', 0];

        if ($formData['wx_id'] != ''){
            $wxSubQuery = M('ad_input_user')
                ->field('wx_id')
                ->where([
                    'alias|nickname' => ['LIKE', '%'.$formData['wx_id'].'%']
                ])
                ->buildSql();
            $where['task.wx_id'] = ['EXP', 'IN '.$wxSubQuery];
        }
        // tad表
        $adWhere = [];
        if ($formData['adClass'] != ''){
            $len = strlen($formData['adClass']);
            $adWhere["LEFT(fadclasscode, $len)"] = ['EQ', $formData['adClass']];
        }
        if ($formData['adClass2'] != ''){
            $len = strlen($formData['adClass2']);
            $adWhere["LEFT(fadclasscode_v2, $len)"] = ['EQ', $formData['adClass2']];
        }
        if ($formData['adName'] != ''){
            $adWhere['fadname'] = ['LIKE', '%'.$formData['adName'].'%'];
        }
        if ($formData['fadid'] != ''){
            $adWhere['fadid'] = ['EQ', $formData['fadid']];
        }
        if ($formData['fbrand'] != ''){
            $adWhere['fbrand'] = ['LIKE', '%'.$formData['fbrand'].'%'];
        }
        if ($formData['fadowner'] != ''){
            $adOwnerSubQuery = M('tadowner')
                ->field('fid')
                ->where([
                    'fname' => ['LIKE', '%'.$formData['fadowner'].'%']
                ])
                ->buildSql();
            $adWhere['fadowner'] = ['EXP', ' IN '.$adOwnerSubQuery];
        }
        // tmedia表
        $mediaWhere = [];
        if ($formData['mediaName'] != ''){
            $mediaWhere['fmedianame'] = ['LIKE', '%'.$formData['mediaName'].'%'];
        }
        // tmediaowner表
        $mediaOwnerWhere = [];
        if ($formData['regionId'] != ''){
            if ($formData['onlyThisLevel'] != 1){
                $region_id_rtrim = A('Common/System','Model')->get_region_left($formData['regionId']);//地区ID去掉末尾的0
                $region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
                $mediaOwnerWhere["LEFT(fregionid, $region_id_strlen)"] = ['EQ', $region_id_rtrim];
            }else{
                $mediaOwnerWhere['fregionid'] = ['LIKE', '%'.$formData['regionId'].'%'];
            }
            $mediaOwnerSubQuery = M('tmediaowner')
                ->field('fid')
                ->where($mediaOwnerWhere)
                ->buildSql();
            $mediaWhere['fmediaownerid'] = ['EXP', 'IN '. $mediaOwnerSubQuery];//地区搜索条件
        }
        if ($mediaWhere != []){
            $mediaSubQuery = M('tmedia')
                ->field('fid')
                ->where($mediaWhere)
                ->buildSql();
            $where['media_id'] = ['EXP', 'IN '. $mediaSubQuery]; // 媒体搜索条件
        }
        if ($adWhere != []){
            $adSubQuery = M('tad')
                ->field('fadid')
                ->where($adWhere)
                ->buildSql();
            $where['fadid'] = ['EXP', 'IN '. $adSubQuery]; // 广告搜索条件
        }

        $res = M('task_input')
            ->alias('task')
            ->field("task.taskid,fadid,sam_id, task.media_class, fillegaltypecode, task_state, quality_state, lasttime, cut_err_state, task.wx_id, back_reason, media_id, a.wx_id as input_user, b.wx_id  as inspect_user")
            ->join("left join task_input_link as a on task.taskid = a.taskid and task.media_class = a.media_class and a.type = 1")
            ->join("left join task_input_link as b on task.taskid = b.taskid and task.media_class = b.media_class and b.type = 2")
            ->where($where)
            ->order('lasttime desc')
            ->page($page[0], $page[1])
            ->select();
        $sqlRes = M('task_input')->getLastSql();
        $count = M('task_input')
            ->alias('task')
            ->where($where)
            ->count();
        foreach ($res as $key => $value) {
            $res[$key]['lasttime'] = date('Y-m-d H:i:s', $value['lasttime']);
            $res[$key]['alias'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $value['wx_id']]])->getField('alias');
            $res[$key]['input_user'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $value['input_user']]])->getField('alias');
            $res[$key]['inspect_user'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $value['inspect_user']]])->getField('alias');
            $res[$key]['fmedianame'] = M('tmedia')->cache(true)->where(['fid' => ['EQ', $value['media_id']]])->getField('fmedianame');
            $adInfo = M('tad')->field('fadname, fadclasscode,fadclasscode_v2, fadowner, fbrand')->find($value['fadid']);
            $res[$key]['fadname'] = $adInfo['fadname'];
            $res[$key]['fadclasscode'] = $adInfo['fadclasscode'];
            $res[$key]['fbrand'] = $adInfo['fbrand'];
            $res[$key]['ffullname'] = M('tadclass')->cache(true)->where(['fcode' => ['EQ', $adInfo['fadclasscode']]])->getField('ffullname');
            $res[$key]['adClassV2'] = M('hz_ad_class')->cache(true)->where(['fcode' => ['EQ', $adInfo['fadclasscode_v2']]])->getField('ffullname');
            $res[$key]['adOwnerName'] = M('tadowner')->cache(true)->where(['fid' => ['EQ', $adInfo['fadowner']]])->getField('fname');
            $res[$key]['fadname'] = r_highlight($res[$key]['fadname'], $formData['adName'], 'red');
            $res[$key]['fmedianame'] = r_highlight($res[$key]['fmedianame'], $formData['mediaName'], 'red');
            if($res[$key]['media_class'] == "1" || $res[$key]['media_class'] == "2" ){
                $samTable = $res[$key]['media_class'] == "1" ? 'ttvsample' : 'tbcsample';
                $res[$key]['fadlen'] = M($samTable)->where(['fid'=>$res[$key]['sam_id']])->getField('fadlen');
                $res[$key]['fadlen'] = $res[$key]['fadlen'] ? $res[$key]['fadlen'] : 0;
            }else{
                $res[$key]['fadlen'] = '--';
            }
        }
        $data = [
            'data' => $res,
            'total' => $count,
            'page' => $page[0],
            'where' => $mediaWhere
        ];
        return $data;
    }

    /**
     * 基础众包任务搜索方法-互联网任务
     * @param $formData array 前端传过来的搜索条件
     * @param $where array 自定义的搜索条件
     * @param int $page
     * @return array
     */
    protected function baseNetAdTaskSearch($formData, $where, $page = [1, 20])
    {
        if ($formData['timeRangeArr']){
            foreach ($formData['timeRangeArr'] as $key => $value) {
                $formData['timeRangeArr'][$key] = $value / 1000;
            }
            $formData['timeRangeArr'][1] = strtotime('+1 day', $formData['timeRangeArr'][1]);
        }
        if ($formData['media_class'] != ''){
            $where['task.media_class'] = ['EQ', $formData['media_class']];
        }
        if ($formData['fillegaltypecode'] != ''){
            $where['fillegaltypecode'] = ['EQ', $formData['fillegaltypecode']];
        }
        if ($formData['task_state'] != ''){
            $where['task_state'] = ['EQ', $formData['task_state']];
        }
        if ($formData['quality_state'] != ''){
            $where['quality_state'] = ['EQ', $formData['quality_state']];
        }
        if ($formData['timeRangeArr'] != ''){
            $where['modifytime'] = ['BETWEEN', $formData['timeRangeArr']];
        }

        $where['cut_err_state'] = ['EQ', 0];

        if ($formData['wx_id'] != ''){
            $wxSubQuery = M('ad_input_user')
                ->field('wx_id')
                ->where([
                    'alias|nickname' => ['LIKE', '%'.$formData['wx_id'].'%']
                ])
                ->buildSql();
            $where['task.wx_id'] = ['EXP', 'IN '.$wxSubQuery];
        }
        // tad表
        $adWhere = [];
        if ($formData['adClass'] != ''){
            $len = strlen($formData['adClass']);
            $adWhere["LEFT(fadclasscode, $len)"] = ['EQ', $formData['adClass']];
        }
        if ($formData['adClass2'] != ''){
            $len = strlen($formData['adClass2']);
            $adWhere["LEFT(fadclasscode_v2, $len)"] = ['EQ', $formData['adClass2']];
        }
        if ($formData['adName'] != ''){
            $adWhere['fadname'] = ['LIKE', '%'.$formData['adName'].'%'];
        }
        if ($formData['fadid'] != ''){
            $adWhere['fadid'] = ['EQ', $formData['fadid']];
        }
        if ($formData['fbrand'] != ''){
            $adWhere['fbrand'] = ['LIKE', '%'.$formData['fbrand'].'%'];
        }
        if ($formData['fadowner'] != ''){
            $adOwnerSubQuery = M('tadowner')
                ->field('fid')
                ->where([
                    'fname' => ['LIKE', '%'.$formData['fadowner'].'%']
                ])
                ->buildSql();
            $adWhere['fadowner'] = ['EXP', ' IN '.$adOwnerSubQuery];
        }
        // tmedia表
        $mediaWhere = [];
        if ($formData['mediaName'] != ''){
            $mediaWhere['fmedianame'] = ['LIKE', '%'.$formData['mediaName'].'%'];
        }
        // tmediaowner表
        $mediaOwnerWhere = [];
        if ($formData['regionId'] != ''){
            if ($formData['onlyThisLevel'] != 1){
                $region_id_rtrim = A('Common/System','Model')->get_region_left($formData['regionId']);//地区ID去掉末尾的0
                $region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
                $mediaOwnerWhere["LEFT(fregionid, $region_id_strlen)"] = ['EQ', $region_id_rtrim];
            }else{
                $mediaOwnerWhere['fregionid'] = ['LIKE', '%'.$formData['regionId'].'%'];
            }
            $mediaOwnerSubQuery = M('tmediaowner')
                ->field('fid')
                ->where($mediaOwnerWhere)
                ->buildSql();
            $mediaWhere['fmediaownerid'] = ['EXP', 'IN '. $mediaOwnerSubQuery];//地区搜索条件
        }
        if ($mediaWhere != []){
            $mediaSubQuery = M('tmedia')
                ->field('fid')
                ->where($mediaWhere)
                ->buildSql();
            $where['media_id'] = ['EXP', 'IN '. $mediaSubQuery]; // 媒体搜索条件
        }
        if ($adWhere != []){
            $adSubQuery = M('tad')
                ->field('fadid')
                ->where($adWhere)
                ->buildSql();
            $where['fadid'] = ['EXP', 'IN '. $adSubQuery]; // 广告搜索条件
        }

        $res = M('tnettask')
            ->alias('task')
            ->field("task.taskid,fadid,major_key, task.media_class, fillegaltypecode, task_state, quality_state, modifytime, cut_err_state, task.wx_id, back_reason, media_id")
            ->where($where)
            ->order('modifytime desc')
            ->page($page[0], $page[1])
            ->select();
        $sqlRes = M('tnettask')->getLastSql();
        $count = M('tnettask')
            ->alias('task')
            ->where($where)
            ->count();
        foreach ($res as $key => $value) {
            $res[$key]['lasttime'] = date('Y-m-d H:i:s', $value['modifytime']);
            $res[$key]['alias'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $value['wx_id']]])->getField('alias');
            $res[$key]['input_user'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $value['input_user']]])->getField('alias');
            $res[$key]['inspect_user'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $value['inspect_user']]])->getField('alias');
            $res[$key]['fmedianame'] = M('tmedia')->cache(true)->where(['fid' => ['EQ', $value['media_id']]])->getField('fmedianame');
            $adInfo = M('tad')->field('fadname, fadclasscode,fadclasscode_v2, fadowner, fbrand')->find($value['fadid']);
            $res[$key]['fadname'] = $adInfo['fadname'];
            $res[$key]['fadclasscode'] = $adInfo['fadclasscode'];
            $res[$key]['fbrand'] = $adInfo['fbrand'];
            $res[$key]['ffullname'] = M('tadclass')->cache(true)->where(['fcode' => ['EQ', $adInfo['fadclasscode']]])->getField('ffullname');
            $res[$key]['adClassV2'] = M('hz_ad_class')->cache(true)->where(['fcode' => ['EQ', $adInfo['fadclasscode_v2']]])->getField('ffullname');
            $res[$key]['adOwnerName'] = M('tadowner')->cache(true)->where(['fid' => ['EQ', $adInfo['fadowner']]])->getField('fname');
            $res[$key]['fadname'] = r_highlight($res[$key]['fadname'], $formData['adName'], 'red');
            $res[$key]['fmedianame'] = r_highlight($res[$key]['fmedianame'], $formData['mediaName'], 'red');
            if($res[$key]['media_class'] == "1" || $res[$key]['media_class'] == "2" ){
                $samTable = $res[$key]['media_class'] == "1" ? 'ttvsample' : 'tbcsample';
                $res[$key]['fadlen'] = M($samTable)->where(['fid'=>$res[$key]['major_key']])->getField('fadlen');
                $res[$key]['fadlen'] = $res[$key]['fadlen'] ? $res[$key]['fadlen'] : 0;
            }else{
                $res[$key]['fadlen'] = '--';
            }
        }
        $data = [
            'data' => $res,
            'total' => $count,
            'page' => $page[0],
            'where' => $mediaWhere
        ];
        return $data;
    }

    /**
     * 基础众包质检任务搜索方法
     * @param $formData array 前端传过来的搜索条件
     * @param $where array 自定义的搜索条件
     * @param int $page
     * @return array
     */
    protected function inspectorTaskInputSearch($formData, $where, $page = [1, 20])
    {
        if ($formData['timeRangeArr']){
            foreach ($formData['timeRangeArr'] as $key => $value) {
                $formData['timeRangeArr'][$key] = $value / 1000;
            }
            $formData['timeRangeArr'][1] = strtotime('+1 day', $formData['timeRangeArr'][1]);
        }
        if ($formData['media_class'] != ''){
            $where['task.media_class'] = ['EQ', $formData['media_class']];
        }
        if ($formData['fillegaltypecode'] != ''){
            $where['fillegaltypecode'] = ['EQ', $formData['fillegaltypecode']];
        }
        if ($formData['task_state'] != ''){
            $where['task_state'] = ['EQ', $formData['task_state']];
        }
        if ($formData['quality_state'] != ''){
            $where['quality_state'] = ['EQ', $formData['quality_state']];
        }
        if ($formData['taskid'] != ''){
            $where['task.taskid'] = ['EQ', $formData['taskid']];
        }
        if ($formData['timeRangeArr'] != ''){
            $where['lasttime'] = ['BETWEEN', $formData['timeRangeArr']];
        }

        $where['cut_err_state'] = ['EQ', 0];

        if ($formData['wx_id'] != ''){
            $wxSubQuery = M('ad_input_user')
                ->field('wx_id')
                ->where([
                    'alias|nickname' => ['LIKE', '%'.$formData['wx_id'].'%']
                ])
                ->buildSql();
            $where['task.wx_id'] = ['EXP', 'IN '.$wxSubQuery];
        }
        // tad表
        $adWhere = [];
        $adClassSubWhere[] = ['NOT IN',['2202','2302']];
        if ($formData['adClass'] != ''){
            $len = strlen($formData['adClass']);
            $adWhere["LEFT(fadclasscode, $len)"] = ['EQ', $formData['adClass']];
        }
        $adWhere['fadclasscode'] = $adClassSubWhere;
        if ($formData['adClass2'] != ''){
            $len = strlen($formData['adClass2']);
            $adWhere["LEFT(fadclasscode_v2, $len)"] = ['EQ', $formData['adClass2']];
        }
        $adSubWhere[] = ['NOT IN',['公益广告', '本媒体宣传', '节目预告']];
        if ($formData['adName'] != ''){
            $adSubWhere[] = ['LIKE', '%'.$formData['adName'].'%'];
            // $adSubWhere['_logic'] = 'AND';
            // $adWhere['fadname'] = ['LIKE', '%'.$formData['adName'].'%'];
        }
        $adWhere['fadname'] = $adSubWhere;
        if ($formData['fadid'] != ''){
            $adWhere['fadid'] = ['EQ', $formData['fadid']];
        }
        if ($formData['fbrand'] != ''){
            $adWhere['fbrand'] = ['LIKE', '%'.$formData['fbrand'].'%'];
        }
        if ($formData['fadowner'] != ''){
            $adOwnerSubQuery = M('tadowner')
                ->field('fid')
                ->where([
                    'fname' => ['LIKE', '%'.$formData['fadowner'].'%']
                ])
                ->buildSql();
            $adWhere['fadowner'] = ['EXP', ' IN '.$adOwnerSubQuery];
        }
        // tmedia表
        $mediaWhere = [];
        if ($formData['mediaName'] != ''){
            $mediaWhere['fmedianame'] = ['LIKE', '%'.$formData['mediaName'].'%'];
        }
        // tmediaowner表
        $mediaOwnerWhere = [];
        if ($formData['regionId'] != ''){
            if ($formData['onlyThisLevel'] != 1){
                $region_id_rtrim = A('Common/System','Model')->get_region_left($formData['regionId']);//地区ID去掉末尾的0
                $region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
                $mediaOwnerWhere["LEFT(fregionid, $region_id_strlen)"] = ['EQ', $region_id_rtrim];
            }else{
                $mediaOwnerWhere['fregionid'] = ['LIKE', '%'.$formData['regionId'].'%'];
            }
            $mediaOwnerSubQuery = M('tmediaowner')
                ->field('fid')
                ->where($mediaOwnerWhere)
                ->buildSql();
            $mediaWhere['fmediaownerid'] = ['EXP', 'IN '. $mediaOwnerSubQuery];//地区搜索条件
        }
        // 子查询
        if ($mediaWhere != []){
            $mediaSubQuery = M('tmedia')
                ->field('fid')
                ->where($mediaWhere)
                ->buildSql();
            $where['media_id'] = ['EXP', 'IN '. $mediaSubQuery]; // 媒体搜索条件
        }
        if ($adWhere != []){
            $adSubQuery = M('tad')
                ->field('fadid')
                ->where($adWhere)
                ->buildSql();
            $where['fadid'] = ['EXP', 'IN '. $adSubQuery]; // 广告搜索条件
        }
        // ttvsample、tbcsample、tpapersample样本表
        $sampleWhere = [
            'fcreatetime' => ['BETWEEN', [date('Y-m-d H:i:s',$formData['timeRangeArr'][0]),date('Y-m-d H:i:s',$formData['timeRangeArr'][1])]]
        ];
        $samTable = 'ttvsample';
        $samId = 'fid';
        switch($formData['media_class']){
            case 1:
                $samTable = 'ttvsample';break;
            case 2:
                $samTable = 'tbcsample';break;
            case 3:
                $samTable = 'tpapersample';
                $samId = 'fpapersampleid';
                break;
            default:
                $samTable = 'ttvsample';break;
        }
        $samSubQuery = M($samTable)
            ->field('DISTINCT '.$samId)
            ->where($sampleWhere)
            ->buildSql();
        $where['sam_id'] = ['EXP','IN '.$samSubQuery];// 查询时间范围内新建的样本任务

        $res = M('task_input')
            ->alias('task')
            ->field("task.taskid,fadid,sam_id, task.media_class, fillegaltypecode, task_state, quality_state, lasttime, cut_err_state, task.wx_id, back_reason, media_id, a.wx_id as input_user, b.wx_id  as inspect_user")
            ->join("left join task_input_link as a on task.taskid = a.taskid and task.media_class = a.media_class and a.type = 1")
            ->join("left join task_input_link as b on task.taskid = b.taskid and task.media_class = b.media_class and b.type = 2")
            ->where($where)
            ->order('lasttime desc')
            ->page($page[0], $page[1])
            ->select();
        $count = M('task_input')
            ->alias('task')
            ->where($where)
            ->count();
        foreach ($res as $key => $value) {
            $res[$key]['lasttime'] = date('Y-m-d H:i:s', $value['lasttime']);
            $res[$key]['alias'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $value['wx_id']]])->getField('alias');
            $res[$key]['input_user'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $value['input_user']]])->getField('alias');
            $res[$key]['inspect_user'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $value['inspect_user']]])->getField('alias');
            $res[$key]['fmedianame'] = M('tmedia')->cache(true)->where(['fid' => ['EQ', $value['media_id']]])->getField('fmedianame');
            $adInfo = M('tad')->field('fadname, fadclasscode,fadclasscode_v2, fadowner, fbrand')->find($value['fadid']);
            $res[$key]['fadname'] = $adInfo['fadname'];
            $res[$key]['fadclasscode'] = $adInfo['fadclasscode'];
            $res[$key]['fbrand'] = $adInfo['fbrand'];
            $res[$key]['ffullname'] = M('tadclass')->cache(true)->where(['fcode' => ['EQ', $adInfo['fadclasscode']]])->getField('ffullname');
            $res[$key]['adClassV2'] = M('hz_ad_class')->cache(true)->where(['fcode' => ['EQ', $adInfo['fadclasscode_v2']]])->getField('ffullname');
            $res[$key]['adOwnerName'] = M('tadowner')->cache(true)->where(['fid' => ['EQ', $adInfo['fadowner']]])->getField('fname');
            $res[$key]['fadname'] = r_highlight($res[$key]['fadname'], $formData['adName'], 'red');
            $res[$key]['fmedianame'] = r_highlight($res[$key]['fmedianame'], $formData['mediaName'], 'red');
            if($res[$key]['media_class'] == "1" || $res[$key]['media_class'] == "2" ){
                $samTable = $res[$key]['media_class'] == "1" ? 'ttvsample' : 'tbcsample';
                $res[$key]['fadlen'] = M($samTable)->where(['fid'=>$res[$key]['sam_id']])->getField('fadlen');
                $res[$key]['fadlen'] = $res[$key]['fadlen'] ? $res[$key]['fadlen'] : 0;
            }else{
                $res[$key]['fadlen'] = '--';
            }
        }
        $data = [
            'data' => $res,
            'total' => $count,
            'page' => $page[0],
            'where' => $mediaWhere
        ];
        return $data;
    }
    
    /**
     * 返回广告类型树(二级)
     * @return array
     */
    protected function getAdClassArrTwo()
    {
        $data = M('tadclass')
            ->field('fcode, fpcode, fadclass')
            ->where(['fstate' => ['EQ', 1], 'fcode' => ['NEQ', 2301]])
            ->cache(true)
            ->select();
        $res = [];
        foreach ($data as $key => &$value) {
            if (strlen($value['fcode']) === 2){
                $res[] = [
                    'value' => $value['fcode'],
                    'label' => $value['fadclass'],
                ];
            }
        }
        foreach ($data as $key => &$value2) {
            $value2['value'] = $value2['fcode'];
            $value2['label'] = $value2['fadclass'];
            if (strlen($value2['fcode']) === 4){
                foreach ($res as &$item) {
                    if ($value2['fpcode'] === $item['value']){
                        $item['children'][] = $value2;
                        break;
                    }
                }
            }
        }
        return $res;
    }

    /**
     * 广告类别树(多级)
     */
    protected function getAdClassArr()
    {
        $data = M('tadclass')
            ->cache(true)
            ->field('fcode value, fpcode, fadclass label')
            ->where(['fstate' => ['EQ', 1], 'fcode' => ['NEQ', 2301]])
            ->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpcode']])){
                $items[$value['fpcode']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }

    /**
     * 获取一级行政区划
     */
    protected function getRegionArr()
    {
        $data = M('tregion')->cache(true)->field('fid value, fname label, ffullname')->where([
            'fstate' => ['EQ', 1],
            'flevel' => ['EQ', 1],
        ])->select();
        foreach ($data as &$datum) {
            $datum['children'] = [];
        }
        return $data;
    }

    /**
     * 获取客户列表
     */
    protected function getCustomerArr()
    {
        $data = M('customer')
            ->cache(true)
            ->field('cr_num value, cr_name label')
            ->where(['cr_status' => ['EQ', 1]])
            ->group('cr_num')
            ->select();
        foreach ($data as &$datum) {
            $datum['children'] = [];
        }
        return $data;
    }

    /**
     * 返回违法表现树
     * @return array
     */
    protected function getIllegalArr()
    {
        $data = M('tillegal')->field('fcode, fpcode, fexpression, fconfirmation, fillegaltype')->where(['fstate' => ['EQ', 1]])->cache(true)->select();
        $res = [];
        foreach ($data as &$datum) {
            $datum['id'] = $datum['fcode'];
            $datum['label'] = $datum['fexpression'];
            if (!$datum['fpcode']){
                $res[] = $datum;
            }
        }
        foreach ($data as $item) {
            foreach ($res as &$re) {
                if ($item['fpcode'] === $re['fcode']){
                    $re['children'][] = $item;
                    break;
                }
            }
        }
        return $res;
    }

    /**
     * 处理提交的剪辑错误
     * @param $taskid
     * @param $reason
     */
    public function submitCutError($taskid, $reason)
    {

        if(!A('InputPc/Task','Model')->user_other_authority(self::USER_AUTH_CODE)){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        $taskInfo = M('task_input')->where(['taskid' => ['EQ', $taskid]])->find();
        $taskUserId = $taskInfo['wx_id'];
        if (!A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)){
            if ($taskUserId != session('wx_id')){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '此任务当前不是你的任务, 刷新页面后如果没有新任务请联系管理员'
                ]);
            }
            if ($taskInfo['task_state'] != 1 && $taskInfo['task_state'] != 4){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '该任务状态异常, 请刷新页面'
                ]);
            }
        }
        $where = [
            'taskid' => ['EQ', $taskid]
        ];
        $data = [
            'cut_err_reason' => $reason,
            'lasttime'       => time(),
            'cut_err_state'  => 1,
            'task_state'     => 2,
            'syn_state'      => 0
        ];
        //如果剪辑错误中有 '地方方言' 一项, 就释放任务并调低优先级, 不退回
        if (in_array('11', explode(',', explode(' ;; ', $reason)[0]))){
            // 查到该媒体的行政区划代码前四位, 将其负数保存在优先级
            $regionId = M()->cache(true)->query("select left(fregionid, 4) as regionId from tmediaowner where fid = (select fmediaownerid from tmedia where fid = {$taskInfo['media_id']})")[0]['regionId'];
            $data['priority']       = $regionId ? $regionId * -1: 11;
            $data['cut_err_reason'] = '此广告为地方方言';
            $data['cut_err_state']  = 0;
            $data['task_state']     = 0;
            $data['wx_id']          = 0;
            $data['syn_state']      = 1;
            $data['fadid']          = A('Common/Ad','Model')->getAdId('方言广告','','','','方言特例_王裕厚','');//获取广告ID
            M('task_input_flow')->add([
                'taskid'       => $taskid,
                'flow_content' => '此广告为地方方言',
                'flow_time'    => time(),
                'wx_id'        => session('wx_id')
            ]);
            $res = M('task_input')->where($where)->save($data);
            if ($res){
                A('Common/AliyunLog','Model')->task_input_log([
                    'type'   => 'save',
                    'data'   => $data,
                    'msg'    => '该任务为地方方言, 保存成功;',
                    'taskid' => $taskid,
                ]);
            }else{
                A('Common/AliyunLog','Model')->task_input_log([
                    'type'   => 'save',
                    'data'   => $data,
                    'msg'    => '该任务为地方方言, 保存失败;',
                    'taskid' => $taskid,
                ]);
            }
            $res = $res === false ? -1 : 0;
            $this->ajaxReturn([
                'code' => $res
            ]);
        }

        $flowData = [
            'taskid'       => $taskid,
            'flow_content' => '提交剪辑错误, 理由: ' . $reason,
            'flow_time'    => time(),
            'wx_id'        => session('wx_id'),
            'task_info'    => json_encode($taskInfo),
        ];
        /*
         * 如果提交了剪辑错误, 无需审核, 直接做同步时相对应的处理
         */
        $sampleClassArr = [
            '',
            'tv',
            'bc',
            'paper',
        ];
        $sampleClass = $sampleClassArr[$taskInfo['media_class']];

        $sampleTable = 't'.$sampleClass.'sample';
        if ($sampleClass == 'paper'){
            A('TaskInput/CutErr', 'Model')->cutErr($sampleClass, $taskInfo['sam_id'], $reason);
            A('Common/AliyunLog','Model')->task_input_log([
                'type' => 'save',
                'task' => $taskInfo,
                'reason' => $reason,
                'msg' => '报纸剪辑错误;'
            ]);
        }else{
            // 删除掉该该媒体该uuid的样本, 将不是该媒体但是相同uuid的版本更新到此任务, 并且记录
            $sampleList = M($sampleTable)
                ->where(array(
								'uuid' => $taskInfo['sam_uuid'],
								'fmediaid' => $taskInfo['media_id']
							))
                ->getField('fid', true);

            foreach ($sampleList as $item) {
                A('TaskInput/CutErr', 'Model')->cutErr($sampleClass, $item, $reason);
            }
            // 找到同uuid不同媒体相同的样本, 并且更新
            $tempSample = M($sampleTable)
                ->field('fissuedate, fmediaid, favifilename')
                ->where([
                    'fstate' => ['EQ', 1],
                    'uuid' => ['EQ', $taskInfo['sam_uuid']],
                    'fmediaid' => ['NEQ', $taskInfo['media_id']],
                ])
                ->find();
            if ($tempSample){
                $data = [
                    'lasttime' => time(),
                    'cut_err_state' => 0,
                    'task_state' => 0,
                    'wx_id' => 0,
                    'syn_state' => 0,
                    'media_id' => $tempSample['fmediaid'],
                    'issue_date' => $tempSample['fissuedate'],
                    'source_path' => $tempSample['favifilename'],
                ];
                $flowData['flow_content'] = '该媒体剪辑错误, 已经替换不同媒体同uuid样本, 剪辑错误理由: ' . $reason;
                A('Common/AliyunLog','Model')->task_input_log([
                    'type' => 'save',
                    'task' => $taskInfo,
                    'reason' => $reason,
                    'tempSample' => $tempSample,
                    'msg' => '剪辑错误, 更新了同媒体同uuid的样本;',
                ]);
            }else{
                A('Common/AliyunLog','Model')->task_input_log([
                    'type' => 'save',
                    'task' => $taskInfo,
                    'reason' => $reason,
                    'msg' => '剪辑错误, 没有找到不同媒体同uuid样本;'
                ]);
            }
            M('task_input_link')
                ->where([
                    'taskid' => ['EQ', $taskid],
                    'state' => 1,
                    'media_class' => ['EQ', $taskInfo['media_class']],
                ])
                ->save([
                    'state' => 0,
                    'update_time' => time(),
                ]);
        }


        $res = M('task_input')->where($where)->save($data) === false ? -1 : 0;
        if ($res){
            A('Common/AliyunLog','Model')->task_input_log([
                'type' => 'save',
                'data' => $data,
                'msg' => '任务剪辑错误, 保存成功;',
                'taskid' => $taskid,
            ]);
        }else{
            A('Common/AliyunLog','Model')->task_input_log([
                'type' => 'save',
                'data' => $data,
                'msg' => '任务剪辑错误, 保存失败;',
                'taskid' => $taskid,
            ]);
        }
        $res = $res === false ? -1 : 0;
        M('task_input_flow')->add($flowData);
        A('InputPc/Task','Model')->change_task_count(session('wx_id'),self::CUT_ERROR_COUNT_FIELD,1);
        $this->ajaxReturn([
            'code' => $res
        ]);
    }

    /**
     * 接收 录入员 主动撤回 请求
     * @param $taskid
     */
    public function receiveRecallRequest($taskid)
    {
        if(  !A('InputPc/Task','Model')->user_other_authority(self::USER_AUTH_CODE)){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        if ($this->isNetAd == 2){
            $netAdController = new NetAdController();
            $netAdController->receiveRecallRequest($taskid);
            return true;
        }elseif($this->isNetAd == 3){
            $outAdController = new OutAdController();
            $outAdController->receiveRecallRequest($taskid);
            return true;
        }
        $taskData = M('task_input')->find($taskid);
        $wxId = $taskData['wx_id'];
        if ($wxId != session('wx_id')) {
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '只有本人才可以撤回任务',
                'type' => 'error',
            ]);
        }elseif ($taskData['task_state'] != 2 && $taskData['quality_state'] != 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '当前任务状态不允许退回',
                'type' => 'error',
            ]);
        }else{
            M()->startTrans();
            $taskInputData = [
                'lasttime' => time(),
                'task_state' => 5,
                'syn_state' => 0
            ];
            $taskInputUpdate = M('task_input')->where(['taskid' => ['EQ', $taskid]])->save($taskInputData) === false ? false : true;
            $flowData = [
                'taskid' => $taskid,
                'flow_content' => '提交撤回申请',
                'wx_id' => $wxId,
                'flow_time' => time()
            ];
            $taskFlowUpdate = M('task_input_flow')->add($flowData) === false ? false : true;
            if ($taskInputUpdate && $taskFlowUpdate){
                M()->commit();
                A('Common/AliyunLog','Model')->task_input_log([
                    'type' => 'save',
                    'task' => $taskData,
                    'wx_id' => $wxId,
                    'msg' => '提交了主动撤回请求, 处理成功'
                ]);
                $this->ajaxReturn([
                    'code' => 0,
                    'msg' => '发出请求成功,待质检员审核后任务将自动退回',
                    'type' => 'success',
                ]);
            }else{
                M()->rollback();
                A('Common/AliyunLog','Model')->task_input_log([
                    'type' => 'save',
                    'task' => $taskData,
                    'wx_id' => $wxId,
                    'msg' => '提交了主动撤回请求, 处理失败'
                ]);
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '未知错误,请检查网络或联系管理员',
                    'type' => 'error',
                ]);
            }
        }
    }

    /**
     * 退回任务请求
     * @param $taskid
     * @param $mediaClass
     * @param string $reason
     * @param int $linkType
     */
    public function submitRejectTaskRequest($taskid, $mediaClass, $reason='', $linkType=0)
    {
        // 任务的用户id
        $wx_id = session('wx_id');
        $taskUserId = M('task_input')->where(['taskid' => ['EQ', $taskid]])->getField('wx_id');
        if(  !A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)  &&  $wx_id != $taskUserId){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        // 判断用户是做网络广告还是传统媒体广告
        if ($mediaClass == 13){
            $NetAdModel = new NetAdModel();
            $res = $NetAdModel->rejectTask($taskid, session('wx_id'), $mediaClass, $linkType, $reason);
            if ($res){
                $this->ajaxReturn([
                    'code' => 0,
                    'msg' => '退回成功',
                ]);
            }else{
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => $NetAdModel->getError(),
                ]);
            }
        }
        M()->startTrans();
        $taskModel = new TaskInputModel();
        $res = $taskModel->rejectTask($taskid, session('wx_id'), $mediaClass, $linkType, $reason);
        if ($res){
            M()->commit();
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '退回成功',
            ]);
        }else{
            M()->rollback();
            $this->ajaxReturn([
                'code' => -1,
                'msg' => $taskModel->getError(),
            ]);
        }
    }

    /**
     * 通过任务的私有方法
     * @param $taskid
     * @return bool
     */
    protected function passTaskMethod($taskid)
    {
        if(  !A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        $wx_id = session('wx_id');
        $time = time();
        $where = [
            'taskid' => ['EQ', $taskid]
        ];
        $taskData = [
            'quality_wx_id' => $wx_id,
            'quality_state' => 2,
            'task_state' => 2,
            'syn_state' => 1,
            'lasttime' => $time,
        ];
        $flowData = [
            'taskid' => $taskid,
            'flow_content' => '质检通过',
            'flow_time' => $time,
            'wx_id' => $wx_id
        ];
        $taskUpdate = M('task_input')->where($where)->save($taskData);
        $flowUpdate = M('task_input_flow')->add($flowData);

        if ($taskUpdate && $flowUpdate){
            A('InputPc/Task','Model')->change_task_count($wx_id, self::QUALITY_COUNT_FIELD, 1);
            return true;
        }else{
            return false;
        }
    }

    /**
     * 通过任务
     * @param $taskid
     */
    public function passTask($taskid)
    {
        if(  !A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        M()->startTrans();
        if ($this->isNetAd == 2){
            $netAdController = new NetAdController();
            $res = $netAdController->passTaskMethod($taskid);
        }elseif($this->isNetAd == 3){
            $outAdController = new OutAdController();
            $res = $outAdController->passTaskMethod($taskid);
        }else{
            $res = $this->passTaskMethod($taskid);
        }
        if ($res){
            M()->commit();
            A('Common/AliyunLog','Model')->task_input_log([
                'type' => 'save',
                'task' => $taskid,
                'msg' => '任务通过成功'
            ]);
            $this->ajaxReturn([
                'code' => 0
            ]);
        }else{
            M()->rollback();
            A('Common/AliyunLog','Model')->task_input_log([
                'type' => 'save',
                'task' => $taskid,
                'msg' => '任务通过失败'
            ]);
            $this->ajaxReturn([
                'code' => -1
            ]);
        }
    }

    /**
     * 通过任务(质检调用)
     * @param string $taskid 任务ID
     * @param string $mediaclass 媒体类别
     */
    public function passTaskByIns($taskid,$mediaclass){
        if(  !A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        M()->startTrans();
        if (in_array($mediaclass,['13',13])){
            $netAdController = new NetAdController();
            $res = $netAdController->passTaskMethod($taskid);
        }elseif(in_array($mediaclass,['05',5])){
            $outAdController = new OutAdController();
            $res = $outAdController->passTaskMethod($taskid);
        }else{
            $res = $this->passTaskMethod($taskid);
        }
        if ($res){
            M()->commit();
            A('Common/AliyunLog','Model')->task_input_log([
                'type' => 'save',
                'task' => $taskid,
                'msg' => '任务通过成功'
            ]);
            return true;
        }else{
            M()->rollback();
            A('Common/AliyunLog','Model')->task_input_log([
                'type' => 'save',
                'task' => $taskid,
                'msg' => '任务通过失败'
            ]);
            return false;
        }
    }

    /**
     * 检查批量修改任务权限
     * @return bool
     */
    private function batchModifyTaskInputAuth()
    {
        $wx_id = session('wx_id');
        $nameList = sys_c('batch_modify_task_input_user');
        if ($nameList){
            $nameList = explode(',', $nameList);
            $where = '';
            foreach ($nameList as $item) {
                $where .= "alias LIKE '$item%' or ";
            }
            $where = rtrim($where, 'or ');
            $res = M('ad_input_user')
                ->where($where)
                ->cache(60)
                ->getField('wx_id', true);
            return in_array($wx_id, $res);
        }else{
            return false;
        }
    }

    /**
     * 任务批量修改页面
     * @return bool
     */
    public function taskBatchModify()
    {
        if(!$this->batchModifyTaskInputAuth()){
            echo '<h1>无权限, 请联系管理员</h1>';
            return false;
        }
        if (IS_POST){
            $where = [
                'task_state' => 2,
                'quality_state' => ['NEQ', 1],
                'cut_err_state' => 0,
            ];
            $formData = I('post.where');
            $page = [I('page'), I('pageSize')];
            if($formData['media_class'] == '13'){
                $data = $this->baseNetAdTaskSearch($formData, $where, $page);
            }else{
                $data = $this->baseTaskInputSearch($formData, $where, $page);
            }
            $this->ajaxReturn($data);
        }else{
            $illegalTypeArr = M('tillegaltype')->field('fcode value, fillegaltype text')->cache(true)->where(['fstate' => ['EQ', 1]])->select();
            $role = 'inspector';
            $type = 'quality';
            $illegalTypeArr = json_encode($illegalTypeArr);
            $adClassArr = json_encode($this->getAdClassArr());
            $regionTree = json_encode($this->getRegionTree());
            $illegalArr = json_encode($this->getIllegalArr());
            $adClassTree = json_encode($this->getAdClassTree()); // 新广告类型分类
            $this->assign(compact('illegalTypeArr', 'regionTree', 'adClassArr', 'illegalArr', 'adClassTree', 'type', 'role'));
            $this->display();
        }
    }

    /**
     * 批量修改任务广告信息
     * @param $adInfo
     * @param $taskIds
     */
    public function submitTaskAdInfoBatchModify($adInfo, $taskIds, $isNetAd)
    {
        if(!$this->batchModifyTaskInputAuth()){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        if($isNetAd == 1){
            $this->submitNetTaskAdInfoBatchModify($adInfo, $taskIds);
            return;
        }
        
        $wxId = session('wx_id');
        $wxInfo = M('ad_input_user')->find($wxId);
        $taskIds = explode(',',$taskIds);
        $fadname = $adInfo['fadname'];
        $fbrand = $adInfo['fbrand'];
        $adclass_code = $adInfo['fadclasscode'];
        $adclass_code_v2 = $adInfo['fadclasscode_v2'];
        $adowner_name = $adInfo['fname'];
        // 任务信息
        $taskInputData = [];
        $taskInputData['fadid'] = A('Common/Ad','Model')->getAdId($fadname,$fbrand,$adclass_code,$adowner_name,$wxInfo['alias'], $adclass_code_v2);//获取广告ID
        $taskInputData['syn_state'] = 1;
//        $taskInputData['quality_state'] = 2;
        $taskInputData['quality_wx_id'] = $wxId;
        $time = time();
        $flowArr = [];
        foreach ($taskIds as $taskId) {
            $flowArr[] = [
                'taskid' => $taskId,
                'flow_content' => '批量修改任务广告信息',
                'flow_time' => $time,
                'wx_id' => $wxId
            ];
        }
        if (!$taskIds || count($taskIds) == 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '没有选中任务',
            ]);
        }else{
            M()->startTrans();
            $taskUpdate = M('task_input')
                ->where(['taskid' => ['IN', $taskIds]])
                ->save($taskInputData) === false ? false : true;
            $flowUpdate = M('task_input_flow')->addAll($flowArr);
            if ($taskUpdate && $flowUpdate){
                M()->commit();
                A('Common/AliyunLog','Model')->task_input_log([
                    'type' => 'save',
                    'taskIds' => $taskIds,
                    'adInfo' => $adInfo,
                    'msg' => '批量修改广告任务信息成功'
                ]);
                $this->ajaxReturn([
                    'code' => 0,
                    'msg' => '修改成功'
                ]);
            }else{
                M()->rollback();
                A('Common/AliyunLog','Model')->task_input_log([
                    'type' => 'save',
                    'taskIds' => $taskIds,
                    'adInfo' => $adInfo,
                    'msg' => '批量修改广告任务信息失败'
                ]);
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '修改失败'
                ]);
            }
        }
    }

    /**
     * 批量修改任务广告信息-互联网任务
     * @param $adInfo
     * @param $taskIds
     */
    public function submitNetTaskAdInfoBatchModify($adInfo, $taskIds)
    {
        if(!$this->batchModifyTaskInputAuth()){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        $wxId = session('wx_id');
        $wxInfo = M('ad_input_user')->find($wxId);
        $taskIds = explode(',',$taskIds);
        $fadname = $adInfo['fadname'];
        $fbrand = $adInfo['fbrand'];
        $adclass_code = $adInfo['fadclasscode'];
        $adclass_code_v2 = $adInfo['fadclasscode_v2'];
        $adowner_name = $adInfo['fname'];
        // 任务信息
        $taskInputData = [];
        $taskInputData['fadid'] = A('Common/Ad','Model')->getAdId($fadname,$fbrand,$adclass_code,$adowner_name,$wxInfo['alias'], $adclass_code_v2);//获取广告ID
        $taskInputData['syn_state'] = 1;
        $taskInputData['quality_wx_id'] = $wxId;
        $time = time();
        $flowArr = [];
        foreach ($taskIds as $taskId) {
            $flowArr[] = [
                'taskid' => $taskId,
                'log' => '批量修改任务广告信息',
                'createtime' => $time,
                'wx_id' => $wxId
            ];
        }
        if (!$taskIds || count($taskIds) == 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '没有选中任务',
            ]);
        }else{
            M()->startTrans();
            $taskUpdate = M('tnettask')
                ->where(['taskid' => ['IN', $taskIds]])
                ->save($taskInputData) === false ? false : true;
            $flowUpdate = M('tnettask_flowlog')->addAll($flowArr);
            if ($taskUpdate && $flowUpdate){
                M()->commit();
                A('Common/AliyunLog','Model')->task_input_log([
                    'type' => 'save',
                    'taskIds' => $taskIds,
                    'adInfo' => $adInfo,
                    'msg' => '批量修改广告任务信息成功'
                ]);
                $this->ajaxReturn([
                    'code' => 0,
                    'msg' => '修改成功'
                ]);
            }else{
                M()->rollback();
                A('Common/AliyunLog','Model')->task_input_log([
                    'type' => 'save',
                    'taskIds' => $taskIds,
                    'adInfo' => $adInfo,
                    'msg' => '批量修改广告任务信息失败'
                ]);
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '修改失败'
                ]);
            }
        }
    }

    /**
     * 批量修改任务违法判定信息
     * @param $illegalInfo
     * @param $taskIds
     */
    public function submitTaskIllegalInfoBatchModify($illegalInfo, $taskIds)
    {
        if(!$this->batchModifyTaskInputAuth()){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        $wxId = session('wx_id');
        $taskIds = explode(',',$taskIds);
        // 任务信息
        $taskInputData = [];
        if ($illegalInfo['fillegaltypecode'] != 0){
            $taskInputData['fexpressioncodes'] = $illegalInfo['fexpressioncodes'];
            if(!empty($illegalInfo['fillegalcontent'])){
                // 前端未传违法内容时，不更新，避免覆盖原违法内容
                $taskInputData['fillegalcontent'] = $illegalInfo['fillegalcontent'];
            }
        }else{
            $taskInputData['fexpressioncodes'] = '';
            $taskInputData['fillegalcontent'] = '';
        }
        $taskInputData['fillegaltypecode'] = $illegalInfo['fillegaltypecode'];
        $taskInputData['syn_state'] = 1;
//        $taskInputData['quality_state'] = 2;
        $taskInputData['quality_wx_id'] = $wxId;
        $time = time();
        $flowArr = [];
        foreach ($taskIds as $taskId) {
            $flowArr[] = [
                'taskid' => $taskId,
                'flow_content' => '批量修改任务违法判定信息',
                'flow_time' => $time,
                'wx_id' => $wxId
            ];
        }
        if (!$taskIds || count($taskIds) == 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '没有选中任务',
            ]);
        }else{
            M()->startTrans();
            $taskUpdate = M('task_input')
                ->where(['taskid' => ['IN', $taskIds]])
                ->save($taskInputData) === false ? false : true;
            $flowUpdate = M('task_input_flow')->addAll($flowArr);
            if ($taskUpdate && $flowUpdate){
                M()->commit();
                A('Common/AliyunLog','Model')->task_input_log([
                    'type' => 'save',
                    'taskIds' => $taskIds,
                    'illegalInfo' => $illegalInfo,
                    'msg' => '批量修改违法判定信息成功'
                ]);
                $this->ajaxReturn([
                    'code' => 0,
                    'msg' => '修改成功'
                ]);
            }else{
                M()->rollback();
                A('Common/AliyunLog','Model')->task_input_log([
                    'type' => 'save',
                    'taskIds' => $taskIds,
                    'illegalInfo' => $illegalInfo,
                    'msg' => '批量修改违法判定信息失败'
                ]);
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '修改失败'
                ]);
            }
        }
    }

    /**
     * 同步任务表与样本表
     */
    public function sampleTableSync()
    {
        header("Content-type: text/html; charset=utf-8");
        //exit('暂停使用');
        if(I('auto') == '1'){
            echo '<meta http-equiv="refresh"content="0.1"/>';
        }
        $sampleTables = [
            [],
            [
                'media_class' => 1,
                'table' => 'ttvsample',
                'primary_key' => 'fid',
                'str' => 'tv',
            ],
            [
                'media_class' => 2,
                'table' => 'tbcsample',
                'primary_key' => 'fid',
                'str' => 'bc',
            ],
            [
                'media_class' => 3,
                'table' => 'tpapersample',
                'primary_key' => 'fpapersampleid',
                'str' => 'paper',
            ],
        ];
        $where = [
            'syn_state' => ['EQ', 1],
            'lasttime' => ['LT', strtotime('-60 second')],
        ];
        // 得到需同步的任务
        $taskList = M('task_input')
            ->field('taskid, media_class, sam_id, fadid, fversion, fspokesman, fillegaltypecode, fillegalcontent, fexpressioncodes, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, wx_id fmodifier, lasttime fmodifytime, cut_err_state, cut_err_reason, is_long_ad, sam_uuid, wx_id finputuser, wx_id finspectuser')
            ->where($where)
            ->limit(0, 1000)
            ->select();
            
        $taskids = []; // 用来保存同步成功的taskid
        foreach ($taskList as $task) {
            $task['media_class'] = intval($task['media_class']);
            $task['finputstate'] = 2;
            $task['finspectstate'] = 2;
            $task['fmodifytime'] = date('Y-m-d H:i:s', $task['fmodifytime'] );
            $task['fmodifier'] = M('ad_input_user')->cache(true, 60)->where(['wx_id' => ['EQ', $task['fmodifier']]])->getField('alias') . '_' . $task['fmodifier'];
            // 处理违法广告
            if ($task['fillegaltypecode'] != 0){
                $expressionCodes = explode(';', $task['fexpressioncodes']);
                $expressionsAndConfirmation = M('tillegal')->field('fexpression, fconfirmation')->cache(true)->where(['fcode' => ['IN', $expressionCodes]])->select();
                $task['fexpressions'] = '';
                $task['fconfirmations'] = '';
                foreach ($expressionsAndConfirmation as $item) {
                    $task['fexpressions'] .= $item['fexpression'] . ' ;; ' ;
                    $task['fconfirmations'] .= $item['fconfirmation'] . ' ;; ' ;
                }
            }else{
                $task['fillegalcontent'] = '';
                $task['fexpressioncodes'] = '';
                $task['fexpressions'] = '';
                $task['fconfirmations'] = '';
            }

            // 更新对应的样本表
            if ($sampleTables[$task['media_class']]['str'] == 'paper'){
                $sampleWhere = [
                    'fpapersampleid' => ['EQ', $task['sam_id']]
                ];
            }else{
                $sampleWhere = [
                    'uuid' => ['EQ', $task['sam_uuid']]
                ];
            }
            $sampleWhere['need_sync'] = ['EQ', 1];
            $sampleUpdate = M($sampleTables[$task['media_class']]['table'])
                ->where($sampleWhere)
                ->save($task);// === false ? false : true;
			if(	$task['media_class'] == 1) M('tbcsample')->where(array('uuid'=>$task['sam_uuid'],'need_sync'=>1))->save($task);
			if(	$task['media_class'] == 2) M('ttvsample')->where(array('uuid'=>$task['sam_uuid'],'need_sync'=>1))->save($task);
            if( $sampleUpdate !== false){
                $sampleUpdate = true;
            }
            $taskids[] = $task['taskid']; // 将更新成功的任务id记下来
            if (!$sampleUpdate){
                file_put_contents('LOG/TASK_SYN_ERR',date('Y-m-d H:i:s').'  '.$task['taskid']."\n",FILE_APPEND);
            }
        }
        // 更新同步成功了的任务同步状态
        $time = time();
        if (count($taskids) > 0){
            M('task_input')->where(['taskid' => ['IN', $taskids]])->save(['syn_state' => 2]);
            $flowArr = [];
            foreach ($taskids as $taskid) {
                $flowArr[] = [
                    'taskid' => $taskid,
                    'flow_content' => '系统定时同步已同步',
                    'flow_time' => $time,
                ];
            }
            M('task_input_flow')->addAll($flowArr);
            A('Common/AliyunLog','Model')->task_input_log([
                'type' => 'save',
                'data' => $taskids,
                'msg' => '系统定时同步已同步;'
            ]);
        }
        // 提醒退回时间超过24小时的任务
        $userIds = M('task_input')
            ->where([
                'task_state' => ['EQ', 4],
                'cut_err_state' => ['EQ', 0],
                'lasttime' => ['LT', strtotime('-1 day')]
            ])
            ->group('wx_id')
            ->getField('wx_id', true);
        foreach ($userIds as $userId) {
            if (!S('task_back_wx_msg_'.$userId)){
                A('Adinput/Weixin','Model')->task_back($userId,'你有任务被退回已经超过24小时','请尽快处理');
                S('task_back_wx_msg_'.$userId, 1, 3600 * 8);
            }
        }
        // 退回超过2小时的任务会被释放掉
        $linkIds = M('task_input_link')
            ->where([
                'media_class' => ['NEQ', 13],
                'state' => ['EQ', 4],
                'update_time' => ['LT', strtotime('-2 hours')],
            ])
            ->getField('taskid', true);
        if ($linkIds){
            $where = [
                'taskid' => ['IN', $linkIds],
                'cut_err_state' => ['EQ', 0],
            ];
        }else{
            $where = [
                'task_state' => ['EQ', 4],
                'cut_err_state' => ['EQ', 0],
                'lasttime' => ['LT', strtotime('-2 hours')],
            ];
        }
        $taskIds = M('task_input')
            ->where($where)
            ->select();
        $flowArr = [];
        foreach ($taskIds as $item) {
            // 更新任务表
            $taskUpdate = M('task_input')
                ->where([
                    'taskid' => ['EQ', $item['taskid']]
                ])
                ->save([
                    'task_state' => 0,
                    'wx_id' => 0,
                ]) !== false;
            // 更新流程表
            if ($taskUpdate){
                M('task_input_link')
                    ->where([
                        'taskid' => ['EQ', $item['taskid']],
                        'media_class' => ['EQ', $item['media_class']],
                        'state' => ['EQ', 4],
                    ])
                    ->save([
                        'state' => 0,
                        'wx_id' => 0,
                        'update_time' => $time,
                    ]);
            }
            if ($taskUpdate){
                $flowArr[] = [
                    'taskid' => $item['taskid'],
                    'flow_content' => '任务被退回超过2小时, 自动释放',
                    'flow_time' => $time,
                    'task_info' => json_encode($item),
                ];
            }
        }
        M('task_input_flow')->addAll($flowArr);
    }

    /**
     * 查看任务统计
     * @return bool
     */
    public function checkTaskCount()
    {
        if(  !A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)){
            echo '<h1>无权限, 请联系管理员</h1>';
            return false;
        }
        if (IS_POST){
            $where = [];
            $timeRange = I('where')['timeRange'];
            $startTime = $timeRange[0];
            $endTime = $timeRange[1];
            if ($startTime == '' && $endTime == ''){
                $startTime = date('Y-m-d');
                $endTime = $startTime;
            }
            if($startTime != '' && $endTime != ''){
                $where['count_date'] = ['BETWEEN', [$startTime, $endTime]];
            }
            $data = M('user_task_count')
                ->field('sum(v3_paper_task) sum_paper_task, sum(v3_net_task) sum_net_task, sum(v3_bc_task) sum_bc_task, sum(v3_tv_task) sum_tv_task, sum(v3_sub_cut_err) sum_cut_err, sum(v3_net_cut_err) sum_net_cut_err, sum(v3_task_back) sum_task_back, sum(v3_quality) sum_quality, sum(v3_ad_sure) sum_ad_sure')
                ->where($where)
                ->find();
            $code = 0;
            $msg = '获取成功';
            if ($data['sum_paper_task'] == null){
                $code = -1;
                $msg = '选择时间段内没有统计数据';
            }
            $this->ajaxReturn([
                'code' => $code,
                'data' => $data,
                'msg' => $msg,
                'where' => $where,
            ]);
        }else{
            $this->display();
        }
    }

    /**
     * 广告基本信息展示与确认
     */
    public function adList()
    {
        die('<h1>该功能暂停使用</h1>');

        if(  !A('InputPc/Task','Model')->user_other_authority(self::USER_AUTH_CODE)){
            echo '<h1>无权限, 请联系管理员</h1>';
            return false;
        }
        if (IS_POST){
            $wx_id = session('wx_id');
            $alias = M('ad_input_user')
                ->where([
                    'wx_id' => ['EQ', $wx_id]
                ])
                ->cache(60)
                ->getField('alias');
            $page = I('page', 1);
            $where = I('where');
            $adWhere = [
                'fcreator' => ['EQ', $alias]
            ];
            if ($where['adName'] != ''){
                $adWhere['fadname'] = ['LIKE', '%'.$where['adName'].'%'];
            }
            if ($where['adClass'] != ''){
                $adWhere['fadclasscode'] = ['EQ', $where['adClass']];
            }
            if ($where['adClass2'] != ''){
                $adWhere['fadclasscode_v2'] = ['EQ', $where['adClass2']];
            }
            if ($where['confirm'] != ''){
                $adWhere['is_sure'] = ['EQ', $where['confirm']];
            }
            if ($where['adBrand'] != ''){
                $adWhere['fbrand'] = ['LIKE', '%'.$where['adBrand'].'%'];
            }
            if ($where['adOwner'] != ''){
                $adWhere['fadowner'] = ['EXP', 'IN '.M('tadowner')->field('fid')->where(['fname' => ['LIKE', '%'.$where['adOwner'].'%']])->buildSql()];
            }
            $data = M('tad')
                ->where($adWhere)
                ->order('fcreatetime desc')
                ->page($page, 10)
                ->select();
            $total = M('tad')->where($adWhere)->count();

            foreach ($data as &$datum) {
                $datum['ffullname'] = M('tadclass')->cache(true)->where(['fcode' => ['EQ', $datum['fadclasscode']]])->getField('ffullname');
                $datum['fadclasscode_v2'] = M('hz_ad_class')->cache(true)->where(['fcode' => ['EQ', $datum['fadclasscode_v2']]])->getField('ffullname');
                $datum['fname'] = M('tadowner')->cache(true, 60)->where(['fid' => ['EQ', $datum['fadowner']]])->getField('fname');
                $datum['isConfirm'] = $datum['is_sure'] == 0 ? '未确认' : '确认';
                $datum['fadname'] = r_highlight($datum['fadname'], $where['adName'], 'red');
                $datum['fbrand'] = r_highlight($datum['fbrand'], $where['adBrand'], 'red');
                $datum['fname'] = r_highlight($datum['fname'], $where['adOwner'], 'red');
            }
            $this->ajaxReturn([
                'data' => $data,
                'total' => $total,
                'code' => 0,
                'adWhere' => $adWhere,
                'where' => $where,
            ]);
        }else{
            $adClassArr = json_encode($this->getAdClassArr());
            $adClassTree = json_encode($this->getAdClassTree());
            $this->assign(compact('adClassArr', 'adClassTree'));
            $this->display();
        }
    }

    /**
     * 违法复查获取m3u8地址
     * @param $mediaId
     * @param $startTime    int  时间戳
     * @param $endTime      int  时间戳
     */
    public function getM3U8Url($mediaId,$startTime,$endTime)
    {
        $url = A('Common/Media','Model')->get_m3u8($mediaId,$startTime,$endTime);
        $this->ajaxReturn([
            'data' => $url
        ]);
    }


    /**
     * 报纸众包任务获取版面
     * @param $taskid
     */
    public function getPaper($taskid)
    {
        $samId = M('task_input')->cache(true,10)->where(array('taskid'=>$taskid))->getField('sam_id');
        $sourceUrl = A('Common/Paper','Model')->get_slim_source($samId);
        $this->ajaxReturn([
            'data' => $sourceUrl
        ]);
    }


    /**
     * 组长查看本组的任务统计
     */
    public function checkTaskCountByGroup()
    {
        $userInfo = M('ad_input_user')->find(session('wx_id'));
        if ($userInfo['is_leader'] != 1){
            echo '<h1>只有组长才可以查看</h1>';
            return;
        }
        if (IS_POST){
            $countWhere = [];
            $page = I('page', 1);
            $where = I('where');
            // 处理时间条件, 默认是当天
            $timeRange = $where['timeRange'];
            $startTime = $timeRange[0];
            $endTime = $timeRange[1];
            if ($startTime == '' && $endTime == ''){
                $startTime = date('Y-m-d');
                $endTime = $startTime;
            }
            if($startTime != '' && $endTime != ''){
                $countWhere['count_date'] = ['BETWEEN', [$startTime, $endTime]];
            }
            $userWhere = [
                'group_id' => ['EQ', $userInfo['group_id']]
            ];
            // 用户表的子查询
            if ($where['alias'] != '' || $userWhere != []){
                $userWhere['alias'] = ['LIKE' , '%' .$where['alias'].'%'];
            }
            $userSubQuery = M('ad_input_user')->field('wx_id')->where($userWhere)->buildSql();
            $countWhere['user_id'] = ['EXP', 'IN ' . $userSubQuery];

            $data = M('user_task_count')
                ->field("
                user_id,
                sum(v3_paper_task) v3_paper_task, 
                sum(v3_bc_task) v3_bc_task, 
                sum(v3_tv_task) v3_tv_task, 
                sum(v3_net_task) v3_net_task, 
                sum(v3_sub_cut_err) v3_sub_cut_err, 
                sum(v3_net_cut_err) v3_net_cut_err, 
                sum(v3_task_back) v3_task_back, 
                sum(v3_quality) v3_quality, 
                sum(v3_ad_sure) v3_ad_sure, 
                sum(paper_page_cut) paper_page_cut, 
                sum(paper_ad_cut) paper_ad_cut, 
                sum(paper_input) paper_input, 
                sum(paper_inspect) paper_inspect, 
                sum(bc_input) bc_input, 
                sum(bc_inspect) bc_inspect, 
                sum(tv_input) tv_input, 
                sum(tv_inspect) tv_inspect, 
                sum(paper_page_up) paper_page_up, 
                sum(sub_paper_cut_err) sub_paper_cut_err, 
                sum(paper_cut_err) paper_cut_err, 
                sum(net_input) net_input, 
                sum(net_inspect) net_inspect, 
                sum(input_err) input_err, 
                sum(inspect_err) inspect_err
                ")
                ->where($countWhere)
                ->group('user_id')
                ->page($page, 100)
                ->select();
            $total = M('user_task_count')
                ->field("
                sum(v3_paper_task) v3_paper_task, 
                sum(v3_bc_task) v3_bc_task, 
                sum(v3_tv_task) v3_tv_task, 
                sum(v3_net_task) v3_net_task, 
                sum(v3_sub_cut_err) v3_sub_cut_err, 
                sum(v3_net_cut_err) v3_net_cut_err, 
                sum(v3_task_back) v3_task_back, 
                sum(v3_quality) v3_quality, 
                sum(v3_ad_sure) v3_ad_sure, 
                sum(paper_page_cut) paper_page_cut, 
                sum(paper_ad_cut) paper_ad_cut, 
                sum(paper_input) paper_input, 
                sum(paper_inspect) paper_inspect, 
                sum(bc_input) bc_input, 
                sum(bc_inspect) bc_inspect, 
                sum(tv_input) tv_input, 
                sum(tv_inspect) tv_inspect, 
                sum(paper_page_up) paper_page_up, 
                sum(sub_paper_cut_err) sub_paper_cut_err, 
                sum(paper_cut_err) paper_cut_err, 
                sum(net_input) net_input, 
                sum(net_inspect) net_inspect, 
                sum(input_err) input_err, 
                sum(inspect_err) inspect_err
                ")
                ->where($countWhere)
                ->find();
            $count = M('user_task_count')->field('count_id')->where($countWhere)->group('user_id')->buildSql();
            $count = M()->table($count . ' a')->count();
            foreach ($data as $key => $value) {
                $userInfo = M('ad_input_user')->cache(true, 60)->field('nickname, group_id, alias')->where(['wx_id' => ['EQ', $value['user_id']]])->find();
                $data[$key]['alias'] = $userInfo['alias'];
                $data[$key]['nickname'] = $userInfo['nickname'];
                $data[$key]['group_name'] = M('ad_input_user_group')->cache(true, 60)->where(['group_id' => ['EQ', $userInfo['group_id']]])->getField('group_name');
            }
            $this->ajaxReturn([
                'data' => $data,
                'count' => $count,
                'total' => $total
            ]);
        }else{
            $this->display();
        }
    }

    /**
     * 获取某人在某个时间段内的任务统计
     * @param $wx_id
     * @param $dateRange
     */
    public function getUserTaskCountData($wx_id, $dateRange)
    {
        $total = M('user_task_count')
            ->field("
                count_date,
                sum(v3_paper_task) v3_paper_task, 
                sum(v3_bc_task) v3_bc_task, 
                sum(v3_tv_task) v3_tv_task, 
                sum(v3_net_task) v3_net_task, 
                sum(v3_sub_cut_err) v3_sub_cut_err, 
                sum(v3_net_cut_err) v3_net_cut_err, 
                sum(v3_task_back) v3_task_back, 
                sum(v3_quality) v3_quality, 
                sum(v3_ad_sure) v3_ad_sure, 
                sum(paper_page_cut) paper_page_cut, 
                sum(paper_ad_cut) paper_ad_cut, 
                sum(paper_input) paper_input, 
                sum(paper_inspect) paper_inspect, 
                sum(bc_input) bc_input, 
                sum(bc_inspect) bc_inspect, 
                sum(tv_input) tv_input, 
                sum(tv_inspect) tv_inspect, 
                sum(paper_page_up) paper_page_up, 
                sum(sub_paper_cut_err) sub_paper_cut_err, 
                sum(paper_cut_err) paper_cut_err, 
                sum(net_input) net_input, 
                sum(net_inspect) net_inspect, 
                sum(input_err) input_err, 
                sum(inspect_err) inspect_err
                ")
            ->where([
                'user_id' => ['EQ', $wx_id],
                'count_date' => ['BETWEEN', $dateRange]
            ])
            ->group('count_date')
            ->select();
        $dateArr = [];
        $data = [];
        foreach ($total as $item) {
            $dateArr[] = $item['count_date'];
            foreach ($item as $key => $value) {
                $data[$key][] = $value;
            }
        }
        unset($data['count_date']);
        $this->ajaxReturn(compact('data', 'dateArr'));
    }

    /**
     * 媒体偏好设置页
     */
    public function mediaPrefer()
    {
        $userInfo = M('ad_input_user')->find(session('wx_id'));
        if ($userInfo['is_leader'] != 1){
            echo '<h1>只有组长才可以查看</h1>';
            return;
        }
        if (IS_POST){

        }else{
            $mediaLabelList = M('tlabel')
                ->field("flabelid value, CONCAT(flabel,'-', flabelclass) as label")
                ->where([
                    'fstate' => ['EQ', 1]
                ])
                ->cache(true)
                ->select();
            $mediaLabelList = json_encode($mediaLabelList);
            $memberList = M('ad_input_user')
                ->field('wx_id value, alias label, prefer_list')
                ->where("group_id = {$userInfo['group_id']} OR find_in_set({$userInfo['group_id']}, have_group_id)")
                ->select();
            $memberList = json_encode($memberList);
            $mediaClassTree = json_encode($this->getMediaClassTree());
            $regionTree = json_encode($this->getRegionTree());
            $this->assign(compact('memberList', 'mediaClassTree', 'regionTree', 'mediaLabelList'));
            $this->display();
        }
    }

    /**
     * 获取某人的媒体偏好列表
     * @param $wx_id
     */
    public function getPreferListByWxID($wx_id)
    {
        $data = M('ad_input_user')
            ->field('priority_list, prefer_list')
            ->where([
                'wx_id' => ['EQ', $wx_id]
            ])
            ->find();
        if ($data['prefer_list']){
            $dataV = M("tmedia")
                ->field('fid `value`, fmedianame `label`')
                ->where([
                    'fid' => ['IN', $data['prefer_list']]
                ])
                ->order('fmedianame')
                ->select();
        }else{
            $dataV = [];
        }
        if ($data['priority_list']){
            $priorityList = explode(',', $data['priority_list']);
        }else{
            $priorityList = [];
        }

        $this->ajaxReturn([
            'code' => 0,
            'mediaList' => $dataV,
            'priorityList' => $priorityList,
        ]);
    }

    /**
     * @param $wx_id
     * @param $list string  媒体偏好列表
     */
    public function editPreferList($wx_id, $list)
    {
        $res = M('ad_input_user')
            ->where([
                'wx_id' => ['IN', $wx_id]
            ])
            ->save([
                'prefer_list' => $list
            ]) === false ? false : true;
        if ($res){
            $msg = '修改成功';
            $code = 0;
        }else{
            $msg = '修改失败';
            $code = -1;
        }
        $this->ajaxReturn(compact('code', 'msg'));
    }

    /**
     * @param $wx_id
     * @param $list string  优先级偏好列表
     */
    public function editPriorityList($wx_id, $list)
    {
        $res = M('ad_input_user')
            ->where([
                'wx_id' => ['IN', $wx_id]
            ])
            ->save([
                'priority_list' => $list
            ]) === false ? false : true;
        if ($res){
            $msg = '修改成功';
            $code = 0;
        }else{
            $msg = '修改失败';
            $code = -1;
        }
        $this->ajaxReturn(compact('code', 'msg'));
    }

    /**
     * 获取媒体类型树
     * @return array
     */
    protected function getMediaClassTree()
    {
        $data = M('tmediaclass')->field('fid value, fpid, fclass label')->cache(true, 60)->where(['fstate' => ['EQ', 1]])->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpid']])){
                $items[$value['fpid']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }

    /**
     * 获取行政区划树
     * @return array
     */
    protected function getRegionTree()
    {
        $data = M('tregion')->field('fid value, fpid, fname label')->cache(true, 60)->where(['fstate' => ['EQ', 1], 'fid' => ['NEQ', 100000]])->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpid']])){
                $items[$value['fpid']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }

    /**
     * 商业类型树
     */
    protected function getAdClassTree()
    {
        $data = M('hz_ad_class')->cache(true, 60)->field('fcode value, fpcode, fname label')->where(['fstatus' => ['EQ', 1]])->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpcode']])){
                $items[$value['fpcode']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }

    /**
     * @param string $mediaClass
     * @return mixed
     */
    public function getCurErrorReason($mediaClass='')
    {
        if ($mediaClass){
            $res = M('cut_error_reason')->where("FIND_IN_SET($mediaClass, media_class) AND state = 1")->select();
        }else{
            $res = M('cut_error_reason')->where('state = 1')->select();

        }
        $this->ajaxReturn($res);
    }

    /**
     * 统计每条任务平均完成时间
     */
    public function timeOfTask(){
        $taskIds = M('task_input_flow')
            ->alias('tf')
            ->field('DISTINCT tf.taskid')
            ->join('task_input AS ti ON ti.taskid = tf.taskid')
            ->where([
                'tf.flow_content' => ['IN',['领取任务', '完成违法审核']],
                'ti.media_class' => ['IN',['2', '13']],
                'ug.group_name' => ['NOT LIKE','许昌%']
            ])
            ->select();
    }

	/**
	 * 上传附件参数
	 * by wyh
	 */
	public function get_file_up(){
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>file_up()));
    }
    /**
     * @Des: 调整任务分组、优先级和任务状态
     * @Edt: yuhou.wang
     * @Date: 2020-01-09 17:54:21
     * @param {String} $media_class 媒介类型 1电视 2广播 3报纸
     * @param {String} $uuid UUID
     * @param {Int} $group_id 分组ID
     * @param {Int} $priority 优先级
     * @return: {Array}
     */    
    public function changeTask(){
        $media_class = $this->P['media_class'];
        $uuid        = $this->P['uuid'];
        $group_id    = $this->P['group_id'];
        $priority    = $this->P['priority'];
        $ret = A('TaskInput/TaskInput','Model')->changeTask($media_class,$uuid,$group_id,$priority);
        $this->ajaxReturn($ret);
    }
    /**
     * @Des: 批量调整任务分组、优先级和任务状态
     * @Edt: yuhou.wang
     * @Date: 2020-01-09 17:54:21
     * @param {String|Int} $media_class 媒介类型 1电视 2广播 3报纸
     * @param {String|Array} $uuid UUID
     * @param {Int} $group_id 分组ID
     * @param {Int} $priority 优先级
     * @return: {Array}
     */    
    public function changeTaskMulti(){
        $taskData = $this->P['taskData'];
        $ret = A('TaskInput/TaskInput','Model')->changeTaskMulti($taskData);
        $this->ajaxReturn($ret);
    }
    /**
     * @Des: 任务置为方言处理
     * @Edt: yuhou.wang
     * @Date: 2020-02-20 10:46:59
     * @param {type} 
     * @return: 
     */
    public function setDialect(){
        $taskid = $this->P['taskid'] ? $this->P['taskid'] : I('taskid');
        $linkType = $this->P['linkType'] ? $this->P['linkType'] : I('linkType');
        $linkType = $linkType ? explode(',',$linkType) : [1,2];
        $ret = A('TaskInput/TaskInput','Model')->setDialect($taskid,$linkType);
        $this->ajaxReturn($ret);
    }
}
