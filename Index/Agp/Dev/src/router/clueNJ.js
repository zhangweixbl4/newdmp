const clueNJ = [{
    path: '/njcluelist',
    name: 'njcluelist',
    component: () => import('../components/clueNJ/njcluelist'),
    meta: ['督办管理', '线索登记']
  },
  {
    path: '/njcluehandle',
    name: 'njcluehandle',
    component: () => import('../components/clueNJ/njcluehandle'),
    meta: ['督办管理', '线索处理']
  },
  {
    path: '/njcluehistory',
    name: 'njcluehistory',
    component: () => import('../components/clueNJ/njcluehistory'),
    meta: ['督办管理', '线索台账']
  },
]
export default clueNJ
