<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 线索处置
 */

class NxschuliController extends BaseController
{
	/**
	 * 获取待处理线索列表
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function index() {

		$p 	= I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录

		$fname 		= I('fname');//登记表名称
	    $ffromtype 	= I('ffromtype');//来源
	    $starttime 	= I('starttime');//受理开始时间
	    $endtime 	= I('endtime');//受理结果时间
	    if(!empty($fname)){
	    	$where['fname'] = array('like','%'.$fname.'%');
	    }
	    if(!empty($ffromtype)){
	    	$where['ffromtype'] = $ffromtype;
	    }
	    if(!empty($starttime)||!empty($endtime)){
	    	$starttime 	= $starttime?date("Y-m-d",strtotime($starttime)):date("Y-m-d");
	    	$endtime 	= $endtime?date("Y-m-d",strtotime($endtime)):date("Y-m-d");
	    	$where['_string'] = 'fregistertime >="'.$starttime.'" and fregistertime<="'.$endtime.'" and ';
	    }

		$where['fstate'] 	= 20;
		$where['_string'] 	.= '((fwaitregulatorid='.session('regulatorpersonInfo.fregulatorpid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid='.session('regulatorpersonInfo.fid').'))';//用户处理权限范围
		$count = M('tregister')
			->where($where)
			->count();//查询满足条件的总记录数

		
		$data = M('tregister')
			->field('fid,fname,(case when ffromtype=10 then "监测发现" when ffromtype=20 then "网络或书面投申诉举报" when ffromtype=30 then "上级机关交办" when ffromtype=40 then "其他机关移送" end) as ffromtypes,(case when fpersontype=10 then "个人" when fpersontype=20 then "单位" when fpersontype=30 then "移送机关" when fpersontype=0 then "其他" end) as fpersontypes,fregisterregulatorname,fregisterpersonname,fregistertime,(case when fstate=13 then "处理退回" when fstate=20 then "待处理" when fstate=40 then "处理完成" end) as fstates,fsupervisestate,freminderstate')
			->where($where)
			->order('fstate asc,freminderstate desc,fsupervisestate desc,fid desc')
			->page($p,$pp)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	/**
	 *获取待处理线索详情
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function get_registerview(){

		if(I('fid')){
			$fid = I('fid');
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}
		$where['tregister.fid'] 	= I('fid');//登记表ID
		$where['tregister.fstate'] 	= 20;
		$where['_string'] 	= '((fwaitregulatorid='.session('regulatorpersonInfo.fregulatorpid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid='.session('regulatorpersonInfo.fid').'))';//用户处理权限范围

		$data = M('tregister')->field('tregister.*,(case when ffromtype=10 then "监测发现" when ffromtype=20 then "网络或书面投申诉举报" when ffromtype=30 then "上级机关交办" when ffromtype=40 then "其他机关移送" end) as ffromtypes,tregulator.fname as fwaitregulatorname,tregulatorperson.fname as fwaitpersonname')->join('tregulator on tregulator.fid=tregister.fwaitregulatorid','left')->join('tregulatorperson on tregulatorperson.fid=tregister.fwaitpersonid','left')->where($where)->find();
		if(!empty($data)){
			$data['flow'] = M('tregisterflow')->field('fflowname,fcreateregualtorpersonname,fcreatetime,fcreateregualtorid,fcreateregualtorname,freason,fcreateinfo')->where('fregisterid='.$data['fid'])->order('flowid desc')->select();//流转信息
			foreach ($data['flow'] as $key => $value) {
				$data['flow'][$key]['fcreateregualtorpname'] = D('Function')->get_tregulatornameaction($value['fcreateregualtorid']);
			}
			$data['refiles'] = M('tregisterfile')->field('fid,fattachname,fattachurl,ffilename,tregisterfile.fflowname,fcreateregualtorpersonid as createpersonid')->join('tregisterflow on tregisterfile.fillegaladflowid=tregisterflow.flowid','left')->where('is_file=1 and tregisterfile.fregisterid='.$data['fid'])->order('fid asc')->select();//获取附件列表

			$do_dt = M('document')->field('dt_id,dt_name,dt_showstate,dt_createpersonid as createpersonid')->join('tregisterflow on tregisterflow.flowid=document.dt_flowid','left')->where('dt_tregisterid='.$data['fid'])->order('dt_id desc')->select();//获取文书列表
			$aa = [];
			foreach ($do_dt as $key => $value) {
				$isf = 1;
				if (($value['dt_showstate']==10 && $value['dt_createpersonid']==session('regulatorpersonInfo.fid'))||$value['dt_flowid']==0) {//仅自己可见
					array_push($aa, $value);
				}elseif($value['dt_showstate']==0){//下级可见
					if($value['fregulatorpersonid']!=session('regulatorpersonInfo.fid')&&$value['fregulatorpersonid']!=0){
						$isf = 2;
					}
					if($value['fregulatorid']!=session('regulatorpersonInfo.fregulatorid')&&$value['fregulatorid']!=0||$value['fregulatorid']==0){
						$isf = 2;
					}
					if($isf == 1){
						array_push($aa, $value);
					}
				}
			}
			$data['document'] = $aa;
			$data['fillegaladid'] = M('tillegalad')->where('fregisterid='.$data['fid'])->order('fillegaladid desc')->getField('fillegaladid',true);//相关违法线索ID
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('data'=>$data)));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据'));
		}
	}

	/**
	 *线索处理操作
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function update_register(){

		$fchecktype = I('fchecktype');//处理方式
		$fid = I('fid');//登记信息ID
		if(empty($fid)||empty($fchecktype)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}

		$where['fid'] 		= $fid;//登记表ID
		$where['fstate'] 	= 20;
		$where['_string'] 	= '((fwaitregulatorid='.session('regulatorpersonInfo.fregulatorpid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid='.session('regulatorpersonInfo.fid').'))';//用户处理权限范围

		$data = M('tregister')->where($where)->find();//获取当前登记信息
		if(!empty($data) && ($fchecktype==10 || $fchecktype==40 || $fchecktype==50)){
			$tregisterdb 		= M('tregister');
			$tregisterflowdb 	= M('tregisterflow');

			$twdo = M('tregisterflow')->field('flowid')->where(['fregisterid'=>$fid,'_string'=>'ffinishtime is null'])->find();//获取前一流程id
			if(!empty($twdo)){
				//更新上一流程的处理人
				$twdata['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
				$twdata['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
				$twdata['ffinishtime']					= date('Y-m-d H:i:s');//处理时间
				$twdo1 = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'ffinishtime is null'])->save($twdata);
			}
			
			if(!empty($twdo1)){
				if($fchecktype==10){//线索办结
					$res['fwaitregulatortype']	= 0;//下一处理单位类型，部门
					$res['fwaitregulatorid']	= 0;//下一处理部门默认无
					$res['fwaitpersonid']		= 0;//下一处理人默认无
					$res['fstate']				= 40;//办结状态
					$res['fdealcontents']				= I('fcheckcontents');//办结说明

					//新增流程记录
					$sh_list['fregisterid'] 				= $fid;
					$sh_list['fupflowid'] 					= $twdo['flowid'];
					$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
					$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
					$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
					$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
					$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
					$sh_list['fflowname'] 					= '线索处理';
					$sh_list['freason'] 					= '线索办结';
					$sh_list['fregulatorid'] 				= session('regulatorpersonInfo.fregulatorid');
					$sh_list['fregulatorname'] 				= session('regulatorpersonInfo.regulatorname');
					$sh_list['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
					$sh_list['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
					$sh_list['fcreateinfo'] 				= I('fcheckcontents');
					$sh_list['fstate'] 						= 40;
					$sh_list['fchildstate'] 				= 10;
					$flowid = M("tregisterflow")->add($sh_list);

				}elseif($fchecktype==40){//线索交办
					$fwaitregulatorid 					= I('fwaitregulatorid');//移交机关ID
					if(empty($fwaitregulatorid)){
						$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
					}
					$res['fwaitregulatortype']			= 10;//下一处理单位类型，机构
					$res['fwaitregulatorid']			= I('fwaitregulatorid');//下一处理部门默认无
					$res['fwaitpersonid']				= 0;//下一处理人默认无
					$res['fstate'] 						= 20;//线索待处置
					
					//新增流程记录
					$sh_list['fregisterid'] 				= $fid;
					$sh_list['fupflowid'] 					= $twdo['flowid'];
					$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
					$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
					$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
					$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
					$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
					$sh_list['fflowname'] 					= '线索处理';
					$sh_list['freason'] 					= '线索交办';
					$sh_list['fregulatorid'] 				= I('fwaitregulatorid');
					$sh_list['fregulatorname'] 				= I('fwaitregulatorname');
					$sh_list['fcreateinfo'] 				= I('fcheckcontents');
					$sh_list['fstate'] 						= 20;
					$sh_list['fchildstate'] 				= 40;
					$flowid = M("tregisterflow")->add($sh_list);

				}elseif($fchecktype==50){//线索退回
					$maxflowdata = M('tregisterflow')->where(['fregisterid'=>$fid,'fistree'=>10])->order('flowid desc')->find();
					$res['fwaitregulatortype']				= 0;//下一处理单位，部门
					$res['fwaitregulatorid']				= $maxflowdata['fcreateregualtorid'];//退回发送过来的部门
					$res['fwaitpersonid']					= $maxflowdata['fcreateregualtorpersonid'];//下一处理人
					$res['fstate'] 							= 13;//处置退回

					//新增流程记录
					$sh_list['fregisterid'] 				= $fid;
					$sh_list['fupflowid'] 					= $twdo['flowid'];
					$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
					$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
					$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
					$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
					$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
					$sh_list['fflowname'] 					= '线索处理';
					$sh_list['freason'] 					= '线索退回';
					$sh_list['fregulatorid'] 				= $maxflowdata['fcreateregualtorid'];
					$sh_list['fregulatorname'] 				= $maxflowdata['fcreateregualtorname'];
					$sh_list['fregulatorpersonid'] 			= $maxflowdata['fcreateregualtorpersonid'];
					$sh_list['fregulatorpersonname'] 		= $maxflowdata['fcreateregualtorpersonname'];
					$sh_list['fcreateinfo'] 				= I('fcheckcontents');
					$sh_list['fstate'] 						= 13;
					$sh_list['fchildstate'] 				= 50;
					$flowid = M("tregisterflow")->add($sh_list);

				}
				if(!empty($flowid)){
					M()->execute('update document set dt_flowid='.$flowid.' where dt_tregisterid='.$fid.' and dt_flowid=0');
					M()->execute('update tregisterfile set fillegaladflowid='.$flowid.' where fregisterid='.$fid.' and fillegaladflowid=0');
				}
			}

			//待写入的初核信息--公共
			$reg['fmodifier']			= session('regulatorpersonInfo.fname');//修改人
			$reg['fmodifytime']			= date('Y-m-d H:i:s');//修改时间
			$trdo = M('tregister')->where(['fid'=>$fid])->save($res);
			if(!empty($trdo) && !empty($flowid)){
				D('Function')->write_log('线索处理',1,'操作成功','nj_tregisterfile',$fid,M('nj_tregisterfile')->getlastsql());
				$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
			}else{
				D('Function')->write_log('线索处理',0,'操作失败','nj_tregisterfile',$fid,M('nj_tregisterfile')->getlastsql());
				$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
			}
		}else{
			D('Function')->write_log('线索处理',0,'操作失败','nj_tregisterfile',$fid,M('nj_tregisterfile')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
		}
	}
	
}