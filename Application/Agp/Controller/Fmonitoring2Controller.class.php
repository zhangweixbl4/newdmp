<?php
namespace Agp\Controller;
use Think\Controller;
use Think\Exception;
use OpenSearch\Client\OpenSearchClient;
use OpenSearch\Client\DocumentClient;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;
import('Vendor.PHPExcel');

/**
 * 监测数据
 */

class Fmonitoring2Controller extends BaseController
{
	/**
	 * 获取监测数据
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function index()
	{
		Check_QuanXian(['home','adinquire','publishlist','illegaltadFJ','televisionFJ','broadcastFJ','newspaperFJ','tadsearchFJ','monitoringData','monitoringData2']);
		session_write_close();
		header('Access-Control-Allow-Origin:*');//允许跨域

		$ALL_CONFIG = getconfig('ALL');
		$fjxsgl = $ALL_CONFIG['fjxsgl'];
		$ischeck = $ALL_CONFIG['ischeck'];
		$system_num = $ALL_CONFIG['system_num'];
		
		$pxtype 	= I('pxtype')?I('pxtype'):2;//排序方式，0默认
		$pxval 		= I('pxval')?I('pxval'):0;//排序值，0默认
		$outtype = I('outtype');//文件导出类型

		// if(strtotime(I('fissuedatest')) < strtotime('2017-07-01')) $_GET['fissuedatest'] = date('Y-m-01');
		if(strtotime(I('fissuedatest')) < strtotime('2017-07-01')) $_GET['fissuedatest'] = date('2018-07-01');
		if(strtotime(I('fissuedateed')) < strtotime('2017-07-01')) $_GET['fissuedateed'] = date('Y-m-d');

		$client = A('Common/OpenSearch','Model')->client();//获得OpenSearchClient
		$searchClient = new SearchClient($client);
		$params = new SearchParamsBuilder();
		
		$params->setAppName(C('OPEN_SEARCH_CUSTOMER'));//设置应用名称

		$where = array();
		
		if(!empty(I('media'))) $where['fmediaid'] = I('media');//搜索媒介ID

		if(!empty(I('fadname'))) $where['fadname'] = I('fadname');//搜索广告名称

		if(!empty(I('fadclasscode'))) $where['fadclasscode'] = I('fadclasscode');//搜索广告类别

		if(I('fillegaltypecode') != '') $where['fillegaltypecode'] = I('fillegaltypecode');//搜索违法类别

		if(!empty(I('mclass'))) $where['fmediaclassid'] = intval(I('mclass'));//搜索媒介类别

		if(!empty(I('netadtype'))) $where['issue_platform_type'] = intval(I('netadtype'));//平台类型

		if(!empty(I('area'))){//如果有传入地区搜索条件
			if(empty(I('iscontain'))){
				$where['fregionid'] = I('area');
			}else{
				$where['regionid_array'] = I('area');
			}
		}else{
			$where['fregionid'] = session('regulatorpersonInfo.regionid');
		}

		$forgid = strlen($system_num) == 6 ? '20'.$system_num:$system_num;
		$where['forgid'] = $forgid;

		$where['_string'][] = ' fissuedate2:['.strtotime(I('fissuedatest')).','.strtotime(I('fissuedateed')).']';//时间搜索条件

		$setQuery = A('Common/OpenSearch','Model')->setQuery($where);//组装搜索字句
		
		$whereFilter = array();

		//是否抽查模式
		if(!empty($ischeck)){
		    $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num,'fmonth'=>['between',[date('Y-m-01',strtotime(I('fissuedatest'))),date('Y-m-01',strtotime(I('fissuedateed')))]]])->select();//查询抽查表数据
		    //如果抽查表有数据
		    if(!empty($spot_check_data)){
		        $dates = [];//定义日期数组
		        foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
		            $year_month = '';
		            $date_str = [];
		            $year_month = substr($spot_check_data_val['fmonth'],0,7);
		            if(!empty($spot_check_data_val['condition'])){
		            	$date_str = explode(',',$spot_check_data_val['condition']);
		            }
		            foreach ($date_str as $date_str_val){
		                $cctime = strtotime($year_month.'-'.$date_str_val);
		            	if(strtotime(I('fissuedatest'))<= $cctime && strtotime(I('fissuedateed'))>=$cctime){
			                $dates[] = $cctime;
		            	}
		            }
		        }
				$whereFilter['fissuedate'] = ['in',$dates];
		    }else{
		        $whereFilter['fissuedate'] = ['in',[0]];
		    }
		}
		
		$setFilter = A('Common/OpenSearch','Model')->setFilter($whereFilter);//组装过滤条件

		$params->setQuery($setQuery);//设置搜索
		$params->setFilter($setFilter);//设置文档过滤条件

		if($pxtype==1){//1时按发布媒体排序
			if($pxval==0){
				$params->addSort('fmediaid', SearchParamsBuilder::SORT_DECREASE);//降序排序
			}else{
				$params->addSort('fmediaid', SearchParamsBuilder::SORT_INCREASE);//升序排序
			}
		}else{//按发布时间排序
			if($pxval==0){
				$params->addSort('fstarttime', SearchParamsBuilder::SORT_DECREASE);//降序排序
			}else{
				$params->addSort('fstarttime', SearchParamsBuilder::SORT_INCREASE);//升序排序
			}
		}
		$params->setFormat("json");//数据返回json格式

		//读取广告类别缓存
		if(!S('adClass')){
			$cacheAdClass = M('tadclass')->cache(true,600)->field('fcode,fadclass,ffullname')->select();
			$adClass = array();
			foreach($cacheAdClass as $cacheAdClassV){
				$adClass[$cacheAdClassV['fcode']] = $cacheAdClassV;
			}
			S('adClass',$adClass,86400);
		}else{
			$adClass = S('adClass');
		}

		//获取数据的所有媒体
		$params->addDistinct(array('key' => 'fmediaid', 'distTimes' => 1, 'distCount' => 1, 'reserved' => 'false'));//设置排重字段
		$params->setStart(0);//起始位置
		$params->setHits(500);//返回数量
		$params->setFetchFields(
			array(	
				'fmediaid',//媒介id
			)
		);//设置需返回哪些字段
		$ret = $searchClient->execute($params->build());
		$result = json_decode($ret->result,true);
		$medialist = $result['result']['items'];
		$medias = [];
		foreach ($medialist as $key => $value) {
			$medias[] = $value['fmediaid'];
		}

		if(!empty($medias)){
			$bdmedias = get_owner_media_ids();//媒体权限组
			$fmedias1 = array_diff($bdmedias,array_diff($bdmedias,$medias));//本地有的媒体权限
			$fmedias2 = array_diff($medias,$bdmedias);//opensearch所有数据的媒体，本地没有的媒体
			if(count($fmedias1)<=250){
				$whereFilter['fmediaid'] = array('in',$fmedias1);
			}elseif(count($fmedias2)<=250){
				$whereFilter['fmediaid'] = array('notin',$fmedias2);
			}else{
				$whereFilter['fmediaid'] = array('=',0);
			}
			$setFilter = A('Common/OpenSearch','Model')->setFilter($whereFilter);//组装过滤条件
			$params->setFilter($setFilter);//设置文档过滤条件
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>0,'list'=>[])));
		}

		$params->setFetchFields(
			array(	
				'identify',//识别码（哈希）
				'fmediaclassid',//媒体类型
				'fmediaid',//媒介id
				'fmedianame',//媒介名称
				'fmediaownername',//媒介机构
				'fadclasscode', //广告类别id
				'fadname',//广告名称
				'fadowner',//广告主
				'fissuedate', //发布日期
				'flength',//发布时长
				'fpage',//版面
				'fstarttime', //发布开始时间戳
				'fendtime', //发布开始时间戳
				'fillegaltypecode', //违法类型
				'fexpressioncodes', //违法代码
				'fillegalcontent', //违法内容
				'fregionid', //所属地区
			)
		);//设置需返回哪些字段

		if(empty($outtype)){
			$p = I('page',1);//当前第几页
			$pp = 20;//每页显示多少记录
			$forcount = 1;
		}else{
			$p = 1;//当前第几页
			$pp = 500;//每页显示多少记录
			$forcount = -1;
		}

		$params->addDistinct();//清除排重字段

		$issueList = array();
		while($forcount!=0){
			$params->setStart(($p-1)*$pp);//起始位置
			$params->setHits($pp);//返回数量
			$ret = $searchClient->execute($params->build());//执行查询并返回信息
			$result = json_decode($ret->result,true);
			$issueList0 = $result['result']['items'];

			foreach($issueList0 as $key => $issu){
				$issu['regionname'] = M('tregion')->cache(true,600)->where(['fid'=>$issu['fregionid']])->getfield('ffullname');
				$issu['regulatorname'] = M('tregulator')->cache(true,600)->where(['fregionid'=>$issu['fregionid'],'fkind'=>1])->getfield('fname');
		
				$issu['fstarttime'] = date('H:i:s',$issu['fstarttime']);
				$issu['fendtime'] = date('H:i:s',$issu['fendtime']);
				if($issu['fmediaclassid'] == 1 || $issu['fmediaclassid'] == 2){
					$issu['fissuedate'] = date('Y-m-d',$issu['fissuedate']).' '.$issu['fstarttime'];
				}else{
					$issu['fissuedate'] = date('Y-m-d',$issu['fissuedate']);
				}
				
				$adclasscode_arr = explode(sprintf("%c", 9),$issu['fadclasscode']);
				$issu['fadclass'] = $adClass[$adclasscode_arr[0]]['ffullname'];

				if($issu['fillegaltypecode'] == '0'){
					$issu['fillegaltype'] = '不违法';
				}else{
					$issu['fillegaltype'] = '违法';
				}
				$issu['mclass'] = $issu['fmediaclassid'];
				$issu['sid'] = $issu['identify'];

				if(!empty($fjxsgl)){
					//获取文号
					if($issu['fmediaclassid'] == 1 || $issu['fmediaclassid'] == 2){
						$where_wh['a.fstarttime']	= date('Y-m-d H:i:s',$issueList0[$key]['fstarttime']);
						$where_wh['a.fendtime'] 	= date('Y-m-d H:i:s',$issueList0[$key]['fendtime']);
					}else{
						$where_wh['a.fissue_date']	= $issu['fissuedate'];
					}
					$where_wh['a.fmedia_id'] 	= $issu['fmediaid'];
					$do_wh = M('tbn_illegal_ad_issue')
						->alias('a')
						->join('tbn_illegal_ad b on a.fillegal_ad_id = b.fid')
						->join('fj_tregister c on b.fregisterid = c.fid')
						->where($where_wh)
						->getField('c.fnumber');
					$issu['wenhao'] = $do_wh?$do_wh:'';
				}else{
					$issu['wenhao'] = '';
				}

				$issueList[] = $issu;
				//销毁无用字段
				unset($issu['fmediaclassid']);
				unset($issu['identify']);
			}

			$p++;

			//循环导出数据专用，上限5000条
			if($forcount<0){
				$forcount = ceil($result['result']['total']/$pp);
			}
			if(!empty($forcount)){
				$forcount--;
			}
			if($p>500){
				break;
			}
		}

		//判断是否是数据导出
		if(!empty($outtype)){
			if(!$issueList || empty($issueList)){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'暂无数据'));
			}
			$web_name = getconfig('web_name');
			if($outtype=='xls'){
				$outdata['title'] = $web_name.'-广告明细数据';//文档内部标题名称
		        $outdata['datalie'] = [
		          '序号'=>'key',
		          '广告名称'=>'fadname',
		          '广告主'=>'fadowner',
		          '广告内容类别'=>'fadclass',
		          '发布媒体'=>'fmedianame',
		          '媒体地区'=>'regionname',
		          '媒体类型'=>[
		            'type'=>'zwif',
		            'data'=>[
		              ['{mclass} == 1','电视'],
		              ['{mclass} == 2','广播'],
		              ['{mclass} == 3','报纸'],
		              ['{mclass} == 13','互联网'],
		            ]
		          ],
		          '违法类型'=>[
		            'type'=>'zwif',
		            'data'=>[
		              ['{fillegaltypecode} == 30','违法'],
		              ['','不违法'],
		            ]
		          ],
		          '发布日期'=>'fissuedate',
		          '开始时间'=>[
		            'type'=>'zwif',
		            'data'=>[
		              ['{mclass} == 1 || {mclass} == 2','{fstarttime}'],
		              ['',''],
		            ]
		          ],
		          '结束时间'=>[
		            'type'=>'zwif',
		            'data'=>[
		              ['{mclass} == 1 || {mclass} == 2','{fendtime}'],
		              ['',''],
		            ]
		          ],
		          '时长/版面'=>[
		            'type'=>'zwif',
		            'data'=>[
		              ['{mclass} == 1 || {mclass} == 2','{flength}'],
		              ['{mclass} == 3','{fpage}'],
		              ['',''],
		            ]
		          ],
		          '违法代码'=>'fexpressioncodes',
		          '涉嫌违法内容'=>'fillegalcontent',
		        ];
			    
		        $outdata['lists'] = $issueList;
		        $ret = A('Api/Function')->outdata_xls($outdata);

		        D('Function')->write_log('监测数据',1,'excel导出成功');
		        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
			}elseif($outtype=='xml'){
				$this->downloadXml($issueList);
			}elseif($outtype=='txt'){
				$this->downloadTxt($issueList);
			}
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count2'=>$result['result']['viewtotal'],'count'=>$result['result']['total'],'list'=>$issueList)));
		}
	}

	//导出xml
	public function downloadXml($data)
	{	
		$system_num = getconfig('system_num');
		$arr = [1=>'电视',2=>'广播',3=>'报刊',13=>'互联网'];
		$xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
		$xml .= "<article>\n";
		foreach ($data as $key =>$value) {
			if($system_num != '62100000'){
				$xml .= "<item>\n";
				$xml .= "<xuhao>" . ($key+1) . "</xuhao>\n";
				$xml .= "<ggmingcheng>" . $value['fadname'] . "</ggmingcheng>\n";
				$xml .= "<ggzmingcheng>" . $value['fadowner'] . "</ggzmingcheng>\n";
				$xml .= "<ggnrleibie>" . $value['fadclass'] . "</ggnrleibie>\n";
				$xml .= "<meiti>" . $value['fmedianame'] . "</meiti>\n";
				$xml .= "<mtleixing>" . $arr[$value['mclass']] . "</mtleixing>\n";
				$xml .= "<wfleixing>" . $value['fillegaltype'] . "</wfleixing>\n";
				$xml .= "<fbshijian>" . $value['fissuedate'] . "</fbshijian>\n";
				if($value['mclass'] == 1 || $value['mclass'] == 2){
					$xml .= "<ksshijian>" . $value['fstarttime'] . "</ksshijian>\n";
					$xml .= "<jsshijian>" . $value['fendtime'] . "</jsshijian>\n";
				}else{
					$xml .= "<ksshijian> </ksshijian>\n";
					$xml .= "<jsshijian> </jsshijian>\n";
				}
				if($value['mclass'] == 1 || $value['mclass'] == 2){
					$xml .= "<scbanmian>" . $value['flength'] . "</scbanmian>\n";
				}elseif($value['mclass'] == 3){
					$xml .= "<scbanmian>" . $value['fpage'] . "</scbanmian>\n";
				}else{
					$xml .= "<scbanmian> </scbanmian>\n";
				}
				$xml .= "<wfdaima>" . $value['fexpressioncodes'] . "</wfdaima>\n";
				$xml .= "<wfyuanyin>" . $value['fillegalcontent'] . "</wfyuanyin>\n";
				$xml .= "</item>\n";
			}else{
				$xml .= "<item>\n";
				$xml .= "<xuhao>" . ($key+1) . "</xuhao>\n";
				$xml .= "<meiti>" . $value['fmedianame'] . "</meiti>\n";
				$xml .= "<mtleixing>" . $arr[$value['mclass']] . "</mtleixing>\n";
				$xml .= "<ggjingying> </ggjingying>\n";
				$xml .= "<ggzmingcheng>" . $value['fadowner'] . "</ggzmingcheng>\n";
				$xml .= "<ggdiqu>" . $value['regionname'] . "</ggdiqu>\n";
				$xml .= "<fbshijian>" . $value['fissuedate'] . "</fbshijian>\n";
				if($value['mclass'] == 1 || $value['mclass'] == 2){
					$xml .= "<ksshijian>" . $value['fstarttime'] . "</ksshijian>\n";
					$xml .= "<jsshijian>" . $value['fendtime'] . "</jsshijian>\n";
				}else{
					$xml .= "<ksshijian> </ksshijian>\n";
					$xml .= "<jsshijian> </jsshijian>\n";
				}
				if($value['mclass'] == 1 || $value['mclass'] == 2){
					$xml .= "<scbanmian>" . $value['flength'] . "</scbanmian>\n";
				}elseif($value['mclass'] == 3){
					$xml .= "<scbanmian>" . $value['fpage'] . "</scbanmian>\n";
				}else{
					$xml .= "<scbanmian> </scbanmian>\n";
				}
				$xml .= "<ggmingcheng>" . $value['fadname'] . "</ggmingcheng>\n";
				$xml .= "<ggnrleibie>" . $value['fadclass'] . "</ggnrleibie>\n";
				$xml .= "<ggchanxiao> </ggchanxiao>\n";
				$xml .= "<ggfugai> </ggfugai>\n";
				$xml .= "<gxxingzheng>".$value['regulatorname']."</gxxingzheng>\n";
				$xml .= "<wfleixing>" . $value['fillegaltype'] . "</wfleixing>\n";
				$xml .= "<wfdaima>" . $value['fexpressioncodes'] . "</wfdaima>\n";
				$xml .= "<wfyuanyin>" . $value['fillegalcontent'] . "</wfyuanyin>\n";
				$xml .= "</item>\n";
			}
		}
		$xml .= "</article>\n";
		
		$date = date('Ymdhis');
		$savefile = './Public/Xls/'.$date.'.xml';
		file_put_contents($savefile,$xml);//生成本地文件
		D('Function')->write_log('监测数据',1,'xml导出成功');

		$ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.xml',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
		unlink($savefile);//删除文件

		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
	}

	//导出txt
	public function downloadTxt($data)
	{
		$system_num = getconfig('system_num');
		if($system_num != '62100000'){
			$txttitle = "序号  广告名称  广告主  广告内容类别  发布媒体  媒体类型  违法类型  发布时间  结束时间  时长/版面  违法代码  违法原因\n";
		}else{
			$txttitle = "序号  发布媒体  媒体类型  广告经营者  广告主  发布地  播出日期  播出开始时间  播出结束时间  广告时长/版面  宣传产品名称  广告内容类别  产、销公司  广告覆盖地域  管辖行政机关  违法类型  违法代码  涉嫌违法内容\n";
		}
		$txtcontent = '';
		$arr = [1=>'电视',2=>'广播',3=>'报刊',13=>'互联网'];
		foreach ($data as $key => $value) {	
			if($system_num != '62100000'){
				$txtcontent .=($key+1).'.  ';
				$txtcontent .=$value['fadname'].'  ';
				$txtcontent .=$value['fadowner'].'  ';
				$txtcontent .=$value['fadclass'].'  ';
				$txtcontent .=$value['fmedianame'].'  ';
				$txtcontent .=$arr[$value['mclass']].'  ';
				$txtcontent .=$value['fillegaltype'].'  ';
				$txtcontent .=$value['fissuedate'].'  ';
				if($value['mclass'] == 1 || $value['mclass'] == 2){
					$txtcontent .=$value['fstarttime'].'  ';
					$txtcontent .=$value['fendtime'].'  ';
				}else{
					$txtcontent .='  ';
					$txtcontent .='  ';
				}
				if($value['mclass'] == 1 || $value['mclass'] == 2){
					$txtcontent .=$value['flength'].'  ';
				}elseif($value['mclass'] == 3){
					$txtcontent .=$value['fpage'].'  ';
				}else{
					$txtcontent .='  ';
				}
				$txtcontent .=$value['fexpressioncodes'].'  ';
				$txtcontent .=$value['fillegalcontent'].'  ';
				$txtcontent .="\n";
			}else{
				$txtcontent .=($key+1).'.  ';
				$txtcontent .=$value['fmedianame'].'  ';
				$txtcontent .=$arr[$value['mclass']].'  ';
				$txtcontent .='  ';
				$txtcontent .=$value['fadowner'].'  ';
				$txtcontent .=$value['regionname'].'  ';
				$txtcontent .=$value['fissuedate'].'  ';
				if($value['mclass'] == 1 || $value['mclass'] == 2){
					$txtcontent .=$value['fstarttime'].'  ';
					$txtcontent .=$value['fendtime'].'  ';
				}else{
					$txtcontent .='  ';
					$txtcontent .='  ';
				}
				if($value['mclass'] == 1 || $value['mclass'] == 2){
					$txtcontent .=$value['flength'].'  ';
				}elseif($value['mclass'] == 3){
					$txtcontent .=$value['fpage'].'  ';
				}else{
					$txtcontent .='  ';
				}
				$txtcontent .=$value['fadname'].'  ';
				$txtcontent .=$value['fadclass'].'  ';
				$txtcontent .='  ';
				$txtcontent .='  ';
				$txtcontent .=$value['regulatorname'].'  ';
				$txtcontent .=$value['fillegaltype'].'  ';
				$txtcontent .=$value['fexpressioncodes'].'  ';
				$txtcontent .=$value['fillegalcontent'].'  ';
				$txtcontent .="\n";
			}
		}

		$date = date('Ymdhis');
		$savefile = './Public/Xls/'.$date.'.txt';
		file_put_contents($savefile,$txttitle.$txtcontent);
		D('Function')->write_log('监测数据',1,'txt导出成功');

		$ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.txt',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
		unlink($savefile);//删除文件
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
	}

	/**
	 * 获取监测数据详情
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据
	 * by zw
	 */
	public function getmonitoringview()
	{
		$identify = I('sid');
		$issueTally = getconfig('issueTally');//广告明细中，当前查看的广告属于第几次播出的显示，0不显示，1显示
		
		header('Access-Control-Allow-Origin:*');//允许跨域
		$client = A('Common/OpenSearch','Model')->client();//获得OpenSearchClient
		$searchClient = new SearchClient($client);
		$params = new SearchParamsBuilder();
		$params->setHits(1);//返回数量
		$params->setAppName(C('OPEN_SEARCH_CUSTOMER'));//设置应用名称

		$setQuery = A('Common/OpenSearch','Model')->setQuery(array('id'=>$identify));//组装搜索字句
		$params->setQuery($setQuery);//设置搜索

		$params->setFormat("json");//数据返回json格式

		$params->setFetchFields(
			array(
				'identify',//识别码
				'fadname',//广告名称
				'fbrand',//品牌
				'flength',//时长
				'fpage',//版面
				'fadclasscode',//广告类别
				'fmediaid',//发布媒体ID
				'fmedianame',//发布媒体
				'fadowner',//广告主
				'fmediaclassid',//媒体类别
				'fmediaownername',//媒体机构名称
				'fissuedate',//发布日期
				'fstarttime',//开始时间
				'fendtime',//结束时间
				'fsampleid',//样本ID
				'fspokesman',//代言人
				'fillegaltypecode',//违法类型
				'fillegalcontent',//违法内容
				'fexpressioncodes',//违法表现代码
				'sam_source_path',//素材路径
				'other_json',
			)
		);//设置需返回哪些字段

		$ret = $searchClient->execute($params->build());//执行查询并返回信息
		$result = json_decode($ret->result,true);//
		$issueInfo = $result['result']['items'][0];

		//<<<<<查询第几次播出
		$issueInfo['issuecount'] = 0;
		if(!empty($issueTally)){
			$whereFilter = array();
			if($issueInfo['fmediaclassid'] == 1 || $issueInfo['fmediaclassid'] == 2){
				$fissuedatest = I('fissuedatest')?I('fissuedatest'):date('Y-m-01',$issueInfo['fstarttime']);
				$whereFilter['fstarttime'] = array('between',array(strtotime($fissuedatest),$issueInfo['fstarttime']));
			}else{
				$fissuedatest = I('fissuedatest')?I('fissuedatest'):date('Y-m-01',$issueInfo['fissuedate']);
				$whereFilter['fissuedate'] = array('between',array(strtotime($fissuedatest),$issueInfo['fissuedate']));
			}
			$setFilter = A('Common/OpenSearch','Model')->setFilter($whereFilter);//组装过滤条件

			$setQuery = A('Common/OpenSearch','Model')->setQuery(array('fsampleid'=>$issueInfo['fsampleid']));//组装搜索字句

			$params->setQuery($setQuery);//设置搜索
			$params->setFilter($setFilter);//设置文档过滤条件
			
			$params->setFetchFields(
				array(	
					'identify',//识别码
				)
			);//设置需返回哪些字段

			$ret = $searchClient->execute($params->build());
			$result = json_decode($ret->result,true);
			$issueInfo['issuecount'] = $result['result']['total'];
		}
		//查询第几次播出>>>>>
		
		if($issueInfo['fillegaltypecode'] == '0'){
			$issueInfo['fillegaltype'] = '不违法';
		}else{
			$issueInfo['fillegaltype'] = '违法';
		}

		//获取违法表现
		if(!empty($issueInfo['fexpressioncodes'])){
			$fexpressioncodes = implode('","',explode(';', $issueInfo['fexpressioncodes']));
			$do_tl = M('tillegal')
				->field('group_concat(fexpression separator ";") as fexpressions,group_concat(fconfirmation separator ";") as fconfirmations ')
				->where('fcode in ("'.$fexpressioncodes.'")')
				->find();
			if(!empty($do_tl)){
				$issueInfo = array_merge($issueInfo,$do_tl);
			}
		}

		//获取广告类别全名
		if(!S('adClass')){
			$cacheAdClass = M('tadclass')->cache(true,600)->field('fcode,fadclass,ffullname')->select();
			$adClass = array();
			foreach($cacheAdClass as $cacheAdClassV){
				$adClass[$cacheAdClassV['fcode']] = $cacheAdClassV;
			}
			S('adClass',$adClass,86400);
		}else{
			$adClass = S('adClass');
		}
		$adclasscode_arr = explode(sprintf("%c", 9),$issueInfo['fadclasscode']);
		$issueInfo['fadclass'] = $adClass[$adclasscode_arr[0]]['ffullname'];

		if($issueInfo['fmediaclassid']== 1){
			$data = M('ttvsample')
				->field('fapprovalid, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, "" as fpage, "" as fsize, favifilepng,favifilename
				')
				->where('fid = '.$issueInfo['fsampleid'])
				->find();
			$issueInfo['issueurl'] = A('Common/Media','Model')->get_m3u8($issueInfo['fmediaid'],$issueInfo['fstarttime'],$issueInfo['fendtime']);

			$where2['source_type'] = 10;
			$where2['source_tid'] = $issueInfo['identify'];
			$where2['source_mediaclass'] = '01';
			$where2['validity_time'] = ['gt',time()];
			$data2 = M('source_make')->field('source_url,source_id,source_state')->where($where2)->find();
			if(!empty($data2)){
				$issueInfo['source_url'] = $data2['source_url'];
				$issueInfo['source_id'] 	= $data2['source_id'];
				$issueInfo['source_state'] = $data2['source_state'];
			}
		}elseif($issueInfo['fmediaclassid']== 2){
			$data = M('tbcsample')
				->field('fapprovalid, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, "" as fpage, "" as fsize, favifilepng,favifilename ')
				->where('fid = '.$issueInfo['fsampleid'])
				->find();

			$issueInfo['issueurl'] = A('Common/Media','Model')->get_m3u8($issueInfo['fmediaid'],$issueInfo['fstarttime'],$issueInfo['fendtime']);

			$where2['source_type'] = 10;
			$where2['source_tid'] = $issueInfo['identify'];
			$where2['source_mediaclass'] = '02';
			$where2['validity_time'] = ['gt',time()];
			$data2 = M('source_make')->field('source_url,source_id,source_state')->where($where2)->find();
			if(!empty($data2)){
				$issueInfo['source_url'] = $data2['source_url'];
				$issueInfo['source_id'] 	= $data2['source_id'];
				$issueInfo['source_state'] = $data2['source_state'];
			}
		}elseif($issueInfo['fmediaclassid']== 3){
			$data = M('tpapersample')
				->field('fapprovalid, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, fjpgfilename as favifilepng,fjpgfilename as favifilename
				')
				->where('fpapersampleid = '.$issueInfo['fsampleid'])
				->find();
		}elseif($issueInfo['fmediaclassid']== 13){
			$data = M('tnetissue')
				->field('fapprovalid, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone,thumb_url_true,net_snapshot,net_original_url,net_target_url,article_content,net_advertiser,ftype,net_original_snapshot,net_platform,tmedia.fmediacode')
				->join('tmedia on tnetissue.fmediaid = tmedia.fid')
				->where('major_key = '.$issueInfo['fsampleid'])
				->find();
		}elseif($issueInfo['fmediaclassid']== 5){
			$issueInfo['favifilename'] = $issueInfo['sam_source_path'];

			//点位数据
			if(!empty($issueInfo['other_json'])){
				$other_json = json_decode($issueInfo['other_json'],true);
				$issueInfo['huwai_point'] = $other_json['huwai_point'];
			}else{
				$issueInfo['huwai_point'] = [];
			}
		}
		if(!empty($data)){
			$issueInfo = array_merge($issueInfo,$data);
		}

		if($issueInfo['fmediaclassid'] == 1){
			$issueInfo['mclass'] = 'tv';
		}elseif($issueInfo['fmediaclassid'] == 2){
			$issueInfo['mclass'] = 'bc';
		}elseif($issueInfo['fmediaclassid'] == 3){
			$issueInfo['mclass'] = 'paper';
		}elseif($issueInfo['fmediaclassid'] == 13){
			$issueInfo['mclass'] = 'net';
		}elseif($issueInfo['fmediaclassid'] == 5){
			$issueInfo['mclass'] = 'od';
		}
		$issueInfo['fillegalcontent'] = htmlspecialchars_decode($issueInfo['fillegalcontent']);
		$issueInfo['fstarttime'] = date('H:i:s',$issueInfo['fstarttime']);
		$issueInfo['fendtime'] = date('H:i:s',$issueInfo['fendtime']);
		$issueInfo['fissuedate'] = date('Y-m-d',$issueInfo['fissuedate']);
		$issueInfo['fadlen'] = $issueInfo['flength'];
		$issueInfo['fadowner_name'] = $issueInfo['fadowner'];

		//销毁无用字段
		unset($issueInfo['fmediaclassid']);
		unset($issueInfo['index_name']);
		unset($issueInfo['flength']);
		if(!empty($issueInfo)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$issueInfo));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据'));
		}
	}

}