<?php
namespace Common\Model;



class AdcluesectionModel{


    /*创建环节*/
    public function create_adcluesection($fadclueid,$fsendunit,$freceiveunit,$fsender,$ftype = 1){
		
		$fsendunit = intval($fsendunit);//源发送单位
		$freceiveunit = intval($freceiveunit);//接收单位
		if($freceiveunit == 0) {
			$freceiveunit = 20100000;////接收单位 工商总局的code
		}

		$freceiveunitname = M('tregulator')->where(array('fcode'=>$freceiveunit))->getField('fname');//接收单位名称
		if($fsendunit > 0){
			$fsendunitname = M('tregulator')->where(array('fcode'=>$fsendunit))->getField('fname');//发送单位名称
		}
		$s_a_data = array();
		$s_a_data['fadclueid'] = $fadclueid;//线索ID
		$s_a_data['fsendunit'] = $fsendunit;//发送单位
		$s_a_data['fsender'] = $fsender;//发送人
		$s_a_data['ftype'] = $ftype;//处置类型
		
		$s_a_data['fsendtime'] = date('Y-m-d H:i:s');//发送时间
		$s_a_data['freceiveunit'] = $freceiveunit;//接收单位	
		$s_a_data['freceiveunitname'] = $freceiveunitname;//接收单位名称
		$s_a_data['fsendunitname'] = $fsendunitname;//发送单位单位名称
		
		$s_a_data['fstate'] = 0;//状态（0未处理，1已接收，2已处理）
		$rr = M('tadcluesection')->add($s_a_data);
		return $rr;

    }



}