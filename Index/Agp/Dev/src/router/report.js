const report = [{
    path: '/monitorReport',
    name: 'monitorReport',
    component: () => import('../components/report/monitorReport'),
    meta: ['数据报告', '监测报告']
  },
  {
    path: '/monitorReportNJ',
    name: 'monitorReportNJ',
    component: () => import('../components/report/monitorReportNJ'),
    meta: ['数据报告', '监测报告']
  },
  {
    path: '/commonReport',
    name: 'commonReport',
    component: () => import('../components/report/commonReport'),
    meta: ['数据报告', '监测报告']
  },
  {
    path: '/monitorReportAH',
    name: 'monitorReportAH',
    component: () => import('../components/report/monitorReportAH'),
    meta: ['数据报告', '监测报告']
  },
  {
    path: '/monitorReportZB',
    name: 'monitorReportZB',
    component: () => import('../components/report/monitorReportZB'),
    meta: ['数据报告', '监测报告']
  },
  {
    path: '/monitorReportZB2',
    name: 'monitorReportZB2',
    component: () => import('../components/report/monitorReportZB2'),
    meta: ['数据报告', '监测报告']
  },
  {
    path: '/monitorReportTJ',
    name: 'monitorReportTJ',
    component: () => import('../components/report/monitorReportTJ'),
    meta: ['数据报告', '监测报告']
  },
  {
    path: '/monitorReportSJZ',
    name: 'monitorReportSJZ',
    component: () => import('../components/report/monitorReportSJZ'),
    meta: ['数据报告', '监测报告']
  },
  {
    path: '/monitorReportHZ',
    name: 'monitorReportHZ',
    component: () => import('../components/report/monitorReportHZ'),
    meta: ['数据报告', '监测报告']
  },
  {
    path: '/recheckReport',
    name: 'recheckReport',
    component: () => import('../components/report/recheckReport'),
    meta: ['数据报告', '复核报告']
  },
  {
    path: '/verificationReport',
    name: 'verificationReport',
    component: () => import('../components/report/verificationReport'),
    meta: ['数据报告', '报告任务']
  },
  {
    path: '/chazhengreport',
    name: 'chazhengreport',
    component: () => import('../components/report/verificationReport'),
    meta: ['数据报告', '查证报告']
  },
  {
    path: '/trackReport',
    name: 'trackReport',
    component: () => import('../components/report/trackReport'),
    meta: ['数据报告', '跟踪报告']
  },
  {
    path: '/evaluationReport',
    name: 'evaluationReport',
    component: () => import('../components/report/evaluationReport'),
    meta: ['数据报告', '评估报告']
  },
  {
    path: '/monitorreportgjj',
    name: 'monitorreportgjj',
    component: () => import('../components/report/monitorReportGjj'),
    meta: ['数据报告', '监测报告']
  },
  {
    path: '/zxreport',
    name: 'zxreport',
    component: () => import('../components/report/zxreport'),
    meta: ['数据报告', '监测报告']
  }
]
export default report
