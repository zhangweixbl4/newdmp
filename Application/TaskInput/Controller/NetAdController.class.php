<?php
namespace TaskInput\Controller;
use Common\Model;
use TaskInput\Model\AdInputUserModel;
use TaskInput\Model\NetAdModel;
use TaskInput\Model\TaskInputModel;
use TaskInput\Model\TaskInputScoreModel;
use Think\Exception;
use Think\Page;
use Open\Model\EvidenceModel;

/**
 * 互联网广告任务控制器
 */
class NetAdController extends BaseController {
    const CUT_ERROR_COUNT_FIELD = 'v3_net_cut_err';
    const TASK_BACK_COUNT_FIELD = 'v3_task_back';
    const QUALITY_COUNT_FIELD = 'v3_quality';
    const USER_AUTH_CODE = '1006';
    const QUALITY_INSPECTOR_AUTH_CODE = '1007';
    const PREVIEW_AUTH_CODE = '1008';     // 预览权限
    const ILLEGAL_REVIEW_COMMENT_AUTH_CODE = '3001';
    const ILLEGAL_REVIEW_COMMENT_CONFIRM_AUTH_CODE = '3002';
    /**
     * 用户Key
     */
    const USERID = '22638a3131d0f0a7346b178fd29f939c';

    /**
     * 任务统计字段名对应关系
     */
    protected $TASK_COUNT_FIELD_MAP = [
        '1' => 'v3_tv_task',
        '2' => 'v3_bc_task',
        '3' => 'v3_paper_task',
        '5' => 'v3_out_task',
        '13' => 'v3_net_task',
    ];

    /**
     * 接收参数
     */
    protected $P;

    /**
     * 任务超时限制
     */
    protected $overtime;

    /**
     * 错误信息
     */
    protected $error = '';

    /**
     * 自动提交不违法判定
     * 名称包含：推广，商城，APP，手机游戏，京东秒杀，淘宝精选，京东闪购，天猫网
     * 域名包含：tmall.com,taobao.com
     * 广告类别：(1101)知识产品类>软件，(1601)互联网服务类>电子商务
     */
    protected  $notIllegalMap = [
        'fadname'        => ['推广','商城','APP','手机游戏','京东秒杀','淘宝精选','京东闪购','天猫网'],
        'net_target_url' => ['tmall.com','taobao.com'],
        'fadclasscode'   => [1101,1601]
    ];

    /**
     * 初始化
     */
    public function _initialize(){
        parent::_initialize();
        $this->P = json_decode(file_get_contents('php://input'),true);
        $this->overtime = sys_c('task_overtime');
    }

    /**
     * 开始任务
     */
    public function index()
    {
        // 暂停任务
        // $this->ajaxReturn(['code' => -1,'msg' => '暂停互联网任务，请优先处理传统任务']);

        if(!A('InputPc/Task','Model')->user_other_authority(self::USER_AUTH_CODE) && !A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)){
            echo '<h1>无权限,请联系管理员</h1>';
            return false;
        }
        if (IS_POST){
            $wx_id = session('wx_id') ? session('wx_id') : 0;
            // 如果参数存在taskid, 则是重做任务, 否则是获取新任务
            if (I('taskid')){
                $taskid = I('taskid');
            }else{
                $taskid = (new NetAdModel())->getTask();
                if (!$taskid){
                    // $taskid = A('Common/InputTask','Model')->get_netad_task();
                }
            }
            // 处理错误情况
            if (!$taskid){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '未找到任务'
                ]);
            }
            // 任务基本信息
            $taskData = M('tnettask')->master(true)->find($taskid);
            $taskData['sam_uuid'] = $taskData['identify_code'];
            $taskData['uuid'] = $taskData['identify_code'];
            $mediaInfo = M('tmedia')->field('fmedianame, fjpgfilename')->find($taskData['media_id']);
            // 获取用户环节权限，如:['1','2']
            $linkTypes = (new AdInputUserModel())->getTaskLinkAuth($wx_id);
            // 任务总环节定义 2019-02-25后取消互联网任务环节表，环节通过tnettask表的task_state状态码进行体现
            $taskState = str_split($taskData['task_state']);
            if(count($taskState) < 2){
                array_unshift($taskState,1);//历史任务状态为一位数时，插入环节，拼装环节两位数
            }
            $taskData['linkTypeArr'] = [];
            $taskLink = [];

            foreach($linkTypes as $key => $val){
                $linkState = 0;
                if($val < $taskState[0]){
                    $linkState = 2;
                }elseif($val > $taskState[0]){
                    $linkState = 1;
                }elseif($val = $taskState[0]){
                    if(isset($taskState[1])){
                        $linkState = $taskState[1];
                    }else{
                        $linkState = 2;//任务最终完成状态
                    }
                }
                $taskLink = [
                    'type' => (string)$val,
                    'state' => (string)$linkState
                ];
                array_push($taskData['linkTypeArr'],$taskLink);
            }
            $taskData['media_name'] = $mediaInfo['fmedianame'];
            $data = [];
            // 如果major_key不是0, 则找到广告相关数据
            if ($taskData['major_key'] != 0){
                $data['ad'] = M('tnetissue')->find($taskData['major_key']);
                $taskData['adCateText1'] = M('tadclass')->where(['fcode' => ['EQ', $taskData['fadclasscode']]])->getField('ffullname');
                $taskData['adCateText2'] = M('hz_ad_class')->where(['fcode' => ['EQ', $taskData['fadclasscode_v2']]])->getField('ffullname');
                $taskData['adCateText2'] = $taskData['adCateText2'] ? $taskData['adCateText2'] : '';
            }
            foreach ($data as $key => $value) {
                if ($value){
                    $taskData = array_merge($taskData, $value);
                }
            }
            // 广告主名称
            $fadOwnerName = M('tadowner')->field('fname')->find($taskData['fadowner']);
            $taskData['fname'] = $fadOwnerName ? $fadOwnerName['fname'] : '广告主不详';
            // 媒介名称
            $mediaInfo = M('tmedia')->field('fmedianame')->find($taskData['media_id']);
            $taskData['media_name'] = $taskData['media_name'] ? $taskData['media_name'] : $mediaInfo['fmedianame'];
            $taskData['fillegaltypecode'] = $taskData['fillegaltypecode'] ? $taskData['fillegaltypecode'] : '0';
            // 如果有违法表现, 查出来
            if ($taskData['fexpressioncodes']){
                $taskData['checkedIllegalArr'] = M('tillegal')->field('fcode, fconfirmation, fexpression, fillegaltype, fpcode, fcode id, fexpression label')->where(['fcode' => ['IN', explode(';',$taskData['fexpressioncodes'])]])->select();
            }
            $taskData['fileUp'] = file_up();
            if ($taskData['fillegalcontent']){
                $taskData['fillegalcontent'] = htmlspecialchars_decode($taskData['fillegalcontent']);
            }
            $data = [
                'code' => 0,
                'data' => $taskData,
            ];
            $this->ajaxReturn($data);
        }else{
            $type           = 'input';
            $illegalTypeArr = json_encode($this->getIllegalTypeArr());
            $adClassArr     = json_encode($this->getAdClassArr());
            $adClassTree    = json_encode($this->getAdClassTree());
            $illegalArr     = json_encode($this->getIllegalArr());
            $this->assign(compact('type', 'illegalTypeArr', 'adClassArr', 'adClassTree', 'illegalArr'));
            $this->display();
        }
    }

    /**
     * 提交任务
     */
    public function submitTask()
    {
        if(!A('InputPc/Task','Model')->user_other_authority(self::USER_AUTH_CODE) && !A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        $nettaskFields = [
            'taskid',
            'major_key',
            'media_class',
            'media_id',
            'media_name',
            'net_id',
            'issue_date',
            'source_path',
            'ftype',
            'fversion',
            'fspokesman',
            'fadclasscode',
            'fadclasscode_v2',
            'fname',
            'fadname',
            'fbrand',
            'fillegaltypecode',
            'fillegalcontent',
            'fexpressioncodes',
            'fadmanuno',
            'fmanuno',
            'fadapprno',
            'fapprno',
            'fadent',
            'fent',
            'fentzone',
            'priority',
            'back_reason',
            'sam_uuid',
            'cut_err_state',
            'cut_err_reason',
            'quality_wx_id',
            'quality_state',
            'syn_state',
            'task_state'
        ];
        $formData = I('post.data');

        $fadname         = $formData['fadname'];
        $fbrand          = $formData['fbrand'];
        $adclass_code    = $formData['fadclasscode'];
        $adclass_code_v2 = $formData['fadclasscode_v2'];
        $adowner_name    = $formData['fname'];
        $mediaName       = $formData['media_name'];
        // 如果不违法,就直接过滤掉违法相关的2个字段
        if ($formData['fillegaltypecode'] == '0'){
            $formData['fexpressioncodes'] = '';
            $formData['fillegalcontent'] = '';
        }else{
            // 互联网违法广告自动添加
            A('Agp/Fnetad','Model')->ad_addaction($formData['major_key']);
        }
        // 取出违法表现内容及判定依据
        $arrExpressioncode = explode(';',$formData['fexpressioncodes']);
        foreach($arrExpressioncode as $key=>$fcode){
            $fexpressionInfo = M('tillegal')->where(['fcode'=>$fcode])->find();
            $fexpressions .= $fexpressionInfo['fexpression'].';';
            $fconfirmations .= $fexpressionInfo['fconfirmation'].';';
        }

        // 提交条件的限制 1. 本人提交本人的任务,或者是质检员修改任务; 2. 任务状态只能是做任务中或被退回
        $wxId = session('wx_id');
        $taskInfo = M('tnettask')->find($formData['taskid']);
        $taskState = $taskInfo['task_state'];
        $currentId = $taskInfo['wx_id'];
        // 整理tnettask的修改数据
        $taskInputData = [];
        $requiredField = [
            'fadclasscode',
            'fadclasscode_v2',
            'fname',
            'fadname',
            'fbrand',
            'fillegaltypecode',
        ];
        foreach ($nettaskFields as $key => $value) {
            if (isset($formData[$value])){
                $taskInputData[$value] = trim($formData[$value]);
            }elseif (in_array($value, $requiredField)){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '有必填字段没有填写, 请重新填写'
                ]);
            }
        }
        $currentState = substr($taskState,0,1);
        $nextTaskState = $currentState.'2';
        $nextTaskState = (int)$nextTaskState;
        $wxInfo = M('ad_input_user')->find($wxId);
        $taskInputData['fadowner']       = A('Common/Ad','Model')->getAdOwnerId($adowner_name,$wxInfo['alias']);
        $taskInputData['modifier']       = $wxId;
        $taskInputData['modifytime']     = time();
        $taskInputData['task_state']     = $nextTaskState;
        $taskInputData['syn_state']      = 1;
        $taskInputData['fexpressions']   = $fexpressions;
        $taskInputData['fconfirmations'] = $fconfirmations;
        $role = 'user'; // 用来标识是质检员修改任务还是录入员做任务
        // 录入员做任务
        if ($wxId == $currentId && substr($taskState,-1,1) != 2){
            $allowTaskState = [11,21,13,23,1,4];//进行中或被退回，兼容原状态码
            $nextTaskState = $currentState.'2';
            $nextTaskState = (int)$nextTaskState;
            $flowData = [
                'taskid' => $taskInputData['taskid'],
                'tasklink' => $currentState,
                'wx_id' => $wxId,
                'logtype' => $nextTaskState,
                'log' => A('TaskInput/NetAd','Model')->getLogType($nextTaskState),
                'creator' => $wxId,
                'createtime' => time()
            ];
        }elseif(A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE) && substr($taskInfo['quality_state'],-1,1) != 2){  // 质检员修改任务
            $allowTaskState = [12,22,2];//已完成状态，兼容原状态码
            $nextTaskState = $currentState.'7';
            $nextTaskState = (int)$nextTaskState;
            $flowData = [
                'taskid' => $taskInputData['taskid'],
                'tasklink' => $currentState,
                'wx_id' => $currentId,
                'logtype' => $nextTaskState,
                'log' => A('TaskInput/NetAd','Model')->getLogType($nextTaskState),
                'creator' => $wxId,
                'createtime' => time()
            ];
            $taskInputData['quality_state'] = 2;
            $taskInputData['quality_wx_id'] = $wxId;
            $role = 'inspector';
        }else{
            $this->ajaxReturn([
                'code' => -2,
                'msg' => '当前任务不是你的,开始下一个任务'
            ]);
        }
        if (!in_array($taskState, $allowTaskState)){
            $this->ajaxReturn([
                'code' => -3,
                'msg' => '当前任务状态非法',
            ]);
        }
        // 更新媒介表
        $mediaInfo = [
            'fmedianame' => $mediaName,
            'fmodifier' => '众包 '.$wxId.$wxInfo['alias'],
            'fmodifytime' => date('Y-m-d H:i:s',time())
        ];
        // 写入样本表数据
        $issueData = $taskInputData;
        $issueData['finputstate'] = 2;//更新为已录入状态 录入状态，0-无需录入，1-未录入，2-已录入
        $issueData['finputuser'] = $wxId;
        $issueData['fmodifier'] = $wxId;
        $issueData['fmodifytime'] = date('Y-m-d H:i:s',time());

        M()->startTrans();
        $tmediaUpdate = M('tmedia')->where(['fid'=>$taskInputData['media_id']])->save($mediaInfo);
        $taskInputUpdate = M('tnettask')->where(['taskid' => ['EQ', $taskInputData['taskid']]])->save($taskInputData) === false ? false : true;
        $netissueUpdate = M('tnetissue')->where(['major_key' => ['EQ', $taskInfo['major_key']]])->save($issueData) === false ? false : true;
        $taskInputFlowUpdate = M('tnettask_flowlog')->add($flowData) === false ? false : true;
        if ($taskInputUpdate && $netissueUpdate && $taskInputFlowUpdate){
            if ($role == 'user'){
                A('InputPc/Task','Model')->change_task_count($wxId, $this->TASK_COUNT_FIELD_MAP[$taskInfo['media_class']], 2);
            }elseif($role == 'inspector'){
                A('InputPc/Task','Model')->change_task_count($currentId, self::TASK_BACK_COUNT_FIELD, 2);
                A('Adinput/Weixin','Model')->task_back($currentId,'质检员修改任务',$fadname);
                A('InputPc/Task','Model')->change_task_count($wxId, self::QUALITY_COUNT_FIELD, 2);
            }
            $scoreModel = new TaskInputScoreModel();
            $scoreModel->addScoreBySubmitNetTask($taskInfo['taskid'], 1);
            $scoreModel->addScoreBySubmitNetTask($taskInfo['taskid'], 2);
            M()->commit();
            $code = 0;
            $msg = '提交成功';
        }else{
            M()->rollback();
            $code = -1;
            if (!$taskInputUpdate){
                $msg = '任务更新失败';
            }else{
                $msg = '任务记录更新失败';
            }
        }
        $this->ajaxReturn(compact('code', 'msg'));
    }
    /**
     * 提交广告信息录入任务
     * @return bool
     */
    public function submitAdInputTask()
    {
        $wx_id = session('wx_id');
        if (!A('InputPc/Task', 'Model')->user_other_authority(self::USER_AUTH_CODE) && !A('InputPc/Task', 'Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)) {
            $this->ajaxReturn(array('code' => -1, 'msg' => '无权限,请联系管理员'));
        }
        $taskInputTableField = [
            'taskid',
            'fspokesman',
            'fversion',
            'fadclasscode',
            'fadclasscode_v2',
            'fname',
            'fadname',
            'fbrand',
            'media_class',
            'major_key',
            'fadid',
        ];
        $formData = I('post.data');
        $taskInputData = [];
        $requiredField = [
            'fadclasscode',
            'fadclasscode_v2',
            'fname',
            'fadname',
            'fbrand',
            'taskid'
        ];
        if ($formData['fadname'] == '社会公益'){
            $adInfo = M('tad')->where(['fadname' => ['EQ', '社会公益']])->cache(true, 60)->find();
            $formData['fadclasscode'] = $adInfo['fadclasscode'];
            $formData['fadclasscode_v2'] = $adInfo['fadclasscode_v2'];
            $formData['fname'] = '广告主不详';
            $formData['fbrand'] = $adInfo['fbrand'];
        }
        foreach ($taskInputTableField as $key => $value) {
            if (isset($formData[$value])){
                $taskInputData[$value] = $formData[$value];
            }elseif (in_array($value, $requiredField)){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '有必填字段没有填写, 请重新填写',
                    'v' => $value
                ]);
            }
        }
        $model = new NetAdModel();
        M()->startTrans();
        $res = $model->submitAdInputTask($taskInputData, $wx_id);
        if ($res){
            $scoreRes = (new TaskInputScoreModel())->addScoreBySubmitNetTask($taskInputData['taskid'], 1);
            $adCateText1 = M('tadclass')->where(['fcode' => ['EQ', $taskInputData['fadclasscode']]])->cache(true)->getField('ffullname');
            $adCateText2 = M('hz_ad_class')->where(['fcode' => ['EQ', $taskInputData['fadclasscode_v2']]])->cache(true)->getField('ffullname');
            $adCateText2 = $adCateText2 ? $adCateText2 : '';
            $major_key = $taskInputData['major_key'];
            if(!$major_key){
                $major_key = M('tnettask')->where(['taskid'=>$formData['taskid']])->getField('major_key');
            }
            $net_target_url = M('tnetissue')->where(['major_key' => $major_key])->getField('net_target_url');
            // 是否需要进行下一步违法审核,不需要则自动提交为不违法。1-需要审核 0-不需要审核
            $isNeedNextStep = 1;
            if(in_array($formData['fadclasscode'],$this->notIllegalMap['fadclasscode']) || substr($formData['fadclasscode'],0,2) == '22' || substr($formData['fadclasscode'],0,2) == '23'){
                $isNeedNextStep = 0;
            }else{
                foreach($this->notIllegalMap['fadname'] as $val){
                    $exist = strpos($formData['fadname'],$val) !== false;
                    if($exist) $isNeedNextStep = 0;
                }
                if($isNeedNextStep != 0){
                    foreach($this->notIllegalMap['net_target_url'] as $val){
                        $exist = strpos($net_target_url,$val) !== false;
                        if($exist) $isNeedNextStep = 0;
                    }
                }
            }
            M()->commit();
            // 不需要审核的自动提交审核环节
            if($isNeedNextStep == 0){
                $taskInputData = [
                    'taskid'           => $formData['taskid'],
                    'media_class'      => 13,
                    'major_key'        => $major_key,
                    'fillegaltypecode' => 0,
                    'fillegalcontent'  => '',
                    'fexpressioncodes' => '',
                    'fmanuno'          => '',
                    'fadmanuno'        => '',
                    'fapprno'          => '',
                    'fadapprno'        => '',
                    'fent'             => '',
                    'fadent'           => '',
                    'fentzone'         => '',
                ];
                $model = new NetAdModel();
                $resJudge = $model->submitJudgeTask($taskInputData, 0);
            }
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '提交成功',
                'adCateText1' => $adCateText1,
                'adCateText2' => $adCateText2,
                'isNeedNextStep' => $isNeedNextStep,
            ]);
        }else{
            M()->rollback();
            $this->ajaxReturn([
                'code' => -1,
                'msg' => $model->getError(),
            ]);
        }
    }

    public function submitJudgeTask()
    {
        $wx_id = session('wx_id');
        if(!A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE) && !A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        $taskInputTableField = [
            'taskid',
            'fspokesman',
            'fversion',
            'is_long_ad',
            'fadclasscode',
            'fadclasscode_v2',
            'fname',
            'fadname',
            'fbrand',
            'fadid',
            'fillegaltypecode',
            'fillegalcontent',
            'fexpressioncodes',
            'fmanuno',
            'fadmanuno',
            'fapprno',
            'fadapprno',
            'fent',
            'fadent' ,
            'media_class',
            'major_key',
            'fentzone',
            'source_path',
        ];
        $formData = I('post.data');
        $taskInputData = [];
        $requiredField = [
            'taskid',
            'fillegaltypecode',
        ];
        foreach ($taskInputTableField as $key => $value) {
            if (isset($formData[$value])){
                $taskInputData[$value] = $formData[$value];
            }elseif (in_array($value, $requiredField)){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '有必填字段没有填写, 请重新填写',
                    'v' => $value
                ]);
            }
        }
        if ($formData['fillegaltypecode'] != '0'){
            $arrExpressioncode = explode(';',$formData['fexpressioncodes']);
            $fexpressionInfo = M('tillegal')->field('fexpression, fconfirmation')->where(['fcode'=>['IN', $arrExpressioncode]])->select();
            $fexpressions = '';
            $fconfirmations = '';
            foreach($fexpressionInfo as $key=>$item){
                $fexpressions .= $item['fexpression'].';';
                $fconfirmations .= $item['fconfirmation'].';';
            }
            $taskInputData['fexpressions'] = $fexpressions;
            $taskInputData['fconfirmations'] = $fconfirmations;
            $taskInputData['net_target_url'] = $formData['net_target_url'];
        }else{
            // 互联网违法广告自动添加
            A('Agp/Fnetad','Model')->ad_addaction($formData['major_key']);
        }

        $model = new NetAdModel();
        // M()->startTrans();
        $res = $model->submitJudgeTask($taskInputData, $wx_id);
        if ($res){
            // M()->commit();
            // 积分
            $taskInfo = M('tnettask')->alias('a')->field('b.fadclasscode')->join('tad b ON b.fadid=a.fadid')->where(['a.taskid'=>$taskInputData['taskid']])->find();
            if(substr($taskInfo['fadclasscode'],0,4) != 2202){
                // 公益广告判定不积分
                $scoreRes = (new TaskInputScoreModel())->addScoreBySubmitNetTask($taskInputData['taskid'], 2);
            }

            $this->ajaxReturn(['code' => 0,'msg' => '提交成功']);
        }else{
            // M()->rollback();
            $this->ajaxReturn(['code' => -1,'msg' => $model->getError()]);
        }
    }
    
    /**
     * 标记为非广告
     * @param $taskid
     * @param $reason
     */
    public function submitError($taskid, $reason)
    {
        if(!A('InputPc/Task','Model')->user_other_authority(self::USER_AUTH_CODE)){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        $taskInfo = M('tnettask')->where(['taskid' => ['EQ', $taskid]])->find();
        $taskUserId = $taskInfo['wx_id'];
        if ($taskUserId != session('wx_id')){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '此任务当前不是你的任务, 刷新页面后如果没有新任务请联系管理员'
            ]);
        }
        if (substr($taskInfo['task_state'],-1,1) != 1 && substr($taskInfo['task_state'],-1,1) != 3 && substr($taskInfo['task_state'],-1,1) != 4){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '该任务状态异常, 请刷新页面'
            ]);
        }
        // 任务表
        $data = [
            'cut_err_reason' => $reason,
            'cut_err_state' => 1,
            'isad' => 0,
            'notaduser' => session('wx_id'),
            'task_state' => 9,
            'syn_state' => 0,
            'modifier' => session('wx_id'),
            'modifytime' => time()
        ];
        // 广告样本表
        $issueData = [
            'isad' => 0,
            'notaduser' => session('wx_id'),
            'fmodifier' => session('wx_id'),
            'fmodifytime' => date('Y-m-d H:i:s')
        ];
        // 日志类型码
        $logType = "96";
        M()->startTrans();
        $res = M('tnettask')
            ->where(['taskid' => $taskid])
            ->save($data);
        $res = $res === false ? false : true;
        $resIssue = M('tnetissue')
            ->where(['major_key'=>$taskInfo['major_key']])
            ->save($issueData);

        try{//尝试新增数据
            $identify_code = M('tnetissue_identify')->where(array('major_key'=>$data['major_key']))->getField('identify_code');
            $major_key_list = M('tnetissue_identify')->where(array('identify_code'=>$identify_code,'major_key'=>array('neq',$data['major_key'])))->getField('major_key',true);
            
            # 把这些样本id的任务改为违法判定已完成  $major_key_list
            if(!empty($major_key_list)){
                $issueData['finputuser'] = 32;
                $issueData['fmodifier'] = 32;
                M('tnetissue')->where(['major_key' => ['in', $major_key_list]])->save($issueData);
            }
            $issueData = array();
        }catch( \Exception $e) { //如果新增失败则获取id
            
        }
            
        $resIssue = $resIssue === false ? false : true;
        if($res && $resIssue){
            // 更新流程日志表
            $resLog = M('tnettask_flowlog')
                ->add([
                    'taskid' => $taskid,
                    'tasklink' => 9,
                    'wx_id' => $taskUserId,
                    'logtype' => $logType,
                    'log' => $this->LogType[$nextTaskState],
                    'creator' => session('wx_id'),
                    'createtime' => time()
                ]);
            $resCount = A('InputPc/Task','Model')->change_task_count(session('wx_id'),self::CUT_ERROR_COUNT_FIELD,1);
            M()->commit();
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '成功'
            ]);
        }else{
            M()->rollback();
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '出错'
            ]);
        }
    }

    /**
     * 测试-添加众包任务
     */
    public function testAddTask(){
        $dataSet = M('tnetissue')
            ->field('major_key,fmediaid,net_platform,net_target_url,net_created_date')
            ->where(['net_target_url'=>['LIKE','https://m.baidu.com/from=%']])
            ->limit(0,10)
            ->select();
        $ret = A('TaskInput/NetAd','Model')->addTask($dataSet);
    }
    
    /**
     * 获取互联网广告(样本)表记录，创建任务
     * @Param $major_key 可选，指定样本表起始ID开始遍历添加到众包任务(按样本ID主键过滤重复)
     */
    public function autoCreateTask($major_key = 0){
        set_time_limit(0);
        if($major_key == 0){
            // 查询任务表中已存在的最大样本ID
            $major_key = M('tnettask')->max('major_key');
        }
        $where = [
            'major_key' => ['GT', $major_key],
            'isad'      => 1,
            '_string'   => 'fadclasscode="2301" OR fadclasscode=""',//广告类别为空或默认值(2301)才需推送到众包任务
        ];
        $dataSet = M('tnetissue')
            ->where($where)
            ->limit(0,500)
            ->select();
        $ret = A('TaskInput/NetAd','Model')->addTask($dataSet);
        if($ret && !empty($dataSet)) {
            $this->autoCreateTask();
        }else{
            $this->ajaxReturn(['msg'=>'成功']);
        }
    }

    /**
     * 批量创建互联网广告众包任务(SQL)
     * @Param $sql 执行的sql语句，得到最终的任务数据，将直接添加到众包任务！
     */
    public function sqlCreateTask($sql = ''){
        set_time_limit(0);
        $pageIndex = 1;
        $pageSize = 20;
        $strSql = "SELECT
                t.*
            FROM
                tnetissue AS t
                INNER JOIN tmedia AS m ON m.fid = t.fmediaid 
            WHERE
                net_created_date > 1535731200000 
                AND t.isad =1 
                AND (t.fadclasscode = 2301 OR t.fadclasscode = '')
                AND (
                    t.fmediaid IN (
                    SELECT
                        fid 
                    FROM
                        tmedia 
                    WHERE
                        fmedianame IN (
                            '趣头条',
                            'WiFi万能钥匙',
                            '聚力视频',
                            '惠头条',
                            '东方头条',
                            '界面新闻',
                            '火猴浏览器',
                            '咪咕视频',
                            '淘最热点',
                            '趣看天下',
                            '喜马拉雅',
                            'PP体育',
                            '阅新闻',
                            '虎扑体育',
                            '哔哩哔哩',
                            '触宝电话',
                            '拼多多',
                            '东方购物',
                            '追书神器',
                            '蜻蜓FM',
                            '2345天气王',
                            '周到上海',
                            'kds宽带山',
                            '东方财富',
                            '澎湃新闻',
                            '沪江网校',
                            '齐家-装修建材',
                            '前程无忧51Job',
                            '连尚头条',
                            'Soul灵犀',
                            '小红书',
                            '全民K歌',
                            '携程旅行',
                            '盒马生鲜',
                            '安居客',
                            '饿了么',
                            '大众点评',
                            '哈罗单车',
                            '上海头条',
                            '上海去哪吃',
                            '发现上海',
                            '上海国拍',
                            '上海圈',
                            '上海妈咪',
                            '中国银行上海分行',
                            '招商银行信用卡上海',
                            '这里是上海',
                            '上海全接触',
                            '上海吃货团',
                            '上海家长',
                            '上海苏宁',
                            '魔都上海',
                            '上海小O',
                            '中国移动上海公司',
                            '上海范儿',
                            '上海精致生活',
                            '上海吃喝玩乐',
                            '上海最前线',
                            '上海乐享生活',
                            '吃喝玩乐IN上海',
                            '上海都市生活圈',
                            '上海交通广播',
                            '浦发银行信用卡上海',
                            '中国农业银行上海市分行',
                            '邂逅at上海',
                            '上海热门资讯',
                            '上海幼升小指导',
                            '上海实习',
                            '小资上海',
                            '上海热点',
                            '上海家具展',
                            '上海时刻',
                            '香草招聘上海',
                            '上海最资讯',
                            '极乐汤上海',
                            '上海佛罗伦萨小镇',
                            '上海E潮流',
                            '发现最美上海',
                            '上海美食',
                            '潮上海',
                            '上海卫康',
                            '上海静安嘉里中心',
                            '看懂上海',
                            '上海宝玉石交易中心',
                            '上海E生活',
                            '上海梵讯',
                            '上海玉佛禅寺' 
                        ) 
                    GROUP BY
                        fmedianame 
                    ) 
                    OR t.fmediaid IN (
                    SELECT
                        fid 
                    FROM
                        tmedia 
                    WHERE
                        fmediacode IN (
                            'com.jifen.qukan',
                            'com.snda.wifilocating',
                            'com.pplive.androidphone',
                            'com.cashtoutiao',
                            'com.songheng.eastnews',
                            'com.jiemian.news',
                            'com.huohoubrowser',
                            'com.cmcc.cmvideo',
                            'com.maihan.tredian',
                            'com.yanhui.qktx',
                            'com.ximalaya.ting.android',
                            'com.pplive.androidphone.sport',
                            'cn.viaweb.toutiao',
                            'com.hupu.games',
                            'tv.danmaku.bili',
                            'com.cootek.smartdialer',
                            'com.xunmeng.pinduoduo',
                            'com.ocj.oms.mobile',
                            'com.ushaqi.zhuishushenqi',
                            'fm.qingting.qtradio',
                            'com.tianqi2345',
                            'com.inmovation.newspaper',
                            'net.pchome.kds',
                            'com.eastmoney.android.berlin',
                            'com.wondertek.paper',
                            'com.hujiang.hjclass',
                            'com.qijia.o2o',
                            'com.job.android',
                            'com.linksure.tt',
                            'cn.soulapp.android',
                            'com.xingin.xhs',
                            'com.tencent.karaoke',
                            'ctrip.android.view',
                            'com.wudaokou.hippo',
                            'com.anjuke.android.app',
                            'me.ele',
                            'com.dianping.v1',
                            'com.jingyao.easybike',
                            'shtt365',
                            'meishi388',
                            'fxsh021',
                            'shanghaiguopai',
                            'shquan001',
                            'shmm310',
                            'SHBOC_ebanking',
                            'cmbcc021',
                            'Shanghai257',
                            'sh360touch',
                            'shanghaigastronome',
                            'shjz52',
                            'suning_shanghai',
                            'modu20',
                            'opposhanghai',
                            'shanghaicmcc',
                            'shpurestyle',
                            'luckyshanghai',
                            'shchih',
                            'amazingSH',
                            'shlexiang',
                            'joyfulsh',
                            'shdsshq',
                            'jt1057',
                            'pfyhxyksh',
                            'ABCHINA_SHANGHAI',
                            'XiehouxiayizhanMSM',
                            'meirishanghai',
                            'shanghai-elitebaby',
                            'intern021',
                            'ChicShanghai',
                            'shsh9178',
                            'Furniture_China',
                            'shanghaishike',
                            'xczpsh',
                            'shn310',
                            'gokurakuyu_weixin',
                            'FVSHoutlet',
                            'ecl021',
                            'discoverysh',
                            'shmsms',
                            'Tied-sh',
                            'shanghaiweicon',
                            'jingankerrycentre',
                            'kankanews_sh',
                            'shgjtc',
                            'esh800',
                            'fan-xun',
                            'shyufotemple_gf' 
                        ) 
                    GROUP BY
                        fmediacode 
                    )
                )";
        if($sql == ''){
            //若未传参，判断是否通过接口传json数据
            $input = json_decode(file_get_contents('php://input'),true);
            $sql = $input['sql'] ? $input['sql'] : $strSql;
        }
        //内部调用传参或接口传json
        if($sql != ''){
            do {
                $dataSet = $this->sqlQuery($sql,$pageIndex,$pageSize);
                if($dataSet && !empty($dataSet)){
                    $ret = A('TaskInput/NetAd','Model')->addTask($dataSet);
                    $pageIndex++;
                }
            } while ($dataSet && !empty($dataSet));

            if($ret){
                $this->ajaxReturn(['code'=>1,'msg'=>'成功']);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'出错']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * sql执行
     */
    protected function sqlQuery($sql = '',$pageIndex = 1,$pageSize = 20){
        $recIndex = ($pageIndex-1)*$pageSize;
        $sql .= "limit ".$recIndex.",".$pageSize;
        $dataSet = M()->query($sql);
        return $dataSet;
    }
    
    /**
     * 返回广告类型树
     * @return array
     */
    protected function getAdClassArr()
    {
        $data = M('tadclass')
            ->field('fcode, fpcode, fadclass')
            ->where(['fstate' => ['EQ', 1], 'fcode' => ['NEQ', 2301]])
            ->cache(true)
            ->select();
        $res = [];
        foreach ($data as $key => &$value) {
            if (strlen($value['fcode']) === 2){
                $res[] = [
                    'value' => $value['fcode'],
                    'label' => $value['fadclass'],
                ];
            }
        }
        foreach ($data as $key => &$value2) {
            $value2['value'] = $value2['fcode'];
            $value2['label'] = $value2['fadclass'];
            if (strlen($value2['fcode']) === 4){
                foreach ($res as &$item) {
                    if ($value2['fpcode'] === $item['value']){
                        $item['children'][] = $value2;
                        break;
                    }
                }
            }
        }
        return $res;
    }

    /**
     * 获取一级行政区划
     */
    protected function getRegionArr()
    {
        $data = M('tregion')->cache(true)->field('fid value, fname label, ffullname')->where([
            'fstate' => ['EQ', 1],
            'flevel' => ['EQ', 1],
        ])->select();
        foreach ($data as &$datum) {
            $datum['children'] = [];
        }
        return $data;
    }

    /**
     * 返回违法表现树
     * @return array
     */
    protected function getIllegalArr()
    {
        $data = M('tillegal')->field('fcode, fpcode, fexpression, fconfirmation, fillegaltype')->where(['fstate' => ['EQ', 1]])->cache(true)->select();
        $res = [];
        foreach ($data as &$datum) {
            $datum['id'] = $datum['fcode'];
            $datum['label'] = $datum['fexpression'];
            if (!$datum['fpcode']){
                $res[] = $datum;
            }
        }
        foreach ($data as $item) {
            foreach ($res as &$re) {
                if ($item['fpcode'] === $re['fcode']){
                    $re['children'][] = $item;
                    break;
                }
            }
        }
        return $res;
    }

    /**
     * 返回违法类型树
     * @return array
     */
    protected function getIllegalTypeArr()
    {
        $data = M('tillegaltype')->field('fcode value, fillegaltype text')->cache(true)->where(['fstate' => ['EQ', 1]])->select();
        return $data;
    }
    
    /**
     * 商业类型树
     */
    protected function getAdClassTree()
    {
        $data = M('hz_ad_class')->field('fcode value, fpcode, fname label')->where(['fstatus' => ['EQ', 1]])->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpcode']])){
                $items[$value['fpcode']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }
    
    /**
     * 基础众包任务搜索方法
     * @param $formData array 前端传过来的搜索条件
     * @param $where array 自定义的搜索条件
     * @param int $page
     * @return array
     */
    public function baseTaskInputSearch($formData, $where, $page)
    {
        if ($formData['timeRangeArr']){
            foreach ($formData['timeRangeArr'] as $key => $value) {
                $formData['timeRangeArr'][$key] = $value / 1000;
            }
            if ($formData['timeRangeArr'][0] == $formData['timeRangeArr'][1]){
                $formData['timeRangeArr'][1] = strtotime('+1 day', $formData['timeRangeArr'][1]);
            }
        }
        if ($formData['media_class'] != ''){
            $where['task.media_class'] = ['EQ', $formData['media_class']];
        }
        if ($formData['fillegaltypecode'] != ''){
            $where['fillegaltypecode'] = ['EQ', $formData['fillegaltypecode']];
        }
        if ($formData['task_state'] != ''){
            $where['task_state'] = ['EQ', $formData['task_state']];
        }
        if ($formData['quality_state'] != ''){
            $where['quality_state'] = ['EQ', $formData['quality_state']];
        }
        if ($formData['timeRangeArr'] != ''){
            $where['modifytime'] = ['BETWEEN', $formData['timeRangeArr']];
        }
        if ($formData['net_platform'] != ''){
            $where['net_platform'] = ['EQ', $formData['net_platform']];
        }

        $where['cut_err_state'] = ['EQ', 0];

        if ($formData['wx_id'] != ''){
            $wxSubQuery = M('ad_input_user')
                ->field('wx_id')
                ->where([
                    'alias|nickname' => ['LIKE', '%'.$formData['wx_id'].'%']
                ])
                ->buildSql();
            $where['task.wx_id'] = ['EXP', 'IN '.$wxSubQuery];
        }
        // 广告搜索条件, 在tad表查找
        $adWhere = [];
        if ($formData['adClass'] != ''){
            $len = strlen($formData['adClass']);
            $adWhere["LEFT(fadclasscode, $len)"] = ['EQ', $formData['adClass']];
        }
        if ($formData['adClass2'] != ''){
            $len = strlen($formData['adClass2']);
            $adWhere["LEFT(fadclasscode_v2, $len)"] = ['EQ', $formData['adClass2']];
        }
        if ($formData['adName'] != ''){
            $adWhere['fadname'] = ['LIKE', '%'.$formData['adName'].'%'];
        }
        if ($adWhere != []){
            $adSubQuery = M('tad')
                ->field('fadid')
                ->where($adWhere)
                ->buildSql();
            $where['fadid'] = ['EXP', 'IN '. $adSubQuery]; // 广告搜索条件
        }
        // tmedia表
        $mediaWhere = [];
        if ($formData['mediaName'] != ''){
            $mediaWhere['fmedianame'] = ['LIKE', '%'.$formData['mediaName'].'%'];
        }
        // tmediaowner表
        $mediaOwnerWhere = [];
        if ($formData['regionId'] != ''){
            if ($formData['onlyThisLevel'] != 1){
                $region_id_rtrim = A('Common/System','Model')->get_region_left($formData['regionId']);//地区ID去掉末尾的0
                $region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
                $mediaOwnerWhere["LEFT(fregionid, $region_id_strlen)"] = ['EQ', $region_id_rtrim];
            }else{
                $mediaOwnerWhere['fregionid'] = ['LIKE', '%'.$formData['regionId'].'%'];
            }
            $mediaOwnerSubQuery = M('tmediaowner')
                ->field('fid')
                ->where($mediaOwnerWhere)
                ->buildSql();
            $mediaWhere['fmediaownerid'] = ['EXP', 'IN '. $mediaOwnerSubQuery];//地区搜索条件
        }
        if ($mediaWhere != []){
            $mediaSubQuery = M('tmedia')
                ->field('fid')
                ->where($mediaWhere)
                ->buildSql();
            $where['media_id'] = ['EXP', 'IN '. $mediaSubQuery]; // 媒体搜索条件
        }
        $res = M('tnettask')
            ->alias('task')
            ->field("task.major_key, task.taskid, task.fadid, task.media_class, fillegaltypecode, task_state, quality_state, modifytime, cut_err_state, task.wx_id, back_reason, media_id, major_key, (select net_platform from tnetissue where major_key = task.major_key) AS net_platform")
            ->where($where)
            ->order('modifytime desc')
            ->page($page[0], $page[1])
            ->select();
        $fetchSql = M('tnettask')->getLastSql();
        $count = M('tnettask')
            ->alias('task')
            ->where($where)
            ->count();
        foreach ($res as $key => $value) {
            // 录入人
            $input_user = M('tnettask_flowlog')
                ->cache(true,120)
                ->where(['taskid'=>$value['taskid'],'logtype'=>12])
                ->order('createtime DESC')
                ->getField('wx_id');
            if(!$input_user){
                // 若12状态未找到则查找2状态
                $input_user = M('tnettask_flowlog')
                    ->cache(true,120)
                    ->where(['taskid'=>$value['taskid'],'logtype'=>2])
                    ->order('createtime DESC')
                    ->getField('wx_id');
            }
            // 判定人
            $inspect_user = M('tnettask_flowlog')
                ->cache(true,120)
                ->where(['taskid'=>$value['taskid'],'logtype'=>22])
                ->order('createtime DESC')
                ->getField('wx_id');
            if(!$inspect_user){
                // 若22状态未找到则查找2状态
                $inspect_user = M('tnettask_flowlog')
                    ->cache(true,120)
                    ->where(['taskid'=>$value['taskid'],'logtype'=>2])
                    ->order('createtime DESC')
                    ->getField('wx_id');
            }
            $res[$key]['modifytime'] = date('Y-m-d H:i:s', $value['modifytime']);
            $res[$key]['alias'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $value['wx_id']]])->getField('alias');
            $res[$key]['input_user'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $input_user]])->getField('alias');
            $res[$key]['inspect_user'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $inspect_user]])->getField('alias');
            $res[$key]['fmedianame'] = M('tmedia')->where(['fid' => ['EQ', $value['media_id']]])->getField('fmedianame');
            // $adInfo = M('tnetissue')->field('fadname, fadclasscode, fadclasscode_v2, fadowner, fbrand')->find($value['major_key']);
            $adInfo = M('tad')->field('fadname,fadclasscode,fadclasscode_v2,fadowner,fbrand')->find($value['fadid']);
            $res[$key]['fadname'] = $adInfo['fadname'];
            $res[$key]['fadclasscode'] = $adInfo['fadclasscode'];
            $res[$key]['fbrand'] = $adInfo['fbrand'];
            $res[$key]['ffullname'] = M('tadclass')->cache(true)->where(['fcode' => ['EQ', $adInfo['fadclasscode']]])->getField('ffullname');
            $res[$key]['adClassV2'] = M('hz_ad_class')->cache(true)->where(['fcode' => ['EQ', $adInfo['fadclasscode_v2']]])->getField('ffullname');
            $res[$key]['adOwnerName'] = M('tadowner')->cache(true)->where(['fid' => ['EQ', $adInfo['fadowner']]])->getField('fname');
            $res[$key]['fadname'] = r_highlight($res[$key]['fadname'], $formData['adName'], 'red');
            $res[$key]['fmedianame'] = r_highlight($res[$key]['fmedianame'], $formData['mediaName'], 'red');
        }
        $data = [
            'data' => $res,
            'total' => $count,
            'page' => $page[0],
            'where' => $mediaWhere
        ];
        return $data;
    }

    /**
     * 基础众包质检任务搜索方法
     * @param $formData array 前端传过来的搜索条件
     * @param $where array 自定义的搜索条件
     * @param int $page
     * @return array
     */
    public function inspectorTaskInputSearch($formData, $where, $page)
    {
        if ($formData['timeRangeArr']){
            foreach ($formData['timeRangeArr'] as $key => $value) {
                $formData['timeRangeArr'][$key] = $value / 1000;
            }
            if ($formData['timeRangeArr'][0] == $formData['timeRangeArr'][1]){
                $formData['timeRangeArr'][1] = strtotime('+1 day', $formData['timeRangeArr'][1]);
            }
        }
        if ($formData['media_class'] != ''){
            $where['task.media_class'] = ['EQ', $formData['media_class']];
        }
        if ($formData['fillegaltypecode'] != ''){
            $where['fillegaltypecode'] = ['EQ', $formData['fillegaltypecode']];
        }
        if ($formData['task_state'] != ''){
            $where['task_state'] = ['EQ', $formData['task_state']];
        }
        if ($formData['quality_state'] != ''){
            $where['quality_state'] = ['EQ', $formData['quality_state']];
        }
        if ($formData['timeRangeArr'] != ''){
            $where['modifytime'] = ['BETWEEN', $formData['timeRangeArr']];
        }
        if ($formData['net_platform'] != ''){
            $where['net_platform'] = ['EQ', $formData['net_platform']];
        }

        $where['cut_err_state'] = ['EQ', 0];

        if ($formData['wx_id'] != ''){
            $wxSubQuery = M('ad_input_user')
                ->field('wx_id')
                ->where([
                    'alias|nickname' => ['LIKE', '%'.$formData['wx_id'].'%']
                ])
                ->buildSql();
            $where['task.wx_id'] = ['EXP', 'IN '.$wxSubQuery];
        }
        // 广告搜索条件, 在tad表查找
        $adWhere = [];
        if ($formData['adClass'] != ''){
            $len = strlen($formData['adClass']);
            $adWhere["LEFT(fadclasscode, $len)"] = ['EQ', $formData['adClass']];
        }
        if ($formData['adClass2'] != ''){
            $len = strlen($formData['adClass2']);
            $adWhere["LEFT(fadclasscode_v2, $len)"] = ['EQ', $formData['adClass2']];
        }
        if ($formData['adName'] != ''){
            $adWhere['fadname'] = ['LIKE', '%'.$formData['adName'].'%'];
        }
        if ($adWhere != []){
            $adSubQuery = M('tad')
                ->field('fadid')
                ->where($adWhere)
                ->buildSql();
            $where['fadid'] = ['EXP', 'IN '. $adSubQuery]; // 广告搜索条件
        }
        // tmedia表
        $mediaWhere = [];
        if ($formData['mediaName'] != ''){
            $mediaWhere['fmedianame'] = ['LIKE', '%'.$formData['mediaName'].'%'];
        }
        // tmediaowner表
        $mediaOwnerWhere = [];
        if ($formData['regionId'] != ''){
            if ($formData['onlyThisLevel'] != 1){
                $region_id_rtrim = A('Common/System','Model')->get_region_left($formData['regionId']);//地区ID去掉末尾的0
                $region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
                $mediaOwnerWhere["LEFT(fregionid, $region_id_strlen)"] = ['EQ', $region_id_rtrim];
            }else{
                $mediaOwnerWhere['fregionid'] = ['LIKE', '%'.$formData['regionId'].'%'];
            }
            $mediaOwnerSubQuery = M('tmediaowner')
                ->field('fid')
                ->where($mediaOwnerWhere)
                ->buildSql();
            $mediaWhere['fmediaownerid'] = ['EXP', 'IN '. $mediaOwnerSubQuery];//地区搜索条件
        }
        if ($mediaWhere != []){
            $mediaSubQuery = M('tmedia')
                ->field('fid')
                ->where($mediaWhere)
                ->buildSql();
            $where['media_id'] = ['EXP', 'IN '. $mediaSubQuery]; // 媒体搜索条件
        }
        // tnetissue互联网样本表
        $sampleWhere = [
            'net_created_date' => ['BETWEEN', [$formData['timeRangeArr'][0]*1000,$formData['timeRangeArr'][1]*1000]]
        ];
        $samTable = 'tnetissue';
        $samId = 'major_key';
        $samSubQuery = M($samTable)
            ->field($samId)
            ->where($sampleWhere)
            ->buildSql();
        $where['major_key'] = ['EXP','IN '.$samSubQuery];// 查询时间范围内新建的样本任务


        $res = M('tnettask')
            ->alias('task')
            ->field("task.major_key, task.taskid, task.fadid, task.media_class, fillegaltypecode, task_state, quality_state, modifytime, cut_err_state, task.wx_id, back_reason, media_id, major_key, (select net_platform from tnetissue where major_key = task.major_key) AS net_platform")
            ->where($where)
            ->order('modifytime desc')
            ->page($page[0], $page[1])
            ->select();
        $count = M('tnettask')
            ->alias('task')
            ->where($where)
            ->count();
        foreach ($res as $key => $value) {
            // 录入人
            $input_user = M('tnettask_flowlog')
                ->cache(true,120)
                ->where(['taskid'=>$value['taskid'],'logtype'=>12])
                ->order('createtime DESC')
                ->getField('wx_id');
            if(!$input_user){
                // 若12状态未找到则查找2状态
                $input_user = M('tnettask_flowlog')
                    ->cache(true,120)
                    ->where(['taskid'=>$value['taskid'],'logtype'=>2])
                    ->order('createtime DESC')
                    ->getField('wx_id');
            }
            // 判定人
            $inspect_user = M('tnettask_flowlog')
                ->cache(true,120)
                ->where(['taskid'=>$value['taskid'],'logtype'=>22])
                ->order('createtime DESC')
                ->getField('wx_id');
            if(!$inspect_user){
                // 若22状态未找到则查找2状态
                $inspect_user = M('tnettask_flowlog')
                    ->cache(true,120)
                    ->where(['taskid'=>$value['taskid'],'logtype'=>2])
                    ->order('createtime DESC')
                    ->getField('wx_id');
            }
            $res[$key]['modifytime'] = date('Y-m-d H:i:s', $value['modifytime']);
            $res[$key]['alias'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $value['wx_id']]])->getField('alias');
            $res[$key]['input_user'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $input_user]])->getField('alias');
            $res[$key]['inspect_user'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $inspect_user]])->getField('alias');
            $res[$key]['fmedianame'] = M('tmedia')->where(['fid' => ['EQ', $value['media_id']]])->getField('fmedianame');
            // $adInfo = M('tnetissue')->field('fadname, fadclasscode, fadclasscode_v2, fadowner, fbrand')->find($value['major_key']);
            $adInfo = M('tad')->field('fadname,fadclasscode,fadclasscode_v2,fadowner,fbrand')->find($value['fadid']);
            $res[$key]['fadname'] = $adInfo['fadname'];
            $res[$key]['fadclasscode'] = $adInfo['fadclasscode'];
            $res[$key]['fbrand'] = $adInfo['fbrand'];
            $res[$key]['ffullname'] = M('tadclass')->cache(true)->where(['fcode' => ['EQ', $adInfo['fadclasscode']]])->getField('ffullname');
            $res[$key]['adClassV2'] = M('hz_ad_class')->cache(true)->where(['fcode' => ['EQ', $adInfo['fadclasscode_v2']]])->getField('ffullname');
            $res[$key]['adOwnerName'] = M('tadowner')->cache(true)->where(['fid' => ['EQ', $adInfo['fadowner']]])->getField('fname');
            $res[$key]['fadname'] = r_highlight($res[$key]['fadname'], $formData['adName'], 'red');
            $res[$key]['fmedianame'] = r_highlight($res[$key]['fmedianame'], $formData['mediaName'], 'red');
        }
        $data = [
            'data' => $res,
            'total' => $count,
            'page' => $page[0],
            'where' => $mediaWhere
        ];
        return $data;
    }

    /**
     * 基础众包任务搜索方法
     * @param $formData array 前端传过来的搜索条件
     * @param $where array 自定义的搜索条件
     * @param int $page
     * @return array
     */
    public function baseTaskInputSearch_V20190303($formData, $where, $page)
    {
        if ($formData['timeRangeArr']){
            foreach ($formData['timeRangeArr'] as $key => $value) {
                $formData['timeRangeArr'][$key] = $value / 1000;
            }
            if ($formData['timeRangeArr'][0] == $formData['timeRangeArr'][1]){
                $formData['timeRangeArr'][1] = strtotime('+1 day', $formData['timeRangeArr'][1]);
            }
        }
        if ($formData['media_class'] != ''){
            $where['task.media_class'] = ['EQ', $formData['media_class']];
        }
        if ($formData['fillegaltypecode'] != ''){
            $where['fillegaltypecode'] = ['EQ', $formData['fillegaltypecode']];
        }
        if ($formData['task_state'] != ''){
            $where['task_state'] = ['EQ', $formData['task_state']];
        }
        if ($formData['quality_state'] != ''){
            $where['quality_state'] = ['EQ', $formData['quality_state']];
        }
        if ($formData['timeRangeArr'] != ''){
            $where['modifytime'] = ['BETWEEN', $formData['timeRangeArr']];
        }

        $where['cut_err_state'] = ['EQ', 0];

        if ($formData['wx_id'] != ''){
            $wxSubQuery = M('ad_input_user')
                ->field('wx_id')
                ->where([
                    'alias|nickname' => ['LIKE', '%'.$formData['wx_id'].'%']
                ])
                ->buildSql();
            $where['task.wx_id'] = ['EXP', 'IN '.$wxSubQuery];
        }
        // 广告搜索条件, 在tnetissue表查找
        $adWhere = [];
        if ($formData['adClass'] != ''){
            $len = strlen($formData['adClass']);
            $adWhere["LEFT(fadclasscode, $len)"] = ['EQ', $formData['adClass']];
        }
        if ($formData['adClass2'] != ''){
            $len = strlen($formData['adClass2']);
            $adWhere["LEFT(fadclasscode_v2, $len)"] = ['EQ', $formData['adClass2']];
        }
        if ($formData['adName'] != ''){
            $adWhere['fadname'] = ['LIKE', '%'.$formData['adName'].'%'];
        }
        if ($formData['net_platform'] != ''){
            $adWhere['net_platform'] = ['EQ', $formData['net_platform']];
        }
        // tmedia表
        $mediaWhere = [];
        if ($formData['mediaName'] != ''){
            $mediaWhere['fmedianame'] = ['LIKE', '%'.$formData['mediaName'].'%'];
        }
        // tmediaowner表
        $mediaOwnerWhere = [];
        if ($formData['regionId'] != ''){
            if ($formData['onlyThisLevel'] != 1){
                $region_id_rtrim = A('Common/System','Model')->get_region_left($formData['regionId']);//地区ID去掉末尾的0
                $region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
                $mediaOwnerWhere["LEFT(fregionid, $region_id_strlen)"] = ['EQ', $region_id_rtrim];
            }else{
                $mediaOwnerWhere['fregionid'] = ['LIKE', '%'.$formData['regionId'].'%'];
            }
            $mediaOwnerSubQuery = M('tmediaowner')
                ->field('fid')
                ->where($mediaOwnerWhere)
                ->buildSql();
            $mediaWhere['fmediaownerid'] = ['EXP', 'IN '. $mediaOwnerSubQuery];//地区搜索条件
        }
        if ($mediaWhere != []){
            $mediaSubQuery = M('tmedia')
                ->field('fid')
                ->where($mediaWhere)
                ->buildSql();
            $where['media_id'] = ['EXP', 'IN '. $mediaSubQuery]; // 媒体搜索条件
        }
        if ($adWhere != []){
            $adSubQuery = M('tnetissue')
                ->field('major_key')
                ->where($adWhere)
                ->buildSql();
            $where['major_key'] = ['EXP', 'IN '. $adSubQuery]; // 广告搜索条件
        }

        $res = M('tnettask')
            ->alias('task')
            ->field("task.major_key, task.taskid, fadid, task.media_class, fillegaltypecode, task_state, quality_state, modifytime, cut_err_state, task.wx_id, back_reason, media_id, major_key, (select net_platform from tnetissue where major_key = task.major_key) AS net_platform, a.wx_id as input_user, b.wx_id  as inspect_user")
            ->join("left join task_input_link as a on task.taskid = a.taskid and a.media_class = 13 and a.type = 1")
            ->join("left join task_input_link as b on task.taskid = b.taskid and b.media_class = 13 and b.type = 2")
            ->where($where)
            ->order('modifytime desc')
            ->page($page[0], $page[1])
            ->select();
        $count = M('tnettask')
            ->alias('task')
            ->where($where)
            ->count();
        foreach ($res as $key => $value) {
            $res[$key]['modifytime'] = date('Y-m-d H:i:s', $value['modifytime']);
            $res[$key]['alias'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $value['wx_id']]])->getField('alias');
            $res[$key]['input_user'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $value['input_user']]])->getField('alias');
            $res[$key]['inspect_user'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $value['inspect_user']]])->getField('alias');
            $res[$key]['fmedianame'] = M('tmedia')->where(['fid' => ['EQ', $value['media_id']]])->getField('fmedianame');
            $adInfo = M('tnetissue')->field('fadname, fadclasscode,fadclasscode_v2, fadowner, fbrand')->find($value['major_key']);
            $res[$key]['fadname'] = $adInfo['fadname'];
            $res[$key]['fadclasscode'] = $adInfo['fadclasscode'];
            $res[$key]['fbrand'] = $adInfo['fbrand'];
            $res[$key]['ffullname'] = M('tadclass')->cache(true)->where(['fcode' => ['EQ', $adInfo['fadclasscode']]])->getField('ffullname');
            $res[$key]['adClassV2'] = M('hz_ad_class')->cache(true)->where(['fcode' => ['EQ', $adInfo['fadclasscode_v2']]])->getField('ffullname');
            $res[$key]['adOwnerName'] = M('tadowner')->cache(true)->where(['fid' => ['EQ', $adInfo['fadowner']]])->getField('fname');
            $res[$key]['fadname'] = r_highlight($res[$key]['fadname'], $formData['adName'], 'red');
            $res[$key]['fmedianame'] = r_highlight($res[$key]['fmedianame'], $formData['mediaName'], 'red');

        }
        $data = [
            'data' => $res,
            'total' => $count,
            'page' => $page[0],
            'where' => $mediaWhere
        ];
        return $data;
    }

    /**
     * 通过任务
     * @param $taskid
     */
    public function passTask($taskid)
    {
        if(  !A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        M()->startTrans();
        if ($this->isNetAd == 2){
            $netAdController = new NetAdController();
            $res = $netAdController->passTaskMethod($taskid);
        }elseif($this->isNetAd == 3){
            $outAdController = new OutAdController();
            $res = $outAdController->passTaskMethod($taskid);
        }else{
            $res = $this->passTaskMethod($taskid);
        }
        if ($res){
            M()->commit();
            A('Common/AliyunLog','Model')->task_input_log([
                'type' => 'save',
                'task' => $taskid,
                'msg' => '任务通过成功'
            ]);
            $this->ajaxReturn([
                'code' => 0
            ]);
        }else{
            M()->rollback();
            A('Common/AliyunLog','Model')->task_input_log([
                'type' => 'save',
                'task' => $taskid,
                'msg' => '任务通过失败'
            ]);
            $this->ajaxReturn([
                'code' => -1
            ]);
        }
    }

    /**
     * 通过任务的私有方法
     * @param $taskid
     * @return bool
     */
    public function passTaskMethod($taskid)
    {
        $wx_id = session('wx_id');
        $time = time();
        $where = [
            'taskid' => ['EQ', $taskid]
        ];
        $taskData = [
            'quality_wx_id' => $wx_id,
            'quality_state' => 2,
            'task_state' => 2,
            'syn_state' => 1,
            'modifier' => $wx_id,
            'modifytime' => $time,
        ];
        $taskUpdate = M('tnettask')->where($where)->save($taskData);
        // 质检通过，表示所有环节都通过，为便于统计，查出每个环节负责人分别产生流程日志
        $links = [12,22];
        foreach($links as $val){
            $whereLog = [
                'taskid'  => $taskid,
                'logtype' => $val,
            ];
            $linkLogInfo = M('tnettask_flowlog')
                ->where($whereLog)
                ->order('createtime DESC')
                ->limit(1)
                ->find();
            $flowData = [
                'taskid' => $taskid,
                'tasklink' => (int)substr((string)$val,0,1),
                'wx_id' => $linkLogInfo['wx_id'],
                'log' => '质检通过',
                'logtype' => 6,
                'creator' => $wx_id,
                'createtime' => $time,
            ];
            $flowUpdate = M('tnettask_flowlog')->add($flowData);
        }

        if ($taskUpdate && $flowUpdate){
            A('InputPc/Task','Model')->change_task_count($wx_id, self::QUALITY_COUNT_FIELD, 1);
            return true;
        }else{
            return false;
        }
    }

    /**
     * 退回任务
     * @param $taskid   Int 任务ID
     * @param $reason   String 退回理由
     * @param $linkType Int 退回环节 1-录入 2-审核
     * @return bool
     */
    public function rejectTask($taskid, $reason = '', $linkType = 1)
    {
        $wx_id = session('wx_id');
        $time = time();
        $logType = (int)($linkType.'2');
        // 找到该退回任务环节的最后完成负责人
        $flowLogInfo = M('tnettask_flowlog')
            ->where(['taskid' => $taskid,'tasklink' => $linkType,'logtype' => $logType])
            ->order('createtime DESC')
            ->find();
        $linkUserId = $flowLogInfo['creator'];
        $taskInfo = M('tnettask')
            ->where(['taskid' => ['EQ', $taskid]])
            ->find();
        $userId = $taskInfo['wx_id'];
        $where = [
            'taskid' => ['EQ', $taskid]
        ];
        $taskState = (int)($linkType.'3');
        $taskData = [
            'wx_id' => $linkUserId,//任务环节最后负责人
            'quality_wx_id' => $wx_id,
            'quality_state' => 0,
            'task_state' => $taskState,
            'syn_state' => 0,
            'modifier' => $wx_id,
            'modifytime' => $time,
            'back_reason' => $reason,
        ];
        $flowData = [
            'taskid' => $taskid,
            'tasklink' => $linkType,
            'wx_id' => $linkUserId,//任务环节最后负责人
            'logtype' => $taskState,
            'log' => A('TaskInput/NetAd','Model')->getLogType($taskState).' 理由: ' . $reason,
            'createtime' => $time,
            'creator' => $wx_id
        ];
        $taskUpdate = M('tnettask')
            ->where($where)
            ->save($taskData);
        $flowUpdate = M('tnettask_flowlog')
            ->add($flowData);
        A('InputPc/Task','Model')
            ->change_task_count($userId, self::TASK_BACK_COUNT_FIELD, 2);
        $adName = M('tnetissue')
            ->where(['major_key' => ['EQ', $taskInfo['major_key']]])
            ->getField('fadname');
        A('Adinput/Weixin','Model')
            ->task_back($userId,$reason,$adName);

        A('InputPc/Task','Model')
            ->change_task_count($wx_id,self::QUALITY_COUNT_FIELD, 2);
        $res = (new TaskInputModel())->rejectTask($taskid, session('wx_id'), 13, 1, $reason);
        $res = (new TaskInputModel())->rejectTask($taskid, session('wx_id'), 13, 2, $reason);

        return $taskUpdate && $flowUpdate;
    }

    /**
     * 接收 录入员 主动撤回 请求
     * @param $taskid
     */
    public function receiveRecallRequest($taskid)
    {
        $taskData = M('tnettask')->find($taskid);
        $wxId = $taskData['wx_id'];
        if ($wxId != session('wx_id')) {
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '只有本人才可以撤回任务',
                'type' => 'error',
            ]);
        }elseif ($taskData['task_state'] != 2 && $taskData['quality_state'] != 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '当前任务状态不允许退回',
                'type' => 'error',
            ]);
        }else{
            M()->startTrans();
            $taskInputData = [
                'modifier' => $wxId,
                'modifytime' => time(),
                'task_state' => 5,
                'syn_state' => 0
            ];
            $taskInputUpdate = M('tnettask')->where(['taskid' => ['EQ', $taskid]])->save($taskInputData) === false ? false : true;
            $flowData = [
                'taskid' => $taskid,
                'log' => '提交撤回申请',
                'logtype' => 7,
                'creator' => $wxId,
                'createtime' => time()
            ];
            $taskFlowUpdate = M('tnettask_flowlog')->add($flowData) === false ? false : true;
            if ($taskInputUpdate && $taskFlowUpdate){
                M()->commit();
                $this->ajaxReturn([
                    'code' => 0,
                    'msg' => '发出请求成功,待质检员审核后任务将自动退回',
                    'type' => 'success',
                ]);
            }else{
                M()->rollback();
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '未知错误,请检查网络或联系管理员',
                    'type' => 'error',
                ]);
            }
        }
    }

    /*
    * 获取客户媒体列表
    * by zw
    */
    public function getUserMediaList(){
        $where_amp['a.fstatus'] = 1;
        $where_amp['a.fmediatype'] = 4;
        $where_amp['a.fmaincode'] = 0;

        $do_amp = M('tapimedia_map')
            ->field('ifnull(b.fmedianame,concat(a.fmediacodename,"（未关联）")) fmedianame,ifnull(b.fid,0) fid')
            ->alias('a')
            ->join('tmedia b on a.fmediaid = b.fid','left')
            ->join('(select fmediaid from tapimedialist group by fmediaid) c on c.fmediaid = a.fid')
            ->where($where_amp)
            ->order('b.fmediacode asc')
            ->select();
        $this->ajaxReturn(['code'=>0, 'msg'=>'获取成功', 'count'=>count($do_amp), 'data'=>$do_amp]);
    }

    /**
     * 获取互联网媒体分类
     * zw
     */
    public function getmediatype(){
        $where_mcs['fpid'] = 13;

        $do_mcs = M('tmediaclass')
            ->field('fid,fclass')
            ->where($where_mcs)
            ->select();
        $this->ajaxReturn(['code'=>0, 'msg'=>'获取成功', 'count'=>count($do_mcs), 'data'=>$do_mcs]);
    }

    /**
     * 上海数据检查-检查任务列表(互联网)
     */
    public function NetPlanList(){
        $fmediaid   = $this->P['fmediaid'];
        $fissuedate = $this->P['fissuedate'];
        $mtype      = $this->P['mtype'];
        $fdstatus   = $this->P['fdstatus'] === ""?-1:$this->P['fdstatus'];
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 20);

        if(isset($fmediaid) && $fmediaid != ''){
            if(!empty($fmediaid)){
                $where_at['c.fid'] = $fmediaid;
            }else{
                $where_at['b.fmediacode'] = 0;
            }
        }
        if(!empty($fissuedate)){
            $where_at['a.fissuedate'] = ['between',[strtotime($fissuedate[0]),strtotime($fissuedate[1])]];
        }
        if(!empty($mtype)){
            $where_at['e.fid'] = $mtype;
        }
        if($fdstatus != -1){
            if($fdstatus == 0){
                $where_at['a.fdstatus'] = ['in',[0,1]];
            }else{
                $where_at['a.fdstatus'] = $fdstatus;
            }
        }

        $where_at['b.fmediatype'] = 4;
        $where_at['b.fstatus'] = 1;
        $where_at['b.fmaincode'] = 0;

        $count_at = M('tapimedialist')
            ->alias('a')
            ->join('tapimedia_map b on a.fmediaid = b.fid')
            ->join('tmedia c on b.fmediaid = c.fid','left')
            ->join('tmediaclass e on e.fid = c.fmediaclassid','left')
            ->join('(select group_concat(tapimedia_map.fmediaid) child_fmediaid,fmaincode,fmediatype from tapimedia_map,tmedia where tapimedia_map.fmediaid = tmedia.fid and fmaincode>0 group by fmaincode,fmediatype) d on b.fmediacode = d.fmaincode and b.fmediatype = d.fmediatype','left')
            ->where($where_at)
            ->count();

        $data_at = M('tapimedialist')
            ->field('a.fid,b.fthreshold,a.fissuedate,a.fcreatetime,a.fdstatus,d.child_fmediaid,b.fmediaid,(case when a.fdstatus = 1 or a.fdstatus = 0 then 1 when a.fdstatus = 14 then 2 when a.fdstatus = 2 then 3 else 4 end) os,ifnull(c.fmedianame,concat(b.fmediacodename,"（未关联）")) fmedianame,b.fid mid,a.fgetcount')
            ->alias('a')
            ->join('tapimedia_map b on a.fmediaid = b.fid')
            ->join('tmedia c on b.fmediaid = c.fid','left')
            ->join('tmediaclass e on e.fid = c.fmediaclassid','left')
            ->join('(select group_concat(tapimedia_map.fmediaid) child_fmediaid,fmaincode,fmediatype from tapimedia_map,tmedia where tapimedia_map.fmediaid = tmedia.fid and fmaincode>0 group by fmaincode,fmediatype) d on b.fmediacode = d.fmaincode and b.fmediatype = d.fmediatype','left')
            ->where($where_at)
            ->page($pageIndex,$pageSize)
            ->order('os asc,a.fissuedate asc,a.fid asc')
            ->select();
        foreach ($data_at as $key => $value) {
            $data_at[$key]['fissuedate'] = date('Y-m-d',$value['fissuedate']);//时间格式重写

            //获取任务计划相关的媒体ID，客户的媒体有可能会关联多个我司媒体
            $medias = [];
            $medias[] = $value['fmediaid'];
            if(!empty($value['child_fmediaid'])){
                $medias = array_merge($medias,explode(',', $value['child_fmediaid']));
            }

            //查询数据量
            $where_ner['ftaskid'] = $value['fid'];
            $data_ner = M('tnetissue_customer') ->field('count(*) zcount,ifnull(sum(case when fstatus = 2 or fstatus = 8 then 1 else 0 end),0) djccount,ifnull(sum(case when fstatus = 14 then 1 else 0 end),0) sbzcount,ifnull(sum(case when fstatus = 4 then 1 else 0 end),0) dblcount,ifnull(sum(case when fstatus = 5 then 1 else 0 end),0) blzcount,ifnull(sum(case when fstatus = 6 then 1 else 0 end),0) tgcount,ifnull(sum(case when fstatus = 2 or fstatus = 4 or fstatus = 5 or fstatus = 6 or fstatus = 7 or fstatus = 8 or fstatus = 14 then 1 else 0 end),0) jtwccount,ifnull(sum(case when adilltype = 30 and fstatus = 6 then 1 else 0 end),0) wfcount ') -> where($where_ner) ->find();
            $data_at[$key] = array_merge($data_at[$key],$data_ner);

            //查询三个月平均计划数据量
            $where_aml = [
                'fissuedate'=>['between',[strtotime(date('Y-m-01',$value['fissuedate']).'-3 month'),strtotime(date('Y-m-01',$value['fissuedate']).'-1 day')]],
                'fdstatus'=>['in',[2,4]],
                'fmediaid'=>$value['mid']
            ];
            $do_aml = M('tapimedialist')
                ->field('sum(fgetcount)/count(*) nevtaskcount')
                ->where($where_aml)
                ->find();//查询近三个月已交付的任务
            if(!empty($do_aml)){
                $data_at[$key]['nevtaskcount'] = intval($do_aml['nevtaskcount']);
            }else{
                $data_at[$key]['nevtaskcount'] = 0;
            }
            
            //计划外的数据量
            $where_ner2['ftaskid'] = 0;
            $where_ner2['title'] = ['notlike','百度推广%'];
            $where_ner2['LENGTH(adtype)'] = ['egt',4];
            $where_ner2['publisher_id'] = ['in',$medias];
            if(!empty($fissuedate)){
                $where_ner2['issue_date'] = ['BETWEEN',[date('Y-m-d',strtotime($fissuedate[0])),date('Y-m-d',strtotime($fissuedate[1]))]];
            }else{
                $where_ner2['issue_date'] = ['BETWEEN',[date('Y-m-01',$value['fissuedate']),date('Y-m-d',$value['fissuedate'])]];
            }
            $do_ner2 = M('tnetissue_customer') 
                ->where($where_ner2) 
                ->count();
            $data_at[$key]['taskoutcount'] = $do_ner2;

            //查询等待处理的数据量
            $where_nk['media_id'] = ['in',$medias];
            $where_nk['task_state'] = 0;
            if(!empty($fissuedate)){
                $where_nk['issue_date'] = ['BETWEEN',[date('Y-m-d',strtotime($fissuedate[0])),date('Y-m-d',strtotime($fissuedate[1]))]];
            }else{
                $where_nk['issue_date'] = ['BETWEEN',[date('Y-m-01',$value['fissuedate']),date('Y-m-d',$value['fissuedate'])]];
            }
            $do_nk = M('tnettask')->where($where_nk)->count();
            $data_at[$key]['taskcount'] = $do_nk;
        }
        $this->ajaxReturn(['code'=>0, 'msg'=>'获取成功', 'count'=>$count_at, 'data'=>$data_at]);
    }

    /**
     * 修改任务优先级
     * zw
     */
    public function PlanDistinguishOrderAction(){
        $taskid = $this->P['taskid'];
        $editpriority = $this->P['editpriority']?$this->P['editpriority']:0;
        if(!empty($taskid)){
            $taskids = explode(',', $taskid);
        }
        $save_aml = M('tapimedialist') -> where(['fid'=>['in',$taskids],'fdstatus'=>['in',[0,1]]]) ->save(['priority'=>$editpriority,'fmodifier'=>session('wx_id'),'fmodifytime'=>date('Y-m-d H:i:s')]);
        if(!empty($save_aml)){
            $this->ajaxReturn(['code'=>0, 'msg'=>'优先级更新成功']);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'优先级更新失败']);
        }
    }

    /**
     * 修改任务数据的日期
     * zw
     */
    public function PlanDataIssueDateAction(){
        $editmonth = $this->P['editmonth']?$this->P['editmonth']:0;
        if(empty($editmonth)){
            $this->ajaxReturn(['code'=>1, 'msg'=>'请选择月份']);
        }

        $nowsttime = $editmonth;
        $nowedtime = date('Y-m-15',strtotime($nowsttime));
        $prvedtime = date('Y-m-d',strtotime($nowsttime.' -1 day'));
        $prvsttime = date('Y-m-16',strtotime($nowsttime.' -1 month'));

        //更新创建日期
        M()->execute('
            update tnetissue_customer set created_date = replace(created_date,issue_date,( select FROM_UNIXTIME(a.fissuedate,"%Y-%m-%d") from tapimedialist a
            inner join tapimedia_map b on a.fmediaid = b.fid
            where b.fmediatype = 4 and a.fissuedate BETWEEN '.strtotime($nowsttime).' and '.strtotime($nowedtime).' and a.fid = tnetissue_customer.ftaskid))
            where ftaskid in (
            select a.fid from tapimedialist a
            inner join tapimedia_map b on a.fmediaid = b.fid
            where b.fmediatype = 4 and a.fissuedate BETWEEN '.strtotime($nowsttime).' and '.strtotime($nowedtime).'
            ) and issue_date between "'.$prvsttime.'" and "'.$prvedtime.'";
        ');

        //更新发布日期
        $updateCount = M()->execute('
            update tnetissue_customer set issue_date = ( select FROM_UNIXTIME(a.fissuedate) from tapimedialist a
            inner join tapimedia_map b on a.fmediaid = b.fid
            where b.fmediatype = 4 and a.fissuedate BETWEEN '.strtotime($nowsttime).' and '.strtotime($nowedtime).' and a.fid = tnetissue_customer.ftaskid)
            where ftaskid in (
            select a.fid from tapimedialist a
            inner join tapimedia_map b on a.fmediaid = b.fid
            where b.fmediatype = 4 and a.fissuedate BETWEEN '.strtotime($nowsttime).' and '.strtotime($nowedtime).'
            ) and issue_date between "'.$prvsttime.'" and "'.$prvedtime.'";
        ');

        $this->ajaxReturn(['code'=>0, 'msg'=>'调整'.$updateCount.'条']);
    }

    /**
     * 修改任务状态
     * zw
     */
    public function PlanFinishAction(){
        $taskid     = $this->P['taskid']?$this->P['taskid']:-1;
        $save_aml = M('tapimedialist') -> where(['fid'=>$taskid,'fdstatus'=>['in',[0,1]]]) ->save(['fdstatus'=>2,'fmodifier'=>session('wx_id'),'fmodifytime'=>date('Y-m-d H:i:s')]);
        if(!empty($save_aml)){
            M('tnetissue_customer')->where(['ftaskid'=>$taskid,'fstatus'=>['in',[0,1,2,3,4,5,8,14]]])->save(['ftaskid'=>0]);
            $this->ajaxReturn(['code'=>0, 'msg'=>'状态更新成功']);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'状态更新失败']);
        }
    }

    /**
     * 计划数据检查
     * zw
     */
    public function PlanDataCheckAction(){
        $taskid     = $this->P['taskid']?$this->P['taskid']:-1;
        $reasonsel  = $this->P['reasonsel'];
        $reason     = $this->P['reason'];
        $types     = $this->P['types'];// 1通过，2不通过，3作废
        if(empty($reason) && $types == 3){
            $this->ajaxReturn(['code'=>1, 'msg'=>'请选择原因']);
        }
        if($types == 2 && empty($reasonsel)){
            $this->ajaxReturn(['code'=>1, 'msg'=>'请选择原因']);
        }

        $where_aml['id'] = $taskid;
        $where_aml['fstatus'] = ['in',[1,2,8]];
        $where_aml['_string'] = 'ftaskid in (select fid from tapimedialist where fdstatus in (0,1))';

        $do_aml = M('tnetissue_customer')
            ->where($where_aml)
            ->find();

        if($types == 1){
            $fstatus = 14;
            $logtype = 5;
        }elseif($types == 2){
            $fstatus = 4;
            $logtype = 6;
            if(!empty($reasonsel)){
                if(!empty($reason)){
                    $reason = '。补充：'.$reason;
                }
                $reason = implode(',', $reasonsel).$reason;
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'请选择原因']);
            }
        }elseif($types == 3){
            $fstatus = 7;
            $logtype = 7;
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'参数有误']);
        }

        $data_aml['fstatus'] = $fstatus;
        if(!empty($reason)){
            $data_aml['check_reason'] = $reason;
        }
        $data_aml['modifytime'] = time();
        $data_aml['check_wxid'] = session('wx_id');
        $data_aml['check_time'] = date('Y-m-d H:i:s');
        $save_aml = M('tnetissue_customer') 
            ->where($where_aml) 
            ->save($data_aml);
        if(!empty($save_aml)){
            //记录日志
            $flowData = [
                'taskid'     => $taskid,
                'wx_id'      => session('wx_id'),
                'logtype'    => $logtype,
                'log'        => $reason,
                'taskinfo'   => json_encode($do_aml),
                'creator'    => session('wx_id'),
                'createtime' => time(),
            ];
            $flowRes = M('tnetissue_customer_flowlog')->add($flowData);

            //通过或作废加分
            $scoreModel = new TaskInputScoreModel();
            if(($types == 1 || $types == 3) && !empty($flowRes)){
                $scoreModel->addScoreByNetCheckShTask($taskid,$do_aml['fstatus']);
            }
            //如果是退回，需要扣除用户分
            if($types == 2){
                $scoreModel->deductionScoreByNetCheckShTask($taskid,$do_aml['fstatus'],$reason);
            }
            
            $this->ajaxReturn(['code'=>0, 'msg'=>'状态更新成功']);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'状态更新失败，数据已处理']);
        }
    }

    /**
     * 任务数据退回
     * zw
     */
    public function PlanDataBackAction(){
        $id   = $this->P['id'];
        $taskid = $this->P['taskid'];

        if(empty($id) || empty($taskid)){
            $this->ajaxReturn(['code'=>1, 'msg'=>'参数有误']);
        }

        M()->execute('update tnetissue_customer set fstatus = 2 where ftaskid = '.$taskid.' and id = '.$id);
        M()->execute('delete from tnetissue_customer_flowlog where taskid = '.$id);
        M()->execute('delete from task_input_score where media_class = 13 and taskid = '.$id);
        $this->ajaxReturn(['code'=>0, 'msg'=>'更新完成']);
    }

    /**
     * 计划任务报表
     * zw
     */
    public function Task_Baobiao(){
        $fmediaid   = $this->P['fmediaid'];
        $fissuedate = $this->P['fissuedate'];
        $mtype      = $this->P['mtype'];

        $bbtitle = '';
        if(isset($fmediaid) && $fmediaid != ''){
            if(!empty($fmediaid)){
                $where_at['c.fid'] = $fmediaid;
                $fields1 = ',c.fmedianame';
            }else{
                $where_at['b.fmediacode'] = 0;
            }
        }
        if(!empty($fissuedate)){
            $where_at['a.fissuedate'] = ['between',[strtotime($fissuedate[0]),strtotime($fissuedate[1])]];
            $bbtitle = $bbtitle?$bbtitle.'，'.$fissuedate[0].'到'.$fissuedate[1]:$fissuedate[0].'到'.$fissuedate[1];
        }
        if(!empty($mtype)){
            $where_at['e.fid'] = $mtype;
            $fields1 = ',e.fclass';
        }
        $where_at['a.fdstatus'] = ['in',[2,4]];
        $where_at['b.fmediatype'] = 4;
        $where_at['b.fstatus'] = 1;
        $where_at['b.fmaincode'] = 0;

        $data_at = M('tapimedialist')
            ->field('g.alias,count(*) zcount,sum(case when f.fstatus = 8 then 1 else 0 end) thcount'.$fields1.$fields2)
            ->alias('a')
            ->join('tapimedia_map b on a.fmediaid = b.fid')
            ->join('tmedia c on b.fmediaid = c.fid','left')
            ->join('tmediaclass e on e.fid = c.fmediaclassid','left')
            ->join('tnetissue_customer f on a.fid = f.ftaskid')
            ->join('tnetissue_customer_flowlog i on f.id = i.taskid and i.logtype = 2')
            ->join('(select max(id) cid from tnetissue_customer_flowlog where logtype=2 group by taskid) h on i.id = h.cid')
            ->join('ad_input_user g on i.wx_id = g.wx_id')
            ->where($where_at)
            ->group('g.wx_id')
            ->select();
        if(!empty($data_at)){
            if(!empty($fields2)){
                $bbtitle = $bbtitle?$bbtitle.'，'.$data_at[0]['fclass'].'分类':$data_at[0]['fclass'];
            }
            if(!empty($fields1)){
                $bbtitle = $bbtitle?$bbtitle.'，'.$data_at[0]['fmedianame']:$data_at[0]['fmedianame'];
            }
            $bbtitle = $bbtitle?'（'.$bbtitle.'）':$bbtitle;
            $outdata['title'] = '互联网数据检查任务处理报表'.$bbtitle.'数据';//文档内部标题名称
            $outdata['datalie'] = [
              '序号'=>'key',
              '众包人员'=>'alias',
              '处理量'=>'zcount',
              '退回量'=>'thcount',
            ];
            $outdata['lists'] = $data_at;
            $ret = A('Agp/Goutxls')->outdata_xls($outdata);

            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'未查到可导出的数据。注意：只能导出已完成或已传送的计划数据']);
        }
    }

    /**
     * 计划任务报表
     * zw
     */
    public function Task_Mingxi(){
        $fmediaid   = $this->P['fmediaid'];
        $fissuedate = $this->P['fissuedate'];
        $mtype      = $this->P['mtype'];

        $bbtitle = '';
        if(isset($fmediaid) && $fmediaid != ''){
            if(!empty($fmediaid)){
                $where_at['c.fid'] = $fmediaid;
                $fields1 = ',c.fmedianame';
            }else{
                $where_at['b.fmediacode'] = 0;
            }
        }
        if(!empty($fissuedate)){
            $where_at['a.fissuedate'] = ['between',[strtotime($fissuedate[0]),strtotime($fissuedate[1])]];
            $bbtitle = $bbtitle?$bbtitle.'，'.$fissuedate[0].'到'.$fissuedate[1]:$fissuedate[0].'到'.$fissuedate[1];
        }
        if(!empty($mtype)){
            $where_at['e.fid'] = $mtype;
            $fields1 = ',e.fclass';
        }
        $where_at['a.fdstatus'] = ['in',[2,4]];
        $where_at['b.fmediatype'] = 4;
        $where_at['b.fstatus'] = 1;
        $where_at['b.fmaincode'] = 0;
        $where_at['f.fstatus'] = 8;

        $data_at = M('tapimedialist')
            ->field('f.*,i.wx_id i1wx,i.createtime i1time,i2.wx_id i2wx,i2.createtime i2time,i3.wx_id i3wx,i3.createtime i3time'.$fields2)
            ->alias('a')
            ->join('tapimedia_map b on a.fmediaid = b.fid')
            ->join('tmedia c on b.fmediaid = c.fid','left')
            ->join('tmediaclass e on e.fid = c.fmediaclassid','left')
            ->join('tnetissue_customer f on a.fid = f.ftaskid')
            ->join('tnetissue_customer_flowlog i on f.id = i.taskid and i.logtype = 2')
            ->join('(select max(id) cid from tnetissue_customer_flowlog where logtype=2 group by taskid) h on i.id = h.cid')
            ->join('tnettask_flowlog i2 on f.fnettaskid = i2.taskid and i2.tasklink = 1 and i2.logtype = 12')
            ->join('(select max(id) cid2 from tnettask_flowlog where tasklink=1 and logtype=12 group by taskid) h2 on i2.id = h2.cid2')
            ->join('tnettask_flowlog i3 on f.fnettaskid = i3.taskid and i3.tasklink = 2 and i3.logtype = 22')
            ->join('(select max(id) cid3 from tnettask_flowlog where tasklink=2 and logtype=22 group by taskid) h3 on i3.id = h3.cid3')
            ->where($where_at)
            ->order('f.id asc')
            ->select();

        if(!empty($data_at)){
            //查询众包人员名字
            $persondata1 = M('ad_input_user')->field('wx_id,alias')->where(['alias'=>['neq','']])->select();
            $persondata2 = [];
            foreach ($persondata1 as $key => $value) {
                $persondata2[$value['wx_id']] = $value['alias'];
            }

            //将人员名字填补到相应字段
            foreach ($data_at as $key => $value) {
                $data_at[$key]['luluuser'] = $persondata2[$value['i2wx']];//录入人员
                $data_at[$key]['lulutime'] = date('Y-m-d H:i:s',$value['i2time']);//录入时间
                $data_at[$key]['weipanuser'] = $persondata2[$value['i3wx']];//违判人员
                $data_at[$key]['weipantime'] = date('Y-m-d H:i:s',$value['i3time']);//违判时间
                $data_at[$key]['jianchauser'] = $persondata2[$value['i1wx']];//审核人员
                $data_at[$key]['jianchatime'] = date('Y-m-d H:i:s',$value['i1time']);//违判时间
                $data_at[$key]['checkuser'] = $persondata2[$value['check_wxid']];//审核人员
            }

            if(!empty($fields2)){
                $bbtitle = $bbtitle?$bbtitle.'，'.$data_at[0]['fclass'].'分类':$data_at[0]['fclass'];
            }
            if(!empty($fields1)){
                $bbtitle = $bbtitle?$bbtitle.'，'.$data_at[0]['publisher_na']:$data_at[0]['publisher_na'];
            }
            $bbtitle = $bbtitle?'（'.$bbtitle.'）':$bbtitle;
            $outdata['title'] = '互联网数据检查任务退回明细'.$bbtitle.'数据';//文档内部标题名称
            $outdata['datalie'] = [
              '序号'=>'key',
              '广告名'=>'title',
              '品牌'=>'brand',
              '广告主'=>'advertiser_na',
              '媒体名'=>'publisher_na',
              '媒体机构'=>'publisher_entname',
              '发现时间'=>'created_date',
              '落地页'=>'target_url',
              '原始截图'=>'shutpage',
              '压缩截图'=>'shutpage_thumb',
              '广告类别'=>'adtype',
              '违法程度'=>[
                'type'=>'zwif',
                'data'=>[
                  ['{adilltype} == 0','不违法'],
                  ['','违法'],
                ]
              ],
              '违法表现代码'=>'adilllaw',
              '违法表现'=>'adillegal',
              '违法内容'=>'adillcontent',
              '录入人员'=>'luluuser',
              '录入时间'=>'lulutime',
              '违法判定人'=>'weipanuser',
              '判定时间'=>'weipantime',
              '质检检员'=>'',
              '质检时间'=>'',
              '检查人员'=>'jianchauser',
              '检查时间'=>'jianchatime',
              '最终审核人'=>'checkuser',
              '审核时间'=>'check_time',
              '审核意见'=>'check_reason',
            ];
            $outdata['lists'] = $data_at;
            $ret = A('Agp/Goutxls')->outdata_xls($outdata);

            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'未查到可导出的数据。注意：只能导出已完成或已传送的计划数据']);
        }
    }

    /**
     * 查看任务数据列表
     * by zw
     */
    public function PlanDataList(){
        $taskid     = $this->P['taskid']?$this->P['taskid']:-1;
        $fadname    = $this->P['fadname'];
        $fadowner   = $this->P['fadowner'];
        $fstatus    = $this->P['fstatus'];
        $sttime     = $this->P['sttime'];
        $endtime    = $this->P['endtime'];
        $adilltype   = $this->P['adilltype'];
        $priority   = $this->P['priority'];
        $timeorder   = $this->P['timeorder'];
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 20);

        if($timeorder == 1){
            $orders = 'a.created_date desc';
        }elseif($timeorder == 2){
            $orders = 'a.created_date asc';
        }elseif($timeorder == 3){
            $orders = 'a.title asc,a.created_date asc';
        }

        if(!empty($fadname)){
            if(is_numeric($fadname)){
                $where_ncr['a.id'] = $fadname;
            }else{
                $where_ncr['a.title'] = ['like','%'.$fadname.'%'];
            }
        }
        if(!empty($fadowner)){
            $where_ncr['a.advertiser_na'] = ['like','%'.$fadowner.'%'];
        }
        if(!empty($fstatus)){
            if($fstatus == 8){
                $where_ncr['a.fstatus'] = ['in',[2,8]];
            }else{
                $where_ncr['a.fstatus'] = $fstatus;
            }
        }
        if(!empty($sttime) && !empty($endtime)){
            $where_ncr['a.issue_date'] = ['between',[$sttime,$endtime]];
        }
        if($adilltype != -1 && $adilltype != null){
            $where_ncr['a.adilltype'] = ['eq',$adilltype];
        }
        if($priority != -1 && $priority != null){
           $where_ncr['a.priority'] = $priority; 
        }

        $where_ncr['a.ftaskid'] = $taskid;
        $count_ncr = M('tnetissue_customer')
            ->alias('a')
            ->where($where_ncr)
            ->count();

        //截图中
        $where_jtz = $where_ncr;
        $where_jtz['a.fstatus'] = 1;
        $count_jtz = M('tnetissue_customer')
            ->alias('a')
            ->where($where_jtz)
            ->count();

        //待检查量
        $where_djc = $where_ncr;
        $where_djc['a.fstatus'] = ['in',[2,8]];
        $count_djc = M('tnetissue_customer')
            ->alias('a')
            ->join('tadclass b on a.adtype = b.fcode','left')
            ->where($where_djc)
            ->count();

        //待补录量
        $where_dbl = $where_ncr;
        $where_dbl['a.fstatus'] = 4;
        $count_dbl = M('tnetissue_customer')
            ->alias('a')
            ->join('tadclass b on a.adtype = b.fcode','left')
            ->where($where_dbl)
            ->count();

        $do_ncr = M('tnetissue_customer')
            ->field('id,title,advertiser_na,created_date,file_url,thumb_url_true,shutpage_thumb,shutpage,target_url,b.ffullname adclassname,a.width,a.height,a.fstatus,a.adilllaw,a.adillegal,a.adillcontent,a.check_status')
            ->alias('a')
            ->join('tadclass b on a.adtype = b.fcode','left')
            ->where($where_ncr)
            ->order($orders)
            ->page($pageIndex,$pageSize)
            ->select();

        $taskids = [];
        foreach ($do_ncr as $key => $value) {
            $taskids[] = $value['id'];
        }
        if(!empty($taskids)){
            $where_ncf['a.taskid'] = ['in',$taskids];
            $do_ncf = M('tnetissue_customer_flowlog') 
                ->alias('a')
                ->field('b.alias,a.taskid,a.logtype,from_unixtime(a.createtime) createtime,a.log content')
                ->join('ad_input_user b on a.wx_id = b.wx_id')
                ->where($where_ncf) 
                ->order('a.createtime desc')
                ->select();
            $user = [];
            foreach ($do_ncf as $key => $value) {
                if($value['logtype'] == 1){
                    $value['types'] = '领取补录任务';
                }elseif($value['logtype'] == 2){
                    $value['types'] = '提交补录任务';
                }elseif($value['logtype'] == 3){
                    $value['types'] = '退回补录任务';
                }elseif($value['logtype'] == 4){
                    $value['types'] = '重置补录任务';
                }elseif($value['logtype'] == 5){
                    $value['types'] = '检查通过';
                }elseif($value['logtype'] == 6){
                    $value['types'] = '检查不通过';
                }elseif($value['logtype'] == 7){
                    $value['types'] = '检查作废';
                }else{
                    $value['types'] = '未知';
                }
                $user[$value['taskid']][] = $value;
            }
            foreach ($do_ncr as $key => $value) {
                $do_ncr[$key]['log'] = $user[$value['id']];
                $do_ncr[$key]['adillcontent'] = htmlspecialchars_decode($value['adillcontent']);
            }
        }
        
        $this->ajaxReturn(['code'=>0, 'msg'=>'获取成功', 'count'=>$count_ncr,'jtzcount'=>$count_jtz,'djccount'=>$count_djc,'dblcount'=>$count_dbl, 'data'=>$do_ncr]);
    }

    /**
     * 数据优先级调整动作
     * by zw
     */
    public function PlanOrderAction(){
        $taskid     = $this->P['taskid']?$this->P['taskid']:-1;
        $selad      = $this->P['selad'];
        $fmedianame = $this->P['fmedianame'];
        $fadname    = $this->P['fadname'];
        $fadowner   = $this->P['fadowner'];
        $sttime     = $this->P['sttime'];
        $endtime    = $this->P['endtime'];
        $fstatus    = $this->P['fstatus'];
        $adilltype  = $this->P['adilltype'];
        $priority   = $this->P['priority'];
        $editpriority = $this->P['editpriority'];

        if(is_array($selad) && !empty($selad)){
            $where_ncr['id'] = ['in',$selad];
            $do_ncr = M('tnetissue_customer')
                ->where($where_ncr)
                ->save(['priority'=>$editpriority]);
        }else{
            if(!empty($fmedianame)){
                $where_ncr['publisher_entname'] = ['like','%'.$fmedianame.'%'];
            }
            if(!empty($fadname)){
                if(is_numeric($fadname)){
                    $where_ncr['a.id'] = $fadname;
                }else{
                    $where_ncr['a.title'] = ['like','%'.$fadname.'%'];
                }
            }
            if(!empty($fadowner)){
                $where_ncr['advertiser_na'] = ['like','%'.$fadowner.'%'];
            }
            if(!empty($fstatus)){
                if($fstatus == 8){
                    $where_ncr['a.fstatus'] = ['in',[2,8]];
                }else{
                    $where_ncr['a.fstatus'] = $fstatus;
                }
            }else{
                $where_ncr['fstatus'] = ['in',[1,2,8]];
            }
            if(!empty($sttime) && !empty($endtime)){
                $where_ncr['issue_date'] = ['between',[$sttime,$endtime]];
            }
            
            // else{
            //     $where_ncr['issue_date'] = ['between',[date('Y-m-01'),date('Y-m-d')]];
            // }
            if($adilltype != -1 && $adilltype != null){
                $where_ncr['adilltype'] = ['eq',$adilltype];
            }
            if($priority != -1 && $priority != null){
               $where_ncr['priority'] = $priority; 
            }
            $where_ncr['ftaskid'] = $taskid;
            $do_ncr = M('tnetissue_customer')
                ->alias('a')
                ->where($where_ncr)
                ->save(['priority'=>$editpriority]);
        }
        $this->ajaxReturn(['code'=>0, 'msg'=>'数据检查优先级设置完成']);
        
    }

    /**
     * 截图优先级调整动作
     * by zw
     */
    public function PlanCutOrderAction(){
        $taskid     = $this->P['taskid']?$this->P['taskid']:-1;
        $selad      = $this->P['selad'];
        $fmedianame = $this->P['fmedianame'];
        $fadname    = $this->P['fadname'];
        $fadowner   = $this->P['fadowner'];
        $sttime     = $this->P['sttime'];
        $endtime    = $this->P['endtime'];
        $adilltype  = $this->P['adilltype'];
        $priority   = $this->P['priority'];
        $editpriority = $this->P['editpriority'];
        $push_data = 0;

        if(is_array($selad) && !empty($selad)){
            $where_ncr['id'] = ['in',$selad];
            $do_ncr = M('tnetissue_customer')
                ->field('cutpriority,id')
                ->where($where_ncr)
                ->select();
            foreach ($do_ncr as $key => $value) {
                if((int)$value['cutpriority'] != (int)$editpriority){
                    $saveids[] = $value['id'];
                }
            }
            M('tnetissue_customer')->where(['id'=>['in',$saveids]])->save(['cutpriority'=>$editpriority]);
        }else{
            if(!empty($fmedianame)){
                $where_ncr['publisher_entname'] = ['like','%'.$fmedianame.'%'];
            }
            if(!empty($fadname)){
                if(is_numeric($fadname)){
                    $where_ncr['a.id'] = $fadname;
                }else{
                    $where_ncr['a.title'] = ['like','%'.$fadname.'%'];
                }
            }
            if(!empty($fadowner)){
                $where_ncr['advertiser_na'] = ['like','%'.$fadowner.'%'];
            }
            $where_ncr['fstatus'] = 1;
            if(!empty($sttime) && !empty($endtime)){
                $where_ncr['issue_date'] = ['between',[$sttime,$endtime]];
            }
            if($adilltype != -1 && $adilltype != null){
                $where_ncr['adilltype'] = ['eq',$adilltype];
            }
            if($priority != -1 && $priority != null){
               $where_ncr['priority'] = $priority; 
            }
            
            $where_ncr['ftaskid'] = $taskid;
            $do_ncr = M('tnetissue_customer')
                ->field('cutpriority,id')
                ->where($where_ncr)
                ->select();
            foreach ($do_ncr as $key => $value) {
                if((int)$value['cutpriority'] != (int)$editpriority){
                    $saveids[] = $value['id'];
                }
            }
            if(!empty($saveids)){
                $post_data['priority'] = $editpriority;
                $post_data['postid'] = implode(',', $saveids);
                $push_data = A('Api/ShIssue')->push_cutpriority($post_data);
                if(!empty($push_data)){
                    $finish_count = M('tnetissue_customer')->where(['id'=>['in',$saveids]])->save(['cutpriority'=>$editpriority]);
                }else{
                    $this->ajaxReturn(['code'=>1, 'msg'=>'设置失败','data'=>$push_data]);
                }
            }
        }
        $this->ajaxReturn(['code'=>0, 'msg'=>'截图优先级设置完成，数量：'.($finish_count?$finish_count:0),'sql'=>M('tnetissue_customer')->getlastsql()]);
        
    }

    /**
     * 返回广告类型树
     * zw
     */
    public function AdClassArr()
    {
        $data = M('tadclass')
            ->field('fcode, fpcode, fadclass')
            ->where(['fstate' => ['EQ', 1], 'fcode' => ['NEQ', 2301]])
            ->cache(true)
            ->select();
        $res = [];
        foreach ($data as $key => &$value) {
            if (strlen($value['fcode']) === 2){
                $res[] = [
                    'value' => $value['fcode'],
                    'label' => $value['fadclass'],
                ];
            }
        }
        foreach ($data as $key => &$value2) {
            $value2['value'] = $value2['fcode'];
            $value2['label'] = $value2['fadclass'];
            if (strlen($value2['fcode']) === 4){
                foreach ($res as &$item) {
                    if ($value2['fpcode'] === $item['value']){
                        $item['children'][] = $value2;
                        break;
                    }
                }
            }
        }
        $this->ajaxReturn(['code'=>0, 'msg'=>'获取成功','data'=>$res]);
    }

    /**
     * 计划添加数据列表
     * zw
     */
    public function PlanDataAddList(){
        session_write_close();
        set_time_limit(120);
        $taskid   = $this->P['taskid']?$this->P['taskid']:-1;
        $fadname    = $this->P['fadname'];
        $fadowner   = $this->P['fadowner'];
        $fstatus    = $this->P['fstatus'];
        $sttime     = $this->P['sttime'];
        $endtime    = $this->P['endtime'];
        $adilltype  = $this->P['adilltype'];
        $adtype     = $this->P['adtype'];
        $dataType   = $this->P['dataType']?$this->P['dataType']:0;
        $limit      = $this->P['limit']!=-1?$this->P['limit']:0;
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 20);
        
        if($dataType == 1){
            $this->PlanDataAddList2();//广告明细样本数据
        }

        $where_aml['a.fid'] = $taskid;
        $do_aml = M('tapimedialist')
            ->field('b.fmediaid,d.child_fmediaid,b.fcustomer,e.fname,a.fuserid,a.fissuedate')
            ->alias('a')
            ->join('tapimedia_map b on a.fmediaid = b.fid')
            ->join('tregion e on b.fcustomer = e.fid')
            ->join('(select group_concat(tapimedia_map.fmediaid) child_fmediaid,fmaincode,fmediatype from tapimedia_map,tmedia where tapimedia_map.fmediaid = tmedia.fid and fmaincode>0 group by fmaincode,fmediatype) d on b.fmediacode = d.fmaincode and b.fmediatype = d.fmediatype','left')
            ->where($where_aml)
            ->find();
        $medias = [];
        $medias[] = $do_aml['fmediaid'];
        if(!empty($do_aml['child_fmediaid']) && !empty($do_aml['fmediaid'])){
            $medias = array_merge($medias,explode(',', $do_aml['child_fmediaid']));
        }

        if(!empty($do_aml)){
            if($do_aml['fuserid'] == '22638a3131d0f0a7346b178fd29f939c'){
                $where_ncr['a.title'][] = ['notlike','百度推广%'];
                $where_ncr['LENGTH(adtype)'] = ['egt',4];
            }
            if(!empty($fadname)){
                $where_ncr['a.title'][] = ['like','%'.$fadname.'%'];
            }
            if(!empty($fadowner)){
                $where_ncr['a.advertiser_na'] = ['like','%'.$fadowner.'%'];
            }
            if(!empty($fstatus)){
                if($fstatus == 40){
                    $where_ncr['a.shutpage_thumb'] = ['neq',''];
                }elseif($fstatus == 8){
                    $where_ncr['a.fstatus'] = ['in',[2,8]];
                }else{
                    $where_ncr['a.fstatus'] = $fstatus;
                }
            }
            if(!empty($adtype)){
                $where_ncr['left(a.adtype,'.strlen($adtype).')'] = $adtype;
            }

            if(!empty($sttime) && !empty($endtime)){
                $where_ncr['a.issue_date'] = ['between',[$sttime,$endtime]];
            }else{
                $where_ncr['a.issue_date'] = ['BETWEEN',[date('Y-m-01',$do_aml['fissuedate']),date('Y-m-d',strtotime(date('Y-m-01',$do_aml['fissuedate']).' +1 month -1 day'))]];
            }
            if($adilltype != -1 && $adilltype != null){
                $where_ncr['a.adilltype'] = ['eq',$adilltype];
            }
            $where_ncr['a.ftaskid'] = 0;
            if(!empty($do_aml['fmediaid'])){
                $where_ncr['a.publisher_id'] = ['in',$medias];
                //数据总量
                $count_ncr = M('tnetissue_customer')
                    ->alias('a')
                    ->join('tadclass b on a.adtype = b.fcode','left')
                    ->where($where_ncr)
                    ->count();

                //限制最大数据量
                if(!empty($limit) && $count_ncr>$limit){
                    $count_ncr = $limit;
                }

                $do_ncr = M('tnetissue_customer')
                    ->alias('a')
                    ->field('a.id,a.title,a.advertiser_na,a.adilltype,b.ffullname adclassname,a.fstatus,a.created_date,a.file_url,a.shutpage,a.publisher_na')
                    ->join('tadclass b on a.adtype = b.fcode','left')
                    ->where($where_ncr)
                    ->page($pageIndex,$pageSize)
                    ->order('a.created_date desc')
                    ->select();

            }else{
                $tregionlevel = $this->get_tregionlevel($do_aml['fcustomer']);//客户所属地市级别
                if($tregionlevel == 1 || $tregionlevel == 2){
                    $midlen = 2;
                }elseif($tregionlevel == 4){
                    $midlen = 4;
                }else{
                    $midlen = 6;
                }

                //获取巡查媒体
                $where_amp['fmediatype'] = 4;
                $where_amp['fstatus'] = 1;
                $where_amp['fmediacode'] = ['neq',0];
                $where_amp['fcustomer'] = $do_aml['fcustomer'];
                $medias = M('tapimedia_map')
                    ->where($where_amp)
                    ->getField('fmediaid',true);
                $medias[] = 11000010008042;//排除万能钥匙
                $where_ncr['a.publisher_id'] = ['notin',$medias];

                $where_ncr['_string'] = 'advertiser_na like "'.$do_aml['fname'].'%" or c.fregion = '.substr($do_aml['fcustomer'], 0, $midlen);
                $count_ncr = M('tnetissue_customer')
                    ->alias('a')
                    ->join('tadclass b on a.adtype = b.fcode','left')
                    ->join('(select fissueid,left(fregion,'.$midlen.') fregion from tnetissue_customer_area where left(fregion,'.$midlen.') = '.substr($do_aml['fcustomer'], 0, $midlen).' group by fissueid) c on a.id = c.fissueid','left')
                    ->where($where_ncr)
                    ->count();

                //限制最大数据量
                if(!empty($limit) && $count_ncr>$limit){
                    $count_ncr = $limit;
                }

                $do_ncr = M('tnetissue_customer')
                    ->alias('a')
                    ->field('a.id,a.title,a.advertiser_na,a.adilltype,b.ffullname adclassname,a.fstatus,a.issue_date created_date,a.file_url,a.shutpage,a.publisher_na')
                    ->join('tadclass b on a.adtype = b.fcode','left')
                    ->join('(select fissueid,left(fregion,'.$midlen.') fregion from tnetissue_customer_area where left(fregion,'.$midlen.') = '.substr($do_aml['fcustomer'], 0, $midlen).' group by fissueid) c on a.id = c.fissueid','left')
                    ->where($where_ncr)
                    ->page($pageIndex,$pageSize)
                    ->order('a.created_date desc')
                    ->select();
            }

            $this->ajaxReturn(['code'=>0, 'msg'=>'获取成功', 'count'=>$count_ncr, 'data'=>$do_ncr]);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'获取失败，任务不存在']);
        }
        
    }

    /**
     * 计划添加数据列表
     * zw
     */
    public function PlanDataAddList2(){
        session_write_close();
        set_time_limit(120);
        $taskid   = $this->P['taskid']?$this->P['taskid']:-1;
        $fadname    = $this->P['fadname'];
        $fadowner   = $this->P['fadowner'];
        $fstatus    = $this->P['fstatus'];
        $sttime     = $this->P['sttime'];
        $endtime    = $this->P['endtime'];
        $adilltype  = $this->P['adilltype'];
        $adtype     = $this->P['adtype'];
        $dataType   = $this->P['dataType'];
        $limit      = $this->P['limit']!=-1?$this->P['limit']:0;
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 20);
        if(empty($outtype)){
            $limitstr = ' limit '.($pageIndex-1)*$pageSize.','.$pageSize;
        }
        $where_str = '1=1';

        $where_aml['a.fid'] = $taskid;
        $do_aml = M('tapimedialist')
            ->field('b.fmediaid,d.child_fmediaid,b.fcustomer,e.fname,a.fuserid,a.fissuedate')
            ->alias('a')
            ->join('tapimedia_map b on a.fmediaid = b.fid')
            ->join('tregion e on b.fcustomer = e.fid')
            ->join('(select group_concat(tapimedia_map.fmediaid) child_fmediaid,fmaincode,fmediatype from tapimedia_map,tmedia where tapimedia_map.fmediaid = tmedia.fid and fmaincode>0 group by fmaincode,fmediatype) d on b.fmediacode = d.fmaincode and b.fmediatype = d.fmediatype','left')
            ->where($where_aml)
            ->find();
        $medias = [];
        $medias[] = $do_aml['fmediaid'];
        if(!empty($do_aml['child_fmediaid']) && !empty($do_aml['fmediaid'])){
            $medias = array_merge($medias,explode(',', $do_aml['child_fmediaid']));
        }

        if(!empty($do_aml)){
            $where_str .= ' and a.marin_major_key not in(select b.marin_major_key from tnetissue_customer a,tnetissue b where a.id = b.major_key and a.ftaskid = '.$taskid.')';
            if($do_aml['fuserid'] == '22638a3131d0f0a7346b178fd29f939c'){
                $where_str .= ' and LENGTH(a.fadclasscode) >= 4';
                $where_str .= ' and a.fadname not like "百度推广%"';
            }
            if(!empty($fadname)){
                $where_str .= ' and a.fadname like "%'.$fadname.'%"';
            }
            if(!empty($fadowner)){
                $where_str .= ' and c.fname like "%'.$fadowner.'%"';
            }
            if(!empty($fstatus)){
                if($fstatus == 1){
                    $where_str .= ' and a.net_snapshot is null ';
                }elseif($fstatus == 2){
                    $where_str .= ' and a.net_snapshot is not null ';
                }
            }
            if(!empty($adtype)){
                $where_str .= ' and left(a.fadclasscode,'.strlen($adtype).') = '.$adtype;
            }
            if(!empty($sttime) && !empty($endtime)){
                $where_str .= ' and b.fissuedate between '.strtotime($sttime).' and '.strtotime($endtime);
            }else{
                $where_str .= ' and b.fissuedate = '.$do_aml['fissuedate'];
                // $where_str .= ' and b.fissuedate between '.strtotime(date('Y-m-01',$do_aml['fissuedate'])).' and '.strtotime(date('Y-m-01',$do_aml['fissuedate']).' +1 month -1 day');
            }
            if($adilltype != -1 && $adilltype != null){
                $where_str .= ' and a.fillegaltypecode = '.$adilltype;
            }
            
            if(!empty($do_aml['fmediaid'])){
                $where_str .= ' and a.fmediaid in ('.implode(',', $medias).')';

                $countsql = 'select count(*) acount from (
                        select a.major_key id
                        from tnetissue a
                        inner join tnetissueputlog b on b.tid=a.major_key and a.finputstate=2
                        inner join tadowner c on a.fadowner = c.fid 
                        inner join tadclass d on a.fadclasscode = d.fcode 
                        inner join tmedia e on a.fmediaid = e.fid
                        where '.$where_str.' 
                        group by a.major_key
                    ) aa';

                $count = M()->query($countsql);
                $count_ncr = $count[0]['acount'];
                //限制最大数据量
                if(!empty($limit) && $count_ncr>$limit){
                    $count_ncr = $limit;
                }
              
                $datasql = '
                    select * from (
                        select d.ffullname adclassname,a.fillegaltypecode adilltype,c.fname advertiser_na,b.net_created_date,a.net_url file_url,(case when net_snapshot is null then 1 else 2 end) fstatus,a.major_key id,e.fmedianame publisher_na,net_snapshot shutpage,a.fadname title
                        from tnetissue a
                        inner join tnetissueputlog b on b.tid=a.major_key and a.finputstate=2
                        inner join tadowner c on a.fadowner = c.fid 
                        inner join tadclass d on a.fadclasscode = d.fcode 
                        inner join tmedia e on a.fmediaid = e.fid
                        where '.$where_str.' 
                        group by a.major_key
                    ) aa order by net_created_date desc'.$limitstr;
                $do_ncr = M()->query($datasql);

                foreach ($do_ncr as $key => $value) {
                   $do_ncr[$key]['created_date'] = date('Y-m-d H:i:s',$value['net_created_date']/1000);
                }
            }else{
                $tregionlevel = $this->get_tregionlevel($do_aml['fcustomer']);//客户所属地市级别
                if($tregionlevel == 1 || $tregionlevel == 2){
                    $midlen = 2;
                }elseif($tregionlevel == 4){
                    $midlen = 4;
                }else{
                    $midlen = 6;
                }

                //获取巡查媒体
                $where_amp['fmediatype'] = 4;
                $where_amp['fstatus'] = 1;
                $where_amp['fmediacode'] = ['neq',0];
                $where_amp['fcustomer'] = $do_aml['fcustomer'];
                $medias = M('tapimedia_map')
                    ->where($where_amp)
                    ->getField('fmediaid',true);
                $medias[] = 11000010008042;//排除万能钥匙
                $where_str .= ' and a.fmediaid not in ('.implode(',', $medias).')';
                $where_str .= ' and c.fname like "'.$do_aml['fname'].'%" or e.media_region_id = '.substr($do_aml['fcustomer'], 0, $midlen);

                $countsql = 'select count(*) acount from (
                        select a.major_key id
                        from tnetissue a
                        inner join tnetissueputlog b on b.tid=a.major_key and a.finputstate=2
                        inner join tadowner c on a.fadowner = c.fid 
                        inner join tadclass d on a.fadclasscode = d.fcode 
                        inner join tmedia e on a.fmediaid = e.fid
                        where '.$where_str.' 
                        group by a.major_key
                    ) aa';
                $count = M()->query($countsql);
                $count_ncr = $count[0]['acount'];

                //限制最大数据量
                if(!empty($limit) && $count_ncr>$limit){
                    $count_ncr = $limit;
                }
              
                $datasql = '
                    select * from (
                        select d.ffullname adclassname,a.fillegaltypecode adilltype,c.fname advertiser_na,b.net_created_date,a.net_url file_url,(case when net_snapshot is null then 1 else 2 end) fstatus,a.major_key id,e.fmedianame publisher_na,net_snapshot shutpage,a.fadname title
                        from tnetissue a
                        inner join tnetissueputlog b on b.tid=a.major_key and a.finputstate=2
                        inner join tadowner c on a.fadowner = c.fid 
                        inner join tadclass d on a.fadclasscode = d.fcode 
                        inner join tmedia e on a.fmediaid = e.fid
                        where '.$where_str.' 
                        group by a.major_key
                    ) aa order by net_created_date desc'.$limitstr;
                $do_ncr = M()->query($datasql);
                foreach ($do_ncr as $key => $value) {
                   $do_ncr[$key]['created_date'] = date('Y-m-d H:i:s',$value['net_created_date']/1000);
                }
            }
            $this->ajaxReturn(['code'=>0, 'msg'=>'获取成功', 'count'=>$count_ncr, 'data'=>$do_ncr]);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'获取失败，任务不存在']);
        }
        
    }

    private function get_tregionlevel($area){
        //判断机构级别，0区级，10市级，20省级，30国家级
        $region_id_rtrim = rtrim($area,'00');//去掉地区后面两位0，判断区
        $region_id_rtrim = rtrim($region_id_rtrim,'000');//去掉后面三位0，判断全国
        $region_id_rtrim = rtrim($region_id_rtrim,'00');//去掉后面两位0，判断市
        $tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位
        if($tregion_len == 3){
            $tregion_len = 4;
        }elseif($tregion_len == 5){
            $tregion_len = 6;
        }
        return $tregion_len;
    }

    /**
     * 添加计划数据
     * zw
     */
    public function PlanDataAddAction(){
        session_write_close();
        set_time_limit(120);
        $taskid     = $this->P['taskid']?$this->P['taskid']:-1;
        $selad      = $this->P['selad'];
        $fadname    = $this->P['fadname'];
        $fadowner   = $this->P['fadowner'];
        $fstatus    = $this->P['fstatus'];
        $sttime     = $this->P['sttime'];
        $endtime    = $this->P['endtime'];
        $adilltype  = $this->P['adilltype'];
        $adtype     = $this->P['adtype'];
        $dataType   = $this->P['dataType']?$this->P['dataType']:0;
        $limit      = $this->P['limit'] != -1?$this->P['limit']:0;

        if($dataType == 1){
            $this->PlanDataAddAction2();
        }

        $where_aml['a.fid'] = $taskid;
        $do_aml = M('tapimedialist')
            ->field('b.fmediaid,d.child_fmediaid,b.fcustomer,e.fname,a.fuserid,a.priority,a.fissuedate')
            ->alias('a')
            ->join('tapimedia_map b on a.fmediaid = b.fid')
            ->join('tregion e on b.fcustomer = e.fid')
            ->join('(select group_concat(tapimedia_map.fmediaid) child_fmediaid,fmaincode,fmediatype from tapimedia_map,tmedia where tapimedia_map.fmediaid = tmedia.fid and fmaincode>0 group by fmaincode,fmediatype) d on b.fmediacode = d.fmaincode and b.fmediatype = d.fmediatype','left')
            ->where($where_aml)
            ->find();
        $medias = [];
        $medias[] = $do_aml['fmediaid'];
        if(!empty($do_aml['child_fmediaid'])){
            $medias = array_merge($medias,explode(',', $do_aml['child_fmediaid']));
        }

        if(!empty($do_aml)){
            
            if(is_array($selad) && !empty($selad)){//针对某几条广告加入到计划
                $where_ncr['ftaskid'] = 0;
                $where_ncr['id'] = ['in',$selad];
            }else{//针对搜索条件得出的广告记录添加到计划
                if(empty($sttime) || empty($endtime)){
                    $this->ajaxReturn(['code'=>1, 'msg'=>'添加所有操作，时间段必选']);
                }

                if($do_aml['fuserid'] == '22638a3131d0f0a7346b178fd29f939c'){
                    $where_ncr['title'] = ['notlike','百度推广%'];
                    $where_ncr['LENGTH(adtype)'] = ['egt',4];
                }

                if(!empty($fadname)){
                    $where_ncr['title'] = ['like','%'.$fadname.'%'];
                }
                if(!empty($fadowner)){
                    $where_ncr['advertiser_na'] = ['like','%'.$fadowner.'%'];
                }
                if(!empty($fstatus)){
                    if($fstatus == 40){
                        $where_ncr['shutpage_thumb'] = ['neq',''];
                    }elseif($fstatus == 8){
                        $where_ncr['fstatus'] = ['in',[2,8]];
                    }else{
                        $where_ncr['fstatus'] = $fstatus;
                    }
                }
                if(!empty($adtype)){
                    $where_ncr['left(adtype,2)'] = $adtype;
                }
                if(!empty($sttime) && !empty($endtime)){
                    $where_ncr['issue_date'] = ['between',[$sttime,$endtime]];
                }
                if($adilltype != -1 && $adilltype != null){
                    $where_ncr['adilltype'] = ['eq',$adilltype];
                }
                $where_ncr['ftaskid'] = 0;

                if(!empty($do_aml['fmediaid'])){
                    $where_ncr['publisher_id'] = ['in',$medias];
                    if(!empty($limit)){
                        $ids = M('tnetissue_customer')->where($where_ncr)->page(0,$limit)->order('created_date desc')->getField('id',true);
                        $where_ncr = [];
                        $where_ncr['id'] = ['in',$ids];
                    }
                }else{
                    $tregionlevel = $this->get_tregionlevel($do_aml['fcustomer']);//客户所属地市级别
                    if($tregionlevel == 1 || $tregionlevel == 2){
                        $midlen = 2;
                    }elseif($tregionlevel == 4){
                        $midlen = 4;
                    }else{
                        $midlen = 6;
                    }

                    //获取重要媒体
                    $where_amp['fmediatype'] = 4;
                    $where_amp['fstatus'] = 1;
                    $where_amp['fmediacode'] = ['neq',0];
                    $where_amp['fcustomer'] = $do_aml['fcustomer'];
                    $medias = M('tapimedia_map')
                        ->where($where_amp)
                        ->getField('fmediaid',true);
                    $medias[] = 11000010008042;//排除万能钥匙
                    $where_ncr['publisher_id'] = ['notin',$medias];

                    $where_ncr['_string'] = 'advertiser_na like "'.$do_aml['fname'].'%" or c.fregion = '.substr($do_aml['fcustomer'], 0, $midlen);
                    if(empty($limit)){
                        $ids = M('tnetissue_customer')
                            ->join('(select fissueid,left(fregion,'.$midlen.') fregion from tnetissue_customer_area where left(fregion,'.$midlen.') = '.substr($do_aml['fcustomer'], 0, $midlen).' group by fissueid) c on tnetissue_customer.id = c.fissueid','left')
                            ->where($where_ncr)
                            ->order('created_date desc')
                            ->getField('id',true);
                    }else{
                        $ids = M('tnetissue_customer')
                            ->join('(select fissueid,left(fregion,'.$midlen.') fregion from tnetissue_customer_area where left(fregion,'.$midlen.') = '.substr($do_aml['fcustomer'], 0, $midlen).' group by fissueid) c on tnetissue_customer.id = c.fissueid','left')
                            ->where($where_ncr)
                            ->page(0,$limit)
                            ->order('created_date desc')
                            ->getField('id',true);
                    }
                    $where_ncr = [];
                    $where_ncr['id'] = ['in',$ids];

                }
                
            }
            $do_ncr = M('tnetissue_customer')
                ->where($where_ncr)
                ->save(['ftaskid'=>$taskid,'priority'=>$do_aml['priority']]);
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功添加'.$do_ncr.'条']);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'获取失败，任务不存在']);
        }
    }

    /**
     * 添加计划数据
     * zw
     */
    public function PlanDataAddAction2(){
        session_write_close();
        set_time_limit(120);
        $taskid     = $this->P['taskid']?$this->P['taskid']:-1;
        $selad      = $this->P['selad'];
        $fadname    = $this->P['fadname'];
        $fadowner   = $this->P['fadowner'];
        $fstatus    = $this->P['fstatus'];
        $sttime     = $this->P['sttime'];
        $endtime    = $this->P['endtime'];
        $adilltype  = $this->P['adilltype'];
        $adtype     = $this->P['adtype'];
        $dataType   = $this->P['dataType'];
        $limit      = $this->P['limit'] != -1?$this->P['limit']:0;
        if(!empty($limit)){
            $limitstr = ' limit 0,'.$limit;
        }
        $where_str = '1=1';

        $where_aml['a.fid'] = $taskid;
        $do_aml = M('tapimedialist')
            ->field('b.fmediaid,d.child_fmediaid,b.fcustomer,e.fname,a.fuserid,a.priority,a.fissuedate')
            ->alias('a')
            ->join('tapimedia_map b on a.fmediaid = b.fid')
            ->join('tregion e on b.fcustomer = e.fid')
            ->join('(select group_concat(tapimedia_map.fmediaid) child_fmediaid,fmaincode,fmediatype from tapimedia_map,tmedia where tapimedia_map.fmediaid = tmedia.fid and fmaincode>0 group by fmaincode,fmediatype) d on b.fmediacode = d.fmaincode and b.fmediatype = d.fmediatype','left')
            ->where($where_aml)
            ->find();
        $medias = [];
        $medias[] = $do_aml['fmediaid'];
        if(!empty($do_aml['child_fmediaid'])){
            $medias = array_merge($medias,explode(',', $do_aml['child_fmediaid']));
        }

        if(!empty($do_aml)){
            if(is_array($selad) && !empty($selad)){//针对某几条广告加入到计划
                $ids = $selad;
            }else{//针对搜索条件得出的广告记录添加到计划
                if(empty($sttime) || empty($endtime)){
                    $this->ajaxReturn(['code'=>1, 'msg'=>'添加所有操作，时间段必选']);
                }

                $where_str .= ' and a.marin_major_key not in(select b.marin_major_key from tnetissue_customer a,tnetissue b where a.id = b.major_key and a.ftaskid = '.$taskid.')';
                if($do_aml['fuserid'] == '22638a3131d0f0a7346b178fd29f939c'){
                    $where_str .= ' and LENGTH(a.fadclasscode) >= 4';
                    $where_str .= ' and a.fadname not like "百度推广%"';
                }
                if(!empty($fadname)){
                    $where_str .= ' and a.fadname like "%'.$fadname.'%"';
                }
                if(!empty($fadowner)){
                    $where_str .= ' and c.fname like "%'.$fadowner.'%"';
                }
                if(!empty($fstatus)){
                    if($fstatus == 1){
                        $where_str .= ' and a.net_snapshot is null ';
                    }elseif($fstatus == 2){
                        $where_str .= ' and a.net_snapshot is not null ';
                    }
                }
                if(!empty($adtype)){
                    $where_str .= ' and left(a.fadclasscode,'.strlen($adtype).') = '.$adtype;
                }
                if(!empty($sttime) && !empty($endtime)){
                    $where_str .= ' and b.fissuedate between '.strtotime($sttime).' and '.strtotime($endtime);
                }else{
                    $where_str .= ' and b.fissuedate = '.$do_aml['fissuedate'];
                    // $where_str .= ' and b.fissuedate between '.strtotime(date('Y-m-01',$do_aml['fissuedate'])).' and '.strtotime(date('Y-m-01',$do_aml['fissuedate']).' +1 month -1 day');
                }
                if($adilltype != -1 && $adilltype != null){
                    $where_str .= ' and a.fillegaltypecode = '.$adilltype;
                }

                if(!empty($do_aml['fmediaid'])){
                    $where_str .= ' and a.fmediaid in ('.implode(',', $medias).')';
                    $datasql = '
                    select * from (
                        select a.major_key id
                        from tnetissue a
                        inner join tnetissueputlog b on b.tid=a.major_key and a.finputstate=2
                        inner join tadowner c on a.fadowner = c.fid 
                        inner join tadclass d on a.fadclasscode = d.fcode 
                        inner join tmedia e on a.fmediaid = e.fid
                        where '.$where_str.' 
                        group by a.major_key
                    ) a '.$limitstr;

                    $do_ncr = M()->query($datasql);
                    $ids = array_column($do_ncr, 'id');
                }else{
                    $tregionlevel = $this->get_tregionlevel($do_aml['fcustomer']);//客户所属地市级别
                    if($tregionlevel == 1 || $tregionlevel == 2){
                        $midlen = 2;
                    }elseif($tregionlevel == 4){
                        $midlen = 4;
                    }else{
                        $midlen = 6;
                    }

                    //获取巡查媒体
                    $where_amp['fmediatype'] = 4;
                    $where_amp['fstatus'] = 1;
                    $where_amp['fmediacode'] = ['neq',0];
                    $where_amp['fcustomer'] = $do_aml['fcustomer'];
                    $medias = M('tapimedia_map')
                        ->where($where_amp)
                        ->getField('fmediaid',true);
                    $medias[] = 11000010008042;//排除万能钥匙
                    $where_str .= ' and a.fmediaid not in ('.implode(',', $medias).')';
                    $where_str .= ' and c.fname like "'.$do_aml['fname'].'%" or e.media_region_id = '.substr($do_aml['fcustomer'], 0, $midlen);

                    $datasql = '
                    select * from (
                        select a.major_key id
                        from tnetissue a
                        inner join tnetissueputlog b on b.tid=a.major_key and a.finputstate=2
                        inner join tadowner c on a.fadowner = c.fid 
                        inner join tadclass d on a.fadclasscode = d.fcode 
                        inner join tmedia e on a.fmediaid = e.fid
                        where '.$where_str.' 
                        group by a.major_key
                    ) a '.$limitstr;
                    $do_ncr = M()->query($datasql);
                    $ids = array_column($do_ncr, 'id');
                }
                
            }
            $ret = A('Api/ShIssue')->push_netsample2($ids,$taskid);
            if(empty($ret['code'])){
                $do_ncr = $ret['data'];
            }else{
                $do_ncr = 0;
            }
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功添加'.$do_ncr.'条']);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'获取失败，任务不存在']);
        }
    }

    /**
     * 互联网任务列表
     * zw
     */
    public function NetTaskList(){
        $sttime     = $this->P['sttime'];
        $endtime    = $this->P['endtime'];
        $fmediaid   = $this->P['fmediaid'];
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 20);

        if(empty($sttime) || empty($endtime)){
            $this->ajaxReturn(['code'=>1, 'msg'=>'请选择时间区间']);
        }
        $where_aml['b.fstatus'] = 1;
        $where_aml['b.fmediatype'] = 4;
        $where_aml['b.fmediaid'] = ['neq',0];
        if(!empty($fmediaid)){
            $where_aml['b.fmediaid'] = $fmediaid;
        }
        $do_aml = M('tapimedia_map')
            ->field('b.fmediaid,d.child_fmediaid')
            ->alias('b')
            ->join('(select group_concat(tapimedia_map.fmediaid) child_fmediaid,fmaincode,fmediatype from tapimedia_map,tmedia where tapimedia_map.fmediaid = tmedia.fid and fmaincode>0 group by fmaincode,fmediatype) d on b.fmediacode = d.fmaincode and b.fmediatype = d.fmediatype','left')
            ->where($where_aml)
            ->select();
        $medias = [];
        foreach ($do_aml as $key => $value) {
            $medias[] = $value['fmediaid'];
            if(!empty($value['child_fmediaid'])){
                $medias = array_merge($medias,explode(',', $value['child_fmediaid']));
            }
        }
        
        $where_nt['a.issue_date'] = ['between',[$sttime,$endtime]];
        $where_nt['a.media_id'] = ['in',$medias];
        $where_nt['a.task_state'] = 0;
        $count_nt = M('tnettask')
            ->alias('a')
            ->join('tnetissue c on c.major_key = a.major_key')
            ->where($where_nt)
            ->count();

        $do_nt = M('tnettask')
            ->alias('a')
            ->field('a.taskid,c.fadname,a.net_created_date,d.fmedianame media_name,a.fillegaltypecode,c.fadowner,a.priority,c.net_original_url,c.net_target_url')
            ->join('tnetissue c on c.major_key = a.major_key')
            ->join('tmedia d on c.fmediaid = d.fid')
            ->where($where_nt)
            ->order('a.net_created_date desc')
            ->page($pageIndex,$pageSize)
            ->select();
        foreach ($do_nt as $key => $value) {
            $do_nt[$key]['net_created_date'] = date('Y-m-d H:i:s',$value['net_created_date']);
        }
        $this->ajaxReturn(['code'=>0, 'msg'=>'获取成功','count'=>$count_nt,'data'=>$do_nt]);
    }

    /**
     * 互联网任务优先级调整动作
     * zw
     */
    public function NetTaskPriority(){
        $selad      = $this->P['selad'];
        $sttime     = $this->P['sttime'];
        $endtime    = $this->P['endtime'];
        $fmediaid    = $this->P['fmediaid'];
        $editpriority    = $this->P['editpriority'];

        if(!empty($selad) && is_array($selad)){//对选中的修改
            $where_nt['taskid'] = ['in',$selad];
        }else{//对条件查询的进行修改
            if(empty($sttime) || empty($endtime)){
                $this->ajaxReturn(['code'=>1, 'msg'=>'请选择时间区间']);
            }
            if(!empty($fmediaid)){
                $where_aml['b.fmediaid'] = $fmediaid;
            }
            $where_aml['b.fstatus'] = 1;
            $where_aml['b.fmediatype'] = 4;
            $where_aml['b.fmediaid'] = ['neq',0];
            $do_aml = M('tapimedia_map')
                ->field('b.fmediaid,d.child_fmediaid')
                ->alias('b')
                ->join('(select group_concat(tapimedia_map.fmediaid) child_fmediaid,fmaincode,fmediatype from tapimedia_map,tmedia where tapimedia_map.fmediaid = tmedia.fid and fmaincode>0 group by fmaincode,fmediatype) d on b.fmediacode = d.fmaincode and b.fmediatype = d.fmediatype','left')
                ->where($where_aml)
                ->select();
            $medias = [];
            foreach ($do_aml as $key => $value) {
                $medias[] = $value['fmediaid'];
                if(!empty($value['child_fmediaid'])){
                    $medias = array_merge($medias,explode(',', $value['child_fmediaid']));
                }
            }
            $where_nt['issue_date'] = ['between',[$sttime,$endtime]];
            $where_nt['media_id'] = ['in',$medias];
            $where_nt['task_state'] = 0;
        }

        $save_nt = M('tnettask')
            ->where($where_nt)
            ->save(['priority'=>$editpriority]);
        if(!empty($save_nt)){
            $this->ajaxReturn(['code'=>0, 'msg'=>'修改成功']);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'修改失败，任务不存在']);
        }
    }

    /**
     * 获取互联网数据检查任务(目前仅检查上海互联网数据)
     */
    public function getNetCheckTask(){
        $getCount = 10; //每次请求获取任务尝试次数限制
        $tableName = 'tnetissue_customer';
        $flowTableName = 'tnetissue_customer_flowlog';
        while($getCount > 0){
            // 领取任务
            $where = [
                'fstatus' => 5,
                'wx_id' => session('wx_id'),
            ];
            $samRow = M($tableName)
                ->master(true)
                ->where($where)
                ->order('priority DESC,issue_date ASC')
                ->find();
            if(!empty($samRow)){
                // 是否超时
                $cur_overtime = time() - $this->overtime;
                if($samRow['modifytime'] <= $cur_overtime){
                    // 更新重置任务状态为初始状态
                    $saveData = [
                        'wx_id' => 0,
                        'fstatus' => 2,
                        'modifytime' => time(),
                    ];
                    $saveRes = M($tableName)
                        ->where(['id'=>$samRow['id']])
                        ->save($saveData);
                    // 流程日志
                    $flowData = [
                        'taskid'     => $samRow['id'],
                        'wx_id'      => $samRow['wx_id'],
                        'logtype'    => 4,
                        'log'        => '超时重置任务',
                        'taskinfo'   => json_encode($row),
                        'creator'    => 'API_getNetCheckTask',
                        'createtime' => time(),
                    ];
                    $flowRes = M($flowTableName)->add($flowData);
                    // 获取到已超时的任务时，重置任务后，继续接下来分配及领取新任务
                    continue;
                }
                // 更新流程
                $flowData = [
                    'taskid'     => $samRow['id'],
                    'wx_id'      => session('wx_id'),
                    'logtype'    => 1,
                    'log'        => '领取任务',
                    'taskinfo'   => json_encode($samRow),
                    'creator'    => session('wx_id'),
                    'createtime' => time(),
                ];
                $flowRes = M($flowTableName)->add($flowData);
                if (!$flowRes){
                    $this->error = '流程更新失败';
                    $this->ajaxReturn(['code'=>1, 'msg'=>$this->error]);
                }
                // 任务数据
                $adclassname = M('tadclass')->cache(true,120)->where(['fcode' => $samRow['adtype']])->getField('ffullname');
                $count = M('tnetissue_customer')->where(['priority'=>['EGT',0],'fstatus'=>['IN',[4,5]]])->count();
                $urgentcount = M('tnetissue_customer')->where(['priority'=>['neq',0],'fstatus'=>4])->count();
                $result = [
                    'id'                => $samRow['id'],
                    'title'             => $samRow['title'],
                    'brand'             => $samRow['brand'],
                    'adtype'            => $samRow['adtype'],
                    'adtypename'        => $adclassname,
                    'advertiser_id'     => $samRow['advertiser_id'],
                    'advertiser_na'     => $samRow['advertiser_na'],
                    'publisher_entname' => $samRow['publisher_entname'],
                    'publisher_na'      => $samRow['publisher_na'],
                    'file_url'          => $samRow['file_url'],
                    'target_url'        => $samRow['target_url'],
                    'original_url'      => $samRow['original_url'],
                    'adilltype'         => $samRow['adilltype'],
                    'adilllaw'          => $samRow['adilllaw'],
                    'adillegal'         => $samRow['adillegal'],
                    'adillcontent'      => $samRow['adillcontent'],
                    'shutpage'          => $samRow['shutpage'],
                    'shutpage_thumb'    => $samRow['shutpage_thumb'],
                    'created_date'      => $samRow['created_date'],
                    'modifytime'        => $samRow['modifytime'],
                    'type'              => $samRow['type'],
                    'fversion'          => $samRow['fversion'],
                    'check_reason'      => $samRow['check_reason'] ? $samRow['check_reason'] : '',
                    'overtime'          => $this->overtime,
                    'count'             => $count,
                    'urgentcount'       => $urgentcount,
                ];
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'overtime'=>$this->overtime, 'data'=>$result]);
            }else{
                // 分配任务
                $whereTask = [
                    'fstatus' => 4,
                    'ftaskid' => ['GT',0],
                    // 'wx_id'   => ['NEQ',session('wx_id')],
                ];
                $bindInfo = [
                    'fstatus'    => 5,
                    'wx_id'      => session('wx_id'),
                    'modifytime' => time(),
                ];
                $bindTaskList = M($tableName)
                    ->master(true)
                    ->where($whereTask)
                    ->order('priority DESC,issue_date ASC')
                    ->limit(1)
                    ->save($bindInfo);
            }
            $getCount--;
        }
        $this->ajaxReturn(['code'=>1, 'msg'=>'暂无任务']);
    }

    /**
     * 获取互联网数据检查任务领取时间，以消除前后端定时回收时间误差
     */
    public function freshNetCheckTask(){
        $id = $this->P['id'];
        if(!empty($id)){
            $taskInfo = M('tnetissue_customer')
                ->field('id,wx_id,fstatus,modifytime')
                ->where(['id'=>$id])
                ->find();
            if(!empty($taskInfo)){
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'data'=>$taskInfo]);
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'未找到任务']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 获取历史记录列表
     */
    public function getHistoryList(){
        $wx_id      = session('wx_id');
        $title      = $this->P['title'];
        $date       = $this->P['date'];
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize'] : 200);
        $resultList = [];
        if(!empty($wx_id)){
            $where = [
                'a.wx_id'    => $wx_id,
                'a.logtype'  => 2,
                'c.fdstatus' => ['NOT IN',[2,4]],
            ];
            if(!empty($title)){
                if(is_numeric($title)){
                    $where['a.taskid'] = ['LIKE','%'.$title.'%'];
                }else{
                    $where['b.title'] = ['LIKE','%'.$title.'%'];
                }
            }
            if(!empty($date)){
                $dateArr = explode(',',$date);
                $dateS = strtotime($dateArr[0]);
                $dateE = strtotime($dateArr[1])+86399;
                $where['a.createtime'] = ['BETWEEN',[$dateS,$dateE]];
            }
            $count = M('tnetissue_customer_flowlog')
                ->alias('a')
                ->join('tnetissue_customer b ON b.id = a.taskid')
                ->join('tapimedialist c on c.fid = b.ftaskid')
                ->where($where)
                ->count();
            $list = M('tnetissue_customer_flowlog')
                ->alias('a')
                ->field('a.createtime,b.id,b.title,b.brand,b.adtype,b.advertiser_id,b.advertiser_na,b.publisher_entname,b.file_url,b.target_url,b.original_url,b.adilltype,b.adilllaw,b.adillegal,b.adillcontent,b.shutpage,b.shutpage_thumb,b.created_date,b.modifytime,b.type,b.check_reason')
                ->join('tnetissue_customer b ON b.id = a.taskid')
                ->join('tapimedialist c on c.fid = b.ftaskid')
                ->where($where)
                ->order('a.createtime DESC')
                ->page($pageIndex,$pageSize)
                ->select();
            if(!empty($list)){
                foreach($list as $key => $samRow){
                    // 任务数据
                    $adclassname = M('tadclass')->cache(true,120)->where(['fcode' => $samRow['adtype']])->getField('ffullname');
                    $resultList[] = [
                        'id'                => $samRow['id'],
                        'title'             => $samRow['title'],
                        'brand'             => $samRow['brand'],
                        'adtype'            => $samRow['adtype'],
                        'adtypename'        => $adclassname,
                        'advertiser_id'     => $samRow['advertiser_id'],
                        'advertiser_na'     => $samRow['advertiser_na'],
                        'publisher_entname' => $samRow['publisher_entname'],
                        'file_url'          => $samRow['file_url'],
                        'target_url'        => $samRow['target_url'],
                        'original_url'      => $samRow['original_url'],
                        'adilltype'         => $samRow['adilltype'],
                        'adilllaw'          => $samRow['adilllaw'],
                        'adillegal'         => $samRow['adillegal'],
                        'adillcontent'      => $samRow['adillcontent'],
                        'shutpage'          => $samRow['shutpage'],
                        'shutpage_thumb'    => $samRow['shutpage_thumb'],
                        'created_date'      => $samRow['created_date'],
                        'modifytime'        => $samRow['modifytime'],
                        'type'              => $samRow['type'],
                        'check_reason'      => $samRow['check_reason'] ? $samRow['check_reason'] : '',
                        'submittime'        => date('Y-m-d H:i:s',$samRow['createtime']),
                    ];
                }
                $this->ajaxReturn(['code'=>0, 'msg'=>'获取成功', 'count'=> $count, 'data'=>$resultList]);
            }else{
                $this->ajaxReturn(['code'=>0, 'msg'=>'暂无数据','data'=>[]]);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数,未登录']);
        }
    }


    /**
     * 获取历史记录列表
     */
    public function getHistoryList_v20190729(){
        $wx_id      = session('wx_id');
        $title      = $this->P['title'];
        $date       = $this->P['date'];
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 200);
        $limitIndex = ($pageIndex-1)*$pageSize;
        $resultList = [];
        if(!empty($wx_id)){
            $where = [
                'a.wx_id' => $wx_id,
                'a.fstatus' => ['IN',[6,7,8]],//TODO:6和7状态即将其他含义 2019-07-26
                'b.fdstatus' => ['notin',[2,4]]
            ];
            if(!empty($title)){
                if(is_numeric($title)){
                    $where['a.id'] = ['LIKE','%'.$title.'%'];
                }else{
                    $where['a.title'] = ['LIKE','%'.$title.'%'];
                }
            }
            if(!empty($date)){
                $dateArr = explode(',',$date);
                $dateS = strtotime($dateArr[0]);
                $dateE = strtotime($dateArr[1])+86399;
                $where['a.modifytime'] = ['BETWEEN',[$dateS,$dateE]];
            }
            $count = M('tnetissue_customer')
                ->alias('a')
                ->join('tapimedialist b on a.ftaskid = b.fid')
                ->where($where)
                ->count();
            $list = M('tnetissue_customer')
                ->alias('a')
                ->join('tapimedialist b on a.ftaskid = b.fid')
                ->where($where)
                ->order('modifytime DESC')
                ->limit($limitIndex,$pageSize)
                ->select();
            if(!empty($list)){
                foreach($list as $key => $samRow){
                    // 任务数据
                    $adclassname = M('tadclass')->cache(true,120)->where(['fcode' => $samRow['adtype']])->getField('ffullname');
                    $result = [
                        'id'                => $samRow['id'],
                        'title'             => $samRow['title'],
                        'brand'             => $samRow['brand'],
                        'adtype'            => $samRow['adtype'],
                        'adtypename'        => $adclassname,
                        'advertiser_id'     => $samRow['advertiser_id'],
                        'advertiser_na'     => $samRow['advertiser_na'],
                        'publisher_entname' => $samRow['publisher_entname'],
                        'file_url'          => $samRow['file_url'],
                        'target_url'        => $samRow['target_url'],
                        'original_url'      => $samRow['original_url'],
                        'adilltype'         => $samRow['adilltype'],
                        'adilllaw'          => $samRow['adilllaw'],
                        'adillegal'         => $samRow['adillegal'],
                        'adillcontent'      => $samRow['adillcontent'],
                        'shutpage'          => $samRow['shutpage'],
                        'shutpage_thumb'    => $samRow['shutpage_thumb'],
                        'created_date'      => $samRow['created_date'],
                        'modifytime'        => $samRow['modifytime'],
                        'type'              => $samRow['type'],
                        'check_reason'      => $samRow['check_reason'] ? $samRow['check_reason'] : '',
                    ];
                    array_push($resultList,$result);
                }
                $this->ajaxReturn(['code'=>0, 'msg'=>'获取成功', 'count'=> $count, 'data'=>$resultList]);
            }else{
                $this->ajaxReturn(['code'=>0, 'msg'=>'暂无数据','data'=>[]]);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数,未登录']);
        }
    }

    /**
     * 获取被退回记录列表
     */
    public function getReturnList(){
        $wx_id      = session('wx_id');
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 200);
        $limitIndex = ($pageIndex-1)*$pageSize;
        $resultList = [];
        if(!empty($wx_id)){
            $where = [
                'a.wx_id' => $wx_id,
                'a.fstatus' => 8,
                // 'b.fdstatus' => ['NOT IN',[2,4]],
            ];
            $count = M('tnetissue_customer')
                ->alias('a')
                // ->join('tapimedialist b on a.ftaskid = b.fid')
                ->where($where)
                ->count();
            $list = M('tnetissue_customer')
                ->alias('a')
                // ->join('tapimedialist b on a.ftaskid = b.fid')
                ->where($where)
                ->order('modifytime DESC')
                ->limit($limitIndex,$pageSize)
                ->select();
            $sql = M('tnetissue_customer')->getLastSql();
            if(!empty($list)){
                foreach($list as $key => $samRow){
                    // 任务数据
                    $adclassname = M('tadclass')->cache(true,120)->where(['fcode' => $samRow['adtype']])->getField('ffullname');
                    $result = [
                        'id'                => $samRow['id'],
                        'title'             => $samRow['title'],
                        'brand'             => $samRow['brand'],
                        'adtype'            => $samRow['adtype'],
                        'adtypename'        => $adclassname,
                        'advertiser_id'     => $samRow['advertiser_id'],
                        'advertiser_na'     => $samRow['advertiser_na'],
                        'publisher_entname' => $samRow['publisher_entname'],
                        'file_url'          => $samRow['file_url'],
                        'target_url'        => $samRow['target_url'],
                        'original_url'      => $samRow['original_url'],
                        'adilltype'         => $samRow['adilltype'],
                        'adilllaw'          => $samRow['adilllaw'],
                        'adillegal'         => $samRow['adillegal'],
                        'adillcontent'      => $samRow['adillcontent'],
                        'shutpage'          => $samRow['shutpage'],
                        'shutpage_thumb'    => $samRow['shutpage_thumb'],
                        'created_date'      => $samRow['created_date'],
                        'modifytime'        => $samRow['modifytime'],
                        'type'              => $samRow['type'],
                    ];
                    array_push($resultList,$result);
                }
                return $resultList;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * 重新上传截图
     */
    public function reUploadShutpage(){
        $id       = $this->P['id'];
        $shutpage = $this->P['shutpage'];
        if(!empty($id) && !empty($shutpage)){
            $oriInfo = M('tnetissue_customer')->where(['id'=>$id])->find();
            // 更新任务原始尺寸截图
            $resSaveShutpage = M('tnetissue_customer')
                ->where(['id'=>$id])
                ->save(['shutpage'=>$shutpage]);
            // 更新原样本原始尺寸截图
            $resSaveNetIssueShutpage = M('tnetissue')
                ->where(['major_key'=>$id])
                ->save(['net_snapshot'=>$shutpage]);
            // 发送压缩图片请求并更新压缩图、压缩图文件大小、原始图文件大小
            $gourl = 'http://'.$_SERVER['HTTP_HOST'] . U('Api/ShIssue/sampleimg') ;
            $data = [
                'issueid' => $id,
                'imgurl'  => $shutpage,
            ];
            $ret = http($gourl,json_encode($data),'POST',false,0);
            $ret = json_decode($ret,true);
            if($ret['code'] == 0){
                // 日志
                $newInfo = M('tnetissue_customer')->find(['id'=>$id]);
                $taskInfo = [
                    'new'=>$newInfo,
                    'ori'=>$oriInfo,
                ];
                $flowTableName = 'tnetissue_customer_flowlog';
                $log = '重新上传截图 由 '.$oriInfo['shutpage'].' -> '.$shutpage;
                $flowData = [
                    'taskid'     => $id,
                    'wx_id'      => session('wx_id'),
                    'logtype'    => 3,
                    'log'        => $log,
                    'taskinfo'   => json_encode($taskInfo),
                    'creator'    => session('wx_id'),
                    'createtime' => time(),
                ];
                $flowRes = M($flowTableName)->add($flowData);
                $this->ajaxReturn(['code'=>0, 'msg'=>'截图上传成功','shutpage_thumb'=>$ret['data']['shutpage_thumb']]);
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'截图上传失败']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 上海数据检查-获取广告样本列表(互联网)
     */
    public function NetCheckList(){
        $fid        = $this->P['fid'];
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 1000);
        $limitIndex = ($pageIndex-1)*$pageSize;
        $resultList = [];   // 检查列表
        $count = 0;         // 记录总条数
        $fmediaid = '';     // 媒体ID
        if(!empty($fid)){
            $planInfo = M('tapimedialist')->where(['fid'=>$fid])->find();
            if(!empty($planInfo['fmediaid'])){
                $mediaMap = M('tapimedia_map')->where(['fid'=>$planInfo['fmediaid']])->find();
                if(!empty($mediaMap['fmediacode'])){
                    $fmediaid = $mediaMap['fmediaid'];
                    // 重要媒体
                    $starttime = $planInfo['ftask_starttime'];
                    $fissuedate = $planInfo['fissuedate'];
                    if(!empty($starttime) && !empty($fissuedate)){
                        $issuedate = ['BETWEEN',[date('Y-m-d',$starttime),date('Y-m-d',$fissuedate)]];
                    }else{
                        $issuedate = date('Y-m-d',$fissuedate);
                    }
                    $samWhere = [
                        'fstatus' => ['IN',[0,1,2,3]],
                        'publisher_id' => $fmediaid,
                        'issuedate' => $issuedate
                    ];
                    
                    $count = M('tnetissue_customer')
                        ->where($samWhere)
                        ->count();

                    if($count > 0){
                        // 更新绑定计划ID
                        $bindCount = M('tnetissue_customer')
                            ->where(['ftaskid'=>$fid])
                            ->count();
                        if($bindCount <= 0){
                            $bindRes = M('tnetissue_customer')->where($samWhere)->save(['ftaskid'=>$fid]);
                        }
                        $samList = M('tnetissue_customer')
                            ->where(['ftaskid'=>$fid,'fstatus'=>['in',[2,5]]])
                            ->limit($limitIndex,$pageSize)
                            ->select();
                        foreach($samList as $key => $samRow){
                            $adclassname = M('tadclass')->cache(true,120)->where(['fcode' => $samRow['adtype']])->getField('ffullname');
                            $result = [
                                'id'               => $samRow['id'],
                                'title'            => $samRow['title'],
                                'brand'            => $samRow['brand'],
                                'adtype'           => $samRow['adtype'],
                                'adtypename'       => $adclassname,
                                'advertiser_id'    => $samRow['advertiser_id'],
                                'advertiser_na'    => $samRow['advertiser_na'],
                                'adilltype'        => $samRow['adilltype'],
                                'adilllaw'         => $samRow['adilllaw'],
                                'adillegal'        => $samRow['adillegal'],
                                'adillcontent'     => $samRow['adillcontent'],
                                'shutpage'         => $samRow['shutpage'],
                                'modifytime'       => $samRow['modifytime']
                            ];
                            array_push($resultList,$result);
                        }
                    }
                }else{
                    // 非重要媒体,取当月1日（或上一个计划的开始日期的下一天）至发布日期间随机数量(阀值5000)样本
                    $fissuedate = $planInfo['fissuedate'];
                    $prePlanInfo = M('tapimedialist')
                        ->join('tapimedia_map ON tapimedia_map.fid = tapimedialist.fmediaid')
                        ->where(['tapimedia_map.fmediacode'=>0,'fissuedate'=>['BETWEEN',[strtotime(date('Y-m-01',$fissuedate)),$fissuedate]]])
                        ->order('fissuedate DESC')
                        ->find();
                    if(!empty($prePlanInfo)){
                        $issue_date = ['BETWEEN',[date('Y-m-d',($prePlanInfo['fissuedate']+86400)),date('Y-m-d',$fissuedate)]];
                    }else{
                        $issue_date = ['BETWEEN',[date('Y-m-01'),date('Y-m-d',$fissuedate)]];
                    }

                    //获取用户所有重要媒体
                    $user_media = M('tapimedia_map') -> where(['fuserid'=>$prePlanInfo['tapimedia_map.fuserid'],'fmediacode'=>'0','fmediatype'=>$prePlanInfo['fmediatype'],'fstatus'=>1]) ->getField('fmediaid');
                    $user_media = $user_media?$user_media:[0];

                    $limitRand = $mediaMap['fthreshold'];
                    $samWhere = [
                        'fstatus' => ['IN',[0,1,2,3]],
                        'publisher_id' => ['not in',$user_media],
                        '_string' => 'publisher_area like "31%" or advertiser_area like "31%" or tracker_area like "31%"',
                        'issuedate' => $issuedate
                    ];
                    $count = M('tnetissue_customer')
                        ->where($samWhere)
                        ->count();
                    if($count > 0){
                        // 更新绑定计划ID
                        $bindCount = M('tnetissue_customer')
                            ->where(['ftaskid'=>$fid])
                            ->count();
                        if($bindCount <= 0){
                            $bindRes = M('tnetissue_customer')
                                ->where($samWhere)
                                ->orderRand()
                                ->limit($limitRand)
                                ->save(['ftaskid'=>$fid]);
                        }
    
                        $samList = M('tnetissue_customer')
                            ->where(['ftaskid'=>$fid,'fstatus'=>['in',[2,5]]])
                            ->limit($limitIndex,$pageSize)
                            ->select();
                        foreach($samList as $key => $samRow){
                            $adclassname = M('tadclass')->cache(true,120)->where(['fcode' => $samRow['adtype']])->getField('ffullname');
                            $result = [
                                'id'               => $samRow['id'],
                                'title'            => $samRow['title'],
                                'brand'            => $samRow['brand'],
                                'adtype'           => $samRow['adtype'],
                                'adtypename'       => $adclassname,
                                'advertiser_id'    => $samRow['advertiser_id'],
                                'advertiser_na'    => $samRow['advertiser_na'],
                                'adilltype'        => $samRow['adilltype'],
                                'adilllaw'         => $samRow['adilllaw'],
                                'adillegal'        => $samRow['adillegal'],
                                'adillcontent'     => $samRow['adillcontent'],
                                'shutpage'         => $samRow['shutpage'],
                                'modifytime'       => $samRow['modifytime']
                            ];
                            array_push($resultList,$result);
                        }
                    }
                }
            }else{
                $resultList = [];
            }

            $dataSet = [
                'fid'        => $fid,
                'fmediaid'   => $fmediaid,
                'fissuedate' => date('Y-m-d H:i:s',$planInfo['fissuedate']),
                'starttime'  => date('Y-m-d H:i:s',$planInfo['ftask_starttime']),
                'fad'        => $resultList,
            ];
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$count, 'data'=>$dataSet]);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 互联网数据检查任务-提交通过和保存(目前仅检查上海互联网数据)
     */
    public function checkPassSave(){
        $id = $this->P['id'];
        $fields = [
            'title'         => '广告名称',
            'brand'         => '广告品牌',
            'adtype'        => '广告类别代码',
            'advertiser_na' => '广告主',
            'fversion'      => '版本说明',
            'adilltype'     => '违法类型',
            'adillcontent'  => '违法内容',
            'adilllaw'      => '违法表现代码',
            'adillegal'     => '违法表现',
        ];
        // 接受的字段
        $saveFields = [];
        foreach($fields as $field => $fname){
            array_push($saveFields,$field);
        }
        // 提交的数据
        $data = [
            'fstatus' => 8,
            'modifytime' => time(),
        ];
        // 如果违法类型adilltype=0不违法,则将adillcontent，adilllaw，adillegal均更新为空字符串'',注意不要写为null,因为isset(null) == false。
        if($this->P['adilltype'] == 0){
            $this->P['adillcontent'] = '';
            $this->P['adilllaw']     = '';
            $this->P['adillegal']    = '';
        }
        // TODO:广告主ID为空值时，表示为新创建的广告主，需添加到tadowner表

        if(!empty($id)){
            $oriInfo = M('tnetissue_customer')->where(['id'=>$id])->find();
            // 截图为空时需上传
            $shutpage_thumb = $oriInfo['shutpage_thumb'];
            if(empty($shutpage_thumb)){
                $this->ajaxReturn(['code'=>1, 'msg'=>'请重新上传截图']);
            }
            // 是否为重做
            $isRepeat = $this->isRepeatSubmit(session('wx_id'),$id);
            if(!$isRepeat){
                $log = '提交 ';
            }else{
                $log = '提交(重做) ';
                // 校验重做历史任务是否允许操作
                $isAllow = $this->isAllowSubmit($id,session('wx_id'));
                if(!$isAllow){
                    $this->ajaxReturn(['code'=>1, 'msg'=>'当前任务已被检查或被其它人处理，仅供查看不允许进行提交操作']);
                }
            }
            foreach($this->P as $key=>$val){
                // 更新变更的字段
                if(isset($val) && in_array($key,$saveFields)){
                    $data[$key] = $val;
                    // 变更的内容
                    if($oriInfo[$key] != $data[$key]){
                        $ori = ($oriInfo[$key] || $oriInfo[$key] == 0) ? $oriInfo[$key] : '(空)';
                        $new = ($data[$key] || $data[$key] == 0) ? $data[$key] : '(空)';
                        $log .= $fields[$key].':'.$ori.' -> '.$new.' ; ';
                    }
                }
            }
            // 保存
            $res = M('tnetissue_customer')
                ->where(['id'=>$id])
                ->save($data);
            if(!$isRepeat){
                // 更新积分
                $scoreRes = (new TaskInputScoreModel())->addScoreByNetCheckTask($id);
            }
            // 更新流程
            $newInfo = M('tnetissue_customer')->where(['id'=>$id])->find();
            $taskInfo = [
                'new'=>$newInfo,
                'ori'=>$oriInfo,
            ];
            $flowTableName = 'tnetissue_customer_flowlog';
            $flowData = [
                'taskid'     => $id,
                'wx_id'      => session('wx_id'),
                'logtype'    => 2,
                'log'        => $log,
                'taskinfo'   => json_encode($taskInfo),
                'creator'    => session('wx_id'),
                'createtime' => time(),
            ];
            $flowRes = M($flowTableName)->add($flowData);
            if (!$flowRes){
                $this->error = '流程更新失败';
                $this->ajaxReturn(['code'=>1, 'msg'=>$this->error]);
            }
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功']);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 互联网数据检查任务-不通过(目前仅检查上海互联网数据)
     */
    public function checkReject(){
        $id = $this->P['id'];
        $taskInfo = M('tnetissue_customer')->where(['id'=>$id])->find();
        $data = [
            'fstatus' => 7,
            'modifytime' => time(),
        ];
        if(!empty($id)){
            $res = M('tnetissue_customer')
                ->where(['id'=>$id])
                ->save($data);
            if($res){
                $isRepeat = $this->isRepeatSubmit(session('wx_id'),$id);
                $log = '不通过';
                if(!$isRepeat){
                    // 流程日志
                    $log = '不通过';
                    // 更新积分
                    $scoreRes = (new TaskInputScoreModel())->addScoreByNetCheckTask($id);
                }else{
                    // 流程日志
                    $log = '不通过(重做)';
                }
                // 更新流程
                $flowTableName = 'tnetissue_customer_flowlog';
                $flowData = [
                    'taskid'     => $id,
                    'wx_id'      => session('wx_id'),
                    'logtype'    => 2,
                    'log'        => $log,
                    'taskinfo'   => json_encode($taskInfo),
                    'creator'    => session('wx_id'),
                    'createtime' => time(),
                ];
                $flowRes = M($flowTableName)->add($flowData);
                if (!$flowRes){
                    $this->error = '流程更新失败';
                    $this->ajaxReturn(['code'=>1, 'msg'=>$this->error]);
                }
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功']);
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'出错']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 互联网数据检查任务-判断是否是重新提交历史任务
     */
    protected function isRepeatSubmit($wx_id, $taskid){
        $where = [
            'wx_id'   => $wx_id,
            'taskid'  => $taskid,
            'logtype' => 2,//提交过的任务
        ];
        $isExist = M('tnetissue_customer_flowlog')
            ->where($where)
            ->count() > 0 ? true : false;
        return $isExist;
    }

    /**
     * 互联网数据检查任务-判断历史任务是否允许提交或重新截图等操作
     */
    public function getIsAllowSubmit(){
        $taskid = $this->P['taskid'];
        $wx_id = session('wx_id') ? session('wx_id') : 469;
        if(!empty($taskid)){
            $isAllow = $this->isAllowSubmit($taskid,$wx_id) ? 1 : 0;
            $msg = $isAllow ? '允许操作' : '仅查看,不允许操作';
            $this->ajaxReturn(['code'=>0, 'msg'=>$msg ,'data'=>$isAllow]);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 判断历史任务是否允许提交或重新截图等操作
     */
    public function isAllowSubmit($taskid,$wx_id){
        $where = [
            'id'      => $taskid,
            'wx_id'   => $wx_id,
            'fstatus' => ['IN',[5,8]],//只有当数据补录提交后未被检查或其它人操作过才允许重新提交或截图，否则只允许查看
        ];
        $isExist = M('tnetissue_customer')
            ->where($where)
            ->count() > 0 ? true : false;
        $sql = M('tnetissue_customer')->getLastSql();
        return $isExist;
    }

    /**
     * 上海互联网数据检查-计划完成
     */
    public function NetPlanFinish(){
        $fid = $this->P['fid'];
        if(!empty($fid)){
            $FinishCount = M('tnetissue_customer')
                ->where(['ftaskid'=>$fid,'fstatus'=>['EQ',6]])
                ->count();
            if($FinishCount > 0){
                $unFinishCount = M('tnetissue_customer')
                    ->where(['ftaskid'=>$fid,'fstatus'=>['in',[2,5]]])
                    ->count();
                if($unFinishCount == 0){
                    $data = [
                        'fmodifier'     => session('wx_id'),
                        'fmodifytime'   => date('Y-m-d H:i:s'),
                        'fdstatus'      => 2,
                        'fdfinishcount' => $FinishCount,
                    ];
                    $res = M('tapimedialist')
                        ->where(['fid'=>$fid])
                        ->save($data);
                    if($res){
                        $this->ajaxReturn(['code'=>0, 'msg'=>'成功']);
                    }else{
                        $this->ajaxReturn(['code'=>1, 'msg'=>'出错']);
                    }
                }else{
                    $this->ajaxReturn(['code'=>1, 'msg'=>'该任务未完成，请核实']);
                }
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'不存在该任务']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 判断互联网检查计划是否可检查
     */
    public function isNetPlanCheckable($planId){

        if(!empty($planId)){
            $planInfo = M('tapimedialist')
                ->where(['fid'=>$planId,'fuserid'=>self::USERID])
                ->find();
            // 1.发布日期是未来日期不可检查
            if($planInfo['fissuedate'] > time()){
                return false;
            }
            // 2.截图完成的样本量小于任务阀值不可检查
            $fthreshold = M('tapimedia_map')
                ->where(['fid'=>$planInfo['fmediaid']])
                ->getField('fthreshold');
            $countS = date('Y-m-d',$planInfo['ftask_starttime']);
            $countE = date('Y-m-d',$planInfo['fissuedate']);
            $countWhere = [
                'issue_date' => ['BETWEEN',[$countS,$countE]],
                'fstatus' => ['in',[2,5,6,7]]
            ];
            $completeCount = M('tnetissue_customer')
                ->where($countWhere)
                ->count();
             
            if($completeCount < $fthreshold){
                return false;
            }
            return true;
        }else{
            return false;
        }
    }

    /**
     * 上海数据检查-确认完成退回计划
     */
    public function finishReProductPlan(){
        $fid = $this->P['fid'];
        if(isset($fid) && !empty($fid)){
            $planInfo = M('tapimedialist')->where(['fid'=>$fid])->find();
            if(!empty($planInfo)){
                M()->startTrans();
                // 更新退回记录状态
                $data = [
                    'fstate'      => 2,
                    'fmodifier'   => session('personInfo.fname'),
                    'fmodifytime' => date('Y-m-d H:i:s'),
                ];
                $where = [
                    'fapimedialistid' => $fid,
                    'fstate' => 0,
                ];
                $resUpLog = M('tapimedialist_backlog')->where($where)->save($data);
                // 更新计划状态
                $planSaveData = [
                    'fdstatus'    => 0,
                    'fmodifier'   => session('personInfo.fname'),
                    'fmodifytime' => date('Y-m-d H:i:s'),
                ];
                $wherePlan = [
                    'fid' => $fid,
                    'fdstatus' => 12,
                ];
                $resUpPlan = M('tapimedialist')->where($wherePlan)->save($planSaveData);
                if($resUpLog && $resUpPlan){
                    M()->commit();
                    $this->ajaxReturn(['code'=>0, 'msg'=>'已完成']);
                }else{
                    M()->rollback();
                    $this->ajaxReturn(['code'=>1, 'msg'=>'出错']);
                }
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'未找到对应任务计划']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 上海数据检查-退回情况列表
     */
    public function reProductPlanList(){
        $fmediaid    = $this->P['fmediaid'];
        $fissuedateS = $this->P['fissuedateS'];
        $fissuedateE = $this->P['fissuedateE'];
        $fcreator    = $this->P['fcreator'];
        $fcreatetime = $this->P['fcreatetime'];
        $fstate      = $this->P['fstate'];
        $pageIndex   = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize    = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 1000);
        $limitIndex  = ($pageIndex-1)*$pageSize;
        $where = [];
        if(!empty($fmediaid)){
            $where['fmediaid'] = $fmediaid;
        }
        if(!empty($fissuedateS) && !empty($fissuedateE)){
            $where['fissuedate'] = ['BETWEEN',[strtotime($fissuedateS),strtotime($fissuedateE)+86400]];
        }
        if(!empty($fcreator)){
            $where['fcreator'] = $fcreator;
        }
        if(!empty($fcreatetime)){
            $where['fcreatetime'] = $fcreatetime;
        }
        if(!empty($fstate)){
            $where['fstate'] = $fstate;
        }
        $count = M('tapimedialist_backlog')
            ->where($where)
            ->count();
        if($count > 0){
            $dataSet = M('tapimedialist_backlog')
                ->where($where)
                ->limit($limitIndex,$pageSize)
                ->select();
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$count, 'data'=>$dataSet]);
        }
    }

    /**
     * 互联网数据检查计划页面
     */
    public function NetPlan(){
        $this->display();
    }

    /**
     * 互联网数据检查页面
     */
    public function NetCheck(){
        $illegalType = M('tillegaltype')->field('fcode value, fillegaltype text')->cache(true)->where(['fstate' => ['EQ', 1]])->select();
        $illegalTypeArr = json_encode($illegalType);
        $adClassArr = json_encode($this->getAdClassArr());
        $adClassTree = json_encode($this->getAdClassTree());
        $illegalArr = json_encode($this->getIllegalArr());
        $this->assign(compact('type', 'adClassTree', 'illegalTypeArr', 'adClassArr', 'illegalArr'));
        $this->display();
    }

    /**
     * 互联网检查数据查看
     */
    public function PlanData(){
        $this->display();
    }

    /**
     * 互联网检查数据添加
     */
    public function PlanDataAdd(){
        $this->display();
    }

    /**
     * 互联网检查数据优先级调整
     */
    public function PlanOrder(){
        $this->display();
    }

    /**
     * 存证列表
     */
    public function getEvidenceList(){
        $fevidenceid = $this->P['fevidenceid'];
        $fstarttime  = $this->P['fstarttime'] ? $this->P['fstarttime'] : date('Y-m-d 00:00:00',strtotime('-1 year'));
        $fendtime    = $this->P['fendtime'] ? $this->P['fendtime'] : date('Y-m-d 23:59:59');
        $fstate      = $this->P['fstate'];
        $pageIndex   = $this->P['pageIndex'] ? $this->P['pageIndex'] : 1;
        $pageSize    = $this->P['pageSize'] ? $this->P['pageSize'] : 1000;
        $limitIndex  = ($pageIndex-1)*$pageSize;
        $where = [];
        if($fevidenceid != ''){
            $where['fevidenceid'] = $fevidenceid;
        }
        if($fstate != ''){
            $where['fstate'] = $fstate;
        }
        $where['cdate'] = ['BETWEEN',[date('Y-m-d 00:00:00',strtotime($fstarttime)),date('Y-m-d 23:59:59',strtotime($fendtime))]];
        $count = M('tb_evidence')->where($where)->count();
        $fields = 'id,title,url,rid,md5,screenshot,cdate,status';
        $dataSet = M('tb_evidence')
            ->field($fields)
            ->where($where)
            ->order('fcreatetime DESC')
            ->limit($limitIndex,$pageSize)
            ->select();
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','count'=>$count,'data'=>$dataSet]);
    }

    /**
     * 新增存证
     */
    public function addEvidence(){
        $eviUrl = $this->P['url'];
        $eviTitle = $this->P['title'];
        if($eviUrl){
            // 保存待存证
            $dataSet = [
                'title' => $eviTitle,
                'url' => $eviUrl,
                'rid' => '',
                'md5' => '',
                'screenshot' => '',
                'wsize' => '1920x1080',
                'cdate' => date('Y-m-d H:i:s'),
                'ua' => 0,
                'render' => 0,
                'flash' => 1,
                'status' => 0,
                'fcreator' => '',
                'fcreatetime' => date('Y-m-d H:i:s'),
                'fmodifier' => '',
                'fmodifytime' => date('Y-m-d H:i:s')
            ];
            // TODO:重复提交规则待定
            // $isExist = M('tb_evidence')->where(['url'=>$eviUrl])->count() > 0 ? true : false;
            // if(!$isExist){
            //     M('tb_evidence')->add($dataSet);
            // }
            $result = M('tb_evidence')->add($dataSet);

            // 发送存证请求
            $reqUrl = base64_encode($eviUrl);
            $sUrl = 'http://interface.e-chains.cn:8309/ecues/delivery';
            // 测试
            // $accessId = 'UFDYKEPJOLCHIRCEFITYAW2';
            // $accessKey = 'RIWEELV6OBREPCIECAPALYUBNUFMIPEEJASHVI';
            // 正式
            $accessId = '15565244962745QrTgfuzvUc55e4f7a7ecbe1d76e94ead56cfa4bdbe9195';
            $accessKey = '15565244962745YyjvSTuHZae1eef14722421f4f9f9ae1937a3e1db32628';
            $uts = time();
            $accessToken = md5($uts.$accessKey);
            $aData = [
                "url"    => $reqUrl,
                "render" => "1",
                "flash"  => "1",
                "wsize"  => "1920x1080",
                "ua"     => "0"
            ];
            $aHeader = [
                'Content-Type: application/json',
                'Uts: '.$uts,
                'Access-id: '.$accessId,
                'Access-token: '.$accessToken,
                'Type: url',
            ];
            $resInfo = http($sUrl,json_encode($aData),'POST',$aHeader,10);
            $resInfo = json_decode($resInfo,true);
            if($resInfo['type'] == 'succ'){
                $objectId = $resInfo['id'];
                $dataSet = [
                    'rid' => $objectId
                ];
                $saveReturn = M('tb_evidence')->where(['id'=>$result])->save($dataSet);
                // TODO:更新存证结果待处理
                $this->_sleepExc($objectId);

                $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>['rid' => $objectId]]);

                // TODO:更新存证结果待处理
                // $this->_sleepExc($objectId);

            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'出错','data'=>$resInfo]);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 下载OSS文件解压并更新存证信息
     */
    public function updateEvidenceInfo(){
        $where = [
            'status' => 0,
            'rid' => ['NEQ',0]
        ];
        $list = M('tb_evidence')
            ->field('id,rid')
            ->where($where)
            ->select();
        foreach($list as $key => $value){
            $this->_sleepExc($value['rid']);
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'成功']);
    }

    /**
     * 等待存证上传OSS成功
     */
    protected function _sleepExc($objectId){
        $isObjectExist = A('Common/AliyunOss','Model')->isObjectExist($objectId);
        if($isObjectExist){
            $this->_ossFile($objectId);
            return true;
        }else{
            sleep(1);
            $this->_sleepExc($objectId);
        }
        return true;
    }

    /**
     * 下载解压上传快照
     */
    protected function _ossFile($objectId){
        // 从OSS下载至本地
        $object = A('Common/AliyunOss','Model')->getObject($objectId);
        // 从OSS加载至内存
        // $objectContent = A('Common/AliyunOss','Model')->getObjectToMem($objectId);
        // 从本地解压上传快照并返回快照url
        $ret = $this->tarObject($objectId);
        // 从内存解压 TODO:内存不足
        // $ret = $this->tarObjectFromMem($object);
        // 获取元信息
        $objectMeta = $this->getObjectMeta($objectId);

        $dataSet = [
            'md5' => $objectMeta['eTag'],
            'screenshot' => $ret['url'],
            'status' => 1,
            'fmodifier' => '',
            'fmodifytime' => date('Y-m-d H:i:s')
        ];
        $saveReturn = M('tb_evidence')->where(['rid'=>$objectId])->save($dataSet);
        return $saveReturn;
    }

    /**
     * 下载OSS存证压缩包
     */
    public function downObj($objectId){
        // 从OSS下载至本地
        $object = A('Common/AliyunOss', 'Model')->getObject($objectId);
    }

    /**
     * 存证完成回调
     */
    public function callBackEvi(){
        $objectId = $this->P['objectId'];
        // 获取元信息
        $objectMeta = $this->getObjectMeta($objectId);
        $dataSet = [
            'md5' => $objectMeta['eTag'],
            'screenshot' => $ret['url'],
            'status' => 1,
            'fmodifier' => '',
            'fmodifytime' => date('Y-m-d H:i:s')
        ];
        $saveReturn = M('tb_evidence')->where(['rid'=>$objectId])->save($dataSet);
        $this->ajaxReturn(['code'=>0,'msg'=>'成功']);
    }

    /**
     * 出证申请
     */
    public function applyEvidence(){
        $data = [
            'fnotarialtype'   => $this->P['fnotarialtype'],
            'ftype'           => $this->P['ftype'],
            'fusage'          => $this->P['fusage'],
            'fusername'       => $this->P['fusername'],
            'fmobile'         => $this->P['fmobile'],
            'fidcard'         => $this->P['fidcard'],
            'fposition'       => $this->P['fposition'],
            'faddress'        => $this->P['faddress'],
            'fidcardurl'      => $this->P['fidcardurl'],
            'fidcardurl2'     => $this->P['fidcardurl2'],
            'fidcardfeelurl'  => $this->P['fidcardfeelurl'],
            'fmandateurl'     => $this->P['fmandateurl'],
            'fnetmandateurl'  => $this->P['fnetmandateurl'],
            'fentname'        => $this->P['fentname'],
            'fregisterno'     => $this->P['fregisterno'],
            'fenturl'         => $this->P['fenturl'],
            'fnotarialno'     => $this->P['fnotarialno'],
            'fdocketzipname'  => $this->P['fdocketzipname'],
            'fegalperson'     => $this->P['fegalperson'],
            'fegalidcard'     => $this->P['fegalidcard'],
            'fegalidcardurl'  => $this->P['fegalidcardurl'],
            'fegalidcardurl2' => $this->P['fegalidcardurl2'],
            'fzipurl'         => $this->P['fzipurl'],
            'fispost'         => $this->P['fispost'],
            'fpostusername'   => $this->P['fpostusername'],
            'fpostmobile'     => $this->P['fpostmobile'],
            'fpostaddress'    => $this->P['fpostaddress'],
            'fremark'         => $this->P['fremark'],
            'fstatus'         => $this->P['fstatus']
        ];
        $existCondition = [
            // TODO:排重条件待完善
            'fnotarialtype' => $this->P['fnotarialtype'],
            'ftype' => $this->P['ftype'],
            'fusage' => $this->P['fusage']
        ];
        $isExist = M('tb_applicant')->where($existCondition)->count() > 0 ? true : false;
        if(!$isExist){
            $ret = M('tb_applicant')->add($data);
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>[]]);
    }

    /**
     * 出证列表
     */
    public function applyEvidenceList(){
        $fevidenceid = $this->P['fevidenceid'];
        $fstarttime  = $this->P['fstarttime'];
        $fendtime    = $this->P['fendtime'];
        $fstate      = $this->P['fstate'];
        $pageIndex   = $this->P['pageIndex'] ? $this->P['pageIndex'] : 1;
        $pageSize    = $this->P['pageSize'] ? $this->P['pageSize'] : 1000;
        $limitIndex  = ($pageIndex-1)*$pageSize;
        $where = [];
        if($fevidenceid != ''){
            $where['fevidenceid'] = $fevidenceid;
        }
        if($fstarttime != ''){
            $where['fstarttime'] = $fstarttime;
        }
        if($fendtime != ''){
            $where['fendtime'] = $fendtime;
        }
        if($fstate != ''){
            $where['fstate'] = $fstate;
        }
        $count = M('tb_evidence')->where($where)->count();
        $fields = 'id,url,rid,md5,screenshot,cdate,status';
        $dataSet = M('tb_evidence')
            ->field($fields)
            ->where($where)
            ->order('fcreatetime DESC')
            ->limit($limitIndex,$pageSize)
            ->select();
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','count'=>$count,'data'=>$dataSet]);
    }

    /**
     * 新增网页存证
     */
    public function addNetEvidence(){
        $eviUrl = $this->P['url'];
        if($eviUrl){
            $reqUrl = base64_encode($eviUrl);
            $sUrl = 'http://interface.e-chains.cn:8309/ecues/delivery';
            $accessId = 'UFDYKEPJOLCHIRCEFITYAW2';
            $accessKey = 'RIWEELV6OBREPCIECAPALYUBNUFMIPEEJASHVI';
            $uts = time();
            $accessToken = md5($uts.$accessKey);
            $aData = [
                "url"    => $reqUrl,
                "render" => "1",
                "flash"  => "1",
                "wsize"  => "1920x1080",
                "ua"     => "0"
            ];
            $aHeader = [
                'Content-Type: application/json',
                'Uts: '.$uts,
                'Access-id: '.$accessId,
                'Access-token: '.$accessToken,
                'Type: url',
            ];
            $resInfo = http($sUrl,json_encode($aData),'POST',$aHeader,10);
            $resInfo = json_decode($resInfo,true);

            $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>['id'=>$resInfo['id']]]);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 网页存证
     */
    public function setNetEvidence(){
        $sUrl = 'http://interface.e-chains.cn:8309/ecues/delivery';
        $accessId = 'UFDYKEPJOLCHIRCEFITYAW2';
        $accessKey = 'RIWEELV6OBREPCIECAPALYUBNUFMIPEEJASHVI';
        $uts = time();
        $accessToken = md5($uts.$accessKey);
        $reqUrl = base64_encode('http://www.baidu.com');
        $aData = [
            "url"    => $reqUrl,
            "render" => "1",
            "flash"  => "1",
            "wsize"  => "1920x1080",
            "ua"     => "0"
        ];
        $aHeader = [
            'Content-Type: application/json',
            'Uts: '.$uts,
            'Access-id: '.$accessId,
            'Access-token: '.$accessToken,
            'Type: url',
        ];
        $resInfo = http($sUrl,json_encode($aData),'POST',$aHeader,10);
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$resInfo]);
    }

    /**
     * 存证列表
     */
    public function getNetEvidenceList(){
        $M = A('Common/AliyunOss','Model');
        $bucket = 'cunzheng';
        $listObjectInfo = $M->listObjects($bucket);
        $objectList = $listObjectInfo->getObjectList();
        $data = [];
        foreach($objectList as $key => $objectInfo){
            $info = [
                'key' => $objectInfo->getKey(),
                'lastModified' => date('Y-m-d H:i:s',strtotime($objectInfo->getLastModified())),
                'eTag' => $objectInfo->getEtag(),
                'type' => $objectInfo->getType(),
                'size' => $objectInfo->getSize(),
                'storageClass' => $objectInfo->getStorageClass()
            ];
            array_push($data,$info);
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$data]);
    }

    /**
     * 获取指定对象元信息
     */
    public function getObjectMeta($object){
        $objectList = A('Common/AliyunOss','Model')->getObjects($object);
        foreach($objectList as $objectInfo){
            $objectMeta = [
                'key' => $objectInfo->getKey(),
                'lastModified' => date('Y-m-d H:i:s',strtotime($objectInfo->getLastModified())),
                'eTag' => $objectInfo->getEtag(),
                'type' => $objectInfo->getType(),
                'size' => $objectInfo->getSize(),
                'storageClass' => $objectInfo->getStorageClass()
            ];
        }
        return $objectMeta;
    }

    /**
     * 下载指定文件
     */
    public function getObject(){
        $M = A('Common/AliyunOss','Model');
        $bucket = 'cunzheng';
        $object = '';
        $options = [];
        $object = $M->getObject($object, $bucket, $options);
        $this->ajaxReturn(['code'=>0,'msg'=>'下载成功']);
    }

    /**
     * 通过授权签名URL下载
     */
    public function getSignUrl(){
        $bucket = 'cunzheng';
        $object = I('fid');
        $timeout = 3600;
        // 生成GetObject的签名URL。
        $signedUrl = A('Common/AliyunOss','Model')->signUrl($bucket, $object, $timeout);
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','signurl'=>$signedUrl]);
    }

    /**
     * 从磁盘解压缩.tar.gz文件
     */
    public function tarObject($objname = ''){
        exec('mkdir -p ./LOG/temp/'.$objname,$out,$ret);
        exec('tar -xzvf ./LOG/'.$objname.'.tar.gz -C ./LOG/temp/'.$objname,$output,$execret);
        $filename = $objname.'_screenshot_'.date('YmdHis').'.png';
        $dir = './LOG/temp/'.$objname.'/screenshot.png';
        if(!file_exists($dir)){
            mkdir($dir);
        }
        $savefile = $dir;
        $ret = A('Common/AliyunOss','Model')
            ->file_up('tem/'.$filename,$savefile,['Content-Disposition' => 'attachment;filename=']);//上传OSS(ddmmpp)
        return $ret;
    }

    /**
     * TODO: 从内存解压缩.tar.gz文件 
     */
    public function tarObjectFromMem($objname = ''){
        exec('mkdir -p ./LOG/temp/'.$objname,$out,$ret);
        exec('tar -xzvf ./LOG/'.$objname.' -C ./LOG/temp/'.$objname,$output,$execret);
        $filename = $objname.'_screenshot_'.date('YmdHis').'.png';
        $dir = './LOG/temp/'.$objname.'/screenshot.png';
        if(!file_exists($dir)){
            mkdir($dir);
        }
        $savefile = $dir;
        $ret = A('Common/AliyunOss','Model')
            ->file_up('tem/'.$filename,$savefile,['Content-Disposition' => 'attachment;filename=']);//上传OSS(ddmmpp)
        return $ret;
    }

    /**
     * 解压缩.tar.gz文件
     */
    public function tarObject2($objname = '436558'){
        // $objname = '436558';
        exec('mkdir -p ./LOG/temp/'.$objname,$out,$ret);
        exec('tar -xzvf ./LOG/'.$objname.' -C ./LOG/temp/'.$objname,$output,$execret);
        $filename = $objname.'_screenshot_'.date('YmdHis').'.png';
        $dir = './LOG/temp/'.$objname.'/screenshot.png';
        if(!file_exists($dir)){
            mkdir($dir);
        }
        $savefile = $dir;
        $ret = A('Common/AliyunOss','Model')
            ->file_up('tem/'.$filename,$savefile,['Content-Disposition' => 'attachment;filename=']);//上传OSS(ddmmpp)
            // ->uploadFile($filename,$savefile);//上传OSS(cunzheng)
        $this->ajaxReturn(['code'=>0,'msg'=>'下载成功','data'=>$ret]);
    }

    /**
     * 获取下载存管函
     */
    public function getEvidenceReceipt2(){
        // TODO:待完成
        $fid = $this->P['fid'];
        $sUrl = 'https://evidence-management.e-chains.cn/ues_evidence/'.$fid.'/pdf';
        $aData = [];
        $sCookie = '_session=eyJpdiI6ImdOQmFtSVhybXdjSjZuN2VJcjBJWEE9PSIsInZhbHVlIjoiaWgxejA0TDE3M2tVa3FHV0FEWTNIM3JDQ2p4cERIUVFxVTVnbEErcTFPSzNZVDhOZ0FGa0dlY1U2OGhcL3VPSVEiLCJtYWMiOiI0MTg1ZDc3NDkxNTQ2NTUwMzhmYjMwZTY2ZGJjMTIxNjBiZTQ4MGM2OGIyZmNjMmUwOGRhZWZhNWRjYmNjMDkzIn0%3D';
        $aHeader = [
            'Cookie: '.$sCookie,
        ];
        $resInfo = http($sUrl,$aData,'GET',$aHeader,10);
        $filename = 'LOG/evidence_'.$fid.'.html';
        file_put_contents($filename,$resInfo);
        $data = [
            'fileurl' => $resInfo
            // 'fileurl' => $_SERVER['SERVER_NAME'].'/'.$filename
            // 'fileurl' => $_SERVER['HTTP_HOST'].'/'.$filename
        ];
        $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$data]);
    }

    /**
     * 获取下载存管函
     */
    public function getEvidenceReceipt(){
        // TODO:待完成
        $fid = $this->P['fid'];
        // $this->showPDF($fid);
        $data = [
            'fileurl' => U('/TaskInput/NetAd/showPDF')
        ];
        $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$data]);
    }

    /**
     * 输出存管函
     */
    public function showPDF(){
        $id = I('fid');
        $sUrl = 'https://evidence-management.e-chains.cn/api/check_certificate';
        $aData = [
            'tab'        => 'check',
            'type'       => 'wpes',
            'id'         => $id,
            'access_id'  => '15565244962745QrTgfuzvUc55e4f7a7ecbe1d76e94ead56cfa4bdbe9195',
            'access_key' => base64_encode('15565244962745YyjvSTuHZae1eef14722421f4f9f9ae1937a3e1db32628')
        ];
        $resInfo = http($sUrl,$aData,'POST',false,10,false);
        //$pdf = pdf($resInfo);
        //$pdf = pdfFromHtml($resInfo);
        //$pdf = pdf(html_entity_decode($resInfo));//生成pdf并上传到服务器
        //$pdf_url = A('Common/AliyunOss','Model')->up_pdf('LOG/mpdf.pdf');//上传阿里云并返回url
        //echo $pdf_url;
        echo $resInfo;
    }

    /**
     * 输出存管函_20190529
     */
    public function showPDF_20190529(){
        $id = I('fid');
        $sUrl = 'https://evidence-management.e-chains.cn/api/check_certificate';
        $aData = [
            'tab' => 'check',
            'type' => 'wpes',
            'id' => $id,
            'name' => '13600522308',
            'password' => hash('sha256','hzsj2019')
        ];
        $resInfo = http($sUrl,$aData,'POST',false,10,false);
        echo $resInfo;
    }

    /**
     * 输出存管函_20190528
     */
    public function showPDF_20190528(){
        // $fid = $this->P['fid'];
        $fid = I('fid');
        $sUrl = 'https://evidence-management.e-chains.cn/ues_evidence/'.$fid.'/pdf';
        $aData = [];
        // $sCookie = '_session=eyJpdiI6Ik9v';
        $sCookie = $this->getSession();
        $aHeader = [
            'Cookie: '.$sCookie,
        ];
        $resInfo = http($sUrl,$aData,'GET',$aHeader,10);
        echo $resInfo;
    }

    /**
     * 获取Session,用于模拟登录
     */
    protected function getSession(){
        $sUrl = 'https://evidence-management.e-chains.cn/ad_go_login';
        $aData = [
            'type' => 1,
            'name' => '13600522308',
            'password' => base64_encode('hzsj2019')
        ];
        $resInfo = http($sUrl,$aData,'POST',false,10,true);
        // 在返回的头信息中匹配出_session=.*得到Cookie
        preg_match('/(_session=.*?);/',$resInfo,$headerArr);
        return $headerArr[1];
    }
}