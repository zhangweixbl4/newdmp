<?php
namespace TaskInput\Controller;

use TaskInput\Model\TaskInputModel;

class IllegalAdReviewController extends TraditionAdController {
    private $mediaClassMap = [
        '1' => [
            'str' => 'tv',
            'table' => 'ttvsample',
            'primaryKey' => 'fid',
        ],
        '2' => [
            'str' => 'bc',
            'table' => 'tbcsample',
            'primaryKey' => 'fid',
        ],
        '3' => [
            'str' => 'paper',
            'table' => 'tpapersample',
            'primaryKey' => 'fpapersampleid',
        ],
    ];
    const CUSTOMER_AUTH_CODE = '3001';
    const DMP_AUTH_CODE = '3002';
    private $userType = '';
    private $authArr = [];
    public function _initialize()
    {
        parent::_initialize();

        $wx_id = session('wx_id');
        $userGroupInfo = M('ad_input_user_group')
            ->field('ad_input_user_group.other_authority')
            ->join('ad_input_user on ad_input_user.group_id = ad_input_user_group.group_id')
            ->where(array('ad_input_user.wx_id'=>$wx_id))
            ->find();
        $other_authority_arr = 	explode(',',$userGroupInfo['other_authority']);
        $this->authArr = $other_authority_arr;
        $referUrl =  explode('/', $_SERVER['HTTP_REFERER']);
        $action = array_pop($referUrl);
        if(	in_array(self::CUSTOMER_AUTH_CODE , $other_authority_arr) && ($action == 'illegalReview' || ACTION_NAME == 'illegalReview')){
            $this->userType = 'customer';
        }elseif(in_array(self::DMP_AUTH_CODE , $other_authority_arr)  && ($action == 'illegalConfirm' || ACTION_NAME == 'illegalConfirm')){
            $this->userType = 'dmp';
        }else{
            //echo '<h1>无权限,请联系管理员</h1>';
            //exit();
        }

    }

    /**
     * 获取用户地区权限
     * @return array
     */
    private function getUserRegionAuth()
    {
        $wx_id = session('wx_id');
        $regionIds = M('ad_input_user')
            ->join('ad_input_user_group on ad_input_user.group_id = ad_input_user_group.group_id')
            ->where(['wx_id' => ['EQ', $wx_id]])
            ->getField('regionauthority');
        $regionWhere = [
            'flevel' => ['EQ', 1]
        ];
        if ($regionIds == 'unrestricted'){  // 媒体权限是无限制
            $regionArr = M('tregion')->cache(true)->field('ffullname label, left(fid, 2) value')->where($regionWhere)->select();
        }elseif (!$regionIds){  // 未查到媒体权限
            $regionArr = [];
        }else{  // 有媒体权限
            $temp = json_decode($regionIds, true);
            if (!$temp){
                $regionArr = [];
            }else{
                $regionArr = [];
                foreach ($temp as $key => $value) {
                    $regionArr[$key]['value'] = $value;
                    // 检查是否达到了6位
                    $regionLen = strlen($value);
                    if (6 - $regionLen != 0){
                        $value = $value . str_repeat('0', 6 - $regionLen );
                        $regionWhere = ["fid" => ['EQ', $value]];
                        $regionArr[$key]['label'] = M('tregion')->cache(true)->where($regionWhere)->getField('ffullname');
                    }else{
                        $regionWhere = ["fid" => ['EQ', $value]];
                        $regionArr[$key]['label'] = M('tregion')->cache(true)->where($regionWhere)->getField('ffullname') . '(本级)';
                    }

                }
            }
        }
        return $regionArr;
    }

    /**
     * 获取完整的小表名称
     * @param $mediaClass
     * @param $month
     * @param $region
     * @return string
     */
    private function returnIssueTableName($mediaClass, $month, $region='')
    {
        $region = substr($region, 0, 2);
        $mediaClassStr = $this->mediaClassMap[$mediaClass]['str'];
        $tableName = "t{$mediaClassStr}issue_{$month}_$region";
        $hasTable = M()->query("show tables like '$tableName'");
        return $hasTable ? $tableName : false;
    }

    /**
     * 处理电视和广播广告的发布表子查询
     * @param $mediaClass
     * @param $month
     * @param $region
     * @param array $where
     * @return bool
     */
    public function processIssueTableSubQuery($mediaClass, $month, $region, $where=[])
    {
        if ($mediaClass == 1){
            $flabelid = 243;
        }elseif ($mediaClass == 2){
            $flabelid = 245;
        }elseif ($mediaClass == 3){
            $flabelid = 244;
        }
        $tableName = $this->returnIssueTableName($mediaClass, $month, $region);
        if (!$tableName){
            return false;
        }
        if ($this->userType === 'dmp'){
            // 国家局限定媒体
            $guojiajuMediaIds = M('tmedialabel')
                ->field('fmediaid')
                ->cache(true, 600)
                ->where([
                    'fstate' => ['EQ', 1],
                    'flabelid' => ['EQ', $flabelid],
                ])
                ->getField('fmediaid', true);
            // 国家局日期限制
            $dateCondition = $this->processZongjuCondition($month,$mediaClass);
            if (!$dateCondition || !$tableName){
                return false;
            }
            $where['issue.fmediaid'][] = ['IN', $guojiajuMediaIds];
            $where["from_unixtime(issue.fissuedate, '%Y-%m-%d')"] = ['IN', $dateCondition];
        }
        // 如果地区id大于2位, 就不是省级
        $regionIdLen = strlen($region);
        if ($regionIdLen > 2){
            $mediaOwnerSubQuery = M('tmediaowner')->field('fid')->where(["LEFT(fregionid, $regionIdLen)" => ['EQ', $region]])->buildSql();
            $mediaSubQuery = M('tmedia')->field('fid')->where(['fmediaownerid' => ['EXP', ' IN '.$mediaOwnerSubQuery]])->buildSql();
            $where['issue.fmediaid'][] = ['EXP', ' IN '.$mediaSubQuery];
        }
        if ($this->userType === 'dmp') {
            $where['review.media_id'] = ['EXP', 'IS NULL'];
        }
        $issueSubQuery = M($tableName)
            ->alias('issue')
            ->field('fsampleid')
            ->join('LEFT JOIN illegal_ad_issue_review review on issue.fmediaid = review.media_id AND issue.fstarttime = review.start_time')
            ->where($where)
            ->group('fsampleid')
            ->buildSql();
        return $issueSubQuery;
    }

    /**
     * 查看每个省级行政区的电视广播违法广告数量
     * @param $month
     * @param $mediaClass
     * @return array
     */
    public function getIllegalTVBCAdCountByMonth($month, $mediaClass)
    {
        $regionArr = $this->getUserRegionAuth();
        foreach ($regionArr as $key => $region) {
            $issueSubQuery = $this->processIssueTableSubQuery($mediaClass, $month, $region['value']);
            if ($issueSubQuery){
                $regionArr[$key]['count'] = M($this->mediaClassMap[$mediaClass]['table'])
                    ->where([
                        'fillegaltypecode' => ['NEQ', '0'],
                        'fid' => ['EXP', ' IN '.$issueSubQuery],
                    ])
                    ->count();
            }else{
                $regionArr[$key]['count'] = 0;
            }
        }
        return $regionArr;

    }

    public function getIllegalTVBCAdCountByMonthAndRegionId()
    {
        
    }

    /**
     * 查看每个省级行政区的报纸违法广告数量
     * @param $month
     * @return array
     */
    public function getIllegalPaperAdCountByMonth($month)
    {
        $baseConditionDate = $month;
        $year = substr($month, 0, 4);
        $month = substr($month, 4, 2);
        $month = "$year-$month";
        $regionArr = $this->getUserRegionAuth();
        $mediaWhere = [];
        $sampleWhere = [
            'issue.fillegaltypecode' => ['NEQ', 0],
            'fissuedate' => [
                ['LIKE', "$month%"]
            ],
        ];
        if ($this->userType === 'dmp'){
            $sampleWhere['review.media_id'] = ['EXP', 'is null'];
            $dateCondition = $this->processZongjuCondition($baseConditionDate,3);
            // 国家局限定媒体
            $guojiajuMediaIds = M('tmedialabel')
                ->field('fmediaid')
                ->where([
                    'fstate' => ['EQ', 1],
                    'flabelid' => ['EQ', 244],
                ])
                ->getField('fmediaid', true);
            $mediaWhere['fid'] = ['IN', $guojiajuMediaIds];
            $sampleWhere['fissuedate'][] = ['IN', $dateCondition];
        }

        foreach ($regionArr as $key => $region) {
            if ($this->userType === 'dmp' && !$dateCondition){
                $regionArr[$key]['count'] = 0;
            }else{
                // 如果这个行政区划代码为6位, 就是只有这一级的权限
                if (strlen($region['value']) == 6){
                    $mediaOwnerSubQuery = M('tmediaowner')
                        ->field('fid')
                        ->where([
                            "fregionid" => ['EQ',$region['value']]
                        ])
                        ->buildSql();
                }else{
                    $region_id_rtrim = A('Common/System','Model')->get_region_left($region['value']);//地区ID去掉末尾的0
                    $region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
                    $mediaOwnerSubQuery = M('tmediaowner')
                        ->field('fid')
                        ->where([
                            "LEFT(fregionid, $region_id_strlen)" => ['EQ',$region_id_rtrim]
                        ])
                        ->buildSql();
                }

                $mediaWhere['fmediaownerid'] = ['EXP', ' IN '.$mediaOwnerSubQuery];
                $mediaSubQuery = M('tmedia')
                    ->field('fid')
                    ->where($mediaWhere)
                    ->buildSql();
                $sampleWhere['fmediaid'] = ['EXP', ' IN '.$mediaSubQuery];
                $regionArr[$key]['count'] = M('tpapersample')
                    ->alias('issue')
                    ->join('LEFT JOIN illegal_ad_issue_review review on issue.fmediaid = review.media_id AND issue.fjpgfilename = review.paper_jpg')
                    ->where($sampleWhere)
                    ->count();
            }
        }
        return $regionArr;
    }

    /**
     * 获取每个地区的违法广告数据的基础方法. 因为要分报纸和其他媒体
     * @param $month
     * @param $mediaClass
     */
    public function checkIllegalAdCountByMonth($month, $mediaClass)
    {
        if ($mediaClass == 3){
            $res = $this->getIllegalPaperAdCountByMonth($month);
        }else{
            $res = $this->getIllegalTVBCAdCountByMonth($month, $mediaClass);
        }
        $this->ajaxReturn([
            'data' => $res
        ]);
    }

    public function illegalReview(){
        

		$this->assign('getRegionTree',A('Common/Region','Model')->getRegionTree());
		$this->assign('getAdClassArr',A('Common/AdClass','Model')->getAdClassArr());
		$this->display();
    }
	
	/*搜索列表*/
	public function search_issue(){
		$p = I('p',1);//当前第几页
		$pp = I('pp',20);//每页显示多少记录
		
		$issuedate_min = I('issuedate_min');//查询开始日期
		$issuedate_max = I('issuedate_max');//查询结束日期
		$sx = I('sx');
		$fadname = I('fadname');
		$fmedianame = I('fmedianame');
		$fregionid = I('fregionid');
		$this_region_id = I('this_region_id');
		$where = array();
		

		$where['sam_id'] = array('gt',0);
		$where['issue_date'] = array('between',array($issuedate_min,$issuedate_max));
		
		if($sx !== ''){
			$where['type'] = $sx;
		}
		if($fadname){
			$where['illegal_ad_issue_review.ad_name'] = array('like','%'.$fadname.'%');
		}
		if($fmedianame){
			$where['tmedia.fmedianame'] = array('like','%'.$fmedianame.'%');
		}
		
		if($fregionid){
			if($this_region_id == 'true'){
				$where['tmedia.media_region_id'] = $fregionid;//地区搜索条件
			}elseif($fregionid != 100000){
				$region_id_rtrim = A('Common/System','Model')->get_region_left($fregionid);//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmedia.media_region_id,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}
		}
		
		$dataList = M('illegal_ad_issue_review')
											->field('
														tmedia.fmedianame,
														illegal_ad_issue_review.media_id,
														illegal_ad_issue_review.ad_name,
														illegal_ad_issue_review.sam_id,
														count(*) as count,
														count(case when type = 0 then 1 else null end) as count0,
														count(case when type = 1 then 1 else null end) as count1,
														count(case when type = -1 then 1 else null end) as count_1
														
														
														
														')
											->join('tmedia on tmedia.fid = illegal_ad_issue_review.media_id')
											->where($where)
											->group('illegal_ad_issue_review.media_id,illegal_ad_issue_review.sam_id')
											
											->page($p,$pp)
											->select();
		$total = M('illegal_ad_issue_review')
											->field('count(distinct illegal_ad_issue_review.media_id,illegal_ad_issue_review.sam_id) as total')
											->join('tmedia on tmedia.fid = illegal_ad_issue_review.media_id')
											->where($where)
											
											
											->select();									
		//var_dump( M('illegal_ad_issue_review')->getLastSql());
		//var_dump($total[0]['total']);
		$this->ajaxReturn(array('code'=>0,'msg'=>'','dataList'=>$dataList,'total'=>intval($total[0]['total']),'issuedate_min'=>$issuedate_min,'issuedate_max'=>$issuedate_max));
	
	}
	
	
	/*违法广告详情*/
	public function info(){
		
		$media_id = I('media_id');
		$sam_id = I('sam_id');
		
		$issuedate_min = I('issuedate_min');//查询开始日期
		$issuedate_max = I('issuedate_max');//查询结束日期
		
		
		$mediaInfo = M('tmedia')->field('tmedia.fid,fmedianame,left(tmedia.fmediaclassid,2) as media_class')->where(array('fid'=>$media_id))->find();
		

		
		$where = array();
		$where['media_id'] = $media_id;
		$where['sam_id'] = $sam_id;
		$where['issue_date'] = array('between',array($issuedate_min,$issuedate_max));
		
		$issueList = M('illegal_ad_issue_review')
												->field('start_time,end_time,issue_date,type')
												->where($where)
												->select();
		
		if($mediaInfo['media_class'] == '01'){
			$samInfo = M('ttvsample')
								
								->field('
											ttvsample.fillegalcontent,
											ttvsample.fexpressioncodes,
											tadclass.ffullname as adclass,
											tad.fadname,
											tad.fadclasscode,
											ttvsample.favifilename as sam_filename

										')
								->join('tad on tad.fadid = ttvsample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->where(array('fid'=>$sam_id))
								->find();
		}elseif($mediaInfo['media_class'] == '02'){
			$samInfo = M('tbcsample')
								
								->field('
											tbcsample.fillegalcontent,
											tbcsample.fexpressioncodes,
											tadclass.ffullname as adclass,
											tad.fadname,
											tad.fadclasscode,
											tbcsample.favifilename as sam_filename

										')
								->join('tad on tad.fadid = tbcsample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->where(array('fid'=>$sam_id))
								->find();
		}elseif($mediaInfo['media_class'] == '03'){
			$samInfo = M('tpapersample')
								
								->field('
											tpapersample.fillegalcontent,
											tpapersample.fexpressioncodes,
											tadclass.ffullname as adclass,
											tad.fadname,
											tad.fadclasscode,
											tpapersample.fjpgfilename as sam_filename

										')
								->join('tad on tad.fadid = tpapersample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->where(array('fpapersampleid'=>$sam_id))
								->find();
		}else{
			
		}
		
												
												
		$this->ajaxReturn(array('code'=>0,'msg'=>'','mediaInfo'=>$mediaInfo,'issueList'=>$issueList,'samInfo'=>$samInfo));								
		
		
		
		
		
	}
	
	

    public function illegalConfirm()
    {
        if (IS_POST){
            $page = I('page', 1);
            $pageSize = I('pageSize', 10);
            $formData = I('where');
            $condition = I('condition');
            $month = $condition['month'];
            $mediaClass = $condition['mediaClass'];
            $region = $condition['region'];
            if (!$month || !$mediaClass || !$region){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '基础搜索数据不全'
                ]);
            }
            if ($mediaClass == 3){
                $this->paperIllegalAdSearch($month, $region, $formData, $page, $pageSize);
            }else{
                $this->tvbcIllegalAdSearch($month, $mediaClass, $region, $formData, $page, $pageSize);
            }
        }else{
            $illegalTypeArr = M('tillegaltype')->field('fcode value, fillegaltype text')->cache(true)->where(['fstate' => ['EQ', 1], ['fcode' => ['NEQ', 0]]])->select();
            $illegalTypeArr = json_encode($illegalTypeArr);
            $adClassArr = json_encode($this->getAdClassArr());
            $adClassTree = json_encode($this->getAdClassTree());
            $regionArr = $this->getUserRegionAuth();
            $regionArr = json_encode($regionArr);
            $userType = 'dmp';
            $this->assign(compact('illegalTypeArr', 'adClassArr', 'regionArr', 'adClassTree', 'userType'));
            $this->display('illegalReview');
        }
    }

    private function processAdSubQuery($formData)
    {
        $adWhere = [];
        if ($formData['adClass'] != ''){
            $len = strlen($formData['adClass']);
            $adWhere["LEFT(fadclasscode, $len)"] = ['EQ', $formData['adClass']];
        }
        if ($formData['adClass2'] != ''){
            $len = strlen($formData['adClass2']);
            $adWhere["LEFT(fadclasscode_v2, $len)"] = ['EQ', $formData['adClass2']];
        }
        if ($formData['adName'] != ''){
            $adWhere['fadname'] = ['LIKE', '%'.$formData['adName'].'%'];
        }
        if ($adWhere != []){
            $adSubQuery = M('tad')
                ->field('fadid')
                ->where($adWhere)
                ->buildSql();
            return $adSubQuery;
        }else{
            return false;
        }
    }

    private function processMediaOwnerSubQuery($formData, $region)
    {
        $mediaOwnerWhere = [];
        // 处理地区搜索
        if ($formData['regionId'] != ''){
            if ($formData['onlyThisLevel'] == 1){
                $mediaOwnerWhere['fregionid'] = ['EQ', $formData['regionId']];
            }else{
                $region_id_rtrim = A('Common/System','Model')->get_region_left($formData['regionId']);//地区ID去掉末尾的0
                $region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
                $mediaOwnerWhere["LEFT(fregionid, $region_id_strlen)"] = ['EQ', $region_id_rtrim];
            }
        }elseif ($formData['onlyThisLevel'] == 1){
            $mediaOwnerWhere['fregionid'] = ['EQ', $region.str_repeat('0', 6-strlen($region))];
        }else{
            $regionLen = strlen($region);
            $mediaOwnerWhere["LEFT(fregionid, $regionLen)"] = ['EQ', $region];
        }
        $mediaOwnerSubQuery = M('tmediaowner')
            ->field('fid')
            ->where($mediaOwnerWhere)
            ->buildSql();
        return $mediaOwnerSubQuery;
    }

    private function processMediaSubQuery($formData, $region)
    {
        $mediaOwnerSubQuery = $this->processMediaOwnerSubQuery($formData, $region);
        $mediaWhere = [];
        $mediaWhere['fmediaownerid'] = ['EXP', ' IN ' . $mediaOwnerSubQuery];
        if ($formData['mediaName'] != ''){
            $mediaWhere['fmedianame'] = ['LIKE', '%'.$formData['mediaName'].'%'];
        }
        $mediaSubQuery = M('tmedia')
            ->field('fid')
            ->where($mediaWhere)
            ->buildSql();
        return $mediaSubQuery;
    }

    /**
     * @param $month
     * @param $mediaClass
     * @param $region
     * @param $formData
     * @param int $page
     * @param int $pageSize
     */
    private function tvbcIllegalAdSearch($month, $mediaClass, $region, $formData, $page=1, $pageSize=10)
    {
        $tableName = $this->returnIssueTableName($mediaClass, $month, $region);
        if (!$tableName){
            $table = [];
            $total = 0;
        }else{
            // 处理媒体和发布记录子查询
            $issueWhere = [];
            $mediaSubQuery = $this->processMediaSubQuery($formData, $region);
            $issueWhere['issue.fmediaid'][] = ['EXP', ' IN '. $mediaSubQuery];
            $issueSubQuery = $this->processIssueTableSubQuery($mediaClass, $month, $region, $issueWhere);

            $sampleWhere = [
                'sample.fillegaltypecode' => ['NEQ', 0],
                'fid' => ['EXP', ' IN '.$issueSubQuery],
            ];
            $adSubQuery = $this->processAdSubQuery($formData);
            if ($adSubQuery){
                $sampleWhere['sample.fadid'] = ['EXP', 'IN '. $adSubQuery]; // 广告搜索条件
            }
            if ($formData['fillegaltypecode'] != ''){
                $sampleWhere['sample.fillegaltypecode'] = ['EQ', $formData['fillegaltypecode']];
            }
            $table = M($this->mediaClassMap[$mediaClass]['table'])
                ->alias('sample')
                ->field('fid, sample.fadid, fmediaid, sample.fexpressioncodes, sample.fexpressions')
                ->where($sampleWhere)
                ->order('sample.fmediaid, sample.fadid')
                ->page($page, $pageSize)
                ->select();
            // 统计违法发布条次
            $sampleIds = [];
            foreach ($table as $item) {
                $sampleIds[] = $item['fid'];
            }
            if ($sampleIds){
                $issueCountWhere = [
                    'fsampleid' => ['IN', $sampleIds],
                ];
                if ($this->userType === 'dmp'){
                    $issueCountWhere['review.start_time'] = ['EXP', 'IS NULL'];
                    $dateCondition = $this->processZongjuCondition($month,$mediaClass);
                    if (!$dateCondition){
                        $table = [];
                        $total = 0;
                        $this->ajaxReturn(compact('table', 'total'));
                    }else{
                        $issueCountWhere["from_unixtime(issue.fissuedate, '%Y-%m-%d')"] =  ['IN', $dateCondition];
                    }
                }
                $issueCount = M($tableName)
                    ->alias('issue')
                    ->field('count(issue.fsampleid) count, fsampleid')
                    ->where($issueCountWhere)
                    ->join('LEFT JOIN illegal_ad_issue_review review on issue.fmediaid = review.media_id AND issue.fstarttime = review.start_time')
                    ->group('fsampleid')
                    ->having('count > 0')
                    ->select();
                $issueCountMap = [];
                foreach ($issueCount as $item) {
                    $issueCountMap[$item['fsampleid']] = $item['count'];
                }
            }else{
                $issueCountMap = [];
            }

            // 组装数据
            foreach ($table as $key => $value) {
                $tempAdInfo = M('tad')->field('fadname, fadclasscode, fadclasscode_v2')->find($value['fadid']);
                $table[$key]['fadname'] = r_highlight($tempAdInfo['fadname'], $formData['adName'], 'red');
                $table[$key]['adClass'] = M('tadclass')->cache(true)->where(['fcode' => ['EQ', $tempAdInfo['fadclasscode']]])->getField('ffullname');
                $table[$key]['adClass2'] = M('hz_ad_class')->cache(true)->where(['fcode' => ['EQ', $tempAdInfo['fadclasscode_v2']]])->getField('ffullname');
                $table[$key]['mediaName'] = M('tmedia')->cache(true)->where(['fid' => ['EQ', $value['fmediaid']]])->getField('fmedianame');
                $table[$key]['mediaName'] = r_highlight($table[$key]['mediaName'], $formData['mediaName'], 'red');
                $table[$key]['fexpressions'] =  array_filter(explode(' ;; ', $table[$key]['fexpressions']));
                $table[$key]['count'] = $issueCountMap[$value['fid']];
                $table[$key]['region'] = $this->getRegionNameByMediaId($value['fmediaid']);
            }
            // 获取总数
            $total = M($this->mediaClassMap[$mediaClass]['table'])
                ->alias('sample')
                ->where($sampleWhere)
                ->count();
        }
        $this->ajaxReturn(compact('table', 'total'));
    }

    public function getIllegalAdInfo()
    {
        $condition = I('condition');
        $month = $condition['month'];
        $mediaClass = $condition['mediaClass'];
        $region = $condition['region'];
        $sampleId = I('sampleId');
        if (!$month || !$mediaClass || !$region || !$sampleId){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '基础搜索数据不全'
            ]);
        }
        // 获取违法发布记录和样本详情
        if ($mediaClass != 3) {
            $issueList = $this->getTvBcIllegalIssue($month, $mediaClass, $region, $sampleId);
            $sampleInfo = M($this->mediaClassMap[$mediaClass]['table'])->find($sampleId);
            // 是否已经提交到国家局
            $tableName = $this->returnIssueTableName($mediaClass, $month, $region);

            $sampleInfo['zongjuCount'] = M('illegal_ad_issue_review')
                ->alias('review')
                ->join("$tableName as issue on issue.fmediaid = review.media_id AND issue.fstarttime = review.start_time")
                ->count();
        }else{
            $sampleInfo = M($this->mediaClassMap[$mediaClass]['table'])->find($sampleId);
            $sampleInfo['paperUrl'] = A('Common/Paper','Model')->get_slim_source($sampleId);
            $sampleInfo['fid'] = $sampleInfo['fpapersampleid'];
            $issueList = [[
                'fissuedate' => strtotime($sampleInfo['fissuedate']),
                'fmediaid' => $sampleInfo['fmediaid'],
                'issue_date' => $sampleInfo['fissuedate'],
                'paper_jpg' => $sampleInfo['fjpgfilename'],
            ]];
            $sampleInfo['zongjuCount'] = M('illegal_ad_issue_review')
                ->where([
                    'paper_jpg' => $sampleInfo['fjpgfilename'],
                    'media_id' => $sampleInfo['fmediaid'],
                ])
                ->count();
        }
        $sampleInfo['issueList'] = $issueList;
        $sampleInfo['mediaClass'] = $mediaClass;
        $tempMediaInfo = M('tmedia')->cache(true)->field('fmedianame, fjpgfilename')->find($sampleInfo['fmediaid']);
        $sampleInfo['media_name'] = $tempMediaInfo['fmedianame'];
        $sampleInfo['media_icon'] = $tempMediaInfo['fjpgfilename'];
        $tempAdInfo = M('tad')->field('fadname, fadclasscode, fadclasscode_v2, fadowner, fbrand')->find($sampleInfo['fadid']);
        $sampleInfo['fadname'] = $tempAdInfo['fadname'];
        $sampleInfo['adClass'] = M('tadclass')->cache(true)->where(['fcode' => ['EQ', $tempAdInfo['fadclasscode']]])->getField('ffullname');
        $sampleInfo['adClass2'] = M('hz_ad_class')->cache(true)->where(['fcode' => ['EQ', $tempAdInfo['fadclasscode_v2']]])->getField('ffullname');
        $sampleInfo['fadowner'] = M('tadowner')->cache(true)->where(['fid' => ['EQ',$tempAdInfo['fadowner']]])->getField('fname');
        $sampleInfo['fbrand'] = $tempAdInfo['fbrand'];
        $sampleInfo['commentList'] = M('illegal_review_record')->where(['sample_id' => ['EQ', $sampleId], 'ad_class' => ['EQ', $mediaClass]])->order('update_time desc')->select();
        foreach ($sampleInfo['commentList'] as &$datum) {
            if ($datum['is_confirm'] == 2){
                $tempArr = explode(' ;; ', $datum['review_comment']);
                $datum['review_comment'] = $tempArr[0];
                $datum['back_comment'] = $tempArr[1];
            }
            $datum['update_time'] = date('Y-m-d H:i:s', $datum['update_time']);
        }
        $this->ajaxReturn([
            'code' => 0,
            'data' => $sampleInfo,
        ]);
    }

    /**
     * 判断确认记录的type值
     * @param $samId
     * @param $mediaClass
     * @param $mediaId
     * @param $item     提交的项
     * @return int
     */
    private function confirmRecordType($samId, $mediaClass, $mediaId, $item)
    {
        // 检查是否在媒体id里面
        $specialLabel = sys_c('illegal_ad_confirm_special_media_label');
        $res = M('tmedialabel')
            ->where([
                'fstate' => ['EQ', 1],
                'fmediaid' => ['IN', [$mediaId]],
                'flabelid' => ['EXP', " IN (select flabelid from tlabel where FIND_IN_SET(flabel,'$specialLabel'))"]
            ])
            ->count();
        if (!$res){
            return 1;
        }
//        // 找到在违法广告表中的id
//        $illegalAdId = M('tbn_illegal_ad')
//            ->where([
//                'fmedia_class' => ['EQ', $mediaClass],
//                'fsample_id' => ['EQ', $samId],
//            ])
//            ->getField('fid');
//        // 查找违法广告发布记录
//        if ($illegalAdId){
//            // 如果是报纸, 就说明日报里已经通报了, 可以上报
//            if ($mediaClass == 3){
//                return 1;
//            }
//            $where = [
//                'fmedia_id' => ['EQ', $mediaId],
//                'fstarttime' => ['EQ', date('Y-m-d H:i:s', $item['start_time'])],
//                'fillegal_ad_id' => ['EQ', $illegalAdId],
//            ];
//            $res = M('tbn_illegal_ad_issue')
//                ->where($where)
//                ->count();
//            if ($res){
//                return 1;
//            }else{
//                return 0;
//            }
//        }else{
//            // 如果找不到id, 就不确认
//            return 0;
//        }
        // 获取任务id
        $taskWhere = [
            'task_state' => ['EQ', 2],
            'syn_state' => ['EQ', 2],
            'cut_err_state' => ['EQ', 0],
            'media_class' => ['EQ', $mediaClass],
            'fillegaltypecode' => ['NEQ', 0],
            '_string' => "unix_timestamp( issue_date ) + (86400 * 2) < ( SELECT flow_time FROM task_input_flow WHERE taskid = task_input.taskid AND flow_content IN ( '完成任务', '完成广告信息录入', '完成违法审核' ) ORDER BY flow_time ASC LIMIT 1 )"
        ];
        if ($mediaClass == 3){
            $taskWhere['sam_id'] = ['EQ', $samId];
        }else{
            $tableName = $this->mediaClassMap[$mediaClass]['table'];
            $sampleSubQuery = M($tableName)->field('uuid')->where(['fid' => ['EQ', $samId]])->buildSql();
            $taskWhere['sam_uuid'] = ['EXP', ' IN '.$sampleSubQuery];
        }
        $res = M('task_input')->where($taskWhere)->count();
        return $res ? 0 : 1;
    }

    /**
     * 确认违法广告发布记录
     * @param $issueList
     */
    public function confirmIssue($issueList)
    {
        if ($this->userType !== 'dmp'){
            $this->ajaxReturn([
                'code' => -2,
                'msg' => '你没有权限确认'
            ]);
        }
        $time = time();
        $wx_id = session('wx_id');
        $adInfo = I('adInfo');
        // 找到需要确认
        foreach ($issueList as $key => $value) {
            $issueList[$key]['create_time'] = $time;
            $issueList[$key]['wx_id'] = $wx_id;
            if ($adInfo['mediaClass'] == 3){
                if ($value['type'] == 1){
                    $issueList[$key]['type'] = $this->confirmRecordType($adInfo['fpapersampleid'], $adInfo['mediaClass'], $adInfo['fmediaid'], $value);
                    
					//$issueList[$key]['ad_name'] = $this->get_ill_ad($value['media_id'],$value['issue_date'],$value['start_time'],$value['paper_jpg']);//查询广告名称
					if ($issueList[$key]['type'] == 0){
                        $issueList[$key]['remark'] = '该记录未在提交客户的日报中';
                    }
                }else{
                    $issueList[$key]['remark'] = $value['reason'];
                }
            }else{
                if ($value['type'] == 1){
                    $issueList[$key]['type'] = $this->confirmRecordType($adInfo['fid'], $adInfo['mediaClass'], $adInfo['fmediaid'], $value);
                    //$issueList[$key]['ad_name'] = $this->get_ill_ad($value['media_id'],$value['issue_date'],$value['start_time'],$value['paper_jpg']);//查询广告名称
					if ($issueList[$key]['type'] == 0){
                        $issueList[$key]['remark'] = '该记录未在提交客户的日报中';
                    }
                }else{
                    $issueList[$key]['remark'] = $value['reason'];
                }
            }
        }
        $res = M('illegal_ad_issue_review')->addAll($issueList);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '更新成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '更新失败'
            ]);
        }
    }

    /**
     * 根据样本id批量确认违法广告发布记录
     * @param $ids  array 样本id
     * @param $condition
     * @param $type
     * @param string $reason    退回的理由
     */
    public function batchConfirm($ids, $condition, $type, $reason='')
    {
        if ($this->userType !== 'dmp'){
            $this->ajaxReturn([
                'code' => -2,
                'msg' => '你没有权限确认'
            ]);
        }
        $month = $condition['month'];
        $mediaClass = $condition['mediaClass'];
        $region = $condition['region'];
        if (!$month || !$mediaClass || !$region || !$ids){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '基础搜索数据不全'
            ]);
        }
        $wx_id = session('wx_id');
        $dateCondition = $this->processZongjuCondition($month,$mediaClass);
        if ($mediaClass == 3){
            $addData = M('tpapersample')
                ->field('fpapersampleid, fmediaid media_id, fissuedate issue_date, fjpgfilename paper_jpg')
                ->where([
                    $this->mediaClassMap[$mediaClass]['primaryKey'] => ['IN', $ids],
                    'fissuedate' => ['IN', $dateCondition],
                ])
                ->select();
            $time = time();
            foreach ($addData as $key => $value) {
                $addData[$key]['type'] = $type;
                $addData[$key]['create_time'] = $time;
                $addData[$key]['wx_id'] = $wx_id;
                if ($type == 1){
                    $addData[$key]['type'] = $this->confirmRecordType($value['fpapersampleid'], 3, $value['media_id'], $value);
                    if ($addData[$key]['type'] == 0){
                        $addData[$key]['remark'] = '该记录未在提交客户的日报中';
                    }
                }else{
                    $addData[$key]['remark'] = $reason;
                }
				$addData[$key]['ad_name'] = $this->get_ill_ad($value['media_id'],$value['issue_date'],0,$value['paper_jpg']);//查询广告名称
                unset($addData[$key]['fpapersampleid']);
            }
        }else{
            foreach ($dateCondition as $key => $value) {
                $dateCondition[$key] = strtotime($value);
            }
            $tableName = $this->returnIssueTableName($mediaClass, $month, $region);
            $addData = M($tableName)
                ->field('fid, fmediaid media_id, fstarttime start_time, fissuedate')
                ->where([
                    'fsampleid' => ['IN', $ids],
                    'fissuedate' => ['IN', $dateCondition],
                ])
                ->select();
            $time = time();
            foreach ($addData as $key => $value) {
                $addData[$key]['type'] = $type;
                $addData[$key]['create_time'] = $time;
                $addData[$key]['issue_date'] = date('Y-m-d', $value['fissuedate']);
                $addData[$key]['wx_id'] = $wx_id;
                if ($type == 1){
                    $addData[$key]['type'] = $this->confirmRecordType($value['fid'], 3, $value['media_id'], $value);
                    if ($addData[$key]['type'] == 0){
                        $addData[$key]['remark'] = '该记录未在提交客户的日报中';
                    }
                }else{
                    $addData[$key]['remark'] = $reason;
                }
				$addData[$key]['ad_name'] = $this->get_ill_ad($value['media_id'],date('Y-m-d', $value['fissuedate']),$value['start_time']);//查询广告名称
					
                unset($addData[$key]['fid']);
            }
        }
        $res = M('illegal_ad_issue_review')->addAll($addData);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '更新成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '更新失败'
            ]);
        }
    }


    /**
     * 获取电视和广播的违法广告发布记录
     * @param $month
     * @param $mediaClass
     * @param $region
     * @param $sampleId
     * @return array
     */
    public function getTvBcIllegalIssue($month, $mediaClass, $region, $sampleId)
    {
        $tableName = $this->returnIssueTableName($mediaClass, $month, $region);
        if (!$tableName){
            $issueList = [];
        }else{
            $where = [
                'fsampleid' => ['EQ', $sampleId],
            ];
            if ($this->userType === 'dmp'){
                $where['review.start_time'] = ['EXP', 'IS NULL'];
                // 国家局日期限制
                $dateCondition = $this->processZongjuCondition($month,$mediaClass);
                if ($dateCondition){
                    $where["from_unixtime(issue.fissuedate, '%Y-%m-%d')"] = ['IN', $dateCondition];
                }else{
                    return [];
                }
            }

            $issueList = M($tableName)
                ->alias('issue')
                ->field('fmediaid, fstarttime, fendtime, fissuedate')
                ->where($where)
                ->join('LEFT JOIN illegal_ad_issue_review review on issue.fmediaid = review.media_id AND issue.fstarttime = review.start_time')
                ->select();
            foreach ($issueList as $key => $value) {
                $issueList[$key]['issue_date'] = date('Y-m-d', $value['fissuedate']);
                $issueList[$key]['startTime'] = date('Y-m-d H:i:s', $value['fstarttime']);
                $issueList[$key]['endTime'] = date('Y-m-d H:i:s', $value['fendtime']);
            }
        }
        return $issueList;
    }

    /**
     * 根据媒体id获得行政区划全名
     * @param $mediaId
     * @return mixed
     */
    private function getRegionNameByMediaId($mediaId)
    {
        $mediaSubQuery = M('tmedia')
            ->field('fmediaownerid')
            ->where([
                'fid' => ['EQ', $mediaId]
            ])
            ->buildSql();
        $mediaOwnerSubQuery = M('tmediaowner')
            ->field('fregionid')
            ->where([
                'fid' => ['EXP', ' IN '. $mediaSubQuery]
            ])
            ->buildSql();
        $res = M('tregion')->where(['fid' => ['EXP', ' IN '. $mediaOwnerSubQuery]])->getField('ffullname');
        return $res;
    }

    /**
     * 获取某个省级行政区下的所有行政区划
     * @param $pid
     */
    public function getSecondLevelRegion($pid)
    {
        $idLen = strlen($pid);
        $res = M('tregion')
            ->field('fid value, fpid, fname label')
            ->where([
                "LEFT(fid, $idLen)" => ['EQ', $pid],
                'fstate' => ['EQ', 1]
            ])
            ->select();
        $items = [];
        foreach($res as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpid']])){
                $items[$value['fpid']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        $this->ajaxReturn([
            'code' => 0,
            'data' => $tree,
        ]);
    }

    /**
     * 获取总局的日期条件
     * @param $date 201807
     * @param $mediaClass
     * @return array|bool   如果返回false是该月没有计划, 否则返回['2018-07-01', '2018-07-02']这种形式的数组
     */
    private function processZongjuCondition($date, $mediaClass)
    {
        $year = substr($date, 0, 4);
        $month = substr($date, 4, 2);
        $date = "$year-$month-01";
        $condition = M('zongju_data_confirm')->where(['fmonth' => ['EQ', $date],'dmp_confirm_state' => ['EQ', 0]])->getField('condition');
        if (!$condition){
            return false;
        }

        $dateCondition = json_decode($condition, true);
        $dateCondition = $dateCondition[$this->mediaClassMap[$mediaClass]['str']];
        $dateArr = [];
        $month = substr($date, 0, 8);
        foreach ($dateCondition as $day) {
            if (!$day){
                continue;
            }
            $dateArr[] = $month.$day;
        }
        return $dateArr;

    }


    private function paperIllegalAdSearch($month, $region, $formData, $page, $pageSize)
    {
        $dateCondition = $this->processZongjuCondition($month,3);
        if ($this->userType === 'dmp' && !$dateCondition){
            $table = [];
            $total = 0;
        }else{
            $month = substr($month, 0, 4) . '-' . substr($month, 4, 2);
            $mediaSubQuery = $this->processMediaSubQuery($formData, $region);
            $where = [
                'fmediaid' => [
                    ['EXP', ' IN '.$mediaSubQuery],
                ],
                'issue.fillegaltypecode' => ['NEQ', 0],
                'issue.fissuedate' => [
                    ['LIKE', "$month%"]
                ],
            ];
            if ($this->userType === 'dmp'){
                $where['review.media_id'] = ['EXP', 'IS NULL'];
                // 国家局限定媒体
                $guojiajuMediaIds = M('tmedialabel')
                    ->field('fmediaid')
                    ->cache(true, 600)
                    ->where([
                        'fstate' => ['EQ', 1],
                        'flabelid' => ['EQ', 244],
                    ])
                    ->getField('fmediaid', true);
                $guojiajuMediaIds = implode(',', $guojiajuMediaIds);
                $where['fmediaid'][] = ['IN', $guojiajuMediaIds];
                $where['issue.fissuedate'][] = ['IN', $dateCondition];
            }
            $tadSubQuery = $this->processAdSubQuery($formData);
            if ($tadSubQuery){
                $where['issue.fadid'] = ['EXP', ' IN ' . $tadSubQuery];
            }
            $table = M('tpapersample')
                ->alias('issue')
                ->join('LEFT JOIN illegal_ad_issue_review review on issue.fmediaid = review.media_id AND issue.fjpgfilename = review.paper_jpg')
                ->where($where)
                ->order('issue.fmediaid, issue.fadid')
                ->page($page, $pageSize)
                ->select();
            foreach ($table as $key => $value) {
                $tempAdInfo = M('tad')->field('fadname, fadclasscode, fadclasscode_v2')->find($value['fadid']);
                $table[$key]['fadname'] = r_highlight($tempAdInfo['fadname'], $formData['adName'], 'red');
                $table[$key]['adClass'] = M('tadclass')->cache(true)->where(['fcode' => ['EQ', $tempAdInfo['fadclasscode']]])->getField('ffullname');
                $table[$key]['adClass2'] = M('hz_ad_class')->cache(true)->where(['fcode' => ['EQ', $tempAdInfo['fadclasscode_v2']]])->getField('ffullname');
                $table[$key]['mediaName'] = M('tmedia')->cache(true)->where(['fid' => ['EQ', $value['fmediaid']]])->getField('fmedianame');
                $table[$key]['mediaName'] = r_highlight($table[$key]['mediaName'], $formData['mediaName'], 'red');
                $table[$key]['fexpressions'] =  array_filter(explode(' ;; ', $table[$key]['fexpressions']));
                $table[$key]['count'] = 1;
                $table[$key]['fid'] = $value['fpapersampleid'];
                $table[$key]['region'] = $this->getRegionNameByMediaId($value['fmediaid']);
            }
            $total = M('tpapersample')
                ->alias('issue')
                ->join('LEFT JOIN illegal_ad_issue_review review on issue.fmediaid = review.media_id AND issue.fjpgfilename = review.paper_jpg')
                ->where($where)
                ->count();
        }
        $this->ajaxReturn(compact('table', 'total'));
    }


    public function addComment($sampleId, $condition, $comment)
    {
        if ($this->userType !== 'customer'){
            $this->ajaxReturn([
                'code' => -2,
                'msg' => '你没有权限添加评论'
            ]);
        }
        $month = $condition['month'];
        $mediaClass = $condition['mediaClass'];
        $region = $condition['region'];
        if (!$month || !$mediaClass || !$region){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '基础搜索数据不全'
            ]);
        }
        $addData = [
            'sample_id' => $sampleId,
            'media_class' => $mediaClass,
            'review_comment' => $comment,
            'is_confirm' => 0,
            'update_time' => time(),
            'reviewer_id' => session('wx_id'),
            'confirm_id' => 0,
            'month' => $month
        ];
        $res = M('illegal_review_record')->add($addData);
        if ($res){
            $data = M('illegal_review_record')->where(['sample_id' => ['EQ', $sampleId]])->order('update_time desc')->select();
            foreach ($data as &$datum) {
                if ($datum['is_confirm'] == 2){
                    $tempArr = explode(' ;; ', $datum['review_comment']);
                    $datum['review_comment'] = $tempArr[0];
                    $datum['back_comment'] = $tempArr[1];
                }
                $datum['update_time'] = date('Y-m-d H:i:s', $datum['update_time']);
            }
            $this->ajaxReturn([
                'code' => 0,
                'data' => $data,
                'msg' => '添加成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '添加失败'
            ]);
        }
    }

    /**
     * 确认客户审核意见
     * @param $commentId
     * @param $sampleId
     * @param $condition
     * @param int $type 是否确认. 1:确认 2: 不确认
     * @param string $reason
     */
    public function confirmComment($commentId, $sampleId, $condition, $type=1, $reason='')
    {
        if ($this->userType !== 'dmp'){
            $this->ajaxReturn([
                'code' => -2,
                'msg' => '你没有权限确认评论'
            ]);
        }
        $month = $condition['month'];
        $mediaClass = $condition['mediaClass'];
        $region = $condition['region'];
        if (!$month || !$mediaClass || !$region){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '基础搜索数据不全'
            ]);
        }

        $saveData = [
            'is_confirm' => $type,
            'update_time' => time(),
            'confirm_id' => session('wx_id'),
        ];
        if ($type == 2){
            $review_comment = M('illegal_review_record')->where(['id' => ['EQ', $commentId]])->getField('review_comment');
            $saveData['review_comment'] = $review_comment . ' ;; '.$reason;
        }
        $res = M('illegal_review_record')->where(['id' => ['EQ', $commentId]])->save($saveData);
        if ($res){
            $data = M('illegal_review_record')->where(['sample_id' => ['EQ', $sampleId], 'media_class' => ['EQ', $mediaClass]])->order('update_time desc')->select();
            foreach ($data as &$datum) {
                if ($datum['is_confirm'] == 2){
                    $tempArr = explode(' ;; ', $datum['review_comment']);
                    $datum['review_comment'] = $tempArr[0];
                    $datum['back_comment'] = $tempArr[1];
                }
                $datum['update_time'] = date('Y-m-d H:i:s', $datum['update_time']);
            }
            $this->ajaxReturn([
                'code' => 0,
                'data' => $data,
                'msg' => '更新成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '更新失败'
            ]);
        }
    }

    /**
     * 质检人员对这个样本做退回处理
     * @param $sampleId
     * @param $condition
     * @param $reason
     */
    public function submitAdError($sampleId, $condition, $reason, $linkType)
    {
        return false;
        $month = $condition['month'];
        $mediaClass = $condition['mediaClass'];
        $region = $condition['region'];
        if (!$month || !$mediaClass || !$region){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '基础搜索数据不全'
            ]);
        }
        // 走任务质检流程
        if ($mediaClass == 3){
            $taskid = M('task_input')->where([
                'task_state' => ['EQ', 2],
                'cut_err_state' => ['EQ', 0],
                'fillegaltypecode' => ['NEQ', 0],
                'sam_id' => ['EQ', $sampleId],
                'media_class' => ['EQ', 3]
            ])
                ->getField('taskid');
        }else{
            $uuid = M($this->mediaClassMap[$mediaClass]['table'])->where(['fid' => ['EQ', $sampleId]])->getField('uuid');
            $taskid = M('task_input')->where([
                'task_state' => ['EQ', 2],
                'cut_err_state' => ['EQ', 0],
                'fillegaltypecode' => ['NEQ', 0],
                'media_class' => ['EQ', $mediaClass],
                'sam_uuid' => ['EQ', $uuid]
            ])
                ->getField('taskid');
        }
        if (!$taskid){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '退回失败, 没有找到符合条件的未退回的任务',
                'f' => M()->getLastSql(),
            ]);
        }
        $reason = '客户反馈:' . $reason;
        // 检查是否有任务流程, 没有的话linkType为0
        $hasLink = M('task_input_link')
            ->where([
                'taskid' => ['EQ', $taskid],
                'media_class' => ['EQ', $mediaClass],
            ])
            ->count() > 0;
        $linkType = $hasLink ? $linkType : 0;
        $taskModel = new TaskInputModel();
        $res = $taskModel->rejectTask($taskid,session('wx_id'), $mediaClass, $linkType, $reason);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '退回成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -2,
                'msg' => $taskModel->getError()
            ]);
        }
    }
	
	/*获取确认违法广告的广告名称*/
	public function get_ill_ad($mediaId=0,$issue_date='',$startTime=0,$paper_jpg=''){
		//var_dump($mediaId,$issue_date,$startTime,$paper_jpg);
		$mediaInfo = M('tmedia')
					  ->field('tmedia.fid,left(tmedia.fmediaclassid,2) as media_class,tmediaowner.fregionid')
					  ->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
					  ->where(array('tmedia.fid'=>$mediaId))->find();
		  if($mediaInfo['media_class'] == '01'){
			try{//
			  $da = M('ttvissue_'.date('Ym',$startTime).'_'.substr($mediaInfo['fregionid'],0,2))->alias('issue')
				->join('ttvsample on ttvsample.fid = issue.fsampleid')
				->join('tad on tad.fadid = ttvsample.fadid')
				->where(array('issue.fstarttime'=>$startTime,'issue.fmediaid'=>$mediaId))
				->getField('tad.fadname');
			}catch( \Exception $e) { //
			
			} 
			
		  }elseif($mediaInfo['media_class'] == '02'){
			try{//
			  $da = M('tbcissue_'.date('Ym',$startTime).'_'.substr($mediaInfo['fregionid'],0,2))->alias('issue')
				->join('tbcsample on tbcsample.fid = issue.fsampleid')
				->join('tad on tad.fadid = tbcsample.fadid')
				->where(array('issue.fstarttime'=>$startTime,'issue.fmediaid'=>$mediaId))
				->getField('tad.fadname');
			}catch( \Exception $e) { //
			
			} 
			
		  }elseif($mediaInfo['media_class'] == '03'){
			$da = M('tpapersample')
				  ->join('tad on tad.fadid = tpapersample.fadid')
				  ->where(array('tpapersample.fjpgfilename'=>$paper_jpg,'tpapersample.fissuedate'=>$issue_date))
				  ->getField('tad.fadname');
		  }        
		  if(!$da) $da = '未知';
		  return $da;
		  
	}
	
	
}