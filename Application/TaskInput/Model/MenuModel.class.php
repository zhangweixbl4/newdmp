<?php
namespace TaskInput\Model;
class MenuModel{
    /*获取菜单*/
    public function menu_list(){
		$menu_list = array();
        if(A('InputPc/Task','Model')->user_other_authority('1006')){
            $menu_list[] = array(
                'menu_name'=>'开始任务',
                'menu_url'=>'TaskInput/TraditionAd/index',
                'icon'=>'0',
            );
            $menu_list[] = array(
                'menu_name'=>'我的任务',
                'menu_url'=>'TaskInput/TraditionAd/myTaskList',
                'icon'=>'0',
            );
            if (in_array(session('wx_id'), [65,414,91,95,379,409,446,469,353,32,51,520,610,614,940,1582,1269,1694,63])) {
                $menu_list[] = array(
                    'menu_name'=>'开始质检',
                    'menu_url'=>'TaskInput/QualityInspection/getTask',
                    'icon'=>'0',
                );
                $menu_list[] = array(
                    'menu_name'=>'我的质检',
                    'menu_url'=>'TaskInput/QualityInspection/myTask',
                    'icon'=>'0',
                );
            }
            if (in_array(session('wx_id'), [65,414,446,469,353,32,51,520,610,614,940,1582,1694,63])) {
                $menu_list[] = array(
                    'menu_name'=>'质检管理',
                    'menu_url'=>'TaskInput/QualityInspection/listTask',
                    'icon'=>'0',
                );
            }
            $menu_list[] = array(
                'menu_name'=>'广告主题',
                'menu_url'=>'TaskInput/TraditionAd/adList',
                'icon'=>'0',
            );
        }
		if(	A('InputPc/Task','Model')->user_other_authority('1003')){
			$menu_list[] = array(
                'menu_name'=>'报纸剪辑',
                'menu_url'=>'InputPc/PaperCut/paperCut',
                'icon'=>'0',
            );
		}					
		if(	A('InputPc/Task','Model')->user_other_authority('1004')){			
			$menu_list[] = array(
                'menu_name'=>'报纸上传',
                'menu_url'=>'InputPc/PaperSource/index',
                'icon'=>'0',
            );
		}	
        $menu_list[] = array(
            'menu_name'=>'报纸管理',
            'menu_url'=>'Adinput/PapUplEdiState/index',
            'icon'=>'0',
        );
        if (M('ad_input_user')->where(['wx_id' => ['EQ', session('wx_id')]])->cache(true, 60)->getField('is_leader') == 1){
            $menu_list[] = array(
                'menu_name'=>'媒体偏好',
                'menu_url'=>'TaskInput/TraditionAd/mediaPrefer',
                'icon'=>'0',
            );
            $menu_list[] = array(
                'menu_name'=>'总量统计',
                'menu_url'=>'TaskInput/TraditionAd/checkTaskCount',
                'icon'=>'0',
            );
            $menu_list[] = array(
                'menu_name'=>'小组统计',
                'menu_url'=>'TaskInput/TraditionAd/checkTaskCountByGroup',
                'icon'=>'0',
            );
        }
        if(A('InputPc/Task','Model')->user_other_authority('1007')){
            // TODO：广西需要违法审核权限但不提供质检任务菜单权限
            if($_SERVER['HTTP_HOST'] != 'gx.hz-sj.vip'){
                $menu_list[] = array(
                    'menu_name'=>'任务管理',
                    'menu_url'=>'TaskInput/TraditionAd/inspectorTaskList',
                    'icon'=>'0',
                );
            }
            $menu_list[] = array(
                'menu_name'=>'批量修改',
                'menu_url'=>'TaskInput/TraditionAd/taskBatchModify',
                'icon'=>'0',
            );
            $menu_list[] = array(
                'menu_name'=>'数据补录',
                'menu_url'=>'TaskInput/NetAd/NetCheck',
                'icon'=>'0',
            );
            if(in_array(session('wx_id'),[51,832,32,87,50,63,414,445,469,65,44,584,614])){
                $menu_list[] = array(
                    'menu_name'=>'计划管理',
                    'menu_url'=>'TaskInput/NetAd/NetPlan',
                    'icon'=>'0',
                );
            }
        }

        if ( A('InputPc/Task','Model')->user_other_authority('3001')){
            $menu_list[] = array(
                'menu_name' => '违法确认',
                'menu_url' => 'TaskInput/illegalAdReview/illegalReview',
                'icon' => '0',
            );
        }
        if ( A('InputPc/Task','Model')->user_other_authority('5001')){
            $menu_list[] = array(
                'menu_name' => '数据加工',
                'menu_url' => '/TaskInput/Standardization/index',
                'icon' => '0',
            );
        }
        if(A('InputPc/Task','Model')->user_other_authority('7001')) {
            $menu_list[] = array(
                'menu_name' => '广告抽取',
                'menu_url' => '/TaskInput/CustomerAdIssueTask/index',
                'icon' => '0',
            );
        }
        if(A('InputPc/Task','Model')->checkTbnDataCheckAuth()) {
            $menu_list[] = array(
                'menu_name' => '数据复审',
                'menu_url' => '/TaskInput/TbnDataCheck/index',
                'icon' => '0',
            );
        }
		if(A('InputPc/Task','Model')->user_other_authority('6003')) {
            $menu_list[] = array(
                'menu_name' => '旅游数据处理',
                'menu_url' => '/TaskInput/Lvyou/index',
                'icon' => '0',
            );
        }
		if(A('InputPc/Task','Model')->user_other_authority('6004')) {
            $menu_list[] = array(
                'menu_name' => '样本数据检查',
                'menu_url' => '/TaskInput/SamCheck/index',
                'icon' => '0',
            );
        }
		
		
		return $menu_list;
    }
}