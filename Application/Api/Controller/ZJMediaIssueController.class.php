<?php
namespace Api\Controller;
use Think\Controller;
class ZJMediaIssueController extends Controller {

	/**
	* 接口接收的JSON参数并解码为对象
	*/
	protected $oParam;

	/**
	 * 构造函数
	 * @Param	Mixed variable
	 * @Return	void 返回的数据
	 */
	public function _initialize(){
		header('Access-Control-Allow-Origin:*');//允许所有域名跨域访问
		//请求方式为options时，不返回结果
		if(strtoupper($_SERVER['REQUEST_METHOD'])== 'OPTIONS'){
	        exit;
	    }
		$this->oParam = json_decode(file_get_contents('php://input'),true);
	}
	
	/*--------------------------------------------推送业务----------------------------------------*/

	/*
	* by zw
	* 轮循推送广告数据，30秒/次
	*/
	public function push_mediaissue(){
		session_write_close();
		set_time_limit(0);
		$customer = 330000;//客户编号
		$getData = $this->oParam;
// $getData['zdmedia'] = 11000010003155;
// $getData['zdissue'] = '2019-12-03';
		$zdmedia = $getData['zdmedia'];//需要生成的媒体，适用手动必需
		$zdissue = $getData['zdissue'];//需要生成的时段，适用手动必需
		$ztest = $getData['ztest'];//是否测试
		if(empty($zdmedia) || empty($zdissue)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
		}else{
			//暂时取消验证
			if(empty($ztest)){
				$checkData = A('Admin/FixedMediaData','Model')->get_state($zdmedia,$zdissue);
				if((int)$checkData != 2){
					$this->ajaxReturn(array('code'=>10,'msg'=>'数据未归档'));
				}
			}
		}

		$where_task['a.fcustomer'] = $customer;
		$where_task['a.fmedia_id'] = $zdmedia;
		$where_task['a.fissue_date'] = $zdissue;
		
		$do_task = M('tmediaissue_pushtask')
			->alias('a')
			->field('a.fid,a.fmedia_id,a.fissue_date,a.fcustomer')
			->join('tmedia b on a.fmedia_id = b.fid')
			->where($where_task)
			->order('a.fissue_date asc')
			->find();
		if(empty($do_task)){
			$do_task = [
				'fmedia_id' => $zdmedia,
				'fissue_date' => $zdissue,
				'fcustomer' => $customer,
				'fcreate_userid' => 0,
				'fcreate_user' => '浙江监测平台',
				'fcreate_time' => date('Y-m-d H:i:s'),
			];
			$do_task['fid'] = M('tmediaissue_pushtask')->add($do_task);
		}
		$do_task['ztest'] = $ztest;

		$iscbd = M('tmedia')
			->alias('a')
			->join('tmedialabel b on a.fid = b.fmediaid and b.flabelid in(449)')
			->where(['b.fstate'=>1,'a.fid'=>$do_task['fmedia_id']])
			->count();
		if(empty($iscbd) || $zdmedia == '11000010004171'){//好易购不获取串播单
			$this->get_mediaissue1($do_task);//获取普通媒体数据
		}else{
			$this->get_mediaissue2($do_task);//获取串播单媒体数据
		}

	}

	/*测试语句*/
	public function testmsg($msg = '',$test = 0){
		if($test == 1 && !empty($msg)){
			if(is_array($msg)){
				dump($msg);
			}else{
				var_dump($msg);
			}
		}
	}

	/*
	* 获取普通媒体数据
	* by zw
	*/
	public function get_mediaissue1($do_task = []){
		session_write_close();
		set_time_limit(600);
// $do_task['fmedia_id'] = 11000010003155;
// $do_task['fissue_date'] = '2019-12-13';
// $do_task['fcustomer'] = '330000';
		$do_media = [];
		$do_mediaowner = [];
		$do_ad = [];
		$do_sample = [];
		$do_issue = [];
		$do_prog_detail = [];

		$do_media = M('tmedia')->where(['fid'=>$do_task['fmedia_id']])->find();//媒体信息
		$do_mediaowner = M('tmediaowner')->where(['fid'=>$do_media['fmediaownerid']])->find(); //广告主信息
		
		$mclass = substr($do_media['fmediaclassid'],0,2);
		if($mclass == '01'){
			$tb_issue = 'ttvissue_'.date('Ym',strtotime($do_task['fissue_date'])).'_'.substr($do_task['fcustomer'],0,2);
			$tb_zissue = 'ttvissue';
			$tb_sample = 'ttvsample';
			$tb_prog = 'ttvprog';
			$sample_joinstr = $tb_sample.' b on a.fsampleid = b.fid and b.fstate in(0,1)';
			$where_issue['a.fissuedate'] = strtotime($do_task['fissue_date']);
			$orders = 'a.fstarttime asc';
		}elseif($mclass == '02'){
			$tb_issue = 'tbcissue_'.date('Ym',strtotime($do_task['fissue_date'])).'_'.substr($do_task['fcustomer'],0,2);
			$tb_zissue = 'tbcissue';
			$tb_sample = 'tbcsample';
			$tb_prog = 'tbcprog';
			$sample_joinstr = $tb_sample.' b on a.fsampleid = b.fid and b.fstate in(0,1)';
			$where_issue['a.fissuedate'] = strtotime($do_task['fissue_date']);
			$orders = 'a.fstarttime asc';
		}elseif($mclass == '03' || $mclass == '04'){
			$tb_issue = 'tpaperissue';
			$tb_sample = 'tpapersample';
			$sample_joinstr = $tb_sample.' b on a.fpapersampleid = b.fpapersampleid and b.fstate in(0,1)';
			$where_issue['a.fissuedate'] = $do_task['fissue_date'];
			$orders = '';
		}
		if($mclass == '01' || $mclass == '02'){
			if(strtotime(date('Y-m-01').' -2 month')<=strtotime($do_task['fissue_date'])){//三个月之内的数据检查主分表是否一致
				A('Api/SynIssue','Model')->check_issue($do_task['fmedia_id'],$do_task['fissue_date']);

				//发布数据差异进行验证
				$sqlstr_IssueCountCheck = 'SELECT A.分表量,B.总表量,A.分表量 - B.总表量 差异数
					FROM (SELECT FMEDIAID,COUNT(1) 分表量
					FROM (select * from '.$tb_issue.'
					WHERE FISSUEDATE = UNIX_TIMESTAMP("'.$do_task['fissue_date'].'") AND FMEDIAID = '.$do_task['fmedia_id'].'
					GROUP BY FMEDIAID,fstarttime) M) A,
					(
					SELECT FMEDIAID,COUNT(1) 总表量
					FROM '.$tb_zissue.'
					WHERE '.$tb_zissue.'.FMEDIAID = '.$do_task['fmedia_id'].'
					AND FISSUEDATE = "'.$do_task['fissue_date'].'"
					GROUP BY FMEDIAID) B
					WHERE A.FMEDIAID = B.FMEDIAID
					ORDER BY 差异数 DESC';
			}else{//三个月之外的数据只需要查分表是否有数据
				$sqlstr_IssueCountCheck = 'select count(*) acount from '.$tb_issue.'
					WHERE FISSUEDATE = UNIX_TIMESTAMP("'.$do_task['fissue_date'].'") 
					AND FMEDIAID = '.$do_task['fmedia_id'];
			}
			
			$do_IssueCountCheck = M()->master(true)->query($sqlstr_IssueCountCheck);
			if(empty($do_IssueCountCheck)){
				$this->updateTaskStatus(['taskid'=>$do_task['fid'],'fstatus'=>2,'userData'=>['fupdate_user'=>'AI'],'returnmsg'=>'无数据导致推送失败','mediaId'=>$do_task['fmedia_id'],'content'=>'issueData=>'.$do_task['fissue_date'],'returncode'=>'not data']);
				$this->ajaxReturn(array('code'=>1,'msg'=>'无数据，传输失败'));
			}else{
				if(strtotime(date('Y-m-01').' -2 month')<=strtotime($do_task['fissue_date'])){
					if((int)$do_IssueCountCheck[0]['差异数'] <> 0){
						$this->updateTaskStatus(['taskid'=>$do_task['fid'],'fstatus'=>2,'userData'=>['fupdate_user'=>'AI'],'returnmsg'=>'主表和分表数据量存在差异导致推送失败']);
						if(!empty($save_push) && !in_array($_SERVER['HTTP_HOST'],C('TESTSERVER'))){
	                        $smsphone = [13735566051,13588258695];//提醒人员，张伟、曹勇
	                        $token = 'f01671bd9464292ce29c4936336baa36e12b5a742f1cca55b6625d0c5fea8d27';
	                        push_ddtask('浙江监测平台',"> 推送提示：\n\n因主表和分表数据量存在差异导致推送失败，主表数据量".$do_IssueCountCheck[0]['总表量']."条，分表数据量".$do_IssueCountCheck[0]["分表量"]."条。\n\n相关媒体：".$do_media['fmedianame']."(".$do_media['fid'].")\n\n任务日期：".$do_task['fissue_date']."\n\n",$smsphone,$token,'markdown');
						}
						$this->ajaxReturn(array('code'=>1,'msg'=>'主表和分表数据量不一致，传输失败'));
					}
				}
			}
		}
		$where_issue['a.fmediaid'] = $do_task['fmedia_id'];
		//获取发布记录
		$do_issue = M($tb_issue)
			->alias('a')
			->field('a.*,b.fadid,b.uuid,c.fadname')
			->join($sample_joinstr)
			->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
			->join('tadclass d on c.fadclasscode = d.fcode')
			->join('tadowner e on c.fadowner = e.fid')
			->where($where_issue)
			->order($orders)
			->select();
		if(!empty($do_issue)){
			$ggData = $this->overlappingData($do_issue,'fstarttime','fendtime');//交叉数据
			if(!empty($ggData['overlapping'])){
				$this->updateTaskStatus(['taskid'=>$do_task['fid'],'fstatus'=>2,'userData'=>['fupdate_user'=>'AI'],'returnmsg'=>'数据存在交叉推送失败','mediaId'=>$do_task['fmedia_id'],'content'=>'issueData=>"'.implode('","', $ggData['overlapping']).'"','returncode'=>'data overlapping']);
				$this->ajaxReturn(array('code'=>1,'msg'=>'存在数据交叉，传输失败','data'=>$ggData['overlapping']));
			}
			if(!empty($ggData['identical'])){
				$this->updateTaskStatus(['taskid'=>$do_task['fid'],'fstatus'=>2,'userData'=>['fupdate_user'=>'AI'],'returnmsg'=>'数据存在重复推送失败','mediaId'=>$do_task['fmedia_id'],'content'=>'issueData=>"'.implode('","', $ggData['identical']).'"','returncode'=>'data identical']);
				$this->ajaxReturn(array('code'=>1,'msg'=>'存在数据重复，传输失败','data'=>$ggData['identical']));
			}

			$uuids = [];//存在未命名广告的uuid
			$hebingArr = ['好易购','本媒体宣传'];//好易购数据合并
			$newstarttime = '';
			foreach ($do_issue as $key => $value) {
				//提取样本ID，格式化发布数据字段名和相应内容
				if($mclass == '01' || $mclass == '02'){
					$do_issue[$key]['fissuedate'] = date('Y-m-d',$value['fissuedate']);
					$do_issue[$key]['fendtime'] = date('Y-m-d H:i:s',$value['fendtime']);
					$do_issue[$key]['fcreatetime'] = date('Y-m-d H:i:s',$value['fcreatetime']);
					if($do_task['fmedia_id'] == 11000010004171){
						if(in_array($value['fadname'], $hebingArr) && ($value['fendtime'] - $value['fstarttime'])<300){
							if(empty($newstarttime)){
								$newstarttime = date('Y-m-d H:i:s',$value['fstarttime']);
							}
							unset($do_issue[$key]);
							continue;
						}else{
							if(empty($newstarttime)){
								$do_issue[$key]['fstarttime'] = date('Y-m-d H:i:s',$value['fstarttime']);
							}else{
								$do_issue[$key]['fstarttime'] = $newstarttime;
								$do_issue[$key]['flength'] = $value['fendtime'] - strtotime($newstarttime);
							}
							$sampleids[] = $value['fsampleid'];
							$newstarttime = '';
						}
					}else{
						$do_issue[$key]['fstarttime'] = date('Y-m-d H:i:s',$value['fstarttime']);
						$sampleids[] = $value['fsampleid'];
					}

					//重叠广告碎片化
					if(!empty($ggData['overlap'][$value['fid']])){
						$overlapAddData = $do_issue[$key];
						$overlapsttime = $value['fstarttime'];
						foreach ($ggData['overlap'][$value['fid']]['child'] as $okey => $ovalue) {
							if($ovalue['fstarttime'] - $overlapsttime >2 ){
								$overlapAddData['fid'] = $overlapsttime;
								$overlapAddData['fstarttime'] = date('Y-m-d H:i:s',$overlapsttime);
								$overlapAddData['fendtime'] = date('Y-m-d H:i:s',$ovalue['fstarttime']);
								$overlapAddData['flength'] = $ovalue['fstarttime'] - $overlapsttime;
								$all_issue[] = $overlapAddData;
							}
							$overlapsttime = $ovalue['fendtime'];
							if($okey == (count($ggData['overlap'][$value['fid']]['child']))-1){
								if(($value['fendtime'] - $overlapsttime) >2 ){
									$overlapAddData['fid'] = $overlapsttime;
									$overlapAddData['fstarttime'] = date('Y-m-d H:i:s',$overlapsttime);
									$overlapAddData['fendtime'] = date('Y-m-d H:i:s',$value['fendtime']); 
									$overlapAddData['flength'] = $value['fendtime'] - $overlapsttime;
									$all_issue[] = $overlapAddData;
								}
							}
						}
						unset($do_issue[$key]);
					}
				}else{
					$do_issue[$key]['fid'] = $value['fpaperissueid'];
					$do_issue[$key]['fsampleid'] = $value['fpapersampleid'];
					$sampleids[] = $value['fpapersampleid'];
				}

				//交叉一两秒的，调整最长广告的时间
				if(!empty($ggData['smalloverlapping'][$value['fid']])){
					$do_issue[$key]['fstarttime'] = date('Y-m-d H:i:s',$ggData['smalloverlapping'][$value['fid']]['fstarttime']);
					$do_issue[$key]['fendtime'] = date('Y-m-d H:i:s',$ggData['smalloverlapping'][$value['fid']]['fendtime']);
					$do_issue[$key]['flength'] = $ggData['smalloverlapping'][$value['fid']]['fendtime'] - $ggData['smalloverlapping'][$value['fid']]['fstarttime'];
				}

				//提取未命名广告
				if(empty($value['fadid'])){
					if(!in_array($value['uuid'], $uuids)){
						$uuids[] = $value['uuid'];
					}
				}else{
					unset($value['fadid']);
				}

				if(!empty($do_issue[$key])){
					$all_issue[] = $do_issue[$key];
				}
			}

			//未命名提醒
			if(!empty($uuids)){
				$this->updateTaskStatus(['taskid'=>$do_task['fid'],'fstatus'=>2,'userData'=>['fupdate_user'=>'AI'],'returnmsg'=>'存在未命名广告导致推送失败','mediaId'=>$do_task['fmedia_id'],'content'=>'uuids=>'.implode('","', $uuids),'returncode'=>'undefined adname']);
				$this->ajaxReturn(array('code'=>1,'msg'=>'存在未命名广告，传输失败'));
			}
			
			//获取样本数据
			if(!empty($sampleids)){
					if($mclass == '01' || $mclass == '02'){
					$where_sample['fid'] = ['IN',$sampleids];
				}else{
					$where_sample['fpapersampleid'] = ['IN',$sampleids];
				}
				$do_sample = M($tb_sample)->where($where_sample)->select();
			}

			//获取广告数据
			foreach ($do_sample as $key => $value) {
				if(!empty($value['fadid'])){
					$adids[] = $value['fadid'];
				}
				if($mclass == '03' || $mclass == '04'){
					$do_sample[$key]['fid'] = $value['fpapersampleid'];
				}
			}
			if(!empty($adids)){
				$do_ad = M('tad')->field('tad.*,tadowner.fname adownername')->join('tadowner on tad.fadowner = tadowner.fid')->where(['fadid'=>['in',$adids]])->select();
			}

			//获取广告主数据
			foreach ($do_ad as $key => $value) {
				if(!empty($value['fadowner'])){
					$adownerids[] = $value['fadowner'];
				}
			}
			if(!empty($adownerids)){
				$do_adowner = M('tadowner')->where(['fid'=>['in',$adownerids]])->select();
			}

			$push_data['task'] = $do_task;//任务信息
			$push_data['media'] = $do_media;//推送的媒体数据
			$push_data['mediaowner'] = $do_mediaowner;//推送的媒体机构数据
			$push_data['adowner'] = $do_adowner;//推送的广告主
			$push_data['ad'] = $do_ad;//推送的广告
			$push_data['sample'] = $do_sample;//推送的样本
			$push_data['issue'] = $all_issue;//推送的发布记录
			$push_data['prog_detail'] = $do_prog_detail;//推送的节目片断
			if($mclass == '03' || $mclass == '04'){
				$where_psource['fmediaid'] = $do_task['fmedia_id'];
				$where_psource['fissuedate'] = $do_task['fissue_date'];
				$do_psource = M('tpapersource')->where($where_psource)->select();
				if(!empty($do_psource)){
					$push_data['papersource'] = $do_psource;
				}
			}
			
			$this->updateTaskStatus(['taskid'=>$do_task['fid'],'fstatus'=>1,'userData'=>['fupdate_user'=>'AI'],'returnmsg'=>'数据传输']);
			$this->ajaxReturn(array('code'=>0,'msg'=>'传输成功','data'=>$push_data));
		}else{
			// $this->testmsg([M($tb_issue)->getlastsql()],$do_task['ztest']);
			$this->updateTaskStatus(['taskid'=>$do_task['fid'],'fstatus'=>2,'userData'=>['fupdate_user'=>'AI'],'returnmsg'=>'无数据导致推送失败','mediaId'=>$do_task['fmedia_id'],'content'=>'issueData=>'.$do_task['fissue_date'],'returncode'=>'not data']);
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据，传输失败'));
		}
	}

	/*
	* 获取串播单数据
	* by zw
	*/
	public function get_mediaissue2($do_task = []){
		session_write_close();
		set_time_limit(0);
		
// header("Content-type:text/html;=utf-8"); 
// $do_task['fmedia_id'] = 11000010002362;
// $do_task['fissue_date'] = '2020-02-26';
		$starttime = strtotime($do_task['fissue_date'].' 00:00:00');
		$endtime = strtotime($do_task['fissue_date'].' 23:59:59');
		$data = A('Common/Media','Model')->getEpgSummaryList($do_task['fmedia_id'],$starttime,$endtime);

		if($data['code'] == 0){
			$do_media = [];//媒体
			$do_mediaowner = [];//媒体机构
			$do_ad = [];//广告
			$do_sample = [];//样本
			$do_issue = [];//广告明细
			$do_prog = [];//节目
			$do_prog_detail = [];//节目片断
			$ad_uuid = [];//样本UUID数据
			$ad_owner = [];//广告主ID数据

			$do_media = M('tmedia')->where(['fid'=>$do_task['fmedia_id']])->find();//媒体信息
			$do_mediaowner = M('tmediaowner')->where(['fid'=>$do_media['fmediaownerid']])->find(); //广告主信息
			
			$mclass = substr($do_media['fmediaclassid'],0,2);
			if($mclass == '01'){
				$tb_sample = 'ttvsample';
				$tb_prog = 'ttvprog';
				$where_issue['fissuedate'] = strtotime($do_task['fissue_date']);
			}elseif($mclass == '02'){
				$tb_sample = 'tbcsample';
				$tb_prog = 'tbcprog';
				$where_issue['fissuedate'] = strtotime($do_task['fissue_date']);
			}

			foreach ($data['dataList'] as $pkey => $pvalue) {
				$progEndTime = '';//节目结束时间
				//获取节目数据
				$progData = [
					'fprogid'=> $pvalue['id']?$pvalue['id']:0,
					'fmediaid'=> $do_task['fmedia_id'],
					'fissuedate'=> $do_task['fissue_date'],
					'fcontent'=> $pvalue['title'],
					'fclasscode'=> $pvalue['category']?$pvalue['category']:0,
					'fstart'=> $pvalue['start_time'],
					'fend'=> $pvalue['end_time'],
					'fstate'=> 1,
					'fcreatetime'=> date('Y-m-d H:i:s'),
				];

				//获取发布数据和其他相关联的样本、广告数据
				foreach ($pvalue['child'] as $ckey => $cvalue) {
					if($cvalue['is_ad'] == 1){
						$do_issue[] = [
							'fid' => strtotime($cvalue['end_time']),
							'fmediaid' => $do_task['fmedia_id'],
							'fissuedate' => $do_task['fissue_date'],
							'uuid' => $cvalue['ad_uuid'],
							'title' => $cvalue['title'],
							'fstarttime' => $cvalue['start_time'],
							'fendtime' => $cvalue['end_time'],
							'flength' => $cvalue['duration'],
							'fregionid' => $do_media['media_region_id'],
							'fcreatetime' => date('Y-m-d H:i:s'),
							'fcreator' => 'AI',
						];
						if(!in_array($cvalue['ad_uuid'], $ad_uuid)){
							$ad_uuid[] = $cvalue['ad_uuid'];
						}
					}

					if($cvalue['is_ad'] == 2){
						$do_prog_detail[] = [
							'fprogid' => strtotime($cvalue['end_time']),
							'fprogpid'=> $pvalue['id'],
							'fmediaid'=> $do_task['fmedia_id'],
							'fissuedate'=> $do_task['fissue_date'],
							'fcontent'=> $pvalue['title'],
							'fclasscode'=> $pvalue['category']?$pvalue['category']:0,
							'fstart'=> $cvalue['start_time'],
							'fend'=> $cvalue['end_time'],
							'fstate'=> 1,
						];
						$progEndTime = $cvalue['end_time'];
					}
				}

				$progData['fend'] = $progEndTime?$progEndTime:$progData['fend'];
				$do_prog[] = $progData;
			}
			
			//获取样本数据
			if(!empty($ad_uuid)){
				$do_sample = M($tb_sample)
					->alias('b')
					->field('b.*')
					->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
					->join('tadclass d on c.fadclasscode = d.fcode')
					->join('tadowner e on c.fadowner = e.fid')
					->where(['b.uuid'=>['in',$ad_uuid],'b.fmediaid'=>$do_task['fmedia_id'],'b.fstate'=>['in',[0,1]]])->group('b.uuid')->select();
				$uuids = [];
				foreach ($do_sample as $key => $value) {
					if(empty($value['fadid'])){
						if(!in_array($value['uuid'], $uuids)){
							$uuids[] = $value['uuid'];
						}
					}
				}
				if(!empty($uuids)){
					$this->updateTaskStatus(['taskid'=>$do_task['fid'],'fstatus'=>2,'userData'=>['fupdate_user'=>'AI'],'returnmsg'=>'存在未命名广告导致推送失败','mediaId'=>$do_task['fmedia_id'],'content'=>'uuids=>'.implode('","', $uuids),'returncode'=>'undefined adname']);
					$this->ajaxReturn(array('code'=>1,'msg'=>'存在未命名广告，传输失败'));
				}

				$sample_uuid = array_column($do_sample,'fid','uuid');
				$ad_ids = array_column($do_sample,'fadid');
			}

			//获取广告数据
			if(!empty($ad_ids)){
				$do_ad = M('tad')->where(['fadid'=>['in',$ad_ids]])->order('fadname asc')->group('fadid')->select();
				$ad_owner = array_column($do_ad,'fadowner');
			}

			//获取广告主数据
			if(!empty($ad_owner)){
				$do_adowner = M('tadowner')->where(['fid'=>['in',$ad_owner]])->select();
			}

			$ggData = $this->overlappingData($do_issue,'fstarttime','fendtime');//交叉数据
			if(!empty($ggData['overlapping'])){
				$this->updateTaskStatus(['taskid'=>$do_task['fid'],'fstatus'=>2,'userData'=>['fupdate_user'=>'AI'],'returnmsg'=>'数据存在交叉推送失败','mediaId'=>$do_task['fmedia_id'],'content'=>'starttimes=>"'.implode('","', $ggData['overlapping']).'"','returncode'=>'data overlapping']);
				$this->ajaxReturn(array('code'=>1,'msg'=>'存在数据交叉，传输失败','data'=>$ggData['overlapping']));
			}
			if(!empty($ggData['identical'])){
				$this->updateTaskStatus(['taskid'=>$do_task['fid'],'fstatus'=>2,'userData'=>['fupdate_user'=>'AI'],'returnmsg'=>'数据存在重复推送失败','mediaId'=>$do_task['fmedia_id'],'content'=>'starttimes=>"'.implode('","', $ggData['identical']).'"','returncode'=>'data identical']);
				$this->ajaxReturn(array('code'=>1,'msg'=>'存在数据重复，传输失败','data'=>$ggData['identical']));
			}

			//重叠广告碎片化
			foreach ($do_issue as $key => $value) {
				if(!empty($ggData['overlap'][$value['fid']])){
					$overlapAddData = $do_issue[$key];
					$overlapsttime = strtotime($value['fstarttime']);
					foreach ($ggData['overlap'][$value['fid']]['child'] as $okey => $ovalue) {
						if($ovalue['fstarttime'] - $overlapsttime >2 ){
							$overlapAddData['fid'] = $overlapsttime;
							$overlapAddData['fstarttime'] = date('Y-m-d H:i:s',$overlapsttime);
							$overlapAddData['fendtime'] = date('Y-m-d H:i:s',$ovalue['fstarttime']);
							$overlapAddData['flength'] = $ovalue['fstarttime'] - $overlapsttime;
							$all_issue[] = $overlapAddData;
						}

						$overlapsttime = $ovalue['fendtime'];
						if($okey == (count($ggData['overlap'][$value['fid']]['child']))-1){
							if((strtotime($value['fendtime']) - $overlapsttime) >2 ){
								$overlapAddData['fid'] = $overlapsttime;
								$overlapAddData['fstarttime'] = date('Y-m-d H:i:s',$overlapsttime);
								$overlapAddData['fendtime'] = $value['fendtime']; 
								$overlapAddData['flength'] = strtotime($value['fendtime']) - $overlapsttime;
								$all_issue[] = $overlapAddData;
							}
						}
					}
					unset($do_issue[$key]);
				}
			}
			if(!empty($all_issue)){
				$do_issue = array_merge($do_issue,$all_issue);
			}

			$jmjcData = $this->overlappingData($do_prog,'fstart','fend');//交叉数据
			if(!empty($jmjcData['overlapping'])){
				$this->updateTaskStatus(['taskid'=>$do_task['fid'],'fstatus'=>2,'userData'=>['fupdate_user'=>'AI'],'returnmsg'=>'节目数据存在交叉推送失败','mediaId'=>$do_task['fmedia_id'],'content'=>'starttimes=>'.implode('","', $jmjcData['overlapping']),'returncode'=>'prog data overlapping']);
				$this->ajaxReturn(array('code'=>1,'msg'=>'存在节目数据交叉，传输失败'));
			}
			if(!empty($jmjcData['identical'])){
				$this->updateTaskStatus(['taskid'=>$do_task['fid'],'fstatus'=>2,'userData'=>['fupdate_user'=>'AI'],'returnmsg'=>'节目数据存在重复推送失败','mediaId'=>$do_task['fmedia_id'],'content'=>'starttimes=>'.implode('","', $jmjcData['identical']),'returncode'=>'prog data identical']);
				$this->ajaxReturn(array('code'=>1,'msg'=>'存在数据重复，传输失败','data'=>$jmjcData['identical']));
			}
			foreach ($do_issue as $key => $value) {
				$do_issue[$key]['fsampleid'] = $sample_uuid[$value['uuid']];
				if(!empty($ggData['smalloverlapping'][$value['fid']])){
					$do_issue[$key]['fstarttime'] = date('Y-m-d H:i:s',$ggData['smalloverlapping'][$value['fid']]['fstarttime']);
					$do_issue[$key]['fendtime'] = date('Y-m-d H:i:s',$ggData['smalloverlapping'][$value['fid']]['fendtime']);
				}
			}

			$push_data['task'] = $do_task;//任务信息
			$push_data['media'] = $do_media;//推送的媒体数据
			$push_data['mediaowner'] = $do_mediaowner;//推送的媒体机构数据
			$push_data['adowner'] = $do_adowner;//推送的广告主
			$push_data['ad'] = $do_ad;//推送的广告
			$push_data['sample'] = $do_sample;//推送的样本
			$push_data['issue'] = array_values($do_issue);//推送的发布记录
			$push_data['prog'] = $do_prog;//推送的节目
			$push_data['prog_detail'] = $do_prog_detail;//推送的节目片断

			if(!empty($do_issue)){
				$this->updateTaskStatus(['taskid'=>$do_task['fid'],'fstatus'=>1,'userData'=>['fupdate_user'=>'串播单'],'returnmsg'=>'串播单传输']);
				$this->ajaxReturn(array('code'=>0,'msg'=>'传输成功','data'=>$push_data));
			}else{
				$this->updateTaskStatus(['taskid'=>$do_task['fid'],'fstatus'=>2,'userData'=>['fupdate_user'=>'串播单'],'returnmsg'=>'无数据导致推送失败','mediaId'=>$do_task['fmedia_id'],'content'=>'issueData=>'.$do_task['fissue_date'],'returncode'=>'not data']);
				$this->ajaxReturn(array('code'=>1,'msg'=>'无数据，传输失败'));
			}
			
		}else{
			$this->updateTaskStatus(['taskid'=>$do_task['fid'],'fstatus'=>2,'userData'=>['fupdate_user'=>'串播单'],'returnmsg'=>'无数据导致推送失败','mediaId'=>$do_task['fmedia_id'],'content'=>'issueData=>'.$do_task['fissue_date'],'returncode'=>'not data']);
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据，传输失败'));
		}
	}

	/*
	* by zw
	* 获取推送回调
	*/
	public function getPushStatus(){
		session_write_close();
		set_time_limit(0);
		$customer = 330000;//客户编号
		$getData = $this->oParam;

		$zdmedia = $getData['zdmedia'];
		$zdissue = $getData['zdissue'];
		$returnData = $getData['returnData'];
		$where_task['fcustomer'] = $customer;
		$where_task['fmedia_id'] = $zdmedia;
		$where_task['fissue_date'] = $zdissue;
		if(empty($returnData['code'])){
			$fstatus = 1;
		}else{
			$fstatus = 2;
		}

		M('tmediaissue_pushtask')->where($where_task)->save(['fstatus'=>$fstatus,'fupdate_userid'=>0,'fupdate_user'=>'浙江监测平台','fupdate_time'=>date('Y-m-d H:i:s'),'flog'=>['exp','CONCAT("浙江监测平台于'.date('Y-m-d H:i:s').'反馈'.$returnData['msg'].'；",flog)']]);
	}

	/*
	* by zw
	* 获取退回任务
	*/
	public function getBackData(){
		session_write_close();
		set_time_limit(0);
		$customer = 330000;//客户编号
		$getData = $this->oParam;

		$taskData = $getData['taskData'];
		$backContent = $getData['backContent'];
		foreach ($taskData as $key => $value) {
			foreach ($value['fissue_date'] as $timekey => $timevalue) {
				$where_task['fmedia_id'] = $value['fmedia_id'];
				$where_task['fissue_date'] = $timevalue;
				$where_task['fcustomer'] = $customer;
				$task_id = M('tmediaissue_pushtask')->where($where_task)->getField('fid');
				if(!empty($task_id)){
					$save_pushtask = $this->updateTaskStatus(['taskid'=>$task_id,'fstatus'=>11,'userData'=>['fupdate_user'=>'浙江监测平台'],'returnmsg'=>'退回任务，原因为'.$backContent]);
				}

				$ret = A('Admin/FixedMediaData','Model')->fixed($value['fmedia_id'],$timevalue,99,'因'.$backContent.'退回','浙江监测平台'); #归档状态 状态码定义：-1删除计划，0未生产，1生产中，1归档，99特殊状态
			}
		}

	}

	/*
	* by zw
	* 推送错误日志给北京，用于数据检查
	*/
	private function pushDataQuestion($info = ''){
		$gourl = 'http://'.C('JXServerUrl3').'/error_add';
		$pubData['info'] = $info;
		$pubData['error_type'] = 20;
		$pubData['depart'] = 'zhejiang';
		if(!empty($info) && !in_array($_SERVER['HTTP_HOST'],C('TESTSERVER'))){
			$ret = json_decode(http($gourl,json_encode($pubData),'POST',false,0),true);
			return $ret;
		}else{
			return false;
		}
		
	}

	/*
	* by zw
	* 更改任务状态
	*/
	private function updateTaskStatus($pushData){
		if(S('task_'.$pushData['taskid']) != $pushData['returnmsg']){
			S('task_'.$pushData['taskid'],$pushData['returnmsg'],7200);//缓存信息
			$saveData['fstatus'] = $pushData['fstatus'];
			$saveData['fupdate_userid'] = $pushData['userData']['fupdate_userid']?$pushData['userData']['fupdate_userid']:0;
			$saveData['fupdate_user'] = $pushData['userData']['fupdate_user']?$pushData['userData']['fupdate_user']:'AI';
			$saveData['fupdate_time'] = date('Y-m-d H:i:s');
			$saveData['flog'] = ['exp','CONCAT("时间：'.date('Y-m-d H:i:s').' 用户：'.$saveData['fupdate_user'].'（'.$saveData['fupdate_userid'].'）'.' 事件：'.$pushData['returnmsg'].'；",flog)'];
			if(!empty($pushData['returncode'])){
				$saveData['fcontent'] = '【log：'.$pushData['returncode'].' | mediaId：'.$pushData['mediaId'].' | content：'.$pushData['content'].'】';
				$this->pushDataQuestion('【log：'.$pushData['returncode'].' | mediaId：'.$pushData['mediaId'].' | content：'.$pushData['content'].'】');
			}

			$where['fid'] = $pushData['taskid'];
			$doTask = M('tmediaissue_pushtask')->field('fissue_date,fmedia_id')->master(true)->where($where)->find();
			if(!empty($doTask)){
				if(!empty($pushData['fstatus'] == 2)){
					$ret = A('Admin/FixedMediaData','Model')->fixed($doTask['fmedia_id'],$doTask['fissue_date'],99,'因'.$pushData['returnmsg'].'退回','浙江监测平台'); #归档状态 状态码定义：-1删除计划，0未生产，1生产中，1归档，99特殊状态
				}
				$ret = M('tmediaissue_pushtask')->where($where)->save($saveData);
			}
		}else{
			$ret = false;
		}
		
		return $ret;
	}

	/*
	* by zw
	* 交叉数据获取
	*/
	private function overlappingData($data,$startname,$endname){
		$returnData['overlapping'] = [];//交叉数据
		$returnData['identical'] = [];//相同数据
		$returnData['smalloverlapping'] = [];//小段交叉需要调整的
		$returnData['overlap'] = [];//重叠
		foreach ($data as $key => $value) {
			foreach ($data as $nokey => $novalue) {
				if($key != $nokey){
					if(is_numeric($novalue[$startname])){
						$st1 = $novalue[$startname];
						$ed1 = $novalue[$endname];
						$st2 = $value[$startname];
						$ed2 = $value[$endname];
					}else{
						$st1 = strtotime($novalue[$startname]);
						$ed1 = strtotime($novalue[$endname]);
						$st2 = strtotime($value[$startname]);
						$ed2 = strtotime($value[$endname]);
					}

					$jiaocha = timeContain($st1,$ed1,$st2,$ed2);
					//重复
					if($jiaocha['use'] == 3 && !in_array(date('Y-m-d H:i:s',$st2), $returnData['identical'])){
						$returnData['identical'][] = date('Y-m-d H:i:s',$st2);
					}
					//交叉
					if($jiaocha['use'] == 1 && !in_array(date('Y-m-d H:i:s',$st2), $returnData['overlapping'])){
						if($jiaocha['length'] >= 3){
							$returnData['overlapping'][] = date('Y-m-d H:i:s',$st2);
						}else{
							if(($ed1 - $st1) < ($ed2 - $st2)){
								if($jiaocha['maxnum'] == 1){
									$returnData['smalloverlapping'][$value['fid']] = [
										$startname => $st2,
										$endname => $st1
									];
								}else{
									$returnData['smalloverlapping'][$value['fid']] = [
										$startname => $ed1,
										$endname => $ed2
									];
								}
							}
						}
					}
					//重叠
					if($jiaocha['use'] == 2){
						if($jiaocha['maxnum'] == 2){
							if(empty($returnData['overlap'][$value['fid']])){
								$returnData['overlap'][$value['fid']] = [
									$startname => $st2,
									$endname => $ed2,
								];
							}
							$returnData['overlap'][$value['fid']]['child'][] = [
								$startname => $st1,
								$endname => $ed1,
							];
						}
					}
				}
			}
		}
		return $returnData;
	}

}