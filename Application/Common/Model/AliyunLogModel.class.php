<?php

namespace Common\Model;
use Think\Exception;

import('Vendor.aliyunLog.Log_Autoload','','.php');//载入阿里云OSS sdk

class AliyunLogModel{


	public function client(){
		
		$endpoint = C('ALI_LOG_ENDPOINT'); // 选择与上面步骤创建 project 所属区域匹配的 Endpoint,'cn-hangzhou.sls.aliyuncs.com'
		$accessKeyId = C('AL_OSSID');        // 使用你的阿里云访问秘钥 AccessKeyId
		$accessKey = C('AL_OSSKEY');             // 使用你的阿里云访问秘钥 AccessKeySecret


		$client = new \Aliyun_Log_Client($endpoint, $accessKeyId, $accessKey);
		return $client;
		
	}
	
	
	public function dmpLogs(){


		$project = 'hzsjdmp';                  // 上面步骤创建的项目名称
		$logstore = 'hzsjdmplog';                // 上面步骤创建的日志库名称

		$client = $this->client();
		
		$topic = "";
		$source = "";

		$contents = array(
							'HTTP_HOST'=> $_SERVER['HTTP_HOST'],
							'REQUEST_URI'=>$_SERVER['REQUEST_URI'],
							'get_client_ip' => get_client_ip(),
							'SESSION'	=> json_encode(session()),
							'cookie'	=> json_encode(cookie()),
							'POST'	=> file_get_contents('php://input'),
							'date'	=> date('Y-m-d'),
							'time'	=>date('H:i:s'),
							
							
						);
				
		$logItem = new \Aliyun_Log_Models_LogItem();
		$logItem->setTime(time());
		$logItem->setContents($contents);
		$logitems =array($logItem);
		
		
		$req2 = new \Aliyun_Log_Models_PutLogsRequest($project, $logstore, $topic, $source, $logitems);
		try {   
			$res2 = $client->putLogs($req2);
			return true;
		} catch ( \Exception $error) {   
			
			return $error->errJson();

		} 

		

		
	}
	
	/*记录日志*/
	public function dmpLogs2($logstore,$contents0){
		
		$contents = array();
		foreach($contents0 as $key => $content){
			$contents[$key] = strval($content);
			
		}
		
		$project = 'hzsjdmp';                  // 上面步骤创建的项目名称

		$client = $this->client();
		
		$topic = "";
		$source = "";

		
				
		$logItem = new \Aliyun_Log_Models_LogItem();
		$logItem->setTime(time());
		$logItem->setContents($contents);
		$logitems =array($logItem);
		
		
		$req2 = new \Aliyun_Log_Models_PutLogsRequest($project, $logstore, $topic, $source, $logitems);
		//var_dump($req2);
		

		
		try {   
			$res2 = $client->putLogs($req2);
			return true;
		} catch ( \Exception $error) {   
			//var_dump($error);
			$this->createLogstore($project,$logstore);
			return $error->errJson();

		} 

	}
	
	/*创建日志库*/
	public function createLogstore($project,$logstore){
		$client = $this->client();
		$req2 = new \Aliyun_Log_Models_CreateLogstoreRequest($project,$logstore,180,2);
		
		try {   
			$res2 = $client -> createLogstore($req2);
			return true;
		} catch ( \Exception $error) {   
			//var_dump($error);
			return $error->errJson();

		} 
	
	}
	
	


    /**
     * 众包任务的相关日志
     * @param $content
     * @return bool
     */
    public function task_input_log($content)
    {
        if (!$content['time']){
            $content['time'] = time();
        }
        if (!$content['session']){
            $content['session'] = session();
        }
        foreach ($content as $key => $value) {
            if (is_array($value)){
                $content[$key] = json_encode($value);
            }
        }
        return $this->dmpLogs2('task_input', $content);
	}

    /**
     * 众包任务的相关日志
     * @param $content
     * @return bool
     */
    public function net_task_log($content)
    {
        if (!$content['time']){
            $content['time'] = time();
        }
        if (!$content['session']){
            $content['session'] = session();
        }
        foreach ($content as $key => $value) {
            if (is_array($value)){
                $content[$key] = json_encode($value);
            }
        }
        return $this->dmpLogs2('net_task', $content);
    }

    /**
     * 电视广播样本的相关记录
     * @param $content
     * @return bool
     */
    public function tv_bc_sample_log($content)
    {
        if (!$content['time']){
            $content['time'] = time();
        }
        if (!$content['session']){
            $content['session'] = session();
        }
        foreach ($content as $key => $value) {
            if (is_array($value)){
                $content[$key] = json_encode($value);
            }
        }
        return $this->dmpLogs2('tv_bc_sample', $content);

    }

    public function ad_log($content)
    {
        if (!$content['time']){
            $content['time'] = time();
        }
        if (!$content['session']){
            $content['session'] = session();
        }
        foreach ($content as $key => $value) {
            if (is_array($value)){
                $content[$key] = json_encode($value);
            }
        }
        return $this->dmpLogs2('ad_info', $content);
    }

    public function paper_source_log($content)
    {
        if (!$content['time']){
            $content['time'] = time();
        }
        if (!$content['session']){
            $content['session'] = session();
        }
        foreach ($content as $key => $value) {
            if (is_array($value)){
                $content[$key] = json_encode($value);
            }
        }
        return $this->dmpLogs2('paper_source', $content);
    }

    public function paper_sample_log($content)
    {
        if (!$content['time']){
            $content['time'] = time();
        }
        if (!$content['session']){
            $content['session'] = session();
        }
        foreach ($content as $key => $value) {
            if (is_array($value)){
                $content[$key] = json_encode($value);
            }
        }
        return $this->dmpLogs2('paper_sample', $content);
    }

    public function task_score_log($content)
    {
        if (!$content['time']){
            $content['time'] = time();
        }
        if (!$content['session']){
            $content['session'] = session();
        }
        foreach ($content as $key => $value) {
            if (is_array($value)){
                $content[$key] = json_encode($value);
            }
        }
        return $this->dmpLogs2('task_input_score', $content);
    }

}