<?php
namespace Admin\Controller;
use Think\Controller;
class ChazhengTaskController extends BaseController {

	//查证列表
	public function index()
	{
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->display();
	}

	//查证列表接口
    public function index_list()
    {
        $p = I('page', 2);//当前第几页
        $pp = 20;//每页显示多少记录
        $fad_name = trim(I('fad_name'));//案件名称
        $fill_type = I('fill_type');//案件类型
        $fsttime = I('fsttime');//开始时间
        $fendtime = I('fendtime');//结束时间
        $fstatus = I('fstate');//状态

        if(!empty($fad_name)){
            $where_tid['fad_name'] = array('like','%'.$fad_name.'%');
        }
        if(!empty($fsttime) && !empty($fendtime)){
            $where_tid['fcreate_time'] = array('between',$fsttime.','.$fendtime);
        }
        if(!empty($fill_type)){
            $where_tid['fill_type'] = $fill_type;
        }
        if(!empty($fstatus)){
            $where_tid['fstatus'] = $fstatus;
        }else{
            $where_tid['fstatus'] = array('in',array(40,41,42));
        }

        $count = M('tbn_illegal_zdad')
            ->alias('a')
            ->join('customer b on a.fcustomer = b.cr_num')
            ->where($where_tid)
            ->count();

        $data = M('tbn_illegal_zdad')
            ->alias('a')
            ->field('b.cr_name,a.*')
            ->join('customer b on a.fcustomer = b.cr_num')
            ->where($where_tid)
            ->order('fstatus asc,fcreate_time desc')
            ->page($p,$pp)
            ->select();

        $this->ajaxReturn([
            'code' => 0,
            'data' => ['count'=>$count,'list'=>$data],
            'msg' => '获取数据成功'
        ]);

    }

    //查证详情
    public function view_chazheng(){

        $fid = I('fid');//线索ID
        $where_tia['a.fstatus']       = array('in',array(40,41,42));
        $where_tia['a.fid']           = $fid;

        $do_tia = M('tbn_illegal_zdad')
          ->field('a.*,b.ffullname')
          ->alias('a')
          ->join('tadclass b on a.ftadclass=b.fcode','left')
          ->where($where_tia)
          ->find();

        //获取所有附件
        $where_tia2['b.fid']    = $fid;
        $where_tia2['a.fstate'] = 0;
        $do_tiza = M('tbn_illegal_zdad_attach')
          ->alias('a')
          ->join('tbn_illegal_zdad b on a.fillegal_ad_id=b.fid')
          ->field('a.fid,a.fattach,a.fattach_url,a.ftype')
          ->where($where_tia2)
          ->select();

        $do_tia['files']['jbfiles'] = [];
        $do_tia['files']['czfiles'] = [];
        foreach ($do_tiza as $key => $value) {
          if($value['ftype'] == 0){
            $do_tia['files']['jbfiles'][] = $value;
          }elseif($value['ftype'] == 20){
            $do_tia['files']['czfiles'][] = $value;
          }
        }

        if(!empty($do_tia)){
          $this->ajaxReturn(array('code'=>0,'msg'=>'获取数据成功','data'=>$do_tia));
        }else{
          $this->ajaxReturn(array('code'=>1,'msg'=>'获取数据失败'));
        }
    }

    //查证处理接口
    public function report_process()
    {
        $user = session('personInfo');
        $fid = I('fid');//主键
        $attachinfo = I('attachinfo'); //上传附件
        $fczcontent = I('fczcontent');//处理说明

        $where_tia['fid'] = $fid;
        $do_tia = M('tbn_illegal_zdad')
            ->field('fstatus')
            ->where($where_tia)
            ->find();

        $data['fczuserid'] = $fid;//处理人
        $data['fstatus'] 	= 42;//处理完成状态
        $data['fcztime'] = date('Y-m-d H:i:s');//处理时间
        $data['fczcontent'] = $fczcontent;//处理说明

        $attach = [];
        //上传附件
        $attach_data['fillegal_ad_id']  = $fid;//违法广告
        $attach_data['fuploader']       = $user['fid'];//上传人
        $attach_data['fupload_time']    = date('Y-m-d H:i:s');//上传时间
        $attach_data['ftype']           = 20;//附件类型
        foreach ($attachinfo as $key2 => $value2){
            $attach_data['fattach']     = $value2['fattachname'];
            $attach_data['fattach_url'] = $value2['fattachurl'];
            array_push($attach,$attach_data);
        }
        if(!empty($attach)){
            M()->execute('update tbn_illegal_zdad_attach set fstate = 10 where fillegal_ad_id = '.$fid.' and ftype = 20');
            M('tbn_illegal_zdad_attach')->addAll($attach);
        }
        $res = M('tbn_illegal_zdad')->where('fid = '.$fid)->save($data);

        if($res !== false){
            $this->ajaxReturn([
                'code'=>0,
                'msg'=>'处理成功'
            ]);
        }
    }


}