function $stringify(param) {
	var query = []
	for (var k in param) {
		query.push(k + '=' + param[k])
	}
	return query.join('&')
}

function padLeftZero(str) {
	return ('00' + str).substr(str.length)
}

function formatDate(date, fmt) {
	if (/(y+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
	}
	let o = {
		'M+': date.getMonth() + 1,
		'd+': date.getDate(),
		'h+': date.getHours(),
		'm+': date.getMinutes(),
		's+': date.getSeconds()
	}
	for (let k in o) {
		if (new RegExp(`(${k})`).test(fmt)) {
			let str = o[k] + ''
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ?
				str :
				padLeftZero(str))
		}
	}
	return fmt
}

Vue.prototype.$ELEMENT = {
	size: 'mini',
	zIndex: 3000
}

var vm = new Vue({
	el: '#app',
	data() {
		return {
			videoUrl: '',
			form: {
				pageIndex: 1,
				date: '',
				fmediaid: ''
			},
			row: {},
			dialogVisible: false,
			allcount2: '0',
			tableData: [],
			activeNames: 0,
			activeNames2: 0,
			abtime: -1000,
			loading: true,
			illdialog: true,
			allIll: [],
			resultdata: {
				fstart: '',
				fend: '',
				fbrand: '',
				fadownername: '',
				fadowner: '',
				fmediaid: '',
				fadname: '',
				fadclasscode: '',
				fadownername: '',
				fspokesman: '',
				fillegaltypecode: '0',
				fexpressioncodes: '',
				fillegalcontent: ''
			},
			checkedIllegalArr: [],
			adOwner: {},
			sdate: '',
			fadclasscode: [],
			timer: null,
			Stime: 0,
			videoJump: '',
			fcousflag: '',
			moveflag: 1,
			mediaList: [],
			fid: '',
			adClassArr: [],
			illegalTypeArr: [],
			regionTree: [],
			illegalArr: [],
			activeTabName: 'adInfo',
			illegalKeyword: '',
			flag: false,
			tableloading: false,
			disabled: false,
			timeDate: [{
				starttime: '00:00:00',
				endtime: '03:59:59'
			}, {
				starttime: '04:00:00',
				endtime: '07:59:59'
			}, {
				starttime: '08:00:00',
				endtime: '11:59:59'
			}, {
				starttime: '12:00:00',
				endtime: '15:59:59'
			}, {
				starttime: '16:00:00',
				endtime: '19:59:59'
			}, {
				starttime: '20:00:00',
				endtime: '23:59:59'
			}],
			resultType: {
				'0': '遗漏',
				'1': '错误',
				'2': '预判'
			},
			propsOptions: {
				value: 'value',
				label: 'label',
				children: 'children'
			},
			showClassTip: false,
		}
	},
	created() {
		this._getCommonData()
		this._getmediaList()
	},
	destroyed() {
		this._cleartimer()
	},
	mounted() {
		this.illdialog = false
		this.$refs.heightcol2.style.height = this.$refs.heightcol.$el.clientHeight - 40 + 'px'
	},
	methods: {
		adNameBlur() {
      this.showClassTip = $h.testAdName(this.resultdata.fadname)
    },
		tmousedown() {
			this._cleartimer()
			this
				.$refs
				.player
				._stop()
		},
		tmousemove() {},
		tmouseup(mw) {
			let current = this
				._cutTime()
				.substring(11)
			this
				.$refs
				.player
				._seek(this._timeToSecond(current) + mw)
			setTimeout(() => {
				this._settimer()
			}, 200)
		},
		_settimer() {
			this._cleartimer()
			this.timer = setInterval(() => {
				this.videoJump = this._cutTime()
			}, 200)
		},
		_cleartimer() {
			clearInterval(this.timer)
			this.timer = null
		},
		seekVideoTime(row,$event) {
			this
				.$refs
				.player
				._seek(this._timeToSecond(row.fstarttime))
			let startTime = Math.floor(this._timeToSecond(row.fstarttime) * 1000) + this.Stime
			this.resultdata.fstart = formatDate(new Date(startTime), 'yyyy-MM-dd hh:mm:ss')
			let endTime = Math.floor(this._timeToSecond(row.fendtime) * 1000) + this.Stime
			this.resultdata.fend = formatDate(new Date(endTime), 'yyyy-MM-dd hh:mm:ss')
			this.videoJump = this.sdate + ' ' + row.fstarttime
			this._edit(row, $event, true)
		},
		_jumpplay(val) {
			this.videoJump = val
			this
				.$refs
				.player
				._seek(this._timeToSecond(this.videoJump.substring(11)))
		},
		_cutTime(type) {
			let time = Math.floor((this.$refs.player._getCurrentTime()) * 1000) + this.Stime
			if (type === 'start') {
				this.resultdata.fend = ''
				this.resultdata.fstart = formatDate(new Date(time), 'yyyy-MM-dd hh:mm:ss')
			} else if (type === 'end') {
				this.resultdata.fend = formatDate(new Date(time), 'yyyy-MM-dd hh:mm:ss')
			} else {
				return formatDate(new Date(time), 'yyyy-MM-dd hh:mm:ss')
			}
		},
		_timeToSecond(time) {
			let inputtime = new Date(`${this.sdate} ${time}`).getTime()
			let dstart = new Date(`${this.sdate} ${this.timeDate[this.activeNames2]['starttime']}`).getTime()
			let dend = new Date(`${this.sdate} ${this.timeDate[this.activeNames2]['endtime']}`).getTime()
			inputtime = (inputtime >= dstart) && (inputtime <= dend) ?
				inputtime :
				inputtime - 60 * 60 * 24 * 1000
			return (inputtime - this.Stime) / 1000
		},
		_addResult() {
			if (!this.resultdata.fstart) {
				this.$message.error('请截取开始时间！')
				return false
			}
			if (!this.resultdata.fend) {
				this.$message.error('请截取结束时间！')
				return false
			}
			if (!this.resultdata.fadname) {
				this.$message.error('请填写广告名称！')
				return false
			}
			if (!this.resultdata.fbrand) {
				this.resultdata.fbrand = '无突出品牌'
			}
			if (!this.resultdata.fadownername) {
				this.adOwner = {
					fid: '0',
					value: '广告主不详'
				}
				this.resultdata.fadownername = '广告主不详'
				this.resultdata.fadowner = '0'
			} else {
				if (this.resultdata.fadownername === this.adOwner.value) {
					if (this.resultdata.fadowner !== this.adOwner.fid) {
						this.resultdata.fadowner = '0'
					}
				} else {
					this.resultdata.fadowner = '0'
				}
			}
			let data = Object.assign({}, this.resultdata)
			if (!this.fadclasscode.length) {
				this.$message.error('请选择广告类型！')
				return false
			} else {
				data.fadclasscode = this.fadclasscode[this.fadclasscode.length - 1]
			}

			if (!$h.checkAdNameAndCode(data.fadname, data.fadclasscode)) {
        return this.$message.error('公益广告请按此格式填写：公益（xxxxxx）')
      }

			if (Number(this.resultdata.fillegaltypecode) && !this.checkedIllegalArr.length) {
				this.$message.error('请选择违法表现！')
			}
			if (Number(this.resultdata.fillegaltypecode) && !this.resultdata.fillegalcontent) {
				this.$message.error('请填写违法内容！')
			}
			if (Number(this.resultdata.fillegaltypecode)) {
				let b = this
					.$refs
					.illegalSelecter
					.getCheckedKeys()
					.filter(item => {
						return /\d/gi.test(item)
					})
				data.fexpressioncodes = b.join(';')
			}
			data.fmediaid = this.form.fmediaid
			let arr = []
			this
				.checkedIllegalArr
				.forEach(item => {
					arr.push(item.fexpression)
				})
			data.fexpressions = arr.join(';')
			if (this.fid)
				data.fid = this.fid
			let url = this.fid ?
				'/TaskInput/Standardization/EditAd' :
				'/TaskInput/Standardization/AddAd'
			axios
				.post(url, JSON.stringify(data))
				.then(res => {
					if (res.data.code === '0') {
						this
							.$message
							.success(res.data.msg)
						this._reset()
						this._dataInit(this.form.pageIndex)
					} else {
						this
							.$message
							.error(res.data.msg)
					}
				})
				.catch(() => {
					this
						.$message
						.error()
				})
		},
		_reset() {
			this.resultdata = {
					fstart: '',
					fend: '',
					fbrand: '',
					fadownername: '',
					fadowner: '',
					fmediaid: '',
					fadname: '',
					fadclasscode: '',
					fadownername: '',
					fspokesman: '',
					fillegaltypecode: '0',
					fexpressioncodes: '',
					fillegalcontent: ''
				},
				this.fadclasscode = []
			this.checkedIllegalArr = []
			this.fid = ''
			this.adOwner = {}
			this
				.$refs
				.illegalSelecter
				.setCheckedKeys([])
		},
		_defaultValue(val){
      if(val === 'fbrand' && !this.disabled){
        this.resultdata.fbrand = '无突出品牌'
      }
      if(val === 'fadownername' && !this.disabled){
        this.resultdata.fadownername = '广告主不详';
        this.resultdata.fadowner = '0'
      }
    },
		_focus(input) {
			this.fcousflag = input
		},
		_dataInit(val) {
			this.tableloading = true
			if (!this.sdate) {
				this._setDate()
			}
			this.form.pageIndex = val || 1
			let data = Object.assign({}, this.form)
			data.pageSize = '10'
			data.fstarttime = `${this.sdate} ${this.timeDate[this.activeNames2]['starttime']}`
			data.fendtime = `${this.sdate} ${this.timeDate[this.activeNames2]['endtime']}`
			data.fmediaid = this.form.fmediaid
			axios
				.post('/TaskInput/Standardization/ListAd', JSON.stringify(data))
				.then(res => {
					if (res.data.code === 0) {
						if (this.videoUrl !== res.data.data.live_m3u8_url) {
							this._m3u8TsartTime(res.data.data.live_m3u8_url)
							this.videoUrl = res.data.data.live_m3u8_url
						}
						this.tableData = res
							.data
							.data
							.fad
							.map(item => {
								item.fstarttime = item
									.fstarttime
									.substring(11)
								item.fendtime = item
									.fendtime
									.substring(11)
								return item
							})
						this.allcount2 = res.data.count || 0
					}
					this.tableloading = false
					this.loading = false
				})
		},
		_change(val) {
			if (typeof (val) === 'number') {
				this.activeNames = val
				this.activeNames2 = val
				this._cleartimer()
				this.videoUrl = ''
				this._dataInit()
				this._reset()
			}
		},
		_setAbTime() {
			let s = new Date(this.sdate + ' 00:00:00').getTime()
			let e = this.Stime
			if (this.abtime === -1000) {
				this.abtime = (s - e) / 1000
			}
		},
		_m3u8TsartTime(url) {
			axios
				.get(url)
				.then(res => {
					if (res.data.indexOf("http") !== -1) {
						let matchReg = res
							.data
							.split("\n")
							.filter(item => {
								return item.indexOf("http") !== -1
							})
						let a = matchReg[0]
						let arr = a.split('/')
						this.Stime = Number(arr[arr.length - 1].substring(0, 10)) * 1000
						this._setAbTime()
					}
				})
		},
		_getmediaList(query) {
			let data = {
				type: '01,02',
				medianame: query || ''
			}
			axios
				.post('/TaskInput/Standardization/getMediaList', JSON.stringify(data))
				.then(res => {
					if (res.data.code === 0) {
						this.mediaList = res.data.data
						if (!this.flag) {
							this.form.fmediaid = res.data.data[0].fid
							this._dataInit()
							this.flag = true
						}
					} else {
						this
							.$message
							.error(res.data.msg)
					}
				})
		},
		_getCommonData() {
			axios
				.post('/TaskInput/Standardization/getAdClassArr')
				.then(res => {
					if (res.data.code === 0) {
						this.adClassArr = res.data.data
					}
				})
			axios
				.post('/TaskInput/Standardization/getIllegalTypeTree')
				.then(res => {
					if (res.data.code === 0) {
						this.illegalTypeArr = res.data.data

					}
				})
			axios
				.post('/TaskInput/Standardization/getRegionArr')
				.then(res => {
					if (res.data.code === 0) {
						this.regionTree = res.data.data
					}
				})
			axios
				.post('/TaskInput/Standardization/getIllegalArr')
				.then(res => {
					if (res.data.code === 0) {
						this.illegalArr = res.data.data
						this.allIll = res
							.data
							.data
							.reduce((accumulator, currentValue) => {
								return accumulator.concat(currentValue.children)
							}, [])
					}
				})
		},
		searchadname(queryString, cb) {
			let data = {
				adName: queryString
			}
			axios
				.post('/Api/Ad/searchAdByName', JSON.stringify(data))
				.then(res => {
					if (res.data.code === 0) {
						cb(res.data.data)
					}
				})
		},
		searchadowner(queryString, cb) {
			let data = {
				name: queryString
			}
			axios
				.post('/Api/Adowner/searchAdOwner', JSON.stringify(data))
				.then(res => {
					if (res.data.code === 0) {
						cb(res.data.data)
					}
				})
		},
		selectOwner(item) {
			this.adOwner = item
			this.resultdata.fadownername = item.value
			this.resultdata.fadowner = item.fid
		},
		_setDate() {
			let yt = new Date().getTime() - 3600 * 24 * 1000
			this.sdate = formatDate(new Date(yt), 'yyyy-MM-dd')
		},
		_delete(obj, e) {
			let data = {
				fid: obj.fid,
				fmediaid: this.form.fmediaid,
				fissuedate: this.sdate
			}
			this
				.$confirm('确认删除该广告记录?', '提示', {
					confirmButtonText: '确定',
					cancelButtonText: '取消',
					type: 'warning'
				})
				.then(() => {
					axios
						.post('/TaskInput/Standardization/DelAd', JSON.stringify(data))
						.then(res => {
							if (res.data.ocode === '0') {
								this
									.$message
									.success(res.data.msg)
								this._dataInit()
							} else {
								this
									.$message
									.error(res.data.msg)
							}
						})
				})
				.catch(() => {
					this.$message({
						type: 'info',
						message: '已取消'
					})
				})
			e.stopPropagation()
		},
		_edit(obj, e, disabled) {
			this.disabled = disabled
			this.fadclasscode = [
				obj
				.fadclasscode
				.substring(0, 2),
				obj.fadclasscode
			]
			for (let key in this.resultdata) {
				this.fid = obj.fid
				if (key === 'fstart') {
					this.resultdata[key] = `${this.sdate} ${obj['fstarttime']}`
				} else if (key === 'fend') {
					this.resultdata[key] = `${this.sdate} ${obj['fendtime']}`
				} else if (key === 'fexpressioncodes') {
					this
						.$refs
						.illegalSelecter
						.setCheckedKeys(obj[key].split(';'))
					this.filterIll(obj[key].split(';'))
				} else if (key === 'fadowner') {
					this.resultdata[key] = obj[key] || '0'
					this.adOwner.fid = obj[key] || '0'
				} else if (key === 'fadownername') {
					this.resultdata[key] = obj[key] || '0'
					this.adOwner.value = obj[key] || '广告主不详'
				} else {
					this.resultdata[key] = obj[key]
				}
			}
			e.stopPropagation()
		},
		_mark(obj, e) {
			this.dialogVisible = true
			this.row = obj
			e.stopPropagation()
		},
		_sendmark(val, obj) {
			let data = {
				fid: obj.fid,
				fis_error: val
			}
			axios
				.post('/TaskInput/Standardization/MarkErrAd', JSON.stringify(data))
				.then(res => {
					if (res.data.code === '0') {
						this
							.$message
							.success(res.data.msg)
						this._dataInit()
					} else {
						this
							.$message
							.error(res.data.msg)
					}
					this.dialogVisible = false
					this.row = {}
				})
		},
		filterNode(value, data) {
			if (!value)
				return true
			return data
				.label
				.indexOf(value) !== -1
		},
		selectIllegal() {
			this.filterIll(this.$refs.illegalSelecter.getCheckedKeys())
		},
		filterIll(arr) {
			this.checkedIllegalArr = this
				.allIll
				.filter(item => {
					return arr.includes(item.fcode)
				})
		},
		changeIllegal() {
			if (this.checkedIllegalArr.length === 0) {
				Vue.set(this.resultdata, 'fexpressioncodes', '')
			}
			var arr = this
				.checkedIllegalArr
				.map((v) => {
					return v.fcode
				})
			var str = arr.join(';')
			Vue.set(this.resultdata, 'fexpressioncodes', str)
		}
	},
	computed: {
		medianame() {
			if (this.mediaList.length) {
				let a = this
					.mediaList
					.filter(item => {
						return item.fid === this.form.fmediaid
					})
				if (a.length) {
					return a[0].fmedianame
				}
			}
		},
		flength() {
			let start = new Date(this.resultdata.fstart).getTime()
			let end = new Date(this.resultdata.fend).getTime()
			if (start && end) {
				return (end - start) / 1000
			} else {
				return '0'
			}
		},
		duringtime() {
			return this.timeDate[this.activeNames2].starttime + ' - ' + this.timeDate[this.activeNames2].endtime
		},
		isIllegal() {
			return this.resultdata.fillegaltypecode !== '0'
		}
	},
	watch: {
		illegalKeyword(val) {
			this
				.$refs
				.illegalSelecter
				.filter(val)
		}
	}
})