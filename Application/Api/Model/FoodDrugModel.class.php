<?php
namespace Api\Model;
use Think\Exception;
use Think\Log;

/**
* 食品药品监督管理总局发布清单数据
* @Date: 2018年6月29日 上午09:48:54
*/
class FoodDrugModel{
	/**
	* 根据日期媒介分批获取发布记录并写入食药总局发布清单表
	* @Param	String $month_date 日期 。如"2018-06-03"
	* @Param	Int media_id 媒体ID
	* @Return	void 返回的数据
	*/
    public function food_drug_issue_2($month_date,$media_id){
		header('Content-Type: text/html; charset=utf-8');
    	set_time_limit(600);
		$dataAll = array();//将要插入的记录集
		$dataUp = array();//将要更新的记录集
		$dataSet = array();
		$dataRow = array();
		
		$where['flevel'] = array('IN','0,1');
		$where['fstate'] = 1;
		$regionPreArr = M('tregion')->cache(true,1200)
			->field('LEFT(fid,2) AS fid')
			->where($where)
			->select();
		$preArr = array(
			'01' => 'ttv',
			'02' => 'tbc',
			'03' => 'tpaper'
		);
		$mediaClass = M('tmedia')->cache(true,1200)
			->field('LEFT(fmediaclassid,2) AS mediaclass')
			->where(array('fid'=>$media_id))
			->find();
		$pre = $preArr[$mediaClass['mediaclass']];
		$strAdClass = '01,02,03,05,06';//01-药品类 02-医疗器械类 03-化妆品类 05-普通食品 06-保健食品类
		$Ym = date('Ym',strtotime($month_date));
		$tableName = 'food_drug_issue_'.$Ym;//要写入数据的表名
		$hasTable = M()->query('SHOW TABLES LIKE "'.$tableName.'"');//判断表是否存在
		$condition['T.fmediaid'] = $media_id;
		if ($pre == 'tpaper'){
			$suf = '';
			$issueTable = $pre.'issue'.$suf;//发布记录表名 如:tpaperissue
			$sampleTable = $pre.'sample';//样本表名
			$field = 'tmedia.fmediaclassid,
				T.fpapersampleid AS fsampleid,
				T.fmediaid,
				tmedia.fmedianame,
				tmediaowner.fregionid,
				tad.fadname,
				tad.fadowner,
				tad.fbrand,
				'.$sampleTable.'.fspokesman,
				tad.fadclasscode,
				T.fissuedate,
				'.$sampleTable.'.fillegaltypecode,
				'.$sampleTable.'.fadmanuno,
				'.$sampleTable.'.fmanuno,
				'.$sampleTable.'.fadapprno,
				'.$sampleTable.'.fapprno,
				'.$sampleTable.'.fadent,
				'.$sampleTable.'.fent,
				'.$sampleTable.'.fentzone,
				'.$sampleTable.'.fexpressioncodes';
			$condition['T.fissuedate'] = date('Y-m-d H:i:s',strtotime($month_date));
			$condition['LEFT(tad.fadclasscode,2)'] = array('in',$strAdClass);
			try {
				$dataSet = M($issueTable)
					->alias('T')
					->cache(true,1200)
					->field($field)
					->join('tmedia ON tmedia.fid = T.fmediaid','LEFT')
					->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid','LEFT')
					->join('tregion ON tregion.fid = tmediaowner.fregionid','LEFT')
					->join($sampleTable.' ON '.$sampleTable.'.fpapersampleid = T.fpapersampleid','LEFT')
					->join('tad ON tad.fadid = tpapersample.fadid','LEFT')
					->where($condition)
					->select();
			} catch (Exception $e) {
				Log::write($e);
				//TODO:异常处理
			}
			if (!empty($dataSet)){
				foreach ($dataSet as $row=>$cols){
					if($hasTable){//表存在
						$fRepeat = array(
							'fmediaid' => $cols['fmediaid'],
							'fstarttime' => $cols['fstarttime']
						);
						$nRep = intval(M($tableName)->where($fRepeat)->count());
					}
					if(!$hasTable || $nRep == 0){//表不存在或无重复记录 写入数据
						$dataRow[$row]['fmediaclassid'] = $cols['fmediaclassid'];
						$dataRow[$row]['fsampleid'] = $cols['fsampleid'];
						$dataRow[$row]['fmediaid'] = $cols['fmediaid'];
						$dataRow[$row]['fmedianame'] = $cols['fmedianame'];
						$dataRow[$row]['fregionid'] = $cols['fregionid'];
						$dataRow[$row]['fadname'] = $cols['fadname'];
						$dataRow[$row]['fadowner'] = $cols['fadowner'];
						$dataRow[$row]['fbrand'] = $cols['fbrand'];
						$dataRow[$row]['fspokesman'] = $cols['fspokesman'];
						$dataRow[$row]['fadclasscode'] = $cols['fadclasscode'];
						$dataRow[$row]['fstarttime'] = $cols['fstarttime'];
						$dataRow[$row]['fendtime'] = $cols['fendtime'];
						$dataRow[$row]['fissuedate'] = date('Y-m-d',$cols['fissuedate']);
						$dataRow[$row]['flength'] = $cols['flength'];
						$dataRow[$row]['fillegaltypecode'] = $cols['fillegaltypecode'];
						$dataRow[$row]['fadmanuno'] = $cols['fadmanuno'];
						$dataRow[$row]['fmanuno'] = $cols['fmanuno'];
						$dataRow[$row]['fadapprno'] = $cols['fadapprno'];
						$dataRow[$row]['fapprno'] = $cols['fapprno'];
						$dataRow[$row]['fadent'] = $cols['fadent'];
						$dataRow[$row]['fent'] = $cols['fent'];
						$dataRow[$row]['fentzone'] = $cols['fentzone'];
						$dataRow[$row]['fexpressioncodes'] = $cols['fexpressioncodes'];
					}
				}
				array_push($dataAll, $dataRow);
			}
			Log::write('参数：日期 '.$month_date.',媒介 '.$media_id.'。结果集：'.json_encode($dataAll).',DataSet:'.json_encode($dataSet).',DataRow:'.json_encode($dataRow),Log::DEBUG);
		}else{
			foreach ($regionPreArr as $regionPre){
				$suf = '_'.$Ym.'_'.$regionPre['fid'];
				$issueTable = $pre.'issue'.$suf;//发布记录表名 如:ttvissue_201712_10
				$sampleTable = $pre.'sample';//样本表名
				$field = 'tmedia.fmediaclassid,
					T.fsampleid,
					T.fmediaid,
					tmedia.fmedianame,
					T.fregionid,
					tad.fadname,
					tad.fadowner,
					tad.fbrand,
					'.$sampleTable.'.fspokesman,
					tad.fadclasscode,
					T.fstarttime,
					T.fendtime,
					T.fissuedate,
					T.flength,
					'.$sampleTable.'.fillegaltypecode,
					'.$sampleTable.'.fadmanuno,
					'.$sampleTable.'.fmanuno,
					'.$sampleTable.'.fadapprno,
					'.$sampleTable.'.fapprno,
					'.$sampleTable.'.fadent,
					'.$sampleTable.'.fent,
					'.$sampleTable.'.fentzone,
					'.$sampleTable.'.fexpressioncodes';
				$condition['T.fissuedate'] = strtotime($month_date);
				$condition['LEFT(tad.fadclasscode,2)'] = array('in',$strAdClass);//五个类别
				try {
					$dataSet = M($issueTable)
						->alias('T')
						->cache(true,1200)
						->field($field)
						->join('tmedia ON tmedia.fid = T.fmediaid','LEFT')
						->join('tregion ON tregion.fid = T.fregionid','LEFT')
						->join($sampleTable.' ON '.$sampleTable.'.fid = T.fsampleid','LEFT')
						->join('tad ON tad.fadid = ttvsample.fadid','LEFT')
						->where($condition)
						->select();
				} catch (Exception $e) {
					Log::write($e);
					//TODO:异常处理
				}
				if (!empty($dataSet)){
					$nRepAll = 0;//重复记录总数
					$nRep = 0;//完全重复记录计数(丢弃)
					$nUp = 0;//需更新记录计数
					foreach ($dataSet as $row=>$cols){
						if($hasTable){//表存在
							$fRepeat = array(
								'fmediaid' => $cols['fmediaid'],
								'fstarttime' => $cols['fstarttime']
							);
							$dataR = M($tableName)->where($fRepeat)->select();
							if (!empty($dataR)){
								$nRepAll++;
								//存在相同记录
								foreach ($dataR as $k => $v){
									sort($v);
									sort($cols);
									if ($v == $cols){
										//完全相同的记录不插入不更新
										$nRep++;
									}else{
										//更新该条记录
										$nUp++;
										$upReturn = M($tableName)
											->where($fRepeat)
											->save($cols);
										//array_push($dataUp, $cols);
									}
								}
							}else{
								$nRepAll = 0;
							}
							//$nRep = intval(M($tableName)->where($fRepeat)->count());//是否重复
						}
						if(!$hasTable || $nRepAll == 0){//表不存在或无重复记录 写入数据
							$dataRow[$row]['fmediaclassid'] = $cols['fmediaclassid'];
							$dataRow[$row]['fsampleid'] = $cols['fsampleid'];
							$dataRow[$row]['fmediaid'] = $cols['fmediaid'];
							$dataRow[$row]['fmedianame'] = $cols['fmedianame'];
							$dataRow[$row]['fregionid'] = $cols['fregionid'];
							$dataRow[$row]['fadname'] = $cols['fadname'];
							$dataRow[$row]['fadowner'] = $cols['fadowner'];
							$dataRow[$row]['fbrand'] = $cols['fbrand'];
							$dataRow[$row]['fspokesman'] = $cols['fspokesman'];
							$dataRow[$row]['fadclasscode'] = $cols['fadclasscode'];
							$dataRow[$row]['fstarttime'] = $cols['fstarttime'];
							$dataRow[$row]['fendtime'] = $cols['fendtime'];
							$dataRow[$row]['fissuedate'] = date('Y-m-d',$cols['fissuedate']);
							$dataRow[$row]['flength'] = $cols['flength'];
							$dataRow[$row]['fillegaltypecode'] = $cols['fillegaltypecode'];
							$dataRow[$row]['fadmanuno'] = $cols['fadmanuno'];
							$dataRow[$row]['fmanuno'] = $cols['fmanuno'];
							$dataRow[$row]['fadapprno'] = $cols['fadapprno'];
							$dataRow[$row]['fapprno'] = $cols['fapprno'];
							$dataRow[$row]['fadent'] = $cols['fadent'];
							$dataRow[$row]['fent'] = $cols['fent'];
							$dataRow[$row]['fentzone'] = $cols['fentzone'];
							$dataRow[$row]['fexpressioncodes'] = $cols['fexpressioncodes'];
						}
					}
					array_push($dataAll, $dataRow);
				}
			}
		}
		Log::write('参数：日期 '.$month_date.',媒介 '.$media_id.'。结果集：'.json_encode($dataAll).',DataRow:'.json_encode($dataRow),Log::DEBUG);
		if (!empty($dataAll)) {
			foreach ($dataAll as $data){
				try{//尝试写入数据，如果写入失败则可能是没有创建表
					M($tableName)->addAll($data,array(),true);
				}catch(Exception $error) {//如果写入失败
					A('Common/SynIssue','Model')->createFoodDrugTable($tableName);	//创建表
					M($tableName)->addAll($data,array(),true);
				}
			}
		}else{
			echo '未找到符合条件的记录';
		}
	}
	
	
	
	/**
	* 根据日期媒介分批获取发布记录并写入食药总局发布清单表
	* @Param	String $month_date 日期 。如"2018-06-03"
	* @Param	Int media_id 媒体ID
	* @Return	void 返回的数据
	*/
    public function food_drug_issue($month_date,$media_id){
	
		$mediaInfo = M('tmedia')
								->cache(true,86400)
								->field('tmediaowner.fregionid,fmedianame,fmediaclassid')
								->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>$media_id))->find();//查询媒介信息
		
		
		
		$mediaclassid = substr($mediaInfo['fmediaclassid'],0,2);
		
		if($mediaclassid == '01'){
			$media_tab = 'tv';
			$table_name = 'ttvissue_'.date('Ym',strtotime($month_date)).'_'.substr($mediaInfo['fregionid'],0,2);
		}elseif($mediaclassid == '02'){
			$media_tab = 'bc';
			$table_name = 'tbcissue_'.date('Ym',strtotime($month_date)).'_'.substr($mediaInfo['fregionid'],0,2);
		}elseif($mediaclassid == '03'){
			$media_tab = 'paper';
			$table_name = 'tpaperissue';
		}else{
			return false;
		}
		
		
		$table_name_0 = 'food_drug_issue_'.date('Ym',strtotime($month_date));
		
		if(!S('table_'.$table_name_0.'_exist')){//判断缓存是否存在，如果存在就当表存在
			$hasTable = M()->query('SHOW TABLES LIKE "'.$table_name_0.'"');//判断表是否存在
			if(!$hasTable){
				A('Common/SynIssue','Model')->createFoodDrugTable($table_name_0);	//创建表
				S('table_'.$table_name_0.'_exist',1,86400);//写入缓存
			}
		}
		

		$data0ListField = 'fmediaclassid,fsampleid,fmedianame,fregionid,fadname,fadowner,fbrand,fspokesman,fadclasscode,fstarttime,fendtime,sam_source_path';
		$data0ListField .= ',fmediaid,fissuedate,flength,fillegaltypecode,fadmanuno,fmanuno,fadapprno,fapprno,fadent,fent,fentzone,fexpressioncodes,fillegalcontent';
		
		$data0List = M($table_name_0)
									->field($data0ListField)
									->where(array('fmediaid'=>$media_id,'fissuedate'=>$month_date))
									->select();//查询食药数据表信息，用于排重

		if($media_tab == 'tv' || $media_tab == 'bc'){
			foreach($data0List as $data0){
				$data3[$media_id.'_'.$data0['fstarttime']] = $data0;//用于排重的数据
			}
			try{//
				$data2List = M($table_name)
							->alias('tissue')
							->cache(true,1200)
							->field('
									tissue.fsampleid,
									tissue.fmediaid,
									tissue.fstarttime,
									tissue.fendtime,
									tad.fadname,
									tad.fadowner,
									tad.fbrand,
									tsample.fspokesman,
									tad.fadclasscode,
									tissue.fissuedate,
									tsample.fillegaltypecode,
									tsample.fadmanuno,
									tsample.fmanuno,
									tsample.fadapprno,
									tsample.fapprno,
									tsample.fadent,
									tsample.fent,
									tsample.fentzone,
									tsample.fexpressioncodes,
									tsample.fillegalcontent,
									tsample.favifilename as sam_source_path
									
									
									')

							->join('t'.$media_tab.'sample as tsample on tsample.fid = tissue.fsampleid')
							->join('tad on tad.fadid = tsample.fadid')
							->where(array('tissue.fmediaid'=>$media_id,'tissue.fissuedate'=>strtotime($month_date),'left(tad.fadclasscode,2)'=>array('in','01,02,03,05,06')))
							->select();	
			}catch(Exception $error) {//
				return false;
			}

			foreach($data2List as $data2){
				
				$e_a_data['fmediaclassid'] = $mediaclassid;//媒介类型
				$e_a_data['fsampleid'] = $data2['fsampleid']; //样本id
				$e_a_data['fmediaid'] = $data2['fmediaid']; //媒介id
				$e_a_data['fmedianame'] = $mediaInfo['fmedianame'];//媒介名称
				$e_a_data['fregionid'] = $mediaInfo['fregionid']; //地区id
				$e_a_data['fadname'] = $data2['fadname'];//广告名称
				$e_a_data['fadowner'] = $data2['fadowner'];//广告主
				$e_a_data['fbrand'] = $data2['fbrand']; //品牌
				$e_a_data['fspokesman'] = $data2['fspokesman']; //代言人
				$e_a_data['fadclasscode'] = $data2['fadclasscode']; //广告类别id
				$e_a_data['fstarttime'] = $data2['fstarttime']; //发布开始时间戳
				$e_a_data['fendtime'] = $data2['fendtime']; //发布结束时间戳
				$e_a_data['fissuedate'] = date('Y-m-d',$data2['fissuedate']); //发布日期
				$e_a_data['flength'] = $data2['fendtime'] - $data2['fstarttime']; //发布时长(秒)
				$e_a_data['fillegaltypecode'] = $data2['fillegaltypecode']; //违法类型
				$e_a_data['fadmanuno'] = $data2['fadmanuno']; //广告中标识的生产批准文号
				$e_a_data['fmanuno'] = $data2['fmanuno']; //生产批准文号
				$e_a_data['fadapprno'] = $data2['fadapprno']; //广告中标识的广告批准文号
				$e_a_data['fapprno'] = $data2['fapprno']; //广告批准文号
				$e_a_data['fadent'] = $data2['fadent']; //广告中标识的生产企业（证件持有人）名称
				$e_a_data['fent'] = $data2['fent']; //生产企业（证件持有人）名称
				$e_a_data['fentzone'] = $data2['fentzone']; //生产企业（证件持有人）所在地区
				$e_a_data['fexpressioncodes'] = $data2['fexpressioncodes']; //'违法表现代码，用“;”分隔
				$e_a_data['fillegalcontent'] = strval($data2['fillegalcontent']); //'涉嫌违法内容
				$e_a_data['sam_source_path'] = strval($data2['sam_source_path']); //'素材路径
				

				if(!$data3[$media_id.'_'.$data2['fstarttime']]){
					$fid = M($table_name_0)->add($e_a_data);

				}else{
					$need_save = false;
					foreach($e_a_data as $data_field => $data_value){//循环判断数据字段是否有修改
						
						if($data_value != $data3[$media_id.'_'.$data2['fstarttime']][$data_field]){
							$e_data[$data_field] = $data_value;//赋值修改字段
							$need_save = 1;//把需要修改设为1
						}
					}
					
					if($need_save){
						$e_state = M($table_name_0)->where(array('fstarttime'=>$data2['fstarttime'],'fmediaid'=>$data2['fmediaid']))->save($e_data);
					}
				}
				

				
			}				
							
			  
			
			
		}elseif($media_tab == 'paper'){
			
			foreach($data0List as $data0){
				$data3[$media_id.'_'.$data0['fissuedate'].'_'.md5($data0['sam_source_path'])] = $data0;//用于排重的数据
			}
			
			$data2List = M($table_name)
							->alias('tissue')
							->cache(true,1200)
							->field('
									tissue.fpapersampleid as fsampleid,
									tissue.fmediaid,
									0 as fstarttime ,
									0 as fendtime,
									tad.fadname,
									tad.fadowner,
									tad.fbrand,
									tsample.fspokesman,
									tad.fadclasscode,
									tissue.fissuedate,
									tsample.fillegaltypecode,
									tsample.fadmanuno,
									tsample.fmanuno,
									tsample.fadapprno,
									tsample.fapprno,
									tsample.fadent,
									tsample.fent,
									tsample.fentzone,
									tsample.fexpressioncodes,
									tsample.fillegalcontent,
									tsample.fjpgfilename as sam_source_path
									
									
									')

							->join('tpapersample as tsample on tsample.fpapersampleid = tissue.fpapersampleid')
							->join('tad on tad.fadid = tsample.fadid')
							->where(array('tissue.fmediaid'=>$media_id,'tissue.fissuedate'=>$month_date,'left(tad.fadclasscode,2)'=>array('in','01,02,03,05,06')))
							->select();	
							
			foreach($data2List as $data2){
				
				$e_a_data['fmediaclassid'] = $mediaclassid;//媒介类型
				$e_a_data['fsampleid'] = $data2['fsampleid']; //样本id
				$e_a_data['fmediaid'] = $data2['fmediaid']; //媒介id
				$e_a_data['fmedianame'] = $mediaInfo['fmedianame'];//媒介名称
				$e_a_data['fregionid'] = $mediaInfo['fregionid']; //地区id
				$e_a_data['fadname'] = $data2['fadname'];//广告名称
				$e_a_data['fadowner'] = $data2['fadowner'];//广告主
				$e_a_data['fbrand'] = $data2['fbrand']; //品牌
				$e_a_data['fspokesman'] = $data2['fspokesman']; //代言人
				$e_a_data['fadclasscode'] = $data2['fadclasscode']; //广告类别id
				$e_a_data['fstarttime'] = $data2['fstarttime']; //发布开始时间戳
				$e_a_data['fendtime'] = $data2['fendtime']; //发布结束时间戳
				$e_a_data['fissuedate'] = date('Y-m-d',strtotime($data2['fissuedate'])); //发布日期
				$e_a_data['flength'] = $data2['fendtime'] - $data2['fstarttime']; //发布时长(秒)
				$e_a_data['fillegaltypecode'] = $data2['fillegaltypecode']; //违法类型
				$e_a_data['fadmanuno'] = $data2['fadmanuno']; //广告中标识的生产批准文号
				$e_a_data['fmanuno'] = $data2['fmanuno']; //生产批准文号
				$e_a_data['fadapprno'] = $data2['fadapprno']; //广告中标识的广告批准文号
				$e_a_data['fapprno'] = $data2['fapprno']; //广告批准文号
				$e_a_data['fadent'] = $data2['fadent']; //广告中标识的生产企业（证件持有人）名称
				$e_a_data['fent'] = $data2['fent']; //生产企业（证件持有人）名称
				$e_a_data['fentzone'] = $data2['fentzone']; //生产企业（证件持有人）所在地区
				$e_a_data['fexpressioncodes'] = $data2['fexpressioncodes']; //'违法表现代码，用“;”分隔
				$e_a_data['fillegalcontent'] = strval($data2['fillegalcontent']); //'涉嫌违法内容
				$e_a_data['sam_source_path'] = strval($data2['sam_source_path']); //'素材路径

				
				
				
				

				if(!$data3[$media_id.'_'.$month_date.'_'.md5($data2['sam_source_path'])]){
					
					
					$fid = M($table_name_0)->add($e_a_data);
					
				}else{
					$need_save = false;
					foreach($e_a_data as $data_field => $data_value){//循环判断数据字段是否有修改
						if($data_value != $data3[$media_id.'_'.$month_date.'_'.md5($data2['sam_source_path'])][$data_field]){
							$e_data[$data_field] = $data_value;//赋值修改字段
							$need_save = 1;//把需要修改设为1
						}
					}
					if($need_save){//需要修改
						$e_state = M($table_name_0)->where(array('fstarttime'=>strtotime($data2['fstarttime']),'fmediaid'=>$data2['fmediaid']))->save($e_data);
					}
				}
				

				
			}								
			
			
		}
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
}