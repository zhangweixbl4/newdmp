var vm = new Vue({
  el: "#app",
  data() {
    return {
      form: {
        fc: '',
        va: ''
      },
      msg: '',
      timer: null,
    }
  },
  watch: {
    'form.fc': {
      handler(val) {
        this.form.va = ''
        if (val) {
          clearTimeout(this.timer)
          this.timer = setTimeout(() => {
            this.search()
          },500)
        }
      }
    }
  },
  methods: {
    search() {
      const { fc } = this.form
      $fly.post('/Test/Iyt/g', { fc }).then(res => {
        this.form.va = res.va
      })
    },
    onSubmit() {
      $fly.post('/Test/Iyt/p', this.form).then(res => {
        this.msg = res.msg
        setTimeout(() => {
          this.msg = ''
        },1000)
        this.$refs.fc.focus()
      })
    }
  },
})