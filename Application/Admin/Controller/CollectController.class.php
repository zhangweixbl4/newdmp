<?php
namespace Admin\Controller;
use Think\Controller;
class CollectController extends BaseController {
	
	
	

    public function index(){

    	if(A('Admin/Authority','Model')->authority('200084') === 0){
			exit('没有权限');
		}
		
		
		
		
		$cache_ftype_arr = S('collect_type_arr');//缓存的采集点类型
		$cache_ftype_list = array();
		foreach($cache_ftype_arr as $cache_ftype){
			$cache_ftype_list[] = array('label'=>$cache_ftype);
		}
		$region_authority = A('Admin/Authority','Model')->region_authority();//获取地区权限
		$where = array();
		$whrer['fstate'] = array('neq',-1);
		if($region_authority){
			$where['left(fregionid,2)'] = array('in',$region_authority);
		}
		
		$CollectionLocationList = M('tcollect')->where($where)->field('fid,fname,fcode,flng as lng ,flat as lat,1000000 as count ')->select();//查询采集点
		//var_dump(M('tcollect')->getLastSql());
		$this->assign('cache_ftype_list',$cache_ftype_list);
		$this->assign('CollectionLocationList',json_encode($CollectionLocationList));//采集点数据,json数据
		$this->display();
	}
	
	public function index3(){

		$CollectionLocationList = M('tcollect')->where(array('fstate'=>array('neq',-1)))->field('fid,fname,fcode,flng as lng ,flat as lat,1000000 as count')->select();//查询采集点

		
		$this->assign('CollectionLocationList',json_encode($CollectionLocationList));//采集点数据,json数据
		$this->display();
	}
	
	
	
	/*添加采集点*/
	public function add_edit_collect(){
		
		$fid= I('fid');//采集点id
		$fid = intval($fid);//转为数字
		$a_e_data = array();
		$a_e_data['fcode'] = I('fcode');//采集点编码
		$a_e_data['fname'] = I('fname');//采集点名称
		$a_e_data['fregionid'] = I('fregionid',0);//行政区划代码
		$a_e_data['faddress'] = I('faddress');//详细地址
		$a_e_data['fmanager'] = I('fmanager');//管理人
		$a_e_data['ftel'] = I('ftel');//联系电话
		$a_e_data['ftype'] = I('ftype');//类型
		//S('collect_type_arr',null);
		$cache_ftype_arr = S('collect_type_arr');//缓存的采集点类型
		$cache_ftype_arr[] = $a_e_data['ftype'];//加入新的采集点类型
		S('collect_type_arr',array_unique($cache_ftype_arr),864000);//存入缓存
		
		
		
		$a_e_data['fstate'] = I('fstate',1);//状态，-1删除，0-无效，1-有效
		
		//if(mb_strlen($a_e_data['fcode']) != 8) $this->ajaxReturn(array('code'=>-1,'msg'=>'采集点编码必须8位数字'));//返回失败
		if($a_e_data['fname'] == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'采集点名称不能为空'));//返回失败
		
		$region_authority = A('Admin/Authority','Model')->region_authority();
		
		if(!in_array(substr($a_e_data['fregionid'],0,2),$region_authority) && $region_authority){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有该地区的权限'));//返回失败
		}
		
		$region_fullname = M('tregion')->where(array('fid'=>$a_e_data['fregionid']))->getField('ffullname');//通过行政区划代码查询行政区划全名
		$baidu_api_url = 'http://api.map.baidu.com/geocoder/v2/?address='.$region_fullname.$a_e_data['faddress'].'&output=json&ak='.C('BAIDU_MAP_AK');//百度地图获取经纬度api
		//var_dump($baidu_api_url);

		$baidu_data = http($baidu_api_url);//向百度地图交换经纬度

		$baidu_data = json_decode($baidu_data,true);//json转为数组

		$a_e_data['flng'] = $baidu_data['result']['location']['lng'];//地址的经度
		$a_e_data['flat'] = $baidu_data['result']['location']['lat'];//地址的维度
		if($a_e_data['flng'] == '' || $a_e_data['flat'] == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'经纬度采集失败,请检查地址是否详细'));//返回失败
		
		if($fid == 0){//判断是添加还是修改
			if(A('Admin/Authority','Model')->authority('200081') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
			}
			$is_repeat = M('tcollect')->where(array('fcode'=>$a_e_data['fcode']))->count();//查询fcode是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'采集点编码重复'));//返回采集点编码重复
			
			$a_e_data['fcreator'] = session('personInfo.fid').'_'.session('personInfo.fname');//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tcollect')->add($a_e_data);//添加数据
			if($rr > 0){//判断是否添加成功
				$collect = array('lng'=>$a_e_data['flng'],'lat'=>$a_e_data['flat'],'fid'=>$rr,'fname'=>$a_e_data['fname'],'fcode'=>$a_e_data['fcode']);//经纬度信息,用于前端渲染采集点
				$this->ajaxReturn(array('code'=>0,'collect'=>$collect,'msg'=>'添加成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));//返回失败
			}
			
		}else{
			if(A('Admin/Authority','Model')->authority('200082') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
			}
			$is_repeat = M('tcollect')->where(array('fid'=>array('neq',$fid),'fcode'=>$a_e_data['fcode']))->count();//查询fcode是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'采集点编码重复'));//返回采集点编码重复
			
			$a_e_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tcollect')->where(array('fid'=>$fid))->save($a_e_data);//修改数据
			if($rr > 0){//判断是否修改成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));//返回失败
			}
		}
		

	}
	
	/*采集点列表*/
	public function ajax_collect_list(){
		
		$p = I('p');//分页的第几页
		$pp = 10;//每页显示多少条记录
		$keyword = I('keyword');//获得搜索关键字
		
		
		$region_authority = A('Admin/Authority','Model')->region_authority();//获取地区权限


		
		$where = array();//初始化搜索条件
		if($region_authority){
			$where['left(fregionid,2)'] = array('in',$region_authority);
		}
		$where['tcollect.fstate'] = array('neq',-1);//查询状态为未删除的
		$where['tcollect.fid'] = array('gt',0);//查询fid大于0的
		
		if($keyword != ''){
			$where['tcollect.fname'] = array('like','%'.$keyword.'%');//搜索条件
		}
		
		
		
		$collectList = M('tcollect')->where($where)->page($p.','.$pp)->select();
		$collectList = list_k($collectList,$p,$pp);//为列表加上序号
		foreach($collectList as $key => $value){
			// $collectList[$key] = $value;
			$collectList[$key]['device_url'] = U('Admin/Device/index',array('collect_name'=>$value['fname'],'collect_id'=>$value['fid']));
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','pp'=>$pp,'collectList'=>$collectList));
		
		
		
	}
	
	
	
	/*采集点详情*/
	public function ajax_collect_details(){
		
		$fid = I('fid');//获取采集点ID
		$collectDetails = M('tcollect')->where(array('fid'=>$fid))->find();
		$collectDetails['region_name'] = M('tregion')->where(array('fid'=>$collectDetails['fregionid']))->getField('ffullname');//行政区划全称
		$collectDetails['device_count'] = M('tdevice')->where(array('fcollectid'=>$collectDetails['fid'],'fstate'=>array('neq',-1)))->count();//设备数量
		$collectDetails['media_count'] = M('tmedia')
												->join('tdevice on tmedia.fdeviceid = tdevice.fid')
												->where(array('tdevice.fcollectid'=>$collectDetails['fid'],'tmedia.fstate'=>array('neq',-1)))
												->count();//媒体数量
		
		
		$this->ajaxReturn(array('code'=>0,'collectDetails'=>$collectDetails));
	}
	
	/*删除采集点*/
	public function ajax_collect_del(){
		if(A('Admin/Authority','Model')->authority('200083') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
		}
		$fid = I('fid');//获取采集点ID
		M('tcollect')->where(array('fid'=>$fid))->save(array('fstate'=>-1));
		$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'.$fid));
		
	}
	
	
	/*获取采集点的经纬度*/
	
	public function get_collect_lng_lat(){
		//exit;
		$collectList = M('tcollect')->where('flng < 1')->limit(100)->select();
		foreach($collectList as $collect){
			$region_fullname = M('tregion')->where(array('fid'=>$collect['fregionid']))->getField('ffullname');//通过行政区划代码查询行政区划全名
			$baidu_api_url = 'http://api.map.baidu.com/geocoder/v2/?address='.$region_fullname.$collect['faddress'].'&output=json&ak='.C('BAIDU_MAP_AK');//百度地图获取经纬度api


			$baidu_data = http($baidu_api_url);//向百度地图交换经纬度

			$baidu_data = json_decode($baidu_data,true);//json转为数组

			$a_e_data['flng'] = $baidu_data['result']['location']['lng'];//地址的经度
			$a_e_data['flat'] = $baidu_data['result']['location']['lat'];//地址的维度
			$a_e_data['fname'] = $region_fullname.'采集点';//采集点名称
			M('tcollect')->where(array('fid'=>$collect['fid']))->save($a_e_data);
			///var_dump($a_e_data);
		}
		
		
	}
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}