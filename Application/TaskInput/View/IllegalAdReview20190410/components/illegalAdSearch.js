const illegalAdSearch = {
    template: `<div v-loading="loading" style="padding:10px">
    <el-form
            :inline="true"
            :model="where"
            ref="whereForm"
            @keyup.enter.native="search"
    >
        <el-row>
            <el-col :span="8">
                <el-form-item label="广告名称" prop="adName">
                    <el-input v-model="where.adName" clearable></el-input>
                </el-form-item>
            </el-col>

            <el-col :span="8">
                <el-form-item label="地区" prop="regionId" class="regionSearch">
                    <el-cascader
                            v-model="searchTempWhere.region"
                            :options="region"
                            change-on-select
                            :filterable="true"
                            clearable
                    ></el-cascader>
                    <el-checkbox v-model="searchTempWhere.onlyThisLevel">只搜索本级</el-checkbox>
                </el-form-item>
            </el-col>
        </el-row>
        <el-row>
            <el-col :span="8">
                <el-form-item label="广告类别" prop="adClass">
                    <el-cascader
                            filterable
                            :options="adClassArr"
                            v-model="searchTempWhere.searchAdClassArr1"
                            change-on-select
                            clearable
                    ></el-cascader>
                </el-form-item>
            </el-col>
            <el-col :span="8">
                <el-form-item label="媒体名称" prop="mediaName">
                    <el-input v-model="where.mediaName" clearable></el-input>
                </el-form-item>
            </el-col>
            <el-col :span="8" v-if="userType === 'customer'">
                <el-form-item class="text-right">
                    <el-button type="primary" @click="search">搜索</el-button>
                    <el-button @click="resetForm">重置</el-button>
                    <el-button @click="refreshPage">返回</el-button>
                </el-form-item>
            </el-col>
        </el-row>
        <el-row v-if="userType === 'dmp'">
            <el-col :span="8">
                <el-form-item label="商业类别">
                    <el-cascader
                            filterable
                            :options="adClassTree"
                            v-model="searchTempWhere.searchAdClassArr2"
                            change-on-select
                            :filterable="true"
                            clearable
                    ></el-cascader>
                </el-form-item>
            </el-col>
            <el-col :span="8">
                <el-form-item class="text-right">
                    <el-button type="primary" @click="search">搜索</el-button>
                    <el-button @click="resetForm">重置</el-button>
                    <el-button @click="refreshPage">返回</el-button>
                    
                </el-form-item>
            </el-col>
        </el-row>
    </el-form>
    <issue-list :table="table" :page="page" :total="total" @page-change="pageChange" @search-req="searchReq"></issue-list>
</div>`,
    props: ['condition', 'region'],
    data: function () {
        return {
            loading: false,
            where: {},
            searchTempWhere: {
                onlyThisLevel: false
            },
            illegalTypeArr,
            adClassArr,
            adClassTree,
            page: 1,
            total: 0,
            pageSize: 10,
            table: [],
            userType,
        }
    },
    components: {
        issueList
    },
    created() {
        // search 方法前有一堆判断，如过不需要就执行 this.searchReq()
        // !下面两个方法 2 选 1
        this.search()
        // this.searchReq()
    },
    methods:{
        search: function(){
            this.loading = true
            this.page = 1
            this.where.onlyThisLevel = this.searchTempWhere.onlyThisLevel ? 1 : 0
            if (this.searchTempWhere.searchAdClassArr1 && this.searchTempWhere.searchAdClassArr1.length !== 0) {
                this.where.adClass = this.searchTempWhere.searchAdClassArr1[this.searchTempWhere.searchAdClassArr1.length - 1]
            }else{
                this.where.adClass = ''
            }
            if (this.searchTempWhere.searchAdClassArr2 && this.searchTempWhere.searchAdClassArr2.length !== 0) {
                this.where.adClass2 = this.searchTempWhere.searchAdClassArr2[this.searchTempWhere.searchAdClassArr2.length - 1]
            }else{
                this.where.adClass2 = ''
            }
            if (this.searchTempWhere.region && this.searchTempWhere.region.length !== 0) {
                this.where.regionId = this.searchTempWhere.region[this.searchTempWhere.region.length - 1]
            }else{
                this.where.regionId = ''
            }
            this.searchReq()
        },
        searchReq: function(){
            this.loading = true
            let {where, page, pageSize, condition} = this
            $.post('', {where, page, pageSize, condition}).then((res) => {
                this.loading = false
                this.table = res.table
                this.total = +res.total
            })
        },
        resetForm: function(){
            this.where = {}
            this.page = 1
            this.searchTempWhere = {
                onlyThisLevel: false
            }
            this.search()
        },
        pageChange: function (page) {
            this.page = page
            this.searchReq()
        },
        refreshPage: function () {
            location.reload()
        }
    }
}