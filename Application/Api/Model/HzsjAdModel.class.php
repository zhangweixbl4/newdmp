<?php
namespace Api\Model;
use Think\Exception;


class HzsjAdModel{

	/*归档电视数据*/
	public function tv($mediaInfo,$date){
		
		$issTable = 'ttvissue_'.date('Ym',strtotime($date)).'_'.substr($mediaInfo['fregionid'],0,2);
		try{//
			$issueList0 = M($issTable)
							->alias('iss')
							->field('
									sam.uuid
									,iss.fmediaid
									,sam.fadid
									,sam.fid as fsampleid
									,sam.fspokesman
									,iss.fstarttime
									,iss.fendtime
									,sam.fillegaltypecode
									,sam.fexpressioncodes
									,sam.fillegalcontent
									,sam.favifilename
									
									
									')
							->join('ttvsample as sam on sam.fid = iss.fsampleid and sam.fadid > 0')
							->where(array('iss.fmediaid'=>$mediaInfo['media_id'],'iss.fissuedate'=>strtotime($date)))
							->select();
		}catch( \Exception $error) {
			$issueList0 = [];	
		}
		$issueList = [];
		$illCount = 0;
		foreach($issueList0 as $tt){
			
			$adInfo = $this->getAdInfo($tt['fadid']);
			#var_dump($adInfo);
			$issue = array();
			$issue['identify'] = md5($mediaInfo['media_id'].'_'.$date.'_'.$tt['fstarttime']);
			$issue['media_class'] = '电视';
			$issue['sample_id'] = $tt['fsampleid'];
			$issue['sample_uuid'] = $tt['uuid'];
			$issue['fmediaid'] = $mediaInfo['media_id'];
			$issue['fmedianame'] = $mediaInfo['fmedianame'];
			$issue['media_regionid'] = $mediaInfo['fregionid'];
			$issue['fadname'] = $adInfo['fadname'];
			$issue['fadowner'] = $adInfo['adowner_name'];
			$issue['adowner_regionid'] = $adInfo['adowner_regionid'];
			$issue['fbrand'] = $adInfo['fbrand'];
			$issue['fspokesman'] = $tt['fspokesman'];
			$issue['fadclasscode'] = $adInfo['fadclasscode'];
			$issue['fstarttime'] = $tt['fstarttime'];
			$issue['fendtime'] = $tt['fendtime'];
			$issue['fissuedate'] = strtotime(date('Y-m-d',$tt['fstarttime']));
			$issue['flength'] = $tt['fendtime'] - $tt['fstarttime'];
			$issue['fillegaltypecode'] = $tt['fillegaltypecode'];
			$issue['fexpressioncodes'] = $tt['fexpressioncodes'];
			$issue['fillegalcontent'] = $tt['fillegalcontent'];
			$issue['sam_source_path'] = $tt['favifilename'];
			$issue['fprice'] = 0;
			$issue['fmediaownername'] = $mediaInfo['mediaowner_name'];
			$issue['last_update_time'] = time();
			
			$issueList[] = $issue;
			
			if($tt['fillegaltypecode'] == 30) $illCount += 1;
		}
		$dCount = M('hzsj_ad','',C('FENXI_DB'))->where(array('fmediaid'=>$mediaInfo['media_id'],'fissuedate'=>strtotime($date)))->delete();
		$wCount = M('hzsj_ad','',C('FENXI_DB'))->addAll($issueList,array(),true);
		
		
		return array('count'=>$wCount,'illCount'=>$illCount);
		
	}
	
	
	/*归档广播数据*/
	public function bc($mediaInfo,$date){
		
		$issTable = 'tbcissue_'.date('Ym',strtotime($date)).'_'.substr($mediaInfo['fregionid'],0,2);
		try{
			$issueList0 = M($issTable)
							->alias('iss')
							->field('
									sam.uuid
									,iss.fmediaid
									,sam.fadid
									,sam.fid as fsampleid
									,sam.fspokesman
									,iss.fstarttime
									,iss.fendtime
									,sam.fillegaltypecode
									,sam.fexpressioncodes
									,sam.fillegalcontent
									,sam.favifilename
									
									
									')
							->join('tbcsample as sam on sam.fid = iss.fsampleid and sam.fadid > 0')
							->where(array('iss.fmediaid'=>$mediaInfo['media_id'],'iss.fissuedate'=>strtotime($date)))
							->select();
		}catch( \Exception $error) {
			$issueList0 = [];	
		}
		$issueList = [];
		$illCount = 0;
		foreach($issueList0 as $tt){
			
			$adInfo = $this->getAdInfo($tt['fadid']);
			#var_dump($adInfo);
			$issue = array();
			$issue['identify'] = md5($mediaInfo['media_id'].'_'.$date.'_'.$tt['fstarttime']);
			$issue['media_class'] = '广播';
			$issue['sample_id'] = $tt['fsampleid'];
			$issue['sample_uuid'] = $tt['uuid'];
			$issue['fmediaid'] = $mediaInfo['media_id'];
			$issue['fmedianame'] = $mediaInfo['fmedianame'];
			$issue['media_regionid'] = $mediaInfo['fregionid'];
			$issue['fadname'] = $adInfo['fadname'];
			$issue['fadowner'] = $adInfo['adowner_name'];
			$issue['adowner_regionid'] = $adInfo['adowner_regionid'];
			$issue['fbrand'] = $adInfo['fbrand'];
			$issue['fspokesman'] = $tt['fspokesman'];
			$issue['fadclasscode'] = $adInfo['fadclasscode'];
			$issue['fstarttime'] = $tt['fstarttime'];
			$issue['fendtime'] = $tt['fendtime'];
			$issue['fissuedate'] = strtotime(date('Y-m-d',$tt['fstarttime']));
			$issue['flength'] = $tt['fendtime'] - $tt['fstarttime'];
			$issue['fillegaltypecode'] = $tt['fillegaltypecode'];
			$issue['fexpressioncodes'] = $tt['fexpressioncodes'];
			$issue['fillegalcontent'] = $tt['fillegalcontent'];
			$issue['sam_source_path'] = $tt['favifilename'];
			$issue['fprice'] = 0;
			$issue['fmediaownername'] = $mediaInfo['mediaowner_name'];
			$issue['last_update_time'] = time();
			
			$issueList[] = $issue;
			
			if($tt['fillegaltypecode'] == 30) $illCount += 1;
			
		}
		$dCount = M('hzsj_ad','',C('FENXI_DB'))->where(array('fmediaid'=>$mediaInfo['media_id'],'fissuedate'=>strtotime($date)))->delete();
		$wCount = M('hzsj_ad','',C('FENXI_DB'))->addAll($issueList,array(),true);
		
		
		return array('count'=>$wCount,'illCount'=>$illCount);
		
	}
	
	
	/*归档报纸数据*/
	public function paper($mediaInfo,$date){
		

		$issueList0 = M('tpaperissue')
							->alias('iss')
							->field('
									sam.uuid
									,iss.fmediaid
									,sam.fpapersampleid as fsampleid
									,sam.fadid
									,sam.fspokesman
									,iss.fissuedate
									,sam.fillegaltypecode
									,sam.fexpressioncodes
									,sam.fillegalcontent
									,sam.fjpgfilename
									
									
									')
							->join('tpapersample as sam on sam.fpapersampleid = iss.fpapersampleid and sam.fadid > 0')
							->where(array('iss.fmediaid'=>$mediaInfo['media_id'],'iss.fissuedate'=>$date))
							->select();
		$issueList = [];
		$illCount = 0;
		foreach($issueList0 as $tt){
			
			$adInfo = $this->getAdInfo($tt['fadid']);
			#var_dump($adInfo);
			$issue = array();
			$issue['identify'] = md5($mediaInfo['media_id'].'_'.$date.'_'.$tt['fjpgfilename']);
			$issue['media_class'] = '报纸';
			$issue['sample_id'] = $tt['fsampleid'];
			$issue['sample_uuid'] = $tt['uuid'];
			$issue['fmediaid'] = $mediaInfo['media_id'];
			$issue['fmedianame'] = $mediaInfo['fmedianame'];
			$issue['media_regionid'] = $mediaInfo['fregionid'];
			$issue['fadname'] = $adInfo['fadname'];
			$issue['fadowner'] = $adInfo['adowner_name'];
			$issue['adowner_regionid'] = $adInfo['adowner_regionid'];
			$issue['fbrand'] = $adInfo['fbrand'];
			$issue['fspokesman'] = $tt['fspokesman'];
			$issue['fadclasscode'] = $adInfo['fadclasscode'];
			$issue['fissuedate'] = strtotime(date('Y-m-d',strtotime($tt['fissuedate'])));
			$issue['fstarttime'] = $issue['fissuedate'];
			$issue['fendtime'] = $issue['fissuedate'];
			
			$issue['flength'] = 0;
			$issue['fillegaltypecode'] = $tt['fillegaltypecode'];
			$issue['fexpressioncodes'] = $tt['fexpressioncodes'];
			$issue['fillegalcontent'] = $tt['fillegalcontent'];
			$issue['sam_source_path'] = $tt['fjpgfilename'];
			$issue['fprice'] = 0;
			$issue['fmediaownername'] = $mediaInfo['mediaowner_name'];
			$issue['last_update_time'] = time();
			
			$issueList[] = $issue;
			
			if($tt['fillegaltypecode'] == 30) $illCount += 1;
		}
		$dCount = M('hzsj_ad','',C('FENXI_DB'))->where(array('fmediaid'=>$mediaInfo['media_id'],'fissuedate'=>strtotime($date)))->delete();
		$wCount = M('hzsj_ad','',C('FENXI_DB'))->addAll($issueList,array(),true);
		
		
		return array('count'=>$wCount,'illCount'=>$illCount);
		
	}
	
	/*归档互联网数据*/
	public function net($mediaInfo,$date){
		

		$issueList0 = M('tnetissueputlog')
                ->alias('tissue')
                ->cache(true,60)
                ->field('
						tissue.major_key as id,
						tissue.tid as fsampleid,
						tissue.fmediaid,
						tissue.net_created_date ,
						tsample.fadid,
						tsample.fadclasscode,
						tsample.fillegaltypecode,
						tsample.fillegalcontent,
						tsample.identify_code,
						tsample.fexpressioncodes,
						tsample.thumb_url_true as sam_source_path

                ')
                ->join('tnetissue as tsample on tsample.major_key = tissue.tid and tsample.fadid > 0')
                ->where(array(/* 'is_first_broadcast' => 1, */'tissue.fmediaid'=>$mediaInfo['media_id'],'tissue.fissuedate'=>strtotime($date)))
				->select();
		$issueList = [];
		$illCount = 0;
		foreach($issueList0 as $tt){
			
			$adInfo = $this->getAdInfo($tt['fadid']);
			#var_dump($adInfo);
			$issue = array();
			$issue['identify'] = md5($mediaInfo['media_id'].'_'.$date.'_'.$tt['id']);
			$issue['media_class'] = '互联网';
			$issue['sample_id'] = $tt['fsampleid'];
			
			$issue['sample_uuid'] = $tt['identify_code'];
			$issue['fmediaid'] = $mediaInfo['media_id'];
			$issue['fmedianame'] = $mediaInfo['fmedianame'];
			$issue['media_regionid'] = $mediaInfo['fregionid'];
			$issue['fadname'] = $adInfo['fadname'];
			$issue['fadowner'] = $adInfo['adowner_name'];
			$issue['adowner_regionid'] = $adInfo['adowner_regionid'];
			$issue['fbrand'] = $adInfo['fbrand'];
			$issue['fspokesman'] = $tt['fspokesman'];
			$issue['fadclasscode'] = $adInfo['fadclasscode'];
			
			$issue['fissuedate'] = strtotime(date('Y-m-d',intval($tt['net_created_date']/1000)));
			$issue['fstarttime'] = intval($tt['net_created_date']/1000);
			$issue['fendtime'] = intval($tt['net_created_date']/1000);
			
			$issue['flength'] = 0;
			$issue['fillegaltypecode'] = $tt['fillegaltypecode'];
			$issue['fexpressioncodes'] = $tt['fexpressioncodes'];
			$issue['fillegalcontent'] = $tt['fillegalcontent'];
			$issue['sam_source_path'] = $tt['sam_source_path'];
			$issue['fprice'] = 0;
			$issue['fmediaownername'] = $mediaInfo['mediaowner_name'];
			$issue['last_update_time'] = time();
			
			$issueList[] = $issue;
			
			if($tt['fillegaltypecode'] == 30) $illCount += 1;
		}
		

		$dCount = M('hzsj_ad','',C('FENXI_DB'))->where(array('fmediaid'=>$mediaInfo['media_id'],'fissuedate'=>strtotime($date)))->delete();
		$wCount = M('hzsj_ad','',C('FENXI_DB'))->addAll($issueList,array(),true);
		return array('count'=>$wCount,'illCount'=>$illCount);
		
	}
	
	
	public function ext_13($wait_ext){
		
		$dataInfo = M('tnetissue')
								->cache(true,60)
								->field('
											fadmanuno
											,fmanuno
											,fadapprno
											,fapprno
											,fadent
											,fent
											,fentzone
											,net_original_url
											,net_original_snapshot
											,net_target_url
											,thumb_url_true
											,net_snapshot
											,ftype
											,fversion_id
										')
								->where(array('fmediaid'=>$wait_ext['fmediaid'],'major_key'=>$wait_ext['sample_id']))
								->find();
		
		$other_arr = array();
		if($dataInfo['fadmanuno']) $other_arr['fadmanuno'] = $dataInfo['fadmanuno'];
		if($dataInfo['fmanuno']) $other_arr['fmanuno'] = $dataInfo['fmanuno'];
		if($dataInfo['fadapprno']) $other_arr['fadapprno'] = $dataInfo['fadapprno'];
		if($dataInfo['fapprno']) $other_arr['fapprno'] = $dataInfo['fapprno'];
		if($dataInfo['fadent']) $other_arr['fadent'] = $dataInfo['fadent'];
		if($dataInfo['fent']) $other_arr['fent'] = $dataInfo['fent'];
		if($dataInfo['fentzone']) $other_arr['fentzone'] = $dataInfo['fentzone'];
		if($dataInfo['net_original_url']) $other_arr['net_original_url'] = $dataInfo['net_original_url'];
		if($dataInfo['net_original_snapshot']) $other_arr['net_original_snapshot'] = $dataInfo['net_original_snapshot'];
		if($dataInfo['net_target_url']) $other_arr['net_target_url'] = $dataInfo['net_target_url'];
		if($dataInfo['thumb_url_true']) $other_arr['thumb_url_true'] = $dataInfo['thumb_url_true'];
		if($dataInfo['net_snapshot']) $other_arr['net_snapshot'] = $dataInfo['net_snapshot'];

		
		$extAdd = array();
		$extAdd['identify'] = $wait_ext['identify'];
		$extAdd['ad_type'] = $dataInfo['ftype'];
		$extAdd['sam_version'] = $dataInfo['fversion_id'];
		
		$extAdd['other_json'] = json_encode($other_arr);
		
		$trackers = M('trackers_record')
								->cache(true,60)
								->field('concat("[",tracker.trackerid,"]") as id,concat("[",tracker.entname,"]") as name')
								->join('tracker on tracker.trackerid = trackers_record.trackerid')
								->where(array('trackers_record.tid'=>$wait_ext['sample_id']))
								->select();

		
		$extAdd['tracker_name_list'] = implode(',',array_column($trackers,'name'));
		$extAdd['tracker_id_list'] = implode(',',array_column($trackers,'id'));
		
		return $extAdd;


		
	}
	
	
	public function ext_01($wait_ext){
		
		$dataInfo = M('ttvsample')
								->cache(true,60)
								->field('
											fadmanuno
											,fmanuno
											,fadapprno
											,fapprno
											,fadent
											,fent
											,fentzone
											,favifilename
											,fversion_id
										')
								->where(array('fmediaid'=>$wait_ext['fmediaid'],'fid'=>$wait_ext['sample_id']))
								->find();
		
		$other_arr = array();
		if($dataInfo['fadmanuno']) $other_arr['fadmanuno'] = $dataInfo['fadmanuno'];
		if($dataInfo['fmanuno']) $other_arr['fmanuno'] = $dataInfo['fmanuno'];
		if($dataInfo['fadapprno']) $other_arr['fadapprno'] = $dataInfo['fadapprno'];
		if($dataInfo['fapprno']) $other_arr['fapprno'] = $dataInfo['fapprno'];
		if($dataInfo['fadent']) $other_arr['fadent'] = $dataInfo['fadent'];
		if($dataInfo['fent']) $other_arr['fent'] = $dataInfo['fent'];
		if($dataInfo['fentzone']) $other_arr['fentzone'] = $dataInfo['fentzone'];
		#if($dataInfo['favifilename']) $other_arr['favifilename'] = $dataInfo['favifilename'];
		
		
		$extAdd = array();
		$extAdd['identify'] = $wait_ext['identify'];
		$extAdd['ad_type'] = 'tv';
		$extAdd['sam_version'] = $dataInfo['fversion_id'];
		
		$extAdd['other_json'] = json_encode($other_arr);
		
		return $extAdd;

	}
	
	public function ext_02($wait_ext){
		
		$dataInfo = M('tbcsample')
								->cache(true,60)
								->field('
											fadmanuno
											,fmanuno
											,fadapprno
											,fapprno
											,fadent
											,fent
											,fentzone
											,favifilename
											,fversion_id
										')
								->where(array('fmediaid'=>$wait_ext['fmediaid'],'fid'=>$wait_ext['sample_id']))
								->find();
		
		$other_arr = array();
		if($dataInfo['fadmanuno']) $other_arr['fadmanuno'] = $dataInfo['fadmanuno'];
		if($dataInfo['fmanuno']) $other_arr['fmanuno'] = $dataInfo['fmanuno'];
		if($dataInfo['fadapprno']) $other_arr['fadapprno'] = $dataInfo['fadapprno'];
		if($dataInfo['fapprno']) $other_arr['fapprno'] = $dataInfo['fapprno'];
		if($dataInfo['fadent']) $other_arr['fadent'] = $dataInfo['fadent'];
		if($dataInfo['fent']) $other_arr['fent'] = $dataInfo['fent'];
		if($dataInfo['fentzone']) $other_arr['fentzone'] = $dataInfo['fentzone'];
		#if($dataInfo['favifilename']) $other_arr['favifilename'] = $dataInfo['favifilename'];
		
		
		$extAdd = array();
		$extAdd['identify'] = $wait_ext['identify'];
		$extAdd['ad_type'] = 'bc';
		$extAdd['sam_version'] = $dataInfo['fversion_id'];
		
		$extAdd['other_json'] = json_encode($other_arr);
		
		return $extAdd;

	}
	
	public function ext_03($wait_ext){
		
		$dataInfo = M('tpapersample')
								->cache(true,60)
								->field('
											fadmanuno
											,fmanuno
											,fadapprno
											,fapprno
											,fadent
											,fent
											,fentzone
											,fjpgfilename
											,fversion_id
										')
								->where(array('fmediaid'=>$wait_ext['fmediaid'],'fpapersampleid'=>$wait_ext['sample_id']))
								->find();
		
		$other_arr = array();
		if($dataInfo['fadmanuno']) $other_arr['fadmanuno'] = $dataInfo['fadmanuno'];
		if($dataInfo['fmanuno']) $other_arr['fmanuno'] = $dataInfo['fmanuno'];
		if($dataInfo['fadapprno']) $other_arr['fadapprno'] = $dataInfo['fadapprno'];
		if($dataInfo['fapprno']) $other_arr['fapprno'] = $dataInfo['fapprno'];
		if($dataInfo['fadent']) $other_arr['fadent'] = $dataInfo['fadent'];
		if($dataInfo['fent']) $other_arr['fent'] = $dataInfo['fent'];
		if($dataInfo['fentzone']) $other_arr['fentzone'] = $dataInfo['fentzone'];
		#if($dataInfo['fjpgfilename']) $other_arr['fjpgfilename'] = $dataInfo['fjpgfilename'];
		
		
		$extAdd = array();
		$extAdd['identify'] = $wait_ext['identify'];
		$extAdd['ad_type'] = 'paper';
		$extAdd['sam_version'] = $dataInfo['fversion_id'];
		
		$extAdd['other_json'] = json_encode($other_arr);
		
		return $extAdd;

	}
	
	
	
	
	/*获取广告信息*/
	public function getAdInfo($fadid){

		$adInfo = S('getAdInfo_'.$fadid);
		#var_dump($adInfo);
		if(!$adInfo){
			#var_dump('mysssss');
			$adInfo = M('tad')
						
						->field('
									tad.fadid
									,tad.fadname
									,tad.fadclasscode
									,tad.fadclasscode_v2
									,tadowner.fname as adowner_name
									,tadowner.fregionid as adowner_regionid
									,tad.fbrand
								')
						->join('tadowner on tadowner.fid = tad.fadowner')
						->where(array('fadid'=>$fadid))
						->find();
			if($adInfo) S('getAdInfo_'.$fadid,$adInfo,600);
		}else{
			#var_dump('cac');
		}
		
		return $adInfo;
		
	}
	
	
	

}



