Vue.component('taskInfo2',{
    template: `<div v-loading="loading" class="task-info">
    <el-row>
        <el-col :span="12">
            <el-row>
                <div class="sample-title">
                    <el-button type="text" @click="openCutErrorForm">提交剪辑错误</el-button>
                </div>
            </el-row>
            <el-row>
                <b>播出媒体</b> : {{task.media_name}}
                <span v-if="adType == 'tv'" style="display: inline-block;width: 50px;">
                    <img :src="task.tv_logo" alt="" style="width: 50px;">
                </span>
            </el-row>
            <el-row class="sample-content">
                <div class="text-center" v-if="adType === 'paper'">
                    <el-button @click="confirmPaper" v-if="!task.paperIsSure" type="info">确认版面无误后, 点击查看广告</el-button>
                    <el-button @click="checkPaper" v-else type="info">查看版面</el-button>
                </div>
                <div v-if="task.media_class == 1 || task.media_class == 2">
                    <div v-if="adType=='tv'">
                        <video controls :src="task.source_path"></video>
                    </div>
                    <div v-if="adType=='bc'">
                        <audio :src="task.source_path" controls></audio>
                    </div>
                </div>
                <div v-if="adType=='paper'">
                    <img :src="task.paperUrl" alt="" v-if="!task.paperIsSure">
                    <img :src="task.source_path" alt="" v-else>
                </div>
                <div class="rate" v-if="adType!='paper'">
                    <b>播放速度</b>
                    <el-radio-group v-model="rate" @change="rateChange">
                        <el-radio :label="0.75">0.75</el-radio>
                        <el-radio :label="1">1</el-radio>
                        <el-radio :label="1.25">1.25</el-radio>
                        <el-radio :label="1.5">1.5</el-radio>
                        <el-radio :label="2.0">2.0</el-radio>
                    </el-radio-group>
                </div>
            </el-row>
        </el-col>
        <el-col :span="11" :offset="1">
            <el-tabs v-model="activeTabName"  type="border-card">
                <el-tab-pane label="广告信息录入" name="adInfo">
                    <ad-info :task="task" :type="adInputType" ref="adInfo" @next="submitSuccess(1)"></ad-info>
                </el-tab-pane>
                <el-tab-pane label="违法判定" name="illegalInfo">
                    <illegal-info :task="task" :type="illegalInputType" ref="illegalInfo" @next="submitSuccess(2)"></illegal-info>
                </el-tab-pane>
            </el-tabs>
        </el-col>
    </el-row>
    <el-dialog title="请选择剪辑错误" :visible.sync="showCutErrorForm" :modal="false">
        <submit-cut-error :id="task.illegalAdId" :media-class="task.media_class" ref="submitCutError" @success="submitCutErrorSuccess"></submit-cut-error> 
    </el-dialog>
</div>`,
    props: ['task'],
    components:{
        taskInfo: this,
        submitCutError,
        adInfo,
        illegalInfo,
    },
    name: 'taskInfo',
    data: function () {
        return{
            loading: false,
            viewerInstance:{destroy:() => {}},
            adClassTree,
            illegalTypeArr,
            adClassArr,
            illegalArr,
            type,
            rate: 1,
            activeTabName: 'adInfo',
            remainTime: 1200,
            intervalId: 0,
            sameAdNameTaskList:[],
            showTaskInfo: false,
            checkTaskInfo: {},
            showCutErrorForm: false,
            showFillBtn: false,
        }
    },
    computed:{
        adType: function () {
            const obj = {
                '1': 'tv',
                '2': 'bc',
                '3': 'paper'
            }
            return obj[this.task.media_class] || null
        },
        isIllegal: function () {
            return this.task.fillegaltypecode !== '0'
        },
        receiveTime: function () {
            return this.task.lasttime ? moment.unix(this.task.lasttime).format('YYYY-MM-DD HH:mm:ss') : ''
        },
        adInputType: function () {
            return 'input'
        },
        illegalInputType: function () {
            return 'input'
        }
    },
    methods:{
        dataInit: function () {
            clearInterval(this.intervalId)
            this.remainTime = 1200
            this.showCutErrorForm = false
            this.showFillBtn = false
            this.loading = false
            this.type = type
            this.activeTabName = 'adInfo'
            this.sameAdNameTaskList = []
            this.checkTaskInfo = {}
            if (this.adType === 'paper'){
                Vue.set(this.task, 'paperIsSure', false)
                this.viewerInit()
            }else{
                try {
                    this.viewerInstance.destroy()
                }catch (e) {

                }
                this.rateChange()
            }
            this.task.is_long_ad = this.task.is_long_ad || '0'
            try {
                if (this.task.fadclasscode) {
                    Vue.set(this.task, 'adCateArr', this._processAdClass(this.task.fadclasscode))
                }else{
                    Vue.set(this.task, 'adCateArr', [])
                    Vue.set(this.task, 'fadclasscode', '')
                }
                if (this.task.fadclasscode_v2){
                    Vue.set(this.task, 'adCateArr2', this._processAdClass(this.task.fadclasscode_v2))
                }else{
                    Vue.set(this.task, 'adCateArr2', [])
                    Vue.set(this.task, 'fadclasscode_v2', '')
                }
                if (!this.task.fadclasscode_v2){
                    Vue.set(this.task, 'fadclasscode_v2', 3203)
                }
            }catch (e) {

            }
            if (this.type == 'input' && this.task.task_state == 1){
                this.intervalId = setInterval(this.timer, 1000)
            }
            if (this.task.linkTypeArr){
                if (this.adInputType === 'input'){
                    this.activeTabName = "adInfo"

                }else if(this.illegalInputType === 'input'){
                    this.activeTabName = "illegalInfo"
                }
            }
            this.$nextTick(() => {
                this.$refs.adInfo.inputAdName()
            })
        },
        timer: function () {
            this.remainTime = moment.unix(+this.task.lasttime + 1200).diff(moment(), 'seconds')
            if (this.remainTime <= 0){
                let tempId = setInterval(() => {})
                while (tempId > 0){
                    clearInterval(tempId)
                    tempId--
                }
                $.post('/Api/TaskInput/back_overtime_task').then(() => {
                    this.$alert('抱歉, 任务超时被收回, 点击确定开始下一个任务').then(() => {
                        this.$emit('next')
                    })
                })
            }
        },
        viewerInit: function () {
            this.$nextTick(() => {
                try {
                    this.viewerInstance.destroy()
                    this.viewerInstance = new Viewer(document.querySelector('.sample-content img'), {
                        inline: true,
                        toolbar: false,
                        navbar: false
                    })
                }catch (e) {}
            })
        },
        rateChange: function (value) {
            value = value || 1
            const selector = this.adType === 'tv' ? '.sample-content video' : '.sample-content audio'
            try {
                const dom = document.querySelector(selector)
                dom.playbackRate = value;
            }catch (e) {}
        },
        reformTask: function () {
            this.$confirm('确认重做任务?').then(() => {
                this.type = 'input'
            }).catch(() => {})
        },
        submitCutErrorSuccess: function () {
            this.$emit('next')
        },
        openCutErrorForm: function () {
            this.showCutErrorForm = true
            this.$nextTick(() => {
                this.$refs.submitCutError.dataInit()
            })
        },
        confirmPaper: function () {
            this.task.paperIsSure = true
            this.viewerInit()
        },
        checkPaper: function () {
            this.task.paperIsSure = false
            this.viewerInit()
        },
        submitRecallRequest: function () {
            this.$confirm('是否发起撤回请求?待质检员审核确认后任务将退回到被退回状态', '是否确认?', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'danger'
            }).then(() => {
                $.post('receiveRecallRequest', {taskid: this.task.taskid, taskType: 1}).then((res) => {
                    if (res.code == -2 && res.msg == 'need log in again') {
                        this.$message.error('登录状态过期,请再次登录')
                    }
                    this.$message({
                        message: res.msg,
                        type: res.type,
                    })
                    this.$emit('back')
                })
            }).catch(() => {})
        },
        passTask: function () {
            this.$confirm('通过通过任务?', '提示' , {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
            }).then(() => {
                const data = {
                    taskid: this.task.taskid,
                    taskType: 1,
                }
                this.loading = true
                $.post('passTask', data).then((res) => {
                    this.loading = false
                    if (res.code == -2 && res.msg == 'need log in again') {
                        this.$message.error('登录状态过期,请再次登录')
                    }
                    if (res.code == 0){
                        this.$message({
                            type: 'success',
                            message: '通过成功'
                        })
                    } else {
                        this.$message({
                            type: 'error',
                            message: '通过失败'
                        })
                    }
                    this.$emit('back')
                })
            }).catch(() => {})
        },
        rejectTask: function (type) {
            this.$prompt('确认退回任务?', '提示' , {
                confirmButtonText: '提交',
                cancelButtonText: '取消',
                inputPattern: /\S/,
                inputErrorMessage: '请输入退回说明'
            }).then((value) => {
                let data = {
                    taskid: this.task.taskid,
                    taskType: 1,
                    reason: value.value,
                    mediaClass: this.task.media_class,
                    linkType: type,
                }
                if (type == 1){
                    this.$confirm('请选择错位类型及说明', '提示', {
                        confirmButtonText: '广告信息错误(广告品牌, 广告主, 广告类别, 商业类别)',
                        cancelButtonText: '非广告信息错误(包含广告名称)',
                    }).then(() => {
                        this.loading = true
                        data.adInfoError = 1
                        $.post('submitRejectTaskRequest', data).then((res) => {
                            this.loading = false
                            if (res.code == 0){
                                this.$message({
                                    type: 'success',
                                    message: res.msg
                                })
                            } else {
                                this.$message({
                                    type: 'error',
                                    message: res.msg
                                })
                            }
                            this.$emit('back')
                        })
                    }).catch(() => {
                        this.loading = true
                        data.adInfoError = 0
                        $.post('submitRejectTaskRequest', data).then((res) => {
                            this.loading = false
                            if (res.code == 0){
                                this.$message({
                                    type: 'success',
                                    message: '成功退回'
                                })
                            } else {
                                this.$message({
                                    type: 'error',
                                    message: '退回失败'
                                })
                            }
                            this.$emit('back')
                        })
                    })
                } else{
                    this.loading = true
                    data.adInfoError = 0
                    $.post('submitRejectTaskRequest', data).then((res) => {
                        this.loading = false
                        if (res.code == 0){
                            this.$message({
                                type: 'success',
                                message: '成功退回'
                            })
                        } else {
                            this.$message({
                                type: 'error',
                                message: '退回失败'
                            })
                        }
                        this.$emit('back')
                    })
                }


            }).catch(() => {})
        },
        checkTask: function (taskid) {
            this.loading = true
            $('video').each(function () {
                this.pause()
            })
            $('audio').each(function () {
                this.pause()
            })
            $.post('index', {taskid: taskid, taskType: 1}).then((res) => {
                if (res.code == 0){
                    this.checkTaskInfo = res.data
                }else{
                    this.$message.warning(res.msg)
                }
                this.showTaskInfo = true
                this.loading = false
                this.$nextTick(() => {
                    this.$refs.task.dataInit()
                    this.$refs.task.type = 'quality'
                    if (this.type === 'input'){
                        this.$refs.task.showFillBtn = true
                    }
                })
            })
        },
        closeTaskInfo: function () {
            this.showTaskInfo = false
            this.checkTaskInfo = {}
        },

        fillTaskData: function () {
            this.$confirm('确认将此广告数据添加到正在录入数据?').then(()=>{
                const fieldArr  = ['fspokesman', 'fversion', 'fmanuno', 'fadmanuno', 'fapprno', 'fadapprno', 'fent', 'fadent' , 'fentzone', 'fillegaltypecode', 'fexpressioncodes', 'fillegalcontent', 'is_long_ad', 'checkedIllegalArr', 'fadname', 'fbrand', 'fname', 'fadclasscode', 'fadclasscode_v2',]
                const data = {}
                fieldArr.forEach((v) => {
                    if (this.task.hasOwnProperty(v)){
                        data[v] = this.task[v]
                    }
                })
                this.$emit('fillTaskData', data)
                this.$emit('back')
            })
        },
        receiveFillTaskData: function (data) {
            this.task = Object.assign(this.task, data)
            this.$nextTick(() => {
                Vue.set(this.task, 'adCateArr', data.hasOwnProperty('fadclasscode') ? this._processAdClass(data.fadclasscode) : [])
                Vue.set(this.task, 'adCateArr2', data.hasOwnProperty('fadclasscode_v2') ? this._processAdClass(data.fadclasscode_v2) : [])
            })
        },
        submitSuccess: function (type) {


        },
        _processAdClass: function (data) {
            let adClass = [];
            if (data.length === 6) {
                adClass.push(data.substr(0, 2))
                adClass.push(data.substr(0, 4))
                adClass.push(data)
            }
            if (data.length === 4) {
                adClass.push(data.substr(0, 2))
                adClass.push(data)
            }
            if (data.length === 2) {
                adClass.push(data)
            }
            return adClass
        },
    },
    filters:{
        taskStateFilter: function (value) {
            const arr = [
                '未分配',
                '任务中',
                '已完成',
                '',
                '被退回',
                '等待撤回通过'
            ]
            return arr[+value]
        }
    }
})