<?php
namespace Api\Controller;
use Think\Controller;

class MediaownerController extends Controller {
	
	/*获取媒介机构列表*/
    public function get_mediaowner_list(){
		
		$mediaowner_name = I('mediaowner_name');//获取媒介机构名称
		$regionid = intval(I('regionid'));
		$where = array();
		$where['fstate'] = array('neq',-1);
		$where['fname'] = array('like','%'.$mediaowner_name.'%');
		if($regionid > 0){
			$regionid_rtrim = rtrim($regionid,'00');//地区ID去掉末尾的0
			$regionid_strlen = strlen($regionid_rtrim);//去掉0后还有几位
			$where['left(tmediaowner.fregionid,'.$regionid_strlen.')'] = $regionid_rtrim;//地区搜索条件
		}
		
		$mediaownerList = M('tmediaowner')->field('fid,fname')->where($where)->limit(10)->select();//查询媒介机构列表

		$this->ajaxReturn(array('code'=>0,'value'=>$mediaownerList));
		
	}
	
	
	/*同步发布记录的地区*/
	public function syn_tv_issue_region(){
		set_time_limit(601);

		/*开始同步电视发布数据*/
		$issueList = M('ttvissue')	
									->field('fmediaid')
									->where(array('fregionid'=>0))
									->group('fmediaid')
									->limit(2)->select();
		$e_c = 0;
		foreach($issueList as $issue){
			
			$regionid = M('tmedia')
									->cache(true,600)
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->where(array('tmedia.fid'=>$issue['fmediaid']))
									->getField('tmediaowner.fregionid');
			$regionid = intval($regionid);						
			$rr = M('ttvissue')->where(array('fmediaid'=>$issue['fmediaid']))->save(array('fregionid'=>$regionid));
			if($rr){
				$e_c += intval($rr);
			}
			
			
		}
		echo $e_c;
		/*结束同步电视发布数据*/
		
	}

	
	/*同步发布记录的地区*/
	public function syn_bc_issue_region(){	
		set_time_limit(601);
		/*开始同步广播发布数据*/
		$issueList = M('tbcissue')
									->field('fmediaid')
									->where(array('fregionid'=>0))
									->group('fmediaid')
									->limit(2)->select();
		$e_c = 0;
		foreach($issueList as $issue){
			
			$regionid = M('tmedia')
									->cache(true,600)
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->where(array('tmedia.fid'=>$issue['fmediaid']))
									->getField('tmediaowner.fregionid');
			$regionid = intval($regionid);						
			$rr = M('tbcissue')->where(array('fmediaid'=>$issue['fmediaid']))->save(array('fregionid'=>$regionid));						
			
			if($rr){
				$e_c += intval($rr);
			}
		}
		echo $e_c;
		/*结束同步广播发布数据*/
		
		
		
	}
	
	
	
	
}