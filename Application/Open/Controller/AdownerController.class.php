<?php
namespace Open\Controller;
use Think\Controller;

class AdownerController extends BaseController {
	


	public function add_edit_adowner(){
		$adowner_data = file_get_contents('php://input');//获取post数据
		$adowner_data = json_decode($adowner_data,true);//把json转化成数组
		//var_dump($adowner_data);
		$fid = intval($adowner_data['fid']);//广告主ID
		
		$a_e_data = array();
		
		$a_e_data['fname'] = $adowner_data['fname'];//广告主名称
		if($a_e_data['fname'] == '') $this->ajaxReturn(array('code'=>10015,'msg'=>'添加广告主失败,广告主名称不能为空'));
		$a_e_data['faddress'] = $adowner_data['faddress'];//广告主地址
		$a_e_data['flinkman'] = $adowner_data['flinkman'];//联系人
		$a_e_data['ftel'] = $adowner_data['ftel'];//联系电话
		$a_e_data['fsource'] = $adowner_data['fsource'];//来源


		
		$a_e_data['fregionid'] = $adowner_data['fregionid'];//行政区划ID
		$region_count = M('tregion')->where(array('fid'=>$a_e_data['fregionid']))->count();
		if($region_count == 0) $this->ajaxReturn(array('code'=>10016,'msg'=>'添加广告主失败,行政区划有错误'));
		
		
		$a_e_data['fstate'] = $adowner_data['fstate'];//状态
		$person_info = $GLOBALS['person_info'];
		
		if($fid == 0 ){
			$a_e_data['fcreator'] = $person_info['fname'];//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = $person_info['fname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			$rr = M('tadowner')->add($a_e_data);
			if($rr > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'添加广告主成功','fid'=>$rr));
			if($rr == 0) $this->ajaxReturn(array('code'=>10011,'msg'=>'添加广告主失败,原因未知'));
		}else{
			$a_e_data['fmodifier'] = $person_info['fname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tadowner')->where(array('fid'=>$fid))->save($a_e_data);
			if($rr > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'修改广告主成功'));
			if($rr == 0) $this->ajaxReturn(array('code'=>10012,'msg'=>'修改广告主失败,原因未知'));
		}
		
		
	
	}
	
	/*获取广告主*/
	public function get_adowner(){
		$adowner_data = file_get_contents('php://input');//获取post数据
		$adowner_data = json_decode($adowner_data,true);//把json转化成数组
		$p = intval($adowner_data['p']);//需要返回的页数
		if($p == 0) $p = 1;
		$pp = intval($adowner_data['pp']);//需要返回的条数
		if($pp <= 0) $pp = 10;
		if($pp > 100) $pp = 100;
		$region_id = intval($adowner_data['region_id']);//地区ID
		if($region_id == 100000) $region_id = 0;//如果传值等于100000，相当于查全国
		$where = array();
		if($region_id > 0){
			
			$region_id_rtrim = rtrim($region_id,'0');//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(tadowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		
		}
		if($adowner_data['fid'] != '') $where['tadowner.fid'] = $adowner_data['fid'] ;
		if($adowner_data['fname'] != '') $where['tadowner.fname'] = array('like','%'.$adowner_data['fname'].'%');
		if($adowner_data['faddress'] != '') $where['tadowner.faddress'] = array('like','%'.$adowner_data['faddress'].'%');
		if($adowner_data['flinkman'] != '') $where['tadowner.flinkman'] = array('like','%'.$adowner_data['flinkman'].'%');
		if($adowner_data['ftel'] != '') $where['tadowner.ftel'] = array('like','%'.$adowner_data['ftel'].'%');
		$count = M('tadowner')->where($where)->count();
		
		$adownerList = M('tadowner')->where($where)->page($p,$pp)->select();
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','adownerList'=>$adownerList,'count'=>$count));
		

	}
	
	
	
	
	
}