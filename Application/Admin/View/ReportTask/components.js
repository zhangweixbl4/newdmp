const REPORT_TYPE = {
  '10': '日报',
  '20': '周报',
  '21': '预警报告',
  '30': '月报',
  '40': '互联网报告',
  '50': '专项报告',
  '60': '复核报告',
  '70': '季报',
  '75': '半年报',
  '80': '年报',
  '90': '汇总报告',
}

const FILE_TYPE = {
  '10': 'WORD',
  '30': 'EXCEL',
}

Vue.component('add-report', {
  data() {
    return {
      dialogVisible: true,
      form: {
        pnfiletype: '10',
        pnurl: '',
        pntype: '10'
      },
      reportDate: [moment().format('YYYY-MM-DD'), moment().format('YYYY-MM-DD')],
      customerList: [],
      pntrepid: [],
      orgList: [],
      areaOptions: {
        value: 'fid',
        label: 'fname',
        children: 'children'
      },
      uploadOptions: Object.assign(UPLOAD_OPTION.val),
      upOptions: {
        up_server: UPLOAD_OPTION.up_server
      },
      REPORT_TYPE: REPORT_TYPE,
      FILE_TYPE: FILE_TYPE,
      pickerOptions: {
        disabledDate(time) {
          return time.getTime() > new Date().getTime()
        }
      },
    }
  },
  created() {
    this.getCustomerList()
  },
  methods: {
    submit() {
      if (!this.form.pnurl) {
        this.$message.error('请上传报告')
        return false
      }
      if (!this.form.pnname) {
        this.$message.error('请输入报告名称')
        return false
      }
      if (!this.form.fcustomer) {
        this.$message.error('请选择客户')
        return false
      }
      let data = {
        ...this.form,
        pnstarttime: this.reportDate[0] || '',
        pnendtime: this.reportDate[1] || '',
        pntrepid: this.pntrepid[this.pntrepid.length - 1]
      }
      $.post('/Admin/ReportTask/send_report', data)
        .then(res => {
          if (res.code === 0) {
            this.$message.success(res.msg)
            this.$emit('close')
          } else {
            this.$message.error(res.msg)
          }
        })
    },
    getCustomerList() {
      $.post('/Admin/ReportTask/get_customer')
        .then(res => {
          if (res.code === 0) {
            this.customerList = res.data
          }
        })
    },
    getOrgList(customerid) {
      this.$set(this.form,'pntrepid','')
      $.post('/Admin/ReportTask/customer_get_tre', {cr_num: customerid})
        .then(res => {
          if (res.code === 0) {
            if (res.data.length) {
              this.$set(this, 'orgList', new Tree(res.data, 'fpid', 'fid').init(res.data[0].fpid))
              this.$set(this,'pntrepid',[res.data[0].fid])
            } else {
              this.$message.error('该客户下无机构！')
            }
          }
        })
    },
    setKey(file) {
      this.uploadOptions.key = file.uid
    },
    uploadSuccess(response, file, fileList) {
      this.form.pnurl = [UPLOAD_OPTION.bucket_url, response.key, '?attname=', file.name].join('')
      this.$set(this.form, 'pnname', file.name)
    },
    uploadError() {
      this.$message.error('上传失败')
    }
  },
  template: `
    <el-dialog
      title="添加报告"
      :close-on-click-modal="false"
      :close-on-press-escape="false"
      :visible.sync="dialogVisible"
      :before-close="() => $emit('close')"
      center
      width="700px">
      <el-form ref="form" label-width="120px">
        <el-form-item label="上传报告：">
          <el-upload
            ref="upload"
            :action="upOptions.up_server"
            :data="uploadOptions"
            :before-upload="setKey"
            auto-upload
            :limit="1"
            :on-remove="() => {form.pnurl = '';form.pnname = ''}"
            :on-success="uploadSuccess"
            :on-error="uploadError">
            <el-button  type="primary">点击上传</el-button>
          </el-upload>
        </el-form-item>
        <el-form-item label="报告名称：">
          <el-input v-model="form.pnname"></el-input>
        </el-form-item>
        <el-form-item label="报告类型：">
          <el-select v-model="form.pntype" style="width:100%">
            <el-option v-for="(value,key) in REPORT_TYPE"
              :key="key"
              :label="value"
              :value="key">
            </el-option>
          </el-select>
        </el-form-item>
        <el-form-item label="文件类型：">
          <el-select v-model="form.pnfiletype" style="width:100%">
            <el-option v-for="(value,key) in FILE_TYPE"
              :key="key"
              :label="value"
              :value="key">
            </el-option>
          </el-select>
        </el-form-item>
        <el-form-item label="报告日期：">
          <el-date-picker
            style="width:100%"
            v-model="reportDate"
            type="daterange"
            :picker-options="pickerOptions"
            range-separator="至"
            value-format="yyyy-MM-dd"
            start-placeholder="开始日期"
            end-placeholder="结束日期">
          </el-date-picker>
        </el-form-item>
        <el-form-item label="选择客户：">
          <el-select v-model="form.fcustomer" style="width:100%" @change="getOrgList">
            <el-option v-for="(item,index) in customerList"
              :key="index"
              :label="item.cr_name"
              :value="item.cr_num">
            </el-option>
          </el-select>
        </el-form-item>
        <el-form-item label="选择机构地区：">
          <el-cascader :disabled="!form.fcustomer" :options="orgList" :props="areaOptions" v-model="pntrepid" style="width:100%" change-on-select>
          </el-cascader>
        </el-form-item>
      </el-form>
      <span slot="footer" class="dialog-footer">
        <el-button type="primary" @click="submit">添 加</el-button>
      </span>
    </el-dialog>
  `
})

Vue.component('send-task', {
  data() {
    return {
      search: {
        page: 1,
        pnname: '',
        pntype: '',
        crname: ''
      },
      searchDate: [],
      tableData: [],
      allCount: '0',
      show: false,
      loading: false,
      pickerOptions: {
        disabledDate(time) {
          return time.getTime() > new Date().getTime()
        }
      },
      REPORT_TYPE: REPORT_TYPE
    }
  },
  created() {
    this.getList()
  },
  methods: {
    getList(page) {
      this.loading = true
      this.search.page = page || 1
      let data = {
        ...this.search,
        pnstarttime: this.searchDate ? (this.searchDate[0] || '') : '',
        pnendtime: this.searchDate ? (this.searchDate[1] || '') : '',
      }
      $.post('/Admin/ReportTask/send_reportlist', data)
        .then(res => {
          if (res.code === 0) {
            this.tableData = res.data.list
            this.allCount = res.data.count
          } else {
            this.$message.error(res.msg)
          }
          this.loading = false
        })
    },
    deleteReport(row) {
      this.$confirm('确认删除该报告？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning',
        center: true,
      }).then(() => {
        $.post('/Admin/ReportTask/del_report', {pnid: row.pnid})
          .then(res => {
            if (res.code === 0) {
              this.getList(this.search.page)
              this.$message.success(res.msg)
            } else {
              this.$message.error(res.msg)
            }
          })
      })
    }
  },
  template: `
    <div>
      <el-form :model="search" ref="form" label-width="90px">
        <el-row :gutter="10">
          <el-col :xs="24"
            :sm="12"
            :md="8"
            :lg="8"
            :xl="8">
            <el-form-item label="报告名称：">
              <el-input v-model="search.pnname" clearable></el-input>
            </el-form-item>
          </el-col>
          <el-col :xs="24"
            :sm="12"
            :md="8"
            :lg="8"
            :xl="8">
            <el-form-item label="报告类型：">
              <el-select v-model="search.pntype" style="width:100%">
                <el-option label="全部" value=""></el-option>
                <el-option v-for="(value,key) in REPORT_TYPE"
                  :key="key"
                  :label="value"
                  :value="key">
                </el-option>
              </el-select>
            </el-form-item>
          </el-col>
          <el-col :xs="24"
            :sm="12"
            :md="8"
            :lg="8"
            :xl="8">
            <el-form-item label="客户名：">
              <el-input v-model="search.crname"></el-input>
            </el-form-item>
          </el-col>
          <el-col :xs="24"
            :sm="12"
            :md="8"
            :lg="8"
            :xl="8">
            <el-form-item label="报告日期：">
              <el-date-picker
                style="width:100%"
                v-model="searchDate"
                type="daterange"
                :picker-options="pickerOptions"
                range-separator="至"
                value-format="yyyy-MM-dd"
                start-placeholder="开始日期"
                end-placeholder="结束日期">
              </el-date-picker>
            </el-form-item>
          </el-col>
          <el-col :span="24">
            <el-form-item  label-width="0px">
              <el-button style="float:right;" icon="el-icon-search" type="primary" @click="getList(1)" :loading="loading">搜 索</el-button>
              <el-button style="float:right;margin-right: 10px;" type="primary" @click="show = true" icon="el-icon-plus">添加报告</el-button>
            </el-form-item>
          </el-col>
        </el-row>
      </el-form>
      <el-table :data="tableData" border v-loading="loading">
        <el-table-column
          type="index"
          width="50">
        </el-table-column>
        <el-table-column
          show-overflow-tooltip
          prop="pnname"
          label="报告名称">
        </el-table-column>
        <el-table-column
          width="90px"
          label="报告类型">
          <template slot-scope="scope">
            {{REPORT_TYPE[scope.row.pntype]}}
          </template>
        </el-table-column>
        <el-table-column
          width="100px"
          label="报告日期">
          <template slot-scope="scope">
            {{scope.row.pnendtime ? scope.row.pnendtime.substring(0,10) : ''}}
          </template>
        </el-table-column>
        <el-table-column
          width="100px"
          label="附件">
          <template slot-scope="scope">
            <a target="_blank" :href="scope.row.pnurl">
              <el-button type="text" icon="el-icon-download">下载</el-button>
            </a>
          </template>
        </el-table-column>
        <el-table-column
          show-overflow-tooltip
          width="120px"
          prop="cr_name"
          label="客户名">
        </el-table-column>
        <el-table-column
          show-overflow-tooltip
          width="160px"
          prop="fname"
          label="接收局">
        </el-table-column>
        <el-table-column
          width="80px"
          label="操作">
          <template slot-scope="scope">
            <el-button type="danger" @click="deleteReport(scope.row)">删除</el-button>
          </template>
        </el-table-column>
      </el-table>
      <el-pagination
        background layout="prev, pager, next, total"
        :page-size="20"
        :current-page="Number(search.page)"
        :total="Number(allCount)"
        @current-change="getList">
      </el-pagination>
      <add-report v-if="show" @close="() => {show = false;getList()}"></add-report>
    </div>
  `
})

Vue.component('report-task', {
  data() {
    return {
      files: [],
      search: {
        page: '1',
        keyword: ''
      },
      loading: false,
      tabledata: [],
      details: {},
      show: false,
      allCount: '0',
      cltype: {
        '1': '未处理',
        '2': '处理中',
        '3': '已处理',
      },
      clcolor: {
        '1': 'dnager',
        '2': 'info',
        '3': 'success',
      },
      reportType: {
        '3': '专项报告',
        '5': '季报',
        '6': '年报',
      }
    }
  },
  created() {
    this.getList()
  },
  methods: {
    getList(page) {
      this.loading = true
      let data = Object.assign({}, this.search)
      data.page = page || '1'
      $.post("/Admin/ReportTask/index_list", this.search, (res) => {
        if (res.code === 0) {
          this.tabledata = res.data.list
          this.allCount = res.data.count
        }
        this.loading = false
      })
    },
    showModal(obj) {
      this.details = obj
      this.show = true
    },
  },
  template: `
    <div>
      <el-form>
          <el-row>
            <el-col :span="12">
              <el-form-item label="">
                <el-input v-model="search.keyword" placeholder="请输入关键字" size="small">
                  <template slot="append">
                    <el-button size="small" type="primary" @click="getList(1)" icon="el-icon-search">搜索</el-button>
                  </template>
                </el-input>
              </el-form-item>
            </el-col>
          </el-row>
        </el-form>
        <el-table v-loading="loading" :data="tabledata" style="width: 100%" border>
          <el-table-column type="index" width="50">
          </el-table-column>
          <el-table-column prop="title" label="任务标题" show-overflow-tooltip>
          </el-table-column>
          <el-table-column label="报告类型" width="80">
            <template slot-scope="scope">
              {{reportType[scope.row.type]}}
            </template>
          </el-table-column>
          <el-table-column label="模板" width="80">
            <template slot-scope="scope">
              <a :href="scope.row.fileurl" target="_blank" v-if="scope.row.fileurl">
                <el-button type="text" size="mini">模板下载</el-button>
              </a>
              <span v-else>无</span>
            </template>
          </el-table-column>
          <el-table-column prop="content" label="说明" show-overflow-tooltip>
          </el-table-column>
          <el-table-column label="创建日期" width="90">
            <template slot-scope="scope">
              {{scope.row.ctime.substring(0,10)}}
            </template>
          </el-table-column>
          <el-table-column label="处理状态" width="90">
            <template slot-scope="scope">
              <el-tag size="small" :type="clcolor[scope.row.status]">{{cltype[scope.row.status]}}</el-tag>
            </template>
          </el-table-column>
          <el-table-column label="截止日期" width="90">
            <template slot-scope="scope">
              {{scope.row.dtime.substring(0,10)}}
            </template>
          </el-table-column>
          <el-table-column label="操作" width="80" fixed="right">
            <template slot-scope="scope">
              <el-button type="primary" :disabled="scope.row.status === '3'" size="mini" @click="showModal(scope.row)">处理</el-button>
            </template>
          </el-table-column>
        </el-table>
        <el-pagination
          background layout="prev, pager, next, total"
          :page-size="20"
          :current-page="Number(search.page)"
          :total="Number(allCount)"
          @current-change="getList">
        </el-pagination>
        <upload-dialog v-if="show" :details="details" @submit="()=>{show = false;getList(1)}"></upload-dialog>
    </div>
  `
})

Vue.component('upload-dialog', {
  props: {
    details: {
      type: Object,
      defaults: () => {}
    }
  },
  data() {
    return {
      dialogVisible: true,
      dfileurl: '',
      uploadOptions: Object.assign(UPLOAD_OPTION.val),
      upOptions: {
        up_server: UPLOAD_OPTION.up_server
      },
      reportType: {
        '3': '专项报告',
        '5': '季报',
        '6': '年报',
      }
    }
  },
  methods: {
    save() {
      if (!this.dfileurl) {
        return false
      }
      var data = {
        fid: this.details.fid,
        dfileurl: this.dfileurl,
      }
      $.post("/Admin/ReportTask/report_process", data, (res) => {
        if (res.code === 0) {
          this.show = false
          this.$emit('submit')
          this.$message.success(res.msg)
        } else {
          this.$message.error(res.msg)
        }
      })
    },
    setKey(file) {
      this.uploadOptions.key = file.uid
    },
    uploadSuccess(response, file, fileList) {
      this.dfileurl = [UPLOAD_OPTION.bucket_url, response.key, '?attname=', file.name].join('')
    },
    uploadError() {
      this.$message.error('上传失败')
    }
  },
  template: `
    <el-dialog
      title="反馈"
      :close-on-click-modal="false"
      :close-on-press-escape="false"
      :visible.sync="dialogVisible"
      center
      width="700px">
      <el-form ref="form" label-width="90px">
        <el-form-item label="任务标题：">
          {{details.title}}
        </el-form-item>
        <el-form-item label="报告类型：">
          {{reportType[details.type]}}
        </el-form-item>
        <el-form-item label="说明：">
          {{details.content}}
        </el-form-item>
        <el-form-item label="创建日期：">
          {{details.ctime ? details.ctime.substring(0,10) : ''}}
        </el-form-item>
        <el-form-item label="截止日期：">
          {{details.dtime ? details.dtime.substring(0,10) : ''}}
        </el-form-item>
        <el-form-item label="上传报告：">
          <el-upload
            :action="upOptions.up_server"
            :data="uploadOptions"
            :before-upload="setKey"
            auto-upload
            :on-remove="() => (dfileurl = '')"
            :on-success="uploadSuccess"
            :on-error="uploadError">
            <el-button  type="primary">点击上传</el-button>
          </el-upload>
        </el-form-item>
      </el-form>
      <span slot="footer" class="dialog-footer">
        <el-button type="primary" @click="save">提 交</el-button>
      </span>
    </el-dialog>
  `
})