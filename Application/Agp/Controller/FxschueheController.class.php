<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 线索处置
 */

class FxschueheController extends BaseController
{
	/**
	 * 获取待初核线索列表
	 * by zw
	 */
	public function index() {
		session_write_close();

		$p 	= I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录

		
		$where['_string'] 	= '((fwaitregulatorid='.session('regulatorpersonInfo.fregulatorpid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid='.session('regulatorpersonInfo.fid').'))';//用户处理权限范围

		$fname 			= I('fname');//登记表名
		$fmodifytime 	= I('fmodifytime');//登记时间
		$status = I('status');//登录来源
		if(!empty($fname)){
			$where['fname'] = array('like','%'.$fname.'%');
		}
		if(!empty($fmodifytime)){
			$where['_string'] .= ' and datediff("'.$fmodifytime.'",fregistertime)=0';
		}
		if(!empty($status)){
			if($status==1){
				$where['fstate'] = 20;
			}elseif($status==2){
				$where['fstate'] = array('in',array(22,23));
			}else{
				$where['fstate'] = array('in',array(20,22,23));
			}
		}else{
			$where['fstate'] 	= array('in',array(20,22,23));
		}

		$count = M('tregister')
			->where($where)
			->count();//查询满足条件的总记录数

		
		$data = M('tregister')
			->where($where)
			->order('fid desc')
			->page($p,$pp)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	/**
	 *线索处置操作
	 * by zw
	*/
	public function update_register(){

		$fchecktype = I('fchecktype');//处理方式
		$fid = I('fid');//登记信息ID
		if(empty($fid)||empty($fchecktype)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}

		$where['fid'] 		= $fid;//登记表ID
		$where['fstate'] 	= array('in',array(20,22,23));
		$where['_string'] 	= '((fwaitregulatorid='.session('regulatorpersonInfo.fregulatorpid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid='.session('regulatorpersonInfo.fid').'))';//用户处理权限范围

		$data = M('tregister')->where($where)->find();//获取当前登记信息
		if(!empty($data) && ($fchecktype==10 || $fchecktype==20 || $fchecktype==30 || $fchecktype==40 || $fchecktype==50)){
			$tregisterdb 		= M('tregister');
			$tregisterflowdb 	= M('tregisterflow');

			$twdo = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'ffinishtime is null'])->find();//获取前一流程id
			if(!empty($twdo)){
				//更新上一流程的处理人
				$twdata['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
				$twdata['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
				$twdata['ffinishtime']					= date('Y-m-d H:i:s');//处理时间
				$twdo1 = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'ffinishtime is null'])->save($twdata);
			}
			$res['fwaitregulatorid']		= I('fwaitregulatorid');//下一处理部门默认无
			$res['fwaitpersonid']			= I('fwaitpersonid');//下一处理人默认无
			if(!empty($twdo1)){
				//新增流程记录
				$sh_list['fregisterid'] 				= $fid;
				$sh_list['fupflowid'] 					= $twdo['flowid'];
				$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
				$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
				$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
				$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
				$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
				$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
				$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
				$sh_list['fflowname'] 					= '线索处置';
				$sh_list['fregulatorid'] 				= I('fwaitregulatorid');
				$sh_list['fcreateinfo'] 				= I('fcheckcontents');

				if($fchecktype==10){//线索自办
					$sh_list['freason'] 					= '线索自办';
					$sh_list['fstate'] 						= 21;
					$sh_list['fchildstate'] 				= 10;
				}elseif($fchecktype==20){//线索移交
					$sh_list['freason'] 					= '线索移交';
					$sh_list['fstate'] 						= 21;
					$sh_list['fchildstate'] 				= 20;
				}elseif($fchecktype==30){//线索转送
					$sh_list['freason'] 					= '线索转送';
					$sh_list['fstate'] 						= 21;
					$sh_list['fchildstate'] 				= 30;
				}elseif($fchecktype==40){//线索处置
					$sh_list['freason'] 					= '线索处置';
					$sh_list['fstate'] 						= 21;
					$sh_list['fchildstate'] 				= 40;
				}elseif($fchecktype==50){//线索退回
					$sh_list['freason'] 					= '线索退回';
					$sh_list['fstate'] 						= 21;
					$sh_list['fchildstate'] 				= 50;
				}
				$flowid = M("tregisterflow")->add($sh_list);

				//待写入的初核信息--公共
				$res['fstate'] 				= 21;//将状态改成初核待签批状态
				$res['fmodifier']			= session('regulatorpersonInfo.fname');//修改人
				$res['fmodifytime']			= date('Y-m-d H:i:s');//修改时间
				$res['fusersid'] 			= $data['fusersid'].session('regulatorpersonInfo.fid').',';//加入到处理用户组
				$res['fcheckregulatorid']	= session('regulatorpersonInfo.fregulatorid');//初核机构id
				$res['fcheckregulatorname']	= session('regulatorpersonInfo.regulatorname');//初核机构名称
				$res['fcheckpersonid']		= session('regulatorpersonInfo.fid');//初核人id
				$res['fcheckpersonname']	= session('regulatorpersonInfo.fname');//初核人姓名
				$res['fcheckcontents']		= I('fcheckcontents');//初核处理建议
				$res['fchecktype']			= $fchecktype;//初核处理方式
				$res['fchecktime']			= date('Y-m-d H:i:s');//初核处理时间
				$trdo = M('tregister')->where(['fid'=>$fid])->save($res);

			}

			
			if(!empty($trdo) && !empty($flowid)){
				D('Function')->write_log('线索处置',1,'操作成功','tregister',$fid,M('tregister')->getlastsql());
				$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
			}else{
				D('Function')->write_log('线索处置',0,'操作失败','tregister',$fid,M('tregister')->getlastsql());
				$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
			}
		}else{
			D('Function')->write_log('报告任务',0,'添加失败','tregister',$fid,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
		}
	}

	/**
	 *获取待签批列表
	 * by zw
	*/
	public function get_dqpregisterlist(){
		session_write_close();
		
		$p 	= I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录

		$where['tregister.fstate'] 				= 21;//待签批状态
		$where['_string'] 	= '((fwaitregulatorid='.session('regulatorpersonInfo.fregulatorpid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid='.session('regulatorpersonInfo.fid').'))';//用户处理权限范围

		$fname 			= I('fname');//登记表名
		$fmodifytime 	= I('fmodifytime');//登记时间
		if(!empty($fname)){
			$where['fname'] = array('like','%'.$fname.'%');
		}
		if(!empty($fmodifytime)){
			$where['_string'] .= ' and datediff("'.$fmodifytime.'",fregistertime)=0';
		}

		$count = M('tregister')
			->where($where)
			->count();//查询满足条件的总记录数

		
		$data = M('tregister')
			->where($where)
			->order('fregistertime desc,fid desc')
			->page($p,$pp)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	/**
	 * 签批操作
	 * by zw
	*/
	public function action_qpregister(){
		
		if(I('fid')){
			$fid = I('fid');
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}
		$submit = I('submit')?I('submit'):1;//1签批2退回

		$where['fid'] 							= $fid;//登记表ID
		$where['tregister.fstate'] 				= 21;//初核待签批状态
		$where['_string'] 	= '((fwaitregulatorid='.session('regulatorpersonInfo.fregulatorpid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid='.session('regulatorpersonInfo.fid').'))';//用户处理权限范围
		$data = M('tregister')->where($where)->find();
		if(!empty($data)){
			if($submit==1){//签批
				$twdo = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'ffinishtime is null'])->find();//获取前一流程id
				if(!empty($twdo)){
					//更新上一流程的处理人
					$twdata['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
					$twdata['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
					$twdata['ffinishtime']					= date('Y-m-d H:i:s');//处理时间
					
					$twdo1 = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'ffinishtime is null'])->save($twdata);

					if(!empty($twdo1)){
						$trdata['fwaitregulatorid'] 		= I('fwaitregulatorid');//下一处理部门
						$trdata['fwaitpersonid'] 			= I('fwaitpersonid');//下一处理人
						$trdata['fstate'] 					= 28;//线索承办
						//新增流程记录
						$sh_list['fregisterid'] 				= $fid;
						$sh_list['fupflowid'] 					= $twdo['flowid'];
						$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
						$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
						$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
						$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
						$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
						$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
						$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
						$sh_list['fflowname'] 					= '线索处置';
						$sh_list['fregulatorid'] 				= I('fwaitregulatorid');
						$sh_list['fcreateinfo'] 				= I('result');
						$sh_list['fstate'] 						= 28;
						$sh_list['fchildstate'] 				= $twdo['fchildstate'];
						if($twdo['fchildstate']==10){
							$sh_list['freason'] 				= '处置签批（自办）';
						}elseif($twdo['fchildstate']==20){
							$sh_list['freason'] 				= '处置签批（移交）';
						}elseif($twdo['fchildstate']==30){
							$sh_list['freason'] 				= '处置签批（转送）';
						}elseif($twdo['fchildstate']==40){
							$sh_list['freason'] 				= '处置签批（处置）';
						}elseif($twdo['fchildstate']==50){
							$trdata['fwaitregulatorid'] 		= $data['fregisterregulatorid'];//下一处理部门
							$trdata['fwaitpersonid'] 			= $data['fregisterpersonid'];//下一处理人
							$trdata['fstate'] 					= 13;//处置退回
							$sh_list['fregulatorid'] 			= $data['fregisterregulatorid'];
							$sh_list['freason'] 				= '处置签批（退回）';
						}
						$flowid = M("tregisterflow")->add($sh_list);

					}
				}

				//保存签批信息
				
				$trdata['fusersid'] 				= $data['fusersid'].session('regulatorpersonInfo.fid').',';//加入到处理用户组
				$trdata['fapprovalcontents'] 		= I('result');//签批建议
				$trdata['fapprovalpersonid'] 		= session('regulatorpersonInfo.fid');//签批人ID
				$trdata['fapprovalpersonname'] 		= session('regulatorpersonInfo.fname');//签批人
				$trdata['fapprovaltime'] 			= date('Y-m-d H:i:s');//签批时间
				$trdo = M('tregister')->where('fid='.$fid)->save($trdata);
			}else{
				$twdo = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'ffinishtime is null'])->find();//获取前一流程id
				if(!empty($twdo)){
					//更新上一流程的处理人
					$twdata['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
					$twdata['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
					$twdata['ffinishtime']					= date('Y-m-d H:i:s');//处理时间
					$twdo1 = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'ffinishtime is null'])->save($twdata);

					if(!empty($twdo1)){
						//新增流程记录
						$sh_list['fregisterid'] 				= $fid;
						$sh_list['fupflowid'] 					= $twdo['flowid'];
						$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
						$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
						$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
						$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
						$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
						$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
						$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
						$sh_list['fflowname'] 					= '线索处置';
						$sh_list['freason'] 					= '处置签批退回';
						$sh_list['fregulatorid'] 				= $twdo['fcreateregualtorid'];
						$sh_list['fcreateinfo'] 				= I('result');
						$sh_list['fstate'] 						= 22;
						$sh_list['fchildstate'] 				= $twdo['fchildstate'];
						$flowid = M("tregisterflow")->add($sh_list);
					}
				}
				//保存签批信息
				$trdata['fapprovalcontents'] 	= I('result');//签批建议
				$trdata['fapprovalpersonid'] 	= session('regulatorpersonInfo.fid');//签批人ID
				$trdata['fapprovalpersonname'] 	= session('regulatorpersonInfo.fname');//签批人
				$trdata['fapprovaltime'] 		= date('Y-m-d H:i:s');//签批时间
				$trdata['fstate'] 				= 22;//签批退回
				$trdata['fwaitregulatorid'] 	= $twdo['fcreateregualtorid'];//下一处理部门
				$trdata['fwaitpersonid'] 		= $twdo['fcreateregualtorpersonid'];//下一处理人
				$trdo = M('tregister')->where('fid='.$fid)->save($trdata);
			}
			D('Function')->write_log('处置签批',1,'签批成功','tregister',$fid,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'签批成功'));
		}else{
			D('Function')->write_log('处置签批',0,'签批失败','tregister',$fid,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'签批失败'));
		}
	}

	/**
	 *获取线索承办列表
	 * by zw
	*/
	public function get_cbregisterlist(){
		session_write_close();
		
		$p 	= I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录

		$where['tregister.fstate'] 				= 28;//承办状态
		$where['_string'] 	= '((fwaitregulatorid='.session('regulatorpersonInfo.fregulatorpid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid='.session('regulatorpersonInfo.fid').'))';//用户处理权限范围

		$fname 			= I('fname');//登记表名
		$fmodifytime 	= I('fmodifytime');//登记时间
		if(!empty($fname)){
			$where['fname'] = array('like','%'.$fname.'%');
		}
		if(!empty($fmodifytime)){
			$where['_string'] .= ' and datediff("'.$fmodifytime.'",fregistertime)=0';
		}

		$count = M('tregister')
			->where($where)
			->count();//查询满足条件的总记录数

		
		$data = M('tregister')
			->where($where)
			->order('fregistertime desc,fid desc')
			->page($p,$pp)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	/**
	 * 承办操作
	 * by zw
	*/
	public function action_cbregister(){
		
		if(I('fid')){
			$fid = I('fid');
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}

		$where['fid'] 							= $fid;//登记表ID
		$where['tregister.fstate'] 				= 28;//承办状态
		$where['_string'] 	= '((fwaitregulatorid='.session('regulatorpersonInfo.fregulatorpid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid='.session('regulatorpersonInfo.fid').'))';//用户处理权限范围
		$data = M('tregister')->where($where)->find();
		if(!empty($data)){
			$twdo = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'ffinishtime is null'])->find();//获取前一流程id
			if(!empty($twdo)){
				//更新上一流程的处理人
				$twdata['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
				$twdata['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
				$twdata['ffinishtime']					= date('Y-m-d H:i:s');//处理时间
				$twdo1 = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'ffinishtime is null'])->save($twdata);

				if(!empty($twdo1)){
					$trdata['fwaitregulatorid'] 		= I('fwaitregulatorid');//下一处理部门
					$trdata['fwaitpersonid'] 			= I('fwaitpersonid');//下一处理人
					$trdata['fstate'] 					= 30;//线索承办
					//新增流程记录
					$sh_list['fregisterid'] 				= $fid;
					$sh_list['fupflowid'] 					= $twdo['flowid'];
					$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
					$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
					$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
					$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
					$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
					$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
					$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
					$sh_list['fflowname'] 					= '线索承办';
					$sh_list['fregulatorid'] 				= I('fwaitregulatorid');
					$sh_list['fcreateinfo'] 				= I('fcheckcontents');
					$sh_list['fstate'] 						= 30;
					$sh_list['fchildstate'] 				= $twdo['fchildstate'];
					if($twdo['fchildstate']==10){
						$sh_list['freason'] 				= '线索承办（自办）';
					}elseif($twdo['fchildstate']==20){
						$sh_list['freason'] 				= '线索承办（移交）';
					}elseif($twdo['fchildstate']==30){
						$sh_list['freason'] 				= '线索承办（转送）';
					}elseif($twdo['fchildstate']==40){
						$sh_list['freason'] 				= '线索承办（处置）';
					}
					$flowid = M("tregisterflow")->add($sh_list);

				}

				if(!empty($flowid)){
					M()->execute('update document set dt_flowid='.$flowid.' where dt_tregisterid='.$fid.' and dt_flowid=0');
					M()->execute('update tregisterfile set fillegaladflowid='.$flowid.' where fregisterid='.$fid.' and fillegaladflowid=0');
				}

				//保存签批信息
				$trdata['fwaitregulatorid'] = I('fwaitregulatorid');//下一处理部门
				$trdata['fwaitpersonid'] 	= I('fwaitpersonid');//下一处理人
				$trdata['fusersid'] 		= $data['fusersid'].session('regulatorpersonInfo.fid').',';//加入到处理用户组
				$trdo = M('tregister')->where($where)->save($trdata);

			}

			
			if(!empty($trdo) && !empty($flowid)){
				D('Function')->write_log('处置签批',1,'签批成功','tregister',$fid,M('tregister')->getlastsql());
				$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
			}else{
				D('Function')->write_log('线索承办',0,'操作失败','tregister',$fid,M('tregister')->getlastsql());
				$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
			}
		}else{
			D('Function')->write_log('线索承办',0,'操作失败','tregister',$fid,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
		}
	}

}