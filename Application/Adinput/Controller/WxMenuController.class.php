<?php
namespace Adinput\Controller;
use Think\Controller;
class WxMenuController extends Controller {
    public function index(){
		
		if(A('Admin/Authority','Model')->authority('200231') === 0){
			exit('没有权限');//验证是否有新增权限
		}
		$getMenuUrl = 'https://api.weixin.qq.com/cgi-bin/menu/get?access_token='.wx_token();
		$menuInfo = http($getMenuUrl);
		$menuInfo = json_decode($menuInfo,1);

		$this->assign('menuInfo',$menuInfo);
		$this->display();
	}
	/*创建菜单*/
	public function create_menu(){
		$menu_data = I('button');//获取请求数据
		foreach($menu_data as $k => $menu){//循环一级菜单
			
			if($menu['sub_button'] && $menu['name'] != ''){//判断是否有二级菜单
				$menu_data2['button'][$k]['name'] = $menu['name'];//一级菜单的名称
				foreach($menu['sub_button'] as $subk => $sub_menu){//循环二级菜单
					
					if($sub_menu['name'] != ''){
						$menu_data2['button'][$k]['sub_button'][$subk]['type'] = $sub_menu['type'];//二级菜单的类型
						$menu_data2['button'][$k]['sub_button'][$subk]['name'] = $sub_menu['name'];//二级菜单的名称
						$menu_data2['button'][$k]['sub_button'][$subk]['url'] = $sub_menu['key'];
						$menu_data2['button'][$k]['sub_button'][$subk]['key'] = $sub_menu['key'];
					}
					
				}
				
			}elseif(!$menu['sub_button'] && $menu['name'] != ''){//没有二级菜单的情况
				
				$menu_data2['button'][$k]['type'] = $menu['type'];//一级菜单的类型
				$menu_data2['button'][$k]['name'] = $menu['name'];//一级菜单的名称
				$menu_data2['button'][$k]['url'] = $menu['key'];
				$menu_data2['button'][$k]['key'] = $menu['key'];
				
				
			}	
		}
		
		//var_dump($menu_data2);
		$postData = json_encode($menu_data2,JSON_UNESCAPED_UNICODE);//数组转为json

		$createMenuUrl = 'https://api.weixin.qq.com/cgi-bin/menu/create?access_token='.wx_token();//创建菜单的URL
		$rr = http($createMenuUrl,$postData,'post');//向微信服务器请求
		$rr = json_decode($rr,1);//微信服务器返回结果转为数组
		
		$this->ajaxReturn($rr);
	}
	
	public function delete_menu(){
		
		$del_url = 'https://api.weixin.qq.com/cgi-bin/menu/delete?access_token='.wx_token();
		$rr = http($del_url);
		$rr = json_decode($rr,1);
		
		$this->ajaxReturn($rr);
	}
	
	public function test(){
		$url = 'https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token='.wx_token();
		$data = '{
					"type":"news",
					"offset":0,
					"count":10
				}';
		$rr = http($url,$data,'post');
		$rr = json_decode($rr,1);
		
		var_dump($rr);
		
		
		
		
		
	}
}