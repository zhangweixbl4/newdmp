const adInfo = {
    template: `<div v-loading="loading">
<el-row class="issueContent">
    <div class="issueList item">
        <div class="h4">发布日期
        </div>
        <span v-if="adInfo.zongjuCount != 0">(该广告已经上报至国家局)</span>
        <br>
        <el-checkbox :indeterminate="isIndeterminate" v-model="checkAll" @change="handleCheckAllChange">全选</el-checkbox>
        <el-checkbox-group v-model="selectedIssue" @change="issueChange">
            <el-checkbox  :label="index" border v-for="(item, index) in adInfo.issueList" :key="index" v-if="adInfo.mediaClass !== '3'">{{item.startTime}} - {{item.endTime}}</el-checkbox >
            <el-checkbox  :label="index" border v-for="(item, index) in adInfo.issueList" :key="index" v-else>{{item.issue_date}}</el-checkbox >
        </el-checkbox-group>
    </div>
    <div class="ad-sample item">
        <el-tabs type="border-card" @tab-click="changeTab" v-if="adInfo.mediaClass === '1' || adInfo.mediaClass === '2'" v-model="active1">
            <el-tab-pane label="发布场景" name="first">
                <div v-if="activeIssue !== null">
                    <video id="player" class="video-js" controls>
                        <source
                                src="http://118.31.11.174:18081/icloud/qiniu/playback.m3u8?tk=5ImWefotsFDCRLwnB7fpaeJzop1DYLdt50Wlr-SgHDVV0Uqr4dsKLw=="
                                type="application/x-mpegURL">
                    </video>
                </div>
                <div class="h4" v-else>请选择左边的发布记录</div>
            </el-tab-pane>
            <el-tab-pane label="样本内容" name="second">
                <video :src="adInfo.favifilename" controls v-if="adInfo.mediaClass === '1'"></video>
                <audio :src="adInfo.favifilename" controls v-else style="width: 100%;"></audio>

            </el-tab-pane>
        </el-tabs>
        <div v-if="adInfo.mediaClass === '3'">
            <div class="text-center">
                <el-button type="info" @click="togglePaperUrl">切换版面/广告</el-button>
            </div>
            <img :src="adInfo.fjpgfilename" alt="" v-if="!showPaper">
            <img :src="adInfo.paperUrl" alt="" v-else>
        </div>
    </div>
    <div class="adInfo item">
        <el-tabs type="border-card" v-model="active2">
            <el-tab-pane label="广告信息" name="first">
                <el-row>
                    <el-col :span="24" v-if="userType === 'dmp'">
                        <el-button type="primary" @click="confirmSelected(1)">确认已选中的发布记录</el-button>
                        <el-button type="warning" @click="confirmSelected(0)">退回已选中的发布记录</el-button>
                    </el-col>
                </el-row>
                <el-row>
                    <el-col :span="4"><b>媒体名称</b></el-col>
                    <el-col :span="16"><b>{{adInfo.media_name}}</b><img :src="adInfo.media_icon" alt="" v-if="adInfo.media_icon && userType === 'dmp'"></el-col>
                </el-row>
                <el-row>
                    <el-col :span="4"><b>广告类别</b></el-col>
                    <el-col :span="16">{{adInfo.adClass}}</el-col>
                </el-row>
                <el-row>
                    <el-col :span="4"><b>广告类别</b></el-col>
                    <el-col :span="16">{{adInfo.adClass2}}</el-col>
                </el-row>
                <el-row>
                    <el-col :span="4"><b>广告主</b></el-col>
                    <el-col :span="16">{{adInfo.fadowner}}</el-col>
                </el-row>
                <el-row>
                    <el-col :span="4"><b>广告品牌</b></el-col>
                    <el-col :span="16">{{adInfo.fbrand}}</el-col>
                </el-row>
                <el-row>
                    <el-col :span="4"><b>代言人</b></el-col>
                    <el-col :span="16">{{adInfo.fspokesman}}</el-col>
                </el-row>
                <el-row>
                    <el-col :span="4"><b>版本说明</b></el-col>
                    <el-col :span="16">{{adInfo.fversion}}</el-col>
                </el-row>
                <el-row>
                    <el-col :span="4"><b>违法内容</b></el-col>
                    <el-col :span="16">{{adInfo.fillegalcontent}}</el-col>
                </el-row>
                <el-row>
                    <el-col :span="4"><b>违法表现代码</b></el-col>
                    <el-col :span="16">{{adInfo.fexpressioncodes}}</el-col>
                </el-row>
                <el-row>
                    <el-col :span="4"><b>违法表现</b></el-col>
                    <el-col :span="16">
                        <ol>
                            <li v-for="(item, index) in adInfo.fexpressions" :key="index">{{item}}</li>
                        </ol>
                    </el-col>
                </el-row>
                <el-row>
                    <el-col :span="4"><b>认定依据</b></el-col>
                    <el-col :span="16">
                        <ol>
                            <li v-for="(item, index) in adInfo.fconfirmations" :key="index">{{item}}</li>
                        </ol>
                    </el-col>
                </el-row>
            </el-tab-pane>
            <el-tab-pane :label="adInfo.commentList != 0 ? '有审核意见' : '无审核意见'" name="second">
                <el-card class="box-card">
                    <el-button type="primary" v-if="userType === 'customer'" @click="addComment">添加意见</el-button>
                    <div v-for="(item, index) in adInfo.commentList" :key="index" class="comment-item" :title="item.update_time">
                        <div class="left">{{item.review_comment}}</div>
                        <div class="right">
                            <div v-if="userType === 'dmp'">
                                <el-button type="primary" v-if="item.is_confirm === '0'" @click="confirmComment(item.id)">回复</el-button>
                                <div v-if="item.is_confirm == '1'">已确认</div>
                                <div v-if="item.is_confirm == '2'">
                                    {{item.back_comment}}
                                </div>
                            </div>
                            <div v-else>
                                <div v-if="item.is_confirm == '0'">未确认</div>
                                <div v-if="item.is_confirm == '1'">已确认</div>
                                <div v-if="item.is_confirm == '2'">
                                    {{item.back_comment}}
                                </div>
                            </div>
                        </div>
                    </div>
                </el-card>
            </el-tab-pane>
        </el-tabs>
       
    </div>
</el-row>
    <el-dialog
            title="请输入退回的备注, 可选择按钮进行快捷输入"
            :visible.sync="showRejectForm"
            :modal="false"
            :before-close="closeRejectForm">
        <div>
            <el-button @click="rejectReason += '串台'">
                串台
            </el-button>
            <el-button @click="rejectReason += '剪辑广告不完整'">
                剪辑广告不完整
            </el-button>
            <el-button @click="rejectReason += '违法判定错误'">
                违法判定错误
            </el-button>
            <el-button @click="rejectReason += '广告类别错误'">
                广告类别错误
            </el-button>
            <el-button @click="rejectReason += '广告类别错误'">
                其他
            </el-button>
            
        </div>
        <br>
        <div>
            <el-input v-model="rejectReason"></el-input>
        </div>
        <hr>
        <div>
            <el-button type="primary" @click="rejectAd">提交</el-button>
        </div>
    </el-dialog>
</div> 
    `,
    props: ['ad'],
    data: function () {
        return {
            adInfo: {},
            loading: false,
            activeIssue: null,
            viewerInstance: {
                destroy: function () {}
            },
            active1: 'first',
            active2: 'first',
            hasAddAuth: false,
            player: {},
            selectedIssue: [],
            checkAll: false,
            isIndeterminate: true,
            showPaper: false , // 是否展示版面
            userType,
            showRejectForm: false,
            rejectReason: '',
        }
    },
    methods:{
        processData: function () {
            this.adInfo = this.ad
            this.showPaper = false
            if (this.adInfo.mediaClass === '3'){
                this.$nextTick( () => {
                    try {
                        this.viewerInstance.destroy()
                        this.viewerInstance = new Viewer(document.querySelector('.ad-sample img'), {
                            inline: true,
                            navbar: false
                        })
                    }catch (e) {}
                })
            }
            this.loading = false
            this.selectedIssue = []
            this.isIndeterminate = true
            this.checkAll = false
            this.activeIssue = null
            if (this.player.dispose){
                this.player.dispose()
            }
            this.player = {}
            this.active1 = 'first'
            this.active2 = 'first'
            this.adInfo.fexpressions = this.adInfo.fexpressions.split(' ;; ').filter((v) => v)
            this.adInfo.fconfirmations = this.adInfo.fconfirmations.split(' ;; ').filter((v) => v)
        },
        handleCheckAllChange(val) {
            this.selectedIssue = []
            if (val){
                let i = 0
                while (this.selectedIssue.length < this.adInfo.issueList.length){
                    this.selectedIssue.push(i)
                    i++
                }
            }
            this.isIndeterminate = false;
        },
        issueChange: function (value) {
            if (value.length === 0){
                return false
            }
            this.activeIssue = value.pop()
            value.push(this.activeIssue)
            value = this.adInfo.issueList[this.activeIssue]
            let data = {
                mediaId: value.fmediaid,
                startTime: value.fstarttime,
                endTime: value.fendtime,
            }
            if (this.adInfo['mediaClass'] !== '3'){
                $.post('getM3U8Url', data).then((res) => {
                    this.player = videojs('player')
                    this.player.src(res.data)
                    this.player.play()
                })
            }
            let checkedCount = value.length;
            this.checkAll = checkedCount === this.adInfo.issueList.length;
            this.isIndeterminate = checkedCount > 0 && checkedCount < this.adInfo.issueList.length;
        },
        changeTab: function () {
            try {
                this.$nextTick(() => {
                    $('video').each(function () {
                        this.pause()
                    })
                    $('audio').each(function () {
                        this.pause()
                    })
                    if (this.player.pause) {
                        this.player.pause()
                    }
                })
            }catch (e) {
                
            }
        },
        confirmSelected: function (res) {
            if (this.selectedIssue.length === 0){
                return this.$alert('请至少选择一项')
            }
            let text = ''
            if (res == 1){
                text = '是否确认已选中的' + this.selectedIssue.length + '项 ? '
                this.$confirm(text).then(() => {
                    this.loading = true
                    let data = {}
                    if (this.adInfo.mediaClass === '3'){
                        data = {
                            issueList: this.selectedIssue.map((v) => {
                                let item = this.adInfo.issueList[v]
                                return {
                                    media_id: item.fmediaid,
                                    paper_jpg: item.paper_jpg,
                                    issue_date: item.issue_date,
                                    type: 1,
                                }
                            }),
                        }
                    } else {
                        data = {
                            issueList: this.selectedIssue.map((v) => {
                                let item = this.adInfo.issueList[v]
                                return {
                                    media_id: item.fmediaid,
                                    start_time: item.fstarttime,
                                    issue_date: item.issue_date,
                                    type: 1
                                }
                            }),
                        }
                    }
                    data.adInfo = this.adInfo
                    $.post('confirmIssue', data).then((res) => {
                        if (res.code == 0){
                            this.$message.success(res.msg)
                        }else{
                            this.$message.warning(res.msg)
                        }
                        this.$emit('success')
                        this.loading = false
                    })
                }).catch(() => {})
            }else{
                this.showRejectForm = true
                this.rejectReason = ''
            }
        },
        rejectAd: function () {
            if (!this.rejectReason){
                this.$alert('请输入理由')
                return
            }
            let text = '是否退回已选中的' + this.selectedIssue.length + '项 ? '
            this.$confirm(text).then(() => {
                this.loading = true
                let data = {}
                if (this.adInfo.mediaClass === '3'){
                    data = {
                        issueList: this.selectedIssue.map((v) => {
                            let item = this.adInfo.issueList[v]
                            return {
                                media_id: item.fmediaid,
                                paper_jpg: item.paper_jpg,
                                issue_date: item.issue_date,
                                type: 0,
                                reason: this.rejectReason,
                            }
                        }),
                    }
                } else {
                    data = {
                        issueList: this.selectedIssue.map((v) => {
                            let item = this.adInfo.issueList[v]
                            return {
                                media_id: item.fmediaid,
                                start_time: item.fstarttime,
                                issue_date: item.issue_date,
                                type: 0,
                                reason: this.rejectReason,
                            }
                        }),
                    }
                }
                data.adInfo = this.adInfo
                $.post('confirmIssue', data).then((res) => {
                    if (res.code == 0){
                        this.$message.success(res.msg)
                    }else{
                        this.$message.warning(res.msg)
                    }
                    this.$emit('success')
                    this.loading = false
                    this.closeRejectForm()
                })
            }).catch(() => {})
        },
        closeRejectForm: function () {
            this.showRejectForm = false
            this.rejectReason = ''
        },
        togglePaperUrl: function () {
            this.showPaper = !this.showPaper
            this.$nextTick( () => {
                try {
                    this.viewerInstance.destroy()
                    this.viewerInstance = new Viewer(document.querySelector('.ad-sample img'), {
                        inline: true,
                        toolbar: false,
                        navbar: false
                    })
                }catch (e) {}
            })
        },
        addComment: function () {
            this.$prompt('请输入审核意见', '提示', {
                confirmButtonText: '提交',
                cancelButtonText: '取消',
                inputPattern: /\S/,
                inputErrorMessage: '请输入审核意见'
            }).then((res) => {
                this.loading = true
                $.post('addComment', {
                    sampleId: this.adInfo.fid,
                    condition: this.$root.baseCondition,
                    comment: res.value
                }).then((res) => {
                    this.loading = false
                    if (res.code == 0) {
                        this.$message.success(res.msg)
                        this.adInfo.commentList = res.data
                    } else {
                        this.$message.warning(res.msg)
                    }
                })
            }).catch(() => {})
        },
        confirmComment: function (id) {
            const data = {
                commentId: id,
                sampleId: this.adInfo.fid,
                condition: this.$root.baseCondition,
            }
            this.$confirm('是否确认?').then(() => {
                data.type = 1
                this.loading = true
                $.post('confirmComment', data).then((res) => {
                    this.loading = false
                    if (res.code == 0) {
                        this.$message.success(res.msg)
                        this.adInfo.commentList = res.data
                    } else {
                        this.$message.warning(res.msg)
                    }
                })
            }).catch(() => {
                this.$prompt('请输入拒绝的原因').then((value) => {
                    data.type = 2
                    data.reason = value.value
                    this.loading = true
                    $.post('confirmComment', data).then((res) => {
                        this.loading = false
                        if (res.code == 0) {
                            this.$message.success(res.msg)
                            this.adInfo.commentList = res.data
                        } else {
                            this.$message.warning(res.msg)
                        }
                    })
                }).catch(() => {
                })
            })

        }
    }
}