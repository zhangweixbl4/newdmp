<?php
namespace Common\Model;


import('Vendor.PHPMailer.PHPMailer');

class SendMailModel{

	/**
	 *邮件发送函数
	 *@param $address 邮件地址

	 */
	public function sent_to_adclue_media($addressArr,$title,$content,$doc_paths,$sender) {

	    $mail = new \PHPMailer();
	    if (C('MAIL_SMTP')) {
	        $mail->IsSMTP();//启用SMTP
	    }
	    $mail->Host = C('MAIL_HOST');//SMTP服务器
	    $mail->SMTPAuth = C('MAIL_SMTPAUTH');//是否启用SMTP认证 

	    $mail->Username = C('MAIL_USERNAME');//发送邮箱
	    $mail->Password = C('MAIL_PASSWORD');//密码
		$mail->Subject = $title; //邮件标题 
	    $mail->CharSet = C('MAIL_CHARSET');//字符集
	    $mail->From = C('MAIL_USERNAME');//发件人地址
	    $mail->FromName = $sender;//发件人姓名
		foreach($addressArr as $address){
			$mail->AddAddress($address, "");//添加收件人（地址，昵称） 
		}
		foreach($doc_paths as $doc_path){
			$mail->AddAttachment($doc_path['path'],$doc_path['name']); // 添加附件,并指定名称 
		}
	    $mail->IsHTML(C('MAIL_ISHTML')); //支持html格式内容
		
		$mail->Body = $content;
	    $rr = $mail->Send();//发送

		return $rr;
	}
    

}