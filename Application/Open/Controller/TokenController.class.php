<?php
namespace Open\Controller;
use Think\Controller;

/**
* 开放接口Token颁发及验证
*/
class TokenController extends Controller {
	/**
	* 构造函数 
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function _initialize() {
		
	}
	
	/**
	* 添加接口用户
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function AddUser(){
		$param = json_decode(file_get_contents('php://input'),true);
		$userid = md5($param['userid']);
		$pwd = md5('pwd:'.$param['userid']);
		$key = $userid;
		$data['fuserid'] = $userid;
		$data['fpwd'] = $pwd;
		$data['fkey'] = $key;
		$ret = M('open_token')->add($data,array(),true);
		if ($ret){
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功','data'=>$data));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'添加失败','data'=>$ret));
		}
	}
	
	/**
	* 签名
	* @Param	string $userid 用户标识
	* @Param	string $timestamp 时间戳
	* @Return	string $sign 返回签名字符串
	*/
	public function Sign($param = array()){
		$userid = $param['userid'];
		$timestamp = $param['timestamp'];
		$pwd = M('open_token')
			->where(array('fuserid'=>$userid))
			->getField('fpwd');
		$sign = md5($userid.$pwd.$timestamp);
		return  $sign;
	}
	
	/**
	* 请求签名校验
	* @Param	Array $req 请求参数数组
	* @Return	Boolean 返回是否校验通过
	*/
	public function AuthRequestSign($req = array()){
		if (empty($req)){
			$req = json_decode(file_get_contents('php://input'),true);
		}
		$userid = $req['userid'];
		$timestamp = $req['timestamp'];
		if ($userid == '' || $timestamp == ''){
			$returndata = array('code'=>1,'msg'=>'缺少必要参数');
			$this->ajaxReturn($returndata);
		}
		if(abs(time()-$timestamp) > 300){
			$returndata = array('code'=>1,'msg'=>'非法请求');
			$this->ajaxReturn($returndata);
		}
		$pwd = M('open_token')
			->where(array('fuserid'=>$userid))
			->getField('fpwd');
		$sign = md5($userid.$pwd.$timestamp);
		
		if($req['sign'] == $sign){
			$returndata = array('code'=>0,'msg'=>'请求成功');
			return true;
		}else{
			$returndata = array('code'=>1,'msg'=>'签名校验未通过');
			$this->ajaxReturn($returndata);
		}
	}

	/**
	* 记录请求日志
	* by zw
	*/
	public function write_log($data){
		file_put_contents('LOG/open_api_'.date('Ymd').'.log', "\n".json_encode($data), FILE_APPEND | LOCK_EX);
	}
	
	/**
	* 生成Key
	* @Param	string $param 任意字符串
	* @Return	void 返回的数据
	*/
	public function CreateKey(){
		$param = I('param','');
		$data['fkey'] = md5($param.time());
		$res = M('open_token')->add($data);
		if ($res){
			$this->ajaxReturn(array('code'=>0,'msg'=>'生成Key成功','data'=>$data['fkey']));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'生成Key失败','data'=>$res));
		}
	}
	
	/**
	* 获取Token
	* @Param	string $key 
	* @Return	string $data
	*/
	public function GetToken(){
		$input = json_decode(file_get_contents('php://input'),true);
		$this->AuthRequestSign($input);
		$userid = $input['userid'];
		$timestamp = $input['timestamp'];
		if ($userid != '' && $timestamp != ''){
			$where['fuserid'] = $userid;
			$tokenRow = M('open_token')
				->where($where)
				->find();
			$mtime = $tokenRow['fmodifytime'];
			$count = $tokenRow['fcount'];							//调用次数，默认2000次/日
			if ($count > C('CountGetToken')){
				$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败，Token接口调用次数已达上限'.C('CountGetToken').'次/日'));
			}elseif($mtime != '' && date('Ymd') == date('Ymd',strtotime($mtime))){
				$count++;											//当天次数+1
			}else{
				$count = 1;											//重新计数
			}
			if ($tokenRow['ftoken'] == ''){
				//token未生成，获取token
				$expire = strtotime("+1 day");						//过期时间24小时
				$str = md5(uniqid(md5(microtime(true)),true)); 		//生成一个不会重复的字符串
				$token = sha1($str); 								//加密
				$savaRet = M('open_token')
					->where($where)
					->save(array('ftoken'=>$token,'fexpire'=>$expire,'fcount'=>$count,'fcreatetime'=>date('Y-m-d H:i:s')));
				if ($savaRet){
					$data = array(
						'token'=>$token,
						'expire'=>$expire
					);
					$this->ajaxReturn(array('code'=>0,'msg'=>'获取Token成功','data'=>$data));
				}
			}else{
				//token已生成，验证或更新有效期
				$isExpire = time() > $tokenRow['fexpire'];			//是否过期
				if ($isExpire){
					$token_back = $tokenRow['ftoken'];				//原token
					$expire_back = $tokenRow['fexpire_back'];		//原expire_back
					$msg = '获取Token成功';
				}else{
					$token_back = $tokenRow['ftoken'];				//原token即将失效
					$expire_back = strtotime("+5 minutes");			//原token有效期5分钟
					$msg = '获取Token成功,原Token将在5分钟后失效';
				}
				$expire = strtotime("+1 day");						//过期时间24小时
				$str = md5(uniqid(md5(microtime(true)),true)); 		//生成一个不会重复的字符串
				$token = sha1($str); 								//加密
				$savaRet = M('open_token')
					->where($where)
					->save(array('ftoken'=>$token,'fexpire'=>$expire,'fcount'=>$count,'ftoken_back'=>$token_back,'fexpire_back'=>$expire_back,'fmodifytime'=>date('Y-m-d H:i:s')));
				if ($savaRet){
					$data = array(
						'token'=>$token,
						'expire'=>$expire
					);
					$this->ajaxReturn(array('code'=>0,'msg'=>$msg,'data'=>$data));
				}
			}
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
		}
	}
	
	/**
	* 刷新Token有效期
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function UpdateToken(){
		
	}
	
	/**
	* 接口Token验证
	* @Param	Mixed variable
	* @Return	bool 验证结果，通过code=0,不通过false或code=1
	*/
	public function AuthToken($token = ''){
		if ($token != ''){
			$where['ftoken|ftoken_back'] = $token;
			$row = M('open_token')
				->where($where)
				->find();
			if ($row){
				$now = time();
				if ($row['fexpire_back'] != '' && $now < $row['fexpire_back']){
					//新旧token均可用
					return array('code'=>0,'msg'=>'新旧Token均有效');
				}elseif($token == $row['ftoken_back'] && $now >= $row['fexpire_back']){
					//旧token失效
					return array('code'=>1,'msg'=>'该Token已过期,请重新获取并使用新Token');
				}elseif($now < $row['fexpire']){
					//token可用
					return array('code'=>0,'msg'=>'旧Token不存在或已失效,Token有效');
				}else{
					//token已过期
					return array('code'=>1,'msg'=>'Token已过期');
				}
			}else{
				return array('code'=>1,'msg'=>'Token不存在');
			}
		}else{
			return array('code'=>1,'msg'=>'缺少token参数');
		}
	}
}