<?php
namespace Api\Controller;
use Think\Controller;

class MediaclassController extends Controller {
	
	/*获取媒介分类列表*/
    public function get_mediaclass_list(){
		
		$fid = I('fid','');//获取fid
		if($fid == 0) $fid = '';

		$mediaclassList = M('tmediaclass')->field('fid,fclass')->where(array('fpid'=>$fid))->select();//查询媒介分类列表

		$mediaclassDetails = M('tmediaclass')->field('fid,left(fclass,4) as fclass,ffullname')->where(array('fid'=>$fid))->find();//查询媒介分类详情
		
		if(!$mediaclassDetails) $mediaclassDetails = array('fid'=>'','fclass'=>'全部','ffullname'=>'全部');
		$this->ajaxReturn(array('code'=>0,'mediaclassList'=>$mediaclassList,'mediaclassDetails'=>$mediaclassDetails));
		
	}
	
	/*修正媒体分类的全称*/
	public function cc(){
		exit;
		$mList = M('tmediaclass')->select();
		
		foreach($mList as $m){
			
			$ffullname = $this->ccc($m['fid'],$m['fclass']);
			M('tmediaclass')->where(array('fid'=>$m['fid']))->save(array('ffullname'=>$ffullname));
			var_dump($ffullname);
		}
		
	}
	
	
	private function ccc($fid = '',$ffullname = ''){
		
		$v = M('tmediaclass')->where(array('fid'=>$fid))->find();
		if ($v['fpid'] == '') return $ffullname;
		if ($v['fpid'] != '') return $this->ccc($v['fpid'],M('tmediaclass')->where(array('fid'=>$v['fpid']))->getField('fclass').'>'.$ffullname);
 			
	}
	
	
}