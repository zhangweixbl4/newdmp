<?php
namespace Adinput\Controller;
use Think\Controller;
class AuditTaskController extends Controller {
	
    public function index(){
		
	}
	
	
	/*任务分配*/
	public function task_assign(){
		$user_info = get_user_info();//获取用户信息
		$taskInfo = M('ad_audit_task')->where(array('task_state'=>1,'user_id'=>$user_info['wx_id']))->find();//获取已接任务信息

		if($taskInfo){//如果有任务直接返回已接任务
			if($taskInfo['mediaclass'] == '电视') $url = U('Adinput/AuditTask/tv_task');//电视任务跳转的链接
			if($taskInfo['mediaclass'] == '广播') $url = U('Adinput/AuditTask/bc_task');//广播任务的跳转链接
			if($taskInfo['mediaclass'] == '报纸') $url = U('Adinput/AuditTask/paper_task');//报纸任务的跳转链接
			$this->ajaxReturn(array('code'=>0,'msg'=>'','url'=>$url));
		}
		if($user_info['audit_seniority'] != 1){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有广告审查资质'));
		}
		$tvTaskInfo = M('ttvsample')->where(array('finspectstate'=>1))->find();//电视任务信息
		$bcTaskInfo = M('tbcsample')->where(array('finspectstate'=>1))->find();//广播任务信息
		
		$paperTaskInfo = M('tpapersample')->where(array('finspectstate'=>1))->find();//报纸任务信息
		
		if($tvTaskInfo){//判断是否有电视任务
			$task_id = A('Adinput/AuditTask','Model')->add_tv_task($tvTaskInfo);
			if($task_id > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'','url'=>U('Adinput/AuditTask/tv_task')));
		}elseif($bcTaskInfo){//判断是否有广播任务
			$task_id = A('Adinput/AuditTask','Model')->add_bc_task($bcTaskInfo);
			if($task_id > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'','url'=>U('Adinput/AuditTask/bc_task')));
		}elseif($paperTaskInfo){//判断是否有报纸任务
			$task_id = A('Adinput/AuditTask','Model')->add_paper_task($paperTaskInfo);
			if($task_id > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'','url'=>U('Adinput/AuditTask/paper_task')));
		}else{//都没有
			$this->ajaxReturn(array('code'=>-1,'msg'=>'当前已无任务'));
		}

	}
	
	/*电视广告任务*/
	public function tv_task(){
		
		$user_info = get_user_info();//获取用户信息
		$taskInfo = M('ad_audit_task')
									->field('ad_audit_task.*,ttvsample.favifilepng,ttvsample.favifilename,tad.fadname,ttvsample.fversion,tadclass.ffullname as adclass_fullname,tadowner.fname as adowner_name')
									->join('ttvsample on ttvsample.fid = ad_audit_task.sample_id')
									->join('tad on tad.fadid = ttvsample.fadid')
									->join('tadowner on tadowner.fid = tad.fadowner')
									->join('tadclass on tadclass.fcode = tad.fadclasscode')
									->where(array('task_state'=>1,'user_id'=>$user_info['wx_id']))->find();
		//var_dump($taskInfo);
		$this->assign('taskInfo',$taskInfo);
		$this->display();
	}
	/*广播广告任务*/
	public function bc_task(){
		
		$user_info = get_user_info();//获取用户信息
		$taskInfo = M('ad_audit_task')
									->field('ad_audit_task.*,tbcsample.favifilename,tad.fadname,tbcsample.fversion,tadclass.ffullname as adclass_fullname,tadowner.fname as adowner_name')
									->join('tbcsample on tbcsample.fid = ad_audit_task.sample_id')
									->join('tad on tad.fadid = tbcsample.fadid')
									->join('tadowner on tadowner.fid = tad.fadowner')
									->join('tadclass on tadclass.fcode = tad.fadclasscode')
									->where(array('task_state'=>1,'user_id'=>$user_info['wx_id']))->find();

		$this->assign('taskInfo',$taskInfo);
		$this->display();
	}
	
	/*广播广告任务*/
	public function paper_task(){
		
		$user_info = get_user_info();//获取用户信息
		$taskInfo = M('ad_audit_task')
									->field('ad_audit_task.*,tpapersample.fjpgfilename,tad.fadname,tpapersample.fversion,tadclass.ffullname as adclass_fullname,tadowner.fname as adowner_name')
									->join('tpapersample on tpapersample.fpapersampleid = ad_audit_task.sample_id')
									->join('tad on tad.fadid = tpapersample.fadid')
									->join('tadowner on tadowner.fid = tad.fadowner')
									->join('tadclass on tadclass.fcode = tad.fadclasscode')
									->where(array('task_state'=>1,'user_id'=>$user_info['wx_id']))->find();

		$this->assign('taskInfo',$taskInfo);
		$this->display();
	}
	
	
	
	
	/*电视广告任务提交*/
	public function ajax_tv_task(){
		$user_info = get_user_info();//获取用户信息
		$task_id = I('task_id');
		$taskInfo = M('ad_audit_task')->where(array('task_id'=>$task_id))->find();//任务信息
		$sample_id = $taskInfo['sample_id'];
		$fexpressioncodes = I('fexpressioncodes');
		$fillegalcontent = I('fillegalcontent');
		$not_illegal = I('not_illegal');//没有违法
		if($not_illegal == 'true'){
			M('ttvsample')->where(array('fid'=>$sample_id))->save(array('fillegaltypecode'=>0));//不违法，违法类型为0
		}else{
			$illegal = A('Open/Tvsample','Model')->tvsampleillegal($sample_id,$fexpressioncodes,$user_info['nickname']);//添加电视广告违法表现对应表并获取冗余字段
			$illegal['fillegalcontent'] = $fillegalcontent;//涉嫌违法内容
			M('ttvsample')->where(array('fid'=>$sample_id))->save($illegal);//修改违法表现冗余字段	
		}
		
		
		M('ad_audit_task')->where(array('task_id'=>$task_id))->save(array('task_state'=>2,'complete_time'=> date('Y-m-d H:i:s')));//修改任务状态
		
		A('Adinput/User','Model')->money($user_info['wx_id'],$taskInfo['money'],'审核广告,任务ID:'.$task_id);//增加余额

		$this->ajaxReturn(array('code'=>0,'msg'=>'审核任务成功'));
		
	}
	
	/*广播广告任务提交*/
	public function ajax_bc_task(){
		$user_info = get_user_info();//获取用户信息
		$task_id = I('task_id');
		$taskInfo = M('ad_audit_task')->where(array('task_id'=>$task_id))->find();//任务信息
		$sample_id = $taskInfo['sample_id'];
		$fexpressioncodes = I('fexpressioncodes');
		$fillegalcontent = I('fillegalcontent');
		$not_illegal = I('not_illegal');//没有违法
		if($not_illegal == 'true'){
			M('tbcsample')->where(array('fid'=>$sample_id))->save(array('fillegaltypecode'=>0));//不违法，违法类型为0
		}else{
			$illegal = A('Open/Bcsample','Model')->bcsampleillegal($sample_id,$fexpressioncodes,$user_info['nickname']);//添加广告违法表现对应表并获取冗余字段
			$illegal['fillegalcontent'] = $fillegalcontent;//涉嫌违法内容
			M('tbcsample')->where(array('fid'=>$sample_id))->save($illegal);//修改违法表现冗余字段	
		}
		M('ad_audit_task')->where(array('task_id'=>$task_id))->save(array('task_state'=>2,'complete_time'=> date('Y-m-d H:i:s')));//修改任务状态
		A('Adinput/User','Model')->money($user_info['wx_id'],$taskInfo['money'],'审核广告,任务ID:'.$task_id);//增加余额
		$this->ajaxReturn(array('code'=>0,'msg'=>'审核任务成功'));
		
	}
	
	/*报纸广告任务提交*/
	public function ajax_paper_task(){
		$user_info = get_user_info();//获取用户信息
		$task_id = I('task_id');
		$taskInfo = M('ad_audit_task')->where(array('task_id'=>$task_id))->find();//任务信息
		$sample_id = $taskInfo['sample_id'];
		$fexpressioncodes = I('fexpressioncodes');
		$fillegalcontent = I('fillegalcontent');
		$not_illegal = I('not_illegal');//没有违法
		if($not_illegal == 'true'){
			M('tpapersample')->where(array('fpapersampleid'=>$sample_id))->save(array('fillegaltypecode'=>0));//不违法，违法类型为0
		}else{
			$illegal = A('Open/Papersample','Model')->papersampleillegal($sample_id,$fexpressioncodes,$user_info['nickname']);//添加广告违法表现对应表并获取冗余字段
			$illegal['fillegalcontent'] = $fillegalcontent;//涉嫌违法内容
			M('tpapersample')->where(array('fpapersampleid'=>$sample_id))->save($illegal);//修改违法表现冗余字段	
		}
		M('ad_audit_task')->where(array('task_id'=>$task_id))->save(array('task_state'=>2,'complete_time'=> date('Y-m-d H:i:s')));//修改任务状态
		A('Adinput/User','Model')->money($user_info['wx_id'],$taskInfo['money'],'审核广告,任务ID:'.$task_id);//增加余额
		$this->ajaxReturn(array('code'=>0,'msg'=>'审核任务成功'));
		
	}
	
	
	
	
	
	

	
	
	
	

}