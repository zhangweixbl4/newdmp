<?php
namespace TaskInput\Model;
use Think\Exception;


class DataModel{

	/*添加任务数据*/
	public function add_data($taskData){

		try{
			//$taskid = M('task_input')->add($taskData);
		}catch(Exception $error) {
			$taskid = 0;
		}

		/* if($taskid > 0){
			$this->add_flow($taskid,'创建任务');
		} */
		return $taskid;
	}
	
	/*添加任务流程*/
	public function add_flow($taskid,$flow_content,$wx_id = 0){
		$flowid = M('task_input_flow')->add(array('wx_id'=>$wx_id,'taskid'=>$taskid,'flow_content'=>$flow_content,'flow_time'=>time()));
		return $flowid;
    }
    
	/*互联网广告-添加任务流程*/
	public function add_netad_flow($taskid, $log = '', $wx_id = 0, $logtype = 1){
        $flowid = M('tnettask_flowlog')
            ->add([
				'taskid'=>$taskid,
				'log'=>$log,
				'logtype'=>$logtype,
				'creator'=>$wx_id,
				'createtime'=>time()
			]);
		return $flowid;
	}
}