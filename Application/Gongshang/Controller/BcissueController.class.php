<?php
//广播广告发行控制器
namespace Gongshang\Controller;
use Think\Controller;
class BcissueController extends BaseController {

    public function index(){
		$regulatorpersonInfo = M('tregulatorperson')->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
		$regulatorInfo = M('tregulator')->where(array('fid'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息

		$region_id = session('regulatorpersonInfo.regionid');//地区ID
		if(I('region_id') != '') $region_id = I('region_id');
		$this_region_id = I('this_region_id');//是否只查询本级地区

		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$keyword = I('keyword');//搜索关键词
		$fadname = I('fadname');// 广告名称
		$fversion = I('fversion');// 版本描述
		$fmediaid  = I('fmediaid');// 媒体ID
		$adclass_code = I('adclass_code');//广告类别
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID
		$fstarttime_s = I('fstarttime_s');// 发布日期
		if($fstarttime_s == ''){
			$fstarttime_s = date('Y-m-d');
			$_GET['fstarttime_s'] = $fstarttime_s;
		}	
		$fstarttime_e = I('fstarttime_e');// 发布日期
		if($fstarttime_e == ''){
			$fstarttime_e = date('Y-m-d');
			$_GET['fstarttime_e'] = $fstarttime_e;
		}
		$where = array();//查询条件

		if($fmediaid == ''){
			//是否有指定的媒体
			$media=$this->get_access_media();//获取权限判断后的最终媒体列表
			if($media){
				$where['tbcissue.fmediaid'] = array('in', $media);//发布媒介
			}
		}else{
			$where['tbcissue.fmediaid'] = $fmediaid;//发布媒介
		}

		//如果只查询当前级
		if($region_id > 0){
			if($this_region_id == 'true'){
				$where['tmediaowner.fregionid'] = $region_id;//地区搜索条件
			}elseif($region_id != 100000){
				$region_id_rtrim = A('Common/System','Model')->get_region_left($region_id);//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmediaowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}
		}
		$where['tbcissue.fstate'] = array('neq',-1);

		//如果没有检索$adclass_code 并且有special_adclass
		//如果没有检索$adclass_code 并且有special_adclass
		if(session('regulatorpersonInfo.special_adclass') &&empty($adclass_code)){
			$s_adclass_list = M('tadclass')->cache(true,6000)->where(array('tadclass.fcode|tadclass.fpcode'=>session('regulatorpersonInfo.special_adclass')))->getField('fcode',true);
		}
		//如果有检索$adclass_code
		if(!empty($adclass_code)){
			$s_adclass_list = M('tadclass')->cache(true,6000)->where(array('tadclass.fcode|tadclass.fpcode'=>$adclass_code))->getField('fcode',true);

		}
		if(count($s_adclass_list) > 0){
			$where['tad.fadclasscode'] = array('in',$s_adclass_list);//code或者pcode 是special_adclass

		}


		if($keyword != ''){
			//$where['tbcissue.fversion'] = array('like','%'.$keyword.'%');
		} 
	
		if($fadname != ''){
			$where['tad.fadname'] = array('like','%'.$fadname.'%');//广告名称
		}
		if($fversion != ''){
			$where['tbcsample.fversion'] = array('like','%'.$fversion.'%');//版本描述
		}
		if(is_array($fillegaltypecode)){
			$where['tbcsample.fillegaltypecode'] = array('in',$fillegaltypecode);//违法类型代码
		}
		if($fstarttime_s != '' || $fstarttime_e != ''){
			$where['tbcissue.fstarttime'] = array('between',$fstarttime_s.','.$fstarttime_e.' 23:59:59');
		}
		
		$special_brand = trim($regulatorInfo['special_brand']);
		if($special_brand != ''){
			$special_brand_list = explode(',',$special_brand);
			$where['tad.fbrand'] = array('in',$special_brand_list);//广告品牌
		}
		
		$count = M('tbcissue')
								->join('tbcsample on tbcsample.fid = tbcissue.fbcsampleid')
								->join('tad on tad.fadid = tbcsample.fadid')
								//->join('tadclass on tadclass.fcode = tad.fadclasscode', 'LEFT')//广告内容列别
								->join('tmedia on tmedia.fid = tbcissue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								//->join('tillegaltype on tillegaltype.fcode = tbcsample.fillegaltypecode')
								->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数

		
		$bcissueList = M('tbcissue')
										->field('
												tbcissue.*,
												(select fadname from tad where fadid = tbcsample.fadid) as fadname,
												tbcsample.fversion,
												tbcsample.favifilename,
												tmedia.fmedianame,
												(select fillegaltype from tillegaltype where fcode = tbcsample.fillegaltypecode) as fillegaltype,
												(select tadclass.ffullname from tadclass where tadclass.fcode = tad.fadclasscode) adclass_code
												
												')
										->join('tbcsample on tbcsample.fid = tbcissue.fbcsampleid')
										->join('tad on tad.fadid = tbcsample.fadid')
										//->join('tadclass on tadclass.fcode = tad.fadclasscode', 'LEFT')//广告内容列别
										->join('tmedia on tmedia.fid = tbcissue.fmediaid')
										->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
										//->join('tillegaltype on tillegaltype.fcode = tbcsample.fillegaltypecode')
										->where($where)
										->order('tbcissue.fstarttime desc')
										->limit($Page->firstRow.','.$Page->listRows)->select();//查询广告样本列表
		//var_dump(	M('tbcissue')->getLastSql());						
		$bcissueList = list_k($bcissueList,$p,$pp);//为列表加上序号
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		//var_dump($bcissueList);
		$this->assign('regionid',session('regulatorpersonInfo.regionid'));
		$this->assign('bcissueList',$bcissueList);//广告发布列表
		$this->assign('illegaltype',$illegaltype);//违法类型
		$this->assign('regulatorInfo',$regulatorInfo);//监管机构信息
		$this->assign('page',$Page->show());//分页输出
		// $this->display();
	}
	/*广告发布详情*/
	public function ajax_bcissue_details(){

		$fbcissueid = I('fbcissueid');//获取发布ID
		$fbcissueid = intval($fbcissueid);
		$bcissueDetails = M('tbcissue')
			->field('tbcissue.*,tad.fadname,tbcsample.fversion,tmedia.fmedianame,
							tbcsample.fadmanuno,tbcsample.fmanuno,tbcsample.fadapprno,tbcsample.fapprno,tbcsample.fadent,tbcsample.fent,tbcsample.fentzone,
							tillegaltype.fillegaltype,
							tbcsample.fillegalcontent,tbcsample.fexpressioncodes,tbcsample.fexpressions')
			->join('tbcsample on tbcsample.fid = tbcissue.fbcsampleid')
			->join('tad on tad.fadid = tbcsample.fadid')
			->join('tadowner on tadowner.fid = tad.fadowner')
			->join('tillegaltype on tillegaltype.fcode = tbcsample.fillegaltypecode')
			->join('tmedia on tmedia.fid = tbcissue.fmediaid')
			->where(array('tbcissue.fbcissueid'=>$fbcissueid))
			->find();//查询样本详情

		$fmp4url = strval($bcissueDetails['fmp4url']);

		$mp4url_state = strval($bcissueDetails['mp4url_state']);
		if($bcissueDetails['validity_time'] < time()){
			$mp4url_state = '0';
		}

		$ad_part = array(
			'fmp4url'=>$fmp4url,
			'mp4url_state'=>$mp4url_state,

		);

		$this->ajaxReturn(array('code'=>0,'bcissueDetails'=>$bcissueDetails,'ad_part'=>$ad_part));
	}

}