<?php
namespace Agp\Controller;
use Think\Controller;
use Think\Log;
class FsmsmanageController extends BaseController {

	/**
	* 短信发送列表
	*/
    public function index(){
    	$p  = I('page', 1);//当前第几页
    	$pp = 20;//每页显示多少记录
    	$sl_phone = I('sl_phone');//接收手机
    	$fmediaowner = I('fmediaowner');//接收机构
    	$sl_returnnote = I('sl_returnnote');//发送内容

    	$where_alg['sl_getobj'] = 1;
    	$where_alg['sl_senduserid'] = session('regulatorpersonInfo.fid');

    	if(!empty($sl_phone)){
    		$where_alg['sl_phone'] = array('like','%'.$sl_phone.'%');
    	}
    	if(!empty($fmediaowner)){
    		$where_alg['b.fname'] = array('like','%'.$fmediaowner.'%');
    	}
    	if(!empty($sl_returnnote)){
    		$where_alg['sl_returnnote'] = array('like','%'.$sl_returnnote.'%');
    	}

    	$count = M('agpsms_log')
    		->alias('a')
    		->where($where_alg)
    		->join('tmediaowner b on a.sl_getuserid = b.fid')
    		->count();

    	$do_alg = M('agpsms_log')
    		->alias('a')
    		->field('sl_content,sl_creattime,sl_phone,sl_returnnote,b.fname as fmediaowner,sl_type')
    		->where($where_alg)
    		->join('tmediaowner b on a.sl_getuserid = b.fid')
    		->order('sl_creattime desc')
    		->page($p,$pp)
    		->select();

    	$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_alg)));
	}

	public function getmediaowner(){
		$system_num = getconfig('system_num');

		$data_ta = M('tmedia')
			->field('tmediaowner.fid,tmediaowner.fname,tmediaowner.ftel')
			->join('tmediaowner on tmediaowner.fid=tmedia.fmediaownerid')
			->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->where($where_ta)
			->order('tmediaowner.fregionid asc,tmedia.fid asc')
			->group('tmediaowner.fid,tmediaowner.fname')
			->select();

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data_ta));
	}

	public function sendsms(){
		$memdiaowner = I('mediaowner');//媒体机构列表，数组
		$smscontent = $_POST['smscontent'];//发送内容
		$sl_type = I('sl_type');//短信类别
		$phonearr = [];//手机号，数组

		if(empty($memdiaowner)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'发送失败，您未选择媒体机构'));
		}
		$replace_arr = ['"',',',':','.',"'"];//被改字符
		$replace_arr2 = ['“','，','：','。','‘'];//替换字符
		foreach ($replace_arr as $key => $value) {
			$smscontent = str_replace($value, $replace_arr2[$key], $smscontent);
		}

		$where_tr['fid'] = array('in',$memdiaowner);
		$do_tr = M('tmediaowner')
			->field('ftel,fid')
			->where($where_tr)
			->select();
		foreach ($do_tr as $key => $value) {
			if(!empty($value['ftel'])){
				$data['sl_type'] = $sl_type;
				$data['sl_content'] = $smscontent;
				$data['sl_creattime'] = date("Y-m-d H:i:s");
				$data['sl_phone'] = $value['ftel'];
				$data['sl_templateno'] = '';
				$data['sl_getobj'] = 1;
				$data['sl_getuserid'] = $value['fid'];
				$data['sl_sendobj'] = 2;
				$data['sl_sendtreid'] = session('regulatorpersonInfo.fregulatorpid');
				$data['sl_senduserid'] = session('regulatorpersonInfo.fid');

                $phonearr[] = $value['ftel'];
				$do_sendsms = ['code' => 0,'note' => '发送成功'];
				$data['sl_returncode'] = $do_sendsms['code'];
				$data['sl_returnnote'] = $do_sendsms['note'];
			}else{
				$data['sl_type'] = $sl_type;
				$data['sl_content'] = $smscontent;
				$data['sl_creattime'] = date("Y-m-d H:i:s");
				$data['sl_phone'] = '';
				$data['sl_templateno'] = '';
				$data['sl_getobj'] = 1;
				$data['sl_getuserid'] = $value['fid'];
				$data['sl_sendobj'] = 2;
				$data['sl_sendtreid'] = session('regulatorpersonInfo.fregulatorpid');
				$data['sl_senduserid'] = session('regulatorpersonInfo.fid');
				$data['sl_returncode'] = -1;
				$data['sl_returnnote'] = '发送失败，手机号码有误';
			}
			$adddata[] = $data;
		}

		if(!empty($phonearr)){
            $this -> user_sendsms($phonearr,$smscontent);
		}

		if(!empty($adddata)){
			M('agpsms_log') -> addAll($adddata);
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'发送完成'));
	}

	/**
     * 发送短信通知各媒介机构
     */
    public function user_sendsms($phonearr = [],$smscontent = ''){
        if(empty($phonearr) || empty($smscontent)){
            return false;
        }

       $url = 'http://10.0.101.102:8888/esb/SMESSAGE/0?authcode=QUhfQU5HR0pDI0BhdXRoQCN6ZGJBUUpsWQ';
        foreach ($phonearr as $key => $value) {
           $params['phone'] = $value;
           $params['content'] = $smscontent;
           http($url,json_encode($params),'POST',array('Content-Type:application/json; charset=utf-8'),1);
        }
        return true;
    }

}
