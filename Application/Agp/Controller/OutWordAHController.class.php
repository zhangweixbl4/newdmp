<?php
namespace Agp\Controller;
use Think\Controller;
use Agp\Model\StatisticalReportModel;
use Agp\Model\StatisticalModel;
class OutWordAHController extends BaseController{

    /**
     * 获取历史报告列表
     * by yjn
     */
    public function index() {
        session_write_close();
        header("Content-type:text/html;charset=utf-8");

        $p                  = I('page', 1);//当前第几页
        $pp                 = 20;//每页显示多少记录
        $pnname             = I('pnname');// 报告名称
        $pntype             = I('pntype');// 报告类型
        $pnfiletype         = I('pnfiletype');// 文件类型
        $pncreatetime       = I('pnendtime');//报告时间
        $system_num = getconfig('system_num');

        if (!empty($pnname)) {
            $where['pnname'] = array('like', '%' . $pnname . '%');//报告名称
        }
        if (!empty($pntype)) {
            $where['pntype'] = $pntype;//报告类型
        }else{
            $where['pntype'] = array('in',array(10,20,30,70,75,80));//报告类型
        }
        if (!empty($pnfiletype)) {
            $where['pnfiletype'] = $pnfiletype;//文件类型
        }
        if(!empty($pncreatetime)){
            $where['pnendtime'] = array('between',array($pncreatetime[0],$pncreatetime[1]));
        }
        $where['pntrepid'] = session('regulatorpersonInfo.fregulatorpid');
        $where['fcustomer']  = $system_num;

        $count = M('tpresentation')
            ->where($where)
            ->count();

        
        $data = M('tpresentation')
            ->where($where)
            ->order('pncreatetime desc')
            ->page($p,$pp)
            ->select();
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
    }

    //安徽报告接口report
    public function create_report_anhui(){

        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $system_num = getconfig('system_num');

        $user = session('regulatorpersonInfo');//获取用户信息

        $StatisticalReport = New StatisticalModel();//实例化数据统计模型
        $s_time = I('s_time',date('Y-m-d',time()));//开始时间
        $e_time = I('e_time',date('Y-m-d',time()));//结束时间

        if(!empty(I('pntype'))){
            $pntype = I('pntype');
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'报告类型未选择'));
        }

        $prv_time = $this->prv_time($pntype,$s_time,$e_time);//获取首末时间戳

        $area = I('area',0);

        $level = -1;
        $area_str = '全省';
        $area_level = '全省';

        switch ($area){
            case 0:
                $area_arr = [
                    [
                        'title'=>'一、省属媒体',
                        'area_str'=>'省属',
                        'area_level'=>'省级',
                        'level' => 1,
                        'is_sub' => 0
                    ],
                    [
                        'title'=>'二、市属媒体',
                        'area_str'=>'市属',
                        'area_level'=>'市级',
                        'level' => [2,3,4],
                        'is_sub' => 2
                    ]
                ];
                break;
            case 1:
                $area_arr = [
                    [
                        'title'=>'一、省属媒体',
                        'area_str'=>'省属',
                        'area_level'=>'省级',
                        'level' => 1,
                        'is_sub' => 0
                    ]
                ];
                break;
            case 2:
                $area_arr = [
                    [
                        'title'=>'二、市属媒体',
                        'area_str'=>'市属',
                        'area_level'=>'市级',
                        'level' => [2,3,4],
                        'is_sub' => 2
                    ]
                ];
                break;
        }

        $report_start_date = strtotime($s_time);//指定初时间戳
        $report_end_date = strtotime($e_time);//指定末时间戳

        $last_s_time = $prv_time['s'];
        $last_e_time = $prv_time['e'];

        $date_ym = date('Y年m月',$report_start_date);//年月字符
        $user = session('regulatorpersonInfo');//获取用户信息

        if(intval($month_last) < 10){
            $month_last_str = '0'.$month_last;
        }else{
            $month_last_str = $month_last;
        }

        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数

        //定义数据数组
        $data = [
            'dotfilename' => 'TemAH.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑60加粗居中",
            "text" => "广告监测通报"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体_DB2312三号加粗",
            "text" => '广告监管专刊（总*期）'
        ];//


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号居中加粗",
            "text" => $user["regulatorpname"].'                            '.$date_end_ymd
        ];

        $month_num = strval(substr($month,5,2));//月份

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体二号居中",
            "text" => $this->time_str($pntype,$s_time).'广告监测通报'
        ];

        foreach ($area_arr as $area_val){

            $ct_media_count = count(get_owner_media_ids($area_val['level']));//传统媒体数量
            $tv_media_count = count(get_owner_media_ids($area_val['level'],'01'));//电视媒体数量
            $tb_media_count = count(get_owner_media_ids($area_val['level'],'02'));//广播媒体数量
            $tp_media_count = count(get_owner_media_ids($area_val['level'],'03'));//报纸媒体数量

/*
            $last_media_class_date = $StatisticalReport->report_ad_monitor('fmedia_class_code',get_owner_media_ids($area_val['level']),false,date('Y-m-d',$last_s_time),date('Y-m-d',$last_e_time),'times_illegal_rate');
            $last_all_count = 0;
            $last_all_illegal_count = 0;
            $last_all_times = 0;
            $last_all_illegal_times = 0;
            $last_all_play_len = 0;
            $last_all_illegal_play_len = 0;
            foreach ($last_media_class_date as $last_media_class_date_key => $last_media_class_date_val){
                $last_all_count += $last_media_class_date_val['fad_count'];
                $last_all_illegal_count += $last_media_class_date_val['fad_illegal_count'];
                $last_all_times += $last_media_class_date_val['fad_times'];
                $last_all_illegal_times += $last_media_class_date_val['fad_illegal_times'];
                $last_all_play_len += $last_media_class_date_val['fad_play_len'];
                $last_all_illegal_play_len += $last_media_class_date_val['fad_illegal_play_len'];
            }*/

            /*广告类别*/
            $every_ad_class = $StatisticalReport->report_ad_monitor('fad_class_code',get_owner_media_ids($area_val['level']),false,date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate');//各广告类别监测总情况
            $ad_class_count = 0;
            $ad_class_illegal_count = 0;
            $ad_class_times = 0;
            $ad_class_illegal_times = 0;
            $ad_class_play_len = 0;
            $ad_class_illegal_play_len = 0;
            $every_ad_class_data = [];
            $tadclass_names = [];
            $tadclass_rates = [];
            foreach ($every_ad_class as $every_ad_class_key => $every_ad_class_val){

                if($every_ad_class_val['fad_illegal_times'] > 0 && count($tadclass_names) < 3){
                    $tadclass_names[] = $every_ad_class_val['tadclass_name'];
                    $tadclass_rates[] = $every_ad_class_val['times_illegal_rate'].'%';
                }

                if(!$every_ad_class_val['titlename']){
                    $ad_class_count += $every_ad_class_val['fad_count'];
                    $ad_class_illegal_count += $every_ad_class_val['fad_illegal_count'];
                    $ad_class_times += $every_ad_class_val['fad_times'];
                    $ad_class_illegal_times += $every_ad_class_val['fad_illegal_times'];
                    $ad_class_play_len += $every_ad_class_val['fad_play_len'];
                    $ad_class_illegal_play_len += $every_ad_class_val['fad_illegal_play_len'];
                }else{
                    $every_ad_class_val['tadclass_name'] = $every_ad_class_val['titlename'];
                }
                $every_ad_class_data[] = [
                    $every_ad_class_key+1,
                    $every_ad_class_val['tadclass_name'],
                    $every_ad_class_val['fad_count'],
                    $every_ad_class_val['fad_illegal_count'].($every_ad_class_val['prv_fad_illegal_count']?$every_ad_class_val['prv_fad_illegal_count']:'(0.00%)'),
                    $every_ad_class_val['counts_illegal_rate'].'%',
                    $every_ad_class_val['fad_times'],
                    $every_ad_class_val['fad_illegal_times'].($every_ad_class_val['prv_fad_illegal_times']?$every_ad_class_val['prv_fad_illegal_times']:'(0.00%)'),
                    $every_ad_class_val['times_illegal_rate'].'%',
                    $every_ad_class_val['fad_play_len'],
                    $every_ad_class_val['fad_illegal_play_len'].($every_ad_class_val['prv_fad_illegal_play_len']?$every_ad_class_val['prv_fad_illegal_play_len']:'(0.00%)'),
                    $every_ad_class_val['lens_illegal_rate'].'%',
                ];

            }


            $every_tv_media_jczqk = $StatisticalReport->report_ad_monitor('fmediaid',get_owner_media_ids($area_val['level']),false,date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','01');//电视各媒体监测总情况


            $every_tb_media_jczqk = $StatisticalReport->report_ad_monitor('fmediaid',get_owner_media_ids($area_val['level']),false,date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','02');//广播各媒体监测总情况

            $every_tp_media_jczqk = $StatisticalReport->report_ad_monitor('fmediaid',get_owner_media_ids($area_val['level']),false,date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','03');//报纸各媒体监测总情况

            $all_count = 0;
            $all_illegal_count = 0;
            $all_times = 0;
            $all_illegal_times = 0;
            $all_play_len = 0;
            $all_illegal_play_len = 0;

            $tv_count = 0;
            $tv_illegal_count = 0;
            $tv_times = 0;
            $tv_illegal_times = 0;
            $tv_play_len = 0;
            $tv_illegal_play_len = 0;
            $every_tv_media_data = [];
            foreach ($every_tv_media_jczqk as $every_tv_media_jczqk_key => $every_tv_media_jczqk_val){

                if(!$every_tv_media_jczqk_val['titlename']){
                    $all_count += $every_tv_media_jczqk_val['fad_count'];
                    $all_illegal_count += $every_tv_media_jczqk_val['fad_illegal_count'];
                    $all_times += $every_tv_media_jczqk_val['fad_times'];
                    $all_illegal_times += $every_tv_media_jczqk_val['fad_illegal_times'];
                    $all_play_len += $every_tv_media_jczqk_val['fad_play_len'];
                    $all_illegal_play_len += $every_tv_media_jczqk_val['fad_illegal_play_len'];

                    $tv_count += $every_tv_media_jczqk_val['fad_count'];
                    $tv_illegal_count += $every_tv_media_jczqk_val['fad_illegal_count'];
                    $tv_times += $every_tv_media_jczqk_val['fad_times'];
                    $tv_illegal_times += $every_tv_media_jczqk_val['fad_illegal_times'];
                    $tv_play_len += $every_tv_media_jczqk_val['fad_play_len'];
                    $tv_illegal_play_len += $every_tv_media_jczqk_val['fad_illegal_play_len'];
                }else{
                    $every_tv_media_jczqk_val['tmedia_name'] = $every_tv_media_jczqk_val['titlename'];
                }


                $every_tv_media_data[] = [
                    $every_tv_media_jczqk_val['tmedia_name'],
                    $every_tv_media_jczqk_val['fad_count'],
                    $every_tv_media_jczqk_val['fad_illegal_count'].($every_tv_media_jczqk_val['prv_fad_illegal_count']?$every_tv_media_jczqk_val['prv_fad_illegal_count']:'(0.00%)'),
                    $every_tv_media_jczqk_val['counts_illegal_rate'].'%',
                    $every_tv_media_jczqk_val['fad_times'],
                    $every_tv_media_jczqk_val['fad_illegal_times'].($every_tv_media_jczqk_val['prv_fad_illegal_times']?$every_tv_media_jczqk_val['prv_fad_illegal_times']:'(0.00%)'),
                    $every_tv_media_jczqk_val['times_illegal_rate'].'%',
                    $every_tv_media_jczqk_val['fad_play_len'],
                    $every_tv_media_jczqk_val['fad_illegal_play_len'].($every_tv_media_jczqk_val['prv_fad_illegal_play_len']?$every_tv_media_jczqk_val['prv_fad_illegal_play_len']:'(0.00%)'),
                    $every_tv_media_jczqk_val['lens_illegal_rate'].'%',
                ];

            }
            $tb_count = 0;
            $tb_illegal_count = 0;
            $tb_times = 0;
            $tb_illegal_times = 0;
            $tb_play_len = 0;
            $tb_illegal_play_len = 0;
            $every_tb_media_data = [];
            foreach ($every_tb_media_jczqk as $every_tb_media_jczqk_key => $every_tb_media_jczqk_val){

                if(!$every_tb_media_jczqk_val['titlename']){
                    $all_count += $every_tb_media_jczqk_val['fad_count'];
                    $all_illegal_count += $every_tb_media_jczqk_val['fad_illegal_count'];
                    $all_times += $every_tb_media_jczqk_val['fad_times'];
                    $all_illegal_times += $every_tb_media_jczqk_val['fad_illegal_times'];
                    $all_play_len += $every_tb_media_jczqk_val['fad_play_len'];
                    $all_illegal_play_len += $every_tb_media_jczqk_val['fad_illegal_play_len'];

                    $tb_count += $every_tb_media_jczqk_val['fad_count'];
                    $tb_illegal_count += $every_tb_media_jczqk_val['fad_illegal_count'];
                    $tb_times += $every_tb_media_jczqk_val['fad_times'];
                    $tb_illegal_times += $every_tb_media_jczqk_val['fad_illegal_times'];
                    $tb_play_len += $every_tb_media_jczqk_val['fad_play_len'];
                    $tb_illegal_play_len += $every_tb_media_jczqk_val['fad_illegal_play_len'];
                }else{
                    $every_tb_media_jczqk_val['tmedia_name'] = $every_tb_media_jczqk_val['titlename'];
                }
                $every_tb_media_data[] = [
                    $every_tb_media_jczqk_val['tmedia_name'],
                    $every_tb_media_jczqk_val['fad_count'],
                    $every_tb_media_jczqk_val['fad_illegal_count'].($every_tb_media_jczqk_val['prv_fad_illegal_count']?$every_tb_media_jczqk_val['prv_fad_illegal_count']:'(0.00%)'),
                    $every_tb_media_jczqk_val['counts_illegal_rate'].'%',
                    $every_tb_media_jczqk_val['fad_times'],
                    $every_tb_media_jczqk_val['fad_illegal_times'].($every_tb_media_jczqk_val['prv_fad_illegal_times']?$every_tb_media_jczqk_val['prv_fad_illegal_times']:'(0.00%)'),
                    $every_tb_media_jczqk_val['times_illegal_rate'].'%',
                    $every_tb_media_jczqk_val['fad_play_len'],
                    $every_tb_media_jczqk_val['fad_illegal_play_len'].($every_tb_media_jczqk_val['prv_fad_illegal_play_len']?$every_tb_media_jczqk_val['prv_fad_illegal_play_len']:'(0.00%)'),
                    $every_tb_media_jczqk_val['lens_illegal_rate'].'%',
                ];

            }

            $tp_count = 0;
            $tp_illegal_count = 0;
            $tp_times = 0;
            $tp_illegal_times = 0;
            $every_tp_media_data = [];
            foreach ($every_tp_media_jczqk as $every_tp_media_jczqk_key => $every_tp_media_jczqk_val){

                if(!$every_tp_media_jczqk_val['titlename']){
                    $all_count += $every_tp_media_jczqk_val['fad_count'];
                    $all_illegal_count += $every_tp_media_jczqk_val['fad_illegal_count'];
                    $all_times += $every_tp_media_jczqk_val['fad_times'];
                    $all_illegal_times += $every_tp_media_jczqk_val['fad_illegal_times'];

                    $tp_count += $every_tp_media_jczqk_val['fad_count'];
                    $tp_illegal_count += $every_tp_media_jczqk_val['fad_illegal_count'];
                    $tp_times += $every_tp_media_jczqk_val['fad_times'];
                    $tp_illegal_times += $every_tp_media_jczqk_val['fad_illegal_times'];
                }else{
                    $every_tp_media_jczqk_val['tmedia_name'] = $every_tp_media_jczqk_val['titlename'];
                }

                $every_tp_media_data[] = [
                    $every_tp_media_jczqk_val['tmedia_name'],
                    $every_tp_media_jczqk_val['fad_count'],
                    $every_tp_media_jczqk_val['fad_illegal_count'].($every_tp_media_jczqk_val['prv_fad_illegal_count']?$every_tp_media_jczqk_val['prv_fad_illegal_count']:'(0.00%)'),
                    $every_tp_media_jczqk_val['counts_illegal_rate'].'%',
                    $every_tp_media_jczqk_val['fad_times'],
                    $every_tp_media_jczqk_val['fad_illegal_times'].($every_tp_media_jczqk_val['prv_fad_illegal_times']?$every_tp_media_jczqk_val['prv_fad_illegal_times']:'(0.00%)'),
                    $every_tp_media_jczqk_val['times_illegal_rate'].'%',
                ];

            }//0824

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "黑体三号标题",
                "text" => $area_val['title']
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "黑体三号标题",
                "text" => '（一）广告监测概况'
            ];

            $all_illegal_count_rate = round(($all_illegal_count/$all_count)*100,2);
            $all_illegal_times_rate = round(($all_illegal_times/$all_times)*100,2);
            $all_illegal_play_len_rate = round(($all_illegal_play_len/$all_play_len)*100,2);
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号正文",
                "text" => $date_ymd.'至'.$date_md.$user["regionname"].'广告监测中心共抽查监测我省'. $area_val['area_str'].'媒体'.$ct_media_count.'家，其中报纸类媒体'.$tp_media_count.'家，电视类媒体'.$tv_media_count.'家，广播类媒体'.$tb_media_count.'家。抽查监测'. $area_val['area_str'].'媒体广告'.$all_count.'条，其中涉嫌违法广告'.$all_illegal_count.'条，条数违法率为'.$all_illegal_count_rate.'%；抽查监测'. $area_val['area_str'].'媒体广告'.$all_times.'条次，其中涉嫌违法广告'.$all_illegal_times.'条次，条次违法率为'.$all_illegal_times_rate.'%；抽查监测'.$area_val['area_str'].'电视、广播类媒体广告总时长为'.$all_play_len.'秒，其中涉嫌违法广告时长'.$all_illegal_play_len.'秒，时长违法率为'.$all_illegal_play_len_rate.'%。'
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "黑体三号标题",
                "text" => '（二）各类媒体广告发布与涉嫌违法情况'
            ];//国家局季报汇总列表

            $num_val_str = ['（1）','（2）','（3）'];

            if(count($every_tp_media_data) > 1){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号标题",
                    "text" => $num_val_str[0].'报纸类'
                ];

                unset($num_val_str[0]);
                $num_val_str = array_values($num_val_str);
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号正文",
                    "text" => $this->time_str($pntype,$s_time).$user["regionname"].'广告监测中心对我省'.$tp_media_count.'家'.$area_val['area_str'].'报纸媒体发布的广告进行了抽查监测，共监测各类广告'.$tp_count.'条，其中涉嫌违法广告'.$tp_illegal_count.'条，条数违法率为'.round(($tp_illegal_count/$tp_count)*100,2).'%；监测各类广告'.$tp_times.'条次，其中涉嫌违法广告'.$tp_illegal_times.'条次，条次违法率为'.round(($tp_illegal_times/$tp_times)*100,2).'%。'
                ];

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "楷体三号居中加粗",
                    "text" => '广告发布及涉嫌违法情况'
                ];

                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "媒体条数与条次发布情况列表",
                    "data" => to_string($every_tp_media_data)
                ];
            }

            if(count($every_tv_media_data) > 1){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "楷体三号标题",
                    "text" => $num_val_str[0].'电视类'
                ];
                unset($num_val_str[0]);
                $num_val_str = array_values($num_val_str);
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号正文",
                    "text" => $this->time_str($pntype,$s_time).$user["regionname"].'广告监测中心对我省'.$tv_media_count.'家'.$area_val['area_str'].'电视媒体发布的广告进行了抽查监测，共监测各类广告'.$tv_count.'条，其中涉嫌违法广告'.$tv_illegal_count.'条，条数违法率为'.round(($tv_illegal_count/$tv_count)*100,2).'%；监测各类广告'.$tv_times.'条次，其中涉嫌违法广告'.$tv_illegal_times.'条次，条次违法率为'.round(($tv_illegal_times/$tv_times)*100,2).'%。'
                ];

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "楷体三号居中加粗",
                    "text" => '广告发布及涉嫌违法情况'
                ];

                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "媒体条数与条次发布情况列表带时长",
                    "data" => to_string($every_tv_media_data)
                ];
            }

            if(count($every_tb_media_data) > 1){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "楷体三号标题",
                    "text" => $num_val_str[0].'广播类'
                ];
                unset($num_val_str[0]);
                $num_val_str = array_values($num_val_str);
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号正文",
                    "text" => $this->time_str($pntype,$s_time).$user["regionname"].'广告监测中心对我省'.$tb_media_count.'家'.$area_val['area_str'].'广播媒体发布的广告进行了抽查监测，共监测各类广告'.$tb_count.'条，其中涉嫌违法广告'.$tb_illegal_count.'条，条数违法率为'.round(($tb_illegal_count/$tb_count)*100,2).'%；监测各类广告'.$tb_times.'条次，其中涉嫌违法广告'.$tb_illegal_times.'条次，条次违法率为'.round(($tb_illegal_times/$tb_times)*100,2).'%。'
                ];

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "楷体三号居中加粗",
                    "text" => '广告发布及涉嫌违法情况'
                ];

                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "媒体条数与条次发布情况列表带时长",
                    "data" => to_string($every_tb_media_data)
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "黑体三号标题",
                "text" => '（三） 各类媒体涉嫌发布违法广告情况'
            ];


            $media_class_ad_tv = $this->media_class_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),1,'340000','340000',$area_val['is_sub']);

            $media_class_ad_tb = $this->media_class_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),2,'340000','340000',$area_val['is_sub']);

            $media_class_ad_tp = $this->media_class_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),3,'340000','340000',$area_val['is_sub']);
            $num_val_str = ['（1）','（2）','（3）'];
            if(count($media_class_ad_tp) > 1){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "楷体三号标题",
                    "text" => $num_val_str[0].'报纸类'
                ];
                unset($num_val_str[0]);
                $num_val_str = array_values($num_val_str);
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "媒体广告类别条次明细列表",
                    "data" => to_string($this->create_ill_list($media_class_ad_tp))
                ];
            }

            if(count($media_class_ad_tv) > 1){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "楷体三号标题",
                    "text" => $num_val_str[0].'电视类'
                ];
                unset($num_val_str[0]);
                $num_val_str = array_values($num_val_str);
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "媒体广告类别条次明细列表",
                    "data" => to_string($this->create_ill_list($media_class_ad_tv))
                ];
            }

            if(count($media_class_ad_tb) > 1){

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "楷体三号标题",
                    "text" => $num_val_str[0].'广播类'
                ];
                unset($num_val_str[0]);
                $num_val_str = array_values($num_val_str);
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "媒体广告类别条次明细列表",
                    "data" => to_string($this->create_ill_list($media_class_ad_tb))
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "黑体三号标题",
                "text" => '（四）各类违法广告占比情况'
            ];

            if(empty($tadclass_names)){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号正文",
                    "text" => "本期各类别无违法广告"
                ];
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号正文",
                    "text" => "从本期监测情况看，".implode($tadclass_names,'、')."的广告条次违法率为".implode($tadclass_rates,'、')."，其他类别条次违法率均小于".$tadclass_rates[count($tadclass_rates)-1]."。"
                ];
            }


            $data['content'][] = [
                "type" => "table",
                "bookmark" => "广告类别条数与条次发布情况表",
                "data" => to_string($every_ad_class_data)
            ];

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号靠右",
            "text" => $user["regulatorpname"]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号靠右",
            "text" => $date_end_ymd.'印'
        ];

        $report_data = json_encode($data);
        //echo $report_data;exit;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            //将生成记录保存
            $focus = I('focus',0);
            $pnname = $area_str.$this->time_str($pntype,$s_time).'报告';//time_str$user['regulatorname'].date('Y年m月',strtotime($month)).$area_str.'月报';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= $pntype;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']    = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }

    //安徽报告接口备份
    public function create_report_anhui_bak_20190705(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $system_num = getconfig('system_num');

        $user = session('regulatorpersonInfo');//获取用户信息
        //$StatisticalReport = New StatisticalReportModel();//实例化数据统计模型
        $StatisticalReport = New StatisticalModel();//实例化数据统计模型
        $s_time = I('s_time',date('Y-m-d',time()));//接收时间  TODO::时间改
        $e_time = I('e_time',date('Y-m-d',time()));//接收时间  TODO::时间改
        $pntype = I('pntype',30);

        $prv_time = $this->prv_time($pntype,$s_time,$e_time);//获取首末时间戳
        $area = I('area',0);
        $level = -1;
        $area_str = '全省';
        $area_level = '全省';
        switch ($area){
            case 0:
                $level = -1;
                break;
            case 1:
                $area_str = '省属';
                $area_level = '省级';
                $level = 1;
                break;
            case 2:
                $area_str = '市属';
                $area_level = '市级';
                $level = [2,3,4];
                break;
        }
        $report_start_date = strtotime($s_time);//指定初时间戳
        $report_end_date = strtotime($e_time);//指定末时间戳

        $last_s_time = $prv_time['s'];
        $last_e_time = $prv_time['e'];

        //mktime(23, 59, 59, date('m', strtotime($last_date))+1, 00);

        $date_ym = date('Y年m月',$report_start_date);//年月字符
        $user = session('regulatorpersonInfo');//获取用户信息
        $date_table = date('Ym',strtotime($month)).'_'.substr($user['regionid'],0,2);//组合表
        if(intval($month_last) < 10){
            $month_last_str = '0'.$month_last;
        }else{
            $month_last_str = $month_last;
        }

        $last_date_table = $year_last.$month_last_str.'_'.substr($user['regionid'],0,2);//组合表

        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数

        $ct_media_count = count(get_owner_media_ids($level));//传统媒体数量
        $tv_media_count = count(get_owner_media_ids($level,'01'));//电视媒体数量
        $tb_media_count = count(get_owner_media_ids($level,'02'));//广播媒体数量
        $tp_media_count = count(get_owner_media_ids($level,'03'));//报纸媒体数量


        $last_media_class_date = $StatisticalReport->report_ad_monitor('fmedia_class_code',get_owner_media_ids($level),false,date('Y-m-d',$last_s_time),date('Y-m-d',$last_e_time),'times_illegal_rate');

/*        $last_media_class_date = $StatisticalReport->ad_monitor_customize($last_date_table,$last_s_time,$last_e_time,get_owner_media_ids($level),'','','fad_illegal_times_rate','fmediaclass');//上周期各媒体类型监测总情况*/
        $last_all_count = 0;
        $last_all_illegal_count = 0;
        $last_all_times = 0;
        $last_all_illegal_times = 0;
        $last_all_play_len = 0;
        $last_all_illegal_play_len = 0;
        foreach ($last_media_class_date as $last_media_class_date_key => $last_media_class_date_val){
            $last_all_count += $last_media_class_date_val['fad_count'];
            $last_all_illegal_count += $last_media_class_date_val['fad_illegal_count'];
            $last_all_times += $last_media_class_date_val['fad_times'];
            $last_all_illegal_times += $last_media_class_date_val['fad_illegal_times'];
            $last_all_play_len += $last_media_class_date_val['fad_play_len'];
            $last_all_illegal_play_len += $last_media_class_date_val['fad_illegal_play_len'];
        }

/*        $every_tv_media_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,get_owner_media_ids($level),'01','','fad_illegal_times_rate','fmediaid');//电视各媒体监测总情况
        $every_tb_media_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,get_owner_media_ids($level),'02','','fad_illegal_times_rate','fmediaid');//广播各媒体监测总情况
        $every_tp_media_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,get_owner_media_ids($level),'03','','fad_illegal_times_rate','fmediaid');//报纸各媒体监测总情况*/

        $every_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',get_owner_media_ids($level),false,date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'fad_illegal_times');//各地区监测总情况


        $region_count = 0;
        $region_illegal_count = 0;
        $region_times = 0;
        $region_illegal_times = 0;
        $region_play_len = 0;
        $region_illegal_play_len = 0;
        $every_region_data = [];
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){
            if(!$every_region_jczqk_val['titlename']){
                $region_count += $every_region_jczqk_val['fad_count'];
                $region_illegal_count += $every_region_jczqk_val['fad_illegal_count'];
                $region_times += $every_region_jczqk_val['fad_times'];
                $region_illegal_times += $every_region_jczqk_val['fad_illegal_times'];
                $region_play_len += $every_region_jczqk_val['fad_play_len'];
                $region_illegal_play_len += $every_region_jczqk_val['fad_illegal_play_len'];
            }else{
                $every_region_jczqk_val['tregion_name'] = $every_region_jczqk_val['titlename'];
            }
            $every_region_data[] = [
                $every_region_jczqk_key+1,
                $every_region_jczqk_val['tregion_name'],
                $every_region_jczqk_val['fad_count'],
                $every_region_jczqk_val['fad_illegal_count'].($every_region_jczqk_val['prv_fad_illegal_count']?$every_region_jczqk_val['prv_fad_illegal_count']:'(0.00%)'),
                $every_region_jczqk_val['counts_illegal_rate'].'%',
                $every_region_jczqk_val['fad_times'],
                $every_region_jczqk_val['fad_illegal_times'].($every_region_jczqk_val['prv_fad_illegal_times']?$every_region_jczqk_val['prv_fad_illegal_times']:'(0.00%)'),
                $every_region_jczqk_val['times_illegal_rate'].'%',
                $every_region_jczqk_val['fad_play_len'],
                $every_region_jczqk_val['fad_illegal_play_len'].($every_region_jczqk_val['prv_fad_illegal_play_len']?$every_region_jczqk_val['prv_fad_illegal_play_len']:'(0.00%)'),
                $every_region_jczqk_val['lens_illegal_rate'].'%',
            ];

        }



        $every_tv_media_jczqk = $StatisticalReport->report_ad_monitor('fmediaid',get_owner_media_ids($level),false,date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','01');//电视各媒体监测总情况


        $every_tb_media_jczqk = $StatisticalReport->report_ad_monitor('fmediaid',get_owner_media_ids($level),false,date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','02');//广播各媒体监测总情况

        $every_tp_media_jczqk = $StatisticalReport->report_ad_monitor('fmediaid',get_owner_media_ids($level),false,date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','03');//报纸各媒体监测总情况

        $all_count = 0;
        $all_illegal_count = 0;
        $all_times = 0;
        $all_illegal_times = 0;
        $all_play_len = 0;
        $all_illegal_play_len = 0;

        $tv_count = 0;
        $tv_illegal_count = 0;
        $tv_times = 0;
        $tv_illegal_times = 0;
        $tv_play_len = 0;
        $tv_illegal_play_len = 0;
        $every_tv_media_data = [];
        foreach ($every_tv_media_jczqk as $every_tv_media_jczqk_key => $every_tv_media_jczqk_val){

            if(!$every_tv_media_jczqk_val['titlename']){
                $all_count += $every_tv_media_jczqk_val['fad_count'];
                $all_illegal_count += $every_tv_media_jczqk_val['fad_illegal_count'];
                $all_times += $every_tv_media_jczqk_val['fad_times'];
                $all_illegal_times += $every_tv_media_jczqk_val['fad_illegal_times'];
                $all_play_len += $every_tv_media_jczqk_val['fad_play_len'];
                $all_illegal_play_len += $every_tv_media_jczqk_val['fad_illegal_play_len'];

                $tv_count += $every_tv_media_jczqk_val['fad_count'];
                $tv_illegal_count += $every_tv_media_jczqk_val['fad_illegal_count'];
                $tv_times += $every_tv_media_jczqk_val['fad_times'];
                $tv_illegal_times += $every_tv_media_jczqk_val['fad_illegal_times'];
                $tv_play_len += $every_tv_media_jczqk_val['fad_play_len'];
                $tv_illegal_play_len += $every_tv_media_jczqk_val['fad_illegal_play_len'];
            }else{
                $every_tv_media_jczqk_val['tmedia_name'] = $every_tv_media_jczqk_val['titlename'];
            }


            $every_tv_media_data[] = [
                $every_tv_media_jczqk_val['tmedia_name'],
                $every_tv_media_jczqk_val['fad_count'],
                $every_tv_media_jczqk_val['fad_illegal_count'].($every_tv_media_jczqk_val['prv_fad_illegal_count']?$every_tv_media_jczqk_val['prv_fad_illegal_count']:'(0.00%)'),
                $every_tv_media_jczqk_val['counts_illegal_rate'].'%',
                $every_tv_media_jczqk_val['fad_times'],
                $every_tv_media_jczqk_val['fad_illegal_times'].($every_tv_media_jczqk_val['prv_fad_illegal_times']?$every_tv_media_jczqk_val['prv_fad_illegal_times']:'(0.00%)'),
                $every_tv_media_jczqk_val['times_illegal_rate'].'%',
                $every_tv_media_jczqk_val['fad_play_len'],
                $every_tv_media_jczqk_val['fad_illegal_play_len'].($every_tv_media_jczqk_val['prv_fad_illegal_play_len']?$every_tv_media_jczqk_val['prv_fad_illegal_play_len']:'(0.00%)'),
                $every_tv_media_jczqk_val['lens_illegal_rate'].'%',
            ];

        }
        $tb_count = 0;
        $tb_illegal_count = 0;
        $tb_times = 0;
        $tb_illegal_times = 0;
        $tb_play_len = 0;
        $tb_illegal_play_len = 0;
        $every_tb_media_data = [];
        foreach ($every_tb_media_jczqk as $every_tb_media_jczqk_key => $every_tb_media_jczqk_val){

            if(!$every_tb_media_jczqk_val['titlename']){
                $all_count += $every_tb_media_jczqk_val['fad_count'];
                $all_illegal_count += $every_tb_media_jczqk_val['fad_illegal_count'];
                $all_times += $every_tb_media_jczqk_val['fad_times'];
                $all_illegal_times += $every_tb_media_jczqk_val['fad_illegal_times'];
                $all_play_len += $every_tb_media_jczqk_val['fad_play_len'];
                $all_illegal_play_len += $every_tb_media_jczqk_val['fad_illegal_play_len'];

                $tb_count += $every_tb_media_jczqk_val['fad_count'];
                $tb_illegal_count += $every_tb_media_jczqk_val['fad_illegal_count'];
                $tb_times += $every_tb_media_jczqk_val['fad_times'];
                $tb_illegal_times += $every_tb_media_jczqk_val['fad_illegal_times'];
                $tb_play_len += $every_tb_media_jczqk_val['fad_play_len'];
                $tb_illegal_play_len += $every_tb_media_jczqk_val['fad_illegal_play_len'];
            }else{
                $every_tb_media_jczqk_val['tmedia_name'] = $every_tb_media_jczqk_val['titlename'];
            }
            $every_tb_media_data[] = [
                $every_tb_media_jczqk_val['tmedia_name'],
                $every_tb_media_jczqk_val['fad_count'],
                $every_tb_media_jczqk_val['fad_illegal_count'].($every_tb_media_jczqk_val['prv_fad_illegal_count']?$every_tb_media_jczqk_val['prv_fad_illegal_count']:'(0.00%)'),
                $every_tb_media_jczqk_val['counts_illegal_rate'].'%',
                $every_tb_media_jczqk_val['fad_times'],
                $every_tb_media_jczqk_val['fad_illegal_times'].($every_tb_media_jczqk_val['prv_fad_illegal_times']?$every_tb_media_jczqk_val['prv_fad_illegal_times']:'(0.00%)'),
                $every_tb_media_jczqk_val['times_illegal_rate'].'%',
                $every_tb_media_jczqk_val['fad_play_len'],
                $every_tb_media_jczqk_val['fad_illegal_play_len'].($every_tb_media_jczqk_val['prv_fad_illegal_play_len']?$every_tb_media_jczqk_val['prv_fad_illegal_play_len']:'(0.00%)'),
                $every_tb_media_jczqk_val['lens_illegal_rate'].'%',
            ];

        }

        $tp_count = 0;
        $tp_illegal_count = 0;
        $tp_times = 0;
        $tp_illegal_times = 0;
        $every_tp_media_data = [];
        foreach ($every_tp_media_jczqk as $every_tp_media_jczqk_key => $every_tp_media_jczqk_val){

            if(!$every_tp_media_jczqk_val['titlename']){
                $all_count += $every_tp_media_jczqk_val['fad_count'];
                $all_illegal_count += $every_tp_media_jczqk_val['fad_illegal_count'];
                $all_times += $every_tp_media_jczqk_val['fad_times'];
                $all_illegal_times += $every_tp_media_jczqk_val['fad_illegal_times'];

                $tp_count += $every_tp_media_jczqk_val['fad_count'];
                $tp_illegal_count += $every_tp_media_jczqk_val['fad_illegal_count'];
                $tp_times += $every_tp_media_jczqk_val['fad_times'];
                $tp_illegal_times += $every_tp_media_jczqk_val['fad_illegal_times'];
            }else{
                $every_tp_media_jczqk_val['tmedia_name'] = $every_tp_media_jczqk_val['titlename'];
            }

            $every_tp_media_data[] = [
                $every_tp_media_jczqk_val['tmedia_name'],
                $every_tp_media_jczqk_val['fad_count'],
                $every_tp_media_jczqk_val['fad_illegal_count'].($every_tp_media_jczqk_val['prv_fad_illegal_count']?$every_tp_media_jczqk_val['prv_fad_illegal_count']:'(0.00%)'),
                $every_tp_media_jczqk_val['counts_illegal_rate'].'%',
                $every_tp_media_jczqk_val['fad_times'],
                $every_tp_media_jczqk_val['fad_illegal_times'].($every_tp_media_jczqk_val['prv_fad_illegal_times']?$every_tp_media_jczqk_val['prv_fad_illegal_times']:'(0.00%)'),
                $every_tp_media_jczqk_val['times_illegal_rate'].'%',
            ];

        }//0824

        //定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $data['content'][] = [
            "type"=>"filltable",
            "bookmark"=>"单个图片",
            "data"=>[
                ["index"=>1,"type"=>"image","content"=>'http://hzaic-gg.oss-cn-hangzhou.aliyuncs.com/1270b73d956a5477eeabd7e3dbc52f46.png']
            ]
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号居中正文",
            "text" => $this->time_str($pntype,$s_time)
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号居中",
            "text" => '广告监管专刊（总*期）'
        ];//


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号落款靠右",
            "text" => $user["regulatorpname"].'                               '.$date_end_ymd
        ];

        $data['content'][] = [
            "type"=>"filltable",
            "bookmark"=>"单个图片",
            "data"=>[
                ["index"=>1,"type"=>"image","content"=>'http://hzaic-gg.oss-cn-hangzhou.aliyuncs.com/eab86a320bdb644ca1e990d5d0c0483a.png']
            ]
        ];
        $month_num = strval(substr($month,5,2));//月份

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号居中标题",
            "text" => $this->time_str($pntype,$s_time).'广告监测通报'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体二号居中标题",
            "text" => $area_str.'媒体'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号标题",
            "text" => '一、广告监测概况'
        ];

        $all_illegal_count_rate = round(($all_illegal_count/$all_count)*100,2);
        $all_illegal_times_rate = round(($all_illegal_times/$all_times)*100,2);
        $all_illegal_play_len_rate = round(($all_illegal_play_len/$all_play_len)*100,2);
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号正文",
            "text" => $date_ymd.'至'.$date_md.$user["regionname"].'广告监测中心共抽查监测我省'.$area_str.'媒体'.$ct_media_count.'家，其中报纸类媒体'.$tp_media_count.'家，电视类媒体'.$tv_media_count.'家，广播类媒体'.$tb_media_count.'家。抽查监测'.$area_str.'媒体广告'.$all_count.'条，其中涉嫌违法广告'.$all_illegal_count.'条，条数违法率为'.$all_illegal_count_rate.'%；抽查监测'.$area_str.'媒体广告'.$all_times.'条次，其中涉嫌违法广告'.$all_illegal_times.'条次，条次违法率为'.$all_illegal_times_rate.'%；抽查监测'.$area_str.'电视、广播类媒体广告总时长为'.$all_play_len.'秒，其中涉嫌违法广告时长'.$all_illegal_play_len.'秒，时长违法率为'.$all_illegal_play_len_rate.'%。'
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号标题",
            "text" => '二、'.$area_level.'涉嫌违法广告发布情况'
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号居中",
            "text" => $area_level.'涉嫌违法广告发布情况'
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "地域条数条次时长汇总列表",
            "data" => to_string($every_region_data)
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号标题",
            "text" => '三、各类媒体广告发布与涉嫌违法情况'
        ];//国家局季报汇总列表

        $num_val_str = ['（1）','（2）','（3）'];

        if(count($every_tp_media_data) > 1){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => $num_val_str[0].'报纸类'
            ];
            unset($num_val_str[0]);
            $num_val_str = array_values($num_val_str);
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号正文",
                "text" => $this->time_str($pntype,$s_time).$user["regionname"].'广告监测中心对我省'.$tp_media_count.'家'.$area_str.'级报纸媒体发布的广告进行了抽查监测，共监测各类广告'.$tp_count.'条，其中涉嫌违法广告'.$tp_illegal_count.'条，条数违法率为'.round(($tp_illegal_count/$tp_count)*100,2).'%；监测各类广告'.$tp_times.'条次，其中涉嫌违法广告'.$tp_illegal_times.'条次，条次违法率为'.round(($tp_illegal_times/$tp_times)*100,2).'%。'
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号居中",
                "text" => $area_str.'级报纸涉嫌违法广告发布情况'
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "涉嫌违法广告发布情况不带时长",
                "data" => to_string($every_tp_media_data)
            ];
        }


        if(count($every_tv_media_data) > 1){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => $num_val_str[0].'电视类'
            ];
            unset($num_val_str[0]);
            $num_val_str = array_values($num_val_str);
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号正文",
                "text" => $this->time_str($pntype,$s_time).$user["regionname"].'广告监测中心对我省'.$tv_media_count.'家'.$area_str.'级电视媒体发布的广告进行了抽查监测，共监测各类广告'.$tv_count.'条，其中涉嫌违法广告'.$tv_illegal_count.'条，条数违法率为'.round(($tv_illegal_count/$tv_count)*100,2).'%；监测各类广告'.$tv_times.'条次，其中涉嫌违法广告'.$tv_illegal_times.'条次，条次违法率为'.round(($tv_illegal_times/$tv_times)*100,2).'%。'
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号居中",
                "text" => $area_str.'级电视涉嫌违法广告发布情况'
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "涉嫌违法广告发布情况带时长",
                "data" => to_string($every_tv_media_data)
            ];
        }

        if(count($every_tb_media_data) > 1){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => $num_val_str[0].'广播类'
            ];
            unset($num_val_str[0]);
            $num_val_str = array_values($num_val_str);
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号正文",
                "text" => $this->time_str($pntype,$s_time).$user["regionname"].'广告监测中心对我省'.$tb_media_count.'家'.$area_str.'级广播媒体发布的广告进行了抽查监测，共监测各类广告'.$tb_count.'条，其中涉嫌违法广告'.$tb_illegal_count.'条，条数违法率为'.round(($tb_illegal_count/$tb_count)*100,2).'%；监测各类广告'.$tb_times.'条次，其中涉嫌违法广告'.$tb_illegal_times.'条次，条次违法率为'.round(($tb_illegal_times/$tb_times)*100,2).'%。'
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号居中",
                "text" => $area_str.'级广播涉嫌违法广告发布情况'
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "涉嫌违法广告发布情况带时长",
                "data" => to_string($every_tb_media_data)
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号标题",
            "text" => '三、广告监测动态'
        ];

        $all_illegal_count_rate = round(($all_illegal_count/$all_count)*100,2);
        $all_illegal_times_rate = round(($all_illegal_times/$all_times)*100,2);
        $all_illegal_play_len_rate = round(($all_illegal_play_len/$all_play_len)*100,2);

        $last_illegal_count_rate = round(($last_all_illegal_count/$last_all_count)*100,2);
        $last_illegal_times_rate = round(($last_all_illegal_times/$last_all_times)*100,2);
        $last_illegal_play_len_rate = round(($last_all_illegal_play_len/$last_all_play_len)*100,2);

        $hb_illegal_count_rate = $all_illegal_count_rate - $last_illegal_count_rate;

        if($hb_illegal_count_rate == 0){
            $hb_illegal_count_rate = '无变化';
        }elseif($hb_illegal_count_rate > 0){
            $hb_illegal_count_rate = '上升'.$hb_illegal_count_rate.'个百分点';
        }elseif($hb_illegal_count_rate < 0){
            $hb_illegal_count_rate = '下降'.($hb_illegal_count_rate*-1).'个百分点';
        }

        $hb_illegal_times_rate = $all_illegal_times_rate - $last_illegal_times_rate;

        if($hb_illegal_times_rate == 0){
            $hb_illegal_times_rate = '无变化';
        }elseif($hb_illegal_times_rate > 0){
            $hb_illegal_times_rate = '上升'.$hb_illegal_times_rate.'个百分点';
        }elseif($hb_illegal_times_rate < 0){
            $hb_illegal_times_rate = '下降'.($hb_illegal_times_rate*-1).'个百分点';
        }

        $hb_illegal_play_len_rate = $all_illegal_play_len_rate - $last_illegal_play_len_rate;

        if($hb_illegal_play_len_rate == 0){
            $hb_illegal_play_len_rate = '无变化';
        }elseif($hb_illegal_play_len_rate > 0){
            $hb_illegal_play_len_rate = '上升'.$hb_illegal_play_len_rate.'个百分点';
        }elseif($hb_illegal_play_len_rate < 0){
            $hb_illegal_play_len_rate = '下降'.($hb_illegal_play_len_rate*-1).'个百分点';
        }

        $prv_m = $prv_m;
        if($prv_m == 0){
            $prv_m = '去年12';
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号正文",
            "text" => '从监测情况看，'.$area_str.'媒体'.$this->time_str($pntype,$s_time).'涉嫌违法广告条数违法率'.$all_illegal_count_rate.'%,较'.$prv_time['w'].$hb_illegal_count_rate.'；条次违法率'.$all_illegal_times_rate.'%，较'.$prv_time['w'].$hb_illegal_times_rate.'；时长违法率'.$all_illegal_play_len_rate.'%，较'.$prv_time['w'].$hb_illegal_play_len_rate.'。'
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "落款工商局",
            "text" => $user["regulatorpname"]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "落款时间",
            "text" => $date_end_ymd.'印'
        ];

        $report_data = json_encode($data);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            //将生成记录保存
            $focus = I('focus',0);
            $pnname = $area_str.$this->time_str($pntype,$s_time).'报告';//time_str$user['regulatorname'].date('Y年m月',strtotime($month)).$area_str.'月报';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= $pntype;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']    = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }

    //时间段中文字符串匹配 默认月报 报告类型 20周报、30月报、70季报、75半年报、80年报
    protected function  time_str($pntype,$date){
        $time_str = '';
        $unix_time = strtotime($date);
        switch ($pntype){
            case '20':
                $time_str = date('Y',$unix_time).'年第'.date('W',$unix_time).'周';
                break;
            case '30':
                $time_str = date('Y年m月',$unix_time);
                break;
            case '70':
                $jd = ceil((date('n', $unix_time))/3);
                $time_str = date('Y',$unix_time)."年第".number2chinese($jd)."季度";//当月是第几季度
                break;
            case '75':
                if(date('m', $unix_time) > 5){
                    $nd = "下半年";
                }else{
                    $nd = "上半年";
                }
                $time_str = date('Y',$unix_time)."年".$nd;//上下半年
                break;
            case '80':
                $time_str = date('Y',$unix_time)."年";//当前年份
                break;
        }
        if(!empty($time_str)){
            return $time_str;
        }else{
            return '未知';
        }
    }

    //获取上个周期的时间戳
    protected function  prv_time($pntype,$s_date,$e_date,$timestamp=1)
    {
        $prv_time = [];
        $s_unix_time = strtotime($s_date);
        $e_unix_time = strtotime($e_date);
        switch ($pntype) {
            case '20':
                $prv_time['s'] = $s_unix_time - (86400 * 7);//上周期开始时间戳
                $prv_time['e'] = $e_unix_time - (86400 * 6) - 1;//上周期结束时间戳
                $prv_time['w'] = "上周";//上周期结束时间戳
                break;
            case '30':
                $l_month = date('m',$s_unix_time) - 1;
                $l_year = date('Y',$s_unix_time);
                if($l_month == 0){
                    $l_year = $l_year - 1;
                    $l_month = '12';
                }
                $prv_time['s'] = strtotime($l_year.'-'.$l_month.'-01');//上周期开始时间戳
                $days = date('t',$prv_time['s']);
                $prv_time['e'] = strtotime($l_year.'-'.$l_month.'-'.$days) + 86399;//上周期结束时间戳
                $prv_time['w'] = "上月";//上周期结束时间戳
                break;
            case '70':
                $l_jd = ceil((date('n', $s_unix_time)) / 3) - 1;
                $l_year = date('Y',$s_unix_time);
                if($l_jd == 0){
                    $l_year = $l_year - 1;
                    $l_jd = 4;
                }
                switch ($l_jd){
                    case 1:
                        $prv_time['s'] = strtotime($l_year.'-01-01');//上周期开始时间戳
                        $days = date('t',strtotime($l_year.'-03-01'));
                        $prv_time['e'] = strtotime($l_year.'-03-'.$days) + 86399;//上周期结束时间戳
                        break;
                    case 2:
                        $prv_time['s'] = strtotime($l_year.'-04-01');//上周期开始时间戳
                        $days = date('t',strtotime($l_year.'-06-01'));
                        $prv_time['e'] = strtotime($l_year.'-06-'.$days) + 86399;//上周期结束时间戳
                        break;
                    case 3:
                        $prv_time['s'] = strtotime($l_year.'-07-01');//上周期开始时间戳
                        $days = date('t',strtotime($l_year.'-09-01'));
                        $prv_time['e'] = strtotime($l_year.'-09-'.$days) + 86399;//上周期结束时间戳
                        break;
                    case 4:
                        $prv_time['s'] = strtotime($l_year.'-10-01');//上周期开始时间戳
                        $days = date('t',strtotime($l_year.'-12-01'));
                        $prv_time['e'] = strtotime($l_year.'-12-'.$days) + 86399;//上周期结束时间戳
                        break;
                }
                $prv_time['w'] = "上季度";//上周期结束时间戳
                break;
            case '75':

                $l_year = date('Y',$s_unix_time);

                if(date('m', $s_unix_time) < 5){
                    $l_year = date('Y',$s_unix_time) - 1;
                    $prv_time['s'] = strtotime($l_year.'-07-01');//上周期开始时间戳
                    $prv_time['e'] = strtotime($l_year.'-12-31') + 86399;//上周期结束时间戳
                    $prv_time['w'] = "去年下半年";//上周期结束时间戳
                }else{
                    $prv_time['s'] = strtotime($l_year.'-01-01');//上周期开始时间戳
                    $prv_time['e'] = strtotime($l_year.'-06-30') + 86399;//上周期结束时间戳
                    $prv_time['w'] = "今年上半年";//上周期结束时间戳
                }

                break;
            case '80':
                $l_year = date('Y',$s_unix_time) - 1;
                $prv_time['s'] = strtotime($l_year.'-01-01');//上周期开始时间戳
                $days = date('t',strtotime($l_year.'-12-01'));
                $prv_time['e'] = strtotime($l_year.'-12-'.$days) + 86399;//上周期结束时间戳
                $prv_time['w'] = "去年";//上周期结束时间戳
                break;
        }
        if (!empty($prv_time)) {
            if($timestamp == 0){
                $prv_time['s'] = date("Y-m-d",$prv_time['s']);
                $prv_time['e'] = date("Y-m-d",$prv_time['e']);
            }
            return $prv_time;
        } else {
            $this->ajaxReturn([
                'code' => 1,
                'msg' => '报告类型不存在！'
            ]);
        }
    }

    //查询
    protected function media_class_ad_list($s_date = '',$e_date = '',$fmedia_class = '',$fcustomer = '',$region_id = '',$is_sub = 0){

        if(!empty($e_date) && !empty($e_date)){
            $where .= "AND tbn_illegal_ad_issue.fissue_date BETWEEN '$s_date' AND '$e_date' ";
        }

        if(!empty($fmedia_class)){
            $where .= "AND tbn_illegal_ad.fmedia_class = $fmedia_class ";
        }

        if(!empty($fcustomer)){
            $where .= "AND tbn_illegal_ad.fcustomer = $fcustomer ";
        }

        if(!empty($region_id) && $is_sub == 0){
            $where .= "AND tregion.fid = '$region_id' ";
        }

        if(!empty($region_id) && $is_sub == 1){
            $where .= "AND (tregion.fpid = '$region_id' OR tregion.fid = '$region_id') ";
        }

        if(!empty($region_id) && $is_sub == 2){
            $where .= "AND tregion.fpid = '$region_id' ";
        }

        $data_sql = "SELECT
                    (case when instr(tmedia.fmedianame,'（') > 0 
                    then left(tmedia.fmedianame,instr(tmedia.fmedianame,'（') -1) 
                    else tmedia.fmedianame end) as fmedianame,
                    tbn_illegal_ad.fad_name,
                    tadclass.ffullname,
                    COUNT(1) AS fad_illegal_times
                    FROM
                        `tbn_illegal_ad_issue`
                    INNER JOIN tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid
                    INNER JOIN tregion ON tregion.fid = tbn_illegal_ad.fregion_id
                    INNER JOIN tmedia ON tmedia.fid = tbn_illegal_ad.fmedia_id
                    AND tmedia.fid = tmedia.main_media_id
                    INNER JOIN tadclass ON tadclass.fcode = tbn_illegal_ad.fad_class_code
                    WHERE 1 = 1
                    AND tbn_illegal_ad.fstatus3 NOT IN (30, 40)
                    AND tbn_illegal_ad_issue.fsend_status = 2
                    AND tbn_illegal_ad.fexamine = 10 ".$where." 
                    GROUP BY
                        tbn_illegal_ad.fmedia_id,tbn_illegal_ad.fid";
        $res = M('')->query($data_sql);
        return $res;
    }

    protected function create_ill_list($data = []){
        $data_res = [];
        foreach ($data as $key => $val){
            $data_res[] = [
                $key+1,
                $val['fmedianame'],
                $val['fad_name'],
                $val['ffullname'],
                $val['fad_illegal_times']
            ];
        }
        return $data_res;
    }



}