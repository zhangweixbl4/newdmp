<?php
namespace Open\Controller;
use Think\Controller;

/**
 * 上海数据接口
 */
class ApiController extends BaseController {

	/**
	* 接口接收的JSON参数并解码为对象
	*/
	protected $oParam;

	/**
	 * 构造函数
	 * @Param	Mixed variable
	 * @Return	void 返回的数据
	 */
	public function _initialize(){
		$this->oParam = json_decode(file_get_contents('php://input'));
		$AGP_SIGNJURISDICTION_URL = ['127.0.0.1','115.236.42.130','180.167.93.126','61.129.59.60','61.129.59.59','::1'];//免验证IP
		$userip = $this->getRealIp();
		if(!in_array($userip,$AGP_SIGNJURISDICTION_URL)){
			TokenController::AuthRequestSign((array)$this->oParam);	//验证请求签名
		}
		$logdata['createtime'] = date('Y-m-d H:i:s');
		$logdata['url'] = $_SERVER['SERVER_NAME'].$_SERVER["REQUEST_URI"];
		$logdata['postdata'] = $req;
		$logdata['serverdata']['userid'] = $userid;
		$logdata['serverdata']['timestamp'] = $timestamp;
		A('Token')->write_log($logdata);
	}

	//获取用户IP地址
    public function getRealIp() {
        $ip=false;
        if(!empty($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
            if ($ip) { array_unshift($ips, $ip); $ip = FALSE; }
            for ($i = 0; $i < count($ips); $i++) {
                if (!eregi ("^(10│172.16│192.168).", $ips[$i])) {
                    $ip = $ips[$i];
                    break;
                }
            }
        }
        return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
    }

	/**
	* 构造函数_back
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function _initialize_back(){
		$this->oParam = json_decode(file_get_contents('php://input'));
		TokenController::AuthRequestSign((array)$this->oParam);	//验证请求签名
		$ret = TokenController::AuthToken($this->oParam->token);//验证Token有效期
		if ($ret['code'] != 0){
			$this->ajaxReturn(array('code'=>1,'msg'=>$ret['msg']));
		}
	}

	/**
	 * 模拟get请求
	 * by zw
	 */
	public function http_get($url){

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

		curl_setopt($ch, CURLOPT_URL, $url);
		//参数为1表示传输数据，为0表示直接输出显示。
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//若给定url自动跳转到新的url,有了下面参数可自动获取新url内容：302跳转
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		//设置cURL允许执行的最长秒数。
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		//https请求必须
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//参数为0表示不带头文件，为1表示带头文件
		curl_setopt($ch, CURLOPT_HEADER,0);
		$output = curl_exec($ch);
		curl_close($ch);
		return $output;
	}

	//钉钉提醒
	public function push_task($title,$content,$phone,$token){
		$msgPost = array();
	    $msgPost['msgtype'] = 'markdown';
	    // $msgPost['title'] = $title;
	    // $msgPost['text']['content'] = $content;
	    $msgPost['markdown']['title'] = $title;
	    $msgPost['markdown']['text'] = $content;
	    $msgPost['at']['atMobiles'] = $phone;

		$ret = http('https://oapi.dingtalk.com/robot/send?access_token='.$token,json_encode($msgPost),'POST',array('Content-Type:application/json; charset=utf-8'));
	}

	/**
	* by zw
	* 获取媒体抽查已处理完成清单
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function GetMediaIssue(){
		$userid = $this->oParam->userid;
		$fmediatype = $this->oParam->fmediatype;
		$count = $this->oParam->count?$this->oParam->count:10;
		$fstarttime = $this->oParam->fstarttime;
		$fendtime = $this->oParam->fendtime;

		if(!empty($fmediatype)){
			$where['b.fmediatype'] = $fmediatype;
		}

		if(!empty($fstarttime) && !empty($fendtime)){
			$where['a.fissuedate'] = ['between',[strtotime($fstarttime),strtotime($fendtime)]];
		}

		$where['a.fdstatus'] = 2;
		$where['a.fuserid'] = $userid;
		$where['b.fstatus'] = 1;
		$dataSet = M('tapimedialist')
			->alias('a')
			->field('b.fmediacode as fmediaid,b.fmediatype,b.fmediacodename as fmedianame,from_unixtime(a.fissuedate,"%Y-%m-%d") as fissuedate')
			->where($where)
			->join('tapimedia_map b on a.fmediaid = b.fid')
			->order('fissuedate desc')
			->limit($count)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$dataSet));
	}

	/**
	* by zw
	* 回传设置获取状态
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function SetIssueStatus(){
		$userid = $this->oParam->userid;
		$fmediaid = $this->oParam->fmediaid;
		$fmediatype = $this->oParam->fmediatype;
		$fstatus = $this->oParam->fstatus;
		$fgetcount = $this->oParam->fgetcount;
		$fungetcount = $this->oParam->fungetcount;
		$fungetdata = $this->oParam->fungetdata;
		$fstatus = $fstatus === '' || $fstatus === null ? 1:$fstatus;//数据处理状态
		$fissuedate = $this->oParam->fissuedate;
		if(empty($fissuedate)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'参数错误'));
		}
		if($fstatus != 1 && $fstatus != 0){
			$this->ajaxReturn(array('code'=>1,'msg'=>'状态值有误'));
		}
		if($fstatus == 0){
			$fstatus = 2;
		}else{
			$fstatus = 4;
		}
		$fissuedate = date('Y-m-d',strtotime($fissuedate));

		$where_tm['fuserid'] = $userid;
		$where_tm['fmediacode'] = $fmediaid;
		$where_tm['fmediatype'] = $fmediatype;
		$where_tm['fmaincode'] = 0;
		// $where_tm['fstatus'] = 1;
		$fmediaid = M('tapimedia_map')
			->cache(60,true)
			->field('fid')
			->where($where_tm)
			->getField('fid');
		if(empty($fmediaid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'媒体不存在'));
		}
		$where['fmediaid'] = $fmediaid;
		$where['fissuedate'] = strtotime($fissuedate);
		$where['fuserid'] = $userid;
		$data['fdstatus'] = $fstatus;
		$data['fmodifier'] = 'customer';
		$data['fmodifytime'] = date('Y-m-d H:i:s');
		if(!empty($fgetcount)){
			$data['fgetcount'] = $fgetcount;
		}
		if(!empty($fungetcount)){
			$data['fungetcount'] = $fungetcount;
		}
		if(!empty($fungetdata)){
			$data['fungetdata'] = json_encode($fungetdata);
		}
		$ret = M('tapimedialist')
			->where($where)
			->save($data);
		if ($ret){
			$this->ajaxReturn(array('code'=>0,'msg'=>'状态设置成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'状态设置失败'));
		}
	}

	/**
	* by zw
	* 抽查计划推送
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function SetMediaPlan(){
		$data = $this->oParam->data;
		$userid = $this->oParam->userid;
		$adddata = [];
		$msgdata = [];

		if(empty(count($data))){
			$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败，无数据推送'));
		}
		foreach ($data as $key => $value) {
			$value = (array)$value;
			$data_tt = [];
			$value['fissuedate'] = date('Y-m-d',strtotime($value['fissuedate']));

			//返回的执行结果基本信息
			$msgdata[$key]['fmediaid'] = $value['fmediaid'];
			$msgdata[$key]['fmediatype'] = $value['fmediatype'];
			$msgdata[$key]['fissuedate'] = $value['fissuedate'];

			if($value['fmediaid'] === '' || empty($value['fmediatype']) || empty($value['fissuedate'])){
				$msgdata[$key]['fstatus'] = 1;
				$msgdata[$key]['freason'] = '参数错误';
				continue;
			}

			if($value['ftype'] == 1){
				$where_tm['a.fuserid'] = $userid;
				$where_tm['a.fmediacode'] = $value['fmediaid'];
				$where_tm['a.fmediatype'] = $value['fmediatype'];
				$where_tm['a.fmaincode'] = 0;
				$where_tm['a.fstatus'] = 1;
				$media = M('tapimedia_map')
				    ->alias('a')
					->field('a.fid,a.fmediaid,a.fmediacodename,a.fmediatype,a.fmediacode,b.fmedianame')
					->join('tmedia b on a.fmediaid = b.fid','left')
					->where($where_tm)
					->find();
				if(!empty($media)){
					if(empty($media['fmediaid']) && !empty($media['fmediacode'])){
						$msgdata[$key]['fstatus'] = 1;
						$msgdata[$key]['freason'] = '数据提供方的媒体未作关联';

                       //钉钉提醒
                        $ftixing = S('shrwtx'.$media['fid']);
                        if(empty($ftixing)){
                           S('shrwtx'.$media['fid'],date('Y-m-d H:i:s'),86400);//一天一提醒
                            $smsdata2['errormedianame'][] = $media['fmediacodename'].$media['fmediacode'].$media['fmediaid'];
                            $smsdata2['atuser'][0] = '13735566051';//对负责媒体关联的人作提醒
                        }
						continue;
					}
					$where_tt['fmediaid'] = $media['fid'];
					$where_tt['fissuedate'] = strtotime($value['fissuedate']);
					$where_tt['fuserid'] = $userid;

					$do_tt = M('tapimedialist')
						->field('fdstatus,fid')
						->where($where_tt)
						->find();
					if(empty($do_tt)){
						$data_tt['fmediaid'] = $media['fid'];
						$data_tt['fissuedate'] = strtotime($value['fissuedate']);
						$data_tt['ftask_starttime'] = $data_tt['fissuedate'];
						$data_tt['fdstatus'] = 0;
						$data_tt['fuserid'] = $userid;
						$data_tt['fcreator'] = $userid;
						$data_tt['fcreatetime'] = date('Y-m-d H:i:s');

						$add_tt = M('tapimedialist')->add($data_tt);
						if(!empty($add_tt) && $media['fmediatype'] == 4){
							$push_data['org_id'] = 45;//机构id 上海为45 如有其他地区需要自定义询问我获取此值
							$push_data['month'] = date('Y-m',strtotime($value['fissuedate']));
							$push_data['check_plan'][] = [
								"media_id"=>$media['fmediaid'],
								"media_name"=>$media['fmedianame'],
            					"media_dates"=>[$value['fissuedate']]
							];
							$gourl = 'http://'.C('GongZhiServerUrl').'/api/NetAd/customize_media_plan';
							$ret = json_decode(http($gourl,json_encode($push_data),'POST',false,0),true);
						}
						// if(!empty($add_tt) && $media['fmediatype'] == 4 && !empty($media['fmediacode'])){
						// 	A('Api/ShIssue') -> update_nettask_priority($add_tt);//调整任务优先级
						// }
						$adddata[] = $data_tt;

						//钉钉提醒
						$smsdata1[$media['fid']]['fmediacodename'] = $media['fmediacodename'];
						$smsdata1[$media['fid']]['fmedianame'] = $media['fmedianame']?$media['fmedianame']:$media['fmediacodename'];
						if($media['fmediatype'] == 1){
							$smsdata1[$media['fid']]['fmediatype'] = "电视";
						}elseif($media['fmediatype'] == 2){
							$smsdata1[$media['fid']]['fmediatype'] = "广播";
						}elseif($media['fmediatype'] == 3){
							$smsdata1[$media['fid']]['fmediatype'] = "报纸";
						}elseif($media['fmediatype'] == 4){
							$smsdata1[$media['fid']]['fmediatype'] = "互联网";
						}
						$smsdata1[$media['fid']]['fissuedate'][] = $value['fissuedate'];
						if($media['fmediatype'] == 4){
							$smsdata2['atuser'][2] = '13858090079';//对负责互联网的人作提醒
						}else{
							$smsdata2['atuser'][1] = '13858090079';//对负责传统媒体的人作提醒
						}

						//返回执行结果的状态
						$msgdata[$key]['fstatus'] = 0;
						$msgdata[$key]['freason'] = '任务已提交';
					}else{
						if($do_tt['fdstatus'] <= 4){
							//返回执行结果的状态
							$msgdata[$key]['fstatus'] = 1;
							$msgdata[$key]['freason'] = '任务已存在';
						}else{
							if($do_tt['fdstatus'] == 5){
								M()->execute('update tapimedialist set fdstatus = 0,fmodifier = "'.$userid.'",fmodifytime = "'.date('Y-m-d H:i:s').'" where fid = '.$do_tt['fid']);
							}elseif($do_tt['fdstatus'] == 6){
								M()->execute('update tapimedialist set fdstatus = 1,fmodifier = "'.$userid.'",fmodifytime = "'.date('Y-m-d H:i:s').'" where fid = '.$do_tt['fid']);
							}elseif($do_tt['fdstatus'] == 7){
								A('Api/ShIssue')->gettask_isnew_tad($do_tt['fid'],0);
							}elseif($do_tt['fdstatus'] == 8){
								M()->execute('update tapimedialist set fdstatus = 3,fmodifier = "'.$userid.'",fmodifytime = "'.date('Y-m-d H:i:s').'" where fid = '.$do_tt['fid']);
							}
							//返回执行结果的状态
							$msgdata[$key]['fstatus'] = 0;
							$msgdata[$key]['freason'] = '任务已提交';
						}
					}
				}else{
					//返回执行结果的状态
					$msgdata[$key]['fstatus'] = 1;
					$msgdata[$key]['freason'] = '媒体不存在';
				}

			}elseif($value['ftype'] == -1){//如果删除，更改任务状态
				$where_tt2['fmediaid'] = $value['fmediaid'];
				$where_tt2['fmediatype'] = $value['fmediatype'];
				$where_tt2['fissuedate'] = strtotime($value['fissuedate']);
				$where_tt2['fuserid'] = $userid;

				$data_tt2['fdstatus'] = -1;
				$data_tt2['fmodifier'] = $userid;
				$data_tt2['fmodifytime'] = date('Y-m-d H:i:s');

				$do_tt2 = M('tapimedialist')
					->where($where_tt2)
					->save($data_tt2);

				//返回的执行结果
				if(!empty($do_tt2)){
					$msgdata[$key]['fstatus'] = 0;
					$msgdata[$key]['freason'] = '任务删除成功';
				}else{
					$msgdata[$key]['fstatus'] = 1;
					$msgdata[$key]['freason'] = '无相关媒体的任务';
				}
			}else{
				$msgdata[$key]['fstatus'] = 1;
				$msgdata[$key]['freason'] = '参数错误';
			}
		}

		//指执行任务添加记录
		if(!empty($adddata) || !empty($smsdata2['errormedianame'])){
			foreach ($smsdata1 as $key => $value) {
				$smsdata[] = $value;
				$smsstr .= $value['fmedianame'].'—'.$value['fmediatype']."（".implode("、", $value['fissuedate'])."）\n\n";
			}
			if(!empty($smsdata2['errormedianame'])){
				$smsstr .= implode("、", $smsdata2['errormedianame'])."媒体未作关联\n\n";
			}
			foreach ($smsdata2['atuser'] as $key => $value) {
				if(!empty($value)){
					$smsphone[] = $value;
					$smsstr .= "@".$value."  ";
				}
			}

			$token = '3cc83737c35805d21c51028067c707418c64a8a257b7ed30bccef3ef5b295efc';
			$this->push_task("最新推送任务","#### 最新推送任务  \n\n > 上海工商局：\n\n".$smsstr,$smsphone,$token);

		}

		//返回结果
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$msgdata));

	}

	/**
	* by zw
	* 抽查计划处理状态查询
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function GetMediaPlanstatus(){
		$fstarttime = $this->oParam->fstarttime;
		$fendtime = $this->oParam->fendtime;
		$fmediaid = $this->oParam->fmediaid;
		$fmediatype = $this->oParam->fmediatype;
		$fdstatus = $this->oParam->fdstatus;
		$userid = $this->oParam->userid;
		$fdstatus = $fdstatus === '' || $fdstatus === null?-1:$fdstatus;//数据处理状态
		$fdstatuss = [-1,0,1,2,3,4];
		if(!in_array($fdstatus,$fdstatuss)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败，数据处理状态值有误'));
		}
		if(empty($fstarttime) || empty($fendtime)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'参数错误'));
		}
		$fstarttime = $fstarttime?strtotime($fstarttime):strtotime(date('Y-m-01'));//搜索开始时间
		$fendtime = $fendtime?strtotime($fendtime):strtotime(date('Y-m-d'));//搜索结束时间

		$where_tt['a.fdstatus'] = array('in',array(0,1,2,3,4));
		$where_tt['a.fuserid'] = $userid;
		$where_tt['b.fmaincode'] = 0;
		$where_tt['b.fstatus'] = 1;
		$where_tt['fissuedate'] = array('between',array($fstarttime,$fendtime));
		if(!empty($fmediaid)){
			$where_tt['fmediacode'] = $fmediaid;
		}
		if(!empty($fmediatype)){
			$where_tt['fmediatype'] = $fmediatype;
		}
		if($fdstatus != -1){
			$where_tt['fdstatus'] = $fdstatus;
		}

		$do_tt = M('tapimedialist')
			->alias('a')
			->field('b.fmediacode as fmediaid,b.fmediatype,b.fmediacodename as fmedianame,FROM_UNIXTIME(fissuedate,"%Y-%m-%d") as fissuedate,fdcount,fdfinishcount,fdillegalcount,fdstatus ')
			->join('tapimedia_map b on a.fmediaid = b.fid')
			->where($where_tt)
			->order('a.fmediaid asc,fissuedate asc')
			->select();
		//返回结果
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_tt));
	}

	/**
	* by zw
	* 获取互联网媒体爬取数据原文
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function GetNetADSpiderInfo(){
		session_write_close();
		set_time_limit(30);
		$fid = $this->oParam->fid;
		if(!empty($fid)){
			$do_ne = M('tnetissue')
				->alias('a')
				->field('a.net_id,b.fcontent')
				->join('tnetissue_spiderinfo b on a.net_id = b.fnet_id and fstatus = 1','left')
				->where(['major_key'=>$fid])
				->find();
			if(!empty($do_ne)){
				if(empty($do_ne['fcontent'])){
					$url = 'http://iad.adlife.com.cn/index/getAttribute?id='.$do_ne['net_id'];
					$data = json_decode($this->http_get($url),true);
					if(!empty($data['code'] == 200) && !empty($data['data'])){
						$content = $data['data'];
						$pattern='/url=.*?:{(.*)/i';
		            	preg_match($pattern,$content,$dt);
						$data['data'] = '{'.$dt[1];

						$addData['fnet_id'] = $do_ne['net_id'];
						$addData['fcreate_time'] = date('Y-m-d H:i:s');
						$addData['fcontent'] = json_encode($data);
						$addData['fstatus'] = 1;
						M('tnetissue_spiderinfo')->add($addData);
					}else{
						$addData['fnet_id'] = $do_ne['net_id'];
						$addData['fcreate_time'] = date('Y-m-d H:i:s');
						$addData['fcontent'] = $data?json_encode($data):'';
						$addData['fstatus'] = 0;
						M('tnetissue_spiderinfo')->add($addData);
						$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败，请尝试重新获取'));
					}
				}else{
					$data = json_decode($do_ne['fcontent']);
				}
				$this->ajaxReturn($data);
				
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败，无效值'));
			}
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败，参数错误'));
		}
	}

	/**
	* by zw
	* 获取互联网水印广告图片
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function GetNetADSnapshutPIC(){
		$pid = $this->oParam->pid;
		$x = $this->oParam->x;
		$y = $this->oParam->y;
		$alpha = $this->oParam->alpha;
		if(!empty($pid)){
			$url = 'http://api2.iad.adlife.com.cn:8085/getsnapshutpic?pid='.$pid.'&x='.$x.'&y='.$y.'&alpha='.$alpha;
			header("Location: ".$url);
		}
	}

	/**
	* by zw
	* 获取媒体对应信息
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function GetMediaMap(){
		$data = $this->oParam->data;
		$userid = $this->oParam->userid;
		$msgdata = [];
		if(!empty($data)){
			foreach ($data as $key => $value) {
				$value = (array)$value;
				if($value['fmediaid'] === '' || empty($value['fmediatype'])){
					$this->ajaxReturn(array('code'=>1,'msg'=>'参数错误'));
				}

				//返回结果
				$msgdata[$key]['fmediaid'] = $value['fmediaid'];
				$msgdata[$key]['fmediatype'] = $value['fmediatype'];

				//查询媒体数据是否存在
				$where_tp['a.fuserid'] = $userid;
				$where_tp['a.fmediatype'] = $value['fmediatype'];
				$where_tp['a.fmediacode'] = $value['fmediaid'];
				$where_tp['a.fmaincode'] = 0;
				$where_tp['a.fstatus'] = 1;
				$do_tp = M('tapimedia_map')
					->field('a.fid,a.fmediacodename,b.fmedianame as fmedianame_hz,b.fid as fmediacode_hz')
					->alias('a')
					->join('tmedia b on a.fmediaid = b.fid','left')
					->where($where_tp)
					->find();
				//根据查询结果返回不同数据
				if(empty($do_tp)){
					$msgdata[$key]['code'] = 1;
					$msgdata[$key]['msg'] = '媒体不存在';
				}else{
					$msgdata[$key]['fid'] = $do_tp['fid'];
					$msgdata[$key]['code'] = 0;
					if(empty($do_tp['fmedianame_hz'])){
						$msgdata[$key]['fmedianame'] = $do_tp['fmediacodename'];
						if($value['fmediaid'] == 0){
							$msgdata[$key]['msg'] = '获取成功';
						}else{
							$msgdata[$key]['msg'] = '媒体未审核';
						}

					}else{
						$msgdata[$key]['fmedianame'] = $do_tp['fmediacodename'];
						$msgdata[$key]['fmediacode_hz'] = $do_tp['fmediacode_hz'];
						$msgdata[$key]['fmedianame_hz'] = $do_tp['fmedianame_hz'];
						$msgdata[$key]['msg'] = '获取成功';
					}
				}
			}
		}else{
			//查询所有媒体数据
			$where_tp['a.fuserid'] = $userid;
			$where_tp['a.fmaincode'] = 0;
			$where_tp['a.fstatus'] = 1;
			$do_tp = M('tapimedia_map')
				->field('a.fid,a.fmediacode as fmediaid,a.fmediatype,a.fmediacodename,b.fmedianame as fmedianame_hz,b.fmediacode as fmediacode_hz')
				->alias('a')
				->join('tmedia b on a.fmediaid = b.fid','left')
				->where($where_tp)
				->select();
			foreach ($do_tp as $key => $value) {
				$msgdata[$key]['fid'] = $value['fid'];
				$msgdata[$key]['code'] = 0;
				$msgdata[$key]['fmediaid'] = $value['fmediaid'];
				$msgdata[$key]['fmediatype'] = $value['fmediatype'];
				if(empty($value['fmedianame_hz'])){
					$msgdata[$key]['fmedianame'] = $value['fmediacodename'];
					$msgdata[$key]['msg'] = '媒体未审核';
				}else{
					$msgdata[$key]['fmedianame'] = $value['fmediacodename'];
					$msgdata[$key]['fmediacode_hz'] = $value['fmediacode_hz'];
					$msgdata[$key]['fmedianame_hz'] = $value['fmedianame_hz'];
					$msgdata[$key]['msg'] = '获取成功';
				}
			}
		}


		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$msgdata));
	}

	/**
	* by zw
	* 更新媒体对应信息，创建或删除已注释
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function SetMediaMap(){
		$data = $this->oParam->data;
		$userid = $this->oParam->userid;
		$msgdata = [];
		foreach ($data as $key => $value) {
			$value = (array)$value;
			// if($value['fid'] == 0){//为0是添加数据
			// 	//返回结果
			// 	$msgdata[$key]['fmediaid'] = $value['fmediaid'];
			// 	$msgdata[$key]['fmediatype'] = $value['fmediatype'];
			// 	$msgdata[$key]['fmedianame'] = $value['fmedianame'];
			// 	if(empty($value['fmediaid']) || empty($value['fmediatype']) || empty($value['fmedianame'])){
			// 		$msgdata[$key]['code'] = 1;
			// 		$msgdata[$key]['msg'] = '创建失败，参数错误';
			// 	}else{
			// 		//查询媒体数据
			// 		$where_tp = [];
			// 		$where_tp['fuserid'] = $userid;
			// 		$where_tp['fmediatype'] = $value['fmediatype'];
			// 		$where_tp['fmediacode'] = $value['fmediaid'];
			// 		$where_tp['fmaincode'] = 0;
			// 		$where_tp['fstatus'] = 1;
			// 		$do_tp = M('tapimedia_map')
			// 			->field('fmediacodename')
			// 			->where($where_tp)
			// 			->find();
			// 		if(empty($do_tp)){
			// 			$data_tp = [];
			// 			$data_tp['fuserid'] = $userid;
			// 			$data_tp['fmediaid'] = 0;
			// 			$data_tp['fmediatype'] = $value['fmediatype'];
			// 			$data_tp['fmediacode'] = $value['fmediaid'];
			// 			$data_tp['fmediacodename'] = $value['fmedianame'];
			// 			$data_tp['fcreator'] = $userid;
			// 			$data_tp['fcreatetime'] = date('Y-m-d H:i:s');

			// 			$msgdata[$key]['code'] = 0;
			// 			$msgdata[$key]['msg'] = '创建成功';
			// 			$msgdata[$key]['fid'] = M('tapimedia_map')->add($data_tp);
			// 		}else{
			// 			$msgdata[$key]['code'] = 1;
			// 			$msgdata[$key]['msg'] = '创建失败，已存在相同编号媒体';
			// 		}
			// 	}
			// }else{
				//返回结果
				$msgdata[$key]['fid'] = $value['fid'];

				// if(empty($value['fmediaid']) && empty($value['fmediatype']) && empty($value['fmedianame'])){//都为空时删除数据
				// 	//查询媒体数据
				// 	$where_tp = [];
				// 	$where_tp['fid'] = $value['fid'];
				// 	$where_tp['fuserid'] = $userid;
				// 	$where_tp['fmaincode'] = 0;
				// 	$where_tp['fstatus'] = 1;
				// 	$do_tp = M('tapimedia_map')
				// 		->field('fmediacode as fmediaid,fmediacodename,fmediatype')
				// 		->where($where_tp)
				// 		->find();
				// 	if(!empty($do_tp)){
				// 		M('tapimedia_map')->where($where_tp)->delete();
				// 		$msgdata[$key]['code'] = 0;
				// 		$msgdata[$key]['msg'] = '删除成功';
				// 		$msgdata[$key]['fid'] = $value['fid'];
				// 		$msgdata[$key]['fmediaid'] = $do_tp['fmediaid'];
				// 		$msgdata[$key]['fmedianame'] = $do_tp['fmediacodename'];
				// 		$msgdata[$key]['fmediatype'] = $do_tp['fmediatype'];
				// 	}else{
				// 		$msgdata[$key]['code'] = 1;
				// 		$msgdata[$key]['msg'] = '删除失败，数据不存在';
				// 	}
				// }else{
					//查询媒体数据
					$where_tp = [];
					$where_tp['fid'] = $value['fid'];
					$where_tp['fuserid'] = $userid;
					$where_tp['fmaincode'] = 0;
					$where_tp['fstatus'] = 1;
					$do_tp = M('tapimedia_map')
						->field('fmediacode as fmediaid,fmediacodename,fmediatype')
						->where($where_tp)
						->find();
					if(empty($do_tp)){
						$msgdata[$key]['code'] = 1;
						$msgdata[$key]['msg'] = '修改失败，数据不存在';
					}else{
						$msgdata[$key]['fmediaid'] = $do_tp['fmediaid'];
						$msgdata[$key]['fmediatype'] = $do_tp['fmediatype'];
						$msgdata[$key]['fmedianame'] = $do_tp['fmediacodename'];

						$data_tp = [];
						if(!empty($value['fmediatype'])){
							$data_tp['fmediatype'] = $value['fmediatype'];
							$msgdata[$key]['fmediatype'] = $value['fmediatype'];
						}
						if(!empty($value['fmediaid'])){
							$data_tp['fmediacode'] = $value['fmediaid'];
							$msgdata[$key]['fmediaid'] = $value['fmediaid'];
						}
						if(!empty($value['fmedianame'])){
							$data_tp['fmedianame'] = $value['fmedianame'];
							$msgdata[$key]['fmedianame'] = $value['fmedianame'];
						}
						$data_tp['fmodifier'] = $userid;
						$data_tp['fmodifytime'] = date('Y-m-d H:i:s');

						$do_tp = M('tapimedia_map')->where($where_tp)->save($data_tp);
						if(!empty($do_tp)){
							$msgdata[$key]['code'] = 0;
							$msgdata[$key]['msg'] = '修改成功';
						}else{
							$msgdata[$key]['code'] = 1;
							$msgdata[$key]['msg'] = '修改失败，提交信息与原信息相同';
						}
					}
				// }

			// }

		}

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$msgdata));
	}

	/**
	* 获取监播数据
	* @Param	Mixed variable
	* @Return	$data 返回的数据
	*/
	public function GetPlayList(){
		session_write_close();
		ini_set('memory_limit','3000M');
        set_time_limit(0);
		$fmediaid = $this->oParam->fmediaid;
		$fmediatype = $this->oParam->fmediatype;
		$issuedate = $this->oParam->issuedate;
		$userid = $this->oParam->userid;
		if(empty($fmediatype) || $fmediaid === '' ||empty($issuedate)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'参数错误'));
		}
		$issuedate = date('Y-m-d',strtotime($issuedate));
		$fissuedatest = strtotime($issuedate);

		//获取主媒体数据
		$where_tp['a.fmediacode'] = $fmediaid;
		$where_tp['a.fmediatype'] = $fmediatype;
		$where_tp['a.fuserid'] = $userid;
		$where_tp['a.fmaincode'] = 0;
		$where_tp['a.fstatus'] = 1;
		$media = M('tapimedia_map')
			->alias('a')
			->field('b.fid, ifnull(b.fmediaclassid,13) fmediaclassid, b.fmediaownerid, b.fmedianame,a.fmediacodename,b.media_region_id AS fregionid,a.fmediacode,a.fid aid')
			->join('tmedia b on a.fmediaid = b.fid','left')
			->where($where_tp)
			->find();
		if(empty($media)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败，媒体不存在'));
		}else{
			if(empty($media['fid']) && !empty($media['fmediacode'])){
				$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败，媒体未作关联'));
			}
		}

		//判断任务是否完成
		$do_task = M('tapimedialist') -> where(['fmediaid' => $media['aid'],'fissuedate' => $fissuedatest,'fdstatus' => ['in',[2,4]]]) ->getField('fid');
		if(empty($do_task)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'任务未完成，无法获取数据'));
		}

		//获取子媒体
		$where_ctp['fmediacode'] = $fmediaid;
		$where_ctp['fmediatype'] = $fmediatype;
		$where_ctp['fuserid'] = $userid;
		$where_ctp['fmaincode'] = $fmediaid;
		$where_ctp['fstatus'] = 1;
		$childmedia = M('tapimedia_map')
			->field('fmediaid')
			->where($where_ctp)
			->select();
		$allmedia = [];
		$allmedia[] = $media['fid'];
		foreach ($childmedia as $key => $value) {
			$allmedia[] = $value['fmediaid'];
		}

		$mediaclass = substr($media['fmediaclassid'], 0, 2);
		if($mediaclass == '13'){
			//AD_TYPE为空或者小于4位、百度推广、CREATE_DATE不在当月范围内的数据不会被接收
			$tb_issue = 'tnetissue_customer';

			$where_str = '1=1';
			if($media['fmediacode'] != 0){
				$where_str .= ' and a.publisher_id in ('.implode(',',$allmedia).')';
			}
			$where_str .= ' and a.fstatus = 6';
			$where_str .= ' and a.ftaskid = '.$do_task;
		}else{
			$tb_issue = 'sh_issue';//发布表
			if($mediaclass == '01'){
	        	$tb_sam = 'ttvsample';
			}elseif($mediaclass == '02'){
	        	$tb_sam = 'tbcsample';
			}elseif($mediaclass == '03'){
				$tb_sam = 'tpapersample';
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败，媒体类型不支持数据获取'));
			}
			$where_str = $tb_issue.".fregionid like '31%' ";
			$where_str .= ' and '.$tb_issue.'.fmediaid in ('.implode(',',$allmedia).')';//媒体ID
			$where_str .= ' and '.$tb_issue.'.fissuedate="'.$issuedate.'"';
			$where_str .= ' and '.$tb_issue.'.fadclasscode not in("2302")';
			$where_str .= ' and '.$tb_issue.'.fadname not like "%本媒体宣传%"';
		}

		if($mediaclass=='01' || $mediaclass=='02'){
			A('Api/ShIssue')->gettask_isnew_tad($do_task,1);//数据是否最新广告状态更新
			$fields = $tb_issue.'.fid as fissue_id,
				'.$tb_issue.'.fissuedate,
				'.$tb_issue.'.fadname,
				'.$tb_issue.'.fbrand,
				'.$tb_issue.'.fadclasscode,
				'.$tb_issue.'.fadownername as fadowner,
				'.$tb_issue.'.fcuturl,
				'.$tb_issue.'.fis_new,
				'.$tb_issue.'.fisillegal,
				'.$tb_issue.'.fcutstatus,
				FROM_UNIXTIME('.$tb_issue.'.fstarttime,"%Y-%m-%d %H:%i:%s") as fstarttime,
				FROM_UNIXTIME('.$tb_issue.'.fendtime,"%Y-%m-%d %H:%i:%s") as fendtime,
				'.$tb_issue.'.flength,
				'.$tb_issue.'.sam_source_path as favifilename,
				'.$tb_sam.'.fid as fsampleid,
				'.$tb_sam.'.fversion,
				'.$tb_sam.'.fspokesman,
				'.$tb_sam.'.fadlen,
				ifnull('.$tb_sam.'.uuid,'.$tb_issue.'.fuuid) uuid';

			$datasql = 'select '.$fields.'
				from '.$tb_issue.'
				left join '.$tb_sam.' on '.$tb_issue.'.fsampleid='.$tb_sam.'.fid
				where '.$where_str.'
				ORDER BY '.$tb_issue.'.fstarttime asc,'.$tb_issue.'.fid asc';
	    }elseif($mediaclass=='03'){
	    	$fields = $tb_issue.'.fid as fissue_id,
				'.$tb_issue.'.fissuedate,
				'.$tb_issue.'.fadname,
				'.$tb_issue.'.fbrand,
				'.$tb_issue.'.fis_new,
				'.$tb_issue.'.fisillegal,
				'.$tb_issue.'.fcutstatus,
				'.$tb_issue.'.fadclasscode,
				'.$tb_issue.'.fadownername as fadowner,
				'.$tb_issue.'.sam_source_path as fjpgfilename,
				'.$tb_issue.'.fpage,
				'.$tb_issue.'.fsize,
				'.$tb_sam.'.fid as fsampleid,
				'.$tb_sam.'.fversion,
				'.$tb_sam.'.fspokesman';

			$datasql = 'select '.$fields.'
				from '.$tb_issue.'
				left join '.$tb_sam.' on '.$tb_issue.'.fsampleid='.$tb_sam.'.fpapersampleid
				where '.$where_str.'
				ORDER BY '.$tb_issue.'.fid asc';
	    }elseif($mediaclass == '13'){
	    	$fields = '
	    		a.adillcontent adillegal,
	    		a.adilllaw,
	    		a.adilltype,
	    		ads_classify,
	    		a.adtype,
	    		advertiser,
	    		a.advertiser_area,
	    		a.advertiser_na,
	    		a.advertiser_id,
	    		approval_num,
	    		approval_unit,
	    		attribute01,
	    		attribute02,
	    		attribute03,
	    		a.attribute04,
	    		attribute05,
	    		attribute06,
	    		attribute07,
	    		audit,
	    		body_height,
	    		body_width,
	    		a.brand,
	    		cookie,
	    		(unix_timestamp(a.created_date)*1000) created_date,
	    		depth,
	    		domain,
	    		fcode,
	    		a.file_url,
	    		fright,
	    		a.height,
	    		a.id,
	    		iframes,
	    		illegal_clause,
	    		illegal_content,
	    		ip,
	    		keywords,
	    		last_seen,
	    		machine_ip,
	    		machine_location,
	    		manufacturer,
	    		material,
	    		md5,
	    		metas,
	    		modified_date,
	    		a.modifytime,
	    		a.monitor,
	    		old_file_url,
	    		old_page_url,
	    		a.original_url,
	    		parent,
	    		a.platform,
	    		production_license_num,
	    		publisher,
	    		a.publisher_area,
	    		a.publisher_entname,
	    		a.publisher_host,
	    		a.publisher_id,
	    		a.publisher_na,
	    		region,
	    		screen,
	    		screen_height,
	    		screen_width,
	    		session_id,
	    		a.shape,
	    		share_url,
	    		shl,
	    		shsj,
	    		a.shutpage,
	    		a.shutpage_thumb shutpage_jpg,
	    		a.shutpage_thumb_size shutpage_jpg_size,
	    		a.shutpage_thumb,
	    		a.shutpage_thumb_size,
	    		a.shutpagesize,
	    		a.shutpage_content,
	    		shyj,
	    		a.size,
	    		snapshot,
	    		spokesman,
	    		status,
	    		subject_md5,
	    		tags,
	    		a.target_url,
	    		thumb_height,
	    		thumb_url,
	    		a.thumb_url_true,
	    		thumb_width,
	    		thumbnail,
	    		a.title,
	    		a.tracker_area,
	    		a.trackers_info,
	    		a.type,
	    		url,
	    		url_md5,
	    		views,
	    		volume,
	    		a.width,
	    		a.left x,
	    		a.right y
	    	';

	    	$datasql = 'select '.$fields.'
				from '.$tb_issue.' a
				where '.$where_str.'
				ORDER BY a.id desc';
	    }
	    $sqldata = M()->master(true)->query($datasql);
	    $data = [];
	    foreach ($sqldata as $key => $row) {
	    	$adddata = [];
			if($mediaclass == '13'){
				$adddata = $row;
				//广告联盟数据处理
				$trackers_info = [];
				try{
					$trackers_info1 = json_decode($row['trackers_info'],true);
					if(isset($trackers_info1[0])){
						$trackers_info = $trackers_info1;
					}else{
						if(!empty($trackers_info1)){
							$trackers_info[] = $trackers_info1;
						}else{
							$trackers_info = [];
						}
					}
		        }catch (Exception $error){
		        	$trackers_info = [];
		        }
	        	$adddata['trackers_info'] = $trackers_info;
				$fadname = str_replace('&',' ',$row['title']);
				if((int)substr($row['adtype'], 0,4) ==2202){
					$pattern='/.*（(.*?)）.*/i';
					$fadname = str_replace('(','（',$fadname);
					$fadname = str_replace(')','）',$fadname);
			        preg_match_all($pattern,$fadname,$fadname_arr);
					if(empty($fadname_arr[1][0])){
			        	$adddata['title'] = $fadname;
			        }else{
						$adddata['title'] = $fadname_arr[1][0];
			        }
				}else{
					$adddata['title']	= $fadname;
				}

				if((int)substr($row['adtype'], 0,4) >=2302 && (int)substr($row['adtype'], 0,4) <=2399){
                    $adddata['adtype'] = 2301;
                }elseif((int)substr($row['adtype'], 0,4) >=1411 && (int)substr($row['adtype'], 0,4) <=1423){
                	if((int)substr($row['adtype'], 0,4) ==1413 || (int)substr($row['adtype'], 0,4) ==1415){
                		$adddata['adtype'] = 1410;
                	}else{
	                    $adddata['adtype'] = 1409;
                	}
                }else{
                    $adddata['adtype'] = $row['adtype'];
                }
				$data[] = $adddata;
			}else{
				$adddata['fissue_id']        = $row['fissue_id'];
				$adddata['fmediaid']        = $fmediaid;
				$adddata['fmediatype']      = $fmediatype;
				$adddata['fmedia_name']      = $media['fmediacodename'];
				$adddata['fissue_date']      = $row['fissuedate'];
				$fadname = str_replace('&',' ',$row['fadname']);
				if((int)substr($row['fadclasscode'], 0,4) ==2202){
					$pattern='/.*（(.*?)）.*/i';
					$fadname = str_replace('(','（',$fadname);
					$fadname = str_replace(')','）',$fadname);
			        preg_match_all($pattern,$fadname,$fadname_arr);
			        if(empty($fadname_arr[1][0])){
			        	$adddata['fad_name'] = $fadname;
			        }else{
						$adddata['fad_name'] = $fadname_arr[1][0];
			        }
				}else{
					$adddata['fad_name']	= $fadname;
				}
				$adddata['fbrand']           = $row['fbrand'];
				//将广告类别转换后提供
				if((int)substr($row['fadclasscode'], 0,4) >=2302 && (int)substr($row['fadclasscode'], 0,4) <=2399){
                    $adddata['fad_class_code'] = 2301;
                }elseif((int)substr($row['fadclasscode'], 0,4) >=1411 && (int)substr($row['fadclasscode'], 0,4) <=1423){
                	if((int)substr($row['fadclasscode'], 0,4) ==1413 || (int)substr($row['fadclasscode'], 0,4) ==1415){
                		$adddata['fad_class_code'] = 1410;
                	}else{
	                    $adddata['fad_class_code'] = 1409;
                	}
                }else{
                    $adddata['fad_class_code'] = $row['fadclasscode'];
                }
				$adddata['fad_owner']        = $row['fadowner'];
				$adddata['fsample_id']       = $row['fsampleid'];
				$adddata['fspokeman']        = $row['fspokesman'];
				$adddata['fversion']     	= $row['fversion'];
				if($mediaclass == '01' || $mediaclass == '02'){//电视、广播
					if($row['fstarttime']>=$row['fendtime']){//不接收开始时间大于结束时间的错误数据
						continue;
					}
					$adddata['fis_new'] 		= $row['fis_new'];
					$adddata['fis_illegal']  	= $row['fisillegal'];
					$adddata['fstart_time']  	= $row['fstarttime'];
					$adddata['fend_time']   	= $row['fendtime'];
					$adddata['fsample_len']      = $row['fadlen'];
					$adddata['flength']          = $row['flength'];
					$adddata['favi_filename']    = $row['fcuturl'];
					$adddata['fsample_uuid']     = $row['uuid'];
					if($row['fcutstatus'] != 1){//不提供剪辑失败的数据
						if(isset($data[count($data)-1])){//判断是否第一条数据
							$sttime1 = strtotime($data[count($data)-1]['fstart_time']);
							$endtime1 = strtotime($data[count($data)-1]['fend_time']);
							$sttime2 = strtotime($adddata['fstart_time']);
							$endtime2 = strtotime($adddata['fend_time']);
							if(check_doubletime($sttime1,$endtime1,$sttime2,$endtime2)){//判断时间前一个时间段和现在这个时间段时否交叉
								// if(($endtime1-$sttime1)<($endtime2-$sttime2)){//交叉时使用时长最大的广告信息
								// 	$data[count($data)-1] = $adddata;
								// }
								$cuttask_status = S('push_question_'.$row['fissue_id']);
		                        if(empty($cuttask_status)){
		                            S('push_question_'.$row['fissue_id'],date('Y-m-d H:i:s'),14400);//两小时内不作提醒
									// 钉钉通知
					                $title = '上海传统媒体计划提醒';
					                $content = "> 友情提示：\n\n上海工商局传统媒体计划数据因播出时段有交叉情况，导致数据推送失败，请检查原因。\n\n媒体名称：".$media['fmedianame']."（".$media['fid']."）"."\n\n计划日期：".$row['fissuedate']."\n\n记录ID：".$row['fissue_id']."\n\n开始时间：".$row['fstarttime']."\n\n结束时间：".$row['fendtime']."\n\n";
					                $smsphone = [13758156171,13735566051];//提醒人员，赵国琪、张伟
					                $token = '6299ea2a7184853c83972cf1df3ae1a838d23d18e1ea207cd168c6b3b92fd8cc';
					                push_ddtask($title,$content,$smsphone,$token,'markdown');
					            }
							}else{
								$data[] = $adddata;
							}
						}else{
							$data[] = $adddata;
						}
					}else{
						$cuttask_status = S('push_question_'.$row['fissue_id']);
                        if(empty($cuttask_status)){
                            S('push_question_'.$row['fissue_id'],date('Y-m-d H:i:s'),14400);//两小时内不作提醒
							// 钉钉通知
			                $title = '上海传统媒体计划提醒';
			                $content = "> 友情提示：\n\n上海工商局传统媒体计划数据因剪辑失败，导致数据推送失败，请检查原因。\n\n媒体名称：".$media['fmedianame']."（".$media['fid']."）"."\n\n计划日期：".$row['fissuedate']."\n\n记录ID：".$row['fissue_id']."\n\n";
			                $smsphone = [13758156171,13735566051,13588258695];//提醒人员，赵国琪、张伟、曹勇
			                $token = '6299ea2a7184853c83972cf1df3ae1a838d23d18e1ea207cd168c6b3b92fd8cc';
			                push_ddtask($title,$content,$smsphone,$token,'markdown');
			            }
					}
				}elseif($mediaclass == '03'){//报纸
					$adddata['fpage']      	= $row['fpage'];
					$adddata['fsize']        = $row['fsize'];
					$adddata['fjpgfilename'] = $row['fjpgfilename'];
					$data[] = $adddata;
				}
			}
	    }

	    if(count($sqldata)!=count($data) && ($mediaclass == '01' || $mediaclass == '02')){
	    	$cuttask_status = S('push_question');
            if(empty($cuttask_status)){
                S('push_question',date('Y-m-d H:i:s'),14400);//两小时内不作提醒
				// 钉钉通知
                $title = '上海传统媒体计划提醒';
                $content = "> 友情提示：\n\n上海工商局传统媒体计划数据因生产数据与所提供的数据量不一致，导致数据推送失败，请检查原因。\n\n媒体名称：".$media['fmedianame']."（".$media['fid']."）"."\n\n计划日期：".$issuedate."\n\n";
                $smsphone = [13758156171,13735566051];//提醒人员，赵国琪、张伟
                $token = '6299ea2a7184853c83972cf1df3ae1a838d23d18e1ea207cd168c6b3b92fd8cc';
                push_ddtask($title,$content,$smsphone,$token,'markdown');
            }
	    }

	    // //导出数据
    	// $outdata['title'] = '监播数据';//文档内部标题名称
     //    $outdata['datalie'] = [
     //      'fad_name'=>'fad_name',
     //      'fsample_uuid'=>'fsample_uuid',
     //    ];
    	// $outdata['lists'] = $data;
     //    $ret = A('Agp/Goutxls')->outdata_xls($outdata);
     //    $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
	    if(empty($cuttask_status)){
	    	if(empty(count($data))){
				// 钉钉通知
                $title = '上海传统媒体计划提醒';
                $content = "> 友情提示：\n\n提供数据量为0，请检查确认。\n\n媒体名称：".$media['fmedianame']."（".$media['fid']."）"."\n\n计划日期：".$issuedate."\n\n";
                $smsphone = [13758156171,13735566051];//提醒人员，赵国琪、张伟
                $token = '6299ea2a7184853c83972cf1df3ae1a838d23d18e1ea207cd168c6b3b92fd8cc';
                push_ddtask($title,$content,$smsphone,$token,'markdown');
            }

		    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	    }else{
	    	$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败，数据未就绪，工作人员正在处理中，请稍候'));
	    }

	}

	/**
	* by zw
	* 广告截取任务推送
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function SetAdCutTask(){
		$data = $this->oParam->data;
		$userid = $this->oParam->userid;
		$adddata = [];
		$msgdata = [];
		foreach ($data as $key => $value) {
			$value = (array)$value;
			$value['fissuedate'] = date('Y-m-d',strtotime($value['fissuedate']));
			//返回的执行结果基本信息
			$msgdata[$key]['fmediaid'] = $value['fmediaid'];
			$msgdata[$key]['fmediatype'] = $value['fmediatype'];
			$msgdata[$key]['fissuedate'] = $value['fissuedate'];
			$msgdata[$key]['fuuid'] = $value['fuuid'];

			$where_tp['fuserid'] = $userid;
			$where_tp['fmediacode'] = $value['fmediaid'];
			$where_tp['fmediatype'] = $value['fmediatype'];
			$where_tp['fmaincode'] = 0;
			$where_tp['fstatus'] = 1;
			$media = M('tapimedia_map')
				->field('fmediaid,fmediatype')
				->where($where_tp)
				->find();
			if(!empty($media)){
				$where_tt['fuser_id'] = $userid;
				$where_tt['fmedia_id'] = $media['fmediaid'];
				$where_tt['fissue_date'] = $value['fissuedate'];
				$where_tt['fuuid'] = $value['fuuid'];
				$do_tt = M('tad_cut')
					->field('fid')
					->where($where_tt)
					->find();
				if(empty($do_tt)){
					$do_sie = [];
					if($media['fmediatype'] == 1){
						$sample = 'ttvsample';
					}elseif($media['fmediatype'] == 2){
						$sample = 'tbcsample';
					}
					$where_sie['a.fmediaid'] = $media['fmediaid'];
					$where_sie['a.fissuedate'] = array('elt',$value['fissuedate']);
					$where_sie['b.uuid'] = $value['fuuid'];
					$do_sie = M('sh_issue')
						->field('a.fstarttime,a.fendtime,a.fsampleid,(case when from_unixtime(a.fstarttime,"%H:%i:%s") >= "18:00:00" and from_unixtime(a.fstarttime,"%H:%i:%s") <= "21:00:00" then 1 else 0 end) as timeorder')
						->alias('a')
						->join($sample.' b on a.fsampleid = b.fid')
						->where($where_sie)
						->order('timeorder desc,a.fissuedate desc')
						->find();
					if(empty($do_sie)){
						$msgdata[$key]['fstatus'] = 1;
						$msgdata[$key]['freason'] = '样本不存在';
						continue;
					}
					$data_td['fuser_id'] = $userid;
					$data_td['fmedia_id'] = $media['fmediaid'];
					$data_td['fissue_date'] = $value['fissuedate'];
					$data_td['fuuid'] = $value['fuuid'];
					$data_td['fsampleid'] = $do_sie['fsampleid'];
					$data_td['fstart_time'] = date('Y-m-d H:i:s',$do_sie['fstarttime']);
					$data_td['fend_time'] = date('Y-m-d H:i:s',$do_sie['fendtime']);
					$data_td['fmedia_class_id'] = $media['fmediatype'];
					$data_td['fcreate_time'] = date('Y-m-d H:i:s');
					$data_td['fcreator'] = $userid;
					$data_td['fstatus'] = 0;

					$adddata[] = $data_td;
					$msgdata[$key]['fstatus'] = 0;
					$msgdata[$key]['freason'] = '任务已提交';
				}else{
					$msgdata[$key]['fstatus'] = 1;
					$msgdata[$key]['freason'] = '任务已存在';
				}
			}else{
				$msgdata[$key]['fstatus'] = 1;
				$msgdata[$key]['freason'] = '媒体不存在';
			}
		}

		//编辑任务添加记录
		if(!empty($adddata)){
			M('tad_cut')->addAll($adddata);
		}

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$msgdata));
	}

	/**
	* by zw
	* 获取广告截取结果清单
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function GetAdCutTaskList(){
		$userid = $this->oParam->userid;
		$count = $this->oParam->count?$this->oParam->count:10;

		$where['a.fstatus'] = 2;
		$where['a.fuser_id'] = $userid;
		$where['b.fmaincode'] = 0;
		$where['b.fstatus'] = 1;
		$dataSet = M('tad_cut')
			->alias('a')
			->field('b.fmediacode as fmediaid,b.fmediatype,a.fissue_date as fissuedate,a.fuuid,a.furl as favifilename')
			->where($where)
			->join('tapimedia_map b on a.fmedia_id = b.fmediaid')
			->order('fissuedate ASC')
			->limit($count)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$dataSet));
	}

	/**
	* by zw
	* 反馈广告截取结果状态（下载成功后调用）
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function SetAdCutTaskStatus(){
		$data = $this->oParam->data;
		$userid = $this->oParam->userid;

		$adddata = [];
		$msgdata = [];
		foreach ($data as $key => $value) {
			$value = (array)$value;
			$value['fissuedate'] = date('Y-m-d',strtotime($value['fissuedate']));
			//返回的执行结果基本信息
			$msgdata[$key]['fmediaid'] = $value['fmediaid'];
			$msgdata[$key]['fmediatype'] = $value['fmediatype'];
			$msgdata[$key]['fissuedate'] = $value['fissuedate'];
			$msgdata[$key]['fuuid'] = $value['fuuid'];

			$where_tp['fuserid'] = $userid;
			$where_tp['fmediacode'] = $value['fmediaid'];
			$where_tp['fmediatype'] = $value['fmediatype'];
			$where_tp['fmaincode'] = 0;
			$where_tp['fstatus'] = 1;
			$mediaid = M('tapimedia_map')
				->where($where_tp)
				->getField('fmediaid');
			if(!empty($mediaid)){
				$where_tt['fuser_id'] = $userid;
				$where_tt['fmedia_id'] = $mediaid;
				$where_tt['fstatus'] = 2;
				$where_tt['fissue_date'] = $value['fissuedate'];
				$where_tt['fuuid'] = $value['fuuid'];
				$do_tt = M('tad_cut')
					->field('fid')
					->where($where_tt)
					->find();
				if(!empty($do_tt)){
					$adddata[] = $do_tt['fid'];
					$msgdata[$key]['fstatus'] = 0;
					$msgdata[$key]['freason'] = '反馈成功';
				}else{
					$msgdata[$key]['fstatus'] = 1;
					$msgdata[$key]['freason'] = '任务已反馈或不存在';
				}
			}else{
				$msgdata[$key]['fstatus'] = 1;
				$msgdata[$key]['freason'] = '媒体不存在';
			}
		}

		if(!empty($adddata)){
			M('tad_cut')
				->where(array('fid'=>array('in',$adddata)))
				->save(array('fstatus'=>3));
		}

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$msgdata));
	}

	/**
	* by zw
	* 广告截取结果
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function GetAdCutTask(){
		$data = $this->oParam->data;
		$userid = $this->oParam->userid;

		$adddata = [];
		$msgdata = [];
		foreach ($data as $key => $value) {
			$value = (array)$value;
			$value['fissuedate'] = date('Y-m-d',strtotime($value['fissuedate']));
			//返回的执行结果基本信息
			$msgdata[$key]['fmediaid'] = $value['fmediaid'];
			$msgdata[$key]['fmediatype'] = $value['fmediatype'];
			$msgdata[$key]['fissuedate'] = $value['fissuedate'];
			$msgdata[$key]['fsample_uuid'] = $value['fuuid'];

			$where_tp['a.fuserid'] = $userid;
			$where_tp['a.fmediacode'] = $value['fmediaid'];
			$where_tp['a.fmediatype'] = $value['fmediatype'];
			$where_tp['a.fmaincode'] = 0;
			$where_tp['a.fstatus'] = 1;
			$media = M('tapimedia_map')
				->field('a.fmediaid,b.fmediaclassid,b.media_region_id as fregionid,a.fmediacode')
				->alias('a')
				->join('tmedia b on a.fmediaid = b.fid','left')
				->where($where_tp)
				->find();
			if(!empty($media)){
				if(empty($media['fmediaid']) && !empty($media['fmediacode'])){
					$msgdata[$key]['fstatus'] = 1;
					$msgdata[$key]['freason'] = '媒体未作关联';
					continue;
				}
				$fmediaclassid = substr($media['fmediaclassid'], 0 , 2);
				if($fmediaclassid == '01' || $fmediaclassid == '02'){
					$where_tt['fuser_id'] = $userid;
					$where_tt['fmedia_id'] = $media['fmediaid'];
					$where_tt['fissue_date'] = $value['fissuedate'];
					$where_tt['fuuid'] = $value['fuuid'];
					$do_tt = M('tad_cut')
						->field('fid,fstatus,furl,fstart_time,fend_time')
						->where($where_tt)
						->find();
					if(!empty($do_tt)){
						if($do_tt['fstatus'] == 2 || $do_tt['fstatus'] == 3){//剪辑完成情况
							$fstart_time = strtotime($do_tt['fstart_time']);
							$fend_time = strtotime($do_tt['fend_time']);
							$tb_issue = 'sh_issue';
							if($fmediaclassid == '01'){
								$tb_sam = 'ttvsample';
							}elseif($fmediaclassid == '02'){
								$tb_sam = 'tbcsample';
							}

							//检索条件
							$where['_string'] = $tb_issue.'.fstarttime='.$fstart_time;
							$where['_string'] = $tb_issue.'.fendtime='.$fend_time;
							$where['_string'] .= ' and '.$tb_issue.'.fadclasscode not in("2302","2101")';
							$where['_string'] .= ' and '.$tb_issue.'.fmediaid = '.$media['fmediaid'];
							$where['_string'] .= ' and '.$tb_sam.'.uuid = "'.$value['fuuid'].'"';

							$fields = $tb_issue.'.fadname,
								'.$tb_issue.'.fbrand,
								'.$tb_issue.'.fadclasscode,
								'.$tb_issue.'.fadownername as fadowner,
								FROM_UNIXTIME('.$tb_issue.'.fstarttime,"%Y-%m-%d %H:%i:%s") as fstarttime,
								FROM_UNIXTIME('.$tb_issue.'.fendtime,"%Y-%m-%d %H:%i:%s") as fendtime,
								'.$tb_sam.'.fid as fsampleid,
								'.$tb_sam.'.fversion,
								'.$tb_sam.'.fspokesman,
								'.$tb_sam.'.fadlen,
								'.$tb_issue.'.flength ';

							$datasql = 'select '.$fields.'
								from '.$tb_issue.'
								inner join '.$tb_sam.' on '.$tb_issue.'.fsampleid='.$tb_sam.'.fid
								where '.$where['_string'].' limit 1';
							$tb_data = M()->query($datasql);

							if(!empty($tb_data)){
								$msgdata[$key]['fad_name'] = $tb_data[0]['fadname'];
								$msgdata[$key]['fbrand'] = $tb_data[0]['fbrand'];
								$msgdata[$key]['fad_class_code'] = $tb_data[0]['fadclasscode'];
								$msgdata[$key]['fad_owner'] = $tb_data[0]['fadowner'];
								$msgdata[$key]['fstart_time'] = $tb_data[0]['fstarttime'];
								$msgdata[$key]['fend_time'] = $tb_data[0]['fendtime'];
								$msgdata[$key]['fsample_id'] = $tb_data[0]['fsampleid'];
								$msgdata[$key]['fversion'] = $tb_data[0]['fversion'];
								$msgdata[$key]['fspokeman'] = $tb_data[0]['fspokesman'];
								$msgdata[$key]['fsample_len'] = $tb_data[0]['fadlen'];
								$msgdata[$key]['flength'] = $tb_data[0]['flength'];
								$msgdata[$key]['favifilename'] = $do_tt['furl'];
								$msgdata[$key]['fstatus'] = 0;
								$msgdata[$key]['freason'] = '获取成功';
							}else{
								$msgdata[$key]['fstatus'] = 1;
								$msgdata[$key]['freason'] = '剪辑未完成';
							}
						}else{
							$msgdata[$key]['fstatus'] = 1;
							$msgdata[$key]['freason'] = '剪辑未完成';
						}
					}else{
						$msgdata[$key]['fstatus'] = 1;
						$msgdata[$key]['freason'] = '任务不存在';
					}
				}else{
					$msgdata[$key]['fstatus'] = 1;
					$msgdata[$key]['freason'] = '该媒体类型无法展示信息';
				}
			}else{
				$msgdata[$key]['fstatus'] = 1;
				$msgdata[$key]['freason'] = '媒体不存在';
			}
		}

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$msgdata));
	}

}
