Vue.prototype.$ELEMENT = { size: 'mini', zIndex: 3000 }
const vm = new Vue({
    el: '#app',
    components:{
        adInfo,
        issueList,
    },
    data: function () {
        return {
            adClassArr,
            illegalTypeArr,
            regionTree,
            mediaClassArr,
            loading: false,
            where: {
                customer: '国家局'
            },
            tempWhere: {},
            table: [],
            page: 1,
            total: 0,
            searchType: 'ad',
            activeMonth: false,
            pickerOptions: {
                disabledDate: (time) => {
                    if (this.activeMonth !== false) {
                        let month = time.getMonth()
                        return month !== this.activeMonth
                    }
                    return false
                },
                onPick: (val) => {
                    this.activeMonth = val.minDate.getMonth()
                },
            },
            showAdInfo: false,
            currentAd: {},
        }
    },
    created: function () {
        this.getLastDate()
        this.search()
    },
    methods:{
        search: function () {
            this.where.onlyThisLevel = this.tempWhere.onlyThisLevel ? 1 : 0
            this.page = 1
            if (this.tempWhere.searchAdClassArr1 && this.tempWhere.searchAdClassArr1.length !== 0) {
                this.where.adClass = this.tempWhere.searchAdClassArr1[this.tempWhere.searchAdClassArr1.length - 1]
            }
            if (this.tempWhere.region && this.tempWhere.region.length !== 0) {
                this.where.regionId = this.tempWhere.region[this.tempWhere.region.length - 1]
            }
            this.searchReq()
        },
        searchReq: function () {
            this.loading = true
            $.post('', {
                where: this.where,
                page: this.page,
                searchType: this.searchType
            }).then((res) => {
                this.loading = false
                this.table = res.table
                this.total = +res.total
            })
        },
        resetWhere:function () {
            this.tempWhere = {}
            this.where = {
                customer: '国家局'
            }
            this.getLastDate()
            this.search()
        },
        getLastDate: function () {
            const arr = []
            let startTime = moment().format('YYYY-MM-') + '01'
            let endTime = moment().format('YYYY-MM-') + moment().daysInMonth()
            arr.push(startTime)
            arr.push(endTime)
            Vue.set(this.where, 'timeRangeArr', arr)
        },
        timeRangeChange: function (date) {
            if (!date){
                this.activeMonth = false
                this.getLastDate()
                return false
            }
            if (moment(date[0]).month() !== moment(date[1]).month()) {
                this.$alert('起始日期必须是同一个月')
                this.getLastDate()
            }
        },
        pageChange: function (page) {
            this.page = page
            this.searchReq()
        },
    },
    filters: {
        mediaClassFilter: function (value) {
            let res = ''
            mediaClassArr.forEach((v) => {
                if (v.value == value){
                    res = v.text
                }
            })
            return res
        }
    }
})