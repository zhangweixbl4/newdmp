<?php
namespace Open\Controller;
use Think\Controller;

class TadCutController extends BaseController {

	public function index(){

	}
	
	public function cutTaskReset(){
		$e_state = M('tad_cut')
					->where(array('fstatus'=>1,'last_make_time'=>array('lt',time()-3600*3)))
					->save(array('fstatus'=>0,'last_make_time'=>time()));
		file_put_contents('LOG/cutTaskReset',date('Y-m-d H:i:s') . '	' .$e_state."\n",FILE_APPEND);			
	}
	
	
	/*获取剪辑任务*/
	public function getCutTask(){
		
		if(!S('cutTaskReset')){
			S('cutTaskReset',1,3600);
			$this->cutTaskReset();
		}
		$limit = intval(I('get.limit'));
		if($limit > 20) $limit = 20;//限制获取的最大数量
		if($limit < 1) $limit = 1;//限制获取的最小数量
		$where = array();
		$where['fstatus'] = 0;
		$fid = I('get.fid');
		if($fid) $where['fid'] = $fid;
		$dbList = M('tad_cut')
								->field('fid,fmedia_id,fstart_time,fend_time,fmedia_class_id')
								->where($where)
								->limit($limit)
								->select();
		$cutList = array();		
		
			
		foreach($dbList as $db){
			$db['m3u8_url'] = A('Common/Media','Model')->get_m3u8($db['fmedia_id'],strtotime($db['fstart_time']),strtotime($db['fend_time']));
			$db['fstart_time'] = strtotime($db['fstart_time']);
			$db['fend_time'] = strtotime($db['fend_time']);
			
			$cutList[] = $db;
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'','cutList'=>$cutList));
		
	}
	
	
	/*修改剪辑任务*/
	public function editCutTask(){
		$fstatus = I('get.fstatus');
		$fid = I('get.fid');
		
		$postData = file_get_contents('php://input');
		
		$postData = json_decode($postData,true);
		$editData = $postData;
		
		//var_dump($fstatus);
		
		if($editData['fstatus'] == -1){
			$err_count = M('tad_cut')->where(array('fid'=>$fid))->getField('make_err_count');
			$editData['make_err_count'] = array('exp','make_err_count + 1');
			if($err_count < 10) $editData['fstatus'] = 0;
		}
		
		$e_state = M('tad_cut')->where(array('fid'=>$fid,'fstatus'=>$fstatus))->save($editData);
		//var_dump(M('tad_cut')->getLastSql());
		
		//exit;
		if($e_state){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败','sql'=>M('tad_cut')->getLastSql()));
		}
		
		
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

}