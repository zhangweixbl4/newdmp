const clueFJ = [{
    path: '/fjcluelist',
    name: 'fjcluelist',
    component: () => import('../components/clueFJ/fjcluelist'),
    meta: ['线索管理', '线索登记']
  },
  {
    path: '/fjresrecheck',
    name: 'fjresrecheck',
    component: () => import('../components/clueFJ/fjresrecheck'),
    meta: ['线索管理', '线索处理']
  },
  {
    path: '/fjcluetrack',
    name: 'fjcluetrack',
    component: () => import('../components/clueFJ/fjcluetrack'),
    meta: ['线索管理', '线索跟踪']
  },
  {
    path: '/fjapprovalHistory',
    name: 'fjapprovalHistory',
    component: () => import('../components/clueFJ/fjapprovalHistory'),
    meta: ['线索管理', '线索查询']
  }
]
export default clueFJ
