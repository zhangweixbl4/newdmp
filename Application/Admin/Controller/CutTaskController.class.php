<?php
//众包任务管理（新）
namespace Admin\Controller;
use TaskInput\Controller\NetAdController;
use TaskInput\Controller\TraditionAdController;
use Think\Controller;
class CutTaskController extends TaskInputController
{
    public function taskList()
    {
        if(A('Admin/Authority','Model')->authority('301001') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限'
            ]);
        }
        if (IS_POST){
            $fromData = I('where');
            $page = I('page', 1);
            $pageSize = I('pageSize', 10);
            $data = $this->baseCutTaskSearch([], $fromData, $page, $pageSize);
            $this->ajaxReturn($data);
        }else{
            $illegalTypeArr = json_encode($this->getIllegalTypeArr());
            $adClassArr = json_encode($this->getAdClassArr());
            $adClassTree = json_encode($this->getAdClassTree());
            $regionTree = json_encode($this->getRegionTree());
            $illegalArr = json_encode($this->getIllegalArr());
            $stateArr = json_encode($this->getCutTaskStateArr2());
            $this->assign(compact('illegalTypeArr', 'adClassArr', 'adClassTree', 'regionTree', 'illegalArr', 'stateArr'));
            $this->display();
        }
    }

    private function baseCutTaskSearch($where, $formData, $page = 1, $pageSize = 10)
    {
        // 发布日期搜索
        if (!$formData['issueDate']){
            $formData['issueDate'] = [
                date('Y-m') . '-01',
            ];
            $formData['issueDate'][] = date('Y-m-d', strtotime('+1 month', strtotime($formData['issueDate'][0])) - 1);
        }
        $where['fissuedate'] = ['BETWEEN',  $formData['issueDate']];
        // 完成日期搜索
/*         if (!$formData['cut_complete_time']){
//            $currentMonth = date('Y-m') . '-01';
//            $nextMonth = date('Y-m-d', strtotime('+1 month', strtotime($currentMonth)) - 1);
//            $formData['cut_complete_time'] = [$currentMonth, $nextMonth];
        }else{
            $where['cut_complete_time'] = ['BETWEEN', $formData['cut_complete_time']];
        } */
        if ($formData['state'] != ''){
            $where['fstate'] = ['EQ', $formData['state']];
        }
		if($formData['cut_task_id']){
			$where['cut_task_id'] = $formData['cut_task_id'];
		}
		if($formData['source_class']){
			$where['source_class'] = $formData['source_class'];
		}

        // 媒体搜索
        // tmedia表
        $mediaWhere = [];
        if ($formData['mediaName'] != ''){
            $mediaWhere['tmedia.fid|tmedia.fmedianame'] = ['LIKE', '%'.$formData['mediaName'].'%'];
        }
        // tmediaowner表
        $mediaOwnerWhere = [];
        if ($formData['regionId'] != ''){
            if ($formData['onlyThisLevel'] != 1){
                $region_id_rtrim = A('Common/System','Model')->get_region_left($formData['regionId']);//地区ID去掉末尾的0
                $region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
                $mediaOwnerWhere["LEFT(fregionid, $region_id_strlen)"] = ['EQ', $region_id_rtrim];
            }else{
                $mediaOwnerWhere['fregionid'] = ['LIKE', '%'.$formData['regionId'].'%'];
            }
            $mediaOwnerSubQuery = M('tmediaowner')
                ->field('fid')
                ->where($mediaOwnerWhere)
                ->buildSql();
            $mediaWhere['fmediaownerid'] = ['EXP', 'IN '. $mediaOwnerSubQuery];//地区搜索条件
        }
        // 媒介标签搜索
        if ($formData['mediaLabel'] != ''){
            $labelSubQuery = M('tlabel')
                ->field('flabelid')
                ->where([
                    'fstate' => ['EQ', 1],
                    'flabel' => ['EQ', $formData['mediaLabel']],
                ])
                ->buildSql();
            $mediaLabelSubQuery = M('tmedialabel')
                ->field('fmediaid')
                ->where([
                    'fstate' => ['EQ', 1],
                    'flabelid' => ['EXP', ' IN ' . $labelSubQuery]
                ])
                ->buildSql();

            $mediaWhere['fid'] = ['EXP', ' IN ' . $mediaLabelSubQuery];
        }
        if ($mediaWhere != []){
            $mediaSubQuery = M('tmedia')
                ->field('fid')
                ->where($mediaWhere)
                ->buildSql();
            $where['fmediaid'] = ['EXP', 'IN '. $mediaSubQuery]; // 媒体搜索条件
        }
        // 剪辑人
        if ($formData['user'] != ''){
            $userSubQuery = M('ad_input_user')
                ->field('wx_id')
                ->where([
                    'alias' => ['LIKE', '%'.$formData['user'].'%']
                ])
                ->buildSql();
            $where['cut_wx_id'] = ['EXP', " IN $userSubQuery"];
        }
        $table = M('cut_task')
			->cache(true,10)
			->field('
						`cut_task_id`,
						`fmediaid`,
						`fissuedate`,
						`fstarttime`,
						`fendtime`,
						`fsourcefilename`,
						`fstate`,
						`source_class`,
						`fremark`,
						`cut_wx_id`,
						`cut_remark`,
						`cut_complete_time`,
						`make_error_count`,
						`priority`,
						`source_length`,
						`time_list_length`,
						`inspector`,
						
						`start_make_time`
						')
            ->where($where)
			->order('fissuedate desc')
            ->page($page, $pageSize)
            ->select();
        $total = M('cut_task')->cache(true,60)->where($where)->count();
        $stateArr = $this->getCutTaskStateArr();
        foreach ($table as $key => $value) {
            $table[$key]['media_name'] = M('tmedia')->cache(true,300)->where(['fid' => ['EQ', $value['fmediaid']]])->cache(true)->getField('fmedianame');
            if ($formData['mediaName'] != ''){
                $table[$key]['media_name'] = r_highlight($table[$key]['media_name'], $formData['mediaName']);
            }
			
			//$fp_wx_id = M('tqc_media_permission')->cache(true,300)->where(array('fmediaid'=>$value['fmediaid']))->getField('wx_id');
			
			$user_name = M('ad_input_user')->cache(true,300)->where(['wx_id' => ['EQ', $value['cut_wx_id']]])->cache(true)->getField('alias');
			//$user_name .= '/';
			//$user_name .= M('ad_input_user')->cache(true,300)->where(['wx_id' => ['EQ', $fp_wx_id]])->cache(true)->getField('alias');
			
            $table[$key]['user_name'] = $user_name;
			$table[$key]['inspector_name'] = M('ad_input_user')->cache(true,300)->where(['wx_id' => $value['inspector']])->cache(true)->getField('alias');
			
           
			//$table[$key]['start_time'] = $value['fstarttime'] ? date('H:i:s', $value['fstarttime']) : '';
            //$table[$key]['end_time'] = $value['fendtime'] ? date('H:i:s', $value['fendtime']) : '';
			$table[$key]['time_section'] = date('H', $value['fstarttime']).'点-'.date('H', $value['fendtime']).'点';
			
			$table[$key]['start_make_time'] = $value['start_make_time'] ? date('m.d H:i:s', $value['start_make_time']) : '';
			$table[$key]['time_list_length'] = s_to_m_time($value['time_list_length']);
			
			
			
			
            $table[$key]['cut_complete_time'] = $value['cut_complete_time'] ? date('Y-m-d H:i:s', $value['cut_complete_time']) : '';
            $table[$key]['state'] = $stateArr[$value['fstate']];
        }

        return compact('table', 'total');
    }

    /**
     * 剪辑任务状态管理数组
     * @return array
     */
    private function getCutTaskStateArr()
    {
        $arr = [
            '0' => '排队待生成',
            '1' => '正在生成',
            '2' => '任务待处理',
            '3' => '正在处理任务',
            '4' => '已处理完',
			'5' => '待质检',
			'6' => '正质检',
			'7' => '已完成质检',
            '-1' => '服务器生成失败',
        ];
        return $arr;
    }

    private function getCutTaskStateArr2()
    {
        $arr = $this->getCutTaskStateArr();
        $res = [];
        foreach ($arr as $key => $value) {
            $res[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $res;
    }

    public function unlockCutTask($ids)
    {
        if(A('Admin/Authority','Model')->authority('301003') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限'
            ]);
        }
        $res = M('cut_task')
            ->where([
                'cut_task_id' => ['IN', $ids],
            ])
            ->save([
                'fstate' => 2,
                'cut_wx_id' => 0,
            ]);
        $idArr = explode(',', $ids);
        $flowArr = [];
        foreach ($idArr as $item) {
            $flowArr[] = [
                'cut_task_id' => $item,
                'flow_time' => time(),
                'flow_content' => '解锁任务, state改为2'
            ];
        }
        M('cut_task_flow')->addAll($flowArr);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
    }

    public function changeState($ids, $state)
    {
        if(A('Admin/Authority','Model')->authority('301003') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限'
            ]);
        }
        $res = M('cut_task')
            ->where([
                'cut_task_id' => ['IN', $ids],
            ])
            ->save([
                'fstate' => $state,
				'start_make_time'=>time(),
            ]);
        $idArr = explode(',', $ids);
        $flowArr = [];
        foreach ($idArr as $item) {
            $flowArr[] = [
                'cut_task_id' => $item,
                'flow_time' => time(),
                'flow_content' => '任务状态被改为'.$state,
            ];
        }
        M('cut_task_flow')->addAll($flowArr);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
    }

    public function changeCutTaskPriority($ids, $priority)
    {
        if(A('Admin/Authority','Model')->authority('301002') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限'
            ]);
        }
        $res = M('cut_task')
                ->where([
                    'cut_task_id' => ['IN', $ids]
                ])
                ->save([
                    'priority' => $priority,
                ]) !== false;
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
    }

    public function deleteCutTask($ids)
    {
        if(A('Admin/Authority','Model')->authority('301004') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限'
            ]);
        }
        $res = M('cut_task')
                ->where([
                    'cut_task_id' => ['IN', $ids]
                ])
                ->delete() !== false;
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
    }

    public function checkCutTaskFlow($cut_task_id)
    {
        $flowList = M('cut_task_flow')
            ->field('flow_content, flow_time, wx_id,flow_short,flow_ip')
            ->where(['cut_task_id' => ['EQ', $cut_task_id]])
            ->select();
        foreach ($flowList as $key => $value) {
            $flowList[$key]['nickname'] = $value['wx_id'] ? M('ad_input_user')->cache(true)->find($value['wx_id'])['alias'] : '';
            $flowList[$key]['flow_time'] = date('Y-m-d H:i:s', $value['flow_time']);
        }


        $this->assign(compact('flowList'));
        $this->display();
    }
	
	/*快剪频道关联的众包人员*/
	public function tqc_media_permission(){
		$mediaList = M('tqc_media_permission')
									->field('
												tmedia.fmedianame,
												tmedia.fid as fmediaid,
												tqc_media_permission.wx_id,
												tqc_media_permission.fid,
												(select alias from ad_input_user where wx_id = tqc_media_permission.wx_id) as wx_alias
												
												')
									->join('tmedia on tmedia.fid = tqc_media_permission.fmediaid')
									->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'','mediaList'=>$mediaList));
	}
	
	
	/*修改快剪频道关联人员*/
	public function change_tqc_media_permission(){
		$fid = I('fid');//记录的主键ID
		$wx_id = I('wx_id');//新的wx_id
		
		$e_state = M('tqc_media_permission')->where(array('fid'=>$fid))->save(array('wx_id'=>$wx_id));
		
		if($e_state){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败'));
		}
		
		
	}
	
	
	/*添加快剪媒介关联*/
	public function add_tqc_media_permission(){
		
		$fmediaid = I('fmediaid');
		$wx_id = I('wx_id');
		
		if(M('tqc_media_permission')->where(array('fmediaid'=>$fmediaid))->count() > 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该记录已存在'));
		}
		
		$mediaInfo = M('tmedia')->cache(true,600)->field('fid,fmedianame')->where(array('fid'=>$fmediaid))->find();
		//var_dump($mediaInfo);
		if(!$mediaInfo){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介ID错误'));
		}
		$fid = M('tqc_media_permission')->add(array('fmediaid'=>$fmediaid,'wx_id'=>$wx_id));
		
		if($fid){
			
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功','fmedianame'=>$mediaInfo['fmedianame']));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));
		}
		
		
	}
	
	
	/*创建快剪任务*/
	public function add_task(){
		$w_count = 0;
		$mediaId = I('mediaId');
		$startDate = I('startDate');
		$endDate = I('endDate');
		
		if(strtotime($endDate) < strtotime($startDate)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'传入日期错误','w_count'=>$w_count));
		}
		
		if((strtotime($endDate) - strtotime($startDate)) > 86400*15){
			$this->ajaxReturn(array('code'=>0,'msg'=>'日期跨度不能大于15天','w_count'=>$w_count));
		}
		
		if(strtotime($startDate) < (time() - 86400*180)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'不允许生成30天前的任务','w_count'=>$w_count));
		}
		
		if(strtotime($endDate) > (time() + 86400*30)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'不允许生成30天后的任务','w_count'=>$w_count));
		}
		
		
		

		$mediaInfo = M('tmedia')->cache(true,600)->field('left(fmediaclassid,2) as media_class,priority')->where(array('fid'=>$mediaId))->find();
		

		
		if($mediaInfo['media_class'] == '01'){
			$source_class = 'mp4';
		}elseif($mediaInfo['media_class'] == '02'){
			$source_class = 'aac';
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'mediaID不正确'));
		}
		
		for($i=strtotime($startDate);$i<(strtotime($endDate)+86400);$i+=3600*8){
			$startTime = $i;
			$endTime = $startTime + 3600*9;
			if($endTime > (strtotime(date('Y-m-d',$i)) + 86400)){
				$endTime = strtotime(date('Y-m-d',$i)) + 86400;
			}
			$m3u8Url = A('Common/Media','Model')->get_m3u8($mediaId,$startTime,$endTime);
			$ck = M('cut_task')->where(array('fmediaid' => $mediaId,'fissuedate' => date('Y-m-d',$startTime),'fstarttime' => $startTime,'fendtime' => $endTime))->count();

			if($ck == 0){
				
				$a_data = array();
				$a_data['fmediaid'] 			= $mediaId;
				$a_data['fissuedate'] 			= date('Y-m-d',$startTime);
				$a_data['fstarttime'] 			= $startTime;
				$a_data['fendtime'] 			= $endTime;
				$a_data['priority'] 			= $mediaInfo['priority'] + 1000;
				$a_data['fstate'] 				= 0;
				$a_data['source_class'] 		= $source_class;
				$a_data['start_make_time']		= $endTime + 3600*5;

				$a_data['m3u8_url'] 			= $m3u8Url;
				
				//var_dump($a_data);
				
				
				$cut_task_id = M('cut_task')->add($a_data);
				if($cut_task_id) $w_count ++;							
			}
			
		}
			
		$this->ajaxReturn(array('code'=>0,'msg'=>'生成了 '.$w_count.' 段任务','w_count'=>$w_count));

		
	}
	
	public function get_media_list(){
		
		$media_name = I('media_name');//获取媒介名称

		$labelidList = M('tlabel')->cache(true,600)->where(array('flabel'=>'快剪'))->getField('flabelid',true);
		$where = array();

		$where['tmedia.fmedianame|tmedia.fid'] = array('like','%'.$media_name.'%');
		$where['tmedia.priority'] = array('egt',0);
		$where['tmedialabel.flabelid'] = array('in',$labelidList);
		


		$mediaList = M('tmedia')
									->field('tmedia.fid,fmedianame')
									->join('tmedialabel on tmedialabel.fmediaid = tmedia.fid')
									->where($where)
									->order('tmedia.fsort desc')
									->limit(10)
									->select();//查询媒介列表
		//var_dump(M('tmedia')->getLastSql());
		$this->ajaxReturn(array('code'=>0,'value'=>$mediaList));
		
	}
	
	
	/*获取m3u8缓存*/
	public function m3u8_cache(){
		$cut_task_id = I('cut_task_id');
		
		$m3u8_cache = M('cut_task')->cache(true,60)->where(array('cut_task_id'=>$cut_task_id))->getField('m3u8_content');
		echo $m3u8_cache;
	}
	
	
	
	
	
}