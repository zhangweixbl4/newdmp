<?php
namespace InputPc\Model;
use Think\Exception;


class TaskModel{


    /*改变任务数量*/
    public function change_task_count($userId,$type,$num,$date = ''){
		/* $typeList = array(
							'paper_page_cut',
							'paper_ad_cut',
							'paper_input',
							'paper_inspect',
							'bc_input',
							'bc_inspect',
							'tv_input',
							'tv_inspect',
							'paper_page_up',
							'sub_paper_cut_err',
							'paper_cut_err',
							'net_input',
							'net_inspect',
							'input_err',
							'inspect_err',
							
							
							
							);
		if(!in_array($type,$typeList)){//判断类型是否存在
			return false;
		} *///无需再验证类型了
		$userId = intval($userId);//转为int类型
		if(intval($userId) == 0){
			return false;
		} 
		if($date == ''){//判断是否传递日期
			$date = date('Y-m-d');
		}
		
		$count_id = M('user_task_count')->where(array('user_id'=>$userId,'count_date'=>$date))->getField('count_id');
		if(intval($count_id) === 0 ){
			try{//尝试新增数据
				$count_id = M('user_task_count')->add(array('user_id'=>$userId,'count_date'=>$date));
			}catch(Exception $e) { //如果新增失败则获取id
				$count_id = M('user_task_count')->where(array('user_id'=>$userId,'count_date'=>$date))->getField('count_id');
			} 
		}
		
		$save_data = array();
		$save_data[$type] = array('exp',$type.' +'.$num);
		$save_ret = M('user_task_count')->where(array('count_id'=>$count_id))->save($save_data);//修改任务数量
		if($save_ret >= 0){
			return true;
		}else{
			return false;
		}
		
    }
	
	/*获取人员权限*/
	public function user_data_authority($wx_id){
		
		$userGroupInfo = M('ad_input_user_group')
												->field('ad_input_user_group.mediaauthority,ad_input_user_group.classauthority,ad_input_user_group.dateauthority')
												->join('ad_input_user on ad_input_user.group_id = ad_input_user_group.group_id')
												->where(array('ad_input_user.wx_id'=>$wx_id))
												->find();
		
		$mediaauthority = implode(',',json_decode($userGroupInfo['mediaauthority'],true));//媒介权限
		$classauthority = json_decode($userGroupInfo['classauthority'],true);//分类权限
		$dateauthority = json_decode($userGroupInfo['dateauthority'],true);//日期权限
		
		if($userGroupInfo['mediaauthority'] != 'unrestricted'){//判断媒介是否为无限制
			
			if($mediaauthority){//判断是否有授权
				$where_string .= ' and tmedia.fid in ('.$mediaauthority.')'."\n";
			}else{
				$where_string .= ' and tmedia.fid = 0'."\n";
			}
		}
		
		
		if($userGroupInfo['classauthority'] != 'unrestricted'){//判断分类是否为无限制
			if($classauthority){//判断是否有授权
				$classauthority_count = count($classauthority);//分类权限数量
				foreach($classauthority as $key => $class){
					$class_strlen = strlen($class);
					if($key == 0){
						$where_string .= ' and (';
					}else{
						$where_string .= ' or ';
					}
					$where_string .= ' left(tad.fadclasscode,'.$class_strlen.') = "'.$class.'"';
					if($classauthority_count == ($key + 1)){
						$where_string .= ' ) '."\n";
					}
					
				}
			}else{
				$where_string .= ' and tad.fadclasscode = "-10000"'."\n";
			}	
		}
		
		if($userGroupInfo['dateauthority'] != 'unrestricted'){//判断日期是否为无限制
			if($dateauthority){//判断是否有授权
				$dateauthority_count = count($dateauthority);//日期权限数量
				foreach($dateauthority as $key => $datearr){
					$date =  explode(',',$datearr);
					if($key == 0){
						$where_string .= ' and (';
					}else{
						$where_string .= ' or ';
					}
					$where_string .= ' sample.fissuedate between "'.date('Y-m-d',strtotime($date[0])).'" and "'.date('Y-m-d',strtotime($date[1])+0).'" ';
					
					if($dateauthority_count == ($key + 1)){
						$where_string .= ' ) '."\n";
					}
				}
			}else{
				$where_string .= ' and sample.fissuedate = "1970-01-01"'."\n";
			}	
			
			
		}
		
		return $where_string;
		
	}

	/*报纸剪辑权限*/
	public function paper_cut_authority($wx_id){
		
		$userGroupInfo = M('ad_input_user_group')
												->field('ad_input_user_group.mediaauthority,ad_input_user_group.classauthority,ad_input_user_group.dateauthority')
												->join('ad_input_user on ad_input_user.group_id = ad_input_user_group.group_id')
												->where(array('ad_input_user.wx_id'=>$wx_id))
												->find();
		
		$mediaauthority = implode(',',json_decode($userGroupInfo['mediaauthority'],true));//媒介权限

		$dateauthority = json_decode($userGroupInfo['dateauthority'],true);//日期权限
		
		if($userGroupInfo['mediaauthority'] != 'unrestricted'){//判断媒介是否为无限制
			
			if($mediaauthority){//判断是否有授权
				$where_string .= ' and tpapersource.fmediaid in ('.$mediaauthority.')'."\n";
			}else{
				$where_string .= ' and tpapersource.fmediaid = 0'."\n";
			}
		}
		
		
		
		if($userGroupInfo['dateauthority'] != 'unrestricted'){//判断日期是否为无限制
			if($dateauthority){//判断是否有授权
				$dateauthority_count = count($dateauthority);//日期权限数量
				foreach($dateauthority as $key => $datearr){
					$date =  explode(',',$datearr);
					if($key == 0){
						$where_string .= ' and (';
					}else{
						$where_string .= ' or ';
					}
					$where_string .= ' tpapersource.fissuedate between "'.date('Y-m-d',strtotime($date[0])).'" and "'.date('Y-m-d',strtotime($date[1])+0).'" ';
					
					if($dateauthority_count == ($key + 1)){
						$where_string .= ' ) '."\n";
					}
				}
			}else{
				$where_string .= ' and tpapersource.fissuedate = "1970-01-01"'."\n";
			}	
			
			
		}
		
		return $where_string;
		
	}
	
	/*判断权限*/
    public function user_other_authority($authority_code,$wx_id=0){
        $direct = array(//设置无须验证的页面
            'Admin/TaskInput/index',
        );
        if(in_array(MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME ,$direct)){
            return 'true';
        }
		if($wx_id == 0){
			$wx_id = intval(session('wx_id'));
		}
        $cacheKey = 'user_wx_id' . $wx_id . 'group_id_key';
        //var_dump($wx_id);
		$userGroupInfo = M('ad_input_user_group')
			->cache($cacheKey,60)
            ->field('ad_input_user_group.other_authority,ad_input_user.user_authority')
            ->join('ad_input_user on ad_input_user.group_id = ad_input_user_group.group_id')
            ->where(array('ad_input_user.wx_id'=>$wx_id))
            ->find();
		//var_dump($userGroupInfo);	
		$other_authority_arr = 	array_merge(explode(',',$userGroupInfo['other_authority']),explode(',',$userGroupInfo['user_authority']));
		
		if(	in_array($authority_code , $other_authority_arr) ){
			return 'true';
		}else{
			return 0;
		}
    }

	/**
	 * 检查数据复审权限判断
	 */
    public function checkTbnDataCheckAuth()
    {
        $wx_id = session('wx_id');
        $nameList = sys_c('tbn_data_check_user');
        if ($nameList){
            $nameList = explode(',', $nameList);
            $where = '';
            foreach ($nameList as $item) {
                $where .= "alias LIKE '$item%' or ";
            }
            $where = rtrim($where, 'or ');
            $res = M('ad_input_user')
                ->where($where)
                ->cache(true,60)
                ->getField('wx_id', true);
            return in_array($wx_id, $res);
        }else{
            return false;
        }
    }
}



