<?php
namespace TaskInput\Controller;
use Think\Controller;

/**
 * 数据标准化(加工)
 */
class StandardizationController extends BaseController{

    /**
     * 接收参数
     */
    protected $P;

    /**
     * 初始化
     */
    public function _initialize(){
        $this->P = json_decode(file_get_contents('php://input'),true);
    }

    /**
     * by 陈浩亮
     */
    public function index(){
        $this->display();
    }

    /**
     * 数据标准化-添加遗漏的广告
     */
    public function AddAd(){
        $creator = 'add_'.session('wx_id');//数据加工额外添加的广告发布记录以'add_'开头
        $fmediaid = $this->P['fmediaid'];
        $fadname = $this->P['fadname'];
        $fadclasscode = $this->P['fadclasscode'];
        $fadclasscode_v2 = $this->P['fadclasscode_v2'] ? $this->P['fadclasscode_v2'] : '3203';
        $fadowner = $this->P['fadowner'];
        $fadownername = $this->P['fadownername'];
        $fbrand = $this->P['fbrand'];
        $fspokesman = $this->P['fspokesman'];
        $fstarttime = strtotime($this->P['fstart']);
        $fendtime = strtotime($this->P['fend']);
        $fissuedate = date('Y-m-d',$fstarttime);
        $flength = $fendtime - $fstarttime;
        $fadlen = $fendtime - $fstarttime;
        $fillegaltypecode = $this->P['fillegaltypecode'];
        $fillegalcontent = $this->P['fillegalcontent'];
        $fexpressioncodes = $this->P['fexpressioncodes'];
        $fexpressions = $this->P['fexpressions'];
        $sam_source_path = A('Common/Media','Model')->get_m3u8($fmediaid, $fstarttime, $fendtime);
        $mediaInfo = M('tmedia')->where(['fid'=>$fmediaid])->find();
        $fmedianame = $mediaInfo['fmedianame'];
        $fmediaclassid = $mediaInfo['fmediaclassid'];
        $fregionid = $mediaInfo['media_region_id'];
		$mediaClass = substr($mediaInfo['fmediaclassid'],0,2);
        $pre = 'tv';
        switch($mediaClass){
            case '01':
                $pre = 'tv';
                break;
            case '02':
                $pre = 'bc';
                break;
            case '03':
                $pre = 'paper';
                break;
            case '13':
                $pre = 'net';
				break;
			default:
				$pre = 'tv';
				break;
        }
        $tableName = 't'.$pre.'issue_'.date('Ym',strtotime($fissuedate)).'_'.substr($fregionid,0,2);
        $tableSingleName = 't'.$pre.'issue'; //发布记录总单表
    	$hasTable = M()->query('SHOW TABLES LIKE "'.$tableName.'"');//判断表是否存在
    	if (!$hasTable){
            $this->ajaxReturn(['code'=>'1','msg'=>'错误，表不存在']);
        }

        $adId = A('Common/Ad','Model')
            ->getAdId($fadname,$fbrand,$fadclasscode,$fadownername,$creator,$fadclasscode_v2);
        $adSample = [
            'fmediaid' => $fmediaid,//媒介ID
            'fissuedate' => $fissuedate,//发布时间
            'fadid' => $adId,//广告ID
            'fspokesman' => $fspokesman,//代言人
            'fadlen' => $fadlen,//样本时长
            'fillegaltypecode' => $fillegaltypecode,//违法类型代码
            'fillegalcontent' => $fillegalcontent,//违法内容
            'fexpressioncodes' => $fexpressioncodes,//违法表现代码
            'fexpressions' => $fexpressions,//违法表现
            //'fconfirmations' => $fconfirmations,//认定依据
            //'fpunishments' => $fpunishments,//处罚依据
            //'fpunishmenttypes' => $fpunishmenttypes,//处罚种类及幅度
            'favifilename' => $sam_source_path,//视频或音频文件名
            'creator' => $creator
        ];
        $adSamId = A('Common/AdSample','Model')
            ->getAdSampleId($adSample);

        $dataAdIssue = [
            'fmediaid' => $fmediaid,//媒介ID
            'fsampleid' => $adSamId,//样本ID
            'fstarttime' => $fstarttime,//开始时间戳
            'fendtime' => $fendtime,//结束时间戳
            'flength' => $flength,//发布时长
            'fissuedate' => strtotime($fissuedate),//发布日期,发布当日零点的时间戳
            'fregionid' => $fregionid,//行政区划代码
            'fcreator' => $creator,//创建人
            'fcreatetime' => date('Y-m-d H:i:s') //创建时间
        ];
        //发布记录单表数据
        $dataAdIssueSingle = [
            'fmediaid' => $fmediaid,//媒介ID
            'f'.$pre.'sampleid' => $adSamId,//样本ID
            'f'.$pre.'modelid' => 20000,//单表不需要同步至分表
            'fstarttime' => date('Y-m-d H:i:s',$fstarttime),//开始时间
            'fendtime' => date('Y-m-d H:i:s',$fendtime),//结束时间
            'flength' => $flength,//发布时长
            'fissuedate' => $fissuedate,//发布日期
            'fregionid' => $fregionid,//行政区划代码
            'fcreator' => $creator,//创建人
            'fcreatetime' => date('Y-m-d H:i:s') //创建时间
        ];
        $existWhere = [
            'fmediaid' => $fmediaid,//媒介ID
            'fsampleid' => $adSamId,//样本ID
            'fstarttime' => $fstarttime,//开始时间戳
            'fendtime' => $fendtime,//结束时间戳
        ];
        $existWhereSingle = [
            'fmediaid' => $fmediaid,//媒介ID
            'f'.$pre.'sampleid' => $adSamId,//样本ID
            'f'.$pre.'modelid' => 20000,//单表不需要同步至分表
            'fstarttime' => date('Y-m-d H:i:s',$fstarttime),//开始时间
            'fendtime' => date('Y-m-d H:i:s',$fendtime),//结束时间
        ];
        $isExist = M($tableName)
            ->where($existWhere)
            ->count() > 0 ? true : false;
        $isExistSingle = M($tableSingleName)
            ->where($existWhereSingle)
            ->count() > 0 ? true : false;
        if(!$isExist){
            // 判断添加的广告与已存在的记录时间是否重叠
            $isOverlap = M($tableName)
                ->where([
                    'fmediaid' => $fmediaid,
                    'fsampleid' => $adSamId,
                    'fstarttime' => ['LT',$fendtime],
                    'fendtime' => ['GT',$fstarttime]
                ])
                ->count();
            if($isOverlap == 0){
                $retSingle = M($tableSingleName)->add($dataAdIssueSingle);//TODO:写入单表，对可能的异常进行处理
                $ret = M($tableName)->add($dataAdIssue);
                $data['fid'] = $ret;
                if($ret){
                    //推送样本至Ai
		            $uuid = md5($mediaClass.$fmediaid.$fissuedate.$adId.$creator);
                    $url = 'http://47.96.182.117/index/saveAdInfo';
                    $reqData = '{"channel": "'.$fmediaid.'", 
                        "start": "'.date('Y-m-d H:i:s',$fstarttime).'", 
                        "end": "'.date('Y-m-d H:i:s',$endtime).'", 
                        "name": "'.$fadname.'", 
                        "editor_id ": "'.$creator.'", 
                        "uuid ": "'.$uuid.'"}';
                    $method = 'POST';
                    $header = ['Content-Type: application/json'];
                    $res = http($url,$reqData,$method,$header,10);

                    $this->ajaxReturn(['code'=>'0','msg'=>'添加成功','data'=>$data]);
                }else{
                    $this->ajaxReturn(['code'=>'1','msg'=>'添加出错']);
                }
            }else{
                $this->ajaxReturn(['code'=>'1','msg'=>'开始或结束时间与已有广告重叠，请修改时间']);
            }
        }else{
            $this->ajaxReturn(['code'=>'1','msg'=>'已存在']);
        }
    }

    /**
     * 数据标准化-编辑遗漏的广告
     */
    public function EditAd(){
        $creator = 'add_'.session('wx_id');//数据加工额外添加的广告发布记录以'add_'开头
        $fid = $this->P['fid'];
        $fmediaid = $this->P['fmediaid'];
        $fadname = $this->P['fadname'];
        $fadclasscode = $this->P['fadclasscode'];
        $fadclasscode_v2 = $this->P['fadclasscode_v2'];
        // $fadowner = $this->P['fadowner'];
        $fadownername = $this->P['fadownername'];
        $fbrand = $this->P['fbrand'];
        $fspokesman = $this->P['fspokesman'];
        $fstarttime = strtotime($this->P['fstart']);
        $fendtime = strtotime($this->P['fend']);
        $fissuedate = date('Y-m-d',$fstarttime);
        $flength = $fendtime - $fstarttime;
        $fadlen = $fendtime - $fstarttime;
        $fillegaltypecode = $this->P['fillegaltypecode'];
        $fexpressioncodes = $this->P['fexpressioncodes'];
        $fexpressions = $this->P['fexpressions'];
        $fillegalcontent = $this->P['fillegalcontent'];
        $sam_source_path = A('Common/Media','Model')->get_m3u8($fmediaid, $fstarttime, $fendtime);
        $mediaInfo = M('tmedia')->where(['fid'=>$fmediaid])->find();
        $fmedianame = $mediaInfo['fmedianame'];
        $fmediaclassid = $mediaInfo['fmediaclassid'];
        $fregionid = $mediaInfo['media_region_id'];
		$mediaClass = substr($mediaInfo['fmediaclassid'],0,2);
        $pre = 'tv';
        switch($mediaClass){
            case '01':
                $pre = 'tv';
                break;
            case '02':
                $pre = 'bc';
                break;
            case '03':
                $pre = 'paper';
                break;
            case '13':
                $pre = 'net';
				break;
			default:
				$pre = 'tv';
				break;
        }
        $tableName = 't'.$pre.'issue_'.date('Ym',strtotime($fissuedate)).'_'.substr($fregionid,0,2);
		$samTable = 't'.$pre.'sample';
    	$hasTable = M()->query('SHOW TABLES LIKE "'.$tableName.'"');//判断表是否存在
    	if (!$hasTable){
            $this->ajaxReturn(['code'=>'1','msg'=>'错误，表不存在']);
        }

        $adIssueInfo = M($tableName)
            ->where(['fid'=>$fid])
            ->find();
        $adSampleInfo = M($samTable)
            ->where(['fid'=>$adIssueInfo['fsampleid']])
            ->find();
        $adInfo = M('tad')
            ->where(['fadid'=>$adSampleInfo['fadid']])
            ->find();

        $oM = M();
        $oM->startTrans();
        //更新广告表
        $fadowner = A('Common/Ad','Model')->getAdOwnerId($fadownername,$creator);//广告主
        $dataAd = [
            'fadname' => $fadname,
            'fbrand' => $fbrand,
            'fadclasscode' => $fadclasscode,
            'fadowner' => $fadowner,
            'fadclasscode_v2' => $fadclasscode_v2,
            'fmodifier' => $creator,
            'fmodifytime' => date('Y-m-d H:i:s')
        ];
        $retSaveAd = M('tad')->where(['fadid'=>$adSampleInfo['fadid']])->save($dataAd);
        //更新样本表
        $dataAdSample = [
            'fmediaid' => $fmediaid,
            'fissuedate' => $fissuedate,
            'fadid' => $adSampleInfo['fadid'],
            'fspokesman' => $fspokesman,
            'fadlen' => $fadlen,
            'fillegaltypecode' => $fillegaltypecode,
            'fillegalcontent' => $fillegalcontent,
            'fexpressioncodes' => $fexpressioncodes,
            'fexpressions' => $fexpressions,
            'favifilename' => $favifilename,
            'fmodifier' => $creator,
            'fmodifytime' => date('Y-m-d H:i:s')
        ];
        $retSaveSample = M($samTable)->where(['fid'=>$adIssueInfo['fsampleid']])->save($dataAdSample);
        //更新发布记录表
        $dataAdIssue = [
            'fmediaid' => $fmediaid,//媒介ID
            'fsampleid' => $adIssueInfo['fsampleid'],//样本ID
            'fstarttime' => $fstarttime,//开始时间戳
            'fendtime' => $fendtime,//结束时间戳
            'flength' => $flength,//发布时长
            'fissuedate' => strtotime($fissuedate),//发布日期,发布当日零点的时间戳
            'fregionid' => $fregionid,//行政区划代码
            // 'fmodifier' => $creator,
            // 'fmodifytime' => date('Y-m-d H:i:s')
        ];

        $existWhere = [
            'fid' => $fid,
        ];
        $isExist = M($tableName)
            ->where($existWhere)
            ->count() > 0 ? true : false;
        if($isExist){
            // 判断编辑后的广告与已存在的记录时间是否重叠
            $isOverlap = M($tableName)
                ->where([
                    'fid' => ['NEQ',$fid],
                    'fmediaid' => $fmediaid,
                    'fsampleid' => $adIssueInfo['fsampleid'],
                    'fstarttime' => ['LT',$fendtime],
                    'fendtime' => ['GT',$fstarttime]
                ])
                ->count();
            if($isOverlap == 0){
                $retSaveAdIssue = M($tableName)->where(['fid'=>$fid])->save($dataAdIssue);
                if($retSaveAdIssue || $retSaveSample || $retSaveAd){
                    $oM->commit();
                    $data['fid'] = $retSaveAdIssue;
                    $this->ajaxReturn(['code'=>'0','msg'=>'编辑成功','data'=>$data]);
                }else{
                    $this->ajaxReturn(['code'=>'1','msg'=>'编辑出错']);
                }
            }else{
                $oM->rollback();
                $this->ajaxReturn(['code'=>'1','msg'=>'开始时间或结束时间与已有广告重叠，请修改时间']);
            }
        }else{
            $oM->rollback();
            $this->ajaxReturn(['code'=>'1','msg'=>'不存在该记录']);
        }
    }

    /**
     * 数据标准化-删除遗漏的广告
     */
    public function DelAd(){
        $fmediaid = $this->P['fmediaid'];
        $fissuedate = $this->P['fissuedate'];
        $fid = $this->P['fid'];
        $mediaInfo = M('tmedia')->where(['fid'=>$fmediaid])->find();
        $fmedianame = $mediaInfo['fmedianame'];
        $fmediaclassid = $mediaInfo['fmediaclassid'];
        $fregionid = $mediaInfo['media_region_id'];
		$mediaClass = substr($mediaInfo['fmediaclassid'],0,2);
        $pre = 'tv';
        switch($mediaClass){
            case '01':
                $pre = 'tv';
                break;
            case '02':
                $pre = 'bc';
                break;
            case '03':
                $pre = 'paper';
                break;
            case '13':
                $pre = 'net';
				break;
			default:
				$pre = 'tv';
				break;
        }
        $tableName = 't'.$pre.'issue_'.date('Ym',strtotime($fissuedate)).'_'.substr($fregionid,0,2);
        $tableSingleName = 't'.$pre.'issue'; //发布记录总单表
    	$hasTable = M()->query('SHOW TABLES LIKE "'.$tableName.'"');//判断表是否存在
    	if (!$hasTable){
            $this->ajaxReturn(['code'=>'1','msg'=>'错误，表不存在']);
        }
        $c = 0;
        if($fid){
            $fids = explode(',', $fid);
            foreach($fids as $k=>$v){
                $isCustomer = M($tableName)->where(['fid'=>$v])->getField('fcreator');
                if(substr($isCustomer,0,3) == 'add'){//数据加工额外添加的广告才允许删除
                    $del = M($tableName)->where(['fid'=>$v])->delete();
                    if($del){
                        $c++;
                    }
                }else{
                    $this->ajaxReturn(['code'=>1,'msg'=>'无法删除系统广告']);
                }
            }
            if($c){
                $this->ajaxReturn(['code'=>0,'msg'=>'成功删除'.$c.'条记录']);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'未删除任何数据']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 数据标准化-标记错误的广告
     */
    public function MarkErrAd(){
        $creator = 'add_'.session('wx_id');//数据加工额外添加的广告发布记录以'add_'开头
        $fid = $this->P['fid'];
        $fis_error = $this->P['fis_error']; // 标记此广告是否有错 0-正常 1-错误
        $dataAd = [
            'fis_error' => $fis_error,
            'fmodifier' => $creator,
            'fmodifytime' => date('Y-m-d H:i:s')
        ];
        $existWhere = [
            'fid' => $fid,
        ];
        $isExist = M($tableName)
            ->where($existWhere)
            ->count() > 0 ? true : false;
        if($isExist){
            $ret = M($tableName)->where($existWhere)->save($dataAd);
            $data['fid'] = $ret;
            if($ret){
                $this->ajaxReturn(['code'=>'0','msg'=>'标记成功','data'=>$data]);
            }else{
                $this->ajaxReturn(['code'=>'1','msg'=>'标记出错']);
            }
        }else{
            $this->ajaxReturn(['code'=>'1','msg'=>'不存在该记录']);
        }
    }

    /**
     * 数据标准化-获取广告列表
     */
    public function ListAd(){
        $fmediaid = $this->P['fmediaid'];
        $fstarttime = $this->P['fstarttime'];
        $fendtime = $this->P['fendtime'];
        $pageIndex = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize = (int)($this->P['pageSize'] ? $this->P['pageSize'] : 100);
        $limitIndex = ($pageIndex-1)*$pageSize;
        if($fmediaid && $fstarttime && $fendtime){
            $m3u8_url = A('Common/Media','Model')->get_m3u8($fmediaid,strtotime($fstarttime),strtotime($fendtime));
            $retList = A('Common/AdIssue','Model')->getAdIssue($fmediaid,$fstarttime,$fendtime);
            $count = $retList['count'];
            $List = $retList['data'];
            $adList = [];
            $resultList = [];
            if(!empty($List)){
                foreach($List as $key=>$row){
                    if($key > 0){
                        //两个广告时间间隔大于3秒,则创建一个空时间段
                        $interval = $row['fstarttime'] - $List[$key-1]['fendtime'];
                        if($interval > 3){
                            $adNull = [
                                'fid' => '',
                                'fsampleid' => '',
                                'fmediaclassid' => '',
                                'fmediaid' => '',
                                'fmedianame' => '',
                                'fregionid' => '',
                                'fadname' => '--',
                                'fadclasscode' => '',
                                'fadclasscode_v2' => '',
                                'fadowner' => '',
                                'fadownername' => '',
                                'fbrand' => '',
                                'fspokesman' => '',
                                'fadclass' => '',
                                'fstarttime'=> date('Y-m-d H:i:s',$List[$key-1]['fendtime']),
                                'fendtime'=> date('Y-m-d H:i:s',$row['fstarttime']),
                                'flength' => $interval,
                                'fillegaltypecode'=> '',
                                'fexpressioncodes'=> '',
                                'fexpressions' => '',
                                'fillegalcontent' => '',
                                'fis_error' => '0',
                                'fis_addition' => '0',
                                'fcreator' => '',
                                'fcreatetime' => '',
                                'fmodifier' => '',
                                'fmodifytime' => ''
                            ];
                            array_push($adList,$adNull);
                            $count++;//创建的空闲时段计入记录总条数
                        }
                    }
                    $ad = [
                        'fid' => $row['fid'],
                        'fsampleid' => $row['fsampleid'],
                        'fmediaclassid' => $row['fmediaclassid'],
                        'fmediaid' => $row['fmediaid'],
                        'fmedianame' => $row['fmedianame'],
                        'fregionid' => $row['fregionid'],
                        'fadname' => $row['fadname'],
                        'fadclasscode' => $row['fadclasscode'],
                        'fadclasscode_v2' => $row['fadclasscode_v2'],
                        'fadowner' => $row['fadowner'],
                        'fadownername' => $row['fadownername'],
                        'fbrand' => $row['fbrand'],
                        'fspokesman' => $row['fspokesman'],
                        'fadclass' => $row['fadclass'],
                        'fstarttime'=> date('Y-m-d H:i:s',$row['fstarttime']),
                        'fendtime'=> date('Y-m-d H:i:s',$row['fendtime']),
                        'flength' => $row['flength'],
                        'fillegaltypecode'=> $row['fillegaltypecode'],
                        'fexpressioncodes' => $row['fexpressioncodes'],
                        'fexpressions' => $row['fexpressions'],
                        'fillegalcontent' => $row['fillegalcontent'],
                        'fis_error' => $row['fis_error'],
                        'fis_addition' => substr($row['fcreator'],0,3) == 'add' ? '1' : '0',
                        'fcreator' => $row['fcreator'] ? $row['fcreator'] : '',
                        'fcreatetime' => $row['fcreatetime'] ? $row['fcreatetime'] : '',
                        'fmodifier' => $row['fmodifier'] ? $row['fmodifier'] : '',
                        'fmodifytime' => $row['fmodifytime'] ? $row['fmodifytime'] : ''
                    ];
                    array_push($adList,$ad);
                }
            }
            foreach($adList as $k=>$v){
                //分页输出(包含空闲时段)
                $resultCount = count($resultList);
                if($resultCount < $pageSize){
                    if($k >= $limitIndex){
                        array_push($resultList,$v);
                    }
                }else{
                    break;
                }
            }
            $dataSet = [
                'fmediaid'=> $fmediaid,
                'live_m3u8_url' => $m3u8_url,
                'fad' => $resultList
            ];
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$count, 'data'=>$dataSet]);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 返回广告类型树 TODO:有待抽象到模型
     */
    public function getAdClassArr()
    {
        $data = M('tadclass')
            ->field('fcode, fpcode, fadclass')
            ->where(['fstate' => ['EQ', 1], 'fcode' => ['NEQ', 2301]])
            ->cache(true)
            ->select();
        $res = [];
        foreach ($data as $key => &$value) {
            if (strlen($value['fcode']) === 2){
                $res[] = [
                    'value' => $value['fcode'],
                    'label' => $value['fadclass'],
                ];
            }
        }
        foreach ($data as $key => &$value2) {
            $value2['value'] = $value2['fcode'];
            $value2['label'] = $value2['fadclass'];
            if (strlen($value2['fcode']) === 4){
                foreach ($res as &$item) {
                    if ($value2['fpcode'] === $item['value']){
                        $item['children'][] = $value2;
                        break;
                    }
                }
            }
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$res]);
    }

    /**
     * 商业类型树 TODO:有待抽象到模型
     */
    public function getAdClassTree()
    {
        $data = M('hz_ad_class')->field('fcode value, fpcode, fname label')->where(['fstatus' => ['EQ', 1]])->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpcode']])){
                $items[$value['fpcode']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$tree]);
    }

    /**
     * 违法类型
     */
    public function getIllegalTypeTree()
    {
        $data = M('tillegaltype')
            ->field('fcode value, fillegaltype text')
            ->where(['fstatus' => ['EQ', 1],'fcode' => ['NOT IN','10,20']])
            ->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpcode']])){
                $items[$value['fpcode']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$tree]);
    }

    /**
     * 获取一级行政区划
     */
    public function getRegionArr()
    {
        $data = M('tregion')->cache(true)->field('fid value, fname label, ffullname')->where([
            'fstate' => ['EQ', 1],
            'flevel' => ['EQ', 1],
        ])->select();
        foreach ($data as &$datum) {
            $datum['children'] = [];
        }
        // return $data;
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$data]);
    }

    /**
     * 返回违法表现树
     * @return array
     */
    public function getIllegalArr()
    {
        $data = M('tillegal')->field('fcode, fpcode, fexpression, fconfirmation, fillegaltype')->where(['fstate' => ['EQ', 1]])->cache(true)->select();
        $res = [];
        foreach ($data as &$datum) {
            $datum['id'] = $datum['fcode'];
            $datum['label'] = $datum['fexpression'];
            if (!$datum['fpcode']){
                $res[] = $datum;
            }
        }
        foreach ($data as $item) {
            foreach ($res as &$re) {
                if ($item['fpcode'] === $re['fcode']){
                    $re['children'][] = $item;
                    break;
                }
            }
        }
        // return $res;
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$res]);
    }

    /**
     * 获取媒介列表
     */
    public function getMediaList(){
        $type = $this->P['type'];
        $medianame = trim($this->P['medianame']);
        $fregionid = trim($this->P['fregionid']);
        $limit = '50';
        $mediaList = A('Common/Media','Model')->getMediaList($type,$medianame,$fregionid,$limit);
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$mediaList]);
        // if($mediaList){
        //     $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$mediaList]);
        // }else{
        //     $this->ajaxReturn(['code'=>1, 'msg'=>'获取媒介列表出错！']);
        // }
    }
}
