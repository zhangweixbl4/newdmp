<?php
namespace Api\Controller;
use Think\Controller;

class FinanceAdController extends Controller {
	
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}
	
	public function push_tv(){
		session_write_close();
		set_time_limit(0);
		header("Content-type: text/html; charset=utf-8");
		$pushUrl = 'http://adv.gzclearing.com:8083/operate/api/ads_tv';
		
		
		/*********************************查询发布记录*******************************************************************/
		$marker = A('Common/System','Model')->important_data('push_finance_ad_tv',null,array('lock'=>true));//查询数据并加锁
		
		$issueWhere = array();
		$issueWhere['fissuedate'] = array('lt',date('Y-m-d',time()-86400*3));
		$issueWhere['ftvissueid'] = array('gt',intval($marker));
		$issueInfoList = M('ttvissue')//查询发布记录，单条
								->field('ftvissueid,ftvsampleid,fmediaid,fstarttime,fendtime')
								->where($issueWhere)->order('ftvissueid asc')->limit(5000)->select();
								
		echo $marker.'	'.$issueInfoList[0]['fstarttime'];						
		//echo M('ttvissue')->getLastSql();
		//var_dump($issueInfoList[count($issueInfoList)-1]['ftvissueid']);
		A('Common/System','Model')->important_data('push_finance_ad_tv',$issueInfoList[count($issueInfoList)-1]['ftvissueid']);
		/*********************************查询发布记录*******************************************************************/
		
		
		
		/*********************************开始循环发布记录***************************************************************/
		foreach($issueInfoList as $issueInfo){
		
			$mediaInfo = $this->get_media($issueInfo['fmediaid']);//查询发布记录的媒介		


			
			$sampleWhere = array();/*********************************查询样本*******************************************************************/
			$sampleWhere['fid'] = $issueInfo['ftvsampleid'];
			$sampleInfo = S('push_tv_sample_'.$issueInfo['ftvsampleid']);
			if(!$sampleInfo){
			
				$sampleInfo = M('ttvsample')->cache(true,86400)//查询样本
											->field()
											->where($sampleWhere)
											->find();/*********************************查询样本*******************************************************************/	
				if(intval($sampleInfo['fillegaltypecode']) == 0){
					$sampleInfo['fillegaltypecode'] = 0;
				}else{
					$sampleInfo['fillegaltypecode'] = 1;
				}										
				S('push_tv_sample_'.$issueInfo['ftvsampleid'],$sampleInfo,86400);
			}
			$adInfo = $this->get_ad($sampleInfo['fadid']);//查询广告信息，含广告主
			$sampleMediaInfo = $this->get_media($sampleInfo['fmediaid']);//查询样本的媒介		
						

			$pushData = array();
			$pushData['tv'] = array(
									
									 "ftvissueid"			=> $issueInfo['ftvissueid'],
									 "fissuedate"			=> strtotime(date('Y-m-d',strtotime($issueInfo['fstarttime']))),
									 "fquantity"			=> 1,
									 "fname"				=> $adInfo['fadname'],
									 "fstarttime"			=> strtotime($issueInfo['fstarttime']),
									 "fendtime"				=> strtotime($issueInfo['fendtime']),
									 "m3u8_url"				=>  U('Api/Media/get_media_m3u8@'.$_SERVER['HTTP_HOST'],
																							array(
																									'media_id'=>$issueInfo['fmediaid'],
																									'start_time'=>strtotime($issueInfo['fstarttime']),
																									'end_time'=>strtotime($issueInfo['fendtime']),
																									))
									);
									
			$pushData['tv_media'] = array(
											"fid"						=>	$mediaInfo['fid'],
											"fmediaclassid"				=>	$mediaInfo['fmediaclassid'],
											"fmedianame"				=> $mediaInfo['fmedianame'],
											"faddr"						=> $mediaInfo['faddr'],
											"fsort"						=> 0,
											"registration_code"			=> "",
											"fregionid"					=> $mediaInfo['fregionid'],
											"registration_authority"	=> "",
											"operation_scope"			=> ""
											);	


			$pushData['sample'] = array(
											"fid"						=>	$sampleInfo['fid'],
											"fissuedate"				=>	strtotime(date('Y-m-d',strtotime($sampleInfo['fissuedate']))),
											"fadid"						=>	$adInfo['fadid'],
											"fadlen"					=>	$sampleInfo['fadlen'],
											"fillegaltypecode"			=>	$sampleInfo['fillegaltypecode'],
											"fspokesman"				=>	$sampleInfo['fspokesman'],
											"fapprovalid"				=>	$sampleInfo['fapprovalid'],
											"fapprovalunit"				=>	$sampleInfo['fapprovalunit'],
											"fillegalcontent"			=>	$sampleInfo['fillegalcontent'],
											"fexpressioncodes"			=>	$sampleInfo['fexpressioncodes'],
											"fexpressions"				=>	$sampleInfo['fexpressions'],
											"fconfirmations"			=>	$sampleInfo['fconfirmations'],
											"fpunishments"				=>	$sampleInfo['fpunishments'],
											"fpunishmenttypes"			=>	$sampleInfo['fpunishmenttypes'],
											"favifilename"				=>	$sampleInfo['favifilename'],
											"favifilepng"				=>	$sampleInfo['favifilepng'],
											"fadclasscode"				=>	$adInfo['fadclasscode'],
											"fclass"					=>	$adInfo['fadclass'],
											"brand"						=>	$adInfo['brand'],
											"deal_opinion"				=>	"",
											"fregionid"					=>	$sampleMediaInfo['fregionid'],
											"fissuetype"				=>	'平播',
											"fname"						=>	$adInfo['fadname']
											);
				//var_dump($sampleInfo);
				$pushData['sample_media'] = array(
													"fid"						=>	$sampleMediaInfo['fid'],
													"fmediaclassid"				=>	$sampleMediaInfo['fmediaclassid'],
													"fmedianame"				=>	$sampleMediaInfo['fmedianame'],
													"faddr"						=>	$sampleMediaInfo['faddr'],
													"fsort"						=>	0,
													"registration_code"			=>	"",
													"fregionid"					=>	$sampleMediaInfo['fregionid'],
													"registration_authority"	=>	"",
													"operation_scope"			=>	""
													);
													
													
				$pushData['sample_mechanism'] = array(
				
														"fid"				=>	$sampleMediaInfo['tmediaowner_fid'],
														"fname"				=>	$sampleMediaInfo['tmediaowner_fname'],
														"fenterprise"		=>	$sampleMediaInfo['fenterprise'],
														"fregaddr"			=>	$sampleMediaInfo['fregaddr'],
														"fofficeaddr"		=>	$sampleMediaInfo['fofficeaddr'],
														"flinkman"			=>	$sampleMediaInfo['flinkman'],
														"ftel"				=>	$sampleMediaInfo['ftel'],
														"fcreditcode"		=>	$sampleMediaInfo['fcreditcode'],
														"flicence"			=>	$sampleMediaInfo['flicence'],
														"fregionid"			=>	$sampleMediaInfo['fregionid']
													);									

				$pushData['sample_tadowner'] = array(
														"fid"					=>$adInfo['tadowner_fid'],
														"fname"					=> $adInfo['tadowner_fname'],
														"fregionid"				=> $adInfo['fregionid'],
														"faddress"				=> $adInfo['fofficeaddr'],
														"flinkman"				=> $adInfo['flinkman'],
														"ftel"					=> $adInfo['ftel'],
														"fmail"					=> "",
														"fstate"				=> 1,
														"registration_code"		=> "",
														"registration_office"	=> "",
														"operation_scope"		=> "",
														"websiterecord"			=> "",
														"organization_name"		=> "",
														"website_address"		=> ""	
													);
											

			
			$pushData = json_encode($pushData,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
			
			
		
			if(substr($adInfo['fadclasscode'],0,2) == '14' && $adInfo['fadclasscode'] != '1402' && substr($mediaInfo['fregionid'],0,2) == '44'){
				
				$pushRet = http($pushUrl,$pushData,'POST',false,60);
			
				echo $pushRet."\n";
				file_put_contents('LOG/PUSH.AD',$pushRet."\n",FILE_APPEND);
				
			}
			
			
			
			
			
			//echo $pushData;
		
		}
		
	}
	
	
	/*推送广播数据*/
	public function push_bc(){
		//exit;
		session_write_close();
		set_time_limit(0);
		
		header("Content-type: text/html; charset=utf-8");
		$pushUrl = 'http://adv.gzclearing.com:8083/operate/api/ads_bc';
		//$pushUrl = 'http://47.92.128.163:8083/operate/api/ads_bc';
		
		
		
		
		/*********************************查询发布记录*******************************************************************/
		$marker = A('Common/System','Model')->important_data('push_finance_ad_bc',null,array('lock'=>true));//查询数据并加锁
		//echo $marker;
		$issueWhere = array();
		$issueWhere['fissuedate'] = array('lt',date('Y-m-d',time()-86400*3));
		$issueWhere['fbcissueid'] = array('gt',intval($marker));
		$issueInfoList = M('tbcissue')//查询发布记录，单条
								->field('fbcissueid,fbcsampleid,fmediaid,fstarttime,fendtime')
								->where($issueWhere)->order('fbcissueid asc')->limit(5000)->select();
		//echo M('tbcissue')->getLastSql();
		echo $marker.'	'.$issueInfoList[0]['fstarttime'];	
		A('Common/System','Model')->important_data('push_finance_ad_bc',$issueInfoList[count($issueInfoList)-1]['fbcissueid']);
		/*********************************查询发布记录*******************************************************************/
		
		
		
		/*********************************开始循环发布记录***************************************************************/
		foreach($issueInfoList as $issueInfo){
		
			$mediaInfo = $this->get_media($issueInfo['fmediaid']);//查询发布记录的媒介		


			
			$sampleWhere = array();/*********************************查询样本*******************************************************************/
			$sampleWhere['fid'] = $issueInfo['fbcsampleid'];
			$sampleInfo = S('push_bc_sample_'.$issueInfo['fbcsampleid']);
			if(!$sampleInfo){
			
				$sampleInfo = M('tbcsample')->cache(true,86400)//查询样本
											->field()
											->where($sampleWhere)
											->find();/*********************************查询样本*******************************************************************/	
				if(intval($sampleInfo['fillegaltypecode']) == 0){
					$sampleInfo['fillegaltypecode'] = 0;
				}else{
					$sampleInfo['fillegaltypecode'] = 1;
				}										
				S('push_bc_sample_'.$issueInfo['fbcsampleid'],$sampleInfo,86400);
			}
			$adInfo = $this->get_ad($sampleInfo['fadid']);//查询广告信息，含广告主
			$sampleMediaInfo = $this->get_media($sampleInfo['fmediaid']);//查询样本的媒介		
						

			$pushData = array();
			$pushData['bc'] = array(
									
									 "fbcissueid"			=> $issueInfo['fbcissueid'],
									 "fissuedate"			=> strtotime(date('Y-m-d',strtotime($issueInfo['fstarttime']))),
									 "fquantity"			=> 1,
									 "fname"				=> $adInfo['fadname'],
									 "fstarttime"			=> strtotime($issueInfo['fstarttime']),
									 "fendtime"				=> strtotime($issueInfo['fendtime']),
									 "m3u8_url"				=>  U('Api/Media/get_media_m3u8@'.$_SERVER['HTTP_HOST'],
																							array(
																									'media_id'=>$issueInfo['fmediaid'],
																									'start_time'=>strtotime($issueInfo['fstarttime']),
																									'end_time'=>strtotime($issueInfo['fendtime']),
																									))
									);
									
			$pushData['bc_media'] = array(
											"fid"						=>	$mediaInfo['fid'],
											"fmediaclassid"				=>	$mediaInfo['fmediaclassid'],
											"fmedianame"				=> $mediaInfo['fmedianame'],
											"faddr"						=> $mediaInfo['faddr'],
											"fsort"						=> 0,
											"registration_code"			=> "",
											"fregionid"					=> $mediaInfo['fregionid'],
											"registration_authority"	=> "",
											"operation_scope"			=> ""
											);	


			$pushData['sample'] = array(
											"fid"						=>	$sampleInfo['fid'],
											"fissuedate"				=>	strtotime(date('Y-m-d',strtotime($sampleInfo['fissuedate']))),
											"fadid"						=>	$adInfo['fadid'],
											"fadlen"					=>	$sampleInfo['fadlen'],
											"fillegaltypecode"			=>	$sampleInfo['fillegaltypecode'],
											"fspokesman"				=>	$sampleInfo['fspokesman'],
											"fapprovalid"				=>	$sampleInfo['fapprovalid'],
											"fapprovalunit"				=>	$sampleInfo['fapprovalunit'],
											"fillegalcontent"			=>	$sampleInfo['fillegalcontent'],
											"fexpressioncodes"			=>	$sampleInfo['fexpressioncodes'],
											"fexpressions"				=>	$sampleInfo['fexpressions'],
											"fconfirmations"			=>	$sampleInfo['fconfirmations'],
											"fpunishments"				=>	$sampleInfo['fpunishments'],
											"fpunishmenttypes"			=>	$sampleInfo['fpunishmenttypes'],
											"favifilename"				=>	$sampleInfo['favifilename'],
											"favifilepng"				=>	$sampleInfo['favifilepng'],
											"fadclasscode"				=>	$adInfo['fadclasscode'],
											"fclass"					=>	$adInfo['fadclass'],
											"brand"						=>	$adInfo['brand'],
											"deal_opinion"				=>	"",
											"fregionid"					=>	$sampleMediaInfo['fregionid'],
											"fissuetype"				=>	'平播',
											"fname"						=>	$adInfo['fadname']
											);
				//var_dump($sampleInfo);
				$pushData['sample_media'] = array(
													"fid"						=>	$sampleMediaInfo['fid'],
													"fmediaclassid"				=>	$sampleMediaInfo['fmediaclassid'],
													"fmedianame"				=>	$sampleMediaInfo['fmedianame'],
													"faddr"						=>	$sampleMediaInfo['faddr'],
													"fsort"						=>	0,
													"registration_code"			=>	"",
													"fregionid"					=>	$sampleMediaInfo['fregionid'],
													"registration_authority"	=>	"",
													"operation_scope"			=>	""
													);
													
													
				$pushData['sample_mechanism'] = array(
				
														"fid"				=>	$sampleMediaInfo['tmediaowner_fid'],
														"fname"				=>	$sampleMediaInfo['tmediaowner_fname'],
														"fenterprise"		=>	$sampleMediaInfo['fenterprise'],
														"fregaddr"			=>	$sampleMediaInfo['fregaddr'],
														"fofficeaddr"		=>	$sampleMediaInfo['fofficeaddr'],
														"flinkman"			=>	$sampleMediaInfo['flinkman'],
														"ftel"				=>	$sampleMediaInfo['ftel'],
														"fcreditcode"		=>	$sampleMediaInfo['fcreditcode'],
														"flicence"			=>	$sampleMediaInfo['flicence'],
														"fregionid"			=>	$sampleMediaInfo['fregionid']
													);									

				$pushData['sample_tadowner'] = array(
														"fid"					=>$adInfo['tadowner_fid'],
														"fname"					=> $adInfo['tadowner_fname'],
														"fregionid"				=> $adInfo['fregionid'],
														"faddress"				=> $adInfo['fofficeaddr'],
														"flinkman"				=> $adInfo['flinkman'],
														"ftel"					=> $adInfo['ftel'],
														"fmail"					=> "",
														"fstate"				=> 1,
														"registration_code"		=> "",
														"registration_office"	=> "",
														"operation_scope"		=> "",
														"websiterecord"			=> "",
														"organization_name"		=> "",
														"website_address"		=> ""	
													);
											

			
			$pushData = json_encode($pushData,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
			
			//echo $pushData;
		
			if(substr($adInfo['fadclasscode'],0,2) == '14' && $adInfo['fadclasscode'] != '1402' && substr($mediaInfo['fregionid'],0,2) == '44'){
				
				$pushRet = http($pushUrl,$pushData,'POST',false,60);
			
				echo $pushRet."\n";
				file_put_contents('LOG/PUSH_BC.AD',$pushRet."\n",FILE_APPEND);
				
			}
			

			//echo $pushData;
		
		}
		
	}
	
	
	
	
	/*推送报纸数据*/
	public function push_paper(){
		//exit;
		session_write_close();
		set_time_limit(0);
		
		header("Content-type: text/html; charset=utf-8");
		$pushUrl = 'http://adv.gzclearing.com:8083/operate/api/ads_paper';
		//$pushUrl = 'http://47.92.128.163:8083/operate/api/ads_paper';
		
		
		
		
		/*********************************查询发布记录*******************************************************************/
		$marker = A('Common/System','Model')->important_data('push_finance_ad_paper',null,array('lock'=>true));//查询数据并加锁
		//echo $marker;
		$issueWhere = array();
		$issueWhere['tpapersample.fissuedate'] = array('lt',date('Y-m-d',time()-86400*3));
		$issueWhere['tpapersample.fpapersampleid'] = array('gt',intval($marker));
		$issueInfoList = M('tpapersample')//查询发布记录，单条
								->field('
										tpapersample.*,
										tpapersource.furl as papersource_url,
										tpapersource.fpagetype,
										tpapersource.fpage,
										tpaperissue.fissuetype,
										tpaperissue.fsize
										
										
										')
								->join('tpapersource on tpapersource.fid = tpapersample.sourceid')
								->join('tpaperissue on tpaperissue.fpapersampleid = tpapersample.fpapersampleid')
								->where($issueWhere)->order('fpapersampleid asc')->limit(5000)->select();
		echo M('tpapersample')->getLastSql();
		A('Common/System','Model')->important_data('push_finance_ad_paper',$issueInfoList[count($issueInfoList)-1]['fpapersampleid']);
		/*********************************查询发布记录*******************************************************************/
		
		
		
		/*********************************开始循环发布记录***************************************************************/
		foreach($issueInfoList as $issueInfo){
		
			$mediaInfo = $this->get_media($issueInfo['fmediaid']);//查询发布记录的媒介		


			$adInfo = $this->get_ad($issueInfo['fadid']);//查询广告信息，含广告主
			$sampleMediaInfo = $this->get_media($issueInfo['fmediaid']);//查询样本的媒介			
			$fillegaltypecode = $issueInfo['fillegaltypecode'];
			if($fillegaltypecode > 0){
				$fillegaltypecode = 1;
			}
			
			$pushData = array();
			$pushData['paper'] = array(
									
										"fpapersampleid"			=>	$issueInfo['fpapersampleid'],
										"fissuedate"				=>	strtotime(date('Y-m-d',strtotime($issueInfo['fissuedate']))),
										"fadid"						=>	$issueInfo['fadid'],
										"fillegaltypecode"			=>	$fillegaltypecode,
										"fspokesman"				=>	$issueInfo['fspokesman'],
										"fapprovalid"				=>	$issueInfo['fapprovalid'],
										"fapprovalunit"				=>	$issueInfo['fapprovalunit'],
										"fillegalcontent"			=>	$issueInfo['fillegalcontent'],
										"fexpressioncodes"			=>	$issueInfo['fexpressioncodes'],
										"fexpressions"				=>	$issueInfo['fexpressions'],
										"fconfirmations"			=>	$issueInfo['fconfirmations'],
										"fpunishments"				=>	$issueInfo['fpunishments'],
										"fpunishmenttypes"			=>	$issueInfo['fpunishmenttypes'],
										"fjpgfilename"				=>	$issueInfo['fjpgfilename'],
										
										"fadclasscode"				=>	$adInfo['fadclasscode'],
										"fclass"					=>	$adInfo['fadclass'],
										"brand"						=>	$adInfo['brand'],
										
										
										"papersource_url"			=>	$issueInfo['papersource_url'],
										"fissuetype"				=>	$issueInfo['fissuetype'],
										"deal_opinion"				=>	"",
										
										"fname"						=>	$adInfo['fadname'],
										"issue_size"				=>	$issueInfo['fsize'],
										"fpagetype"					=>	$issueInfo['fpagetype'],
										"fpage"						=>	$issueInfo['fpage']
										
										
									);
									
			$pushData['paper_media'] = array(
											"fid"						=>	$mediaInfo['fid'],
											"fmediaclassid"				=>	$mediaInfo['fmediaclassid'],
											"fmedianame"				=> 	$mediaInfo['fmedianame'],
											"faddr"						=> 	$mediaInfo['faddr'],
											"fsort"						=> 0,
											"registration_code"			=> "",
											"fregionid"					=> $mediaInfo['fregionid'],
											"registration_authority"	=> "",
											"operation_scope"			=> ""
											);	
													
													
				$pushData['paper_mechanism'] = array(
				
														"fid"				=>	$sampleMediaInfo['tmediaowner_fid'],
														"fname"				=>	$sampleMediaInfo['tmediaowner_fname'],
														"fenterprise"		=>	$sampleMediaInfo['fenterprise'],
														"fregaddr"			=>	$sampleMediaInfo['fregaddr'],
														"fofficeaddr"		=>	$sampleMediaInfo['fofficeaddr'],
														"flinkman"			=>	$sampleMediaInfo['flinkman'],
														"ftel"				=>	$sampleMediaInfo['ftel'],
														"fcreditcode"		=>	$sampleMediaInfo['fcreditcode'],
														"flicence"			=>	$sampleMediaInfo['flicence'],
														"fregionid"			=>	$sampleMediaInfo['fregionid']
													);									

				$pushData['paper_tadowner'] = array(
														"fid"					=> $adInfo['tadowner_fid'],
														"fname"					=> $adInfo['tadowner_fname'],
														"fregionid"				=> $adInfo['fregionid'],
														"faddress"				=> $adInfo['fofficeaddr'],
														"flinkman"				=> $adInfo['flinkman'],
														"ftel"					=> $adInfo['ftel'],
														"fmail"					=> "",
														"fstate"				=> 1,
														"registration_code"		=> "",
														"registration_office"	=> "",
														"operation_scope"		=> "",
														"websiterecord"			=> "",
														"organization_name"		=> "",
														"website_address"		=> ""	
													);
											

			
			$pushData = json_encode($pushData,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
			
			//echo $pushData;
			//exit;
		
			if(substr($adInfo['fadclasscode'],0,2) == '14' && $adInfo['fadclasscode'] != '1402' && substr($mediaInfo['fregionid'],0,2) == '44'){
				
				$pushRet = http($pushUrl,$pushData,'POST',false,15);
			
				echo $pushRet."\n";
				file_put_contents('LOG/PUSH_PAPER.AD',$pushRet."\n",FILE_APPEND);
				
			}
			

			//echo $pushData;
		
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	/*根据媒介ID查询媒介详细信息*/
	private function get_media($mediaId){
		
		
		$mediaWhere = array();
		$mediaWhere['tmedia.fid'] = $mediaId;
		
		$mediaInfo = S('push_finance_ad_media_'.$mediaId);
		if(!$mediaInfo){
			
			$mediaInfo = M('tmedia')
									->cache(true,864000)
									->field('
												tmedia.fid,
												fmediaclassid,
												fmedianame,
												faddr,
												tmediaowner.fregionid,
												
												tmediaowner.fid as tmediaowner_fid,
												tmediaowner.fname as tmediaowner_fname,
												tmediaowner.fenterprise,
												tmediaowner.fregaddr,
												tmediaowner.fofficeaddr,
												tmediaowner.flinkman,
												tmediaowner.ftel,
												tmediaowner.fcreditcode,
												tmediaowner.flicence
												
												')
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->where($mediaWhere)
									->find();
			S('push_finance_ad_media_'.$mediaId,$mediaInfo,86400);						
		}						
		return $mediaInfo;
	}/*********************************结束循环发布记录***************************************************************/
	
	
	
	/*根据广告id获取广告详细信息*/
	private function get_ad($adId){
		
		$adWhere = array();
		$adWhere['fadid'] = $adId;//查询条件，广告id
		
		$adInfo = S('push_finance_ad_ad_'.$adId);
		if(!$adInfo){
			$adInfo = M('tad')//查询广告信息
							->cache(true,864000)
							->field('
									tadowner.fid as tadowner_fid,
									tadowner.fname as tadowner_fname,
									tadowner.fregionid,
									tadowner.faddress,
									tadowner.flinkman,
									tadowner.ftel,
									tadowner.fmail,
									
									tad.fadid,
									tad.fadname,
									tad.fbrand,
									tad.fadclasscode

									')
							->join('tadowner on tadowner.fid = tad.fadowner')
							->where($adWhere)->find();
			S('push_finance_ad_ad_'.$adId,$adInfo,86400);				
		}					
		//var_dump(M('tad')->getLastSql());
		
		$adInfo['fadclass'] = S('adclass_'.$adInfo['fadclasscode']);
		if(!$adInfo['fadclass']){
			$adInfo['fadclass'] = M('tadclass')->cache(true,864000)->where(array('fcode'=>$adInfo['fadclasscode']))->getField('fadclass');//查询广告分类信息
			S('adclass_'.$adInfo['fadclasscode'],$adInfo['fadclass'],864000);
		}
		
		
		
		return $adInfo;
	}
	
	
	
	
	
	
	
	
	
}