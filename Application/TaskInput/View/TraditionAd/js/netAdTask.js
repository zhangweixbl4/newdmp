const netAdTask = {
  template: `<div v-loading="loading" class="net-ad">
    <div class="header" v-if="task.task_state == 1 && type == 'input'">
      <div class="h3">开始任务</div>
      <div class="timer">
        <div class="left">
          <i class="el-icon-warning"></i>
        </div>
        <div class="right">
          <div class="item">
            任务领取时间: {{startTime}}
          </div>
          <div class="item">
            剩余时间: <b>{{remainTime}}</b>秒
          </div>
        </div>
      </div>
      <div style="margin-left: 50px;" v-if="type === 'input'">
        <el-button type="primary" @click="submit">提交</el-button>
        <el-button type="warning" @click="openCutErrorForm">非广告</el-button>
      </div>
    </div>
    <div class="header" v-else>
      <div class="h3">检查任务</div>
      <div class="button-area">
        <div class="item" v-if="task.task_state == 2 && task.quality_state == 0 && type == 'review'">
          <el-button type="warning" @click="rejectTask">退回任务</el-button>
        </div>
        <div class="item" v-if="task.task_state == 4 && type == 'review'">
          <el-button type="primary" v-if="type != 'input'" @click="reformTask">重做任务</el-button>
        </div>
        <div class="item" v-if="task.task_state == 4">
          <span style="color: red;">任务被退回</span> 退回原因:{{task.back_reason}}
        </div>
        <div class="item" v-if="type == 'quality'">
          <el-button type="primary" @click="reformTask" v-if="task.task_state == 2">质检员重做任务</el-button>
          <el-button type="success" @click="passTask" v-if="task.task_state == 2">通过任务</el-button>
          <el-button type="warning" @click="rejectTask">退回任务</el-button>
        </div>
      </div>
    </div>
    <div class="content-wrapper">
      <div class="content">
        <div class="h3">广告基本信息</div>
        <div class="sample1-wrapper">
          <div class="img-wrapper" v-if="task.ftype == 'image'">
            <img :src="task.net_url" alt="广告图片">
          </div>
          <div v-if="task.ftype == 'swf'">
            <object width="100%" id="" type="application/x-shockwave-flash" :data="task.net_url">
              <param name="wmode" value="transparent"></object>
          </div>
          <div v-if="task.ftype == 'flv'">
            <video :src="task.net_url" controls></video>
          </div>
        </div>
        <el-form ref="form" :model="task" :disabled="type != 'input'" label-width="80px" :rules="rule">
          <el-row>
            <el-col :span="24">
              <el-form-item label="广告名称" prop="fadname">
                <el-autocomplete v-model="task.fadname" :fetch-suggestions="searchAdName" @input="inputAdName">
                </el-autocomplete>
              </el-form-item>
            </el-col>
          </el-row>
          <el-form-item v-if="task.adIsSure">
            <el-button type="text" @click="adError">该广告已经确认, 广告信息有误?</el-button>
          </el-form-item>
          <el-form-item v-else>
            <el-button type="text" @click="addConfirmedAd">广告信息无误, 将此广告信息确认</el-button>
          </el-form-item>
          <el-row>
            <el-col :span="24">
              <el-form-item label="广告主" prop="fname">
                <el-autocomplete v-model="task.fname" :fetch-suggestions="searchAdOwner" :disabled="task.adIsSure && (task.fname != '广告主不详' && task.fname!='')">
                  <el-button slot="append" @click="task.fname = '广告主不详'" v-if="!task.adIsSure">不详</el-button>
                </el-autocomplete>
              </el-form-item>
            </el-col>
          </el-row>
          <el-row>
            <el-col :span="24">
              <el-form-item label="商业类别" prop="adCateArr2">
                <el-cascader @change="changeFormAdClass2" :options="adClassTree" v-model="task.adCateArr2" ref="adCateArr2"
                  :filterable="true" v-if="type == 'input'" :disabled="task.adIsSure"></el-cascader>
                <span v-else>{{task.adCateText2}}</span>
              </el-form-item>
            </el-col>
          </el-row>
          <el-row>
            <el-col :span="24">
              <el-form-item label="广告类别" prop="adCateArr">
                <el-cascader :options="adClassArr" v-model="task.adCateArr" ref="adCateArr" :filterable="true" v-if="type == 'input'"
                  :disabled="task.adIsSure"></el-cascader>
                <span v-else>{{task.adCateText1}}</span>
              </el-form-item>
            </el-col>
          </el-row>
          <el-row>
            <el-col :span="24">
              <el-form-item label="广告品牌" prop="fbrand">
                <el-input v-model="task.fbrand" :disabled="task.adIsSure">
                  <el-button slot="append" @click="appendAbBrand" v-if="!task.adIsSure">无突出品牌</el-button>
                </el-input>
              </el-form-item>
            </el-col>
          </el-row>
          <el-row>
            <el-col :span="24">
              <el-form-item label="媒体名称" prop="media_name">
                <!--<el-input v-model="task.media_name"></el-input>-->
                {{task.media_name}}
              </el-form-item>
              <el-form-item label="任务id">
                {{task.taskid}}
              </el-form-item>
            </el-col>
          </el-row>
          <el-row>
            <el-col :span="24">
              <el-form-item label="代言人">
                <el-input v-model="task.fspokesman"></el-input>
              </el-form-item>
            </el-col>
          </el-row>
          <el-row>
            <el-col :sapn="24">
              <el-form-item label="版本说明">
                <el-input v-model="task.fversion"></el-input>
              </el-form-item>
            </el-col>
          </el-row>
          <el-row v-if="task.ftype != 2 && task.ftype != 9">
            <el-col :span="24">
              <el-form-item label="发布页" style="overflow: auto;">
                <a :href="task.net_original_url" target="_blank" style="white-space: nowrap">{{task.net_original_url}}</a>
              </el-form-item>
            </el-col>
          </el-row>
          <el-row v-if="task.ftype !== 9">
            <el-col :span="24">
              <el-form-item label="落地页" style="overflow: auto;">
                <a :href="task.old_target_url" target="_blank" style="white-space: nowrap">{{task.old_target_url}}</a>
              </el-form-item>
            </el-col>
          </el-row>
          <div class="text-center" v-if="!showMoreInfo && type === 'input'" style="cursor: pointer" @click="toggleShowMoreInfo">显示额外信息</div>
          <div v-else>
            <el-form-item label="生产批准文号">
              <el-input v-model="task.fmanuno"></el-input>
            </el-form-item>
            <el-form-item label="广告中生产批准文号">
              <el-input v-model="task.fadmanuno"></el-input>
            </el-form-item>
            <el-form-item label="广告批准文号">
              <el-input v-model="task.fapprno"></el-input>
            </el-form-item>
            <el-form-item label="广告中广告批准文号">
              <el-input v-model="task.fadapprno"></el-input>
            </el-form-item>
            <el-form-item label="生产企业名称">
              <el-input v-model="task.fent"></el-input>
            </el-form-item>
            <el-form-item label="广告中生产企业名称">
              <el-input v-model="task.fadent"></el-input>
            </el-form-item>
            <el-form-item label="生产企业地区">
              <el-input v-model="task.fentzone"></el-input>
            </el-form-item>
            <div class="text-center" v-if="showMoreInfo  && type === 'input'" style="cursor: pointer" @click="toggleShowMoreInfo">隐藏额外信息</div>
          </div>
          <div class="h3">涉嫌违法信息</div>
          <el-row>
            <el-col :span="24">
              <el-form-item label="违法程度" prop="fillegaltypecode">
                <el-radio-group v-model="task.fillegaltypecode" @change="changeIllegalType">
                  <el-radio :label="item.value" v-for="item in illegalTypeArr" :key="item.value">{{item.text}}</el-radio>
                </el-radio-group>
              </el-form-item>
            </el-col>
          </el-row>
          <div v-if="isIllegal" style="margin-bottom: 18px;">
            <el-form-item label="违法内容" prop="fillegalcontent">
            </el-form-item>
            <div ref="editor"></div>
          </div>
          <div v-if="isIllegal || !task.fillegaltypecode">
            <el-form-item label="违法表现" prop="fexpressioncodes" class="illegalExpression">
              <el-popover placement="right-start" width="800" trigger="click">
                <div>
                  <el-input placeholder="输入关键字进行过滤" v-model="illegalKeyword">
                  </el-input>
                  <el-tree class="filter-tree" :data="illegalArr" node-key="fcode" accordion show-checkbox
                    :filter-node-method="filterNode" @check-change="selectIllegal" ref="illegalSelecter">
                  </el-tree>
                </div>
                <el-button slot="reference">点击选择</el-button>
              </el-popover>
              <el-card>
                <div v-for="(item,index) in task.checkedIllegalArr" :key="item.fcode" style="margin: 5px;" title="点击删除"
                  @click="deleteIllegal(index)">
                  <b>{{index+1}}.</b> {{item.fexpression}}
                </div>
              </el-card>
            </el-form-item>
          </div>
          <el-row>
            <el-col :span="12" :offset="2">
              <el-button type="primary" @click="submit">提交</el-button>
            </el-col>
            <el-col :span="10">
              <el-button type="warning" @click="openCutErrorForm">非广告</el-button>
            </el-col>
          </el-row>
          <el-dialog title="点击行即填充" :visible.sync="showFillDataList" width="80%" v-if="type === 'input'">
            <el-table :data="task.fillData" highlight-current-row @current-change="selectMoreInfoRow">
              <el-table-column property="fent" label="生产企业（证件持有人）名称"></el-table-column>
              <el-table-column property="fadent" label="广告中标识的生产企业（证件持有人）名称"></el-table-column>
              <el-table-column property="fmanuno" label="生产批准文号"></el-table-column>
              <el-table-column property="fadmanuno" label="广告中生产批准文号"></el-table-column>
              <el-table-column property="fapprno" label="广告批准文号"></el-table-column>
              <el-table-column property="fadapprno" label="广告中广告批准文号"></el-table-column>
              <el-table-column property="fentzone" label="生产企业（证件持有人）所在地区"></el-table-column>
            </el-table>
          </el-dialog>
        </el-form>
      </div>
      <div class="content">
        <div class="h3">
          <span>广告内容</span>
          <div style="font-size: 14px;">
            <a @click="reloadAdIFrame">刷新广告页</a>
            <a :href="task.old_target_url" style="font-size: 14px;" target="_blank">新窗口打开</a>
          </div>
        </div>
        <div class="iframe-wrapper">
          <iframe :src="task.net_target_url" frameborder="0" sandbox="allow-scripts" ref="adIframe"></iframe>
        </div>
      </div>
    </div>
    <el-dialog title="请选择剪辑错误" :visible.sync="showCutErrorForm" :modal="false">
      <submit-cut-error :taskid="task.taskid" :media-class="task.media_class" ref="submitCutError" @success="submitCutErrorSuccess"></submit-cut-error>
    </el-dialog>
  </div>`,
  props: ['task'],
  components: {
    submitCutError
  },
  data: function () {
    const validateAdBrand = (rule, value, callback) => {
      if (!value) {
        callback()
        return false
      }
      if (value.indexOf('&') === -1 && value !== '无突出品牌' && value.indexOf('无突出品牌') !== -1) {
        callback(new Error('如果无突出品牌, 此栏就只允许填写无突出品牌'))
      }
      // 获取广告名称, 取出广告名称中有多少个&
      let adNameAndCount = this.task.fadname.match(/&/g)
      adNameAndCount = adNameAndCount ? adNameAndCount.length : 0
      let adBrandAndCount = this.task.fbrand.match(/&/g)
      adBrandAndCount = adBrandAndCount ? adBrandAndCount.length : 0
      if (adNameAndCount < adBrandAndCount) {
        callback(new Error('广告名称中的&数量与广告品牌中的&数量不同'))
      }
      // 品牌可能是多个
      if (value.indexOf('&') !== -1) {
        if (value.split('&').some(v => v !== '无突出品牌' && !v.match(/^.牌|&.牌/) && v.indexOf('牌') !== -1)) {
          this.$message('请确认录入是否正确, 一般情况下品牌中不需要输入XX牌')
        }
      } else {
        if (!value.match(/^.牌|&.牌/) && value !== '无突出品牌' && value.indexOf('牌') !== -1) {
          this.$message('请确认录入是否正确, 一般情况下品牌中不需要输入XX牌')
        }
      }
      if (value.match(/第\D届/)) {
        this.$message('请确认录入是否正确, 届数需要用数字')
      }
      if (value.match(/\D年/)) {
        this.$message('请确认录入是否正确, 年份需要用数字')
      }
      if (value.match(/&$/)) {
        callback('广告品牌不允许以&结尾')
      }
      callback()
    }
    const validateAdOwner = (rule, value, callback) => {
      if (value && value.indexOf('广告主不详') !== -1 && value !== '广告主不详') {
        callback(new Error('如果广告主不详, 此栏就只允许填写广告主不详'))
      }
      callback()
    }
    const validateAdName = (rule, value, callback) => {
      // 广告可能是多个
      if (value && value.indexOf('牌') !== -1) {
        this.$message('请确认录入是否正确, 一般情况下广告名称中不需要输入XX牌')
      }
      if (value.match(/&$/)) {
        callback('广告名称不允许以&结尾')
      }
      if (value.match(/第\D届/)) {
        this.$message('请确认录入是否正确, 届数需要用数字')
      }
      if (value.match(/\D年/)) {
        this.$message('请确认录入是否正确, 年份需要用数字')
      }
      callback()
    }
    return {
      adClassTree,
      illegalTypeArr,
      adClassArr,
      illegalArr,
      type,
      loading: false,
      showMoreInfo: false,
      showFillDataList: false,
      remainTime: 1200,
      intervalId: 0,
      viewerInstance: {
        destroy: () => {}
      },
      illegalKeyword: '',
      rule: {
        fadname: [{
            required: true,
            message: '请填写广告名称',
            trigger: 'change'
          },
          {
            validator: validateAdName,
            trigger: 'change'
          },
        ],
        fname: [{
            required: true,
            message: '请填写广告主',
            trigger: 'submit'
          },
          {
            validator: validateAdOwner,
            trigger: 'change'
          },
        ],
        fillegaltypecode: [{
          required: true,
          message: '请选择违法程度',
          trigger: 'change'
        }],
        fillegalcontent: [{
          required: true,
          message: '请填写违法内容',
          trigger: 'change'
        }],
        fexpressioncodes: [{
          required: true,
          message: '请选择违法表现',
          trigger: 'change'
        }],
        fbrand: [{
            required: true,
            message: '请输入品牌',
            trigger: 'submit'
          },
          {
            validator: validateAdBrand,
            trigger: 'change'
          },
        ],
        adCateArr2: [{
          required: true,
          message: '请选择商业类别',
          trigger: 'submit'
        }],
        adCateArr: [{
          required: true,
          message: '请选择广告类别',
          trigger: 'submit'
        }],
        media_name: [{
          required: true,
          message: '请输入媒体名称',
          trigger: 'submit'
        }],
      },
      showCutErrorForm: false,
    }
  },
  computed: {
    startTime: function () {
      return this.task.modifytime ? moment.unix(this.task.modifytime).format('YYYY-MM-DD HH:mm:ss') : ''
    },
    isIllegal: function () {
      return this.task.fillegaltypecode !== '0'
    },
  },
  watch: {
    illegalKeyword(val) {
      this.$refs.illegalSelecter.filter(val);
    },
    type: function (val) {
      this.changeIllegalType()
    }
  },
  methods: {
    dataInit: function () {
      clearInterval(this.intervalId)
      let tempUrl = this.task.net_target_url
      this.task.net_target_url = ''
      this.task.net_target_url = tempUrl
      this.loading = false
      this.type = type
      this.showMoreInfo = this.type != 'input'
      this.showFillDataList = false
      this.showCutErrorForm = false
      this.illegalKeyword = ''
      this.task.old_target_url = this.task.net_target_url
      if (this.task.net_platform == 9) {
        // this.task.article_content = this.task.article_content.split('mmbiz.qpic.cn').join('hz-weixin-img.oss-cn-hangzhou.aliyuncs.com')
        this.task.net_target_url = '/Api/NetAd/getNetAdSnapshot/adId/' + this.task.major_key
      }
      if (this.task.ftype === 'image') {
        this.viewerInit()
      } else {
        this.viewerInstance.destroy()
      }
      try {
        if (this.task.fadclasscode) {
          var adClass = [];
          if (this.task.fadclasscode.length === 4) {
            adClass.push(this.task.fadclasscode.substr(0, 2))
            adClass.push(this.task.fadclasscode)
          }
          if (this.task.fadclasscode.length === 2) {
            adClass.push(this.task.fadclasscode)
          }
          if (adClass.length === 0) {
            Vue.set(this.task, 'fadclasscode', '')
          } else {
            Vue.set(this.task, 'fadclasscode', adClass[adClass.length - 1])
          }
          Vue.set(this.task, 'adCateArr', adClass)
        } else {
          Vue.set(this.task, 'adCateArr', [])
          Vue.set(this.task, 'fadclasscode', '')
        }
        if (this.task.fadclasscode_v2) {
          var adClass2 = [];
          for (var i = 2; i <= this.task.fadclasscode_v2.length; i += 2) {
            adClass2.push(this.task.fadclasscode_v2.substr(0, i))
          }
          if (adClass2.length === 0) {
            Vue.set(this.task, 'fadclasscode_v2', '')
          } else {
            Vue.set(this.task, 'fadclasscode_v2', adClass2[adClass2.length - 1])
          }
          Vue.set(this.task, 'adCateArr2', adClass2)
        } else {
          Vue.set(this.task, 'adCateArr2', [])
          Vue.set(this.task, 'fadclasscode_v2', '')
        }
        if (!this.task.fadclasscode_v2) {
          Vue.set(this.task, 'fadclasscode_v2', 3203)
        }
      } catch (e) {

      }
      if (this.type == 'input' && this.task.task_state == 1) {
        this.intervalId = setInterval(this.timer, 1000)
      }
      this.changeIllegalType()
      this.$nextTick(() => {
        document.querySelector('.iframe-wrapper>iframe').src = ''
        document.querySelector('.iframe-wrapper>iframe').src = this.task.net_target_url
      })
    },
    timer: function () {
      this.remainTime = moment.unix(+this.task.modifytime + 1200).diff(moment(), 'seconds')
      if (this.remainTime <= 0) {
        let tempId = setInterval(() => {})
        while (tempId > 0) {
          clearInterval(tempId)
          tempId--
        }
        $.post('/Api/TaskInput/back_overtime_task_net').then(() => {
          this.$alert('抱歉, 任务超时被收回, 点击确定开始下一个任务').then(() => {
            this.$emit('next')
          })
        })
      }
    },
    viewerInit: function () {
      this.$nextTick(() => {
        try {
          this.viewerInstance.destroy()
          this.viewerInstance = new Viewer(document.querySelector('.sample1-wrapper img'), {
            inline: true,
            toolbar: false,
            navbar: false
          })
        } catch (e) {}
      })
    },
    searchAdName: function (queryString, cb) {
      $.post("/Api/Ad/searchAdByName", {
        adName: queryString
      }).then((res) => {
        cb(res.data);
      })
    },
    inputAdName: function () {
      if (this.type !== 'input') {
        return false
      }
      $.post('/Api/Ad/checkAdIsSureByName', {
        adName: this.task.fadname
      }).then((res) => {
        if (res.data.is_sure != 0) {
          this.task = Object.assign(this.task, res.data)
          this.task.adIsSure = true
          this.task.adCateArr = this._processAdClass(this.task.fadclasscode)
          this.task.adCateArr2 = this._processAdClass(this.task.fadclasscode_v2)
        } else {
          this.task.adIsSure = false
        }
        this.$forceUpdate()
      })
    },
    searchAdOwner: function (queryString, cb) {
      $.post("/Api/Adowner/searchAdOwner", {
        name: queryString
      }).then((res) => {
        cb(res.data)
      })
    },
    changeFormAdClass2: function (value) {
      var code = value[value.length - 1]
      Vue.set(this.task, 'fadclasscode_v2', code)
      $.post('/Api/Adclass/getAdClassA', {
        code: code
      }).then((res) => {
        if (res.code == 0) {
          let fadclasscode = res.data
          let adCateArr = this._processAdClass(res.data)
          if (this.task.fadclasscode) {
            this.$confirm('该广告已有广告类别, 是否改为商业类别对应的广告类别 ?').then(() => {
              this._updateAdCateArr(adCateArr, fadclasscode)
            }).catch(() => {})
          } else {
            this._updateAdCateArr(adCateArr, fadclasscode)
          }
        } else {
          this.$message('该分类未关联广告类别, 请手动选择')
        }
      })
    },
    _processAdClass: function (data) {
      let adClass = [];
      if (data.length === 6) {
        adClass.push(data.substr(0, 2))
        adClass.push(data.substr(0, 4))
        adClass.push(data)
      }
      if (data.length === 4) {
        adClass.push(data.substr(0, 2))
        adClass.push(data)
      }
      if (data.length === 2) {
        adClass.push(data)
      }
      return adClass
    },
    _updateAdCateArr: function (adCateArr, fadclasscode) {
      Vue.set(this.task, 'adCateArr', adCateArr)
      Vue.set(this.task, 'fadclasscode', fadclasscode)
    },
    filterNode(value, data) {
      if (!value) return true;
      return data.label.indexOf(value) !== -1;
    },
    selectIllegal: function (data, isChecked) {
      if (data.fpcode === '') {
        return false
      }
      this.task.checkedIllegalArr = this.task.checkedIllegalArr || []
      let index = this.task.checkedIllegalArr.indexOf(data);
      if (isChecked && index === -1 && data.fpcode !== '') {
        this.task.checkedIllegalArr.push(data)
      }
      if (!isChecked && index !== -1) {
        this.task.checkedIllegalArr.splice(index, 1)
      }
      this.changeIllegal()
      if (this.task.checkedIllegalArr.length > 0) {
        let maxCode = 0
        this.task.checkedIllegalArr.forEach((v) => {
          if (v.fillegaltype > maxCode) {
            maxCode = v.fillegaltype
          }
        })
        Vue.set(this.task, 'fillegaltypecode', maxCode)
      }
      this.$forceUpdate()
    },
    deleteIllegal: function (index) {
      if (this.type !== 'input') {
        return false
      }
      this.task.checkedIllegalArr.splice(index, 1);
      this.$refs.illegalSelecter.setCheckedNodes(this.task.checkedIllegalArr);
      this.changeIllegal()
    },
    changeIllegal: function () {
      if (this.task.checkedIllegalArr.length === 0) {
        Vue.set(this.task, 'fexpressioncodes', '')
      }
      let arr = this.task.checkedIllegalArr.map((v) => {
        return v.fcode
      });
      let str = arr.join(';')
      Vue.set(this.task, 'fexpressioncodes', str)
    },
    toggleShowMoreInfo: function () {
      if (!this.showMoreInfo) {
        this.loading = true
        $.post('/api/ad/get_net_ad_more_field', {
          fadname: this.task.fadname
        }).then((res) => {
          if (res.code == 0) {
            this.task.fillData = res.moreFieldList
            if (this.task.fillData.length !== 0) {
              this.showFillDataList = true
            }
          }

          this.loading = false
        })
      }
      this.showMoreInfo = !this.showMoreInfo
    },
    selectMoreInfoRow: function (row) {
      Object.assign(this.task, row)
    },
    submit: function () {
      this.$refs.form.validate((res) => {
        if (!res) {
          this.$alert('表单中有未填写的必填项')
          return false
        }
        this.$confirm('确认提交?').then(() => {
          if (this.loading) {
            this.$message.warning('正在提交中, 请不要重复提交')
            return false
          }
          this.loading = true;
          const tagObj = {}
          // if (this.task.adIsSure){
          //     tagObj.isSure = 1
          // }
          if (this.isIllegal) {
            tagObj.isIllegal = 1
          }
          this.task.fadclasscode = this.task.adCateArr[this.task.adCateArr.length - 1]
          this.task.fadclasscode_v2 = this.task.adCateArr2[this.task.adCateArr2.length - 1]

          this.task.fadname = this.task.fadname.replace(/＆/g, '&')
          this.task.fbrand = this.task.fbrand.replace(/＆/g, '&')
          Vue.set(this.task, 'fbrand', this.task.fbrand)
          Vue.set(this.task, 'fadclasscode', this.task.fadclasscode)
          if (!this.task.fadclasscode_v2) {
            Vue.set(this.task, 'fadclasscode_v2', 3203)
          }
          $.post('submitTask', {
            data: this.task,
            tags: tagObj
          }).then((res) => {
            if (res.code == 0) {
              this.$confirm('提交成功, 是否继续做新任务?', '提交成功', {
                confirmButtonText: '继续',
                cancelButtonText: '取消',
                type: 'success',
              }).then(() => {
                this.$emit('next')
              }).catch(() => {
                this.$emit('back')
              })
            } else {
              this.$message({
                message: res.msg,
                type: 'error'
              })
              this.$emit('error')
            }
          })
        })
      })
    },
    openCutErrorForm: function () {
      this.showCutErrorForm = true
      this.$nextTick(() => {
        this.$refs.submitCutError.dataInit()
      })
    },
    submitCutErrorSuccess: function () {
      this.$emit('next')
    },
    reformTask: function () {
      this.$confirm('确认重做任务?').then(() => {
        this.type = 'input'
      }).catch(() => {})
    },
    passTask: function () {
      this.$confirm('通过通过任务?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
      }).then(() => {
        const data = {
          taskid: this.task.taskid
        }
        this.loading = true
        $.post('passTask', data).then((res) => {
          this.loading = false
          if (res.code == -2 && res.msg == 'need log in again') {
            this.$message.error('登录状态过期,请再次登录')
          }
          if (res.code == 0) {
            this.$message({
              type: 'success',
              message: '通过成功'
            })
          } else {
            this.$message({
              type: 'error',
              message: '通过失败'
            })
          }
          this.$emit('back')
        })
      }).catch(() => {})
    },
    rejectTask: function () {
      this.$prompt('确认退回任务?', '提示', {
        confirmButtonText: '提交',
        cancelButtonText: '取消',
        inputPattern: /\S/,
        inputErrorMessage: '请输入退回说明'
      }).then((value) => {
        const data = {
          taskid: this.task.taskid,
          reason: value.value,
          mediaClass: this.task.media_class
        }
        this.loading = true
        $.post('submitRejectTaskRequest', data).then((res) => {
          this.loading = false
          if (res.code == -2 && res.msg == 'need log in again') {
            this.$message.error('登录状态过期,请再次登录')
          }
          if (res.code == 0) {
            this.$message({
              type: 'success',
              message: '成功退回'
            })
          } else {
            this.$message({
              type: 'error',
              message: '退回失败'
            })
          }
          this.$emit('back')
        })
      }).catch(() => {})
    },
    submitRecallRequest: function () {
      this.$confirm('是否发起撤回请求?待质检员审核确认后任务将退回到被退回状态', '是否确认?', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'danger'
      }).then(() => {
        $.post('receiveRecallRequest', {
          taskid: this.task.taskid
        }).then((res) => {
          if (res.code == -2 && res.msg == 'need log in again') {
            this.$message.error('登录状态过期,请再次登录')
          }
          this.$message({
            message: res.msg,
            type: res.type,
          })
          this.$emit('back')
        })
      }).catch(() => {})
    },
    appendAbBrand: function () {
      if (!this.task.fbrand) {
        this.task.fbrand = '无突出品牌'
        return true
      }
      if (this._validateAdBrand()) {
        this.task.fbrand += '&无突出品牌'
      } else {
        this.$message.warning('品牌中的&数量不得超过广告名称中&的数量')
      }
    },
    _validateAdBrand: function () {
      if (!this.task.fadname) {
        this.$message.warning('请输入广告名称')
        return false
      }
      if (!this.task.fbrand) {
        this.task.fbrand = ''
        return false
      }
      // 获取广告名称, 取出广告名称中有多少个&
      let adNameAndCount = this.task.fadname.match(/&\S/g)
      adNameAndCount = adNameAndCount ? adNameAndCount.length : 0
      let adBrandAndCount = this.task.fbrand.match(/&/g)
      adBrandAndCount = adBrandAndCount ? adBrandAndCount.length : 0
      // 只有品牌中的&数量小于等于名称中的&数量, 才允许添加
      return adNameAndCount > adBrandAndCount
    },
    changeIllegalType: function () {
      if (this.task.fillegaltypecode != 0) {
        this.$nextTick(() => {
          const editor = new wangEditor(this.$refs.editor)
          editor.customConfig.onchange = (html) => {
            if (html === '<p><br></p>') {
              this.task.fillegalcontent = ''
            } else {
              this.task.fillegalcontent = html
            }
          }
          editor.customConfig.uploadImgServer = this.task.fileUp.up_server
          editor.customConfig.uploadFileName = this.task.fileUp.file
          editor.customConfig.uploadImgParams = {
            token: this.task.fileUp.val.token
          }
          const vm = this
          editor.customConfig.uploadImgHooks = {
            before: function (xhr, editor, files) {
              vm.$message('图片上传中')
            },
            fail: function (xhr, editor, result) {
              vm.$message('图片上传失败')
            },
            error: function (xhr, editor) {
              vm.$message('图片上传失败')
            },
            customInsert: function (insertImg, result, editor) {
              vm.$message.success('图片上传成功')
              const url = vm.task.fileUp.bucket_url + result[vm.task.fileUp.key]
              editor.txt.append(`<a href="${url}" target="_blank" title="点击查看大图"><img src="${url}" alt=""></a>`)
              // insertImg(vm.task.fileUp.bucket_url + url)
            }
          }
          editor.customConfig.menus = []
          editor.customConfig.zIndex = 100
          editor.create()
          editor.txt.html(this.task.fillegalcontent)
          editor.$textElem.attr('contenteditable', this.type === 'input')

        })
      }
    },
    adError: function () {
      this.$prompt('请填写广告错误处与正确信息', '提示', {
        confirmButtonText: '提交',
        cancelButtonText: '取消',
        inputPattern: /\S/,
        inputErrorMessage: '请输入错误之处与正确信息'
      }).then((value) => {
        const data = {
          fadid: this.task.fadid,
          comment: value.value
        }
        this.loading = true
        $.post('/api/ad/changeAdIsSure', data).then((res) => {
          this.loading = false
          if (res.code == -2 && res.msg == 'need log in again') {
            this.$message.error('登录状态过期,请再次登录')
          }
          if (res.code == 0) {
            this.$message({
              type: 'success',
              message: res.msg
            })
          } else {
            this.$message({
              type: 'error',
              message: res.msg
            })
          }
        })
      }).catch(() => {})
    },
    reloadAdIFrame: function () {
      const tempUrl = this.$refs.adIframe.src
      this.$refs.adIframe.src = ''
      this.$refs.adIframe.src = tempUrl
    },
    addConfirmedAd: function () {
      this.$confirm('确认广告信息正确并且合乎规范 ?').then(() => {
        this.$refs.form.validate((res) => {
          if (res) {
            const adInfo = {
              'fadname': this.task.fadname,
              'fname': this.task.fname,
              'fadclasscode_v2': this.task.adCateArr2[this.task.adCateArr2.length - 1],
              'fadclasscode': this.task.adCateArr[this.task.adCateArr.length - 1],
              'fbrand': this.task.fbrand,
            }
            this.loading = true
            $.post('/Api/Ad/addConfirmedAd', {
              adInfo: adInfo
            }).then((res) => {
              this.loading = false
              if (res.code == 0) {
                this.$message.success(res.msg)
                this.inputAdName()
              } else {
                this.$message.error(res.msg)
              }
            })
          }
        })
      })

    }
  },

}