<?php

namespace Common\Model;
use Think\Exception;

/**
 * 广告发布记录模型
 */
class AdIssueModel{

    /**
     * 获取指定媒介指定时间段内广告发布记录及违法情况-暂只适用于电视
     * @Param $pageIndex页码 $pageSize每页条数 不传参或0则不分页
     */
    public function getAdIssueByMedia($fmediaid=0,$fstarttime,$fendtime,$pageIndex=0,$pageSize=0){
        $mediaInfo = M('tmedia')
            ->where(['fid'=>$fmediaid])
            ->find();
        $reg = substr($mediaInfo['media_region_id'],0,2);
    	$tableName = 'ttvissue_'.date('Ym',strtotime($fstarttime)).'_'.$reg;//按查询日期确定表名
    	$hasTable = M()->query('SHOW TABLES LIKE "'.$tableName.'"');		//判断表是否存在
    	if (!$hasTable){
    		return false;
        }
        $where = [
            'T.fmediaid' => $fmediaid,
            'T.fstarttime' => ['EGT',strtotime($fstarttime)],
            'T.fendtime' => ['ELT',strtotime($fendtime)],
        ];
        $count = M($tableName)
            ->alias('T')
            ->join('ttvsample as s ON s.fid=T.fsampleid')
            ->join('tad as a ON a.fadid=s.fadid')
            ->where($where)
            ->count();
        if($pageIndex == 0 || $pageSize == 0){
            //不分页
            $List = M($tableName)
                ->field('T.*,s.*,a.*,T.fid fissueid')
                ->alias('T')
                ->join('ttvsample as s ON s.fid=T.fsampleid')
                ->join('tad as a ON a.fadid=s.fadid')
                ->where($where)
                ->order('T.fstarttime ASC')
                ->select();
        }else{
            //分页
            $limitIndex = ($pageIndex-1)*$pageSize;
            $List = M($tableName)
                ->field('T.*,s.*,a.*,T.fid fissueid')
                ->alias('T')
                ->join('ttvsample as s ON s.fid=T.fsampleid')
                ->join('tad as a ON a.fadid=s.fadid')
                ->where($where)
                ->order('T.fstarttime ASC')
                ->limit($limitIndex,$pageSize)
                ->select();
        }
    	foreach ($List as $k=>$row){
    		$List[$k]['fadclass'] = M('tadclass')
    			->cache(true,3600)
    			->where(array('fcode'=>$row['fadclasscode']))
    			->getField('ffullname');
    		$List[$k]['fregion'] = M('tregion')
    			->cache(true,3600)
    			->where(array('fid'=>$row['fregionid']))
    			->getField('ffullname');
    		if ($media_class == '01' || $media_class == '02'){
	    		$List[$k]['favifilename'] = M($sampleTable)
	    			->cache(true,3600)
	    			->where(array($sampleId => $row['fsampleid']))
	    			->getField('favifilename');
    		}
    		//发布时间显示精确到秒
    		$List[$k]['fissuedate'] = date('Y-m-d H:i:s',$row['fstarttime']);
        }
        $dataSet = [
            'count' => $count,
            'data' => $List
        ];
        return $dataSet;
    }

    /**
     * 国家局-获取指定媒介指定时间段内广告发布记录
     * @Param $pageIndex页码 $pageSize每页条数 不传参或0则不分页
     */
    public function getAdIssueByMediaGov($fmediaid=0,$fstarttime,$fendtime,$pageIndex=0,$pageSize=0){
    	$tableName = 'sh_issue';
        $where = [
            'T.fmediaid'   => $fmediaid,
            'T.fendtime'   => ['EGT',strtotime($fstarttime)],//广告结束时间在起始时间后
            'T.fstarttime' => ['ELT',strtotime($fendtime)],//广告开始时间在截止时间前
        ];
        $count = M($tableName)
            ->alias('T')
            ->where($where)
            ->count();
        if($pageIndex == 0 || $pageSize == 0){
            //不分页
            $List = M($tableName)
                ->alias('T')
                ->where($where)
                ->order('T.fstarttime ASC')
                ->select();
        }else{
            //分页
            $List = M($tableName)
                ->alias('T')
                ->where($where)
                ->order('T.fstarttime ASC')
                ->page($pageIndex,$pageSize)
                ->select();
        }
    	foreach ($List as $k=>$row){
    		$List[$k]['fadclass'] = M('tadclass')
    			->cache(true,120)
    			->where(array('fcode'=>$row['fadclasscode']))
    			->getField('ffullname');
    		$List[$k]['fregion'] = M('tregion')
    			->cache(true,120)
    			->where(array('fid'=>$row['fregionid']))
    			->getField('ffullname');
        }
        $dataSet = [
            'count' => $count,
            'data' => $List
        ];
        return $dataSet;
    }

    /**
     * 上海-获取指定媒介指定时间段内广告发布记录
     * @Param $pageIndex页码 $pageSize每页条数 不传参或0则不分页
     */
    public function getAdIssueByMediaCustomer($fmediaid=0,$fstarttime,$fendtime,$fcustomer,$pageIndex=0,$pageSize=0){
        $tableName = 'sh_issue';//上海数据检查模块广告发布记录独立表sh_issue

        $where = [
            'T.fmediaid' => $fmediaid,
            'T.fendtime' => ['EGT',strtotime($fstarttime)],//广告结束时间在起始时间后
            'T.fstarttime' => ['ELT',strtotime($fendtime)],//广告开始时间在截止时间前
            'T.fuserid' => $fcustomer,//广告开始时间在截止时间前
            // 'T.fadclasscode' => ['NEQ','2302'] //过滤预告片，广告类别2302
        ];
        $count = M($tableName)
            ->alias('T')
            ->where($where)
            ->count();
        if($pageIndex == 0 || $pageSize == 0){
            //不分页
            $List = M($tableName)
                // ->cache(true,30)
                ->alias('T')
                ->where($where)
                ->order('T.fstarttime ASC')
                ->select();
        }else{
            //分页
            $limitIndex = ($pageIndex-1)*$pageSize;
            $List = M($tableName)
                // ->cache(true,30)
                ->alias('T')
                ->where($where)
                ->order('T.fstarttime ASC')
                ->limit($limitIndex,$pageSize)
                ->select();
        }

        foreach ($List as $k=>$row){
            $List[$k]['fadclass'] = M('tadclass')
                ->cache(true,120)
                ->where(array('fcode'=>$row['fadclasscode']))
                ->getField('ffullname');
            $List[$k]['fregion'] = M('tregion')
                ->cache(true,120)
                ->where(array('fid'=>$row['fregionid']))
                ->getField('ffullname');
            //广告主名称
            $List[$k]['fadownername'] = M('tadowner')
             ->cache(true,3600)
             ->where(array('fid'=>$row['fadowner']))
                ->getField('fname');
        }
        $dataSet = [
            'count' => $count,
            'data' => $List
        ];
        return $dataSet;
    }

    /**
     * 上海-获取指定媒介指定时间段内广告发布记录
     * @Param $pageIndex页码 $pageSize每页条数 不传参或0则不分页
     */
    public function getAdIssueByMediaSh($fmediaid=0,$fstarttime,$fendtime,$pageIndex=0,$pageSize=0){
        $system_num = getconfig('system_num');
    	$tableName = 'sh_issue';//上海数据检查模块广告发布记录独立表sh_issue

        $where = [
            'T.fuserid' => $system_num,
            'T.fmediaid' => $fmediaid,
            'T.fendtime' => ['EGT',strtotime($fstarttime)],//广告结束时间在起始时间后
            'T.fstarttime' => ['ELT',strtotime($fendtime)],//广告开始时间在截止时间前
            // 'T.fadclasscode' => ['NEQ','2302'] //过滤预告片，广告类别2302
        ];
        $count = M($tableName)
            ->alias('T')
            ->where($where)
            ->count();
        if($pageIndex == 0 || $pageSize == 0){
            //不分页
            $List = M($tableName)
                // ->cache(true,30)
                ->alias('T')
                ->where($where)
                ->order('T.fstarttime ASC')
                ->select();
        }else{
            //分页
            $limitIndex = ($pageIndex-1)*$pageSize;
            $List = M($tableName)
                // ->cache(true,30)
                ->alias('T')
                ->where($where)
                ->order('T.fstarttime ASC')
                ->limit($limitIndex,$pageSize)
                ->select();
        }

    	foreach ($List as $k=>$row){
    		$List[$k]['fadclass'] = M('tadclass')
    			->cache(true,120)
    			->where(array('fcode'=>$row['fadclasscode']))
    			->getField('ffullname');
    		$List[$k]['fregion'] = M('tregion')
    			->cache(true,120)
    			->where(array('fid'=>$row['fregionid']))
    			->getField('ffullname');
            //广告主名称
    		// $List[$k]['fadownername'] = M('tadowner')
    		// 	// ->cache(true,3600)
    		// 	->where(array('fid'=>$row['fadowner']))
            //     ->getField('fname');
        }
        $dataSet = [
            'count' => $count,
            'data' => $List
        ];
        return $dataSet;
    }

    /**
     * 北京-获取指定媒介指定时间段内广告发布记录
     * @Param $pageIndex页码 $pageSize每页条数 不传参或0则不分页
     */
    public function getAdIssueByMediaBj($fmediaid=0,$fstarttime,$fendtime,$pageIndex=0,$pageSize=0){
    	$tableName = 'bj_issue';//北京数据检查模块广告发布记录独立表bj_issue
        $where = [
            'T.fmediaid' => $fmediaid,
            'T.fendtime' => ['EGT',strtotime($fstarttime)],//广告结束时间在起始时间后
            'T.fstarttime' => ['ELT',strtotime($fendtime)],//广告开始时间在截止时间前
            // 'T.fadclasscode' => ['NEQ','2302'] //过滤预告片，广告类别2302
        ];
        $count = M($tableName)
            ->alias('T')
            ->where($where)
            ->count();
        if($pageIndex == 0 || $pageSize == 0){
            //不分页
            $List = M($tableName)
                // ->cache(true,30)
                ->alias('T')
                ->where($where)
                ->order('T.fstarttime ASC')
                ->select();
        }else{
            //分页
            $limitIndex = ($pageIndex-1)*$pageSize;
            $List = M($tableName)
                // ->cache(true,30)
                ->alias('T')
                ->where($where)
                ->order('T.fstarttime ASC')
                ->limit($limitIndex,$pageSize)
                ->select();
        }

    	foreach ($List as $k=>$row){
    		$List[$k]['fadclass'] = M('tadclass')
    			->cache(true,120)
    			->where(array('fcode'=>$row['fadclasscode']))
    			->getField('ffullname');
    		$List[$k]['fregion'] = M('tregion')
    			->cache(true,120)
    			->where(array('fid'=>$row['fregionid']))
    			->getField('ffullname');
            //广告主名称
    		// $List[$k]['fadownername'] = M('tadowner')
    		// 	// ->cache(true,3600)
    		// 	->where(array('fid'=>$row['fadowner']))
            //     ->getField('fname');
        }
        $dataSet = [
            'count' => $count,
            'data' => $List
        ];
        return $dataSet;
    }

    /**
     * 获取指定媒介指定时间段内广告发布记录及违法情况-暂仅限电视、广播
     * @Param $pageIndex页码 $pageSize每页条数 不传参或0则不分页
     */
    public function getAdIssue($fmediaid=0,$fstarttime,$fendtime,$pageIndex=0,$pageSize=0){
        $mediaInfo = M('tmedia')
            ->where(['fid'=>$fmediaid])
            ->find();
        $reg = substr($mediaInfo['media_region_id'],0,2);
        $mediaclass = substr($mediaInfo['fmediaclassid'],0,2);
        $pre = 'tv';
        switch($mediaclass){
            case '01':
                $pre = 'tv';
                break;
            case '02':
                $pre = 'bc';
                break;
            case '03':
                $pre = 'paper';
                break;
            case '13':
                $pre = 'net';
                break;
        }
        $tableName = 't'.$pre.'issue_'.date('Ym',strtotime($fstarttime)).'_'.$reg;//按查询日期确定表名
        $samName = 't'.$pre.'sample'; //样本表
    	$hasTable = M()->query('SHOW TABLES LIKE "'.$tableName.'"');		//判断表是否存在
    	if (!$hasTable){
    		return false;
        }
        $where = [
            'T.fmediaid' => $fmediaid,
            'T.fstarttime' => ['EGT',strtotime($fstarttime)],
            'T.fendtime' => ['ELT',strtotime($fendtime)],
        ];
        $count = M($tableName)
            ->alias('T')
            ->join($samName.' as s ON s.fid=T.fsampleid')
            ->join('tad as a ON a.fadid=s.fadid')
            ->where($where)
            ->count();
        if($pageIndex == 0 || $pageSize == 0){
            //不分页
            $List = M($tableName)
                ->field('
                    T.fid,
                    T.fmediaid,
                    T.fsampleid,
                    T.fstarttime,
                    T.fendtime,
                    T.flength,
                    T.fissuedate,
                    T.faudiosimilar,
                    T.fregionid,
                    T.fcreatetime,
                    T.fcreator,
                    s.fadid,
                    s.fversion,
                    s.fspokesman,
                    s.fadlen,
                    s.fillegaltypecode,
                    s.fillegalcontent,
                    s.fexpressioncodes,
                    s.fexpressions,
                    s.fconfirmations,
                    s.favifilename,
                    s.uuid,
                    a.fadname,
                    a.fbrand,
                    a.fadclasscode,
                    a.fadowner,
                    a.fadclasscode_v2')
                ->alias('T')
                ->join($samName.' as s ON s.fid=T.fsampleid')
                ->join('tad as a ON a.fadid=s.fadid')
                ->where($where)
                ->order('T.fstarttime ASC')
                ->select();
        }else{
            //分页
            $limitIndex = ($pageIndex-1)*$pageSize;
            $List = M($tableName)
                ->field('
                    T.fid,
                    T.fmediaid,
                    T.fsampleid,
                    T.fstarttime,
                    T.fendtime,
                    T.flength,
                    T.fissuedate,
                    T.faudiosimilar,
                    T.fregionid,
                    T.fcreatetime,
                    T.fcreator,
                    s.fadid,
                    s.fversion,
                    s.fspokesman,
                    s.fadlen,
                    s.fillegaltypecode,
                    s.fillegalcontent,
                    s.fexpressioncodes,
                    s.fexpressions,
                    s.fconfirmations,
                    s.favifilename,
                    s.uuid,
                    a.fadname,
                    a.fbrand,
                    a.fadclasscode,
                    a.fadowner,
                    a.fadclasscode_v2')
                ->alias('T')
                ->join($samName.' as s ON s.fid=T.fsampleid')
                ->join('tad as a ON a.fadid=s.fadid')
                ->where($where)
                ->order('T.fstarttime ASC')
                ->limit($limitIndex,$pageSize)
                ->select();
        }
        $sql = M($tableName)->getLastSql();

    	foreach ($List as $k=>$row){
            //广告类别全称
    		$List[$k]['fadclass'] = M('tadclass')
    			->cache(true,3600)
    			->where(array('fcode'=>$row['fadclasscode']))
                ->getField('ffullname');
            //地区
    		$List[$k]['fregion'] = M('tregion')
    			->cache(true,3600)
    			->where(array('fid'=>$row['fregionid']))
                ->getField('ffullname');
            //广告主名称
    		$List[$k]['fadownername'] = M('tadowner')
    			->cache(true,3600)
    			->where(array('fid'=>$row['fadowner']))
                ->getField('fname');

            //视频或音频文件链接
    		if ($media_class == '01' || $media_class == '02'){
	    		$List[$k]['favifilename'] = M($sampleTable)
	    			->cache(true,3600)
	    			->where(array($sampleId => $row['fsampleid']))
	    			->getField('favifilename');
    		}
    		//发布时间显示精确到秒
    		$List[$k]['fissuedate'] = date('Y-m-d H:i:s',$row['fstarttime']);
        }
        $dataSet = [
            'count' => $count,
            'data' => $List
        ];
        return $dataSet;
    }

    /**
     * 获取指定媒介指定时间段内广告发布记录及违法情况-暂仅限报纸 TODO:待合并到getAdIssue统一方法
     * @Param $pageIndex页码 $pageSize每页条数 不传参或0则不分页
     */
    public function getAdIssuePaper($fmediaid=0,$fissuedate,$pageIndex=0,$pageSize=0){
        $mediaInfo = M('tmedia')
            ->where(['fid'=>$fmediaid])
            ->find();
        $reg = substr($mediaInfo['media_region_id'],0,2);
        $mediaclass = substr($mediaInfo['fmediaclassid'],0,2);
        $pre = 'tv';
        switch($mediaclass){
            case '01':
                $pre = 'tv';
                break;
            case '02':
                $pre = 'bc';
                break;
            case '03':
                $pre = 'paper';
                break;
            case '13':
                $pre = 'net';
                break;
        }
        $tableName = 't'.$pre.'issue';//表名
        $samName = 't'.$pre.'sample'; //样本表
    	$hasTable = M()->query('SHOW TABLES LIKE "'.$tableName.'"');		//判断表是否存在
    	if (!$hasTable){
    		return false;
        }
        $where = [
            'T.fmediaid' => $fmediaid,
            'T.fissuedate' => $fissuedate,
            'T.fstate' => 1
        ];
        $count = M($tableName)
            ->alias('T')
            ->where($where)
            ->count();
        if($pageIndex == 0 || $pageSize == 0){
            //不分页
            $List = M($tableName)
                ->field('
                    T.fpaperissueid,
                    T.fmediaid,
                    T.fpapersampleid,
                    T.fissuedate,
                    T.fpage,
                    T.fpagetype,
                    T.fsize,
                    T.fissuetype,
                    T.famounts,
                    T.fquantity,
                    T.fissample,
                    T.sourceid,
                    T.fregionid,
                    T.fcreatetime,
                    T.fcreator,
                    s.fjpgfilename,
                    s.fadid,
                    s.fversion,
                    s.fspokesman,
                    s.fillegaltypecode,
                    s.fillegalcontent,
                    s.fexpressioncodes,
                    s.fexpressions,
                    s.fconfirmations,
                    a.fadname,
                    a.fbrand,
                    a.fadclasscode,
                    a.fadowner,
                    a.fadclasscode_v2')
                ->alias('T')
                ->join($samName.' as s ON s.fpapersampleid=T.fpapersampleid')
                ->join('tad as a ON a.fadid=s.fadid')
                ->where($where)
                ->order('T.fpage ASC')
                ->select();
        }else{
            //分页
            $limitIndex = ($pageIndex-1)*$pageSize;
            $List = M($tableName)
                ->field('
                    T.fpaperissueid,
                    T.fmediaid,
                    T.fpapersampleid,
                    T.fissuedate,
                    T.fpage,
                    T.fpagetype,
                    T.fsize,
                    T.fissuetype,
                    T.famounts,
                    T.fquantity,
                    T.fissample,
                    T.sourceid,
                    T.fregionid,
                    T.fcreatetime,
                    T.fcreator,
                    s.fadid,
                    s.fversion,
                    s.fspokesman,
                    s.fillegaltypecode,
                    s.fillegalcontent,
                    s.fexpressioncodes,
                    s.fexpressions,
                    s.fconfirmations,
                    a.fadname,
                    a.fbrand,
                    a.fadclasscode,
                    a.fadowner,
                    a.fadclasscode_v2')
                ->alias('T')
                ->join($samName.' as s ON s.fpapersampleid=T.fpapersampleid')
                ->join('tad as a ON a.fadid=s.fadid')
                ->where($where)
                ->order('T.fpage ASC')
                ->limit($limitIndex,$pageSize)
                ->select();
        }

    	foreach ($List as $k=>$row){
            //广告类别全称
    		$List[$k]['fadclass'] = M('tadclass')
    			->cache(true,3600)
    			->where(array('fcode'=>$row['fadclasscode']))
                ->getField('ffullname');
            //地区
    		$List[$k]['fregion'] = M('tregion')
    			->cache(true,3600)
    			->where(array('fid'=>$row['fregionid']))
                ->getField('ffullname');
            //广告主名称
    		$List[$k]['fadownername'] = M('tadowner')
    			->cache(true,3600)
    			->where(array('fid'=>$row['fadowner']))
                ->getField('fname');
    		//发布时间显示精确到秒
    		$List[$k]['fissuedate'] = date('Y-m-d H:i:s',$row['fstarttime']);
        }
        $dataSet = [
            'count' => $count,
            'data' => $List
        ];
        return $dataSet;
    }

    /**
     * 获取指定报纸版面列表
     */
    public function getPages($fmediaid = 0,$fissuedate = ''){
        if($fmediaid != 0 && $fissuedate != ''){
            $where = [
                'fmediaid' => $fmediaid,
                'fissuedate' => $fissuedate
            ];
            $count = M('tpapersource')
                ->where($where)
                ->count();
            $List = M('tpapersource')
                ->where($where)
                ->order('fpage')
                ->select();
            $dataSet = [
                'count' => $count,
                'data' => $List
            ];
            return $dataSet;
        }else{
            return false;
        }
    }
}
