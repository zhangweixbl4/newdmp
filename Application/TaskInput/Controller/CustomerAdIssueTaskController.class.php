<?php
namespace TaskInput\Controller;

use Think\Exception;

class CustomerAdIssueTaskController extends TraditionAdController
{
    public function _initialize()
    {
		
        parent::_initialize();
        if(!A('InputPc/Task','Model')->user_other_authority('7001')){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'该模块使用数据归档、客户数据交付功能替代'));
        }
    }
    public function index()
    {
		header("Content-type: text/html; charset=utf-8");
		echo '该模块使用数据归档、客户数据交付功能替代';
		exit;
        if (IS_POST){
            $formData = I('where');
            $page = I('page', 1);
            $pageSize = I('pageSize', 10);
            $this->ajaxReturn($this->searchCustomerAdIssueTask([], $formData, $page, $pageSize));

        }else{
            $regionTree = json_encode($this->getRegionTree());
            $mediaClassTree = json_encode($this->getMediaClassTree());
            $mediaLabelList = M('tlabel')
                ->field("flabelid value, CONCAT(flabel,'-', flabelclass) as label")
                ->where([
                    'fstate' => ['EQ', 1]
                ])
                ->cache(true)
                ->select();
            $mediaLabelList = json_encode($mediaLabelList);
            $this->assign(compact('regionTree', 'mediaClassTree', 'mediaLabelList'));
            $this->display();
        }
    }

    private function searchCustomerAdIssueTask($where, $formData, $page, $pageSize)
    {
		$this->ajaxReturn( array('code'=>-1,'msg'=>'该模块使用数据归档、客户数据交付功能替代'));
        if ($formData['wx_id'] != ''){
            $where['wx_id'] = ['EXP', " IN (select wx_id from ad_input_user where alias like '%{$formData['wx_id']}%')"];
        }
        if ($formData['task_date'] != ''){
            $where['task_date'] = ['BETWEEN', $formData['task_date']];
        }
        if ($formData['state'] != ''){
            $where['state'] = ['EQ', $formData['state']];
        }
        if ($formData['remark'] != ''){
            $where['remark'] = ['LIKE', '%'.$formData['remark'].'%'];
        }
        $model = M('customer_ad_issue_task');
        $table = $model
            ->where($where)
            ->order('create_date desc')
            ->page($page, $pageSize)
            ->select();
        $total = $model->where($where)->count();
        $stateMap = [
            '0' => '尚未开始',
            '1' => '正在进行',
            '2' => '完成',
        ];
        foreach ($table as $key => $value) {
            $table[$key]['wx_id'] = M('ad_input_user')->cache(60)->where(['wx_id' => ['EQ', $value['wx_id']]])->getField('alias');
            $table[$key]['state'] = $stateMap[$value['state']];
            // 获取该任务对应的媒体
            /* $mediaIds = M('make_customer_ad_issue')
                ->field('fmediaid')
                ->where([
                    'task_extract_id' => ['EQ', $value['id']]
                ])
                ->buildSql();
            $table[$key]['mediaList'] = M('tmedia')->where(['fid' => ['EXP', ' IN '.$mediaIds]])->cache(true, 60)->getField('fmedianame', true);
			 */
		}
        return compact('total', 'table');
    }
	/*获取任务对应的媒体*/
	public function searchCustomerMedia(){
		$this->ajaxReturn( array('code'=>-1,'msg'=>'该模块使用数据归档、客户数据交付功能替代'));
		$task_extract_id = I('task_extract_id');
		if(!$task_extract_id){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'task_extract_id 错误','mediaList'=>array()));
		}
		$mediaIds = M('make_customer_ad_issue')
                ->field('fmediaid')
                ->where([
                    'task_extract_id' => ['EQ', $task_extract_id]
                ])
                ->buildSql();
        $mediaList = M('tmedia')->cache(true, 60)->where(['fid' => ['EXP', ' IN '.$mediaIds]])->getField('fmedianame', true);
        
		$this->ajaxReturn(array('code'=>0,'msg'=>'','mediaList'=>$mediaList));
		
	}
	

    public function searchMedia()
    {
		$this->ajaxReturn( array('code'=>-1,'msg'=>'该模块使用数据归档、客户数据交付功能替代'));
        $form = I('where');
        $page = I('page',1);
        $pageSize = I('pageSize', 50);
        $task_date = I('task_date');
        $where = [
            'priority' => ['EGT', 0]
        ];
        $mediaOwnerWhere = [];
        if($form['region'] != ''){
            if ($form['onlyThisLevel'] == 1){
                $mediaOwnerWhere["fregionid"] = ['EQ', $form['region']];
            }else{
                $form['region'] = rtrim($form['region'], '0');
                $len = strlen($form['region']);
                $mediaOwnerWhere["LEFT(fregionid, $len)"] = ['EQ', $form['region']];
            }
        }
        if (count($mediaOwnerWhere) != 0){
            $mediaOwnerSubQuery = M('tmediaowner')
                ->field('fid')
                ->where($mediaOwnerWhere)
                ->buildSql();
            $where['fmediaownerid'] = ['EXP', 'IN '.$mediaOwnerSubQuery];
        }
        if ($form['mediaClass'] != ''){
            $len = strlen($form['mediaClass']);
            $where["LEFT(fmediaclassid, $len)"] = ['EQ', $form['mediaClass']];
        }
        if ($form['mediaName'] != ''){
            $where["fmedianame"] = ['LIKE', '%'.$form['mediaName'].'%'];
        }
        if ($form['mediaLabel'] != ''){
            $where["tmedia.fid"] = ['EXP', "IN (select fmediaid from tmedialabel where flabelid = {$form['mediaLabel']})"];
        }
        $list = M('tmedia')
            ->field('tmedia.fid `value`, fmedianame label, IF(task.fstate is not null, 1, 0) as state')
            ->join("left join make_customer_ad_issue task on tmedia.fid = task.fmediaid and task.fdate = '{$task_date}'")
            ->where($where)
            ->page($page, $pageSize)
            ->select();
        $total = M('tmedia')->where($where)->count();
        $this->ajaxReturn([
            'code' => 0,
            'data' => $list,
            'total' => $total,
        ]);
    }

    public function addCustomerAdIssueTask($task)
    {
		$this->ajaxReturn( array('code'=>-1,'msg'=>'该模块使用数据归档、客户数据交付功能替代'));
        $wx_id = session('wx_id');
        $date = date('Y-m-d H:i:s');
        $task['state'] = 1;
        $task['wx_id'] = $wx_id;
        $task['create_date'] = $date;
        $taskModel = M('customer_ad_issue_task');
        $issueModel = M('make_customer_ad_issue');
        M()->startTrans();
        $taskId = $taskModel->add($task);
        if (!$taskId){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '添加任务失败',
            ]);
        }
        $issueData = [];
        // 找出媒体类型
        $mediaIds = M('tmedia')
            ->field('fid, left(fmediaclassid, 2) as media_class')
            ->where([
                'fid' => ['IN', $task['media_ids']]
            ])
            ->select();
        foreach ($mediaIds as $mediaItem) {
            $issueData[] = [
                'fmediaid' => $mediaItem['fid'],
                'fdate' => $task['task_date'],
                'task_extract_id' => $taskId,
                'fcreatetime' => $date,
                'fmediaclass' => $mediaItem['media_class'],
            ];
			A('Admin/FixedMediaData','Model')->fixed($mediaItem['fid'],$task['task_date'],'1','抽取数据时归档','wx_id:'.$wx_id); #归档状态修改
        }
        try{
            $issueAddRes = $issueModel->addAll($issueData);
        }catch (Exception $error){
            M()->rollback();
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '添加媒体发布记录报错, 唯一键引起报错',
            ]);
        }
        if ($issueAddRes){
            M()->commit();
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '添加成功',
            ]);
        }else{
            M()->rollback();
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '添加媒体发布记录失败',
            ]);
        }
    }

    public function getTaskProgress($id)
    {
		$this->ajaxReturn( array('code'=>-1,'msg'=>'该模块使用数据归档、客户数据交付功能替代'));
		session_write_close();
        // 查询任务状态
        $taskState = M('customer_ad_issue_task')->where(['id' => ['EQ', $id]])->getField('state');
        if ($taskState == 100){
            $this->ajaxReturn([
                'data' => 100
            ]);
        }
        // 查找各个状态的数量
        $table = M('make_customer_ad_issue')
            ->field('fstate, count(1) as `count`')
            ->where([
                'task_extract_id' => ['EQ', $id]
            ])
            ->group('fstate')
            ->select();
        $total = 0;
        $completeCount = 0;
        foreach ($table as $key => $value) {
            $total += intval($value['count']);
            if ($value['fstate'] == 1){
                $completeCount = intval($value['count']);
            }
        }
        $res = $completeCount / $total;
        if ($res){
            if ($res !== 1){
                $res *= 100;
                $res = substr(strval($res), 0, 2);
                M('customer_ad_issue_task')
                    ->where([
                        'id' => ['EQ', $id]
                    ])
                    ->save([
                        'state' => 1
                    ]);
            }else{
                M('customer_ad_issue_task')
                    ->where([
                        'id' => ['EQ', $id]
                    ])
                    ->save([
                        'state' => 2
                    ]);
                $res = 100;
            }
        }else{
            $res = 0;
            M('customer_ad_issue_task')
                ->where([
                    'id' => ['EQ', $id]
                ])
                ->save([
                    'state' => 0
                ]);
        }
        $this->ajaxReturn([
            'data' => $res
        ]);
    }
}