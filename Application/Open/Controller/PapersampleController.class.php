<?php
namespace Open\Controller;
use Think\Controller;

class PapersampleController extends BaseController {
	
	public function add_edit_papersample(){

		$papersample_data = file_get_contents('php://input');//获取post数据
		//file_put_contents('LOG/paper',$papersample_data);
		$papersample_data = json_decode($papersample_data,true);//把json转化成数组

		$wx_id = $papersample_data['wx_id'];//获取微信ID
		$nickname = M('ad_input_user')->where(array('wx_id' => $wx_id))->getField('nickname');//获取创建人名称
		if($nickname == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'wx_id错误'));
	
		$a_e_data = array();
		$a_e_data['fmediaid'] = intval($papersample_data['fmediaid']);//媒体ID
		$media_count = M('tmedia')->where(array('fid'=>$a_e_data['fmediaid']))->count();
		if($media_count == 0) $this->ajaxReturn(array('code'=>10016,'msg'=>'添加或修改数据失败,媒介ID有错误'));

		$a_e_data['fissuedate'] = $papersample_data['fissuedate'];//发布日期
		if($a_e_data['fissuedate'] == "") $this->ajaxReturn(array('code'=>10016,'msg'=>'添加或修改数据失败,发布日期有错误'));
		$a_e_data['fissuedate'] = date("Y-m-d H:i:s",strtotime($a_e_data['fissuedate']));

		$fadname = $papersample_data['fadname'];//广告名称
		$fbrand = $papersample_data['fbrand'];//品牌名称
		$fadclasscode = $papersample_data['fadclasscode'];//广告分类code
		if($fadclasscode == '') $fadclasscode = '2301';
		$adowner_name = $papersample_data['adowner_name'];//广告主名称
		
		$adid = A('Adinput/InputTask','Model')->get_ad_id($fadname,$fbrand,$fadclasscode,$adowner_name);//获取广告ID
		
		$a_e_data['fadid'] = intval($adid);//广告ID

		$a_e_data['fversion'] = $papersample_data['fversion'];//版本说明
		$a_e_data['fspokesman'] = $papersample_data['fspokesman'];//代言人
		$a_e_data['fapprovalid'] = $papersample_data['fapprovalid'];//审批号
		$a_e_data['fapprovalunit'] = $papersample_data['fapprovalunit'];//审批单位


		$a_e_data['fillegaltypecode'] = intval($tvsample_data['fillegaltypecode']);//违法类型代码

		if($papersample_data['fjpgfilename'] == "") $this->ajaxReturn(array('code'=>10016,'msg'=>'添加或修改数据失败,图片不能为空'));
		$a_e_data['fjpgfilename'] = $papersample_data['fjpgfilename'];//图片文件名
		if($fadname == '' ) $a_e_data['finputstate'] = 1;//是否需要录入广告信息
		if(intval($papersample_data['fillegaltypecode']) < 0) $a_e_data['finspectstate'] = 1;//是否需要判断违法
		
		
		
		

		$a_e_data['fstate'] = $papersample_data['fstate'];//状态		

		M()->startTrans();//开启事务方法

		$a_e_data['fcreator'] = $nickname;//创建人
		$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$a_e_data['fmodifier'] = $nickname;//修改人
		$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
		$sampleid = M('tpapersample')->add($a_e_data);

		if($sampleid == 0){ 
			M()->rollback();//事务回滚方法
			$this->ajaxReturn(array('code'=>10011,'msg'=>'添加报纸广告样本失败,原因未知'));
		}
		
		
		$illegal = A('Open/Papersample','Model')->papersampleillegal($sampleid,$papersample_data['fexpressioncodes'],$nickname);//添加报纸广告违法表现对应表并获取冗余字段
		
		$illegal['fillegalcontent'] = $papersample_data['fillegalcontent'];//涉嫌违法内容
		if($papersample_data['fillegaltypecode'] != '' &&  intval($papersample_data['fillegaltypecode']) >= 0){
			$illegal['fillegaltypecode'] = $papersample_data['fillegaltypecode'];//违法类型
		}
		M('tpapersample')->where(array('fpapersampleid'=>$sampleid))->save($illegal);//修改违法表现冗余字段
		//var_dump($papersample_data['fexpressioncodes']);


		$issue_list = $papersample_data['issueList'];//发布记录，数组
		if($issue_list == "") $this->ajaxReturn(array('code'=>-1,'msg'=>'未添加发布信息'));
		//file_put_contents('LOG/paper',json_encode($issue_list));
		foreach($issue_list as $ke => $issue){//循环发布记录/*******************************************************开始添加发布记录*/
			$issue_data = array();
			$issue_data['fpapersampleid'] = $sampleid;//样本ID
			$issue_data['fmediaid'] = $a_e_data['fmediaid'];//媒介ID
			$issue_data['fissuedate'] = $a_e_data['fissuedate'];//发布日期
			$issue_data['fpage'] = $issue['page']?$issue['page']:'1';//版面
			$issue_data['fpagetype'] = $issue['pagetype'];//版面类别
			$issue_data['fsize'] = $issue['size'];//规格
			$issue_data['fissuetype'] = $issue['issuetype']?$issue['issuetype']:'黑白';//发布类型
			$issue_data['famounts'] = intval($issue['amounts']);//金额
			$issue_data['fquantity'] = intval($issue['quantity']);//数量
			
			$issue_data['fcreator'] = $nickname;//创建人
			$issue_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$issue_data['fmodifier'] = $nickname;//修改人
			$issue_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			if(intval($issue['issample']) == 1) $issue_data['fissample'] = 1;//是否样本，1是，0不是
				
			$issue_add = A('Open/SampleModel','Model')->add_paperissue($issue_data);//新增电视发布并获取信息
			if($issue_add) $issueList[] = $issue_add;
			
		}/*结束添加发布记录*****************************************************************************************结束添加发布记录*/
		if(count($issueList) == 0){
			M()->rollback();//事务回滚方法
			$this->ajaxReturn(array('code'=>-1,'msg'=>'发布记录全部失败,数据已回滚'));
		}

		M()->commit();//事务提交方法
		$this->ajaxReturn(array('code'=>0,'msg'=>'','issueCount'=>count($issueList),'issueList'=>$issueList));

	}


	public function add_edit_papersample1(){
		$papersample_data = file_get_contents('php://input');//获取post数据
		
		$papersample_data = json_decode($papersample_data,true);//把json转化成数组
		//var_dump($papersample_data);
		$fid = $papersample_data['fid'];//广告样本ID
		
		$a_e_data = array();

		$a_e_data['fmediaid'] = intval($papersample_data['fmediaid']);//媒体ID
		$media_count = M('tmedia')->where(array('fid'=>$a_e_data['fmediaid']))->count();
		if($media_count == 0) $this->ajaxReturn(array('code'=>10016,'msg'=>'添加或修改数据失败,媒体ID有错误'));
		
		$a_e_data['fissuedate'] = date("Y-m-d H:i:s",strtotime($papersample_data['fissuedate']));//发布日期
		
		$a_e_data['fadid'] = $papersample_data['fadid'];//广告ID
		$ad_count = M('tad')->where(array('fadid'=>$a_e_data['fadid']))->count();
		if($ad_count == 0) $this->ajaxReturn(array('code'=>10016,'msg'=>'添加或修改数据失败,广告ID有错误'));
		
		$a_e_data['fversion'] = $papersample_data['fversion'];//版本说明
		
		$a_e_data['fspokesman'] = $papersample_data['fspokesman'];//代言人
		
		$a_e_data['fadlen'] = intval($papersample_data['fadlen']);//样本长度
		$a_e_data['fillegaltypecode'] = intval($papersample_data['fillegaltypecode']);//违法类型代码
		$a_e_data['fillegalcontent'] = $papersample_data['fillegalcontent'];//涉嫌违法内容
		$a_e_data['fexpressioncodes'] = $papersample_data['fexpressioncodes'];//违法表现代码，数组
		
		
		$a_e_data['favifilename'] = $papersample_data['favifilename'];//视频文件名
		$a_e_data['fstate'] = $papersample_data['fstate'];//状态
		
		$person_info = $GLOBALS['person_info'];//获取接口请求人的信息

		
		if($fid == '' ){
			$a_e_data['fcreator'] = $person_info['fname'];//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = $person_info['fname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			$rr = M('tpapersample')->add($a_e_data);
			$illegal = A('Open/papersample','Model')->papersampleillegal($rr,$a_e_data['fexpressioncodes'],$person_info['fname']);//添加报纸广告违法表现对应表并获取冗余字段
			M('tpapersample')->where(array('fid'=>$rr))->save($illegal);//修改违法表现冗余字段
			if($rr > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'添加报纸广告样本成功','fid'=>$rr));
			if($rr == 0) $this->ajaxReturn(array('code'=>10011,'msg'=>'添加报纸广告样本失败,原因未知'));
		}else{
			$a_e_data['fmodifier'] = $person_info['fname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tpapersample')->where(array('fid'=>$fid))->save($a_e_data);
			$illegal = A('Open/papersample','Model')->papersampleillegal($fid,$a_e_data['fexpressioncodes'],$person_info['fname']);//添加报纸广告违法表现对应表并获取冗余字段
			M('tpapersample')->where(array('fid'=>$fid))->save($illegal);//修改违法表现冗余字段
			if($rr > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'修改报纸广告样本成功'));
			if($rr == 0) $this->ajaxReturn(array('code'=>10012,'msg'=>'修改报纸广告样本失败,原因未知'));
		}
		
		
	
	}

	/*获取报纸样本*/
	public function get_papersample(){

		$papersample_data = file_get_contents('php://input');//获取post数据
		$papersample_data = json_decode($papersample_data,true);//把json转化成数组

		$fmediaid = $papersample_data['mediaid'];
		if($fmediaid == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'未选择媒介'));
		$fcreatetime_s = date('Y-m-d H:i:s',strtotime($papersample_data['start_date']));//创建时间 开始
		if($fcreatetime_s == '') $fcreatetime_s = '2015-01-01';
		$fcreatetime_e = date('Y-m-d H:i:s',strtotime($papersample_data['end_date'])+86399);//创建时间 结束
		if($fcreatetime_e == '') $fcreatetime_e = date('Y-m-d H:i:s');

		$where['fmediaid'] = $fmediaid;
		$where['fissuedate'] = array('between',$fcreatetime_s.','.$fcreatetime_e);

		$papersampleList = M('tpapersample')->where($where)->select();
		$count = M('tpapersample')->where($where)->count();

		$this->ajaxReturn(array('code'=>0,'msg'=>'','papersampleList'=>$papersampleList,'count'=>$count));

	}
	
			/*获取报纸样本*/
	public function get_papersample_list(){
		$papersample_data = file_get_contents('php://input');//获取post数据
		
		$papersample_data = json_decode($papersample_data,true);//把json转化成数组
		$p = intval($papersample_data['p']);//需要返回的页数
		if($p == 0) $p = 1;
		$pp = intval($papersample_data['pp']);//需要返回的条数
		if($pp <= 0) $pp = 10;
		if($pp > 100) $pp = 100;
		
		$fcreatetime_s = $papersample_data['fcreatetime_s'];//创建时间 开始
		if($fcreatetime_s == '') $fcreatetime_s = '2015-01-01';
		$fcreatetime_e = $papersample_data['fcreatetime_e'];//创建时间 结束
		if($fcreatetime_e == '') $fcreatetime_e = date('Y-m-d H:i:s');

		$where = array();
		$where['fcreatetime'] = array('between',$fcreatetime_s.','.$fcreatetime_e);
		if($papersample_data['fpapersampleid'] != '') $where['tpapersample.fpapersampleid'] = $papersample_data['fpapersampleid'];//样本ID
		if($papersample_data['fmediaid'] != '') $where['tpapersample.fmediaid'] = $papersample_data['fmediaid'];//媒介ID
		if($papersample_data['fadid'] != '') $where['tpapersample.fadid'] = $papersample_data['fadid'];//广告ID
		if($papersample_data['fillegaltypecode'] != '') $where['tpapersample.fillegaltypecode'] = $papersample_data['fillegaltypecode'];//违法类型
		
		if($papersample_data['fversion'] != '') $where['tpapersample.fversion'] = array('like','%'.$papersample_data['fversion'].'%');//版本说明
		if($papersample_data['fspokesman'] != '') $where['tpapersample.fspokesman'] = array('like','%'.$papersample_data['fspokesman'].'%');//代言人
		
		$count = M('tpapersample')->where($where)->count();
		
		$papersampleList = M('tpapersample')->where($where)->page($p,$pp)->select();

		$this->ajaxReturn(array('code'=>0,'msg'=>'','papersampleList'=>$papersampleList,'count'=>$count));
		

	}
	
	/*获取报纸规格*/
	public function get_papersize(){

		$paper_size = M('tpapersize')->field('fcode,fsize')->where('fstate = 1')->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'','paper_size'=>$paper_size));

	}
	
	/*查询样本详情*/
	public function sampleInfo(){
		
		$post_data = file_get_contents('php://input');//获取post数据
		$post_data = json_decode($post_data,true);//把json转化成数组

		$sample_id = intval($post_data['sample_id']);//样本ID
		if($sample_id == 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'sample_id 不能为空'));
		
		
		$sampleInfo = M('tpapersample')->alias('sample')
										->field('
												    sample.fpapersampleid as sample_id,
													sample.fissuedate as issuedate,
													sample.fversion as version,
													sample.fspokesman as spokesman,
													sample.fapprovalid as approvalid,
													sample.fapprovalunit as approvalunit,
													sample.fillegalcontent as illegalcontent,
													sample.fjpgfilename as jpgfilename,
													
													tmedia.fmedianame as media_name,
													tad.fadname as ad_name,
													tadclass.fadclass as adclass,
													tadclass.ffullname as adclass_fullname,
													tadowner.fname as adowner_name,
													tillegaltype.fillegaltype as illegaltype
													
												')
										->join('tmedia on tmedia.fid = sample.fmediaid')
										->join('tad on tad.fadid = sample.fadid')
										->join('tadclass on tadclass.fcode = tad.fadclasscode')
										->join('tadowner on tadowner.fid = tad.fadowner')	
										->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
										
										->where(array('sample.fpapersampleid'=>$sample_id))->find();
		$illegalList = M('tpapersampleillegal')
												->field('fillegalcode,fexpression,fconfirmation,fpunishment,fpunishmenttype')
												->where(array('fsampleid'=>$sampleInfo['sample_id']))->select();		
		$sampleInfo['illegalList'] = $illegalList;	//违法信息赋值
			
		$this->ajaxReturn(array('code'=>0,'msg'=>'','sampleInfo'=>$sampleInfo));								
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}