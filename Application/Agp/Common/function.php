<?php
//获取参数
function getconfig($cgname = '',$fcustomer = ''){

    if($cgname == 'ALL'){
        $cg = config_array($fcustomer);
    }else{
        $cg = config_array($fcustomer)[$cgname];

        if($cgname == 'web_loginwz' || $cgname == 'web_logowz' || $cgname == 'web_name'){
            $cg = htmlspecialchars_decode($cg);
        }
    }

    return $cg;
}

//获取配置
function config_array($fcustomer = ''){
    header("Content-type: text/html; charset=utf-8");
    $config_array = C('DMOAIN_CONFIG');
    $config_url = [];//定义客户域名数组

    //若强制设定配置,有优先获取
    if(!empty($fcustomer)){
        //如若以上情况都没有、则查询数据库配置
        if(is_numeric($fcustomer)){
            $config_data = M('customer')->cache(true,600)
                ->field("
                cr_id,
                cr_url,
                cr_num
                ")
                ->where(['cr_status'=>1,'cr_id'=>["NEQ",1],'_string'=>'cr_num = "'.$fcustomer.'"'])
                ->order('cr_id')
                ->select();
            if(count($config_data)>1){//同一客户编号存在多域名情况处理
                foreach ($config_data as $key => $value) {
                    if($_SERVER['HTTP_HOST'] == $value['cr_url']){
                        $config_url = $value;
                    }
                }
            }
            $config_url = $config_url?$config_url:$config_data[0];
        }else{
            $config_url = M('customer')->cache(true,600)
                ->field("
                cr_id,
                cr_url,
                cr_num
                ")
                ->where(['cr_status'=>1,'cr_id'=>["NEQ",1],'_string'=>'cr_num = "'.$fcustomer.'" OR cr_url = "'.$fcustomer.'"'])
                ->order('cr_id')
                ->find();
        }
        
    }elseif(C('FORCE_SELF_URL_CONFIG') == $_SERVER['HTTP_HOST']){
        //先从本地配置取
        foreach ($config_array as $config_array_key=>$config_array_val){
            //匹配域名或者客户id
            if($config_array_val['system_num'] == C('FORCE_NUM_CONFIG') || $config_array_key == C('FORCE_NUM_CONFIG')){
                C('GET_DMOAIN_CONFIG',$config_array_val);
                return C('GET_DMOAIN_CONFIG');
            }
        }

        //本地配置没有就去数据库取
        $map['_string'] = 'cr_num = "'.C('FORCE_NUM_CONFIG').'" OR cr_url = "'.C('FORCE_NUM_CONFIG').'"';

        $config_url = M('customer')->field("cr_id,cr_url,cr_num")
            ->where([
                'cr_status'=>1,
                'cr_id'=>["NEQ",1]
            ])
            ->where($map)
            ->order('cr_id')
            ->find();
    } elseif (!empty($config_array[$_SERVER['HTTP_HOST']])){
        //获取当前域名下的配置
        $FORCE_NUM_CONFIG = $config_array[$_SERVER['HTTP_HOST']]['FORCE_NUM_CONFIG'];

        $map['_string'] = 'cr_num = "'.$FORCE_NUM_CONFIG.'" OR cr_url = "'.$FORCE_NUM_CONFIG.'"';

        //查询客户表
        $config_url = M('customer')->field("cr_id,cr_url,cr_num")
            ->where([
                'cr_status'=>1,
                'cr_id'=>["NEQ",1]
            ])
            ->where($map)
            ->order('cr_id')
            ->find();
    } else{
        //如若以上情况都没有、则查询数据库配置
        $config_url = M('customer')->cache(true,600)
            ->field("
            cr_id,
            cr_url,
            cr_num
            ")
            ->where(['cr_status'=>1,'cr_id'=>["NEQ",1],'cr_url'=>$_SERVER['HTTP_HOST']])
            ->order('cr_id')
            ->find();
    }
    if(!empty($config_url)){
        //给当前域名下的用户配置参数
        $config_fields = M('customer_systemset')
            ->cache(true,120)
            ->field("
            cst_name,
            cst_val,
            cst_type
            ")
            ->where(['cst_crid'=>$config_url['cr_id']])
            ->select();
        //查询默认配置
        $default_config_fields = M('customer_systemset')
            ->cache(true,600)
            ->field("
            cst_name,
            cst_val,
            cst_type
            ")
            ->where(['cst_crid'=>1])
            ->select();

        $config_fields[] = ['cst_name' => 'system_num','cst_val' => $config_url['cr_num'], 'cst_type' => 'int'];//客户id

        //循环配置参数
        $config_data = [];//清空数组
        $config_fields_array = [];//参数名称数组
        foreach ($config_fields as $config_key=>$config_value){
            $val = $config_value['cst_val'];
            if($config_value['cst_type'] == 'int'){
                $val = intval($val);
            }
            $config_fields_array[] = $config_value['cst_name'];
            C('GET_DMOAIN_CONFIG.'.$config_value['cst_name'],$val);
        }
        //获取默认配置
        foreach ($default_config_fields as $default_config_key=>$default_config_value){
            if(!in_array($default_config_value['cst_name'],$config_fields_array)){
                $default_val = $default_config_value['cst_val'];
                if($default_config_value['cst_type'] == 'int'){
                    $default_val = intval($default_val);
                }
                C('GET_DMOAIN_CONFIG.'.$default_config_value['cst_name'],$default_val);
            }
        }
    }
    return C('GET_DMOAIN_CONFIG');

}

/**
 * 获取时间条件，$years年份 $timetypes时间分类，$timeval查询时间值，$zd检索时间字段，$isfbstatus是否需要发布确认
 * by zw
 */
function gettimecondition($years = '', $timetypes = '', $timeval = '', $zd = 'fissue_date', $isfbstatus = 0, $isrelease = 0){
    $years      = $years?$years:'';
    $timetypes  = $timetypes;
    $timeval    = $timeval?$timeval:Date("m");
    $zd = $zd?$zd:'fissue_date';
    $where_time =' 1=1 ';
    if(!empty($years)){
        switch ($timetypes) {
          case 0:
            $timese = explode(',',$timeval);
            $starttime  = $timese[0];
            $endtime    = $timese[1];
            $where_time .= ' and date('.$zd.') between "'.$starttime.'" and "'.$endtime.'"';
            break;
          case 10:
            $where_time .=' and year('.$zd.')='.$years;
            $where_time .= ' and weekofyear('.$zd.')='.$timeval;
            break;
          case 20:
            $month = floor($timeval/2);
            $ts = $timeval%2;
            if($ts == 1){
                $starttime  = $years.'-'.($month+1).'-01';
                if(($month+1) == 2){
                    $endtime    = $years.'-'.($month+1).'-14';
                }else{
                    $endtime    = $years.'-'.($month+1).'-15';
                }
                $where_time .= ' and date('.$zd.') between "'.$starttime.'" and "'.$endtime.'"';
            }else{
                if(($month+1) == 2){
                    $starttime  = $years.'-'.$month.'-15';
                }else{
                    $starttime  = $years.'-'.$month.'-16';
                }
                $where_time .= ' and date('.$zd.') between "'.$starttime.'" and LAST_DAY("'.$starttime.'")';
            }
            break;
          case 30:
            $starttime = $years.'-'.$timeval.'-01';
            $endtime = date('Y-m-d',strtotime($starttime.' +1 month -1 day'));
            $where_time .= ' and '.$zd.' between "'.$starttime.'" and "'.$endtime.'"';
            break;
          case 40:
            if($timeval == 1){
                $starttime = $years.'-01-01';
                $endtime = $years.'-03-31';
            }elseif($timeval == 2){
                $starttime = $years.'-04-01';
                $endtime = $years.'-06-30';
            }elseif($timeval == 3){
                $starttime = $years.'-07-01';
                $endtime = $years.'-09-30';
            }else{
                $starttime = $years.'-10-01';
                $endtime = $years.'-12-31';
            }
            $where_time .= ' and '.$zd.' between "'.$starttime.'" and "'.$endtime.'"';
            break;
          case 50:
            if($timeval == 1){
              $starttime  = $years.'-01-01';
              $endtime    = $years.'-06-30';
            }else{
              $starttime  = $years.'-07-01';
              $endtime    = $years.'-12-31';
            }
            $where_time .= ' and '.$zd.' between "'.$starttime.'" and "'.$endtime.'"';
            break;
          case 60:
            $starttime = $years.'-01-01';
            $endtime = $years.'-12-31';
            $where_time .= ' and '.$zd.' between "'.$starttime.'" and "'.$endtime.'"';
            break;
        }
    }

    if(!empty($isrelease)){
        if(!empty($isfbstatus)){
            if($isfbstatus != -1 && $isfbstatus!=-2){
                if($isrelease == 2 && $isfbstatus == 1){
                    $where_time   .= ' and fsend_status in(0,1)';
                }else{
                    $where_time   .= ' and fsend_status='.$isfbstatus;
                }
            }elseif($isfbstatus == -2){
                if($isrelease == 2){
                    $where_time   .= ' and fsend_status in(0,1,2)';
                }else{
                    $where_time   .= ' and fsend_status in (1,2)';
                }
            }
        }else{
            $where_time   .= ' and fsend_status=2';
        }
    }else{
        if(!empty($isfbstatus) && $isfbstatus>0){
            $where_time   .= ' and fsend_status='.$isfbstatus;
        }
    }
    
    return $where_time;
}

/**
 * 获取随机数字
 * @return array|string  $data 数据
 * by zw
 */
function make_num( $length = 8,$types = 0) { 
    $chars1 = '0123456789';
    $chars2 = 'abcdefghijklmnopqrstuvwxyz';
    $chars3 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if($types == 1){
        $chars = $chars1.$chars2;//数字和小写字母
    }elseif($types == 2){
        $chars = $chars1.$chars3;//数字和大写字母
    }elseif($types == 3){
        $chars = $chars2.$chars3;//大小写字母
    }elseif($types == 4){
        $chars = $chars1.$chars2.$chars3;//所有
    }elseif($types == 5){
        $chars = $chars2;//小写
    }elseif($types == 6){
        $chars = $chars3;//大写
    }else{
        $chars = $chars1;
    }
    $num_str = ''; 
    for ( $i = 0; $i < $length; $i++ ) 
    { 
        $num_str .= $chars[ mt_rand(0, strlen($chars) - 1) ]; 
    } 
    return $num_str; 
}

/**
 * 获取发布表名
 * by zw
 */
function gettable($tb,$tm,$regionid) { 
    if(!empty($regionid)){  
        $provinceId     = substr($regionid,0,2);
    }else{
        $provinceId     = substr(session('regulatorpersonInfo.regionid'),0,2);
    }
    if($tb == 'tv'){
        return 'ttvissue_' . date('Ym',$tm) . '_' . $provinceId;
    }elseif($tb == 'bc'){
        return 'tbcissue_' . date('Ym',$tm) . '_' . $provinceId;
    }elseif($tb == 'paper'){
        return 'tpaperissue_' . date('Ym',$tm) . '_' . $provinceId;
    }
}

/**
 * 获取最近七天所有日期，$n获取几天，$d截止时间几天前，默认获取7天，截止日期为昨天
 */
function get_weeks($time = '', $format='Y-m-d',$n = 7,$d = 1){
  $time = $time != '' ? $time : time();
  //组合数据
  $date = [];
  for ($i=0; $i<=$n-1; $i++){
    $date[$i] = date($format ,strtotime( '+' . $i-($n-1+$d) .' days', $time));
  }
  return $date;
}


/**
 * 导入excel文件
 * @param  string $file excel文件路径
 * @param  number $index 从第几行开始
 * @return array        excel文件内容数组
 */
function import_excel($file,$index=1){
    // 判断文件是什么格式
    $type = pathinfo($file);
    $type = strtolower($type["extension"]);
    $extension = $type;
    $type=$type==='csv' ? $type : 'Excel5';

    ini_set('max_execution_time', '0');
    import('Vendor.PHPExcel');
    //Vendor('PHPExcel.PHPExcel');
    // 判断使用哪种格式
    $objReader = PHPExcel_IOFactory::createReader($type);

    if ($extension =='xlsx') {
        $objReader = new PHPExcel_Reader_Excel2007();
        $objPHPExcel = $objReader ->load($file);
    } else if ($extension =='xls') {
        $objReader = new PHPExcel_Reader_Excel5();
        $objPHPExcel = $objReader ->load($file);
    } else if ($extension=='csv') {
        $PHPReader = new PHPExcel_Reader_CSV();

        //默认输入字符集
        $PHPReader->setInputEncoding('GBK');

        //默认的分隔符
        $PHPReader->setDelimiter(',');

        //载入文件
        $objPHPExcel = $PHPReader->load($file);
    }

    $objPHPExcel = $objReader->load($file);

    $sheet = $objPHPExcel->getSheet(0);
    // 取得总行数
    $highestRow = $sheet->getHighestRow();
    // 取得总列数
    $highestColumn = $sheet->getHighestColumn();
    //循环读取excel文件,读取一条,插入一条
    $data=array();
    //从第一行开始读取数据
    for($j=1;$j<=$highestRow;$j++){
        if($j <= $index){
            continue;
        }
        //从A列读取数据
        for($k='A';$k<=$highestColumn;$k++){
            // 读取单元格
            $data[$j][]=$objPHPExcel->getActiveSheet()->getCell("$k$j")->getValue();
        }
    }
    return array_values($data);
}



function get_gjj_label_media(){
    $gjj_label_media_map = [];
    $gjj_label_media_map['tmedialabel.flabelid'] = ['IN',[243,244,245,248]];
    $gjj_label_media_map['tregion.flevel'] = ['IN',[1,2,3]];
    $gjj_label_media = M('tmedialabel')
        ->join("tmedia on tmedia.fid = tmedialabel.fmediaid")
        ->join("tregion on tregion.fid = tmedia.media_region_id")
        ->where($gjj_label_media_map)
        ->group('fmediaid')
        ->getField('fmediaid',true);
    $gjj_label_media = array_unique($gjj_label_media);

    return $gjj_label_media;
}

function get_owner_media_ids($level = -1,$media_type = '',$out_media_type = ''){
    $system_num = getconfig('system_num');
    $map = [];

    if(!empty($media_type)){
        $map['tmedia.fmediaclassid'] = ['like',"$media_type%"];
    }

    if(!empty($out_media_type)){
        $map['tmedia.fmediaclassid'] = ['notlike',"$out_media_type%"];
    }

    if($level != -1){
        if(is_array($level)){
            $map['tregion.flevel'] = ['IN',$level];
        }else{
            $map['tregion.flevel'] = $level;
        }
    }
    $media_ids = M('tmedia_temp')
        ->join("tmedia on tmedia.fid = tmedia_temp.fmediaid")
        ->join("tregion on tregion.fid = tmedia.media_region_id")
        ->where([
            'tmedia_temp.ftype' => 1,
            'tmedia_temp.fcustomer' => $system_num,
            'tmedia_temp.fuserid' => session('regulatorpersonInfo.fid')
        ])
        ->where($map)
        ->getField('tmedia_temp.fmediaid',true);

    return $media_ids;
}

function getimages($str)
{
    $res = preg_match('/<img.+src=\"?(.+\.(jpg|gif|bmp|bnp|png))\"?.+>/i',$str,$match);
    if($res){
        return $match[1];
    }else{
        return false;
    }
}

function arr_page($arr,$p = 1,$count = 10){
    $start = ($p-1)*$count;//设置开始地点
    //循环获取
    for ($i=$start;$i<$start+$count;$i++){

        if (!empty($arr[$i])){

            $new_arr[$i] = $arr[$i];
        }
    }
    //返回
    return $new_arr;
}

    function table_exist($table_name){
        $table_exist = M('')->query('show tables like "'.$table_name.'"');
        if(empty($table_exist)){
            return false;
        }else{
            return true;
        };
    }

    //判断某个链接是否是图片链接by yjn
    function is_img_link($url){
        //图片的链接地址
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); //是否跟着爬取重定向的页面
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //将curl_exec()获取的值以文本流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_HEADER,  1); // 启用时会将头文件的信息作为数据流输出
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); //设置超时时间
        curl_setopt($ch, CURLOPT_NOBODY,true);//设置只获取头信息
        curl_setopt($ch, CURLOPT_URL, $url);  //设置URL
        $content = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);  //curl的httpcode
        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE); //获取头大小
        curl_close($ch);
        $headers = substr($content, 0, $headerSize); //根据头大小截取头信息
        $head_data=preg_split('/\n/',$headers);  //逐行放入数组中
        $head_data = array_filter($head_data);  //过滤空数组
        $headers_arr = [];
        foreach($head_data as $val){  //按:分割开
            list($k,$v) = explode(":",$val); //:前面的作为key，后面的作为value，放入数组中
            $headers_arr[$k] = $v;
        }
        $img_type = explode("/",trim($headers_arr['Content-Type']));  //然后将获取到的Content-Type中的值用/分隔开
        if ($httpcode == 200 && strcasecmp($img_type[0],'image') == 0) {//如果httpcode为200，并且Content-type前面的部分为image，则说明该链接可以访问成功，并且是一个图片类型的
            return true;
        } else {
            return false;
        }


    }


    function get_check_dates(){
        $ischeck = getconfig('ischeck');
        $system_num = getconfig('system_num');
        if(!empty($ischeck)){
            $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
            //如果抽查表有数据
            if(!empty($spot_check_data)){
                $dates = [];//定义日期数组
                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                    $cc_condition = trim($spot_check_data_val['condition']);
                    if(!empty($cc_condition)){
                        $year_month = '';
                        $date_str = [];
                        $year_month = substr($spot_check_data_val['fmonth'],0,7);
                        $date_str = explode(',',$spot_check_data_val['condition']);
                        foreach ($date_str as $date_str_val){
                            $dates[] = $year_month.'-'.$date_str_val;
                        }
                    }

                }
                if(empty($dates)){
                    return [];
                }else{
                    return $dates;
                }
            }else{
                return [];
            }
        }
    }

    //获取用户IP地址
    function getRealIp() {
        $ip=false;
        if(!empty($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
            if ($ip) { array_unshift($ips, $ip); $ip = FALSE; }
            for ($i = 0; $i < count($ips); $i++) {
                if (!eregi ("^(10│172.16│192.168).", $ips[$i])) {
                    $ip = $ips[$i];
                    break;
                }
            }
        }
        return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
    }

    /**
     * 判断密码强弱
     */
    function judgemimaqiangdu($mm) {
        $score = 0;
        if(!empty($mm)){
            $str = $mm;
        } else{
            $str = '';
        }
        if(preg_match("/[0-9]+/",$str))
        {
            $score ++;
        }
        if(preg_match("/[0-9]{3,}/",$str))
        {
            $score ++;
        }
        if(preg_match("/[a-z]+/",$str))
        {
            $score ++;
        }
        if(preg_match("/[a-z]{3,}/",$str))
        {
            $score ++;
        }
        if(preg_match("/[A-Z]+/",$str))
        {
            $score ++;
        }
        if(preg_match("/[A-Z]{3,}/",$str))
        {
            $score ++;
        }
        if(preg_match("/[_|\-|+|=|*|!|@|#|$|%|^|&|(|)]+/",$str))
        {
            $score += 2;
        }
        if(preg_match("/[_|\-|+|=|*|!|@|#|$|%|^|&|(|)]{3,}/",$str))
        {
            $score ++ ;
        }
        if(strlen($str) >= 10)
        {
            $score ++;
        }
        return $score;
    }

    //获取月份差
    function get_month_diff( $date1, $date2, $tags='-' ){
        $date1 = explode($tags,$date1);
        $date2 = explode($tags,$date2);
        return abs($date1[0] - $date2[0]) * 12 + abs($date1[1] - $date2[1]);
    }

    //获取天数差
    function get_day_diff($a,$b){
        if(!is_numeric($a)){
            $a_dt = getdate(strtotime($a));
            $b_dt = getdate(strtotime($b));
        }else{
            $a_dt = getdate($a);
            $b_dt = getdate($b);
        }
        $a_new=mktime(12,0,0,$a_dt['mon'],$a_dt['mday'],$a_dt['year']);
        $b_new=mktime(12,0,0,$b_dt['mon'],$b_dt['mday'],$b_dt['year']);
        return round(abs($a_new-$b_new)/86400)+1;
    }

    function get_tregionlevel($area){
        //判断机构级别，0区级，10市级，20省级，30国家级
        if((int)$area == 500000){
            return 2;
        }
        $region_id_rtrim = rtrim($area,'00');//去掉地区后面两位0，判断区
        $region_id_rtrim = rtrim($region_id_rtrim,'000');//去掉后面三位0，判断全国
        $region_id_rtrim = rtrim($region_id_rtrim,'00');//去掉后面两位0，判断市
        $tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位
        if($tregion_len == 3){
            $tregion_len = 4;
        }elseif($tregion_len == 5){
            $tregion_len = 6;
        }
        return $tregion_len;
    }


    //强制转字符串，报告专用
    function to_string($array){

        if(is_array($array) && !empty($array)){
            foreach ($array as $key => $val){
                foreach ($val as $val_k => $val_v){
                    $array[$key][$val_k] = strval($val_v);
                }
            }
            return $array;
        }else{
            return [];
        }
    }

    //生成报告所需数组
    function create_report_array($type = '',$bookmark = '',$data_class = 'text',$data = ''){
        $array = [
            "type" => $type,
            "bookmark" => $bookmark,
            $data_class => $data
        ];
        return $array;
    }

    /**
     * 数字转换为中文
     * @param  integer  $num  目标数字 yjn
     */
    function number2chinese($num)
    {
        $num = intval($num);
        $char = array('零', '一', '二', '三', '四', '五', '六', '七', '八', '九');
        $unit = ['', '十', '百', '千', '万'];
        $return = '';
        if ($num < 10) {
            $return = $char[$num];
        } elseif ($num%10 == 0) {
            $firstNum = substr($num, 0, 1);
            if ($num != 10) $return .= $char[$firstNum];
            $return .= $unit[strlen($num) - 1];
        } elseif ($num < 20) {
            $return = $unit[substr($num, 0, -1)]. $char[substr($num, -1)];
        } else {
            $numData = str_split($num);
            $numLength = count($numData) - 1;
            foreach ($numData as $k => $v) {
                if ($k == $numLength) continue;
                $return .= $char[$v];
                if ($v != 0) $return .= $unit[$numLength - $k];
            }
            $return .= $char[substr($num, -1)];
        }
        return $return;

    }

    /*
     * 返回地域id末尾不含0的字符串*/
    function wipe_regionid_zero($regionid=''){
        if(empty($regionid)){
            $regionid = session('regulatorpersonInfo.regionid');//默认获取当前登录的客户地域
        }
        $fregionid = rtrim($regionid,0);//去除为0的后缀
        //如果是国家局这种的特殊机构补0
        if(strlen($fregionid) == 1){
            $fregionid = $fregionid.'0';
        }
        return $fregionid;
    }

    /*
    * 接收头信息
    * by zw
    */
     function em_getallheaders()
    {
       foreach ($_SERVER as $name => $value)
       {
           if (substr($name, 0, 5) == 'HTTP_')
           {
               $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
           }
       }
       return $headers;
    }

    /**
    * 判断是否有权限访问
    * by zw
    */
    function Check_QuanXian($modes,$returntype = 'ajax'){
        $is_menujurisdiction = getconfig('is_menujurisdiction');
        if(!empty($is_menujurisdiction)){
            $menujurisdiction = S(session('regulatorpersonInfo.fcode').'_menujurisdiction');
            $jn = false;
            if(is_array($modes)){
                foreach ($modes as $key => $value) {
                    if(in_array($value, $menujurisdiction)){
                        $jn = true;
                        break;
                    }
                }
            }else{
                if(in_array($modes, $menujurisdiction)){
                    $jn = true;
                }
            }
            if($returntype == 'ajax'){
                if(!$jn){
                    echo json_encode(array('code'=>401,'msg'=>'权限不足'));
                    die;
                }
            }else{
                return $jn;
            }
        }
        
    }

    //通过违法表现代码获取法条$data_type 1字符串 2数组
    function get_confirmation_by_code_string($code_string,$separator = ';',$data_type = 1){

        if(!empty(trim($code_string))){
            $codes = explode($separator,$code_string);
            $confirmations = M('tillegal')->where(['fcode'=>['IN',$codes],'fstate'=>1])->getField('fconfirmation',true);
            if(!empty($confirmations)){
                switch ($data_type){
                    case 1:
                        return implode($confirmations,';');
                        break;
                    case 2:
                        return $confirmations;
                        break;
                }
            }
        }
        switch ($data_type){
            case 1:
                return '';
                break;
            case 2:
                return [];
                break;
            default:
                return '';
        }
    }

     /*******文件生成类方法开始*****/
    function word_start() {
        ob_start();
        echo '<html xmlns:o="urn:schemas-microsoft-com:office:office"
        xmlns:w="urn:schemas-microsoft-com:office:word"
        xmlns="http://www.w3.org/TR/REC-html40">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
        table {border-collapse:collapse;border:none;}
        .tb tr td{ background:#ffffff; text-align:center;border:solid #000 1px;}
        </style>';
    }

    function word_save($path) {
        echo "</html>";
        $data = ob_get_contents();

        ob_end_clean();
        word_wirtefile ($path,$data);
    }

    function word_wirtefile ($fn,$data) {
        $fp=fopen($fn,"wa+");
        fwrite($fp,$data);
        fclose($fp);
    }

    
