<?php
//控制器
namespace Admin\Controller;
use Think\Controller;
class VideoCuttingTaskController extends BaseController {

	public function index(){
		
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		
		
		$media_dpid = I('media_dpid');//媒体的平台ID
		if($media_dpid != '') $media_id = intval(M('tmedia')->where(array('fdpid'=>$media_dpid))->getField('fid'));//查询媒体ID

		$Start_s = I('Start_s');//任务时间
		$Start_e = I('Start_e');//任务时间

		if($Start_s == '') $Start_s = '2015-01-01';

		if($Start_e == '') $Start_e = date('Y-m-d');
		
		$task_state = I('task_state');//任务状态:'NULL'正在添加、'0'添加成功、'1'添加失败、'2'待审核、'3'审核通过、'4'审核不通过
		$user_nickname = I('user_nickname');//提交用户
		
		$where = array();
		if($Start_s != '' || $Start_e != ''){
			$where['video_cutting_task.Start'] = array('between',strtotime($Start_s).','.strtotime($Start_e.'23:59:59'));
		}
		
		if($media_id > 0){
			$where['video_cutting_task.Media'] = $media_id;//媒体查询
		}
		if($user_nickname != ''){
			$where['ad_input_user.nickname'] = array('like','%'.$user_nickname.'%');//用户查询
		}
		if($task_state == 'NULL'){//正在添加
			$where['video_cutting_task.Code'] = array('EXP','is null');
		}elseif($task_state == '0'){//添加成功
			$where['video_cutting_task.Code'] = 0;
			$where['video_cutting_task.User'] = array('EXP','is null');
			
		}elseif($task_state == '1'){//添加失败
			$where['video_cutting_task.Code'] = array('gt',0);

		}elseif($task_state == '2'){//待审核
			$where['video_cutting_task.User'] = array('EXP','is not null');
			$where['video_cutting_task.Verify'] = array('EXP','is null');
			
		}elseif($task_state == '3'){//审核通过
			$where['video_cutting_task.Commit'] = array('gt',0);
			
		}
		
		
		
		
		$count = M('video_cutting_task')
										->join('tmedia on tmedia.fid = video_cutting_task.Media')
										->join('ad_input_user on ad_input_user.wx_id = video_cutting_task.User','left')
										->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		
		
		$taskList = M('video_cutting_task')
										->field('video_cutting_task.*,tmedia.fmedianame,ad_input_user.nickname')
										->join('tmedia on tmedia.fid = video_cutting_task.Media')
										->join('ad_input_user on ad_input_user.wx_id = video_cutting_task.User','left')
										->where($where)
										->order('video_cutting_task.Index desc')
										->limit($Page->firstRow.','.$Page->listRows)->select();//查询广告样本列表->select();
		//var_dump(M('video_cutting_task')->getLastSql());
		$this->assign('page',$Page->show());//分页输出
		$taskList = list_k($taskList,$p,$pp);
		$this->assign('taskList',$taskList);
		$this->display();
	}
	
	
	
	/*任务详情*/
	public function ajax_task_details(){
		$Index = I('Index');//任务索引编号
		$taskInfo = M('video_cutting_task')
										->field('video_cutting_task.*,tmedia.fsavepath as media_savepath,tmedia.fmedianame,ad_input_user.nickname')
										->join('tmedia on tmedia.fid = video_cutting_task.Media')
										->join('ad_input_user on ad_input_user.wx_id = video_cutting_task.User','left')
										->where(array('Index'=>$Index))->find();
		$taskInfo['Start_time'] = date('Y-m-d H:i:s',$taskInfo['Start']);
		$taskInfo['Stop_time'] = date('Y-m-d H:i:s',$taskInfo['Stop']);
		$taskInfo['Add_time'] = date('Y-m-d H:i:s',$taskInfo['Add']);
		if($taskInfo['Commit'] > 0) $taskInfo['Commit_time'] = date('Y-m-d H:i:s',$taskInfo['Commit']);
		if($taskInfo['Verify'] > 0) $taskInfo['Verify_time'] = date('Y-m-d H:i:s',$taskInfo['Verify']);

		$taskInfo['Duration'] = s_to_m_time($taskInfo['Duration']/10);
		
		
		if($taskInfo['m3u8_url'] == ''){//判断数据库是否已经存储了播放地址

			$taskInfo['m3u8_url'] = A('Common/Shilian','Model')->get_playback_url($taskInfo['Media'],$taskInfo['Start'],$taskInfo['Stop'],$taskInfo['media_savepath']);//拾联接口查询播放地址
			M('video_cutting_task')->where(array('Index'=>$Index))->save(array('m3u8_url'=>$taskInfo['m3u8_url']));//修改数据库视频播放地址
		}
		$taskInfo['m3u8_url'] = U('Admin/VideoCuttingTask/m3u8',array('m3u8_url'=>sys_auth($taskInfo['m3u8_url'])));
		//$taskInfo['m3u8_url'] = $taskInfo['m3u8_url'];
		$mediaInfo = M('tmedia')->field('fid,fmediaclassid')->where(array('fid'=>$taskInfo['Media']))->find();//媒介信息
		if(substr($mediaInfo['fmediaclassid'],0,2) == '01'){
			$mediaclass_tb = 'tv';
		}elseif(substr($mediaInfo['fmediaclassid'],0,2) == '02'){
			$mediaclass_tb = 'bc';
		}
		
		$originalPartLast = M('t'.$mediaclass_tb.'issue')
										->field('
												DATE_FORMAT(fstarttime,"%H:%i:%s") as Start_time,
												DATE_FORMAT(fendtime,"%H:%i:%s") as Stop_time ,
												timestampdiff(second,"1970-01-01 08:00:00",fstarttime) as Start,
												timestampdiff(second,"1970-01-01 08:00:00",fendtime) as Stop,
												tad.fadname as name
											')
										->join('t'.$mediaclass_tb.'sample on t'.$mediaclass_tb.'sample.fid = t'.$mediaclass_tb.'issue.f'.$mediaclass_tb.'sampleid')	
										->join('tad on tad.fadid = t'.$mediaclass_tb.'sample.fadid')
										->where(array(
														't'.$mediaclass_tb.'issue.fmediaid'=>$taskInfo['Media'],
														't'.$mediaclass_tb.'issue.fstarttime'=>array('lt',date('Y-m-d H:i:s',$taskInfo['Stop'])),//开始时间小于任务的结束时间
														't'.$mediaclass_tb.'issue.fendtime'=>array('gt',date('Y-m-d H:i:s',$taskInfo['Start'])),//结束时间大于任务的开始时间
														))
														->order('t'.$mediaclass_tb.'issue.fstarttime asc')
														->select();
		//var_dump(M('video_cutting_part')->getLastSql());
		
		$partLast = A('Admin/VideoCuttingTask','Model')->filling_part($taskInfo,$originalPartLast);//填充播出片段
		$this->ajaxReturn(array('code'=>0,'msg'=>'','taskInfo'=>$taskInfo,'partLast'=>$partLast));
	}
	
	/*m3u8处理*/
	public function m3u8(){

		$m3u8_url = I('m3u8_url');
		$m3u8 = http(sys_auth($m3u8_url,'DECODE'),'','get',false,10);

		$m3u8_arr = explode("\n",$m3u8);

		foreach($m3u8_arr as $m3u8){
			if(strstr($m3u8,'#EXTINF:') && strstr($m3u8,',')){
				$s = str_replace('#EXTINF:','',$m3u8);
				$s = str_replace(',','',$s);
				echo '#EXTINF:'.(intval($s)+1) . ','."\n";
				//echo $m3u8."\n";
			}else{
				echo $m3u8."\n";
			}
		}

	}
	
	/*添加任务

	*/
	public function task_add(){
		// $this->ajaxReturn(array('code'=>0,'msg'=>'任务已进入创建流程'));
		// $this->ajaxReturn(array('code'=>-1,'msg'=>'任务可能创建失败'));
		$time = time();//	时间标记
		$token = video_cutting_token($time);//	验证令牌
		$media = I('media');//	媒体索引
		$media = M('tmedia')->where(array('fdpid'=>$media))->getField('fid');
		$start = strtotime(I('start'));//	起始时间
		$stop = strtotime(I('stop'));//	结束时间
		if(($stop - $start) >86400 ) $this->ajaxReturn(array('code'=>-1,'msg'=>'任务不能超过一天'));
		$params = array();
		$params['time'] = $time;
		$params['token'] = $token;
		$params['media'] = $media;
		$params['start'] = $start;
		$params['stop'] = $stop;
		//var_dump($params);
		//exit;
		$add_url = C('VIDEO_CUTTING_URL').'interface/admin_task_add.php';//添加任务的接口地址
		
		$rr = http($add_url,$params,'POST',false,2);
		//var_dump($rr);
		//exit;
		$rr = json_decode($rr,true);
		if($rr === null){
			$is_success = 0;
			while(intval($is_success) == 0 && $nu < 5){
				$is_success = M('video_cutting_task')->where(array('Media'=>$media,'Start'=>$start,'Stop'=>$stop))->count();//查询数据库是否添加成功
				usleep(400);
				$nu++;
			}
			
			if($is_success == 1){
				$this->ajaxReturn(array('code'=>0,'msg'=>'任务已进入创建流程'));
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'任务可能创建失败'));
			}
		
		}

		$code[0] =	'添加任务成功';
		$code[1] =	'时间标记非法';
		$code[2] =	'验证令牌非法';
		$code[3] =	'媒体索引非法';
		$code[4] =	'起始时间非法';
		$code[5] =	'结束时间非法';
		$code[6] =	'验证密钥非法';
		$code[7] =	'起始结束非法';
		$code[8] =	'设置超时失败';
		$code[9] =	'连接数据失败';
		$code[10] =	'查询数据失败';
		$code[11] =	'没有媒体信息';
		$code[12] =	'查询数据失败';
		$code[13] =	'没有设备信息';
		$code[14] =	'插入数据失败';
		$code[15] =	'初始传输失败';
		$code[16] =	'设置传输失败';
		$code[17] =	'请求传输失败';
		$code[18] =	'获取地址失败';
		$code[19] =	'执行命令失败';
		$code[20] =	'获取起始失败';
		$code[21] =	'获取结束失败';
		$code[22] =	'执行命令失败';
		$code[23] =	'获取持续失败';
		$code[24] =	'更新数据失败';
		$code[25] =	'执行命令失败';
		$code[26] =	'执行命令失败';
		$code[27] =	'执行命令失败';
		$code[28] =	'执行命令失败';
		$code[29] =	'更新数据失败'; 
		$this->ajaxReturn(array('code'=>$rr['Code'],'msg'=>$code[$rr['Code']]));
	}
	
	
	/*删除任务*/
	public function task_delete(){
		$time = time();//	时间标记
		$token = video_cutting_token($time);//	验证令牌

		$task = I('task');//任务索引
		$params = array();
		$params['time'] = $time;
		$params['token'] = $token;
		$params['task'] = $task;
		$taskInfo = M('video_cutting_task')->where(array('Index'=>$task))->find();//查询任务信息
		if($taskInfo['Code'] == '' && (time() - intval($taskInfo['Add'])) < 86400){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'当前状态不允许删除'));
		}
		$delete_url = C('VIDEO_CUTTING_URL').'interface/admin_task_delete.php';

		$rr = http($delete_url,$params,'POST',false,10);
		//var_dump($rr);
		$rr = json_decode($rr,true);
		
		$code[0] =	'删除任务成功';
		$code[1] =	'时间标记非法';
		$code[2] =	'验证令牌非法';
		$code[3] =	'任务索引非法';
		$code[4] =	'验证密钥非法';
		$code[5] =	'连接数据失败';
		$code[6] =	'设置数据失败';
		$code[7] =	'查询数据失败';
		$code[8] =	'没有任务信息';
		$code[9] =	'删除数据失败';
		$code[10] =	'提交数据失败';
		//var_dump($rr);
		$this->ajaxReturn(array('code'=>$rr['Code'],'msg'=>$code[$rr['Code']]));
	}
	
	/*审核任务*/
	public function task_verify(){
		
		$task = I('task');//任务索引
		$approve = I('approve');//通过状态，通过true，不通过false
		$taskInfo = M('video_cutting_task')->where(array('Index'=>$task))->find();
		if($taskInfo['Commit'] == null){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该任务还未提交'));
		}
		if($taskInfo['Verify'] > 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该任务已审核'));
		}
		
		
		if($approve == 'false'){//审核不通过
			$rr = M('video_cutting_task')->where(array('Index'=>$task))->save(array('Commit'=>null,'User'=>null));
			if($rr > 0){
				$this->ajaxReturn(array('code'=>0,'msg'=>'已重置任务状态'));
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'重置任务失败'));
			}
		}
		
		
		if($approve == 'true'){//审核通过

			$time = time();//	时间标记
			$token = video_cutting_token($time);//	验证令牌

			$params = array();
			$params['time'] = $time;
			$params['token'] = $token;
			$params['task'] = $task;

			$verify_url = C('VIDEO_CUTTING_URL').'interface/admin_task_verify.php';
			
			$task_info = M('video_cutting_task')->where(array('Index'=>$task))->find();//任务信息
	
			$media_info = M('tmedia')->where(array('fid'=>$task_info['Media']))->find();//媒介信息

			
			
			$rr = http($verify_url,$params,'POST',false,1);
			$rr = json_decode($rr,true);
			
			$code[0] =	'审核任务成功';
			$code[1] =	'时间标记非法';
			$code[2] =	'验证令牌非法';
			$code[3] =	'任务索引非法';
			$code[4] =	'验证密钥非法';
			$code[5] =	'连接数据失败';
			$code[6] =	'设置数据失败';
			$code[7] =	'查询数据失败';
			$code[8] =	'没有任务信息';
			$code[9] =	'还未提交任务';
			$code[10] =	'已经审核任务';
			$code[11] =	'更新数据失败';
			$code[12] =	'提交数据失败';
			
			$this->ajaxReturn(array('code'=>0,'msg'=>'任务审核已经提交,服务器处理大概需要10秒钟,你可以继续处理其它任务'));
		}
		
		
		
	}
	
	/*导入数据到我们的数据表*/
	public function leading_in_issue(){
		
		$rr = A('Admin/VideoCuttingTask','Model')->leading_in_issue();
		$this->ajaxReturn(array('code'=>0,'msg'=>'导入了'.$rr['in'].'条记录,删除了'.$rr['de'].'条记录'));
		
	}
	
	/*剪辑样本管理*/
	public function sample(){
		
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$Add_s = I('Add_s');//创建时间
		$Add_e = I('Add_e');//创建时间
		if($Add_s == '') $Add_s = '2015-01-01';
		if($Add_e == '') $Add_e = date('Y-m-d');
		$media_dpid = I('media_dpid');//媒体的平台ID
		if($media_dpid != '') $media_id = intval(M('tmedia')->where(array('fdpid'=>$media_dpid))->getField('fid'));//查询媒体ID
		$user_nickname = I('user_nickname');//提交用户
		$keyword = I('keyword');
		
		$where = array();
		if($keyword != '') $where['video_cutting_sample.Name|tad.fadname'] = array('like','%'.$keyword.'%');
		if($Add_s != '' || $Add_e != ''){
			$where['video_cutting_sample.Add'] = array('between',strtotime($Add_s).','.strtotime($Add_e.'23:59:59'));
		}
		
		if($media_id > 0){
			$where['video_cutting_sample.Media'] = $media_id;//媒体查询
		}
		if($user_nickname != ''){
			$where['ad_input_user.nickname'] = array('like','%'.$user_nickname.'%');//用户查询
		}
		$count = M('video_cutting_sample')->join('tad on tad.fadid = video_cutting_sample.fadid')
										->join('tmedia on tmedia.fid = video_cutting_sample.Media')
										->join('ad_input_user on ad_input_user.wx_id = video_cutting_sample.User','left')
										->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		
		$sampleList = M('video_cutting_sample')
												->field('video_cutting_sample.*,tad.fadname,tmedia.fmedianame,ad_input_user.nickname')
												->join('tad on tad.fadid = video_cutting_sample.fadid')
												->join('tmedia on tmedia.fid = video_cutting_sample.Media')
												->join('ad_input_user on ad_input_user.wx_id = video_cutting_sample.User','left')
												->where($where)
												->order('video_cutting_sample.Index desc')
												->limit($Page->firstRow.','.$Page->listRows)
												->select();
		$sampleList = list_k($sampleList,$p,$pp);										
		$this->assign('page',$Page->show());//分页输出									
		$this->assign('sampleList',$sampleList);										
		$this->display();
	}
	
	/*样本添加*/
	public function sample_add(){
		
		$time = time();//	时间标记
		$token = video_cutting_token($time);//	验证令牌
		$media = I('media');//	媒体索引

		$media = intval(M('tmedia')->where(array('fdpid'=>$media))->getField('fid'));//查询媒体ID
		//var_dump($media);
		$address = I('address');//	样本地址
		$name = I('name');//	样本名称
		$start = time() - 7200;//	最早时间
		$stop = time() - 3600;//	最晚时间

		$params = array();
		$params['time'] = $time;
		$params['token'] = $token;
		$params['media'] = $media;
		$params['address'] = $address;
		$params['name'] = $name;
		$params['start'] = $start;
		$params['stop'] = $stop;
		
		$add_url = C('VIDEO_CUTTING_URL').'interface/admin_sample_add.php';
		
		$rr = http($add_url,$params,'POST',false,10);
		$rr = json_decode($rr,true);

		$code[0] =	'添加样本成功';
		$code[1] =	'时间标记非法';
		$code[2] =	'验证令牌非法';
		$code[3] =	'媒体索引非法';
		$code[4] =	'样本地址非法';
		$code[5] =	'样本名称非法';
		$code[6] =	'最早时间非法';
		$code[7] =	'最晚时间非法';
		$code[8] =	'验证密钥非法';
		$code[9] =	'起始结束非法';
		$code[10] =	'设置超时失败';
		$code[11] =	'连接数据失败';
		$code[12] =	'查询数据失败';
		$code[13] =	'没有媒体信息';
		$code[14] =	'插入数据失败';
		$code[15] =	'请求传输失败';
		$code[16] =	'获取地址失败';
		$code[17] =	'执行命令失败';
		$code[18] =	'写入文件失败';
		$code[19] =	'更新数据失败';

		$this->ajaxReturn(array('code'=>$rr['Code'],'msg'=>$code[$rr['Code']]));
		
	}
	
	/*样本删除*/
	public function sample_delete(){
		
		$Index = I('Index');
		$time = time();//	时间标记
		$token = video_cutting_token($time);//	验证令牌

		$params = array();
		$params['time'] = $time;
		$params['token'] = $token;
		$params['sample'] = $Index;

		$delete_url = C('VIDEO_CUTTING_URL').'interface/admin_sample_delete.php';
		
		$rr = http($delete_url,$params,'POST',false,10);
		//var_dump($delete_url);
		$rr = json_decode($rr,true);
		

		$code[0] =	'删除样本成功';
		$code[1] =	'时间标记非法';
		$code[2] =	'验证令牌非法';
		$code[3] =	'样本索引非法';
		$code[4] =	'验证密钥非法';
		$code[5] =	'连接数据失败';
		$code[6] =	'删除数据失败';
		$code[7] =	'没有样本信息';

		
		$this->ajaxReturn(array('code'=>$rr['Code'],'msg'=>$code[$rr['Code']]));
	}
	
	/*删除所有样本，此操作危险*/
	public function delete_all_sample(){
		$sampleList = M('video_cutting_sample')->select();
		
		foreach($sampleList as $sample){
			$this->sample_delete($sample['Index']);
		}
	}
	
	
	
	
	

	
	
	
	
}