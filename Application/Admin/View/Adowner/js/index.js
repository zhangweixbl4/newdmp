Vue.prototype.$ELEMENT = { size: "mini", zIndex: 3000 };
const vm = new Vue({
  el: '#app',
  mixins: [listMixin],
  components: {adOwnerDetail},
  data() {
    return {
      searchForm: {},
      mainAdOwner: '',
      listUrl: '/Admin/Adowner/getList',

      adOwnerForm: {
        fid: '',
        fname: '',
        fregionid: '',
        faddress: '',
        flinkman: '',
        ftel: '',
        fmail: '',
        fstate: '',
      },
      activeRow: {},

      multipleSelection: [],
      showMerge: false,
      showAdOwnerInfo: false,
      clickflag: false,
      wrapperClosable: true,

      regionTree: [],
    }
  },
  computed: {
    canMerge() {
      return this.multipleSelection.length >= 2
    }
  },
  created() {
    this.getRegionTree()
  },
  methods: {
    addAdOwver() {
      this.activeRow = {}
      this.showAdOwnerInfo = true
      this.$nextTick(() => {
        this.$refs.detail.addAdOwver()
      })
    },
    submitMerge() {
      let data = {
        fids: this.multipleSelection.map(i => i.fid).join(','),
        mfid: this.mainAdOwner,
      }
      $fly.post('/Admin/Adowner/mergeAdowner', data).then(res => {
        $h.notify.success(res.msg)
        this.beforeClose()
        this.resetTable()
        this.getList(this.page.pageIndex)
      })
    },
    beforeClose() {
      this.showMerge = false
    },
    getRegionTree() {
      $fly.post('/Open/Region/getRegionTree').then(res => {
        this.regionTree = [
          {
            fpid: '0',
            label: '全国',
            value: '100000',
            children: res.data
          }
        ]
      })
    },
    rowClick(row, column, event) {
      if (this.clickflag) {
        this.clickflag = clearTimeout(this.clickflag)
      }
      this.clickflag = setTimeout(() => {
        this.$refs.table.toggleRowSelection(row)
      },300)
    },
    openAdOwnerInfo(row) {
      if (this.clickflag) {
        this.clickflag = clearTimeout(this.clickflag)
      }
      this.activeRow = row
      this.showAdOwnerInfo = true
    },
    closeAdOwnerInfo(reload = false) {
      this.showAdOwnerInfo = false
      this.activeRow = {}
      if(reload) this.getList(this.page.pageIndex)
    },
    resetTable() {
      this.mainSample = ''
      this.$refs.table.clearSelection()
      this.multipleSelection = []
    },
  },
})