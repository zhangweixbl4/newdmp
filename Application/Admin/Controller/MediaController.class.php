<?php
namespace Admin\Controller;
use Think\Controller;
class MediaController extends BaseController {
	
	
	

    public function index(){
    	if(A('Admin/Authority','Model')->authority('200104') === 0){
			exit('没有权限');//验证是否有新增权限
		}
    	// 检查是否有替换源的权限
        if(A('Admin/Authority','Model')->authority('200107') === 0){
            $replaceSourceAuth = 0;
        }else{
            $replaceSourceAuth = 1;
        }
        $this->assign('replaceSourceAuth',$replaceSourceAuth);
		$p = I('p',1);//当前第几页
		$pp = 36;//每页显示多少记录
		$keyword = I('get.keyword');//搜索关键词
		$region_id = I('region_id');//地区ID
		$this_region_id = I('this_region_id');//是否只查询本级地区
		//if($region_id == 100000) $region_id = 0;//如果传值等于100000，相当于查全国
		$mediaowner_id = I('mediaowner_id');//媒介机构ID
		$mediaclass = I('mediaclass');//媒介类型
		$fdeviceid = I('fdeviceid');//设备ID
		$fcollectedid = I('fcollectedid');//采集点
		$abnormal_code = I('abnormal_code');//异常状态
		$priority = I('priority');//优先级
		$group_id = I('group_id');//分组
		$collecttype = I('collecttype');//采集方式
		$medialabel = I('medialabel');//媒介标签
		$where = array();//查询条件
		if($medialabel != ''){//是否搜索媒介标签
			
			$medialabel_where = array();//媒介标签包含的媒介id查询条件初始化
			
			$medialabel_where['tlabel.flabel'] = str_replace('#','',$medialabel);//查询条件，去掉井号
			$mediaIdList = M('tmedialabel')
										->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
										->where($medialabel_where)
										->getField('fmediaid',true);//查询媒介标签包含的媒介id
			if(strstr($medialabel,'#')){//是否包含井号，包含逗号即表示搜索非该标签媒介
				$mediaId_where = 'notin';
			}else{
				$mediaId_where = 'in';
			}
			
			if($mediaIdList){
				$where['tmedia.fid'] = array($mediaId_where,$mediaIdList);	
			}else{
				$where['tmedia.fid'] = array($mediaId_where,array(0));
			}	
		}//查询媒介标签包含的媒介id结束***************************************************************
		
		$where['tmedia.fstate'] = array('egt',0);
		if(I('fstate') != ''){
			$where['tmedia.fstate'] = I('fstate');
		}
		
		if($collecttype != ''){
			$where['tmedia.fcollecttype'] = $collecttype;
		}
		
		if($priority){
			
			$where_priority = intval(str_replace(array('=',"&lt;","&gt;"),array('','',''),$priority));//去掉=<>,然后转为数字
			
			if(strstr($priority,"&lt;")){
				
				$where['tmedia.priority'] = array('lt',$where_priority);
			}elseif(strstr($priority,"&gt;")){
				$where['tmedia.priority'] = array('gt',$where_priority);
			}else{
				$where['tmedia.priority'] = $where_priority;
			}
			
		}
		if(!empty($group_id) || $group_id===0){
			$where['tmedia.group_id'] = $group_id;
		}
		//$where['tmedia.priority'] = 0;
		
		if($abnormal_code != ''){
			$where['abnormal_code'] = $abnormal_code;
		}
		if($mediaclass){
			$where['left(tmedia.fmediaclassid,2)'] = array('in',$mediaclass);
		}
		if($mediaowner_id != ''){
			$where['tmediaowner.fid'] = $mediaowner_id;
		}

		if($region_id > 0){
			if($this_region_id == 'true'){
				$where['tmediaowner.fregionid'] = $region_id;//地区搜索条件
			}elseif($region_id != 100000){
				$region_id_rtrim = A('Common/System','Model')->get_region_left($region_id);//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmediaowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}
		}
		$region_authority = A('Admin/Authority','Model')->region_authority();//获取地区权限

		if($region_authority){
			$where['left(tcollect.fregionid,2)'] = array('in',$region_authority);
		}
		if($keyword != ''){
			$where['tmedia.fmedianame|tmedia.fid|tmedia.fdpid'] = array('like','%'.$keyword.'%');
		} 
		if($fdeviceid != ''){
			$where['tmedia.fdeviceid'] = $fdeviceid;
		}
		if($fcollectedid != ''){
			$where['tcollect.fid'] = $fcollectedid;
		}
		$count = M('tmedia')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid','left')//连接媒介机构
							->join('tdevice on tdevice.fid = tmedia.fdeviceid','left')//连接设备表
							->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')//连接媒介类型表
							->join('tcollect on tcollect.fid = tdevice.fcollectid')
							->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$mediaList = M('tmedia')
								->field('
										tmedia.*,
										left(tmedia.fmediaclassid,2) as media_classid,
										tdevice.fname as device_name,
										tmediaclass.fclass as mediaclass_name,
										tmediaclass.icon as mediaclass_icon
										')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid','left')//连接媒介机构
								->join('tdevice on tdevice.fid = tmedia.fdeviceid','left')//连接设备表
								->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')//连接媒介类型表
								->join('tcollect on tcollect.fid = tdevice.fcollectid')
								->where($where)->limit($Page->firstRow.','.$Page->listRows)
								->order('left(tmedia.fmediaclassid,2),tmedia.priority desc,tmedia.fid desc')
								
								->select();//查询媒介列表
		//var_dump(M('tmedia')->getLastSql());						
		$mediaList = list_k($mediaList,$p,$pp);//为列表加上序号
		foreach($mediaList as $key => $media){
			$mediaList[$key] = $media;
			$mediaList[$key]['savepath'] = U('Admin/Media/paper_media',array('url'=>sys_auth($media['fsavepath'])));
		}
					
		//var_dump($mediaList);
		$collecttypeList = M('tcollecttype')->select();//采集方式
		$mediaclassList = M('tmediaclass')->field('fid,fclass,icon')->where(array('fpid'=>''))->select();
		// 分组
        $fields = 'group_id,group_name';
        $filter = ['group_name' => ['LIKE','数据_%组']];
        $groupList = A('Common/Tag','Model')->getList($fields,$filter);
		$this->assign('mediaList',$mediaList);//媒介列表
		$this->assign('collecttypeList',$collecttypeList);//采集方式列表
		$this->assign('mediaclassList',$mediaclassList);//媒介类型列表
		$this->assign('groupList',$groupList);//媒介类型列表
		
		/*以下两行代码注释是因为不再DMP不再提交媒介异常*/
		//$media_abnormal_type_list = M('media_abnormal_type')->select();//
		//$this->assign('media_abnormal_type_list',$media_abnormal_type_list);//媒介异常类型列表
		

		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->assign('page',$Page->show());//分页输出

		$this->display();

	}

	//获取采集方式下的设备
	public function get_media_device(){

		$name = I('name');
		$where['tdevice.fstate'] = 1;

		if($name) $where['tdevice.fname'] = array('like','%'.trim($name,' ').'%');

		$device = M('tdevice')->field('tdevice.*')->where($where)->limit(10)->select();

		$this->ajaxReturn(array('code'=>0,'device'=>$device));

	}

	/*查看更多*/
	public function index2(){

		$this->display();

	}

	public function detials(){

		$fid = I('fid');//媒体ID
		$media = M('tmedia')->where(array('fid' => $fid))->find();//媒体详情
		//var_dump($media);
		if($media['fcollecttype'] == 24 ){
			$live_m3u8 = U('Api/Media/flv_live',array('flv'=>urlencode($media['live_m3u8'])));//直播地址
		}else{
			$live_m3u8 = U('Api/Media/m3u8_bc_live',array('mediaId'=>$media['fid'],'volume'=>100));//直播地址;
		}
		

		$ad = array();
		//判断媒体类别 查询最近播出广告
		if(substr($media['fmediaclassid'],0,2) == 01){
			$ad = M('ttvsample')->where(array('fmediaid' => $fid))->order('fissuedate desc')->limit(8)->select();
		}elseif(substr($media['fmediaclassid'],0,2) == 02){
			$ad = M('tbcsample')->where(array('fmediaid' => $fid))->order('fissuedate desc')->limit(8)->select();
		}elseif(substr($media['fmediaclassid'],0,2) == 03){
			$ad = M('tpapersample')->where(array('fmediaid' => $fid))->order('fissuedate desc')->limit(8)->select();
		}

		$this->assign('ad',$ad);
		$this->assign('live_m3u8',$live_m3u8);
		$this->display();

	}

	/*获取电视*/
	public function ajax_tv_media(){

		if(I('keyword')) $where['fmedianame'] = array('like','%'.trim(I('keyword'),' ').'%');
		$fmediaclassid = I('fmediaclassid');//电视级别
		$where['left(fmediaclassid,'.strlen($fmediaclassid).')'] = $fmediaclassid;
		$where['fstate'] = 1;

		$tv_media = M('tmedia')->field('fid,fmedianame,live_m3u8,fcollecttype,fjpgfilename')->where($where)->order('fmedianame asc')->select();
		foreach($tv_media as $key => $tv){
			$tv_media[$key]['detials'] =  U('Admin/Media/detials',array('fid'=>$tv['fid']));//直播地址
		}
		$this->ajaxReturn(array('code'=>0,'media'=>$tv_media));

	}

	/*获取广播*/
	public function ajax_bc_media(){

		if(I('keyword')) $where['fmedianame'] = array('like','%'.trim(I('keyword'),' ').'%');
		$where['left(fmediaclassid,2)'] = 02;
		$where['fstate'] = 1;

		$bc_media = M('tmedia')->field('fid,fmedianame,live_m3u8,fcollecttype')->where($where)->select();
		foreach($bc_media as $key => $bc){
			$bc_media[$key]['detials'] =  U('Admin/Media/detials',array('fid'=>$mediaDetails['fid']));//直播地址
			if($bc['fcollecttype'] == 24) $bc_media[$key]['live_url'] = U('Api/Media/flv_live',array('flv'=>urlencode($bc['live_m3u8'])));//直播地址
		}
		$this->ajaxReturn(array('code'=>0,'media'=>$bc_media));

	}

	/*获取报纸*/
	public function ajax_paper_media(){

		if(I('keyword')) $where['fmedianame'] = array('like','%'.trim(I('keyword'),' ').'%');
		$where['left(fmediaclassid,2)'] = 03;
		$where['fstate'] = 1;

		$paper_media = M('tmedia')->field('fid,fmedianame,fcollecttype,fsavepath')->where($where)->select();
		foreach ($paper_media as $key => $media) {
			$mediaList[$key] = $media;
			$mediaList[$key]['savepath'] = U('Admin/Media/paper_media',array('url'=>sys_auth($media['fsavepath'])));
		}

		$this->ajaxReturn(array('code'=>0,'media'=>$mediaList));

	}
	
	/*电视、广播媒体直播*/
	public function live(){
		$this->display();
	}
	
	/*素材补充*/
	public function media_source_supply(){
		$postDataStr = file_get_contents('php://input');
		$postDataArr = json_decode($postDataStr,true);
		$fmediaid = $postDataArr['fmediaid'];
		$video_path = $postDataArr['video_path'];
		$fstarttime = strtotime($postDataArr['fstarttime']);
		$fdpid = M('tmedia')->cache(true,60)->where(array('fid'=>$fmediaid))->getField('fdpid');
		
		
		
		$fid = M('media_source_supply')->add(array('fdpid'=>$fdpid,'fstarttime'=>$fstarttime,'fmediaid'=>$fmediaid,'video_path'=>$video_path));
		
		if($fid){
			$this->ajaxReturn(array('code'=>0,'msg'=>''));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败'));
		}
		
		
	}
	
	/*添加、编辑媒介*/
	public function add_edit_media(){
		
		session_write_close();
		$fid= I('fid');//设备ID
		
		if(!$fid && !in_array(intval(session('personInfo.fid')),array(108,46,131)) && in_array(substr(I('fmediaclassid'),0,2),array('01','02','03'))){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'没有新增媒介的权限'));
		}

		
		
		$a_e_data = array();
		$a_e_data['fmediaclassid'] = I('fmediaclassid');//媒介类别id
		$a_e_data['watermark'] = I('watermark');//自定义水印
		
		$a_e_data['fmediaownerid'] = I('fmediaownerid');//媒介机构ID
		$a_e_data['fdeviceid'] = I('fdeviceid');//采集设备ID
		$a_e_data['fmediacode'] = I('fmediacode');//媒介代码
		$a_e_data['fmedianame'] = I('fmedianame');//媒介名称
		$a_e_data['faddr'] = I('faddr');//媒介地址
		$a_e_data['fpostcode'] = I('fpostcode');//邮编
		$a_e_data['main_media_id'] = I('main_media_id');//主通道id
		if($a_e_data['main_media_id'] && M('tmedia')->where(array('fid'=>$a_e_data['main_media_id']))->count() == 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'主媒体id错误'));
		}
		if(mb_strlen($a_e_data['fpostcode']) > 6) $this->ajaxReturn(array('code'=>-1,'msg'=>'邮政编码错误'));//返回邮政编码错误
		$a_e_data['femail'] = I('femail');//电子邮件
		$a_e_data['fcollecttype'] = intval(I('fcollecttype'));//采集方式
		$a_e_data['fliveparam'] = I('fliveparam');//直播参数
		$a_e_data['firparam'] = I('firparam');//红外参数
		$a_e_data['fsavepath'] = I('fsavepath');//保存位置
		$a_e_data['fchannelid'] = I('fchannelid');//通道号
		$a_e_data['auto_inspection'] = I('auto_inspection');//是否自动巡检
		$a_e_data['expire_date'] = I('expire_date');//到期时间
		$a_e_data['valid_start_time'] = I('valid_start_time');//有效开始时间，用于停台时间段标识
		$a_e_data['valid_end_time'] = I('valid_end_time');//有效结束时间，用于停台时间段标识
		$a_e_data['save_days'] = I('save_date');//保存天数
		$a_e_data['auto_ext_day_ago'] = intval(I('auto_ext_day_ago'));//自动抽取多少天前的数据
		
		$a_e_data['live_m3u8'] = I('live_m3u8','','');
		
			
		if(	$a_e_data['save_days'] != 90 && $a_e_data['fsavepath'] != 'q'){
			$a_e_data['fsavepath'] = 'q';
		}

		if( $fid == 0 || A('Admin/Authority','Model')->authority('200105')){//判断修改媒介的扩展字段权限
			$a_e_data['priority'] = intval(I('priority'));//优先级
		}
		if($a_e_data['priority'] >= 0 && $a_e_data['auto_ext_day_ago'] == 0){
			$a_e_data['auto_ext_day_ago'] = 4;
		}
		
		$a_e_data['fjpgfilename'] = I('fjpgfilename');//台标图片路径
		$a_e_data['group_id'] = I('group_id');//分组ID
		$a_e_data['remark'] = I('remarks');//备注
		$a_e_data['m3u8_backup'] = $_POST['m3u8_backup'];//备用频道片段
		$media_label = array_unique(I('media_label'));//媒介标签，去重
		
		
		if(strstr($a_e_data['fmedianame'],'广播') && substr($a_e_data['fmediaclassid'],0,2) != '02'){
			//$this->ajaxReturn(array('code'=>01,'msg'=>'媒介类别选择错误'));
		}
		
		if(strstr($a_e_data['fmedianame'],'频道') && substr($a_e_data['fmediaclassid'],0,2) != '01'){
			//$this->ajaxReturn(array('code'=>01,'msg'=>'媒介类别选择错误'));
		}
		
		$collectInfo = M('tcollect')->cache(true,60)
												->field('tcollect.*')
												->join('tdevice on tdevice.fcollectid = tcollect.fid')
												->where(array('tdevice.fid'=>$a_e_data['fdeviceid']))->find();//采集点信息
		
		$region_authority = A('Admin/Authority','Model')->region_authority();//判断地区权限
		
		if(!in_array(substr($collectInfo['fregionid'],0,2),$region_authority) && $region_authority){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有该地区的权限'));//返回失败
		}
		
		
		$device_info = M('tdevice')->where(array('fid'=>$a_e_data['fdeviceid']))->find();//查询设备信息
		
		if(intval($a_e_data['fchannelid']) > 0 && $device_info['fdevicetypeid'] == 3){//判断是否拾联云盒
			
			$save_date = intval(I('save_date'));
			if($save_date == 0) $option = 2;
			if($save_date > 0)  $option = 1;
			
			$device_serial = $device_info['fcode'];//获取设备序列号
			
			$watermark = $a_e_data['watermark'];
			if(!$watermark) $watermark = $a_e_data['fmedianame'];
			
			A('Common/Shilian','Model')->device_update($device_serial,$a_e_data['fchannelid'],$watermark);//修改通道名称
			$shilian_get_dpid = A('Common/Shilian','Model')->get_dpid($device_serial,$a_e_data['fchannelid']);//拾联平台号
			//var_dump($shilian_get_dpid);
			
			A('Common/Shilian','Model')->record_plan($device_serial,$a_e_data['fchannelid'] );//设置录像计划

			if(substr($a_e_data['fmediaclassid'],0,2) == '01'){//判断是电视则开启移动侦测
				$detection_ret = A('Common/Shilian','Model')->detection($device_serial,$a_e_data['fchannelid'],1 );//设置移动侦测
				$rec_mode = 0;
			}else{//不是电视则关闭移动侦测
				$detection_ret = A('Common/Shilian','Model')->detection($device_serial,$a_e_data['fchannelid'],2 );//设置移动侦测
				if($a_e_data['watermark']){
					$rec_mode = 0;
				}else{
					$rec_mode = 2;
				}
				
			}
			$save_time = A('Common/Shilian','Model')->save_time($device_serial,$a_e_data['fchannelid'],$option,$save_date*1440 ,strtoupper($a_e_data['fsavepath']),$rec_mode);//设置媒介的保存位置
			$a_e_data['fdpid'] = $shilian_get_dpid['dpid'];
			if($save_date == 90){//判断保存时间是否是90天
				$rr1 = A('Common/Qiniu','Model')->rules_delete('hzsj-video-nb',$a_e_data['fdpid']);//如果是90天，删除规则
			}else{
				$rr1 = A('Common/Qiniu','Model')->rules_add('hzsj-video-nb',$a_e_data['fdpid'],$save_date);//如果不是90天，创建规则
			}
			
			
			$a_e_data['live_m3u8'] = $save_time['html5_url'];
		}else{
			$a_e_data['fdpid'] = 0;
		} 

		
		if(I('fstate') != ''){
			$a_e_data['fstate'] = I('fstate');//状态，-1删除，0-无效，1-有效
		}
        if(I('video_quality') != ''){
            $a_e_data['video_quality'] = I('video_quality');//清晰度，0=无，1=普清，2=标清，3=高清，4=超清
        }
		if($fid == 0){//判断是修改还是新增

			if(A('Admin/Authority','Model')->authority('200101') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
			}
			$is_repeat = M('tmedia')->where(array('fmediacode'=>$a_e_data['fmediacode']))->count();//查询媒体代码是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'媒体代码重复'));//返回媒体代码重复
            // 获取媒体地区id
            $a_e_data['media_region_id'] = M('tmediaowner')->where(['fid' => ['EQ', $a_e_data['fmediaownerid']]])->getField('fregionid');
			$a_e_data['fcreator'] = session('personInfo.fid').'_'.session('personInfo.fname');//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			if(I('define_mediaid') > 0){
				if(M('tmedia')->where(array('fid'=>I('define_mediaid')))->count() > 0){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'指定的媒体ID已存在'));
				}
				$a_e_data['fid'] = I('define_mediaid');
				
			}
			
			
			$rr = M('tmedia')->add($a_e_data);//添加数据
			
			if($rr > 0){//判断是否添加成功
				
				if(I('live_m3u8') == ' ' && $a_e_data['fcollecttype'] == 10){
					$saveData['live_m3u8'] =  U('Api/Media/get_media_m3u8@'.$_SERVER['HTTP_HOST'],array('media_id'=>$rr,'s'=>'m.m3u8'));
				}elseif(I('live_m3u8') == ' ' && in_array($a_e_data['fcollecttype'],array(25,26,27))){
					$saveData['live_m3u8'] =  'http://118.31.14.6/broadcast/live?channel='.$rr.'&s=m.m3u8';
				}elseif(I('live_m3u8') != ' ' && I('live_m3u8') != ''){
					$saveData['live_m3u8'] = I('live_m3u8');
				}	

				if(!$a_e_data['main_media_id']){
					$saveData['main_media_id'] = $rr;
					
				}
				
				
				M('tmedialabel')->where(array('fmediaid'=>$rr))->delete();//删除媒介关联的标签记录
				foreach($media_label as $label){//循环传入的标签
					$flabelid = A('Admin/Media','Model')->mediaLabel($rr,$label);
					M('tmedialabel')->add(array('fmediaid'=>$rr,'flabelid'=>$flabelid,'fcreator'=>session('personInfo.fid').'_'.session('personInfo.fname'),'fcreatetime'=>date('Y-m-d H:i:s')));
				}
				
				M('tmedia')->where(array('fid' => $rr))->save($saveData);
				if($a_e_data['save_days'] <= 0 && $a_e_data['priority'] <= -2){
					$n_media_collect_on_off = -1;
				}else{
					$n_media_collect_on_off = 1;
				}
				M('media_collect_on_off')->add(array('fmediaid'=>$rr,'on_off'=>$n_media_collect_on_off,'ftime'=>time(),'person_id'=>session('personInfo.fid')));

				$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功,【'.$a_e_data['fmedianame'].'】'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));//返回失败
			}
			
		}else{//修改
			
			if(A('Admin/Authority','Model')->authority('200102') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
			}
			$is_repeat = M('tmedia')->where(array('fid'=>array('neq',$fid),'fmediacode'=>$a_e_data['fmediacode']))->count();//查询媒体代码是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'媒体代码重复'));//返回媒体代码重复
			$a_e_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			if(I('live_m3u8') == ' ' && $a_e_data['fcollecttype'] == 10){
				$a_e_data['live_m3u8'] =  U('Api/Media/get_media_m3u8@'.$_SERVER['HTTP_HOST'],array('media_id'=>$fid,'s'=>'m.m3u8'));
			}elseif(I('live_m3u8') == ' ' && $a_e_data['fcollecttype'] == 25){
				$a_e_data['live_m3u8'] =  'http://118.31.14.6/broadcast/live?channel='.$fid.'&s=m.m3u8';
			}elseif(I('live_m3u8') != ' ' && I('live_m3u8') != ''){
				$a_e_data['live_m3u8'] = I('live_m3u8');
			}
			
			$mediaInfo = M('tmedia')->field('
											fmediaclassid,
											fdpid,
											fmediaownerid,
											fdeviceid,
											fmediacode,
											fmedianame,
											faddr,
											fpostcode,
											femail,
											fcollecttype,
											fliveparam,
											firparam,
											fsavepath,
											fchannelid,
											expire_date,
											valid_start_time,
											valid_end_time,
											priority,
											fjpgfilename,
											live_m3u8,
											remark,
											fstate,
											save_days
												')->where(array('fid'=>$fid))->find();
			if($mediaInfo['expire_date'] == '0000-00-00'){
				$mediaInfo['expire_date'] = '';
			}	
			if($a_e_data['expire_date'] == '0000-00-00'){
				$a_e_data['expire_date'] = '';
			}



			
			if($mediaInfo['fmedianame'] != $a_e_data['fmedianame'] && !in_array(intval(session('personInfo.fid')),array(108,46,131)) && in_array(substr(I('fmediaclassid'),0,2),array('01','02','03'))){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'没有修改媒介名称的权限'));
			}
					
			$arr_contrast = arr_contrast($a_e_data,$mediaInfo,'fmodifier,fmodifytime');
			if($mediaInfo['save_days'] <= 0 && $mediaInfo['priority'] <= -2){
				$s_media_collect_on_off = -1;
			}else{
				$s_media_collect_on_off = 1;
			}
			
			
			if($a_e_data['save_days'] <= 0 && $a_e_data['priority'] <= -2){
				$n_media_collect_on_off = -1;
			}else{
				$n_media_collect_on_off = 1;
			}
			
			if($s_media_collect_on_off != $n_media_collect_on_off){
				M('media_collect_on_off')->add(array('fmediaid'=>$fid,'on_off'=>$n_media_collect_on_off,'ftime'=>time(),'person_id'=>session('personInfo.fid')));
			}
			
			
			
			
			// 当媒体的信息有变动时, 将该媒体的地区id修改为其媒体机构的地区id
            $a_e_data['media_region_id'] = M('tmediaowner')->where(['fid' => ['EQ', $a_e_data['fmediaownerid']]])->getField('fregionid');
			if(!$a_e_data['main_media_id']){
				$a_e_data['main_media_id'] = $fid;
			}
			$rr = M('tmedia')->where(array('fid'=>$fid))->save($a_e_data);//修改数据

			if($rr > 0){//判断是否修改成功
				$oldLabelList = M('tmedialabel')->where(array('fmediaid'=>$fid))->getField('flabelid',true);//查询旧的媒介标签
				$newLabelList = array();
				foreach($media_label as $label){//循环传入的标签
					$flabelid = A('Admin/Media','Model')->mediaLabel($fid,$label);//获取标签ID
					$newLabelList[] = $flabelid;
					if(!in_array($flabelid,$oldLabelList)){//判断新的标签是否在旧标签里面
						M('tmedialabel')->add(array(
													'fmediaid'=>$fid,
													'flabelid'=>$flabelid,
													'fcreator'=>session('personInfo.fid').'_'.session('personInfo.fname'),
													'fcreatetime'=>date('Y-m-d H:i:s')
													));//添加媒介关联标签
												
					}
					
				}
				foreach($oldLabelList as $oldLabel){//循环老的标签
					if(!in_array($oldLabel,$newLabelList)){
						M('tmedialabel')->where(array('fmediaid'=>$fid,'flabelid'=>$oldLabel))->delete();//删除老的标签
						
					}
				}
				
				M('save_table_log')->add(array(
												'save_table' => 'tmedia',
												'primary_key' => $fid,
												'save_time' => date('Y-m-d H:i:s'),
												'save_ago' => json_encode($arr_contrast['ago_arr'],JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT),
												'save_later' => json_encode($arr_contrast['later_arr'],JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT),
												'save_user' => session('personInfo.fid').'_'.session('personInfo.fname'),
													));
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功,【'.$fid.' '.$a_e_data['fmedianame'].'】'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));//返回失败
			}
			
		}
			
		
	}
	
	/*报纸详情*/
	public function paper_media(){
		session_write_close();//停止使用session
		//str_replace(array('{yyyy}','{MM}','{dd}'),array(date('Y'),date('m'),date('d')),$mediaList[$key]['fsavepath'])
		$savepath = sys_auth(I('url'),'DECODE');//原始路径
		$time = time();//当前时间
		$target_url = '';//目标URL
		$num = 0;
		while($target_url == '' && $num < 10){//如果目标路径等于空和次数小于10
			$url = str_replace(array('{yyyy}','{MM}','{dd}'),array(date('Y',$time),date('m',$time),date('d',$time)),$savepath);//URL
			$header = get_headers($url,1);//获取头部
			if(!strstr($header[0],'404')){//判断返回码是否包含404
				$target_url = $url;//目标地址等于URL
			}
			$time = $time - 86400;
			$num++;
		}
		if($target_url == ''){
			echo '最近'.$num.'天没有找到该报纸素材';
		}else{
			header("Location:".$target_url); 
			exit;
		}
		
	}
	
		
	/*媒介详情*/
	public function ajax_media_details(){
		//exit;
		$fid = I('fid');//获取媒介ID
		$mediaDetails = M('tmedia')->where(array('fid'=>$fid))->find();//查询媒介详情
		$abnormal_type_list = M('media_abnormal')
										->cache(true,30)
										->field('abnormal_content,abnormal_time')
										
										->where(array('media_id'=>$fid))
										->order('abnormal_time desc')
										->limit(6)
										->select();//查询异常类型
		$abnormal_type = '';								
		foreach($abnormal_type_list as $LL){
			$abnormal_type .= $LL['abnormal_content'].'	'.date('Y-m-d H:i:s',$LL['abnormal_time'])."\n";//循环组装异常类型列表
		}							
		
		
		$mediaDetails['abnormal_type'] = strval($abnormal_type);
		
		
		
		$mediaDetails['group_name'] = M('ad_input_user_group')->cache(true,600)->where(['group_id'=>$mediaDetails['group_id']])->getField('group_name');//查询分组名称
		$mediaDetails['mediaclass_fullname'] = M('tmediaclass')->cache(true,600)->where(array('fid'=>$mediaDetails['fmediaclassid']))->getField('ffullname');//查询媒介类别全称
		$mediaDetails['mediaclass_name'] = M('tmediaclass')->cache(true,600)->where(array('fid'=>substr($mediaDetails['fmediaclassid'],0,2)))->getField('ffullname');//查询媒介类别全称
		
		$mediaDetails['mediaowner_name'] = M('tmediaowner')->cache(true,600)->where(array('fid'=>$mediaDetails['fmediaownerid']))->getField('fname');//查询媒介机构名称
		$deviceInfo = M('tdevice')->cache(true,600)->where(array('fid'=>$mediaDetails['fdeviceid']))->find();//采集设备

		$mediaDetails['device_name'] = $deviceInfo['fname'];//设备名称
		
		
		
		
		$mediaDetails['live_url'] = U('Api/Media/live',array('chnID'=>$mediaDetails['fdpid']));//直播地址(flash)
		
		$mediaDetails['live_m3u8_url'] = U('Api/Media/m3u8_live',array('fdpid'=>$mediaDetails['fdpid'],'fid'=>$mediaDetails['fid'],'volume'=>30));//直播地址(m3u8)
		
		$mediaDetails['player'] = array(); 
		$backupMediaList = M('tmedia')
									->cache(true,600)
									->field('tdevice.fname as device_name,tmedia.fchannelid,tmedia.fid,fdpid,main_media_id')
									->join('tdevice on tdevice.fid = tmedia.fdeviceid')
									->where(array('main_media_id'=>$mediaDetails['main_media_id']))
									->select();
			
		
		foreach($backupMediaList as $backupMedia){
			
			
			if($backupMedia['fid'] == $backupMedia['main_media_id']){
				$z_b = '主:';
			}else{
				$z_b = '备:';
			}
			
			
			
			$mediaDetails['player'][] = array(
											'fmediaid'	=> $backupMedia['fid'],
											'ch_name'	=> $z_b.$backupMedia['device_name'].'->'.$backupMedia['fchannelid'],
											'live_url' => U('Api/Media/live',array('chnID'=>$backupMedia['fdpid'])),//直播地址(flash),
											'live_m3u8_url'	=> U('Api/Media/m3u8_live',array('fdpid'=>$backupMedia['fdpid'],'fid'=>$backupMedia['fid'],'volume'=>30)),//直播地址(m3u8)
												);
		}

		
		
		if(strval($deviceInfo['infrared_code']) == 'new_cloud_box_2'){//判断红外控制类型
			$mediaDetails['ir_type'] = 2;
		}elseif(strval($deviceInfo['infrared_code']) != ''){
			$mediaDetails['ir_type'] = 1;
		}else{
			$mediaDetails['ir_type'] = 0;
		}
			
		  
		$mediaDetails['mobile_live_url'] = U('Api/Media/mobile_live@'.$_SERVER['HTTP_HOST'],array('fid'=>$fid));//直播地址
		
		$mediaDetails['lock_nickname'] = strval($inputUser['nickname']);//锁定的用户昵称
		
		
		$mediaDetails['collecttype_name'] = M('tcollecttype')->cache(true,600)->where(array('fcollecttypecode'=>$mediaDetails['fcollecttype']))->getField('fcollecttype');//采集方式名称
		$device_serial =  M('tdevice')->cache(true,600)->where(array('fid'=>$mediaDetails['fdeviceid']))->getField('fcode');//通过设备ID获取设备序列号
		//$save_time = A('Common/Shilian','Model')->save_time($device_serial,$mediaDetails['fchannelid'],0,0,$mediaDetails['fsavepath']);//
		
		//$mediaDetails['save_date'] = intval($save_time['minute']) / 1440;
		
		$mediaDetails['save_date'] = $mediaDetails['save_days'];
		$deviceDetails['device_run_state'] = '未知';
		if($mediaDetails['fdpid'] > 0){
			$device_run_state = A('Common/Shilian','Model')->device_checkonline(array($mediaDetails['fdpid']));
			if($device_run_state['ret'] === 0 ){
				$device_run_state = $device_run_state['state'][0];
				
				if($device_run_state === true) M('tmedia')->where(array('fid'=>$fid))->save(array('fonline'=>1));
				if($device_run_state === false) M('tmedia')->where(array('fid'=>$fid))->save(array('fonline'=>0));
				//var_dump(M('tmedia')->getLastSql());
				
			}
			
		}
		//var_dump($mediaDetails['mediaclass_name']);
		$mediaDetails['medialabel'] = M('tmedialabel')
													->cache(true,6)
													->field('tlabel.flabelid,tlabel.flabel')
													->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
													->where(array('tmedialabel.fmediaid'=>$mediaDetails['fid']))
													->select();
		
		
		if($device_run_state === true) $mediaDetails['device_run_state'] = '在线';
		if($device_run_state === false) $mediaDetails['device_run_state'] = '离线';
		if($mediaDetails['expire_date'] == '0000-00-00') $mediaDetails['expire_date'] = '';
		$mediaDetails['m3u8_backup'] = json_decode($mediaDetails['m3u8_backup'],true);

		foreach ($mediaDetails['m3u8_backup'] as $m3u8_key => $m3u8_value) {
			$mediaDetails['m3u8_backup'][$m3u8_key]['s_t'] = date("Y-m-d H:i:s",$m3u8_value['s_t']);
			$mediaDetails['m3u8_backup'][$m3u8_key]['e_t'] = date("Y-m-d H:i:s",$m3u8_value['e_t']);
		}
		$this->ajaxReturn(array('code'=>0,'mediaDetails'=>$mediaDetails));
	}
	
	
	/*删除媒介*/
	public function ajax_media_del(){
		if(A('Admin/Authority','Model')->authority('200103') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
		}
		$fid = I('fid');//获取媒介ID
		//$ret = M('tmedia')->where(array('fid'=>$fid))->save(array('fstate'=>-1,'flockuserid'=>null));//修改数据库状态
		
		$del_info = array();
		$del_info['del_table'] = 'tmedia';
		$del_info['primary_key'] = $fid;
		$del_info['del_time'] = date('Y-m-d H:i:s');
		$del_info['del_user'] = session('personInfo.fid').'_'.session('personInfo.fname');
		$del_info['info_josn'] = json_encode(M('tmedia')->where(array('fid'=>$fid))->find());
		
		
		
		M('del_table_log')->add($del_info);
		M('tmedia')->where(array('fid'=>$fid))->delete();//删除数据记录
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'.$fid));
		
	}
	
	
	/*获取媒体回放M3U8地址*/

	public function get_playback_url(){
		$st_time = time();
		header('Access-Control-Allow-Origin:*');
		$media_id = I('media_id');
		$start_time = I('start_time');
		$end_time = I('end_time');

		//$html5_url = A('Common/Shilian','Model')->get_playback_url($media_id,$start_time,$end_time);
		
		$html5_url = A('Common/Media','Model')->get_m3u8($media_id,$start_time,$end_time,1);
		
		$mediaInfo = M('tmedia')->cache(true,600)->where(array('fid'=>$media_id))->find();
		$media_name = $mediaInfo['fmedianame'];
		if($mediaInfo['priority'] < 0){
			//$html5_url = $mediaInfo['priority'];
		}
		
		$this->assign('html5_url',$html5_url);
		$this->assign('media_name',$media_name);
		
		$this->display();
		
		
		
		$logData = '';
		$logData .= date('Y-m-d H:i:s');
		$logData .= '	IP:'.get_client_ip();
		$logData .= '	媒介:'.$mediaInfo['fmedianame'].'('.$media_id.')';
		$logData .= '	请求开始时间:'.date('YmdHis',$start_time);
		$logData .= '	共'.($end_time - $start_time).'秒';
		$logData .= '	响应时间'.(time() - $st_time);
		$logData .= '	响应地址'.$html5_url;
		
		
		$logData .= "\n";

		
		
		if((time() - $st_time) >= 2){
			file_put_contents('LOG/get_playback_url'.date('Ymd').'.log',$logData,FILE_APPEND);
		}
	}
	
	/*解除锁定分离广告用户*/
	public function remove_lock_user(){
		$media_id = I('media_id');
		$rr = M('tmedia')->where(array('fid'=>$media_id))->save(array('flockuserid'=>NULL));//修改锁定用户字段
		if($rr > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'解锁成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'解锁失败'));
		}
		
	}
	
	/*添加媒介异常类型*/
	public function add_media_abnormal(){
		$media_id = I('media_id');//媒介id
		$abnormal_code = I('abnormal_code');//异常代码
		$a_data = array();
		$a_data['media_id'] = $media_id;
		$a_data['abnormal_code'] = $abnormal_code;
		$a_data['creator'] = session('personInfo.fname');
		$a_data['createtime'] = date('Y-m-d H:i:s');
		
		
		$a_ret = M('media_abnormal')->add($a_data);
		$save_where = array();
		$save_where['abnormal_code'] = $abnormal_code;
		/* if($abnormal_code == 105){
			$save_where['priority'] = -1;
		}
		if($abnormal_code == 100){
			$save_where['priority'] = 0;
		} */
		
		
		
		M('tmedia')->where(array('fid'=>$media_id))->save($save_where);
		
		if($a_ret > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败'));
		}
	}

	/*添加编辑遥控器任务*/
	public function add_edit_opera_plan(){
		if(A('Admin/Authority','Model')->authority('200106') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有控制任务编辑权限
		}
		$media_id = I('media_id');//媒介id
		$opera_plan = I('opera_plan');//任务列表string

		$opera_plan = preg_replace('# #','',str_replace('，',',',str_replace('：',':',str_replace('；',';',$opera_plan))));//替换中文符号
		

		$a_ret = M('tmedia')->where(array('fid'=>$media_id))->save(array('ir_plan' => $opera_plan));
		if($a_ret > 0 || $a_ret === 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'提交成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'提交失败'));
		}
	}
	
	/*切换ts流*/
	public function change_ts(){
		// if(A('Admin/Authority','Model')->authority('200107') === 0){
		// 	$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有 权限
		// }
		$mediaId = I('mediaId');
		$sourceMediaId = I('sourceMediaId');
		
		$startTime = I('startTime');
		$endTime = I('endTime');
		
		$start_time = strtotime($startTime);
		$end_time = strtotime($endTime);

		$issue_date = date('Y-m-d',$start_time);//发布日期
		
		if(date('Y-m-d',$start_time) != date('Y-m-d',$end_time)){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'开始结束时间不允许跨天'));
		}
		
		
		
		
		//查询当前媒体id的ts列表********************************************************************************************/
		$startPK = array (
			array('fmediaid', intval($mediaId)),
			array('start_time',$start_time - 10),
			array('end_time',$start_time + 0),
			array('fdate', date('Y-m-d',$start_time)),
		);
		$endPK = array (
			array('fmediaid', intval($mediaId)),
			array('start_time',$end_time - 0),
			array('end_time',$end_time + 10),
			array('fdate', date('Y-m-d',$start_time)),
		);

		$limit = intval((($end_time - $start_time) / 10 ) + 100);
		$m3u8_list = A('Common/TableStore','Model')->getRange('ts_key',$startPK,$endPK,$limit);
		//查询当前媒体id的ts列表***************************************************************************结束***************/
		
		
		$DdataList = array();
		$ots_d_num = 0;
		foreach($m3u8_list as $M3u8){
			$DdataList[] = array ( // 第一行
							"operation_type" => 'DELETE',            //操作是PUT
							'condition' => 'IGNORE',
							'primary_key' => array (
								array('fmediaid',intval($mediaId)),
								array('start_time',intval($M3u8['start_time'])),
								array('end_time',intval($M3u8['end_time'])),
								array('fdate',$issue_date),
								
							),
							
						);//组装写入tablestore的数据结构
			if(	count($DdataList) == 200){//判断数据是否达到200条，因为table的最大限制是一次性写入200条
				$response = A('Common/TableStore','Model')->batchWriteRow('ts_key',$DdataList);//写入数据到tablestore
				$ots_d_num += $response['successNum'];//写入ots成功的数量
				$DdataList = array();	
			}				
								
		}
		if(	count($DdataList) > 0){//
			$response = A('Common/TableStore','Model')->batchWriteRow('ts_key',$DdataList);//写入数据到tablestore
			$ots_d_num += $response['successNum'];//写入ots成功的数量
			$DdataList = array();	
		}
	

		
		
		//查询原媒体id的ts列表********************************************************************************************/
		$startPK = array (
			array('fmediaid', intval($sourceMediaId)),
			array('start_time',$start_time - 10),
			array('end_time',$start_time + 0),
			array('fdate', date('Y-m-d',$start_time)),
		);
		$endPK = array (
			array('fmediaid', intval($sourceMediaId)),
			array('start_time',$end_time - 0),
			array('end_time',$end_time + 10),
			array('fdate', date('Y-m-d',$start_time)),
		);

		$limit = intval((($end_time - $start_time) / 10 ) + 100);
		$m3u8_list = A('Common/TableStore','Model')->getRange('ts_key',$startPK,$endPK,$limit);
		//查询原媒体id的ts列表***************************************************************************结束***************/
		
		//写入当前媒体的ts列表**********************************************************************************************/
		$WdataList = array();//写入tablestore的数据
		$ots_w_num = 0;
		foreach($m3u8_list as $sourceM3u8){
			$WdataList[] = array ( // 第一行
							"operation_type" => 'PUT',            //操作是PUT
							'condition' => 'IGNORE',
							'primary_key' => array (
								array('fmediaid',intval($mediaId)),
								array('start_time',intval($sourceM3u8['start_time'])),
								array('end_time',intval($sourceM3u8['end_time'])),
								array('fdate',$issue_date),
								
							),
							'attribute_columns' => array(array('ts_url',$sourceM3u8['ts_url']),array('create_time',date('Y-m-d H:i:s'))),
						);//组装写入tablestore的数据结构
			if(	count($WdataList) == 200){//判断数据是否达到200条，因为table的最大限制是一次性写入200条
				$response = A('Common/TableStore','Model')->batchWriteRow('ts_key',$WdataList);//写入数据到tablestore
				$ots_w_num += $response['successNum'];//写入ots成功的数量
				$WdataList = array();	
			}						
		}
		
		if(	count($WdataList) > 0){
			$response = A('Common/TableStore','Model')->batchWriteRow('ts_key',$WdataList);//写入数据到tablestore
			$ots_w_num += $response['successNum'];//写入ots成功的数量
			$WdataList = array();
		}
		//写入当前媒体的ts列表*******************************************************************************结束***********/
		
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'替换完成,删除:'.$ots_d_num.',写入:'.$ots_w_num,'ots_w_num'=>$ots_w_num,'ots_d_num'=>$ots_d_num));
		
	}
	
	
	
	
}