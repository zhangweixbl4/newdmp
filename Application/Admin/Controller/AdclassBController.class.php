<?php
//广告类别控制器
namespace Admin\Controller;
use Think\Controller;
class AdclassBController extends BaseController {
	
    public function index(){
		
		if(A('Admin/Authority','Model')->authority('200181') === 0){
			exit('没有权限');//验证是否有新增权限
		}
		if (IS_POST){
		    $addList = I('add');
		    $updateList = I('update');
		    $deleteList = I('delete');
		    $model = M('hz_ad_class');
		    $model->startTrans();
		    $addUpdate = true;
		    if (!$addList){
		        $addData = [];
                foreach ($addList as $item) {
                    $fullname = [];
                    for ($i = strlen($item['id']); $i >= 2; $i -= 2){
                        $addData[] = M('hz_ad_class')->where(['fcode' => ['EQ', substr($item['id'], $i)]])->getField('fname');
                    }
                    $addData[] = [
                        'fcode' => $item['id'],
                        'fpcode' => $item['fpcode'],
                        'fname' => $item['label'],
                        'ffullname' => implode('>', $fullname)
                    ];
		        }
		        $this->ajaxReturn($addData);
//                $addUpdate = $model->addAll($addData) === false ? false : true;
            }
        }else{
		    $adClassATree = json_encode($this->adClassATree());
		    $adClassBTree = json_encode($this->adClassBTree());
		    $this->assign(compact('adClassBTree', 'adClassATree'));
            $this->display();
        }
	}

    private function adClassBTree()
    {
        $data = M('hz_ad_class')->field('fcode id, fpcode, fname label, ffullname')->where(['fstate' => ['EQ', 1]])->select();
        $items = [];
        foreach($data as $value){
            $items[$value['id']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpcode']])){
                $items[$value['fpcode']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
	}

    private function adClassATree()
    {
        $data = M('tadclass')->field('fcode value, fpcode, fadclass label, ffullname')->where(['fstate' => ['EQ', 1]])->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpcode']])){
                $items[$value['fpcode']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }

    public function getClassInfo($fcode)
    {
        $data = M('hz_ad_class')->where(['fcode' => ['EQ', $fcode]])->find();
        $this->ajaxReturn([
            'data' => $data
        ]);
	}

    public function saveClassA($fcode, $classA)
    {
        $res = M('hz_ad_class')
            ->where([
                'fcode' => ['EQ', $fcode]
            ])
            ->save(['fcode1' => $classA]) === false ? false : true;
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '保存成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '保存失败'
            ]);
        }
	}
}