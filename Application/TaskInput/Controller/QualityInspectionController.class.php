<?php
namespace TaskInput\Controller;

/**
 * 众包质检任务控制器
 */
class QualityInspectionController extends BaseController {
    // 接收参数
    protected $P;
    // 任务超时限制
    protected $overtime;
    // 错误信息
    protected $error = '';
    // 媒体类别
    protected $mediaclass = [
        1  => '电视',
        2  => '广播',
        3  => '报纸',
        5  => '户外',
        13 => '互联网',
    ];

    /**
     * 初始化
     */
    public function _initialize(){
        parent::_initialize();
        $this->P = json_decode(file_get_contents('php://input'),true);
        $this->overtime = sys_c('task_overtime');
    }

    /**
     * 添加质检任务
     */
    public function addTask(){
        // 任务数据
        $taskData = [
            'ftablename' => $ftablename,
            'ftaskid' => $ftaskid,
            'fmediaclass' => $fmediaclass,
            'foperator_id' => $foperator_id,
            'fstate' => $fstate,
            'creator' => $creator,
            'createtime' => time(),
        ];
        $ftask_quality_inspection_id = M('ttask_quality_inspection')->add($taskData);
        // 流程
        $flowData = [
            'ftask_quality_inspection_id' => $ftask_quality_inspection_id,
            'foperator_id' => $foperator_id,
            'log' => $log,
            'logtype' => 1,
            'creator' => 'API',
            'createtime' => time(),
        ];
        M('ttask_quality_inspection_flowlog')->add($flowData);
    }

    /**
     * 领取质检任务
     */
    public function getTask(){
        if(IS_POST){
            $data = $this->getTaskIns();
            $total = M('ttask_quality_inspection')->where(['fstate'=>['EGT',0]])->count();
            $uncount = M('ttask_quality_inspection')->where(['fstate'=>['IN',[0,1]]])->count();
            if(empty($data)){
                $data = null;
            }
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'overtime'=>$this->overtime, 'total'=>$total, 'uncount'=>$uncount, 'data'=>$data]);
        }else{
            $this->display();
        }
    }

    /**
     * 获取任务,质检表
     */
    public function getTaskIns(){
        $getCount = 10; //每次请求获取任务尝试次数限制
        $adTable = 'tad';
        $tableName = 'ttask_quality_inspection';
        $flowTableName = 'ttask_quality_inspection_flowlog';
        // 测试环境用
        $wx_id = session('wx_id') ? session('wx_id') : '469';
        while($getCount > 0){
            // 领取任务
            $where = [
                'foperator_id' => $wx_id,
                'fstate' => 1,
            ];
            $taskInfo = M($tableName)
                ->master(true)
                ->where($where)
                ->order('fpriority DESC,fcreatetime ASC')
                ->find();
            if(!empty($taskInfo)){
                // 任务表样本ID字段名
                $taskSamPkey = 'sam_id';
                // 样本表
                $samTable = 'ttvsample';
                // 样本表主键
                $samPkey = 'fid';
                // 样本表媒体ID字段名
                $fmediaidField = 'media_id';
                // 素材内容类型 电视-mp4 广播-mp3 报纸-img 网络-url(flv、swf、image、text)
                $ftype = '';
                switch((int)$taskInfo['fmediaclass']){
                    case 1:
                    case 01:
                        $samTable = 'ttvsample';
                        $fmediaidField = 'media_id';
                        $ftype = 'mp4';
                        break;
                    case 2:
                    case 02:
                        $samTable = 'tbcsample';
                        $fmediaidField = 'media_id';
                        $ftype = 'mp3';
                        break;
                    case 3:
                    case 03:
                        $samTable = 'tpapersample';
                        $samPkey = 'fpapersampleid';
                        $fmediaidField = 'media_id';
                        $ftype = 'img';
                        break;
                    case 5:
                    case 05:
                        $samTable = 'todsample';
                        $fmediaidField = 'media_id';
                        break;
                    case 13:
                        $taskSamPkey = 'major_key';
                        $samTable = 'tnetissue';
                        $samPkey = 'major_key';
                        $fmediaidField = 'media_id';
                        $ftype = 'url';//TODO:互联网素材类型需进一步考虑
                        break;
                }
                // 是否超时
                $isovertime = $this->isOvertime($taskInfo['fmodifytime']);
                if($isovertime){
                    // 更新重置任务状态为初始
                    $saveData = [
                        'foperator_id' => 0,
                        'fstate' => 0,
                        'fmodifier' => 'getTaskIns',
                        'fmodifytime' => time(),
                    ];
                    $saveRes = M($tableName)
                        ->master(true)
                        ->where(['fid'=>$taskInfo['fid']])
                        ->save($saveData);
                    // 更新流程
                    $flowData = [
                        'ftask_quality_inspection_id' => $taskInfo['fid'],
                        'foperator_id'                => $taskInfo['foperator_id'],
                        'logtype'                     => 9,
                        'log'                         => '质检任务超时重置',
                        'taskinfo'                    => json_encode($taskData),
                        'creator'                     => '/QualityInspection/getTask',
                        'createtime'                  => time(),
                    ];
                    $flowRes = M($flowTableName)->add($flowData);
                    // 获取到已超时的任务时，重置任务后，继续接下来分配及领取新任务
                    continue;
                }
                // 条件
                $whereData = [
                    'a.taskid' => $taskInfo['ftaskid'],
                ];
                $taskData = M($taskInfo['ftablename'])
                    ->alias('a')
                    ->join($samTable.' b ON b.'.$samPkey.' = a.'.$taskSamPkey,'LEFT')
                    ->join($adTable.' c ON c.fadid = b.fadid','LEFT')
                    ->where($whereData)
                    ->find();
                $sql = M($taskInfo['ftablename'])->getLastSql();
                // 流程日志
                $taskInsInfo = M('ttask_quality_inspection')->where($where)->find();
                $logArr = [
                    'ftask_quality_inspection_id' => $taskInsInfo['fid'],
                    'foperator_id'                => $taskInfo['foperator_id'],
                    'logtype'                     => 2,
                    'log'                         => '领取任务',
                    'taskinfo'                    => json_encode($taskData),
                ];
                $flowRes = A('TaskInput/QualityInspection','Model')->log($logArr);
                if (!$flowRes){
                    $this->error = '流程更新失败';
                    $this->ajaxReturn(['code'=>1, 'msg'=>$this->error]);
                }
                // 任务数据
                $adclassname = M('tadclass')->cache(true,120)->where(['fcode' => $taskData['adtype']])->getField('ffullname');
                $fmedianame = M('tmedia')->cache(true,86400)->where(['fid'=>$taskData[$fmediaidField]])->getField('fmedianame');
                $fadowner = M('tadowner')->cache(true,86400)->where(['fid'=>$taskData['fadowner']])->getField('fname');
                $adCateText1 = M('tadclass')->cache(true,120)->where(['fcode' => $taskData['fadclasscode']])->getField('ffullname');
                $adCateText2 = M('hz_ad_class')->cache(true,120)->where(['fcode' => $taskData['fadclasscode_v2']])->getField('ffullname');
                $adCateText2 = $adCateText2 ? $adCateText2 : '';
                // TODO：互联网ftype需要进一步考虑 2019-07-15
                // $ftype = $taskData['ftype'] ? $taskData['ftype'] : $ftype;
                $man = $this->getMan($taskInfo['ftablename'],$taskInfo['ftaskid']);
                $data = [
                    'taskid'           => $taskInfo['ftaskid'],
                    'wx_id'            => $taskInfo['foperator_id'],
                    'fmediaclass'      => $taskInfo['fmediaclass'],
                    'fmediaid'         => $taskData[$fmediaidField],
                    'fmedianame'       => $fmedianame,
                    'fissue_date'      => $taskData['issue_date'],
                    'fsourcepath'      => $taskData['source_path'],
                    'ftype'            => $ftype,
                    'fadid'            => $taskData['fadid'],
                    'fadname'          => $taskData['fadname'],
                    'fbrand'           => $taskData['fbrand'],
                    'fadowner'         => $fadowner,
                    'fversion'         => $taskData['fversion'],
                    'fspokesman'       => $taskData['fspokesman'],
                    'is_long_ad'       => $taskData['is_long_ad'],
                    'net_created_date' => $taskData['net_created_date'],
                    'net_original_url' => $taskData['net_original_url'],
                    'net_target_url'   => $taskData['net_target_url'],
                    'thumb_url_true'   => $taskData['thumb_url_true'],
                    'net_snapshot'     => $taskData['net_snapshot'],
                    'fadclasscode'     => $taskData['fadclasscode'],
                    'fadclasscode_v2'  => $taskData['fadclasscode_v2'],
                    'adCateText1'      => $adCateText1,
                    'adCateText2'      => $adCateText2,
                    'fillegaltypecode' => $taskData['fillegaltypecode'],
                    'fillegalcontent'  => htmlspecialchars_decode($taskData['fillegalcontent']),
                    'fexpressioncodes' => $taskData['fexpressioncodes'],
                    'fexpressions'     => $taskData['fexpressions'],
                    'fconfirmations'   => $taskData['fconfirmations'],
                    'fadmanuno'        => $taskData['fadmanuno'],
                    'fmanuno'          => $taskData['fmanuno'],
                    'fadapprno'        => $taskData['fadapprno'],
                    'fapprno'          => $taskData['fapprno'],
                    'fadent'           => $taskData['fadent'],
                    'fent'             => $taskData['fent'],
                    'fentzone'         => $taskData['fentzone'],
                    'modifytime'       => date('Y-m-d H:i:s',$taskInfo['fmodifytime']),
                    'finputman'        => $man['finputman'],
                    'finputtime'       => $man['finputtime'],
                    'fjudgeman'        => $man['fjudgeman'],
                    'fjudgetime'       => $man['fjudgetime'],
                    'finspectman'      => $man['fjudgeman'],
                    'finspecttime'     => $man['fjudgetime'],
                ];
                return $data;
            }else{
                // 分配任务
                // $ret = $this->assignTask($tableName);
                // 分配任务
                $whereTask = [
                    'foperator_id' => 0,
                    'fstate' => 0,
                    'fpriority' => ['EGT',0],
                ];
                $bindInfo = [
                    'foperator_id' => $wx_id,
                    'fstate' => 1,
                    'fmodifier' => 'getTaskIns',
                    'fmodifytime' => time(),
                ];
                $bindTaskList = M($tableName)
                    ->master(true)
                    ->where($whereTask)
                    ->order('fpriority DESC,fcreatetime ASC')
                    ->limit(1)
                    ->save($bindInfo);
            }
            $getCount--;
        }
        return false;
    }
    /**
     * 质检任务分配
     */
    public function assignTask($tableName='ttask_quality_inspection'){
        // 分配任务
        $whereTask = [
            'foperator_id' => 0,
            'fstate' => 0,
            'fpriority' => ['EGT',0],
        ];
        $bindInfo = [
            'foperator_id' => $wx_id,
            'fstate' => 1,
            'fmodifier' => 'getTaskIns',
            'fmodifytime' => time(),
        ];
        $bindTaskList = M($tableName)
            ->master(true)
            ->where($whereTask)
            ->order('fpriority DESC,fcreatetime ASC')
            ->limit(1)
            ->save($bindInfo);
        return $bindTaskList;
    }
    /**
     * 质检任务分配_方案B
     */
    public function assignTaskB($tableName='ttask_quality_inspection'){
        $fid = $this->assignTaskRules($tableName);
        $whereTask = [
            'fid' => $fid,
        ];
        $bindInfo = [
            'foperator_id' => $wx_id,
            'fstate' => 1,
            'fmodifier' => 'getTaskIns',
            'fmodifytime' => time(),
        ];
        $bindTaskList = M($tableName)
            ->master(true)
            ->where($whereTask)
            ->order('fpriority DESC,fcreatetime ASC')
            ->limit(1)
            ->save($bindInfo);
        return $bindTaskList;
    }
    /**
     * 质检任务分配规则
     */
    public function assignTaskRules($tableName='ttask_quality_inspection'){
        $ret = 0;
        while($ret == 0){
            $whereTask = [
                'foperator_id' => 0,
                'fstate' => 0,
                'fpriority' => ['EGT',0],
            ];
            $taskInfo = M($tableName)
                ->master(true)
                ->where($whereTask)
                ->order('fpriority DESC,fcreatetime ASC')
                ->find();
            if($taskInfo['ftablename'] == 'task_input'){
                // 传统媒体排除违法
                $traTaskInfo = M('task_input')->where(['taskid'=>$taskInfo['ftaskid']])->find();
                if($traTaskInfo['fillegaltypecode'] > 0){
                    $ret = 0;
                }else{
                    $ret = $taskInfo['fid'];
                }
            }elseif($taskInfo['ftablename'] == 'tnettask'){
                // 互联网排除上海、杭州及其下属地区
                $netTaskInfo = M('tnettask')->where(['taskid'=>$taskInfo['ftaskid']])->find();
                $mediaArr = M('tmedia')->cache(true,86400)->where(['LEFT(fmediaclassid,2)'=>13,['LEFT(media_region_id,2)'=>31,'LEFT(media_region_id,4)'=>3301,'_logic'=>'OR']])->getField('fid',true);
                if(in_array($netTaskInfo['media_id'],$mediaArr)){
                    $ret = 0;
                }else{
                    $ret = $taskInfo['fid'];
                }
            }
        }
        return $ret;
    }
    /**
     * 提交质检任务
     * @param int       $type 提交类型 2-质检通过 3-退回录入 4-退回审核 5-退回剪辑错误
     * @param string    $mediaclass 媒介类别 01-电视 02-广播 03-报纸 05-户外 13-互联网
     * @param int       $taskid 任务ID
     * @param string    $reason 理由
     */
    public function submitTask(){
        $type       = $this->P['type'];
        $mediaclass = $this->P['mediaclass'];
        $taskid     = $this->P['taskid'];
        $reason     = $this->P['reason'];
        if(IS_POST){
            // TODO:未完
            $data = $this->P;
            switch($type){
                case 2:
                    $passRes = $this->passTask($mediaclass,$taskid,$reason);
                    break;
                case 3:
                    $inputRes = $this->backTaskInput($mediaclass,$taskid,$reason);
                    break;
                case 4:
                    $judgeRes = $this->backTaskJudge($mediaclass,$taskid,$reason);
                    break;
                case 5:
                    $cutRes = $this->backTaskCut($mediaclass,$taskid,$reason);
                    break;
                default:
                    $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数：type提交类型']);
                    break;
            }
            // 质检加分
            $scoreRes = A('TaskInput/TaskInputScore','Model')->addScoreByQuaIns($taskid,$mediaclass,session('wx_id'));

            $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'data'=>$data]);
        }else{
            $this->display();
        }
    }

    /**
     * 质检通过
     * @param string    $mediaclass 媒介类别 01-电视 02-广播 03-报纸 05-户外 13-互联网
     * @param int       $taskid 任务ID
     * @param string    $reason 理由
     */
    public function passTask($mediaclass,$taskid,$reason){
        // 任务更新
        $where = [
            'fmediaclass' => $mediaclass,
            'ftaskid' => $taskid,
        ];
        $saveData = [
            'foperator_id' => session('wx_id'),
            'fstate'       => 2,
            'freason'      => $reason,
            'fmodifier'    => '',
            'fmodifytime'  => time(),
        ];
        $res = M('ttask_quality_inspection')
            ->where($where)
            ->save($saveData);
        // 流程日志
        $taskInsInfo = M('ttask_quality_inspection')->where($where)->find();
        $logArr = [
            'ftask_quality_inspection_id' => $taskInsInfo['fid'],
            'foperator_id'                => session('wx_id'),
            'log'                         => '质检通过',
            'logtype'                     => 4,
        ];
        $logres = A('TaskInput/QualityInspection','Model')->log($logArr);
        // 更新来源任务表质检状态
        $tablename = $this->getTableNameByMediaClass($mediaclass);
        $setRes = A('TaskInput/QualityInspection','Model')->setTaskQualityIns($tablename,$taskid,2,session('wx_id'),$reason);
        // 通过来源任务
        $passRes = A('TaskInput/TraditionAd')->passTaskByIns($taskid,$mediaclass);
        if(!$res){
            $this->error = '提交时出错';
            return false;
        }
        return true;
    }

    /**
     * 退回录入
     * @param string    $mediaclass 媒介类别 01-电视 02-广播 03-报纸 05-户外 13-互联网
     * @param int       $taskid 任务ID
     * @param string    $reason 理由
     */
    public function backTaskInput($mediaclass,$taskid,$reason){
        // 任务更新
        $where = [
            'fmediaclass' => $mediaclass,
            'ftaskid' => $taskid,
        ];
        $saveData = [
            'foperator_id' => session('wx_id'),
            'fstate'       => 3,
            'freason'      => $reason,
            'fmodifier'    => '',
            'fmodifytime'  => time(),
        ];
        $res = M('ttask_quality_inspection')
            ->where($where)
            ->save($saveData);
        // 流程日志
        $taskInsInfo = M('ttask_quality_inspection')->where($where)->find();
        $logArr = [
            'ftask_quality_inspection_id' => $taskInsInfo['fid'],
            'foperator_id' => session('wx_id'),
            'log'          => '退回录入 '.$reason,
            'logtype'      => 5,
        ];
        $logres = A('TaskInput/QualityInspection','Model')->log($logArr);
        // 更新来源任务表质检状态
        $tablename = $this->getTableNameByMediaClass($mediaclass);
        $setRes = A('TaskInput/QualityInspection','Model')->setTaskQualityIns($tablename,$taskid,2,session('wx_id'),$reason);
        // 退回来源任务包括扣除积分
        $rejectRes = A('TaskInput/TaskInput','Model')->rejectTask($taskid, session('wx_id'), $mediaclass, 1, $reason);

        if(!$res){
            $this->error = '提交时出错';
            return false;
        }
        return true;
    }

    /**
     * 退回审核
     * @param string    $mediaclass 媒介类别 01-电视 02-广播 03-报纸 05-户外 13-互联网
     * @param int       $taskid 任务ID
     * @param string    $reason 理由
     */
    public function backTaskJudge($mediaclass,$taskid,$reason){
        // 任务更新
        $where = [
            'fmediaclass' => $mediaclass,
            'ftaskid' => $taskid,
        ];
        $saveData = [
            'foperator_id' => session('wx_id'),
            'fstate'       => 4,
            'freason'      => $reason,
            'fmodifier'    => '',
            'fmodifytime'  => time(),
        ];
        $res = M('ttask_quality_inspection')
            ->where($where)
            ->save($saveData);
        // 流程日志
        $taskInsInfo = M('ttask_quality_inspection')->where($where)->find();
        $logArr = [
            'ftask_quality_inspection_id' => $taskInsInfo['fid'],
            'foperator_id' => session('wx_id'),
            'log'          => '退回审核 '.$reason,
            'logtype'      => 6,
        ];
        $logres = A('TaskInput/QualityInspection','Model')->log($logArr);
        // 更新来源任务表质检状态
        $tablename = $this->getTableNameByMediaClass($mediaclass);
        $setRes = A('TaskInput/QualityInspection','Model')->setTaskQualityIns($tablename,$taskid,2,session('wx_id'),$reason);
        // 退回来源任务包括扣除积分
        $rejectRes = A('TaskInput/TaskInput','Model')->rejectTask($taskid, session('wx_id'), $mediaclass, 2, $reason);

        if(!$res){
            $this->error = '提交时出错';
            return false;
        }
        return true;
    }

    /**
     * 退回剪辑
     */
    public function backTaskCut($mediaclass,$taskid,$reason){
        // TODO：待定
        return true;
    }
    /**
     * 获取任务列表
     * @param int $wx_id 可选，传值则获取人员质检列表，不传值则获取全部列表
     */
    public function getList($wx_id = 0){
        $taskid           = $this->P['taskid'];
        $fadname          = $this->P['fadname'];
        $fbrand           = $this->P['fbrand'];
        $fadowner         = $this->P['fadowner'];
        $fmediaclass      = $this->P['media_class'];
        $fmedianame       = $this->P['fmedianame'];
        $fillegaltypecode = $this->P['fillegaltypecode'];
        $finspectstate    = $this->P['finspectstate'];
        $finspecttime     = $this->P['finspecttime'] ? $this->P['finspecttime'] : '';
        $fissue_date      = $this->P['fissue_date'] ? $this->P['fissue_date'] : '';
        $pageIndex        = $this->P['pageIndex'] ? $this->P['pageIndex'] : 1;
        $pageSize         = $this->P['pageSize'] ? $this->P['pageSize'] : 20;
        $stime            = $this->P['stime'] ? $this->P['stime'] : date('Y-01-01');
        $etime            = $this->P['etime'] ? $this->P['etime'] : date('Y-m-d');
        $stimeu           = strtotime($stime);
        $etimeu           = strtotime('+1 day',strtotime($etime))-1;
        $issueDate        = $fissue_date ? explode(',',$fissue_date) : [];
        $inspectDate      = $finspecttime ? explode(',',$finspecttime) : [];
        $where = [
            'a.fcreatetime' => ['BETWEEN',[$stimeu,$etimeu]],
        ];
        if(!empty($taskid)){
            $where['a.ftaskid'] = ['LIKE','%'.$taskid.'%'];
        }
        if(!empty($fadname)){
            $taskIdAd = [];
            $traTaskid = M('task_input')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->where(['b.fadname'=>['LIKE','%'.$fadname.'%']])
                ->getField('a.taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->where(['b.fadname'=>['LIKE','%'.$fadname.'%']])
                ->getField('a.taskid',true);
            $taskIdAd = array_merge($traTaskid,$netTaskid);
            if(!empty($taskIdAd)){
                $where['a.ftaskid'] = ['IN',$taskIdAd];
            }
        }
        if(!empty($fbrand)){
            $taskIdAd = [];
            $traTaskid = M('task_input')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->where(['b.fbrand'=>['LIKE','%'.$fbrand.'%']])
                ->getField('a.taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->where(['b.fbrand'=>['LIKE','%'.$fbrand.'%']])
                ->getField('a.taskid',true);
            $taskIdAd = array_merge($traTaskid,$netTaskid);
            if(!empty($taskIdAd)){
                $where['a.ftaskid'] = ['IN',$taskIdAd];
            }
        }
        if(!empty($fadowner)){
            $taskIdAd = [];
            $traTaskid = M('task_input')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->join('tadowner c ON c.fid=b.fadowner')
                ->where(['c.fname'=>['LIKE','%'.$fadowner.'%']])
                ->getField('a.taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->join('tadowner c ON c.fid=b.fadowner')
                ->where(['c.fname'=>['LIKE','%'.$fadowner.'%']])
                ->getField('a.taskid',true);
            $taskIdAd = array_merge($traTaskid,$netTaskid);
            if(!empty($taskIdAd)){
                $where['a.ftaskid'] = ['IN',$taskIdAd];
            }
        }
        if(!empty($fmediaclass)){
            $where['a.fmediaclass'] = $fmediaclass;
        }
        if(!empty($fmedianame)){
            $taskIdMedia = [];
            $traTaskid = M('task_input')
                ->alias('a')
                ->join('tmedia b ON b.fid=a.media_id')
                ->where(['b.fmedianame'=>['LIKE','%'.$fmedianame.'%']])
                ->getField('a.taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask')
                ->alias('a')
                ->join('tmedia b ON b.fid=a.media_id')
                ->where(['b.fmedianame'=>['LIKE','%'.$fmedianame.'%']])
                ->getField('a.taskid',true);
            $taskIdMedia = array_merge($traTaskid,$netTaskid);
            if(!empty($taskIdAd)){
                $where['a.ftaskid'] = ['IN',$taskIdMedia];
            }
        }
        if(!empty($issueDate)){
            $taskIdDate = [];
            $traTaskid = M('task_input')
                ->alias('a')
                ->where(['a.issue_date'=>['BETWEEN',[$issueDate[0],$issueDate[1]]]])
                ->getField('a.taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask')
                ->alias('a')
                ->where(['a.issue_date'=>['BETWEEN',[$issueDate[0],$issueDate[1]]]])
                ->getField('a.taskid',true);
            $taskIdDate = array_merge($traTaskid,$netTaskid);
            if(!empty($taskIdDate)){
                $where['a.ftaskid'] = ['IN',$taskIdDate];
            }
        }
        if(!empty($fillegaltypecode) || $fillegaltypecode === 0){
            $taskIdIllegal = [];
            $traTaskidIllegal = M('task_input')
                ->alias('a')
                ->where(['a.fillegaltypecode'=>$fillegaltypecode])
                ->getField('a.taskid',true);
            $traTaskidIllegal = $traTaskidIllegal ? $traTaskidIllegal : [];
            $netTaskidIllegal = M('tnettask')
                ->alias('a')
                ->where(['a.fillegaltypecode'=>$fillegaltypecode])
                ->getField('a.taskid',true);
            $taskIdIllegal = array_merge($traTaskidIllegal,$netTaskidIllegal);
            if(!empty($taskIdIllegal)){
                $where['a.ftaskid'] = ['IN',$taskIdIllegal];
            }
        }
        if(!empty($finspectstate) || $finspectstate === 0){
            $where['a.fstate'] = $finspectstate;
        }
        if(!empty($inspectDate)){
            $sInsDate = strtotime($inspectDate[0]);
            $eInsDate = strtotime('+1 day',strtotime($inspectDate[1])) - 1;
            $where['a.fmodifytime'] = ['BETWEEN',[$sInsDate,$eInsDate]];
        }
        $data = [];
        if(IS_POST){
            $count = M('ttask_quality_inspection')
                ->alias('a')
                ->where($where)
                ->count();
            $dataSet = M('ttask_quality_inspection')
                ->alias('a')
                ->where($where)
                ->order('a.fcreatetime DESC')
                ->page($pageIndex,$pageSize)
                ->select();
            $sql = M('ttask_quality_inspection')->getLastSql();
            foreach($dataSet as $key => $row){
                $taskRow = M($row['ftablename'])->where(['taskid'=>$row['ftaskid']])->find();
                $adRow = M('tad')->where(['fadid'=>$taskRow['fadid']])->find();
                $mediaRow = M('tmedia')->where(['fid'=>$taskRow['media_id']])->find();
                $man = $this->getMan($row['ftablename'],$row['ftaskid']);
                $dataRow = [
                    'fid' => $row['fid'],
                    'taskid' => $row['ftaskid'],
                    'fpriority' => $row['fpriority'],
                    'tadname' => $taskRow['fadname'] ? $taskRow['fadname'] : $adRow['fadname'],
                    'fadname' => $taskRow['fadname'] ? $taskRow['fadname'] : $adRow['fadname'],
                    'adCateText1' => M('tadclass')->cache(true,86400)->where(['fcode' => $adRow['fadclasscode']])->getField('ffullname'),
                    'fmediaclass' => $this->mediaclass[$taskRow['media_class']],
                    'fmedianame' => $mediaRow['fmedianame'],
                    'fissue_date' => $taskRow['issue_date'],
                    'fillegaltypecode' => $taskRow['fillegaltypecode'],
                    'finputman' => $man['finputman'],
                    'finputtime' => $man['finputtime'],
                    'fjudgeman' => $man['fjudgeman'],
                    'fjudgetime' => $man['fjudgetime'],
                    'finspectman' => M('ad_input_user')->cache(true,86400*7)->where(['wx_id'=>$row['foperator_id']])->getField('alias'),
                    'finspectstate' => $row['fstate'],
                    'freason' => $row['freason'] ? $row['freason'] : null,
                    'finspecttime' => $row['fmodifytime'] ? date('Y-m-d H:i:s',$row['fmodifytime']) : '',
                ];
                array_push($data,$dataRow);
            }
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功','count'=>$count, 'data'=>$data]);
        }else{
            $this->display();
        }
    }

    /**
     * 获取所有质检任务列表(管理员)
     */
    public function listTask(){
        $taskid           = $this->P['taskid'];
        $fadname          = $this->P['fadname'];
        $fbrand           = $this->P['fbrand'];
        $fadowner         = $this->P['fadowner'];
        $fmediaclass      = $this->P['fmediaclass'];
        $fmedianame       = $this->P['fmedianame'];
        $fillegaltypecode = $this->P['fillegaltypecode'];
        $finspectstate    = $this->P['finspectstate'];
        $finspectman      = $this->P['finspectman'];
        $finputman        = $this->P['finputman'];
        $fjudgeman        = $this->P['fjudgeman'];
        $finspecttime     = $this->P['finspecttime'] ? $this->P['finspecttime'] : '';
        $finputtime       = $this->P['finputtime'] ? $this->P['finputtime'] : '';
        $fjudgetime       = $this->P['fjudgetime'] ? $this->P['fjudgetime'] : '';
        $fissue_date      = $this->P['fissue_date'] ? $this->P['fissue_date'] : '';
        $pageIndex        = $this->P['pageIndex'] ? $this->P['pageIndex'] : 1;
        $pageSize         = $this->P['pageSize'] ? $this->P['pageSize'] : 20;
        $stime            = $this->P['stime'] ? $this->P['stime'] : date('Y-01-01');
        $etime            = $this->P['etime'] ? $this->P['etime'] : date('Y-m-d');
        $stimeu           = strtotime($stime);
        $etimeu           = strtotime('+1 day',strtotime($etime))-1;
        $issueDate        = $fissue_date ? explode(',',$fissue_date) : [];
        $inspectDate      = $finspecttime ? explode(',',$finspecttime) : [];
        $inputDate        = $finputtime ? explode(',',$finputtime) : [];
        $judgeDate        = $fjudgetime ? explode(',',$fjudgetime) : [];
        $where = [
            'a.fcreatetime' => ['BETWEEN',[$stimeu,$etimeu]],
        ];
        $taskIdsArr = [];//过滤条件
        if(!empty($taskid)){
            $where['a.ftaskid'] = $taskid;
        }
        if(!empty($fmediaclass)){
            $where['a.fmediaclass'] = $fmediaclass;
        }
        // 搜索：质检状态
        if(!empty($finspectstate) || $finspectstate === '0'){
            if($finspectstate == 10){
                $where['a.fstate'] = ['IN',[0,1]];
            }elseif($finspectstate == 12){
                $where['a.fstate'] = ['IN',[2,3,4,5]];
            }else{
                $where['a.fstate'] = $finspectstate;
            }
        }
        // 搜索：质检时间
        if(!empty($inspectDate)){
            $sInsDate = strtotime($inspectDate[0]);
            $eInsDate = strtotime('+1 day',strtotime($inspectDate[1])) - 1;
            $where['a.fmodifytime'] = ['BETWEEN',[$sInsDate,$eInsDate]];
        }
        // 搜索：质检人
        if(!empty($finspectman)){
            $finspectmanid = M('ad_input_user')
                ->where(['wx_id|alias|nickname' => ['LIKE','%'.$finspectman.'%']])
                ->getField('wx_id',true);
            if(!empty($finspectmanid)){
                $where['a.foperator_id'] = ['IN',$finspectmanid];
            }
        }
        // 搜索：录入人
        $inputManIds = [];
        if(!empty($finputman)){
            $finputmanid = M('ad_input_user')
                ->where(['alias' => ['LIKE',$finputman.'%']])
                ->getField('wx_id');
            $traTaskid = M('task_input_link')
                ->alias('m')
                ->where(['wx_id'=>$finputmanid,'type'=>1])
                ->getField('DISTINCT m.taskid',true);
            $netTaskid = M('tnettask_flowlog')
                ->alias('n')
                ->where(['wx_id'=>$finputmanid,'logtype'=>12])
                ->getField('DISTINCT n.taskid',true);
            $inputManIds = array_merge($traTaskid,$netTaskid);
            $taskIdsArr['inputManIds'] = $inputManIds;
        }
        // 搜索：判定人
        $judgeManIds = [];
        if(!empty($fjudgeman)){
            $finputmanid = M('ad_input_user')
                ->where(['alias' => ['LIKE',$fjudgeman.'%']])
                ->getField('wx_id');
            $traTaskid = M('task_input_link')
                ->alias('m')
                ->where(['wx_id'=>$finputmanid,'type'=>2])
                ->getField('DISTINCT m.taskid',true);
            $netTaskid = M('tnettask_flowlog')
                ->alias('n')
                ->where(['wx_id'=>$finputmanid,'logtype'=>22])
                ->getField('DISTINCT n.taskid',true);
            $judgeManIds = array_merge($traTaskid,$netTaskid);
            $taskIdsArr['judgeManIds'] = $judgeManIds;
        }
        // 搜索：录入时间
        $taskInputDateIds = [];
        if(!empty($inputDate)){
            $sInpDate = strtotime($inputDate[0]);
            $eInpDate = strtotime('+1 day',strtotime($inputDate[1])) -1;
            $traTaskid = M('task_input_link')
                ->where(['update_time'=>['BETWEEN',[$sInpDate,$eInpDate]],'type'=>1])
                ->getField('taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask_flowlog')
                ->where(['createtime'=>['BETWEEN',[$sInpDate,$eInpDate]],'logtype'=>12])
                ->getField('taskid',true);
            $netTaskid = $netTaskid ? $netTaskid : [];
            $taskInputDateIds = array_merge($traTaskid,$netTaskid);
            $taskIdsArr['taskInputDateIds'] = $taskInputDateIds;
        }
        // 搜索：判定时间
        $taskJudgeDateIds = [];
        if(!empty($judgeDate)){
            $sJudgeDate = strtotime($judgeDate[0]);
            $eJudgeDate = strtotime('+1 day',strtotime($judgeDate[1])) -1;
            $traTaskid = M('task_input_link')
                ->where(['update_time'=>['BETWEEN',[$sJudgeDate,$eJudgeDate]],'type'=>2])
                ->getField('taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask_flowlog')
                ->where(['createtime'=>['BETWEEN',[$sJudgeDate,$eJudgeDate]],'logtype'=>22])
                ->getField('taskid',true);
            $netTaskid = $netTaskid ? $netTaskid : [];
            $taskJudgeDateIds = array_merge($traTaskid,$netTaskid);
            $taskIdsArr['taskJudgeDateIds'] = $taskJudgeDateIds;
        }
        // 搜索：广告名称
        $adNameIds = [];
        if(!empty($fadname)){
            $traTaskid = M('task_input')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->where(['b.fadname'=>['LIKE','%'.$fadname.'%']])
                ->getField('a.taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->where(['b.fadname'=>['LIKE','%'.$fadname.'%']])
                ->getField('a.taskid',true);
            $netTaskid = $netTaskid ? $netTaskid : [];
            $adNameIds = array_merge($traTaskid,$netTaskid);
            $taskIdsArr['adNameIds'] = $adNameIds;
        }
        // 搜索：广告品牌
        $brandIds = [];
        if(!empty($fbrand)){
            $traTaskid = M('task_input')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->where(['b.fbrand'=>['LIKE','%'.$fbrand.'%']])
                ->getField('a.taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->where(['b.fbrand'=>['LIKE','%'.$fbrand.'%']])
                ->getField('a.taskid',true);
            $netTaskid = $netTaskid ? $netTaskid : [];
            $brandIds = array_merge($traTaskid,$netTaskid);
            $taskIdsArr['brandIds'] = $brandIds;
        }
        // 搜索：广告主
        $adOwnerIds = [];
        if(!empty($fadowner)){
            $traTaskid = M('task_input')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->join('tadowner c ON c.fid=b.fadowner')
                ->where(['c.fname'=>['LIKE','%'.$fadowner.'%']])
                ->getField('a.taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->join('tadowner c ON c.fid=b.fadowner')
                ->where(['c.fname'=>['LIKE','%'.$fadowner.'%']])
                ->getField('a.taskid',true);
            $netTaskid = $netTaskid ? $netTaskid : [];
            $adOwnerIds = array_merge($traTaskid,$netTaskid);
            $taskIdsArr['adOwnerIds'] = $adOwnerIds;
        }
        // 搜索：媒体名称
        $mediaIds = [];
        if(!empty($fmedianame)){
            $traTaskid = M('task_input')
                ->alias('a')
                ->join('tmedia b ON b.fid=a.media_id')
                ->where(['b.fmedianame'=>['LIKE','%'.$fmedianame.'%']])
                ->getField('a.taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask')
                ->alias('a')
                ->join('tmedia b ON b.fid=a.media_id')
                ->where(['b.fmedianame'=>['LIKE','%'.$fmedianame.'%']])
                ->getField('a.taskid',true);
            $netTaskid = $netTaskid ? $netTaskid : [];
            $mediaIds = array_merge($traTaskid,$netTaskid);
            $taskIdsArr['mediaIds'] = $mediaIds;
        }
        // 搜索：发布日期
        $issueDateIds = [];
        if(!empty($issueDate)){
            $traTaskid = M('task_input')
                ->alias('a')
                ->where(['a.issue_date'=>['BETWEEN',[$issueDate[0],$issueDate[1]]]])
                ->getField('a.taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask')
                ->alias('a')
                ->where(['a.issue_date'=>['BETWEEN',[$issueDate[0],$issueDate[1]]]])
                ->getField('a.taskid',true);
            $netTaskid = $netTaskid ? $netTaskid : [];
            $issueDateIds = array_merge($traTaskid,$netTaskid);
            $taskIdsArr['issueDateIds'] = $issueDateIds;
        }
        // 搜索：违法类型
        $illegalIds = [];
        if(!empty($fillegaltypecode) || $fillegaltypecode === 0){
            $traTaskidIllegal = M('task_input')
                ->alias('a')
                ->where(['a.fillegaltypecode'=>$fillegaltypecode])
                ->getField('a.taskid',true);
            $traTaskidIllegal = $traTaskidIllegal ? $traTaskidIllegal : [];
            $netTaskidIllegal = M('tnettask')
                ->alias('a')
                ->where(['a.fillegaltypecode'=>$fillegaltypecode])
                ->getField('a.taskid',true);
            $netTaskid = $netTaskid ? $netTaskid : [];
            $illegalIds = array_merge($traTaskidIllegal,$netTaskidIllegal);
            $taskIdsArr['illegalIds'] = $illegalIds;
        }
        // 搜索条件ID取交集
        // $taskIdsArr = ['inputManIds','judgeManIds','taskInputDateIds','taskJudgeDateIds','adNameIds','brandIds','adOwnerIds','mediaIds','issueDateIds','illegalIds'];
        // $taskIdsArr = compact('inputManIds','judgeManIds','taskInputDateIds','taskJudgeDateIds','adNameIds','brandIds','adOwnerIds','mediaIds','issueDateIds','illegalIds');
        // foreach($taskIdsArr as $key=>$Ids){
        //     if(!empty($Ids)){
        //         if($firstKey === 0){
        //             $taskIds = $Ids;
        //             $firstKey = $key;
        //         }else{
        //             $taskIds = array_intersect($taskIds,$Ids);
        //         }
        //     }
        // }
        $taskIds = [];
        $firstKey = 0;
        foreach($taskIdsArr as $key=>$Ids){
            if(empty($Ids)){
                $taskIds = [];
                break;
            }else{
                if($firstKey === 0){
                    $taskIds = $Ids;
                    $firstKey = $key;
                }else{
                    $taskIds = array_intersect($taskIds,$Ids);
                }
            }
        }
        $data = [];
        if(IS_POST){
            if(!empty($taskIdsArr) && empty($taskIds)){
                $count = 0;
            // }elseif(!empty($taskIdsArr) && !empty($taskIds)){
            }elseif(!empty($taskIds)){
                $where['a.ftaskid'] = ['IN',$taskIds];
                
                $count = M('ttask_quality_inspection')
                    ->alias('a')
                    ->where($where)
                    ->count();
                if ($count > 0) {
                    $dataSet = M('ttask_quality_inspection')
                    ->alias('a')
                    ->where($where)
                    ->order('a.fcreatetime DESC')
                    ->page($pageIndex, $pageSize)
                    ->select();
                    $sql = M('ttask_quality_inspection')->getLastSql();
                    foreach ($dataSet as $key => $row) {
                        $taskRow = M($row['ftablename'])->where(['taskid'=>$row['ftaskid']])->find();
                        $adRow = M('tad')->where(['fadid'=>$taskRow['fadid']])->find();
                        $mediaRow = M('tmedia')->cache(true)->where(['fid'=>$taskRow['media_id']])->find();
                        $man = $this->getMan($row['ftablename'], $row['ftaskid']);
                        $data[] = [
                            'fid' => $row['fid'],
                            'taskid' => $row['ftaskid'],
                            'fpriority' => $row['fpriority'],
                            'tadname' => $taskRow['fadname'] ? $taskRow['fadname'] : $adRow['fadname'],
                            'fadname' => $taskRow['fadname'] ? $taskRow['fadname'] : $adRow['fadname'],
                            'adCateText1' => M('tadclass')->cache(true, 86400)->where(['fcode' => $adRow['fadclasscode']])->getField('ffullname'),
                            'fmediaclass' => $this->mediaclass[$taskRow['media_class']],
                            'fmedianame' => $mediaRow['fmedianame'],
                            'fissue_date' => $taskRow['issue_date'],
                            'fillegaltypecode' => $taskRow['fillegaltypecode'],
                            'finputman' => $man['finputman'],
                            'finputtime' => $man['finputtime'],
                            'fjudgeman' => $man['fjudgeman'],
                            'fjudgetime' => $man['fjudgetime'],
                            'finspectman' => M('ad_input_user')->cache(true, 86400*7)->where(['wx_id'=>$row['foperator_id']])->getField('alias'),
                            'finspectstate' => $row['fstate'],
                            'freason' => $row['freason'] ? $row['freason'] : null,
                            'finspecttime' => $row['fmodifytime'] ? date('Y-m-d H:i:s', $row['fmodifytime']) : '',
                        ];
                    }
                }
            }
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功','count'=>$count, 'data'=>$data]);
        }else{
            $this->display();
        }
    }

    /**
     * 获取我的质检任务列表(质检员)
     */
    public function myTask(){
        $taskid           = $this->P['taskid'];
        $fadname          = $this->P['fadname'];
        $fbrand           = $this->P['fbrand'];
        $fadowner         = $this->P['fadowner'];
        $fmediaclass      = $this->P['fmediaclass'];
        $fmedianame       = $this->P['fmedianame'];
        $fillegaltypecode = $this->P['fillegaltypecode'];
        $finspectstate    = $this->P['finspectstate'];
        $finspecttime     = $this->P['finspecttime'] ? $this->P['finspecttime'] : '';
        $fissue_date      = $this->P['fissue_date'] ? $this->P['fissue_date'] : '';
        $pageIndex        = $this->P['pageIndex'] ? $this->P['pageIndex'] : 1;
        $pageSize         = $this->P['pageSize'] ? $this->P['pageSize'] : 20;
        $stime            = $this->P['stime'] ? $this->P['stime'] : date('Y-01-01');
        $etime            = $this->P['etime'] ? $this->P['etime'] : date('Y-m-d');
        $stimeu           = strtotime($stime);
        $etimeu           = strtotime('+1 day',strtotime($etime))-1;
        $issueDate        = $fissue_date ? explode(',',$fissue_date) : [];
        $inspectDate      = $finspecttime ? explode(',',$finspecttime) : [];
        // 测试环境用
        $wx_id = session('wx_id') ? session('wx_id') : '469';
        if(!empty($taskid)){
            $where['a.ftaskid'] = ['LIKE','%'.$taskid.'%'];
        }
        if(!empty($fadname)){
            $taskIdAd = [];
            $traTaskid = M('task_input')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->where(['b.fadname'=>['LIKE','%'.$fadname.'%']])
                ->getField('a.taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->where(['b.fadname'=>['LIKE','%'.$fadname.'%']])
                ->getField('a.taskid',true);
            $taskIdAd = array_merge($traTaskid,$netTaskid);
            if(!empty($taskIdAd)){
                $where['a.ftaskid'] = ['IN',$taskIdAd];
            }
        }
        if(!empty($fbrand)){
            $taskIdBrand = [];
            $traTaskid = M('task_input')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->where(['b.fbrand'=>['LIKE','%'.$fbrand.'%']])
                ->getField('a.taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->where(['b.fbrand'=>['LIKE','%'.$fbrand.'%']])
                ->getField('a.taskid',true);
            $taskIdBrand = array_merge($traTaskid,$netTaskid);
            if(!empty($taskIdBrand)){
                $where['a.ftaskid'] = ['IN',$taskIdBrand];
            }
        }
        if(!empty($fadowner)){
            $taskIdAdOwner = [];
            $traTaskid = M('task_input')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->join('tadowner c ON c.fid=b.fadowner')
                ->where(['c.fname'=>['LIKE','%'.$fadowner.'%']])
                ->getField('a.taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask')
                ->alias('a')
                ->join('tad b ON b.fadid=a.fadid')
                ->join('tadowner c ON c.fid=b.fadowner')
                ->where(['c.fname'=>['LIKE','%'.$fadowner.'%']])
                ->getField('a.taskid',true);
            $taskIdAdOwner = array_merge($traTaskid,$netTaskid);
            if(!empty($taskIdAdOwner)){
                $where['a.ftaskid'] = ['IN',$taskIdAdOwner];
            }
        }
        if(!empty($fmedianame)){
            $taskIdMedia = [];
            $traTaskid = M('task_input')
                ->alias('a')
                ->join('tmedia b ON b.fid=a.media_id')
                ->where(['b.fmedianame'=>['LIKE','%'.$fmedianame.'%']])
                ->getField('a.taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask')
                ->alias('a')
                ->join('tmedia b ON b.fid=a.media_id')
                ->where(['b.fmedianame'=>['LIKE','%'.$fmedianame.'%']])
                ->getField('a.taskid',true);
            $taskIdMedia = array_merge($traTaskid,$netTaskid);
            if(!empty($taskIdMedia)){
                $where['a.ftaskid'] = ['IN',$taskIdMedia];
            }
        }
        if(!empty($issueDate)){
            $taskIdDate = [];
            $traTaskid = M('task_input')
                ->alias('a')
                ->where(['a.issue_date'=>['BETWEEN',[$issueDate[0],$issueDate[1]]]])
                ->getField('a.taskid',true);
            $traTaskid = $traTaskid ? $traTaskid : [];
            $netTaskid = M('tnettask')
                ->alias('a')
                ->where(['a.issue_date'=>['BETWEEN',[$issueDate[0],$issueDate[1]]]])
                ->getField('a.taskid',true);
            $taskIdDate = array_merge($traTaskid,$netTaskid);
            if(!empty($taskIdDate)){
                $where['a.ftaskid'] = ['IN',$taskIdDate];
            }
        }
        if(!empty($fillegaltypecode) || $fillegaltypecode === 0){
            $taskIdIllegal = [];
            $traTaskidIllegal = M('task_input')
                ->alias('a')
                ->where(['a.fillegaltypecode'=>$fillegaltypecode])
                ->getField('a.taskid',true);
            $traTaskidIllegal = $traTaskidIllegal ? $traTaskidIllegal : [];
            $netTaskidIllegal = M('tnettask')
                ->alias('a')
                ->where(['a.fillegaltypecode'=>$fillegaltypecode])
                ->getField('a.taskid',true);
            $taskIdIllegal = array_merge($traTaskidIllegal,$netTaskidIllegal);
            if(!empty($taskIdIllegal)){
                $where['a.ftaskid'] = ['IN',$taskIdIllegal];
            }
        }
        if(!empty($fmediaclass)){
            $where['a.fmediaclass'] = $fmediaclass;
        }
        if(!empty($finspectstate) || $finspectstate === '0'){
            $where['a.fstate'] = $finspectstate;
        }
        if(!empty($inspectDate)){
            $sInsDate = strtotime($inspectDate[0]);
            $eInsDate = strtotime('+1 day',strtotime($inspectDate[1])) - 1;
            $where['a.fmodifytime'] = ['BETWEEN',[$sInsDate,$eInsDate]];
        }
        $data = [];
        if(IS_POST){
            $where = [
                'a.fstate' => ['EGT',2],
                'b.foperator_id' => $wx_id,
                'b.logtype' => ['IN',[4,5,6,7]],
                'b.createtime' => ['BETWEEN',[$stimeu,$etimeu]],
            ];
            $count = M('ttask_quality_inspection_flowlog')
                ->alias('b')
                ->join('ttask_quality_inspection a ON a.fid = b.ftask_quality_inspection_id')
                ->where($where)
                ->count();
            $dataSet = M('ttask_quality_inspection_flowlog')
                ->alias('b')
                ->field('a.fid,a.ftablename,a.ftaskid,a.fpriority,a.fstate,a.freason,a.fmodifytime,b.foperator_id,b.foperator')
                ->join('ttask_quality_inspection a ON a.fid = b.ftask_quality_inspection_id')
                ->where($where)
                ->page($pageIndex,$pageSize)
                ->order('b.createtime DESC')
                ->select();
            $sql = M('ttask_quality_inspection_flowlog')->getLastSql();
            foreach($dataSet as $key => $row){
                $taskRow = M($row['ftablename'])->where(['taskid'=>$row['ftaskid']])->find();
                $adRow = M('tad')->where(['fadid'=>$taskRow['fadid']])->find();
                $mediaRow = M('tmedia')->where(['fid'=>$taskRow['media_id']])->find();
                $man = $this->getMan($row['ftablename'],$row['ftaskid']);
                $dataRow = [
                    'fid' => $row['fid'],
                    'taskid' => $row['ftaskid'],
                    'fpriority' => $row['fpriority'],
                    'tadname' => $taskRow['fadname'] ? $taskRow['fadname'] : $adRow['fadname'],
                    'fadname' => $taskRow['fadname'] ? $taskRow['fadname'] : $adRow['fadname'],
                    'adCateText1' => M('tadclass')->cache(true,86400)->where(['fcode' => $adRow['fadclasscode']])->getField('ffullname'),
                    'fmediaclass' => $this->mediaclass[$taskRow['media_class']],
                    'fmedianame' => $mediaRow['fmedianame'],
                    'fissue_date' => $taskRow['issue_date'],
                    'fillegaltypecode' => $taskRow['fillegaltypecode'],
                    'finputman' => $man['finputman'],
                    'finputtime' => $man['finputtime'],
                    'fjudgeman' => $man['fjudgeman'],
                    'fjudgetime' => $man['fjudgetime'],
                    'finspectman' => M('ad_input_user')->cache(true,86400*7)->where(['wx_id'=>$row['foperator_id']])->getField('alias'),
                    'finspectstate' => $row['fstate'],
                    'freason' => $row['freason'] ? $row['freason'] : null,
                    'finspecttime' => $row['fmodifytime'] ? date('Y-m-d H:i:s',$row['fmodifytime']) : '',
                ];
                array_push($data,$dataRow);
            }
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功','count' => $count, 'data'=>$data]);
        }else{
            $this->display();
        }
    }

    /**
     * 获取任务详情
     */
    public function getTaskInfo(){
        $adTable = 'tad';
        $tableName = 'ttask_quality_inspection';
        $flowTableName = 'ttask_quality_inspection_flowlog';

        $fid = $this->P['id'];
        $fmediaclass = $this->P['fmediaclass'];
        $taskid = $this->P['taskid'];
        $where = [
            'fid' => $fid,
        ];
        $taskInfo = M('ttask_quality_inspection')
            ->where($where)
            ->find();
        if(!empty($taskInfo)){
            // 任务表样本ID字段名
            $taskSamPkey = 'sam_id';
            // 样本表
            $samTable = 'ttvsample';
            // 样本表主键
            $samPkey = 'fid';
            // 样本表媒体ID字段名
            $fmediaidField = 'media_id';
            // 素材内容类型 电视-mp4 广播-mp3 报纸-img 网络-flv、swf、image、text
            $ftype = '';
            switch((int)$taskInfo['fmediaclass']){
                case 1:
                case 01:
                    $samTable = 'ttvsample';
                    $fmediaidField = 'media_id';
                    $ftype = 'mp4';
                    break;
                case 2:
                case 02:
                    $samTable = 'tbcsample';
                    $fmediaidField = 'media_id';
                    $ftype = 'mp3';
                    break;
                case 3:
                case 03:
                    $samTable = 'tpapersample';
                    $samPkey = 'fpapersampleid';
                    $fmediaidField = 'media_id';
                    $ftype = 'img';
                    break;
                case 5:
                case 05:
                    $samTable = 'todsample';
                    $fmediaidField = 'media_id';
                    break;
                case 13:
                    $taskSamPkey = 'major_key';
                    $samTable = 'tnetissue';
                    $samPkey = 'major_key';
                    $fmediaidField = 'media_id';
                    $ftype = 'url';//TODO:互联网素材类型需进一步考虑
                    break;
            }
            // 条件
            $whereData = [
                'a.taskid' => $taskInfo['ftaskid'],
            ];
            $taskData = M($taskInfo['ftablename'])
                ->alias('a')
                ->join($samTable.' b ON b.'.$samPkey.' = a.'.$taskSamPkey,'LEFT')
                ->join($adTable.' c ON c.fadid = b.fadid','LEFT')
                ->where($whereData)
                ->find();
            // 任务数据
            $adclassname = M('tadclass')->cache(true,120)->where(['fcode' => $taskData['adtype']])->getField('ffullname');
            $fmedianame = M('tmedia')->cache(true,86400)->where(['fid'=>$taskData[$fmediaidField]])->getField('fmedianame');
            $fadowner = M('tadowner')->cache(true,86400)->where(['fid'=>$taskData['fadowner']])->getField('fname');
            $adCateText1 = M('tadclass')->cache(true,120)->where(['fcode' => $taskData['fadclasscode']])->getField('ffullname');
            $adCateText2 = M('hz_ad_class')->cache(true,120)->where(['fcode' => $taskData['fadclasscode_v2']])->getField('ffullname');
            $adCateText2 = $adCateText2 ? $adCateText2 : '';
            // TODO：互联网ftype需要进一步考虑 2019-07-15
            // $ftype = $taskData['ftype'] ? $taskData['ftype'] : $ftype;
            $man = $this->getMan($taskInfo['ftablename'],$taskInfo['ftaskid']);
            $data = [
                'taskid'           => $taskInfo['ftaskid'],
                'wx_id'            => $taskInfo['foperator_id'],
                'fmediaclass'      => $taskInfo['fmediaclass'],
                'fmediaid'         => $taskData[$fmediaidField],
                'fmedianame'       => $fmedianame,
                'fissue_date'      => $taskData['issue_date'],
                'fsourcepath'      => $taskData['source_path'],
                'ftype'            => $ftype,
                'fadid'            => $taskData['fadid'],
                'fadname'          => $taskData['fadname'],
                'fbrand'           => $taskData['fbrand'],
                'fadowner'         => $fadowner,
                'fversion'         => $taskData['fversion'],
                'fspokesman'       => $taskData['fspokesman'],
                'is_long_ad'       => $taskData['is_long_ad'],
                'net_created_date' => $taskData['net_created_date'],
                'net_original_url' => $taskData['net_original_url'],
                'net_target_url'   => $taskData['net_target_url'],
                'thumb_url_true'   => $taskData['thumb_url_true'],
                'net_snapshot'     => $taskData['net_snapshot'],
                'fadclasscode'     => $taskData['fadclasscode'],
                'fadclasscode_v2'  => $taskData['fadclasscode_v2'],
                'adCateText1'      => $adCateText1,
                'adCateText2'      => $adCateText2,
                'fillegaltypecode' => $taskData['fillegaltypecode'],
                'fillegalcontent'  => htmlspecialchars_decode($taskData['fillegalcontent']),
                'fexpressioncodes' => $taskData['fexpressioncodes'],
                'fexpressions'     => $taskData['fexpressions'],
                'fconfirmations'   => $taskData['fconfirmations'],
                'fadmanuno'        => $taskData['fadmanuno'],
                'fmanuno'          => $taskData['fmanuno'],
                'fadapprno'        => $taskData['fadapprno'],
                'fapprno'          => $taskData['fapprno'],
                'fadent'           => $taskData['fadent'],
                'fent'             => $taskData['fent'],
                'fentzone'         => $taskData['fentzone'],
                'modifytime'       => date('Y-m-d H:i:s',$taskInfo['fmodifytime']),
                'finputman'        => $man['finputman'],
                'finputtime'       => $man['finputtime'],
                'fjudgeman'        => $man['fjudgeman'],
                'fjudgetime'       => $man['fjudgetime'],
                'finspectman'      => $man['fjudgeman'],
                'finspecttime'     => $man['fjudgetime'],
            ];
        }
        $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'data'=>$data]);
    }

    /**
     * 修改质检任务优先级
     * @param string $fid 质检任务ID，多个以逗号隔开
     * @param int   $fpriority 优先级，整数
     */
    public function setPriority(){
        $fid = $this->P['fid'];
        $fpriority = $this->P['fpriority'];
        if(!empty($fid)){
            $where = [
                'fid' => ['IN',$fid],
            ];
            $saveData = [
                'fpriority' => $fpriority,
            ];
            $res = M('ttask_quality_inspection')
                ->where($where)
                ->save($saveData);
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功']);
        }
        $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
    }

    /**
     * 判断任务领取后是否超时
     * @param   Int     $time 任务领取时间戳
     * @return  Boolean true-超时 false-未超时
     */
    public function isOvertime($time){
        $limittime = time() - $this->overtime;
        return (int)$time <= $limittime;
    }

    /**
     * 根据媒体类型返回任务表名
     */
    public function getTableNameByMediaClass($fmediaclass){
        $tablename = '';
        switch((int)$fmediaclass){
            case 1:
            case 01:
            case 2:
            case 02:
            case 3:
            case 03:
                $tablename = 'task_input';
                break;
            case 5:
            case 05:
                $tablename = 'todtask';
                break;
            case 13:
                $tablename = 'tnettask';
                break;
            default:
                break;
        }
        return $tablename;
    }

    /**
     * 根据任务获取相关操作人及操作时间
     */
    public function getMan($tablename,$taskid){
        $data = [];
        switch($tablename){
            case 'task_input':
                $finputInfo = M('task_input_link')->where(['taskid'=>$taskid,'type'=>1,'state'=>2])->find();
                $fjudgeInfo = M('task_input_link')->where(['taskid'=>$taskid,'type'=>2,'state'=>2])->find();
                $data = [
                    'finputman' => M('ad_input_user')->cache(true,86400*30)->where(['wx_id'=>$finputInfo['wx_id']])->getField('alias'),
                    'finputtime' => date('Y-m-d H:i:s',$finputInfo['update_time']),
                    'fjudgeman' => M('ad_input_user')->cache(true,86400*30)->where(['wx_id'=>$fjudgeInfo['wx_id']])->getField('alias'),
                    'fjudgetime' => date('Y-m-d H:i:s',$fjudgeInfo['update_time']),
                ];
                break;
            case 'tnettask':
                $finputInfo = M('tnettask_flowlog')->where(['taskid'=>$taskid,'logtype'=>12])->order('createtime DESC')->find();
                $fjudgeInfo = M('tnettask_flowlog')->where(['taskid'=>$taskid,'logtype'=>2])->order('createtime DESC')->find();
                $data = [
                    'finputman' => M('ad_input_user')->cache(true,86400*30)->where(['wx_id'=>$finputInfo['wx_id']])->getField('alias'),
                    'finputtime' => date('Y-m-d H:i:s',$finputInfo['createtime']),
                    'fjudgeman' => M('ad_input_user')->cache(true,86400*30)->where(['wx_id'=>$fjudgeInfo['wx_id']])->getField('alias'),
                    'fjudgetime' => date('Y-m-d H:i:s',$fjudgeInfo['createtime']),
                ];
                break;
        }
        return $data;
    }

    /**
     * 
     */
    public function PlanOrder(){
        $this->display();
    }

    /**
     * 测试用-添加质检任务测试数据
     */
    public function testAddTaskData(){
        $ftablename = $this->P['ftablename'] ? $this->P['ftablename'] : 'task_input';
        $media_class = $this->P['media_class'] ? $this->P['media_class'] : '01,02,03,13';
        // $media_class = implode(',',$media_class);
        $times = 10;
        $addCounts = 0;
        while($times > 0){
            $where = [
                'quality_state' => 0,
                'task_state' => 2,
                'media_class' => ['IN',$media_class],
            ];
            $taskList = M($ftablename)
                ->where($where)
                ->order('issue_date DESC')
                ->limit(100)
                ->select();
            foreach($taskList as $key => $taskData){
                $count = M('ttask_quality_inspection')
                    ->where([
                        'ftablename' => $ftablename,
                        'ftaskid' => $taskData['taskid']
                    ])
                    ->count();
                if($count == 0){
                    // 质检任务数据
                    $taskData = [
                        'ftablename' => $ftablename,
                        'ftaskid' => $taskData['taskid'],
                        'fmediaclass' => $taskData['media_class'],
                        'foperator_id' => 0,
                        'fstate' => 0,
                        'fpriority' => 0,
                        'fcreator' => 'test_'.$taskData['wx_id'],
                        'fcreatetime' => time(),
                    ];
                    $ftask_quality_inspection_id = M('ttask_quality_inspection')->add($taskData);
                    // 流程
                    $flowData = [
                        'ftask_quality_inspection_id' => $ftask_quality_inspection_id,
                        'foperator_id' => 0,
                        'log' => '创建质检任务',
                        'logtype' => 1,
                        'creator' => 'API_testAddTaskData',
                        'createtime' => time(),
                    ];
                    M('ttask_quality_inspection_flowlog')->add($flowData);
                    // 添加量
                    $addCounts++;
                }
            }
            if($addCounts > 0){
                break;
            }
            continue;
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'成功 '.$addCounts]);
    }
}