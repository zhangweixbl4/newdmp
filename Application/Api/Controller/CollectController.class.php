<?php
namespace Api\Controller;
use Think\Controller;

class CollectController extends Controller {
	
		/*获取采集点列表*/
    public function get_collect_list(){
		
		$collect_name = I('collect_name');//获取采集点名称

		
		$collectList = M('tcollect')->field('fid,fname,fcode')->where(array('tcollect.fstate'=>array('neq',-1),'fname'=>array('like','%'.$collect_name.'%')))->limit(10)->select();//查询采集点列表

		$this->ajaxReturn(array('code'=>0,'value'=>$collectList));
		
	}
	

	
}