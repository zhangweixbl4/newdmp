const adInfoInputArea = {
  template: `<div v-loading="loading">
    <el-form ref="adForm" :model="adInfo" :rules="rules" label-width="100px">
      <el-form-item label="广告名称" prop="fadname">
        <el-autocomplete @input="inputAdName" v-model="adInfo.fadname" :fetch-suggestions="searchAdName"></el-autocomplete>
      </el-form-item>
      <div>
        <el-form-item label="广告主" prop="fname">
          <el-autocomplete :disabled="adIsSure" v-model="adInfo.fname" :fetch-suggestions="searchAdOwner"></el-autocomplete>
        </el-form-item>
        <el-form-item label="商业类别" prop="adCateArr2">
          <el-cascader :disabled="adIsSure" :options="adClassTree" v-model="adInfo.adCateArr2" :filterable="true"></el-cascader>
        </el-form-item>
        <el-form-item label="广告类别" prop="adCateArr">
          <el-cascader :disabled="adIsSure" :options="adClassArr" v-model="adInfo.adCateArr" :filterable="true"></el-cascader>
        </el-form-item>
        <el-form-item label="广告品牌" prop="fbrand">
          <el-input v-model="adInfo.fbrand" :disabled="adIsSure"></el-input>
        </el-form-item>
      </div>
      <el-form-item>
        <el-button type="primary" @click="submitModify">提交</el-button>
      </el-form-item>
    </el-form>
  </div>`,
  data: function () {
    return {
      loading: false,
      adIsSure: false,
      adInfo: {},
      adClassArr: adClassArr,
      adClassTree: adClassTree,
      rules: {
        fadname: [{
          required: true,
          message: '请填写广告名称',
          trigger: 'change'
        }],
        fname: [{
          required: true,
          message: '请输入广告主',
          trigger: 'change'
        }],
        fbrand: [{
          required: true,
          message: '请输入广告品牌',
          trigger: 'change'
        }],
        adCateArr: [{
          required: true,
          message: '请选择',
          trigger: 'change'
        }],
        adCateArr2: [{
          required: true,
          message: '请选择',
          trigger: 'change'
        }],
      },
    }
  },
  props: [taskInfo],
  methods: {
    inputAdName: function () {
      $.post('/Api/Ad/checkAdIsSureByName', {
        adName: this.adInfo.fadname
      }).then((res) => {
        if (res.data.is_sure != 0) {
          this.adIsSure = true
        } else {
          this.adIsSure = false
        }
        // 只要找到广告信息, 就更新到任务中
        if (res.data.fadid) {
          this.adInfo = Object.assign(this.adInfo, res.data)
          this.adInfo.adCateArr = this._processAdClass(this.adInfo.fadclasscode)
          this.adInfo.adCateArr2 = this._processAdClass(this.adInfo.fadclasscode_v2)
        }
        this.$forceUpdate()
      })
    },
    _processAdClass: function (data) {
      let adClass = [];
      if (data.length === 6) {
        adClass.push(data.substr(0, 2))
        adClass.push(data.substr(0, 4))
        adClass.push(data)
      }
      if (data.length === 4) {
        adClass.push(data.substr(0, 2))
        adClass.push(data)
      }
      if (data.length === 2) {
        adClass.push(data)
      }
      return adClass
    },
    searchAdName: function (queryString, cb) {
      $.post("/Api/Ad/searchAdByName", {
        adName: queryString
      }, function (res) {
        cb(res.data);
      });
    },
    searchAdOwner: function (queryString, cb) {
      $.post("/Api/Adowner/searchAdOwner", {
        name: queryString
      }, function (res) {
        cb(res.data);
      });
    },
    submitModify: function () {
      this.adInfo.fadclasscode = !this.adInfo.adCateArr ? '' : this.adInfo.adCateArr[this.adInfo.adCateArr.length - 1]
      this.adInfo.fadclasscode_v2 = !this.adInfo.adCateArr2 ? '' : this.adInfo.adCateArr2[this.adInfo.adCateArr2.length - 1]
      this.adInfo.adIsSure = +this.adIsSure
      this.adInfo.media_class = taskInfo.media_class
      this.$refs.adForm.validate((res, fields) => {
        if (res) {
          this.$emit('submit', this.adInfo)
        } else {
          var msg = fields[Object.keys(fields)[0]][0].message;
          this.$alert(msg, '提示')
          this.loading = false
        }
      })
    },
  }
}