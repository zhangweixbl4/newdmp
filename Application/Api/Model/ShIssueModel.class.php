<?php
namespace Api\Model;
use Think\Exception;
use Think\Log;

/**
* 上海数据检查广告发布数据
*/
class ShIssueModel{
	/**
	* sh_issue表计划重新抽取，原数据转移备份
	* @param	String $month_date 日期 。如"2018-06-03"
	* @param	Int media_id 媒体ID
	* @param	Int userid 客户ID
	* @return	void 返回的数据
	*/
	public function backUpIssue($month_date,$media_id,$userid = 310000){
		$dataSet = M('sh_issue')->where(['fissuedate'=>$month_date,'fmediaid'=>$media_id,'fuserid'=>$userid])->select();
		$ret = M('sh_issue_origin')->addAll($dataSet);
		return true;
	}
	/**
	* sh_issue表计划重新抽取，原数据清空
	* @param	String $month_date 日期 。如"2018-06-03"
	* @param	Int media_id 媒体ID
	* @param	Int userid 客户ID
	* @return	void 返回的数据
	*/
	public function emptyIssue($month_date,$media_id,$userid = 310000){
		$ret = M('sh_issue')->where(['fissuedate'=>$month_date,'fmediaid'=>$media_id,'fuserid'=>$userid])->delete();
		return $ret;
	}
	/**
	* sh_issue表计划重新抽取备份并清理
	* @param	String $month_date 日期 。如"2018-06-03"
	* @param	Int media_id 媒体ID
	* @param	Int userid 客户ID
	* @return	void 返回的数据
	*/
	public function backUpEmptyIssue($month_date,$media_id,$userid = 310000){
		$retBack = $this->backUpIssue($month_date,$media_id,$userid);
		$retEmpty = $this->emptyIssue($month_date,$media_id,$userid);
		if($retBack){
			return true;
		}
		return false;
	}
	/**
	* 根据日期媒介分批获取发布记录并写入上海发布记录表sh_issue
	* @param	String $month_date 日期 。如"2018-06-03"
	* @param	Int media_id 媒体ID
	* @param	Int userid 客户ID
	* @return	void 返回的数据
	*/
	public function ShIssue($month_date,$media_id,$userid = 310000){
		$mediaInfo = M('tmedia')
			->alias('T')
			->cache(true,86400)
			->field('mo.fregionid,fmedianame,fmediaclassid')
			->join('tmediaowner AS mo ON mo.fid = T.fmediaownerid')
			->where(['T.fid'=>$media_id])
			->find();
		$mediaclassid = substr($mediaInfo['fmediaclassid'],0,2);
		if($mediaclassid == '01'){
			$media_tab = 'tv';
			$table_name = 'ttvissue_'.date('Ym',strtotime($month_date)).'_'.substr($mediaInfo['fregionid'],0,2);
		}elseif($mediaclassid == '02'){
			$media_tab = 'bc';
			$table_name = 'tbcissue_'.date('Ym',strtotime($month_date)).'_'.substr($mediaInfo['fregionid'],0,2);
		}elseif($mediaclassid == '03'){
			$media_tab = 'paper';
			$table_name = 'tpaperissue';
		}else{
			return false;
		}
		$tableName = 'sh_issue';
		$listFields = '
			fuserid,
			fmediaclassid,
			fsampleid,
			fmedianame,
			fregionid,
			fadname,
			fadowner,
			fbrand,
			fspokesman,
			fadclasscode,
			fstarttime,
			fendtime,
			sam_source_path,
			fmediaid,
			fissuedate,
			flength';
		$issueList = M($tableName)
			->field($listFields)
			->where(['fmediaid'=>$media_id,'fissuedate'=>$month_date,'fuserid'=>$userid])
			->select();	//查询上海数据表信息，用于排重
		// 保存上一条发布记录数据，用于判断和合并连续或重叠的相同样本的不同发布记录
		$preIssueInfo = [];
		if($media_tab == 'tv' || $media_tab == 'bc'){
			foreach($issueList as $issueData){
				$dataRep[$media_id.'_'.$issueData['fstarttime'].'_'.$userid] = $issueData;//用于排重的数据
			}
			try{
				$issueInfoList = M($table_name)
					->alias('tissue')
					->cache(true,1200)
					->field('
						tissue.fsampleid,
						tissue.fmediaid,
						tissue.fstarttime,
						tissue.fendtime,
						tissue.fissuedate,
						tissue.flength,
						tad.fadname AS fadname_ori,
						(case when tad.fadclasscode like "2202%" then replace(replace(tad.fadname,"）",""),"公益（","") else fadname end) AS fadname,
						tad.fadowner,
						tad.fbrand,
						tad.fadclasscode,
						tsample.fspokesman,
						tsample.favifilename as sam_source_path,
						tsample.fcreator,
						tsample.fcreatetime,
						tsample.fmodifier,
						tsample.fmodifytime
						')
					->join('t'.$media_tab.'sample as tsample on tsample.fid = tissue.fsampleid')
					->join('tad on tad.fadid = tsample.fadid')
					->where([
						'tissue.fmediaid'          => $media_id,
						'tissue.fissuedate'        => strtotime($month_date),
						'tad.fstate'               => ['GT',0],
						'LEFT(tad.fadclasscode,2)' => ['NOT IN',['23']],//过滤[其它类],包括节目预告(2302)
						'tad.fadname'              => ['notlike','%本媒体宣传%'],//本媒体宣传(2101)
						'tsample.fstate'           => 1,
						])
					->order('tissue.fstarttime ASC')
					->select();	
				$sql = M($table_name)->getLastSql();
			}catch(Exception $error) {
				return false;
			}
			foreach($issueInfoList as $key => $issueInfo){
				// 发布记录数据在添加到sh_issue表时，初审状态继承相同样本最新一条(天)发布记录的初审状态；
				$sameSampleDate = M($tableName)
					->where(['fsampleid'=>$issueInfo['fsampleid'],'fmediaid'=>$issueInfo['fmediaid'],'fuserid'=>$userid])
					->order('fissuedate DESC')
					->find();
				// TODO：1、继承同样本的字段真实值为0时的情况需处理 2、用户未修改但重新生产后正确的值会被继承的发布记录数据错误覆盖 
				$fisillegal   = $sameSampleDate['fisillegal'] ? $sameSampleDate['fisillegal'] : 0;
				$fadname      = $sameSampleDate['fadname'] ? $sameSampleDate['fadname'] : $issueInfo['fadname'];
				$fadowner     = $sameSampleDate['fadowner'] ? $sameSampleDate['fadowner'] : $issueInfo['fadowner'];
				$fbrand       = $sameSampleDate['fbrand'] ? $sameSampleDate['fbrand'] : $issueInfo['fbrand'];
				$fspokesman   = $sameSampleDate['fspokesman'] ? $sameSampleDate['fspokesman'] : $issueInfo['fspokesman'];
				$fadclasscode = $sameSampleDate['fadclasscode'] ? $sameSampleDate['fadclasscode'] : $issueInfo['fadclasscode'];
				// 发布记录数据
				$e_a_data['fuserid']   		 = $userid;												//客户ID
				$e_a_data['fmediaclassid']   = $mediaInfo['fmediaclassid'];							//媒介类型
				$e_a_data['fsampleid']       = $issueInfo['fsampleid']; 							//样本id
				$e_a_data['fmediaid']        = $issueInfo['fmediaid']; 								//媒介id
				$e_a_data['fmedianame']      = $mediaInfo['fmedianame'];							//媒介名称
				$e_a_data['fregionid']       = $mediaInfo['fregionid']; 							//地区id
				$e_a_data['fadname']         = $fadname;											//广告名称
				$e_a_data['fadowner']        = $fadowner;											//广告主
				$e_a_data['fbrand']          = $fbrand; 											//品牌
				$e_a_data['fspokesman']      = $fspokesman; 										//代言人
				$e_a_data['fadclasscode']    = $fadclasscode; 										//广告类别id
				$e_a_data['fstarttime']      = $issueInfo['fstarttime']; 							//发布开始时间戳
				$e_a_data['fendtime']        = $issueInfo['fendtime']; 								//发布结束时间戳
				$e_a_data['fissuedate']      = date('Y-m-d',$issueInfo['fissuedate']); 				//发布日期
				$e_a_data['flength']         = $issueInfo['flength']; 								//发布时长(秒)
				$e_a_data['sam_source_path'] = strval($issueInfo['sam_source_path']); 				//素材路径
				$e_a_data['fcreator']        = $issueInfo['fcreator'];								//创建人
				$e_a_data['fcreatetime']     = $issueInfo['fcreatetime'];							//创建时间
				$e_a_data['fmodifier']       = $issueInfo['fmodifier'];								//修改人
				$e_a_data['fmodifytime']     = $issueInfo['fmodifytime'];							//修改时间
				$e_a_data['fisillegal']      = $fisillegal;											//初审状态

				if(!$dataRep[$media_id.'_'.$issueInfo['fstarttime'].'_'.$userid]){
					// 记录不存在
					// TODO:暂跳过相同样本合并操作 2019-06-25 false
					if(false && !empty($preIssueInfo) && $e_a_data['fsampleid'] == $preIssueInfo['fsampleid']){
						// 如果样本相同，继续判断是否连续或重叠,将上一条记录更新为最小的开始时间和最大的结束时间
						if($e_a_data['fstarttime'] > $preIssueInfo['fstarttime'] && $e_a_data['fstarttime'] <= $preIssueInfo['fendtime']){
							$new_fendtime = $e_a_data['fendtime'] - $preIssueInfo['fendtime'] > 0 ? $e_a_data['fendtime'] : $preIssueInfo['fendtime'];
							$newIssueInfo = [
								'fstarttime' => $preIssueInfo['fstarttime'],
								'fendtime' => $new_fendtime,
							];
							$resPre = M($tableName)
								->where(['fmediaid'=>$preIssueInfo['fmediaid'],'fstarttime'=>$preIssueInfo['fstarttime']])
								->save($newIssueInfo);
						}
					}else{
						// 样本不相同，直接插入
						$fid = M($tableName)->add($e_a_data);
					}
				}else{
					// 记录已存在
					$need_save = false;
					$e_data = [];
					foreach($e_a_data as $data_field => $data_value){					//循环判断数据字段是否有修改
						if($data_value != $dataRep[$media_id.'_'.$issueInfo['fstarttime'].'_'.$userid][$data_field]){
							$e_data[$data_field] = $data_value;							//赋值修改字段
							$need_save = 1;												//把需要修改设为1
						}
					}
					if($need_save){
						$e_state = M($tableName)
							->where(['fstarttime'=>$issueInfo['fstarttime'],'fmediaid'=>$issueInfo['fmediaid'],'fuserid'=>$userid])
							->save($e_data);
					}
				}
				// 保存当前任务
				$preIssueInfo = $e_a_data;
			}
		}elseif($media_tab == 'paper'){
			foreach($issueList as $issueData){
				$dataRep[$media_id.'_'.$issueData['fissuedate'].'_'.md5($issueData['sam_source_path'])] = $issueData;//用于排重的数据
			}
			$issueInfoList = M($table_name)
				->alias('tissue')
				->cache(true,1200)
				->field('
					tissue.fpapersampleid as fsampleid,
					tissue.fmediaid,
					0 as fstarttime ,
					0 as fendtime,
					tad.fadname AS fadname_ori,
					(case when tad.fadclasscode like "2202%" then replace(replace(tad.fadname,"）",""),"公益（","") else fadname end) AS fadname,
					tad.fadowner,
					tad.fbrand,
					tsample.fspokesman,
					tad.fadclasscode,
					tissue.fissuedate,
					tsample.fjpgfilename as sam_source_path,
					tsample.fcreator,
					tsample.fcreatetime,
					tsample.fmodifier,
					tsample.fmodifytime
					')
				->join('tpapersample as tsample on tsample.fpapersampleid = tissue.fpapersampleid')
				->join('tad on tad.fadid = tsample.fadid')
				->where([
					'tissue.fmediaid'   => $media_id,
					'tissue.fissuedate' => $month_date,
					'tad.fstate'        => ['GT',0],
					'tad.fadclasscode'  => ['NOT IN',['2302']],//过滤节目预告(2302)
					'tad.fadname'       => ['notlike','%本媒体宣传%'],//本媒体宣传(2101)
					'tsample.fstate'    => 1,
					])
				->select();	
			foreach($issueInfoList as $issueInfo){
				// 发布记录数据在添加到sh_issue表时，初审状态继承相同样本最新一条(天)发布记录的初审状态；
				$sameSampleDate = M('sh_issue')
					->where(['fsampleid'=>$issueInfo['fsampleid'],'fmediaid'=>$issueInfo['fmediaid']])
					->order('fissuedate DESC')
					->find();
				$fisillegal   = $sameSampleDate['fisillegal'] ? $sameSampleDate['fisillegal'] : 0;
				$fadname      = $sameSampleDate['fadname'] ? $sameSampleDate['fadname'] : $issueInfo['fadname'];
				$fadowner     = $sameSampleDate['fadowner'] ? $sameSampleDate['fadowner'] : $issueInfo['fadowner'];
				$fbrand       = $sameSampleDate['fbrand'] ? $sameSampleDate['fbrand'] : $issueInfo['fbrand'];
				$fspokesman   = $sameSampleDate['fspokesman'] ? $sameSampleDate['fspokesman'] : $issueInfo['fspokesman'];
				$fadclasscode = $sameSampleDate['fadclasscode'] ? $sameSampleDate['fisillegal'] : $issueInfo['fadclasscode'];
				// 发布记录数据
				$e_a_data['fmediaclassid']   = $mediaInfo['fmediaclassid'];							//媒介类型
				$e_a_data['fsampleid']       = $issueInfo['fsampleid']; 							//样本id
				$e_a_data['fmediaid']        = $issueInfo['fmediaid']; 								//媒介id
				$e_a_data['fmedianame']      = $mediaInfo['fmedianame'];							//媒介名称
				$e_a_data['fregionid']       = $mediaInfo['fregionid']; 							//地区id
				$e_a_data['fadname']         = $fadname;											//广告名称
				$e_a_data['fadowner']        = $fadowner;											//广告主
				$e_a_data['fbrand']          = $fbrand; 											//品牌
				$e_a_data['fspokesman']      = $fspokesman; 										//代言人
				$e_a_data['fadclasscode']    = $fadclasscode; 										//广告类别id
				$e_a_data['fstarttime']      = $issueInfo['fstarttime']; 							//发布开始时间戳
				$e_a_data['fendtime']        = $issueInfo['fendtime']; 								//发布结束时间戳
				$e_a_data['fissuedate']      = date('Y-m-d',strtotime($issueInfo['fissuedate']));	//发布日期
				$e_a_data['flength']         = $issueInfo['fendtime'] - $issueInfo['fstarttime'];	//发布时长(秒)
				$e_a_data['sam_source_path'] = strval($issueInfo['sam_source_path']); 				//素材路径
				$e_a_data['fcreator']        = $issueInfo['fcreator'];								//创建人
				$e_a_data['fcreatetime']     = $issueInfo['fcreatetime'];							//创建时间
				$e_a_data['fmodifier']       = $issueInfo['fmodifier'];								//修改人
				$e_a_data['fmodifytime']     = $issueInfo['fmodifytime'];							//修改时间
				$e_a_data['fisillegal']      = $fisillegal;											//初审状态
				if(!$dataRep[$media_id.'_'.$month_date.'_'.md5($issueInfo['sam_source_path'])]){
					$fid = M($tableName)->add($e_a_data);
				}else{
					$need_save = false;
					$e_data = [];
					foreach($e_a_data as $data_field => $data_value){//循环判断数据字段是否有修改
						if($data_value != $dataRep[$media_id.'_'.$month_date.'_'.md5($issueInfo['sam_source_path'])][$data_field]){
							$e_data[$data_field] = $data_value;//赋值修改字段
							$need_save = 1;//把需要修改设为1
						}
					}
					if($need_save){//需要修改
						$e_state = M($tableName)
							->where(['fstarttime'=>strtotime($issueInfo['fstarttime']),'fmediaid'=>$issueInfo['fmediaid']])
							->save($e_data);
					}
				}
			}
		}
	}

	/**
	* 根据日期媒介分批获取发布记录并写入发布记录表sh_issue
	* @param	String $month_date 日期 。如"2018-06-03"
	* @param	Int media_id 媒体ID
	* @param	Int userid 用户标识ID
	* @return	Boolen 是否执行成功
	*/
	public function CheckIssue($month_date,$media_id,$userid = 0){
		$mediaInfo = M('tmedia')
			->alias('T')
			->cache(true,86400)
			->field('mo.fregionid,fmedianame,fmediaclassid')
			->join('tmediaowner AS mo ON mo.fid = T.fmediaownerid')
			->where(['T.fid'=>$media_id])
			->find();
		$mediaclassid = substr($mediaInfo['fmediaclassid'],0,2);
		if($mediaclassid == '01'){
			$media_tab = 'tv';
			$table_name = 'ttvissue_'.date('Ym',strtotime($month_date)).'_'.substr($mediaInfo['fregionid'],0,2);
		}elseif($mediaclassid == '02'){
			$media_tab = 'bc';
			$table_name = 'tbcissue_'.date('Ym',strtotime($month_date)).'_'.substr($mediaInfo['fregionid'],0,2);
		}elseif($mediaclassid == '03'){
			$media_tab = 'paper';
			$table_name = 'tpaperissue';
		}else{
			return false;
		}
		$tableName = 'sh_issue';
		$listFields = '
			fuserid,
			fmediaclassid,
			fsampleid,
			fmedianame,
			fregionid,
			fadname,
			fadowner,
			fbrand,
			fspokesman,
			fadclasscode,
			fstarttime,
			fendtime,
			sam_source_path,
			fmediaid,
			fissuedate,
			flength,
			fillegaltypecode,
			fillegalcontent,
			fexpressioncodes,
			fexpressions
		';
		$issueList = M($tableName)
			->field($listFields)
			->where(['fmediaid'=>$media_id,'fissuedate'=>$month_date,'fuserid'=>$userid])
			->select();	//查询上海数据表信息，用于排重
		// 保存上一条发布记录数据，用于判断和合并连续或重叠的相同样本的不同发布记录
		$preIssueInfo = [];
		if($media_tab == 'tv' || $media_tab == 'bc'){
			foreach($issueList as $issueData){
				$dataRep[$media_id.'_'.$issueData['fstarttime'].'_'.$userid] = $issueData;//用于排重的数据
			}
			try{
				$issueInfoList = M($table_name)
					->alias('tissue')
					->cache(true,1200)
					->field('
						tissue.fsampleid,
						tissue.fmediaid,
						tissue.fstarttime,
						tissue.fendtime,
						tissue.fissuedate,
						tissue.flength,
						tad.fadname,
						tad.fadowner,
						tad.fbrand,
						tad.fadclasscode,
						tsample.fspokesman,
						tsample.favifilename as sam_source_path,
						tsample.fcreator,
						tsample.fcreatetime,
						tsample.fmodifier,
						tsample.fmodifytime,
						tsample.fillegaltypecode,
						tsample.fillegalcontent,
						tsample.fexpressioncodes,
						tsample.fexpressions
						')
					->join('t'.$media_tab.'sample as tsample on tsample.fid = tissue.fsampleid')
					->join('tad on tad.fadid = tsample.fadid')
					->where([
						'tissue.fmediaid'=>$media_id,
						'tissue.fissuedate'=>strtotime($month_date),
						// 'LEFT(tad.fadclasscode,2)'=>['NOT IN',['23']],//过滤[其它类],包括节目预告(2302)
						'tad.fadname'=>['notlike','%本媒体宣传%'],//本媒体宣传(2101)
						'tsample.fstate' => 1,
						])
					->order('tissue.fstarttime ASC')
					->select();	
			}catch(Exception $error) {
				return false;
			}
			foreach($issueInfoList as $key => $issueInfo){
				$fisillegal   = 0;
				$fadname      = $issueInfo['fadname'];
				$fadowner     = $issueInfo['fadowner'];
				$fbrand       = $issueInfo['fbrand'];
				$fspokesman   = $issueInfo['fspokesman'];
				$fadclasscode = $issueInfo['fadclasscode'];
				// 发布记录数据
				$e_a_data['fuserid']    	  = $userid;											//客户标识ID
				$e_a_data['fmediaclassid']    = $mediaInfo['fmediaclassid'];						//媒介类型
				$e_a_data['fsampleid']        = $issueInfo['fsampleid']; 							//样本id
				$e_a_data['fmediaid']         = $issueInfo['fmediaid']; 							//媒介id
				$e_a_data['fmedianame']       = $mediaInfo['fmedianame'];							//媒介名称
				$e_a_data['fregionid']        = $mediaInfo['fregionid']; 							//地区id
				$e_a_data['fadname']          = $fadname;											//广告名称
				$e_a_data['fadowner']         = $fadowner;											//广告主
				$e_a_data['fbrand']           = $fbrand; 											//品牌
				$e_a_data['fspokesman']       = $fspokesman; 										//代言人
				$e_a_data['fadclasscode']     = $fadclasscode; 										//广告类别id
				$e_a_data['fstarttime']       = $issueInfo['fstarttime']; 							//发布开始时间戳
				$e_a_data['fendtime']         = $issueInfo['fendtime']; 							//发布结束时间戳
				$e_a_data['fissuedate']       = date('Y-m-d',$issueInfo['fissuedate']); 			//发布日期
				$e_a_data['flength']          = $issueInfo['flength']; 								//发布时长(秒)
				$e_a_data['sam_source_path']  = strval($issueInfo['sam_source_path']); 				//素材路径
				$e_a_data['fcreator']         = $issueInfo['fcreator'];								//创建人
				$e_a_data['fcreatetime']      = $issueInfo['fcreatetime'];							//创建时间
				$e_a_data['fmodifier']        = $issueInfo['fmodifier'];							//修改人
				$e_a_data['fmodifytime']      = $issueInfo['fmodifytime'];							//修改时间
				$e_a_data['fisillegal']       = $fisillegal;										//初审状态
				$e_a_data['fillegaltypecode'] = $issueInfo['fillegaltypecode'];						//违法类型
				$e_a_data['fillegalcontent']  = $issueInfo['fillegalcontent'];						//违法内容
				$e_a_data['fexpressioncodes'] = $issueInfo['fexpressioncodes'];						//违法表现代码
				$e_a_data['fexpressions']     = $issueInfo['fexpressions'];							//违法表现

				if(!$dataRep[$media_id.'_'.$issueInfo['fstarttime'].'_'.$userid]){
					// 记录不存在
					// TODO:暂跳过相同样本合并操作 2019-06-25 false
					if(false && !empty($preIssueInfo) && $e_a_data['fsampleid'] == $preIssueInfo['fsampleid']){
						// 如果样本相同，继续判断是否连续或重叠,将上一条记录更新为最小的开始时间和最大的结束时间
						if($e_a_data['fstarttime'] > $preIssueInfo['fstarttime'] && $e_a_data['fstarttime'] <= $preIssueInfo['fendtime']){
							$new_fendtime = $e_a_data['fendtime'] - $preIssueInfo['fendtime'] > 0 ? $e_a_data['fendtime'] : $preIssueInfo['fendtime'];
							$newIssueInfo = [
								'fstarttime' => $preIssueInfo['fstarttime'],
								'fendtime' => $new_fendtime,
							];
							$resPre = M($tableName)
								->where(['fmediaid'=>$preIssueInfo['fmediaid'],'fstarttime'=>$preIssueInfo['fstarttime']])
								->save($newIssueInfo);
						}
					}else{
						// 样本不相同，直接插入
						$fid = M($tableName)->add($e_a_data);
					}
				}else{
					// 记录已存在
					$need_save = false;
					$e_data = [];
					foreach($e_a_data as $data_field => $data_value){					//循环判断数据字段是否有修改
						if($data_value != $dataRep[$media_id.'_'.$issueInfo['fstarttime'].'_'.$userid][$data_field]){
							$e_data[$data_field] = $data_value;							//赋值修改字段
							$need_save = 1;												//把需要修改设为1
						}
					}
					if($need_save){
						$e_state = M($tableName)
							->where(['fstarttime'=>$issueInfo['fstarttime'],'fmediaid'=>$issueInfo['fmediaid'],'fuserid'=>$userid])
							->save($e_data);
					}
				}
				// 保存当前任务
				$preIssueInfo = $e_a_data;
			}
		}elseif($media_tab == 'paper'){
			// TODO：报纸数据抽取需完善
			foreach($issueList as $issueData){
				$dataRep[$media_id.'_'.$issueData['fissuedate'].'_'.md5($issueData['sam_source_path'])] = $issueData;//用于排重的数据
			}
			$issueInfoList = M($table_name)
				->alias('tissue')
				->cache(true,1200)
				->field('
					tissue.fpapersampleid as fsampleid,
					tissue.fmediaid,
					0 as fstarttime ,
					0 as fendtime,
					tad.fadname AS fadname_ori,
					(case when tad.fadclasscode like "2202%" then replace(replace(tad.fadname,"）",""),"公益（","") else fadname end) AS fadname,
					tad.fadowner,
					tad.fbrand,
					tsample.fspokesman,
					tad.fadclasscode,
					tissue.fissuedate,
					tsample.fjpgfilename as sam_source_path,
					tsample.fcreator,
					tsample.fcreatetime,
					tsample.fmodifier,
					tsample.fmodifytime
					')
				->join('tpapersample as tsample on tsample.fpapersampleid = tissue.fpapersampleid')
				->join('tad on tad.fadid = tsample.fadid')
				->where([
					'tissue.fmediaid'=>$media_id,
					'tissue.fissuedate'=>$month_date,
					'tad.fadclasscode'=>['NOT IN',['2302']],//过滤节目预告(2302)
					'tad.fadname'=>['notlike','%本媒体宣传%'],//本媒体宣传(2101)
					'tsample.fstate' => 1,
					])
				->select();	
			foreach($issueInfoList as $issueInfo){
				// 发布记录数据在添加到sh_issue表时，初审状态继承相同样本最新一条(天)发布记录的初审状态；
				$sameSampleDate = M('sh_issue')
					->where(['fsampleid'=>$issueInfo['fsampleid'],'fmediaid'=>$issueInfo['fmediaid']])
					->order('fissuedate DESC')
					->find();
				$fisillegal   = $sameSampleDate['fisillegal'] ? $sameSampleDate['fisillegal'] : 0;
				$fadname      = $sameSampleDate['fadname'] ? $sameSampleDate['fadname'] : $issueInfo['fadname'];
				$fadowner     = $sameSampleDate['fadowner'] ? $sameSampleDate['fadowner'] : $issueInfo['fadowner'];
				$fbrand       = $sameSampleDate['fbrand'] ? $sameSampleDate['fbrand'] : $issueInfo['fbrand'];
				$fspokesman   = $sameSampleDate['fspokesman'] ? $sameSampleDate['fspokesman'] : $issueInfo['fspokesman'];
				$fadclasscode = $sameSampleDate['fadclasscode'] ? $sameSampleDate['fisillegal'] : $issueInfo['fadclasscode'];
				// 发布记录数据
				$e_a_data['fmediaclassid']   = $mediaInfo['fmediaclassid'];							//媒介类型
				$e_a_data['fsampleid']       = $issueInfo['fsampleid']; 							//样本id
				$e_a_data['fmediaid']        = $issueInfo['fmediaid']; 								//媒介id
				$e_a_data['fmedianame']      = $mediaInfo['fmedianame'];							//媒介名称
				$e_a_data['fregionid']       = $mediaInfo['fregionid']; 							//地区id
				$e_a_data['fadname']         = $fadname;											//广告名称
				$e_a_data['fadowner']        = $fadowner;											//广告主
				$e_a_data['fbrand']          = $fbrand; 											//品牌
				$e_a_data['fspokesman']      = $fspokesman; 										//代言人
				$e_a_data['fadclasscode']    = $fadclasscode; 										//广告类别id
				$e_a_data['fstarttime']      = $issueInfo['fstarttime']; 							//发布开始时间戳
				$e_a_data['fendtime']        = $issueInfo['fendtime']; 								//发布结束时间戳
				$e_a_data['fissuedate']      = date('Y-m-d',strtotime($issueInfo['fissuedate']));	//发布日期
				$e_a_data['flength']         = $issueInfo['fendtime'] - $issueInfo['fstarttime'];	//发布时长(秒)
				$e_a_data['sam_source_path'] = strval($issueInfo['sam_source_path']); 				//素材路径
				$e_a_data['fcreator']        = $issueInfo['fcreator'];								//创建人
				$e_a_data['fcreatetime']     = $issueInfo['fcreatetime'];							//创建时间
				$e_a_data['fmodifier']       = $issueInfo['fmodifier'];								//修改人
				$e_a_data['fmodifytime']     = $issueInfo['fmodifytime'];							//修改时间
				$e_a_data['fisillegal']      = $fisillegal;											//初审状态
				if(!$dataRep[$media_id.'_'.$month_date.'_'.md5($issueInfo['sam_source_path'])]){
					$fid = M($tableName)->add($e_a_data);
				}else{
					$need_save = false;
					$e_data = [];
					foreach($e_a_data as $data_field => $data_value){//循环判断数据字段是否有修改
						if($data_value != $dataRep[$media_id.'_'.$month_date.'_'.md5($issueInfo['sam_source_path'])][$data_field]){
							$e_data[$data_field] = $data_value;//赋值修改字段
							$need_save = 1;//把需要修改设为1
						}
					}
					if($need_save){//需要修改
						$e_state = M($tableName)
							->where(['fstarttime'=>strtotime($issueInfo['fstarttime']),'fmediaid'=>$issueInfo['fmediaid']])
							->save($e_data);
					}
				}
			}
		}
		return true;
	}

	/**
	* 根据媒介日期获取处理完成(剪辑和录入判定均完成)的发布记录并写入上海发布记录表sh_issue
	* @Param	String $date 日期 。如"2018-06-03"
	* @Param	Int media 媒体ID
	* @Return	void 返回的数据
	*/
	public function getIssueData($date,$media){
		// TODO:计划内媒介日期且数据已完成

		// TODO:计划外媒介日期且数据已完成

	}

	/**
	 * 操作日志
	 */
	public function log($logArr = []){
		if(!empty($logArr)){
			$log = [
				'fapimedialist_id' => $logArr['fapimedialist_id'] ? $logArr['fapimedialist_id'] : 0,
				'foperator'        => $logArr['foperator'] ? $logArr['foperator'] : 0,
				'foperatetime'     => $logArr['foperatetime'] ? $logArr['foperatetime'] : date('Y-m-d H:i:s'),
				'flogtype'         => $logArr['flogtype'] ? $logArr['flogtype'] : 0,
				'flog'             => $logArr['flog'] ? $logArr['flog'] : '',
				'fcontent'         => $logArr['fcontent'] ? $logArr['fcontent'] : '',
				'fcreator'         => $logArr['fcreator'] ? $logArr['fcreator'] : $this->getClientIp(),
				'fcreatetime'      => time(),
			];
			$res = M('tsh_flowlog')->add($log);
			if($res){
				return true;
			}
		}
		return false;
	}

	/**
	 * 获取客户端IP地址
	 * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
	 * @param boolean $adv 是否进行高级模式获取（有可能被伪装）
	 * @return mixed
	 */
	public function getClientIp($type = 0,$adv = false) {
		$type = $type ? 1 : 0;
		static $ip = NULL;
		if ($ip !== NULL) return $ip[$type];
		if($adv){
			if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
				$pos = array_search('unknown',$arr);
				if(false !== $pos) unset($arr[$pos]);
				$ip = trim($arr[0]);
			}elseif(isset($_SERVER['HTTP_CLIENT_IP'])) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			}elseif(isset($_SERVER['REMOTE_ADDR'])) {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
		}elseif(isset($_SERVER['REMOTE_ADDR'])) {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		// IP地址合法验证
		$long = sprintf("%u",ip2long($ip));
		$ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
		return $ip[$type];
	}
}