<?php
//开放接口控制器
namespace Admin\Controller;
use Think\Controller;
class OpenapiController extends BaseController {

    public function index(){
		
		if(A('Admin/Authority','Model')->authority('200241') === 0){
			exit('没有权限');
		}
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$keyword = I('keyword');//搜索关键词
		$where = array();
		if($keyword != ''){
			$where['open_api.api_name|api_path'] = array('like','%'.$keyword.'%');
		} 
		$where['fstate'] = array('neq',-1);
		$count = M('open_api')->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$openapiList = M('open_api')->where($where)->order('api_id desc')->limit($Page->firstRow.','.$Page->listRows)->select();

		$openapiList = list_k($openapiList,$p,$pp);//为列表加上序号
		$this->assign('openapiList',$openapiList);
		$this->assign('page',$Page->show());//分页输出
		$this->display();
	}
	
	
	/*编辑添加*/
	public function add_edit_api(){

		$api_id = I('api_id');
		$apiInfo = M('open_api')->where(array('api_id'=>$api_id))->find();
		$this->assign('apiInfo',$apiInfo);
		$this->display();
		
	}
	
	/*编辑添加 处理*/
	public function ajax_add_edit_api(){
		
		$api_id = I('api_id',0);//获取接口ID
		$a_e_data = array();
		$a_e_data['api_name'] = I('api_name');//接口名称
		$a_e_data['api_path'] = I('api_path');//接口路径
		$a_e_data['request_mode'] = I('request_mode');//请求方式	
		$a_e_data['api_details'] = I('api_details','','');//接口详情

		
		
		$a_e_data['fstate'] = I('fstate',1);//状态，-1删除，0-无效，1-有效
		
		
		if($api_id == 0){
			if(A('Admin/Authority','Model')->authority('200242') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
			}
			
			$is_repeat = M('open_api')->where(array('api_name'=>$a_e_data['api_name']))->count();//查询接口名称是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'接口名称重复'));//返回接口名称重复
			
			$a_e_data['fcreator'] = '系统最高管理员';//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = '系统最高管理员';//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('open_api')->add($a_e_data);
			if($rr > 0){//判断是否添加成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功','api_id'=>$rr));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));//返回失败
			}
		}else{
			if(A('Admin/Authority','Model')->authority('200243') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
			}
			$is_repeat = M('open_api')->where(array('api_id'=>array('neq',$api_id),'api_name'=>$a_e_data['api_name']))->count();//查询接口名称是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'接口名称重复'));//返回接口名称重复
			
			$a_e_data['fmodifier'] = '系统最高管理员';//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('open_api')->where(array('api_id'=>$api_id))->save($a_e_data);
			if($rr > 0){//判断是否添加成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功','api_id'=>$api_id));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));//返回失败
			}
		}
	}
	
	/*接口调试页面*/
	public function api_debug(){
		if(A('Admin/Authority','Model')->authority('200251') === 0){
			exit('没有权限');
		}
		$this->display();
	}
	
	/*接口调试请求页*/
	public function ajax_api_debug(){
		$personInfo = session('personInfo');//人员信息
		$request = I('request','','');
		$api_path = I('api_path');
		if(strstr($api_path,'http://') || strstr($api_path,'https://')){
			$url = $api_path;
		}else{
			$url = 'http://'.$_SERVER['HTTP_HOST'].$api_path.'?person_code='.$personInfo['fcode'].'&person_password='.$personInfo['fpassword'];
		}
		
		$reply = http($url,$request,'POST',false,30);
		//var_dump($reply);
		$format_reply = json_decode($reply);
		if($format_reply == null){
			echo $reply;
			exit;
		}
		
		$format_reply = json_encode($format_reply,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','reply'=>$format_reply."\n\n----------------------以下为格式化前原返回内容-----------------------------------------\n\n".$reply));
		
	}
	
	
	
	
	
		
	

}