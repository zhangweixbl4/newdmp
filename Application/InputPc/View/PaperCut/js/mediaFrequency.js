const mediaFrequency = {
    name: 'mediaFrequency',
    props: ['mediaid', 'frequency'],
    template: `<div v-loading="loading">
    <el-form :model="frequency">
        <el-form-item label="类型">
            <el-radio-group v-model="frequency.type" @change="changeType">
                <el-radio label="daily">每日</el-radio>
                <el-radio label="weekly">每周</el-radio>
                <el-radio label="monthly">每月</el-radio>
                <el-radio label="yearly">每年</el-radio>
            </el-radio-group>
        </el-form-item>
        <el-form-item label="详情" v-if="frequency.type && frequency.type !== 'daily'">
            <el-checkbox :indeterminate="isIndeterminate" v-model="checkAll" @change="handleCheckAllChange">全选</el-checkbox>
            <br>
            <el-checkbox-group v-model="frequency.date">
                <el-checkbox :label="item" v-for="item in dateList"></el-checkbox>
            </el-checkbox-group>
            <br>
            <el-checkbox-group v-model="frequency.date2" v-if="frequency.type === 'yearly' && frequency.date.length !== 0">
                <el-checkbox :label="item" v-for="item in monthDateList"></el-checkbox>
            </el-checkbox-group>
        </el-form-item>
        <el-form-item v-if="frequency.type">
            <el-button type="primary" @click="submit">保存发布频率</el-button>
        </el-form-item>
    </el-form>
</div>`,
    data: function () {
        return {
            loading: false,
            // frequency: {
            //     type: null,
            //     date: [],
            //     date2: [],
            // },
            checkAll: false,
            isIndeterminate: false,
        }
    },
    computed: {
        dateList: function () {
            let count = 1
            let suffix = ''
            if (this.frequency.type === 'weekly'){
                count = 7
            } else if (this.frequency.type === 'monthly'){
                count = 31
                suffix = '日'
            } else if (this.frequency.type === 'yearly'){
                count = 12
                suffix = '月'
            }
            let arr = []
            for (let i = 1; i <= count; i++){
                arr.push(i + suffix)
            }
            return arr
        },
        monthDateList: function () {
            let arr = []
            for (let i = 1; i <= 31; i++){
                arr.push(i + '日')
            }
            return arr
        }
    },
    methods: {
        dataInit: function () {
            this.frequency = {
                type: null,
                date: [],
                date2: [],
            }
            this.checkAll = false
            this.isIndeterminate = false
        },
        handleCheckAllChange: function (value) {
            this.frequency.date = value ? this.dateList : [];
            this.isIndeterminate = false;
        },
        changeType: function () {
            this.frequency.date = []
            this.frequency.date2 = []
            this.checkAll = false
            this.isIndeterminate = false
        },
        submit: function () {
            $.post('/Api/Media/editFrequency', {frequency: this.frequency, mediaId: this.mediaid}).then((res) => {
                if (res.code == 0){
                    this.$message.success(res.msg)
                    this.$emit('success')
                }else{
                    this.$message.error(res.msg)
                }
            })
        }

    }
}