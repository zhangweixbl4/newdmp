<?php
/**
 * 基础控制器

 */
namespace Gongshang\Controller;
use Think\Controller;

class BaseController extends Controller {

	/**
	* 访问数据过滤

	*/
	public function _initialize() {
		$direct = array(//设置无须验证的页面
					'Gongshang/Login/ajax_login',
					'Gongshang/Index/index',
					'Gongshang/Login/index',
					'Gongshang/Login/verify',
					'Gongshang/Media/edit_tv_ad_part',
					'Gongshang/Media/edit_bc_ad_part',
					
					
						);
		if(!session('regulatorpersonInfo.fid') && !in_array(MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME ,$direct)){
			header('HTTP/1.1 404 Not Found');
			header("status: 404 Not Found");
			$this->error('登录已过期，请重新登录!!',U('Gongshang/Login/index'));
//			$this->ajaxReturn(array('code'=>-2,'msg'=>'need log in again'));
		}

	}

	/**
	 * 判断权限，获取相应最终媒体列表
	 * @return array $media 媒体列表
	 */
	public function get_access_media() {
		//是否有指定的媒体
		if(session('regulatorpersonInfo.special_media')){
			$media=json_decode(session('regulatorpersonInfo.special_media'),true);//json转成数组
		}else{
			$media=$this->get_regulator_mediaid(session('regulatorpersonInfo.fregulatorid'));//监管机构对应的媒体
			//如果没有对应的媒体，用地区id取媒体列表
//			if(!$media){
//				$media=$this->get_region_mediaid(session('regulatorpersonInfo.regulator_regionid'));
//			}
		}
		return  $media;
	}

	/**
 * 监管机构对应的媒体
 * @param $regulator_code  机构id
 * @return array
 * by hs
 */
	public function get_regulator_mediaid($regulator_code) {
		$res= M('tregulatormedia')->field('fmediaid')->where(array('fregulatorcode' => $regulator_code,'tregulatormedia.fstate' => array('neq',-1)))->select();
		$data = array_column($res, 'fmediaid');
		if(!$data){
			$data='';
		}
		return  $data;
	}

	/**
	 * @param $fregionid 机构的地区id
	 * @return array mediaid 列表
	 */
	public function get_region_mediaid($fregionid) {
		$res =M('tmedia')
			->field('tmedia.fid')
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid ', 'LEFT')//媒体列别
			->where(array('tmediaowner.fstate'=>1,'tmedia.fstate'=>1,'tmediaowner.fregionid'=>$fregionid))
			->select();
		$data = array_column($res, 'fid');
		return  $data;
	}
	/*检索 发布媒体*/
	public function ajax_tregulator_media(){
		$where=array();
		if(I('fmedianame')){
			$where['tmedia.fmedianame'] = array('like','%'.I('fmedianame').'%');
		}
		$data= M('tregulatormedia')->field('tregulatormedia.fmediaid, tmedia.fmedianame')
			->join('tmedia on  tregulatormedia.fmediaid=tmedia.fid', 'LEFT')//媒介信息
			->where(array('tregulatormedia.fregulatorcode' =>session('regulatorpersonInfo.fregulatorid')))
			->where($where)
			->limit(10)
			->select();
		$this->ajaxReturn(array('code'=>0,'value'=>$data));
	}
	/**
	 * 对应违法类型
	 * @param $code
	 * @return array
	 * by hs
	 */
	public function get_fillegaltype($code) {
		$res= M('tillegaltype')->where(array('fcode' => $code))->getField('fillegaltype');
		return  $res;

	}

	/**
	 * @param $regulator_id
	 * @return array|string
	 * 机构id对应机构信息
	 */
	public function get_regulator_data($regulator_id) {
		$res= M('tregulator')->where(array('fid' => $regulator_id,'fstate' =>1))->find();
		return  $res;
	}

	/**
	 * @param $regulator_id 要查找下级的部门的 id
	 * @return mixed
	 */
	public function get_next_regulator($regulator_id) {
		$data = M('tregulator')->where(array('fpid'=>$regulator_id,'fkind'=>2,'fstate' =>1))->select();
		return  $data;
	}

	/**
	 * @param $person_id
	 * @return mixed
	 * 人员信息
	 */
	public function get_tregulatorperson_details($person_id) {
		$data = M('tregulatorperson')->where(array('fid'=>$person_id))->find(); //人员详情、
		return  $data;
	}

	/**
	 * @param $fpcode 上级广告类别代码
	 * @param $self  是否加自己
	 * @return array|mixed
	 */
	public function get_next_tadclasscode($fpcode,$self=false) {
		$data = M('tadclass')->where(array('fpcode'=>$fpcode))->getField('fcode',true); //人员详情、
		if($data && $self){
			$data[]=$fpcode;
		}
		return  $data;
	}

	/**
	 * 添加数据 tregulatormedia
	 */
	public function insert_tregulatormedia() {
//		SELECT r.fid,r.fregionid as rfregionid , mo.fregionid as mofregionid,m.fid as mediaid,m.fmedianame FROM tregulator as r
//LEFT JOIN tmediaowner as mo on r.fregionid = mo.fregionid
//LEFT JOIN tmedia as m on mo.fid = m.fmediaownerid
//WHERE r.ftype= 20 and r.fkind= 1;


		$data =M('tregulator')
			->field('
				 tregulator.fid as fregulatorcode,
				 tregulator.fregionid as regulator_fregionid ,
				 tmediaowner.fregionid as mediaowner_fregionid,
				 tmedia.fid as fmediaid
									')
			->join('tmediaowner on tregulator.fregionid=tmediaowner.fregionid', 'LEFT')//违法类型
			->join('tmedia on tmediaowner.fid = tmedia.fmediaownerid ', 'LEFT')//媒体列别
			->where(array('tregulator.ftype'=>20,'tregulator.fkind'=>1,'tregulator.fstate'=>1,'tmediaowner.fstate'=>1,'tmedia.fstate'=>1,))
		->select();
		$res=M('tregulatormedia')->addAll($data);
	}

}