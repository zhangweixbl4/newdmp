const data = `http://a7.debug.hz-data.com/Admin/Media/change_ts?sourceMediaId=11000010003980&mediaId=11100010000056 &startTime=2018-11-01 00:00:00&endTime=2018-11-01 23:59:59
http://a7.debug.hz-data.com/Admin/Media/change_ts?sourceMediaId=11000010003980&mediaId=11100010000056 &startTime=2018-11-02 00:00:00&endTime=2018-11-02 23:59:59
http://a7.debug.hz-data.com/Admin/Media/change_ts?sourceMediaId=11000010003980&mediaId=11100010000056 &startTime=2018-11-03 00:00:00&endTime=2018-11-03 23:59:59
http://a7.debug.hz-data.com/Admin/Media/change_ts?sourceMediaId=11000010003980&mediaId=11100010000056 &startTime=2018-11-04 00:00:00&endTime=2018-11-04 23:59:59
http://a7.debug.hz-data.com/Admin/Media/change_ts?sourceMediaId=11000010003980&mediaId=11100010000056 &startTime=2018-11-05 00:00:00&endTime=2018-11-05 23:59:59
http://a7.debug.hz-data.com/Admin/Media/change_ts?sourceMediaId=11000010003980&mediaId=11100010000056 &startTime=2018-11-06 00:00:00&endTime=2018-11-06 23:59:59
http://a7.debug.hz-data.com/Admin/Media/change_ts?sourceMediaId=11000010003980&mediaId=11100010000056 &startTime=2018-11-07 00:00:00&endTime=2018-11-07 23:59:59
http://a7.debug.hz-data.com/Admin/Media/change_ts?sourceMediaId=11000010003980&mediaId=11100010000056 &startTime=2018-11-08 00:00:00&endTime=2018-11-08 23:59:59
http://a7.debug.hz-data.com/Admin/Media/change_ts?sourceMediaId=11000010003980&mediaId=11100010000056 &startTime=2018-11-09 00:00:00&endTime=2018-11-09 23:59:59`

const attrArr = data.split('\n')
const LENGTH = attrArr.length
let i = 0
let err = []
function getUrl(i) {
  axios.get(attrArr[i])
    .then(res => {
      if (res.data.code !== 0) {
        err.push(`第${i}条请求执行出错，错误信息：${res.data.msg}\n`)
      }
      if (i === (LENGTH - 1)) {
        console.log(`共执行${LENGTH}条请求，${err.length} 条请求执行出错`)
        console.log(err.join('') || '无错误日志！')
        return false
      }
      return getUrl( i += 1)
    })
}

getUrl(i)