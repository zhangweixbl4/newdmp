<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 国家局跨地域重点案件线索列表
 * by zw
 */

class GckkdyzdanjianController extends BaseController{
  /**
  * 重点线索线索接收列表
  * by zw
  */
  public function jszdanjian_list(){
    Check_QuanXian(['kdyanjian']);
    session_write_close();

    $p  = I('page', 1);//当前第几页
    $pp = 20;//每页显示多少记录
    $area = I('area');

     //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $where_time = gettimecondition($years,$timetypes,$timeval,'a.fcreate_time',-1);

    $where_tia['a.fstatus']       = array('gt',0);
    $where_tia['a.fdomain_isout'] = 10;

    if(!empty($area)){
      $where_tia['fget_regid']  = '20'.$area;
    }else{
      $where_tia['_string'] = 'locate('.session('regulatorpersonInfo.fregulatorpid').',fparticipate_regids)>0 and '.$where_time;
    }

    $count = M('tbn_illegal_zdad')
      ->alias('a')
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    
    $do_tia = M('tbn_illegal_zdad')
      ->alias('a')
      ->field('a.fid,a.fad_name,a.fill_type,a.fsend_time,a.fresult_datetime,a.fget_regname,a.fsend_regname,a.fview_status,a.fresult,a.fcreate_time,a.fstatus')
      ->where($where_tia)
      ->order('a.fid desc')
      ->page($p,$pp)
      ->select();
  
    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
  }

  /**
  * 重点线索接收信息获取
  * by zw
  */
  public function view_jszdanjian(){
    $fid = I('fid');//线索ID
    $where_tia['a.fid']         = $fid;
    $where_tia['a.fstatus']     = array('gt',0);
    $where_tia['a.fdomain_isout'] = 10;
    $do_tia = M('tbn_illegal_zdad')
      ->field('a.*,b.ffullname')
      ->alias('a')
      ->join('tadclass b on a.ftadclass=b.fcode','left')
      ->where($where_tia)
      ->find();//接收信息

    if(!empty($do_tia)){
      M()->execute('update tbn_illegal_zdad set fview_status=10 where fid='.$fid);
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_tia));
    }else{
      $this->ajaxReturn(array('code'=>1,'msg'=>'获取失败'));
    }
  }

  /**
  * 重点线索接收信息附件信息获取
  * by zw
  */
  public function attach_zdanjian(){
    $fid    = I('fid');//线索ID
    $types  = I('types');//附件类型，0交办附件，10上报证据
    // $where_tia['_string']     = 'locate('.session('regulatorpersonInfo.fregulatorpid').',fparticipate_regids)>0';
    $where_tia['b.fid']         = $fid;
    $where_tia['b.fstatus']     = array('gt',0);
    $where_tia['b.fdomain_isout'] = 10;
    $where_tia['a.fstate'] = 0;

    $do_tiza = M('tbn_illegal_zdad_attach')->alias('a')->join('tbn_illegal_zdad b on a.fillegal_ad_id=b.fid')->field('a.fid,a.fattach,a.fattach_url')->where(['fillegal_ad_id'=>$fid,'a.ftype'=>$types,'a.fstate'=>0])->select();

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','filedata'=>$do_tiza));
  }

  /**
  * 重点线索线索派发列表
  * by zw
  */
  public function jbzdanjian_list(){
    Check_QuanXian(['kdyanjian']);
    session_write_close();

    $p  = I('page', 1);//当前第几页
    $pp = 20;//每页显示多少记录
    $area = I('area');

     //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $where_time = gettimecondition($years,$timetypes,$timeval,'a.fcreate_time',-1);

    $where_tia['a.fstatus']       = array('gt',0);
    $where_tia['a.fdomain_isout'] = 10;

    if(!empty($area)){
      $where_tia['fsend_regid']  = '20'.$area;
    }else{
      $where_tia['_string'] = '((a.fsend_regid = '.session('regulatorpersonInfo.fregulatorpid').' and locate('.session('regulatorpersonInfo.fregulatorpid').',fparticipate_regids)=0) or (locate('.session('regulatorpersonInfo.fregulatorpid').',fparticipate_regids)>0 and fwaitregid<>0 and fwaitregid<>'.session('regulatorpersonInfo.fregulatorpid').')) and '.$where_time;
    }

    $count = M('tbn_illegal_zdad')
      ->alias('a')
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    
    $do_tia = M('tbn_illegal_zdad')
      ->alias('a')
      ->field('a.fid,a.fad_name,a.fill_type,a.fsend_time,a.fresult_datetime,a.fget_regname,a.fsend_regname,a.fview_status,a.fresult,a.fcreate_time,a.fill_content,a.fresult_endtime,a.fstatus')
      ->where($where_tia)
      ->order('a.fid desc')
      ->page($p,$pp)
      ->select();
  
    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
  }

  /**
  * 重点线索交办信息获取
  * by zw
  */
  public function view_jbzdanjian(){
    $system_num = getconfig('system_num');//客户编号
    $fid = I('fid');//线索ID
    $where_tia['a.fid']         = $fid;
    $where_tia['a.fstatus']     = array('gt',0);
    $where_tia['a.fdomain_isout'] = 10;
    $do_tia = M('tbn_illegal_zdad')
      ->field('a.*,b.ffullname')
      ->alias('a')
      ->join('tadclass b on a.ftadclass=b.fcode','left')
      ->where($where_tia)
      ->find();//接收信息

    //获取所有附件
    $where_tia2['b.fid']         = $fid;
    $where_tia2['b.fdomain_isout'] = 10;
    $where_tia2['b.fcustomer'] = $system_num;
    $where_tia2['a.fstate'] = 0;

    $do_tiza = M('tbn_illegal_zdad_attach')
      ->alias('a')
      ->field('a.fid,a.fattach,a.fattach_url,a.ftype')
      ->join('tbn_illegal_zdad b on a.fillegal_ad_id=b.fid')
      ->where($where_tia2)
      ->select();

    $do_tia['files']['jbfiles'] = [];
    $do_tia['files']['clfiles'] = [];
    $do_tia['files']['czfiles'] = [];
    foreach ($do_tiza as $key => $value) {
      if($value['ftype'] == 0){
        $do_tia['files']['jbfiles'][] = $value;
      }elseif($value['ftype'] == 10){
        $do_tia['files']['clfiles'][] = $value;
      }elseif($value['ftype'] == 20){
        $do_tia['files']['czfiles'][] = $value;
      }
    }

    if(!empty($do_tia)){
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_tia));
    }else{
      $this->ajaxReturn(array('code'=>1,'msg'=>'获取失败'));
    }
  }

  /**
  * 重点线索交办信息附件信息获取
  * by zw
  */
  public function attach_jbzdanjian(){
    $fid    = I('fid');//线索ID
    $types  = I('types');//附件类型，0交办附件，10上报证据
    $where_tia['b.fid']         = $fid;
    $where_tia['b.fstatus']     = array('gt',0);
    $where_tia['b.fdomain_isout'] = 10;
    $where_tia['a.ftype']       = $types;
    $where_tia['a.fstate']      = 0;
    // $where_tia['_string'] = '((fsend_regid = '.session('regulatorpersonInfo.fregulatorpid').' and locate('.session('regulatorpersonInfo.fregulatorpid').',fparticipate_regids)=0) or (locate('.session('regulatorpersonInfo.fregulatorpid').',fparticipate_regids)>0 and fwaitregid<>0 and fwaitregid<>'.session('regulatorpersonInfo.fregulatorpid').'))';

    $do_tiza = M('tbn_illegal_zdad_attach')->alias('a')->join('tbn_illegal_zdad b on a.fillegal_ad_id=b.fid')->field('a.fid,a.fattach,a.fattach_url')->where($where_tia)->select();

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','filedata'=>$do_tiza));
    
  }


}