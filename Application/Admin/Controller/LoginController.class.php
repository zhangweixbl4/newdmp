<?php
//登录管理控制器
namespace Admin\Controller;
use Think\Controller;
class LoginController extends BaseController {

    public function index(){
		
		//var_dump(C());
		$login = I('get.login');
		if($login == 'out_login'){
			session('personInfo',null);//如果传入退出参数，则退出
			cookie('personInfoCookie',null);
		}	
		if(session('personInfo.fcode') != ''){
			header("Location:".U('Admin/Index/index')); 
			exit;
		}
		if(C('ADMIN_ALLOW_LOGIN_ERROR_COUNT') < 0) $need_verify = 'true';
		$this->assign('need_verify',$need_verify);
		$this->display();
	}
	
	/*退出登录*/
	public function out_login(){
		exit;
		session('personInfo',null);
		session('fregionid',null);
		cookie('personInfoCookie',null);
		$this->ajaxReturn(array('code'=>0,'msg'=>'退出成功','url'=>U('Admin/Login/index')));
	}
	
	
	/*登录处理*/
	public function ajax_login(){

		$allow_login_error_count = 3;//允许管理员登陆错误的次数
		$fcode = I('post.fcode');//获取登录用户名
		$fpassword = I('post.fpassword');//获取登录密码
		$verify = I('post.verify');//获取验证码
		$tel_verify = I('post.tel_verify');//获取验证码
		
		
		$where = array();
		$where['fcode|fmobile|fmail'] = $fcode;//登录用户名查询条件
		//$where['fpassword'] = md5($fpassword);//登录密码查询条件
		
		if (S($fcode.'_login_error_count') > $allow_login_error_count){//判断是否需要验证码
			$need_verify = 'true';
			if($verify == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'登录失败,需要验证码','need_verify'=>$need_verify));//返回ajax
			if(!$this->check_verify($verify)){
				S($fcode.'_login_error_count', intval(S($fcode.'_login_error_count'))+1,3600);//人员登录错误次数+1
				$this->ajaxReturn(array('code'=>-1,'msg'=>'登录失败,验证码错误','need_verify'=>$need_verify));//返回ajax
			} 
		}
		
		
		
		
		

		$personInfo = M('tperson')->field('fid,fcode,fname,fpassword,fmobile,favatar,fregionid,fstate')->cache(true,10)->where($where)->find();//查询人员信息,10秒缓存
		if(!$personInfo){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'不存在该用户'));//返回ajax
		}
		
		$admin_login_check = S('admin_login_check_'.$personInfo['fid']);//获取账号安全检查数据
		
		//header('admin_login_check_'.$personInfo['fid'].':'. json_encode($admin_login_check) . $admin_login_check);
		
		if(!$admin_login_check){//如果没有安全检查数据，则不允许登录
			$this->ajaxReturn(array('code'=>-1,'msg'=>'账号安全检查过期,请刷新页面','need_verify'=>$need_verify));//返回ajax
		}
		
		if($admin_login_check['need_tel_verify'] == 1 && ($admin_login_check['tel_verify'] != $tel_verify || $admin_login_check['tel_verify'] == '')){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'手机验证码错误','need_verify'=>$need_verify));//返回ajax
		}
		
		
		if($personInfo['fpassword'] != md5($fpassword)){
			S($fcode.'_login_error_count', intval(S($fcode.'_login_error_count'))+1,3600);//人员登录错误次数+1
			$this->ajaxReturn(array('code'=>-1,'msg'=>'登录失败,密码错误','need_verify'=>$need_verify));//返回ajax
		}
		
		if($personInfo['fstate'] != 1){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'登录失败,该用户不允许登录','need_verify'=>$need_verify));//返回ajax
		}
		
		if(in_array($fpassword,array('123456'))){
			$this->ajaxReturn(array('code'=>3,'msg'=>'密码过于简单 <a href="'.U('Admin/Login/edit_password').'">点此修改密码</a>'));//返回ajax
		}
		
		
		if($personInfo['fstate'] == 1 && $personInfo['fpassword'] == md5($fpassword)){//判断人员是否正常状态
			
			session('personInfo',$personInfo);//人员信息写入session
			S($fcode.'_login_error_count',null,1);//重置人员登陆错误次数
			$get_client_ip = get_client_ip();

			$dmpLogs2 = A('Common/AliyunLog','Model')->dmpLogs2('admin_login_log',array(
																					'login_ip'=>$get_client_ip,
																					'login_time'=>date('Y-m-d H:i:s'),
																					'person_id'=>$personInfo['fid'],
																					
																					'fname'=>$personInfo['fname'],
																					'city'=>M('ip')->cache(true,600)->where(array('ip'=>$get_client_ip))->getField('city'),
																	
																	));	
			
			$personInfoCookie = array('fid'=>sys_auth($personInfo['fid']),'fpassword'=>sys_auth($personInfo['fpassword']));
			
			cookie('personInfoCookie',$personInfoCookie,array('expire'=>3600*24*7));//保存用户cookie
			cookie('personInfoCheck_username',sys_auth($personInfo['fid']),array('expire'=>3600*24*7));//保存用户cookie
			$this->ajaxReturn(array('code'=>0,'msg'=>'','url'=>U('Admin/Index/index',array('index_page'=>cookie('admin_index_page')))));//返回ajax
		}
		
		$this->ajaxReturn(array('code'=>-1,'msg'=>'登录失败,未知错误','need_verify'=>$need_verify));//返回ajax
		
	}
	
	/*修改密码*/
	public function ajax_edit_password(){
		$fcode = I('fcode');
        $password = I('password');
		$new_password = I('new_password');
		$new_password_2 = I('new_password_2');
        $verify = I('verify');
		
		if($fcode == ''){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'请输入用户名或手机号'));//返回ajax
		}
		
		if($new_password != $new_password_2 || $new_password == ''){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'新密码与确认密码不相同'));//返回ajax
		}
		
		if(!$this->check_verify($verify)){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'验证码输入错误'));//返回ajax
		} 
		$where = array();
		$where['fcode|fmobile|fmail'] = $fcode;
		$where['fpassword'] = md5($password);
		$personInfo = M('tperson')->field('fid,fcode,fpassword')->cache(true,10)->where($where)->find();//查询人员信息,10秒缓存
		//var_dump($personInfo);
		//var_dump(M('tperson')->getLastSql());
		if(!$personInfo){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'用户名或原密码错误'));//返回ajax
		}
		
		$save_state = M('tperson')->where(array('fid'=>$personInfo['fid']))->save(array('fpassword'=>md5($new_password)));
		if($save_state > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功 <a href="'.U('Admin/Login/index').'">点此重新登录</a>'));//返回ajax
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));//返回ajax
		}
		
		
	}
	
	/*登录验证码*/
	public function verify(){

		$Verify = new \Think\Verify(array(
											
											'useCurve'	=>	false,
											'useNoise'	=>	false,
											'length'	=>	4,
									));
		$Verify->entry();
	}
	
	
	
	/*验证码验证*/
	function check_verify($code, $id = ''){
		$verify = new \Think\Verify();
		return $verify->check($code, $id);
	}
	
	
	
	/*账号安全检查*/
	public function check_username(){
		$get_client_ip = get_client_ip();
		$fcode = I('fcode');
		if($fcode == ''){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'用户名不能为空'));//返回ajax
		}
		cookie('admin_login_username_cookie',$fcode,array('expire'=>3600*24*7));//保存用户cookie
		$personInfo = M('tperson')->field('fid,fcode,fname,fmobile,fstate')->cache(true,10)->where(array('fcode|fmobile|fmail'=>$fcode))->find();//查询人员信息,10秒缓存
		
		if(sys_auth(cookie('personInfoCheck_username'),'DECODE') == $personInfo['fid']){//判断用户传入的id是否正确
			S('admin_login_check_'.$personInfo['fid'],array('need_tel_verify'=>0),120);
			$this->ajaxReturn(array('code'=>0,'msg'=>'','tel'=>'','need_tel_verify'=>0));//返回ajax
		}
		

		// var_dump($get_client_ip);
		if(
			strlen($personInfo['fmobile']) == 11 
			&& substr($personInfo['fmobile'],0,1) == '1' 
			&& $get_client_ip <> '127.0.0.1' 
			&& $get_client_ip <> '115.236.42.130' 
			&& $get_client_ip <> '0.0.0.0'
			&& $personInfo['fmobile'] <> '13588258695'
		){
			S('admin_login_check_'.$personInfo['fid'],array('need_tel_verify'=>1),120);

			$mobile = substr($personInfo['fmobile'],0,3).'****'.substr($personInfo['fmobile'],7,4);

			$this->ajaxReturn(array('code'=>0,'msg'=>'','tel'=>$mobile,'need_tel_verify'=>1,));//返回ajax
		
		}else{
			S('admin_login_check_'.$personInfo['fid'],array('need_tel_verify'=>0),120);
			$this->ajaxReturn(array('code'=>0,'msg'=>'','tel'=>'','need_tel_verify'=>0));//返回ajax
		}
	
	}
	
	/*发送手机验证码*/
	public function send_tel_verify(){
		
		$fcode = I('fcode');
		if($fcode == ''){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'用户名不能为空'));//返回ajax
		}
		
		$personInfo = M('tperson')->field('fid,fcode,fname,fmobile,fstate')->cache(true,10)->where(array('fcode|fmobile|fmail'=>$fcode))->find();//查询人员信息,10秒缓存
		
		if(strlen($personInfo['fmobile']) != 11 || substr($personInfo['fmobile'],0,1) != '1'){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该账号没有绑定正确的手机号'));//返回ajax
		}
		
	
		$tel_verify = rand(100000,999999);
		$send_state = A('Common/Alitongxin','Model')->identifying_sms($personInfo['fmobile'],$tel_verify,'DMP登录');
		if($send_state['code'] === 0){
			S('admin_login_check_'.$personInfo['fid'],array('need_tel_verify'=>1,'tel_verify'=>$tel_verify),120);//存储验证码
			$this->ajaxReturn(array('code'=>0,'msg'=>$send_state['msg']));//返回ajax
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>$send_state['msg']));//返回ajax
		}

		
	}

	

}