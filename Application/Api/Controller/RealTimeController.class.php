<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;

#http://dmptest/Api/RealTime/all



class RealTimeController extends Controller {
	
	public $dmpDb = 'mysql://read_only:bFtL3z6j5U4ojonmvLeon60G46lj9NOp@pc-bp189m868q2750ekx.mysql.polardb.rds.aliyuncs.com:3306/hzsj_dmp';
	public $wwqSqlDb = 'mysql://root:root123456ABC!@#@118.31.14.6:3306/broadcast';
	
	
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}
	
	public function all(){
		$this->mt();
		$this->td();
		$this->rw();
		$this->kj();
		$this->lately_issue();
		$this->lately_score();
		$this->bz();
		$this->lately_sam();
		$this->cjyc();
		$this->cjyc2();
		$this->ctrw();
		$this->jf();
		
		
		
		
	}

	
	
	/*计算当前采集媒体数量*/
	public function mt(){
		
		$sql = "
					select left(fmediaclassid,2) as media_class,count(*) as count,count(case when fchannelid > 0 then 1 else null end) as count2
					from tmedia
					where fstate = 1 and priority >= 0 and left(fmediaclassid,2) in ('01','02','03','13')
					GROUP BY left(fmediaclassid,2)
						";
		$data1List = M('','',$this->dmpDb)->query($sql);
		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'媒体','fitem'=>'媒介类型','fdata1'=>'应采数量','fdata2'=>'实采数量'),array(),true);
		foreach($data1List as $data1){
			
			if($data1['media_class'] == '01'){
				$save = array('fcustomer'=>'华治','fname'=>'媒体','fitem'=>'电视','fdata1'=>$data1['count'],'fdata2'=>$data1['count2']);
				
			}elseif($data1['media_class'] == '02'){
				$save = array('fcustomer'=>'华治','fname'=>'媒体','fitem'=>'广播','fdata1'=>$data1['count'],'fdata2'=>$data1['count2']);
			}elseif($data1['media_class'] == '03'){
				$save = array('fcustomer'=>'华治','fname'=>'媒体','fitem'=>'报纸','fdata1'=>$data1['count'],'fdata2'=>$data1['count2']);
			}elseif($data1['media_class'] == '13'){
				$save = array('fcustomer'=>'华治','fname'=>'媒体','fitem'=>'互联网','fdata1'=>$data1['count'],'fdata2'=>$data1['count2']);
			}
			
			$rr = M('treal_time_info','',C('ZIZHI_DB'))->add($save,array(),true);
			
			
		}
		
		echo 'success	'.date('Y-m-d H:i:s') . "\n";
	
	}
	
	/*计算当前采集的通道数量*/
	public function td(){
		$sql = "
					select left(fmediaclassid,2) as media_class,count(*) as count,count(case when now_collect_state < 1 then 1 else null end) as count2
					from tmedia
					where fstate = 1 and priority >= -1 and left(fmediaclassid,2) in ('01','02','03')
					GROUP BY left(fmediaclassid,2)
						";
		$data1List = M('','',$this->dmpDb)->query($sql);
		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'采集','fitem'=>'媒介类型','fdata1'=>'当前采集量（含备用）','fdata2'=>'采集异常数量','fdata5'=>'异常率'),array(),true);
		foreach($data1List as $data1){
			
			if($data1['media_class'] == '01'){
				$save = array('fcustomer'=>'华治','fname'=>'采集','fitem'=>'电视','fdata1'=>$data1['count'],'fdata2'=>$data1['count2'],'fdata5'=>$data1['count2']/$data1['count']);
				
			}elseif($data1['media_class'] == '02'){
				$save = array('fcustomer'=>'华治','fname'=>'采集','fitem'=>'广播','fdata1'=>$data1['count'],'fdata2'=>$data1['count2'],'fdata5'=>$data1['count2']/$data1['count']);
			}elseif($data1['media_class'] == '03'){
				$save = array('fcustomer'=>'华治','fname'=>'采集','fitem'=>'报纸','fdata1'=>$data1['count'],'fdata2'=>$data1['count2'],'fdata5'=>$data1['count2']/$data1['count']);
			}
			
			$rr = M('treal_time_info','',C('ZIZHI_DB'))->add($save,array(),true);
			
			
		}
		
		echo 'success	'.date('Y-m-d H:i:s'). "\n";
	
	}
	
	
	/*计算当前任务状态*/
	public function rw(){
		$sql = "
					SELECT
					  COUNT( CASE WHEN b.media_class = 1 AND b.type = 1 AND b.state = 0 THEN 1 ELSE NULL END ) a0,
					  COUNT( CASE WHEN b.media_class = 1 AND b.type = 2 AND b.state = 0 THEN 1 ELSE NULL END ) a1,
					  COUNT( CASE WHEN b.media_class = 2 AND b.type = 1 AND b.state = 0 THEN 1 ELSE NULL END ) a2,
					  COUNT( CASE WHEN b.media_class = 2 AND b.type = 2 AND b.state = 0 THEN 1 ELSE NULL END ) a3,
					  COUNT( CASE WHEN b.media_class = 3 AND b.type = 1 AND b.state = 0 THEN 1 ELSE NULL END ) a4,
					  COUNT( CASE WHEN b.media_class = 3 AND b.type = 2 AND b.state = 0 THEN 1 ELSE NULL END ) a5,
					  (SELECT COUNT(1) FROM tnettask WHERE task_state=0 AND priority>0) a6,
					  (SELECT COUNT(1) FROM tnettask WHERE task_state IN (0,12) AND priority>0) a7
					FROM task_input a
					JOIN task_input_link b ON b.taskid = a.taskid 
					WHERE a.task_state = 0;

						";
		$data1List = M('','',$this->dmpDb)->query($sql);
		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'众包任务量','fitem'=>'媒介类型','fdata1'=>'待完成录入任务','fdata2'=>'待完成初审任务'),array(),true);
		if($data1List){
			$save = array('fcustomer'=>'华治','fname'=>'众包任务量','fitem'=>'电视','fdata1'=>$data1List[0]['a0'],'fdata2'=>$data1List[0]['a1']);
			$rr = M('treal_time_info','',C('ZIZHI_DB'))->add($save,array(),true);

			$save = array('fcustomer'=>'华治','fname'=>'众包任务量','fitem'=>'广播','fdata1'=>$data1List[0]['a2'],'fdata2'=>$data1List[0]['a3']);
			$rr = M('treal_time_info','',C('ZIZHI_DB'))->add($save,array(),true);

			$save = array('fcustomer'=>'华治','fname'=>'众包任务量','fitem'=>'报纸','fdata1'=>$data1List[0]['a4'],'fdata2'=>$data1List[0]['a5']);
			$rr = M('treal_time_info','',C('ZIZHI_DB'))->add($save,array(),true);

			$save = array('fcustomer'=>'华治','fname'=>'众包任务量','fitem'=>'互联网','fdata1'=>$data1List[0]['a6'],'fdata2'=>$data1List[0]['a7']);
			$rr = M('treal_time_info','',C('ZIZHI_DB'))->add($save,array(),true);
		}
		
		
		
		
		echo 'success	'.date('Y-m-d H:i:s'). "\n";
		
	}
	
	/*计算快剪任务状态*/
	public function kj(){
		
		$sql = "
				select  left(tmedia.fmediaclassid,2) as media_class,fmediaid,fissuedate
					from cut_task 
					join tmedia on tmedia.fid = cut_task.fmediaid
					where cut_task.fstate in(0,1,2,3) and left(tmedia.fmediaclassid,2) in('01','02')


						";
		$data1List = M('','',$this->dmpDb)->query($sql);
		
		
		$rw_tv_num = 0;
		$rw_bc_num = 0;
		$yw_tv_num = 0;
		$yw_bc_num = 0;
		
		
		foreach($data1List as $data1){
			
			$auto_ext_day_ago = S('auto_ext_day_ago_'.$data1['fmediaid']);
			
			if(!$auto_ext_day_ago && $auto_ext_day_ago !== '0'){

				$auto_ext_day_ago = M('tmedia','',$this->dmpDb)->cache(true,60)->where(array('fid'=>$data1['fmediaid']))->getField('auto_ext_day_ago');
				S('auto_ext_day_ago_'.$data1['fmediaid'],$auto_ext_day_ago,3600);

			}

			
			if($data1['media_class'] == '01'){
				
				if((time()-strtotime($data1['fissuedate']))/86400 > $auto_ext_day_ago && $auto_ext_day_ago > 0){
					$yw_tv_num += 1;
					
				}
				$rw_tv_num += 1;
			}elseif($data1['media_class'] == '02'){
				if((time()-strtotime($data1['fissuedate']))/86400 > $auto_ext_day_ago && $auto_ext_day_ago > 0){
					$yw_bc_num += 1;
				}
				$rw_bc_num += 1;
			}
			
			
			
			
			
		}
		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'快剪任务量','fitem'=>'媒介类型','fdata1'=>'待处理任务','fdata2'=>'延误任务'),array(),true);
		$save = array('fcustomer'=>'华治','fname'=>'快剪任务量','fitem'=>'电视','fdata1'=>$rw_tv_num,'fdata2'=>$yw_tv_num);
		$rr = M('treal_time_info','',C('ZIZHI_DB'))->add($save,array(),true);
		
		$save = array('fcustomer'=>'华治','fname'=>'快剪任务量','fitem'=>'广播','fdata1'=>$rw_bc_num,'fdata2'=>$yw_bc_num);
		$rr = M('treal_time_info','',C('ZIZHI_DB'))->add($save,array(),true);
			
			

		
		echo 'success	'.date('Y-m-d H:i:s'). "\n";
		
	}
	
	
	/*计算近7日广告发布情况*/
	public function lately_issue(){
		
		$date = I('date',date('Y-m-d'));
		$issueCount = array();
		for($i=0;$i<14;$i+=1){
			$jdate = date('Y-m-d',(strtotime($date) - $i*86400));
			$issueCount[$jdate]['01'] = 0;
			$issueCount[$jdate]['02'] = 0;
			$issueCount[$jdate]['03'] = 0;
			$issueCount[$jdate]['13'] = 0;
			$issueCount[$jdate]['fsort'] = 0-$i;
			
			
			$ridList = M('tregion','',$this->dmpDb)->where(array('fpid'=>100000))->getField('fid',true);
			foreach($ridList as $rid){
				$issueTvTable = 'ttvissue_'.date('Ym',strtotime($jdate)).'_'.substr($rid,0,2);
				$issueBcTable = 'tbcissue_'.date('Ym',strtotime($jdate)).'_'.substr($rid,0,2);
				
				#var_dump($issueTvTable,$issueBcTable);
				try{
					$issueCount[$jdate]['01'] += M($issueTvTable,'',$this->dmpDb)->where(array('fissuedate'=>strtotime($jdate)))->count();
				}catch( \Exception $error2) {  }
				
				try{
					$issueCount[$jdate]['02'] += M($issueBcTable,'',$this->dmpDb)->where(array('fissuedate'=>strtotime($jdate)))->count();
				}catch( \Exception $error2) {  }
	
			}
			
			$issueCount[$jdate]['03'] = M('tpaperissue','',$this->dmpDb)->where(array('fissuedate'=>$jdate))->count();
			$issueCount[$jdate]['13'] = M('tnetissueputlog','',$this->dmpDb)->where(array('fissuedate'=>strtotime($jdate)))->count();
			
			
			
			
		}
		
		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'最近电视发布','fitem'=>'日期','fdata1'=>'发布量'),array(),true);
		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'最近广播发布','fitem'=>'日期','fdata1'=>'发布量'),array(),true);
		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'最近报纸发布','fitem'=>'日期','fdata1'=>'发布量'),array(),true);
		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'最近网络发布','fitem'=>'日期','fdata1'=>'发布量'),array(),true);
			
			
		$addDb = [];
		foreach($issueCount as $jd => $iss){
			$addDb[] = array('fsort'=>$iss['fsort'],'fcustomer'=>'华治','fname'=>'最近电视发布','fitem'=>date('m.d',strtotime($jd)),'fdata1'=>$iss['01']);
			$addDb[] = array('fsort'=>$iss['fsort'],'fcustomer'=>'华治','fname'=>'最近广播发布','fitem'=>date('m.d',strtotime($jd)),'fdata1'=>$iss['02']);
			$addDb[] = array('fsort'=>$iss['fsort'],'fcustomer'=>'华治','fname'=>'最近报纸发布','fitem'=>date('m.d',strtotime($jd)),'fdata1'=>$iss['03']);
			$addDb[] = array('fsort'=>$iss['fsort'],'fcustomer'=>'华治','fname'=>'最近网络发布','fitem'=>date('m.d',strtotime($jd)),'fdata1'=>$iss['13']);
			
		}
		M('treal_time_info','',C('ZIZHI_DB'))->where(array('fcustomer'=>'华治','fname'=>'最近电视发布'))->delete();
		M('treal_time_info','',C('ZIZHI_DB'))->where(array('fcustomer'=>'华治','fname'=>'最近广播发布'))->delete();
		M('treal_time_info','',C('ZIZHI_DB'))->where(array('fcustomer'=>'华治','fname'=>'最近报纸发布'))->delete();
		M('treal_time_info','',C('ZIZHI_DB'))->where(array('fcustomer'=>'华治','fname'=>'最近网络发布'))->delete();
		
		
		
		$rr = M('treal_time_info','',C('ZIZHI_DB'))->addAll($addDb,array(),true);
		echo 'success	'.date('Y-m-d H:i:s'). "\n";

	}
	
	
	/*最近积分情况*/
	public function lately_score(){
		$date = I('date',date('Y-m-d'));
		M('treal_time_info_def','',C('ZIZHI_DB'))->add(array('fcustomer'=>'华治','fname'=>'最近积分','fitem'=>'日期','fdata1'=>'积分量','fdata2'=>'平均分','fdata3'=>'最高分'),array(),true);
		$scoreCount = [];
		for($i=0;$i<14;$i+=1){
			$jdate = date('Y-m-d',(strtotime($date) - $i*86400));
			$fdata = M('task_input_score','',$this->dmpDb)->field('sum(score) as fdata1,sum(score) / count(DISTINCT wx_id) as fdata2')->where(array('score_date'=>$jdate))->find();
			$fdata3 = M('task_input_score','',$this->dmpDb)->where(array('score_date'=>$jdate))->group('wx_id')->order('fdata3 desc')->getField('sum(score) as fdata3');
			
			#var_dump($fdata3);
			
			$scoreCount[] = array('fsort'=>0-$i,'fcustomer'=>'华治','fname'=>'最近积分','fitem'=>date('m.d',strtotime($jdate)),'fdata1'=>$fdata['fdata1'],'fdata2'=>$fdata['fdata2'],'fdata3'=>$fdata3);
			
			
		}
		M('treal_time_info','',C('ZIZHI_DB'))->where(array('fcustomer'=>'华治','fname'=>'最近积分'))->delete();
		
		
		
		$rr = M('treal_time_info','',C('ZIZHI_DB'))->addAll($scoreCount,array(),true);
		echo 'success	'.date('Y-m-d H:i:s'). "\n";
		
	}
	
	
	/*报纸上传情况*/
	public function bz(){
		$date = I('date',date('Y-m-d'));
		M('treal_time_info_def','',C('ZIZHI_DB'))->add(array('fcustomer'=>'华治','fname'=>'报纸上传情况','fitem'=>'日期','fdata1'=>'版面数量','fdata2'=>'媒体数量'),array(),true);
		
		for($i=0;$i<14;$i+=1){
			$jdate = date('Y-m-d',(strtotime($date) - $i*86400));
			$fdata = M('tpapersource','',$this->dmpDb)->field('count(*) as fdata1,count(DISTINCT fmediaid) as fdata2')->where(array('fissuedate'=>$jdate))->find();
			
			$pageCount[] = array('fsort'=>0-$i,'fcustomer'=>'华治','fname'=>'报纸上传情况','fitem'=>date('m.d',strtotime($jdate)),'fdata1'=>$fdata['fdata1'],'fdata2'=>$fdata['fdata2']);
			
			
		}
		
		M('treal_time_info','',C('ZIZHI_DB'))->where(array('fcustomer'=>'华治','fname'=>'报纸上传情况'))->delete();
		
		$rr = M('treal_time_info','',C('ZIZHI_DB'))->addAll($pageCount,array(),true);
		echo 'success	'.date('Y-m-d H:i:s'). "\n";
		
		
	}
	
	
	/*计算近7日广告发布情况*/
	public function lately_sam(){
		
		$date = I('date',date('Y-m-d'));
		$issueCount = array();
		for($i=0;$i<14;$i+=1){
			$jdate = date('Y-m-d',(strtotime($date) - $i*86400));
			$issueCount[$jdate]['01'] = 0;
			$issueCount[$jdate]['02'] = 0;
			$issueCount[$jdate]['fsort'] = 0-$i;

			$issueCount[$jdate]['01'] = M('ttvsample','',$this->dmpDb)->where(array('fissuedate'=>$jdate))->count();
			$issueCount[$jdate]['02'] = M('tbcsample','',$this->dmpDb)->where(array('fissuedate'=>$jdate))->count();
			
			
			
			
		}
		
		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'最近电视新增样本','fitem'=>'日期','fdata1'=>'样本量'),array(),true);
		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'最近广播新增样本','fitem'=>'日期','fdata1'=>'样本量'),array(),true);
		
		$addDb = [];
		foreach($issueCount as $jd => $iss){
			$addDb[] = array('fsort'=>$iss['fsort'],'fcustomer'=>'华治','fname'=>'最近电视新增样本','fitem'=>date('m.d',strtotime($jd)),'fdata1'=>$iss['01']);
			$addDb[] = array('fsort'=>$iss['fsort'],'fcustomer'=>'华治','fname'=>'最近广播新增样本','fitem'=>date('m.d',strtotime($jd)),'fdata1'=>$iss['02']);

		}
		M('treal_time_info','',C('ZIZHI_DB'))->where(array('fcustomer'=>'华治','fname'=>'最近电视新增样本'))->delete();
		M('treal_time_info','',C('ZIZHI_DB'))->where(array('fcustomer'=>'华治','fname'=>'最近广播新增样本'))->delete();

		
		
		
		$rr = M('treal_time_info','',C('ZIZHI_DB'))->addAll($addDb,array(),true);
		echo 'success	'.date('Y-m-d H:i:s'). "\n";

	}
	
	
	/*统计最近采集异常情况*/
	public function cjyc(){
		
		
		
		$sql = "select a.error_type,b.typename,a.cnt,a.channelcnt,a.now_cnt
				from (
				select error_type,count(*) cnt,count(distinct channel) channelcnt,count(DISTINCT case when `status` in (1,2) then channel else null end ) now_cnt
				from labor_abnormal
				where create_time between date_add(sysdate(), interval -30 day) and sysdate()
				#where status in (1,2)
				group by error_type) a,
				(
							select 1 typeid,'欠费停机、未授权、信号异常' typename
				union select 2,'黑屏,画面异常'
				union select 3,'无声音'
				union select 4,'音画不同步'
				union select 5,'同一频道多次串台'
				union select 6,'其他'
				union select 7,'断流'
				union select 9,'噪音'
				union select 10,'流缺失'
				union select 21,'m3u8地址下载404'
				union select 22,'ffmpeg 无法转换'
				union select 23,'该频道没有下载地址'
				union select 24,'m3u8时间过早'
				union select 25,'m3u8时间过晚'
				union select 26,'ts文件名解析失败'
				union select 27,'直播流异常'
				) b
				where a.error_type = b.typeid
				order by a.error_type;";
		$dataList = M('','',$this->wwqSqlDb)->query($sql);
		
		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'采集异常情况','fitem'=>'异常类型','fdata1'=>'近30天异常媒体次','fdata2'=>'近30天异常媒体数','fdata3'=>'待处理异常媒体数'),array(),true);
		$addDb = [];
		foreach($dataList as $data){
			$addDb[] = array('fcustomer'=>'华治','fname'=>'采集异常情况','fitem'=>$data['typename'],'fdata1'=>$data['cnt'],'fdata2'=>$data['channelcnt'],'fdata3'=>$data['now_cnt']);
		}
		$rr = M('treal_time_info','',C('ZIZHI_DB'))->addAll($addDb,array(),true);
		echo 'success	'.date('Y-m-d H:i:s'). "\n";
	
	}
	

	public function cjyc2(){
		$date = I('date',date('Y-m-d'));
		M('treal_time_info_def','',C('ZIZHI_DB'))->add(array('fcustomer'=>'华治','fname'=>'采集异常趋势','fitem'=>'日期','fdata1'=>'当日新增异常数','fdata2'=>'当日处理异常数' ),array(),true);
		$cjyc2DataList = [];
		for($i=0;$i<14;$i+=1){
			$jdate = date('Y-m-d',(strtotime($date) - $i*86400));
			$sql = "select a.dt,data1,data2
					from (
					select date(create_time) dt ,count(1) data1 #每日新增量
					from labor_abnormal
					where create_time between  '".$jdate." 00:00:00' and '".$jdate." 23:59:59'
					group by date(create_time)
					) a,
					(
					select date(update_time) dt ,count(1) data2 #每日处理量
					from labor_abnormal
					where update_time between  '".$jdate." 00:00:00' and '".$jdate." 23:59:59'
					and status = 3
					group by date(update_time)
					) b
					where a.dt = b.dt;";

			$data0 = M('','',$this->wwqSqlDb)->query($sql);
			
			$cjyc2DataList[] = array('fsort'=>0-$i,'fcustomer'=>'华治','fname'=>'采集异常趋势','fitem'=>date('m.d',strtotime($jdate)),'fdata1'=>$data0[0]['data1'],'fdata2'=>$data0[0]['data2']);
			
			
		}
		M('treal_time_info','',C('ZIZHI_DB'))->where(array('fcustomer'=>'华治','fname'=>'采集异常趋势'))->delete();
		
		
		
		$rr = M('treal_time_info','',C('ZIZHI_DB'))->addAll($cjyc2DataList,array(),true);
		echo 'success	'.date('Y-m-d H:i:s'). "\n";
		
	}
	
	
	public function ctrw(){
		
		$date = I('date',date('Y-m-d'));
		$taskCount = array();
		for($i=0;$i<14;$i+=1){
			$jdate = date('Y-m-d',(strtotime($date) - $i*86400));

			$taskCount[$jdate]['fsort'] = 0-$i;

			$taskCount[$jdate]['001'] = M('','',$this->dmpDb)->query("SELECT COUNT(*) count FROM task_input WHERE fcreatetime BETWEEN '".$jdate." 00:00:00' AND '".$jdate." 23:59:59'")[0]['count']; #-- 传统新增任务量
			$taskCount[$jdate]['002'] = M('','',$this->dmpDb)->query("SELECT COUNT(*) count FROM task_input_link WHERE media_class<>13 AND type=1 AND state=2 AND wx_id=0 AND update_time BETWEEN ".strtotime($jdate)." AND ".(strtotime($jdate)+86399))[0]['count']; #-- 传统任务自动完成量
			$taskCount[$jdate]['003'] = M('','',$this->dmpDb)->query("SELECT COUNT(DISTINCT taskid) count FROM task_input_score WHERE media_class IN (1,2,3) AND type=1 AND score>=0 AND update_time BETWEEN ".strtotime($jdate)." AND ".(strtotime($jdate)+86399))[0]['count']; #-- 传统任务人工完成量
			$taskCount[$jdate]['004'] = M('','',$this->dmpDb)->query("SELECT COUNT(*) count FROM task_input_link a INNER JOIN task_input b ON b.taskid=a.taskid WHERE a.media_class<>13 AND a.type=2 AND a.state=2 AND a.wx_id=0 AND a.update_time BETWEEN ".strtotime($jdate)." AND ".(strtotime($jdate)+86399))[0]['count']; #-- 传统任务审核自动完成量
			$taskCount[$jdate]['005'] = M('','',$this->dmpDb)->query("SELECT COUNT(DISTINCT taskid) count FROM task_input_score WHERE media_class IN (1,2,3) AND type=2 AND score>=0 AND update_time BETWEEN ".strtotime($jdate)." AND ".(strtotime($jdate)+86399))[0]['count']; #-- 传统任务审核人工完成量
			
			
			
			
		}

		
		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'最近新增传统任务','fitem'=>'日期','fdata1'=>'新增量'),array(),true);
		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'最近自动录入任务','fitem'=>'日期','fdata1'=>'完成量'),array(),true);
		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'最近人工录入任务','fitem'=>'日期','fdata1'=>'完成量'),array(),true);
		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'最近自动审核任务','fitem'=>'日期','fdata1'=>'完成量'),array(),true);
		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'最近人工审核任务','fitem'=>'日期','fdata1'=>'完成量'),array(),true);
			
			
		
		$addDb = [];
		foreach($taskCount as $jd => $iss){
			$addDb[] = array('fsort'=>$iss['fsort'],'fcustomer'=>'华治','fname'=>'最近新增传统任务','fitem'=>date('m.d',strtotime($jd)),'fdata1'=>$iss['001']);
			$addDb[] = array('fsort'=>$iss['fsort'],'fcustomer'=>'华治','fname'=>'最近自动录入任务','fitem'=>date('m.d',strtotime($jd)),'fdata1'=>$iss['002']);
			$addDb[] = array('fsort'=>$iss['fsort'],'fcustomer'=>'华治','fname'=>'最近人工录入任务','fitem'=>date('m.d',strtotime($jd)),'fdata1'=>$iss['003']);
			$addDb[] = array('fsort'=>$iss['fsort'],'fcustomer'=>'华治','fname'=>'最近自动审核任务','fitem'=>date('m.d',strtotime($jd)),'fdata1'=>$iss['004']);
			$addDb[] = array('fsort'=>$iss['fsort'],'fcustomer'=>'华治','fname'=>'最近人工审核任务','fitem'=>date('m.d',strtotime($jd)),'fdata1'=>$iss['005']);
			
		}
		M('treal_time_info','',C('ZIZHI_DB'))->where(array('fcustomer'=>'华治','fname'=>'最近新增传统任务'))->delete();
		M('treal_time_info','',C('ZIZHI_DB'))->where(array('fcustomer'=>'华治','fname'=>'最近自动录入任务'))->delete();
		M('treal_time_info','',C('ZIZHI_DB'))->where(array('fcustomer'=>'华治','fname'=>'最近人工录入任务'))->delete();
		M('treal_time_info','',C('ZIZHI_DB'))->where(array('fcustomer'=>'华治','fname'=>'最近自动审核任务'))->delete();
		M('treal_time_info','',C('ZIZHI_DB'))->where(array('fcustomer'=>'华治','fname'=>'最近人工审核任务'))->delete();
		
		
		
		
		$rr = M('treal_time_info','',C('ZIZHI_DB'))->addAll($addDb,array(),true);
		echo 'success	'.date('Y-m-d H:i:s'). "\n";

		
		
	}
	
	/*客户交付数据*/
	public function jf(){
		$date = I('date',date('Y-m-d'));
		$jfCount = array();
		for($i=0;$i<14;$i+=1){
			$jdate = date('Y-m-d',(strtotime($date) - $i*86400));

			

			$jfCount[$jdate] = M('','',C('ZIZHI_DB'))->query("
																select 
																	count(*) as count1 
																	,count(case when fstatus = 2 then 1 else null end) as count2
																	,count(case when fplan_delivery_date < now() and fstatus != 2 then 1 else null end ) as count3
																from tcustomer_inspect_plan
																where fissue_date = '".$jdate."' and fstatus >= 0")[0]; #
			$jfCount[$jdate]['fsort'] = 0-$i;

			
		}


		M('treal_time_info_def','',C('ZIZHI_DB'))
			->add(array('fcustomer'=>'华治','fname'=>'交付情况汇总','fitem'=>'日期','fdata1'=>'当日需交付','fdata2'=>'实际已交付','fdata3'=>'逾期未交付'),array(),true);

		$addDb = [];
		foreach($jfCount as $jd => $jf){
			
			$addDb[] = array('fsort'=>$jf['fsort'],'fcustomer'=>'华治','fname'=>'交付情况汇总','fitem'=>date('m.d',strtotime($jd)),'fdata1'=>$jf['count1'],'fdata2'=>$jf['count2'],'fdata3'=>$jf['count3']);
		
		}
		M('treal_time_info','',C('ZIZHI_DB'))->where(array('fcustomer'=>'华治','fname'=>'交付情况汇总'))->delete();

		
		
		
		
		$rr = M('treal_time_info','',C('ZIZHI_DB'))->addAll($addDb,array(),true);
		echo 'success	'.date('Y-m-d H:i:s'). "\n";
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
}