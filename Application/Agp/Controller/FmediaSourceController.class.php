<?php
namespace Agp\Controller;
use Think\Controller;


/**
 * 媒体素材文件上传
 */

class FmediaSourceController extends BaseController
{
	//获取数据
	public function index(){
		session_write_close();
		$system_num = getconfig('system_num');
		$outtype = I('outtype');//导出类型
		$fmediaid = I('fmediaid');//媒体ID
		$fissuedate = I('fissuedate');//素材日期
		$fnumber = I('fnumber');//批次号
		
		if(empty($fmediaid) || empty($fissuedate)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
		}

		$whereSource['a.fmediaid'] = $fmediaid;
		$whereSource['a.fissuedate'] = $fissuedate;
		$whereSource['a.fstatus'] = 1;
		if(!empty($fnumber)){
			$whereSource['a.fnumber'] = $fnumber;
		}
		if(!empty(session('regulatorpersonInfo'))){
			$whereSource['_string'] = 'a.fmediaid in(select fmediaid from tmedia_temp where ftype = 1 and fstate = 1 and fcustomer = "'.$system_num.'" and fuserid = '.session('regulatorpersonInfo.fid').' and fmediaid = '.$fmediaid.')';
		}

		$countSource = M('tmediasource')
			->alias('a')
	    	->join('tmedia b on a.fmediaid = b.fid')
			->where($whereSource)
			->count();//查询满足要求的总记录数

	    $dataSource = M('tmediasource')
	    	->alias('a')
	    	->field('a.fid,b.fmedianame,a.fmediaid,a.fissuedate,a.fnumber,a.ffilename,a.ffileurl')
	    	->join('tmedia b on a.fmediaid = b.fid')
	    	->where($whereSource)
	    	->order('a.fcreatetime desc')
	    	->select();

	    if(empty($outtype)){
		    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$countSource,'list'=>$dataSource)));//返回ajax
	    }else{
	    	if(empty($dataSource)){
	          $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
	        }

	        $outdata['datalie'] = [
	          '序号'=>'key',
	          '媒体名称'=>'fmedianame',
	          '素材日期'=>'fissuedate',
	          '批次'=>'fnumber',
	          '文件名'=>'ffilename',
	          '文件地址'=>'ffileurl',
	        ];
	        $outdata['lists'] = $dataSource;
	        $ret = A('Api/Function')->outdata_xls($outdata);

	        D('Function')->write_log('媒体素材文件',1,'导出成功');
	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
	    }
	}

	/*
	* 上传素材
	* by zw
	*/
	public function addSource(){
		$system_num = getconfig('system_num');
		$fmediaid = I('fmediaid');//媒体ID
		$fissuedate = I('fissuedate');//素材日期
		$fnumber = I('fnumber');//批次号
		$ffilename = I('ffilename');//文件名
		$ffileurl = I('ffileurl');//文件地址
		$ffiledata = I('ffiledata');//文件数据

		if(empty($fmediaid) || empty($fissuedate) || empty($fnumber)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
		}
		if((empty($ffilename) || empty($ffileurl)) && empty($ffiledata)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'请上传文件'));
		}

		if(!empty($ffiledata)){
			foreach ($ffiledata as $key => $value) {
				$dataSource = [];
				$dataSource['fmediaid'] = $fmediaid;
				$dataSource['fissuedate'] = $fissuedate;
				$dataSource['fnumber'] = $fnumber;
				$dataSource['ffilename'] = $value['ffilename'];
				$dataSource['ffileurl'] = $value['ffileurl'];
				$dataSource['fcreatetime'] = date('Y-m-d H:i:s');
				$dataSource['fcreater'] = session('regulatorpersonInfo.fname');
				$dataSource['fstatus'] = 1;
				$dataSource['fcustomer'] = $system_num;
				$addData[] = $dataSource;
			}
			if(!empty($addData)){
				$addSource = M('tmediasource')->addAll($addData);
			}
		}else{
			$dataSource['fmediaid'] = $fmediaid;
			$dataSource['fissuedate'] = $fissuedate;
			$dataSource['fnumber'] = $fnumber;
			$dataSource['ffilename'] = $ffilename;
			$dataSource['ffileurl'] = $ffileurl;
			$dataSource['fcreatetime'] = date('Y-m-d H:i:s');
			$dataSource['fcreater'] = session('regulatorpersonInfo.fname');
			$dataSource['fstatus'] = 1;
			$dataSource['fcustomer'] = $system_num;
			$addSource = M('tmediasource')->add($dataSource);
		}
		
		if(!empty($addSource)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'添加失败'));
		}
	}

	/*
	* 删除素材
	* by zw
	*/
	public function delSource(){
		$system_num = getconfig('system_num');
		$fid = I('fid');//素材ID
		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
		}

		$whereSource['fid'] = $fid;
		$whereSource['fcustomer'] = $system_num;
		$delSource = M('tmediasource')->where($whereSource)->save(['fstatus'=>-1,'fmodifytime'=>date('Y-m-d H:i:s'),'fmodifyer'=>session('regulatorpersonInfo.fname')]);
		if(!empty($delSource)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败，无权限'));
		}
	}

	/*
	* 提交素材任务
	* by zw
	*/
	public function submitSourceTask(){
		$mediaId = I('fmediaid');//媒体ID
		$issueDate = I('fissuedate');//任务日期

		if(empty($mediaId) || empty($issueDate)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
		}

		$whereSource['a.fmediaid'] = $mediaId;
		$whereSource['a.fissuedate'] = $issueDate;
		$whereSource['a.fstatus'] = 1;
		
		$countSource = M('tmediasource a')->where($whereSource)->count();

		if(empty($countSource)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂无素材，任务提交失败'));
		}

		$viewMA = M('tmedia')->where(['fid'=>$mediaId])->find();

		//创建监测计划
		$params[] =[
			'data'=>[
				[
					'inspectLevel'=>1,
					'inspectType'=>0,
					'issueDate'=>$issueDate,
					'planTime'=>date('Y-m-d',strtotime($issueDate.'+1 month'))
				]
			],
			'inspectOrg' => '上海市监局',
			'inspectOrgId' => 0,
			'inspectPerson' => '分众剪辑',
			'inspectPersonId' => 0,
			'mediaId' => $mediaId
		];

		$ret = accessService('/api-commer/inspectplan/batch_add',$params,'POST');
		if(empty($ret['status'])){
			$this->ajaxReturn(array('code'=>1,'msg'=>'任务已存在，请勿重复提交'));
		}

		//获取监测计划结果
		$params2 =[
			'issue_start_time' => $issueDate,
			'issue_end_time' => $issueDate,
			'plan_end_time' => date('Y-m-d',strtotime($issueDate.'+1 month')),
			'plan_start_time' => date('Y-m-d',strtotime($issueDate.'+1 month')),
			'media_id' => $mediaId
		];

		$ret2 = accessService('/api-commer/inspectplan/detail_list',$params2,'POST');//计划任务添加

		if(isset($ret2['data']['list'][0]['id'])){
			$ipnId = $ret2['data']['list'][0]['id'];
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'监测计划创建失败'));
		}

		//添加任务
		$params3 = [
			'app_id' => 1004,
			'biz_key' => $ipnId,
			'biz_name' => '分众剪辑',
			'creator' => '',
			'creator_id' => 0,
			'creator_org' => '',
			'creator_org_id' => 0,
			'person_group_id' => 0,
			'rent_id' => 0,
			'link_code' => 200101,
			'link_name' => '分众剪辑',
			'title' => '分众剪辑：'.$viewMA['fmedianame'].'（'.$issueDate.'）',
		];

		$ret3 = accessService('/api-process/task20/addTask',$params3,'POST');//添加任务
		if(empty($ret3['status'])){
			$this->ajaxReturn(array('code'=>1,'msg'=>'任务创建失败，请联系技术人员处理'));
		}else{
			//钉钉提醒
			$title = '上海分众任务提醒';
            $content = "> 分众任务提醒，请关注。\n\n媒体名称：".$viewMA['fmedianame']."（".$mediaId."） \n\n计划日期：".$issueDate."\n\n计划ID：".$ipnId."\n\n任务ID：".$ret3['data']['task_id']."\n\n任务环节ID：".$ret3['data']['task_link_id']."\n\n";
            $smsphone = [13758156171];//提醒人员，赵国琪
            $token = '6299ea2a7184853c83972cf1df3ae1a838d23d18e1ea207cd168c6b3b92fd8cc';
            push_ddtask($title,$content,$smsphone,$token,'markdown');

			$this->ajaxReturn(array('code'=>0,'msg'=>'任务提交成功'));
		}
	}

}