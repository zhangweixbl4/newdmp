<?php
namespace Api\Model;
use Think\Exception;


class ZonesAdModel{



	/*跨地区广告数据写入，月、季度、半年、年*/
    public function zones_common($table_name,$media_id,$fdate,$fad_name,$countArr){
		
		/* var_dump($table_name);
		var_dump($media_id);
		var_dump($fdate);
		var_dump($fad_name);
		var_dump($countArr); */
		//var_dump($countArr);
		
		
		$media_id = intval($media_id);//转为int类型
		if(intval($media_id) == 0){
			return false;
		} 

		
		$fid = M($table_name)->where(array('fmedia_id'=>$media_id,'fdate'=>$fdate,'fad_name'=>$fad_name))->getField('fid');
		if(intval($fid) === 0 ){
			try{//尝试新增数据
				$fid = M($table_name)->add(array('fmedia_id'=>$media_id,'fdate'=>$fdate,'fad_name'=>$fad_name));
			}catch(Exception $e) { //如果新增失败则获取id
				//var_dump($e);
				$fid = M($table_name)->where(array('fmedia_id'=>$media_id,'fdate'=>$fdate,'fad_name'=>$fad_name))->getField('fid');
			} 
		}
		
		$save_data = $countArr;

		$save_ret = M($table_name)->where(array('fid'=>$fid))->save($save_data);//修改数量

		if($save_ret >= 0){
			return true;
		}else{
			return false;
		}
		
    }
	
	
	
	/*汇总数据，月*/
	public function ad_month($issueList,$fad_name,$fadclasscode,$fmediaclass_id){
		
		
		$monthDataList = array();
		foreach($issueList as $issue){
			$rr = A('Common/Zongju','Model')->get_confirm($issue['fmediaid'],date('Y-m-d',$issue['fissuedate']));
			if(!$rr['zongju_confirm_state']) continue;//结束本次循环
			$monthDataList[$issue['fmediaid'].'_'.date('Y-m-01',$issue['fissuedate']).'_'.$issue['fregionid']][] = 1; 
		}
		
		
		foreach($monthDataList as $key => $monthData){
			$keyarr = explode('_',$key);
			$mediaId = $keyarr[0];
			$issueMonth = $keyarr[1];
			$fregion_id = $keyarr[2];
			A('ZonesAd','Model')->zones_common('tbn_zones_ad_month',$mediaId,$issueMonth,$fad_name,array(
				
																												'fregion_id' => $fregion_id,	
																												'fad_class' => $fadclasscode,
																												'fmediaclass_id' => $fmediaclass_id,
																												'issue_count' => count($monthData),
																													));
		}
		
		
	}
	
	
	/*汇总数据，年*/
	public function ad_year($issueList,$fad_name,$fadclasscode,$fmediaclass_id){
		
		
		$DataList = array();
		foreach($issueList as $issue){
			$rr = A('Common/Zongju','Model')->get_confirm($issue['fmediaid'],date('Y-m-d',$issue['fissuedate']));
			if(!$rr['zongju_confirm_state']) continue;//结束本次循环
			$DataList[$issue['fmediaid'].'_'.date('Y-01-01',$issue['fissuedate']).'_'.$issue['fregionid']][] = 1; 
		}
		
		
		foreach($DataList as $key => $Data){
			$keyarr = explode('_',$key);
			$mediaId = $keyarr[0];
			$issueYear = $keyarr[1];
			$fregion_id = $keyarr[2];
			A('ZonesAd','Model')->zones_common('tbn_zones_ad_year',$mediaId,$issueYear,$fad_name,array(
				
																												'fregion_id' => $fregion_id,	
																												'fad_class' => $fadclasscode,
																												'fmediaclass_id' => $fmediaclass_id,
																												'issue_count' => count($Data),
																													));
		}
		
		
	}
	
	
	
	/*汇总数据，半年*/
	public function ad_half_year($issueList,$fad_name,$fadclasscode,$fmediaclass_id){
		
		
		$DataList = array();
		foreach($issueList as $issue){
			
			$rr = A('Common/Zongju','Model')->get_confirm($issue['fmediaid'],date('Y-m-d',$issue['fissuedate']));
			if(!$rr['zongju_confirm_state']) continue;//结束本次循环
			
			if(intval(date('n',$issue['fissuedate'])) <= 6){
				$date = date('Y-01-01',$issue['fissuedate']);
			}
			
			if(intval(date('n',$issue['fissuedate'])) >= 7){
				$date = date('Y-07-01',$issue['fissuedate']);
			}

			
			
			$DataList[$issue['fmediaid'].'_'.$date.'_'.$issue['fregionid']][] = 1; 
		}
		
		
		foreach($DataList as $key => $Data){
			$keyarr = explode('_',$key);
			$mediaId = $keyarr[0];
			$issueYear = $keyarr[1];
			$fregion_id = $keyarr[2];
			A('ZonesAd','Model')->zones_common('tbn_zones_ad_half_year',$mediaId,$issueYear,$fad_name,array(
				
																												'fregion_id' => $fregion_id,	
																												'fad_class' => $fadclasscode,
																												'fmediaclass_id' => $fmediaclass_id,
																												'issue_count' => count($Data),
																													));
		}
		
		
	}
	
	
	
	
	/*汇总数据，半月*/
	public function ad_half_month($issueList,$fad_name,$fadclasscode,$fmediaclass_id){
		
		
		$DataList = array();
		foreach($issueList as $issue){
			$rr = A('Common/Zongju','Model')->get_confirm($issue['fmediaid'],date('Y-m-d',$issue['fissuedate']));
			if(!$rr['zongju_confirm_state']) continue;//结束本次循环
			
			if(intval(date('j',$issue['fissuedate'])) <= 15){
				$date = date('Y-m-01',$issue['fissuedate']);
			}
			
			if(intval(date('j',$issue['fissuedate'])) >= 16){
				$date = date('Y-m-16',$issue['fissuedate']);
			}
			
			$DataList[$issue['fmediaid'].'_'.$date.'_'.$issue['fregionid']][] = 1; 
		}
		
		
		foreach($DataList as $key => $Data){
			$keyarr = explode('_',$key);
			$mediaId = $keyarr[0];
			$issueDDate = $keyarr[1];
			$fregion_id = $keyarr[2];
			A('ZonesAd','Model')->zones_common('tbn_zones_ad_half_month',$mediaId,$issueDDate,$fad_name,array(
				
																												'fregion_id' => $fregion_id,	
																												'fad_class' => $fadclasscode,
																												'fmediaclass_id' => $fmediaclass_id,
																												'issue_count' => count($Data),
																													));
		}
		
		
	}
	
	
	
	/*汇总数据，季度*/
	public function ad_half_quarter($issueList,$fad_name,$fadclasscode,$fmediaclass_id){
		
		
		$DataList = array();
		foreach($issueList as $issue){
			$rr = A('Common/Zongju','Model')->get_confirm($issue['fmediaid'],date('Y-m-d',$issue['fissuedate']));
			if(!$rr['zongju_confirm_state']) continue;//结束本次循环
			
			$season_month = intval(ceil((date('n',$issue['fissuedate']))/3) * 3 - 2);//获取该日期属于哪一季度
				

			$date = date('Y-'.$season_month.'-01',$issue['fissuedate']);
			
			$DataList[$issue['fmediaid'].'_'.$date.'_'.$issue['fregionid']][] = 1; 
		}
		
		
		foreach($DataList as $key => $Data){
			$keyarr = explode('_',$key);
			$mediaId = $keyarr[0];
			$issueDDate = $keyarr[1];
			$fregion_id = $keyarr[2];
			A('ZonesAd','Model')->zones_common('tbn_zones_ad_quarter',$mediaId,$issueDDate,$fad_name,array(
				
																												'fregion_id' => $fregion_id,	
																												'fad_class' => $fadclasscode,
																												'fmediaclass_id' => $fmediaclass_id,
																												'issue_count' => count($Data),
																													));
		}
		
		
	}
	
	
	/*汇总数据，季度*/
	public function ad_half_week($issueList,$fad_name,$fadclasscode,$fmediaclass_id){
		
		
		$DataList = array();
		foreach($issueList as $issue){
			$rr = A('Common/Zongju','Model')->get_confirm($issue['fmediaid'],date('Y-m-d',$issue['fissuedate']));
			if(!$rr['zongju_confirm_state']) continue;//结束本次循环
			
			$year_week = dayweek($issue['fissuedate']);//获取该日期属于哪一周
			$weeknum = 	$year_week['weeknum'];
			
			if($weeknum < 10){
				$weeknum = '0'.strval($weeknum);
			}

			$date = $year_week['year'].$weeknum;
			
			$DataList[$issue['fmediaid'].'_'.$date.'_'.$issue['fregionid']][] = 1; 
		}
		
		
		foreach($DataList as $key => $Data){
			$keyarr = explode('_',$key);
			$mediaId = $keyarr[0];
			$issueDDate = $keyarr[1];
			$fregion_id = $keyarr[2];
			A('ZonesAd','Model')->zones_common('tbn_zones_ad_week',$mediaId,$issueDDate,$fad_name,array(
				
																												'fregion_id' => $fregion_id,	
																												'fad_class' => $fadclasscode,
																												'fmediaclass_id' => $fmediaclass_id,
																												'issue_count' => count($Data),
																													));
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}



