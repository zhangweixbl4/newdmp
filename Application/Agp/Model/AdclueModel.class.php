<?php
namespace Agp\Model;



class AdclueModel{


    /*获取广告发布详细信息*/
    public function get_issue_info($fmediaclass,$fsampleid){

		
		$issue_info = array();
		
		if($fmediaclass == '01'){//电视

			$issue_info = M('ttvissue')->field('date_format(max(fissuedate),"%Y/%m/%d") as max_fissuedate,
												date_format(min(fissuedate),"%Y/%m/%d") as min_fissuedate,
												sum(famounts) as famounts,
												sum(fquantity) as fquantity,
												sum(flength) as flength'
											)->where(array('ftvsampleid'=>$fsampleid))->select();//
			$issue_info = $issue_info[0];

			
		}elseif($fmediaclass == '02'){//广播
			
			$issue_info = M('tbcissue')->field('date_format(max(fissuedate),"%Y/%m/%d") as max_fissuedate,
												date_format(min(fissuedate),"%Y/%m/%d") as min_fissuedate,
												sum(famounts) as famounts,
												sum(fquantity) as fquantity,
												sum(flength) as flength'
											)->where(array('fbcsampleid'=>$fsampleid))->select();//
			$issue_info = $issue_info[0];
			
			
			
			
		}elseif($fmediaclass == '03'){//报纸
			
			$issue_info = M('tpaperissue')->field('date_format(max(fissuedate),"%Y/%m/%d") as max_fissuedate,
												date_format(min(fissuedate),"%Y/%m/%d") as min_fissuedate,
												sum(famounts) as famounts,
												sum(fquantity) as fquantity'
											)->where(array('fpapersampleid'=>$fsampleid))->select();//
			$issue_info = $issue_info[0];
			
			
			
			
		}
		
		return $issue_info;
		
    }
	
	/*获取播出列表*/
	public function get_issue_list($fmediaclass,$fsampleid,$p,$pp){
		//var_dump($fmediaclass);
		if($fmediaclass == '01'){//电视

			$where = array();
			$where['ttvissue.ftvsampleid'] = $fsampleid;
			$issueList = M('ttvissue')
									->field('ttvissue.fstarttime,tmedia.fmedianame')
									->join('tmedia on tmedia.fid = ttvissue.fmediaid')
									->where($where)->page($p.','.$pp)->select();
			$issueList = list_k($issueList,$p,$pp);//为列表加上序号
			
		}elseif($fmediaclass == '02'){//广播
			
			$where = array();
			$where['tbcissue.fbcsampleid'] = $fsampleid;
			$issueList = M('tbcissue')
									->field('tbcissue.fstarttime,tmedia.fmedianame')
									->join('tmedia on tmedia.fid = tbcissue.fmediaid')
									->where($where)->page($p.','.$pp)->select();
			$issueList = list_k($issueList,$p,$pp);//为列表加上序号

		}elseif($fmediaclass == '03'){//报纸
			
			$where = array();
			$where['tpaperissue.fpapersampleid'] = $fsampleid;
			$issueList = M('tpaperissue')
										->field('tpaperissue.fissuedate as fstarttime,tmedia.fmedianame')
										->join('tmedia on tmedia.fid = tpaperissue.fmediaid')
										->where($where)->page($p.','.$pp)->select();
			$issueList = list_k($issueList,$p,$pp);//为列表加上序号
			

		}
		
		return $issueList;
	}
	
	
	/*数据复核*/
	public function check_illegal(){
		
		$fadclueid = I('fadclueid');//线索ID
		$fillegalcontent = I('fillegalcontent');//违法内容
		$fexpressioncodes_array = I('fexpressioncodes');//违法表现代码

		foreach($fexpressioncodes_array as $k => $v){//循环违法表现代码
			$illegal_info = M('tillegal')->where(array('fcode'=>$v,'fstate'=>1))->find();
			if($illegal_info){
				$fexpressioncodes .= $illegal_info['fcode'].';';//把合规的违法表现代码加入字符串
				$fexpression .= $illegal_info['fexpression'].';'."\n";//把合规的违法表现加入字符串
				$fconfirmation .= $illegal_info['fconfirmation'].';'."\n";//把合规的认定依据加入字符串
				$fpunishment .= $illegal_info['fpunishment'].';'."\n";//把合规的处罚依据加入字符串
				$fpunishmenttype .= $illegal_info['fpunishmenttype'].';'."\n";//把合规的处罚种类及幅度加入字符串
				if($illegal_info['fillegaltype'] > $fillegaltypecode) $fillegaltypecode = $illegal_info['fillegaltype'];//获取违法类型代码
			}
			
			
		}
		$adclue_data = array();
		$adclue_data['fexpressioncodes'] = rtrim($fexpressioncodes,';');//去掉末尾分号
		$adclue_data['fexpressions'] = rtrim($fexpression,';'."\n");//去掉末尾分号
		$adclue_data['fconfirmations'] = rtrim($fconfirmation,';'."\n");//去掉末尾分号
		$adclue_data['fpunishments'] = rtrim($fpunishment,';'."\n");//去掉末尾分号
		$adclue_data['fpunishmenttypes'] = rtrim($fpunishmenttype,';'."\n");//去掉末尾分号
		$adclue_data['fillegaltypecode'] = intval($fillegaltypecode);//违法类型代码
		$adclue_data['fillegalcontent'] = $fillegalcontent;//违法内容
		
		$adcluesectionInfo = M('tadcluesection')->where(array('fadclueid'=>$fadclueid))->order('fadcluesectionid desc')->find();//最后一个环节的信息
		$e_s_data = array();
		$e_s_data['fexecuter'] = session('regulatorpersonInfo.fname');//执行人
		$e_s_data['fexecutetime'] = date('Y-m-d H:i:s');//执行时间
		$e_s_data['fdescribe'] = $fdescribe;//说明
		$e_s_data['fstate'] = 2;//把状态改为已转办

		M('tadclue')->where(array('fadclueid'=>$fadclueid))->save(array('fdescribe',$fdescribe));//修改线索
		M('tadcluesection')->where(array('fadcluesectionid'=>$adcluesectionInfo['fadcluesectionid']))->save($e_s_data);//修改原环节信息
		M('tadclue')->where(array('fadclueid'=>$fadclueid))->save($adclue_data);//修改线索信息
		
		
		$freceiveunit = M('tadcluesection')->where(array('ftype'=>2,'fadclueid'=>$fadclueid))->order('fadcluesectionid asc')->getField('fsendunit');//查询最早一个发起复核的机构
		A('Common/Adcluesection','Model')->create_adcluesection($fadclueid,session('regulatorpersonInfo.fregulatorid'),$freceiveunit,session('regulatorpersonInfo.fname'),1);//创建环节
		return true;
	}
	
	/*维持原数据*/
	public function maintain_illegal(){
		
		$fadclueid = I('fadclueid');//线索ID
		$fdescribe = I('fdescribe');//说明
		$adcluesectionInfo = M('tadcluesection')->where(array('fadclueid'=>$fadclueid))->order('fadcluesectionid desc')->find();//最后一个环节的信息
		$e_s_data = array();
		$e_s_data['fexecuter'] = session('regulatorpersonInfo.fname');//执行人
		$e_s_data['fexecutetime'] = date('Y-m-d H:i:s');//执行时间
		$e_s_data['fdescribe'] = $fdescribe;//说明
		$e_s_data['fstate'] = 2;//把状态改为已转办

		M('tadclue')->where(array('fadclueid'=>$fadclueid))->save(array('fdescribe',$fdescribe));//修改线索
		M('tadcluesection')->where(array('fadcluesectionid'=>$adcluesectionInfo['fadcluesectionid']))->save($e_s_data);//修改原环节信息
		
		$freceiveunit = M('tadcluesection')->where(array('ftype'=>2,'fadclueid'=>$fadclueid))->order('fadcluesectionid asc')->getField('fsendunit');//查询最早一个发起复核的机构
		A('Common/Adcluesection','Model')->create_adcluesection($fadclueid,session('regulatorpersonInfo.fregulatorid'),$freceiveunit,session('regulatorpersonInfo.fname'),1);//创建环节
		return true;
	}
	
	/*没有违法*/
	public function not_illegal(){
		
		$fadclueid = I('fadclueid');//线索ID
		$fdescribe = I('fdescribe');//说明

		
		$adclue_data = array();
		$adclue_data['fexpressioncodes'] = '';//去掉末尾分号
		$adclue_data['fexpressions'] = '';//去掉末尾分号
		$adclue_data['fconfirmations'] = '';//去掉末尾分号
		$adclue_data['fpunishments'] = '';//去掉末尾分号
		$adclue_data['fpunishmenttypes'] = '';//去掉末尾分号
		$adclue_data['fillegaltypecode'] = '';//违法类型代码
		$adclue_data['fillegalcontent'] = '';//违法内容
		$adcluesectionInfo = M('tadcluesection')->where(array('fadclueid'=>$fadclueid))->order('fadcluesectionid desc')->find();//最后一个环节的信息
		M('tadclue')->where(array('fadclueid'=>$fadclueid))->save($adclue_data);//修改线索信息
		
		M('tadclue')->where(array('fadclueid'=>$fadclueid))->save(array(
																		'fstate'=>1,//处理状态，已处理
																		'fhandletime'=>date('Y-m-d H:i:s'),//处理时间
																		'fhandler'=>session('regulatorpersonInfo.fname'),//处理人
																		'fresults'=>'不违法',//处理结果 拼接字段
																		'fdescribe'=>$fdescribe,//说明
																		'fresult'=>0,//处理结果，最严重ID
																	));//修改线索信息
		M('tadcluesection')->where(array('fadcluesectionid'=>$adcluesectionInfo['fadcluesectionid']))->save(array(
																													'fexecuter'=>session('regulatorpersonInfo.fname'),//执行人
																													'fexecutetime'=>date('Y-m-d H:i:s'),//执行时间
																													'fdescribe'=>$fdescribe,//说明
																													'fstate'=>3,//处理状态，已处理
																													
																												));//修改原环节信息
		return true;

	}
	
	/*发送告知单给媒介*/
	public function sent_to_adclue_media($adclueInfo,$media_mail){
							
		$issueList = A('Agp/Adclue','Model')->get_issue_list($adclueInfo['fmediaclass'],$adclueInfo['fsampleid'],1,100);
		$issue_text .= '<table>';
		foreach($issueList as $issue){
			$issue_text .= '<tr><td>'.$issue['fstarttime'].'</td></tr>';//<td>'.$issue['fmedianame'].'</td>
		}
		$issue_text .= '</table>';
		
		$doc_content .= '<html style="font-family:仿宋" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">'; 
		$doc_content .= '<head>';  
		$doc_content .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>';  
		$doc_content .= '<xml><w:WordDocument><w:View>Print</w:View></xml>';  
		$doc_content .= '</head>';
		$doc_content .= '<body>';
		$doc_content .= $adclueInfo['fmedianame'].'：<br />';
		$doc_content .= '&nbsp;&nbsp;现告知你单位播出的“'.$adclueInfo['fadname'].'-'.$adclueInfo['fversion'].'”涉嫌'.$adclueInfo['fillegaltype'].'，具体如下'."\n";
		$doc_content .= '<table border="1"  style="font-family:仿宋">';
		$doc_content .= '<tr> <td width="80">广告名称</td> <td>'.$adclueInfo['fadname'].'</td> </tr>';
		$doc_content .= '<tr> <td>广告版本</td> <td>'.$adclueInfo['fversion'].'</td> </tr>';
		$doc_content .= '<tr> <td>广告类别</td> <td>'.$adclueInfo['adclass_fullname'].'</td> </tr>';
		$doc_content .= '<tr> <td>发布条次</td> <td>'.count($issueList).'</td> </tr>';
		$doc_content .= '<tr> <td>违法类型</td> <td>'.str_replace("\n","\n<br />",$adclueInfo['fillegaltype']).'</td> </tr>';
		$doc_content .= '<tr> <td>违法内容</td> <td>'.str_replace("\n","\n<br />",$adclueInfo['fillegalcontent']).'</td> </tr>';
		$doc_content .= '<tr> <td>违法表现</td> <td>'.str_replace("\n","\n<br />",$adclueInfo['fexpressions']).'</td> </tr>';
		$doc_content .= '<tr> <td>认定依据</td> <td>'.str_replace("\n","\n<br />",$adclueInfo['fconfirmations']).'</td> </tr>';
		$doc_content .= '<tr> <td>播出记录</td> <td>'.$issue_text.'</td> </tr>';
		$doc_content .= '</table>';
		$doc_content .= '<p align="right">'.$adclueInfo['freceiveunitname'].'广告监测中心</p>';
		$doc_content .= '<p align="right">'.date('Y-m-d').'</p>';
		
		$doc_content .= '</body>';
		$doc_content .= '</html>';
		
		$mail_send_state = A('Common/SendMail','Model')->sent_to_adclue_media($media_mail,'涉嫌违法广告告知书',$doc_content,$doc_content,$adclueInfo['freceiveunitname'].'广告监测中心');
		return $mail_send_state;
		
		
	}
	
	



}