<?php
namespace Api\Controller;
use Think\Controller;

class SjzHwAdController extends Controller {
	
	#获取户外广告媒体
	public function GetMedias(){
		$remoteUrl = 'http://www.ggjcxt.com/api/OutdoorMedia/GetMedias';#?beginTime=2000-01-01&endTime=2020-01-01
		$beginTime = I('beginTime');
		$endTime = I('endTime');
		$rett = http($remoteUrl,array('beginTime'=>$beginTime,'endTime'=>$endTime),'get');
		$rett = json_decode($rett,true);
		foreach($rett['Data'] as $mediaData){
			$ck_media = M('tmedia')->where(array('fmediacode'=>$mediaData['MediaId']))->count();
			if($ck_media) continue;
			
			$mediaowner = M('tmediaowner')->master(true)->where(array('fcreditcode'=>'hw_'.$mediaData['MediaRegion']))->find();
			if($mediaowner){
				$mediaownerid = $mediaowner['fid'];
			}else{
				
				$regionInfo = M('tregion')->where(array('fid'=>$mediaData['MediaRegion']))->find(); #查询地区信息

				$a_e_data = array();
				$a_e_data['fregaddr'] = '';//注册地址
				$a_e_data['fofficeaddr'] = '';//办公地址
				$a_e_data['fname'] = $regionInfo['ffullname'].'_户外';//媒介机构名称
				$a_e_data['flinkman'] = '';//联系人
				$a_e_data['ftel'] = '';//联系电话
				$a_e_data['fcreditcode'] = 'hw_'.$mediaData['MediaRegion'];//企业信用代码
				$a_e_data['fenterprise'] = '';//设立主体
				
				$a_e_data['flicence'] = '';//特许经营许可证
				$a_e_data['fregionid'] = $mediaData['MediaRegion'];//行政区划ID
				
				$a_e_data['fstate'] = 1;//状态，-1删除，0-无效，1-有效
				$a_e_data['fcreator'] = '石家庄户外接口';//创建人
				$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
				$a_e_data['fmodifier'] = '石家庄户外接口';//修改人
				$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
				$mediaownerid = M('tmediaowner')->add($a_e_data);//添加数据
				
			}
			
			$a_e_data = array();
			$a_e_data['fmediaclassid'] = '05';//媒介类别id
			
			$a_e_data['fmediaownerid'] = $mediaownerid;//媒介机构ID
			$a_e_data['media_region_id'] = $mediaData['MediaRegion'];
			$a_e_data['fdeviceid'] = 0;//采集设备ID
			$a_e_data['fmediacode'] = $mediaData['MediaId'];//媒介代码
			$a_e_data['fmedianame'] = $mediaData['MediaName'].'(石家庄户外)';//媒介名称
			$a_e_data['faddr'] = $mediaData['MediaAddress'];//媒介地址

			$a_e_data['fcollecttype'] = 30;//采集方式

			$a_e_data['fchannelid'] = 0;//通道号

			$a_e_data['priority'] = 1;//优先级
			
			$a_e_data['auto_ext_day_ago'] = 0;
			
			$a_e_data['fcreator'] = '石家庄户外接口';//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = '石家庄户外接口';//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			$media_id = M('tmedia')->add($a_e_data);//添加数据
			if($media_id){
				M('tmedia')->where(array('fid'=>$media_id))->save(array('main_media_id'=>$media_id));
			}
		}
	}
	
	
	
	#获取监测广告信息
	public function GetAdvertisements(){
		
		set_time_limit(600);
		$remoteUrl = 'http://www.ggjcxt.com/api/OutdoorMedia/GetAdvertisements';#?beginTime=2000-01-01&endTime=2020-01-01
		$beginTime = I('beginTime');
		$endTime = I('endTime');
		$rett = http($remoteUrl,array('beginTime'=>$beginTime,'endTime'=>$endTime),'get',false,60);
		#var_dump($rett);
		$rett = json_decode($rett,true);
		
		foreach($rett['Data'] as $Data){
			$mediaInfo = M('tmedia')->cache(true,60)->where(array('fmediacode'=>$Data['TaskMediaId']))->find();

			$a_data = array();

			$a_data['fissuedate'] = date('Y-m-d',strtotime($Data['TaskUploadTime']));
			$a_data['fissuedatetime'] = $Data['TaskUploadTime'];
			$a_data['fadname'] = $Data['TaskAdvertisementContent'];
			$a_data['fadbrand'] = $Data['TaskAdvertisementBrand'];
			$a_data['fimgs'] = '';
			$a_data['fmediaid'] = $mediaInfo['fid'];
			$a_data['identify'] = md5($mediaInfo['fid'].'_'.$Data['TaskId']);
			$a_data['spread_info'] = json_encode(array('TaskId'=>$Data['TaskId']));
			$a_data['fcreatetime'] = date('Y-m-d H:i:s');
			$a_data['fsource'] = '石家庄接口';

			if(M('huwai_ad')->where(array('identify'=>$a_data['identify']))->count() > 0) continue;

			M('huwai_ad')->add($a_data,array(),true);
			

		}
		
		echo 'SUCCESS';
		
	}
	
	
	#处理图片
	public function handle_img(){
		set_time_limit(600);
		$dataList = M('huwai_ad')->field('fid,spread_info')->where(array('fsource'=>'石家庄接口','fimgs'=>array('exp',' = ""')))->limit(10)->select();
		$getImgUrl = 'http://www.ggjcxt.com/api/OutdoorMedia/GetAdvertisementImages'; #?taskId=5D2408BED4C6EB1428430390
		foreach($dataList as $data){
			
			
			$fimgArr = array();
			$spread_info = json_decode($data['spread_info'],true); #扩展信息
			$rettt = http($getImgUrl,array('taskId'=>$spread_info['TaskId']),'get',false,30); # 获取图片信息
			
			$rettt = json_decode($rettt,true);
			foreach($rettt['Data'] as $ke => $item){
				
				$imgKey = 'shijiazhuanghw/'.$spread_info['TaskId'].'/'.$ke.'.jpg';
				#file_put_contents('LOG/'.$imgKey, base64_decode($item['AdvertisementImage']));
				
				
				$rrr = A('Common/Qiniu','Model')->delete_keys(array($imgKey));
				$imgUrl = A('Common/Qiniu','Model')->qiniu_up_str(base64_decode($item['AdvertisementImage']),$imgKey);
				
				$fimgArr[] = $imgUrl;
			}
			
			if(count($fimgArr) > 0){
				M('huwai_ad')->where(array('fid'=>$data['fid']))->save(array('fimgs'=>implode(',',$fimgArr)));
			}
			
			
		}
		
		echo 'SUCCESS';
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}