const commonHeader = {
  name: 'commonHeader',
  props: ['title'],
  template: `
  <header style="background-color: #fff;color: rgba(24, 144, 255, 0.996078431372549);height:65px;line-height:65px;padding-left: 30px;margin-bottom: 20px;">
    <img src="/Application/TaskInput/View/Lvyou/img/logo.png" height="25px" alt="logo" style="vertical-align: middle;">
    <span style="vertical-align: middle;">{{title}}</span>
  </header>
  `
}