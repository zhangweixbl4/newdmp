Vue.prototype.$ELEMENT = { size: "mini", zIndex: 3000 };
Vue.use(VueClipboard)

const vm = new Vue({
  el: '#app',
  data() {
    return {
      tableData:[],
      tableHeight: '400px',
      count: '0',
      loading: false,
      fdate: [],
      onlyillegal: false,
      form: {
        fstate: '',
        fmedianame: '',
        fdate: '',
        onlyillegal: '0'
      },
      fstate: {
        '0': {
          label: '未检查',
          type: 'danger'
        },
        '2': {
          label: '已检查',
          type: 'success'
        },
      },
      page: {
        p: 1,
        pp: 20,
      },
      sourceForm: '',
    }
  },
  created() {
    this.fdate = this.setFdate()
    this.sourceForm = JSON.stringify(this.form)
    this.getList()
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight()
      document.querySelector('#app').style.opacity = 1
    })
    window.onresize = () => {
      this.getTableHeight()
    }
  },
  methods: {
    fdateChange(val) {
      this.form.fdate = val.join(',')
    },
    getList(page) {
      this.page.p = page || 1
      let data = {
        ...this.form,
        ...this.page,
      }
      this.loading = true
      fly.post('/TaskInput/SamCheck/dataList', Qs.stringify(data)).then(({ data }) => {
        if (data.code === 0) {
          this.count = +data.dataCount
          this.tableData = data.dataList
        } else {
          $h.notify.error(data.msg, this)
        }
        this.loading = false
      })
    },
    reset() {
      this.form = JSON.parse(this.sourceForm)
      this.fdate = this.setFdate()
      this.getList()
    },
    getTableHeight() {
      let formHeight = this.$refs.sform.$el.clientHeight
      this.tableHeight = document.body.clientHeight - formHeight - 116 + 'px'
    },
    sizeChange(size) {
      this.page.pp = size
      this.getList(this.page.p)
    },
    setFdate() {
      let start = dayjs().subtract(30, 'days').format('YYYY-MM-DD')
      let end = dayjs().format('YYYY-MM-DD')
      this.form.fdate = `${start},${end}`
      return [start,end]
    }
  },
})