<?php
namespace Gongshang\Controller;
use Think\Controller;

class SupervisionController extends BaseController {
    public function issueRanking(){
		session_write_close();
		$regulatorpersonInfo = M('tregulatorperson')->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
		$regulatorInfo = M('tregulator')->where(array('fcode'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息
		$regulatorRegionInfo = M('tregion')->field('fid,fname')->where(array('fid'=>$regulatorInfo['fregionid']))->find();//监管地区信息信息	
		$this->assign('regionid',$regulatorRegionInfo['fid']);

		$this->display();
	}

	//发布区域统计
	public function ajax_region_count(){
		session_write_close();
		$regulatorpersonInfo = M('tregulatorperson')->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
		$regulatorInfo = M('tregulator')->where(array('fcode'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息
		$regulatorRegionInfo = M('tregion')->field('fid,fname')->where(array('fid'=>$regulatorInfo['fregionid']))->find();//监管地区信息信息	
		
		$regionid = $regulatorRegionInfo['fid'];//地区ID
		if(I('regionid') != '') $regionid = I('regionid');
		if($regionid == '100000') $regionid = '000000';
		
		$adclass = I('adclass');//媒介类型
		$startTime = I('startTime')?date('Y-m-d H:i:s',strtotime(I('startTime'))):'';//开始时间
		$endTime = I('endTime')?date('Y-m-d H:i:s',strtotime(I('endTime'))):'';//结束时间
		if($startTime == '' && $endTime) $startTime = date('Y-m-d H:i:s',strtotime($endTime)-30*86400);
		if($endTime == '' && $startTime) $endTime = date("Y-m-d 00:00:00",time()+86400);
		$region_id_rtrim = rtrim($regionid,'00');//去掉地区后面的00
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位

		$where = array();
		$where['left(region.fid,'.$tregion_len.')'] = $region_id_rtrim;
		$where['region.fid'] = array('neq',$regionid);

		if($startTime && $endTime){
			$where['issue.fissuedate'] = array(array('egt',$startTime),array('lt',$endTime),'and');
		}

		//区域电视
		if($adclass == '' || in_array('1',$adclass)){
			$tvissue = M('ttvissue')->cache(true,6000)
								->alias('issue')
								->field('region.fname as name,region.fid as region_id,count(region.fid) as issue_count,count(case when ttvsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when ttvsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
								->join('tregion as region on region.fid =  concat(left(tregion.fid,'.($tregion_len + 2).'),"'.str_pad('',6 - ($tregion_len + 2),'0').'")') //连接地区表（监管机构的下一级地区）
								->where($where)
								->group('region.fid')
								->order('issue_count desc')
								->select();
		}else{
			$tvissue = array();
		}
		// var_dump(M()->getlastSql());
		//区域广播
		if($adclass == '' || in_array('2',$adclass)){
			$bcissue = M('tbcissue')->cache(true,6000)
								->alias('issue')
								->field('region.fname as name,region.fid as region_id,count(region.fid) as issue_count,count(case when tbcsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tbcsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
								->join('tregion as region on region.fid =  concat(left(tregion.fid,'.($tregion_len + 2).'),"'.str_pad('',6 - ($tregion_len + 2),'0').'")') //连接地区表（监管机构的下一级地区）
								->where($where)
								->group('region.fid')
								->order('issue_count desc')
								->select();
		}else{
			$bcissue = array();
		}
		//区域报纸
		if($adclass == '' || in_array('3',$adclass)){
			$paperissue = M('tpaperissue')->cache(true,6000)
								->alias('issue')
								->field('region.fname as name,region.fid as region_id,count(region.fid) as issue_count,count(case when tpapersample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tpapersample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
								->join('tregion as region on region.fid =  concat(left(tregion.fid,'.($tregion_len + 2).'),"'.str_pad('',6 - ($tregion_len + 2),'0').'")') //连接地区表（监管机构的下一级地区）
								->where($where)
								->group('region.fid')
								->order('issue_count desc')
								->select();
		}
		else{
			$paperissue = array();
		}
		//区域总统计
		$region_count = A('Gongshang/Supervise','Model')->region_count($tvissue,$bcissue,$paperissue);
		//对数据进行从大到小的排序
		$region_sort_count = my_sort($region_count,'issue_count',SORT_DESC);
		$this->ajaxReturn(array('code'=>1,'issue_name'=>'地域','data'=>$region_count,'sort'=>$region_sort_count));

	}

	//发布分类统计
	public function ajax_class_count(){
		session_write_close();
		$regulatorpersonInfo = M('tregulatorperson')->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
		$regulatorInfo = M('tregulator')->where(array('fcode'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息
		$regulatorRegionInfo = M('tregion')->field('fid,fname')->where(array('fid'=>$regulatorInfo['fregionid']))->find();//监管地区信息信息	
		
		$regionid = $regulatorRegionInfo['fid'];//地区ID
		if(I('regionid') != '') $regionid = I('regionid');
		if($regionid == '100000') $regionid = '000000';
		
		
		$search_issue = A('Gongshang/Supervise','Model')->search_issue($regionid ,I('startTime'),I('endTime'));
		$adclass = I('adclass');//媒介类型
		$where = $search_issue['where'];
		//分类电视
		if($adclass == '' || in_array('1',$adclass)){
			$tvclass = M('ttvissue')->cache(true,6000)
								->alias('issue')
								->field('substring_index(tadclass.ffullname,">",1) as name,left(tadclass.fcode,2) as class_code,count(case when ttvsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when ttvsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
								->join('tad on tad.fadid = ttvsample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where($where)
								->group('left(tadclass.fcode,2)')
								->order('issue_count desc')
								->select();
		}else{
			$tvclass = array();
		}
		//分类广播
		if($adclass == '' || in_array('2',$adclass)){
			$bcclass = M('tbcissue')->cache(true,6000)
								->alias('issue')
								->field('substring_index(tadclass.ffullname,">",1) as name,left(tadclass.fcode,2) as class_code,count(case when tbcsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tbcsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
								->join('tad on tad.fadid = tbcsample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where($where)
								->group('left(tadclass.fcode,2)')
								->order('issue_count desc')
								->select();
		}else{
			$bcclass = array();
		}
		//分类报纸
		if($adclass == '' || in_array('3',$adclass)){
			$paperclass = M('tpaperissue')->cache(true,6000)
								->alias('issue')
								->field('substring_index(tadclass.ffullname,">",1) as name,left(tadclass.fcode,2) as class_code,count(case when tpapersample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tpapersample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
								->join('tad on tad.fadid = tpapersample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where($where)
								->group('left(tadclass.fcode,2)')
								->order('issue_count desc')
								->select();
		}else{
			$paperclass = array();
		}
		//分类总统计
		$class_count = A('Gongshang/Supervise','Model')->class_count($tvclass,$bcclass,$paperclass);
		//对数据进行从大到小的排序
		$class_sort_count = my_sort($class_count,'issue_count',SORT_DESC);
		$this->ajaxReturn(array('code'=>1,'issue_name'=>'分类','data'=>$class_count,'sort'=>$class_sort_count));

	}

	//发布媒介机构统计
	public function ajax_mediaowner_count(){
		session_write_close();
		$regulatorpersonInfo = M('tregulatorperson')->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
		$regulatorInfo = M('tregulator')->where(array('fcode'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息
		$regulatorRegionInfo = M('tregion')->field('fid,fname')->where(array('fid'=>$regulatorInfo['fregionid']))->find();//监管地区信息信息	
		
		$regionid = $regulatorRegionInfo['fid'];//地区ID
		if(I('regionid') != '') $regionid = I('regionid');
		if($regionid == '100000') $regionid = '000000';
		
		
		$search_issue = A('Gongshang/Supervise','Model')->search_issue($regionid,I('startTime'),I('endTime'));
		$adclass = I('adclass');//媒介类型
		$where = $search_issue['where'];
		//媒介机构电视
		if($adclass == '' || in_array('1',$adclass)){
			$mediaowner_tv = M('ttvissue')->cache(true,6000)
								->alias('issue')
								->field('tmediaowner.fname as name,tmediaowner.fid as mediaowner_id,count(case when ttvsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when ttvsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmediaowner.fid')
								->order('issue_count desc')
								->limit(10)
								->select();
		}else{
			$mediaowner_tv = array();
		}
		//媒介机构广播
		if($adclass == '' || in_array('2',$adclass)){
			$mediaowner_bc = M('tbcissue')->cache(true,6000)
								->alias('issue')
								->field('tmediaowner.fname as name,tmediaowner.fid as mediaowner_id,count(case when tbcsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tbcsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmediaowner.fid')
								->order('issue_count desc')
								->limit(10)
								->select();
		}else{
			$mediaowner_bc = array();
		}
		//媒介机构报纸
		if($adclass == '' || in_array('3',$adclass)){
			$mediaowner_paper = M('tpaperissue')->cache(true,6000)
								->alias('issue')
								->field('tmediaowner.fname as name,tmediaowner.fid as mediaowner_id,count(case when tpapersample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tpapersample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmediaowner.fid')
								->order('issue_count desc')
								->limit(10)
								->select();
		}else{
			$mediaowner_paper = array();
		}
		//媒介机构
		$mediaowner_count = A('Gongshang/Supervise','Model')->mediaowner_count($mediaowner_tv,$mediaowner_bc,$mediaowner_paper);
		//对数据进行从大到小的排序
		$mediaowner_sort_count = my_sort($mediaowner_count,'issue_count',SORT_DESC);
		$this->ajaxReturn(array('code'=>1,'issue_name'=>'媒介机构','data'=>$mediaowner_count,'sort'=>$mediaowner_sort_count));

	}

	//发布媒介统计
	public function ajax_media_count(){
		session_write_close();
		$regulatorpersonInfo = M('tregulatorperson')->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
		$regulatorInfo = M('tregulator')->where(array('fcode'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息
		$regulatorRegionInfo = M('tregion')->field('fid,fname')->where(array('fid'=>$regulatorInfo['fregionid']))->find();//监管地区信息信息	
		
		$regionid = $regulatorRegionInfo['fid'];//地区ID
		if(I('regionid') != '') $regionid = I('regionid');
		if($regionid == '100000') $regionid = '000000';
		
		
		$search_issue = A('Gongshang/Supervise','Model')->search_issue($regionid,I('startTime'),I('endTime'));
		$adclass = I('adclass');//媒介类型
		$where = $search_issue['where'];
		if($adclass == '' || in_array('1',$adclass)){
			$media_tv = M('ttvissue')->cache(true,6000)
								->alias('issue')
								->field('tmedia.fmedianame as name,tmedia.fid as media_id,count(tmedia.fid) as issue_count,count(case when ttvsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when ttvsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmedia.fid')
								->order('issue_count desc')
								->limit(10)
								->select();
		}else{
			$media_tv = array();
		}
		if($adclass == '' || in_array('2',$adclass)){
			$media_bc = M('tbcissue')->cache(true,6000)
								->alias('issue')
								->field('tmedia.fmedianame as name,tmedia.fid as media_id,count(tmedia.fid) as issue_count,count(case when tbcsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tbcsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmedia.fid')
								->order('issue_count desc')
								->limit(10)
								->select();
		}else{
			$media_bc = array();
		}
		if($adclass == '' || in_array('3',$adclass)){
			$media_paper = M('tpaperissue')->cache(true,6000)
								->alias('issue')
								->field('tmedia.fmedianame as name,tmedia.fid as media_id,count(tmedia.fid) as issue_count,count(case when tpapersample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tpapersample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmedia.fid')
								->order('issue_count desc')
								->limit(10)
								->select();
		}else{
			$media_paper = array();
		}
		//媒介总统计
		$media_count = A('Gongshang/Supervise','Model')->media_count($media_tv,$media_bc,$media_paper);//统计总数
		//对数据进行从大到小的排序
		$media_sort_count = my_sort($media_count,'issue_count',SORT_DESC);
		$this->ajaxReturn(array('code'=>1,'issue_name'=>'媒介','data'=>$media_count,'sort'=>$media_sort_count));

	}

	//发布媒介类型排名
	public function ajax_mediaclass_ranking(){
		session_write_close();
		$regulatorpersonInfo = M('tregulatorperson')->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
		$regulatorInfo = M('tregulator')->where(array('fcode'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息
		$regulatorRegionInfo = M('tregion')->field('fid,fname')->where(array('fid'=>$regulatorInfo['fregionid']))->find();//监管地区信息信息	
		
		$regionid = $regulatorRegionInfo['fid'];//地区ID
		if(I('regionid') != '') $regionid = I('regionid');
		if($regionid == '100000') $regionid = '000000';
		
		
		$search_issue = A('Gongshang/Supervise','Model')->search_issue($regionid,I('startTime'),I('endTime'));
		$where = $search_issue['where'];

		$mediaclass_tv = M('ttvissue')->cache(true,6000)
								->alias('issue')
								->field('tmedia.fmedianame as name,tmedia.fid as media_id,count(tmedia.fid) as issue_count,count(case when ttvsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when ttvsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmedia.fid')
								->order('issue_count desc')
								->limit(5)
								->select();

		$mediaclass_bc = M('tbcissue')->cache(true,6000)
								->alias('issue')
								->field('tmedia.fmedianame as name,tmedia.fid as media_id,count(tmedia.fid) as issue_count,count(case when tbcsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tbcsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmedia.fid')
								->order('issue_count desc')
								->limit(5)
								->select();

		$mediaclass_paper = M('tpaperissue')->cache(true,6000)
								->alias('issue')
								->field('tmedia.fmedianame as name,tmedia.fid as media_id,count(tmedia.fid) as issue_count,count(case when tpapersample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tpapersample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmedia.fid')
								->order('issue_count desc')
								->limit(5)
								->select();

		
		$this->ajaxReturn(array('code'=>1,'tv'=>$mediaclass_tv,'bc'=>$mediaclass_bc,'paper'=>$mediaclass_paper));

	}

	//监测报告列表
	public function monitoringReport(){
		session_write_close();
		$p = I('p',1);//当前第几页
		$pp = 20;//每页显示多少记录
		$regulator_id = session('regulatorpersonInfo.fregulatorid');

		$where['monitor_report.fstate'] = 1;
		$where['monitor_report.regulator_id'] = $regulator_id; 
		$count = M('monitor_report')->field('monitor_report.*,type.type_name')->join('monitor_report_type as type on type.type_id = monitor_report.report_type')->where($where)->count();
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数

		$monitoringReport = M('monitor_report')->field('monitor_report.*,type.type_name')->join('monitor_report_type as type on type.type_id = monitor_report.report_type')->where($where)->order('fcreatetime desc')->limit($Page->firstRow.','.$Page->listRows)->select();
		$monitoringReport = list_k($monitoringReport,$p,$pp);
		$this->assign('monitoringReport',$monitoringReport);
		$this->assign('page',$Page->show());//分页输出
		$this->display();
	
	}

	//监测报告删除
	public function report_delete(){
		session_write_close();
		$report_id = I('report_id');
		$regulator_id = session('regulatorpersonInfo.fregulatorid');
		$result = M('monitorReport')->where(array('report_id' => $report_id,'regulator_id' => $regulator_id))->delete();
		if($result){
			$this->ajaxReturn(array('code' => 0,'msg' => '删除成功'));
		}else{
			$this->ajaxReturn(array('code' => -1,'msg' => '删除失败'));
		}
		// $this->ajaxReturn(array('code' => -1,'msg' => '删除失败'));
	}

	//监测报告下载
	public function report_down(){
		session_write_close();
		$report_id = I('report_id');
		$result = M('monitorReport')->where(array('report_id' => $report_id))->save(array('down_count' => array('exp','down_count + 1'),'last_down_time' => date('Y-m-d H:i:s',time())));
		if($result){
			$this->ajaxReturn(array('code' => 0,'msg' => '更改成功'));
		}else{
			$this->ajaxReturn(array('code' => -1,'msg' => '更改失败'));
		}

	}

	//监测报告预览
	public function report_show(){
		
		$report_url = I('report_url');
		$url = str_replace("/",'\\',$report_url);
		word2html($url,$url.".html");

		$this->ajaxReturn(array('code' => 0,'url' => $url));

	}

	// 监管排名
	public function supervisionRanking(){
		session_write_close();
		$tadclass = M('tadclass')->cache(true,6000)->where(array('fpcode' => array('eq',' '),'fcode' => array('neq',0)))->field('fcode,fadclass')->select();
		$this->assign('tadclass',$tadclass);//广告类别
		$this->assign('regionid',session('regulatorpersonInfo.regionid'));//区域ID
		$this->display();

	}

	// 监管区域统计
	public function sup_ajax_region_count(){
		session_write_close();
		$regionid = session('regulatorpersonInfo.regionid');
		if(I('regionid') != '') $regionid = I('regionid');
		if($regionid == '100000') $regionid = '000000';
		
		$sup_issue = A('Gongshang/Supervise','Model')->sup_issue($regionid ,I('startTime'),I('endTime'),I('adclasscode'));
		$adclass = I('adclass');//媒介类型
		
		$region_id_rtrim = rtrim($regionid,'00');//去掉地区后面的00
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位

		$where = $sup_issue['where'];	

		//区域电视
		if($adclass == '' || in_array('1',$adclass)){
			$tvissue = M('ttvissue')->cache(true,6000)
								->alias('issue')
								->field('region.fname as name,region.fid as region_id,count(region.fid) as issue_count,count(case when ttvsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when ttvsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
								->join('tad on tad.fadid = ttvsample.fadid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
								->join('tregion as region on region.fid =  concat(left(tregion.fid,'.($tregion_len + 2).'),"'.str_pad('',6 - ($tregion_len + 2),'0').'")') //连接地区表（监管机构的下一级地区）
								->where($where)
								->group('region.fid')
								->order('issue_count desc')
								->select();
		}else{
			$tvissue = array();
		}
		//区域广播
		if($adclass == '' || in_array('2',$adclass)){
			$bcissue = M('tbcissue')->cache(true,6000)
								->alias('issue')
								->field('region.fname as name,region.fid as region_id,count(region.fid) as issue_count,count(case when tbcsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tbcsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
								->join('tad on tad.fadid = tbcsample.fadid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
								->join('tregion as region on region.fid =  concat(left(tregion.fid,'.($tregion_len + 2).'),"'.str_pad('',6 - ($tregion_len + 2),'0').'")') //连接地区表（监管机构的下一级地区）
								->where($where)
								->group('region.fid')
								->order('issue_count desc')
								->select();
		}else{
			$bcissue = array();
		}
		//区域报纸
		if($adclass == '' || in_array('3',$adclass)){
			$paperissue = M('tpaperissue')->cache(true,6000)
								->alias('issue')
								->field('region.fname as name,region.fid as region_id,count(region.fid) as issue_count,count(case when tpapersample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tpapersample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
								->join('tad on tad.fadid = tpapersample.fadid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
								->join('tregion as region on region.fid =  concat(left(tregion.fid,'.($tregion_len + 2).'),"'.str_pad('',6 - ($tregion_len + 2),'0').'")') //连接地区表（监管机构的下一级地区）
								->where($where)
								->group('region.fid')
								->order('issue_count desc')
								->select();
		}
		else{
			$paperissue = array();
		}
		//区域总统计
		$region_count = A('Gongshang/Supervise','Model')->region_count($tvissue,$bcissue,$paperissue);
		//对数据进行从大到小的排序
		$region_sort_count = my_sort($region_count,'issue_count',SORT_DESC);
		$this->ajaxReturn(array('code'=>1,'issue_name'=>'地域','data'=>$region_count,'sort'=>$region_sort_count));

	}

	// 监管分类统计
	public function sup_ajax_class_count(){
		session_write_close();
		$regionid = session('regulatorpersonInfo.regionid');//地区ID
		if(I('regionid') != '') $regionid = I('regionid');
		if($regionid == '100000') $regionid = '000000';
		
		$sup_issue = A('Gongshang/Supervise','Model')->sup_issue($regionid ,I('startTime'),I('endTime'),I('adclasscode'));
		$adclass = I('adclass');//媒介类型
		$where = $sup_issue['where'];

		//分类电视
		if($adclass == '' || in_array('1',$adclass)){
			$tvclass = M('ttvissue')->cache(true,6000)
								->alias('issue')
								->field('substring_index(tadclass.ffullname,">",1) as name,left(tadclass.fcode,2) as class_code,count(case when ttvsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when ttvsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
								->join('tad on tad.fadid = ttvsample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where($where)
								->group('left(tadclass.fcode,2)')
								->order('issue_count desc')
								->select();
		}else{
			$tvclass = array();
		}
		//分类广播
		if($adclass == '' || in_array('2',$adclass)){
			$bcclass = M('tbcissue')->cache(true,6000)
								->alias('issue')
								->field('substring_index(tadclass.ffullname,">",1) as name,left(tadclass.fcode,2) as class_code,count(case when tbcsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tbcsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
								->join('tad on tad.fadid = tbcsample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where($where)
								->group('left(tadclass.fcode,2)')
								->order('issue_count desc')
								->select();
		}else{
			$bcclass = array();
		}
		//分类报纸
		if($adclass == '' || in_array('3',$adclass)){
			$paperclass = M('tpaperissue')->cache(true,6000)
								->alias('issue')
								->field('substring_index(tadclass.ffullname,">",1) as name,left(tadclass.fcode,2) as class_code,count(case when tpapersample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tpapersample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
								->join('tad on tad.fadid = tpapersample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where($where)
								->group('left(tadclass.fcode,2)')
								->order('issue_count desc')
								->select();
		}else{
			$paperclass = array();
		}
		//分类总统计
		$class_count = A('Gongshang/Supervise','Model')->class_count($tvclass,$bcclass,$paperclass);
		//对数据进行从大到小的排序
		$class_sort_count = my_sort($class_count,'issue_count',SORT_DESC);
		$this->ajaxReturn(array('code'=>1,'issue_name'=>'分类','data'=>$class_count,'sort'=>$class_sort_count));

	}

	// 监管媒介机构统计
	public function sup_ajax_mediaowner_count(){
		session_write_close();
		$regionid = session('regulatorpersonInfo.regionid');//地区ID
		if(I('regionid') != '') $regionid = I('regionid');
		if($regionid == '100000') $regionid = '000000';
				
		$sup_issue = A('Gongshang/Supervise','Model')->sup_issue($regionid ,I('startTime'),I('endTime'),I('adclasscode'));
		$adclass = I('adclass');//媒介类型
		$where = $sup_issue['where'];
		//媒介机构电视
		if($adclass == '' || in_array('1',$adclass)){
			$mediaowner_tv = M('ttvissue')->cache(true,6000)
								->alias('issue')
								->field('tmediaowner.fname as name,tmediaowner.fid as mediaowner_id,count(case when ttvsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when ttvsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
								->join('tad on tad.fadid = ttvsample.fadid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmediaowner.fid')
								->order('issue_count desc')
								->limit(10)
								->select();
		}else{
			$mediaowner_tv = array();
		}
		//媒介机构广播
		if($adclass == '' || in_array('2',$adclass)){
			$mediaowner_bc = M('tbcissue')->cache(true,6000)
								->alias('issue')
								->field('tmediaowner.fname as name,tmediaowner.fid as mediaowner_id,count(case when tbcsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tbcsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
								->join('tad on tad.fadid = tbcsample.fadid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmediaowner.fid')
								->order('issue_count desc')
								->limit(10)
								->select();
		}else{
			$mediaowner_bc = array();
		}
		//媒介机构报纸
		if($adclass == '' || in_array('3',$adclass)){
			$mediaowner_paper = M('tpaperissue')->cache(true,6000)
								->alias('issue')
								->field('tmediaowner.fname as name,tmediaowner.fid as mediaowner_id,count(case when tpapersample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tpapersample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
								->join('tad on tad.fadid = tpapersample.fadid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmediaowner.fid')
								->order('issue_count desc')
								->limit(10)
								->select();
		}else{
			$mediaowner_paper = array();
		}
		//媒介机构
		$mediaowner_count = A('Gongshang/Supervise','Model')->mediaowner_count($mediaowner_tv,$mediaowner_bc,$mediaowner_paper);
		//对数据进行从大到小的排序
		$mediaowner_sort_count = my_sort($mediaowner_count,'issue_count',SORT_DESC);
		$this->ajaxReturn(array('code'=>1,'issue_name'=>'媒介机构','data'=>$mediaowner_count,'sort'=>$mediaowner_sort_count));

	}

	// 监管媒介统计
	public function sup_ajax_media_count(){
		session_write_close();
		$regionid = session('regulatorpersonInfo.regionid');//地区ID
		if(I('regionid') != '') $regionid = I('regionid');
		if($regionid == '100000') $regionid = '000000';
				
		$sup_issue = A('Gongshang/Supervise','Model')->sup_issue($regionid ,I('startTime'),I('endTime'),I('adclasscode'));
		$adclass = I('adclass');//媒介类型
		$where = $sup_issue['where'];
		if($adclass == '' || in_array('1',$adclass)){
			$media_tv = M('ttvissue')->cache(true,6000)
								->alias('issue')
								->field('tmedia.fmedianame as name,tmedia.fid as media_id,count(tmedia.fid) as issue_count,count(case when ttvsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when ttvsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
								->join('tad on tad.fadid = ttvsample.fadid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmedia.fid')
								->order('issue_count desc')
								->limit(10)
								->select();
		}else{
			$media_tv = array();
		}
		if($adclass == '' || in_array('2',$adclass)){
			$media_bc = M('tbcissue')->cache(true,6000)
								->alias('issue')
								->field('tmedia.fmedianame as name,tmedia.fid as media_id,count(tmedia.fid) as issue_count,count(case when tbcsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tbcsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
								->join('tad on tad.fadid = tbcsample.fadid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmedia.fid')
								->order('issue_count desc')
								->limit(10)
								->select();
		}else{
			$media_bc = array();
		}
		if($adclass == '' || in_array('3',$adclass)){
			$media_paper = M('tpaperissue')->cache(true,6000)
								->alias('issue')
								->field('tmedia.fmedianame as name,tmedia.fid as media_id,count(tmedia.fid) as issue_count,count(case when tpapersample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tpapersample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
								->join('tad on tad.fadid = tpapersample.fadid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmedia.fid')
								->order('issue_count desc')
								->limit(10)
								->select();
		}else{
			$media_paper = array();
		}
		//媒介总统计
		$media_count = A('Gongshang/Supervise','Model')->media_count($media_tv,$media_bc,$media_paper);//统计总数
		//对数据进行从大到小的排序
		$media_sort_count = my_sort($media_count,'issue_count',SORT_DESC);
		$this->ajaxReturn(array('code'=>1,'issue_name'=>'媒介','data'=>$media_count,'sort'=>$media_sort_count));

	}

	// 监管媒介类型排名
	public function sup_ajax_mediaclass_ranking(){
		session_write_close();
		$regionid = session('regulatorpersonInfo.regionid');//地区ID
		if(I('regionid') != '') $regionid = I('regionid');
		if($regionid == '100000') $regionid = '000000';
				
		$sup_issue = A('Gongshang/Supervise','Model')->sup_issue($regionid ,I('startTime'),I('endTime'),I('adclasscode'));
		$where = $sup_issue['where'];

		$mediaclass_tv = M('ttvissue')->cache(true,6000)
								->alias('issue')
								->field('tmedia.fmedianame as name,tmedia.fid as media_id,count(tmedia.fid) as issue_count,count(case when ttvsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when ttvsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
								->join('tad on tad.fadid = ttvsample.fadid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmedia.fid')
								->order('issue_count desc')
								->limit(5)
								->select();

		$mediaclass_bc = M('tbcissue')->cache(true,6000)
								->alias('issue')
								->field('tmedia.fmedianame as name,tmedia.fid as media_id,count(tmedia.fid) as issue_count,count(case when tbcsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tbcsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
								->join('tad on tad.fadid = tbcsample.fadid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmedia.fid')
								->order('issue_count desc')
								->limit(5)
								->select();

		$mediaclass_paper = M('tpaperissue')->cache(true,6000)
								->alias('issue')
								->field('tmedia.fmedianame as name,tmedia.fid as media_id,count(tmedia.fid) as issue_count,count(case when tpapersample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tpapersample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
								->join('tad on tad.fadid = tpapersample.fadid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmedia.fid')
								->order('issue_count desc')
								->limit(5)
								->select();

		
		$this->ajaxReturn(array('code'=>1,'tv'=>$mediaclass_tv,'bc'=>$mediaclass_bc,'paper'=>$mediaclass_paper));

	}

	// 重点排名
	public function emphasisRanking(){
		session_write_close();
		$regionid = session('regulatorpersonInfo.regionid');//地区ID
		if(I('regionid') != '') $regionid = I('regionid');
		if($regionid == '100000') $regionid = '000000';
				
		$startTime = I('startTime')?date('Y-m-d H:i:s',strtotime(I('startTime'))):'';//开始时间
		$endTime = I('endTime')?date('Y-m-d H:i:s',strtotime(I('endTime'))):'';//结束时间
		if($startTime == '' && $endTime) $startTime = date('Y-m-d H:i:s',strtotime($endTime)-30*86400);
		if($endTime == '' && $startTime) $endTime = date("Y-m-d 00:00:00",time()+86400);
		$region_id_rtrim = rtrim($regionid,'00');//去掉地区后面的00
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位

		$where = array();
		$where['left(tmediaowner.fregionid,'.$tregion_len.')'] = $region_id_rtrim;
		if($startTime && $endTime){
			$where['issue.fissuedate'] = array(array('egt',$startTime),array('lt',$endTime),'and');
		}
		$fadclasscode_arr = M('tadclass')->where(array('keynote' => 0))->field('fcode')->select();
		foreach ($fadclasscode_arr as $key => $value) {
			$fadclasscode[] = $value['fcode'];
		}
		$where['left(tad.fadclasscode,2)'] = array('in',$fadclasscode);
		//媒介机构电视
		$mediaowner_tv = M('ttvissue')->cache(true,6000)
								->alias('issue')
								->field('
										tmediaowner.fname as name,tmediaowner.fid as mediaowner_id,
										count(case when ttvsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,
										count(case when ttvsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,
										count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,
										count(issue.fmediaid) as issue_count,
										count(distinct issue.ftvsampleid) as sample_count,
										count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
								->join('tad on tad.fadid = ttvsample.fadid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmediaowner.fid')
								->order('issue_count desc')
								->limit(10)
								->select();
		//媒介机构广播
		$mediaowner_bc = M('tbcissue')->cache(true,6000)
							->alias('issue')
								->field('tmediaowner.fname as name,tmediaowner.fid as mediaowner_id,count(case when tbcsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tbcsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(distinct issue.fbcsampleid) as sample_count,count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
								->join('tad on tad.fadid = tbcsample.fadid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmediaowner.fid')
								->order('issue_count desc')
								->limit(10)
								->select();
		//媒介机构报纸
		$mediaowner_paper = M('tpaperissue')->cache(true,6000)
								->alias('issue')
								->field('tmediaowner.fname as name,tmediaowner.fid as mediaowner_id,count(case when tpapersample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tpapersample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(distinct issue.fpapersampleid) as sample_count,count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
								->join('tad on tad.fadid = tpapersample.fadid')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->where($where)
								->group('tmediaowner.fid')
								->order('issue_count desc')
								->limit(10)
								->select();
		//媒介机构
		$mediaowner_count = A('Gongshang/Supervise','Model')->sup_mediaowner_count($mediaowner_tv,$mediaowner_bc,$mediaowner_paper);
		//对数据进行从大到小的排序
		$mediaowner_sort_count = my_sort($mediaowner_count,'issue_count',SORT_DESC);

		//分类电视
		$tvclass = M('ttvissue')->cache(true,6000)
								->alias('issue')
								->field('substring_index(tadclass.ffullname,">",1) as name,left(tadclass.fcode,2) as class_code,count(case when ttvsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when ttvsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(distinct issue.ftvsampleid) as sample_count,count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
								->join('tad on tad.fadid = ttvsample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where($where)
								->group('left(tadclass.fcode,2)')
								->order('issue_count desc')
								->select();
		//分类广播
		$bcclass = M('tbcissue')->cache(true,6000)
								->alias('issue')
								->field('substring_index(tadclass.ffullname,">",1) as name,left(tadclass.fcode,2) as class_code,count(case when tbcsample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tbcsample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(distinct issue.fbcsampleid) as sample_count,count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
								->join('tad on tad.fadid = tbcsample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where($where)
								->group('left(tadclass.fcode,2)')
								->order('issue_count desc')
								->select();
		//分类报纸
		$paperclass = M('tpaperissue')->cache(true,6000)
								->alias('issue')
								->field('substring_index(tadclass.ffullname,">",1) as name,left(tadclass.fcode,2) as class_code,count(case when tpapersample.fillegaltypecode = 10 then 1 else null end) as slight_illegal,count(case when tpapersample.fillegaltypecode = 20 then 1 else null end) as general_illegal,count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_illegal,count(issue.fmediaid) as issue_count,count(distinct issue.fpapersampleid) as sample_count,count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
								->join('tad on tad.fadid = tpapersample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where($where)
								->group('left(tadclass.fcode,2)')
								->order('issue_count desc')
								->select();
		//分类总统计
		$class_count = A('Gongshang/Supervise','Model')->sup_class_count($tvclass,$bcclass,$paperclass);
		//对数据进行从大到小的排序
		$class_sort_count = my_sort($class_count,'issue_count',SORT_DESC);
		//统计分类违法的发布总数量
		$illegal_count = A('Gongshang/Supervise','Model')->illegal_class_count($class_count);

		//星期电视
		$tv_weeks = M('ttvissue')->cache(true,6000)
								->alias('issue')
								->field('DATE_FORMAT(issue.fissuedate,"%w") as weeks,count(issue.fmediaid) as issue_count,count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
								->join('tad on tad.fadid = ttvsample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where($where)
								->group('weeks')
								->order('weeks')
								->select();
        //星期广播
        $bc_weeks = M('tbcissue')->cache(true,6000)
								->alias('issue')
								->field('DATE_FORMAT(issue.fissuedate,"%w") as weeks,count(issue.fmediaid) as issue_count,count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
								->join('tad on tad.fadid = tbcsample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where($where)
								->group('weeks')
								->order('weeks')
								->select();
		//星期报纸
		$paper_weeks = M('tpaperissue')->cache(true,6000)
								->alias('issue')
								->field('DATE_FORMAT(issue.fissuedate,"%w") as weeks,count(issue.fmediaid) as issue_count,count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegal_count')
								->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
								->join('tad on tad.fadid = tpapersample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where($where)
								->group('weeks')
								->order('weeks')
								->select();
		//星期总统计
		$weeks_count = A('Gongshang/Supervise','Model')->sup_weeks_count($tv_weeks,$bc_weeks,$paper_weeks);
		$this->assign('mediaowner_sort_count',$mediaowner_sort_count);//已排序媒介机构
		$this->assign('class_sort_count',$class_sort_count);//已排序分类
		$this->assign('illegal_count',$illegal_count);//分类违法总数量
		$this->assign('weeks_count',$weeks_count);//星期
		$this->assign('regionid',session('regulatorpersonInfo.regionid'));//区域ID
		$this->display();

	}

}