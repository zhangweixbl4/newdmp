let illegalInfo = {
  name: 'illegalInfo',
  props: {
    task: {
      type: Object,
      default: () => ({})
    },
    type: {
      type: String,
      default: ''
    },
    fromManage: {
      type: Boolean,
      default: false
    },
    showtips: {
      type: Boolean,
      default: false
    },
    showButton: {
      type: Boolean,
      default: true
    },
    ringtype: {
      type: String,
      default: ''
    }
  },
  template: `<div class="illegalInfo" v-loading="loading">
    <el-form ref="illegalInfo" :model="task" :label-width="this.task.media_class == 13 ? '80px' : '140px'" :rules="rule"
      :disabled="type !== 'input' && ringtype !== 'review'">
      <el-form-item label="违法程度" prop="fillegaltypecode">
        <el-radio-group v-model="task.fillegaltypecode" @change="changeIllegalType">
          <el-radio :label="item.value" v-for="item in illegalTypeArr" :key="item.value">{{item.text}}</el-radio>
        </el-radio-group>
      </el-form-item>
      <el-form-item v-if="this.task.media_class == 13" label="网页截图" prop="uploadsnapshot">
          <el-upload
            auto-upload
            :multiple="false"
            :action="upOptions.up_server"
            :data="uploadOptions"
            :before-upload="_setKey"
            :on-success="_uploadSuccess"
            :on-progress="_uploadProgress"
            :show-file-list="false"
            :on-error="_uploadError">
          <el-button
            type="primary"
            @click="_getToken"
            :loading="uploading">点击上传</el-button>
            <div>{{imgurl.name}}</div>
        </el-upload>
      </el-form-item>
      <template v-if="isIllegal || !task.fillegaltypecode">
        <el-form-item label="违法表现" prop="fexpressioncodes" class="illegalExpression">
          <el-popover placement="left" width="800" trigger="click">
            <div>
              <el-input placeholder="输入关键字进行过滤" v-model="illegalKeyword">
              </el-input>
              <el-tree class="filter-tree" :data="illegalArr" node-key="fcode" accordion show-checkbox
                :filter-node-method="filterNode" @check-change="selectIllegal" ref="illegalSelecter">
              </el-tree>
            </div>
            <el-button slot="reference">点击选择</el-button>
          </el-popover>
          <el-card>
            <div v-for="(item,index) in task.checkedIllegalArr" :key="item.fcode" style="margin: 5px;" title="点击删除"
              @click="deleteIllegal(index)">
              <b>{{index+1}}.</b> {{item.fexpression}}
            </div>
          </el-card>
        </el-form-item>
      </template>
      <template v-if="isIllegal">
        <el-form-item label="违法内容" prop="fillegalcontent">
          <div ref="editor" v-if="task.media_class == 13 && type === 'input'"></div>
          <el-input v-if="task.media_class != 13" type="textarea" :rows="3" v-model="task.fillegalcontent">
          </el-input>
          <div v-html="task.fillegalcontent" v-if="task.media_class == 13 && type != 'input'" style="border: 1px solid #EBEEF5; overflow: auto;">
          </div>
        </el-form-item>
      </template>
      <el-form-item v-if="showtips">
          <p style="color: red;">截图必须上传完整网页截图，请每位用户勿必查看帮助说明</p>
          <a href="http://publichz.oss-cn-hangzhou.aliyuncs.com/%E4%BA%92%E8%81%94%E7%BD%91%E4%BC%97%E5%8C%85%E4%BB%BB%E5%8A%A1%E5%85%A8%E5%B1%8F%E6%88%AA%E5%9B%BE%E8%AF%B4%E6%98%8E.pdf" target="_blank">帮助说明</a>
      </el-form-item>
      <div class="text-center" v-if="!showMoreInfo && type === 'input'" style="cursor: pointer" @click="toggleShowMoreInfo">显示额外信息</div>
      <div v-else>
        <el-form-item>
          <a :href="'http://app2.sfda.gov.cn/datasearchp/all.do?tableName=TABLE25&formRender=cx&searchcx=&name=' + task.fadname"
            target="_blank">在食药监局数据库搜索该广告</a>
        </el-form-item>
        <el-form-item label="生产批准文号" :required="needRequire">
          <el-input v-model="task.fmanuno" @focus="() => $emit('focus')" @blur="() => $emit('blur')"></el-input>
        </el-form-item>
        <el-form-item label="广告中生产批准文号" :required="needRequire">
          <el-input v-model="task.fadmanuno" @focus="() => $emit('focus')" @blur="() => $emit('blur')"></el-input>
        </el-form-item>
        <el-form-item label="广告批准文号" :required="needRequire">
          <el-input v-model="task.fapprno" @focus="() => $emit('focus')" @blur="() => $emit('blur')"></el-input>
        </el-form-item>
        <el-form-item label="广告中广告批准文号" :required="needRequire">
          <el-input v-model="task.fadapprno" @focus="() => $emit('focus')" @blur="() => $emit('blur')"></el-input>
        </el-form-item>
        <el-form-item label="生产企业名称" :required="needRequire">
          <el-input v-model="task.fent" @focus="() => $emit('focus')" @blur="() => $emit('blur')"></el-input>
        </el-form-item>
        <el-form-item label="广告中生产企业名称" :required="needRequire">
          <el-input v-model="task.fadent" @focus="() => $emit('focus')" @blur="() => $emit('blur')"></el-input>
        </el-form-item>
        <el-form-item label="生产企业地区" :required="needRequire">
          <el-input v-model="task.fentzone" @focus="() => $emit('focus')" @blur="() => $emit('blur')"></el-input>
        </el-form-item>
        <div class="text-center" style="text-algin: center" v-if="showMoreInfo  && type === 'input'" style="cursor: pointer" @click="toggleShowMoreInfo">隐藏额外信息</div>
      </div>
      <br><br>
      <el-form-item v-if="type === 'input' && showButton">
        <el-button type="primary" @click="onSubmit">提 交</el-button>
        <el-button type="success" @click="changeLanguage" v-if="task.media_class !== '13'">转交方言</el-button>
      </el-form-item>
    </el-form>
    <el-dialog title="点击行即填充" :visible.sync="showFillDataList" width="80%" v-if="type === 'input'" append-to-body>
      <el-table :data="task.fillData" highlight-current-row @current-change="selectMoreInfoRow">
        <el-table-column property="fent" label="生产企业（证件持有人）名称"></el-table-column>
        <el-table-column property="fadent" label="广告中标识的生产企业（证件持有人）名称"></el-table-column>
        <el-table-column property="fmanuno" label="生产批准文号"></el-table-column>
        <el-table-column property="fadmanuno" label="广告中生产批准文号"></el-table-column>
        <el-table-column property="fapprno" label="广告批准文号"></el-table-column>
        <el-table-column property="fadapprno" label="广告中广告批准文号"></el-table-column>
        <el-table-column property="fentzone" label="生产企业（证件持有人）所在地区"></el-table-column>
      </el-table>
    </el-dialog>
  </div>`,
  data: function () {
    return {
      rule: {
        fillegaltypecode: [{
          required: true,
          message: '请选择违法程度',
          trigger: 'change'
        }],
        fillegalcontent: [{
          required: true,
          message: '请填写违法内容',
          trigger: 'change'
        }],
        fexpressioncodes: [{
          required: true,
          message: '请选择违法表现',
          trigger: 'change'
        }],
      },
      loading: false,
      illegalTypeArr,
      illegalArr,
      illegalKeyword: '',
      showMoreInfo: false,
      showFillDataList: false,
      fileList: [],
      uploading: false,
      uploadOptions: {},
      upOptions: {
        up_server: ""
      },
      imgurl: {}
    }
  },
  watch: {
    illegalKeyword(val) {
      this.$refs.illegalSelecter.filter(val);
    }
  },
  computed: {
    isInclude() {
      // 三品一械
      let type = (this.task.fadclasscode || '').substr(0, 2)
      return ['01', '02', '06'].includes(type)
    },
    needRequire() {
      if (this.isInclude && this.task.fillegaltypecode == '0') {
        return true
      }
      return false
    },
    isIllegal: function () {
      return this.task.fillegaltypecode !== '0'
    },
    adType: function () {
      const obj = {
        '1': 'tv',
        '2': 'bc',
        '3': 'paper',
        '13': 'net',
      }
      return obj[this.task.media_class] || null
    },
  },
  created() {
    if (this.isInclude) {
      this.showMoreInfo = true
    }
  },
  methods: {
    setLoading(loading) {
      this.loading = loading
    },
    changeLanguage() {
      this.$confirm('确认转交为方言处理?', '提示', {
        confirmButtonText: '继续',
        cancelButtonText: '取消',
        type: 'success',
      }).then(() => {
        $.post('/TaskInput/TraditionAd/setDialect', {
        taskid: this.task.taskid,
        linkType: 2,
      }).then((res) => {
        if(res.code == 0) {
          this.$emit('next')
          }
        }).catch(err => {
          this.$message.error(err.msg)
        })
      }).catch(err => {
      })
    },
    selectMoreInfoRow: function (row) {
      Object.assign(this.task, row)
    },
    filterNode(value, data) {
      if (!value) return true;
      return data.label.indexOf(value) !== -1;
    },
    selectIllegal: function (data, isChecked) {
      if (data.fpcode === '') {
        return false
      }
      this.task.checkedIllegalArr = this.task.checkedIllegalArr || []
      var index = this.task.checkedIllegalArr.indexOf(data);
      if (isChecked && index === -1 && data.fpcode !== '') {
        this.task.checkedIllegalArr.push(data)
      }
      if (!isChecked && index !== -1) {
        this.task.checkedIllegalArr.splice(index, 1)
      }
      this.changeIllegal()
      if (this.task.checkedIllegalArr.length > 0) {
        var maxCode = 0
        this.task.checkedIllegalArr.forEach((v) => {
          if (v.fillegaltype > maxCode) {
            maxCode = v.fillegaltype
          }
        })
        Vue.set(this.task, 'fillegaltypecode', maxCode)
      }
      this.$forceUpdate()
    },
    deleteIllegal: function (index) {
      this.task.checkedIllegalArr.splice(index, 1);
      this.$refs.illegalSelecter.setCheckedNodes(this.task.checkedIllegalArr);
      this.changeIllegal()
    },
    changeIllegal: function () {
      if (this.task.checkedIllegalArr.length === 0) {
        Vue.set(this.task, 'fexpressioncodes', '')
      }
      var arr = this.task.checkedIllegalArr.map((v) => {
        return v.fcode;
      });
      var str = arr.join(';');
      Vue.set(this.task, 'fexpressioncodes', str)
    },
    toggleShowMoreInfo: function () {
      if (!this.showMoreInfo) {
        this.loading = true
        const typeMap = {
          paper: 'get_paper_ad_more_field',
          tv: 'get_tv_ad_more_field',
          bc: 'get_bc_ad_more_field',
          net: 'get_net_ad_more_field',
        }
        $.post('/api/ad/' + typeMap[this.adType], {
          fadname: this.task.fadname
        }).then((res) => {
          if (res.code == 0) {
            this.task.fillData = res.moreFieldList
            if (this.task.fillData.length !== 0) {
              this.showFillDataList = true
            }
          }
          this.loading = false
        })
      }
      this.showMoreInfo = !this.showMoreInfo
    },
    changeIllegalType: function () {
      if (this.task.media_class == 13 && this.task.fillegaltypecode != 0) {
        this.$nextTick(() => {
          const editor = new wangEditor(this.$refs.editor)
          editor.customConfig.onchange = (html) => {
            if (html === '<p><br></p>') {
              this.task.fillegalcontent = ''
            } else {
              this.task.fillegalcontent = html
            }
          }
          editor.customConfig.uploadImgServer = this.task.fileUp.up_server
          editor.customConfig.uploadFileName = this.task.fileUp.file
          editor.customConfig.uploadImgParams = {
            token: this.task.fileUp.val.token
          }
          const vm = this
          editor.customConfig.uploadImgHooks = {
            before: function (xhr, editor, files) {
              vm.$message('图片上传中')
            },
            fail: function (xhr, editor, result) {
              vm.$message('图片上传失败')
            },
            error: function (xhr, editor) {
              vm.$message('图片上传失败')
            },
            customInsert: function (insertImg, result, editor) {
              vm.$message.success('图片上传成功')
              const url = vm.task.fileUp.bucket_url + result[vm.task.fileUp.key]
              editor.txt.append(`<a href="${url}" target="_blank" title="点击查看大图"><img src="${url}" alt=""></a>`)
              // insertImg(vm.task.fileUp.bucket_url + url)
            }
          }
          editor.customConfig.menus = []
          editor.customConfig.zIndex = 100
          editor.create()
          editor.txt.html(this.task.fillegalcontent)
          editor.$textElem.attr('contenteditable', this.type === 'input')

        })
      }
    },
    initData: function () {
      if (this.loading) {
        this.$message.warning('正在提交中, 请不要重复提交')
        return Promise.reject()
      }
      if(this.adType == 'net' && this.isIllegal && !this.imgurl.url){
        this.$message.error('请上传图片！')
        return Promise.reject()
      }
      if (this.adType === 'paper' && !this.task.paperIsSure) {
        this.$message.warning('请确认版面无误后点击报纸上方查看广告按钮')
        return Promise.reject()
      }
      const { fentzone, fadent, fent, fadapprno, fapprno, fadmanuno, fmanuno } = this.task
      if (this.needRequire && (!fentzone || !fadent || !fent || !fadapprno || !fapprno || !fadmanuno || !fmanuno)) {
        this.$message.warning('* 为必填项，请填写完整后提交')
        return Promise.reject()
      }
      return new Promise((resolve, reject) => {
        this.$refs['illegalInfo'].validate((res) => {
          if (res) {
            const params = [
              'taskid',
              'fillegaltypecode',
              'fillegalcontent',
              'fexpressioncodes',
              'fmanuno',
              'fadmanuno',
              'fapprno',
              'fadapprno',
              'fent',
              'fadent' ,
              'media_class',
              'fentzone',
            ]
            const data = {
              source_path: this.imgurl.url || ''
            }
            params.forEach(i => {
              data[i] = this.task[i]
            })
            resolve(data)
          } else {
            this.$alert('有必填字段未填写')
          }
        })
      })
    },
    onSubmit: function () {
      this.initData().then(data => {
        let text = '确认提交?'
        this.$confirm(text).then(() => {
          this.loading = true
          $.post('submitJudgeTask', { data }).then((res) => {
            if (res.code == 0) {
              if (this.fromManage) {
                return this.$emit('close')
              }
              this.$emit('next')
              this.imgurl = {}
            } else {
              this.$message({
                message: res.msg,
                type: 'error'
              })
              this.$emit('error')
            }
            this.loading = false
          })
        }).catch(() => {})
      })
    },
    // 上传落地页截图相关
    handleRemove(file, fileList) {
      console.log(file, fileList);
    },
    handlePreview(file) {
      console.log(file);
    },
    handleExceed(files, fileList) {
      this.$message.warning(`当前限制选择 3 个文件，本次选择了 ${files.length} 个文件，共选择了 ${files.length + fileList.length} 个文件`);
    },
    beforeRemove(file, fileList) {
      return this.$confirm(`确定移除 ${ file.name }？`);
    },
    /**
     * 获取上传七牛服务器和token
     */
    _getToken() {
      $.post('/TaskInput/TraditionAd/get_file_up')
        .then(res => {
          if (res.code === 0) {
            this.upOptions = res.data
            this.uploadOptions = res.data.val
            this.bucket_url = res.data.bucket_url
          } else {
            this.$message.error(res.msg)
          }
        })
        .catch(() => {
          this.$message.error('网络错误')
        })
    },
    _setKey(file) {
      this.uploadOptions.key = `${file.uid}.${file.type.split('/').pop()}`
    },
    _uploadSuccess(response, file, fileList) {
      this.uploading = false
      this.imgurl = {
        url: this.bucket_url + response.key,
        name: file.name
      }
      this.$message.success('文件上传完成！')
    },
    _uploadError() {
      this.uploading = false
      this.$message.error('上传出错！')
      this.$emit('uploadError')
    },
    _uploadProgress() {
      this.uploading = true
      this.$emit('uploadProgress')
    },
  },
}