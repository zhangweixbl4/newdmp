<?php

namespace Common\Model;
use Think\Exception;
use Aliyun\Core\Config;
use Aliyun\Core\Exception\ClientException;
use Aliyun\Core\Profile\DefaultProfile;
use Aliyun\Core\DefaultAcsClient;
use Aliyun\Api\Dyvms\Request\V20170525\SingleCallByVoiceRequest;
use Aliyun\Api\Dyvms\Request\V20170525\SingleCallByTtsRequest;
use Aliyun\Api\Dyvms\Request\V20170525\IvrCallRequest;
use Aliyun\Api\Dyvms\Request\V20170525\MenuKeyMap;
use Aliyun\Api\Dyvms\Request\V20170525\ClickToDialRequest;
use Aliyun\Api\Dyvms\Request\V20170525\CancelCallRequest;
use Aliyun\Api\Dyvms\Request\V20170525\QueryCallDetailByCallIdRequest;



import('Vendor.aliyun-dyvms-php-sdk.api_sdk.vendor.autoload', '', '.php');//载入七牛sdk
Config::load();


class YuyinModel{
	static $acsClient = null;
		
		
		
		
		
		
		
	/*发送文本转语音通知*/	
	public function sendVoice($ShowNumber,$CalledNumber,$TtsCode,$sendValue){
        $request = new SingleCallByTtsRequest();//组装请求对象-具体描述见控制台-文档部分内容
        $request->setCalledShowNumber($ShowNumber);//必填-被叫显号
        $request->setCalledNumber($CalledNumber); //必填-被叫号码
        $request->setTtsCode($TtsCode); //必填-Tts模板Code
        $request->setTtsParam(json_encode($sendValue));//选填-Tts模板中的变量替换JSON,假如Tts模板中存在变量，则此处必填
        $request->setOutId("");//选填-外呼流水号
        $response = static::getAcsClient()->getAcsResponse($request); //hint 此处可能会抛出异常，注意catch
		$response = json_decode(json_encode($response),true);
		
		$log_data = array();
		$log_data['mobile_no'] = $CalledNumber;
		$log_data['template_no'] = $TtsCode;
		$log_data['send_value'] = json_encode($sendValue,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
		$log_data['send_time'] = date('Y-m-d H:i:s');
		$log_data['ret_message'] = $response['Message'];
		$log_data['requestid'] = $response['RequestId'];
		$log_data['ret_code'] = $response['Code'];
		$log_data['ret_bizid'] = $response['CallId'];
		M('sms_log')->add($log_data);
		return $response;
		
	}
	
	
	public static function clickToDial($ShowNumber,$CallerNumber,$CalledNumber) {
        
        $request = new ClickToDialRequest();//组装请求对象-具体描述见控制台-文档部分内容
        
        $request->setCallerShowNumber($ShowNumber);//必填-主叫显号
        
        $request->setCallerNumber($CallerNumber);//必填-主叫号码
        
        $request->setCalledShowNumber($ShowNumber);//必填-被叫显号
        
        $request->setCalledNumber($CalledNumber);//必填-被叫号码
       
        $response = static::getAcsClient()->getAcsResponse($request); //hint 此处可能会抛出异常，注意catch
        return $response;
    }
	
	
	
	
	
	
	
	
	
	public static function getAcsClient() {
       $product = "Dyvmsapi"; //产品名称:云通信流量服务API产品,开发者无需替换
        $domain = "dyvmsapi.aliyuncs.com";//产品域名,开发者无需替换
        $accessKeyId = C('AL_OSSID'); // AccessKeyId
        $accessKeySecret = C('AL_OSSKEY'); // AccessKeySecret
        $region = "cn-hangzhou";// 暂时不支持多Region
        $endPointName = "cn-hangzhou"; // 服务结点

        if(static::$acsClient == null) {
            $profile = DefaultProfile::getProfile($region, $accessKeyId, $accessKeySecret);//初始化acsClient,暂不支持region化
            DefaultProfile::addEndpoint($endPointName, $region, $product, $domain);// 增加服务结点
            static::$acsClient = new DefaultAcsClient($profile); // 初始化AcsClient用于发起请求
        }
        return static::$acsClient;
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}