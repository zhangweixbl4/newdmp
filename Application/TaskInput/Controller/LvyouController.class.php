<?php
namespace TaskInput\Controller;
use Think\Controller;
class LvyouController extends Controller {
	
	public function _initialize() {
		A('Common/AliyunLog','Model')->dmpLogs();//记录日志
	}
	
	/*列表页*/
    public function index(){
		session_write_close();
		$this->assign('platformList',['携程','飞猪','途牛','去哪儿','马蜂窝','穷游网',]);
		$this->display();
	}

	/*详情页*/
    public function details(){
		session_write_close();
		$regionList = M('tregion','',C('ZIZHI_DB'))->where(array('fpid'=>100000))->getField('fsimple_name',true);
		$destinationList = M('ttour_inspect_product','',C('ZIZHI_DB'))->group('fdestination')->getField('fdestination',true);
		#var_dump($destinationList);
		$this->assign('regionList',$regionList);
		$this->assign('trip_types',array('自由行','跟团游','其它'));
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->assign('destinationList',$destinationList);//
		$this->display();
	}
	
	/*修改详情接口*/
	public function edit(){
		session_write_close();
		$fid = I('get.fid');
		$editData = I('post.');
		
		
		$editData['wx_id'] = session('wx_id');
		$editData['visual_checked'] = 1;
		$editData['visual_check_time'] = date('Y-m-d H:i:s');

		
		$dataInfo = M('ttour_product_week','',C('ZIZHI_DB'))->where(array('fid'=>$fid))->find();
		
		if(($editData['max_price'] * 0.75) < $dataInfo['base_price']){ #低于参考价的75%
			$rr = A('Common/Cunzheng','Model')->cz('109',$dataInfo['fid'].'_'.$dataInfo['proname'],$dataInfo['download_url']);
			#var_dump($rr);
		}

		#var_dump($dataInfo);
		
		
		$editRet = M('ttour_product_week','',C('ZIZHI_DB'))->where(array('fid'=>$fid))->save($editData);
		
		
		
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>''));
	}
	
	
	/*详情接口*/
    public function data_details(){
		session_write_close();
		$fid = I('fid');
		
		$data_details = M('ttour_product_week','',C('ZIZHI_DB'))
											->field('
													`fid`,
													`platform`	,
													`destination`	,
													`destination_city`	,
													`download_url`	,
													`product_id`	,
													`proname`	,
													`score`	,
													`travelcount`	,
													`depart_province`	,
													`departurecityname`	,
													`image`	,
													`detail_image`	,
													`snapshot_img`	,
													`vendor_brand`	,
													`vendor_name`	,
													`bc_scanning`	,
													`days`	,
													`type`	,
													`min_price`	,
													`max_price`	,
													`self_pay_num`	,
													`shoping_num`	,
													`bad_comment_count`	,
													`comment_count`
														
														')
											->where(array('fid'=>$fid))
											->find();
		$this->ajaxReturn(array('code'=>0,'msg'=>'','data_details'=>$data_details));								
		
	}
	
	
	
	/*列表请求接口*/
	public function data_list(){
		session_write_close();
		$p = intval(I('p')); #页码
		if ($p <= 0) $p = 1;
		
		$pp = intval(I('pp')); #每页显示条数
		if ($pp <= 0) $pp = 1;
		if ($pp > 100) $pp = 100;
		
		
		$days = I('days'); #行程天数
		$depart_province = I('depart_province'); #出发省份
		$departurecityname = I('departurecityname'); #出发地
		$destination = I('destination'); #目的地
		$platform = I('platform'); #平台
		$proname = I('proname'); #产品名称
		$type = I('type'); #出游方式
		$is_low_price = strval(I('is_low_price')); #是否涉嫌低价，0不涉嫌，1涉嫌低价，2涉嫌不合理低价
		$visual_checked = strval(I('visual_checked')); #是否人工检查过，0没有，1检查过
		
		$week = I('week');
		if(!$week) $week = dayweek(time())['weeknum'];
		
		#var_dump($week);

		
		
		$where = array();
		$where['is_inspect'] = 1;
		$where['is_count'] = 1;
		if ($week) $where['week'] = $week;
		if ($days) $where['days'] = $days; #行程天数
		if ($depart_province) $where['depart_province'] = $depart_province; #出发省份
		if ($destination) $where['destination'] = array('like','%'.$destination.'%'); #旅游目的地 
		if ($platform) $where['platform'] = $platform; #旅游平台
		if ($proname) $where['proname'] = array('like','%'.$proname.'%'); #产品名称
		if ($is_low_price != '') $where['is_low_price'] = $is_low_price; #是否涉嫌低价，0不涉嫌，1涉嫌低价，2涉嫌不合理低价
		if ($visual_checked != '') $where['visual_checked'] = $visual_checked; #是否人工检查过，0没有，1检查过
		

		
		#var_dump($where);
		
		$dataCount = M('ttour_product_week','',C('ZIZHI_DB'))->cache(true,60)->where($where)->count();
		
		
		$dataList = M('ttour_product_week','',C('ZIZHI_DB'))
						->field('
									fid,
									
									departurecityname,
									depart_province,
									destination,
									days,
									type,
									proname,
									is_low_price,
									visual_checked,
									min_price,
									max_price,
									platform
									
									')
									
						->where($where)
						->page($p,$pp)
						->select();
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','dataList'=>$dataList,'dataCount'=>$dataCount));
		
		
		
	}
	
	/*批量获取检查状态*/
	public function get_visual_checked(){
		$fids = I('fids');
		$fids = explode(',',$fids);
		if (count($fids) > 0) {
			$visual_check_list = M('ttour_product_week','',C('ZIZHI_DB'))->field('fid,visual_checked')->where(array('fid'=>array('in',$fids)))->select();
		}else{
			$visual_check_list = array();
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','visual_check_list'=>$visual_check_list));
		
	}
	
	
	
	/*获取旅游投诉数据*/
	public function get_complaint(){
		set_time_limit(600);
		session_write_close();
		header("Content-type: text/html; charset=utf-8");
		$download_date = I('download_date');
		
		$url = 'http://47.93.225.121:8000/complaint/all/?source=all&page=1&page_size=200&download_date='.$download_date;
		
		while(strlen($url) > 50){

			$remoteData = http($url,array(),'GET',false,60);
			$remoteData = json_decode($remoteData,true);
			$url = $remoteData['pager']['next_page'];
			$complaints = $remoteData['complaints'];
			M('crawl_complaint_20190912','',C('ZIZHI_DB'))->addAll($complaints,array(),true);
			
		}
		
		
		echo 'SUCCESS';
		
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	

}