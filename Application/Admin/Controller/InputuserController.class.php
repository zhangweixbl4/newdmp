<?php
namespace Admin\Controller;
use Think\Controller;
class InputuserController extends BaseController {
    public function index(){
    	if(A('Admin/Authority','Model')->authority('200221') === 0){
			exit('没有权限');//验证是否有新增权限
		}
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$keyword = I('keyword');
		$subscribe = I('subscribe');
		$group_id = I('group_id');//分组id
		$hasGroup = I('hasGroup');//是否分组
		$is_leader = I('is_leader');//是否分组

		$where = array();
		if($keyword != '') $where['nickname|openid|mobile|mail|alias'] = array('like','%'.$keyword.'%');
		if($subscribe == '1' || $subscribe == '0') $where['subscribe'] = $subscribe;
		if($hasGroup == '-1'){
			$where['group_id'] = 0;
		}elseif(intval($group_id) > 0){
			$where['group_id'] = $group_id;
		}
        if($is_leader != ''){
            $where['is_leader'] = $is_leader;
        }


        $count      = M('ad_input_user')->where($where)->count();// 查询满足要求的总记录数
		$Page       = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		// var_dump(M('ad_input_user')->getField('wx_id',true));
		$userList = M('ad_input_user')->where($where)->order('wx_id desc')->limit($Page->firstRow.','.$Page->listRows)->select();
		$userList = list_k($userList,$p,$pp);//为列表加上序号
		$groupList = M('ad_input_user_group')->select();
		foreach ($groupList as $key => $value) {
			$group_name[$value['group_id']] = $value['group_name'];
		}
		$group_name[0] = '';

		$this->assign('page',$Page->show());// 赋值分页输出
		$this->assign('userList',$userList);
		$this->assign('group_name',$group_name);
		$this->assign('groupList',$groupList);
		$this->display();
	}

	public function ajax_group_list(){

		$group_name = I('keyword');

		$where = array();
		if($group_name) $where['group_name'] = array('like','%'.$group_name.'%');
		$groupList = M('ad_input_user_group')->where($where)->select();
		$this->ajaxReturn(array('code'=>0,'value'=>$groupList,'key'=>$group_name));

	}
	
	/*用户详情*/
	public function ajax_user_details(){
		$wx_id = I('wx_id');//用户ID
		$userInfo = M('ad_input_user')->where(array('wx_id'=>$wx_id))->find();
		$group_name = M('ad_input_user_group')->getField('group_id,group_name',true);
		if($userInfo['group_id']){
			$userInfo['group_name'] = $group_name[$userInfo['group_id']];
		}else{
			$userInfo['group_name'] = '';
		}	
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','userInfo'=>$userInfo,'group_name'=>$group_name));
	}
	
	/*用户资料修改*/
	public function add_edit_user_info(){
		if(A('Admin/Authority','Model')->authority('200222') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
		}
		$wx_id = I('wx_id');//用户ID
		
		$nickname = I('nickname');//昵称
		$input_seniority = I('input_seniority');//广告录入资质
		$audit_seniority = I('audit_seniority');//广告审查资质
		$cutting_seniority = I('cutting_seniority');//广告剪辑资质
		$mobile = I('mobile');//用户手机号
		$mail = I('mail');//用户邮箱
		$alias = I('alias');//别名
		$group_id = I('group_id');//分组ID
		$is_leader = I('is_leader');//分组ID

		$password = I('password');//用户密码
		$e_data = array();
		if ($input_seniority != '') $e_data['input_seniority'] = intval($input_seniority);
		if ($audit_seniority != '') $e_data['audit_seniority'] = intval($audit_seniority);
		if ($cutting_seniority != '') $e_data['cutting_seniority'] = intval($cutting_seniority);
		if ($password != '') $e_data['password'] = md5($password);
		$e_data['mobile'] = $mobile;
		$e_data['mail'] = $mail;
		$e_data['alias'] = $alias;
		$e_data['group_id'] = $group_id;
		$e_data['nickname'] = $nickname;
		$e_data['is_leader'] = $is_leader;

		if($wx_id){//修改
			$rr = M('ad_input_user')->where(array('wx_id'=>$wx_id))->save($e_data);
			if($rr){
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败'));
			}
		}else{//添加
			if($password == '') $e_data['password'] = md5('123456');
			$rr = M('ad_input_user')->add($e_data);
			if($rr){
				$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败'));
			}
		}
		
	}
	
	/*众包工作量统计*/
	public function OLD_user_task_count(){
		if(A('Admin/Authority','Model')->authority('200211') === 0){
			exit('没有权限');//验证是否有新增权限
		}
		$p = I('p',1);//当前第几页
		$pp = 100;//每页显示多少记录
		
		$keyword = I('keyword');//关键字
		$group_name = I('group_name');//分组名称
		
		
		
		$date_s = I('date_s');
		if($date_s == ''){
			$date_s = date('Y-m-d',time() - 86400*7);
			$_GET['date_s'] = $date_s;
		}	
		$date_e = I('date_e');
		if($date_e == ''){
			$date_e = date('Y-m-d',time());
			$_GET['date_e'] = $date_e;
		}
		$where = array();
		if($group_name != ''){
			$where['group_name'] = array('like','%'.$group_name.'%');
		}
		
		if($keyword != ''){
			$where['ad_input_user.nickname|ad_input_user.mobile|ad_input_user.alias'] = array('like','%'.$keyword.'%');
		}
		$where['user_task_count.count_date'] = array('between',array($date_s,$date_e));
		
		
		$count      = M('user_task_count')
									->join('ad_input_user on ad_input_user.wx_id = user_task_count.user_id')
									->join('ad_input_user_group on ad_input_user_group.group_id = ad_input_user.group_id')
									->where($where)

									->count('distinct(user_task_count.user_id)');// 查询满足要求的总记录数
									
						
		$Page       = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		
		
		
		
		$countAll = M('user_task_count')
									->field('
											-1 as user_id,
											"合计" as nickname,
											
											sum(paper_page_cut) as paper_page_cut,
											sum(paper_ad_cut) as paper_ad_cut,
											sum(paper_input) as paper_input,
											sum(paper_inspect) as paper_inspect,
											sum(bc_input) as bc_input,
											sum(bc_inspect) as bc_inspect,
											sum(tv_input) as tv_input,
											sum(tv_inspect) as tv_inspect,
											sum(net_input) as net_input,
											sum(net_inspect) as net_inspect,
											sum(input_err) as input_err,
											
											
											sum(paper_page_up) as paper_page_up,
											sum(sub_paper_cut_err) as sub_paper_cut_err,
											sum(paper_cut_err) as paper_cut_err
											
											
											')
									->join('ad_input_user on ad_input_user.wx_id = user_task_count.user_id')
									->join('ad_input_user_group on ad_input_user_group.group_id = ad_input_user.group_id')
									->where($where)
									->select();//查询合计数量
		
		$countArrList = M('user_task_count')
									->field('
											user_task_count.user_id,
											ad_input_user.nickname,
											ad_input_user.alias,
											ad_input_user_group.group_name,
											sum(paper_page_cut) as paper_page_cut,
											sum(paper_ad_cut) as paper_ad_cut,
											sum(paper_input) as paper_input,
											sum(paper_inspect) as paper_inspect,
											sum(bc_input) as bc_input,
											sum(bc_inspect) as bc_inspect,
											sum(tv_input) as tv_input,
											sum(tv_inspect) as tv_inspect,
											sum(net_input) as net_input,
											sum(net_inspect) as net_inspect,
											sum(input_err) as input_err,
											
											
											sum(paper_page_up) as paper_page_up,
											sum(sub_paper_cut_err) as sub_paper_cut_err,
											sum(paper_cut_err) as paper_cut_err,
											
											sum(paper_page_cut) + sum(paper_ad_cut) + sum(paper_input) + sum(paper_inspect) + 
											sum(bc_input) + sum(bc_inspect) + sum(tv_input) + sum(tv_inspect) + 
											sum(paper_page_up) + sum(sub_paper_cut_err) + sum(paper_cut_err)  as all_count
											
											')
									->join('ad_input_user on ad_input_user.wx_id = user_task_count.user_id')
									->join('ad_input_user_group on ad_input_user_group.group_id = ad_input_user.group_id')
									->where($where)
									->group('user_task_count.user_id')
									->order('all_count desc')
									->limit($Page->firstRow.','.$Page->listRows)
									->select();//查询每个人的数量
									
									
									
		$countArrList = list_k($countArrList,$p,$pp);//为列表加上序号
		//var_dump($countArrList);
		$this->assign('page',$Page->show());// 赋值分页输出							
									
		$this->assign('countAll',$countAll);
		$this->assign('countArrList',$countArrList);
		
		$this->assign('date_interval',array('s'=>$date_s,'e'=>$date_e));							
		
		$this->display();
	}

    /**
     * 任务量统计
     */
    public function user_task_count()
    {
        if(A('Admin/Authority','Model')->authority('200211') === 0){
            exit('没有权限');//验证是否有新增权限
        }
        if (IS_POST){
            $countWhere = [];
            $page = I('page', 1);
            $where = I('where');
            // 处理时间条件, 默认是当天
            $timeRange = $where['timeRange'];
            $startTime = $timeRange[0];
            $endTime = $timeRange[1];
            if ($startTime == '' && $endTime == ''){
                $startTime = date('Y-m-d');
                $endTime = $startTime;
            }
            if($startTime != '' && $endTime != ''){
                $countWhere['count_date'] = ['BETWEEN', [$startTime, $endTime]];
            }
            $startTimeStamp = strtotime($startTime);
            $endTimeStamp = strtotime('+1 day', $startTimeStamp) -1;
            $userWhere = [];
            // 用户分组表的子查询
            if ($where['group_name'] != ''){
                $groupSubQuery = M('ad_input_user_group')->field('group_id')->where([
                    'group_name' => ['LIKE' , '%' .$where['group_name'].'%']
                ])->buildSql();
                $userWhere['group_id'] = ['EXP', 'IN ' . $groupSubQuery];
            }
            // 用户表的子查询
            if ($where['alias'] != '' || $userWhere != []){
                $userWhere['alias|nickname'] = ['LIKE' , '%' .$where['alias'].'%'];
                $userSubQuery = M('ad_input_user')->field('wx_id')->where($userWhere)->buildSql();
                $countWhere['user_id'] = ['EXP', 'IN ' . $userSubQuery];
            }
            // (select sum(score) from task_input_score where wx_id = user_task_count.user_id and update_time between $startTimeStamp and $endTimeStamp) as score,//统计积分字段暂删除优化速度
            $data = M('user_task_count')
                ->field("
                user_id,
                sum(v3_paper_task) v3_paper_task, 
                sum(v3_bc_task) v3_bc_task, 
                sum(v3_tv_task) v3_tv_task, 
                sum(v3_net_task) v3_net_task, 
                sum(v3_sub_cut_err) v3_sub_cut_err, 
                sum(v3_net_cut_err) v3_net_cut_err, 
                sum(v3_task_back) v3_task_back, 
                sum(v3_quality) v3_quality, 
                sum(v3_ad_sure) v3_ad_sure, 
                sum(paper_page_cut) paper_page_cut, 
                sum(paper_ad_cut) paper_ad_cut, 
                sum(paper_input) paper_input, 
                sum(paper_inspect) paper_inspect, 
                sum(bc_input) bc_input, 
                sum(bc_inspect) bc_inspect, 
                sum(tv_input) tv_input, 
                sum(tv_inspect) tv_inspect, 
                sum(paper_page_up) paper_page_up, 
                sum(sub_paper_cut_err) sub_paper_cut_err, 
                sum(paper_cut_err) paper_cut_err, 
                sum(net_input) net_input, 
                sum(net_inspect) net_inspect, 
                sum(input_err) input_err, 
                sum(inspect_err) inspect_err
                ")
                ->where($countWhere)
                ->group('user_id')
                ->page($page, 100)
                ->select();
            $sql = M('user_task_count')->getLastSql();
            $total = M('user_task_count')
                ->field("
                sum(v3_paper_task) v3_paper_task, 
                sum(v3_bc_task) v3_bc_task, 
                sum(v3_tv_task) v3_tv_task, 
                sum(v3_net_task) v3_net_task, 
                sum(v3_sub_cut_err) v3_sub_cut_err, 
                sum(v3_net_cut_err) v3_net_cut_err, 
                sum(v3_task_back) v3_task_back, 
                sum(v3_quality) v3_quality, 
                sum(v3_ad_sure) v3_ad_sure, 
                sum(paper_page_cut) paper_page_cut, 
                sum(paper_ad_cut) paper_ad_cut, 
                sum(paper_input) paper_input, 
                sum(paper_inspect) paper_inspect, 
                sum(bc_input) bc_input, 
                sum(bc_inspect) bc_inspect, 
                sum(tv_input) tv_input, 
                sum(tv_inspect) tv_inspect, 
                sum(paper_page_up) paper_page_up, 
                sum(sub_paper_cut_err) sub_paper_cut_err, 
                sum(paper_cut_err) paper_cut_err, 
                sum(net_input) net_input, 
                sum(net_inspect) net_inspect, 
                sum(input_err) input_err, 
                sum(inspect_err) inspect_err
                ")
                ->where($countWhere)
                ->find();
            $count = M('user_task_count')->field('count_id')->where($countWhere)->group('user_id')->buildSql();
            $count = M()->table($count . ' a')->count();
            foreach ($data as $key => $value) {
                $userInfo = M('ad_input_user')->cache(true, 60)->field('nickname, group_id, alias')->where(['wx_id' => ['EQ', $value['user_id']]])->find();
                $data[$key]['alias'] = $userInfo['alias'];
                $data[$key]['nickname'] = $userInfo['nickname'];
                $data[$key]['group_name'] = M('ad_input_user_group')->cache(true, 60)->where(['group_id' => ['EQ', $userInfo['group_id']]])->getField('group_name');
            }
            $this->ajaxReturn([
                'data' => $data,
                'count' => $count,
                'total' => $total
            ]);
        }else{
            $this->display();
        }
    }

    /**
     * 导出到Excel
     */
    public function exportTaskCountByExcel()
    {
        $startTime = I('startTime');
        $endTime =  I('endTime');
        if ($startTime == '' && $endTime == ''){
            $startTime = date('Y-m-d');
            $endTime = $startTime;
        }
        $startTimeStamp = strtotime($startTime);
        $endTimeStamp = strtotime('+1 day', $startTimeStamp) -1;
        $where['count_date'] = ['BETWEEN', [$startTime, $endTime]];
        vendor('PHPExcel', '', '.class.php');
        $objExcel = new \PHPExcel();
        $data = M('user_task_count')
            ->field("
                user_id,
                sum(v3_paper_task) v3_paper_task,
                sum(v3_bc_task) v3_bc_task,
                sum(v3_tv_task) v3_tv_task,
                sum(v3_net_task) v3_net_task,
                sum(v3_sub_cut_err) v3_sub_cut_err,
                sum(v3_net_cut_err) v3_net_cut_err,
                sum(v3_task_back) v3_task_back,
                sum(v3_quality) v3_quality,
                sum(v3_ad_sure) v3_ad_sure,
                sum(paper_page_cut) paper_page_cut,
                sum(paper_ad_cut) paper_ad_cut,
                sum(paper_input) paper_input,
                sum(paper_inspect) paper_inspect,
                sum(bc_input) bc_input,
                sum(bc_inspect) bc_inspect,
                sum(tv_input) tv_input,
                sum(tv_inspect) tv_inspect,
                sum(paper_page_up) paper_page_up,
                sum(sub_paper_cut_err) sub_paper_cut_err,
                sum(paper_cut_err) paper_cut_err,
                sum(net_input) net_input,
                sum(net_inspect) net_inspect,
                sum(input_err) input_err,
                sum(inspect_err) inspect_err
                ")
            ->where($where)
            ->group('user_id')
            ->select();
        foreach ($data as $key => $value) {
            $userInfo = M('ad_input_user')->field('nickname, alias, group_id')->cache(true, 60)->where(['wx_id' => ['EQ', $value['user_id']]])->find();
            $data[$key]['nickname'] = $userInfo['nickname'];
            $data[$key]['alias'] = $userInfo['alias'];
            $data[$key]['group_name'] = M('ad_input_user_group')->where(['group_id' => ['EQ', $userInfo['group_id']]])->getField('group_name');
        }

        $letterArr = range('A', 'Z');
        $titleArr = [
            '姓名',
            '分组',
            '积分',
            '报纸任务量',
            '广播任务量',
            '电视任务量',
            '网络任务量',
            '报纸录入', '报纸审核',
            '广播录入', '广播审核',
            '电视录入', '电视审核',
            '网络录入', '网络审核',
            '录入错误', '审核错误',
            '剪辑错误提交', '网络抓取错误提交',
        ];
        $sheet = $objExcel->getActiveSheet();
        // 表头
        $sheet->mergeCells("$letterArr[0]1:$letterArr[0]3");
        $sheet->setCellValue("$letterArr[0]1" , '姓名');
        $sheet->mergeCells("$letterArr[1]1:$letterArr[1]3");
        $sheet->setCellValue("$letterArr[1]1" , '积分');
        $sheet->mergeCells("$letterArr[2]1:$letterArr[2]3");
        $sheet->setCellValue("$letterArr[2]1" , '广告确认');
        $sheet->mergeCells("$letterArr[3]1:$letterArr[14]1");
        $sheet->setCellValue("$letterArr[3]1" , '新系统');
        $sheet->mergeCells("$letterArr[3]2:$letterArr[4]2");
        $sheet->setCellValue("$letterArr[3]2" , '报纸任务');
        $sheet->setCellValue("$letterArr[3]3" , '录入量');
        $sheet->setCellValue("$letterArr[4]3" , '审核量');
        $sheet->mergeCells("$letterArr[5]2:$letterArr[6]2");
        $sheet->setCellValue("$letterArr[5]2" , '广播任务');
        $sheet->setCellValue("$letterArr[5]3" , '录入量');
        $sheet->setCellValue("$letterArr[6]3" , '审核量');
        $sheet->mergeCells("$letterArr[7]2:$letterArr[8]2");
        $sheet->setCellValue("$letterArr[7]2" , '电视任务');
        $sheet->setCellValue("$letterArr[7]3" , '录入量');
        $sheet->setCellValue("$letterArr[8]3" , '审核量');
        $sheet->mergeCells("$letterArr[9]2:$letterArr[10]2");
        $sheet->setCellValue("$letterArr[9]2" , '网络任务');
        $sheet->setCellValue("$letterArr[9]3" , '录入量');
        $sheet->setCellValue("$letterArr[10]3" , '审核量');
        $sheet->mergeCells("$letterArr[11]2:$letterArr[11]3");
        $sheet->setCellValue("$letterArr[11]2" , '提交剪辑错误');
        $sheet->mergeCells("$letterArr[12]2:$letterArr[12]3");
        $sheet->setCellValue("$letterArr[12]2" , '提交网络抓取错误');
        $sheet->mergeCells("$letterArr[13]2:$letterArr[13]3");
        $sheet->setCellValue("$letterArr[13]2" , '被退回任务');
        $sheet->mergeCells("$letterArr[14]2:$letterArr[14]3");
        $sheet->setCellValue("$letterArr[14]2" , '质检任务');
        $sheet->mergeCells("$letterArr[15]1:$letterArr[0]$letterArr[1]1");
        $sheet->setCellValue("$letterArr[15]1" , '旧系统');
        $sheet->mergeCells("$letterArr[15]2:$letterArr[15]3");
        $sheet->setCellValue("$letterArr[15]2" , '录入错误');
        $sheet->mergeCells("$letterArr[16]2:$letterArr[17]2");
        $sheet->setCellValue("$letterArr[16]2" , '电视');
        $sheet->setCellValue("$letterArr[16]3" , '录入量');
        $sheet->setCellValue("$letterArr[17]3" , '审核量');
        $sheet->mergeCells("$letterArr[18]2:$letterArr[19]2");
        $sheet->setCellValue("$letterArr[18]2" , '广播');
        $sheet->setCellValue("$letterArr[18]3" , '录入量');
        $sheet->setCellValue("$letterArr[19]3" , '审核量');
        $sheet->mergeCells("$letterArr[20]2:$letterArr[21]2");
        $sheet->setCellValue("$letterArr[20]2" , '网络');
        $sheet->setCellValue("$letterArr[20]3" , '录入量');
        $sheet->setCellValue("$letterArr[21]3" , '审核量');
        $sheet->mergeCells("$letterArr[22]2:$letterArr[0]$letterArr[2]2");
        $sheet->setCellValue("$letterArr[22]2" , '报纸');
        $sheet->setCellValue("$letterArr[22]3" , '录入量');
        $sheet->setCellValue("$letterArr[23]3" , '审核量');
        $sheet->setCellValue("$letterArr[24]3" , '剪辑版面');
        $sheet->setCellValue("$letterArr[25]3" , '剪辑广告');
        $sheet->setCellValue("$letterArr[0]$letterArr[0]3" , '上传');
        $sheet->setCellValue("$letterArr[0]$letterArr[1]3" , '提交错误');
        $sheet->setCellValue("$letterArr[0]$letterArr[2]3" , '剪辑错误');
        $sheet->mergeCells("$letterArr[0]$letterArr[3]1:$letterArr[0]$letterArr[3]3");
        $sheet->setCellValue("$letterArr[0]$letterArr[3]1" , '分组');
        // 填充数据
        $fieldArr = [
            'alias', 'score', 'v3_ad_sure',
            'v3_paper_task', 'v3_paper_task',
            'v3_bc_task', 'v3_bc_task',
            'v3_tv_task', 'v3_tv_task',
            'v3_net_task', 'v3_net_task',
            'v3_sub_cut_err', 'v3_net_cut_err', 'v3_task_back', 'v3_quality',
            'input_err',
            'tv_input', 'tv_inspect',
            'bc_input', 'bc_inspect',
            'net_input', 'net_inspect',
            'paper_input', 'paper_inspect',
            'paper_page_cut', 'paper_ad_cut', 'paper_page_up', 'sub_paper_cut_err', 'paper_cut_err',
            'group_name',
        ];
        foreach ($data as $key1 => $value1) {
            foreach ($fieldArr as $key2 => $value2) {
                if ($key2 > 25){
                    $sheet->setCellValue($letterArr[0] . $letterArr[$key2 - 26] . ($key1 + 4) , $value1[$value2]);
                }else{
                    $sheet->setCellValue($letterArr[$key2] . ($key1 + 4) , $value1[$value2]);
                }
            }
        }
        $fileName = $startTime. '至' . $endTime;
        header('pragma:public');
        header('Content-type:application/vnd.ms-excel;charset=utf-8;name="'.$fileName.'.xls"');
        header("Content-Disposition:attachment;filename=$fileName.xls");//attachment新
        $objWriter = \PHPExcel_IOFactory::createWriter($objExcel, 'Excel5');
        $objWriter->save('php://output');

    }
	
	/*众包分组管理*/
	public function userGroup(){
		$userGroupList0 = M('ad_input_user_group')->select();
		foreach($userGroupList0 as $userGroup0){
			$userGroup0['count_users'] = M('ad_input_user')->cache(true,60)->where(array('group_id'=>$userGroup0['group_id']))->count(); 
			$userGroupList[] = $userGroup0;
		}
		$mediaclassList = M('tmediaclass')->cache(true,600)->field('fid,fclass')->where(array('fpid'=>''))->select();
		$this->assign('mediaclassList',$mediaclassList);//媒介类型列表
		$this->assign('userGroupList',$userGroupList);
		$this->display();
	}
	
	
	/*添加、编辑分组*/
	public function add_edit_user_group(){
		//echo 0;
		$group_id = intval(I('group_id'));//分组id
		$mediaAuthority = explode(',',I('mediaAuthority'));//媒介权限
		$classAuthority = I('classAuthority');//广告分类权限
		$dateAuthority = I('dateAuthority');//日期权限
		$regionAuthority = I('regionAuthority');//地区权限
		$group_name = I('group_name');//分组名称
		$media_unrestricted = I('media_unrestricted');//媒介无限制
		$class_unrestricted = I('class_unrestricted');//分类无限制
		$date_unrestricted  = I('date_unrestricted'); //日期无限制
		$region_unrestricted  = I('region_unrestricted'); //日期无限制
		$other_authority= I('post.other_authority');//其他权限
		$other_authority_str = implode(',', $other_authority);


		$a_e_data = array();
		$a_e_data['mediaauthority'] = $media_unrestricted?'unrestricted':json_encode($mediaAuthority);
		$a_e_data['classauthority'] = $class_unrestricted?'unrestricted':json_encode($classAuthority);
		$a_e_data['dateauthority'] = $date_unrestricted?'unrestricted':json_encode($dateAuthority);
		$a_e_data['regionauthority'] = $region_unrestricted?'unrestricted':json_encode($regionAuthority);
		$a_e_data['group_name'] = $group_name;
		$a_e_data['other_authority'] = $other_authority_str;
		
		//var_dump($a_e_data);
		if($group_name == ''){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'分组名称不能为空'));
		}
		
		
		
		
		
		if($group_id === 0){//新增分组
			if(M('ad_input_user_group')->where(array('group_name'=>$group_name))->count() > 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'分组名称重复'));
			}
		
		
			$a_e_data['fcreator'] = session('personInfo.fid').'_'.session('personInfo.fname');//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$group_id = M('ad_input_user_group')->add($a_e_data);//新增数据
			
			if($group_id){
				$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败'));
			}
			
		}else{//修改分组
			if(M('ad_input_user_group')->where(array('group_name'=>$group_name,'group_id'=>array('neq',$group_id)))->count() > 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'分组名称重复'));
			}
			$a_e_data['fcreator'] = session('personInfo.fid').'_'.session('personInfo.fname');//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$save_state = M('ad_input_user_group')->where(array('group_id'=>$group_id))->save($a_e_data);//保存数据
			if($save_state){
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败'));
			}
		}
		
		
		
		
		
	}
	
	
	/*删除分组*/
	public function del_user_group(){
		$group_id = intval(I('group_id'));//分组id
		if($group_id === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'group_id错误'));
		}
		$del_state = M('ad_input_user_group')->where(array('group_id'=>$group_id))->delete();
		
		if($del_state){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败,原因未知'));
		}
		
	}
	
	/*分组详情*/
	public function user_group_details(){
		$group_id = intval(I('group_id'));//分组id
		$userGroupInfo = M('ad_input_user_group')->field('group_id,group_name,mediaauthority,classauthority,dateauthority, regionauthority,other_authority')->where(array('group_id'=>$group_id))->find();
		if($userGroupInfo['mediaauthority'] == 'unrestricted'){
			$userGroupInfo['mediaauthority'] = 1;
		}else{
			$mediaauthority = array();
			if(is_array(json_decode($userGroupInfo['mediaauthority'],true))){//判断是否有关联的媒介
				$mediaauthority = M('tmedia')->field('fid as media_id,fmedianame')->where(array('fid'=>array('in',json_decode($userGroupInfo['mediaauthority'],true))))->select();
			}
			$userGroupInfo['mediaauthority'] = $mediaauthority;
		}		
		
		if($userGroupInfo['classauthority'] == 'unrestricted'){
			$userGroupInfo['classauthority'] = 1;
		}else{
			$classauthority = array();
			if(is_array(json_decode($userGroupInfo['classauthority'],true))){//判断是否有关联的媒介
				$classauthority = M('tadclass')->field('fcode as adclass_code,ffullname as adclass_fullname')->where(array('fcode'=>array('in',json_decode($userGroupInfo['classauthority'],true))))->select();
			}
			$userGroupInfo['classauthority'] = $classauthority;
		}
		
		if($userGroupInfo['dateauthority'] == 'unrestricted'){
			$userGroupInfo['dateauthority'] = 1;
		}else{
			$userGroupInfo['dateauthority'] = json_decode($userGroupInfo['dateauthority'],true);
		}

        if($userGroupInfo['regionauthority'] == 'unrestricted'){
            $userGroupInfo['regionauthority'] = 1;
        }elseif (!$userGroupInfo['regionauthority']){

        }else{
		    $ids = json_decode($userGroupInfo['regionauthority'],true);
		    // 分2次查, 一次查全部名称的,也就是本级的, 一次查fid不满6位的,也就是包含的
            $fullIds = [];
            foreach ($ids as $key => $value) {
                $len = strlen($value);
                if ($len == 6){
                    $fullIds[] = $value;
                }else{
                    $ids[$key] .= str_repeat('0', 6 - $len);
                }
            }
            $res = M('tregion')->field('fid, ffullname')->where(['fid' => ['IN', $ids]])->select();
            foreach ($res as $key => $value) {
                $res[$key]['data'] = $value['fid'];
                if (in_array($value['fid'], $fullIds)){
                    $res[$key]['ffullname'] .= '(本级)';
                }else{
                    if (substr($value['fid'], 4, 2) == '00'){
                        $res[$key]['data'] = substr($value['fid'], 0, 4);
                        if (substr($value['fid'], 2, 2) == '00'){
                            $res[$key]['data'] = substr($value['fid'], 0, 2);
                        }
                    }
                }
            }
            $userGroupInfo['regionauthority'] = $res;
        }

		$this->ajaxReturn(array('code'=>0,'userGroupInfo'=>$userGroupInfo));
		
	}

	//编辑人员
	public function add_edit_user(){

		$wx_id = I('wx_id');//微信ID
		$group_id = I('group_id');//分组ID

		//分组ID先归零后添加
		M('ad_input_user')->where(array('group_id' => $group_id))->save(array('group_id' => 0));
		$where = array();
		if($wx_id){
		 	$where['wx_id'] = array('in',$wx_id);		
		 	$result = M('ad_input_user')->where($where)->save(array('group_id' => $group_id));
		 	if(!$result) $this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败'));
		}	
		$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));	

	}

	//分组人员列表
	public function ajax_group_user(){

		$group_id = I('group_id');//分组ID
		if(!$group_id) $this->ajaxReturn(array('code'=>-1,'msg'=>'无分组ID'));

		$where['group_id'] = $group_id;
		$groupUser = M('ad_input_user')->where($where)->select();

		$this->ajaxReturn(array('code'=>0,'groupUser'=>$groupUser));

	}

	//人员列表
	public function ajax_user_list(){

		$keyword = I('keyword');//关键字搜索

		$where = array();
		//昵称 别名 手机号条件查询
		if($keyword) $where['nickname|alias|mobile'] = array('like','%'.$keyword.'%');
		$userList = M('ad_input_user')->where($where)->limit(10)->select();

		$this->ajaxReturn(array('code'=>0,'value'=>$userList));

	}

	//验证人员是否已经关联分组
	public function ajax_is_group(){

		$wx_id = I('wx_id');

		$group_id = M('ad_input_user')->where(array('wx_id' => $wx_id))->getField('group_id');

		if(intval($group_id) > 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该人员已关联分组'));
		}

	}

    public function userList()
    {
        if (IS_POST){
            $page = I('page', 10);
            $pageSize = I('pageSize', 10);
            $where = [
                'state' => ['EGT',0]
            ];
            $formData = I('where');
            if ($formData['keyword'] != ''){
                $where['nickname|alias|mobile|mail'] = ['LIKE', "%{$formData['keyword']}%"];
            }
            if ($formData['group_id'] != '' && $formData['group_id'] != '0'){
                $groupSubQuery = M('ad_input_user_group')
                    ->field('group_id')
                    ->where([
                        'group_id' => ['EQ', $formData['group_id']]
                    ])
                    ->buildSql();
                $where['group_id'] = ['EXP', 'IN '.$groupSubQuery];
            }
            if ($formData['group_id'] == '0'){
                $where['group_id'] = ['EQ', '0'];
            }
            if ($formData['is_leader'] != ''){
                $where['is_leader'] = ['EQ', $formData['is_leader']];
            }
            if ($formData['user_tag'] != ''){
                $where['_string'] = "(";
                foreach ($formData['user_tag'] as $formDatum) {
                    $where['_string'] .= "FIND_IN_SET('$formDatum', user_tag) AND ";
                }
                $where['_string'] = rtrim($where['_string'], ' AND ');
                $where['_string'] .= ')';
            }
            $table = M('ad_input_user')
                ->where($where)
                ->page($page, $pageSize)
                ->order('wx_id desc')
                ->select();
            $total = M('ad_input_user')
                ->where($where)
                ->count();
            foreach ($table as $key => $value) {
                $table[$key]['group_name'] = M('ad_input_user_group')->cache(true, 60)->where(['group_id' => ['EQ', $value['group_id']]])->getField('group_name');
                if ($formData['keyword'] != ''){
                    $table[$key]['nickname'] = r_highlight($table[$key]['nickname'], $formData['keyword'], 'red');
                    $table[$key]['alias'] = r_highlight($table[$key]['alias'], $formData['keyword'], 'red');
                    $table[$key]['mobile'] = r_highlight($table[$key]['mobile'], $formData['keyword'], 'red');
                    $table[$key]['mail'] = r_highlight($table[$key]['mail'], $formData['keyword'], 'red');
                }
            }
            $this->ajaxReturn([
                'table' => $table,
                'total' => $total,
            ]);
        }else{
            $group_list = M('ad_input_user_group')
                ->field('group_id value, group_name label')
                ->cache(true, 60)
                ->select();
            $group_list[] = [
                'value' => '0',
                'label' => '未分组',
            ];
            $userTags = json_encode(explode(',', sys_c('user_tags')));
            $group_list = json_encode($group_list);
            $this->assign(compact('userTagArr', 'group_list', 'userTags'));
            $this->display();
        }
	}

    public function getUserInfo($wx_id)
    {
        $data = M('ad_input_user')->find($wx_id);
        $data['group_list'] = M('ad_input_user_group')
            ->field('group_id `value`, group_name label')
            ->cache(true, 60)
            ->select();
        if ($data['have_group_id']){
            $data['haveGroupList'] = M('ad_input_user_group')
                ->field('group_id `value`, group_name label')
                ->where("field(`group_id`, {$data['have_group_id']})")
                ->select();
        }else{
            $data['haveGroupList'] = [];
        }
        $data['user_tag'] = explode(',', $data['user_tag']);
		$data['userAuthority'] = explode(',',$data['user_authority']);
        $data['group_list'][] = [
            'value' => '0',
            'label' => '未分组',
        ];
        $this->ajaxReturn([
            'data' => $data
        ]);
    }

    public function saveUserInfo($userInfo)
    {
        $fillField = [
            'alias',
            'mobile',
            'mail',
            'group_id',
            'is_leader',
            'have_group_id',
        ];
        $data = [];
        foreach ($userInfo as $key => $value) {
            if (in_array($key, $fillField)){
                $data[$key] = $value;
            }
        }
        if ($userInfo['password1'] != ''){
            $data['password'] = md5($userInfo['password1']);
        }
        if ($userInfo['user_tag'] != ''){
            $sys_user_tags = explode(',', sys_c('user_tags'));
            $saveTagArr = [];
            foreach ($userInfo['user_tag'] as $key => $value) {
                if (in_array($value, $sys_user_tags)){
                    $saveTagArr[] = $value;
                }
            }
            $data['user_tag'] = implode(',', $saveTagArr);
        }else{
            $data['user_tag'] = '';
        }
		
		$data['user_authority'] = strval(implode(',',$userInfo['userAuthority']));
		
		
		
		
        $res = M('ad_input_user')
            ->where([
                'wx_id' => ['EQ', $userInfo['wx_id']]
            ])
            ->save($data);
        if($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
    }

    public function batchModifyInfo($wxIds, $userInfo)
    {
        $where = [
            'wx_id' => ['IN', $wxIds]
        ];
        $data = [
            'user_tag' => implode(',', $userInfo['user_tag']),
            'have_group_id' => $userInfo['have_group_id'],
            'group_id' => $userInfo['group_id'],
        ];
        foreach ($data as $key => $value) {
            if (!$value){
                unset($data[$key]);
            }
        }
        $res = M('ad_input_user')
            ->where($where)
            ->save($data);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败',
            ]);
        }
	}

    /**
     * 删除用户(逻辑删除)
     */
    public function delUsers($wxIds){
        $where = [
            'wx_id' => ['IN', $wxIds]
        ];
        $data = [
            'state' => -1,
        ];
        $res = M('ad_input_user')
            ->where($where)
            ->save($data);
        if($res){
            $this->ajaxReturn(['code' => 0,'msg' => '删除成功']);
        }else{
            $this->ajaxReturn(['code' => -1,'msg' => '删除失败']);
        }
	}

    /**
     * 积分汇总页面
     */
    public function taskInputScoreSummary()
    {
        session_write_close();
        if(IS_POST){
            $formData = I('post.');
            switch($formData['filtertype']){
                case 1://明细
                    $table = $this->detailScore($formData);
                    break;
                case 2://按日
                    $table = $this->sumScore($formData,2);
                    break;
                case 3://按月
                    $table = $this->sumScore($formData,3);
                    break;
                case 4://按年
                    $table = $this->sumScore($formData,4);
                    break;
                default://默认按日
                    $table = $this->sumScore($formData,2);
                    break;
            }
            $this->ajaxReturn($table);
        }else{
            $userTags = json_encode(explode(',', sys_c('user_tags')));
            $groupList = M('ad_input_user_group')
                ->field('group_id value, group_name label')
                ->cache(true, 60)
                ->select();
            $groupList = json_encode($groupList);
            $fieldList = json_encode($this->getScoreCountField());
            $this->assign(compact('userTags', 'groupList', 'fieldList'));
            $this->display();
        }
	}

    /**
     * 导出积分排名到Excel
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function exportScoreExcel()
    {
        if (IS_POST){
            $formData = I('post.');
            $this->ajaxReturn(U('exportScoreExcel').'?data='.json_encode($formData));
        }
        $formData = json_decode(htmlspecialchars_decode(I('get.data')), true);
        switch($formData['filtertype']){
            case 1://明细
                $table = $this->detailScore($formData);
                break;
            case 2://按日
                $table = $this->sumScore($formData,2);
                break;
            case 3://按月
                $table = $this->sumScore($formData,3);
                break;
            case 4://按年
                $table = $this->sumScore($formData,4);
                break;
            default://默认按日
                $table = $this->sumScore($formData,2);
                break;
        }
        // 字段顺序应该与上面方法返回字段顺序一致
        $fieldArr = $this->getScoreCountField($formData['filtertype']);
        $letterArr = range('A', 'Z');
        vendor('PHPExcel', '', '.class.php');
        $objExcel = new \PHPExcel();
        $sheet = $objExcel->getActiveSheet();
        // 设置表头
        foreach ($fieldArr as $key => $value) {
            $sheet->setCellValue("$letterArr[$key]1" , $value['label']);
        }
        // 填充数据
        foreach ($table as $key1 => $row){
            foreach ($fieldArr as $key2 => $value) {
                $sheet->setCellValue($letterArr[$key2].($key1+2) , $row[$value['field']]);
            }
        }
        $fileName = '任务积分'.date('YmdHis');
        header('pragma:public');
        header('Content-type:application/vnd.ms-excel;charset=utf-8;name="'.$fileName.'.xls"');
        header("Content-Disposition:attachment;filename=$fileName.xls");//attachment新
        $objWriter = \PHPExcel_IOFactory::createWriter($objExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    /**
     * @Des: 积分记录明细
     * @Edt: yuhou.wang
     * @Date: 2020-02-28 16:22:01
     * @param {type} 
     * @return: 
     */
    public function detailScore($search=[]){
        // 用户名
        $where['b.alias'] = ['NEQ',''];
        if($search['userName'] != ''){
            $where['b.alias'] = ['LIKE','%'.$search['userName'].'%'];
        }
        // 标签
        if($search['userTag']){
            foreach($search['userTag'] as $val){
                $user_tag[] = '%'.$val.'%';
            }
            $where['b.user_tag'] = ['LIKE',$user_tag];
        }
        // 分组
        if($search['group_id']){
            $where['b.group_id'] = ['EQ', $search['group_id']];
        }
        // 时间范围,缺省为当天
        $stime = date('Y-m-d 00:00:00');
        $etime = date('Y-m-d H:i:s');
        if (!empty($search['timeRangeArr'])){
            // 若有时间搜索条件
            foreach ($search['timeRangeArr'] as $key => $value) {
                $search['timeRangeArr'][$key] = ($value/1000);
            }
            $stime = $search['timeRangeArr'][0];
            $etime = 86399+$search['timeRangeArr'][1];
        }
        $where['a.update_time'] = ['BETWEEN',[$stime,$etime]];
        $fields = '
            a.wx_id,
            b.alias,
            FROM_UNIXTIME( a.update_time ) AS score_datetime,
            a.score,
            a.reason,
            a.taskid,
            ( CASE a.type WHEN 1 THEN "录入" WHEN 2 THEN "审核" WHEN 3 THEN "快剪" WHEN 4 THEN "补录" WHEN 5 THEN "检查" WHEN 6 THEN "质检" WHEN 7 THEN "上传报纸" WHEN 8 THEN "违法确认" WHEN 9 THEN "临时工作" END ) AS type,
            ( CASE a.media_class WHEN 1 THEN "电视" WHEN 2 THEN "广播" WHEN 3 THEN "报纸" WHEN 13 THEN "网络" END ) AS media_class
        ';
        $dataSet = M('task_input_score')
            ->alias('a')
            ->field($fields)
            ->join('ad_input_user b ON b.wx_id=a.wx_id')
            ->where($where)
            ->order('a.wx_id,a.score_date')
            ->select();
        return $dataSet;
    }
    /**
     * @Des: 积分汇总
     * @Edt: yuhou.wang
     * @Date: 2020-02-19 14:29:10
     * @param {Array} $search 查询条件
     * @param {Array} $sumtype 2按日 3按月 4按年
     * @return: {Array}
     */ 
    public function sumScore($search=[],$sumtype=2){
        // 用户名
        $where['b.alias'] = ['NEQ',''];
        if($search['userName'] != ''){
            $where['b.alias'] = ['LIKE','%'.$search['userName'].'%'];
        }
        // 标签
        if($search['userTag']){
            foreach($search['userTag'] as $val){
                $user_tag[] = '%'.$val.'%';
            }
            $where['b.user_tag'] = ['LIKE',$user_tag];
        }
        // 分组
        if($search['group_id']){
            $where['b.group_id'] = ['EQ', $search['group_id']];
        }
        // 时间范围,缺省为当天
        $stime = date('Y-m-d');
        $etime = date('Y-m-d');
        if (!empty($search['timeRangeArr'])){
            // 若有时间搜索条件
            foreach ($search['timeRangeArr'] as $key => $value) {
                $search['timeRangeArr'][$key] = substr($value, 0, 10);
            }
            $stime = date('Y-m-d',$search['timeRangeArr'][0]);
            $etime = date('Y-m-d',$search['timeRangeArr'][1]);
        }
        $where['a.score_date'] = ['BETWEEN',[$stime,$etime]];
        switch($sumtype){
            case 2:$grouptype = 'a.score_date';break;
            case 3:$grouptype = 'LEFT(a.score_date,7)';break;
            case 4:$grouptype = 'LEFT(a.score_date,4)';break;
            default:$grouptype = 'a.score_date';break;
        }
        // +sum( CASE WHEN a.type = 3 THEN score ELSE 0 END ) //快剪总积分
        $fields = '
            a.wx_id AS wx_id,
            b.alias AS alias,
            '.$grouptype.' AS score_date,
            (
                sum( CASE WHEN a.media_class = 1 AND a.type IN ( 1, 2 ) THEN score ELSE 0 END )
                +sum( CASE WHEN a.media_class = 2 AND a.type IN ( 1, 2 ) THEN score ELSE 0 END )
                +sum( CASE WHEN a.media_class = 3 AND a.type IN ( 1, 2 ) THEN score ELSE 0 END )
                +sum( CASE WHEN a.media_class = 13 AND a.type IN ( 1, 2 ) THEN score ELSE 0 END )
                +sum( CASE WHEN a.media_class = 13 AND a.type IN ( 4 ) THEN score ELSE 0 END )
                +sum( CASE WHEN a.media_class = 13 AND a.type IN ( 5 ) THEN score ELSE 0 END )
                +sum( CASE WHEN a.media_class = 1 AND a.type = 3 THEN score ELSE 0 END )
                +sum( CASE WHEN a.media_class = 2 AND a.type = 3 THEN score ELSE 0 END )
                +sum( CASE WHEN a.type = 6 THEN score ELSE 0 END )
                +sum( CASE WHEN a.type = 7 THEN score ELSE 0 END )
                +sum( CASE WHEN a.type = 8 THEN score ELSE 0 END )
                +sum( CASE WHEN a.type = 9 THEN score ELSE 0 END )
            ) AS sum_score,
            sum( CASE WHEN a.media_class = 1 AND a.type IN ( 1, 2 ) THEN score ELSE 0 END ) AS tv_score,
            sum( CASE WHEN a.media_class = 2 AND a.type IN ( 1, 2 ) THEN score ELSE 0 END ) AS bc_score,
            sum( CASE WHEN a.media_class = 3 AND a.type IN ( 1, 2 ) THEN score ELSE 0 END ) AS paper_score,
            sum( CASE WHEN a.media_class = 13 AND a.type IN ( 1, 2 ) THEN score ELSE 0 END ) AS net_score,
            sum( CASE WHEN a.media_class = 13 AND a.type IN ( 4 ) THEN score ELSE 0 END ) AS net_append_score,
            sum( CASE WHEN a.media_class = 13 AND a.type IN ( 5 ) THEN score ELSE 0 END ) AS net_check_score,
            sum( CASE WHEN a.type = 3 THEN score ELSE 0 END ) AS qc_score,
            sum( CASE WHEN a.media_class = 1 AND a.type = 3 THEN score ELSE 0 END ) AS qc_score_tv,
            sum( CASE WHEN a.media_class = 2 AND a.type = 3 THEN score ELSE 0 END ) AS qc_score_bc,
            sum( CASE WHEN a.type = 6 THEN score ELSE 0 END ) AS quality_score,
            sum( CASE WHEN a.type = 7 THEN score ELSE 0 END ) AS paper_upload,
            sum( CASE WHEN a.type = 8 THEN score ELSE 0 END ) AS illegal_confirm,
            sum( CASE WHEN a.type = 9 THEN score ELSE 0 END ) AS work_score
        ';
        $dataSet = M('task_input_score')
            ->alias('a')
            ->field($fields)
            ->join('ad_input_user b ON b.wx_id=a.wx_id')
            ->where($where)
            ->group('a.wx_id,'.$grouptype)
            ->order('a.wx_id,a.score_date')
            ->select();
        return $dataSet;
    }

    /**
     * 积分统计 2019-06-27
     * type 1: 录入 2: 审核  3:快剪任务 4:互联网数据检查 5:互联网数据检查审核 6:质检
     * @param $formData Array 搜索条件
     */
    public function summaryScore($formData){

        // 用户名
        $userWhere['alias'] = ['NEQ',''];
        if ($formData['userName'] != ''){
            $userWhere['alias'] = ['LIKE','%'.$formData['userName'].'%'];
        }
        // 标签
        if ($formData['userTag']){
            $userWhere['_string'] = "(";
            foreach ($formData['userTag'] as $formDatum) {
                $userWhere['_string'] .= "FIND_IN_SET('$formDatum', user_tag) AND ";
            }
            $userWhere['_string'] = rtrim($userWhere['_string'], ' AND ');
            $userWhere['_string'] .= ')';
        }
        // 分组
        if ($formData['group_id']){
            $userWhere['group_id'] = ['EQ', $formData['group_id']];
        }
        // 用户
        $userSubQuery = M('ad_input_user')
            ->field('wx_id, alias')
            ->where($userWhere)
            ->select();
        // 用户ID
        $IdArr = [];
        foreach($userSubQuery as $key => $val){
            array_push($IdArr,$val['wx_id']);
        }

        // 时间范围,缺省为本月
        $stime = strtotime(date('Y-m'));
        $etime = strtotime('+1 month', $stime) - 1;
        if (!empty($formData['timeRangeArr'])){
            // 若有时间搜索条件
            foreach ($formData['timeRangeArr'] as $key => $value) {
                $formData['timeRangeArr'][$key] = substr($value, 0, 10);
            }
            $stime = (int)$formData['timeRangeArr'][0];
            $etime = strtotime('+1 day', $formData['timeRangeArr'][1]) - 1;// 时间戳, 需要加1天减1秒
        }
        
        // 传统媒体任务积分
        $scoreWhereTradition = [
            'wx_id' => ['IN',$IdArr],
            'update_time' => ['BETWEEN',[$stime,$etime]],
            'type' => ['IN','1,2'],
            'media_class' => ['IN','1,2,3'],
        ];
        $scoreSubQueryTradition = M('task_input_score')
            ->field('
                wx_id,
                sum(score) AS score
                ')
            ->where($scoreWhereTradition)
            ->group('wx_id')
            ->select();
        // 网络媒体任务积分
        $scoreWhereNet = [
            'wx_id' => ['IN',$IdArr],
            'update_time' => ['BETWEEN',[$stime,$etime]],
            'type' => ['IN','1,2,4,5'],
            'media_class' => 13,
        ];
        $scoreSubQueryNet = M('task_input_score')
            ->field('
                wx_id,
                sum(score) AS score
                ')
            ->where($scoreWhereNet)
            ->group('wx_id')
            ->select();
        // 快剪积分
        $qcWhere = [
            'wx_id' => ['IN',$IdArr],
            'update_time' => ['BETWEEN',[$stime,$etime]],
            'type' => 3,
        ];
        $qcSubQuery = M('task_input_score')
            ->field('
                wx_id,
                sum(score) AS score
                ')
            ->where($qcWhere)
            ->group('wx_id')
            ->select();
        // 质检积分
        $qiWhere = [
            'wx_id' => ['IN',$IdArr],
            'update_time' => ['BETWEEN',[$stime,$etime]],
            'type' => 6,
        ];
        $qiSubQuery = M('task_input_score')
            ->field('
                wx_id,
                sum(score) AS score
                ')
            ->where($qiWhere)
            ->group('wx_id')
            ->select();
        // 临时任务(本职工作)积分
        $workWhere = [
            'wx_id' => ['IN',$IdArr],
            'score_date' => ['BETWEEN',[$stime,$etime]],
            'state' => 1,
        ];
        $workSubQuery = M('work_score')
            ->field('
                wx_id,
                sum(score) AS score
                ')
            ->where($workWhere)
            ->group('wx_id')
            ->select();
        // 合并汇总
        $dataSet = [];
        foreach($userSubQuery as $key => $user){
            $dataRow = [
                'alias'      => $user['alias'],
                'sum_score'  => 0,
                'tra_score'  => 0,
                'net_score'  => 0,
                'qc_score'   => 0,
                'work_score' => 0,
            ];
            $sum_score = 0;
            foreach($scoreSubQueryTradition as $keyTra => $rowTra){
                if($rowTra['wx_id'] == $user['wx_id']){
                    $dataRow['tra_score'] = $rowTra['score'];
                    $sum_score += $rowTra['score'];
                }
            }
            foreach($scoreSubQueryNet as $keyNet => $rowNet){
                if($rowNet['wx_id'] == $user['wx_id']){
                    $dataRow['net_score'] = $rowNet['score'];
                    $sum_score += $rowNet['score'];
                }
            }
            foreach($qcSubQuery as $keyQc => $rowQc){
                if($rowQc['wx_id'] == $user['wx_id']){
                    $dataRow['qc_score'] = $rowQc['score'];
                    $sum_score += $rowQc['score'];
                }
            }
            // 质检积分合并到临时任务(本职工作)积分，暂未显示在积分查询页面
            foreach($qiSubQuery as $keyQi => $rowQi){
                if($rowQi['wx_id'] == $user['wx_id']){
                    $dataRow['work_score'] += $rowQi['score'];
                    $sum_score += $rowQi['score'];
                }
            }
            foreach($workSubQuery as $keyWork => $rowWork){
                if($rowWork['wx_id'] == $user['wx_id']){
                    $dataRow['work_score'] += $rowWork['score'];
                    $sum_score += $rowWork['score'];
                }
            }
            $dataRow['sum_score'] = $sum_score;
            array_push($dataSet,$dataRow);
        }
        //根据字段sum_score对数组$dataSet进行降序排列
        $sum_score = array_column($dataSet,'sum_score');
        array_multisort($sum_score,SORT_DESC,$dataSet);
        return $dataSet;
    }
    
    /**
     * 积分统计
     * @param $formData Array 搜索条件
     */
    private function processScoreData($formData){
        // 用户名
        if ($formData['userName'] != ''){
            $userWhere['alias'] = ['LIKE','%'.$formData['userName'].'%'];
        }
        // 标签
        if ($formData['userTag']){
            $userWhere['_string'] = "(";
            foreach ($formData['userTag'] as $formDatum) {
                $userWhere['_string'] .= "FIND_IN_SET('$formDatum', user_tag) AND ";
            }
            $userWhere['_string'] = rtrim($userWhere['_string'], ' AND ');
            $userWhere['_string'] .= ')';
        }
        // 分组
        if ($formData['group_id']){
            $userWhere['group_id'] = ['EQ', $formData['group_id']];
        }
        // 时间范围
        if (!$formData['timeRangeArr']){
            // 如果没有搜索条件, 就搜索本月的
            $currentMonthTimeStamp = strtotime(date('Y-m'));
            $scoreWhere['update_time'] = ['BETWEEN', [$currentMonthTimeStamp, strtotime('+1 month', $currentMonthTimeStamp) - 1]];
            $scoreWhereTradition['update_time'] = ['BETWEEN', [$currentMonthTimeStamp, strtotime('+1 month', $currentMonthTimeStamp) - 1]];
            $scoreWhereNet['update_time'] = ['BETWEEN', [$currentMonthTimeStamp, strtotime('+1 month', $currentMonthTimeStamp) - 1]];
            $qcWhere['update_time'] = ['BETWEEN', [$currentMonthTimeStamp, strtotime('+1 month', $currentMonthTimeStamp) - 1]];
            $endTime = strtotime('+1 month', $currentMonthTimeStamp) - 1;
            $workScoreWhere = "work_score.score_date between $currentMonthTimeStamp and $endTime ";
        }else{
            foreach ($formData['timeRangeArr'] as $key => $value) {
                $formData['timeRangeArr'][$key] = substr($value, 0, 10);
            }
            // 因为是时间戳, 需要加一天
            $scoreWhere['update_time'] = ['BETWEEN', [$formData['timeRangeArr'][0], strtotime('+1 day', $formData['timeRangeArr'][1]) - 1]];
            $scoreWhereTradition['update_time'] = ['BETWEEN', [$formData['timeRangeArr'][0], strtotime('+1 day', $formData['timeRangeArr'][1]) - 1]];
            $scoreWhereNet['update_time'] = ['BETWEEN', [$formData['timeRangeArr'][0], strtotime('+1 day', $formData['timeRangeArr'][1]) - 1]];
            $qcWhere['update_time'] = ['BETWEEN', [$formData['timeRangeArr'][0], strtotime('+1 day', $formData['timeRangeArr'][1]) - 1]];
            $startTime = $formData['timeRangeArr'][0];
            $endTime = strtotime('+1 day', $formData['timeRangeArr'][1]) - 1;
            $workScoreWhere = "work_score.score_date between $startTime and $endTime ";
        }
        // 用户
        $userSubQuery = M('ad_input_user')
            ->field('wx_id, alias')
            ->where($userWhere)
            ->buildSql();
        // $userSet = M('ad_input_user')
        //     ->field('wx_id, alias')
        //     ->where($userWhere)
        //     ->select();
        // // 传统媒体任务积分
        // $scoreWhereTradition['type'] = ['IN','1,2'];
        // $scoreWhereTradition['media_class'] = ['IN','1,2,3'];
        // // 网络媒体任务积分
        // $scoreWhereNet['type'] = ['IN','1,2'];
        // $scoreWhereNet['media_class'] = 13;
        // // 快剪积分
        // $qcWhere['type'] = 3;
        // foreach($userSet as $key => $user){
        //     $scoreWhereTradition['wx_id'] = $user['wx_id'];
        //     $scoreSubQueryTradition = M('task_input_score')
        //         ->field("
        //             sum(score) as score
        //             ")
        //         ->where($scoreWhereTradition)
        //         ->find();

        //     $scoreWhereNet['wx_id'] = $user['wx_id'];
        //     $scoreSubQueryNet = M('task_input_score')
        //         ->field("
        //             sum(score) as score
        //             ")
        //         ->where($scoreWhereNet)
        //         ->find();

        //     $qcWhere['wx_id'] = $user['wx_id'];
        //     $qcSubQuery = M('task_input_score')
        //     ->field("
        //         sum(score) as score
        //         ")
        //     ->where($qcWhere)
        //     ->find();

        //     $userSet[$key]['tra_score'] = $scoreSubQueryTradition['score'];
        //     $userSet[$key]['net_score'] = $scoreSubQueryNet['score'];
        //     $userSet[$key]['qc_score'] = $qcSubQuery['score'];
        // }
        // return $userSet;

        // 任务积分,录入+审核
        // $scoreWhere['type'] = ['IN','1,2'];
        // $scoreSubQuery = M('task_input_score')
        //     ->field("
        //         wx_id,
        //         sum(score) as score
        //         ")
        //     ->where($scoreWhere)
        //     ->group('wx_id')
        //     ->buildSql();
        // 传统媒体任务积分-子查询
        $scoreWhereTradition['type'] = ['IN','1,2'];
        $scoreWhereTradition['media_class'] = ['IN','1,2,3'];
        $scoreSubQueryTradition = M('task_input_score')
            ->field("
                wx_id,
                sum(score) as score
                ")
            ->where($scoreWhereTradition)
            ->group('wx_id')
            ->buildSql();
        // 网络媒体任务积分-子查询
        $scoreWhereNet['type'] = ['IN','1,2,4'];
        $scoreWhereNet['media_class'] = 13;
        $scoreSubQueryNet = M('task_input_score')
            ->field("
                wx_id,
                sum(score) as score
                ")
            ->where($scoreWhereNet)
            ->group('wx_id')
            ->buildSql();
        // 快剪积分-子查询
        $qcWhere['type'] = 3;
        $qcSubQuery = M('task_input_score')
            ->field("
                wx_id,
                sum(score) as score
                ")
            ->where($qcWhere)
            ->group('wx_id')
            ->buildSql();
        $table = M()
            ->field("
                (ifnull(tra_score.score, 0 ) + ifnull(net_score.score, 0 ) + ifnull(qc.score, 0 ) + (select ifnull(sum( score ), 0) from work_score where $workScoreWhere and state = 1 and work_score.wx_id = `user`.wx_id)) as sum_score,
                (ifnull(tra_score.score, 0 )) as tra_score,
                (ifnull(net_score.score, 0 )) as net_score,
                (ifnull(qc.score, 0 )) as qc_score,
                (select ifnull(sum( score ), 0) from work_score where $workScoreWhere and state = 1 and work_score.wx_id = `user`.wx_id) as work_score,
                `user`.alias")
            ->table("$userSubQuery as `user`")
            ->join("left join $scoreSubQueryTradition as tra_score on `user`.wx_id = tra_score.wx_id")
            ->join("left join $scoreSubQueryNet as net_score on `user`.wx_id = net_score.wx_id")
            ->join("left join $qcSubQuery as qc on qc.wx_id = `user`.wx_id")
            ->order('sum_score desc')
            ->select();
        $orgSql = M()->getLastSql();
        return $table;
    }
    
    /**
     * 积分统计
     * @param $formData Array 搜索条件
     */
    private function processScoreData_V20190319($formData){
        // 用户名
        if ($formData['userName'] != ''){
            $userWhere['alias'] = ['LIKE','%'.$formData['userName'].'%'];
        }
        // 标签
        if ($formData['userTag']){
            $userWhere['_string'] = "(";
            foreach ($formData['userTag'] as $formDatum) {
                $userWhere['_string'] .= "FIND_IN_SET('$formDatum', user_tag) AND ";
            }
            $userWhere['_string'] = rtrim($userWhere['_string'], ' AND ');
            $userWhere['_string'] .= ')';
        }
        // 分组
        if ($formData['group_id']){
            $userWhere['group_id'] = ['EQ', $formData['group_id']];
        }
        // 时间范围
        if (!$formData['timeRangeArr']){
            // 如果没有搜索条件, 就搜索本月的
            $currentMonthTimeStamp = strtotime(date('Y-m'));
            $scoreWhere['update_time'] = ['BETWEEN', [$currentMonthTimeStamp, strtotime('+1 month', $currentMonthTimeStamp) - 1]];
            $qcWhere['update_time'] = ['BETWEEN', [$currentMonthTimeStamp, strtotime('+1 month', $currentMonthTimeStamp) - 1]];
            $endTime = strtotime('+1 month', $currentMonthTimeStamp) - 1;
            $workScoreWhere = "work_score.score_date between $currentMonthTimeStamp and $endTime ";
        }else{
            foreach ($formData['timeRangeArr'] as $key => $value) {
                $formData['timeRangeArr'][$key] = substr($value, 0, 10);
            }
            // 因为是时间戳, 需要加一天
            $scoreWhere['update_time'] = ['BETWEEN', [$formData['timeRangeArr'][0], strtotime('+1 day', $formData['timeRangeArr'][1]) - 1]];
            $qcWhere['update_time'] = ['BETWEEN', [$formData['timeRangeArr'][0], strtotime('+1 day', $formData['timeRangeArr'][1]) - 1]];
            $startTime = $formData['timeRangeArr'][0];
            $endTime = strtotime('+1 day', $formData['timeRangeArr'][1]) - 1;
            $workScoreWhere = "work_score.score_date between $startTime and $endTime ";
        }
        // 用户
        $userSubQuery = M('ad_input_user')
            ->field('wx_id, alias')
            ->where($userWhere)
            ->buildSql();
        // 任务积分,录入+审核
        $scoreWhere['type'] = ['IN','1,2'];
        $scoreSubQuery = M('task_input_score')
            ->field("
                wx_id,
                sum(score) as score
                ")
            ->where($scoreWhere)
            ->group('wx_id')
            ->buildSql();
        // 快剪积分
        $qcWhere['type'] = 3;
        $qcSubQuery = M('task_input_score')
            ->field("
                wx_id,
                sum(score) as score
                ")
            ->where($qcWhere)
            ->group('wx_id')
            ->buildSql();
        $table = M()
            ->field("
                (ifnull(score.score, 0 ) + ifnull(qc.score, 0 ) + (select ifnull(sum( score ), 0) from work_score where $workScoreWhere and state = 1 and work_score.wx_id = `user`.wx_id)) as sum_score,
                ifnull(score.score, 0 ) as task_score,
                (ifnull(qc.score, 0 )) as qc_score,
                (select ifnull(sum( score ), 0) from work_score where $workScoreWhere and state = 1 and work_score.wx_id = `user`.wx_id) as work_score,
                `user`.alias")
            ->table("$userSubQuery as `user`")
            ->join("left join $scoreSubQuery as score on `user`.wx_id = score.wx_id")
            ->join("left join $qcSubQuery as qc on qc.wx_id = `user`.wx_id")
            ->order('sum_score desc')
            ->select();
        return $table;
    }

    /**
     * @param $formData array   前端传来的搜索条件
     * @return array    score表的where条件
     */
    private function processScoreData_20190108($formData)
    {
        $scoreWhere = [];
        $userWhere = [];
        $taskCountWhere = [];
        $cutTaskWhere = [];     //快剪任务积分条件
        $cutIssueWhere = [];    //快剪条次积分条件
        $workScoreWhere = '';
        if ($formData['userName'] != ''){
            $userWhere['alias'] = ['LIKE','%'.$formData['userName'].'%'];
        }
        // 处理用户标签
        if ($formData['userTag']){
            $userWhere['_string'] = "(";
            foreach ($formData['userTag'] as $formDatum) {
                $userWhere['_string'] .= "FIND_IN_SET('$formDatum', user_tag) AND ";
            }
            $userWhere['_string'] = rtrim($userWhere['_string'], ' AND ');
            $userWhere['_string'] .= ')';
        }
        if ($formData['group_id']){
            $userWhere['group_id'] = ['EQ', $formData['group_id']];
        }
        if (!$formData['timeRangeArr']){
            // 如果没有搜索条件, 就搜索本月的
            $currentMonthTimeStamp = strtotime(date('Y-m'));
            $scoreWhere['update_time'] = ['BETWEEN', [$currentMonthTimeStamp, strtotime('+1 month', $currentMonthTimeStamp) - 1]];
            // $workScoreWhere['work_score.score_date'] = ['BETWEEN', [$currentMonthTimeStamp, strtotime('+1 month', $currentMonthTimeStamp) - 1]];
            $endTime = strtotime('+1 month', $currentMonthTimeStamp) - 1;
            $workScoreWhere = "work_score.score_date between $currentMonthTimeStamp and $endTime ";
            $taskCountWhere['count_date'] = ['BETWEEN', [date('Y-m-d', $currentMonthTimeStamp), date('Y-m-d', strtotime('+1 month', $currentMonthTimeStamp) - 1)]];
            $cutTaskWhere['cut_complete_time'] = ['BETWEEN',[$currentMonthTimeStamp, strtotime('+1 month', $currentMonthTimeStamp) - 1]];
            $cutIssueWhere['fcreattime'] = ['BETWEEN', [date('Y-m-d', $currentMonthTimeStamp), date('Y-m-d', strtotime('+1 month', $currentMonthTimeStamp) - 1)]];
        }else{
            foreach ($formData['timeRangeArr'] as $key => $value) {
                $formData['timeRangeArr'][$key] = substr($value, 0, 10);
            }
            // 因为是时间戳, 需要加一天
            $scoreWhere['update_time'] = ['BETWEEN', [$formData['timeRangeArr'][0], strtotime('+1 day', $formData['timeRangeArr'][1]) - 1]];
            $taskCountWhere['count_date'] = ['BETWEEN', [
                date('Y-m-d', $formData['timeRangeArr'][0]),
                date('Y-m-d', strtotime('+1 day', $formData['timeRangeArr'][1]) - 1)
            ]];
            // $workScoreWhere['work_score.score_date'] = ['BETWEEN', [$formData['timeRangeArr'][0], strtotime('+1 day', $formData['timeRangeArr'][1]) - 1]];
            $startTime = $formData['timeRangeArr'][0];
            $endTime = strtotime('+1 day', $formData['timeRangeArr'][1]) - 1;
            $workScoreWhere = "work_score.score_date between $startTime and $endTime ";
            $cutTaskWhere['cut_complete_time'] = ['BETWEEN', [$formData['timeRangeArr'][0], strtotime('+1 day', $formData['timeRangeArr'][1]) - 1]];
            $cutIssueWhere['fcreattime'] = ['BETWEEN', [
                date('Y-m-d', $formData['timeRangeArr'][0]),
                date('Y-m-d', strtotime('+1 day', $formData['timeRangeArr'][1]) - 1)
            ]];
        }
        $userSubQuery = M('ad_input_user')
            ->field('wx_id, alias')
            ->where($userWhere)
            ->buildSql();
        $scoreSubQuery = M('task_input_score')
            ->field("
                wx_id,
                sum(score) as score
                ")
            ->where($scoreWhere)
            ->group('wx_id')
            ->buildSql();
        $taskCountSubQuery = M('user_task_count')
            ->field("
                sum(v3_tv_task) as v3_tv_task,
                sum(v3_bc_task) as v3_bc_task,
                sum(v3_paper_task) as v3_paper_task,
                sum(v3_net_task) as v3_net_task,
                user_id
            ")
            ->where($taskCountWhere)
            ->group('user_id')
            ->buildSql();
        // 快剪积分统计-样本量积分
        $qcSubQuery = M('tbcsample')
            ->field("
                fcreator,
                count(case when tbcsample.fadlen <= 600 and fillegaltypecode = 0 then 1 else null end) * 4 +
                count(case when tbcsample.fadlen <= 600 and fillegaltypecode > 0 then 1 else null end) * 9 +
                count(case when tbcsample.fadlen > 600 and fillegaltypecode = 0 then 1 else null end) * 8 +
                count(case when tbcsample.fadlen > 600 and fillegaltypecode > 0 then 1 else null end) * 12 as score
            ")
            ->where([
                'fstate'  => ['GT', 0],
                'fmediaid' => ['EXP', " IN (select fmediaid from tqc_media_permission)"],
                'date(fcreatetime)' => $taskCountWhere['count_date'],
            ])
            ->group('fcreator')
            ->buildSql();
        // 快剪积分统计-快剪任务积分
        $cutTaskWhere['fstate'] = 4;
        $qcTaskSubQuery = M('cut_task')
            ->field("
                cut_wx_id,
                count(1) * 20 as score
            ")
            ->where($cutTaskWhere)
            ->group('cut_wx_id')
            ->buildSql();
        // 快剪积分统计-条次积分
        $cutIssueWhere['fstate'] = 1;
        $qcIssueCountSubQuery = M('tbcissue')
            ->field("
                fcreator,
                ROUND( ( count( 1 ) / 20 ), 2 ) AS score 
            ")
            ->where($cutIssueWhere)
            ->group('fcreator')
            ->buildSql();

        $table = M()
            ->field("
                (ifnull(score.score, 0 ) + ifnull(qc.score, 0 ) + ifnull(qctask.score, 0 ) + ifnull(qcissue.score, 0 ) + (select ifnull(sum( score ), 0) from work_score where $workScoreWhere and state = 1 and work_score.wx_id = `user`.wx_id)) as sum_score,
                ifnull(score.score, 0 ) as task_score,
                (ifnull(qc.score, 0 ) + ifnull(qctask.score, 0 ) + ifnull(qcissue.score, 0 )) as qc_score,
                (select ifnull(sum( score ), 0) from work_score where $workScoreWhere and state = 1 and work_score.wx_id = `user`.wx_id) as work_score,
                `user`.alias")
            ->table("$userSubQuery as `user`")
            // ->join("left join $taskCountSubQuery as count on user.wx_id = count.user_id")
            ->join("left join $scoreSubQuery as score on user.wx_id = score.wx_id")
            ->join("left join $qcSubQuery as qc on qc.fcreator = `user`.alias")
            ->join("left join $qcTaskSubQuery as qctask on qctask.cut_wx_id = `user`.wx_id")
            ->join("left join $qcIssueCountSubQuery as qcissue on qcissue.fcreator = `user`.alias")
            ->order('sum_score desc')
            ->select();
        return $table;
    }
    /**
     * @Des: 列表字段定义
     * @Edt: yuhou.wang
     * @Date: 2020-02-28 17:06:35
     * @param {type} 1明细 2按日 3按月 4按年
     * @return: 
     */
    public function getScoreCountField($type=0){
        $fieldArr[1] = [
            [
                'field' => 'wx_id',
                'label' => 'ID',
            ],
            [
                'field' => 'alias',
                'label' => '姓名',
            ],
            [
                'field' => 'score_datetime',
                'label' => '时间',
            ],
            [
                'field' => 'score',
                'label' => '积分',
            ],
            [
                'field' => 'reason',
                'label' => '原因',
            ],
            [
                'field' => 'type',
                'label' => '类型',
            ],
            [
                'field' => 'media_class',
                'label' => '媒介类别',
            ],
            [
                'field' => 'taskid',
                'label' => '任务ID',
            ],
        ];
        $fieldArr[2] = [
            [
                'field' => 'wx_id',
                'label' => 'ID',
            ],
            [
                'field' => 'alias',
                'label' => '姓名',
            ],
            [
                'field' => 'score_date',
                'label' => '日期',
            ],
            [
                'field' => 'sum_score',
                'label' => '总积分',
            ],
            [
                'field' => 'tv_score',
                'label' => '电视',
            ],
            [
                'field' => 'bc_score',
                'label' => '广播',
            ],
            [
                'field' => 'paper_score',
                'label' => '报纸',
            ],
            [
                'field' => 'net_score',
                'label' => '网络',
            ],
            [
                'field' => 'qc_score_tv',
                'label' => '快剪电视',
            ],
            [
                'field' => 'qc_score_bc',
                'label' => '快剪广播',
            ],
            [
                'field' => 'quality_score',
                'label' => '质检',
            ],
            [
                'field' => 'net_check_score',
                'label' => '检查',
            ],
            [
                'field' => 'net_append_score',
                'label' => '补录',
            ],
            [
                'field' => 'paper_upload',
                'label' => '报纸上传',
            ],
            [
                'field' => 'illegal_confirm',
                'label' => '违法确认',
            ],
            [
                'field' => 'work_score',
                'label' => '临时工作',
            ],
        ];
        $fieldArr[3] = [
            [
                'field' => 'wx_id',
                'label' => 'ID',
            ],
            [
                'field' => 'alias',
                'label' => '姓名',
            ],
            [
                'field' => 'score_date',
                'label' => '月份',
            ],
            [
                'field' => 'sum_score',
                'label' => '总积分',
            ],
            [
                'field' => 'tv_score',
                'label' => '电视',
            ],
            [
                'field' => 'bc_score',
                'label' => '广播',
            ],
            [
                'field' => 'paper_score',
                'label' => '报纸',
            ],
            [
                'field' => 'net_score',
                'label' => '网络',
            ],
            [
                'field' => 'qc_score_tv',
                'label' => '快剪电视',
            ],
            [
                'field' => 'qc_score_bc',
                'label' => '快剪广播',
            ],
            [
                'field' => 'quality_score',
                'label' => '质检',
            ],
            [
                'field' => 'net_check_score',
                'label' => '检查',
            ],
            [
                'field' => 'net_append_score',
                'label' => '补录',
            ],
            [
                'field' => 'paper_upload',
                'label' => '报纸上传',
            ],
            [
                'field' => 'illegal_confirm',
                'label' => '违法确认',
            ],
            [
                'field' => 'work_score',
                'label' => '临时工作',
            ],
        ];
        $fieldArr[4] = [
            [
                'field' => 'wx_id',
                'label' => 'ID',
            ],
            [
                'field' => 'alias',
                'label' => '姓名',
            ],
            [
                'field' => 'score_date',
                'label' => '年份',
            ],
            [
                'field' => 'sum_score',
                'label' => '总积分',
            ],
            [
                'field' => 'tv_score',
                'label' => '电视',
            ],
            [
                'field' => 'bc_score',
                'label' => '广播',
            ],
            [
                'field' => 'paper_score',
                'label' => '报纸',
            ],
            [
                'field' => 'net_score',
                'label' => '网络',
            ],
            [
                'field' => 'qc_score_tv',
                'label' => '快剪电视',
            ],
            [
                'field' => 'qc_score_bc',
                'label' => '快剪广播',
            ],
            [
                'field' => 'quality_score',
                'label' => '质检',
            ],
            [
                'field' => 'net_check_score',
                'label' => '检查',
            ],
            [
                'field' => 'net_append_score',
                'label' => '补录',
            ],
            [
                'field' => 'paper_upload',
                'label' => '报纸上传',
            ],
            [
                'field' => 'illegal_confirm',
                'label' => '违法确认',
            ],
            [
                'field' => 'work_score',
                'label' => '临时工作',
            ],
        ];
        if($type){
            return $fieldArr[$type];
        }else{
            return $fieldArr;
        }
    }
	
	
	
	
	
}