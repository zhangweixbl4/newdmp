<?php
namespace Api\Controller;
use Think\Controller;
class AiController extends Controller {
	
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}
	
	/*
	验证请求签名
	signature计算方法：md5($secret.$timestamp)
	*/
	private function checkSignature($timestamp,$signature){
		$secret = '5Qb9a;gfeT65mNbG4U5aw5p6z68gv7uep0gdichx56';//秘钥
		$time_offset = 6000000;//调用接口时允许的时间误差
		if ($signature != md5($secret.$timestamp)){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'signature error'));//判断签名是否正确
		}	
		if ($timestamp + $time_offset < time() || $timestamp - $time_offset > time()){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'timestamp error'));//判断签名是否正确
		}	
	}
	
    public function index(){
		file_put_contents('LOG/'.date('YmdHis').'-Ai',file_get_contents('php://input'));
		$post_data = json_decode(file_get_contents('php://input'),true);
		$this->checkSignature($post_data['timestamp'],$post_data['signature']);
		$this->ajaxReturn(array('code'=>0,'msg'=>'SUCCESS'));

	}
	
	#反馈样本完成状态
	public function fk_sam(){
		session_write_close();
		set_time_limit(600);
		$mm = I('mm');
		$where = array();
		
		#$where['fissuedate'] = array('gt',date('Y-m-d',time()-86400*31));
		$where['fadid'] = array('gt',0);
		$where['fsource'] = 0;
		$where['fk_sc'] = 0;
		
		#exit;
		$tvSamList = [];
		$bcSamList = [];
		if($mm == 'tv' || !$mm){
			$tvSamList = M('ttvsample')->field('"tv" as mm,fid,uuid,fmediaid')->where($where)->order('fid desc')->limit(300)->select();
			echo M('ttvsample')->getLastSql() ."\n";
		}
		if($mm == 'bc' || !$mm){
			$bcSamList = M('tbcsample')->field('"bc" as mm,fid,uuid,fmediaid')->where($where)->order('fid desc')->limit(300)->select();
			echo M('tbcsample')->getLastSql() ."\n";
		}
		$samList = array_merge($tvSamList,$bcSamList);
		$ehttptime = 0;
		foreach($samList as $sam){
			
			$samInfo = M('t'.$sam['mm'].'sample')->alias('tsam')
									->field('
												tad.fadname
												,tad.fbrand
												,tad.fadclasscode
												,tsam.fillegaltypecode
												,ifnull(tsam.fillegalcontent, "") as fillegalcontent
												,ifnull(tsam.fexpressioncodes, "") as fexpressioncodes
												')
									->join('join tad on tad.fadid = tsam.fadid')
									->where(array('tsam.fid'=>$sam['fid']))
									->find();

			
			$postToData = array();
			$postToData['channel_id'] = $sam['fmediaid'];
			$postToData['sp_uuid'] = $sam['uuid'];
			$postToData['status'] = 1;
			$postToData['msg'] = '';
			$postToData['detail'] = json_encode($samInfo);

			#var_dump($postToData);
			
			
			$fk_url = 'http://192.168.0.153:8002/sp_check';
			$msectime = msectime();
			$reqStr = http($fk_url,$postToData,'POST');
			$ehttptime += msectime()-$msectime;
			#var_dump($reqStr);
			$reqArr = json_decode($reqStr,true);
			
			$editA = array();
			$editA['fk_sc_time'] = time();
			$editA['fk_sc_req'] = $reqStr;
			
			
			if($reqArr['ret'] === 0 || $reqArr['ret'] === 1){
				$editA['fk_sc'] = 1;
			}
			
			$ec = M('t'.$sam['mm'].'sample')->where(array('fid'=>$sam['fid']))->save($editA);
		
		}
		echo date('Y-m-d H:i:s'). "\n";
		echo '执行时间（毫秒）：'.$ehttptime. "\n";
		
		echo 'end';
		
	}
	
	#反馈样本完成状态(需要删除的样本)
	public function fk_sam_del(){
		session_write_close();
		set_time_limit(600);
		$where = array();
		
		#$where['fissuedate'] = array('gt',date('Y-m-d',time()-86400*31));
		$where['fstate'] = -1;
		#$where['fk_sc'] = 0;
		
		#exit;
		$tvSamList = M('ttvsample')
								->field('"tv" as mm,fid,uuid,fmediaid,abnormal_reason,fmodifytime,fmodifier')
								->where($where)->limit(100)->select();
		echo M('ttvsample')->getLastSql() ."\n";
		$bcSamList = M('tbcsample')
								->field('"bc" as mm,fid,uuid,fmediaid,abnormal_reason,fmodifytime,fmodifier')
								->where($where)->limit(100)->select();
		echo M('tbcsample')->getLastSql() ."\n";
		$samList = array_merge($tvSamList,$bcSamList);
		
		foreach($samList as $sam){
			$mediaInfo = M('tmedia')
						->cache(true,60)
						->field('fid,fmedianame,media_region_id')
						->where(array('fid'=>$sam['fmediaid']))
						->find(); #查询媒介信息
			
			$postToData = array();
			$postToData['channel_id'] = $sam['fmediaid'];
			$postToData['sp_uuid'] = $sam['uuid'];
			$postToData['status'] = -1;
			$postToData['msg'] = $sam['abnormal_reason'];
			$postToData['detail'] = '{}';

			#var_dump($postToData);
			
			
			$fk_url = 'http://192.168.0.153:8002/sp_check'; #接口地址
			$reqStr = http($fk_url,$postToData,'POST'); #把数据发送给孙涛
			#var_dump($reqStr);
			#var_dump($postToData);
			$reqArr = json_decode($reqStr,true);
			
			
			
			if($reqArr['ret'] === 0){ #如果接口请求成功
				M()->startTrans();
				$dmpLogs2 = A('Common/AliyunLog','Model')->dmpLogs2('sp_check_del_sam',array(
																	'uuid'=>$sam['uuid'],
																	'fmediaid'=>$sam['fmediaid'],
																	'fid'=>$sam['fid'],
																	'ai_api_ret'=>$reqStr,
					
																	)); #记录日志
				$needDelIssueCount = 0; #删除小表记录的数量
				for($ii=0;$ii>-6;$ii--){
					$month = date('Ym',strtotime($ii." months"));
					$issueTable = 't'.$sam['mm'].'issue_'.$month.'_'.substr($mediaInfo['media_region_id'],0,2);

					try{
						$needDelIssueCount += M($issueTable)->where(array('fsampleid'=>$sam['fid']))->delete();
					}catch( \ Exception $error2) { 
					
					}
				}

				$needDelIssue0Count = M('t'.$sam['mm'].'issue')
											->where(array('f'.$sam['mm'].'sampleid'=>$sam['fid']))
											->delete(); #删除大表记录的数量																		
				$ec = M('t'.$sam['mm'].'sample')->where(array('fid'=>$sam['fid']))->delete(); #删除样本
				
				M()->commit();	
				#M()->rollback();				
			}
			
			
			
			echo 	
					'	'.$sam['mm'] . 
					'	'.$mediaInfo['fid'] . 
					#'	' .$mediaInfo['fmedianame'].
					'	大表删除 ' .$needDelIssue0Count. 
					', 小表删除 '.$needDelIssueCount.
					', 样本删除状态 '.$ec.
					', 请求接口反馈状态 '.$reqArr['ret'].
					'	'."\n";
					
			
			
		}
		echo date('Y-m-d H:i:s'). "\n";
		echo 'end'. "\n";
		
	}
	
	
	
	
	
	/*
	获取媒介
	POST传入参数（json）：
	timestamp：时间戳，必须
	signature：动态签名，必须，算法为：md5(秘钥+timestamp)
	fmediaclassid：媒介类别，01=>电视，02=>广播,非必须，未传入该字段或该字段为空时将查询所有媒介
	fmedianame：媒介名称，非必须，该字段值不为空时将模糊匹配
	
	*/
	public function get_media(){
		header('Access-Control-Allow-Origin:*');
		$post_data = json_decode(file_get_contents('php://input'),true);//获得请求数据
		//$this->checkSignature($data['timestamp'],$data['signature']);
		$fmediaclassid = $post_data['fmediaclassid'];
		if(!$fmediaclassid){
			$fmediaclassid = I('get.fmediaclassid');
		}
		$where = array();
		//$where['fdpid'] = array('gt',0);//平台id大于0 
		$where['priority'] = array('egt',-1);//优先级
		
		
		if($fmediaclassid == '01'){
			$where['left(fmediaclassid,2)'] = '01';
		}elseif($fmediaclassid == '02'){
			$where['left(fmediaclassid,2)'] = '02';
			
		}elseif($fmediaclassid == '03'){
			$where['left(fmediaclassid,2)'] = '03';
			
		}else{
			$where['left(fmediaclassid,2)'] = array('in','01,02');
		}
		
		$media_id = $post_data['media_id'];
		if($media_id == '') $media_id = I('media_id');
		$fmedianame = $post_data['fmedianame'];
		if($fmedianame == '') $fmedianame = I('fmedianame');
		
		
		if($media_id) $where['tmedia.fid'] = $media_id; 
		
		if($fmedianame != '') $where['fmedianame'] = array('like','%'.$fmedianame.'%');
		$mediaList = M('tmedia')
								->cache(true,600)
								->field('
										tmedia.fid,
										tmedia.fmediaclassid,

										tmedia.fmediacode,
										tmedia.fmedianame,
										tmedia.live_m3u8,
										tmediaowner.fregionid,
										tregion.fname as region_name,
										tmedia.valid_start_time,
										tmedia.valid_end_time,
										tmedia.priority,
										tmedia.fdeviceid,
										tmedia.main_media_id,
										tmedia.fjpgfilename as icon,
										tdevice.fonline as device_online,
										tdevice.fcode as device_code,
										tmedia.fcollecttype
										
										')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregion on tregion.fid = tmediaowner.fregionid')
								->join('tdevice on tdevice.fid = tmedia.fdeviceid')
								->where($where)
								->select();
		$this->ajaxReturn(array('code'=>0,'mediaList'=>$mediaList,'mediaCount'=>count($mediaList)));						
	}						
	
	
	
	/*接口测试*/
	public function api_test(){
		
		
		$url = 'http://'.$_SERVER['HTTP_HOST'].'/Api/Ai';
		$secret = '5Qb9a;gfeT65mNbG4U5aw5p6z68gv7uep0gdichx56';//秘钥
		$timestamp = time();//当前时间戳
		$signature = md5($secret.$timestamp);//签名
		$post_data = array();
		$post_data['timestamp'] = $timestamp;
		$post_data['signature'] = $signature;
		$post_data_json = json_encode($post_data);
		
		
		$rr = http($url,$post_data_json,'POST');
		//var_dump($post_data_json);
		echo $rr;
		
	}
	
	
	
	
	/*校验与AI的发布记录差异并修正*/
	public function check_ai_issue(){
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		set_time_limit(120);
		for($i=0;$i<=20;$i++){
			
			$this->check_ai_issue_do();
		}

	}
	
	/*校验与AI的发布记录差异并修正*/
	public function check_ai_issue_do(){
		
		set_time_limit(60);
		$aiApiUrl = C('AI_DOMAIN').'uuid/summary';//请求发布记录列表的地址
		
		$ai_syn_evolve = A('Common/System','Model')->important_data('ai_syn_evolve');//获取AI同步进展
		$ai_syn_evolve = json_decode($ai_syn_evolve,true);//转为数组
		if(!$ai_syn_evolve){//如果是空的
			$issue_date = date('Y-m-d',time() - 86400*8);
			$media_id = 0;
			
		}else{
			$issue_date = $ai_syn_evolve['issue_date'];//存储的上传处理的日期
			$media_id = $ai_syn_evolve['media_id'];//存储的上次处理到哪个媒介了
		}
		
		$mediaInfo = M('tmedia')->field('fid')->where(array('fid'=>array('gt',$media_id),'priority'=>array('egt',0),'fcollecttype'=>10))->order('fid')->find();//取出一个媒介
		
		if(!$mediaInfo){//如果取出媒介已经为空了
			
			if((time() - strtotime($issue_date) ) < 86400*1){//判断日期是否处理到了最近一天
				A('Common/System','Model')->important_data('ai_syn_evolve','{}');
			}else{
				A('Common/System','Model')->important_data('ai_syn_evolve',json_encode(array('issue_date'=>date('Y-m-d',strtotime($issue_date) + 86400),'media_id'=>0)));//不是最近一天
			}

			exit;
		}else{
			A('Common/System','Model')->important_data('ai_syn_evolve',json_encode(array('issue_date'=>$issue_date,'media_id'=>$mediaInfo['fid'])));
		}

		
		$ai_request_data = array();
		$ai_request_data['channel'] = $mediaInfo['fid'];//媒介id
		$ai_request_data['start'] = strtotime($issue_date);//开始时间
		$ai_request_data['end'] = strtotime($issue_date) + 86399;//结束时间
		
		
		$aiRet = http($aiApiUrl,$ai_request_data,'GET',10);//获取请求
		//var_dump($aiApiUrl);
		//var_dump($aiRet);
		$aiRet = json_decode($aiRet,true);//转为数组
		

		
		$add_issue_data = array();
		$add_issue_data['mediaId'] = $aiRet['ret']['mediaId'];
		$add_issue_data['issueList'] = $aiRet['ret']['issueList'];
		echo '日期:'.$issue_date.'	媒介ID:'.$aiRet['ret']['mediaId'].'	数量:'.count($aiRet['ret']['issueList'])."\n";
		
		
		
		$add_issue_ret = http(U('Open/Ai/add_issue@'.$_SERVER['HTTP_HOST']),json_encode($add_issue_data),'POST',false,30);//向添加发布数据接口推送数据
		
	}
	
	
	
	
	
	

}