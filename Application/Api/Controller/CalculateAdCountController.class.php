<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;

class CalculateAdCountController extends Controller {
	
	public function _initialize() {
		set_time_limit(120);
		session_write_close();
		header("Content-type: text/html; charset=utf-8");
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}
	
	
	
	/*按年统计广告发布条数*/
	public function tbn_ad_summary_year(){

		$confirmed = I('confirmed');
		$dataList = M('tbn_ad_summary_year'.$confirmed)
											->field('fid,fmediaid,fdate,left(fmedia_class_code,2) as media_class,fregionid,fad_class_code')
											->where(array('calc_sam'=>0))
											->limit(1000)->select();
		foreach($dataList as $data){
			$start_time = strtotime($data['fdate']);//开始时间，秒级时间戳
			$end_time = strtotime("+1 years",strtotime($data['fdate']))-86400;//结束时间戳
			
			$adCount = A('Api/CalculateAdCount','Model')//获取该区间的样本数和违法样本数
							->CalculateAdCount($data['fregionid'],$data['fmediaid'],$data['media_class'],$data['fad_class_code'],$start_time,$end_time,$confirmed);
			if(($adCount['ill_total'] + $adCount['nill']) < 0){
				//M('tbn_ad_summary_year'.$confirmed)->where(array('fid'=>$data['fid']))->delete();//如果广告数据等于0就删除这条记录
			}else{
				M('tbn_ad_summary_year'.$confirmed)
									->where(array('fid'=>$data['fid']))
									->save(array(
												'fad_count'=>($adCount['ill_total'] + $adCount['nill']),//广告总数
												'fad_illegal_count'=>$adCount['ill'],//严重违法广告数
												'fad_illegal_count_total'=>$adCount['ill_total'],//违法违法广告总数
												'calc_sam'=>1
												));
			}								
		}

	}
	
	/*按半年统计广告发布条数*/
	public function tbn_ad_summary_half_year(){
		$confirmed = I('confirmed');
		$dataList = M('tbn_ad_summary_half_year'.$confirmed)
											->field('fid,fmediaid,fdate,left(fmedia_class_code,2) as media_class,fregionid,fad_class_code')
											->where(array('calc_sam'=>0))
											->limit(1500)->select();
											
		foreach($dataList as $data){
			$start_time = strtotime($data['fdate']);//开始时间，秒级时间戳
			$end_time = strtotime("+6 months",strtotime($data['fdate']))-86400;//结束时间戳
			
			
			$adCount = A('Api/CalculateAdCount','Model')//获取该区间的样本数和违法样本数
							->CalculateAdCount($data['fregionid'],$data['fmediaid'],$data['media_class'],$data['fad_class_code'],$start_time,$end_time,$confirmed);
			
			if(($adCount['ill_total'] + $adCount['nill']) < 0){
				//M('tbn_ad_summary_half_year'.$confirmed)->where(array('fid'=>$data['fid']))->delete();//如果广告数据等于0就删除这条记录
			}else{
				M('tbn_ad_summary_half_year'.$confirmed)
									->where(array('fid'=>$data['fid']))
									->save(array(
												'fad_count'=>($adCount['ill_total'] + $adCount['nill']),//广告总数
												'fad_illegal_count'=>$adCount['ill'],//严重违法广告数
												'fad_illegal_count_total'=>$adCount['ill_total'],//违法违法广告总数
												'calc_sam'=>1
												));
			}								
		}
		
		
		
		
	}
	
	/*按月统计广告发布条数*/
	public function tbn_ad_summary_month(){
		$confirmed = I('confirmed');
		$dataList = M('tbn_ad_summary_month'.$confirmed)
											->field('fid,fmediaid,fdate,left(fmedia_class_code,2) as media_class,fregionid,fad_class_code')
											->where(array('calc_sam'=>0))
											->limit(2000)->select();
		foreach($dataList as $data){
			$start_time = strtotime($data['fdate']);//开始时间，秒级时间戳
			$end_time = strtotime("+1 months",strtotime($data['fdate']))-86400;//结束时间戳
		
			$adCount = A('Api/CalculateAdCount','Model')//获取该区间的样本数和违法样本数
							->CalculateAdCount($data['fregionid'],$data['fmediaid'],$data['media_class'],$data['fad_class_code'],$start_time,$end_time,$confirmed);
			
			if(($adCount['ill_total'] + $adCount['nill']) < 0){
				//M('tbn_ad_summary_month'.$confirmed)->where(array('fid'=>$data['fid']))->delete();//如果广告数据等于0就删除这条记录
			}else{
				M('tbn_ad_summary_month'.$confirmed)
									->where(array('fid'=>$data['fid']))
									->save(array(
												'fad_count'=>($adCount['ill_total'] + $adCount['nill']),//广告总数
												'fad_illegal_count'=>$adCount['ill'],//严重违法广告数
												'fad_illegal_count_total'=>$adCount['ill_total'],//违法违法广告总数
												'calc_sam'=>1
												));
			}						
		}
		
		
		
		
	}
	
	/*按半月统计广告发布条数*/
	public function tbn_ad_summary_half_month(){
		$confirmed = I('confirmed');
		$dataList = M('tbn_ad_summary_half_month'.$confirmed)
											->field('fid,fmediaid,fdate,left(fmedia_class_code,2) as media_class,fregionid,fad_class_code')
											->where(array('calc_sam'=>0))
											->limit(2500)->select();
		//var_dump($dataList);									
		foreach($dataList as $data){
			$start_time = strtotime($data['fdate']);//开始时间，秒级时间戳
			if(date('d',$start_time) == '01'){
				$end_time = $start_time + 14*86400;//结束时间戳
			}else{
				$end_time_l = strtotime("+1 months",strtotime($data['fdate']));//结束时间戳
				$end_time_l = date('Y-m-01',$end_time_l);
				$end_time = strtotime($end_time_l) - 86400;
			}
			//var_dump(date('Y-m-d H:i:s',$start_time));
			//var_dump(date('Y-m-d H:i:s',$end_time));

			
		
			$adCount = A('Api/CalculateAdCount','Model')//获取该区间的样本数和违法样本数
							->CalculateAdCount($data['fregionid'],$data['fmediaid'],$data['media_class'],$data['fad_class_code'],$start_time,$end_time,$confirmed);
			
			if(($adCount['ill_total'] + $adCount['nill']) < 0){
				//M('tbn_ad_summary_half_month'.$confirmed)->where(array('fid'=>$data['fid']))->delete();//如果广告数据等于0就删除这条记录
			}else{
				M('tbn_ad_summary_half_month'.$confirmed)
									->where(array('fid'=>$data['fid']))
									->save(array(
												'fad_count'=>($adCount['ill_total'] + $adCount['nill']),//广告总数
												'fad_illegal_count'=>$adCount['ill'],//严重违法广告数
												'fad_illegal_count_total'=>$adCount['ill_total'],//违法违法广告总数
												'calc_sam'=>1
												));
			}								
		}
		
		
		
		
	}
	
	
	/*按季度统计广告发布条数*/
	public function tbn_ad_summary_quarter(){
		$confirmed = I('confirmed');
		
		$dataList = M('tbn_ad_summary_quarter'.$confirmed)
											->field('fid,fmediaid,fdate,left(fmedia_class_code,2) as media_class,fregionid,fad_class_code')
											->where(array('calc_sam'=>0))
											->limit(1500)->select();
		//var_dump($dataList);									
		foreach($dataList as $data){
			$start_time = strtotime($data['fdate']);//开始时间，秒级时间戳
			$end_time = strtotime("+3 months",strtotime($data['fdate']))-86400;//结束时间戳
			//var_dump(date('Y-m-d H:i:s',$start_time));
			//var_dump(date('Y-m-d H:i:s',$end_time));

			
		
			$adCount = A('Api/CalculateAdCount','Model')//获取该区间的样本数和违法样本数
							->CalculateAdCount($data['fregionid'],$data['fmediaid'],$data['media_class'],$data['fad_class_code'],$start_time,$end_time,$confirmed);
			
			if(($adCount['ill_total'] + $adCount['nill']) < 0){
				//M('tbn_ad_summary_quarter'.$confirmed)->where(array('fid'=>$data['fid']))->delete();//如果广告数据等于0就删除这条记录
			}else{
				M('tbn_ad_summary_quarter'.$confirmed)
									->where(array('fid'=>$data['fid']))
									->save(array(
												'fad_count'=>($adCount['ill_total'] + $adCount['nill']),//广告总数
												'fad_illegal_count'=>$adCount['ill'],//严重违法广告数
												'fad_illegal_count_total'=>$adCount['ill_total'],//违法违法广告总数
												'calc_sam'=>1
												));
			}								
		}
		
		
		
		
	}
	
	
	/*按周统计广告发布条数*/
	public function tbn_ad_summary_week(){
		$confirmed = I('confirmed');
		$dataList = M('tbn_ad_summary_week'.$confirmed)
											->field('fid,fmediaid,fweek,left(fmedia_class_code,2) as media_class,fregionid,fad_class_code')
											->where(array('calc_sam'=>0))
											->limit(3000)->select();
		foreach($dataList as $data){
			
			$weekday = weekday(substr($data['fweek'],0,4),substr($data['fdate'],4,2));

			$start_time = $weekday['start'];//开始时间，秒级时间戳
			$end_time = $weekday['end'] - 86399;//结束时间戳

			$adCount = A('Api/CalculateAdCount','Model')//获取该区间的样本数和违法样本数
							->CalculateAdCount($data['fregionid'],$data['fmediaid'],$data['media_class'],$data['fad_class_code'],$start_time,$end_time,$confirmed);
			
			if(($adCount['ill_total'] + $adCount['nill']) < 0){
				//M('tbn_ad_summary_week'.$confirmed)->where(array('fid'=>$data['fid']))->delete();//如果广告数据等于0就删除这条记录
			}else{
				M('tbn_ad_summary_week'.$confirmed)
									->where(array('fid'=>$data['fid']))
									->save(array(
												'fad_count'=>($adCount['ill_total'] + $adCount['nill']),//广告总数
												'fad_illegal_count'=>$adCount['ill'],//严重违法广告数
												'fad_illegal_count_total'=>$adCount['ill_total'],//违法违法广告总数
												'calc_sam'=>1
												));
			}									
		}
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}