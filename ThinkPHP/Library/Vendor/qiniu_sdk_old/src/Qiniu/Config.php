<?php
namespace Qiniu;

use Qiniu\Zone;

final class Config
{
    const SDK_VER = '7.1.3';

    const BLOCK_SIZE = 4194304; //4*1024*1024 分块上传块大小，该参数为接口规格，不能修改

	const RSF_HOST = 'rsf.qiniu.com';
    const API_HOST = 'api.qiniu.com';
    const RS_HOST = 'rs.qiniu.com';      //RS Host
    const UC_HOST = 'https://uc.qbox.me';              //UC Host

    public $zone;

    public function __construct(Zone $z = null)         // 构造函数，默认为zone0
    {
        // if ($z === null) {
            $this->zone = new Zone();
        // }
    }
}
