const addEditDialog = {
  name: 'addEditDialog',
  props: {
    data: {
      type: Object,
      default: {}
    },
    customer: {
      type: Array,
      default: []
    }
  },
  template: `
    <el-dialog
      :title="form.fmediacodename || '添加主媒体'"
      :visible.sync="show"
      center
      width="600px"
      :close-on-click-modal="false"
      :close-on-press-escape="false"
      @close="() => $emit('close')">
        <el-form :model="form" :rules="rules" ref="form" label-width="120px">
          <el-form-item label="用户编号" prop="fuserid">
            <el-select v-model="form.fuserid">
              <el-option v-for="item in customer"
                :key="item.userid"
                :label="item.name"
                :value="item.userid">
              </el-option>
            </el-select>
          </el-form-item>
          <el-form-item label="媒体编号" prop="fmediaid">
            <el-input v-model="form.fmediaid" clearable></el-input>
          </el-form-item>
          <el-form-item label="媒体类型" prop="fmediatype">
            <el-select v-model="form.fmediatype">
              <el-option v-for="(value,key) in mediaType"
                :key="key"
                :label="value"
                :value="key">
              </el-option>
            </el-select>
          </el-form-item>
          <el-form-item label="客户媒体编号" prop="fmediacode">
            <el-input v-model="form.fmediacode" clearable></el-input>
          </el-form-item>
          <el-form-item label="客户媒体名称" prop="fmediacodename">
            <el-input v-model="form.fmediacodename" clearable></el-input>
          </el-form-item>
          <el-form-item label="阈值" prop="fthreshold" prop="fthreshold">
            <el-input-number v-model="form.fthreshold" :min="0"></el-input-number>
          </el-form-item>
          <el-form-item label="任务等级" prop="ftasklevel">
            <el-input-number v-model="form.ftasklevel" :min="0"></el-input-number>
          </el-form-item>
          <el-form-item label="开启状态">
            <el-switch
              v-model="form.fstatus"
              active-text="开启"
              inactive-text="关闭"
              active-value="1"
              inactive-value="0">
          </el-switch>
          </el-form-item>
        </el-form>
      <span slot="footer">
        <el-button type="primary" @click="submit">提 交</el-button>
        <el-button @click="() => $emit('close')">取 消</el-button>
      </span>
    </el-dialog>
  `,
  data(){
    return {
      show: true,
      form: {
        fid: '',
        fuserid: '',
        fmediaid: '',
        fmediatype: '',
        fmediacode: '',
        fmediacodename: '',
        fthreshold: '',
        ftasklevel: '',
        fmaincode: '',
        fstatus: '1'
      },
      rules: {
        fuserid: [{ required: true, message: '请选择用户编号', trigger: 'change' }],
        fmediaid: [{ required: true, message: '请输入媒体编号', trigger: 'blur' }],
        fmediatype: [{ required: true, message: '请选择媒体类型', trigger: 'change' }],
        fmediacodename: [{ required: true, message: '请输入客户媒体名称', trigger: 'blur' }],
        fmediacode: [{ required: true, message: '请输入客户媒体编号', trigger: 'blur' }],
      },
      mediaType: {
        '1': '电视',
        '2': '广播',
        '3': '报纸',
        '4': '互联网',
      },
    }
  },
  created() {
    this.initData()
  },
  methods: {
    initData() {
      if(this.data.fid){
        return Object.keys(this.form).forEach(i => {
          this.form[i] = this.data[i]
        })
      }
      return false
    },
    submit() {
      this.$refs.form.validate((valid) => {
        if (valid) {
          $.post('/Admin/Thirdmedia/addedit_media', this.form).then(res => {
            if (res.code === 0) {
              // 如果传入 true 则关闭后刷新列表
              this.$emit('close',true)
            } else {
              this.$message.error(res.msg)
            }
          })
        }
      })
    },
  },
}