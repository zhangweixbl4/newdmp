<?php
namespace Agp\Controller;
use Think\Controller;
use Agp\Model\StatisticalReportModel;
use Agp\Model\StatisticalModel;

import('Vendor.PHPExcel');

class OutXlsNJController extends BaseController{

	/**
	 * 生成交办表
	 * by zw
	 */
    public function create_taizhanglist(){
    	session_write_close();
    	
		header("Content-type: text/html; charset=utf-8"); 
	    $objPHPExcel = new \PHPExcel();  
	    $objPHPExcel
	      ->getProperties()  //获得文件属性对象，给下文提供设置资源
	      ->setCreator( "MaartenBalliauw")             //设置文件的创建者
	      ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
	      ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
	      ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
	      ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
	      ->setKeywords( "office 2007 openxmlphp")        //设置标记
	      ->setCategory( "Test resultfile");                //设置类别

	    $fname     = I('fname');//广告名称
        $fnumber = I('fnumber');//文号
        $ffromtype  = I('ffromtype');//来源分类
        $fmedianame = I('fmedianame');//发布媒体
        $djrqst = I('djrqst');//交办开始日期
        $djrqed = I('djrqed');//交办结束日期
        $sfbj   = I('sfbj');//是否办结,0全部，10已办结，20未办结
        $rwzt   = I('rwzt');//任务状态，0全部，10未定，20一般，30紧急

        $where_fr['_string'] = '1=1';

        if(!empty($rwzt)){
            if($rwzt == 10){
                $where_fr['_string'] .= ' and a.fid not in (select ftk_tregisterid from nj_tregistertrack,nj_tregister where nj_tregistertrack.ftk_tregisterid=nj_tregister.fid and (ftk_trepid='.session('regulatorpersonInfo.fregulatorpid').' or ftk_eqtrepid='.session('regulatorpersonInfo.fregulatorpid').') group by ftk_tregisterid)';
            }elseif($rwzt == 20){
                $where_fr['_string'] .= ' and a.fid in (select ftk_tregisterid from nj_tregistertrack,nj_tregister where nj_tregistertrack.ftk_tregisterid=nj_tregister.fid and (ftk_trepid='.session('regulatorpersonInfo.fregulatorpid').' or ftk_eqtrepid='.session('regulatorpersonInfo.fregulatorpid').') and TIMESTAMPDIFF(day,curdate(),ftk_endtime)>3 group by ftk_tregisterid)';
            }elseif($rwzt == 30){
                $where_fr['_string'] .= ' and a.fid in (select ftk_tregisterid from nj_tregistertrack,nj_tregister where nj_tregistertrack.ftk_tregisterid=nj_tregister.fid and (ftk_trepid='.session('regulatorpersonInfo.fregulatorpid').' or ftk_eqtrepid='.session('regulatorpersonInfo.fregulatorpid').') and TIMESTAMPDIFF(day,curdate(),ftk_endtime)<=3 group by ftk_tregisterid)';
            }
        }

	    if(!empty($fname)){
            $where_fr['a.fname'] = array('like','%'.$fname.'%');
        }
        if(!empty($fnumber)){
            $where_fr['a.fnumber'] = array('like','%'.$fnumber.'%');
        }
        if(!empty($ffromtype)){
            $where_fr['a.ffromtype'] = $ffromtype;
        }
        if(!empty($fmedianame)){
            $where_fr['tm.fmedianame'] = array('like','%'.$fmedianame.'%');
        }
        if(!empty($djrqst)&&!empty($djrqed)){
            $where_fr['a.fgivetime'] = array('between',array($djrqst,$djrqed));
        }
        if(!empty($sfbj)){
            if($sfbj == 10){
                $where_fr['a.fstate'] = 40;
            }elseif($sfbj==20){
                $where_fr['a.fstate'] = array('neq',40);
            }
        }
        $where_fr['a.fregisterregulatorpid'] = session('regulatorpersonInfo.fregulatorpid');

		$data = M('nj_tregister')
            ->field('a.*,b.fflowname, fgivetime,(fendtimes - TIMESTAMPDIFF(day,fgivetime,now())) as diffdays,tm.fmedianame')
            ->alias('a')
            ->join('tmedia tm on a.fmediaid=tm.fid')
            ->join('(select * from nj_tregisterflow where flowid in (select max(flowid) as wid from nj_tregisterflow group by fregisterid)) b on a.fid=b.fregisterid')
            ->where($where_fr)
            ->order('a.fid desc')
            ->select();
		$sheet = $objPHPExcel->setActiveSheetIndex(0);
		$sheet ->setCellValue('A1','序号');
		$sheet ->setCellValue('B1','交办编号');
		$sheet ->setCellValue('C1','广告名称');
        $sheet ->setCellValue('D1','广告主');
        $sheet ->setCellValue('E1','发布媒体');
        $sheet ->setCellValue('F1','发布时间');
		$sheet ->setCellValue('G1','发布条次');
        $sheet ->setCellValue('H1','线索来源');
        $sheet ->setCellValue('I1','举报人类别');
		$sheet ->setCellValue('J1','投诉方联系方式');
		$sheet ->setCellValue('K1','交办人');
        $sheet ->setCellValue('L1','交办单位');
        $sheet ->setCellValue('M1','交办时间');
		$sheet ->setCellValue('N1','回复时限');
        $sheet ->setCellValue('O1','交办进度');
        $sheet ->setCellValue('P1','投诉内容');
        $sheet ->setCellValue('Q1','处理状态');
        $sheet ->setCellValue('R1','反馈时间');
		$sheet ->setCellValue('S1','处罚方式');
		foreach ($data as $key => $value) {	
			$sheet ->setCellValue('A'.($key+2),$key+1);
            $sheet ->setCellValue('B'.($key+2),$value['fnumber']);
            $sheet ->setCellValue('C'.($key+2),$value['fname']);
            $sheet ->setCellValue('D'.($key+2),$value['fadowner']);
            $sheet ->setCellValue('E'.($key+2),$value['fmedianame']);
            $sheet ->setCellValue('F'.($key+2),date('Y-m-d',strtotime($value['fsendtime'])));
            $sheet ->setCellValue('G'.($key+2),$value['fsendcount']);
            if($value['ffromtype']==10){
                $sheet ->setCellValue('H'.($key+2),'广告监测');
            }elseif($value['ffromtype']==20){
                $sheet ->setCellValue('H'.($key+2),'网络或书面投申诉举报');
            }elseif($value['ffromtype']==30){
                $sheet ->setCellValue('H'.($key+2),'上级机关交办');
            }elseif($value['ffromtype']==40){
                $sheet ->setCellValue('H'.($key+2),'其他机关移送');
            }
            if($value['fpersontype']==10){
                $sheet ->setCellValue('I'.($key+2),'个人');
            }elseif($value['fpersontype']==20){
                $sheet ->setCellValue('I'.($key+2),'单位');
            }elseif($value['fpersontype']==30){
                $sheet ->setCellValue('I'.($key+2),'移送机关');
            }elseif($value['fpersontype']==40){
                $sheet ->setCellValue('I'.($key+2),'其他');
            }
            $sheet ->setCellValue('J'.($key+2),$value['fpersonmobile']);
            $sheet ->setCellValue('K'.($key+2),$value['fregisterpersonname']);
            $sheet ->setCellValue('L'.($key+2),$value['fregisterregulatorname']);
            $sheet ->setCellValue('M'.($key+2),date('Y-m-d',strtotime($value['fgivetime'])));
            $sheet ->setCellValue('N'.($key+2),$value['fendtimes'].'天');
            $sheet ->setCellValue('O'.($key+2),'剩余 '.$value['diffdays'].' 天');
            $sheet ->setCellValue('P'.($key+2),$value['fappeal']);
            if($value['fstate'] == 0){
                $sheet ->setCellValue('Q'.($key+2),'待登记');
            }elseif($value['fstate'] == 40){
                $sheet ->setCellValue('Q'.($key+2),'处理中');
            }else{
                $sheet ->setCellValue('Q'.($key+2),'已办结');
            }
            
            $sheet ->setCellValue('R'.($key+2),$value['fdealtime']);
			$sheet ->setCellValue('S'.($key+2),$value['fpunish']);
		}

		$objActSheet =$objPHPExcel->getActiveSheet();
		//给当前活动的表设置名称
		$objActSheet->setTitle('Sheet1');

		$objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$date = date('Ymdhis');
		$savefile = './Public/Xls/'.$date.'.xlsx';
        $objWriter->save($savefile);

		$ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		//即时导出下载
		// header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// header('Content-Disposition:attachment;filename="01simple.xlsx"');
		// header('Cache-Control:max-age=0');
		// $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
		// $objWriter->save( 'php://output');
    }


    /*
     * 南京专项报告
     * BY yjn
     * */
    public function create_special_report(){

        session_write_close();
        header("Content-type: text/html; charset=utf-8");
        $system_num = getconfig('system_num');
        $nj_media = M('tmedia_temp')->where('ftype=1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid'))->getField('fmediaid',true);
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别
        $user = session('regulatorpersonInfo');//获取用户信息
        $daytime_s = I('s_time',date('Y-m-d'));//接收时间
        $daytime_e = I('e_time',date('Y-m-d'));//接收时间
/*        $daytime_s = '2019-01-01';//接收时间
        $daytime_e = '2019-12-31';//接收时间*/
        //$month = I('daytime','2018-06');//接收时间
        $report_start_date = strtotime($daytime_s);//指定月份月初时间戳
        $report_end_date = strtotime($daytime_e) + 86399;//指定月份月末时间戳
        $StatisticalReport1 = New StatisticalReportModel();//实例化数据统计模型
        $StatisticalReport = New StatisticalModel();//实例化数据统计模型
        $usermedia = $StatisticalReport1->distinguish_media(implode(',',$nj_media),'320000');
        /*        if($report_end_date > time()){
                    $report_end_date = time();
                }*/

        $date_day = date('Y年m月',$report_end_date);
        $date_day_s = date('Y年m月d日',$report_start_date);
        $date_day_e = date('Y年m月d日',$report_end_date);
        $medias = I('medias','');

        $adtypes = I('adtypes');
        $pntype = I('pntype',50);
        $report_type = I('report_type','');

        if($report_type == ''){
            $report_type_array = [1,2,3,4];
        }else{
            $report_type_array = [$report_type];
        }

        $isszs = I('isszs','bs');
        //区分省直属和本市
        if($isszs == 'szs'){
            if($medias == ''){
                $medias = $usermedia[1];
            }
            $name = '省直属';
            $r_name = '江苏省';
            $region_id = '320000';
        }else{
            if($medias == ''){
                $medias = $usermedia[0];
            }
            $name = '南京市';
            $r_name = '南京市';
            $region_id = '320100';
        }

        $date_table = date('Ym',$report_end_date).'_'.substr($user['regionid'],0,2);//组合表
        //$date_table = date('Ym',strtotime('2019-09-30')).'_'.substr($user['regionid'],0,2);//组合表

        //
        $num = 0;
        foreach ($report_type_array as $report_type_key=>$report_type){
            $objPHPExcel->createSheet();
            $sheet = $objPHPExcel->setActiveSheetIndex($report_type_key);
            switch ($report_type){
                case 1:
                    $data1_2 = $StatisticalReport->report_ad_monitor('fregionid',$medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','',$adtypes);//当前客户全部互联网广告数据统计
                    //$data1[] = $data1_1[0];
                    $data1 = $data1_2;
                    $sheet ->setCellValue('A1','表1');
                    $sheet ->mergeCells("A2:K2");
                    $sheet ->mergeCells("A3:A4");
                    $sheet ->mergeCells("B3:B4");
                    $sheet ->mergeCells("C3:E3");
                    $sheet ->mergeCells("F3:H3");
                    $sheet ->mergeCells("I3:K3");
                    $report_name = $r_name.$date_day_s.'至'.$date_day_e.'广告监测统计情况(表'.$report_type.')';
                    $sheet ->setCellValue('A2',$report_name);
                    $sheet ->setCellValue('A3','序号');
                    $sheet ->setCellValue('B3','地域名称');
                    $sheet ->setCellValue('C3','广告条数');
                    $sheet ->setCellValue('F3','广告条次');
                    $sheet ->setCellValue('I3','广告时长(单位：秒)');
                    $sheet ->setCellValue('C4','总条数');
                    $sheet ->setCellValue('D4','违法条数');
                    $sheet ->setCellValue('E4','条数违法率');
                    $sheet ->setCellValue('F4','总条次');
                    $sheet ->setCellValue('G4','违法条次');
                    $sheet ->setCellValue('H4','条次违法率');
                    $sheet ->setCellValue('I4','总时长');
                    $sheet ->setCellValue('J4','违法时长');
                    $sheet ->setCellValue('K4','时长违法率');
                    $sum_data1 = [];
                    foreach ($data1 as $key => $value) {
                        $sum_data1['fad_count'] += $value['fad_count'];
                        $sum_data1['fad_illegal_count'] += $value['fad_illegal_count'];
                        $sum_data1['fad_times'] += $value['fad_times'];
                        $sum_data1['fad_illegal_times'] += $value['fad_illegal_times'];
                        $sum_data1['fad_play_len'] += $value['fad_play_len'];
                        $sum_data1['fad_illegal_play_len'] += $value['fad_illegal_play_len'];
                    }

                    $data_count = count($data1);
                    $sum_data1_row = ($data_count+5);
                    $sum_data1['counts_illegal_rate'] = (round($sum_data1['fad_illegal_count']/$sum_data1['fad_count'],4)*100).'%';
                    $sum_data1['times_illegal_rate'] = (round($sum_data1['fad_illegal_times']/$sum_data1['fad_times'],4)*100).'%';
                    $sum_data1['lens_illegal_rate'] = (round($sum_data1['fad_illegal_play_len']/$sum_data1['fad_play_len'],4)*100).'%';

                    $sheet ->setCellValue('A'.(5),1);
                    $sheet ->setCellValue('B'.(5),$r_name);
                    $sheet ->setCellValue('C'.(5),$sum_data1['fad_count']);
                    $sheet ->setCellValue('D'.(5),$sum_data1['fad_illegal_count']);
                    $sheet ->setCellValue('E'.(5),$sum_data1['counts_illegal_rate']);
                    $sheet ->setCellValue('F'.(5),$sum_data1['fad_times']);
                    $sheet ->setCellValue('G'.(5),$sum_data1['fad_illegal_times']);
                    $sheet ->setCellValue('H'.(5),$sum_data1['times_illegal_rate']);
                    $sheet ->setCellValue('I'.(5),$sum_data1['fad_play_len']);
                    $sheet ->setCellValue('J'.(5),$sum_data1['fad_illegal_play_len']);
                    $sheet ->setCellValue('K'.(5),$sum_data1['lens_illegal_rate']);

                    $sheet ->mergeCells("A".$sum_data1_row.":B".$sum_data1_row);
                    $sheet ->setCellValue('A'.$sum_data1_row,'全部地域');
                    $sheet ->setCellValue('C'.$sum_data1_row,$sum_data1['fad_count']);
                    $sheet ->setCellValue('D'.$sum_data1_row,$sum_data1['fad_illegal_count']);
                    $sheet ->setCellValue('E'.$sum_data1_row,$sum_data1['counts_illegal_rate']);
                    $sheet ->setCellValue('F'.$sum_data1_row,$sum_data1['fad_times']);
                    $sheet ->setCellValue('G'.$sum_data1_row,$sum_data1['fad_illegal_times']);
                    $sheet ->setCellValue('H'.$sum_data1_row,$sum_data1['times_illegal_rate']);
                    $sheet ->setCellValue('I'.$sum_data1_row,$sum_data1['fad_play_len']);
                    $sheet ->setCellValue('J'.$sum_data1_row,$sum_data1['fad_illegal_play_len']);
                    $sheet ->setCellValue('K'.$sum_data1_row,$sum_data1['lens_illegal_rate']);
                    $objActSheet =$objPHPExcel->getActiveSheet();
                    //给当前活动的表设置名称
                    $objActSheet->setTitle('广告监测统计情况(表'.$report_type.')');
                    break;
                case 2:
                    $data2_1 = $StatisticalReport->report_ad_monitor('fmedia_class_code',$medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','',$adtypes);//当前客户全部互联网广告数据统计
                    $data2_2 = $StatisticalReport->report_ad_monitor('fmediaid',$medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','01',$adtypes);//当前客户全部互联网广告数据统计
                    $data2_3 = $StatisticalReport->report_ad_monitor('fmediaid',$medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','02',$adtypes);//当前客户全部互联网广告数据统计
                    $data2_4 = $StatisticalReport->report_ad_monitor('fmediaid',$medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','03',$adtypes);//当前客户全部互联网广告数据统计
                    $sheet ->setCellValue('A1','表2');
                    $sheet ->mergeCells("A2:J2");
                    $sheet ->mergeCells("A3:J3");
                    $sheet ->mergeCells("A4:A5");
                    $sheet ->mergeCells("B4:D4");
                    $sheet ->mergeCells("E4:G4");
                    $sheet ->mergeCells("H4:J4");
                    $report_name = $r_name.$date_day_s.'至'.$date_day_e.'涉嫌违法广告监测统计情况(表'.$report_type.')';
                    $sheet ->setCellValue('A2',$report_name);
                    $sheet ->setCellValue('A3','（一）'.$name.'广告监测统计情况汇总');
                    $sheet ->setCellValue('A4','类别');
                    $sheet ->setCellValue('B4','广告条数');
                    $sheet ->setCellValue('E4','广告条次');
                    $sheet ->setCellValue('H4','广告时长(单位：秒)');
                    $sheet ->setCellValue('B5','总条数');
                    $sheet ->setCellValue('C5','违法条数');
                    $sheet ->setCellValue('D5','条数违法率');
                    $sheet ->setCellValue('E5','总条次');
                    $sheet ->setCellValue('F5','违法条次');
                    $sheet ->setCellValue('G5','条次违法率');
                    $sheet ->setCellValue('H5','总时长');
                    $sheet ->setCellValue('I5','违法时长');
                    $sheet ->setCellValue('J5','时长违法率');
                    $sum_data2_1 = [];
                    foreach ($data2_1 as $key => $value) {

                        $sheet ->setCellValue('A'.($key+6),!empty($value['tmediaclass_name']) ? $value['tmediaclass_name'] : $value['titlename']);
                        $sheet ->setCellValue('B'.($key+6),$value['fad_count']);
                        $sheet ->setCellValue('C'.($key+6),$value['fad_illegal_count']);
                        $sheet ->setCellValue('D'.($key+6),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('E'.($key+6),$value['fad_times']);
                        $sheet ->setCellValue('F'.($key+6),$value['fad_illegal_times']);
                        $sheet ->setCellValue('G'.($key+6),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('H'.($key+6),$value['fad_play_len']);
                        $sheet ->setCellValue('I'.($key+6),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('J'.($key+6),$value['lens_illegal_rate'].'%');
                    }
                    $data2_1_count = count($data2_1);
                    $sum_data2_1_row = $data2_1_count + 6;

                    $sum_data2_2_start_row = $sum_data2_1_row + 3;
                    $sheet ->mergeCells("A".$sum_data2_2_start_row.":J".$sum_data2_2_start_row);
                    $sheet ->mergeCells("A".($sum_data2_2_start_row+1).":A".($sum_data2_2_start_row+2));
                    $sheet ->mergeCells("B".($sum_data2_2_start_row+1).":D".($sum_data2_2_start_row+1));
                    $sheet ->mergeCells("E".($sum_data2_2_start_row+1).":G".($sum_data2_2_start_row+1));
                    $sheet ->mergeCells("H".($sum_data2_2_start_row+1).":J".($sum_data2_2_start_row+1));
                    $sheet ->setCellValue('A'.$sum_data2_2_start_row,'（二）'.$name.'电视广告监测统计情况');
                    $sheet ->setCellValue('A'.($sum_data2_2_start_row+1),'媒体名称');
                    $sheet ->setCellValue('B'.($sum_data2_2_start_row+1),'广告条数');
                    $sheet ->setCellValue('E'.($sum_data2_2_start_row+1),'广告条次');
                    $sheet ->setCellValue('H'.($sum_data2_2_start_row+1),'广告时长(单位：秒)');
                    $sheet ->setCellValue('B'.($sum_data2_2_start_row+2),'总条数');
                    $sheet ->setCellValue('C'.($sum_data2_2_start_row+2),'违法条数');
                    $sheet ->setCellValue('D'.($sum_data2_2_start_row+2),'条数违法率');
                    $sheet ->setCellValue('E'.($sum_data2_2_start_row+2),'总条次');
                    $sheet ->setCellValue('F'.($sum_data2_2_start_row+2),'违法条次');
                    $sheet ->setCellValue('G'.($sum_data2_2_start_row+2),'条次违法率');
                    $sheet ->setCellValue('H'.($sum_data2_2_start_row+2),'总时长');
                    $sheet ->setCellValue('I'.($sum_data2_2_start_row+2),'违法时长');
                    $sheet ->setCellValue('J'.($sum_data2_2_start_row+2),'时长违法率');
                    $sum_data2_2 = [];
                    foreach ($data2_2 as $key => $value) {

                        $sheet ->setCellValue('A'.($key+$sum_data2_2_start_row+3),!empty($value['tmedia_name']) ? $value['tmedia_name'] : $value['titlename']);
                        $sheet ->setCellValue('B'.($key+$sum_data2_2_start_row+3),$value['fad_count']);
                        $sheet ->setCellValue('C'.($key+$sum_data2_2_start_row+3),$value['fad_illegal_count']);
                        $sheet ->setCellValue('D'.($key+$sum_data2_2_start_row+3),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('E'.($key+$sum_data2_2_start_row+3),$value['fad_times']);
                        $sheet ->setCellValue('F'.($key+$sum_data2_2_start_row+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('G'.($key+$sum_data2_2_start_row+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('H'.($key+$sum_data2_2_start_row+3),$value['fad_play_len']);
                        $sheet ->setCellValue('I'.($key+$sum_data2_2_start_row+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('J'.($key+$sum_data2_2_start_row+3),$value['lens_illegal_rate'].'%');
                    }
                    $sum_data2_2_count = count($data2_2);
                    $sum_data2_2_row = $sum_data2_2_count + $sum_data2_2_start_row + 3;

                    $sum_data2_3_start_row = $sum_data2_2_row + 3;
                    $sheet ->mergeCells("A".$sum_data2_3_start_row.":J".$sum_data2_3_start_row);
                    $sheet ->mergeCells("A".($sum_data2_3_start_row+1).":A".($sum_data2_3_start_row+2));
                    $sheet ->mergeCells("B".($sum_data2_3_start_row+1).":D".($sum_data2_3_start_row+1));
                    $sheet ->mergeCells("E".($sum_data2_3_start_row+1).":G".($sum_data2_3_start_row+1));
                    $sheet ->mergeCells("H".($sum_data2_3_start_row+1).":J".($sum_data2_3_start_row+1));
                    $sheet ->setCellValue('A'.$sum_data2_3_start_row,'（三）'.$name.'广播广告监测统计情况');
                    $sheet ->setCellValue('A'.($sum_data2_3_start_row+1),'媒体名称');
                    $sheet ->setCellValue('B'.($sum_data2_3_start_row+1),'广告条数');
                    $sheet ->setCellValue('E'.($sum_data2_3_start_row+1),'广告条次');
                    $sheet ->setCellValue('H'.($sum_data2_3_start_row+1),'广告时长(单位：秒)');
                    $sheet ->setCellValue('B'.($sum_data2_3_start_row+2),'总条数');
                    $sheet ->setCellValue('C'.($sum_data2_3_start_row+2),'违法条数');
                    $sheet ->setCellValue('D'.($sum_data2_3_start_row+2),'条数违法率');
                    $sheet ->setCellValue('E'.($sum_data2_3_start_row+2),'总条次');
                    $sheet ->setCellValue('F'.($sum_data2_3_start_row+2),'违法条次');
                    $sheet ->setCellValue('G'.($sum_data2_3_start_row+2),'条次违法率');
                    $sheet ->setCellValue('H'.($sum_data2_3_start_row+2),'总时长');
                    $sheet ->setCellValue('I'.($sum_data2_3_start_row+2),'违法时长');
                    $sheet ->setCellValue('J'.($sum_data2_3_start_row+2),'时长违法率');
                    $sum_data2_3 = [];
                    foreach ($data2_3 as $key => $value) {

                        $sheet ->setCellValue('A'.($key+$sum_data2_3_start_row+3),!empty($value['tmedia_name']) ? $value['tmedia_name'] : $value['titlename']);
                        $sheet ->setCellValue('B'.($key+$sum_data2_3_start_row+3),$value['fad_count']);
                        $sheet ->setCellValue('C'.($key+$sum_data2_3_start_row+3),$value['fad_illegal_count']);
                        $sheet ->setCellValue('D'.($key+$sum_data2_3_start_row+3),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('E'.($key+$sum_data2_3_start_row+3),$value['fad_times']);
                        $sheet ->setCellValue('F'.($key+$sum_data2_3_start_row+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('G'.($key+$sum_data2_3_start_row+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('H'.($key+$sum_data2_3_start_row+3),$value['fad_play_len']);
                        $sheet ->setCellValue('I'.($key+$sum_data2_3_start_row+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('J'.($key+$sum_data2_3_start_row+3),$value['lens_illegal_rate'].'%');
                    }
                    $sum_data2_3_count = count($data2_3);
                    $sum_data2_3_row = $sum_data2_3_count + $sum_data2_3_start_row + 3;

                    $sum_data2_4_start_row = $sum_data2_3_row + 3;
                    $sheet ->mergeCells("A".$sum_data2_4_start_row.":J".$sum_data2_4_start_row);
                    $sheet ->mergeCells("A".($sum_data2_4_start_row+1).":A".($sum_data2_4_start_row+2));
                    $sheet ->mergeCells("B".($sum_data2_4_start_row+1).":D".($sum_data2_4_start_row+1));
                    $sheet ->mergeCells("E".($sum_data2_4_start_row+1).":G".($sum_data2_4_start_row+1));
                    $sheet ->setCellValue('A'.$sum_data2_4_start_row,'（四）'.$name.'报纸广告监测统计情况');
                    $sheet ->setCellValue('A'.($sum_data2_4_start_row+1),'媒体名称');
                    $sheet ->setCellValue('B'.($sum_data2_4_start_row+1),'广告条数');
                    $sheet ->setCellValue('E'.($sum_data2_4_start_row+1),'广告条次');
                    $sheet ->setCellValue('B'.($sum_data2_4_start_row+2),'总条数');
                    $sheet ->setCellValue('C'.($sum_data2_4_start_row+2),'违法条数');
                    $sheet ->setCellValue('D'.($sum_data2_4_start_row+2),'条数违法率');
                    $sheet ->setCellValue('E'.($sum_data2_4_start_row+2),'总条次');
                    $sheet ->setCellValue('F'.($sum_data2_4_start_row+2),'违法条次');
                    $sheet ->setCellValue('G'.($sum_data2_4_start_row+2),'条次违法率');
                    $sum_data2_4 = [];
                    foreach ($data2_4 as $key => $value) {

                        $sheet ->setCellValue('A'.($key+$sum_data2_4_start_row+3),!empty($value['tmedia_name']) ? $value['tmedia_name'] : $value['titlename']);
                        $sheet ->setCellValue('B'.($key+$sum_data2_4_start_row+3),$value['fad_count']);
                        $sheet ->setCellValue('C'.($key+$sum_data2_4_start_row+3),$value['fad_illegal_count']);
                        $sheet ->setCellValue('D'.($key+$sum_data2_4_start_row+3),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('E'.($key+$sum_data2_4_start_row+3),$value['fad_times']);
                        $sheet ->setCellValue('F'.($key+$sum_data2_4_start_row+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('G'.($key+$sum_data2_4_start_row+3),$value['times_illegal_rate'].'%');
                    }

                    $objActSheet =$objPHPExcel->getActiveSheet();
                    //给当前活动的表设置名称
                    $objActSheet->setTitle('涉嫌违法广告监测统计情况(表'.$report_type.')');
                    break;
                case 3:
                    $data3 = $StatisticalReport->ad_illegal_customize($report_start_date,$report_end_date,$medias,$adtypes);
                    $sheet ->setCellValue('A1','表3');
                    $sheet ->mergeCells("A2:I2");
                    $report_name = $r_name.$date_day_s.'至'.$date_day_e.'涉嫌违法广告监测线索(表'.$report_type.')';
                    $sheet ->setCellValue('A2',$report_name);
                    $sheet ->setCellValue('A3','序号');
                    $sheet ->setCellValue('B3','媒体类别');
                    $sheet ->setCellValue('C3','发布媒体');
                    $sheet ->setCellValue('D3','广告类别');
                    $sheet ->setCellValue('E3','广告名称');
                    $sheet ->setCellValue('F3','发布日期');
                    $sheet ->setCellValue('G3','发布条次');
                    $sheet ->setCellValue('H3','涉嫌违法条款');
                    $sheet ->setCellValue('I3','违法描述');
                    $sum_data3 = [];
                    foreach ($data3 as $key => $value) {

                        $sheet ->setCellValue('A'.($key+4),($key+1));
                        $sheet ->setCellValue('B'.($key+4),$value['media_class']);
                        $sheet ->setCellValue('C'.($key+4),$value['fmedianame']);
                        $sheet ->setCellValue('D'.($key+4),$value['fadclass']);
                        $sheet ->setCellValue('E'.($key+4),$value['fad_name']);
                        $sheet ->setCellValue('F'.($key+4),$value['fissue_date']);
                        $sheet ->setCellValue('G'.($key+4),$value['total']);
                        $sheet ->setCellValue('H'.($key+4),$value['fillegal_code']);
                        $sheet ->setCellValue('I'.($key+4),$value['fillegal']);
                    }
                    $objActSheet =$objPHPExcel->getActiveSheet();
                    //给当前活动的表设置名称
                    $objActSheet->setTitle('涉嫌违法广告监测线索(表'.$report_type.')');
                    break;
                case 4:
                    $data4 = $StatisticalReport->report_ad_monitor('fad_class_code',$medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','',$adtypes);//当前客户全部互联网广告数据统计

                    $sheet ->setCellValue('A1','表4');
                    $sheet ->mergeCells("A2:K2");
                    $sheet ->mergeCells("A3:A4");
                    $sheet ->mergeCells("B3:B4");
                    $sheet ->mergeCells("C3:E3");
                    $sheet ->mergeCells("F3:H3");
                    $sheet ->mergeCells("I3:K3");
                    $report_name = $r_name.$date_day_s.'至'.$date_day_e.'涉嫌违法广告分类别情况(表'.$report_type.')';
                    $sheet ->setCellValue('A2',$report_name);
                    $sheet ->setCellValue('A3','序号');
                    $sheet ->setCellValue('B3','广告类别');
                    $sheet ->setCellValue('C3','广告条数');
                    $sheet ->setCellValue('F3','广告条次');
                    $sheet ->setCellValue('I3','广告时长(单位：秒)');
                    $sheet ->setCellValue('C4','总条数');
                    $sheet ->setCellValue('D4','违法条数');
                    $sheet ->setCellValue('E4','条数违法率');
                    $sheet ->setCellValue('F4','总条次');
                    $sheet ->setCellValue('G4','违法条次');
                    $sheet ->setCellValue('H4','条次违法率');
                    $sheet ->setCellValue('I4','总时长');
                    $sheet ->setCellValue('J4','违法时长');
                    $sheet ->setCellValue('K4','时长违法率');
                    $sum_data4 = [];
                    foreach ($data4 as $key => $value) {

                        $sheet ->setCellValue('A'.($key+5),($key+1));
                        $sheet ->setCellValue('B'.($key+5),!empty($value['tadclass_name']) ? $value['tadclass_name'] : '全部类别合计');
                        $sheet ->setCellValue('C'.($key+5),$value['fad_count']);
                        $sheet ->setCellValue('D'.($key+5),$value['fad_illegal_count']);
                        $sheet ->setCellValue('E'.($key+5),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('F'.($key+5),$value['fad_times']);
                        $sheet ->setCellValue('G'.($key+5),$value['fad_illegal_times']);
                        $sheet ->setCellValue('H'.($key+5),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('I'.($key+5),$value['fad_play_len']);
                        $sheet ->setCellValue('J'.($key+5),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('K'.($key+5),$value['lens_illegal_rate'].'%');
                    }
                    $objActSheet =$objPHPExcel->getActiveSheet();
                    //给当前活动的表设置名称
                    $objActSheet->setTitle('涉嫌违法广告分类别情况(表'.$report_type.')');
                    break;
            }
        }
        if(count($report_type_array) > 1){
            $report_name = $r_name.$date_day_s.'至'.$date_day_e.'专项报告（内含四个sheet表）';
        }
        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $date_now = $date;
        $date = date('Ymdhis');
        $savefile = './Public/Xls/'.$date.'.xlsx';
        $objWriter->save($savefile);

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

        $system_num = getconfig('system_num');
        
        //将生成记录保存
        $data['pnname'] 			= $report_name;
        $data['pntype'] 			= $pntype;
        $data['pnfiletype'] 		= 30;
        $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
        $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
        $data['pncreatetime'] 		= date('Y-m-d H:i:s');
        $data['pnurl'] 				= $ret['url'];
        $data['pnhtml'] 			= '';
        $data['pnfocus']            = 0;
        $data['pntrepid']           = $user['fregulatorpid'];
        $data['pncreatepersonid']   = $user['fid'];
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if($do_tn){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'生成失败'));
        }
    }

    /*
     * 南京专项报告媒体抽选与广告类别抽选数据
     * BY yjn
     * */
    public function special_report_lottery(){
        header("Content-type: text/html; charset=utf-8");
        $system_num = getconfig('system_num');
        $nj_media = M('tmedia_temp')->where('ftype=1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid'))->getField('fmediaid',true);
        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型
        //选广告大类id
        $ad_class_id_map['fpcode'] = [['neq',''],['neq','0']];
        $ad_class_id = M('tadclass')->where($ad_class_id_map)->distinct(true)->getField('fpcode',true);

        //选广告大类名称
        $ad_class_map['fcode'] = ['IN',$ad_class_id];

        $ad_class = M('tadclass')->field('fcode,ffullname as fadclass')->where($ad_class_map)->distinct(true)->order('fadclassid')->select();//选广告大类名称

        $usermedia = $StatisticalReport->distinguish_media(implode(',',$nj_media),'320000');//是否是选择了省直属



        foreach ($usermedia as $usermedia_key => $usermedia_val){
            $medias = [
                'tv' => [],
                'bc' => [],
                'paper' => []
            ];
            foreach ($usermedia_val as $nj_media_val){
                $media_data = M('tmedia')->field('fid,left(fmediaclassid,2) as fmediaclassid,fmedianame')->where(['fid'=>$nj_media_val])->find();
                if($media_data){
                    switch ($media_data['fmediaclassid']){
                        case '01':
                            $medias['tv'][] = $media_data;
                            break;
                        case '02':
                            $medias['bc'][] = $media_data;
                            break;
                        case '03':
                            $medias['paper'][] = $media_data;
                            break;
                    }
                }
            }
            if($usermedia_key == 1){
                $media_array['szs'] = $medias;
            }else{
                $media_array['bs'] = $medias;
            }
        }



        $this->ajaxReturn([
            'code' => 0,
            'msg' =>'接收成功',
            'data' => ['medias'=>$media_array,'adclass' => $ad_class]
        ]);

    }


}