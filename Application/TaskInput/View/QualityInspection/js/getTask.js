Vue.use(VueViewer.default)
Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}

const vm = new Vue({
  el: '#app',
  data() {
    return {
      loading: false,
      boxHeight: '',
      taskInfo: {},
      rate: 1,
      overtime: 1200,
      intervalTime: 1200,
      getTaskTime: '',
      timer: null,
      bodyStyle: {
        'height': '100%',
        'box-sizing': 'border-box',
        'padding': '10px',
        'overflow': 'auto'
      },
      illegaltypecode: {
        '0': '不违法',
        '10': '违法',
        '20': '违法',
        '30': '违法',
      },
      fmediaType: {
        '1':'电视',
        '2':'广播',
        '3':'报纸',
        '5':'户外',
        '13':'互联网',
      },
      visibleEntry: false,
      visiblePass: false,
      visibleReview: false,
      entryInfo: '',
      reviewInfo: '',
      area: '123',
      showView: true,
      uncount: '',
      total: '',
    }
  },
  computed: {
    taskTitle() {
      return `领取：${this.getTaskTime}，剩余 ${this.intervalTime} 秒`
    }
  },
  created() {
    this.getTaskInfo()
  },
  mounted() {
    this.$nextTick(() => {
      this.getBoxHeight()
      document.querySelector('#app').style.opacity = 1
    })
    window.onresize = () => {
      this.getBoxHeight()
    }
  },
  methods: {
    show(){
      setTimeout(() => {
        this.$refs.bbbbb.$el.focus()
      })
    },
    getTaskInfo() {
      this.loading = true
      $.post('/TaskInput/QualityInspection/getTask')
      .then(res => {
        if (res.code === 0) {
          if(res.data == null) {
            this.showView = false
            clearInterval(this.timer)
          } else {
            this.taskInfo = res.data
            this.overtime = Number(res.overtime)
            this.uncount = res.uncount
            this.total = res.total
            this.getTaskTime = moment(this.taskInfo.modifytime).format('HH:mm:ss')
            this.setTimer(+new Date(this.taskInfo.modifytime) / 1000)
          }
        } else {
          this.$Notify.error(res.msg)
        }
        this.loading = false
      })
    },
    // 提交按钮
    submit(val) {
      const data = {
        "type": val,
        "mediaclass": this.taskInfo.fmediaclass,
        "taskid": this.taskInfo.taskid,
      }
      switch (val) {
        case 2:
            this.visiblePass = false
            this._submit(data, '')
        break
        case 3:
            this.visibleEntry = false
            this._submit(data, this.entryInfo)
        break
        case 4:
            this.visibleReview = false
            this._submit(data, this.reviewInfo)
        break
      }
    },
    // 提交请求方法
    _submit(data, value) {
      data.reason = value
      $.post('/TaskInput/QualityInspection/submitTask',JSON.stringify(data))
        .then(res => {
          if(res.code === 0) {
            this.entryInfo = '',
            this.reviewInfo = '',
            this.getTaskInfo()
          }
        })
    },
    // 任务时间
    setTimer(time) {
      clearInterval(this.timer)
      this.intervalTime = this.overtime || 1200
      let endtime = Number(time) + this.overtime
      this.timer = setInterval(() => {
        let nowtime = +new Date() / 1000
        let intervalTime = parseInt(endtime - nowtime)
        if (intervalTime <= 0) {
          clearInterval(this.timer)
          this.$alert('抱歉, 任务超时被收回, 点击确定开始下一个任务').then(res => {
            this.getTaskInfo()
          })
        } else {
          this.intervalTime = intervalTime
        }
      }, 1000)
    },
    /**
     * 设置表格高度
     */
    getBoxHeight() {
      this.boxHeight = `${document.body.clientHeight - 20}px`
    },
    rateChange(value) {
      value = value || 1
      try {
        const dom = document.querySelector('#video')
        dom.playbackRate = value;
      } catch (e) {}
    },
    reloadAdIFrame() {
      const tempUrl = this.$refs.adIframe.src
      this.$refs.adIframe.src = ''
      this.$refs.adIframe.src = tempUrl
    },
  },
})