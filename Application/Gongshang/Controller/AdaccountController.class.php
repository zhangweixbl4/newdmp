<?php
namespace Gongshang\Controller;
use Think\Controller;

class AdaccountController extends BaseController
{
	public function index()
	{
		$p = I('p', 1);//当前第几页
		$pp = 20;//每页显示多少记录
//发布媒介、广告名称、广告内容类别、违法类型、涉嫌违法内容、发布次数、指导次数、状态、操作（详情）；

		$fadname = I('fadname');// 广告名称
		$fmediaclass = I('fmediaclass');//媒体类别
		$fcreatetime_s = I('fcreatetime_s');// 创建时间  开始
		$fcreatetime_e = I('fcreatetime_e');// 创建时间  结束
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID
		$fadclasscode = I('fadclasscode');// 广告内容类别
		$fmediaid = I('fmediaid');// 发布媒介

		if ($fcreatetime_s == '') {
			$fcreatetime_s = '2015-01-01';
		}
		if ($fcreatetime_e == '') {
			$fcreatetime_e = date('Y-m-d');
		}
		$where = array();
		$where['tillegaladflow.fcreatetime'] = array('between', $fcreatetime_s . ',' . $fcreatetime_e . ' 23:59:59'); //时间
		if ($fadname != '') {
			$where['tad.fadname'] = array('like', '%' . $fadname . '%');//广告名称
		}
		if ($fmediaclass != '') {
			$where['tillegalad.fmediaclassid'] = $fmediaclass;//媒体类别
		}
		if (is_array($fillegaltypecode)) {
			$where['tillegalad.fillegaltypecode'] = array('in', $fillegaltypecode);//违法类型代码
		}
		if ($fadclasscode != '') {
			$arr_code=$this->get_next_tadclasscode($fadclasscode,true);//获取下级广告类别代码
			if($arr_code){
				$where['tad.fadclasscode'] = array('in', $arr_code);
			}else{
				$where['tad.fadclasscode'] = $fadclasscode;
			}
		}
		if($fmediaid == ''){
			$media=$this->get_regulator_mediaid(session('regulatorpersonInfo.fregulatorid'));//监管机构对应的媒体
			$where['tillegalad.fmediaid'] = array('in', $media);//发布媒介
		}else{
			$where['tillegalad.fmediaid'] = $fmediaid;//发布媒介
		}

		$count = M('tillegaladflow')
			->join('tillegalad on tillegaladflow.fillegaladid=tillegalad.fillegaladid', 'LEFT')//违法类型
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on  tad.fadclasscode=tadclass.fcode ', 'LEFT')//广告内容列别
			->join('tmedia on  tillegalad.fmediaid=tmedia.fid', 'LEFT')//媒介信息
			->join('tillegaltype on tillegalad.fillegaltypecode=tillegaltype.fcode', 'LEFT')//违法类型
			->join('tadcase on tillegaladflow.fillegaladid=tadcase.fillegaladid', 'LEFT')//违法表
			->where($where)
			->where(array('tillegaladflow.fregulatorid'=>session('regulatorpersonInfo.fregulatorid'),'tillegaladflow.fbizname'=>'线索处理'))
			->count('distinct tillegaladflow.fillegaladid');// 查询满足要求的总记录数


		$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$data = M('tillegaladflow')
			->field('
						tillegaladflow.fillegaladflowid,
						tillegaladflow.fillegaladid,
			            tad.fadname,
			            tmedia.fmedianame,
			            tadclass.ffullname as adclass_fullname,
			            tillegaltype.fillegaltype,
			            tillegalad.fillegalcontent,
			            tillegalad.fissuetimes,
			            tadcase.fstate
									')
			->join('tillegalad on tillegaladflow.fillegaladid=tillegalad.fillegaladid', 'LEFT')//违法类型
//			->join('tmediaclass on tillegalad.fmediaclassid = tmediaclass.fid ', 'LEFT')//媒体列别
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on  tad.fadclasscode=tadclass.fcode ', 'LEFT')//广告内容列别
			->join('tmedia on  tillegalad.fmediaid=tmedia.fid', 'LEFT')//媒介信息
			->join('tillegaltype on tillegalad.fillegaltypecode=tillegaltype.fcode', 'LEFT')//违法类型
			->join('tadcase on tillegaladflow.fillegaladid=tadcase.fillegaladid', 'LEFT')//违法表
			->where($where)
			->where(array('tillegaladflow.fregulatorid'=>session('regulatorpersonInfo.fregulatorid'),'tillegaladflow.fbizname'=>'线索处理'))
			->group('tillegaladflow.fillegaladid')
			->limit($Page->firstRow . ',' . $Page->listRows)
			->select();//查询违法广告\

		$data = list_k($data, $p, $pp);//为列表加上序号
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate' => 1))->select();//查询违法类型
		$this->assign('illegaltype', $illegaltype); //违法类型
		$this->assign('data', $data); //列表信息
		$this->assign('page', $Page->show());//分页输出
		$this->display('adclueAccount');
	}

	/*线索详情*/
	public function details()
	{
		$where = array();
		if(I('fillegaladid')){
			$fillegaladid = I('fillegaladid');//线索ID
		}else{
			$this->error('缺失fillegaladid!');
		}
		$where['tillegalad.fillegaladid'] = $fillegaladid;
		$data = M('tillegalad')
			->field('
						tillegalad.fillegaladid,
						tillegalad.fmediaclassid,
						tillegalad.fadid,
			            tad.fadname,
			            tadclass.ffullname as adclass_fullname,
			            tillegalad.fissuetimes,
			            tillegalad.fdisposetimes,
			            tillegalad.ffirstissuetime,
			            tillegalad.flastissuetime,
			            tillegalad.fconfirmations,
			            tillegalad.fpunishments,

			            tillegaltype.fillegaltype,
			            tillegalad.fillegalcontent,
			            tillegalad.fexpressions,
			            tillegalad.fconfirmations,
			            tillegalad.fsampleid,
			            tmediaowner.fname as mediaowner_name

									')//id，<<媒介类别>>,广告id. 广告名称，广告内容列别，发布次数。指导次数。首次发布日期，末次发布日期，认定依据，处罚依据
			// 违法类型：违法内容：违法表现：认定依据,样本id：
			->join('tmediaclass on tmediaclass.fid = tillegalad.fmediaclassid', 'LEFT')//媒体列别
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on tadclass.fcode = tad.fadclasscode', 'LEFT')//广告内容列别
			->join('tmedia on tmedia.fid = tillegalad.fmediaid', 'LEFT')//媒介信息
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid', 'LEFT')
			->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode', 'LEFT')//违法类型
			->where($where)
			->find();//查询广告信息
		if ($data['fmediaclassid']) {
			switch ($data['fmediaclassid']) {
				case 'bc':
					$adclueInfo = M('tbcsample')->field('fid,favifilename')->where(array('fadid' => $data['fadid']))->find();
					break;
				case 'tv':
					$adclueInfo = M('ttvsample')->field('fid,favifilename')->where(array('fadid' => $data['fadid']))->find();
					break;
				case 'paper':
					$adclueInfo = M('tpapersample')->field('fpapersampleid,fjpgfilename')->where(array('fadid' => $data['fadid']))->find();
					break;
			}
		}
		//附件信息
		$attach = M('tillegaladattach')->where(array('fillegaladid' => $fillegaladid))->select();
		$attach_count = count($attach);
		//流转信息
		$flow = M('tillegaladflow')->where(array('fillegaladid' =>$fillegaladid))->select();
		//登记信息
		$dj_data = M('tadcase')->where(array('fillegaladid' =>$fillegaladid))->find();
		//处罚方式
		$fpentype = explode(",",$dj_data['fpentype']);
		$dj_data['chufa_type'] = M('tclueresulttype')->where(array('fstate' =>1,'fratio'=>array('in',$fpentype)))->select();
		$this->assign('data', $data); //违法信息
		$this->assign('adclueInfo', $adclueInfo); //视频信息
		$this->assign('attach',$attach);//附件
		$this->assign('attach_count',$attach_count);//附件数量
		$this->assign('flow', $flow); //流转信息
		$this->assign('dj_data', $dj_data); //登记信息
		$this->display('adclueAccountDetails');
	}

	/*指导反馈*/
	public function results()
	{
		if(I('fillegaladid')){
			$fillegaladid = I('fillegaladid');
		}else{
		    $this->ajaxReturn(array('code'=>-1,'msg'=>'fillegaladid不存在！'));
		}
		if(I('fillegaladflowid')){
			$fillegaladflowid = I('fillegaladflowid');//线索流程id
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'fillegaladflowid不存在！'));
		}
		$is_dj = M('tillegaladflow')
			->where(array('fillegaladflowid' => $fillegaladflowid,'fstate'=>2))
			->find();//查询违法广告信息
		if($is_dj){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该广告已被操作过，请刷新~！'));
		}
		//修改线索表
		$save_data['fclose'] = session('regulatorpersonInfo.fname');//办结人
		$save_data['fclosetime'] =date('Y-m-d H:i:s');//办结时间
		$save_data['fdepart'] = session('regulatorpersonInfo.fname');//办案人
		$save_data['ftraner'] =session('regulatorpersonInfo.fregulatorid');;//办案单位

		$save_data['fdocno'] = I('fdocno');//处罚文书编号
//		$save_data['fpenbasis'] = I('fpenbasis');//处罚依据
		$save_data['fpentype'] = I('fpentype');//处罚方式
		$save_data['fforfe'] = I('fforfe');//没款
		$save_data['ffine'] = I('ffine');//罚款
		$save_data['fdocdate'] = date('Y-m-d H:i:s');///处罚决定书日期
		$save_data['fstate'] = 4;///线索流程id
		$reg = M("tadcase")->where(array('fillegaladid' => $fillegaladid,'fstate' =>3))->save($save_data);
		if(!$reg){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'线索办结失败！！'));
		}

		//更改办结信息
		$save_list['fregulatorpersonid'] = session('regulatorpersonInfo.fid');//处理人ID
		$save_list['fregulatorpersonname'] = session('regulatorpersonInfo.fname');//处理人
		$save_list['ffinishtime'] = date('Y-m-d H:i:s');//完成时间
		$save_list['fstate'] = 2;
		$save_reg = M("tillegaladflow")->where(array('fillegaladflowid' => $fillegaladflowid))->save($save_list);
		if(!$save_reg){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'线索办结失败！'));
		}

	}
}