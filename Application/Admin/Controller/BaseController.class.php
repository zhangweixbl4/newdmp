<?php
namespace Admin\Controller;
use Think\Controller;
class BaseController extends Controller {
	public function _initialize() {
		if(intval($_SERVER['SERVER_PORT']) == 80 && $_SERVER['HTTP_HOST'] == 'dmp.hz-data.com'){
			//header('location:https://dmp.hz-data.com'.$_SERVER['REQUEST_URI']);
			//exit;
		}
		A('Common/AliyunLog','Model')->dmpLogs();
		$direct = array(//设置无须验证的页面
			'Admin/Login/ajax_login',
			'Admin/Index/index',
			'Admin/Login/index',
			'Admin/Login/verify',
			'Admin/Media/get_playback_url',
			'Admin/Media/paper_media',
			'Admin/Login/edit_password',
			'Admin/Login/ajax_edit_password',
			'Admin/Login/check_username',
			'Admin/Login/send_tel_verify',
			'Admin/Issue/search_customer_issue',
			'Admin/Media/change_ts',
			'Admin/InputUser/summaryScore',
			'Admin/Inputuser/taskInputScoreSummary',
			'Admin/Inputuser/sumScore',
			'Admin/Version/getList',
			'Admin/Version/getSamList',
			'Admin/Version/delSam',
			'Admin/Version/getAddVersionInfo',
			'Admin/Version/addVersion',
			'Admin/Version/delVersion',
			'Admin/Version/editVersion',
			'Admin/Version/testAddVersion',
			'Admin/Ad/getDetail',
			'Admin/Ad/mergeAd',
			'Admin/Ad/mergeAdUpdateRelated',
			'Admin/Ad/mergeAdUpdateRelatedSp',
			'Admin/Ad/getEditAdReason',
			'Admin/Ad/editAd',
			'Admin/Ad/setShowAd',
			'Admin/Ad/getHistory',
			'Admin/Tag/getList',
			'Admin/TaskInput/setGroupId',
			'Admin/TaskInput/submitAdInputTask',
			'Admin/TaskInput/submitJudgeTask',
			'Admin/Adowner/index',
			'Admin/Adowner/getList',
			'Admin/Adowner/getDetail',
			'Admin/Adowner/saveAdowner',
			'Admin/Adowner/mergeAdowner',
			'Admin/CutTask/tqc_media_permission',
		);
		$personInfoCookie = cookie('personInfoCookie');
		if(!session('personInfo.fid') && $personInfoCookie){
			$where = array('fid'=>sys_auth($personInfoCookie['fid'],'DECODE'),'fpassword'=>sys_auth($personInfoCookie['fpassword'],'DECODE'));
			$personInfo = M('tperson')->field('fid,fcode,fname,fpassword,fmobile,favatar,fregionid,fstate')->cache(true,60)->where($where)->find();//查询人员信息,60秒缓存
			session('personInfo',$personInfo);//人员信息写入session
			M('admin_login_log')->add(array('login_ip'=>get_client_ip(),'login_time'=>date('Y-m-d H:i:s'),'person_id'=>$personInfo['fid']));//记录管理员登陆记录
			M('tperson')->where(array('fid'=>$personInfo['fid']))->save(array('flogincount'=>array('exp','flogincount + 1')));//增加登陆次数
			
			cookie('personInfoCookie',$personInfoCookie,array('expire'=>3600*24*7));//保存用户cookie
		}
		$personState = M('tperson')->cache(true,60)->where(array('fid'=>session('personInfo.fid'),'fpassword'=>session('personInfo.fpassword')))->getField('fstate');//查询人员信息,60秒缓存
		if((!session('personInfo.fid') || $personState != 1) && !in_array(MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME ,$direct)){
			header('HTTP/1.1 404 Not Found');
			header("status: 404 Not Found");
			$this->ajaxReturn(array('code'=>-2,'msg'=>'need log in again', 'des'=> '登录过期, 请刷新页面重新登录'));
		}
	}

	/**
	 * @param $sample_id 样本 id
	 * @param $media_type 媒体种类 tv , bc , paper
	 * by hs
	 */
	public function set_tillegalad_data($sample_id,$media_type) {
		$sample_model ='t'.$media_type.'sample';
		$issue_model ='t'.$media_type.'issue';
		if($media_type =='paper'){
			$sample= M($sample_model)->where(array('fpapersampleid' => $sample_id,'fstate' =>1))->find();//查找样本信息
		}else{
			$sample= M($sample_model)->where(array('fid' => $sample_id,'fstate' =>1))->find();//查找样本信息
		}
		if(isset($sample['fillegaltypecode']) && !empty($sample['fillegaltypecode'])){
			$data=M($issue_model)->field('*,min(fissuedate)as ffirstissuetime,max(fissuedate) as flastissuetime,count(fissuedate) as fissuetimes')->where(array('f'.$media_type.'sampleid' => $sample_id,'fstate' =>1))->group('fmediaid')->select();//播放记录，fbcsampleid,ftvsampleid
			foreach($data as $k=>$v){
				//是否又正在处理的违法信息
				$is_set=M('tillegalad')->where(array('fmediaid' =>$v['fmediaid'],'fsampleid' =>$sample_id,'fstate'=>array('in','0,1')))->find();
				if($is_set){
					/*有正在处理的，更新*/
					$arr['ffirstissuetime']=$v['ffirstissuetime'];//首次播出日期
					$arr['flastissuetime']=$v['flastissuetime'];//未次播出日期
					$arr['fissuetimes']=$v['fissuetimes'];//发布次数
					$res = M("tillegalad")->where(array('fillegaladid' => $is_set['fillegaladid']))->save($arr);
				}else {
					/*新增违法广告表*/
					$list['fmediaclassid'] = $media_type;//媒介类别ID
					$list['fmediaid'] = $v['fmediaid'];//媒介ID
					$list['fsampleid'] = $sample_id;//样本ID
					$list['fadid'] = $sample['fadid'];//广告ID
					$list['fversion'] = $sample['fversion'];//版本
					$list['fspokesman'] = $sample['fspokesman'];//代言人
					$list['fapprovalid'] = $sample['fapprovalid'];//审批号
					$list['fapprovalunit'] = $sample['fapprovalunit'];//审批单位
					$list['fadlen'] = $sample['fadlen'];//广告长度
					$list['fillegaltypecode'] = $sample['fillegaltypecode'];//违法类型代码
					$list['fillegalcontent'] = $sample['fillegalcontent'];//涉嫌违法内容
					$list['fexpressioncodes'] = $sample['fexpressioncodes'];//违法表现代码
					$list['fexpressions'] = $sample['fexpressions'];//违法表现
					$list['fconfirmations'] = $sample['fconfirmations'];//认定依据
					$list['fpunishments'] = $sample['fpunishments'];//处罚依据
					$list['fpunishmenttypes'] = $sample['fpunishmenttypes'];//处罚种类及幅度
					$list['ffirstissuetime'] = $v['ffirstissuetime'];//首次播出日期
					$list['flastissuetime'] = $v['flastissuetime'];//未次播出日期
					$list['fissuetimes'] = $v['fissuetimes'];//发布次数
					if ($media_type == 'peper') {
						$list['favifilename'] = $sample['fjpgfilename'];//图片文件名
					} else {
						$list['favifilename'] = $sample['favifilename'];//视频文件名
					}

					$list['tsource'] = $v['fsource'];//来源
					$list['fcreator'] =  $sample['fcreator'];//创建人
					$list['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
					$fdisposetimes=M('tillegalad')->where(array('fmediaid' =>$v['fmediaid'],'fsampleid' =>$sample_id,'fstate'=>2))->count();
					$list['fdisposetimes'] = $fdisposetimes;//处置次数
					$list['fdisposestyle'] = 0;//处置方式（0-待定，1-指导, 2 ,合并线索，3，复核，4，线索，5，不予处理，10，其他）
					$list['fstate'] = 0;//状态（0-待处理，1-正处理，2-已处理）
					$res = M("tillegalad")->add($list);
				}
			}
		}
	}

	/**
	 * @param $fmediaid 媒介id
	 * @return mixed
	 * 获取媒介信息
	 * by  hs
	 */
	public function get_tmedia_data($fmediaid){
		$data= M('tmedia')->where(array('fid' =>$fmediaid,'fstate' =>1))->find();
		return $data;
	}

}