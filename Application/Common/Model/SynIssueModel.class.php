<?php
namespace Common\Model;
use Think\Exception;
use phpDocumentor\Reflection\Types\Boolean;

class SynIssueModel{
	/*创建表电视、广播发布记录分表*/
	public function create_issue_part_table($table_name) {

		$create_table_sql = "	CREATE TABLE `".$table_name."` (
											  `fid` bigint(18) NOT NULL AUTO_INCREMENT COMMENT '主键',
											  `fmediaid` bigint(18) DEFAULT '0' COMMENT '媒介ID',
											  `fsampleid` bigint(18) DEFAULT '0' COMMENT '样本ID',
											  `fstarttime` int(11) DEFAULT '0' COMMENT '开始时间戳',
											  `fendtime` int(11) DEFAULT '0' COMMENT '结束时间戳',
											  `flength` int(11) DEFAULT '0' COMMENT '发布时长',
											  `fissuedate` int(11) DEFAULT '0' COMMENT '发布日期,发布当日零点的时间戳',
											  `faudiosimilar` int(11) DEFAULT '0' COMMENT '识别相似度分值',
											  `fregionid` int(11) DEFAULT '0' COMMENT '行政区划代码',
											  `fcreatetime` int(11) DEFAULT '0' COMMENT '创建时间',
											  `fcreator` varchar(50) DEFAULT '' COMMENT '创建人',
											  PRIMARY KEY (`fid`),
											  KEY `fmediaid` (`fmediaid`) USING BTREE,
											  KEY `fstarttime` (`fstarttime`) USING BTREE,

											  KEY `fissuedate` (`fissuedate`) USING BTREE,
											  KEY `fsampleid` (`fsampleid`) USING BTREE,
											  KEY `fissuedate_fstarttime` (`fissuedate`,`fstarttime`) USING BTREE,
											  KEY `fregionid` (`fregionid`) USING BTREE
											) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";
		try{//尝试创建表
			$create_state = M()->execute($create_table_sql);
		}catch(Exception $error) { //如果创建失败
			//var_dump($error);
		} 						
	}
    
	/**
	* 创建食药总局发布清单分表
	* @Param	Mixed $tableName 要创建的表名
	* @Return	Boolean 返回的数据
	*/
	public function createFoodDrugTable($tableName){
		$createTableSql = "CREATE TABLE `".$tableName."` (
			`fid` BIGINT (18) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
			`fmediaclassid` VARCHAR (10) DEFAULT NULL COMMENT '媒介类型',
			`fsampleid` INT (11) DEFAULT NULL COMMENT '样本id',
			`fmediaid` BIGINT (18) DEFAULT NULL COMMENT '媒介id',
			`fmedianame` VARCHAR (50) DEFAULT NULL COMMENT '媒介名称',
			`fregionid` INT (11) DEFAULT NULL COMMENT '地区id',
			`fadname` VARCHAR (100) DEFAULT NULL COMMENT '广告名称',
			`fadowner` BIGINT (14) DEFAULT NULL COMMENT '广告主id',
			`fbrand` VARCHAR (50) DEFAULT NULL COMMENT '品牌',
			`fspokesman` VARCHAR (100) DEFAULT NULL COMMENT '代言人',
			`fadclasscode` VARCHAR (8) DEFAULT '' COMMENT '广告类别id',
			`fstarttime` INT (11) DEFAULT NULL COMMENT '发布开始时间戳',
			`fendtime` INT (11) DEFAULT NULL COMMENT '发布结束时间戳',
			`fissuedate` date DEFAULT NULL COMMENT '发布日期',
			`flength` INT (11) DEFAULT NULL COMMENT '发布时长(秒)',
			`fillegaltypecode` TINYINT (4) DEFAULT NULL COMMENT '违法类型',
			`fadmanuno` VARCHAR (100) DEFAULT '' COMMENT '广告中标识的生产批准文号',
			`fmanuno` VARCHAR (100) DEFAULT '' COMMENT '生产批准文号',
			`fadapprno` VARCHAR (100) DEFAULT '' COMMENT '广告中标识的广告批准文号',
			`fapprno` VARCHAR (100) DEFAULT '' COMMENT '广告批准文号',
			`fadent` VARCHAR (100) DEFAULT '' COMMENT '广告中标识的生产企业（证件持有人）名称',
			`fent` VARCHAR (100) DEFAULT '' COMMENT '生产企业（证件持有人）名称',
			`fentzone` VARCHAR (100) DEFAULT '' COMMENT '生产企业（证件持有人）所在地区',
			`fexpressioncodes` VARCHAR (512) DEFAULT '' COMMENT '违法表现代码，用“;”分隔',
			`fillegalcontent` varchar(512) DEFAULT '' COMMENT '涉嫌违法内容',
			`sam_source_path` varchar(255) DEFAULT '' COMMENT '样本素材路径',
			
			
			PRIMARY KEY (`fid`),
			KEY `fmediaclassid` (`fmediaclassid`),
			KEY `fsampleid` (`fsampleid`),
			KEY `fmediaid` (`fmediaid`),
			KEY `fregionid` (`fregionid`),
			KEY `fmedianame` (`fmedianame`),
			KEY `fadname` (`fadname`),
			KEY `fadclasscode` (`fadclasscode`),
			KEY `fissuedate` (`fissuedate`),
			KEY `fillegaltypecode` (`fillegaltypecode`),
			KEY `fmediaid_2` (`fmediaid`,`fstarttime`)
		) ENGINE = INNODB DEFAULT CHARSET = utf8;";
		try {
			M()->execute($createTableSql);
		} catch (Exception $e) {
			//TODO:创建失败的情况处理
		}
	}
}