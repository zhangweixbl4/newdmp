Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}

var vm = new Vue({
  el: '#app',
  name: "customer_issue",
  data() {
    return {
      loading: true, //加载状态
      mediaCount: 0,
      form: {
        fmedianame: '',//媒介名称
        fmediaownername: '',//媒介机构名称
        fadname: '',//广告名称
        fadowner: '',//广告主
        fbrand: '',//品牌
        this_region_id: 'false',//地区仅本级
        fspokesman: '', //代言人
        fillegaltypecode: '', //违法类型
		net_platform: '', //发布平台

        pp: 20, //每页数量
        p: 1, //分页
      },
      copy: false,
      area: [],//地区
      adclasscode: [],//广告类别
      issDate: [], //发布日期
      tabelData: [], //列表数据
      allCount: '0', //列表数据总数
      dialogVisible: false, //详情弹窗显示隐藏字段
      details: {},//详情数据
      preData: {},//上次搜索的请求参数
      regiontree: REGIONTREE,
      adclass: ADCLASS,
      mjlx: {
        '01': '电视',
        '02': '广播',
        '03': '报纸',
        '13': '互联网',
      },
      wflx: {
        '0': '不违法',
        '30': '违法',
      },
	  fbpt: {
        '1': 'PC',
        '2': '手机',
		'9': '微信',
        '4': 'OTT',
		
      },

	  
      pickerOptions: {
        disabledDate: function (time) {
          return time.getTime() > new Date().getTime()
        }
      }
    }
  },
  created() {
    this.loading = !this.loading
    this.lastMonth()
    this.getList()
  },
  mounted() {
    this.$refs.app.style.display = 'block'
    new ClipboardJS('.copied')
  },
  methods: {
    /**
     * 获取列表
     * @param {number} p 页码
     */
    getList(p) {
    this.preData = Object.assign({}, this.form)
      this.preData.issuedate_min = this.issDate[0] || ''
      this.preData.issuedate_max = this.issDate[1] || ''
      this.preData.fregionid = this.area[this.area.length - 1] || ''
      this.preData.fadclasscode = this.adclasscode[this.adclasscode.length - 1] || ''
      this.preData.p = p || 1
      this.loading = true
      axios.post('/Admin/Netsam/search_issue', Qs.stringify(this.preData))
        .then(res => {
          if (res.data.code === 0) {
			      this.mediaCount = res.data.mediaCount
            this.tabelData = res.data.issueList
            this.allCount = res.data.total
          } else {
            this.$message.error(res.data.msg)
          }
          this.loading = false
        })
        .catch(e => {
          this.$message.error('网络错误')
          this.loading = false
        })
    },
    /**
     * 用于获取某条广告的详情
     * @param {Object} row 当前行的数据对象
     */
    show(row) {
      let data = {
        identify: row.identify
      }
      axios.post('/Admin/Netsam/issue_info', Qs.stringify(data))
        .then(res => {
          if (res.data.code === 0) {
            this.details = res.data.issueInfo
          } else {
            this.$message.error(res.data.msg)
          }
          this.dialogVisible = true
        })
        .catch(e => {
		
          this.$message.error('网络错误')
        })
    },
    /**
     * 详情弹窗关闭时触发
     */
    dialogClose() {
      this.dialogVisible = false
      this.details = {}
    },
    /**
     * 设置每页显示条数
     * @param {Number} size 每页显示条数
     */
    sizeChange(size) {
      this.form.pp = size
      this.getList()
    },
    // 设置筛选条件时间为近30天
    lastMonth() {
      let time = new Date().getTime()
      let lastMonth = time - (60 * 60 * 24 * 30 * 1000)
      this.issDate = Array.of(formatDate(new Date(lastMonth), 'yyyy-MM-dd'), formatDate(new Date(time), 'yyyy-MM-dd'))
    },
    hide() {
      this.copy = true
      setTimeout(() => {
        this.copy = false
      },500)
    }
  }
})