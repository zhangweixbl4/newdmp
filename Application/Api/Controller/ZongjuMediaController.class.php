<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;



class ZongjuMediaController extends Controller {
	
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}
	
	
	/*计算控制服务*/
	public function tbn_ad_summary_day_ctrl(){
		
		$taskIdList = M('z_gather_task')->where($where)->limit(200)->getField('fid',true);
		
		
		$urlArr = array();

		foreach($taskIdList as $fid){//按媒体、日期循环
			$urlArr[] = urlencode(U('Api/ZongjuMedia/tbn_ad_summary_day@'.$_SERVER['HTTP_HOST'],array('fid'=>$fid)));
		
		}
		
		if($urlArr){
			$retArr = A('Common/System','Model')->async_get_node($urlArr);
		}
		echo count($retArr);
		
	}
	

	
	/*计算媒介发布数量*/
	public function tbn_ad_summary_day(){
	
		
		set_time_limit(1200);
		session_write_close();
		
		
		$where = array();
		$where['fstate'] = 0;
		if(I('fid')){
			$where['fid'] = I('fid');
		}
		
		$taskList = M('z_gather_task')->where($where)->limit(200)->select();
		//var_dump($taskList);
		
		foreach($taskList as $task){//按媒体、日期循环
			
			$mediaId = $task['fmediaid'];//媒介id
			$fdate = $task['fdate'];//日期
			
			
			$mediaInfo = M('tmedia')
									->cache(true,60)
									->field('left(tmedia.fmediaclassid,2) as media_class , tmediaowner.fregionid')
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->where(array('tmedia.fid'=>$mediaId))
									->find();
			//var_dump($mediaInfo);

			$adclass_data = array();//初始化广告分类数据
			
			
			if($mediaInfo['media_class'] == '01'){
			
				
				$table_name = 'ttvissue_'.date('Ym',strtotime($fdate)) .'_'. substr($mediaInfo['fregionid'],0,2);//表名
				$sam_table_name = 'ttvsample';
				
				if($task['fcustomer'] == '370300'){
					$table_name = 'ttvissue_2018_370300';//表名
					$sam_table_name = 'ttvsample_370300';
				}
			
				try{
								
					$dmp_tv_ad_class = M($table_name)//按广告分类查询
									->alias('ttvissue')
									->field('
											left(tad.fadclasscode,2) as fad_class_code,
											count(*) as fad_times,
											count(case when ttvissue.flength < 600 then 1 else null end) as basic_ad_times,
											ttvsample.fillegaltypecode,
											sum(case when ttvissue.flength < 600 then ttvissue.flength else 0 end) as fad_play_len,
											count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) fad_illegal_times,
											
											concat(ttvsample.fid,"_01") as sam_id,
											ttvsample.fadlen,
											sum(ttvissue.flength) as in_long_ad_play_len
											')
									->join($sam_table_name.' as ttvsample on ttvsample.fid = ttvissue.fsampleid')
									->join('tad on tad.fadid = ttvsample.fadid and ttvsample.fadid > 0')
									->where(array('ttvissue.fissuedate'=>strtotime($fdate),'ttvissue.fmediaid'=>$mediaId,'ttvissue.flength'=>array('gt',0)))
									->group('left(tad.fadclasscode,2),ttvsample.fid')
									->select();

					foreach($dmp_tv_ad_class as $ad_class_data){
						$adclass_data[$ad_class_data['fad_class_code']][] = $ad_class_data;//分类数据重新组装
					}
					
					
				}catch(Exception $error) {
					//var_dump($error);
				} 
				

		 
			}elseif($mediaInfo['media_class'] == '02'){
				$table_name = 'tbcissue_'.date('Ym',strtotime($fdate)) .'_'. substr($mediaInfo['fregionid'],0,2);//表名
				$sam_table_name = 'tbcsample';
				if($task['fcustomer'] == '370300'){
					$table_name = 'tbcissue_2018_370300';//表名
					$sam_table_name = 'tbcsample_370300';
				}
				try{
								
					$dmp_bc_ad_class = M($table_name)//按广告分类查询
									->alias('tbcissue')
									->field('
											left(tad.fadclasscode,2) as fad_class_code,
											count(*) as fad_times,
											count(case when tbcissue.flength < 600 then 1 else null end) as basic_ad_times,
											
											tbcsample.fillegaltypecode,
											sum(case when tbcissue.flength < 600 then tbcissue.flength else 0 end) as fad_play_len,
											count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) fad_illegal_times,
											
											concat(tbcsample.fid,"_02") as sam_id,
											tbcsample.fadlen,
											sum(tbcissue.flength) as in_long_ad_play_len
											')
									->join($sam_table_name.' as tbcsample on tbcsample.fid = tbcissue.fsampleid')
									->join('tad on tad.fadid = tbcsample.fadid and tbcsample.fadid > 0')
									->where(array('tbcissue.fissuedate'=>strtotime($fdate),'tbcissue.fmediaid'=>$mediaId,'tbcissue.flength'=>array('gt',0)))
									->group('left(tad.fadclasscode,2),tbcsample.fid')
									->select();

					foreach($dmp_bc_ad_class as $ad_class_data){
						$adclass_data[$ad_class_data['fad_class_code']][] = $ad_class_data;//分类数据重新组装
					}
					
					
				}catch(Exception $error) {
					//var_dump($error);
				} 
				
				//var_dump($dmp_bc_ad_class);
				
				
				
			}elseif($mediaInfo['media_class'] == '03'){
				$table_name = 'tpaperissue';//表名
				$sam_table_name = 'tpapersample';	
				if($task['fcustomer'] == '370300'){
					$table_name = 'tpaperissue_370300';//表名
					$sam_table_name = 'tpapersample_370300';
				}
				try{ 				
								
					$dmp_paper_ad_class = M($table_name)//按广告分类查询
									->alias('tpaperissue')
									->field('
											
											left(tad.fadclasscode,2) as fad_class_code,
											count(*) as fad_times,
											count(*) as basic_ad_times,
											tpapersample.fillegaltypecode,
											0 as fad_play_len,
											
											count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) fad_illegal_times,
											
											0 as fad_illegal_play_len,

											concat(tpaperissue.fpapersampleid,"_03") as sam_id,
											0 fadlen,
											0 as in_long_ad_play_len,
											0 as in_long_ad_illegal_play_len
											
											
											')
									->join($sam_table_name.' as tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
									->join('tad on tad.fadid = tpapersample.fadid and tpapersample.fadid > 0')
									->where(array('tpaperissue.fissuedate'=>$fdate,'tpaperissue.fmediaid'=>$mediaId))
									->group('left(tad.fadclasscode,2),tpaperissue.fpapersampleid')
									->select();

					
					//var_dump($adclass_data0);
					
					foreach($dmp_paper_ad_class as $ad_class_data){
						$adclass_data[$ad_class_data['fad_class_code']][] = $ad_class_data;//分类数据重新组装
					}
					
					
					//var_dump($dmp_paper);			
				}catch(Exception $error) { 


				}
				
				
			}
			

			
								

			
			
			
			$class_ad_arr = array();//初始化本次包含的分类
			foreach($adclass_data as $ad_class => $adDataArr){
				
				if($ad_class == '23') continue; //如果广告分类是23，直接结束本次循环
				$adData0 = array();
				$adData0['fad_times'] = 0;//广告发布条次
				$adData0['basic_ad_times'] = 0;//不含长广告的发布条次
				
				$adData0['fad_play_len'] = 0;//不含长广告的发布时长
				$adData0['in_long_ad_play_len'] = 0;//广告发布时长
				$adData0['fad_sam_list'] = '';//样本id列表
				$adData0['basic_ad_sam_list'] = '';//不含长广告的样本列表
				
				
				foreach($adDataArr as $adData){
					$adData0['fad_times'] += $adData['fad_times'];
					$adData0['basic_ad_times'] += $adData['basic_ad_times'];

					$adData0['fad_play_len'] += $adData['fad_play_len'];
					$adData0['in_long_ad_play_len'] += $adData['in_long_ad_play_len'];
					$adData0['fad_sam_list'] .= ','.$adData['sam_id'];
					if(intval($adData['fadlen']) < 600){
						$adData0['basic_ad_sam_list'] .= ','.$adData['sam_id'];
					}

					
				}
				
				$adData0['fad_sam_list'] = ltrim($adData0['fad_sam_list'],',');
				$adData0['basic_ad_sam_list'] = ltrim($adData0['basic_ad_sam_list'],',');
				
				$adData0['fad_class_code'] = $ad_class;
				
				$this->tbn_ad_summary_day_do($mediaId,$fdate,$ad_class,array(
																			'fad_times'=>$adData0['fad_times'],
																			'basic_ad_times'=>$adData0['basic_ad_times'],
																			
																			'fad_play_len'=>$adData0['fad_play_len'],
																			'fad_sam_list'=>$adData0['fad_sam_list'],
																			'basic_ad_sam_list'=>$adData0['basic_ad_sam_list'],
																			
																			'in_long_ad_play_len' => $adData0['in_long_ad_play_len'],
																			),
												$task['fcustomer']						
												);
				$class_ad_arr[] = $ad_class;//赋值本次执行的分类数据			
				
			}


			$AdClassList = A('Common/AdClass','Model')->getAdClassCode();
			foreach($AdClassList as $ad_class){
				if(in_array($ad_class,$class_ad_arr)) continue;//退出循环
				
				$del_state = M('tbn_ad_summary_day_z')->where(array('fmediaid'=>$mediaId,'fdate'=>$fdate,'fad_class_code'=>$ad_class))->delete();
				
			}
		
			echo M('z_gather_task')->where(array('fid'=>$task['fid']))->save(array('fstate'=>1,'ffinishtime'=>date('Y-m-d H:i:s')));
			
		}//按媒体、日期循环

		

	}
	
	/*修改媒介发布数量*/
    private function tbn_ad_summary_day_do($media_id,$date,$fad_class_code,$countArr,$fcustomer = '100000'){
		//var_dump($fad_class_code);
		
		$mediaInfo = M('tmedia')
								->cache(true,3600)
								->field('left(tmedia.fmediaclassid,2) as fmedia_class_code,tmedia.fmediaownerid,tmediaowner.fregionid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>$media_id))
								->find();
		
		if(!$mediaInfo){//判断媒介是否存在
			M('tbn_ad_summary_day_z')->where(array('fmediaid'=>$media_id,'fdate'=>$date,'fad_class_code'=>$fad_class_code))->delete();//删除数据
			return false;
		} 

		
		

		$fid = M('tbn_ad_summary_day_z')->where(array('fmediaid'=>$media_id,'fdate'=>$date,'fad_class_code'=>$fad_class_code,'fcustomer'=>$fcustomer))->getField('fid');
		if(!$fid){
			try{//尝试新增数据
				$fid = M('tbn_ad_summary_day_z')->add(array('fmediaid'=>$media_id,'fdate'=>$date,'fad_class_code'=>$fad_class_code,'fcustomer'=>$fcustomer));
				
			}catch(Exception $e) { //如果新增失败则获取id
				$fid = M('tbn_ad_summary_day_z')->where(array('fmediaid'=>$media_id,'fdate'=>$date,'fad_class_code'=>$fad_class_code,'fcustomer'=>$fcustomer))->getField('fid');
				
			} 
		}

		
		
		
		$countArr['fmedia_class_code'] = $mediaInfo['fmedia_class_code'];
		$countArr['fmediaownerid'] = $mediaInfo['fmediaownerid'];
		$countArr['fregionid'] = $mediaInfo['fregionid'];

		
		$save_ret = M('tbn_ad_summary_day_z')->where(array('fid'=>$fid))->save($countArr);//修改数量
		
		
		if($save_ret >= 0){
			return true;
		}else{
			return false;
		}
		
    }
	
	
	
	
	/*计算长广告发布条次*/
	public function long_ad_times(){
		
		$summaryList = M('tbn_ad_summary_day_z')->where(array('stamp'=>0))->limit(100)->select();
		
		foreach($summaryList as $summary){
			//var_dump($summary);
			$mediaInfo = M('tmedia')->cache(true,60)->field('fid,left(fmediaclassid,2) as media_class')->where(array('fid'=>$summary['fmediaid']))->find();
			
			if($mediaInfo['media_class'] == '01'){
				$tab = 'tv';
			}elseif($mediaInfo['media_class'] == '02'){	
				$tab = 'bc';
			}else{
				M('tbn_ad_summary_day_z')->where(array('fid'=>$summary['fid']))->save(array('stamp'=>1));
				continue;
			}

			$issue_table_name = 't'.$tab.'issue_'.date('Ym',strtotime($summary['fdate'])).'_'.substr($summary['fregionid'],0,2);
			
			//var_dump($issue_table_name);
			try{//
				$long_ad_times = M($issue_table_name)
									->alias('issue')
									->join('t'.$tab.'sample as sample on sample.fid = issue.fsampleid')
									->join('tad on tad.fadid = sample.fadid')
									->where(array(
													'left(tad.fadclasscode,2)'=>$summary['fad_class_code'],
													'issue.fissuedate'=>strtotime($summary['fdate']),
													'issue.fmediaid'=>$summary['fmediaid'],
													'sample.fadlen'=>array('egt',300)
											))
									->count();//创广告条次
			}catch(Exception $e) { //
				$long_ad_times = 0;
			}
			
			$basic_ad_times = $summary['fad_times']	- $long_ad_times; //一般广告条次等于所有广告条次减去长广告条次	
			
			try{//
				$long_ad_play_len = M($issue_table_name)
									->alias('issue')
									->join('t'.$tab.'sample as sample on sample.fid = issue.fsampleid')
									->join('tad on tad.fadid = sample.fadid')
									->where(array(
													'left(tad.fadclasscode,2)'=>$summary['fad_class_code'],
													'issue.fissuedate'=>strtotime($summary['fdate']),
													'issue.fmediaid'=>$summary['fmediaid'],
													'sample.fadlen'=>array('egt',300)
											))
									->sum('issue.flength');//创广告时长
			}catch(Exception $e) { //
				$long_ad_play_len = 0;
			}
			
			
			$long_ad_play_len = intval($long_ad_play_len);						
			//var_dump(M($issue_table_name)->getLastSql());						
			$fad_play_len = $summary['in_long_ad_play_len']	- $long_ad_play_len; //一般广告发布时长等于包含长广告的发布时长减去长广告发布时长
			
			
			$basic_ad_sam_list = array();
			foreach(explode(',',$summary['fad_sam_list']) as $sam_id){
				
				$sam_id = str_replace('_'.$mediaInfo['media_class'],'',$sam_id);
				$is_long_ad = M('t'.$tab.'sample')->where(array('fid'=>$sam_id,'fadlen'=>array('egt',300)))->count();
				
				if($is_long_ad == 0){
					$basic_ad_sam_list[] = $sam_id.'_'.$mediaInfo['media_class'];
				}
			}
			$basic_ad_sam_list = implode(',',$basic_ad_sam_list);
			//var_dump($basic_ad_sam_list);
			
			
			
			
			
			
			
			
			echo $basic_ad_sam_list."<br />\n";
			echo $summary['fad_times'].'	- '.$long_ad_times."<br />\n";
			echo $summary['in_long_ad_play_len'].'	- '.$long_ad_play_len."<br />\n";
			echo "<br />\n";
			
			
			
			
			
			M('tbn_ad_summary_day_z')
								->where(array('fid'=>$summary['fid']))
								->save(array(
												'basic_ad_times'=>$basic_ad_times,
												'basic_ad_sam_list'=>$basic_ad_sam_list,
												'fad_play_len'=>$fad_play_len,
												'stamp'=>1
											));

				
		}
		
		
	}
	
	
	
	
	
	



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
}