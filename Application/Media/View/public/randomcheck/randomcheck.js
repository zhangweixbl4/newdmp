"use strict";
// 提交随机请求
function send_random(url,is_random_all) {
    var random_start_time = $("#Sstart").val();
    var random_end_time = $("#Send").val();
    var random_days = $("#random_days").val();
    var address = $("#address").attr("fid");
    var material_type = $("#infoClass").find("select").val();
    var media_type = $("#mediaClass").find("select").val();
    var media_list = [];
    $("#mediaClass .mediaLabel").each(function () {
        if ($(this).hasClass("mediaLabelActive")){
            media_list.push($(this).attr("data"));
        }
    })
    var data = {
        "random_start_time":random_start_time,
        "random_end_time":random_end_time,
        "random_days":random_days,
        "address":address,
        "material_type":material_type,
        "media_type": media_type,
        "is_random_all":is_random_all,
        "media_list":media_list
    }
    $.post(url, data, function (req) {
        $("#confirmModal").find("#confirm_yes").attr("disabled", true);
        if (req.code != 0) {
            $("#confirmModal").find("h2").text(req.msg);
            $("#confirmModal").find("#confirm_yes").hide();
            $("#confirmModal").find("#confirm_yes").removeAttr("disbaled");
        } else {
            $('#confirmModal').find("h2").text("任务生成成功，前往任务页面？");
            $("#confirmModal").find("#confirm_yes").hide();
            $("#confirmModal").find("#confirm_no").before(`<button type="button" href="`+platformUrl+`" class="btn btn-primary J_menuItem2" style="margin-right:20px;" title2="检查">前往任务列表</button>`);
        }
    })
}
$(function () {
    // 媒体类型选择媒体
    $(document).on("click",".mediaLabel",function () {
        $(this).hasClass("mediaLabelActive") ? $(this).removeClass("mediaLabelActive") : $(this).addClass("mediaLabelActive");
    })

    $("#mediaClass").find("select").change(function () {
        if ($(this).val() == "00") {
            $("#MediaList").empty();
        } else {
            $.post(mediaListUrl, { "media_type": $(this).val() }, function (req) {
                if (req.code != 0) {
                    $("#MediaList").text(req.msg);
                } else {
                    var list = "";
                    for (var i = 0; length = req.mediaList.length, i < length; i++){
                        list += `
                        <div class="mediaLabel" data="`+req.mediaList[i].fid+`">`+req.mediaList[i].fmedianame+`</div>
                        `
                    }
                }
            })
        }
    })

    // 点击下一页
    $('#Next').click(function () {
        if (!$('#random_check').find('.active').prev('li').find('a').length) {
            $('#Pre').show();
            $('#Radom').hide();
        }
        if ($('#random_check').find('.active').next('li').find('a').length) {
            $('#random_check').find('.active').next('li').find('a').click();
        }
        if (!$('#random_check').find('.active').next('li').find('a').length) {
            $('#Next').hide();
            $('#start_random').show();
        }
    })


    // 点击上一页
    $('#Pre').click(function () {
        if (!$('#random_check').find('.active').next('li').find('a').length) {
            $('#Next').show();
            $('#start_random').hide();
        }
        if ($('#random_check').find('.active').prev('li').find('a').length) {
            $('#random_check').find('.active').prev('li').find('a').click();
        }
        if (!$('#random_check').find('.active').prev('li').find('a').length) {
            $('#Pre').hide();
            $('#Radom').show();
        }
    })


    // 重置随机
    $('#reset').click(function () {
        location.reload();
    })


    // 提交随机
    $("#start_random").click(function () {
        var url = "";
        confirmModal("确认开始随机？", function () {
            var is_random_all = false;
            send_random(radomUrl,is_random_all);
        }, function () {})
    })

    $("#Radom").click(function () {
        var url = "";
        confirmModal("确认随机全部项？", function () {
            var is_random_all = true;
            send_random(radomUrl,is_random_all);
        },function () {})
    })

    //信息类型选择原始素材，媒体类型报纸不可选
    $("#infoClass").find('select').change(function () {
        $("#mediaClass").find("select").val("00");
        $("#MediaList").empty();
        if($(this).val() != 1){
            $("#mediaClass select").find("option[value=03]").removeAttr("disabled");
        }else{
            $("#mediaClass select").find("option[value=03]").attr("disabled","disabled");
        }
    })
})



// 日期选择插件
$(function () {
    laydate.skin('molv');
    var Sstart = {
        elem: '#Sstart',
        format: 'YYYY-MM-DD',
        min: '1970-01-01 00:00:00', //设定最小日期为当前日期
        max: '', //最大日期
        istime: false,
        istoday: false,
        choose: function (datas) {
            Send.min = datas; //开始日选好后，重置结束日的最小日期
            Send.start = datas //将结束日的初始值设定为开始日
        }
    };
    var Send = {
        elem: '#Send',
        format: 'YYYY-MM-DD',
        min: '',
        max: '2099-06-16 23:59:59',
        istime: false,
        istoday: false,
        choose: function (datas) {
            Sstart.max = datas; //结束日选好后，重置开始日的最大日期
        }
    };
    laydate(Sstart);
    laydate(Send);
})