<?php
namespace Gongshang\Controller;
use Think\Controller;

/**
 * Class LxnoticeController
 * @package Gongshang\Controller
 * 联席_公告
 */
class LxnoticeController extends BaseController
{
	/*接收通告列表*/
	public function reportList()
	{
		$p = I('p', 1);//当前第几页
		$pp = 20;//每页显示多少记录

		$report_type = I('report_type');//1，已收   2 已发
		$is_read = I('is_read');//1,全部  2 已读 3 未读
		$keywords= I('keywords');//关键字
		$data=array();
		$where= array();
		if(isset($keywords) && !empty($keywords)){
			$where['notice_title'] = array('like', '%' . $keywords . '%');//关键词
		}
		if($report_type == 1){
			if($is_read==1) {
				$count = M('lx_notice_receive')
					->join('lx_notice on lx_notice.notice_id = lx_notice_receive.notice_receive_id', 'LEFT')//通知主表
					->where(array('notice_receive_gov_id' => session('regulatorpersonInfo.fregulatorid')))
					->where($where)
					->count();// 查询满足要求的总记录数

				$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数

				$data = M('lx_notice_receive')
					->field('lx_notice.*,lx_notice_receive.id as receive_id ')
					->join('lx_notice on lx_notice.notice_id = lx_notice_receive.notice_receive_id', 'LEFT')//通知主表
					->where(array('notice_receive_gov_id' => session('regulatorpersonInfo.fregulatorid')))
					->where($where)
					->limit($Page->firstRow . ',' . $Page->listRows)
					->select();//查询违法广告
			}
			if($is_read==2) {

				$receive_data = M('lx_notice_receive')
								->join('lx_notice on lx_notice.notice_id = lx_notice_receive.notice_receive_id', 'LEFT')//通知主表
								->where(array('notice_receive_gov_id'=>session('regulatorpersonInfo.fregulatorid')))
								->where($where)
								->select();//本部门的通告

				foreach($receive_data as $k=> $v){
					$arr_uid = explode(",",$v['notice_read_user_id']);
					if(in_array(session('regulatorpersonInfo.fid'),$arr_uid)){
						$receive_id[]=$v['id']; //已读
					}
				}
				$count=count($receive_id);
				$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数
				if($count){
					$data = M('lx_notice_receive')
						->field('lx_notice.*,lx_notice_receive.id as receive_id ')
						->join('lx_notice on lx_notice.notice_id = lx_notice_receive.notice_receive_id', 'LEFT')//通知主表
						->where(array('notice_receive_gov_id' => session('regulatorpersonInfo.fregulatorid')))
						->where(array('id' => array('in', $receive_id)))
						->where($where)
						->limit($Page->firstRow . ',' . $Page->listRows)
						->select();//查询违法广告
				}
			}
			if($is_read==3){
				$receive_data = M('lx_notice_receive')
					->join('lx_notice on lx_notice.notice_id = lx_notice_receive.notice_receive_id', 'LEFT')//通知主表
					->where(array('notice_receive_gov_id'=>session('regulatorpersonInfo.fregulatorid')))
					->where($where)
					->select();//本部门的通告
				foreach($receive_data as $k=> $v){
					$arr_uid = explode(",",$v['notice_read_user_id']);
					if(!in_array(session('regulatorpersonInfo.fid'),$arr_uid)){
						$receive_id[]=$v['id']; //未读的id
					}

				}
				$count=count($receive_id);
				$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数
				if($count){
					$data = M('lx_notice_receive')
						->field('lx_notice.*,lx_notice_receive.id as receive_id ')
						->join('lx_notice on lx_notice.notice_id = lx_notice_receive.notice_receive_id', 'LEFT')//通知主表
						->where(array('notice_receive_gov_id' => session('regulatorpersonInfo.fregulatorid')))
						->where(array('id' => array('in', $receive_id)))
						->where($where)
						->limit($Page->firstRow . ',' . $Page->listRows)
						->select();//查询违法广告
				}

			}

		}elseif($report_type == 2){
			$count = M('lx_notice')
				->where(array('notice_gov_id' => session('regulatorpersonInfo.fregulatorid')))
				->where($where)
				->count();// 查询满足要求的总记录数

			$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数

			$data = M('lx_notice')
				->where(array('notice_gov_id' => session('regulatorpersonInfo.fregulatorid')))
				->where($where)
				->limit($Page->firstRow . ',' . $Page->listRows)
				->select();//查询违法广告
		}
		$this->assign('page', $Page->show());//分页输出
		$this->assign('data', $data); //列表信息
		$this->display('reportList');
	}

	/*添加公告*/
	public function cadd()
	{
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->display('reportEdit');
	}

	/*添加公告AJAX*/
	public function ajax_addNotice(){

		$lx_notice_model=M("lx_notice");
		$lx_notice_model->startTrans();//开启事务
		$lx_notice_receive_model=M("lx_notice_receive");
		$lx_notice_receive_model->startTrans();//开启事务
		$lx_uploads_file_model=M("lx_uploads_file");
		$lx_uploads_file_model->startTrans();//开启事务

		//1，添加通知
		$data['notice_content'] = I('notice_content');
		$data['notice_title'] = I('notice_title');
		$data['notice_gov_id'] = session('regulatorpersonInfo.fregulatorid');//通知发送部门
		$data['notice_post_time'] = date('Y-m-d H:i:s');
		$data['notice_user_id'] = session('regulatorpersonInfo.fid');///通知发送人id
		$data['notice_user_name'] = session('regulatorpersonInfo.fname');///通知发送人姓名
		if(session('regulatorpersonInfo.regulator_type')=='20'){
			$count=M("tjointmember")->where(array('fmainid'=>session('regulatorpersonInfo.fregulatorid')))->count();//该部门对应的联席个数
		}elseif(session('regulatorpersonInfo.regulator_type')=='30'){
			$fmainid = M("tjointmember")->where(array('fmemberid'=>session('regulatorpersonInfo.fregulatorid')))->getField('fmainid');//联系对应人员
			$count=M("tjointmember")->where(array('fmainid'=>$fmainid))->count();//该部门对应的联席个数
		}
		if($count=='0'){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有对应的联席部门。'));
		}
		$data['notice_requie_times'] = $count;
		$reg =  $lx_notice_model->add($data);

		//2，添加通知接收
		if(session('regulatorpersonInfo.regulator_type')=='20'){
			$list = M("tjointmember")->where(array('fmainid'=>session('regulatorpersonInfo.fregulatorid')))->getField('fmemberid',true);//工商对应联席人员
		}elseif(session('regulatorpersonInfo.regulator_type')=='30'){
			$list = M("tjointmember")->where(array('fmainid'=>$fmainid,'fmemberid'=>array('neq',session('regulatorpersonInfo.fregulatorid'))))->getField('fmemberid',true);//联系对应人员
			$list[]=$fmainid; //把工商加进去
		}

		foreach($list as $k=>$v){
			$dataList[] = array('notice_receive_id'=>$reg,'notice_receive_gov_id'=>$v);
		}
		$res = $lx_notice_receive_model->addAll($dataList);

		if($reg && $res){
			$lx_notice_model->commit();//成功则提交
			$lx_notice_receive_model->commit();//成功则提交
		}else{
			$lx_notice_model->rollback();
			$lx_notice_receive_model->rollback();
			$this->ajaxReturn(array('code'=>-1,'msg'=>'提交复核失败！'));

		}

		//附件
		if(I('attachinfo')){
			$attachinfo = I('attachinfo');
			foreach ($attachinfo as $key => $value) {
				if($value['id'] == '') $attachlist[] = $value;
			}
			if($attachlist){
				$attach_data['user_id'] = session('regulatorpersonInfo.fid');
				$attach_data['modelname'] ='公告';
				$attach_data['modelid'] =$reg;
				$attach_data['ffilename'] = ' ';
				$attach_data['fuploadtime'] = date('Y-m-d H:i:s',time());
				foreach ($attachlist as $key => $value) {
					$attach_data['fattachname'] = $value['name'];
					$attach_data['fattachurl'] = $value['url'];
					$attach_data['ffilename'] = preg_replace('/\..*/','',$value['url']);
					$attach_data['ffiletype'] = preg_replace('/.*\./','',$value['url']);
					$attach[$key] = $attach_data;
				}
				$attachid = $lx_uploads_file_model->addAll($attach);
				if($attachid){
					$lx_uploads_file_model->commit();//成功则提交
				}else{
					$lx_uploads_file_model->rollback();
					$this->ajaxReturn(array('code'=>-1,'msg'=>'附件添加失败！'));

				}
			}
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'公告发送成功！'));
	}

	/*已收通告详情*/
	public function details()
	{
		if(I('receive_id')){
			$receive_id = I('receive_id');//接收id
		}else{
			$this->error('缺失receive_id');
		}

		/*1,埋点 修改lx_notice_receive*/
		$receive_data = M('lx_notice_receive')->where(array('id'=>$receive_id))->find();//当前接收详情
		$notice_id =$receive_data['notice_receive_id'];//通知id

		$arr_uid = explode(",",$receive_data['notice_read_user_id']);
		//是否已经读过
		if(!in_array(session('regulatorpersonInfo.fid'),$arr_uid)){
			$arr['notice_receive_status'] = 2;//已读
			if(empty($receive_data['notice_read_user_id'])){
				$arr['notice_read_user_id'] = session('regulatorpersonInfo.fid');//阅读人员id
			}else{
				$arr['notice_read_user_id'] = $receive_data['notice_read_user_id'].','.session('regulatorpersonInfo.fid');//阅读人员id ,拼接
			}
			$arr['notice_read_user_num']=$receive_data['notice_read_user_num']+1;//本部门已阅人总数
			if(empty($receive_data['notice_read_log'])){
				$arr['notice_read_log'] = date('Y-m-d H:i:s').'=>'.session('regulatorpersonInfo.fid');
			}else{
				$arr['notice_read_log'] = $receive_data['notice_read_log'].','.date('Y-m-d H:i:s').'=>'.session('regulatorpersonInfo.fid');//首次阅读时间拼接，与阅读人员id对应
			}
			$receive = M('lx_notice_receive')->where(array('id'=>$receive_id))->save($arr);

			/*2,埋点 修改lx_notice*/
			$read_num = M('lx_notice_receive')
				->where(array('notice_receive_id'=>$notice_id,'notice_receive_status'=>2))
				->count();// 查询满足要求的总记录数

			$notice = M('lx_notice')->where(array('notice_id'=>$notice_id))->save(array('notice_read_times'=>$read_num));
		}

		/*通告详情*/
		$data = M('lx_notice_receive')
			->field('lx_notice.*,
					 lx_notice_receive.id as receive_id,
					 lx_notice_receive.notice_read_user_id,
					 lx_notice_receive.notice_read_user_num,
					 lx_notice_receive.notice_read_log
					 ')
			->join('lx_notice on lx_notice.notice_id = lx_notice_receive.notice_receive_id', 'LEFT')//通知主表
			->where(array('lx_notice_receive.id'=>$receive_id))
			->find();//已收通告详情

		//附件信息
		$attach = M('lx_uploads_file')->where(array('modelname' =>'公告','modelid' =>$notice_id))->select();
		$attach_count = count($attach);

		//评论
		$answer = M('lx_notice_answer')->where(array('notice_id' =>$notice_id))->select();

		$this->assign('data', $data); //违法信息
		$this->assign('attach',$attach);//附件
		$this->assign('attach_count',$attach_count);//附件数量
		$this->assign('answer',$answer);//评论
		$this->display('report');
	}

	/*已发通告详情*/
	public function sendDetails(){
		if(I('notice_id')){
			$notice_id = I('notice_id');//通知
		}else{
			$this->error('缺失notice_id');
		}
		/*通告详情*/
		$data = M('lx_notice')->where(array('notice_id'=>$notice_id))->find();//通告详情

		//附件信息
		$attach = M('lx_uploads_file')->where(array('modelname' =>'公告','modelid' =>$notice_id))->select();
		$attach_count = count($attach);
		//评论
		$answer = M('lx_notice_answer')->where(array('notice_id' =>$notice_id))->select();

		$this->assign('data', $data); //违法信息
		$this->assign('attach',$attach);//附件
		$this->assign('attach_count',$attach_count);//附件数量
		$this->assign('answer',$answer);//评论
		$this->display('report');

	}

	/*发布评论AJAX*/
	public function ajax_addComment(){
		if(I('notice_id')){
			$notice_id = I('notice_id');//通知id
		}else{
			$this->error('缺失notice_id');
		}
		$lx_notice_answer_model=M("lx_notice_answer");
		$lx_notice_answer_model->startTrans();//开启事务

		//1，添加评论表
		$data['notice_id'] = $notice_id;//通知回复内容
		$data['notice_answer_content'] = I('notice_answer_content');//通知回复内容
		$data['notice_answer_user_id'] = session('regulatorpersonInfo.fid');///通知评论人id
		$data['notice_answer_user_name'] = session('regulatorpersonInfo.fname');///通知评论人姓名
		$data['notice_answer_time'] = date('Y-m-d H:i:s');
		$reg =  M("lx_notice_answer")->add($data);
		if($reg){
			$lx_notice_answer_model->commit();//成功则提交
			$this->ajaxReturn(array('code'=>0,'msg'=>'公告发送成功！'));
		}else{
			$lx_notice_answer_model->rollback();
			$this->ajaxReturn(array('code'=>-1,'msg'=>'提交复核失败！'));

		}

	}
	//删除附件
	public function ajax_delete_attach(){

		$file_id = I('file_id');
		if(file_id) M('lx_uploads_file')->where(array('file_id' => $file_id))->delete();
		$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
	}
}