<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 检查计划
 * by zw
 */

class GjcjihuaController extends BaseController{

    /**
     * 检查计划提交动作
     * by zw
     */
    public function jcjihua_addaction(){
        $years  = I('years')?I('years'):date('Y');//检查年份
        $months = I('months')?I('months'):date('m');//检查月份
        $cjlist = I('cjlist');//对应抽检关系数组
        $adddata = [];

        $where_dit['dit_year']      = $years;
        $where_dit['dit_month']     = $months;
        M('tbn_data_inspect')->where($where_dit)->delete();
        foreach ($cjlist as $key => $value) {
            $data_dit['dit_year']      = $years;
            $data_dit['dit_month']     = $months;
            $data_dit['dit_tnid']      = $value['dit_tnid'];
            $data_dit['dit_covertnid'] = $value['dit_covertnid'];
            $data_dit['dit_adduserid'] = session('regulatorpersonInfo.fid');
            $data_dit['dit_addtime'] = date('Y-m-d H:i:s');
            $adddata[] = $data_dit;
        }

        if(!empty($adddata)){
            M('tbn_data_inspect')->addall($adddata);
        }

        $this->ajaxReturn(array('code'=>0,'msg'=>'保存成功'));

    }

    /**
     * 获取检查计划
     * by zw
     */
    public function getjihu_list(){
        $years  = I('years')?I('years'):date('Y');//检查年份
        $months = I('months')?I('months'):date('m');//检查月份

        $where_dit['dit_year'] = $years;
        $where_dit['dit_month'] = $months;
        $do_dit = M('tbn_data_inspect')
            ->field('dit_tnid,dit_covertnid')
            ->where($where_dit)
            ->order('dit_covertnid asc')
            ->select();

        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>count($do_dit),'list'=>$do_dit)));
    }

    /**
     * 获取抽检机构
     * by zw
     */
    public function getcjjigou_list(){
        $where_tn['flevel'] = array('in',[1,2,3]);
        $where_tn['fstate'] = 1;
        $do_tn = M('tregion')
            ->field('fid,fname1')
            ->where($where_tn)
            ->order('fid asc')
            ->select();

        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>count($do_tn),'list'=>$do_tn)));
    }


}