<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 国家局重点案件线索结果上报
 * by zw
 */

class GzdajjieguoController extends BaseController{
  /**
  * 重点线索线索待处理列表
  * by zw
  */
  public function dclzdanjian_list(){
    session_write_close();
    $system_num = getconfig('system_num');//客户编号

    $p  = I('page', 1);//当前第几页
    $pp = 20;//每页显示多少记录

    $model = I('model')?I('model'):0;//模板类型

    $where_tia['a.fmodel_type']   = $model;
    $where_tia['a.fstatus']       = 20;
    $where_tia['a.fdomain_isout'] = 0;
    $where_tia['a.fcustomer'] = $system_num;
    $where_tia['b.frece_reg_id'] = session('regulatorpersonInfo.fregulatorpid');
    $where_tia['b.fstatus']       = 0;

    $count = M('tbn_illegal_zdad a')
      ->join('tbn_zdcase_send b on a.fid = b.fillegal_ad_id')
      ->where($where_tia)
      ->count();//查询满足条件的总记录数
    
    $do_tia = M('tbn_illegal_zdad a')
      ->field('a.*')
      ->join('tbn_zdcase_send b on a.fid = b.fillegal_ad_id')
      ->where($where_tia)
      ->order('a.fid desc')
      ->page($p,$pp)
      ->select();
  
    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
  }

  /**
  * 重点案件线索上报
  * by zw
  */
  public function sbzdanjian(){
    $system_num = getconfig('system_num');//客户编号
    $selfid     = I('selfid');//违法广告ID组
    $attachinfo = I('attachinfo');//上传的文件信息
    $selfresult = I('selfresult');//处理结果
    $attach     = [];//预添加的附件信息
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    if(empty($attachinfo)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请上传证据'));
    }
    if(!empty($selfresult)){
      foreach ($selfid as $value) {
        $where_tia['a.fid']           = $value;
        $where_tia['a.fstatus']       = 20;
        $where_tia['a.fdomain_isout'] = 0;
        $where_tia['a.fcustomer'] = $system_num;
        $where_tia['b.frece_reg_id'] = session('regulatorpersonInfo.fregulatorpid');
        $where_tia['b.fstatus']       = 0;

        $count = M('tbn_illegal_zdad a')
          ->join('tbn_zdcase_send b on a.fid = b.fillegal_ad_id')
          ->where($where_tia)
          ->count();//查询满足条件的总记录数

        if(!empty($count)){
          $data_tia['fstatus']          = 30;//已上报
          $data_tia['fview_status']     = 10;//已查看
          $data_tia['fresult']          = $selfresult;//处理结果
          $data_tia['fresult_datetime'] = date('Y-m-d H:i:s');//上报时间
          $data_tia['fresult_unit']     = session('regulatorpersonInfo.regulatorpname');//上报机构
          $data_tia['fresult_user']     = session('regulatorpersonInfo.fname');//上报人
          $do_tia = M('tbn_illegal_zdad')->where(['fid'=>$value])->save($data_tia);
        }else{
          $do_tia = '';
        }

        if(!empty($do_tia)){
          M()->execute('update tbn_zdcase_send set fstatus=10,freceiver="'.session('regulatorpersonInfo.fname').'",frece_time=CURDATE() where fillegal_ad_id='.$value.' and frece_reg_id='.session('regulatorpersonInfo.fregulatorpid').' and fstatus=0');//更新上级派发任务的处理状态

          //上传附件
          $attach_data['fillegal_ad_id']  = $value;//违法广告
          $attach_data['fuploader']       = session('regulatorpersonInfo.fname');//上传人
          $attach_data['fupload_time']    = date('Y-m-d H:i:s');//上传时间
          $attach_data['ftype']           = 10;//附件类型
          foreach ($attachinfo as $key2 => $value2){
            $attach_data['fattach']     = $value2['fattachname'];
            $attach_data['fattach_url'] = $value2['fattachurl'];
            $attach[] = $attach_data;
          }
        }
      }
      if(!empty($attach)){
        M('tbn_illegal_zdad_attach')->addAll($attach);
        D('Function')->write_log('重点案件线索上报',1,'上报成功','tbn_illegal_zdad',0,M('tbn_illegal_zdad')->getlastsql());
        $this->ajaxReturn(array('code'=>0,'msg'=>'上报成功'));
      }else{
        D('Function')->write_log('重点案件线索上报',0,'上报失败','tbn_illegal_zdad',0,M('tbn_illegal_zdad')->getlastsql());
        $this->ajaxReturn(array('code'=>1,'msg'=>'上报失败'));
      }
    }else{
      D('Function')->write_log('重点案件线索上报',0,'请选择处理结果','tbn_illegal_zdad',0);
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择处理结果'));
    }
  }

  /**
  * 重点案件线索派发
  * by zw
  */
  public function pfzdanjian(){
    $system_num = getconfig('system_num');//客户编号

    $selfid     = I('selfid');//违法广告ID组
    $tregionid  = I('tregionid');//行政区划ID

    $adddata    = [];
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    if(empty($tregionid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择需要派发的机构'));
    }
    $where_tr['fregionid'] = $tregionid;
    $where_tr['_string'] = 'fstate=1 and ftype=20 and fkind=1';
    $do_tr = M('tregulator')->field('fid,fname')->where($where_tr)->find();
    if(empty($do_tr)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'该区域还未建立相应机构'));
    }
    foreach ($selfid as $key => $value) {
      M()->execute('update tbn_illegal_zdad set fview_status=0 and fcustomer = "'.$system_num.'" where fid='.$value);//更新查看状态
      M()->execute('update tbn_zdcase_send set fstatus=20,freceiver="'.session('regulatorpersonInfo.fname').'",frece_time=CURDATE() where fillegal_ad_id='.$value.' and frece_reg_id='.session('regulatorpersonInfo.fregulatorpid').' and fstatus=0');//更新上级派发任务的处理状态

      //将需要添加的派发记录放到数组中
      $data_tcs['fillegal_ad_id'] = $value;
      $data_tcs['frece_reg_id']   = $do_tr['fid'];
      $data_tcs['frece_reg']      = $do_tr['fname'];
      $data_tcs['fsend_reg_id']   = session('regulatorpersonInfo.fregulatorpid');
      $data_tcs['fsend_reg']      = session('regulatorpersonInfo.regulatorpname');
      $data_tcs['fsender']        = session('regulatorpersonInfo.fname');
      $data_tcs['fsend_time']     = date('Y-m-d H:i:s');
      array_push($adddata, $data_tcs);
    }
    if(!empty($adddata)){
      M('tbn_zdcase_send')->addAll($adddata);
      D('Function')->write_log('重点案件线索上报',1,'重点线索派发成功','tbn_illegal_zdad',0);
      $this->ajaxReturn(array('code'=>0,'msg'=>'派发成功'));
    }else{
      D('Function')->write_log('重点案件线索上报',0,'重点线索派发失败','tbn_illegal_zdad',0);
      $this->ajaxReturn(array('code'=>1,'msg'=>'派发失败'));
    }
  }

}