// 关闭窗口并清空内容函数
function closeModal(obj1, obj2) {
    $(document).on('click', obj1, function () {
        if ($('#mediaclass-select')) {
            $('#mediaclass-select').hide();
        }
        if ($('#region-select')) {
            $('#region-select').hide();
        }
        if ($('input[type=radio]').length > 0) {
            $('input[type=radio]').iCheck('uncheck');
        }
        if ($('input[type=time]').length > 0) {
            $('input[type=time]').val('');
        }
        if ($('textarea').length > 0) {
            $('textarea').val('');
        }
        if ($('#infoDiv') && $('#ctrlDiv')) {
            $('#infoDiv').show();
            $('#ctrlDiv').hide();
        }
        if ($('#liveIframe')) {
            $('#liveIframe').attr('src', '');
        }
        if ($('#backMsg')) {
            $('#backMsg').hide();
        }
        if ($('#videoModal').length > 0) {
            $('#videoModal').find('video').attr('src', '');
        }
        if ($('.vidoeBlock').length > 0) {
            $('.vidoeBlock').find('video').attr('src', '');
        }
        if ($(obj2).find(".tvIcon").length > 0) {
            $(obj2).find(".tvIcon").attr("src", "");
        }

        $(obj2).modal('hide');
        $(obj2).find('input[type=text],input[type=password],textarea,select').val('');
    })
}
var var_func;
var var_func2;
function Delete(text, func, func2) {
    if ($('#deleteModal')) {
        $('#deleteModal').remove();
    }
    $('body').append('<div class="modal inmodal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">\
        <div class="modal-dialog">\
            <div class="modal-content" style="padding:40px 0px 10px 0px;">\
                <h2 class="h2 text-center">'+ text + '</h2>\
                <div class="container-fluid">\
                    <div class="row text-center" style="margin:30px 0">\
                        <button type="button" class="btn btn-primary" id="Delte_yes" style="margin-right:20px;">确认</button>\
                        <button type="button" class="btn btn-danger" id="Delte_no" style="margin-left:20px;">取消</button>\
                    </div>\
                </div>\
            </div>\
        </div>\
    </div>')
    $('#deleteModal').modal('show');
    var_func = func;
    if (func2) {
        var_func2 = func2;
    }
}
$(document).on('click', '#Delte_yes', function () {
    var_func()
})
$(document).on('click', '#Delte_no', function () {
    if (var_func2) {
        var_func2();
    }
    $('#deleteModal').modal('hide');
    return false;
})

$(document).on('click', '.J_menuItem2', function () {
    let href = $(this).attr('href');
    let name;
    if ($(this).attr('title2')) {
        name = $(this).attr('title2');
    } else {
        name = $(this).text()
    }

    let have = false;
    $.each($("body", parent.document).find('.page-tabs-content a'), function () {
        if ($(this).attr('data-id') == href) {
            $(this).addClass('active');
            $("body", parent.document).find('.J_tabShowActive').click();
            have = true;
        } else {
            $(this).removeClass('active');
        }
    });

    if (have == true) {
        for (let i = 0; i < $("body", parent.document).find('#content-main iframe').length; i++) {
            if ($("body", parent.document).find('#content-main iframe').eq(i).attr('data-id') == href) {
                $("body", parent.document).find('#content-main iframe').hide();
                $("body", parent.document).find('#content-main iframe').eq(i).css('display', 'inline');
            }
        }
    } else {
        $("body", parent.document).find('.page-tabs-content a').removeClass('active');
        $("body", parent.document).find('.page-tabs-content').append('<a href="javascript:;" class="J_menuTab active" data-id="' + href + '">' + name + ' <i class="fa fa-times-circle"></i></a>');
        $("body", parent.document).find('.J_tabShowActive').click();
        $("body", parent.document).find('#content-main iframe').hide();
        $("body", parent.document).find('#content-main').append('<iframe style="display:inline" class="J_iframe" name="iframe0" width="100%" height="100%" src="' + href + '" frameborder="0" data-id="' + href + '" seamless></iframe>')
    }
})
function alertMSG(msg, type, time, callback) {
    if (!msg) {
        alert('请输入打印信息!')
        return false;
    }
    $("#alertMsg").remove();
    $('body').append(`
    <div style="position: fixed;width: 50%;bottom: 10%;left: 50%;transform: translateX(-50%);z-index: 9999;" class="alert alert-`+ type + ` text-center" id="alertMsg">` + msg + `</div>
    `)
    if (callback) {
        $('#alertMsg').fadeOut(time, callback)
    } else {
        $('#alertMsg').fadeOut(time)
    }
}

function tagJump(addr,fid) {
    var thispage = window.location.pathname;
    if ($("body", parent.document).find("iframe[data-id='" + addr + "']").length) {
        if (fid) {
            $("body", parent.document).find("iframe[data-id='" + addr + "']").show().contents().find('tr[data=' + fid + ']').remove();
        } else {
            $("body", parent.document).find("iframe[data-id='"+ addr +"']").attr('src',addr).show();
        }
        $("body", parent.document).find('.page-tabs-content a[data-id="'+ addr +'"]').addClass('active').siblings().removeClass('active');
    }
    $("body", parent.document).find('.page-tabs-content a[data-id="' + thispage + '"] .fa-times-circle').click();
}
function closePage() {
    let host = window.location.host
    let url = window.location.href.split('//').pop()
    let thispage = url.replace(host,'')
    $("body", parent.document).find('.page-tabs-content a[data-id="'+ thispage +'"] .fa-times-circle').click();
}

function hasPermission(booleam, str, func) {
    if (!booleam) {
        $(str).unbind().attr('disabled',true).attr('title','您没有此权限')
        if(func) func()
    }
}

function getQueryString(name) {
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    let r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
  }