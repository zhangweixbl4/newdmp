<?php
namespace Common\Model;



class IllegalModel{

	public function get_illegal_list(){
		
		$IllegalList = M('tillegal')->where(array('fpcode'=>'','fstate'=>1))->select();//查询一级分类
		
		foreach($IllegalList as $k => $Illegal){
			$IllegalList[$k] = $Illegal;
			$IllegalList[$k]['list_2'] = M('tillegal')->where(array('fpcode'=>$Illegal['fcode'],'fstate'=>1))->select();//查询二级分类
		}
		return $IllegalList;
	}
	
	
	
	/**
	 * @param $sample_id 样本 id
	 * @param $media_type 媒体种类 tv , bc , paper
	 * by hs
	 */
	public function set_tillegalad_data($sample_id,$media_type) {
		$sample_table ='t'.$media_type.'sample';//样本表名称
		$issue_table ='t'.$media_type.'issue';//发布表名称
		if($media_type =='paper'){//如果是报纸，因为标志的样本主键id字段不一样
			$sample= M($sample_table)->where(array('fpapersampleid' => $sample_id,'fstate' =>1))->find();//查找样本信息
		}else{
			$sample= M($sample_table)->where(array('fid' => $sample_id,'fstate' =>1))->find();//查找样本信息
		}
		
		
		if(isset($sample['fillegaltypecode']) && !empty($sample['fillegaltypecode'])){//判断是否违法
			$issueList=M($issue_table)->field('
											*,
											min(fissuedate)as ffirstissuetime,
											max(fissuedate) as flastissuetime,
											count(fissuedate) as fissuetimes
										')->where(array('f'.$media_type.'sampleid' => $sample_id,'fstate' =>1))->group('fmediaid')->select();//播放记录，fbcsampleid,ftvsampleid
			foreach($issueList as $k=>$issue){
				
				$is_set=M('tillegalad')->where(array('fmediaid' =>$issue['fmediaid'],'fsampleid' =>$sample_id,'fstate'=>array('in','0,1')))->find();//是否有正在处理的违法信息
				if($is_set){
					/*有正在处理的，更新*/
					$arr['ffirstissuetime']=$issue['ffirstissuetime'];//首次播出日期
					$arr['flastissuetime']=$issue['flastissuetime'];//未次播出日期
					$arr['fissuetimes']=$issue['fissuetimes'];//发布次数
					$res = M("tillegalad")->where(array('fillegaladid' => $is_set['fillegaladid']))->save($arr);
				}else {
					/*新增违法广告表*/
					$list = array();
					$list['fmediaclassid'] = $media_type;//媒介类别ID
					$list['fmediaid'] = $issue['fmediaid'];//媒介ID
					$list['fsampleid'] = $sample_id;//样本ID
					$list['fadid'] = $sample['fadid'];//广告ID
					$list['fversion'] = $sample['fversion'];//版本
					$list['fspokesman'] = $sample['fspokesman'];//代言人
					$list['fapprovalid'] = $sample['fapprovalid'];//审批号
					$list['fapprovalunit'] = $sample['fapprovalunit'];//审批单位
					$list['fadlen'] = $sample['fadlen'];//广告长度
					$list['fillegaltypecode'] = $sample['fillegaltypecode'];//违法类型代码
					$list['fillegalcontent'] = $sample['fillegalcontent'];//涉嫌违法内容
					$list['fexpressioncodes'] = $sample['fexpressioncodes'];//违法表现代码
					$list['fexpressions'] = $sample['fexpressions'];//违法表现
					$list['fconfirmations'] = $sample['fconfirmations'];//认定依据
					$list['fpunishments'] = $sample['fpunishments'];//处罚依据
					$list['fpunishmenttypes'] = $sample['fpunishmenttypes'];//处罚种类及幅度
					$list['ffirstissuetime'] = $issue['ffirstissuetime'];//首次播出日期
					$list['flastissuetime'] = $issue['flastissuetime'];//未次播出日期
					$list['fissuetimes'] = $issue['fissuetimes'];//发布次数
					if ($media_type == 'peper') {
						$list['favifilename'] = $sample['fjpgfilename'];//图片文件名
					} else {
						$list['favifilename'] = $sample['favifilename'];//视频文件名
					}
					$list['tsource'] = $issue['fsource'];//来源
					$list['fcreator'] =  $sample['fcreator'];//创建人
					$list['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
					$fdisposetimes=M('tillegalad')->where(array('fmediaid' =>$issue['fmediaid'],'fsampleid' =>$sample_id,'fstate'=>2))->count();
					$list['fdisposetimes'] = $fdisposetimes;//处置次数
					$list['fdisposestyle'] = 0;//处置方式（0-待定，1-指导, 2 ,合并线索，3，复核，4，线索，5，不予处理，10，其他）
					$list['fstate'] = 0;//状态（0-待处理，1-正处理，2-已处理）
					$res = M("tillegalad")->add($list);
				}
			}
		}
	}
    

}