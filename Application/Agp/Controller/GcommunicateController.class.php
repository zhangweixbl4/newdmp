<?php
namespace Agp\Controller;
use Think\Controller;

class GcommunicateController extends BaseController
{
      /**
       * 发送消息
       */
      public function sendMessage()
      {
          $title = I('title','');//信息标题
          $content = I('content','');//消息内容
          $attachinfo = I('attachinfo');//上传的文件信息
      	  $receiveinfo = I('tregionid');//接受的机构信息
      	  if(empty($receiveinfo)){
      		  $this->ajaxReturn(array('code'=>-1,'msg'=>'请选择需要派发的机构'));
      		}
          $data['me_title'] = $title;
          $data['me_content'] = $content;
          $data['me_sendtime'] = date('Y-m-d H:i:s');//添加时间
          $data['me_sendtreid'] = session('regulatorpersonInfo.fregulatorpid');//发送机构ID
          $data['me_sendtrename'] = session('regulatorpersonInfo.regulatorpname');//发送机构名
          $data['me_sendusername'] = session('regulatorpersonInfo.fname');//上传人
          $data['me_senduserid'] = session('regulatorpersonInfo.fid');//上传人id
          $message_id = M('tbn_message')->add($data);
          if($message_id){
            $this->addFileList($attachinfo ,$message_id);//添加附件信息
			      $this->addMeReceive($receiveinfo ,$message_id);//添加接收消息机构
            D('Function')->write_log('监管动态交流',1,'消息发送成功','tbn_message',$message_id,M('tbn_message')->getlastsql());
            $this->ajaxReturn(array('code'=>0,'msg'=>'消息发送成功'));
          }else{
            D('Function')->write_log('监管动态交流',0,'消息发送失败','tbn_message',0,M('tbn_message')->getlastsql());
            $this->ajaxReturn(array('code'=>-1,'msg'=>'消息发送失败'));
          }

      }

      public function tregionList()
      {
        $system_num = getconfig('system_num');

        $regionid = session('regulatorpersonInfo.regionid');//机构行政区划ID
        $tregion_len = get_tregionlevel($regionid);
        $res = [];
        $res1 = [];
        $res2 = [];
        $wheres1 = ' and 1=1';
        if($system_num == '100000'){
          $wheres1 .= ' and flevel in (1,2,3)';
        }
        if($tregion_len == 1){//国家级
          $res = M('tregion')->field('fname,fid,fpid')->where('fid='.$regionid.$wheres1)->select();//国家
          $res1 = M('tregion')->field('fname,fid,fpid')->where('fpid='.$regionid.$wheres1)->select();//国家下面的所有省
          $res3 = array_merge($res,$res1);
        }elseif($tregion_len == 2){//省级
          $res = M('tregion')->field('fname,fid,fpid')->where('fid=100000'.$wheres1)->select();//国家;
          $res1 = M('tregion')->field('fname,fid,fpid')->where('fpid=100000'.$wheres1)->select();//所有省
          $res2 = M('tregion')->field('fname,fid,fpid')->where('fpid='.$regionid.$wheres1)->select();//本省下面的所有市
          $res3 = array_merge($res,$res1,$res2);
        }elseif($tregion_len == 4){//市级
          $res = M('tregion')->field('fname,fid,fpid')->where('fid='.substr($regionid , 0 , 2).'0000'.$wheres1)->select();//上级所属省
          $res1 = M('tregion')->field('fname,fid,fpid')->where('fpid='.substr($regionid , 0 , 2).'0000'.$wheres1)->select();//同级市
          $res2 = M('tregion')->field('fname,fid,fpid')->where('fpid='.$regionid)->select();//本市下面的所有区县
          $res3 = array_merge($res,$res1,$res2);
        }elseif($tregion_len == 6){//县级
          $res = M('tregion')->field('fname,fid,fpid')->where('fid='.substr($regionid , 0 , 4).'00'.$wheres1)->select();//上级所属市
          $res1 = M('tregion')->field('fname,fid,fpid')->where('fpid='.substr($regionid , 0 , 4).'00'.$wheres1)->select();//同级区县
          $res3 = array_merge($res,$res1);
        }
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$res3));
      }

      //添加附件信息
      protected function addFileList($attachinfo ,$message_id)
      {

          $attach_data['mef_meid'] = $message_id;
          $attach_data['mef_uploadtreid'] = session('regulatorpersonInfo.fregulatorpid');//机构ID
          $attach_data['mef_uploadtrename'] = session('regulatorpersonInfo.regulatorpname');//机构名
          $attach_data['mef_uploader'] = session('regulatorpersonInfo.fname');//上传人
          $attach_data['mef_uploadtime'] = date('Y-m-d H:i:s');//上传时间
          $attach = [];
          foreach ($attachinfo as $key2 => $value2){
              $attach_data['mef_filename'] = $value2['fattachname'];
              $attach_data['mef_url'] = $value2['fattachurl'];
              array_push($attach,$attach_data);
          }

          M('tbn_message_file')->addAll($attach);

      }
	  
	  //添加接收消息机构
	  protected function addMeReceive($receiveinfo ,$message_id)
      {

          $attach_data['mrr_meid'] = $message_id;
            
      	  $do_tr = M('tregulator')->field('fid,fname')->where(array('fregionid'=>array('in',$receiveinfo),'fstate'=>1,'fkind'=>1,'ftype'=>20))->select();
          $attach = [];
      	  foreach ($do_tr as $key2 => $value2){
      		  $attach_data['mrr_receivetreid'] = $value2['fid'];//接收机构ID
      		  $attach_data['mrr_receivetrename'] = $value2['fname'];//接收机构名称
      		  array_push($attach,$attach_data);
      	  }

          M('tbn_message_receiveuser')->addAll($attach);

      }
	  

      //消息反馈
      public function feedbackInfo()
      {
          $mrr_id = I('id');//接收消息表id
          $content = I('content');
		      $data['mrr_id'] = $mrr_id;
          $data['mrr_receiveuserid'] = session('regulatorpersonInfo.fid');//回复用户id
          $data['mrr_receiveusername'] = session('regulatorpersonInfo.fname');//回复用户姓名
          $data['mrr_receivetime'] = date('Y-m-d H:i:s');//回复时间
          $data['mrr_content'] = $content;//反馈内容
          $data['mrr_status'] = 10;
          $res = M('tbn_message_receiveuser')->where('mrr_receivetreid='.session('regulatorpersonInfo.fregulatorpid'))->save($data);
          if($res){
              D('Function')->write_log('监管动态交流',1,'反馈成功','tbn_message_receiveuser',$res,M('tbn_message_receiveuser')->getlastsql());
              $this->ajaxReturn(array('code'=>0,'msg'=>'反馈成功'));
          }else{
              D('Function')->write_log('监管动态交流',0,'反馈失败','tbn_message_receiveuser',0,M('tbn_message_receiveuser')->getlastsql());
              $this->ajaxReturn(array('code'=>-1,'msg'=>'反馈失败'));
          }

      }
	  
	  //单个消息详情
	  public function messageInfo()
	  {

  		  $mrr_id = I('id');//接收消息表id
        $where['mrr_id'] = $mrr_id;
        $data = M('tbn_message_receiveuser')
            ->alias('mr')
            ->field('
              m.me_title,
              m.me_content,
              m.me_sendtrename,
              mr.mrr_id,
              mr.mrr_meid,
              mr.mrr_receivetrename,
              mr.mrr_content
            ')
            ->join('tbn_message as m on mr.mrr_meid = m.me_id','LEFT')
            ->where($where)
            ->find();
        $data['file_list'] = M('tbn_message_file')
            ->field('mef_filename,mef_url')
            ->where(array('mef_meid'=>$data['mrr_meid']))
            ->select();//发送附件
  		  if($data){
  			  $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
  		  }else{
  			  $this->ajaxReturn(array('code'=>-1,'msg'=>'获取失败'));
  		  }
	  }
	  
	  //单个接收消息详情
	  /*public function getMessageInfo()
	  //{
		//  $id = I('id');
		  $mrr_meid = M('tbn_message_receiveuser')->where(array('mrr_id'=>$id))->getField('mrr_meid');
		  $res = M('tbn_message')->field('me_id,me_title,me_content,me_sendtrename')->find($mrr_meid);
		  if($res){
              $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$res));
          }else{
              $this->ajaxReturn(array('code'=>-1,'msg'=>'获取失败');
          }
	  }*/
	  
	  //接收信息列表
	  public function getMessageList()
	  {
      Check_QuanXian(['xxlbjiaoliu']);
		  $id = session('regulatorpersonInfo.fregulatorpid');//机构ID
      $p  = I('page', 1);//当前第几页
      $pp = 10;//每页显示多少记录
      $where['mrr_receivetreid'] = $id;
      $count = M('tbn_message_receiveuser')
          ->alias('mr')
          ->join('tbn_message as m on mr.mrr_meid = m.me_id ', 'LEFT')//接收消息
          ->where($where)
          ->count();// 查询满足要求的总记录数

      

      $data = M('tbn_message_receiveuser')
          ->alias('mr')
          ->field('
		        mr.mrr_id,
		        mr.mrr_status,
            m.me_sendtrename,
            m.me_sendtime,
            m.me_title,
            m.me_content
	         ')
          ->join('tbn_message as m on mr.mrr_meid = m.me_id ', 'LEFT')//接收消息
          ->where($where)
          ->order('mr.mrr_id desc')
          ->page($p,$pp)
          ->select();//查询消息

          $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	  }
	  
	  //发送信息列表
	  public function sendMessageList()
	  {
		  $id = session('regulatorpersonInfo.fregulatorpid');//机构ID
		  $use_id = session('regulatorpersonInfo.fid');//发送用户id
      $p  = I('page', 1);//当前第几页
      $pp = 10;//每页显示多少记录

      $where['me_sendtreid'] = $id;
      $where['me_senduserid'] = $use_id;

      $count = M('tbn_message')
          ->alias('m')
          ->join('tbn_message_receiveuser as mr on m.me_id = mr.mrr_meid','LEFT')
          ->where($where)
          ->count();// 查询满足要求的总记录数

      

      $data = M('tbn_message')
          ->alias('m')
          ->field('
            mr.mrr_id,
		        mr.mrr_status,
            mr.mrr_receivetrename,
            m.me_sendtime,
            m.me_title,
            m.me_content
          ')
          ->join('tbn_message_receiveuser as mr on m.me_id = mr.mrr_meid','LEFT')
          ->where($where)
          ->order('m.me_id desc')
          ->page($p,$pp)
          ->select();

      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	  }
	  
	  //查询消息信息
	  private function queryMessage($me_id)
	  {
		  $res = M('tbn_message')->find($me_id);
		  return $res;
	  }

}