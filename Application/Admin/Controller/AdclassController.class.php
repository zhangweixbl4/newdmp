<?php
//广告类别控制器
namespace Admin\Controller;
use Think\Controller;
class AdclassController extends BaseController {
	
    public function index(){
		
		if(A('Admin/Authority','Model')->authority('200181') === 0){
			exit('没有权限');//验证是否有新增权限
		}
		$where = array();
		$where['fpcode'] = '';
		$where['fcode'] = array('neq','0');
		
		$adclassList = M('tadclass')->where($where)->select();//查询广告类别（顶级）
		//var_dump($adclassList);
		$this->assign('adclassList',$adclassList);//广告类别列表
		$this->display();
	}
	
	
	/*异步加载广告分类列表*/
	public function ajax_adclass_list(){
		
		$fcode = I('fcode');//获取广告分类CODE
		$adclassList = M('tadclass')->where(array('fpcode'=>$fcode))->select();//通过传递过来的广告分类code查询下级广告分类
		if(!$adclassList){//判断是否有查询结果
			$this->ajaxReturn(array('code'=>-1,'msg'=>'无数据'));
		}
		$this->assign('adclassList',$adclassList);
		$this->display();
	}
	
	/*广告分类详情*/
	public function ajax_adclass_details(){
		$fcode = I('fcode');//获取广告分类CODE
		$adclassDetails = M('tadclass')->where(array('fcode'=>$fcode))->find();//查询广告分类详情
        if ($adclassDetails['ad_class_v2_ids']){
            $adclassDetails['adClass2'] = M('hz_ad_class')->field('fcode, ffullname')->where(['fcode' => ['IN', $adclassDetails['ad_class_v2_ids']]])->select();
        }
		$this->ajaxReturn(array('code'=>0,'adclassDetails'=>$adclassDetails));//ajax返回详情
	}

	/*广告类别搜索接口*/
	public function get_adclass_list(){
		$fadclass = I('fadclass');//获取监管机构名称
		$where = array();
		$where['fstate'] = array('neq',-1);
		$where['fadclass'] = array('like','%'.$fadclass.'%');
		// $where['fcode'] = array('neq','0');
		$where['fpcode'] = array('neq','');
		if($fadclass != ''){
			$adclassList = M('tadclass')->field('fcode,fadclass')->where($where)->limit(10)->select();//查询广告类别列表
		}
		$this->ajaxReturn(array('code'=>0,'value'=>$adclassList));
		
	}

	/*获取上级广告分类机构列表*/
	public function get_pid_list(){
		$fcode = I('fcode');//获取点击的广告分类
		$num = 0;
		$fpcode = array();
		while($fcode != 0 && $num < 10){
			$info = M('tadclass')->where(array('fcode'=>$fcode))->find();
			if($info['fpcode'] > 0 ){
				$fpcode[] = $info['fpcode'];
			}
			$fcode = $info['fpcode'];
			$num++;
		}
		sort($fpcode);
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','fpcode'=>$fpcode));
	}
	
	
	/*修正广告分类的全称*/
	public function adjust_full_name(){
		set_time_limit(30);
		$mList = M('tadclass')->select();
		
		foreach($mList as $m){
			
			$ffullname = $this->adjust_full_name2($m['fcode'],$m['fadclass']);
			M('tadclass')->where(array('fcode'=>$m['fcode']))->save(array('ffullname'=>$ffullname));
			$n++;
			
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'修正'.$n.'个分类'));
		
	}
	
	
	private function adjust_full_name2($fcode = '',$ffullname = '',$num = 0){
		
		$v = M('tadclass')->where(array('fcode'=>$fcode))->find();
		if ($v['fpcode'] == '' || $num > 5){
			return $ffullname;
		}else{
			return $this->adjust_full_name2($v['fpcode'],M('tadclass')->where(array('fcode'=>$v['fpcode']))->getField('fadclass').'>'.$ffullname,$num+1);
		}	

 			
	}
    /**
     * 商业类型树
     */
    private function getAdClassTree()
    {
        $data = M('hz_ad_class')->field('fcode value, fpcode, fname label, ffullname')->where(['fstatus' => ['EQ', 1]])->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpcode']])){
                $items[$value['fpcode']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }

    public function editAdClassV2($fcode)
    {
        if (IS_POST){
            $data = [
                'ad_class_v2_ids' => I('ids')
            ];
            $where = [
                'fcode' => ['EQ', $fcode]
            ];
            $res = M('tadclass')->where($where)->save($data) === false ? false : true;
            if ($res){
                $this->ajaxReturn([
                    'code' => 0,
                    'msg' => '修改成功'
                ]);
            }else{
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '修改失败'
                ]);
            }
        }else{
            $adClassV2Ids = M('tadclass')->where(['fcode' => ['EQ', $fcode]])->getField('ad_class_v2_ids');
            if ($adClassV2Ids){
                $adClassV2List = M('hz_ad_class')->where(['fcode' => ['IN', $adClassV2Ids]])->select();
            }else{
                $adClassV2List = [];
            }
            $adClassV2List = json_encode($adClassV2List);
            $adClassTree = json_encode( $this->getAdClassTree());
            $this->assign(compact('adClassTree', 'adClassV2List'));
            $this->display();
        }
    }
}