//时间选择器
$('.form_date').datetimepicker({
    language:  'zh-CN',
    weekStart: 1,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0
});

//全选 全不选 有多个全选框
$(".checkAll").click(function(){
    if(this.checked){
        $(this).parents(".table").find('input[name="ggybbh"]').prop("checked",true);
    }else{
        $(this).parents(".table").find('input[name="ggybbh"]').prop("checked",false);
    }
});

//全选 全不选 只有一个全选框
$("#checkAll").click(function(){
    if(this.checked){
        $('input[name="ggybbh"]').prop("checked",true);
    }else{
        $('input[name="ggybbh"]').prop("checked",false);
    }
});

//选择统计方式
$('select[name="stype"]').click(function(){
    //清空选项
    var selhtml=$('select[name="seldate"]').html();
    selhtml="";
    var rel=$('select[name="stype"] option:selected').val();
    if(rel==2){
        //按月统计
        var html="<option value='0'>本月</option><option value='1'>1月</option><option value='2'>2月</option><option value='3'>3月</option><option value='4'>4月</option><option value='5'>5月</option><option value='6'>6月</option><option value='7'>7月</option><option value='8'>8月</option><option value='9'>9月</option><option value='10'>10月</option><option value='11'>11月</option><option value='12'>12月</option>";
    }else if(rel==3){
        //按季度统计
        var html="<option value='0'>本季度</option><option value='1'>第一季度</option><option value='2'>第二季度</option><option value='3'>第三季度</option><option value='4'>第四季度</option>";

    }else if(rel==4){
        //按半年统计
        var html="<option value='1'>上半年</option><option value='2'>下半年</option>";
    }else if(rel==5){
        //按整年统计
        var syear=$('select[name="syear"] option:selected').val();
        var html="<option>"+syear+"年</option>";
    }
    $('select[name="seldate"]').html(html);
});

//清空多选框、单选框和隐藏表单的值
function clearInput(){
    $('.checkAll').prop("checked",false);
    $('#checkAll').prop("checked",false);
    //清空表格的多选项
    $('input[name="ggybbh"]').prop("checked",false);
    //清空处理结果的单选项
    $('input[name="result"]').prop("checked",false);
    //清空隐藏表单的值
    $("input[name='ggybbhs']").val();
    $("input[name='results']").val();
}