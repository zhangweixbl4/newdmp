<?php
namespace Agp\Controller;
use Think\Controller;
import('Vendor.PHPExcel');

/**
 * 数据检查
 */
class Inspectiontask2Controller extends BaseController{

    /**
     * 应用标识(客户行政区划代码),如：国家局100000
     */
    const CUSTOMER = '100000';

    /**
     * 用户Key
     */
    const USERID = '22638a3131d0f0a7346b178fd29f939c';

    /**
     * 接收参数
     */
    protected $P;

    /**
     * 初始化
     */
    public function _initialize(){
        parent::_initialize();
        $this->P = json_decode(file_get_contents('php://input'),true);
    }

    /**
     * 数据检查计划-获取列表
     */
    public function Index(){
        header("Content-type: text/html; charset=utf-8"); 
        session_write_close();
        Check_QuanXian(['ginspectresult2','inspectiontask2']);
        $regulatorpersonInfo = session('regulatorpersonInfo');
        if($regulatorpersonInfo['regionid'] == 100000){
            //国家局登录时，检查机关过滤条件由搜索条件传入
            $qx = Check_QuanXian(['inspectiontask2'],'');
            if($qx){
                $fcheckorg = $this->P['fcheckorg'];
            }else{
                $fcheckorg = 100000;
            }
        }else{
            //其它机构登录时，即领取分配给本机构的检查任务
            $fcheckorg = $regulatorpersonInfo['regionid'];
        }
        $fmonth = $this->P['fmonth'];
        $fregionid = $this->P['fregionid'];
        $fmediaid = $this->P['fmediaid'];
        $fstate = $this->P['fstate'];
        $pageIndex = $this->P['pageIndex'] ? $this->P['pageIndex'] : 1;
        $pageSize = $this->P['pageSize'] ? $this->P['pageSize'] : 1000;
        $outtype = $this->P['outtype'];
        $limitIndex = ($pageIndex-1)*$pageSize;
        $where = [
            'a.fcustomer' => self::CUSTOMER
        ];
        if(!empty($fmonth)){
            $where['a.fissuedate'] = ['between',[$fmonth[0],$fmonth[1]]];
        }
        if($fregionid != ''){
            $where['a.fregionid'] = $fregionid;
        }
        if($fmediaid != ''){
            $where['a.fmediaid'] = $fmediaid;
        }
        if($fcheckorg != ''){
            $where['a.fcheckorg'] = $fcheckorg;
        }
        if($fstate != ''){
            $where['a.fstate'] = $fstate;
        }
        $count = M('tbn_data_check_plan')->alias('a')->where($where)->count();
        if(!empty($outtype)){
            $dataSet = M('tbn_data_check_plan')
                ->alias('a')
                ->field('c.fmedianame,ifnull(b.zcount,0) zcount,ifnull(b.loucount,0) loucount,ifnull(b.cuocount,0) cuocount,ifnull(b.wucount,0) wucount,a.fissuedate,a.fmediaid,a.fregionid,c.fmediaclassid,d.fname fregion')
                ->join('(select fdata_check_plan_id,count(*) zcount,sum(case when fresult_type = 0 then 1 else 0 end) loucount,sum(case when fresult_type = 1 then 1 else 0 end) cuocount,sum(case when fresult_type = 2 then 1 else 0 end) wucount from tbn_data_check_result group by fdata_check_plan_id) b on a.fid = b.fdata_check_plan_id','left')
                ->join('tmedia c on a.fmediaid = c.fid')
                ->join('tregion d on a.fregionid = d.fid')
                ->where($where)
                ->order('a.fregionid asc,c.fmediaclassid asc,a.fmediaid asc,a.fissuedate asc')
                ->select();
            foreach ($dataSet as $key => $value) {
                $wherpl = [];
                $wherpl['T.fmediaid'] = $value['fmediaid'];
                $wherpl['T.fissuedate'] = strtotime($value['fissuedate']);
                if(substr($value['fmediaclassid'],0,2) == '01'){
                    $tableName = 'ttvissue_'.date('Ym',strtotime($value['fissuedate'])).'_'.substr($value['fregionid'], 0,2);
                    $samName = 'ttvsample';
                }elseif(substr($value['fmediaclassid'],0,2) == '02'){
                    $tableName = 'tbcissue_'.date('Ym',strtotime($value['fissuedate'])).'_'.substr($value['fregionid'], 0,2);
                    $samName = 'tbcsample';
                }else{
                    $dataSet[$key]['count'] = 0;
                    continue;
                }
                $dataSet[$key]['count'] = M($tableName)
                    ->alias('T')
                    ->join($samName.' as s ON s.fid=T.fsampleid')
                    ->join('tad as a ON a.fadid=s.fadid')
                    ->where($wherpl)
                    ->count();
            }
        }else{
            $dataSet = M('tbn_data_check_plan')
                ->alias('a')
                ->where($where)
                ->limit($limitIndex,$pageSize)
                ->order('a.fid desc')
                ->select();

            foreach($dataSet as $key=>$row){
                $dataSet[$key]['fregion'] = M('tregion')->cache(true,60)->where(['fid'=>$row['fregionid']])->getField('fname');
                $dataSet[$key]['fmedianame'] = M('tmedia')->cache(true,60)->where(['fid'=>$row['fmediaid']])->getField('fmedianame');
                $dataSet[$key]['fissuedate'] = date('Y-m-d',strtotime($row['fissuedate']));
                $dataSet[$key]['fplandate'] = $row['fplandate'] ? date('Y-m-d',strtotime($row['fplandate'])) : '';
                $dataSet[$key]['ffinishdate'] = $row['ffinishdate'] ? date('Y-m-d',strtotime($row['ffinishdate'])) : '';
                if($row['fcheckorg'] == 100000){
                    $dataSet[$key]['fcheckorgname'] = '第三方';
                }else{
                    $dataSet[$key]['fcheckorgname'] = M('tregulator')->cache(true,60)->where(['fid'=>'20'.$row['fcheckorg']])->getField('fname');
                }
            }
        }
        
        if(!empty($outtype)){
            if(!empty($fmonth)){
                $headstr = date('Y年m月d日',strtotime($fmonth[0])).'至'.date('m月d日',strtotime($fmonth[1]));
            }
            $html = '<body style="font-size:18px; line-height:24px;">';
            $html .= '<div align="center" style="font-size:22px; padding:10px; font-weight:bold;">'.$headstr.'广告监测数据复核情况汇总表</div>';
            $html .= '<div align="left" style="font-size:18px; padding:10px;">&nbsp;</div>';
            $html .= '<div align="left" style="font-size:18px; padding:10px;">______________省（市）市场监管局</div>';
            $html .= '<table border="1" cellpadding="0" width="100%" cellspacing="0" style="border-collapse: collapse;">';
            $html .= '<tr>
                <td align="center">序号</td><td align="center">媒体名称</td><td align="center">地域</td><td align="center">复核日期</td><td align="center">广告总条次</td><td align="center">经复核广告总条次</td><td align="center">经复核漏判条次</td><td align="center">经复核错判条次</td><td align="center">经复核误判条次</td>
            </tr>';
            $count = 0;
            $zcount = 0;
            $loucount = 0;
            $cuocount = 0;
            $wucount = 0;
            foreach ($dataSet as $key => $value) {
                $html .= '<tr>
                    <td align="center">'.($key+1).'</td>
                    <td align="center">'.$value['fmedianame'].'</td>
                    <td align="center">'.$value['fregion'].'</td>
                    <td align="center">'.date('Y年m月d日',strtotime($value['fissuedate'])).'</td>
                    <td align="center">'.$value['count'].'</td>
                    <td align="center">'.$value['zcount'].'</td>
                    <td align="center">'.$value['loucount'].'</td>
                    <td align="center">'.$value['cuocount'].'</td>
                    <td align="center">'.$value['wucount'].'</td>
                </tr>';
                $count += $value['count'];
                $zcount += $value['zcount'];
                $loucount += $value['loucount'];
                $cuocount += $value['cuocount'];
                $wucount += $value['wucount'];
            }
            $html .= '<tr>
                    <td align="center" colspan = "4">总计</td>
                    <td align="center">'.$count.'</td>
                    <td align="center">'.$zcount.'</td>
                    <td align="center">'.$loucount.'</td>
                    <td align="center">'.$cuocount.'</td>
                    <td align="center">'.$wucount.'</td>
                </tr>';
            $html .= '</table>';
            $html .= '<div align="left" style="font-size:18px; padding:10px;">&nbsp;</div>';
            $html .= '<div align="left" style="font-size:18px; padding:10px;font-weight:bold;">备注说明：</div>';
            $html .= '<div align="left" style="font-size:18px; padding:10px;">漏判：遗漏全天（24小时）原始素材中的广告</div>';
            $html .= '<div align="left" style="font-size:18px; padding:10px;">错判：（1）将非广告判定为广告；（2）将不违法的广告判定为违法广告</div>';
            $html .= '<div align="left" style="font-size:18px; padding:10px;">误判：（1）对涉嫌违法广告的具体违法表现判定不准确；（2）涉嫌违法广告适用的监测编码不准确</div>';
            $html .= '</body>';
            $date = date('Ymdhis');
            $savefilename = $date.'.doc';
            $savefile = './LOG/'.$date.'.doc';

            //将文档保存
            word_start();
            echo $html;
            word_save($savefile);
            ob_flush();//每次执行前刷新缓存
            flush();

            $ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.doc',$savefile,array('Content-Disposition' => 'attachment;filename='.$date.'.doc'));//上传云
            unlink($savefile);//删除文件
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','count'=>$count, 'data'=>$dataSet]);
        }
    }

    /**
     * 数据检查计划-新增
     */
    public function Add(){
        $fmonth = strtotime($this->P['fmonth']);
        $fmediaid = $this->P['fmediaid'];
        $fregionid = $this->P['fregionid'];
        $fissuedate = $this->P['fissuedate'];
        $fcheckorg = $this->P['fcheckorg'];
        $fplandate = $this->P['fplandate'];
        $dataSet = [];
        $succRow = [];
        $errRow = [];
        $fhtime_arr = [];
        if($fissuedate){
            $arrFissuedate = explode(',',$fissuedate);
            foreach($arrFissuedate as $key=>$val){
                $dataRow = [
                    'fcustomer' => self::CUSTOMER,
                    'fmonth' => $fmonth,
                    'fmediaid' => $fmediaid,
                    'fregionid' => $fregionid,
                    'fissuedate' => $val,
                    'fcheckorg' => $fcheckorg,
                    'fplandate' => $fplandate,
                    'fplaner' => session('regulatorpersonInfo.fid'),
                    'fplantime' => date('Y-m-d H:i:s')
                ];
                $condition = [
                    'fcustomer' => self::CUSTOMER,
                    'fmonth' => $fmonth,
                    'fmediaid' => $fmediaid,
                    'fissuedate' => $val,
                    'fcheckorg' => $fcheckorg,
                    'fplandate' => $fplandate,
                ];
                $isExist = M('tbn_data_check_plan')->where($condition)->count() > 0 ? true : false;
                if(!$isExist){
                    $fid = M('tbn_data_check_plan')->add($dataRow);
                    $fhtime_arr[] = $val;
                    if($fid){
                        array_push($succRow,['fid'=>$fid]);
                    }
                }else{
                    array_push($errRow,$dataRow);
                }
            }


            $msg = '总'.count($arrFissuedate).'条记录。';
            if(count($succRow) > 0){
                $msg .= ' 成功:'.count($succRow).' 条';
                $dataSet = $succRow;
                if(count($errRow) > 0){
                    $msg .= ' 重复:'.count($errRow).'条';
                    $dataSet = $succRow + $errRow;
                }

                /*钉钉提醒部分-开始*/
                $smsstr = '';
                $do_dcp = M('tbn_data_check_plan') 
                    ->alias('a')
                    ->field('b.fmedianame,c.fname regionname,d.fname trename,a.fplantime')
                    ->master(true)
                    ->join('tmedia b on a.fmediaid = b.fid')
                    ->join('tregion c on a.fregionid = c.fid')
                    ->join('tregulator d on concat("20",a.fcheckorg) = d.fid')
                    ->where(['a.fid'=>$fid])
                    ->find();

                $smsstr .= "**复核媒体：**".$do_dcp['fmedianame']."（".$do_dcp['regionname']."）\n\n";
                $smsstr .= "**复核日期：**".implode($fhtime_arr, '、')."\n\n";
                $smsstr .= "**复核机构：**".$do_dcp['trename']."\n\n";
                $smsstr .= "**截止日期：**".$fplandate."\n\n";
                $smsstr .= "**发布时间：**".$do_dcp['fplantime']."\n\n";
                $smsstr .= "针对以上计划，请相关人员在两小时内完成数据完整性、准确性检查\n\n";

                $token = '4916d1ab6a29a2ac4a755e8e7113d477c1e687736d08d0977f324f7c5a187e96';
                $smsphone = [17611266159];//提醒人员，马国琪
                // push_ddtask("最新推送任务","#### 国家局第三方数据复核任务提醒\n\n > 国家工商总局：\n\n".$smsstr,$smsphone,$token,'markdown');
                /*钉钉提醒部分-结束*/
                foreach($arrFissuedate as $key=>$val){
                    A('Api/ShIssue')->push_error_backtask($fmediaid,'国家局对该媒体'.$val.'的监测情况进行抽查检查，请及时安排人员进行检查，确保数据完整、准确，并在两小时内完成该项检查。',$val,100000);
                }

                $this->ajaxReturn(['code'=>0, 'msg'=>$msg, 'data'=>$dataSet]);
            }else{
                $msg .= ' 失败:'.count($errRow).' 条';
                $this->ajaxReturn(['code'=>1, 'msg'=>$msg, 'data'=>$errRow]);
            }
        }
        $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
    }

    /**
     * 数据检查计划-详情
     */
    public function Detail(){
        $fid = $this->P['fid'];
        $plantype = [
            '01' => 'tv',
            '02' => 'bc',
            '03' => 'paper'
        ];
        if($fid){
            $info = M('tbn_data_check_plan')->where(['fid'=>$fid])->find();
            $fmediaclassid = M('tmedia')->where(['fid'=>$info['fmediaid']])->getField('fmediaclassid');
            $mtype = substr($fmediaclassid,0,2);
            $data = [
                'plantype'    => $plantype[$mtype],// TODO:复核计划类型，待增加到计划表字段 'tv' 、'bc'、'paper'
                'fid'         => $info['fid'],
                'fmonth'      => $info['fmonth'],
                'fregionid'   => $info['fregionid'],
                'fmediaid'    => $info['fmediaid'],
                'fissuedate'  => $info['fissuedate'],
                'fcheckorg'   => $info['fcheckorg'],
                'fchecker'    => $info['fchecker'],
                'fplandate'   => $info['fplandate'],
                'ffinishdate' => $info['ffinishdate'],
                'fplaner'     => $info['fplaner'],
                'fplantime'   => $info['fplantime'],
                'fstate'      => $info['fstate'],
                'freason'     => $info['freason'],
                'fsummary'    => $info['fsummary']
            ];
            $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$data]);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }

    }

    /**
     * 数据检查计划-删除
     */
    public function Delete(){
        $fid = $this->P['fid'];
        $c = 0;
        if($fid){
            $fids = explode(',', $fid);
            foreach($fids as $k=>$v){
                $del = M('tbn_data_check_plan')->where(['fid'=>$v])->delete();
                if($del){
                    $c++;
                }
            }
            if($c){
                $this->ajaxReturn(['code'=>0,'msg'=>'成功删除'.$c.'条记录']);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'未删除任何数据']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 数据检查计划-查看结果
     */
    public function ViewResult(){
        header("Content-type: text/html; charset=utf-8"); 
        session_write_close();
        $fid = $this->P['fid'];
        $nottype = $this->P['nottype'];//排除结果类型，多个以逗号分隔
        $pageIndex = $this->P['pageIndex'] ? $this->P['pageIndex'] : 1;
        $pageSize = $this->P['pageSize'] ? $this->P['pageSize'] : 1000;
        $outtype = $this->P['outtype'];
        $limitIndex = ($pageIndex-1)*$pageSize;
        if($fid){
            if(empty($outtype)){
                $where['fdata_check_plan_id'] = $fid;
                if($nottype != '') $where['fresult_type'] = ['NOT IN',$nottype];//排除结果类型 0-遗漏 1-错误 2-误判
                $count = M('tbn_data_check_result')->where($where)->count();
                $dataSet = M('tbn_data_check_result')
                    ->alias('a')
                    ->field('a.*,b.ffullname fadclassname,(unix_timestamp(a.fend)-unix_timestamp(a.fstart)) flength')
                    ->join('tadclass b on a.fadclasscode = b.fcode','left')
                    ->where($where)
                    // ->limit($limitIndex,$pageSize)
                    ->order('fstart ASC,fpage ASC')
                    ->select();
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$count, 'data'=>$dataSet]);
            }else{
                $where['fdata_check_plan_id'] = $fid;
                $dataSet = M('tbn_data_check_result')
                    ->alias('a')
                    ->field('a.*,b.ffullname fadclassname,(unix_timestamp(a.fend)-unix_timestamp(a.fstart)) flength,(case when a.fillegaltypecode > 0 then "违法" else "不违法" end) fillegaltype,(case when instr(d.fmedianame,"（") > 0 then left(d.fmedianame,instr(d.fmedianame,"（") -1) else d.fmedianame end) as fmedianame')
                    ->join('tadclass b on a.fadclasscode = b.fcode','left')
                    ->join('tbn_data_check_plan c on a.fdata_check_plan_id = c.fid')
                    ->join('tmedia d on c.fmediaid = d.fid')
                    ->where($where)
                    ->order('fstart ASC,fpage ASC')
                    ->select();

                if(empty($dataSet)){
                  $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
                }

                $adddata = [];
                foreach ($dataSet as $key => $value) {
                    $adddata[$value['fresult_type']][] = $value;
                }

                $title = ['一','二','三','四','五'];
                $fresult_types = ['0'=>'漏判广告明细','1'=>'错判广告明细','2'=>'误判广告明细'];
                $num = 0;
                $html = '<body style="font-size:18px; line-height:24px;">';
                $html .= '<div align="center" style="font-size:22px; padding:10px;">广告监测数据复核情况明细表</div>';
                foreach ($adddata as $key => $value) {
                    $html .= '<div align="left" style="font-size:18px; padding:10px;">&nbsp;</div>';
                    $html .= '<div align="left" style="font-size:18px; padding:10px;">'.$title[$num].'、'.$fresult_types[$key].'</div>';
                    $html .= '<table border="1" cellpadding="0" width="100%" cellspacing="0" style="border-collapse: collapse;">';
                    if($key == 0 || $key == 2){
                        $html .= '<tr>
                            <td align="center">序号</td><td align="center">媒体名称</td><td align="center">广告名称</td><td align="center">广告类别</td><td align="center">发布时段</td><td align="center">广告时长（秒）</td><td align="center">是否涉嫌违法</td><td align="center">监测编码</td><td align="center">具体违法内容</td>
                        </tr>';
                    }else{
                        $html .= '<tr>
                            <td align="center">序号</td><td align="center">媒体名称</td><td align="center">广告名称</td><td align="center">广告类别</td><td align="center">发布时段</td><td align="center">广告时长（秒）</td><td align="center">复核意见</td>
                        </tr>';
                    }
                    foreach ($value as $key2 => $value2) {
                        if($key == 0 || $key == 2){
                            $html .= '<tr>
                                <td align="center">'.($key2+1).'</td>
                                <td align="center">'.$value2['fmedianame'].'</td>
                                <td align="center">'.$value2['fadname'].'</td>
                                <td align="center">'.$value2['fadclassname'].'</td>
                                <td align="center">'.$value2['fstart'].'~'.date('H:i:s',strtotime($value2['fend'])).'</td>
                                <td align="center">'.$value2['flength'].'</td>
                                <td align="center">'.$value2['fillegaltype'].'</td>
                                <td align="center">'.$value2['fexpressioncodes'].'</td>
                                <td align="center">'.$value2['fillegalcontent'].'</td>
                            </tr>';
                        }else{
                            $html .= '<tr>
                                <td align="center">'.($key2+1).'</td>
                                <td align="center">'.$value2['fmedianame'].'</td>
                                <td align="center">'.$value2['fadname'].'</td>
                                <td align="center">'.$value2['fadclassname'].'</td>
                                <td align="center">'.$value2['fstart'].'~'.date('H:i:s',strtotime($value2['fend'])).'</td>
                                <td align="center">'.$value2['flength'].'</td>
                                <td align="center">'.$value2['fresult'].'</td>
                            </tr>';
                        }
                    }
                    $html .= '</table>';
                    $num++;
                }
                $html .= '</body>';

                $date = date('Ymdhis');
                $savefilename = $date.'.doc';
                $savefile = './Public/Word/'.$date.'.doc';

                //将文档保存
                word_start();
                echo $html;
                word_save($savefile);
                ob_flush();//每次执行前刷新缓存
                flush();

                $ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.doc',$savefile,array('Content-Disposition' => 'attachment;filename='.$date.'.doc'));//上传云
                unlink($savefile);//删除文件
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
            }
            
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 数据检查计划-退回重新检查
     */
    public function BackReCheck(){
        $fid = $this->P['fid'];
        $freason = $this->P['freason'];
        if($fid){
            $where['fid'] = $fid;
            $dataRow = [
                'fstate' => 2,
                'freason' => $freason
            ];
            $ret = M('tbn_data_check_plan')->where($where)->save($dataRow);
            $data['fid'] = $ret;
            $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$data]);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 数据检查-开始检查
     */
    public function Check(){
        $fid        = $this->P['fid'];
        $fstarttime = $this->P['fstarttime'];
        $fendtime   = $this->P['fendtime'];
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex']: 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 1000);
        $limitIndex = ($pageIndex-1)*$pageSize;
        if($fid && $fstarttime && $fendtime){
            $planInfo = M('tbn_data_check_plan')
                ->where(['fid'=>$fid])
                ->find();
            if($planInfo){
                $fmediaid   = $planInfo['fmediaid'];
                $m3u8_url   = A('Common/Media','Model')->get_m3u8($fmediaid,strtotime($fstarttime),strtotime($fendtime));
                $retList    = A('Common/AdIssue','Model')->getAdIssueByMediaGov($fmediaid,$fstarttime,$fendtime);
                $count      = (int)$retList['count'];//包含非广告时段总条数
                $countAds   = (int)$retList['count'];//仅广告总条数
                $List       = $retList['data'];
                $adList     = [];
                $resultList = [];
                if(!empty($List)){
                    foreach($List as $key=>$row){
                        if($key == 0){//第一条
                            $interval = $row['fstarttime'] - strtotime($fstarttime); //与起始时间间隔大于3秒,则创建一个空时间段
                            if($interval > 3){
                                $adNull = [
                                    'fadname'          => '-',
                                    'fadclass'         => '',
                                    'fadclasscode'     => 0,
                                    'fstarttime'       => date('Y-m-d H:i:s',strtotime($fstarttime)),
                                    'fendtime'         => date('Y-m-d H:i:s',$row['fstarttime']),
                                    'flength'          => $interval,
                                    'fillegaltypecode' => '',
                                    'fexpressioncodes' => '',
                                    'fexpressions'     =>  ''
                                ];
                                array_push($adList,$adNull);
                                $count++;//创建的空闲时段计入记录总条数
                            }
                        }elseif(0 < $key && $key < count($List)){
                            $interval = $row['fstarttime'] - $List[$key-1]['fendtime']; //两个广告时间间隔大于3秒,则创建一个空时间段
                            if($interval > 3){
                                $adNull = [
                                    'fadname'          => '-',
                                    'fadclass'         => '',
                                    'fadclasscode'     => 0,
                                    'fstarttime'       => date('Y-m-d H:i:s',$List[$key-1]['fendtime']),
                                    'fendtime'         => date('Y-m-d H:i:s',$row['fstarttime']),
                                    'flength'          => $interval,
                                    'fillegaltypecode' => '',
                                    'fexpressioncodes' => '',
                                    'fexpressions'     =>  ''
                                ];
                                array_push($adList,$adNull);
                                $count++;//创建的空闲时段计入记录总条数
                            }
                        }
                        //广告记录
                        $ad = [
                            'fadname'          => $row['fadname'],
                            'fadclass'         => $row['fadclass'],
                            'fadclasscode'     => $row['fadclasscode'],
                            'fstarttime'       => date('Y-m-d H:i:s',$row['fstarttime']),
                            'fendtime'         => date('Y-m-d H:i:s',$row['fendtime']),
                            'flength'          => $row['flength'],
                            'fillegaltypecode' => $row['fillegaltypecode'] > 0 ? '违法' : '不违法',
                            'fexpressioncodes' => $row['fexpressioncodes'],
                            'fexpressions'     => $row['fexpressions']
                        ];
                        array_push($adList,$ad);
                        //最后一条后的空时间段
                        if($key == (count($List)-1)){
                            $interval = strtotime($fendtime) - $row['fendtime']; //与结束时间间隔大于3秒,则创建一个空时间段
                            if($interval > 3){
                                $adNull = [
                                    'fadname'          => '-',
                                    'fadclass'         => '',
                                    'fadclasscode'     => 0,
                                    'fstarttime'       => date('Y-m-d H:i:s',$row['fendtime']),
                                    'fendtime'         => date('Y-m-d H:i:s',strtotime($fendtime)),
                                    'flength'          => $interval,
                                    'fillegaltypecode' => '',
                                    'fexpressioncodes' => '',
                                    'fexpressions'     =>  ''
                                ];
                                array_push($adList,$adNull);
                                $count++;//创建的空闲时段计入记录总条数
                            }
                        }
                    }
                }else{
                    $adNull = [
                        'fadname'          => '-',
                        'fadclass'         => '',
                        'fadclasscode'     => 0,
                        'fstarttime'       => $fstarttime,
                        'fendtime'         => $fendtime,
                        'flength'          => strtotime($fendtime) - strtotime($fstarttime),
                        'fillegaltypecode' => '',
                        'fexpressioncodes' => '',
                        'fexpressions'     => ''
                    ];
                    array_push($adList,$adNull);
                    $count++;//创建的空闲时段计入记录总条数
                }
                $resultList = array_slice($adList,$limitIndex,$pageSize);
                $dataSet = [
                    'fid'=> $fid,
                    'live_m3u8_url' => $m3u8_url,
                    'fad' => $resultList
                ];
                $countNotAds = $count - $countAds;
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$count, 'countAds'=>$countAds,'countNotAds' => $countNotAds, 'data'=>$dataSet]);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'未找到该检查计划']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 广告数据正确性检查-获取广告列表
     */
    public function CheckAds(){
        $fid        = $this->P['fid'];
        $fstarttime = $this->P['fstarttime'];
        $fendtime   = $this->P['fendtime'];
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 1000);
        $limitIndex = ($pageIndex-1)*$pageSize;
        if($fid && $fstarttime && $fendtime){
            $planInfo = M('tbn_data_check_plan')
                ->where(['fid'=>$fid])
                ->find();
            if($planInfo){
                $fmediaid   = $planInfo['fmediaid'];
                // $retList    = A('Common/AdIssue','Model')->getAdIssueByMedia($fmediaid,$fstarttime,$fendtime);
                $retList    = A('Common/AdIssue','Model')->getAdIssue($fmediaid,$fstarttime,$fendtime);
                $count      = $retList['count'];
                $List       = $retList['data'];
                $adList     = [];
                $resultList = [];
                if(!empty($List)){
                    foreach($List as $key=>$row){
                        //广告记录
                        $ad = [
                            'fadname'          => $row['fadname'],
                            'fadclass'         => $row['fadclass'],
                            'fadclasscode'     => $row['fadclasscode'],
                            'fstarttime'       => date('Y-m-d H:i:s',$row['fstarttime']),
                            'fendtime'         => date('Y-m-d H:i:s',$row['fendtime']),
                            'flength'          => $row['flength'],
                            'fillegaltypecode' => $row['fillegaltypecode'] > 0 ? '违法' : '不违法',
                            'fexpressioncodes' => $row['fexpressioncodes'],
                            'fexpressions'     => $row['fexpressions']
                        ];
                        array_push($adList,$ad);
                    }
                }
                $resultList = array_slice($adList,$limitIndex,$pageSize);
                $dataSet = [
                    'fid'=> $fid,
                    'fad' => $resultList
                ];
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$count, 'data'=>$dataSet]);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'未找到该检查计划']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 广告数据正确性检查-获取报纸广告列表
     */
    public function CheckAdsPaper(){
        $fid        = $this->P['fid'];
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 1000);
        $limitIndex = ($pageIndex-1)*$pageSize;
        if($fid){
            $planInfo = M('tbn_data_check_plan')
                ->where(['fid'=>$fid])
                ->find();
            if($planInfo){
                $fmediaid   = $planInfo['fmediaid'];
                $fissuedate = $planInfo['fissuedate'];
                $retList    = A('Common/AdIssue','Model')->getAdIssuePaper($fmediaid,$fissuedate);
                $count      = $retList['count'];
                $List       = $retList['data'];
                $adList     = [];
                $resultList = [];
                if(!empty($List)){
                    foreach($List as $key=>$row){
                        //广告记录
                        $ad = [
                            'fissueid'         => $row['fpaperissueid'],
                            'fadname'          => $row['fadname'],
                            'fadclass'         => $row['fadclass'],
                            'fpage'            => $row['fpage'],
                            'fpagetype'        => $row['fpagetype'],
                            'fjpgfilename'     => $row['fjpgfilename'],
                            'fillegaltypecode' => $row['fillegaltypecode'] > 0 ? '违法': '不违法',
                            'fexpressioncodes' => $row['fexpressioncodes'],
                            'fexpressions'     => $row['fexpressions']
                        ];
                        array_push($adList,$ad);
                    }
                }
                $resultList = array_slice($adList,$limitIndex,$pageSize);
                $dataSet = [
                    'fid'=> $fid,
                    'fad' => $resultList
                ];
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$count, 'data'=>$dataSet]);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'未找到该检查计划']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 广告数据正确性检查-获取广告素材
     */
    public function GetAdm3u8(){
        $fid        = $this->P['fid'];
        $fstarttime = $this->P['fstarttime'];
        $fendtime   = $this->P['fendtime'];
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 1000);
        $limitIndex = ($pageIndex-1)*$pageSize;
        if($fid && $fstarttime && $fendtime){
            $planInfo = M('tbn_data_check_plan')
                ->where(['fid'=>$fid])
                ->find();
            if($planInfo){
                $fmediaid   = $planInfo['fmediaid'];
                $m3u8_url   = A('Common/Media','Model')->get_m3u8($fmediaid,strtotime($fstarttime),strtotime($fendtime));
                $dataSet = [
                    'fid'=> $fid,
                    'live_m3u8_url' => $m3u8_url
                ];
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'data'=>$dataSet]);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'未找到该检查计划']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 广告数据正确性检查-获取报纸版面列表
     */
    public function GetPages(){
        $fid        = $this->P['fid'];
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 1000);
        $limitIndex = ($pageIndex-1)*$pageSize;
        if($fid){
            $planInfo = M('tbn_data_check_plan')
                ->where(['fid'=>$fid])
                ->find();
            if($planInfo){
                $fmediaid   = $planInfo['fmediaid'];
                $fissuedate = $planInfo['fissuedate'];
                $fpages   = A('Common/AdIssue','Model')->getPages($fmediaid,$fissuedate);
                $dataSet = [
                    'fid'=> $fid,
                    'fpages' => $fpages
                ];
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'data'=>$dataSet]);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'未找到该检查计划']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 数据检查-新增结果
     */
    public function AddResult(){
        $fdata_check_plan_id = $this->P['fid'];
        $fissueid = $this->P['fissueid'] ? $this->P['fissueid'] : 0;
        $fstart = $this->P['fstart'];
        $fend = $this->P['fend'];
        $fpage = $this->P['fpage'] ? $this->P['fpage'] : '';
        $fadname = $this->P['adTitle'] ? $this->P['adTitle'] : '';
        $fresult_type = $this->P['fresult_type'];
        $fresult = $this->P['fresult'];
        $fadclasscode = $this->P['fadclasscode'];//广告类别
        $fillegaltypecode = $this->P['fillegaltypecode'];//违法类型，0或30
        $fillegalcontent = $this->P['fillegalcontent'];//违法内容
        $fexpressioncodes = $this->P['fexpressioncodes'];//违法表现代码
        $fexpressions = $this->P['fexpressions'];//违法表现
        if($fadname == '查看段'){
            $this->ajaxReturn(['code'=>1,'msg'=>'请正确输入广告名称']);
        }
        if($fdata_check_plan_id){
            $condition = [
                'fdata_check_plan_id' => $fdata_check_plan_id,
                'fstart' => $fstart,
                'fend' => $fend,
                'fpage' => $fpage,
                'fadname' => $fadname,
                'fresult_type' => $fresult_type,
                'fresult' => $fresult
            ];
            $isExist = M('tbn_data_check_result')->where($condition)->count() > 0 ? true : false;

            $dataRow['fdata_check_plan_id'] = $fdata_check_plan_id;
            $dataRow['fissueid'] = $fissueid;
            $dataRow['fstart'] = $fstart;
            $dataRow['fend'] = $fend;
            $dataRow['fpage'] = $fpage;
            $dataRow['fadname'] = $fadname;
            $dataRow['fresult_type'] = $fresult_type;
            $dataRow['fadclasscode'] = $fadclasscode;
            $dataRow['fcreator'] = session('regulatorpersonInfo.fid');
            $dataRow['fcreatetime'] = date('Y-m-d H:i:s');
            $dataRow['fresult'] = $fresult;
            if($fresult_type == 2 || $fresult_type == 0){
                $dataRow['fillegaltypecode'] = $fillegaltypecode;
                if(!empty($fillegaltypecode)){
                    $dataRow['fillegalcontent'] = $fillegalcontent;
                    $dataRow['fexpressioncodes'] = $fexpressioncodes;
                    $dataRow['fexpressions'] = $fexpressions;
                }
                
            }

            if(!$isExist){
                $fid = M('tbn_data_check_result')->add($dataRow);
                if($fid){
                    $this->ajaxReturn(['code'=>0,'msg'=>'成功']);
                }
            }
            $this->ajaxReturn(['code'=>1,'msg'=>'请勿重复添加']);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 数据检查-删除结果
     */
    public function DeleteResult(){
        $fid = $this->P['fid'];
        $c = 0;
        if($fid){
            $fids = explode(',', $fid);
            foreach($fids as $k=>$v){
                $del = M('tbn_data_check_result')->where(['fid'=>$v])->delete();
                if($del){
                    $c++;
                }
            }
            if($c){
                $this->ajaxReturn(['code'=>0,'msg'=>'成功删除'.$c.'条记录']);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'未删除任何数据']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 获取媒介月份可用日期
     */
    public function GetDays(){
        $fmonth = date('Y-m-01',strtotime($this->P['fmonth']));
        $fmediaid = $this->P['fmediaid'];
        $mediaInfo = M('tmedia')->where(['fid'=>$fmediaid])->find();
        $c = substr($mediaInfo['fmediaclassid'],0,2);
        $aType = [
            '01'=>'tv',
            '02'=>'bc',
            '03'=>'paper',
            '13'=>'net'
        ];
        $days = M('zongju_data_confirm')->where(['fmonth'=>$fmonth])->getField('condition');
        $aDays = json_decode($days,true);
        $data = $aDays[$aType[$c]];
        sort($data);
        foreach($data as $k=>$v){
            $data[$k] = $this->P['fmonth'].'-'.$v;
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$data]);
    }

    /**
     * 完成检查-提交
     */
    public function Submit(){
        $fid = $this->P['fid'];
        $regulatorpersonInfo = session('regulatorpersonInfo');
        if($fid){
            $fchecker = $regulatorpersonInfo['fid'];
            $ffinishdate = date('Y-m-d H:i:s');
            $fstate = 1;
            $fsummaryArr = $this->resultSummary($fid);
            $fsummary = '';
            if(!empty($fsummaryArr)){
                $total = 0;
                foreach($fsummaryArr as $key => $val){
                    $count = (int)$val['count'];
                    $total += $count;
                    if($count > 0){
                        switch($val['fresult_type']){
                            case "0":
                                $fsummary .= '遗漏：'.$count.'条 ';
                                break;
                            case "1":
                                $fsummary .= '错误：'.$count.'条 ';
                                break;
                            case "2":
                                $fsummary .= '误判：'.$count.'条 ';
                                break;
                        }
                    }
                }
                if($total == 0) $fsummary = '';//正常
            }else{
                $fsummary = '';//正常
            }
            $data = [
                'fchecker' => $fchecker,
                'ffinishdate' => $ffinishdate,
                'fstate' => $fstate,
                'fsummary' => $fsummary
            ];
            $ret = M('tbn_data_check_plan')->where(['fid'=>$fid])->save($data);
            if($ret){
                $this->ajaxReturn(['code'=>0,'msg'=>'提交成功']);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'提交时出错']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 数据检查-汇总检查结果
     */
    protected function resultSummary($fid){
        if($fid){
            $where['fdata_check_plan_id'] = $fid;
            $ret = M('tbn_data_check_result')
                ->field('fresult_type,count(fresult_type) AS count')
                ->where($where)
                ->group('fresult_type')
                ->select();
            return $ret;
        }else{
            return false;
        }
    }

    /**
     * 上海数据检查-添加遗漏的广告
     */
    public function AddAd(){
        $system_num = getconfig('system_num');
        $fmediaid = $this->P['fmediaid'];
        $fadname = $this->P['fadname'];
        $fadclasscode = $this->P['fadclasscode'];
        $fadowner = $this->P['fadowner'];
        $fadownername = $this->P['fadownername'];
        $fbrand = $this->P['fbrand'];
        $fspokesman = $this->P['fspokesman'];
        $fstarttime = strtotime($this->P['fstart']);
        $fendtime = strtotime($this->P['fend']);
        $fissuedate = date('Y-m-d',$fstarttime);
        $flength = $fendtime - $fstarttime;
        if($flength<1 || $flength>86400){
            $this->ajaxReturn(['code'=>1, 'msg'=>'播出时段选择有误']);
        }
        if($fadname == '查看段'){
            $this->ajaxReturn(['code'=>1,'msg'=>'请正确输入广告名称']);
        }

        $sam_source_path = A('Common/Media','Model')->get_m3u8($fmediaid, $fstarttime, $fendtime);
        $mediaInfo = M('tmedia')->where(['fid'=>$fmediaid])->find();
        $fmedianame = $mediaInfo['fmedianame'];
        $fmediaclassid = $mediaInfo['fmediaclassid'];
        $fregionid = $mediaInfo['media_region_id'];
        // 校验是否可检查
        $isAvailable = $this->isMediaDateFixed($fmediaid,strtotime($fissuedate));
        // $isAvailable = $this->isMediaDateAvailable($fmediaid,strtotime($fissuedate));
        if(!$isAvailable){
            $this->ajaxReturn(['code'=>1, 'msg'=>'数据处理中，无法提交']);
        }

        $fuuid = strtolower(createNoncestr(8).'-'.createNoncestr(4).'-'.createNoncestr(4).'-'.createNoncestr(4).'-'.createNoncestr(12));
        $dataAd = [
            'fuserid' => $system_num,
            'fmediaid' => $fmediaid,
            'fmedianame' => $fmedianame,
            'fmediaclassid' => $fmediaclassid,
            'fregionid' => $fregionid,
            'fadname' => $fadname,
            'fadclasscode' => $fadclasscode,
            'fadowner' => $fadowner,
            'fadownername' => $fadownername,
            'fbrand' => $fbrand,
            'fspokesman' => $fspokesman,
            'fstarttime' => $fstarttime,
            'fendtime' => $fendtime,
            'fissuedate' => $fissuedate,
            'flength' => $flength,
            'fuuid' => $fuuid,
            'sam_source_path' => $sam_source_path,
            'fcreator' => 'add_'.session('regulatorpersonInfo.fid'),
            'fcreatetime' => date('Y-m-d H:i:s'),
            'fmodifier' => session('regulatorpersonInfo.fid'),
            'fmodifytime' => date('Y-m-d H:i:s')
        ];
        $existWhere = [
            'fmediaid' => $fmediaid,
            'fstarttime' => $fstarttime,
            'fendtime' => $fendtime,
        ];
        $isExist = M('sh_issue')
            ->where($existWhere)
            ->count() > 0 ? true : false;
        if(!$isExist){
            // 判断添加的广告与已存在的记录时间是否重叠
            $testOverlap = M('sh_issue')
                ->where([
                    'fmediaid' => $fmediaid,
                    'fstarttime' => ['LT',$fendtime],
                    'fendtime' => ['GT',$fstarttime]
                ])
                ->find();
            $isOverlap = M('sh_issue')
                ->where([
                    'fmediaid' => $fmediaid,
                    'fstarttime' => ['LT',$fendtime],
                    'fendtime' => ['GT',$fstarttime]
                ])
                ->count();
            if($isOverlap == 0){
                $ret = M('sh_issue')->add($dataAd);
                if($ret){
                    // 操作日志
                    $fapimedialist_id = M('tapimedialist')
                        ->alias('a')
                        ->cache(true,86400)
                        ->join('tapimedia_map b ON b.fid=a.fmediaid')
                        ->where(['b.fmediaid'=>$fmediaid,'a.fissuedate'=>strtotime($fissuedate)])
                        ->getField('a.fid');
                    $logArr = [
                        'fapimedialist_id' => (int)$fapimedialist_id,
                        'foperator'        => session('regulatorpersonInfo.fid'),
                        'flogtype'         => 2,
                        'flog'             => '新增广告记录,ID:'.$ret.' 广告名称:'.$dataAd['fadname'],
                        'fcontent'         => json_encode($dataAd),
                    ];
                    A('Api/ShIssue','Model')->log($logArr);
                    // 返回新增的记录ID
                    $data['fid'] = $ret;
                    $this->ajaxReturn(['code'=>0,'msg'=>'添加成功','data'=>$data]);
                }else{
                    $this->ajaxReturn(['code'=>1,'msg'=>'添加出错']);
                }
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'开始时间或结束时间与已有广告重叠，请修改时间']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'已存在']);
        }
    }

    /**
     * 上海数据检查-在现有广告时段内插入新广告
     */
    public function InsertAd(){
        $system_num = getconfig('system_num');
        $fid             = $this->P['fid'];
        $fmediaid        = $this->P['fmediaid'];
        $fadname         = $this->P['fadname'];
        $fadclasscode    = $this->P['fadclasscode'];
        $fadowner        = $this->P['fadowner'];
        $fadownername    = $this->P['fadownername'];
        $fbrand          = $this->P['fbrand'];
        $fspokesman      = $this->P['fspokesman'];
        $fstarttime      = strtotime($this->P['fstart']);
        $fendtime        = strtotime($this->P['fend']);
        $fissuedate      = date('Y-m-d',$fstarttime);
        $flength         = $fendtime - $fstarttime;
        if($flength<1 || $flength>86400){
            $this->ajaxReturn(['code'=>1, 'msg'=>'播出时段选择有误']);
        }
        if($fadname == '查看段'){
            $this->ajaxReturn(['code'=>1,'msg'=>'请正确输入广告名称']);
        }

        $sam_source_path = A('Common/Media','Model')->get_m3u8($fmediaid, $fstarttime, $fendtime);
        $mediaInfo       = M('tmedia')->where(['fid' => $fmediaid])->find();
        $fmedianame      = $mediaInfo['fmedianame'];
        $fmediaclassid   = $mediaInfo['fmediaclassid'];
        $fregionid       = $mediaInfo['media_region_id'];
        // 校验是否允许提交
        $avaTimeStamp    = A('Common/Media','Model')->available_time($fmediaid);
        $isAvailable     = (strtotime($fissuedate) <= $avaTimeStamp) ? true : false;
        if(!$isAvailable){
            $this->ajaxReturn(['code'=>1, 'msg'=>'数据处理中，无法提交']);
        }

        // 查找包含新插入广告的时段，暂只支持在一条广告时段内插入，不支持跨广告时段插入新广告
        $parentWhere = [
            'fmediaid'   => $fmediaid,
            'fstarttime' => ['ELT',$fstarttime],
            'fendtime' => ['EGT',$fendtime],
        ];
        $parentAd = M('sh_issue')
            ->where($parentWhere)
            ->order('fstarttime DESC')
            ->find();
        if(empty($parentAd)){
            $this->ajaxReturn(['code'=>1, 'msg'=>'暂只支持在一条广告时段内插入']);
        }
        if($fendtime > $parentAd['fendtime']){
            $this->ajaxReturn(['code'=>1, 'msg'=>'请确认起止时间，不支持跨时段插入新广告']);
        }

        $dataAd = [
            'fuserid'        => $system_num,
            'fmediaid'        => $fmediaid,
            'fmedianame'      => $fmedianame,
            'fmediaclassid'   => $fmediaclassid,
            'fregionid'       => $fregionid,
            'fadname'         => $fadname,
            'fadclasscode'    => $fadclasscode,
            'fadowner'        => $fadowner,
            'fadownername'    => $fadownername,
            'fbrand'          => $fbrand,
            'fspokesman'      => $fspokesman,
            'fstarttime'      => $fstarttime,
            'fendtime'        => $fendtime,
            'fissuedate'      => $fissuedate,
            'flength'         => $flength,
            'sam_source_path' => $sam_source_path,
            'fcreator'        => 'insert_'.session('regulatorpersonInfo.fid'),
            'fcreatetime'     => date('Y-m-d H:i:s'),
            'fmodifier'       => session('regulatorpersonInfo.fid'),
            'fmodifytime'     => date('Y-m-d H:i:s')
        ];
        $existWhere = [
            'fmediaid'     => $fmediaid,
            'fadname'      => $fadname,
            'fadclasscode' => $fadclasscode,
            'fadowner'     => $fadowner,
            'fadownername' => $fadownername,
            'fbrand'       => $fbrand,
            'fspokesman'   => $fspokesman,
            'fstarttime'   => $fstarttime,
            'fendtime'     => $fendtime,
            'flength'      => $flength,
        ];
        $isExist = M('sh_issue')
            ->where($existWhere)
            ->count() > 0 ? true : false;
        if(!$isExist){
            // 判断添加的广告与已存在的记录时间是否重叠
            $testOverlap = M('sh_issue')
                ->where([
                    'fmediaid' => $fmediaid,
                    'fstarttime' => ['LT',$fendtime],
                    'fendtime' => ['GT',$fstarttime]
                ])
                ->find();
            $isOverlap = M('sh_issue')
                ->where([
                    'fmediaid' => $fmediaid,
                    'fstarttime' => ['LT',$fendtime],
                    'fendtime' => ['GT',$fstarttime]
                ])
                ->count();
            if($isOverlap == 0){
                $this->ajaxReturn(['code'=>1,'msg'=>'暂只支持在同一广告内插入']);
            }else{
                // 一、开头插入
                if ($parentAd['fstarttime'] == $fstarttime && $parentAd['fendtime'] > $fendtime) {
                    // 1、更新现有广告开始时间
                    M('sh_issue')->where(['fid'=>$parentAd['fid']])->save(['fstarttime'=>$fendtime,'flength'=>$parentAd['fendtime']-$fendtime]);
                    // 2、插入新广告
                    M('sh_issue')->add($dataAd);
                }
                // 二、中间插入
                if($parentAd['fstarttime'] < $fstarttime && $parentAd['fendtime'] > $fendtime){
                    // 1、更新现有广告结束时间
                    M('sh_issue')->where(['fid'=>$parentAd['fid']])->save(['fendtime'=>$fstarttime,'flength'=>$fstarttime-$parentAd['fstarttime']]);
                    // 2、插入新广告
                    M('sh_issue')->add($dataAd);
                    // 3、插入尾部广告
                    $dataAdNew = [
                        'fuserid'        => $system_num,
                        'fmediaid'        => $parentAd['fmediaid'],
                        'fmedianame'      => $parentAd['fmedianame'],
                        'fmediaclassid'   => $parentAd['fmediaclassid'],
                        'fregionid'       => $parentAd['fregionid'],
                        'fadname'         => $parentAd['fadname'],
                        'fadclasscode'    => $parentAd['fadclasscode'],
                        'fadowner'        => $parentAd['fadowner'],
                        'fadownername'    => $parentAd['fadownername'],
                        'fbrand'          => $parentAd['fbrand'],
                        'fspokesman'      => $parentAd['fspokesman'],
                        'fstarttime'      => $fendtime,
                        'fendtime'        => $parentAd['fendtime'],
                        'fissuedate'      => $parentAd['fissuedate'],
                        'flength'         => $parentAd['fendtime'] - $fendtime,
                        'sam_source_path' => $parentAd['sam_source_path'],
                        'fcreator'        => 'insert_'.session('regulatorpersonInfo.fid'),
                        'fcreatetime'     => date('Y-m-d H:i:s'),
                        'fmodifier'       => session('regulatorpersonInfo.fid'),
                        'fmodifytime'     => date('Y-m-d H:i:s')
                    ];
                    M('sh_issue')->add($dataAdNew);
                }
                // 三、尾部插入
                if ($parentAd['fstarttime'] < $fstarttime && $parentAd['fendtime'] == $fendtime) {
                    // 1、更新现有广告结束时间
                    M('sh_issue')->where(['fid'=>$parentAd['fid']])->save(['fendtime'=>$fstarttime,'flength'=>$fstarttime - $parentAd['fstarttime']]);
                    // 2、插入新广告
                    M('sh_issue')->add($dataAd);
                }
                // 操作日志
                $fapimedialist_id = M('tapimedialist')
                    ->alias('a')
                    ->cache(true,86400)
                    ->join('tapimedia_map b ON b.fid=a.fmediaid')
                    ->where(['b.fmediaid'=>$fmediaid,'a.fissuedate'=>strtotime($fissuedate)])
                    ->getField('a.fid');
                $logArr = [
                    'fapimedialist_id' => (int)$fapimedialist_id,
                    'foperator'        => session('regulatorpersonInfo.fid'),
                    'flogtype'         => 3,
                    'flog'             => '插入广告记录,ID:'.$ret.' 广告名称:'.$dataAd['fadname'],
                    'fcontent'         => json_encode(['new'=>$dataAd,'ori'=>$parentAd]),
                ];
                A('Api/ShIssue','Model')->log($logArr);

                $this->ajaxReturn(['code'=>0,'msg'=>'成功']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'已存在']);
        }
    }

    /**
     * 上海数据检查-编辑遗漏的广告
     */
    public function EditAd(){
        $system_num = getconfig('system_num');
        $fid = $this->P['fid'];
        $fmediaid = $this->P['fmediaid'];
        $fadname = $this->P['fadname'];
        $fadclasscode = $this->P['fadclasscode'];
        $fadowner = $this->P['fadowner'];
        $fadownername = $this->P['fadownername'];
        $fbrand = $this->P['fbrand'];
        $fspokesman = $this->P['fspokesman'];
        $fstarttime = strtotime($this->P['fstart']);
        $fendtime = strtotime($this->P['fend']);
        $fissuedate = date('Y-m-d',$fstarttime);
        $flength = $fendtime - $fstarttime;
        if($flength<1 || $flength>86400){
            $this->ajaxReturn(['code'=>1, 'msg'=>'播出时段选择有误']);
        }
        if($fadname == '查看段'){
            $this->ajaxReturn(['code'=>1,'msg'=>'请正确输入广告名称']);
        }
        
        $sam_source_path = A('Common/Media','Model')->get_m3u8($fmediaid, $fstarttime, $fendtime);
        $mediaInfo = M('tmedia')->where(['fid'=>$fmediaid])->find();
        $fmedianame = $mediaInfo['fmedianame'];
        $fmediaclassid = $mediaInfo['fmediaclassid'];
        $fregionid = $mediaInfo['media_region_id'];
        // 校验是否可检查
        $isAvailable = $this->isMediaDateFixed($fmediaid,strtotime($fissuedate));
        // $isAvailable = $this->isMediaDateAvailable($fmediaid,strtotime($fissuedate));
        if(!$isAvailable){
            $this->ajaxReturn(['code'=>1, 'msg'=>'数据处理中，无法提交']);
        }

        $dataAd = [
            'fuserid' => $system_num,
            'fmediaid' => $fmediaid,
            'fmedianame' => $fmedianame,
            'fmediaclassid' => $fmediaclassid,
            'fregionid' => $fregionid,
            'fadname' => $fadname,
            'fadclasscode' => $fadclasscode,
            'fadowner' => $fadowner,
            'fadownername' => $fadownername,
            'fbrand' => $fbrand,
            'fspokesman' => $fspokesman,
            'fstarttime' => $fstarttime,
            'fendtime' => $fendtime,
            'fissuedate' => $fissuedate,
            'flength' => $flength,
            'sam_source_path' => $sam_source_path,
            'fmodifier' => session('regulatorpersonInfo.fid'),
            'fmodifytime' => date('Y-m-d H:i:s')
        ];
        $existWhere = [
            'fid' => $fid,
        ];
        $isExist = M('sh_issue')
            ->where($existWhere)
            ->count() > 0 ? true : false;
        if($isExist){
            // 原广告
            $oriInfo = M('sh_issue')
                ->where($existWhere)
                ->find();
            $testOverlap = M('sh_issue')
                ->where([
                    'fid' => ['NEQ',$fid],
                    'fmediaid' => $fmediaid,
                    'fstarttime' => ['LT',$fendtime],
                    'fendtime' => ['GT',$fstarttime]
                ])
                ->find();
            // 判断编辑后的广告与已存在的记录时间是否重叠
            $isOverlap = M('sh_issue')
                ->where([
                    'fid' => ['NEQ',$fid],
                    'fmediaid' => $fmediaid,
                    'fstarttime' => ['LT',$fendtime],
                    'fendtime' => ['GT',$fstarttime]
                ])
                ->count();
            if($isOverlap == 0){
                // 更新当前发布记录
                $ret = M('sh_issue')->where($existWhere)->save($dataAd);
                $data['fid'] = $fid;
                // 更新当天相同样本的所有发布记录
                $dataRelateAd = [
                    'fadname' => $fadname,
                    'fadclasscode' => $fadclasscode,
                    'fadowner' => $fadowner,
                    'fadownername' => $fadownername,
                    'fbrand' => $fbrand,
                    'fmodifier' => session('regulatorpersonInfo.fid'),
                    'fmodifytime' => date('Y-m-d H:i:s')
                ];
                $issueInfo = M('sh_issue')->field('fsampleid,fregionid,fissuedate')->where(['fid'=>$fid])->find();
                $saveWhere = [
                    'fsampleid' => $issueInfo['fsampleid'],
                    'fregionid' => $issueInfo['fregionid'],
                    'fissuedate' => $issueInfo['fissuedate'],
                ];
                $retRelate = M('sh_issue')->where($saveWhere)->save($dataRelateAd);
                if($ret){
                    // 操作日志
                    $fapimedialist_id = M('tapimedialist')
                        ->alias('a')
                        ->cache(true,86400)
                        ->join('tapimedia_map b ON b.fid=a.fmediaid')
                        ->where(['b.fmediaid'=>$fmediaid,'a.fissuedate'=>strtotime($fissuedate)])
                        ->getField('a.fid');
                    $log = '修改广告记录,ID:'.$fid;
                    if($oriInfo['fadname'] != $dataAd['fadname'])
                        $log .= ' 广告名称:'.$oriInfo['fadname'].' -> '.$dataAd['fadname'];
                    if($oriInfo['fadclasscode'] != $dataAd['fadclasscode'])
                        $log .= ' 广告类别:'.$oriInfo['fadclasscode'].' -> '.$dataAd['fadclasscode'];
                    if($oriInfo['fstarttime'] != $dataAd['fstarttime'] || $oriInfo['fendtime'] != $dataAd['fendtime'])
                        $log .= ' 起止时间:'.date('Y-m-d H:i:s',$oriInfo['fstarttime']).'~'.date('Y-m-d H:i:s',$oriInfo['fendtime']).' -> '.date('Y-m-d H:i:s',$dataAd['fstarttime']).'~'.date('Y-m-d H:i:s',$dataAd['fendtime']);
                    if($oriInfo['fbrand'] != $dataAd['fbrand'])
                        $log .= ' 广告品牌:'.$oriInfo['fbrand'].' -> '.$dataAd['fbrand'];
                    if($oriInfo['fadownername'] != $dataAd['fadownername'])
                        $log .= ' 广告主:'.$oriInfo['fadownername'].' -> '.$dataAd['fadownername'];
                    $logArr = [
                        'fapimedialist_id' => (int)$fapimedialist_id,
                        'foperator'        => session('regulatorpersonInfo.fid'),
                        'flogtype'         => 4,
                        'flog'             => $log,
                        'fcontent'         => json_encode(['new'=>$dataAd,'ori'=>$oriInfo]),
                    ];
                    A('Api/ShIssue','Model')->log($logArr);

                    $this->ajaxReturn(['code'=>0,'msg'=>'编辑成功','data'=>$data]);
                }else{
                    $this->ajaxReturn(['code'=>1,'msg'=>'编辑出错']);
                }
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'开始时间或结束时间与已有广告重叠，请修改时间']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'不存在该记录']);
        }
    }

    /**
     * 上海数据检查-删除遗漏的广告
     */
    public function DelAd(){
        $fid = $this->P['fid'];
        $c = 0;
        $oriIssueArr = [];
        if($fid){
            $fids = explode(',', $fid);
            foreach($fids as $k=>$v){
                $issueInfo = M('sh_issue')->where(['fid'=>$v])->find();
                if($issueInfo['fcreator']){
                    $del = M('sh_issue')->where(['fid'=>$v])->delete();
                    if($del){
                        $c++;
                    }
                    // 被删除的记录
                    array_push($oriIssueArr,$issueInfo);
                }else{
                    $this->ajaxReturn(['code'=>1,'msg'=>'无法删除系统广告']);
                }
            }
            if($c){
                // 操作日志
                $fapimedialist_id = M('tapimedialist')
                    ->alias('a')
                    ->cache(true,86400)
                    ->join('tapimedia_map b ON b.fid=a.fmediaid')
                    ->where(['b.fmediaid'=>$issueInfo['fmediaid'],'a.fissuedate'=>strtotime($issueInfo['fissuedate'])])
                    ->getField('a.fid');
                $log = '删除广告记录,ID:'.$fid;
                $logArr = [
                    'fapimedialist_id' => (int)$fapimedialist_id,
                    'foperator'        => session('regulatorpersonInfo.fid'),
                    'flogtype'         => 5,
                    'flog'             => $log,
                    'fcontent'         => json_encode($oriIssueArr),
                ];
                A('Api/ShIssue','Model')->log($logArr);
                
                $this->ajaxReturn(['code'=>0,'msg'=>'成功删除'.$c.'条记录']);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'未删除任何数据']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 上海数据检查-标记错误的广告
     */
    public function MarkErrAd(){
        $fid = $this->P['fid'];
        $fis_error = $this->P['fis_error']; // 标记此广告是否有错 0-正常 1-错误
        $dataAd = [
            'fis_error' => $fis_error,
            'fmodifier' => session('regulatorpersonInfo.fid'),
            'fmodifytime' => date('Y-m-d H:i:s')
        ];
        $existWhere = [
            'fid' => $fid,
        ];
        $isExist = M('sh_issue')
            ->where($existWhere)
            ->count() > 0 ? true : false;
        if($isExist){
            $ret = M('sh_issue')->where($existWhere)->save($dataAd);
            $data['fid'] = $ret;
            if($ret){
                $this->ajaxReturn(['code'=>0,'msg'=>'标记成功','data'=>$data]);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'标记出错']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'不存在该记录']);
        }
    }

    /**
     * 根据媒介获取可检查日期
     */
    public function getAvaDateByMedia(){
        $fmediaid = $this->P['fmediaid'];
        if($fmediaid){
            $avaTimeStamp = A('Common/Media','Model')->available_time($fmediaid);
            $data = [
                'avatimestamp' => date('Y-m-d',$avaTimeStamp)
            ];
            $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$data]);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 根据媒介获取可检查日期
     */
    public function getAvaDateByMediaId(){
        $type = I('type') ? I('type') : 'json';
        $fmediaid = I('fmediaid');
        $fmediaidArr = explode(',',$fmediaid);
        if(!empty($fmediaidArr)){
            foreach($fmediaidArr as $key => $mediaid){
                $data[] = A('Common/Media','Model')->getMediaProcessTime($mediaid);
            }
            if($type == 'json'){
                $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$data]);
            }elseif($type == 'table'){
                foreach($data as $k => $v){
                    foreach($v as $key => $val){
                        echo $key.' : '.$val.' , ';
                    }
                    echo '。</br>';
                }
            }else{
                var_dump($data);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 上海数据检查-检查任务列表
     */
    public function PlanList(){
        $fmediaid    = $this->P['fmediaid'];
        $fissuedateS = $this->P['fissuedateS'];
        $fissuedateE = $this->P['fissuedateE'];
        $fdstatus    = $this->P['fdstatus'];
        $pageIndex   = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize    = (int)($this->P['pageSize'] ? $this->P['pageSize'] : 1000);
        $limitIndex  = ($pageIndex-1)*$pageSize;
        $where = [];
        // 媒体
        if(!empty($fmediaid)){
            $fmediaidmap = M('tapimedia_map')->cache(true,120)->where(['fmediaid'=>$fmediaid])->getField('fid');
            $whereMediaid[] = ['EQ',$fmediaidmap];
        }
        // 日期范围
        if(!empty($fissuedateS) && !empty($fissuedateE)){
            $where['fissuedate'] = ['BETWEEN',[strtotime($fissuedateS),(strtotime($fissuedateE)+86399)]];
        }
        if(!empty($fdstatus)){
            // 状态搜索：(0-不可检查)、1-待检查、正检查 3-已检查、4-已传送、11-剪辑中、12-已退回(13同)
            switch((int)$fdstatus){
                case 1:
                    $where['fdstatus'] = ['IN',[0,1,3,6,8,-4,5]];break;
                case 3:
                    $where['fdstatus'] = ['IN',[2,7]];break;
                case 4:
                    $where['fdstatus'] = 4;break;
                case 11:
                    $where['fdstatus'] = 11;break;
                case 12:
                case 13:
                    $where['fdstatus'] = ['IN',[12,13]];break;
                default:
                    $where['fdstatus'] = ['IN',[0,1,2,3,4,5,6,7,8,11,12,13,-4]];break;
            }
        }
        // 过滤互联网媒介和未映射的媒介
        $fmediaids = M('tapimedia_map')->cache(true,10)->where(['fmediaid'=>0,'fmediatype'=>4,'_logic'=>'OR'])->getField('fid',true);
        $whereMediaid[] = ['NOT IN',$fmediaids];
        $where['fmediaid'] = $whereMediaid;

        $data = [];
        $count = M('tapimedialist')
            ->where($where)
            ->count();
        $notCount = 0;//排除不可检查数量
        if($count >= 0){
            $dataSet = M('tapimedialist')
                ->where($where)
                ->limit($limitIndex,$pageSize)
                ->order('fmediaid,fissuedate DESC')
                ->select();
            // SQL
            $sql = M('tapimedialist')->getLastSql();
            // 显示状态：0-不可检查、1-待检查、2-正检查、3-已检查、4-已传送、11-剪辑中、12-已退回
            $fshowstatus = 0;
            foreach($dataSet as $key => $val){
                // 是否可检查。默认1可检查
                $isAvailable = 1;
                // 从媒体编码映射表中取出DMP系统内对应媒体ID
                $fmediaMapInfo = M('tapimedia_map')->cache(true,120)->where(['fid'=>$val['fmediaid']])->find();
                $fmediaidMap = $fmediaMapInfo['fmediaid'];
                $fmediacodeName = $fmediaMapInfo['fmediacodename'];
                // 取出媒介类型
                $fmediatype = $val['fmediatype'] == 4 ? 13 : $val['fmediatype'];
                // 显示状态
                if(in_array($val['fdstatus'],[0,5])){
                    // 校验是否可检查
                    $isAvailable = $this->isMediaDateFixed($fmediaidMap,$val['fissuedate']);
                    // $isAvailable = $this->isMediaDateAvailable($fmediaidMap,$val['fissuedate']);
                    $fshowstatus = $isAvailable ? 1 : 0;
                }elseif(in_array($val['fdstatus'],[1,3,6,8,-4])){
                    $fshowstatus = 2;
                }elseif(in_array($val['fdstatus'],[2,7])){
                    $fshowstatus = 3;
                }elseif(in_array($val['fdstatus'],[4])){
                    $fshowstatus = 4;
                }elseif(in_array($val['fdstatus'],[11])){
                    $fshowstatus = 11;
                    $isAvailable = 0;
                }elseif(in_array($val['fdstatus'],[12,13,14,15])){
                    $fshowstatus = 12;
                    $isAvailable = 0;
                }else{
                    $fshowstatus = $val['fdstatus'];
                }
                $fisplan = ($val['fdstatus'] <= 4 || in_array($val['fdstatus'],[11,12,13,14,15])) ? 0 : 1;
                // 退回原因及退回记录状态
                $freason = '';
                $fbackstate = 0;//处理状态。0-未处理 1-剪辑处理中 2-处理完成 3-录入处理中
                if(in_array($val['fdstatus'],[12,13,15])){
                    $backlog = M('tapimedialist_backlog')->where(['fapimedialistid'=>$val['fid']])->order('fcreatetime DESC')->select();
                    if(!empty($backlog)){
                        foreach($backlog as $k=>$row){
                            // 多次退回原因一并展示，最新在最前
                            $freason .= ($k+1).'.'.$row['freason'].'['.$row['fcreatetime'].']；';
                            if($k == 0){
                                // 最新退回记录状态
                                $fbackstate = $row['fstate'];
                            }
                        }
                    }
                }
                // 若有状态搜索条件，则排除不可检查的数据
                if(!empty($where['fdstatus'])){
                    if($fshowstatus == 12 || $isAvailable === 1){
                        $data[] = [
                            'fid'            => $val['fid'],
                            'fmediaid'       => $fmediaidMap,
                            'fmedianame'     => M('tmedia')->cache(true,1200)->where(['fid' => $fmediaidMap])->getField('fmedianame'),
                            'fmediacodename' => $fmediacodeName,
                            'fmediaclass'    => M('tmediaclass')->cache(true,1200)->where(['fid' => $fmediatype])->getField('fclass'),
                            'fissuedate'     => date('Y-m-d',$val['fissuedate']),
                            'fstatus'        => $val['fstatus'],
                            'fcreatetime'    => $val['fcreatetime'],
                            'fmodifytime'    => $val['fmodifytime'],
                            'fdcount'        => $val['fdcount'],
                            'fdfinishcount'  => $val['fdfinishcount'],
                            'fdillegalcount' => $val['fdillegalcount'],
                            'fdstatus'       => $val['fdstatus'],
                            'fisplan'        => $fisplan,
                            'fisavailable'   => $isAvailable,
                            'fshowstatus'    => $fshowstatus,
                            'freason'        => $freason,
                            'fbackstate'     => $fbackstate,
                        ];
                    }else{
                        // 不可检查的数量
                        $notCount++;
                    }
                }else{
                    $data[] = [
                        'fid'            => $val['fid'],
                        'fmediaid'       => $fmediaidMap,
                        'fmedianame'     => M('tmedia')->cache(true,1200)->where(['fid' => $fmediaidMap])->getField('fmedianame'),
                        'fmediacodename' => $fmediacodeName,
                        'fmediaclass'    => M('tmediaclass')->cache(true,1200)->where(['fid' => $fmediatype])->getField('fclass'),
                        'fissuedate'     => date('Y-m-d',$val['fissuedate']),
                        'fstatus'        => $val['fstatus'],
                        'fcreatetime'    => $val['fcreatetime'],
                        'fmodifytime'    => $val['fmodifytime'],
                        'fdcount'        => $val['fdcount'],
                        'fdfinishcount'  => $val['fdfinishcount'],
                        'fdillegalcount' => $val['fdillegalcount'],
                        'fdstatus'       => $val['fdstatus'],
                        'fisplan'        => $fisplan,
                        'fisavailable'   => $isAvailable,
                        'fshowstatus'    => $fshowstatus,
                        'freason'        => $freason,
                        'fbackstate'     => $fbackstate,
                    ];
                }
            }
            // 返回的记录总数以排除不可检查记录后为准
            $realCount = $count - $notCount;
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$realCount, 'data'=>$data]);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'无数据']);
        }
    }

    /**
     * 上海数据检查-检查任务统计列表
     */
    public function summaryPlanList(){
        $fmediaid    = $this->P['fmediaid'];
        $fissuedateS = $this->P['fissuedateS'];
        $fissuedateE = $this->P['fissuedateE'];
        $fdstatus    = $this->P['fdstatus'];
        $fisreback   = $this->P['fisreback'];
        $pageIndex   = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize    = (int)($this->P['pageSize'] ? $this->P['pageSize'] : 1000);
        $limitIndex  = ($pageIndex-1)*$pageSize;
        $where = [];
        // 搜索：媒体
        if(!empty($fmediaid)){
            $fmediaidmap = M('tapimedia_map')->cache(true,120)->where(['fmediaid'=>$fmediaid])->getField('fid');
            $whereMediaid[] = ['EQ',$fmediaidmap];
        }
        // 搜索：日期范围
        if(!empty($fissuedateS) && !empty($fissuedateE)){
            $where['fissuedate'] = ['BETWEEN',[strtotime($fissuedateS),(strtotime($fissuedateE)+86399)]];
        }
        // 搜索：是否曾退回
        if((isset($fisreback) && $fisreback != '') || $fisreback == 0){
            // 曾退回
            $isrebackList = M('tapimedialist_backlog')->getField('DISTINCT fapimedialistid',true);
            if($fisreback == 1){
                $where['fid'] = ['IN',$isrebackList];
            }else{
                $where['fid'] = ['NOT IN',$isrebackList];
            }
        }
        if(!empty($fdstatus)){
            // 状态搜索：(0-不可检查)、1-待检查、正检查 3-已检查、4-已传送、11-剪辑中
            switch($fdstatus){
                case 1:
                    $where['fdstatus'] = ['IN','0,1,3,6,8,-4,5'];break;
                case 3:
                    $where['fdstatus'] = ['IN','2,7'];break;
                case 4:
                    $where['fdstatus'] = 4;break;
                case 11:
                    $where['fdstatus'] = 11;break;
                default:
                    $where['fdstatus'] = ['IN','0,1,2,3,4,5,6,7,8,11,-4'];break;
            }
        }
        // 过滤互联网媒介和未映射的媒介
        $fmediaids = M('tapimedia_map')->cache(true,10)->where(['fmediaid'=>0,'fmediatype'=>4,'_logic'=>'OR'])->getField('fid',true);
        $whereMediaid[] = ['NOT IN',$fmediaids];
        $where['fmediaid'] = $whereMediaid;

        $data = [];
        $count = M('tapimedialist')
            ->where($where)
            ->count();
        $notCount = 0;//排除不可检查数量
        if($count >= 0){
            $dataSet = M('tapimedialist')
                ->where($where)
                ->limit($limitIndex,$pageSize)
                ->order('fmediaid,fissuedate DESC')
                ->select();
            // 显示状态：0-不可检查、1-待检查、2-正检查、3-已检查、4-已传送、11-剪辑中
            $fshowstatus = 0;
            foreach($dataSet as $key => $val){
                // 从媒体编码映射表中取出DMP系统内对应媒体ID
                $fmediaMapInfo = M('tapimedia_map')->cache(true,120)->where(['fid'=>$val['fmediaid']])->find();
                $fmediaidMap = $fmediaMapInfo['fmediaid'];
                $fmediacodeName = $fmediaMapInfo['fmediacodename'];
                // 补充量
                $whereCustomerAdd = [
                    'fmediaid'   => $fmediaidMap,
                    'fissuedate' => date('Y-m-d',$val['fissuedate']),
                    'fsampleid'  => null,
                ];
                $countCustomerAdd = (int)M('sh_issue')
                    ->where($whereCustomerAdd)
                    ->count();
                // 是否存在退回
                $isReback = M('tapimedialist_backlog')
                    ->where(['fapimedialistid'=>$val['fid']])
                    ->count() > 0 ? 1 : 0;
                // 校验是否可检查
                $isAvailable = $this->isMediaDateFixed($fmediaidMap,$val['fissuedate']);
                // $isAvailable = $this->isMediaDateAvailable($fmediaidMap,$val['fissuedate']);
                // 取出媒介类型
                $fmediatype = $val['fmediatype'] == 4 ? 13 : $val['fmediatype'];
                // 显示状态
                if($isAvailable === 0){
                    $fshowstatus = 0;
                }elseif(in_array($val['fdstatus'],[0,5])){
                    $fshowstatus = 1;
                }elseif(in_array($val['fdstatus'],[1,3,6,8,-4])){
                    $fshowstatus = 2;
                }elseif(in_array($val['fdstatus'],[2,7])){
                    $fshowstatus = 3;
                }elseif(in_array($val['fdstatus'],[4])){
                    $fshowstatus = 4;
                }elseif(in_array($val['fdstatus'],[11])){
                    $fshowstatus = 11;
                }
                // 若有状态搜索条件，则排除不可检查的数据
                if(!empty($where['fdstatus'])){
                    if($isAvailable === 1){
                        $data[] = [
                            'fid'            => $val['fid'],
                            'fmediaid'       => $fmediaidMap,
                            'fmedianame'     => M('tmedia')->cache(true,120)->where(['fid' => $fmediaidMap])->getField('fmedianame'),
                            'fmediacodename' => $fmediacodeName,
                            'fmediaclass'    => M('tmediaclass')->cache(true,1200)->where(['fid' => $fmediatype])->getField('fclass'),
                            'fissuedate'     => date('Y-m-d',$val['fissuedate']),
                            'fstatus'        => $val['fstatus'],
                            'fcreatetime'    => $val['fcreatetime'],
                            'fmodifytime'    => $val['fmodifytime'],
                            'fdcount'        => $val['fdcount'],
                            'fdfinishcount'  => $val['fdfinishcount'],
                            'fdillegalcount' => $val['fdillegalcount'],
                            'fdstatus'       => $val['fdstatus'],
                            'fisplan'        => ($val['fdstatus'] <= 4 || $val['fdstatus'] == 11) ? 0 : 1,
                            'fisavailable'   => $isAvailable,
                            'fshowstatus'    => $fshowstatus,
                            'fcountadd'      => $countCustomerAdd,
                            'fisreback'      => $isReback,
                        ];
                    }else{
                        // 不可检查的数量
                        $notCount++;
                    }
                }else{
                    $data[] = [
                        'fid'            => $val['fid'],
                        'fmediaid'       => $fmediaidMap,
                        'fmedianame'     => M('tmedia')->cache(true,120)->where(['fid' => $fmediaidMap])->getField('fmedianame'),
                        'fmediacodename' => $fmediacodeName,
                        'fmediaclass'    => M('tmediaclass')->cache(true,1200)->where(['fid' => $fmediatype])->getField('fclass'),
                        'fissuedate'     => date('Y-m-d',$val['fissuedate']),
                        'fstatus'        => $val['fstatus'],
                        'fcreatetime'    => $val['fcreatetime'],
                        'fmodifytime'    => $val['fmodifytime'],
                        'fdcount'        => $val['fdcount'],
                        'fdfinishcount'  => $val['fdfinishcount'],
                        'fdillegalcount' => $val['fdillegalcount'],
                        'fdstatus'       => $val['fdstatus'],
                        'fisplan'        => ($val['fdstatus'] <= 4 || $val['fdstatus'] == 11) ? 0 : 1,
                        'fisavailable'   => $isAvailable,
                        'fshowstatus'    => $fshowstatus,
                        'fcountadd'      => $countCustomerAdd,
                        'fisreback'      => $isReback,
                    ];
                }
            }
            // 返回的记录总数以排除不可检查记录后为准
            $realCount = $count - $notCount;
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$realCount, 'data'=>$data]);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'无数据']);
        }
    }

    /**
     * 上海数据检查-检查任务列表(互联网)
     */
    public function NetPlanList(){
        $fmediaid   = $this->P['fmediaid'];
        $fissuedate = $this->P['fissuedate'];
        $fdstatus   = $this->P['fdstatus'];
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 1000);
        $limitIndex = ($pageIndex-1)*$pageSize;
        $where = [];
        if(!empty($fmediaid)){
            $fmediaidmap = M('tapimedia_map')
                ->cache(true,120)
                ->where(['fmediaid'=>$fmediaid])
                ->getField('fid');
            $whereMediaid[] = ['EQ',$fmediaidmap];
        }
        if(!empty($fissuedate)){
            $where['fissuedate'] = strtotime($fissuedate);
        }
        if(!empty($fdstatus)){
            // 状态搜索：(0-不可检查)、1-待检查、2-正检查、3-已检查、4-已传送、11-剪辑中
            switch($fdstatus){
                case 1:
                    $where['fdstatus'] = ['IN','0,5'];break;
                case 2:
                    $where['fdstatus'] = ['IN','1,3,6,8,-4'];break;
                case 3:
                    $where['fdstatus'] = ['IN','2,7'];break;
                case 4:
                    $where['fdstatus'] = 4;break;
                case 11:
                    $where['fdstatus'] = 11;break;
                default:
                    $where['fdstatus'] = ['IN','0,1,2,3,4,5,6,7,8,11,-4'];break;
            }
        }
        // 只取互联网媒介
        $fmediaids = M('tapimedia_map')
            ->cache(true,10)
            ->where(['fmediatype'=>4])
            ->getField('fid',true);
        $whereMediaid[] = ['IN',$fmediaids];
        $where['fmediaid'] = $whereMediaid;

        $data = [];
        $count = M('tapimedialist')
            ->where($where)
            ->count();
        $notCount = 0;//排除不可检查数量
        if($count >= 0){
            $dataSet = M('tapimedialist')
                ->where($where)
                ->limit($limitIndex,$pageSize)
                ->order('fmediaid,fissuedate DESC')
                ->select();
            // 显示状态：0-不可检查、1-待检查、2-正检查、3-已检查、4-已传送、11-剪辑中
            $fshowstatus = 0;
            foreach($dataSet as $key => $val){
                $fmediaidMap = 0;
                $fmediaNameMap = '';
                // 计划媒体或随机抽查媒体(fmediaid=0)
                if(!empty($val['fmediaid'])){
                    // 从媒体编码映射表中取出DMP系统内对应媒体ID
                    $fmediaidMap = M('tapimedia_map')
                        ->cache(true,120)
                        ->where(['fid'=>$val['fmediaid']])
                        ->getField('fmediaid');
                    $fmediaNameMap = M('tmedia')->cache(true,120)->where(['fid' => $fmediaidMap])->getField('fmedianame');
                    // 校验是否可检查
                    $isAvailable = $this->isNetPlanCheckable($val['fid']) ? 1 : 0;
                }else{
                    $fmediaidMap = 0;
                    $fmediaNameMap = '随机抽查媒体';
                }
                // 取出媒介类型
                $fmediatype = $val['fmediatype'] == 4 ? 13 : $val['fmediatype'];
                // 显示状态
                if($isAvailable === 0){
                    $fshowstatus = 0;
                }elseif(in_array($val['fdstatus'],[0,5])){
                    $fshowstatus = 1;
                }elseif(in_array($val['fdstatus'],[1,6,3,8,-4])){
                    $fshowstatus = 2;
                }elseif(in_array($val['fdstatus'],[2,7])){
                    $fshowstatus = 3;
                }elseif(in_array($val['fdstatus'],[4])){
                    $fshowstatus = 4;
                }elseif(in_array($val['fdstatus'],[11])){
                    $fshowstatus = 11;
                }
                // 若有状态搜索条件，则排除不可检查的数据
                if(!empty($where['fdstatus'])){
                    if($isAvailable === 1){
                        $data[$key] = [
                            'fid'            => $val['fid'],
                            'fmediaid'       => $fmediaidMap,
                            'fmedianame'     => $fmediaNameMap,
                            'fmediaclass'    => M('tmediaclass')->cache(true,1200)->where(['fid' => $fmediatype])->getField('fclass'),
                            'fissuedate'     => date('Y-m-d',$val['fissuedate']),
                            'fstatus'        => $val['fstatus'],
                            'fcreatetime'    => $val['fcreatetime'],
                            'fmodifytime'    => $val['fmodifytime'],
                            'fdcount'        => $val['fdcount'],
                            'fdfinishcount'  => $val['fdfinishcount'],
                            'fdillegalcount' => $val['fdillegalcount'],
                            'fdstatus'       => $val['fdstatus'],
                            'fisplan'        => ($val['fdstatus'] <= 4 || $val['fdstatus'] == 11) ? 0 : 1,
                            'fisavailable'   => $isAvailable,
                            'fshowstatus'    => $fshowstatus,
                        ];
                    }else{
                        // 不可检查的数量
                        $notCount++;
                    }
                }else{
                    $data[$key] = [
                        'fid'            => $val['fid'],
                        'fmediaid'       => $fmediaidMap,
                        'fmedianame'     => $fmediaNameMap,
                        'fmediaclass'    => M('tmediaclass')->cache(true,1200)->where(['fid' => $fmediatype])->getField('fclass'),
                        'fissuedate'     => date('Y-m-d',$val['fissuedate']),
                        'fstatus'        => $val['fstatus'],
                        'fcreatetime'    => $val['fcreatetime'],
                        'fmodifytime'    => $val['fmodifytime'],
                        'fdcount'        => $val['fdcount'],
                        'fdfinishcount'  => $val['fdfinishcount'],
                        'fdillegalcount' => $val['fdillegalcount'],
                        'fdstatus'       => $val['fdstatus'],
                        'fisplan'        => ($val['fdstatus'] <= 4 || $val['fdstatus'] == 11) ? 0 : 1,
                        'fisavailable'   => $isAvailable,
                        'fshowstatus'    => $fshowstatus,
                    ];
                }
            }
            // 返回的记录总数以排除不可检查记录后为准
            $realCount = $count - $notcount;
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$realCount, 'data'=>$data]);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'无数据']);
        }
    }

    /**
     * 开始检查前校验及数据获取
     */
    public function startCheckPlan(){
        $fid        = $this->P['fid'];
        $fmediaid   = $this->P['fmediaid'];
        $fissuedate = $this->P['fissuedate'];
        $jiancha    = $this->P['jiancha'];//是否进行重新检查
        if(!empty($fmediaid) && !empty($fissuedate)){
            // 获取媒体数据准备就绪的时间戳，判断是否可以检查
            try {
                // 校验是否可检查
                $isAvailable = $this->isMediaDateFixed($fmediaid,strtotime($fissuedate));
                if($isAvailable){
                    // 校验是否已存在该计划及计划检查状态,计划外则添加计划
                    $fmediaidMap = M('tapimedia_map')->where(['fmediaid'=>$fmediaid,'userid'=>self::USERID])->getField('fid');
                    if($fmediaidMap){
                        // 操作日志
                        $log = '开始检查,计划ID:'.$fid;
                        $logArr = [
                            'fapimedialist_id' => (int)$fid,
                            'foperator'        => session('regulatorpersonInfo.fid'),
                            'flogtype'         => 1,
                            'flog'             => $log,
                        ];
                        A('Api/ShIssue','Model')->log($logArr);
            
                        // 计划添加或更新状态
                        $whereIsPlanExist = [
                            'fmediaid' => $fmediaidMap,
                            'fissuedate' => strtotime($fissuedate),
                            'fuserid' => self::USERID
                        ];
                        $infoPlanExist = M('tapimedialist')->where($whereIsPlanExist)->find();
                        if(count($infoPlanExist) > 0){
                            // 抽取数据至sh_issue
                            if($infoPlanExist['fstatus'] == 0){
                                $resBackEmpty = A('Api/ShIssue','Model')->backUpEmptyIssue($fissuedate,$fmediaid,310000);
                                $resIssue = A('Api/ShIssue','Model')->ShIssue($fissuedate,$fmediaid);
                            }
                            // 数据处理状态（0待处理，1正在处理，2处理完成，3完成后再检查处理中，4数据已传送完成，-4已传送再检查处理中，5计划外待处理，6计划外正在处理，7计划外处理完成 ，8计划外完成再检查进行中，11剪辑中）
                            switch($infoPlanExist['fdstatus']){
                                case 0:
                                case 1:
                                    $fdstatus = 1;break;
                                case 2:
                                    if(!empty($jiancha)){
                                        $fdstatus = 3;
                                    }else{
                                        $fdstatus = 2;
                                    }
                                    break;
                                case 3:
                                    $fdstatus = 3;break;
                                case 4:
                                case -4:
                                    $fdstatus = -4;break;
                                case 5:
                                case 6:
                                    $fdstatus = 6;break;
                                case 7:
                                case 8:
                                    $fdstatus = 8;break;
                                case 11:
                                    $fdstatus = 11;break;
                                case 12:
                                    $fdstatus = 12;break;
                                case 13:
                                    $fdstatus = 13;break;
                                default:
                                    $this->ajaxReturn(['code'=>1, 'msg'=>'该计划状态异常，请联系管理员']);break;
                            }
                            // 更新计划表状态为进行中且fstatus抽取状态为1
                            $setPlanExist = [
                                'fstatus'     => 1,
                                'fmodifier'   => session('regulatorpersonInfo.fid') ? session('regulatorpersonInfo.fid') : 'API',
                                'fmodifytime' => date('Y-m-d H:i:s'),
                                'fdstatus'    => $fdstatus,
                            ];
                            $res = M('tapimedialist')->where($whereIsPlanExist)->save($setPlanExist);
                            // 若是正在检查的任务进行提示但仍允许检查
                            if(in_array($infoPlanExist['fdstatus'],[1,3,6,8,-4])){
                                $this->ajaxReturn(['code'=>2, 'msg'=>'该计划正在被检查中，是否仍然检查']);
                            }
                            $this->ajaxReturn(['code'=>0, 'msg'=>'成功']);
                        }else{
                            // 抽取数据至sh_issue
                            $resIssue = A('Api/ShIssue','Model')->ShIssue($fissuedate,$fmediaid);
                            // 计划外则添加计划
                            $addPlan = [
                                'fmediaid' => $fmediaidMap,
                                'fissuedate' => strtotime($fissuedate),
                                'fuserid' => self::USERID,
                                'fstatus' => 1,
                                'fcreator' => session('regulatorpersonInfo.fid') ? session('regulatorpersonInfo.fid') : 'API',
                                'fcreatetime' => date('Y-m-d H:i:s'),
                                'fdstatus' => 5,
                            ];
                            $res = M('tapimedialist')->add($addPlan);
                            if($res){
                                $this->ajaxReturn(['code'=>0, 'msg'=>'成功']);
                            }else{
                                $this->ajaxReturn(['code'=>1, 'msg'=>'出错']);
                            }
                        }
                    }else{
                        $this->ajaxReturn(['code'=>1, 'msg'=>'错误，该媒体未与用户系统映射']);
                    }
                    $this->ajaxReturn(['code'=>0, 'msg'=>'数据已就绪']);
                }else{
                    $this->ajaxReturn(['code'=>1, 'msg'=>'数据生产中，请稍后检查']);
                }
            } catch (\Throwable $th) {
                $this->ajaxReturn(['code'=>1, 'msg'=>$th]);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 上海数据检查-返回时恢复状态
     */
    public function backPlan(){
        $fid = $this->P['fid'];
        if($fid){
            $planInfo = M('tapimedialist')->where(['fid'=>$fid])->find();
            if(!empty($planInfo)){
                $fdstatus = $planInfo['fdstatus'];
                if(in_array($fdstatus,[1,3,6,8,-4])){
                    switch($planInfo['fdstatus']){
                        case 1:
                            $fdstatus = 0;break;
                        case 3:
                            $fdstatus = 2;break;
                        case -4:
                            $fdstatus = 4;break;
                        case 6:
                            $fdstatus = 5;break;
                        case 8:
                            $fdstatus = 7;break;
                        default:
                            $fdstatus = $planInfo['fdstatus'];break;
                    }
                    $data = [
                        'fdstatus' => $fdstatus,
                    ];
                    $res = M('tapimedialist')
                        ->where(['fid'=>$fid])
                        ->save($data);
                    if($res){
                        // 状态变化提示
                        $fdstatusChange = ' 状态由 '.$planInfo['fdstatus'].' 更新为 '.$fdstatus;
                        // 操作日志
                        $log = '离开检查页，计划ID:'.$fid.$fdstatusChange;
                        $logArr = [
                            'fapimedialist_id' => (int)$fid,
                            'foperator'        => session('regulatorpersonInfo.fid'),
                            'flogtype'         => 10,
                            'flog'             => $log,
                            'fcontent'         => '',
                        ];
                        A('Api/ShIssue','Model')->log($logArr);

                        $this->ajaxReturn(['code'=>0, 'msg'=>'成功'.$fdstatusChange]);
                    }else{
                        $this->ajaxReturn(['code'=>1, 'msg'=>'状态更新出错']);
                    }
                }
                $this->ajaxReturn(['code'=>0, 'msg'=>'该状态无需更新']);
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'未找到对应任务']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 上海数据检查-完成检查
     */
    public function submitPlan(){
        $fid = $this->P['fid'];
        if($fid){
            // 校验是否允许提交
            $planInfo = M('tapimedialist')->where(['fid'=>$fid])->find();
            $fmediaid = M('tapimedia_map')->where(['fid'=>$planInfo['fmediaid']])->getField('fmediaid');
            $fissuedate = $planInfo['fissuedate'];
            // 校验是否可检查
            $isAvailable = $this->isMediaDateFixed($fmediaid,$fissuedate);
            // $isAvailable = $this->isMediaDateAvailable($fmediaid,$fissuedate);
            if($isAvailable){
                $where = [
                    'fid' => $fid
                ];
                // 操作日志
                $log = '完成检查,计划ID:'.$fid;
                $logArr = [
                    'fapimedialist_id' => (int)$fid,
                    'foperator'        => session('regulatorpersonInfo.fid'),
                    'flogtype'         => 8,
                    'flog'             => $log,
                    'fcontent'         => '',
                ];
                A('Api/ShIssue','Model')->log($logArr);

                // 数据处理状态（0待处理，1正在处理，2处理完成，3完成后再检查处理中，5计划外待处理，6计划外正在处理，7计划外处理完成，8计划外完成再检查进行中)
                switch($planInfo['fdstatus']){
                    case 1:
                    case 3:
                    case -4:
                        // 计划内任务提交时校验是否新广告且生成素材地址，状态更新为11剪辑中，完成剪辑后回调更新为2任务完成状态
                        $res = A('Api/ShIssue')->gettask_isnew_tad($fid);
                        $this->ajaxReturn(['code'=>0, 'msg'=>'成功','check'=>$res]);
                        break;
                    case 6:
                    case 8:
                        $fdstatus = 7;break;
                    case 0:
                    case 5:
                    case 2:
                    case 7:
                    case 4:
                    default:
                        $this->ajaxReturn(['code'=>1, 'msg'=>'该计划状态无法进行此操作']);break;
                }
                // TODO:再次提交完成检查时，重置数据是否已获取状态，确保数据再次被获取
                $data = [
                    'fdstatus' => $fdstatus,
                    'fmodifier' => session('regulatorpersonInfo.fid'),
                    'fmodifytime' => date('Y-m-d H:i:s')
                ];
                $res = M('tapimedialist')
                    ->where($where)
                    ->save($data);
                if($res){
                    $this->ajaxReturn(['code'=>0, 'msg'=>'成功']);
                }else{
                    $this->ajaxReturn(['code'=>1, 'msg'=>'出错']);
                }
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'数据处理中，无法提交']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 上海数据检查-切换初审不违法与待初审状态
     */
    public function switchState(){
        $fid = $this->P['fid'];
        $fid = explode(',',$fid);
        $fisillegal = $this->P['fisillegal'] ? $this->P['fisillegal'] : 0;
        $fisillegal_ori = $fisillegal ? 0 : 1;
        $state = ['初审不违法','待初审'];
        if(isset($fid) && !empty($fid)){
            // 支持单条和多条更新
            $res = M('sh_issue')->where(['fid'=>['IN',$fid]])->save(['fisillegal'=>$fisillegal]);
            // 相同样本当天所有发布记录同时更新初审状态
            foreach($fid as $id){
                $issueInfo = M('sh_issue')->where(['fid'=>$id])->find();
                $issueSameSampleList = M('sh_issue')
                    ->where(['fissuedate'=>$issueInfo['fissuedate'],'fsampleid'=>$issueInfo['fsampleid']])
                    ->save(['fisillegal'=>$fisillegal]);
            }
            // 操作日志
            $fapimedialist_id = M('tapimedialist')
                ->alias('a')
                ->cache(true,86400)
                ->join('tapimedia_map b ON b.fid=a.fmediaid')
                ->where(['b.fmediaid'=>$issueInfo['fmediaid'],'a.fissuedate'=>strtotime($issueInfo['fissuedate'])])
                ->getField('a.fid');
            $log = '标记初审状态,ID:'.$this->P['fid'].' '.$state[$fisillegal_ori].'->'.$state[$fisillegal];
            $logArr = [
                'fapimedialist_id' => (int)$fapimedialist_id,
                'foperator'        => session('regulatorpersonInfo.fid'),
                'flogtype'         => 6,
                'flog'             => $log,
                'fcontent'         => '',
            ];
            A('Api/ShIssue','Model')->log($logArr);

            $this->ajaxReturn(['code'=>0, 'msg'=>'更新成功']);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 上海数据检查-退回情况列表
     */
    public function reProductPlanList(){
        $fmediaid    = $this->P['fmediaid'];
        $fissuedateS = $this->P['fissuedateS'];
        $fissuedateE = $this->P['fissuedateE'];
        $fcreator    = $this->P['fcreator'];
        $fcreatetime = $this->P['fcreatetime'];
        $fstate      = $this->P['fstate'];
        $pageIndex   = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize    = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 1000);
        $limitIndex  = ($pageIndex-1)*$pageSize;
        $where = [];
        if(!empty($fmediaid)){
            $where['fmediaid'] = $fmediaid;
        }
        if(!empty($fissuedateS) && !empty($fissuedateE)){
            $where['fissuedate'] = ['BETWEEN',[strtotime($fissuedateS),strtotime($fissuedateE)+86399]];
        }
        if(!empty($fcreator)){
            $where['fcreator'] = $fcreator;
        }
        if(!empty($fcreatetime)){
            $where['fcreatetime'] = $fcreatetime;
        }
        if(!empty($fstate)){
            $where['fstate'] = $fstate;
        }
        $count = M('tapimedialist_backlog')
            ->where($where)
            ->count();
        if($count > 0){
            $dataSet = M('tapimedialist_backlog')
                ->where($where)
                ->limit($limitIndex,$pageSize)
                ->select();
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$count, 'data'=>$dataSet]);
        }
    }

    /**
     * 上海数据检查-退回计划重做(剪辑和/或录入)
     */
    public function reProductPlan(){
        $fid = $this->P['fid'];
        $freason = $this->P['freason'];
        if(isset($fid) && !empty($fid)){
            $planInfo = M('tapimedialist')->where(['fid'=>$fid])->find();
            if(!empty($planInfo)){
                // M()->startTrans();
                // 添加退回记录
                $data = [
                    'fapimedialistid' => $planInfo['fid'],
                    'fmediaid'        => $planInfo['fmediaid'],
                    'fissuedate'      => $planInfo['fissuedate'],
                    'freason'         => $freason,
                    'fstate'          => 0,
                    'fcreator'        => session('regulatorpersonInfo.fid'),
                    'fcreatetime'     => date('Y-m-d H:i:s'),
                ];
                $resAdd = M('tapimedialist_backlog')->add($data);
                // 更新计划状态
                $planSaveData = [
                    'fdstatus'    => 12,
                    // 'fstatus'     => 0,//数据抽取状态改变迁移到任务[提交]后
                    'fmodifier'   => session('regulatorpersonInfo.fid'),
                    'fmodifytime' => date('Y-m-d H:i:s'),
                ];
                $resUpdate = M('tapimedialist')->where(['fid'=>$fid])->save($planSaveData);
                // 媒体、日期
                $mediaInfo = M('tmedia')
                    ->alias('t')
                    ->cache(true,120)
                    ->field('t.fid,t.fmedianame')
                    ->join('tapimedia_map AS tm ON tm.fmediaid = t.fid')
                    ->where(['tm.fid'=>$planInfo['fmediaid']])
                    ->find();
                $issuedate = date('Y-m-d',$planInfo['fissuedate']);
                $isDev = $_SERVER['HTTP_HOST'] == 'shanghai.hz-data.xyz' ? false : true;
                if(!$isDev){
                    // 钉钉通知
                    $title = '被退回';
                    $content = "> 友情提示：\n\n上海有数据计划被退回，请处理：\n\n".$mediaInfo['fmedianame']." ".$issuedate."\n\n原因：".$freason."\n\n";
                    $smsphone = ['13858090079'];//提醒人员，沈露
                    $token = '3cc83737c35805d21c51028067c707418c64a8a257b7ed30bccef3ef5b295efc';
                    push_ddtask($title,$content,$smsphone,$token,'markdown');
                    // 提交抽检错误信息
                    // $resPush = A('Api/ShIssue')->push_error_backtask($mediaInfo['fid'],$freason,$issuedate,310000);
                }

                if($resAdd && $resUpdate){
                    // M()->commit();
                    // 操作日志
                    $log = '退回，计划ID:'.$fid.' 原因:'.$freason;
                    $logArr = [
                        'fapimedialist_id' => (int)$fid,
                        'foperator'        => session('regulatorpersonInfo.fid'),
                        'flogtype'         => 9,
                        'flog'             => $log,
                        'fcontent'         => '',
                    ];
                    A('Api/ShIssue','Model')->log($logArr);

                    $this->ajaxReturn(['code'=>0, 'msg'=>'退回成功']);
                }else{
                    // M()->rollback();
                    $this->ajaxReturn(['code'=>1, 'msg'=>'出错']);
                }
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'未找到对应任务计划']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 上海数据检查-确认完成退回计划
     */
    public function finishReProductPlan(){
        $fid = $this->P['fid'];
        if(isset($fid) && !empty($fid)){
            $planInfo = M('tapimedialist')->where(['fid'=>$fid])->find();
            if(!empty($planInfo)){
                M()->startTrans();
                // 更新退回记录状态
                $data = [
                    'fstate'      => 2,
                    'fmodifier'   => session('regulatorpersonInfo.fid'),
                    'fmodifytime' => date('Y-m-d H:i:s'),
                ];
                $where = [
                    'fapimedialistid' => $fid,
                    'fstate' => 0,
                ];
                $resUpLog = M('tapimedialist_backlog')->where($where)->save($data);
                // 更新计划状态
                $planSaveData = [
                    'fdstatus'    => 0,
                    'fmodifier'   => session('regulatorpersonInfo.fid'),
                    'fmodifytime' => date('Y-m-d H:i:s'),
                ];
                $wherePlan = [
                    'fid' => $fid,
                    'fdstatus' => 12,
                ];
                $resUpPlan = M('tapimedialist')->where($wherePlan)->save($planSaveData);
                if($resUpLog && $resUpPlan){
                    M()->commit();
                    $this->ajaxReturn(['code'=>0, 'msg'=>'已完成']);
                }else{
                    M()->rollback();
                    $this->ajaxReturn(['code'=>1, 'msg'=>'出错']);
                }
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'未找到对应任务计划']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 上海数据检查-超过2小时未处理完成的退回任务进行钉钉提醒
     */
    public function overtimeNotify(){
        // 超时时长定义(秒) 2小时
        $timeLimit = 2*3600;
        $fctime = date('Y-m-d H:i:s',(time() - $timeLimit));
        // 提醒间隔时长定义(秒) 10分钟
        $timeRange = 10*60;
        $fmtime = time() - $timeRange;

        $where = [
            'fstate' => ['NEQ',2],
            'fcreatetime' => ['ELT',$fctime],
        ];
        $overtimePlanList = M('tapimedialist_backlog')
            ->where($where)
            ->select();
        if(count($overtimePlanList) > 0){
            $countTips = 0;
            $content = "> 友情提示：\n\n被退回计划超过2小时尚未处理，请及时处理：\n\n";
            foreach($overtimePlanList as $key => $planInfo){
                if($planInfo['fmodifytime'] <= date('Y-m-d H:i:s',$fmtime)){
                    // 需通知的任务条数
                    $countTips++;
                    // 媒体、日期
                    $medianame = M('tmedia')
                        ->alias('t')
                        ->cache(true,120)
                        ->join('tapimedia_map AS tm ON tm.fmediaid = t.fid')
                        ->where(['tm.fid'=>$planInfo['fmediaid']])
                        ->getField('t.fmedianame');
                    $issuedate = date('Y-m-d',$planInfo['fissuedate']);
                    // 钉钉通知内容拼装
                    $content .= $medianame." ".$issuedate."\n\n";
                    // 记录钉钉通知时间,以限制通知频率
                    M('tapimedialist_backlog')
                        ->where(['fid'=>$planInfo['fid']])
                        ->save(['fmodifier'=>'钉钉通知','fmodifytime'=>date('Y-m-d H:i:s')]);
                }
            }
            if($countTips > 0){
                $content .= "共：".$countTips." 条\n\n";
                // 钉钉通知
                $title = '超时提醒';
                $smsphone = [13799348980];//提醒人员，王裕厚
                $token = '90ad9c45e6874ac55c93c85b374eebd29be04a3c4831e51ec56eef9981d4595e';
                push_ddtask($title,$content,$smsphone,$token,'markdown');
            }
        }
        $this->ajaxReturn(['code'=>0, 'msg'=>'完成']);
    }

    /**
     * 上海数据检查-获取任务详情
     */
    public function GetPlanInfo(){
        $fid = $this->P['fid'];
        if($fid){
            $where = [
                'fid' => $fid
            ];
            $res = M('tapimedialist')
                ->where($where)
                ->find();
            if($res){
                // 从媒体编码映射表中取出DMP系统内对应媒体ID
                $fmediaid = M('tapimedia_map')->cache(true,120)->where(['fid'=>$res['fmediaid']])->getField('fmediaid');
                $data = [
                    'fid'            => $res['fid'],
                    'fmediaid'       => $fmediaid,
                    'fmedianame'     => M('tmedia')->cache(true,120)->where(['fid' => $fmediaid])->getField('fmedianame'),
                    'fissuedate'     => date('Y-m-d',$res['fissuedate']),
                    'fstatus'        => $res['fstatus'],
                    'fcreatetime'    => $res['fcreatetime'],
                    'fmodifytime'    => $res['fmodifytime'],
                    'fdcount'        => $res['fdcount'],
                    'fdfinishcount'  => $res['fdfinishcount'],
                    'fdillegalcount' => $res['fdillegalcount'],
                    'fdstatus'       => $res['fdstatus']
                ];
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'data'=>$data]);
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'记录不存在']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 上海数据检查-获取广告列表
     */
    public function CheckList(){
        $fid   = $this->P['fid'];
        $fmediaid   = $this->P['fmediaid'];
        $fstarttime = $this->P['fstarttime'];
        $fendtime   = $this->P['fendtime'];
        $isnewsample   = $this->P['isnewsample']?$this->P['isnewsample']:0;
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 1000);
        $limitIndex = ($pageIndex-1)*$pageSize;
        if($fmediaid && $fstarttime && $fendtime){
            A('Api/ShIssue')->gettask_isnew_tad($fid,1);//数据是否最新广告状态更新
            $fdstatus = M('tapimedialist')->where(['fid'=>$fid])->getField('fdstatus');
            $m3u8_url   = A('Common/Media','Model')->get_m3u8($fmediaid,strtotime($fstarttime),strtotime($fendtime));
            $retList    = A('Common/AdIssue','Model')->getAdIssueByMediaSh($fmediaid,$fstarttime,$fendtime);
            $count      = (int)$retList['count'];//包含非广告时段总条数
            $countAds   = (int)$retList['count'];//仅广告总条数
            $List       = $retList['data'];
            $adList     = [];
            $resultList = [];
            if((string)$fmediaid == '11200000004897'){
                $isAdNull = false;//不需要查看段
            }else{
                $isAdNull = true;
            }
            if(!empty($List)){
                foreach($List as $key=>$row){
                    if($key == 0){//第一条
                        $max_endtime = $row['fendtime'];
                        $interval = $row['fstarttime'] - strtotime($fstarttime); //与起始时间间隔大于3秒,则创建一个空时间段
                        if($interval > 3 && $isAdNull){
                            $adNull = [
                                'fid'              => '',
                                'fsampleid'        => '',
                                'favifilename'     => '',
                                'fmediaclassid'    => '',
                                'fmediaid'         => '',
                                'fmedianame'       => '',
                                'fregionid'        => '',
                                'fadname'          => '查看段',
                                'fadclasscode'     => '',
                                'fadclasscode_v2'  => '',
                                'fadowner'         => '',
                                'fadownername'     => '',
                                'fbrand'           => '',
                                'fspokesman'       => '',
                                'fadclass'         => '',
                                'fstarttime'       => date('Y-m-d H:i:s',strtotime($fstarttime)),
                                'fendtime'         => date('Y-m-d H:i:s',$row['fstarttime']),
                                'flength'          => $interval,
                                'fillegaltypecode' => '',
                                'fexpressioncodes' => '',
                                'fexpressions'     => '',
                                'fis_error'        => '0',
                                'fcreator'         => '',
                                'fcreatetime'      => '',
                                'fmodifier'        => '',
                                'fmodifytime'      => '',
                                'fisnew'           => 0,
                            ];
                            if($isnewsample == 0){
                                array_push($adList,$adNull);
                            }
                            $count++;//创建的空闲时段计入记录总条数
                        }
                    }elseif($key > 0 && $key <= (count($List)-1)){
                        //两个广告时间间隔大于3秒,则创建一个空时间段
                        // if($max_endtime<$List[$key-1]['fendtime']){
                        //     $max_endtime = $List[$key-1]['fendtime'];
                        // }
                        // $interval = $row['fstarttime'] - $max_endtime ;
                        if($max_endtime<=$List[$key-1]['fendtime']){
                            $max_endtime = $List[$key-1]['fendtime'];
                            $interval = $row['fstarttime'] - $List[$key-1]['fendtime'];
                        }else{
                            $interval = 0 ;
                        }
                        if($interval > 3 && $isAdNull){
                            $adNull = [
                                'fid'              => '',
                                'fsampleid'        => '',
                                'favifilename'     => '',
                                'fmediaclassid'    => '',
                                'fmediaid'         => '',
                                'fmedianame'       => '',
                                'fregionid'        => '',
                                'fadname'          => '查看段',
                                'fadclasscode'     => '',
                                'fadclasscode_v2'  => '',
                                'fadowner'         => '',
                                'fadownername'     => '',
                                'fbrand'           => '',
                                'fspokesman'       => '',
                                'fadclass'         => '',
                                'fstarttime'       => date('Y-m-d H:i:s',$List[$key-1]['fendtime']),
                                'fendtime'         => date('Y-m-d H:i:s',$row['fstarttime']),
                                'flength'          => $interval,
                                'fillegaltypecode' => '',
                                'fexpressioncodes' => '',
                                'fexpressions'     => '',
                                'fis_error'        => '0',
                                'fcreator'         => '',
                                'fcreatetime'      => '',
                                'fmodifier'        => '',
                                'fmodifytime'      => '',
                                'fisnew'           => 0,
                            ];
                            if($isnewsample == 0){
                                array_push($adList,$adNull);
                            }
                            $count++;//创建的空闲时段计入记录总条数
                        }
                    }
                    $startpos = strpos($row['fmodifier'],'_') == 0 ? 0 : (strpos($row['fmodifier'])+1);
                    $samTable = substr($row['fmediaclassid'],0,2) == '01' ? 'ttvsample' : 'tbcsample';
                    $favifilename = M($samTable)->cache(true,120)->where(['fid'=>$row['fsampleid']])->getField('favifilename');
                    if(!empty($row['fis_new'])){
                        if(empty($row['fsampleid'])){
                            $fisnew = 2;
                        }else{
                            $fisnew = 1;
                        }
                    }else{
                        $fisnew = 0;
                    }
                    $ad = [
                        'fid'              => $row['fid'],
                        'fsampleid'        => $row['fsampleid'],
                        'favifilename'     => $favifilename,
                        'fmediaclassid'    => $row['fmediaclassid'],
                        'fmediaid'         => $row['fmediaid'],
                        'fmedianame'       => $row['fmedianame'],
                        'fregionid'        => $row['fregionid'],
                        'fadname'          => $row['fadname'],
                        'fadclasscode'     => $row['fadclasscode'],
                        'fadclasscode_v2'  => $row['fadclasscode_v2'],
                        'fadowner'         => $row['fadowner'],
                        'fadownername'     => $row['fadownername'],
                        'fbrand'           => $row['fbrand'],
                        'fspokesman'       => $row['fspokesman'],
                        'fadclass'         => $row['fadclass'],
                        'fstarttime'       => date('Y-m-d H:i:s',$row['fstarttime']),
                        'fendtime'         => date('Y-m-d H:i:s',$row['fendtime']),
                        'flength'          => $row['flength'],
                        'fillegaltypecode' => $row['fillegaltypecode'] > 0 ? '违法': '不违法',
                        'fexpressioncodes' => $row['fexpressioncodes'],
                        'fexpressions'     => $row['fexpressions'],
                        'fis_error'        => $row['fis_error'],
                        'fisillegal'       => $row['fisillegal'],
                        'fcreator'         => (substr($row['fcreator'],0,4) == 'add_' || $row['fcreator'] == 916) ? $row['fcreator'] : '',//是否允许编辑删除或标记错误以此为判断依据，为空时不允许编辑修改允许标记
                        // 'fcreator'      => $row['fcreator'] ? preg_replace('/\D/s', '', substr($row['fcreator'],$startpos)) : '',//TODO:部分中文编码导致出错，另只展示数字部分
                        'fcreatetime'      => $row['fcreatetime'] ? $row['fcreatetime'] : '',
                        'fmodifier'        => $row['fmodifier'] ? preg_replace('/\D/s', '', substr($row['fmodifier'],$startpos)) : '',//TODO:部分中文编码导致出错，另只展示数字部分
                        'fmodifytime'      => $row['fmodifytime'] ? $row['fmodifytime'] : '',
                        'fisnew'           => $fisnew
                    ];
                    if($isnewsample == 0 || ($fisnew == 0 && $isnewsample == 2) || ($fisnew == 1 && $isnewsample == 1)){
                        array_push($adList,$ad);
                    }
                    //最后一条后的空时间段
                    if($key == (count($List)-1)){
                        $interval = strtotime($fendtime) - $row['fendtime']; //与结束时间间隔大于3秒,则创建一个空时间段
                        if($interval > 3 && $isAdNull){
                            $adNull = [
                                'fid'              => '',
                                'fsampleid'        => '',
                                'favifilename'     => '',
                                'fmediaclassid'    => '',
                                'fmediaid'         => '',
                                'fmedianame'       => '',
                                'fregionid'        => '',
                                'fadname'          => '查看段',
                                'fadclasscode'     => '',
                                'fadclasscode_v2'  => '',
                                'fadowner'         => '',
                                'fadownername'     => '',
                                'fbrand'           => '',
                                'fspokesman'       => '',
                                'fadclass'         => '',
                                'fstarttime'       => date('Y-m-d H:i:s',$row['fendtime']),
                                'fendtime'         => date('Y-m-d H:i:s',strtotime($fendtime)),
                                'flength'          => $interval,
                                'fillegaltypecode' => '',
                                'fexpressioncodes' => '',
                                'fexpressions'     => '',
                                'fis_error'        => '0',
                                'fcreator'         => '',
                                'fcreatetime'      => '',
                                'fmodifier'        => '',
                                'fmodifytime'      => '',
                                'fisnew'           => 0,
                            ];
                            if($isnewsample == 0){
                                array_push($adList,$adNull);
                            }
                            $count++;//创建的空闲时段计入记录总条数
                        }
                    }
                }
            }else{
                if($isAdNull){
                    $adNull = [
                        'fid'              => '',
                        'fsampleid'        => '',
                        'favifilename'     => '',
                        'fmediaclassid'    => '',
                        'fmediaid'         => '',
                        'fmedianame'       => '',
                        'fregionid'        => '',
                        'fadname'          => '查看段',
                        'fadclasscode'     => '',
                        'fadclasscode_v2'  => '',
                        'fadowner'         => '',
                        'fadownername'     => '',
                        'fbrand'           => '',
                        'fspokesman'       => '',
                        'fadclass'         => '',
                        'fstarttime'       => $fstarttime,
                        'fendtime'         => $fendtime,
                        'flength'          => strtotime($fendtime) - strtotime($fstarttime),
                        'fillegaltypecode' => '',
                        'fexpressioncodes' => '',
                        'fexpressions'     => '',
                        'fis_error'        => '0',
                        'fcreator'         => '',
                        'fcreatetime'      => '',
                        'fmodifier'        => '',
                        'fmodifytime'      => '',
                        'fisnew'           => 0,
                    ];
                    if($isnewsample == 0){
                        array_push($adList,$adNull);
                    }
                }
                
                $count++;//创建的空闲时段计入记录总条数
            }
            if(!empty($adList)){
                $resultList = array_slice($adList,$limitIndex,$pageSize);
            }else{
                $resultList = [];
            }

            $dataSet = [
                'fmediaid'      => $fmediaid,
                'live_m3u8_url' => $m3u8_url,
                'fad'           => $resultList
            ];
            
            $countNotAds = $count - $countAds;
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$count, 'countAds'=>$countAds,'countNotAds'=>$countNotAds, 'data'=>$dataSet, 'fdstatus' =>$fdstatus]);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 上海数据检查-获取广告样本列表(互联网)
     */
    public function NetCheckList(){
        $fid        = $this->P['fid'];
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 1000);
        $limitIndex = ($pageIndex-1)*$pageSize;
        if(!empty($fid)){
            $planInfo = M('tapimedialist') -> where(['fid'=>$fid]) ->find();
            if(!empty($planInfo['fmediaid'])){
                $starttime = $planInfo['ftask_starttime'];
                $issuedate = $planInfo['fissuedate'];
                $samWhere = [
                    'created_date' => ['BETWEEN',[date('Y-m-d H:i:s',$starttime),date('Y-m-d H:i:s',$issuedate)]]
                ];
                $samList = M('tnetissue_customer')
                    ->where($samWhere)
                    ->limit($limitIndex,$pageSize)
                    ->select();
                // TODO:
                $resultList = [
                    'fid'              => '',
                    'fsampleid'        => '',
                    'fmediaclassid'    => '',
                    'fmediaid'         => '',
                    'fmedianame'       => '',
                    'fregionid'        => '',
                    'fadname'          => '查看段',
                    'fadclasscode'     => '',
                    'fadclasscode_v2'  => '',
                    'fadowner'         => '',
                    'fadownername'     => '',
                    'fbrand'           => '',
                    'fspokesman'       => '',
                    'fadclass'         => '',
                    'fstarttime'       => $fstarttime,
                    'fendtime'         => $fendtime,
                    'flength'          => strtotime($fendtime) - strtotime($fstarttime),
                    'fillegaltypecode' => '',
                    'fexpressioncodes' => '',
                    'fexpressions'     => '',
                    'fis_error'        => '0',
                    'fcreator'         => '',
                    'fcreatetime'      => '',
                    'fmodifier'        => '',
                    'fmodifytime'      => ''
                ];
            }else{
                $resultList = [];
            }

            $dataSet = [
                'fid'        => $fid,
                'fmediaid'   => $fmediaid,
                'fissuedate' => $planInfo['fissuedate'],
                'fad'        => $resultList,
            ];
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$count, 'data'=>$dataSet]);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 上海数据检查-截取下载广告视频片段
     */
    public function shAdCut(){
        $fmediaid   = $this->P['fmediaid'];
        $fstarttime = $this->P['fstarttime'];
        $fendtime   = $this->P['fendtime'];
        $start_time = strtotime($fstarttime);
        $end_time   = strtotime($fendtime);
        $tableName  = 'sh_issue';

        $data = S('issue_m3u8_'.$fmediaid.'_'.$start_time);
        if($data && intval($data['code']) >= 0){
            $this->ajaxReturn([
                'code'=>0,
                'msg'=>'完成',
                'data'=>['fileurl'=>$data['source_url'].'?attname='] //七牛增加attname属性参数以立即下载
                ]);
        }else{
            $back_url = U('Agp/Inspectiontask2/shAdCutCallback@'.$_SERVER['HTTP_HOST'],['fmediaid'=>$fmediaid,'start_time'=>$start_time]);
            $cut_url = C('ISSUE_CUT_URL');//剪辑广告片段的url
            $mediaInfo = M('tmedia')
                ->cache(true,300)
                ->field('left(fmediaclassid,2) as mediaClass')
                ->where(array('fid'=>$fmediaid))
                ->find();
            switch ($mediaInfo['mediaClass']) {
                case '01':
                    $file_type = 'mp4';
                    break;
                case '02';
                    $file_type = 'mp3';
                    break;
                default:
                    $file_type = 'mp4';
                    break;
            }
            $m3u8_url = A('Common/Media','Model')->get_m3u8($fmediaid,$start_time,$end_time);
            $post_data = [
                'start_time' => $start_time,
                'end_time'   => $end_time,
                'm3u8_url'   => urlencode($m3u8_url),
                'file_type'  => $file_type,
                'back_url'   => urlencode($back_url),
                'file_name'  => urlencode(date('YmdHis',$start_time).'_'.$tableName.'_'.$fmediaid),
            ];
            $ret = http($cut_url,$post_data,'GET',false,60);
            if(!$ret){
                $this->ajaxReturn(['code'=>-1,'msg'=>'生成出错','data'=>['fileurl'=>'']]);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'生成中...','data'=>['fileurl'=>'']]);
            }
        }
    }

    /**
     * 上海数据检查-广告视频片段裁剪回调
     */
    public function shAdCutCallback(){
        $post_data = file_get_contents('php://input');
        $post_data = json_decode($post_data,true);
        S('issue_m3u8_'.I('get.fmediaid').'_'.I('get.start_time'),$post_data,864000);
    }

    /**
     * 上海数据检查-获取广告视频片段生成状态
     */
    public function shAdCutGet(){
        session_write_close();
        $fmediaid   = $this->P['fmediaid'];
        $fstarttime = $this->P['fstarttime'];
        $fendtime   = $this->P['fendtime'];
        $start_time = strtotime($fstarttime);
        $end_time   = strtotime($fendtime);
        $data = S('issue_m3u8_'.$fmediaid.'_'.$start_time);
        if($data && intval($data['code']) >= 0){
            $this->ajaxReturn([
                'code'=>0,
                'msg'=>'完成',
                'data'=>['fileurl'=>$data['source_url'].'&attname='] //七牛增加attname属性参数以立即下载
                ]);
        }else{
            $this->ajaxReturn([
                'code'=>-1,
                'msg'=>'请等待',
                'data'=>['fileurl'=>'']
                ]);
        }
    }
    
    /**
     * 上海数据检查-查询广告发布记录
     */
    public function GetAdIssueList(){
        $tableName        = 'sh_issue';                                                         //表名
        $fadname          = $this->P['fadname'];                                                //广告名称
        $fadclasscode     = $this->P['fadclasscode'];                                           //广告类别
        $fmediaid         = $this->P['fmediaid'];                                               //媒介ID
        $media_class      = $this->P['media_class'] ? $this->P['media_class'] : '01';           //媒介类型，01电视，02广播，03报纸
        $fmedianame       = $this->P['fmedianame'];                                             //媒介名称
        $region_id        = $this->P['region_id'] ? $this->P['region_id'] : 310000;             //地区ID
        $fbrand           = $this->P['fbrand'];                                                 //品牌
        $fstarttime       = $this->P['fstarttime'] ? $this->P['fstarttime'] : date('Y-m-01');   //发布日期起始
        $fendtime         = $this->P['fendtime'] ? $this->P['fendtime'] : date('Y-m-d');        //发布日期结束
        $pageIndex        = $this->P['pageIndex'] ? $this->P['pageIndex'] : 1;                  //页码
        $pageSize         = $this->P['pageSize'] ? $this->P['pageSize'] : 10;                   //每页记录数
        $this_region_id   = $this->P['this_region_id'];                                         //是否只查询本级地区
        $preArr = array(
            '01' => 'ttv',
            '02' => 'tbc',
            '03' => 'tpaper'
        );
        $pre = $preArr[$media_class] ? $preArr[$media_class] : 'ttv';
        $sampleTable = $pre.'sample';               //样本表名
        $sampleId = $media_class == '03' ? 'fpapersampleid' : 'fid';//样本表主键ID字段名
        $where = [];//查询条件
        if($region_id > 0){
            if($this_region_id == 'true'){
                $where['T.fregionid'] = $region_id; //地区搜索条件
            }elseif($region_id != 100000){
                $region_id_rtrim = A('Common/System','Model')->get_region_left($region_id); //地区ID去掉末尾的0
                $region_id_strlen = strlen($region_id_rtrim);                               //去掉0后还有几位
                $where['left(T.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;       //地区搜索条件
            }
        }
        if($fadname != ''){
            $where['T.fadname'] = array('like','%'.$fadname.'%');                           //广告名称
        }
        if ($fadclasscode != ''){
            $len = strlen($fadclasscode);
            $where['LEFT(T.fadclasscode,'.$len.')'] = $fadclasscode;                        //广告类别代码
        }
        if ($fmediaid != ''){
            $where['T.fmediaid'] = $fmediaid;                                               //媒介ID
        }
        if ($fmedianame != ''){
            $where['T.fmedianame'] = array('like','%'.$fmedianame.'%');                     //媒介名称
        }
        if ($media_class != ''){
            $where['LEFT(T.fmediaclassid,2)'] = $media_class;                               //广告类别ID
        }
        if ($fbrand != ''){
            $where['T.fbrand'] = $fbrand;                                                   //品牌名称
        }
        if($fstarttime != '' || $fendtime != ''){
            $where['T.fissuedate'] = array('between',$fstarttime.','.$fendtime);
        }
        $count = M($tableName)
            ->alias('T')
            ->where($where)
            ->count();
        $field = '
            fid,
            fmediaclassid,
            fsampleid,
            fmediaid,
            fmedianame,
            fregionid,
            fadname,
            fadowner,
            fbrand,
            fspokesman,
            fadclasscode,
            fstarttime,
            fendtime,
            fissuedate,
            flength,
            sam_source_path';
        $s = ($pageIndex-1)*$pageSize;//第几行
        $issueList = M($tableName)
            ->alias('T')
            ->cache(true,1200)
            ->field($field)
            ->where($where)
            ->order('fissuedate DESC')
            ->limit($s,$pageSize)
            ->select();
        //序号前缀，T-电视 B-广播 P-报纸
        $fNameArr = array(
            '01'=>'T',
            '02'=>'B',
            '03'=>'P'
        );
        $fName = $fNameArr[$media_class] ? $fNameArr[$media_class] : 'T';
        foreach ($issueList as $k=>$row){
            $issueList[$k]['fadclass'] = M('tadclass')
                ->cache(true,3600)
                ->where(array('fcode'=>$row['fadclasscode']))
                ->getField('ffullname');
            $issueList[$k]['fregion'] = M('tregion')
                ->cache(true,3600)
                ->where(array('fid'=>$row['fregionid']))
                ->getField('ffullname');
            if ($media_class == '01' || $media_class == '02'){
                $issueList[$k]['favifilename'] = M($sampleTable)
                    ->cache(true,3600)
                    ->where(array($sampleId => $row['fsampleid']))
                    ->getField('favifilename');
            }
            $mName = date('YmdHis',$row['fstarttime']);
            $lName = substr($row['fid'],-2);
            $issueList[$k]['findex'] = $fName.$mName.$lName;
            $issueList[$k]['fstarttime'] = date('Y-m-d H:i:s',$row['fstarttime']);
            $issueList[$k]['fendtime'] = date('Y-m-d H:i:s',$row['fendtime']);
            //发布时间显示精确到秒
            $issueList[$k]['fissuedate'] = date('Y-m-d H:i:s',$row['fstarttime']);
        }
        $data = array(
            'count' => $count,
            'list'  => $issueList
        );
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
    }
    
    /**
    * 上海数据检查-导出广告发布记录
    * @Param    Mixed variable
    * @Return   void 返回的数据
    */
    public function ExportAdIssueList(){
        set_time_limit(600);
        $tableName        = 'sh_issue';                                                         //表名
        $fadname          = $this->P['fadname'];                                                //广告名称
        $fadclasscode     = $this->P['fadclasscode'];                                           //广告类别
        $fmediaid         = $this->P['fmediaid'];                                               //媒介ID
        $media_class      = $this->P['media_class'] ? $this->P['media_class'] : '01';           //媒介类型，01电视，02广播，03报纸
        $fmedianame       = $this->P['fmedianame'];                                             //媒介名称
        $region_id        = $this->P['region_id'];                                              //地区ID
        $fbrand           = $this->P['fbrand'];                                                 //品牌
        $fstarttime       = $this->P['fstarttime'] ? $this->P['fstarttime'] : date('Y-m-01');   //发布日期起始
        $fendtime         = $this->P['fendtime'] ? $this->P['fendtime'] : date('Y-m-d');        //发布日期结束
        $file_type        = $this->P['file_type'] ? strtoupper($this->P['file_type']) : 'EXCEL';//导出文件类型 EXCEL、TXT、XML
        $file_name        = $this->P['file_name'] ? $this->P['file_name'] : '导出文件'.time();   //导出文件名
        $this_region_id   = $this->P['this_region_id'];     

        $preArr = [
            '01' => 'ttv',
            '02' => 'tbc',
            '03' => 'tpaper'
        ];
        $pre = $preArr[$media_class] ? $preArr[$media_class] : 'ttv';
        $sampleTable = $pre.'sample';//样本表名
        $sampleId = $media_class == '03' ? 'fpapersampleid' : 'fid';//样本表主键ID字段名
        $where = [];//查询条件
        if($region_id > 0){
            if($this_region_id == 'true'){
                $where['T.fregionid'] = $region_id; //地区搜索条件
            }elseif($region_id != 100000){
                $region_id_rtrim = A('Common/System','Model')->get_region_left($region_id); //地区ID去掉末尾的0
                $region_id_strlen = strlen($region_id_rtrim);                               //去掉0后还有几位
                $where['left(T.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;       //地区搜索条件
            }
        }
        if($fadname != ''){
            $where['T.fadname'] = array('like','%'.$fadname.'%');                           //广告名称
        }
        if ($fadclasscode != ''){
            $where['T.fadclasscode'] = $fadclasscode;                                       //广告类别代码
        }
        if ($fmediaid != ''){
            $where['T.fmediaid'] = $fmediaid;                                               //发布媒介
        }
        if ($fmedianame != ''){
            $where['T.fmedianame'] = array('like','%'.$fmedianame.'%');                     //媒介名称
        }
        if ($fbrand != ''){
            $where['T.fbrand'] = $fbrand;                                                   //品牌名称
        }
        if ($media_class != ''){
            $where['LEFT(T.fmediaclassid,2)'] = $media_class;                               //广告类别ID
        }
        if($fstarttime != '' || $fendtime != ''){
            $where['T.fissuedate'] = array('between',$fstarttime.','.$fendtime);
        }
        $count = M($tableName)
            ->alias('T')
            ->where($where)
            ->count();
        if ($count > 20000){
            $this->ajaxReturn(array('code'=>1,'msg'=>'无法导出，请筛选条件确保数据在20000条以内！'));
        }
        $field = '
            T.fid,
            T.fsampleid,
            T.fmediaid,
            T.fmedianame,
            T.fmediaclassid,
            T.fregionid,
            T.fadname,
            T.fadclasscode,
            T.fadowner,
            T.fadownername,
            T.fbrand,
            T.fspokesman,
            T.fstarttime,
            T.fendtime,
            T.fissuedate,
            T.flength,
            T.sam_source_path';
        $issueList = M($tableName)
            ->alias('T')
            ->field($field)
            ->where($where)
            ->order('fissuedate DESC')
            ->select();
        //导出文件数据集含表头
        $dataSet[0] = ['序号','广告名称','广告类别','播出媒体','媒体类别','品牌','发布日期','开始时间','结束时间','时长(s)','广告主','链接'];
        foreach ($issueList as $k => $row){
            $issueList[$k]['fadclass'] = M('tadclass')
                ->cache(true,3600)
                ->where(array('fcode'=>substr($row['fadclasscode'], 0, 2)))
                ->getField('ffullname');
            $issueList[$k]['fregion'] = M('tregion')
                ->cache(true,3600)
                ->where(array('fid'=>$row['fregionid']))
                ->getField('ffullname');
            $issueList[$k]['fmediaclass'] = M('tmediaclass')
                ->cache(true,3600)
                ->where(array('fid'=>$row['fmediaclassid']))
                ->getField('ffullname');
            if ($media_class == '01' || $media_class == '02'){
                $issueList[$k]['favifilename'] = M($sampleTable)
                    ->cache(true,3600)
                    ->where(array($sampleId => $row['fsampleid']))
                    ->getField('favifilename');
            }
            //显式地赋值,便于与表头对应及后续维护
            $data[$k] = [
                'findex'        => $k+1,
                'fadname'       => $row['fadname'],
                'fadclass'      => $issueList[$k]['fadclass'],
                'fmedianame'    => $row['fmedianame'],
                'fmediaclass'   => $issueList[$k]['fmediaclass'],
                'fbrand'        => $row['fbrand'],
                'fissuedate'    => date('Y-m-d',strtotime($row['fissuedate'])),
                'fstarttime'    => date('H:i:s',$row['fstarttime']),
                'fendtime'      => date('H:i:s',$row['fendtime']),
                'flength'       => $row['flength'],
                'fadownername'  => $row['fadownername'],
                'favifilename'  => $issueList[$k]['favifilename'],
            ];
            array_push($dataSet,$data[$k]);
        }
        if(!$dataSet || empty($dataSet)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
        }
        if($file_type=='EXCEL'){
            $this->downloadExcel($dataSet);
        }elseif($file_type=='XML'){
            $this->downloadXml($dataSet);
        }elseif($file_type=='TXT'){
            $this->downloadTxt($dataSet);
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'暂不支持导出该文件类型，可选项：EXCEL、XML、TXT'));
        }
    }
    
    /**
     * 导出到Excel文件
     * @Param   Array $data 数据集，包含表头且表头与内容列顺序对应
     * @Return  String $url 生成上传OSS文件下载地址
     */
    public function downloadExcel($data)
    {
        header("Content-type: text/html; charset=utf-8"); 
        $objPHPExcel = new \PHPExcel();  
        $objPHPExcel
            ->getProperties()                   //获得文件属性对象，给下文提供设置资源
            ->setCreator("DATAGATHER")          //设置文件的创建者
            ->setLastModifiedBy("DATAGATHER")   //设置最后修改者
            ->setTitle("导出文件")               //设置标题
            ->setSubject("Office2007 XLSX" )    //设置主题
            ->setDescription("DATAGATHER")      //设置备注
            ->setKeywords("DATAGATHER")         //设置标记
            ->setCategory("XLSX");              //设置类别
        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        $AZ = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        foreach ($data as $key => $value) {
            $i = 0;
            foreach($value as $col => $content){
                $cell = $AZ[$i].($key+1);
                $sheet -> setCellValue($cell,$content);
                $i++;
            }
        }
        $objActSheet =$objPHPExcel->getActiveSheet();
        $objActSheet->setTitle('导出数据');//设置当前活动工作表名称
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $filename = date('YmdHis').'.xlsx';
        $dir = './LOG/';
        if(!file_exists($dir)){
            mkdir($dir);
        }
        $savefile = $dir.$filename;
        $objWriter->save($savefile);
        D('Function')->write_log('广告发布记录',1,'Excel导出成功');
        $ret = A('Common/AliyunOss','Model')
            ->file_up('tem/'.$filename,$savefile,['Content-Disposition' => 'attachment;filename=']);//上传云
        unlink($savefile);//删除文件
        $this->ajaxReturn(['code'=>0,'msg'=>'导出成功','data'=>['file_url' => $ret['url']]]);
    }

    /**
     * 导出到XML文件
     * @Param   Array $data 数据集，包含表头且表头与内容列顺序对应
     * @Return  String $url 生成上传OSS文件下载地址
     */
    public function downloadXml($data)
    {
        $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        $xml .= "<ads>\n";
        foreach ($data as $key =>$value) {
            if($key > 0){
                //第一行表头不导出
                $xml .= "<item>\n";
                foreach($value as $col => $content){
                    $xml .= "<".$col.">" . $content . "</".$col.">\n";
                }
                $xml .= "</item>\n";
            }
        }
        $xml .= "</ads>\n";
        
        $filename = date('YmdHis').'.xml';
        $dir = './LOG/';
        if(!file_exists($dir)){
            mkdir($dir);
        }
        $savefile = $dir.$filename;
        file_put_contents($savefile,$xml);//生成本地文件
        D('Function')->write_log('广告发布记录',1,'XML导出成功');
        $ret = A('Common/AliyunOss','Model')
            ->file_up('tem/'.$filename,$savefile,['Content-Disposition' => 'attachment;filename=']);//上传云
        unlink($savefile);//删除文件
        $this->ajaxReturn(['code'=>0,'msg'=>'导出成功','data'=>['file_url' => $ret['url']]]);
    }

    /**
     * 导出到TXT文件
     * @Param   Array $data 数据集，包含表头且表头与内容列顺序对应
     * @Return  String $url 生成上传OSS文件下载地址
     */
    public function downloadTxt($data)
    {
        $txtcontent = '';
        foreach ($data as $key => $value) {
            foreach($value as $col => $content){
                $txtcontent .= $content.'  ';
            }
            $txtcontent .= "\n";
        }

        $filename = date('YmdHis').'.txt';
        $dir = './LOG/';
        if(!file_exists($dir)){
            mkdir($dir);
        }
        $savefile = $dir.$filename;
        file_put_contents($savefile,$txtcontent);
        D('Function')->write_log('广告发布记录',1,'TXT导出成功');
        $ret = A('Common/AliyunOss','Model')
            ->file_up('tem/'.$filename,$savefile,['Content-Disposition' => 'attachment;filename=']);//上传云
        unlink($savefile);//删除文件
        $this->ajaxReturn(['code'=>0,'msg'=>'导出成功','data'=>['file_url' => $ret['url']]]);
    }

    /**
     * 上海数据检查-查询样本列表
     */
    public function GetAdSampleList(){

    }

    /**
     * 返回广告类型树 TODO:有待抽象到模型
     */
    public function getAdClassArr()
    {
        $data = M('tadclass')
            ->field('fcode, fpcode, fadclass')
            ->where(['fstate' => ['EQ', 1], 'fcode' => ['NEQ', 2301]])
            ->cache(true)
            ->select();
        $res = [];
        foreach ($data as $key => &$value) {
            if (strlen($value['fcode']) === 2){
                $res[] = [
                    'value' => $value['fcode'],
                    'label' => $value['fadclass'],
                ];
            }
        }
        foreach ($data as $key => &$value2) {
            $value2['value'] = $value2['fcode'];
            $value2['label'] = $value2['fadclass'];
            if (strlen($value2['fcode']) === 4){
                foreach ($res as &$item) {
                    if ($value2['fpcode'] === $item['value']){
                        $item['children'][] = $value2;
                        break;
                    }
                }
            }
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$res]);
    }

    /**
     * 商业类型树 TODO:有待抽象到模型
     */
    public function getAdClassTree()
    {
        $data = M('hz_ad_class')->field('fcode value, fpcode, fname label')->where(['fstatus' => ['EQ', 1]])->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpcode']])){
                $items[$value['fpcode']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$tree]);
    }

    /**
     * 北京数据检查-获取广告列表
     */
    public function BjCheckList(){
        $fmediaid   = $this->P['fmediaid'];
        $fstarttime = $this->P['fstarttime'];
        $fendtime   = $this->P['fendtime'];
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex']: 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 1000);
        $limitIndex = ($pageIndex-1)*$pageSize;
        if($fmediaid && $fstarttime && $fendtime){
            $m3u8_url = A('Common/Media','Model')->get_m3u8($fmediaid,strtotime($fstarttime),strtotime($fendtime));
            $retList = A('Common/AdIssue','Model')->getAdIssueByMediaBj($fmediaid,$fstarttime,$fendtime);
            $count = $retList['count'];
            $List = $retList['data'];
            $adList = [];
            $resultList = [];
            if(!empty($List)){
                foreach($List as $key=>$row){
                    if($key > 0){
                        //两个广告时间间隔大于3秒,则创建一个空时间段
                        $interval = $row['fstarttime'] - $List[$key-1]['fendtime'];
                        if($interval > 3){
                            $adNull = [
                                'fid' => '',
                                'fsampleid' => '',
                                'fmediaclassid' => '',
                                'fmediaid' => '',
                                'fmedianame' => '',
                                'fregionid' => '',
                                'fadname' => '--',
                                'fadclasscode' => '',
                                'fadclasscode_v2' => '',
                                'fadowner' => '',
                                'fadownername' => '',
                                'fbrand' => '',
                                'fspokesman' => '',
                                'fadclass' => '',
                                'fstarttime'=> date('Y-m-d H:i:s',$List[$key-1]['fendtime']),
                                'fendtime'=> date('Y-m-d H:i:s',$row['fstarttime']),
                                'flength' => $interval,
                                'fillegaltypecode'=> '',
                                'fexpressioncodes'=> '',
                                'fexpressions' => '',
                                'fis_error' => '0',
                                'fcreator' => '',
                                'fcreatetime' => '',
                                'fmodifier' => '',
                                'fmodifytime' => ''
                            ];
                            array_push($adList,$adNull);
                            $count++;//创建的空闲时段计入记录总条数
                        }
                    }
                    $startpos = strpos($row['fmodifier'],'_') == 0 ? 0 : (strpos($row['fmodifier'])+1);
                    $ad = [
                        'fid' => $row['fid'],
                        'fsampleid' => $row['fsampleid'],
                        'fmediaclassid' => $row['fmediaclassid'],
                        'fmediaid' => $row['fmediaid'],
                        'fmedianame' => $row['fmedianame'],
                        'fregionid' => $row['fregionid'],
                        'fadname' => $row['fadname'],
                        'fadclasscode' => $row['fadclasscode'],
                        'fadclasscode_v2' => $row['fadclasscode_v2'],
                        'fadowner' => $row['fadowner'],
                        'fadownername' => $row['fadownername'],
                        'fbrand' => $row['fbrand'],
                        'fspokesman' => $row['fspokesman'],
                        'fadclass' => $row['fadclass'],
                        'fstarttime'=> date('Y-m-d H:i:s',$row['fstarttime']),
                        'fendtime'=> date('Y-m-d H:i:s',$row['fendtime']),
                        'flength' => $row['flength'],
                        'fillegaltypecode'=> $row['fillegaltypecode'] > 0 ? '违法' : '不违法',
                        'fexpressioncodes' => $row['fexpressioncodes'],
                        'fexpressions' => $row['fexpressions'],
                        'fis_error' => $row['fis_error'],
                        'fcreator' => (substr($row['fcreator'],0,4) == 'add_' || $row['fcreator'] == 916) ? $row['fcreator'] : '',//是否允许编辑删除或标记错误以此为判断依据，为空时不允许编辑修改允许标记
                        // 'fcreator' => $row['fcreator'] ? preg_replace('/\D/s', '', substr($row['fcreator'],$startpos)) : '',//TODO:部分中文编码导致出错，另只展示数字部分
                        'fcreatetime' => $row['fcreatetime'] ? $row['fcreatetime'] : '',
                        'fmodifier' => $row['fmodifier'] ? preg_replace('/\D/s', '', substr($row['fmodifier'],$startpos)) : '',//TODO:部分中文编码导致出错，另只展示数字部分
                        'fmodifytime' => $row['fmodifytime'] ? $row['fmodifytime'] : ''
                    ];
                    array_push($adList,$ad);
                }
            }else{
                $adNull = [
                    'fid'              => '',
                    'fsampleid'        => '',
                    'fmediaclassid'    => '',
                    'fmediaid'         => '',
                    'fmedianame'       => '',
                    'fregionid'        => '',
                    'fadname'          => '--',
                    'fadclasscode'     => '',
                    'fadclasscode_v2'  => '',
                    'fadowner'         => '',
                    'fadownername'     => '',
                    'fbrand'           => '',
                    'fspokesman'       => '',
                    'fadclass'         => '',
                    'fstarttime'       => $fstarttime,
                    'fendtime'         => $fendtime,
                    'flength'          => strtotime($fendtime) - strtotime($fstarttime),
                    'fillegaltypecode' => '',
                    'fexpressioncodes' => '',
                    'fexpressions'     => '',
                    'fis_error'        => '0',
                    'fcreator'         => '',
                    'fcreatetime'      => '',
                    'fmodifier'        => '',
                    'fmodifytime'      => ''
                ];
                array_push($adList,$adNull);
                $count++;//创建的空闲时段计入记录总条数
            }
            $resultList = array_slice($adList,$limitIndex,$pageSize);
            $dataSet = [
                'fmediaid'      => $fmediaid,
                'live_m3u8_url' => $m3u8_url,
                'fad'           => $resultList
            ];
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$count, 'data'=>$dataSet]);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 判断互联网检查计划是否可检查
     */
    public function isNetPlanCheckable($planId){
        if(!empty($planId)){
            $planInfo = M('tapimedialist')
                ->where(['fid'=>$planId,'fuserid'=>self::USERID])
                ->find();
            // 1.发布日期是未来日期不可检查
            if($planInfo['fissuedate'] > time()){
                return false;
            }
            // 2.截图完成的样本量小于任务阀值不可检查
            $fthreshold = M('tapimedia_map')
                ->where(['fid'=>$planInfo['fmediaid']])
                ->getField('fthreshold');
            $countS = date('Y-m-d H:i:s',$planInfo['ftask_starttime']);
            $countE = date('Y-m-d H:i:s',$planInfo['fissuedate']);
            $countWhere = [
                'created_date' => ['BETWEEN',[$countS,$countE]]
            ];
            $completeCount = M('tnetissue_customer')
                ->where($countWhere)
                ->count();
            if($completeCount < $fthreshold){
                return false;
            }
            return true;
        }else{
            return false;
        }
    }
    /**
     * @Des: 判断媒体日期是否已归档就绪
     * @Edt: yuhou.wang
     * @Date: 2020-03-09 13:01:24
     * @param   Int $fmediaid 媒体ID
     * @param   Int $fissuedate 日期时间戳
     * @return  Int $fixedState 1-就绪 0-未就绪
     */
    public function isMediaDateFixed($fmediaid = 0,$fissuedate = 0){
        $fixedState = A('Admin/FixedMediaData','Model')->get_state($fmediaid,date('Y-m-d',$fissuedate));
        return $fixedState == 2 ? 1 : 0;
    }

    /**
     * 判断媒体日期数据是否就绪
     * @param   Int $fmediaid 媒体ID
     * @param   Int $fissuedate 日期时间戳
     * @return  Int $isAvailable 1-就绪 0-未就绪
     */
    public function isMediaDateAvailable($fmediaid = 0,$fissuedate = 0){
        $isAvailable = 0;
        if($fmediaid && $fissuedate){
            $avaTimeStamp = A('Common/Media','Model')->available_time($fmediaid);
            $isAvailable = ($fissuedate <= $avaTimeStamp) ? 1 : 0;
        }
        return $isAvailable;
    }

    /**
     * 上海局计划数据抽取
     */
    public function shIssue(){
        $fissuedate = $this->P['fissuedate'];
        $fmediaid   = $this->P['fmediaid'];
        $userid   = $this->P['userid'];
        $resIssue = A('Api/ShIssue','Model')->ShIssue($fissuedate,$fmediaid,$userid);
        $this->ajaxReturn(['code'=>0, 'msg'=>'成功']);
    }

    /**
     * 国家局计划数据抽取
     */
    public function checkIssue(){
        $fissuedate = $this->P['fissuedate'];
        $fmediaid   = $this->P['fmediaid'];
        $userid   = $this->P['userid'];
        $resIssue = A('Api/ShIssue','Model')->CheckIssue($fissuedate,$fmediaid,$userid);
        $this->ajaxReturn(['code'=>0, 'msg'=>'成功']);
    }
    /**
     * @Des: 上海数据检查计划管理-发送给剪辑
     * @Edt: yuhou.wang
     * @Date: 2019-10-30 09:06:00
     * @param {type} 
     * @return: 
     */
    public function sendCut(){
        $fid = $this->P['fid'];
        if(!empty($fid)){
            $planInfo = M('tapimedialist')->where(['fid'=>$fid,'fdstatus'=>12,'fuserid'=>self::USERID])->find();
            if(!empty($planInfo)){
                $backInfo = M('tapimedialist_backlog')->cache(true)->where(['fapimedialistid'=>$fid,'fstate'=>0])->order('fcreatetime DESC')->find();
                // 媒体
                $mediaInfo = M('tmedia')
                    ->alias('t')
                    ->cache(true,120)
                    ->field('t.fid,t.fmedianame')
                    ->join('tapimedia_map AS tm ON tm.fmediaid = t.fid')
                    ->where(['tm.fid'=>$planInfo['fmediaid']])
                    ->find();
                $issuedate = date('Y-m-d',$planInfo['fissuedate']);
                $isDev = $_SERVER['HTTP_HOST'] == 'shanghai.hz-data.xyz' ? false : true;
                if (!$isDev) {
                    // 发送重新剪辑
                    $resPush = A('Api/ShIssue')->push_error_backtask($mediaInfo['fid'], $backInfo['freason'], $issuedate, 310000);
                    // 钉钉通知
                    $title = '被退回，请剪辑处理';
                    $content = "> 友情提示：\n\n上海有数据计划被退回，请剪辑处理：\n\n".$mediaInfo['fmedianame']." ".$issuedate."\n\n原因：".$backInfo['freason']."\n\n";
                    $smsphone = ['17611266159'];//提醒人员，马国琪
                    $token = '3cc83737c35805d21c51028067c707418c64a8a257b7ed30bccef3ef5b295efc';
                    push_ddtask($title, $content, $smsphone, $token, 'markdown');
                }
                // 退回记录状态更新为剪辑中
                M('tapimedialist_backlog')->where(['fid'=>$backInfo['fid']])->save(['fstate'=>1,'fmodifier'=>session('regulatorpersonInfo.fid'),'fmodifytime'=>date('Y-m-d H:i:s')]);
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功']);
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'此计划当前不允许该操作']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }
    /**
     * @Des: 上海数据检查计划管理-完成剪辑
     * @Edt: yuhou.wang
     * @Date: 2019-10-30 09:06:00
     * @param {type} 
     * @return: 
     */
    public function finishCut(){
        $fid = $this->P['fid'];
        if(!empty($fid)){
            $planInfo = M('tapimedialist')->where(['fid'=>$fid,'fdstatus'=>12,'fuserid'=>self::USERID])->find();
            if(!empty($planInfo)){
                $backInfo = M('tapimedialist_backlog')->cache(true)->where(['fapimedialistid'=>$fid,'fstate'=>['IN',[0,1]]])->order('fcreatetime DESC')->find();
                // 媒体
                $mediaInfo = M('tmedia')
                    ->alias('t')
                    ->cache(true,120)
                    ->field('t.fid,t.fmedianame')
                    ->join('tapimedia_map AS tm ON tm.fmediaid = t.fid')
                    ->where(['tm.fid'=>$planInfo['fmediaid']])
                    ->find();
                $issuedate = date('Y-m-d',$planInfo['fissuedate']);
                $isDev = $_SERVER['HTTP_HOST'] == 'shanghai.hz-data.xyz' ? false : true;
                if (!$isDev) {
                    // 钉钉通知
                    $title = '完成剪辑';
                    $content = "> 友情提示：\n\n上海退回计划已完成剪辑，请录入审核：\n\n".$mediaInfo['fmedianame']." ".$issuedate."\n\n原因：".$backInfo['freason']."\n\n";
                    $smsphone = ['18109017112'];//提醒人员，曹席亮
                    $token = '3cc83737c35805d21c51028067c707418c64a8a257b7ed30bccef3ef5b295efc';
                    push_ddtask($title, $content, $smsphone, $token, 'markdown');
                }
                // 计划状态更新为剪辑完成
                M('tapimedialist')->where(['fid'=>$planInfo['fid']])->save(['fdstatus'=>13,'fmodifier'=>session('regulatorpersonInfo.fid'),'fmodifytime'=>date('Y-m-d H:i:s')]);
                // 退回记录状态更新为录入中
                M('tapimedialist_backlog')->where(['fid'=>$backInfo['fid']])->save(['fstate'=>3,'fmodifier'=>session('regulatorpersonInfo.fid'),'fmodifytime'=>date('Y-m-d H:i:s')]);
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功']);
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'此计划当前不允许该操作']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }
    /**
     * @Des: 上海数据检查计划管理-发送给录入
     * @Edt: yuhou.wang
     * @Date: 2019-10-30 09:06:00
     * @param {type} 
     * @return: 
     */
    public function sendInput(){
        $fid = $this->P['fid'];
        if(!empty($fid)){
            $planInfo = M('tapimedialist')->where(['fid'=>$fid,'fdstatus'=>['IN',[12,13]],'fuserid'=>self::USERID])->find();
            if(!empty($planInfo)){
                $backInfo = M('tapimedialist_backlog')->cache(true)->where(['fapimedialistid'=>$fid,'fstate'=>['IN',[0,1,3]]])->order('fcreatetime DESC')->find();
                // 媒体
                $mediaInfo = M('tmedia')
                    ->alias('t')
                    ->cache(true,120)
                    ->field('t.fid,t.fmedianame')
                    ->join('tapimedia_map AS tm ON tm.fmediaid = t.fid')
                    ->where(['tm.fid'=>$planInfo['fmediaid']])
                    ->find();
                $issuedate = date('Y-m-d',$planInfo['fissuedate']);
                $isDev = $_SERVER['HTTP_HOST'] == 'shanghai.hz-data.xyz' ? false : true;
                if (!$isDev) {
                    // 钉钉通知
                    $title = '被退回，请录入审核';
                    $content = "> 友情提示：\n\n上海退回计划无需剪辑，请录入审核：\n\n".$mediaInfo['fmedianame']." ".$issuedate."\n\n原因：".$backInfo['freason']."\n\n";
                    $smsphone = ['18109017112'];//提醒人员，曹席亮
                    $token = '3cc83737c35805d21c51028067c707418c64a8a257b7ed30bccef3ef5b295efc';
                    push_ddtask($title, $content, $smsphone, $token, 'markdown');
                }
                // 计划状态更新为剪辑完成
                M('tapimedialist')->where(['fid'=>$planInfo['fid']])->save(['fdstatus'=>13,'fmodifier'=>session('regulatorpersonInfo.fid'),'fmodifytime'=>date('Y-m-d H:i:s')]);
                // 退回记录状态更新为录入中
                M('tapimedialist_backlog')->where(['fid'=>$backInfo['fid']])->save(['fstate'=>3,'fmodifier'=>session('regulatorpersonInfo.fid'),'fmodifytime'=>date('Y-m-d H:i:s')]);
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功']);
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'此计划当前不允许该操作']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }
    /**
     * @Des: 上海数据检查计划管理-完成录入
     * @Edt: yuhou.wang
     * @Date: 2019-10-30 09:06:00
     * @param {type} 
     * @return: 
     */
    public function finishInput(){
        $fid = $this->P['fid'];
        if(!empty($fid)){
            $planInfo = M('tapimedialist')->where(['fid'=>$fid,'fdstatus'=>['IN',[12,13]],'fuserid'=>self::USERID])->find();
            if(!empty($planInfo)){
                $backInfo = M('tapimedialist_backlog')->cache(true)->where(['fapimedialistid'=>$fid,'fstate'=>['IN',[0,1,3]]])->order('fcreatetime DESC')->find();
                // 媒体
                $mediaInfo = M('tmedia')
                    ->alias('t')
                    ->cache(true,120)
                    ->field('t.fid,t.fmedianame')
                    ->join('tapimedia_map AS tm ON tm.fmediaid = t.fid')
                    ->where(['tm.fid'=>$planInfo['fmediaid']])
                    ->find();
                $issuedate = date('Y-m-d',$planInfo['fissuedate']);
                $isDev = $_SERVER['HTTP_HOST'] == 'shanghai.hz-data.xyz' ? false : true;
                if (!$isDev) {
                    // 钉钉通知
                    $title = '完成录入审核';
                    $content = "> 友情提示：\n\n上海退回计划已完成录入审核，请确认后提交：\n\n".$mediaInfo['fmedianame']." ".$issuedate."\n\n原因：".$backInfo['freason']."\n\n";
                    $smsphone = ['13858090079'];//提醒人员，沈露
                    $token = '3cc83737c35805d21c51028067c707418c64a8a257b7ed30bccef3ef5b295efc';
                    push_ddtask($title, $content, $smsphone, $token, 'markdown');
                }
                // 计划状态更新为录入完成,抽取状态更新为需要重新抽取
                M('tapimedialist')->where(['fid'=>$planInfo['fid']])->save(['fdstatus'=>15,'fstate'=>0,'fmodifier'=>session('regulatorpersonInfo.fid'),'fmodifytime'=>date('Y-m-d H:i:s')]);
                // 退回记录状态更新为处理完成
                M('tapimedialist_backlog')->where(['fid'=>$backInfo['fid']])->save(['fstate'=>2,'fmodifier'=>session('regulatorpersonInfo.fid'),'fmodifytime'=>date('Y-m-d H:i:s')]);
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功']);
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'此计划当前不允许该操作']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }
    /**
     * @Des: 上海数据检查计划管理-提交计划
     * @Edt: yuhou.wang
     * @Date: 2019-10-30 09:06:00
     * @param {type} 
     * @return: 
     */
    public function reSubmitPlan(){
        $fid = $this->P['fid'];
        if(!empty($fid)){
            $planInfo = M('tapimedialist')->where(['fid'=>$fid,'fdstatus'=>['IN',[12,13,15]],'fuserid'=>self::USERID])->find();
            if(!empty($planInfo)){
                $backInfo = M('tapimedialist_backlog')->cache(true)->where(['fapimedialistid'=>$fid,'fstate'=>['IN',[0,1,3]]])->order('fcreatetime DESC')->find();
                // 媒体
                $mediaInfo = M('tmedia')
                    ->alias('t')
                    ->cache(true,120)
                    ->field('t.fid,t.fmedianame')
                    ->join('tapimedia_map AS tm ON tm.fmediaid = t.fid')
                    ->where(['tm.fid'=>$planInfo['fmediaid']])
                    ->find();
                $issuedate = date('Y-m-d',$planInfo['fissuedate']);
                $isDev = $_SERVER['HTTP_HOST'] == 'shanghai.hz-data.xyz' ? false : true;
                if (!$isDev) {
                    // 钉钉通知
                    $title = '已提交待检查';
                    $content = "> 友情提示：\n\n上海退回计划已完成提交：\n\n".$mediaInfo['fmedianame']." ".$issuedate."\n\n原因：".$backInfo['freason']."\n\n";
                    $smsphone = ['13858090079'];//提醒人员，沈露
                    $token = '3cc83737c35805d21c51028067c707418c64a8a257b7ed30bccef3ef5b295efc';
                    push_ddtask($title, $content, $smsphone, $token, 'markdown');
                }
                // 计划状态更新为录入完成
                M('tapimedialist')->where(['fid'=>$planInfo['fid']])->save(['fdstatus'=>0,'fmodifier'=>session('regulatorpersonInfo.fid'),'fmodifytime'=>date('Y-m-d H:i:s')]);
                // 退回记录状态更新为处理完成
                M('tapimedialist_backlog')->where(['fid'=>$backInfo['fid']])->save(['fstate'=>2,'fmodifier'=>session('regulatorpersonInfo.fid'),'fmodifytime'=>date('Y-m-d H:i:s')]);
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功']);
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'此计划当前不允许该操作']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }
    public function testDing(){
        // 钉钉通知
        $title = '(测试请忽略)已提交待检查';
        $content = "> 友情提示：\n\n上海退回计划已完成提交：\n\n";
        $smsphone = ['13799348980'];//提醒人员，沈露
        $token = '3cc83737c35805d21c51028067c707418c64a8a257b7ed30bccef3ef5b295efc';
        push_ddtask($title, $content, $smsphone, $token, 'markdown');
    }
}
