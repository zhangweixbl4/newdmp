<?php
namespace Admin\Controller;
use Think\Controller;
class ThirdmediaController extends Controller {
	
	/*
	* 第三方媒体列表
	* by zw
	*/
	public function media_list(){
		session_write_close();

		$p  = I('page', 1);//当前第几页ks
		$pp = 20;//每页显示多少记录
		$maincode = I('maincode')?I('maincode'):0;//用户主媒体编号
		$fmediacodename = I('fmediacodename');//媒体名称
		$fmediacode = I('fmediacode');//媒体编号
		$fstatus = I('fstatus');//媒体状态
		$fmediatype = I('fmediatype');//媒体类型
		if(!empty($maincode)){
			$pp = 1000;
		}

		if(!empty($fmediacodename)){
			$where_tmp['b.fmedianame|a.fmediacodename'] = ['like','%'.$fmediacodename.'%'];
		}
		if(!empty($fmediacode)){
			$where_tmp['a.fmediacode|a.fmediaid'] = ['like','%'.$fmediacode.'%'];
		}
		if($fstatus != ''){
			$where_tmp['a.fstatus'] = $fstatus;
		}
		if(!empty($fmediatype)){
			$where_tmp['a.fmediatype'] = $fmediatype;
		}

		$where_tmp['a.fmaincode'] = $maincode;

		$count = M('tapimedia_map')
			->alias('a')
			->join('tmedia b on a.fmediaid=b.fid','left')
			->where($where_tmp)
			->count();

		$do_tmp = M('tapimedia_map')
			->field('a.fid,a.fuserid,a.fstatus,a.fmediaid,a.fmediacode,a.fmediatype,a.fmediacodename,a.fthreshold,a.ftasklevel,b.fmedianame,c.child_fmediaid,c.cids,c.child_fmedianame')
			->alias('a')
			->join('tmedia b on a.fmediaid=b.fid','left')
			->join('(select group_concat(tapimedia_map.fmediaid) child_fmediaid,group_concat(tapimedia_map.fid) cids,group_concat(fmedianame) child_fmedianame,fmaincode,fmediatype from tapimedia_map,tmedia where tapimedia_map.fmediaid = tmedia.fid and fmaincode>0 group by fmaincode,fmediatype) c on a.fmediacode = c.fmaincode and a.fmediatype = c.fmediatype','left')
			->where($where_tmp)
			->order('a.fid desc')
			->page($p,$pp)
			->select();
		foreach ($do_tmp as $key => $value) {
			if(empty($value['child_fmediaid'])){
				$do_tmp[$key]['child_fmediaid'] = [];
				$do_tmp[$key]['cids'] = [];
				$do_tmp[$key]['child_fmedianame'] = [];
			}else{
				$do_tmp[$key]['child_fmediaid'] = explode(",",$value['child_fmediaid']);
				$do_tmp[$key]['cids'] = explode(",",$value['cids']);
				$do_tmp[$key]['child_fmedianame'] = explode(",",$value['child_fmedianame']);
			}
		}

	    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tmp)));
	}

	/*
	* 第三方媒体添加修改
	* by zw
	*/
	public function addedit_media(){
		$fid = I('fid');//媒体主键，修改时必传
		$fuserid = I('fuserid');//用户编号
		$fmediaid = I('fmediaid');//媒体编号
		$fmediatype = I('fmediatype');//媒体类型
		$fmediacode = I('fmediacode');//用户媒体编号
		$fmediacodename = I('fmediacodename');//用户媒体名称
		$fthreshold = I('fthreshold');//阀值
		$ftasklevel = I('ftasklevel')?I('ftasklevel'):0;//任务等级
		$fstatus = I('fstatus')?I('fstatus'):1;//开启状态

		$mustfield = [
			['name'=>'fuserid','msg'=>'用户必填','types'=>'string'],
			['name'=>'fmediatype','msg'=>'媒体类型有误','types'=>'numsection','otherdata'=>['min'=>1]],
			['name'=>'fmediacode','msg'=>'用户媒体编号必填','types'=>'string'],
			['name'=>'fmediacodename','msg'=>'用户媒体名称必填','types'=>'string']
		];
		$msgdata = check_field($mustfield);
		if(!empty($msgdata['code'])){
			$this->ajaxReturn($msgdata);
		}

		$data_tmp['fuserid'] = $fuserid;
		$data_tmp['fmediaid'] = $fmediaid;
		$data_tmp['fmediatype'] = $fmediatype;
		$data_tmp['fmediacode'] = $fmediacode;
		$data_tmp['fmediacodename'] = $fmediacodename;
		$data_tmp['fthreshold'] = $fthreshold;
		$data_tmp['ftasklevel'] = $ftasklevel;
		$data_tmp['fmaincode'] = 0;
		$data_tmp['fstatus'] = $fstatus;
		
		$where_tmp['fmediacode'] = $fmediacode;
		$where_tmp['fmaincode'] = 0;
		if(!empty($fid)){
			$where_tmp['fid'] = ['neq',$fid];
		}
		$do_tmp = M('tapimedia_map') -> where($where_tmp) ->find();
		if(!empty($do_tmp)){
			$code = 1;
			$msg = '媒体已存在';
		}else{
			if(!empty($fid)){
				$data_tmp['fmodifier'] = session('personInfo.fname');
				$data_tmp['fmodifytime'] = date('Y-m-d H:i:s');
				$edit_tmp = M('tapimedia_map') -> where('fid = '.$fid) ->save($data_tmp);
				if(!empty($edit_tmp)){
					$code = 0;
					$msg = '修改成功';
				}else{
					$code = 1;
					$msg = '修改失败，信息未调整';
				}
			}else{
				$data_tmp['fcreator'] = session('personInfo.fname');
				$data_tmp['fcreatetime'] = date('Y-m-d H:i:s');
				$add_tmp = M('tapimedia_map') -> add($data_tmp);
				if(!empty($add_tmp)){
					$code = 0;
					$msg = '添加成功';
				}else{
					$code = 1;
					$msg = '添加失败，未知原因';
				}
			}
			
		}
		$this->ajaxReturn(compact('code', 'msg'));
	}

	/*
	* 第三方媒体子媒体添加修改
	* by zw
	*/
	public function addedit_childmedia(){
		$fid = I('fid');//主媒体主键ID
		$cid = I('cid');//辅媒体主键ID
		$fmediaid = I('fmediaid');//用户媒体编号

		$mustfield = [
			['name'=>'fid','msg'=>'主媒体ID必传','types'=>'int'],
			['name'=>'fmediaid','msg'=>'媒体编号必填','types'=>'string'],
		];
		$msgdata = check_field($mustfield);
		if(!empty($msgdata['code'])){
			$this->ajaxReturn($msgdata);
		}

		$do_ta = M('tmedia') -> where(['fid'=>$fmediaid]) ->find();
		if(empty($do_ta)){

			$this->ajaxReturn(['code'=>1,'msg'=>'平台媒体不存在']);
		}

		$do_tmp = M('tapimedia_map') ->where(['fid'=>$fid,'fmaincode'=>0]) ->find();
		if(empty($do_tmp)){
			$code = 1;
			$msg = '添加失败，主媒体不存在';
		}else{
			$data_tmp['fuserid'] = $do_tmp['fuserid'];
			$data_tmp['fmediaid'] = $fmediaid;
			$data_tmp['fmediatype'] = $do_tmp['fmediatype'];
			$data_tmp['fmediacode'] = $do_tmp['fmediacode'];
			$data_tmp['fmediacodename'] = $do_tmp['fmediacodename'];
			$data_tmp['fthreshold'] = 0;
			$data_tmp['ftasklevel'] = 0;
			$data_tmp['fmaincode'] = $do_tmp['fmediacode'];
			$data_tmp['fstatus'] = 1;
			$data_tmp['fcreator'] = session('personInfo.fname');
			$data_tmp['fcreatetime'] = date('Y-m-d H:i:s');

			$where_tmp2['fmaincode'] = $do_tmp['fmediacode'];
			$where_tmp2['fmediacode'] = $do_tmp['fmediacode'];
			$where_tmp2['fmediatype'] = $do_tmp['fmediatype'];
			$where_tmp2['fmediaid'] = $fmediaid;
			if(!empty($cid)){
				$where_tmp2['fid'] = ['neq',$cid];
			}
			$do_tmp2 = M('tapimedia_map') -> where($where_tmp2) ->find();
			if(!empty($do_tmp2)){
				$code = 1;
				$msg = '辅媒体已存在';
			}else{
				if(!empty($cid)){
					$edit_tmp = M('tapimedia_map') -> where('fid = '.$cid) ->save($data_tmp);
					if(!empty($edit_tmp)){
						$code = 0;
						$msg = '修改成功';
					}else{
						$code = 1;
						$msg = '修改失败，信息未调整';
					}
				}else{
					$add_tmp = M('tapimedia_map') -> add($data_tmp);
					if(!empty($add_tmp)){
						$code = 0;
						$msg = '添加成功';
					}else{
						$code = 1;
						$msg = '添加失败，未知原因';
					}
				}
				
			}
		}
		$this->ajaxReturn(compact('code', 'msg'));
	}

	/*
	* 删除第三方媒体
	* by zw
	*/
	public function delete_media(){
		$fid = I('fid');//媒体主键
		$do_tmp = M('tapimedia_map')->where(['fid'=>$fid])->find();
		if(!empty($do_tmp)){
			if($do_tmp['fmaincode'] == 0){
				M('tapimedia_map')->where(['fmaincode'=>$do_tmp['fmediacode']])->delete();
			}
			$del_tmp = M('tapimedia_map')->where(['fid'=>$fid])->delete();
			$code = 0;
			$msg = '删除成功';
		}else{
			$code = 1;
			$msg = '删除失败，记录存在';
		}
		
		$this->ajaxReturn(compact('code', 'msg'));
	}

	/*
	* 用户编号
	* by zw
	*/
	public function getuserlist(){
		$userdata['name'] = '上海用户';
		$userdata['userid'] = '22638a3131d0f0a7346b178fd29f939c';
		$data[] = $userdata;
		$code = 0;
		$msg = '获取成功';
		$this->ajaxReturn(compact('code', 'msg','data'));
	}

}