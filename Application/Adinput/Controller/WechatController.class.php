<?php
namespace Adinput\Controller;
use Think\Controller;
class WechatController extends Controller {
    public function index(){
		if(I('get.echostr') != ''){//判断是否微信服务器验证请求
			if($this->checkSignature() == true) echo I('get.echostr');//验证正确
		}else{
			$data = file_get_contents('php://input');//获取微信服务器请求消息
			
			$data = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);//把微信服务器请求消息转为对象
			$openid = strval($data->FromUserName);//获取粉丝openid

			
			if($data->MsgType == 'event'){//事件推送
				if($data->Event == 'subscribe' || $data->Event == 'SCAN'){//关注或扫描事件
					
					
					$getWxInfoUrl = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token='.wx_token().'&openid='.$openid.'&lang=zh_CN';
					$wxInfo = http($getWxInfoUrl);
					$wxInfo = json_decode($wxInfo,true);
					$wxInfo = json_encode($wxInfo);
					$wxInfo = json_decode($wxInfo,true);
					$wxInfo['nickname'] = wx_nickname_filter($wxInfo['nickname']);

					
					if(M('ad_input_user')->where(array('openid'=>$openid))->count() == 0){//判断数据库是否存在该openid
                        $wxInfo['alias'] = $wxInfo['nickname'];
                        M('ad_input_user')->add($wxInfo);//新增记录
					}else{
						M('ad_input_user')->where(array('openid'=>$openid))->save($wxInfo);//修改记录
					}
					
					$qr_sn = str_replace('qrscene_','',strval($data->EventKey));//获取二维码内容
					if(strstr($qr_sn,'confirm_login_')){
						A('Adinput/WeixinLogin','Model')->confirm_login(str_replace('confirm_login_','',$qr_sn),$openid);
					}
					
					//$this->auto_reply($openid,'关注_'.$qr_sn);
				}	
				
				if($data->Event == 'CLICK'){//菜单点击事件
					$content = strval($data->EventKey);//消息内容
					$this->auto_reply($openid,$content);
				}
				
				if($data->Event == 'unsubscribe'){//取消关注
					M('ad_input_user')->where(array('openid'=>$openid))->save(array('subscribe'=>0));
				}
				
				
				
			}
			
			if($data->MsgType == 'text'){//普通文字消息
				$content = strval($data->Content);//消息内容
				$this->auto_reply($openid,$content);
			}
			
			
			
			
			
		}
	}
	
	/*自动回复方法*/
	private function auto_reply($openid,$content){
		if($openid == '' || $content == '') return;
		
		$this->service_sms($openid,'text',$content);//发送消息
	}
	
	/*发送客服消息*/
	public function service_sms($touser,$msgtype,$content,$media_id = ''){ 

		$send_url="https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=".wx_token();//客服消息的url	
		$post_data='{
					"touser":"'.$touser.'",
					"msgtype":"'.$msgtype.'",
					"'.$msgtype.'":
						{
							"content":"'.$content.'",
							"media_id":"'.$media_id.'"
						}
					}';
		$re = http($send_url,$post_data,'POST'); 
		$re = json_decode($re,true);
		return $re['errcode'];//返回发送状态代码，0代表发送成功，45015代表超过时间限制或已经取消关注，发送失败
	}
	
	/*
	*验证微信服务器
	*/
	private function checkSignature(){
		$signature = $_GET["signature"];
		$timestamp = $_GET["timestamp"];
		$nonce = $_GET["nonce"];	
					
		$token = C('WX_TOKEN');
		$tmpArr = array($token, $timestamp, $nonce);
		sort($tmpArr, SORT_STRING);
		$tmpStr = implode( $tmpArr );
		$tmpStr = sha1( $tmpStr );
		
		if( $tmpStr == $signature ){
			return true;
		}else{
			return false;
		}
	}
	

}