<?php

	/**
	 * 发送HTTP请求方法，目前只支持CURL发送请求
	 * @param  string  $url    请求URL
	 * @param  array   $params 请求参数
	 * @param  string  $method 请求方法GET/POST
	 * @return array   $data   响应数据
	 */
	function http($url, $params = array(), $method = 'GET',$header = false,$CURLOPT_TIMEOUT = 5,$CURLOPT_HEADER = false){
		$msectime = msectime();
		$http_log = '';
		$http_log .= date('H:i:s').'	'.$url.'	';
		set_time_limit(600);
		$opts = array(
			CURLOPT_TIMEOUT        => $CURLOPT_TIMEOUT,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_SSL_VERIFYHOST => false,
		);
		switch(strtoupper($method)){
			case 'GET':
				$getQuerys = !empty($params) ? '?'. http_build_query($params) : '';
				$opts[CURLOPT_URL] = $url . $getQuerys;
				//var_dump($opts[CURLOPT_URL]);
				break;
			case 'POST':
				$opts[CURLOPT_URL] = $url;
				$opts[CURLOPT_POST] = 1;
				$opts[CURLOPT_POSTFIELDS] = $params;
				break;
			case "DELETE":
				$opts[CURLOPT_URL] = $url;
				$opts[CURLOPT_CUSTOMREQUEST] = 'DELETE';
                $opts[CURLOPT_POSTFIELDS] = $params;
                break;

		}
		/* 初始化并执行curl请求 */
		$ch = curl_init();
		// CURLOPT_HEADER启用时会将头文件的信息作为数据流输出。
		curl_setopt ( $ch, CURLOPT_HEADER, $CURLOPT_HEADER );// yuhou.wang

		if($header) curl_setopt ( $ch, CURLOPT_HTTPHEADER, $header );

		curl_setopt_array($ch, $opts);
		$data   = curl_exec($ch);
		$err    = curl_errno($ch);
		$errmsg = curl_error($ch);
		curl_close($ch);

		$http_log .= msectime() - $msectime;
		$http_log .= "\n";
		//file_put_contents('LOG/http_log'.date('Ymd').'.log',$http_log,FILE_APPEND);
		if ($err > 0) {
			return false;
		}else {
			return $data;
		}
	}

/**
* 模拟post请求
* by zw
*/
function http_post($url,$data_string){
    $ch = curl_init();
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		"Content-Type: application/json; charset=utf-8",
		"Content-Length: " . strlen($data_string))
	);
	ob_start();
	curl_exec($ch);
	$return_content = ob_get_contents();
	ob_end_clean();
	$return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	return array($return_code, $return_content);
}

/*
 * 判断网页地址是否能正常打开
 * */
function getHttpStatus($url) {
    $curl = curl_init();
    curl_setopt($curl,CURLOPT_URL,$url);
    curl_setopt($curl,CURLOPT_NOBODY,1);
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($curl,CURLOPT_TIMEOUT,5);
    curl_exec($curl);
    $re = curl_getinfo($curl,CURLINFO_HTTP_CODE);
    curl_close($curl);
    return  $re;
}


	/**
* 字符串加密、解密函数
* @param	string	$txt		字符串
* @param	string	$operation	ENCODE为加密，DECODE为解密，可选参数，默认为ENCODE，
* @param	string	$key		密钥：数字、字母、下划线
* @param	string	$expiry		过期时间
* @return	string
*/
function sys_auth($string, $operation = 'ENCODE', $key = '', $expiry = 0) {
	$ckey_length = 4;
	$key = md5($key != '' ? $key : "AUTH_KEY");
	$keya = md5(substr($key, 0, 16));
	$keyb = md5(substr($key, 16, 16));
	$keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length): substr(md5(microtime()), -$ckey_length)) : '';
	$cryptkey = $keya.md5($keya.$keyc);
	$key_length = strlen($cryptkey);
	$string = $operation == 'DECODE' ? base64_decode(strtr(substr($string, $ckey_length), '-_', '+/')) : sprintf('%010d', $expiry ? $expiry + time() : 0).substr(md5($string.$keyb), 0, 16).$string;
	$string_length = strlen($string);
	$result = '';
	$box = range(0, 255);
	$rndkey = array();
	for($i = 0; $i <= 255; $i++) {
		$rndkey[$i] = ord($cryptkey[$i % $key_length]);
	}
	for($j = $i = 0; $i < 256; $i++) {
		$j = ($j + $box[$i] + $rndkey[$i]) % 256;
		$tmp = $box[$i];
		$box[$i] = $box[$j];
		$box[$j] = $tmp;
	}
	for($a = $j = $i = 0; $i < $string_length; $i++) {
		$a = ($a + 1) % 256;
		$j = ($j + $box[$a]) % 256;
		$tmp = $box[$a];
		$box[$a] = $box[$j];
		$box[$j] = $tmp;
		$result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
	}
	if($operation == 'DECODE') {
		if((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26).$keyb), 0, 16)) {
			return substr($result, 26);
		} else {
			return '';
		}
	} else {
		return $keyc.rtrim(strtr(base64_encode($result), '+/', '-_'), '=');
	}
}


/*REDIS连接方法*/
function redis(){
	$redis = new \Redis();//实例化Redis

	$redis_host = C('REDIS_HOST');//使用配置地址


	if (!$redis->connect($redis_host,C('REDIS_PORT'),2)){

		return false; //连接REDIS服务器
	}
	if (!$redis->auth(C('REDIS_AUTH'))){
		return false; //REDIS密码
	}
	$redis->select(C('REDIS_DB'));//REDIS的DB
	return $redis;
}




/*列表序号*/
function list_k($list,$p=1,$pp=10){
	foreach($list as $k => $v){

		$list[$k] = $v;
		$list[$k]['k'] = ($p - 1) * $pp + $k + 1 ;  //（当前页-1）* 每页显示的条数 + 当前索引号 + 1
	}
	return $list;

}

/*form搜索表单需要添加的隐藏域*/

function form_hidden(){
	if(C('URL_MODEL') != 0) return;

	return '
		<input type="hidden" name="'.C('VAR_MODULE').'" value="'.MODULE_NAME.'">
		<input type="hidden" name="'.C('VAR_CONTROLLER').'" value="'.CONTROLLER_NAME.'">
		<input type="hidden" name="'.C('VAR_ACTION').'" value="'.ACTION_NAME.'">
	';
}





/**
 * 	随机字符串
 */
function createNoncestr( $length = 32 )
{
	$chars = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$str ="";
	for ( $i = 0; $i < $length; $i++ )  {
		$str.= substr($chars, mt_rand(0, strlen($chars)-1), 1);
	}
	return $str;
}


	/**
	 * 对秒数进行时间转换
	 * @param  [type] $seconds [description]
	 * @return [type]          [description]
	 */
	function changeTimeType($seconds){
	    if ($seconds > 3600){
	        $hours = intval($seconds/3600);
	        $minutes = $seconds % 3600;
	        $time = $hours."小时".gmstrftime('%M分%S秒', $minutes);
	    }elseif($seconds > 60 && $seconds <= 3600){
	        $time = gmstrftime('%M分%S秒', $seconds);
	    }elseif($seconds > 0 && $seconds <= 60){
	    	$time = gmstrftime('%S秒', $seconds);
	    }else{
	    	return false;
	    }
	    return $time;
	}



	 /**
     * 时间戳换取距离时间
	 *
     */
	function time_to_time($time){
		$time_difference = time() - $time;//时间差
		if ($time_difference < 60) return $time_difference.'秒前';
		if ($time_difference >= 60 && $time_difference < 3600) return round($time_difference / 60 , 0) .'分钟前';
		if ($time_difference >= 3600 && $time_difference < 86400) return round($time_difference / 3600 , 0) .'小时前';
		if ($time_difference >= 86400) return round( $time_difference / 86400 , 0) .'天前';
    }



/* 二位数组排序
 * $arrays 要排序的数组
 * $sort_key  排序依据列
 * $sort_order 排序类型[SORT_ASC or SORT_DESC]
 * $sort_type 规定排序类型
 * return 排好序的数组
 * @author
 */
function my_sort($arrays,$sort_key,$sort_order=SORT_ASC,$sort_type=SORT_NUMERIC ){
	if(is_array($arrays)){
		foreach ($arrays as $array){
			if(is_array($array)){
				$key_arrays[] = $array[$sort_key];
			}else{
				return false;
			}
		}
	}else{
		return false;
	}

	array_multisort($key_arrays,$sort_order,$sort_type,$arrays);

	foreach($arrays as $array_arr) {
		$array_array[] = $array_arr;
	}
	return $array_array;
}

/*高亮突出显示*/
function r_highlight($name,$highlight,$color='#FF4040'){
	return str_replace($highlight,'<span style="color:'.$color.'">'.$highlight.'</span>',$name);

}


/*阿里云上传文件用到的方法*/
function al_oss(){
	$accessKeyId = C('AL_OSSID');
	$accessKeySecret = C('AL_OSSKEY');
	$endpoint = C('AL_OSSBUCKET_URL');

	$expire_time = time() + 3600;//过期时间，秒级时间戳
	$expiration =date('Y-m-d',$expire_time).'T'.date('H:i:s',$expire_time).'Z';//按阿里的格式组装过期时间
	$condition = array('content-length-range',0,1048576000);//最大文件大小.可以自己设置
	$conditions = array($condition);//组装数组
	$arr = array('expiration'=>$expiration,'conditions'=>$conditions);//组装数组
	$policy = json_encode($arr);//json编码
	$base64_policy = base64_encode($policy);//base64编码
	$signature = base64_encode(hash_hmac('sha1', $base64_policy, $accessKeySecret, true));//哈希加密

	$val = array();
	$val['OSSAccessKeyId'] = $accessKeyId;
	$val['policy'] = $base64_policy;
	$val['Signature'] = $signature;//上传签名
	$val['success_action_status'] = '201';//上传成功时返回的状态码

	$data = array();
	$data['val'] = $val;
	$data['up_server'] = $endpoint;
	$data['file'] = 'file';//指定传文件的name
	$data['bucket_url'] = C('AL_OSSBUCKET_URL');//云服务器的URL前缀
	$data['key'] = 'key';//指定文件名的字段name
	$data['createtime'] = time();//生成时间戳
	return $data;
}

/*判断是否启用https协议*/
function is_https() {
    if ($_SERVER['SERVER_PORT'] == 443) {
		return true;
    }
    return false;
}

/*七牛云上传文件用到的方法*/
function qn_oss(){
	$val = array();
	$val['token'] = A('Common/Qiniu','Model')->qiniu_updload_token();//七牛云的token
	$data = array();
	$data['val'] = $val;
	if(is_https()){
		$data['up_server'] = str_replace('http://','//',C('QINIU_UP_URL'));//云服务器的URL
	}else{
		$data['up_server'] = C('QINIU_UP_URL');//云服务器的URL
	}
	$data['file'] = 'file';//指定传文件的name
	$data['bucket_url'] = C('QINIU_ENDPOINT');
	$data['key'] = 'key';//指定文件名的字段name
	$data['createtime'] = time();//生成时间戳
	return $data;
}


/*文件上传方法*/
function file_up(){
	$memory_yun = strtoupper(C('MEMORY_YUN'));//获取存储云的配置
	if($memory_yun == 'ALI') return al_oss();//阿里云的上传方法
	if($memory_yun == 'QINIU') return qn_oss();//七牛云的上传方法

}

/*与视频剪辑平台的签名方法*/
function video_cutting_token($time){
	$token = md5(C('VIDEO_CUTTING_KEY').$time);//计算token
	return $token;

}


/* 描述换取转化后的时长
 * $second 需要转化的时间，单位秒
 * return 转化后的时间
 */
function s_to_m_time($second){
	if ($second < 60) return  $second.'秒';
	if ($second >= 60 and $second < 3600) return floor($second / 60) . '分' . $second % 60 . '秒';
	if ($second >= 3600) return floor($second / 3600) . '时' . floor( ($second % 3600) / 60 ) . '分' . ($second % 3600) % 60 . '秒';
}


/*mp4地址换取缩略图*/
function mp4_thumbnail($url,$s = 2){
	return $url.'?vframe/jpg/offset/'.$s.'/h/100';
}


// function word2html($wordname,$htmlname){
//    	$word = new COM("word.application") or die("Unable to instanciate Word");
//     $word->Visible = 1;
//    	$word->Documents->Open($wordname);
//     $word->Documents[1]->SaveAs($htmlname,8);
//     $word->Quit();
//     $word = null;
//  	unset($word);

// }
/*
 * sh
 */
function aa($message, $exit = false,$mark=false)
{
	if($exit==='script' || $exit==='s'){
		if(!$mark){
			$mark=date('Y-m-d H:i:s',time());
		}
		$json=json_encode($message);
		echo '<script>';
		echo 'var amsg='.$json.';';
		echo 'console.log("%c log-----'.$mark.'","color:red");';
		echo 'console.dir(amsg);';
		echo '</script>';
	}else{
		echo '<pre>';
		var_dump ( $message );
		echo '</pre>';
		if ($exit) {
			exit ();
		}
	}
}
/*
 * sh
 */
function alog($message,$file=null,$mark=false,$p= ''){
	if(!$mark){
		$mark=date('Y-m-d H:i:s');
	}
	$file = $file ? $file : 'log';
	createFolder('./Runtime/Logs');
	$log_filename='./Runtime/Logs/'.$file.'.log';

	$per = $p ? $p : 'log';
	file_put_contents($log_filename,PHP_EOL.$per.'-----'.$mark.PHP_EOL,FILE_APPEND);
	file_put_contents($log_filename,var_export($message,true).PHP_EOL,FILE_APPEND);
}
function checkOrMakeDir($path){
	if(file_exists($path) && is_dir($path)){
		mkdir($path, 0777);
		return true;
	}else{
		createFolder($path);
	}
}
function createFolder($path){
	if (!file_exists($path)){
		createFolder(dirname($path));
		mkdir($path, 0777);
	}
}
function apm($model) {
	return D($model);
}

function ap($model, $fun = 'index',$param=array()) {
	unset($param['where']);
	unset($param['field']);
	$m = apm($model);
	$res = $m->$fun($param);
	return  $res;
}

function finishTime($time){
	if($time >= 0){
        if($time < 60){
            return $time . '秒';
        }elseif($time < 3600){
            return intval($time / 60) . '分钟';
        }elseif($time < 86400){
            return intval($time / 3600) . '小时';
        }else{
            return intval($time / 86400) . '天';
        }
    }else{
        return $time;
    }
}

function pdf($html){
    //引入类库
    Vendor('mpdf.mpdf');
    //设置中文编码
    $mpdf = new \mPDF('zh-cn','A4', 0, '宋体', 20, 20);
    //html内容
    $html = $html;
    $mpdf->WriteHTML($html);
    $mpdf->Output('LOG/mpdf.pdf');
    return 'LOG/mpdf.pdf';
}

/**
 * HTML转PDF文件
 */
function pdfFromHtml($html){
    //引入类库
    Vendor('mpdf.mpdf');
    $mpdf=new \mPDF('zh-cn','A4',0,'宋体',15,15);
    $mpdf->useAdobeCJK = true;
    //	$mpdf->autoScriptToLang(AUTOFONT_ALL);
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->watermark_font = 'GB';
    $mpdf->SetWatermarkText('中国水印',0.1);
    $strContent = $html;
    //print_r($strContent);die;
    //$mpdf->showWatermarkText = true;
    //$mpdf->SetAutoFont();
    //$mpdf->SetHTMLHeader( '头部' );
    //$mpdf->SetHTMLFooter( '底部' );
    $mpdf->WriteHTML($strContent);
    $mpdf->Output('LOG/电子数据证据信息存管函.pdf');
    //$mpdf->Output('tmp.pdf',true);
    //$mpdf->Output('tmp.pdf','d');
    //$mpdf->Output();
    return 'LOG/电子数据证据信息存管函.pdf';
}

/**
 * @param $sample_id 样本 id
 * @param $media_type 媒体种类 tv , bc , paper
 * by hs
 */
function set_tillegalad_data($sample_id,$media_type) {
	$sample_model ='t'.$media_type.'sample';
	$issue_model ='t'.$media_type.'issue';
	if($media_type =='paper'){
		$sample= M($sample_model)->where(array('fpapersampleid' => $sample_id,'fstate' =>1))->find();//查找样本信息
	}else{
		$sample= M($sample_model)->where(array('fid' => $sample_id,'fstate' =>1))->find();//查找样本信息
	}
	if(isset($sample['fillegaltypecode']) && !empty($sample['fillegaltypecode'])){
		$data=M($issue_model)->field('*,min(fissuedate)as ffirstissuetime,max(fissuedate) as flastissuetime,count(fissuedate) as fissuetimes')->where(array('f'.$media_type.'sampleid' => $sample_id,'fstate' =>1))->group('fmediaid')->select();//播放记录，fbcsampleid,ftvsampleid
		foreach($data as $k=>$v){
			//是否又正在处理的违法信息
			$is_set=M('tillegalad')->where(array('fmediaid' =>$v['fmediaid'],'fsampleid' =>$sample_id,'fstate'=>array('in','0,1')))->find();
			if($is_set){
				/*有正在处理的，更新*/
				$arr['ffirstissuetime']=$v['ffirstissuetime'];//首次播出日期
				$arr['flastissuetime']=$v['flastissuetime'];//未次播出日期
				$arr['fissuetimes']=$v['fissuetimes'];//发布次数
				$res = M("tillegalad")->where(array('fillegaladid' => $is_set['fillegaladid']))->save($arr);
			}else {
				/*新增违法广告表*/
				$list['fmediaclassid'] = $media_type;//媒介类别ID
				$list['fmediaid'] = $v['fmediaid'];//媒介ID
				$list['fsampleid'] = $sample_id;//样本ID
				$list['fadid'] = $sample['fadid'];//广告ID
				$list['fversion'] = $sample['fversion'];//版本
				$list['fspokesman'] = $sample['fspokesman'];//代言人
				$list['fapprovalid'] = $sample['fapprovalid'];//审批号
				$list['fapprovalunit'] = $sample['fapprovalunit'];//审批单位
				$list['fadlen'] = $sample['fadlen'];//广告长度
				$list['fillegaltypecode'] = $sample['fillegaltypecode'];//违法类型代码
				$list['fillegalcontent'] = $sample['fillegalcontent'];//涉嫌违法内容
				$list['fexpressioncodes'] = $sample['fexpressioncodes'];//违法表现代码
				$list['fexpressions'] = $sample['fexpressions'];//违法表现
				$list['fconfirmations'] = $sample['fconfirmations'];//认定依据
				$list['fpunishments'] = $sample['fpunishments'];//处罚依据
				$list['fpunishmenttypes'] = $sample['fpunishmenttypes'];//处罚种类及幅度
				$list['ffirstissuetime'] = $v['ffirstissuetime'];//首次播出日期
				$list['flastissuetime'] = $v['flastissuetime'];//未次播出日期
				$list['fissuetimes'] = $v['fissuetimes'];//发布次数
				if ($media_type == 'peper') {
					$list['favifilename'] = $sample['fjpgfilename'];//图片文件名
				} else {
					$list['favifilename'] = $sample['favifilename'];//视频文件名
				}
				$list['tsource'] = $v['fsource'];//来源
				$list['fcreator'] =  $sample['fcreator'];//创建人
				$list['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
				$fdisposetimes=M('tillegalad')->where(array('fmediaid' =>$v['fmediaid'],'fsampleid' =>$sample_id,'fstate'=>2))->count();
				$list['fdisposetimes'] = $fdisposetimes;//处置次数
				$list['fdisposestyle'] = 0;//处置方式（0-待定，1-指导, 2 ,合并线索，3，复核，4，线索，5，不予处理，10，其他）
				$list['fstate'] = 0;//状态（0-待处理，1-正处理，2-已处理）
				$res = M("tillegalad")->add($list);
			}
		}
	}
}


/**
*  将数组转换为xml


*/
function arrayToXml($arr,$root = true){
	if($root){
		$xml .= "<xml>\n";
	}


	foreach ($arr as $key=>$val){
		if(is_array($val)){
			$xml.="<".$key.">\n	".arrayToXml($val,false)."</".$key.">\n";
		}else{
			$xml.="		<".$key.">".$val."</".$key.">\n";
		}
	}
	if($root){
		$xml.="	</xml>\n";
	}

	return $xml;

}


/*数组差异对比，返回第一个数组有，且与第二个数组不同值的第二个数组的元素*/
function arr_contrast($arr1,$arr2,$remove_arr){
	if(!is_array($remove_arr)){
		$remove_arr = explode(',',$remove_arr);
	}
	$ago_arr = array();//初始化要返回的数组
	$later_arr = array();//初始化要返回的数组
	foreach($arr1 as $key => $arr){//循环第一个数组
		if($arr != $arr2[$key]&& !in_array($key,$remove_arr)){//判断第二个数组的值是否与第一个数组相同，且不在例外元素里面
			$ago_arr[$key] = $arr2[$key];//修改前的值
			$later_arr[$key] = $arr1[$key];//修改后的值
		}

	}

	$ret = array();
	$ret['ago_arr'] = $ago_arr;
	$ret['later_arr'] = $later_arr;

	return $ret;



}


//返回当前的毫秒时间戳
function msectime() {
   list($msec, $sec) = explode(' ', microtime());
   $msectime =  (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
   return $msectime;
}




/*返回某一周的开始时间和结束时间*/
function weekday($year,$week=1){

	$year = strval($year);//转为字符串
	$week = intval($week);//转为数字
	if(intval($year) < 2001 || intval($year) > 3000) return false;//判断传入的年是否合法
	if($week < 1) $week = 1;//如果传入的周数小于1就让它等于1


	$week_1 = date('w',strtotime($year.'-01-01'));//一月一号是周几
	if($week_1 === '0') $week_1 = 7;

	$week_1_d_last = strtotime($year.'-01-01')+(7-$week_1)*86400;//第一个周末是几号（秒级时间戳）


	$start = ($week_1_d_last - 6*86400 ) + (($week - 1) * 86400 * 7);//开始的日期

	if($start < strtotime($year.'-01-01')) $start = strtotime($year.'-01-01');//如果小于第一天那么就赋值第一天
	if($start > strtotime($year.'-12-31')) return false;//如果开始日期大于本年，那么一定是传入的周数太大，直接返回false


	$end = $week_1_d_last + (($week - 1) * 86400 * 7);//结束的日期

	if($end > strtotime($year.'-12-31')) $end = strtotime($year.'-12-31');//如果小于第一天那么就赋值第一天
	$end += 86399;


	return array(
					'start'=>$start,
					'end'=>$end
					);
}

/*返回某个时间是哪一年的第几周*/
function dayweek($time){
	$year = date('Y',$time);
	if(intval($year) < 2001 || intval($year) > 3000) return false;//判断传入的年是否合法
	$date_1_week = date('w',strtotime($year.'-01-01'));//一月一号是周几
	if($date_1_week === '0') $date_1_week = 7;



	if(intval($date_1_week) > 1) $compensate = 1;

	$D_value = $time - (strtotime($year.'-01-01'));
	$D_value = $D_value + (($date_1_week - 1)*86400);



	$week = intval(floor(($D_value / 86400)/7)+1);



	return array(
					'year'=>$year,
					'weeknum'=>$week
					);
}



/*16进制转2进制*/
function ConvertHexToText($Hex){
	$HexLength = strlen($Hex);
	if($HexLength <= 0) return(false);
	if($HexLength % 2 != 0) return(false);
	$ConvertArray = array();
	for($i = 0;$i < $HexLength; $i = $i + 2){
		$ConvertResult = substr($Hex, $i, 2);
		$ConvertResult = hexdec($ConvertResult);
		$ConvertResult = chr($ConvertResult);
		$ConvertArray[$i] = $ConvertResult;
	}
	$ConvertResult = implode("", $ConvertArray);
	return($ConvertResult);
}


/*2进制转16进制*/
function ConvertTextToHex($Text){
	$TextLength = strlen($Text);
	if($TextLength <= 0) return(false);
	$ConvertArray = array();
	for($i = 0;$i < $TextLength; $i++){
		$ConvertResult = substr($Text, $i, 1);
		$ConvertResult = ord($ConvertResult);
		$ConvertResult = dechex($ConvertResult);
		$ResultLength = strlen($ConvertResult);
		if($ResultLength < 2) $ConvertResult = str_repeat("0", 2 - $ResultLength) . $ConvertResult;
		else if($ResultLength > 2) $ConvertResult = substr($ConvertResult, 0, 2);
		//$ConvertResult = strtoupper($ConvertResult);
		$ConvertArray[$i] = $ConvertResult;
	}
	$ConvertResult = implode("", $ConvertArray);
	return($ConvertResult);
}


	/*获取微信openID*/
	function get_openid(){
		$wx_openid = session('wx_openid');
		$wx_id = session('wx_id');
		//var_dump($wx_openid);
		//exit;
		if (($wx_openid == '' || $wx_id == '') && strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false){

			$url = U('Adinput/Getwx/wx_openid');
			cookie('back_url',$_SERVER['REQUEST_URI']);
			//var_dump($url);
			header("location:{$url}");//跳转的微信请求授权
			exit;
		}
		return $wx_openid;
	}

	/*获取用户信息*/
	function get_user_info(){
		return M('ad_input_user')->where(array('openid'=>get_openid()))->find();
	}


	/*获取微信接口调用token*/
	function wx_token(){

		$wx_token_info = A('Common/System','Model')->important_data('jjz_wx_token');
		$wx_token_info = json_decode($wx_token_info,true);
		$wx_token = $wx_token_info['access_token'];//获取微信token
		if (!$wx_token_info || $wx_token_info['expire_time'] < time()){
			$appid = C('WX_APPID');
			$secret = C('WX_APPSECRET');
			$token_url= "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$appid}&secret={$secret}";//向微信请求凭证
			$json=http($token_url);	//返回的json数据
			$result=json_decode($json,true);	//对JSON 格式的字符串进行解码
			$wx_token = $result['access_token'];//token
			$expire_time = time() + $result['expires_in'] - 10;
			A('Common/System','Model')->important_data('jjz_wx_token',json_encode(array('access_token'=>$wx_token,'expire_time'=>$expire_time)));


		}
		//var_dump($wx_token);
		return $wx_token;//取得的凭证
	}

	/**
	* $str  微信昵称
	**/
    function wx_nickname_filter($str) {
        if($str){
            $name = $str;
            $name = preg_replace('/\xEE[\x80-\xBF][\x80-\xBF]|\xEF[\x81-\x83][\x80-\xBF]/', '', $name);
            $name = preg_replace('/xE0[x80-x9F][x80-xBF]‘.‘|xED[xA0-xBF][x80-xBF]/S','?', $name);
            $return = json_decode(preg_replace("#(\\\ud[0-9a-f]{3})#ie","",json_encode($name)));

        }else{
            $return = '';
        }
        return $return;

	}



	/**
	 * 锁
	 * @param  string  $lock_key    锁的名称
	 * @param  string  $type 锁类型，true:加锁，null:解锁
	 * @param  string  $expire 锁过期时间，单位秒
	 * @return bool   	   响应数据
	 */
	function lock($lock_key = '',$type = true,$expire = 8){
		$lock_key = strval($lock_key);//锁名称

		if(!$lock_key) return false;//锁名称不合规

		if($type === true){//加锁
			$lock = false;//锁状态未取得
			while(!$lock){
				$lock = redis()->setNX($lock_key , time());
				if(!$lock) usleep(100000);//如果没有获取到锁 休息0.1秒
			}
			redis()->expire($lock_key,  $expire);//设置锁时间
			return $lock;
		}


		if($type === null){//释放锁
			$r_lock = redis()->delete($lock_key);//释放锁
			if($r_lock){
				return true;
			}else{
				return false;
			}

		}


	}


	/*获取系统自定义配置*/
	function sys_c($fkey){
		return M('sys_config')->cache(true,60)->where(array('fkey'=>$fkey))->getField('fvalue');

	}

	/*
	* 推送钉钉提醒
	* $title 标题 string, $content 发送内容 string ,$phone 手机号 array, $token 密钥 string
	* $types 模板类型 string
	* by zw
	*/
	function push_ddtask($title,$content,$phone,$token,$types = 'title'){
		if(in_array($_SERVER['HTTP_HOST'],C('DOMAIN_LIST'))){//开发环境不作提醒
			return true;
		}

		foreach ($phone as $value) {
            $smsstr .= "@".$value."  ";
        }
		$msgPost = array();
	    $msgPost['msgtype'] = $types;
	    if($types == 'title'){
	    	$msgPost['title'] = $title;
	    	$msgPost['text']['content'] = $content.$smsstr;
	    }elseif($types  == 'markdown'){
	    	$msgPost['markdown']['title'] = $title;
	    	$msgPost['markdown']['text'] = $content.$smsstr;
	    }else{
	    	return false;
	    }
	    $msgPost['at']['atMobiles'] = $phone;
		$ret = http('https://oapi.dingtalk.com/robot/send?access_token='.$token,json_encode($msgPost),'POST',array('Content-Type:application/json; charset=utf-8'));
		return $ret;
	}

	/**
    * php实现下载远程图片保存到本地
    **
    * $url 图片所在地址
    * $path 保存图片的路径
    * $fileName 图片自定义命名
    * $type 使用什么方式下载
    * 0:curl方式,1:readfile方式,2file_get_contents方式
    * return 文件名
    */
    function getFile($url,$path='',$fileName='',$type=0){
        if($url==''){return false;}
        //获取远程文件数据
        if($type===0){
            $ch=curl_init();
            $timeout=60;
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);//最长执行时间
            curl_setopt($ch,CURLOPT_TIMEOUT,$timeout);//最长等待时间

            $img=curl_exec($ch);
            curl_close($ch);
        }
        if($type===1){
            ob_start();
            readfile($url);
            $img=ob_get_contents();
            ob_end_clean();
        }
        if($type===2){
            $img=file_get_contents($url);
        }
        //判断下载的数据 是否为空 下载超时问题
        if(empty($img)){
            // throw new \Exception("下载错误,无法获取下载文件！");
            return false;
        }

        //没有指定路径则默认当前路径
        if($path===''){
            $path="./";
        }
        //如果命名为空
        if($fileName===""){
            $fileName=md5($url);
        }
        //获取后缀名
        $ext=substr($url, strrpos($url, '.'));
        if($ext && strlen($ext)<5){
            $fileName .= $ext;
        }else{
            $fileName .= '.jpg';
        }

        //防止"/"没有添加
        $path=rtrim($path,"/")."/";
        if(!is_file($path.$fileName)){
        	$fp2=@fopen($path.$fileName,'a');
	        fwrite($fp2,$img);
	        fclose($fp2);
        }

        $fileSize = filesize($path.$fileName);//文件大小
        $data['fileName'] = $fileName;
        $data['filePath'] = $path.$fileName;
        $data['fileSize'] = $fileSize;
        return $data;
    }

    /**
     * 按照指定的尺寸压缩图片成JPG格式
     * @param $source_path  原图路径
     * @param $target_path  保存路径
     * @param $imgWidth     目标宽度
     * @param $imgHeight    目标高度
     * @return bool|string
     */
    function resize_image($source_path,$target_path,$imgWidth = 0,$imgHeight = 0) {
    	ini_set('memory_limit','2048M');

        $source_info = getimagesize($source_path);
        $source_mime = $source_info['mime'];
        switch ($source_mime)
        {
            case 'image/gif':
                $source_image = imagecreatefromgif($source_path);
                break;

            case 'image/jpeg':
                $source_image = imagecreatefromjpeg($source_path);
                break;

            case 'image/png':
                $source_image = imagecreatefrompng($source_path);
                break;

            default:
                return false;
                break;
        }
        if(empty($imgWidth) && !empty($imgHeight)){
            $imgWidth = $imgHeight * (int)$source_info[0] / (int)$source_info[1];
        }elseif(!empty($imgWidth) && empty($imgHeight)){
            $imgHeight = $imgWidth * (int)$source_info[1] / (int)$source_info[0];
        }elseif(empty($imgWidth) && empty($imgHeight)){
            $imgWidth = $source_info[0];
            $imgHeight = $source_info[1];
        }
        //如果图片超宽
        if($imgWidth>65000){
            $imgWidth = 65000;
            $imgHeight = $imgWidth * (int)$source_info[1] / (int)$source_info[0];
        }
        //如果图片超高
        if($imgWidth>65000){
            $imgHeight = 65000;
            $imgWidth = $imgHeight * (int)$source_info[0] / (int)$source_info[1];
        }

        $target_image     = imagecreatetruecolor($imgWidth, $imgHeight); //创建一个彩色的底图
        imagecopyresampled($target_image, $source_image, 0, 0, 0, 0, $imgWidth, $imgHeight, $source_info[0], $source_info[1]);
        //保存图片到本地

        if (!is_dir($target_path)) {
            mkdir($target_path, 0777);
        }

        $fileName = date("YmdHis").uniqid().'.jpg';
        if(imagejpeg($target_image,'./'.$target_path.$fileName)){
            $fileSize = filesize($target_path.$fileName);
        }
        $data['fileName'] = $fileName;
        $data['filePath'] = $target_path.$fileName;
        $data['fileSize'] = $fileSize;
        imagedestroy($target_image);
        return $data;
    }

    /**
     * 判断是时间是否有交叉，true有交叉，false无交叉
     * by zw
     */
    function check_doubletime($sttime1,$endtime1,$sttime2,$endtime2) {
		if(($sttime1-$sttime2)>2 && ($endtime2-$sttime1)>2 || ($sttime2-$sttime1)>2 && ($endtime1-$sttime2)>2){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 判断数组维数 TODO：暂只支持一维与多维，待扩展为返回准确维数
	 * @param Array $arr 目标数组
	 * @return Int|Boolean 一维返回1,多维返回2,错误返回0;
	 */
	function countArrDimensions($arr = []){
		if(!empty($arr)){
			if(count($arr) == count($arr,1)){
				return 1;
			}else{
				return 2;
			}
		}
		return 0;
	}

	/**
	 * 获取客户端IP地址
	 * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
	 * @param boolean $adv 是否进行高级模式获取（有可能被伪装）
	 * @return mixed
	 */
	function getClientIp($type = 0,$adv = false) {
		$type = $type ? 1 : 0;
		static $ip = NULL;
		if ($ip !== NULL) return $ip[$type];
		if($adv){
			if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
				$pos = array_search('unknown',$arr);
				if(false !== $pos) unset($arr[$pos]);
				$ip = trim($arr[0]);
			}elseif(isset($_SERVER['HTTP_CLIENT_IP'])) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			}elseif(isset($_SERVER['REMOTE_ADDR'])) {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
		}elseif(isset($_SERVER['REMOTE_ADDR'])) {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		// IP地址合法验证
		$long = sprintf("%u",ip2long($ip));
		$ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
		return $ip[$type];
	}

    /**
     * 根据媒体类型返回任务表名
     */
    function getTableNameByMediaClass($fmediaclass){
        $tablename = '';
        switch((string)$fmediaclass){
            case '1':
            case '01':
            case '2':
            case '02':
            case '3':
            case '03':
                $tablename = 'task_input';
                break;
            case '5':
            case '05':
                $tablename = 'todtask';
                break;
            case '13':
                $tablename = 'tnettask';
                break;
            default:
                break;
        }
        return $tablename;
    }
	

    /**
     * 判断媒体日期数据是否就绪
     * @param   Int $fmediaid 媒体ID
     * @param   Int $fissuedate 日期时间戳
     * @return  Int $isAvailable 1-就绪 0-未就绪
     */
    function isMediaDateAvailable($fmediaid = 0,$fissuedate = 0){
        $isAvailable = 0;
        if($fmediaid && $fissuedate){
            $avaTimeStamp = A('Common/Media','Model')->available_time($fmediaid);
            $isAvailable = ($fissuedate <= $avaTimeStamp) ? 1 : 0;
        }
        return $isAvailable;
    }

    /**
     * 根据媒介获取可检查日期
     */
    function getAvaDateByMedia($fmediaid = 0,$fissuedate = ''){
        if($fmediaid){
            $avaTimeStamp = A('Common/Media','Model')->available_time($fmediaid,$fissuedate);
            return date('Y-m-d',$avaTimeStamp);
        }else{
        	return false;
        }
    }

    /**
     * 根据媒介获取可检查日期
     */
    function getAvaDateByMedia2($fmediaid = 0,$fissuedate = ''){
        $gourl = 'http://'.C('JXServerUrl').'/day_done/'.$fmediaid.'/'.$fissuedate.'/';
        $data = json_decode(http($gourl,[],'GET',false,0),true);
        if($data['data'] == 'ok'){
        	return true;
        }else{
        	return false;
        }
    }

    /**
     * 获取媒体数据的就绪状态
     */
    function getMediaDataStatus(){
    	$gourl = 'http://'.C('JXServerUrl').'/all_day_sync/?nsukey=RMMInTvZur7hMPk3Mk1ecJOCN9wZvQrTbvTJTabqO4VnV7VXF8Zjzf2p9ik%2B6KeC2MO6sWR3ku8UIjJY%2BN5WUEhnt5WWSZU%2BS7GGNBa%2BrxabukUguuQPo0koyYlBce%2FHbSNl7Qo8ZN7V64vh23rNbnHTMRWW5BBVEXWc9i%2Fc9ppQnkE%2FFasq9qVYn6fRnZlnG%2F0ySnPI4k5NlfFNIrga0g%3D%3D';
        $data = json_decode(http($gourl,[],'GET',false,0),true);
        $finishMedia = [];
        if(empty($data['ret'])){
        	foreach ($data['data'] as $key => $value) {
        		if($value['status'] == 4){
        			$finishMedia[$value['channel']][] = $value['day'];
        		}
        	}
        }
        return $finishMedia;
    }

	/**
	 * Notes: 判断IP在不在IP网段内
	 * Date: 2019/6/18
	 * Time: 15:22
	 * @param $ip 要查询的IP地址
	 * @param $network IP段  例： 192.168.1.1/24
	 * @return bool
	 */
	function ip_in_network($ip, $network){

		
		$ip = (double) (sprintf("%u", ip2long($ip)));

		$s = explode('/', $network);
		if(count($s) == 1) $s[1] = '32';
		$network_start = (double) (sprintf("%u", ip2long($s[0])));
		#if((32 - $s[1]) < 0) return false;
		$network_len = pow(2, 32 - $s[1]);
		$network_end = $network_start + $network_len - 1;
	 
		if ($ip >= $network_start && $ip <= $network_end) {
			return true;
		}
		return false;
	}

	/**
    * RSA数据加密解密
    * by zw
    * @param type $type encode加密  decode解密
    */
    function RAS_openssl($data,$type='encode'){
        if (empty($data)) {
            return 'data参数不能为空';
        }
        $RSA_PRIVATE = C('private_key');
        $RSA_PUBLIC = C('public_key');

        //私钥解密
        if ($type=='decode') {
            $private_key = openssl_pkey_get_private($RSA_PRIVATE);
            if (!$private_key) {
                return('私钥不可用');
            }
            $return_de = openssl_private_decrypt(base64_decode($data), $decrypted, $private_key);
            if (!$return_de) {
                return('解密失败,请检查RSA秘钥');
            }
            return $decrypted;
        }

        //公钥加密
        $key = openssl_pkey_get_public($RSA_PUBLIC);
        if (!$key) {
            return('公钥不可用');
        }
        $return_en = openssl_public_encrypt($data, $crypted, $key);
        if (!$return_en) {
            return('加密失败,请检查RSA秘钥');
        }
        return base64_encode($crypted);
	}
	/**
	 * @Des: 删除指定文件夹以及文件夹下的所有文件
	 * @Edt: yuhou.wang
	 * @Date: 2019-11-01 17:40:19
	 * @param {type} 
	 * @return: 
	 */	
	function deldir($dir) {
		//先删除目录下的文件：
		$dh=opendir($dir);
		while ($file=readdir($dh)) {
			if($file!="." && $file!="..") {
				$fullpath=$dir."/".$file;
				if(!is_dir($fullpath)) {
					unlink($fullpath);
				} else {
					deldir($fullpath);
				}
			}
		}
		closedir($dh);
		//删除当前文件夹：
		if(rmdir($dir)){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 获取excel列的字母
	 * by zw
	 */
	function getXlsNum($num = 65){
		$a = floor(($num-65)/26);
		$b = ($num-65)%26;
		$ENNum = chr(65+$b);
		if(!empty($a)){
			$ENNum = chr(64+$a).$ENNum;
		}
		return $ENNum;
	}

	/**
	 * 信息返回方式，0 json方式（用于接口请求的返回），1 array方式（用于内部方法调用的返回）
	 * by zw
	 */
	function rtMsg($ret = [], $retType = 0){
		if(!empty($retType)){
			return $ret;
		}else{
			exit(json_encode($ret,0));
		}
	}

	/*
	 * 数据排序
	 * by zw
	 * data 需要排序的数据数组，score_rule 用来排序的字段，is_desc排序方式（true时以降序形式），language数据编码方式（一般中文的排序用cn，其他用en）
	 * */
	function pxsf($data = [],$score_rule = 'com_score',$is_desc = true,$language = 'en'){
	    $data_count = count($data);
	    if($language == 'cn'){
	        $fData = array_column($data, $score_rule);
	        $fData = eval('return '.mb_convert_encoding(var_export($fData,true), "gbk","utf-8").";");
	        if($is_desc){
	            arsort($fData);
	        }else{
	            asort($fData);
	        }
	        $fData = eval('return '.mb_convert_encoding(var_export($fData,true), "utf-8", "gbk").";");
	        foreach ($fData as $key => $value) {
	            $ret[] = $data[$key];
	        }
	        return $ret;
	    }else{
	        for($k=0;$k<=$data_count;$k++) {
	            for($j=$data_count-1;$j>$k;$j--){
	                $orderdata1 = $data[$j][$score_rule];
	                $orderdata2 = $data[$j-1][$score_rule];
	                if($is_desc){
	                    if($orderdata1>$orderdata2){
	                        $temp = $data[$j];
	                        $data[$j] = $data[$j-1];
	                        $data[$j-1] = $temp;
	                    }
	                }else{
	                    if($orderdata1<$orderdata2){
	                        $temp = $data[$j];
	                        $data[$j] = $data[$j-1];
	                        $data[$j-1] = $temp;
	                    }
	                }

	            }
	        }
	        return $data;
	    }
	}

	/**
     * @Des: 访问中台微服务
     * @param {type} 
     * @return: 
     */
    function accessService($action='',$params=false,$type='POST',$access_key='431e6c0019c2503bb912dcb81471fd7e',$secret_key='94bd9635d1965998a8352fe57501cc38',$app_code='36ba362c-6763-9120-2be9-5873a943a0c3'){
        $url = C('api_server').$action;
        $httpInfo = [];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Data');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        
        $msectime = time()*1000;
        $token = md5($access_key.$secret_key.$msectime);
        $headers = [
            'Content-type: application/json',
            'accessKey:'.$access_key,
            'appCode:'.$app_code,
            'timestamp:'.$msectime,
            'token:'.$token
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        //判断前端请求类型
        switch (mb_strtolower($type)){
            case "get" :
                curl_setopt($ch, CURLOPT_HTTPGET, true);
                break;
            case "post":
                curl_setopt($ch, CURLOPT_POST,true);
                curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($params));
                break;
            case "put" :
                curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($params));
                break;
            case "delete":
                curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($params));
                break;
        }
        $response = curl_exec($ch);
        if ($response === FALSE) {
            return false;
        }
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $httpInfo = array_merge($httpInfo, curl_getinfo($ch));
        curl_close($ch);
        $response = json_decode($response,true);
        $result = [];
        $result['httpCode'] = $httpCode;
        $result['info'] = $response;
        return $response;
    }

    /**
     * 两个时间段是否有包含、交叉关系
     * by zw
     * @param int $beginTime1 开始时间1
     * @param int $endTime1 结束时间1
     * @param int $beginTime2 开始时间2
     * @param int $endTime2 结束时间2
     * @return bool
     */
    function isTimeContain($beginTime1=0, $endTime1=0, $beginTime2=0, $endTime2=0) {
        $status = $beginTime2 - $beginTime1;
        if($status > 0){
            $status3 = $endTime1 - $beginTime2;
            if($status3 > 0){//至少交叉
            	$status2 = $endTime2 - $endTime1;
            	if($status2 > 0){
	                return 1; // 交叉
	            }else{
	                return 2; // 被包含
	            }
            }else{
            	return 0;//无交集
            }
        }elseif($status < 0){
            $status3 = $endTime2 - $beginTime1;
            if($status3 > 0){//至少交叉
            	$status2 = $endTime1 - $endTime2;
            	if($status2 > 0){
	                return 1; // 交叉
	            }else{
	                return 2; // 被包含
	            }
            }else{
            	return 0;//无交集
            }
        }else{
        	$status3 = $endTime2 - $endTime1;
        	if($status3 == 0){
        		return 3;//相同
        	}else{
        		return 2;//包含
        	}
        }
    }

    /**
     * 两个时间段的重叠部分
     * by zw
     * @param int $beginTime1 开始时间1
     * @param int $endTime1 结束时间1
     * @param int $beginTime2 开始时间2
     * @param int $endTime2 结束时间2
     * @return bool
     */
    function timeContain($beginTime1=0, $endTime1=0, $beginTime2=0, $endTime2=0) {

        $status = $beginTime2 - $beginTime1;
        if($status > 0){
            $status3 = $endTime1 - $beginTime2;
            if($status3 > 0){//至少交叉
                $status2 = $endTime2 - $endTime1;
                if($status2 > 0){
                    // 交叉
                    $ret['st'] = $beginTime2;
                    $ret['ed'] = $endTime1;
                    $ret['use'] = 1;
                    $ret['maxnum'] = 2;//第二个数据大
                }else{
                    // 被包含
                    $ret['st'] = $beginTime2;
                    $ret['ed'] = $endTime2;
                    $ret['use'] = 2;
                    $ret['maxnum'] = 1;//第二个数据大
                }
                $ret['length'] = $ret['ed'] - $ret['st'];
            }else{
                //无交集
                $ret['st'] = 0;
                $ret['ed'] = 0;
                $ret['use'] = 0;
                $ret['maxnum'] = 0;
            }
        }elseif($status < 0){
            $status3 = $endTime2 - $beginTime1;
            if($status3 > 0){//至少交叉
                $status2 = $endTime1 - $endTime2;
                if($status2 > 0){
                    // 交叉
                    $ret['st'] = $beginTime1;
                    $ret['ed'] = $endTime2;
                    $ret['use'] = 1;
                    $ret['maxnum'] = 1;//第一个数据大
                }else{
                    // 被包含
                    $ret['st'] = $beginTime1;
                    $ret['ed'] = $endTime1;
                    $ret['use'] = 2;
                    $ret['maxnum'] = 2;//第一个数据大
                }
                $ret['length'] = $ret['ed'] - $ret['st'];
            }else{
                //无交集
                $ret['st'] = 0;
                $ret['ed'] = 0;
                $ret['use'] = 0;
                $ret['maxnum'] = 0;
            }
        }else{
            $status3 = $endTime2 - $endTime1;
            if($status3 == 0){
                // 相同
                $ret['st'] = $beginTime1;
                $ret['ed'] = $endTime1;
                $ret['use'] = 3;
                $ret['maxnum'] = 0;//一样大
            }elseif($status3>0){
                // 包含
                $ret['st'] = $beginTime1;
                $ret['ed'] = $endTime1;
                $ret['use'] = 2;
                $ret['maxnum'] = 2;//第二个数据大
            }else{
                // 包含
                $ret['st'] = $beginTime2;
                $ret['ed'] = $endTime2;
                $ret['use'] = 2;
                $ret['maxnum'] = 1;//第一个数据大
            }
            $ret['length'] = $ret['ed'] - $ret['st'];
        }
        return $ret;
    }

    /**
	 * 相同值判断
	 * by zw
	 */
	function checkEditData($data1,$data2,$keyname1 = [],$keyname2 = [],$retname = []){
		$keyname2 = $keyname2?$keyname2:$keyname1;
		$res = [];//返回内容
		foreach ($keyname1 as $key => $value) {
			if($data1[$keyname1[$key]] != $data2[$keyname2[$key]] && isset($data2[$keyname2[$key]]) && isset($data1[$keyname1[$key]])){
				if(!empty($retname)){
					$res[$retname[$key]] = $data1[$keyname1[$key]].' >>调整为>> '.$data2[$keyname2[$key]];
				}else{
					$res[$value] = $data1[$keyname1[$key]].' >>调整为>> '.$data2[$keyname2[$key]];
				}
			}
		}
		return $res;
	}