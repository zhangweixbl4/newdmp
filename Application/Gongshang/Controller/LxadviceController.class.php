<?php
namespace Gongshang\Controller;
use Think\Controller;

/**
 * Class LxadviceController
 * @package Gongshang\Controller
 * 联席_建议
 */
class LxadviceController extends BaseController
{
	/*接收通告列表*/
	public function adviceList()
	{
		//todo 咨询列表
		$p = I('p', 1);//当前第几页
		$pp = 20;//每页显示多少记录
		$report_type = I('report_type');//1，已收   2 已发
		$is_read = I('is_read');//1,全部  2 已读 3 未读
		$keywords= I('keywords');//关键字
		$data=array();
		$where= array();
		if(isset($keywords) && !empty($keywords)){
			$where['advice_title'] = array('like', '%' . $keywords . '%');//关键词
		}
		if($report_type == 1){
			if($is_read==1) {
				$count = M('lx_advice_receive')
					->join('lx_advice on lx_advice.advice_id = lx_advice_receive.advice_receive_id', 'LEFT')//通知主表
					->where(array('advice_receive_gov_id' => session('regulatorpersonInfo.fregulatorid')))
					->where($where)
					->count();// 查询满足要求的总记录数

				$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数

				$data = M('lx_advice_receive')
					->field('lx_advice.*,lx_advice_receive.id as receive_id ')
					->join('lx_advice on lx_advice.advice_id = lx_advice_receive.advice_receive_id', 'LEFT')//通知主表
					->where(array('advice_receive_gov_id' => session('regulatorpersonInfo.fregulatorid')))
					->where($where)
					->limit($Page->firstRow . ',' . $Page->listRows)
					->select();//查询违法广告
			}
			if($is_read==2) {

				$receive_data = M('lx_advice_receive')
					->join('lx_advice on lx_advice.advice_id = lx_advice_receive.advice_receive_id', 'LEFT')//通知主表
					->where(array('advice_receive_gov_id'=>session('regulatorpersonInfo.fregulatorid')))
					->where($where)
					->select();//本部门的通告

				foreach($receive_data as $k=> $v){
					$arr_uid = explode(",",$v['advice_read_user_ids']);
					if(in_array(session('regulatorpersonInfo.fid'),$arr_uid)){
						$receive_id[]=$v['id']; //已读
					}
				}
				$count=count($receive_id);
				$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数
				if($count){
					$data = M('lx_advice_receive')
						->field('lx_advice.*,lx_advice_receive.id as receive_id ')
						->join('lx_advice on lx_advice.advice_id = lx_advice_receive.advice_receive_id', 'LEFT')//通知主表
						->where(array('advice_receive_gov_id' => session('regulatorpersonInfo.fregulatorid')))
						->where(array('id' => array('in', $receive_id)))
						->where($where)
						->limit($Page->firstRow . ',' . $Page->listRows)
						->select();//查询违法广告
				}

			}
			//todo
			if($is_read==3){
				$receive_data = M('lx_advice_receive')
					->join('lx_advice on lx_advice.advice_id = lx_advice_receive.advice_receive_id', 'LEFT')//通知主表
					->where(array('advice_receive_gov_id'=>session('regulatorpersonInfo.fregulatorid')))
					->where($where)
					->select();//本部门的通告

				foreach($receive_data as $k=> $v){
					$arr_uid = explode(",",$v['advice_read_user_ids']);
					if(!in_array(session('regulatorpersonInfo.fid'),$arr_uid)){
						$receive_id[]=$v['id']; //未读的id
					}
				}
				$count=count($receive_id);
				$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数
				if($count){
					$data = M('lx_advice_receive')
						->field('lx_advice.*,lx_advice_receive.id as receive_id ')
						->join('lx_advice on lx_advice.advice_id = lx_advice_receive.advice_receive_id', 'LEFT')//通知主表
						->where(array('advice_receive_gov_id' => session('regulatorpersonInfo.fregulatorid')))
						->where(array('id' => array('in', $receive_id)))
						->where($where)
						->limit($Page->firstRow . ',' . $Page->listRows)
						->select();//查询违法广告
				}
			}

		}elseif($report_type == 2){
			$count = M('lx_advice')
				->where(array('advice_gov_id' => session('regulatorpersonInfo.fregulatorid')))
				->where($where)
				->count();// 查询满足要求的总记录数

			$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数

			$data = M('lx_advice')
				->where(array('advice_gov_id' => session('regulatorpersonInfo.fregulatorid')))
				->where($where)
				->limit($Page->firstRow . ',' . $Page->listRows)
				->select();//查询违法广告
		}

		$this->assign('page', $Page->show());//分页输出
		$this->assign('data', $data); //列表信息
		$this->display('adviceList');
	}

	/*添加建议*/
	public function cadd()
	{
		//是工商局 or 联席成员
		if(session('regulatorpersonInfo.regulator_type')=='20'){
			$list =M('tjointmember')->alias('a')
				->field('
				a.fmemberid,
				tregulator.fname
				     ')
				->join('tregulator on tregulator.fid = a.fmemberid', 'LEFT')//通知主表
				->where(array('a.fmainid'=>session('regulatorpersonInfo.fregulatorid')))
				->select();
			if(!$list){
				$list=2;//没有对应的联系成员。
			}
		}elseif(session('regulatorpersonInfo.regulator_type')=='30'){
			$fmainid = M("tjointmember")->where(array('fmemberid'=>session('regulatorpersonInfo.fregulatorid')))->getField('fmainid');//联席对应的工商局
			if($fmainid){
				$list = M("tjointmember")->alias('a')
					->field('
				a.fmemberid,
				tregulator.fname
				     ')
					->join('tregulator on a.fmemberid=tregulator.fid', 'LEFT')//机构
					->where(array('fmainid'=>$fmainid,'fmemberid'=>array('neq',session('regulatorpersonInfo.fregulatorid'))))
					->select();//工商对应的除了自己的联席人员
				$regulator_data=$this->get_regulator_data($fmainid);//获取机构信息
				$list[]=array('fmemberid'=>$fmainid,'fname'=>$regulator_data['fname']); //把工商加进去

			}else{
				$list=1;//没有对应的联系成员。
			}

		}
		$this->assign('list',$list);//联席人员名单
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->display('adviceEdit');
	}

	/*添加建议AJAX*/
	public function ajax_addAdvice(){

		$model=M("lx_advice");
		$model->startTrans();//开启事务
		$receive_model=M("lx_advice_receive");
		$receive_model->startTrans();//开启事务
		$lx_uploads_file_model=M("lx_uploads_file");
		$lx_uploads_file_model->startTrans();//开启事务

		//1，添加通知
		$data['advice_content'] = I('advice_content');
		$data['advice_title'] = I('advice_title');
		$data['advice_gov_id'] = session('regulatorpersonInfo.fregulatorid');//通知发送部门id
		$data['advice_gov'] = session('regulatorpersonInfo.regulatorname');//通知发送部门
		$data['advice_post_time'] = date('Y-m-d H:i:s');
		$data['advice_user_id'] = session('regulatorpersonInfo.fid');///建议发送人ID
		$data['advice_username'] = session('regulatorpersonInfo.fname');///建议发送人
		$arr_memberid=I('fmemberid');//选择发送的机构id
		$count=count($arr_memberid);
		if($count=='0'){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有选择的联席部门。'));
		}
		$data['advice_requie_times'] = $count;
		$reg =  $model->add($data);

		//2，添加通知接收

		foreach($arr_memberid as $k=>$v){
			$dataList[] = array('advice_receive_id'=>$reg,'advice_receive_gov_id'=>$v);
		}

		$res = $receive_model->addAll($dataList);

		if($reg && $res){
			$model->commit();//成功则提交
			$receive_model->commit();//成功则提交
		}else{
			$model->rollback();
			$receive_model->rollback();
			$this->ajaxReturn(array('code'=>-1,'msg'=>'提交复核失败！'));

		}

		//附件
		if(I('attachinfo')){
			$attachinfo = I('attachinfo');
			foreach ($attachinfo as $key => $value) {
				if($value['id'] == '') $attachlist[] = $value;
			}
			if($attachlist){
				$attach_data['user_id'] = session('regulatorpersonInfo.fid');
				$attach_data['modelname'] ='咨询';
				$attach_data['modelid'] =$reg;
				$attach_data['ffilename'] = ' ';
				$attach_data['fuploadtime'] = date('Y-m-d H:i:s',time());
				foreach ($attachlist as $key => $value) {
					$attach_data['fattachname'] = $value['name'];
					$attach_data['fattachurl'] = $value['url'];
					$attach_data['ffilename'] = preg_replace('/\..*/','',$value['url']);
					$attach_data['ffiletype'] = preg_replace('/.*\./','',$value['url']);
					$attach[$key] = $attach_data;
				}
				$attachid = $lx_uploads_file_model->addAll($attach);
				if($attachid){
					$lx_uploads_file_model->commit();//成功则提交
				}else{
					$lx_uploads_file_model->rollback();
					$this->ajaxReturn(array('code'=>-1,'msg'=>'附件添加失败！'));

				}
			}
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'公告发送成功！'));
	}

	/*已收通告详情*/
	public function details()
	{
		if(I('receive_id')){
			$receive_id = I('receive_id');//接收id
		}else{
			$this->error('缺失receive_id');
		}

		/*1,埋点 修改lx_advice_receive*/
		$receive_data = M('lx_advice_receive')->where(array('id'=>$receive_id))->find();//当前接收详情
		$advice_id =$receive_data['advice_receive_id'];//通知id

		$arr_uid = explode(",",$receive_data['advice_read_user_ids']);
		//是否已经读过
		if(!in_array(session('regulatorpersonInfo.fid'),$arr_uid)){
			$arr['advice_receive_status'] = 2;//已读
			if(empty($receive_data['advice_read_user_ids'])){
				$arr['advice_read_user_ids'] = session('regulatorpersonInfo.fid');//阅读人员id
			}else{
				$arr['advice_read_user_ids'] = $receive_data['advice_read_user_ids'].','.session('regulatorpersonInfo.fid');//阅读人员id ,拼接
			}
			$arr['advice_read_user_num']=$receive_data['advice_read_user_num']+1;//本部门已阅人总数
			if(empty($receive_data['advice_read_log'])){
				$arr['advice_read_log'] = date('Y-m-d H:i:s').'=>'.session('regulatorpersonInfo.fid');
			}else{
				$arr['advice_read_log'] = $receive_data['advice_read_log'].','.date('Y-m-d H:i:s').'=>'.session('regulatorpersonInfo.fid');//首次阅读时间拼接，与阅读人员id对应
			}
			$receive = M('lx_advice_receive')->where(array('id'=>$receive_id))->save($arr);

			/*2,埋点 修改lx_advice*/
			$read_num = M('lx_advice_receive')
				->where(array('advice_receive_id'=>$advice_id,'advice_receive_status'=>2))
				->count();// 查询满足要求的总记录数

			$advice = M('lx_advice')->where(array('advice_id'=>$advice_id))->save(array('advice_read_times'=>$read_num));
		}

		/*通告详情*/
		$data = M('lx_advice_receive')
			->field('lx_advice.*,
					 lx_advice_receive.id as receive_id,
					 lx_advice_receive.advice_read_user_ids,
					 lx_advice_receive.advice_read_user_num,
					 lx_advice_receive.advice_read_log
					 ')
			->join('lx_advice on lx_advice.advice_id = lx_advice_receive.advice_receive_id', 'LEFT')//通知主表
			->where(array('lx_advice_receive.id'=>$receive_id))
			->find();//已收通告详情

		//附件信息
		$attach = M('lx_uploads_file')->where(array('modelname' =>'咨询','modelid' =>$advice_id))->select();
		$attach_count = count($attach);

		//评论
		$answer = M('lx_advice_answer')->where(array('advice_id' =>$advice_id))->select();

		$this->assign('data', $data); //违法信息
		$this->assign('attach',$attach);//附件
		$this->assign('attach_count',$attach_count);//附件数量
		$this->assign('answer',$answer);//评论
		$this->display('advice');
	}

	/*已发通告详情*/
	public function sendDetails(){
		if(I('advice_id')){
			$advice_id = I('advice_id');//通知
		}else{
			$this->error('缺失advice_id');
		}
		/*通告详情*/
		$data = M('lx_advice')->where(array('advice_id'=>$advice_id))->find();//通告详情

		//附件信息
		$attach = M('lx_uploads_file')->where(array('modelname' =>'咨询','modelid' =>$advice_id))->select();
		$attach_count = count($attach);
		//评论
		$answer = M('lx_advice_answer')->where(array('advice_id' =>$advice_id))->select();

		$this->assign('data', $data); //违法信息
		$this->assign('attach',$attach);//附件
		$this->assign('attach_count',$attach_count);//附件数量
		$this->assign('answer',$answer);//评论
		$this->display('advice');

	}

	/*发布评论AJAX*/
	public function ajax_addComment(){
		if(I('advice_id')){
			$advice_id = I('advice_id');//通知id
		}else{
			$this->error('缺失advice_id');
		}
		$answer_model=M("lx_advice_answer");
		$answer_model->startTrans();//开启事务

		//1，添加评论表
		$data['advice_id'] = $advice_id;//建议主表id
		$data['advice_answer_content'] = I('advice_answer_content');//通知回复内容
		$data['advice_answer_user_id'] = session('regulatorpersonInfo.fid');///通知评论人id
		$data['advice_answer_user_name'] = session('regulatorpersonInfo.fname');///通知评论人姓名
		$data['advice_answer_time'] = date('Y-m-d H:i:s');
		$reg =  M("lx_advice_answer")->add($data);
		if($reg){
			$answer_model->commit();//成功则提交
			$this->ajaxReturn(array('code'=>0,'msg'=>'公告发送成功！'));
		}else{
			$answer_model->rollback();
			$this->ajaxReturn(array('code'=>-1,'msg'=>'提交复核失败！'));

		}

	}
	//删除附件
	public function ajax_delete_attach(){

		$file_id = I('file_id');
		if(file_id) M('lx_uploads_file')->where(array('file_id' => $file_id))->delete();
		$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
	}
}