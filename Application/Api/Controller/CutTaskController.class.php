<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;
/*

select cut_task_id,day(fissuedate),fissuedate,fmediaid
from cut_task
where fissuedate BETWEEN '2019-03-01' and '2019-03-15' 
		and fstate in(0,1,2) 
		and day(fissuedate) not in(2,3,9,10,13) 
		and (select count(*) from tmedialabel where fmediaid = cut_task.fmediaid and flabelid not in(245,280)) = 0
;
*/

class CutTaskController extends Controller {
	
	

	
	public function make_task(){
		set_time_limit(600);
		session_write_close();
		lock('CutTask_make_task',true,15);//获得线程锁

		$inspect_plan_list = M('tinspect_plan','',C('ZIZHI_DB'))->field('fid,fmedia_id,fissue_date')->where(array('fissue_date'=>['between',[date('Y-m-d',time()-86400*30),date('Y-m-d')]],'finspect_type'=>3,'fstatus'=>0))->select();
		
		$inC = 0;
		foreach($inspect_plan_list as $inspect_plan){
			$inC += A('Api/CutTask','Model')->make_task($inspect_plan['fmedia_id'],$inspect_plan['fissue_date']);
			if(!C('IS_TEST')) M('tinspect_plan','',C('ZIZHI_DB'))->where(array('fstatus'=>0,'fid'=>$inspect_plan['fid']))->save(array('fstatus'=>1));
		}
		#A('Api/CutTask','Model')->make_task($mediaId,$date);

		lock('CutTask_make_task',null);//释放线程锁
		echo date('Y-m-d H:i:s').'	'.$inC;
	}
	
	public function make_task0(){
		return false;
		lock('CutTask_make_task',true,15);//获得线程锁
		$date = I('date');
		if($date == ''){
			$date = date('Y-m-d');
		}
		$mediaIdList = M('tmedialabel')->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')->where(array('tlabel.flabel'=>'快剪'))->getField('fmediaid',true);
		
	
		foreach($mediaIdList as $mediaId){
			
			
			$planCount = M('tcustomer_inspect_plan','',C('ZIZHI_DB'))->where(array('fmedia_id'=>$mediaId,'fissue_date'=>$date,'fstatus'=>['egt',0]))->count();
			
			if($planCount == 0) continue; #如果没有交付计划
			$mediaInfo = M('tmedia')->cache(true,600)->field('left(fmediaclassid,2) as media_class,priority')->where(array('fid'=>$mediaId))->find();
			

			
			if($mediaInfo['media_class'] == '01'){
				$source_class = 'mp4';
			}elseif($mediaInfo['media_class'] == '02'){
				$source_class = 'aac';
			}else{
				continue;//结束循环
			}
			
			for($i=0;$i<86400;$i+=3600*8){
				$startTime = strtotime($date) + $i;
				$endTime = $startTime + 3600*9;
				if($endTime > (strtotime($date) + 86400)){
					$endTime = strtotime($date) + 86400;
				}
				$m3u8Url = A('Common/Media','Model')->get_m3u8($mediaId,$startTime,$endTime);
				$ck = M('cut_task')->where(array('fmediaid' => $mediaId,'fissuedate' => $date,'fstarttime' => $startTime,'fendtime' => $endTime))->count();
				if($ck == 0){
					M('cut_task')->add(array(
												
												'fmediaid' => $mediaId,
												'fissuedate' => $date,
												'fstarttime' => $startTime,
												'fendtime' => $endTime,
												'priority' => $mediaInfo['priority'],
												'fstate' => 0,
												'source_class' => $source_class,
												'time_list' => '[]',
												
												'start_make_time'=>$endTime + 3600*5,
												'cut_wx_id'=> M('tqc_media_permission')->cache(true,300)->where(array('fmediaid'=>$mediaId))->getField('wx_id'),

												'm3u8_url' => $m3u8Url
											));
				}
				
			}
			
		}
		lock('CutTask_make_task',null);//释放线程锁

	}
	
	
	#监控快剪任务生成排队情况
	public function monitor(){
		
		$state0Count = M('cut_task')
									->field('
												count(*) count 
												,count(case when source_class = "aac" then 1 else null end) as count_aac
												,count(case when source_class = "mp4" then 1 else null end) as count_mp4
												
												
											')
									->where(array('fstate'=>0,'start_make_time'=>array('lt',time())))
									->find();
		$hh = date('H');
		$ww = date('w');
		
		
		$countZ = $state0Count['count_aac'] + $state0Count['count_mp4'];
		
		$value = '';
		$value .= '有 '.$countZ.' 条正在排队生成的快剪任务，其中 AAC ('.$state0Count['count_aac'].'条) MP4 ('.$state0Count['count_mp4'].'条) ，请处理'."\n";

		
		echo $value;
		
		if ($ww > 0 && $ww < 6 && $hh > 8 && $hh <= 18 && $countZ > 800){
		
			$ddUrl = 'https://oapi.dingtalk.com/robot/send?access_token=94e5694dade6b4a19e681ef99eae45b7c9cbd2b3cc294431f5487fe38b46611d';
			$msgPost = array();
			$msgPost['msgtype'] = 'text';
			$msgPost['text']['content'] = $value;
			$msgPost['at']['atMobiles'] = array('18813183941','15858258071');
			$rr = http($ddUrl,json_encode($msgPost),'POST',array('Content-Type:application/json; charset=utf-8'));
			
			echo '已发送钉钉消息:'.$rr."\n";
			

		}
	}
	

	
	
	
	
	
	
	
	
	
}