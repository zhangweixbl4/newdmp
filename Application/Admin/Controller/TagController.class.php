<?php
/*
 * @Description: 通用标签(分组)
 * @Author: yuhou.wang
 * @Date: 2019-12-04 13:59:00
 * @LastEditors: yuhou.wang
 * @LastEditTime: 2019-12-04 14:42:44
 */
namespace Admin\Controller;
class TagController extends BaseController {
	// 接收JSON参数
    protected $P;
	/**
	 * @Des: 初始化
	 * @Edt: yuhou.wang
	 */	
    public function _initialize(){
        parent::_initialize();
        $this->P = json_decode(file_get_contents('php://input'),true);
    }
    /**
     * @Des: 获取标签(分组)列表
     * @Edt: yuhou.wang
     * @Date: 2019-12-04 11:34:55
     * @param {String} $fields 返回字段
     * @param {Array} $filter 过滤条件 
     * @return: 
     */
    public function getList(){
        $where = [
            'group_name' => ['LIKE','数据_%组']
        ];
        $fields=$this->P['fields'] ? $this->P['fields'] : 'group_id,group_name';
        $filter=$this->P['filter'] ? $this->P['filter'] : $where;
        $dataSet = [];
        $dataSet = A('Common/Tag','Model')->getList($fields,$filter);
        $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$dataSet]);
    }
}