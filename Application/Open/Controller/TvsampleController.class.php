<?php
namespace Open\Controller;
use Think\Controller;

class TvsampleController extends BaseController {
	



	public function add_edit_tvsample(){
		$tvsample_data = file_get_contents('php://input');//获取post数据
		$tvsample_data = json_decode($tvsample_data,true);//把json转化成数组
		//var_dump($tvsample_data);
		$fid = $tvsample_data['fid'];//广告样本ID
		
		$a_e_data = array();

		$a_e_data['fmediaid'] = intval($tvsample_data['fmediaid']);//媒体ID
		$media_count = M('tmedia')->where(array('fid'=>$a_e_data['fmediaid']))->count();
		if($media_count == 0) $this->ajaxReturn(array('code'=>10016,'msg'=>'添加或修改数据失败,媒体ID有错误'));
		
		$a_e_data['fissuedate'] = date("Y-m-d H:i:s",strtotime($tvsample_data['fissuedate']));//发布日期
		
		$a_e_data['fadid'] = $tvsample_data['fadid'];//广告ID
		$ad_count = M('tad')->where(array('fadid'=>$a_e_data['fadid']))->count();

		if($ad_count == 0) $this->ajaxReturn(array('code'=>10016,'msg'=>'添加或修改数据失败,广告ID有错误'));
		
		$a_e_data['fversion'] = $tvsample_data['fversion'];//版本说明
		
		$a_e_data['fspokesman'] = $tvsample_data['fspokesman'];//代言人
		
		$a_e_data['fadlen'] = intval($tvsample_data['fadlen']);//样本长度
		$a_e_data['fillegaltypecode'] = intval($tvsample_data['fillegaltypecode']);//违法类型代码
		$a_e_data['fillegalcontent'] = $tvsample_data['fillegalcontent'];//涉嫌违法内容
		$a_e_data['fexpressioncodes'] = $tvsample_data['fexpressioncodes'];//违法表现代码，数组
		
		
		$a_e_data['favifilename'] = $tvsample_data['favifilename'];//视频文件名
		$a_e_data['fstate'] = $tvsample_data['fstate'];//状态
		
		$person_info = $GLOBALS['person_info'];//获取接口请求人的信息
		
		
		if($fid == '' ){
			$a_e_data['fcreator'] = $person_info['fname'];//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = $person_info['fname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			//var_dump($a_e_data);exit;
			$rr = M('ttvsample')->add($a_e_data);
			
			$illegal = A('Open/Tvsample','Model')->tvsampleillegal($rr,$a_e_data['fexpressioncodes'],$person_info['fname']);//添加电视广告违法表现对应表并获取冗余字段
			M('ttvsample')->where(array('fid'=>$rr))->save($illegal);//修改违法表现冗余字段
			if($rr > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'添加电视广告样本成功','fid'=>$rr));
			if($rr == 0) $this->ajaxReturn(array('code'=>10011,'msg'=>'添加电视广告样本失败,原因未知'));
		}else{
			$a_e_data['fmodifier'] = $person_info['fname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('ttvsample')->where(array('fid'=>$fid))->save($a_e_data);
			$illegal = A('Open/Tvsample','Model')->tvsampleillegal($fid,$a_e_data['fexpressioncodes'],$person_info['fname']);//添加电视广告违法表现对应表并获取冗余字段
			M('ttvsample')->where(array('fid'=>$fid))->save($illegal);//修改违法表现冗余字段
			if($rr > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'修改电视广告样本成功'));
			if($rr == 0) $this->ajaxReturn(array('code'=>10012,'msg'=>'修改电视广告样本失败,原因未知'));
		}
		
		
	
	}
	
			/*获取电视样本*/
	public function get_tvsample_list(){
		$tvsample_data = file_get_contents('php://input');//获取post数据
		
		$tvsample_data = json_decode($tvsample_data,true);//把json转化成数组
		$p = intval($tvsample_data['p']);//需要返回的页数
		if($p == 0) $p = 1;
		$pp = intval($tvsample_data['pp']);//需要返回的条数
		if($pp <= 0) $pp = 10;
		if($pp > 100) $pp = 100;
		
		$fcreatetime_s = $tvsample_data['fcreatetime_s'];//创建时间 开始
		if($fcreatetime_s == '') $fcreatetime_s = '2015-01-01';
		$fcreatetime_e = $tvsample_data['fcreatetime_e'];//创建时间 结束
		if($fcreatetime_e == '') $fcreatetime_e = date('Y-m-d H:i:s');

		$where = array();
		$where['fcreatetime'] = array('between',$fcreatetime_s.','.$fcreatetime_e);
		if($tvsample_data['fid'] != '') $where['ttvsample.fid'] = $tvsample_data['fid'];//样本ID
		if($tvsample_data['fmediaid'] != '') $where['ttvsample.fmediaid'] = $tvsample_data['fmediaid'];//媒介ID
		if($tvsample_data['fadid'] != '') $where['ttvsample.fadid'] = $tvsample_data['fadid'];//广告ID
		if($tvsample_data['fillegaltypecode'] != '') $where['ttvsample.fillegaltypecode'] = $tvsample_data['fillegaltypecode'];//违法类型
		
		if($tvsample_data['fversion'] != '') $where['ttvsample.fversion'] = array('like','%'.$tvsample_data['fversion'].'%');//版本说明
		if($tvsample_data['fspokesman'] != '') $where['ttvsample.fspokesman'] = array('like','%'.$tvsample_data['fspokesman'].'%');//代言人
		
		$count = M('ttvsample')->where($where)->count();
		
		$tvsampleList = M('ttvsample')->where($where)->page($p,$pp)->select();

		$this->ajaxReturn(array('code'=>0,'msg'=>'','tvsampleList'=>$tvsampleList,'count'=>$count));
		

	}
	
}