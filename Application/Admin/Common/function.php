<?php
	/*
	* 数据有效性校验
	* by zw
	* $data数据格式[
		['name'=>'fuserid','msg'=>'用户必填','types'=>'string'],
		['name'=>'fuserid','msg'=>'用户必填','types'=>'int','otherdata'=>[] ]
	]
	*待更多情况完善
	*/
	function check_field($data){
		$code = 0;
		$msg = '验证通过';
		foreach ($data as $key => $value) {
			switch ($value['types']) {
				case 'string'://字符串
					//判断是否为空
					if(I($value['name']) == ''){
						$code = 1;
						$msg = $value['msg'];
					}
					break;
				case 'int'://数值
					if(!is_numeric(I($value['name']))){
						$code = 1;
						$msg = $value['msg'];
					}
					break;
				case 'limitlen'://英文限长
					break;
				case 'numsection'://数值区间
					//判断值不能比min小
					if(!empty($value['otherdata']['min'])){
						if((int)I($value['name']) < $value['otherdata']['min']){
							$code = 1;
							$msg = $value['msg'];
						}
					}
					//判断值不能比max大
					if(!empty($value['otherdata']['max'])){
						if((int)I($value['name']) > $value['otherdata']['max']){
							$code = 1;
							$msg = $value['msg'];
						}
					}
					break;
				default:
					break;
			}
			//校验失败，退出循环
			if(!empty($code)){
				break;
			}
		}
		return compact('code', 'msg');
	}