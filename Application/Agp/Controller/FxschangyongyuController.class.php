<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 线索登记常用语管理模块
 */

class FxschangyongyuController extends BaseController
{
	/**
	 * 获取常用语列表
	 * by zw
	 */
	public function index()
	{
		session_write_close();

		$where['a.tg_createpersonid'] 	= session('regulatorpersonInfo.fid');

		$data = M('tregistercommonlng')
			->alias('a')
			->field('tg_id,tg_content')
			->where($where)
			->order('a.tg_order desc,a.tg_hot desc')
			->select();//查询复核数据

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>count($data),'list'=>$data)));
	}

	/**
	 * 添加常用语
	 * by zw
	 */
	public function addcommonlng(){
		$content = I('content');//内容

		$data_tg['tg_content'] = $content;
		$data_tg['tg_trepid'] = session('regulatorpersonInfo.fregulatorpid');
		$data_tg['tg_createpersonid'] = session('regulatorpersonInfo.fid');
		$data_tg['tg_createtime'] = date('Y-m-d');

		$do_tg = M('tregistercommonlng')->add($data_tg);
		if(!empty($do_tg)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'添加失败'));
		}
	}

	/**
	 * 删除常用语
	 * by zw
	 */
	public function delcommonlng(){
		$fid = I('fid');//ID

		$where_tg['tg_createpersonid'] = session('regulatorpersonInfo.fid');
		$where_tg['tg_id'] = $fid;

		$do_tg = M('tregistercommonlng')->where($where_tg)->delete();
		if(!empty($do_tg)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败'));
		}
	}
	
}