<?php
namespace TaskInput\Model;
use Think\Model;
class TaskInputModel extends Model{
    const CUT_ERROR_COUNT_FIELD = 'v3_sub_cut_err';
    const TASK_BACK_COUNT_FIELD = 'v3_task_back';
    const QUALITY_COUNT_FIELD = 'v3_quality';
    const USER_AUTH_CODE = '1006';  // 录入权限
    const QUALITY_INSPECTOR_AUTH_CODE = '1007';     // 审核权限
    const PREVIEW_AUTH_CODE = '1008';     // 预览权限
    const ILLEGAL_REVIEW_COMMENT_AUTH_CODE = '3001';
    const ILLEGAL_REVIEW_COMMENT_CONFIRM_AUTH_CODE = '3002';

    protected $task_count_field_map = [
        '1' => [
            '1' => 'tv_input',
            '2' => 'tv_inspect',
        ],
        '2' => [
            '1' => 'bc_input',
            '2' => 'bc_inspect',
        ],
        '3' => [
            '1' => 'paper_input',
            '2' => 'paper_inspect',
        ],
        '13' => [
            '1' => 'net_input',
            '2' => 'net_inspect',
        ],
    ]; // 第一层是媒体类型, 第二层是环节类型

    protected $v3_task_count_map = [
        '1' => 'v3_tv_task',
        '2' => 'v3_bc_task',
        '3' => 'v3_paper_task',
        '13' => 'v3_net_task',
    ];

    /**
     * 提交广告信息录入任务
     * @param $data
     * @param $wx_id
     * @return bool
     */
    public function submitAdInputTask($data, $wx_id)
    {
        if ($data['fadclasscode'] == 2301){
            $this->error = '广告分类不允许';
            return false;
        }
        $wxInfo = M('ad_input_user')
            ->cache(true, 60)
            ->where(['wx_id' => ['EQ', $wx_id]])
            ->find();
        // 新广告主题则加入质检任务
        if(empty($data['fadid']) && !empty($data['fadname'])){
            $addRes = A('TaskInput/QualityInspection','Model')->addInsTask($data,true);
        }
        // 获取广告ID，不存在则创建新广告主题并返回fadid
        if ($data['media_class'] != 13){
            $data['fadid'] = A('Common/Ad','Model')->getAdId($data['fadname'],$data['fbrand'],$data['fadclasscode'],$data['fname'],$wxInfo['alias'], $data['fadclasscode_v2']);//获取广告ID
        }else{
            $data['fadid'] = A('Common/Ad','Model')->getAdId($data['fadname'],$data['fbrand'],$data['fadclasscode'],$data['fname'],$wxInfo['alias'], $data['fadclasscode_v2']);//获取广告ID
            $data['fadowner'] = A('Common/Ad','Model')->getAdOwnerId($data['fname'],$wxInfo['alias']);
            $issueData = $data;
            $issueData['finputstate'] = 2;//更新为已录入状态 录入状态，0-无需录入，1-未录入，2-已录入
            $issueData['finputuser'] = $wx_id;
            $issueData['fmodifier'] = $wx_id;
            $issueData['fmodifytime'] = date('Y-m-d H:i:s');
            $netIssueRes = M('tnetissue')
                ->where([
                    'major_key' => ['EQ', $data['major_key']]
                ])
                ->save($issueData);
            if (!$netIssueRes){
                $this->error = '网络发布更新失败';
                return false;
            }
        }
        return $this->submitTask($data['taskid'], $data, 1 ,$wx_id);
    }

    /**
     * 提交互联网广告信息录入任务
     * @param $data
     * @param $wx_id
     * @return bool
     */
    public function submitNetAdInputTask($data, $wx_id)
    {
        if ($data['fadclasscode'] == 2301){
            $this->error = '广告分类不允许';
            return false;
        }
        $wxInfo = M('ad_input_user')
            ->cache(true, 60)
            ->where(['wx_id' => ['EQ', $wx_id]])
            ->find();
        // 新广告主题则加入质检任务
        if(empty($data['fadid']) && !empty($data['fadname'])){
            $addRes = A('TaskInput/QualityInspection','Model')->addInsTask($data,true);
        }
        if ($data['media_class'] != 13){
            $data['fadid'] = A('Common/Ad','Model')->getAdId($data['fadname'],$data['fbrand'],$data['fadclasscode'],$data['fname'],$wxInfo['alias'], $data['fadclasscode_v2']);//获取广告ID
        }else{
            $data['fadid'] = A('Common/Ad','Model')->getAdId($data['fadname'],$data['fbrand'],$data['fadclasscode'],$data['fname'],$wxInfo['alias'], $data['fadclasscode_v2']);//获取广告ID
            $data['fadowner'] = A('Common/Ad','Model')->getAdOwnerId($data['fname'],$wxInfo['alias']);
            $issueData = $data;
            $issueData['finputstate'] = 2;//更新为已录入状态 录入状态，0-无需录入，1-未录入，2-已录入
            $issueData['finputuser'] = $wx_id;
            $issueData['fmodifier'] = $wx_id;
            $issueData['fmodifytime'] = date('Y-m-d H:i:s');
            $netIssueRes = M('tnetissue')
                ->where([
                    'major_key' => ['EQ', $data['major_key']]
                ])
                ->save($issueData);
            if (!$netIssueRes){
                $this->error = '网络发布更新失败';
                return false;
            }
        }
        return $this->submitNetTask($data['taskid'], $data, 1 ,$wx_id);
    }


    public function submitJudgeTask($data, $wx_id)
    {
        // 如果广告非法, 过滤掉其他字段
        if ($data['fillegaltypecode'] == '0'){
            unset($data['fillegalcontent']);
            unset($data['fexpressioncodes']);
            unset($data['fexpressions']);
            unset($data['fconfirmations']);
        }
        if ($data['media_class'] == 13) {
            $issueData = $data;
            $issueData['finspectstate'] = 2;//更新为已录入状态 录入状态，0-无需录入，1-未录入，2-已录入
            $issueData['finspectuser'] = $wx_id;
            $issueData['fmodifier'] = $wx_id;
            $issueData['fmodifytime'] = date('Y-m-d H:i:s');
            $netIssueRes = M('tnetissue')
                ->where([
                    'major_key' => ['EQ', $data['major_key']]
                ])
                ->save($issueData);
            if (!$netIssueRes){
                $this->error = '网络发布更新失败';
                return false;
            }
        }
        return $this->submitTask($data['taskid'], $data, 2 ,$wx_id);
    }


    /**
     * @param $taskid
     * @param $data array   任务的广告录入信息或者违法判定信息, 不包括状态信息
     * @param $type int     环节类型
     * @param $wx_id
     * @return bool
     */
    private function submitTask($taskid, $data, $type, $wx_id)
    {
        $flowText = $type == 1 ? '完成广告信息录入' : '完成违法审核';
        // 检查流程状态及录入人
        $taskInputLinkModel = new TaskInputLinkModel();
        if (!$taskInputLinkModel->checkAdInputValidate($taskid, $type, $data['media_class'])){
            $this->error = $taskInputLinkModel->getError();
            return false;
        }
        $where = [
            'taskid' => ['EQ', $taskid]
        ];
        $data['wx_id'] = $wx_id;
        // 更新任务的广告录入信息或者违法判定信息
        $tableName = $data['media_class'] == 13 ? 'tnettask' : 'task_input';
        // 审核时修改录入信息时，减扣录入人积分及记录历史
        if($type == 2){
            $isChangeInput = false;
            $wxInfo = M('ad_input_user')->cache(true, 60)->where(['wx_id'=>['EQ', $wx_id]])->find();
            $data['fadid'] = A('Common/Ad','Model')->getAdId($data['fadname'],$data['fbrand'],$data['fadclasscode'],$data['fname'],$wxInfo['alias'], $data['fadclasscode_v2']);//获取广告ID
            $beforeInfo = M($tableName)->where(['taskid'=>$taskid])->find();
            $afterInfo = $beforeInfo;
            $fields = ['fspokesman','fversion','is_long_ad','fadid'];
            foreach($fields as $field){
                $isChangeInput = $beforeInfo[$field] != $data[$field];
                if($isChangeInput == true) break;//有一项不同则修改过录入信息
            }
            // 找到录入人
            if($data['media_class'] == 13){
                $userId = M('tnettask_flowlog')->where(['taskid'=>$taskid,'logtype'=>12])->order('createtime DESC')->getField('wx_id');
            }else{
                $userId = M('task_input_link')->where(['taskid'=>$taskid,'type'=>1,'state'=>2])->order('update_time DESC')->getField('wx_id');
            }
            // 记录变更
            if($isChangeInput == true && $userId != $wx_id){
                $historyTable = $tableName.'_history';
                $addRet = M($historyTable)->add($beforeInfo);
                $afterInfo['fadid'] = $data['fadid'];
                $afterInfo['fspokesman'] = $data['fspokesman'];
                $afterInfo['fversion'] = $data['fversion'];
                $afterInfo['is_long_ad'] = $data['is_long_ad'];
                $afterInfo['fmodifier'] = $wxInfo['alias'];
                $afterInfo['fmodifytime'] = date('Y-m-d H:i:s');
                $addAfterRet = M($historyTable)->add($afterInfo);
                // 减扣积分
                $retScore = A('TaskInput/TaskInputScore','Model')->modifyTask($taskid, $data['media_class'], 1, $userId, $wx_id);
                // 增加录入积分
                $scoreRes = A('TaskInput/TaskInputScore','Model')->addScoreBySubmitTask($taskid, 1, '任务修改加分:');
            }
        }
        $saveRes = M($tableName)->where($where)->save($data) !== false;

        if (!$saveRes){
            $this->error = '任务信息更新失败';
            return false;
        }
        // 是否质检
        if($type == 2){
            $addRes = A('TaskInput/QualityInspection','Model')->addInsTask($data);
        }
        // 更新流程
        if ($data['media_class'] == 13){
            $flowTableName = 'tnettask_flowlog';
            $flowData = [
                'taskid' => $taskid,
                'log' => $flowText,
                'logtype' => 2,
                'creator' => $wx_id,
                'createtime' => time(),
            ];
        }else{
            $flowTableName = 'task_input_flow';
            $flowData = [
                'taskid' => $taskid,
                'flow_content' => $flowText,
                'flow_time' => time(),
                'wx_id' => $wx_id,
            ];
        }
        $flowRes = M($flowTableName)
            ->add($flowData);
        if (!$flowRes){
            $this->error = '广告信息更新失败';
            return false;
        }
        // 更新任务及环节状态
        $media_class = $data['media_class'];
        $linkRes = $taskInputLinkModel->submitTaskLink($type, $taskid, $media_class, $wx_id);
        if (!$linkRes){
            $this->error = $taskInputLinkModel->getError();
            return false;
        }
        // 若录入了22-非商业类 23-其它时，自动完成违法审核(不违法)
        if(substr($data['fadclasscode'],0,2) == '22' || substr($data['fadclasscode'],0,2) == '23'){
            $linkRes = $taskInputLinkModel->submitTaskLink(2, $taskid, $media_class, $wx_id);
        }
        if ($media_class == 13){
            A('Common/AliyunLog','Model')->net_task_log([
                'type' => 'save',
                'taskid' => $taskid,
                'data' => $data,
                'link_type' => $type,
                'msg' => '提交任务成功;'
            ]);
        }else{
            A('Common/AliyunLog','Model')->task_input_log([
                'type' => 'save',
                'taskid' => $taskid,
                'media_class' => $media_class,
                'data' => $data,
                'link_type' => $type,
                'msg' => '提交任务成功;'
            ]);
        }
        // 获取媒介类型, 计数
        A('InputPc/Task','Model')->change_task_count($wx_id,$this->task_count_field_map[$media_class][$type],1);
        A('InputPc/Task','Model')->change_task_count($wx_id,$this->v3_task_count_map[$media_class],1);
        return true;
    }

    /**
     * @param $taskid
     * @param $data array   任务的广告录入信息或者违法判定信息, 不包括状态信息
     * @param $type int     环节类型
     * @param $wx_id
     * @return bool
     */
    private function submitNetTask($taskid, $data, $type, $wx_id)
    {
        $flowText = $type == 1 ? '完成广告信息录入' : '完成违法审核';
        $where = [
            'taskid' => ['EQ', $taskid]
        ];
        $data['wx_id'] = $wx_id;
        // 更新任务的广告录入信息或者违法判定信息
        $tableName = $data['media_class'] == 13 ? 'tnettask' : 'task_input';
        $saveRes = M($tableName)
                ->where($where)
                ->save($data) !== false;
        if (!$saveRes){
            $this->error = '任务信息更新失败';
            return false;
        }
        // 更新流程
        if ($data['media_class'] == 13){
            $flowTableName = 'tnettask_flowlog';
            $flowData = [
                'taskid' => $taskid,
                'log' => $flowText,
                'logtype' => 2,
                'creator' => $wx_id,
                'createtime' => time(),
            ];
        }else{
            $flowTableName = 'task_input_flow';
            $flowData = [
                'taskid' => $taskid,
                'flow_content' => $flowText,
                'flow_time' => time(),
                'wx_id' => $wx_id,
            ];
        }
        $flowRes = M($flowTableName)
            ->add($flowData);
        if (!$flowRes){
            $this->error = '广告信息更新失败';
            return false;
        }
        // 更新任务及环节状态
        $media_class = $data['media_class'];
        $linkRes = $taskInputLinkModel->submitTaskLink($type, $taskid, $media_class, $wx_id);
        if (!$linkRes){
            $this->error = $taskInputLinkModel->getError();
            return false;
        }
        if ($media_class == 13){
            A('Common/AliyunLog','Model')->net_task_log([
                'type' => 'save',
                'taskid' => $taskid,
                'data' => $data,
                'link_type' => $type,
                'msg' => '提交任务成功;'
            ]);
        }else{
            A('Common/AliyunLog','Model')->task_input_log([
                'type' => 'save',
                'taskid' => $taskid,
                'media_class' => $media_class,
                'data' => $data,
                'link_type' => $type,
                'msg' => '提交任务成功;'
            ]);
        }
        // 获取媒介类型, 计数
        A('InputPc/Task','Model')->change_task_count($wx_id,$this->task_count_field_map[$media_class][$type],1);
        A('InputPc/Task','Model')->change_task_count($wx_id,$this->v3_task_count_map[$media_class],1);
        return true;
    }

    public function getTaskTrans()
    {
        $wx_id = intval(session('wx_id'));
        // 获取用户所在组的权限信息
        $userGroupInfo = M('ad_input_user_group')
            ->field('ad_input_user_group.group_id, ad_input_user_group.other_authority, ad_input_user_group.mediaauthority, ad_input_user_group.regionauthority, ad_input_user_group.dateauthority, ad_input_user.prefer_list, ad_input_user.priority_list')
            ->join('ad_input_user on ad_input_user.group_id = ad_input_user_group.group_id')
            ->where(array('ad_input_user.wx_id'=>$wx_id))
            ->find();
        $linkTypes = (new AdInputUserModel())->getTaskLinkAuth($wx_id); // 录入权限
        // 判断广告信息录入权限
        $taskid = $this
            ->alias('task')
            ->join('task_input_link as link on task.taskid = link.taskid and task.media_class = link.media_class')
            ->where([
                'link.type' => ['IN', $linkTypes],
                'link.state' => ['EQ', 1],
                'task.task_state' => ['EQ', 1],
                'task.wx_id' => ['EQ', $wx_id],
                'link.wx_id' => ['EQ', $wx_id],
                'link.media_class' => ['NEQ', 13],
            ])
            ->getField('task.taskid');
        if($taskid) return $taskid;//如果有正在执行的任务，直接返回正在执行的任务id
        // 获取媒体偏好
        $mediaPrefer = $userGroupInfo['prefer_list'];
        // 获取优先级偏好
        $priorityList = $userGroupInfo['priority_list'];

        $searchWhere = [
            'link.type' => ['IN', $linkTypes],
            'link.state' => ['EQ', 0],
            // 'link.media_class' => ['NEQ', 13],
            'task.cut_err_state' => ['EQ', 0],
            'task.task_state' => ['EQ', 0]
        ];
        // if(!in_array($wx_id,[1694,93,408,538,89,511,104,379,1582,940,51])){
        //     // 王凌（质检）,王妍（甘肃）,穆琼琳（淄博）,郭萍萍（淄博）,金梅（甘肃）,王芬,张庆,王玲,仇海春,李伟,赵国琪
        //     $searchWhere['task.group_id'] = 0;
        // }
        // 人员数据分组
        $have_group_id = M('ad_input_user')->cache(true,120)->where(['wx_id'=>$wx_id])->getField('have_group_id');
        if(!empty($have_group_id)){
            $task_have_group_id = explode(',',$have_group_id);
        }
        $task_have_group_id[] = '0';//增加未分组0
        $searchWhere['task.group_id'] = ['IN',$task_have_group_id];
        // 许昌任务不领取广播
        if($userGroupInfo['group_id'] == 49){
            $searchWhere['link.media_class'] = ['IN', ['1', '3']];
        }
        $mediaIds = [];

        // 生成搜索条件及子查询
        if ($userGroupInfo['mediaauthority'] !== 'unrestricted'){
            if (!$userGroupInfo['mediaauthority'] || $userGroupInfo['mediaauthority'] === '""'){
                return false;
            }
            $mediaIds = array_merge($mediaIds, json_decode($userGroupInfo['mediaauthority'], true));
            // $searchWhere['task.media_id'] = ['IN', $mediaIds];
        }
        // 地区限制的条件
        if ($userGroupInfo['regionauthority'] !== 'unrestricted'){
            if (!$userGroupInfo['regionauthority'] || $userGroupInfo['regionauthority'] === '""'){
                return false;
            }
            $userGroupInfo['regionauthority'] = json_decode($userGroupInfo['regionauthority'], true);
            // 区分并组合查找本级的和下属的条件, 生成mediaOwner的子查询搜索条件
            $inArr = [];
            $leftArr = [];
            foreach ($userGroupInfo['regionauthority'] as $item) {
                $len = strlen($item);
                if ($len == 6){
                    $inArr[] = $item;
                }else{
                    $leftArr[] = "(LEFT(fregionid, $len) = $item)";
                }
            }
            // 搜索条件有2个部分, 一个是全部地区id的本级媒体, 一个是部分地区id本级及下属媒体,互相之间的关系应该是OR
            $_mediaOwnerWhere = [
                '_logic' => 'OR',
            ];
            if (count($inArr) != 0){
                $_mediaOwnerWhere['fregionid'] = ['IN', $inArr];
            }
            if (count($leftArr) != 0){
                $_string = implode(' OR ', $leftArr);
                $_mediaOwnerWhere['_string'] = $_string;
            }
            // 生成mediaOwner子查询
            $subQuery = M('tmediaowner')->field('fid')->where($_mediaOwnerWhere)->buildSql();
            $_mediaWhere = [
                'fmediaownerid' => ['EXP', 'IN '. $subQuery]
            ];
            $res = M('tmedia')->where($_mediaWhere)->getField('fid', true);
            // 得到允许的媒体id
            $mediaIds = array_merge($mediaIds, $res);
            // $searchWhere['task.media_id'] = ['IN', $mediaIds];
        }
        // 加入媒体偏好元素
        if ($mediaPrefer){
            $mediaPrefer = explode(',', $mediaPrefer);
            if ($userGroupInfo['mediaauthority'] !== 'unrestricted') {
                $preferList = array_intersect($mediaPrefer, $mediaIds);
            }else{
                $preferList = $mediaPrefer;
            }
            $preferList = implode(',', $preferList);
        }
        if (!$preferList){
            $preferList = "null";
        }
        // 加入优先级偏好元素
        if (!$priorityList){
            $priorityList = "null";
        }
        // 发布日期限制条件
        if ($userGroupInfo['dateauthority'] !== 'unrestricted'){
            if (!$userGroupInfo['dateauthority'] || $userGroupInfo['dateauthority'] === '""'){
                return false;
            }
            $dateauthority = json_decode($userGroupInfo['dateauthority'], true);
            $dateWhere = [];
            foreach ($dateauthority as $key => $value) {
                $dateWhere[] = ['BETWEEN', explode(' , ', $value)];
            }
            $dateWhere[] = 'OR';
            // $searchWhere['task.issue_date'] = $dateWhere;
        }

        $while_num = 0;
        $e_task_state = false;
        while(!$e_task_state){
            $while_num++;
            $orderArr = [];
            if ($priorityList !== 'null'){
                // $orderArr[] = "field(task.priority, $priorityList) desc";
            }
            if ($preferList !== 'null'){
                // $orderArr[] = "field(task.media_id, $preferList) desc";
            }
            $orderArr[] = 'task.group_id desc';
            $orderArr[] = 'task.priority desc';
            $orderArr[] = 'task.issue_date ASC';
            $order = implode(',', $orderArr);
            $taskids = $this
                ->alias('task')
                ->join('task_input_link as link on task.taskid = link.taskid and task.media_class = link.media_class and link.media_class <> 13')
                ->where($searchWhere)
                ->order($order)
                ->limit(5)
                ->getField('DISTINCT task.taskid', true);
            $sql = $this->getLastSql();
            if(!$taskids) return false;//如果没有获取到任务，就返回false
            // 为了保证高优先级任务优先分配, 第一项就不打乱顺序了
            $tempItem = $taskids[0];
            // 因为是从环节表查到的让任务id，需要去重
            $taskids = array_unique($taskids);
            // 打乱顺序
            shuffle($taskids);
            $taskids[0] = $tempItem;
            foreach ($taskids as $taskid) {
                M()->startTrans();
                $e_task_state = M('task_input_link')
                    ->where([
                        'taskid'      => ['EQ', $taskid],
                        'type'        => ['IN',$linkTypes],//解决无提交按钮的BUG 2019-01-19
                        'state'       => ['EQ', 0],
                        'media_class' => ['NEQ','13']
                    ])
                    ->save([
                        'wx_id' => $wx_id,
                        'state' => 1,
                        'update_time' => time()
                    ]);
                if ($e_task_state){
                    $e_task_state = $this
                        ->where([
                            'taskid' => ['EQ', $taskid],
                            'task_state' => ['EQ', 0],
                        ])
                        ->save([
                            'wx_id' => $wx_id,
                            'task_state' => 1,
                            'lasttime' => time()
                        ]);
                }
                if($e_task_state){
                    A('TaskInput/Data','Model')->add_flow($taskid,'领取任务',$wx_id);
                    A('Common/AliyunLog','Model')->task_input_log([
                        'type' => 'save',
                        'result' => $e_task_state,
                        'data' => $taskid,
                        'msg' => '领取任务成功;',
                    ]);
                    M()->commit();
                    return $taskid;
                }
                M()->rollback();
            }
            A('Common/AliyunLog','Model')->task_input_log([
                'type' => 'save',
                'result' => $e_task_state,
                'msg' => '领取任务失败;'
            ]);
            if($while_num > 10) return false;//如果抢了10次还没有任务，那就返回false
        }
    }

    /**
     * @param $taskid
     * @param int $wx_id            当前用户id
     * @param int $mediaClass       媒体类型 1:电视 2: 广播 3: 报纸 13: 网络
     * @param int $linkType         环节类型 1:广告录入环节  2: 违法审核环节
     * @param string $reason        退回原因 如果以 '客户反馈' 开头, 则不计入统计
     * @return bool
     */
    public function rejectTask($taskid, $wx_id, $mediaClass, $linkType=1, $reason = '') //$linkType环节类型默认为1处理 2019-02-12
    {
        
        if(in_array($mediaClass,['13',13])){
            return A('TaskInput/NetAd','Model')->rejectTask($taskid, $wx_id, $mediaClass, $linkType, $reason);
        }
        // 互联网退回方法暂单独调用，以下关于互联网的判断可忽略
        
        $time = time();
        $taskTable = $mediaClass != 13 ? 'task_input' : 'tnettask';
        $taskInfo = M($taskTable)->find($taskid);
        $userId = $taskInfo['wx_id'];

        if ($linkType == 2){
            $linkTypeText = '违法判定被退回, ';
            $userId = M('task_input_link')
                ->where([
                    'media_class' => ['EQ', $mediaClass],
                    'type' => $linkType,
                    'taskid' => ['EQ', $taskid]
                ])
                ->getField('wx_id');
        }elseif ($linkType == 1){
            $fadname = M('tad')->find($taskInfo['fadid'])['fadname'];
            $linkTypeText = '广告录入被退回, 原广告主题 "'.$fadname.'" ,';
            $userId = M('task_input_link')
                ->where([
                    'media_class' => ['EQ', $mediaClass],
                    'type' => $linkType,
                    'taskid' => ['EQ', $taskid]
                ])
                ->getField('wx_id');
        }else{
            $linkTypeText = '任务被退回';
        }
        $taskData = [
            'quality_wx_id' => $wx_id,
            'task_state' => 4,
            'syn_state' => 0,
            'lasttime' => $time,
            'modifytime' => $time,
            'wx_id' => $userId,
            'back_reason' => $linkTypeText .$reason. ' 退回人:' .M('ad_input_user')->where(['wx_id' => ['EQ', $wx_id]])->cache(true, 60)->getField('alias'),
        ];
        $flowData = [
            'taskid' => $taskid,
            'flow_content' => $linkTypeText . ' ' . $reason,
            'log' => $linkTypeText . ' ' . $reason,
            'flow_time' => $time,
            'createtime' => $time,
            'wx_id' => $wx_id,
            'creator' => $wx_id,
            'task_info' => json_encode($taskInfo),
        ];
        $where = [
            'taskid' => ['EQ', $taskid],
        ];
        $taskUpdate = M($taskTable)->where($where)->save($taskData);
        if (!$taskUpdate){
            $this->error = '任务更新失败';
            return false;
        }
        $flowTable = $mediaClass != 13 ? 'task_input_flow' : 'tnettask_flowlog';
        $flowUpdate = M($flowTable)->add($flowData);
        if (!$flowUpdate){
            $this->error = '任务流程更新失败';
            return false;
        }
        // 更新对应的环节, 先检查是否有对应的环节
        $hasTaskLink = M('task_input_link')
            ->where([
                'taskid' => ['EQ', $taskid],
                'media_class' => ['EQ', $mediaClass],
                'type' => ['EQ', $linkType],
            ])
            ->find();
        if ($hasTaskLink){
            $linkUpdate = M('task_input_link')
                ->where([
                    'id' => ['EQ', $hasTaskLink['id']],
                ])
                ->save([
                    'state' => 4,
                    'wx_id' => $userId,
                    'update_time' => time(),
                ]);
            // 需要释放当前任务的其他环节
            M('task_input_link')
                ->where([
                    'taskid' => ['EQ', $taskid],
                    'media_class' => ['EQ', $mediaClass],
                    'state' => ['EQ', 1],
                ])
                ->save([
                    'state' => 0,
                    'wx_id' => 0,
                    'update_time' => time(),
                ]);
        }else {
            $linkUpdate = true;
        }
        if (!$linkUpdate){
            $this->error = '任务流程更新失败';
            return false;
        }
        if (mb_substr($reason, 0, 4) !== '客户反馈'){
            if ($linkType == 1){
                A('InputPc/Task','Model')->change_task_count($userId, 'v3_task_back', 1);
                A('InputPc/Task','Model')->change_task_count($userId, 'input_err', 1);
                A('InputPc/Task','Model')->change_task_count($wx_id,'v3_quality', 1);
            }elseif ($linkType == 2){
                A('InputPc/Task','Model')->change_task_count($userId, 'inspect_err', 1);
                A('InputPc/Task','Model')->change_task_count($userId, 'v3_task_back', 1);
                A('InputPc/Task','Model')->change_task_count($wx_id,'v3_quality', 1);
            }else{
                A('InputPc/Task','Model')->change_task_count($userId, 'v3_task_back', 2);
                A('InputPc/Task','Model')->change_task_count($wx_id,'v3_quality', 2);
            }
            // 更新积分
            $scoreModel = new TaskInputScoreModel();
            if ($linkType == 0){
                $scoreUpdate = $scoreModel->rejectTask($taskid, $mediaClass, 1, $userId, $wx_id);
                $scoreUpdate = $scoreModel->rejectTask($taskid, $mediaClass, 2, $userId, $wx_id);
            }else{
                $scoreUpdate = $scoreModel->rejectTask($taskid, $mediaClass, $linkType, $userId, $wx_id);
            }
        }
        $adName = M('tad')->where(['fadid' => ['EQ', $taskInfo['fadid']]])->getField('fadname');
        A('Adinput/Weixin','Model')->task_back($userId,$reason,$adName);
        A('Common/AliyunLog','Model')->task_input_log([
            'type' => 'save',
            'task' => $taskInfo,
            'media_class' => $mediaClass,
            'reason' =>  $reason,
            'linkType' => $linkType,
            'msg' => '任务被退回',
        ]);

        return true;
    }

    /**
     * @param $taskid
     * @param int $wx_id            当前用户id
     * @param int $mediaClass       媒体类型 1:电视 2: 广播 3: 报纸 13: 网络
     * @param int $linkType         环节类型 1:广告录入环节  2: 违法审核环节
     * @param string $reason        退回原因 如果以 '客户反馈' 开头, 则不计入统计
     * @return bool
     */
    public function rejectTask_20190222($taskid, $wx_id, $mediaClass, $linkType=1, $reason = '') //$linkType环节类型默认为1处理 2019-02-12
    {
        $time = time();
        $taskTable = $mediaClass != 13 ? 'task_input' : 'tnettask';
        $taskInfo = M($taskTable)->find($taskid);
        $userId = $taskInfo['wx_id'];

        if ($linkType == 2){
            $linkTypeText = '违法判定被退回, ';
            $userId = M('task_input_link')
                ->where([
                    'media_class' => ['EQ', $mediaClass],
                    'type' => $linkType,
                    'taskid' => ['EQ', $taskid]
                ])
                ->getField('wx_id');
        }elseif ($linkType == 1){
            $fadname = M('tad')->find($taskInfo['fadid'])['fadname'];
            $linkTypeText = '广告录入被退回, 原广告主题 "'.$fadname.'" ,';
            $userId = M('task_input_link')
                ->where([
                    'media_class' => ['EQ', $mediaClass],
                    'type' => $linkType,
                    'taskid' => ['EQ', $taskid]
                ])
                ->getField('wx_id');
        }else{
            $linkTypeText = '任务被退回';
        }
        $taskData = [
            'quality_wx_id' => $wx_id,
            'task_state' => 4,
            'syn_state' => 0,
            'lasttime' => $time,
            'modifytime' => $time,
            'wx_id' => $userId,
            'back_reason' => $linkTypeText .$reason. ' 退回人:' .M('ad_input_user')->where(['wx_id' => ['EQ', $wx_id]])->cache(true, 60)->getField('alias'),
        ];
        $flowData = [
            'taskid' => $taskid,
            'flow_content' => $linkTypeText . ' ' . $reason,
            'log' => $linkTypeText . ' ' . $reason,
            'flow_time' => $time,
            'createtime' => $time,
            'wx_id' => $wx_id,
            'creator' => $wx_id,
            'task_info' => json_encode($taskInfo),
        ];
        $where = [
            'taskid' => ['EQ', $taskid],
        ];
        $taskUpdate = M($taskTable)->where($where)->save($taskData);
        if (!$taskUpdate){
            $this->error = '任务更新失败';
            return false;
        }
        $flowTable = $mediaClass != 13 ? 'task_input_flow' : 'tnettask_flowlog';
        $flowUpdate = M($flowTable)->add($flowData);
        if (!$flowUpdate){
            $this->error = '任务流程更新失败';
            return false;
        }
        // 更新对应的环节, 先检查是否有对应的环节
        $hasTaskLink = M('task_input_link')
            ->where([
                'taskid' => ['EQ', $taskid],
                'media_class' => ['EQ', $mediaClass],
                'type' => ['EQ', $linkType],
            ])
            ->find();
        if ($hasTaskLink){
            $linkUpdate = M('task_input_link')
                ->where([
                    'id' => ['EQ', $hasTaskLink['id']],
                ])
                ->save([
                    'state' => 4,
                    'wx_id' => $userId,
                    'update_time' => time(),
                ]);
            // 需要释放当前任务的其他环节
            M('task_input_link')
                ->where([
                    'taskid' => ['EQ', $taskid],
                    'media_class' => ['EQ', $mediaClass],
                    'state' => ['EQ', 1],
                ])
                ->save([
                    'state' => 0,
                    'wx_id' => 0,
                    'update_time' => time(),
                ]);
        }else {
            $linkUpdate = true;
        }
        if (!$linkUpdate){
            $this->error = '任务流程更新失败';
            return false;
        }
        if (mb_substr($reason, 0, 4) !== '客户反馈'){
            if ($linkType == 1){
                A('InputPc/Task','Model')->change_task_count($userId, 'v3_task_back', 1);
                A('InputPc/Task','Model')->change_task_count($userId, 'input_err', 1);
                A('InputPc/Task','Model')->change_task_count($wx_id,'v3_quality', 1);
            }elseif ($linkType == 2){
                A('InputPc/Task','Model')->change_task_count($userId, 'inspect_err', 1);
                A('InputPc/Task','Model')->change_task_count($userId, 'v3_task_back', 1);
                A('InputPc/Task','Model')->change_task_count($wx_id,'v3_quality', 1);
            }else{
                A('InputPc/Task','Model')->change_task_count($userId, 'v3_task_back', 2);
                A('InputPc/Task','Model')->change_task_count($wx_id,'v3_quality', 2);
            }
            // 更新积分
            $scoreModel = new TaskInputScoreModel();
            if ($linkType == 0){
                $scoreUpdate = $scoreModel->rejectTask($taskid, $mediaClass, 1, $userId, $wx_id);
                $scoreUpdate = $scoreModel->rejectTask($taskid, $mediaClass, 2, $userId, $wx_id);
            }else{
                $scoreUpdate = $scoreModel->rejectTask($taskid, $mediaClass, $linkType, $userId, $wx_id);
            }
        }
        $adName = M('tad')->where(['fadid' => ['EQ', $taskInfo['fadid']]])->getField('fadname');
        A('Adinput/Weixin','Model')->task_back($userId,$reason,$adName);
        A('Common/AliyunLog','Model')->task_input_log([
            'type' => 'save',
            'task' => $taskInfo,
            'media_class' => $mediaClass,
            'reason' =>  $reason,
            'linkType' => $linkType,
            'msg' => '任务被退回',
        ]);

        return true;
    }

    /**
     * 快剪任务添加到task_input
     * @param Array $taskData 任务数组，支持单条或多条任务
     * @return Int|Boolean $ids 成功则返回新增记录的IDs,失败返回False
     */
    public function cutIntoTaskInput($taskData = []){
        if(is_array($taskData) && !empty($taskData)){
            $c = countArrDimensions($taskData);
            $ids = '';
            if($c == 1){
                // 单条
                $taskRow = [
                    'media_class'       => 2,
                    'media_id'          => $taskData['fmediaid'],
                    'fadid'             => $taskData['fadid'],
                    'sam_id'            => $taskData['sam_id'],
                    'fversion'          => $taskData['fversion'],
                    'fspokesman'        => $taskData['fspokesman'],
                    'fillegaltypecode'  => $taskData['fillegaltypecode'],
                    'source_path'       => $taskData['source_path'],
                    'issue_date'        => $taskData['issue_date'],
                    'fillegalcontent'   => $taskData['fillegalcontent'],
                    'fexpressioncodes'  => $taskData['fexpressioncodes'],
                    'fadmanuno'         => $taskData['fadmanuno'],
                    'fmanuno'           => $taskData['fmanuno'],
                    'fadapprno'         => $taskData['fadapprno'],
                    'fapprno'           => $taskData['fapprno'],
                    'fadent'            => $taskData['fadent'],
                    'fent'              => $taskData['fent'],
                    'fentzone'          => $taskData['fentzone'],
                    'wx_id'             => $taskData['wx_id'],
                    'quality_wx_id'     => $taskData['quality_wx_id'],
                    'quality_state'     => $taskData['quality_state'],
                    'task_state'        => $taskData['task_state'],
                    'lasttime'          => $taskData['lasttime'],
                    'priority'          => $taskData['priority'],
                    'fcreator'          => $taskData['fcreator'] ? $taskData['fcreator'] : '快剪',
                    'fcreatetime'       => date('Y-m-d H:i:s'),
                ];
                $ids = M('task_input')->add($taskRow);
            }elseif($c == 2){
                // 多条
                $dot = '';
                foreach($taskData as $key => $taskInfo){
                    $taskRow = [
                        'media_class'       => 2,
                        'media_id'          => $taskInfo['fmediaid'],
                        'fadid'             => $taskInfo['fadid'],
                        'sam_id'            => $taskInfo['sam_id'],
                        'fversion'          => $taskInfo['fversion'],
                        'fspokesman'        => $taskInfo['fspokesman'],
                        'fillegaltypecode'  => $taskInfo['fillegaltypecode'],
                        'source_path'       => $taskInfo['source_path'],
                        'issue_date'        => $taskInfo['issue_date'],
                        'fillegalcontent'   => $taskInfo['fillegalcontent'],
                        'fexpressioncodes'  => $taskInfo['fexpressioncodes'],
                        'fadmanuno'         => $taskInfo['fadmanuno'],
                        'fmanuno'           => $taskInfo['fmanuno'],
                        'fadapprno'         => $taskInfo['fadapprno'],
                        'fapprno'           => $taskInfo['fapprno'],
                        'fadent'            => $taskInfo['fadent'],
                        'fent'              => $taskInfo['fent'],
                        'fentzone'          => $taskInfo['fentzone'],
                        'wx_id'             => $taskInfo['wx_id'],
                        'quality_wx_id'     => $taskInfo['quality_wx_id'],
                        'quality_state'     => $taskInfo['quality_state'],
                        'task_state'        => $taskInfo['task_state'],
                        'lasttime'          => $taskInfo['lasttime'],
                        'priority'          => $taskInfo['priority'],
                        'fcreator'          => $taskInfo['fcreator'] ? $taskInfo['fcreator'] : '快剪',
                        'fcreatetime'       => date('Y-m-d H:i:s'),
                    ];
                    $id = M('task_input')->add($taskRow);
                    $ids .= $dot.$id;
                    $dot = ',';
                }
            }
            if($ids) return $ids;
        }
        return false;
    }

    /**
     * 质检通过
     * @param string $taskid 任务ID
     * @param string $wx_id 质检人
     * @return boolean 
     */
    public function pass($taskid,$wx_id){
        $time = time();
        $where = [
            'taskid' => ['EQ', $taskid]
        ];
        $taskData = [
            'quality_wx_id' => $wx_id,
            'quality_state' => 2,
            'task_state' => 2,
            'syn_state' => 1,
            'lasttime' => $time,
        ];
        $flowData = [
            'taskid' => $taskid,
            'flow_content' => '质检通过',
            'flow_time' => $time,
            'wx_id' => $wx_id
        ];
        $taskUpdate = M('task_input')->where($where)->save($taskData);
        $flowUpdate = M('task_input_flow')->add($flowData);

        if ($taskUpdate && $flowUpdate){
            A('InputPc/Task','Model')->change_task_count($wx_id, self::QUALITY_COUNT_FIELD, 1);
            return true;
        }else{
            return false;
        }
    }
    /**
     * @Des: 批量调整任务分组、优先级和任务状态
     * @Edt: yuhou.wang
     * @Date: 2020-01-09 17:54:21
     * @param {String|Int} $media_class 媒介类型 1电视 2广播 3报纸
     * @param {String|Array} $uuid UUID
     * @param {Int} $group_id 分组ID
     * @param {Int} $priority 优先级
     * @return: {Array}
     */    
    public function changeTaskMulti($taskData){
        if($taskData){
            foreach($taskData as $key=>$taskInfo){
                $media_class = $taskInfo['media_class'] ? $taskInfo['media_class'] : 1;
                $uuid        = $taskInfo['uuid'] ? $taskInfo['uuid'] : '';
                $group_id    = $taskInfo['group_id'] ? $taskInfo['group_id'] : 0;
                $priority    = $taskInfo['priority'] ? $taskInfo['priority'] : false;
                $ret[] = $this->changeTask($media_class,$uuid,$group_id,$priority);
            }
            return $ret;
        }else{
            return ['code'=>1,'msg'=>'缺少必要参数'];
        }
    }
    /**
     * @Des: 调整任务分组、优先级和任务状态
     * @Edt: yuhou.wang
     * @Date: 2020-01-09 17:54:21
     * @param {String|Int} $media_class 媒介类型 1电视 2广播 3报纸
     * @param {String|Array} $uuid UUID
     * @param {Int} $group_id 分组ID
     * @param {Int} $priority 优先级
     * @param {String} $fmodifier 修改者
     * @param {Array} $linkType 任务环节 默认所有环节
     * @return: {Array}
     */    
    public function changeTask($media_class=1,$uuid,$group_id=0,$priority=false,$fmodifier='changeTask干预',$linkType=[1,2]){
        if($uuid){
            $resetInfo = [
                'task_state'     => 0,
                'wx_id'          => 0,
                'cut_err_state'  => 0,
                'cut_err_reason' => '',
                'quality_wx_id'  => 0,
                'quality_state'  => 0,
                'group_id'       => $group_id,
                'fmodifier'      => $fmodifier
            ];
            if($priority !== false){
                $resetInfo['priority'] = $priority;
            }
            $taskids = M('task_input')->where(['media_class'=>$media_class,'sam_uuid'=>['IN',$uuid]])->getField('taskid',true);
            $msg = '';
            if(empty($taskids)){
                // 若未找到任务则创建新任务
                $samTable = [
                    1 => ['table'=>'ttvsample','key'=>'fid'],
                    2 => ['table'=>'tbcsample','key'=>'fid'],
                    3 => ['table'=>'tpapersample','key'=>'fpapersampleid'],
                ];
                $table = $samTable[(int)$media_class]['table'];
                $key = $samTable[(int)$media_class]['key'];
                $sam_id = M($table)->where(['uuid'=>$uuid])->order('fissuedate DESC')->getField($key);
                $taskids = A('Common/InputTask','Model')->add_task($media_class,$sam_id);
                $msg .= '未找到,已创建新任务ID:'.$taskids.'; ';
            }
            if($taskids){
                // 更新任务
                $retTask = M('task_input')->where(['taskid'=>['IN',$taskids]])->save($resetInfo);
                // 更新环节
                $retLink = M('task_input_link')->where(['media_class'=>$media_class,'taskid'=>['IN',$taskids],'type'=>['IN',$linkType]])->save(['wx_id'=>0,'state'=>0,'update_time'=>time()]);
                $msg .= '更新成功'.$retTask.'条任务及相应'.$retLink.'条环节记录';
                // 日志
                $flowData = [
                    'taskid'       => $taskids,
                    'flow_content' => '接口干预任务:'.$msg,
                    'flow_time'    => time(),
                    'wx_id'        => 0,
                ];
                $flowRes = M('task_input_flow')->add($flowData);
                return ['code'=>0,'msg'=>$msg,'data'=>$taskids];
            }
            return ['code'=>1,'msg'=>'未找到任务,且未创建新任务'];
        }
        return ['code'=>1,'msg'=>'缺少UUID参数'];
    }
    /**
     * @Des: 任务置为方言处理
     * @Edt: yuhou.wang
     * @Date: 2020-02-20 10:46:59
     * @param {type} 
     * @return: 
     */
    public function setDialect($taskid,$linkType=[1,2],$media_class){
        $taskInfo = M('task_input')->where(['taskid'=>$taskid])->find();
        $media_region_id = M('tmedia')->where(['fid'=>$taskInfo['media_id']])->getField('media_region_id');
        if(substr($media_region_id,0,2) == 44){
            // 粤语
            $group_id = 66;
        }else{
            $group_id = substr($media_region_id,0,4);
        }
        $media_class = $taskInfo['media_class'];
        $uuid = $taskInfo['sam_uuid'];
        $priority = false;
        $fmodifier = '转交方言setDialect';
        $ret = $this->changeTask($media_class,$uuid,$group_id,$priority,$fmodifier,$linkType);
        $flowData = [
            'taskid'       => $taskid,
            'flow_content' => '转交方言处理',
            'flow_time'    => time(),
            'wx_id'        => session('wx_id'),
            'task_info'    => json_encode($taskInfo),
        ];
        $flowRes = M('task_input_flow')->add($flowData);
        return $ret;
    }
}