<?php
namespace Open\Controller;
use Think\Controller;

class IssueController extends BaseController {


	/*查询一个媒体一天的数据，通用接口*/
	public function getIssue(){
		$month_date = I('get.month_date');//查询的日期
		$media_id = I('get.media_id');//查询的媒介
		
		
		ini_set('memory_limit','512M');
		$mediaInfo = M('tmedia')
								->cache(true,86400)
								->field('tmediaowner.fregionid,fmedianame,fmediaclassid')
								->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>$media_id))->find();//查询媒介信息
		
		
		
		$mediaclassid = substr($mediaInfo['fmediaclassid'],0,2);
		
		if($mediaclassid == '01'){
			$media_tab = 'tv';
			$table_name = 'ttvissue_'.date('Ym',strtotime($month_date)).'_'.substr($mediaInfo['fregionid'],0,2);
		}elseif($mediaclassid == '02'){
			$media_tab = 'bc';
			$table_name = 'tbcissue_'.date('Ym',strtotime($month_date)).'_'.substr($mediaInfo['fregionid'],0,2);
		}elseif($mediaclassid == '03'){
			$media_tab = 'paper';
			$table_name = 'tpaperissue';
		}else{//这里处理媒介错误
			
		}


		if($media_tab == 'tv' || $media_tab == 'bc'){
			
			try{//
				$data2List = M($table_name)
							->alias('tissue')
							//->cache(true,1200)
							->field('
									tissue.fstarttime,
									tissue.fendtime,
									tissue.fmediaid,
									tad.fadname,
									(select fname from tadowner where fid = tad.fadowner) as fadownername,
									tad.fbrand,
									tsample.fissuedate as sam_issuedate,
									tsample.fadlen as sam_adlen,
									
			
									tsample.fspokesman,
									
									tsample.fversion,
									tad.fadclasscode,
									tissue.fissuedate,
									tsample.fillegaltypecode,
									tsample.fexpressioncodes,
									tsample.fillegalcontent,
									tsample.favifilename as sam_source_path
									
									
									')

							->join('t'.$media_tab.'sample as tsample on tsample.fid = tissue.fsampleid')
							->join('tad on tad.fadid = tsample.fadid')
							->where(array('tissue.fmediaid'=>$media_id,'tissue.fissuedate'=>strtotime($month_date)))
							->select();	
			}catch( \Exception $error) {//
				$data2List = array();
			}	
			  
			
		}elseif($media_tab == 'paper'){
		
			
			$data2List = M($table_name)
							->alias('tissue')
							//->cache(true,1200)
							->field('
									0 as fstarttime ,
									0 as fendtime,
									tissue.fmediaid,
									tissue.fpage,
									tad.fadname,
									(select fname from tadowner where fid = tad.fadowner) as fadownername,
									tad.fbrand,
									tsample.fissuedate as sam_issuedate,
									0 as sam_adlen,
									tsample.fspokesman,
									tsample.fversion,
									tad.fadclasscode,
									UNIX_TIMESTAMP(tissue.fissuedate) as fissuedate,
									tsample.fillegaltypecode,
									tsample.fexpressioncodes,
									tsample.fillegalcontent,
									tsample.fjpgfilename as sam_source_path
									')

							->join('tpapersample as tsample on tsample.fpapersampleid = tissue.fpapersampleid')
							->join('tad on tad.fadid = tsample.fadid')
							->where(array('tissue.fmediaid'=>$media_id,'tissue.fissuedate'=>$month_date))
							->select();	
								
			
			
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','issueList'=>$data2List));
	
	
	
		
	
	
	}
	
	public function trend(){
		header("Content-type: text/html; charset=utf-8"); 
		header('Access-Control-Allow-Origin:*');
		$mediaId = I('mediaId');//媒介ID
		$startDate = I('startDate');//开始日期
		$endDate = I('endDate');//结束日期
		
		$mediaInfo = M('tmedia')
								->cache(true,600)
								->field('
											fid,
											fmedianame,
											left(fmediaclassid,2) as media_class,
											media_region_id
										')
								->where(array('fid'=>$mediaId))
								->find();//查询媒体详情
		
		//var_dump($mediaInfo);
		if($mediaInfo['media_class'] == '01'){//电视
			$prefixTable = 'ttvissue_';
		}elseif($mediaInfo['media_class'] == '02'){//广播
			$prefixTable = 'tbcissue_';
		}else{
			
		}
		
		$dateList = array();
		for($i=strtotime($startDate);$i<=strtotime($endDate);$i+=86400){//循环日期
			
			$issueTable = $prefixTable.date('Ym',$i).'_'.substr($mediaInfo['media_region_id'],0,2);//发布记录表名称
			
			
			$issueList = M($issueTable)
									//->field('hour(FROM_UNIXTIME(fstarttime)) as hour,count(*) as count')
									->where(array('fmediaid'=>$mediaInfo['fid'],'fissuedate'=>$i))
									->group('hour(FROM_UNIXTIME(fstarttime))')
									->getField('hour(FROM_UNIXTIME(fstarttime)) as hour,count(*) as count');//查询发布记录，按小时分组
									
									
			
			$na_url = 'http://47.96.182.117/manage/channel/getChannelServiceTime';//查询闭台数据的地址
			$ret = http($na_url,array('channel'=>$mediaInfo['fid'],'date'=>date('Y-m-d',$i)));//去远程查询
			$ret = json_decode($ret,true);//查询结果转化为数组			
			$data = array();
			$data['date'] = date('Y-m-d',$i);//日期
			$data['abnormal'] = array();//异常数据
			$data['hours'] = array();
			for($h=0;$h<24;$h++){
				$data['hours'][$h] = intval($issueList[$h]);//设置每个小时的发布数量
			}
			
			foreach($ret['dataList'] as $rr){//循环查到的结果，判断是否包含查询时间
				//var_dump($rr); 
				$abnormal = array();
				$abnormal['start'] = $rr['start'];
				$abnormal['end'] = $rr['end'];
				$abnormal['start_time'] = $rr['start_time'];
				$abnormal['end_time'] = $rr['end_time'];
				
				if($rr['type'] == 1){
					$abnormal['title'] = '停台';
					$abnormal['color'] = '颜色代码';
				}elseif($rr['type'] == 2){
					$abnormal['title'] = '停机检修';
					$abnormal['color'] = '颜色代码';
				}	
				$data['abnormal'][] = $abnormal;
			}
			
			
			$dateList[] = $data;
			
			
			
			//var_dump($issueList);
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'','dataList'=>$dateList));
		
	}
	
	
	


}