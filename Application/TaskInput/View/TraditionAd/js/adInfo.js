let adInfo = {
  name: 'adInfo',
  props: {
    task: {
      type: Object,
      default: () => ({})
    },
    type: {
      type: String,
      default: ''
    },
    fromManage: {
      type: Boolean,
      default: false
    },
    showButton: {
      type: Boolean,
      default: true
    }
  },
  template: `<div class="adInfo" v-loading="loading">
  <el-form ref="adInfo" :model="task" :label-width="this.task.media_class == 13 ? '80px' : '120px'" :rules="rule"
  :disabled="disabled">
      <el-form-item label="临时广告名称" v-if="task.tem_ad_name">
        {{task.tem_ad_name}}
      </el-form-item>
      <el-form-item label="广告名称" prop="fadname">
        <el-autocomplete v-model="task.fadname" :fetch-suggestions="task.taskid && searchAdName" @input="inputAdName" :placeholder="task.fadname" @focus="() => $emit('focus')" @blur="adNameBlur">
        </el-autocomplete>
      </el-form-item>
      <el-form-item>
        <a :href="'https://www.baidu.com/s?wd=' + task.fadname" target="_blank">百度搜索此广告名</a>
      </el-form-item>
      <el-form-item label="广告品牌" prop="fbrand">
        <el-input v-model="task.fbrand" :disabled="task.adIsSure" @focus="() => $emit('focus')" @blur="() => $emit('blur')">
          <el-button slot="append" @click="appendAbBrand" v-if="type === 'input' && !task.adIsSure">无突出品牌</el-button>
        </el-input>
      </el-form-item>
      <el-form-item label="广告主" prop="fname" class="jcsb">
        <el-autocomplete v-model="task.fname" :fetch-suggestions="searchAdOwner" :disabled="task.adIsSure" @focus="() => $emit('focus')" @blur="() => $emit('blur')">
          <el-button slot="append" @click="setAdOwner('广告主不详')" v-if="type === 'input' && !task.adIsSure">广告主不详</el-button>
        </el-autocomplete>
      </el-form-item>
      <el-form-item label="商业类别" prop="adCateArr2">
        <el-cascader @change="changeFormAdClass2" :options="adClassTree" v-model="task.adCateArr2" ref="adCateArr2" @focus="() => $emit('focus')" @blur="() => $emit('blur')"
          :filterable="true" :disabled="task.adIsSure"></el-cascader>
        <span v-else>{{task.adCateText2}}</span>
      </el-form-item>
      <el-form-item label="广告类别" prop="adCateArr">
        <el-cascader :options="adClassArr" v-model="task.adCateArr" ref="adCateArr" :filterable="true" @focus="() => $emit('focus')" @blur="() => $emit('blur')"
          :disabled="task.adIsSure"></el-cascader>
      </el-form-item>
      <el-form-item v-if="showClassTip">
        <span style="color: red;font-size: 12px;line-height: 20px;">检测到“公益”，请确认是否为公益广告并选择正确广告类别</span>
      </el-form-item>
      <el-form-item label="是否为栏目广告" v-if="task.media_class == 1 || task.media_class == 2" prop="is_long_ad">
        <el-radio-group v-model="task.is_long_ad">
          <el-radio label="0">否</el-radio>
          <el-radio label="1">是</el-radio>
        </el-radio-group>
      </el-form-item>
      <el-form-item label="代言人">
        <el-input v-model="task.fspokesman" @focus="() => $emit('focus')" @blur="() => $emit('blur')"></el-input>
      </el-form-item>
      <el-form-item label="版本说明">
        <el-input v-model="task.fversion" @focus="() => $emit('focus')" @blur="() => $emit('blur')"></el-input>
      </el-form-item>
      <el-form-item v-if="type === 'input' && showButton">
        <el-button type="primary" @click="onSubmit">提交</el-button>
        <el-button type="success" @click="changeLanguage" v-if="task.media_class !== '13'">转交方言</el-button>
      </el-form-item>
      </el-form>
      <div style="text-align: center" v-if="!showButton && type != 'input'">
        <el-button type="primary" @click="Disabled = false">编辑信息</el-button>
      </div>
  </div>`,
  data: function () {
    const validateAdBrand = (rule, value, callback) => {
      if (!value) {
        callback()
        return false
      }
      if (value.indexOf('&') === -1 && value !== '无突出品牌' && value.indexOf('无突出品牌') !== -1) {
        callback(new Error('如果无突出品牌, 此栏就只允许填写无突出品牌'))
      }
      // 获取广告名称, 取出广告名称中有多少个&
      let adNameAndCount = this.task.fadname.match(/&/g)
      adNameAndCount = adNameAndCount ? adNameAndCount.length : 0
      let adBrandAndCount = this.task.fbrand.match(/&/g)
      adBrandAndCount = adBrandAndCount ? adBrandAndCount.length : 0
      if (adNameAndCount < adBrandAndCount && this.task.fadclasscode.substr(0,2) !== '04') {
        callback(new Error('广告名称中的&数量与广告品牌中的&数量不同'))
      }
      // 品牌可能是多个
      if (value.indexOf('&') !== -1) {
        if (value.split('&').some(v => v !== '无突出品牌' && !v.match(/^.牌|&.牌/) && v.indexOf('牌') !== -1)) {
          this.$message('请确认录入是否正确, 一般情况下品牌中不需要输入XX牌')
        }
      } else {
        if (!value.match(/^.牌|&.牌/) && value !== '无突出品牌' && value.indexOf('牌') !== -1) {
          this.$message('请确认录入是否正确, 一般情况下品牌中不需要输入XX牌')
        }
      }
      if (value.match(/第\D届/)) {
        this.$message('请确认录入是否正确, 届数需要用数字')
      }
      if (value.match(/\D年/)) {
        this.$message('请确认录入是否正确, 年份需要用数字')
      }
      if (value.match(/&$/)) {
        callback('广告品牌不允许以&结尾')
      }
      callback()
    }
    const validateAdOwner = (rule, value, callback) => {
      // if (value && value.indexOf('广告主不详') !== -1 && value !== '广告主不详') {
      //   callback(new Error('如果广告主不详, 此栏就只允许填写广告主不详'))
      // }
      callback()
    }
    const validateAdName = (rule, value, callback) => {
      if (!value) {
        callback()
        return
      }
      // 广告可能是多个
      if (value && value.indexOf('牌') !== -1) {
        this.$message('请确认录入是否正确, 一般情况下广告名称中不需要输入XX牌')
      }
      if (value.match(/第\D届/)) {
        this.$message('请确认录入是否正确, 届数需要用数字')
      }
      if (value.match(/\D年/)) {
        this.$message('请确认录入是否正确, 年份需要用数字')
      }
      if (value.match(/&$/)) {
        callback('广告名称不允许以&结尾')
      }
      callback()
    }
    return {
      loading: false,
      showClassTip: false,
      Disabled: true,
      rule: {
        fadname: [{
            required: true,
            message: '请填写广告名称',
            trigger: 'submit'
          },
          {
            validator: validateAdName,
            trigger: 'change'
          },
        ],
        fname: [{
            required: true,
            message: '请填写广告主',
            trigger: 'submit'
          },
          {
            validator: validateAdOwner,
            trigger: 'change'
          },
        ],
        fbrand: [{
            required: true,
            message: '请输入品牌',
            trigger: 'submit'
          },
          {
            validator: validateAdBrand,
            trigger: 'change'
          },
        ],
        adCateArr2: [{
          required: true,
          message: '请选择商业类别',
          trigger: 'submit'
        }],
        adCateArr: [{
          required: true,
          message: '请选择广告类别',
          trigger: 'submit'
        }],
        is_long_ad: [{
          required: true,
          message: '请选择',
          trigger: 'submit'
        }],
      },
      adClassTree,
      illegalTypeArr,
      adClassArr,
      illegalArr,
      sourceTask: ''
    }
  },
  computed: {
    disabled() {
      return !this.showButton && this.Disabled && this.type != 'input'
    },
    adType: function () {
      const obj = {
        '1': 'tv',
        '2': 'bc',
        '3': 'paper'
      }
      return obj[this.task.media_class] || null
    },
  },
  watch: {
    'task.taskid': {
      handler() {
        this.sourceTask = JSON.stringify(this.task)
      },
    }
  },
  created() {
    this.sourceTask = JSON.stringify(this.task)
  },
  methods: {
    checkChange(arr) {
      let flag = false
      const source = JSON.parse(this.sourceTask)
      arr.forEach(i => {
        if (source[i] != this.task[i]) {
          flag = true
        }
      })
      return flag
    },
    adNameBlur() {
      this.showClassTip = $h.testAdName(this.task.fadname)
      this.$emit('blur')
    },
    searchAdName: function (queryString, cb) {
      const data = {
        adName: queryString
      }
      if(this.task.media_class == 13) data.fpriority = "1|13"
      $.post("/Api/Ad/searchAdByName", data).then((res) => {
        cb(res.data);
      })
    },
    searchAdOwner: function (queryString, cb) {
      $.post("/Api/Adowner/searchAdOwner", {
        name: queryString
      }).then((res) => {
        cb(res.data);
      })
    },
    inputAdName: function () {
      this.$emit('inputAdName')
      if (!this.task.fadname) {
        return false
      }
      $.post('/Api/Ad/checkAdIsSureByName', {
        adName: this.task.fadname
      }).then((res) => {
        if (res.data.is_sure != 0) {
          this.task.adIsSure = true
        } else {
          this.task.adIsSure = false
        }
        // 只要找到广告信息, 就更新到任务中
        if (res.data.fadid) {
          this.task = Object.assign(this.task, res.data)
          this.task.adCateArr = this._processAdClass(this.task.fadclasscode)
          this.task.adCateArr2 = this._processAdClass(this.task.fadclasscode_v2)
        }
        this.$forceUpdate()
      })
    },
    _processAdClass: function (data) {
      let adClass = [];
      if (data.length === 6) {
        adClass.push(data.substr(0, 2))
        adClass.push(data.substr(0, 4))
        adClass.push(data)
      }
      if (data.length === 4) {
        adClass.push(data.substr(0, 2))
        adClass.push(data)
      }
      if (data.length === 2) {
        adClass.push(data)
      }
      return adClass
    },
    // 更新视图中的广告分类
    _updateAdCateArr: function (adCateArr, fadclasscode) {
      if (adCateArr) {
        Vue.set(this.task, 'adCateArr', adCateArr)
      }
      if (fadclasscode) {
        Vue.set(this.task, 'fadclasscode', fadclasscode)
      }
    },
    appendAbBrand: function () {
      if (!this.task.fbrand) {
        this.task.fbrand = '无突出品牌'
        this.$forceUpdate()
        return true
      }
      if (this._validateAdBrand()) {
        this.task.fbrand += '&无突出品牌'
        this.$forceUpdate()
      } else {
        this.$message.warning('品牌中的&数量不得超过广告名称中&的数量')
      }
    },
    setAdOwner: function (name) {
      this.task.fname = name
      this.$forceUpdate()
    },
    _validateAdBrand: function () {
      if (!this.task.fadname) {
        this.$message.warning('请输入广告名称')
        return false
      }
      if (!this.task.fbrand) {
        this.task.fbrand = ''
        return false
      }
      // 获取广告名称, 取出广告名称中有多少个&
      let adNameAndCount = this.task.fadname.match(/&\S/g)
      adNameAndCount = adNameAndCount ? adNameAndCount.length : 0
      let adBrandAndCount = this.task.fbrand.match(/&/g)
      adBrandAndCount = adBrandAndCount ? adBrandAndCount.length : 0
      // 只有品牌中的&数量小于等于名称中的&数量, 才允许添加
      return adNameAndCount > adBrandAndCount
    },
    changeFormAdClass2: function (value) {
      var code = value[value.length - 1]
      Vue.set(this.task, 'fadclasscode_v2', code)
    },
    changeLanguage() {
      this.$confirm('确认转交为方言处理?', '提示', {
        confirmButtonText: '继续',
        cancelButtonText: '取消',
        type: 'success',
      }).then(() => {
        $.post('/TaskInput/TraditionAd/setDialect', {
        taskid: this.task.taskid
      }).then((res) => {
        if(res.code == 0) {
            this.$emit('nextTask')
          }
        }).catch(err => {
          this.$message.error(err.msg)
        })
      }).catch(err => {
      })
    },
    initData() {
      return new Promise((resolve, reject) => {
        if (this.adType === 'paper' && !this.task.paperIsSure) {
          this.$message.warning('请确认版面无误后点击报纸上方查看广告按钮')
          reject()
        } else {
          this.$refs['adInfo'].validate(res => {
            if (res) {
              if (this.loading) {
                this.$message.warning('正在提交中, 请不要重复提交')
                reject()
              }
              this.task.fadclasscode = this.task.adCateArr[this.task.adCateArr.length - 1]
              if (!$h.checkAdNameAndCode(this.task.fadname, this.task.fadclasscode)) {
                this.loading = false
                this.$message.error('公益广告请按此格式填写：公益（xxxxxx）')
                reject()
              }
              if (this.task.fadclasscode == 2301) {
                this.loading = false
                this.$alert('当前广告分类非法, 请选择另一个分类')
                reject()
              }
              this.task.fadclasscode_v2 = this.task.adCateArr2[this.task.adCateArr2.length - 1]
              this.task.fadname = this.task.fadname.replace(/＆/g, '&')
              this.task.fbrand = this.task.fbrand.replace(/＆/g, '&')
              Vue.set(this.task, 'fbrand', this.task.fbrand)
              Vue.set(this.task, 'fadclasscode', this.task.fadclasscode)
              if (!this.task.fadclasscode_v2) {
                Vue.set(this.task, 'fadclasscode_v2', 3203)
              }
              if (!this.showButton) {
                const checkParams = ['fadname', 'fadowner', 'fversion', 'fbrand', 'fadclasscode', 'fadclasscode_v2', 'fadowner', 'fspokesman']
                const change = this.checkChange(checkParams)
                this.$set(this.task, 'change', change)
              }
              this.Disabled = true
              resolve(this.task)
            }
          })
        }
      })
    },
    onSubmit() {
      this.initData().then(data => {
        let text = '确认提交?'
        this.$confirm(text).then(() => {
          this.loading = true
          $.post('submitAdInputTask', { data }).then((res) => {
            if (res.code == 0) {
              if (this.fromManage) {
                return this.$emit('close')
              }
              this.task.adCateText2 = res.adCateText2
              this.task.adCateText1 = res.adCateText1
              const adclass = data.fadclasscode.substr(0, 2)
              if (adclass === '22' || adclass === '23') {
                this.$emit('nextTask')
              } else {
                this.$emit('nextTask')
                // this.$emit('next',res.isNeedNextStep)
              }
            } else {
              this.$message({
                message: res.msg,
                type: 'error'
              })
              this.$emit('error')
            }
            this.loading = false
          })
        }).catch(() => {})
      })
    },

  }
}