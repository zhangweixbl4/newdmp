const adInfo = {
    template: `<div v-loading="loading">
<el-row class="issueContent">
    <el-col :span="4" class="issueList">
        <div class="h4">发布日期</div>
        <el-checkbox :indeterminate="isIndeterminate" v-model="checkAll" @change="handleCheckAllChange">全选</el-checkbox>
        <el-checkbox-group v-model="selectedIssue" @change="issueChange" v-for="(item, index) in adInfo.issueList" :key="index">
            <el-checkbox  :label="index" border>{{item.issue_date}} {{item.media_name}}</el-checkbox >
        </el-checkbox-group>
    </el-col>
    <el-col :span="10" class="ad-sample">
        <el-tabs type="border-card" @tab-click="changeTab" v-if="adInfo.adType === 'tv' || adInfo.adType === 'bc'" v-model="active1">
            <el-tab-pane label="发布场景" name="first">
                <div v-if="activeIssue !== null">
                    <video id="player" class="video-js" controls>
                        <source
                                src="http://118.31.11.174:18081/icloud/qiniu/playback.m3u8?tk=5ImWefotsFDCRLwnB7fpaeJzop1DYLdt50Wlr-SgHDVV0Uqr4dsKLw=="
                                type="application/x-mpegURL">
                    </video>
                </div>
                <div class="h4" v-else>请选择左边的发布记录</div>
            </el-tab-pane>
            <el-tab-pane label="样本内容" name="second">
                <video :src="adInfo.favifilename" controls v-if="adInfo.adType === 'tv'"></video>
                <audio :src="adInfo.favifilename" controls v-else></audio>

            </el-tab-pane>
        </el-tabs>
        <div v-if="adInfo.adType === 'paper'">
            <div class="text-center">
                <el-button type="info" @click="togglePaperUrl">切换版面/广告</el-button>
            </div>
            <img :src="adInfo.fjpgfilename" alt="" v-if="!showPaper">
            <img :src="adInfo.paperUrl" alt="" v-else>
        </div>
    </el-col>
    <el-col :span="10" class="adInfo">
        <el-row>
            <el-col :span="24">
                <el-button type="primary" @click="confirmSelected(9)">确认已选中的发布记录</el-button>
                <el-button type="warning" @click="confirmSelected(-1)">退回已选中的发布记录</el-button>
            </el-col>
        </el-row>
        <el-row>
            <el-col :span="6"><b>媒体名称</b></el-col>
            <el-col :span="16"><b>{{adInfo.media_name}}</b></el-col>
        </el-row>
        <el-row>
            <el-col :span="6"><b>广告类别</b></el-col>
            <el-col :span="16">{{adInfo.ad_class}}</el-col>
        </el-row>
        <el-row>
            <el-col :span="6"><b>广告主</b></el-col>
            <el-col :span="16">{{adInfo.fadowner}}</el-col>
        </el-row>
        <el-row>
            <el-col :span="6"><b>广告品牌</b></el-col>
            <el-col :span="16">{{adInfo.fbrand}}</el-col>
        </el-row>
        <el-row>
            <el-col :span="6"><b>代言人</b></el-col>
            <el-col :span="16">{{adInfo.fspokesman}}</el-col>
        </el-row>
        <el-row>
            <el-col :span="6"><b>版本说明</b></el-col>
            <el-col :span="16">{{adInfo.fversion}}</el-col>
        </el-row>
        <el-row>
            <el-col :span="6"><b>违法内容</b></el-col>
            <el-col :span="16">{{adInfo.fillegalcontent}}</el-col>
        </el-row>
        <el-row>
            <el-col :span="6"><b>违法表现代码</b></el-col>
            <el-col :span="16">{{adInfo.fillegal_code}}</el-col>
        </el-row>
        <el-row>
            <el-col :span="6"><b>违法表现</b></el-col>
            <el-col :span="16"><ol ><li v-for="(item,index) in adInfo.fexpressions" :key="item"><b>{{index + 1}}. </b>{{item}}</li></ol></el-col>
        </el-row>
        <el-row>
            <el-col :span="6"><b>认定依据</b></el-col>
            <el-col :span="16"><ol><li v-for="(item, index) in adInfo.fconfirmations" :key="item"><b>{{index + 1}}. </b>{{item}}</li></ol></el-col>
        </el-row>
        <div class="text-center">
            <el-button type="warning" @click="submitAdError">广告信息或判定有误</el-button>
        </div>
    </el-col>
</el-row>
</div> 
    `,
    props: ['ad'],
    data: function () {
        return {
            adInfo: {},
            loading: false,
            activeIssue: null,
            viewerInstance: {
                destroy: function () {}
            },
            active1: 'first',
            hasAddAuth: false,
            player: {},
            selectedIssue: [],
            checkAll: false,
            isIndeterminate: true,
            showPaper: false , // 是否展示版面
        }
    },
    methods:{
        processData: function () {
            this.adInfo = this.ad
            this.showPaper = false
            if (this.adInfo.adType === 'paper'){
                this.$nextTick( () => {
                    try {
                        this.viewerInstance.destroy()
                        this.viewerInstance = new Viewer(document.querySelector('.ad-sample img'), {
                            inline: true,
                            // toolbar: false,
                            navbar: false
                        })
                    }catch (e) {}
                })
            }
            this.loading = false
            this.selectedIssue = []
            this.isIndeterminate = true
            this.checkAll = false
            this.activeIssue = null
            if (this.player.dispose){
                this.player.dispose()
            }
            this.player = {}
            this.active1 = 'first'
        },
        handleCheckAllChange(val) {
            this.selectedIssue = []
            if (val){
                let i = 0
                while (this.selectedIssue.length < this.adInfo.issueList.length){
                    this.selectedIssue.push(i)
                    i++
                }

            }
            this.isIndeterminate = false;
        },
        issueChange: function (value) {
            if (value.length === 0){
                return false
            }
            this.activeIssue = value.pop()
            value.push(this.activeIssue)
            value = this.adInfo.issueList[this.activeIssue]
            let data = {
                mediaId: value.media_id,
                startTime: moment(value.start_time).unix(),
                endTime: moment(value.end_time).unix(),
            }
            if (this.adInfo['fmedia_class'] != 3){
                $.post('getM3U8Url', data).then((res) => {
                    this.player = videojs('player')
                    this.player.src(res.data)
                    this.player.play()
                })
            }
            let checkedCount = value.length;
            this.checkAll = checkedCount === this.adInfo.issueList.length;
            this.isIndeterminate = checkedCount > 0 && checkedCount < this.adInfo.issueList.length;
        },
        changeTab: function () {
            try {
                this.$nextTick(() => {
                    $('video').each(function () {
                        this.pause()
                    })
                    $('audio').each(function () {
                        this.pause()
                    })
                    if (this.player.pause) {
                        this.player.pause()
                    }
                })
            }catch (e) {
                
            }
        },
        confirmSelected: function (res) {
            if (this.selectedIssue.length === 0){
                return this.$alert('请至少选择一项')
            }
            let text = ''
            if (res == 1){
                text = '是否确认已选中的' + this.selectedIssue.length + '项 ? '
            } else {
                text = '是否退回已选中的' + this.selectedIssue.length + '项 ? '
            }
            this.$confirm(text).then(() => {
                this.loading = true
                const data = {
                    ids: this.selectedIssue.map(v => this.adInfo.issueList[v].fid).join(','),
                    res: res,
                }
                $.post('confirmIssue', data).then((res) => {
                    if (res.code == 0){
                        this.$message.success(res.msg)
                    }else{
                        this.$message.warning(res.msg)
                    }
                    this.$emit('success')
                    this.loading = false
                })
            }).catch(() => {})
        },
        togglePaperUrl: function () {
            this.showPaper = !this.showPaper
            this.$nextTick( () => {
                try {
                    this.viewerInstance.destroy()
                    this.viewerInstance = new Viewer(document.querySelector('.ad-sample img'), {
                        inline: true,
                        toolbar: false,
                        navbar: false
                    })
                }catch (e) {}
            })
        },
        submitAdError: function () {
            this.$confirm('提交此广告信息或判定错误?').then(() => {
                this.loading = true
                $.post('submitAdError', {
                    adId: this.adInfo.fid
                }).then((res) => {
                    this.loading = false
                    if (res.code == 0) {
                        this.$message.success(res.msg)
                    } else {
                        this.$message.warning(res.msg)
                    }
                    this.$emit('success')
                })
            })
        }
    }
}