<?php
namespace TaskInput\Controller;
use Common\Model;
use TaskInput\Model\AdInputUserModel;
use TaskInput\Model\OutAdModel;
use TaskInput\Model\TaskInputModel;
use TaskInput\Model\TaskInputScoreModel;
use Think\Exception;
use Think\Page;

/**
 * 互联网广告任务控制器
 */
class OutAdController extends BaseController {
    const CUT_ERROR_COUNT_FIELD = 'v3_net_cut_err';
    const TASK_BACK_COUNT_FIELD = 'v3_task_back';
    const QUALITY_COUNT_FIELD = 'v3_quality';
    const USER_AUTH_CODE = '1006';
    const QUALITY_INSPECTOR_AUTH_CODE = '1007';
    const PREVIEW_AUTH_CODE = '1008';     // 预览权限
    const ILLEGAL_REVIEW_COMMENT_AUTH_CODE = '3001';
    const ILLEGAL_REVIEW_COMMENT_CONFIRM_AUTH_CODE = '3002';
    // 任务总环节
    // protected $taskLinks = [1,2];

    protected $TASK_COUNT_FIELD_MAP = [
        '1' => 'v3_tv_task',
        '2' => 'v3_bc_task',
        '3' => 'v3_paper_task',
        '5' => 'v3_out_task',
        '13' => 'v3_net_task',
    ];

    /**
     * 接收参数
     */
    protected $P;

    /**
     * 初始化
     */
    public function _initialize(){
        $this->P = json_decode(file_get_contents('php://input'),true);
    }

    /**
     * 户外广告预审补分
     */
    public function updateuserscore(){
        $where_ok['a.fadsource_id'] = 0;
        $where_ok['a.task_state'] = 2;
        $where_ok['a.wx_id'] = ['gt',0];
        $taskInfo = M('todtask')
            ->field('taskid,check')
            ->alias('a')
            ->join('tod_ad_info b on a.fadinfo_id = b.fid')
            ->where($where_ok)
            ->select();
        $scoreModel = new TaskInputScoreModel();
        $data = [];
        foreach ($taskInfo as $key => $value) {
            $scoreModel->addScoreByOdCheckTask($value['taskid'], 1,$value['check']);
            $data[] = $value['taskid'];
        }
        dump($data);
    }

    /**
     * 开始任务
     */
    public function index()
    {
        // session('wx_id',692);
        if(!A('InputPc/Task','Model')->user_other_authority(self::PREVIEW_AUTH_CODE)){
            echo '<h1>无权限,请联系管理员</h1>';
            return false;
        }
        $wx_id = session('wx_id');
        // 如果参数存在taskid, 则是重做任务, 否则是获取新任务
        if (I('taskid')){
            $taskid = I('taskid');
        }else{
            $taskid = (new OutAdModel())->getTask();
        }
        // 处理错误情况
        if (empty($taskid)){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '未找到任务'
            ]);
        }
        // 任务基本信息
        $taskData = M('todtask')->master(true)->find($taskid);

        if($taskData['fadsource_id']>0){
            $do_lon = M('tod_lat_and_lon')->field('fillegaltypecode,fillegalcontent,fexpressioncodes,fexpressions,fconfirmations,determine_time')->where(['source_id'=>$taskData['fadsource_id']])->find();
            $taskData['illdata']['fillegaltypecode'] = $do_lon['fillegaltypecode'];
            $taskData['illdata']['fillegalcontent'] = $do_lon['fillegalcontent'];
            $taskData['illdata']['fexpressioncodes'] = $do_lon['fexpressioncodes'];
            $taskData['illdata']['fexpressions'] = $do_lon['fexpressions'];
            $taskData['illdata']['fconfirmations'] = $do_lon['fconfirmations'];
            $taskData['illdata']['determine_time'] = $do_lon['determine_time'];
        }

        // 获取用户环节权限，如:['1','2']
        $linkTypes = (new AdInputUserModel())->getTaskLinkAuth($wx_id);
        // 任务总环节定义 2019-02-25后取消互联网任务环节表，环节通过tnettask表的task_state状态码进行体现
        $taskState = str_split($taskData['task_state']);
        if(count($taskState) < 2){
            array_unshift($taskState,1);//历史任务状态为一位数时，插入环节，拼装环节两位数
        }
        $taskData['linkTypeArr'] = [];
        $taskLink = [];

        foreach($linkTypes as $key => $val){
            $linkState = 0;
            if($val < $taskState[0]){
                $linkState = 2;
            }elseif($val > $taskState[0]){
                $linkState = 1;
            }elseif($val = $taskState[0]){
                if(isset($taskState[1])){
                    $linkState = $taskState[1];
                }else{
                    $linkState = 2;//任务最终完成状态
                }
            }
            $taskLink = [
                'type' => (string)$val,
                'state' => (string)$linkState
            ];
            array_push($taskData['linkTypeArr'],$taskLink);
        }

        //初始化参数
        $taskData['upunit_info'] = [];
        $taskData['owner_info'] = [];
        $taskData['ad_info'] = [];
        $taskData['ad_lat'] = [];
        $taskData['videofile'] = [];
        $taskData['imgfile'] = [];
        $taskData['otherfile'] = [];
        $taskData['check_data'] = [];
        $taskData['check_imgfile'] = [];
        //广告信息
        $addata = M('tod_ad_info') -> where(['fid' => $taskData['fadinfo_id']])->find();

        //预审数据
        $taskData['pretrial']['check'] = $addata['check'];
        $taskData['pretrial']['pretrial_time'] = $addata['pretrial_time'];
        $taskData['pretrial']['pretrial_count'] = $addata['pretrial_count'];
        $taskData['pretrial']['pretrial_result'] = $addata['pretrial_result'];

        if(!empty($addata)){
            $taskData['is_car'] = $addata['is_car'];
            $data = [];
            $data[] = ['fieldname' => '企业名称', 'fieldvalue' => $addata['upunit_name']];
            $data[] = ['fieldname' => '联系人', 'fieldvalue' => $addata['upunit_person']];
            $data[] = ['fieldname' => '企业注册号', 'fieldvalue' => $addata['upunit_num']];
            $data[] = ['fieldname' => '联系电话', 'fieldvalue' => $addata['upunit_phone']];
            $data[] = ['fieldname' => '所属区域', 'fieldvalue' => $addata['upunit_area']];
            $data[] = ['fieldname' => '详细地址', 'fieldvalue' => $addata['upunit_address']];
            $taskData['upunit_info'] = $data;

            $data = [];
            $data[] = ['fieldname' => '广告主', 'fieldvalue' => $addata['manage_name']];
            $data[] = ['fieldname' => '联系方式', 'fieldvalue' => $addata['manage_phone']];
            $data[] = ['fieldname' => '企业法人', 'fieldvalue' => $addata['manage_person']];
            $data[] = ['fieldname' => '企业类别', 'fieldvalue' => $addata['manage_classname']];
            $taskData['owner_info'] = $data;

            $data = [];
            $data[] = ['fieldname' => '广告名称', 'fieldvalue' => $addata['name']];
            $data[] = ['fieldname' => '广告类型', 'fieldvalue' => $addata['ad_typename']];
            $data[] = ['fieldname' => '广告内容类别', 'fieldvalue' => $addata['class_name']];
            $taskData['ad_info'] = $data;

            $where_lat['ad_id'] = $addata['id'];
            if(!empty($taskData['fadsource_id'])){
                $where_lat['source_id'] = $taskData['fadsource_id'];
            }
            $do_lat = M('tod_lat_and_lon') -> where($where_lat) ->select();
            if($addata['is_car'] == 2){
                $data = [];
                $upunit_name = [];
                $data[] = ['fieldname' => '发布期限', 'fieldvalue' => date('Y-m-d',$do_lat[0]['s_date']).'~'.date('Y-m-d',$do_lat[0]['e_date'])];
                $data[] = ['fieldname' => '广告形式', 'fieldvalue' => $do_lat[0]['form']];
                foreach ($do_lat as $key => $value) {
                    $upunit_name[] = $value['traffic_id'];
                }
                $data[] = ['fieldname' => '广告发布地址', 'fieldvalue' => $upunit_name];
                $number = $do_lat[0]['number'];
                if(!empty($do_lat[0]['cars'])){
                    $number .= '  '.$do_lat[0]['cars'].'（辆）';
                }
                if(!empty($do_lat[0]['area'])){
                    $number .= '  '.$do_lat[0]['area'].'（面积/m²）';
                }
                if(!empty($do_lat[0]['video_length'])){
                    $number .= '  '.$do_lat[0]['video_length'].'（时长/s）';
                }
                $data[] = ['fieldname' => '发布规格', 'fieldvalue' => $number];
                $taskData['ad_lat'] = $data;
            }else{
                $data = [];
                foreach ($do_lat as $key => $value) {
                    $data[] = ['fieldname' => '广告发布地址', 'fieldvalue' => $value['traffic_id']];
                    $data[] = ['fieldname' => '广告形式', 'fieldvalue' => $value['form']];
                    $data[] = ['fieldname' => '发布期限', 'fieldvalue' => date('Y-m-d',$value['s_date']).'~'.date('Y-m-d',$value['e_date'])];
                    $number = $value['number'];
                    if(!empty($value['cars'])){
                        $number .= '  '.$value['cars'].'（辆）';
                    }
                    if(!empty($value['area'])){
                        $number .= '  '.$value['area'].'（面积/m²）';
                    }
                    if(!empty($value['video_length'])){
                        $number .= '  '.$value['video_length'].'（时长/s）';
                    }
                    $data[] = ['fieldname' => '发布规格', 'fieldvalue' => $number];
                }
                $taskData['ad_lat'] = $data;
            }
            
            $do_ml = M('tod_material') -> field('material_url,material_type,material_detail,check_sn') -> where(['ad_id' => $addata['id']]) ->select();
            
            foreach ($do_ml as $key => $value) {
                switch ($value['material_type']) {
                    case 3:
                        $taskData['videofile'][] = $value;
                        break;
                    case 1:
                        $taskData['imgfile'][] = $value;
                        break;
                    default:
                        $taskData['otherfile'][] = $value;
                        break;
                }
            }
            if(!empty($taskData['fadsource_id'])){
                $data = [];
                $data[] = ['fieldname' => '广告名称', 'fieldvalue' => $do_lat[0]['check_name']];
                $data[] = ['fieldname' => '广告主名称', 'fieldvalue' => $do_lat[0]['check_owner']];
                $data[] = ['fieldname' => '广告发布形式', 'fieldvalue' => $do_lat[0]['check_type']];
                $data[] = ['fieldname' => '广告发布地址', 'fieldvalue' => $do_lat[0]['address']];
                if($do_lat[0]['check_result'] == 1){
                    $check_result = '不一样';
                }elseif($do_lat[0]['check_result'] == 2){
                    $check_result = '部分一样';
                }else{
                    $check_result = '完全一样';
                }
                $data[] = ['fieldname' => '检查核实说明', 'fieldvalue' => $check_result];
                $taskData['check_data'] = $data;

                $do_tcp = M('tod_check_pic') ->field('material_url,material_type,material_detail') -> where(['source_id' => $taskData['fadsource_id']]) ->select();
                $taskData['check_imgfile'] = [];
                foreach ($do_tcp as $key => $value) {
                    $taskData['check_imgfile'][] = $value;
                }
            }

        }

        $taskData['taskType'] = 3;//告诉前端是户外广告
        $data = [
            'code' => 0,
            'data' => $taskData,
        ];
        $this->ajaxReturn($data);
        
    }

    /**
     * 提交任务
     */
    public function submitTask()
    {   
        // session('wx_id',692);
        if(!A('InputPc/Task','Model')->user_other_authority(self::USER_AUTH_CODE) && !A('InputPc/Task','Model')->user_other_authority(self::QUALITY_INSPECTOR_AUTH_CODE)){
            $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
        }
        
        $formData = $_POST;

        // 提交条件的限制 1. 本人提交本人的任务,或者是质检员修改任务; 2. 任务状态只能是做任务中或被退回
        $wxId = session('wx_id');
        $taskInfo = M('todtask')->find($formData['taskid']);
        $taskState = $taskInfo['task_state'];
        $currentId = $taskInfo['wx_id'];
        $role = 'user'; // 用来标识是质检员修改任务还是录入员做任务
        $scoreModel = new TaskInputScoreModel();
        $currentState = substr($taskState,0,1);
        $allowTaskState = [11,21,13,23,1,4];//录入
        //判断当前流程，根据流程执行不同操作
        if($currentState == 1){//预审
            if ($wxId == $currentId && in_array($taskState, $allowTaskState)){// 录入员做任务
                $data_ad['pretrial_time'] = date('Y-m-d H:i:s');//预审完成时间
                $data_task['task_state'] = 2;//预审环节完成
                $data_task['syn_state'] = 2;//推送成功
                if(!empty($formData['faudit'])){//预审通过
                    $data_ad['check'] = 2;
                }else{//预审不通过
                    if(empty($formData['freason'])){//判断不通过理由
                        $this->ajaxReturn([
                            'code' => -1,
                            'msg' => '填写信息不完整，请检查'
                        ]);
                    }else{
                        $data_ad['pretrial_result'] = $formData['freason'];//预审退回原因
                    }
                    $data_ad['check'] = 3;
                }

                //流程记录内容
                $flowData = [
                    'taskid' => $taskInfo['taskid'],
                    'tasklink' => $taskState,
                    'wx_id' => $wxId,
                    'fnote' => $data_ad['pretrial_result'],
                    'logtype' => 12,
                    'log' => A('TaskInput/OutAd','Model')->getLogType(12),
                    'creator' => $wxId,
                    'createtime' => time()
                ];
            }else{
                $this->ajaxReturn([
                    'code' => -2,
                    'msg' => '当前任务不是你的,开始下一个任务'
                ]);
            }

            M()->startTrans();
            $taskUpdate = M('todtask')->where(['taskid' => ['EQ', $formData['taskid']]])->save($data_task) === false ? false : true;//更改任务状态
            $taskFlowUpdate = M('todtask_flowlog')->add($flowData) === false ? false : true;//添加任务记录
            $adupdate = M('tod_ad_info') -> where(['fid' => ['eq',$taskInfo['fadinfo_id']]]) ->save($data_ad) === false ? false : true;//广告信息保存
            if(in_array($taskState, $allowTaskState) && !empty($data_task['task_state'])){
                $pushtask = A('Open/Outad')->get_pretrial_detail($taskInfo['fadinfo_id']);//预审信息推送
            }else{
                $pushtask = true;
            }
            if ($taskUpdate && $taskFlowUpdate && $pushtask && $adupdate){
                
                $scoreModel = new TaskInputScoreModel();
                $scoreModel->addScoreByOdCheckTask($taskInfo['taskid'], 1,$data_ad['check']);
                M()->commit();
                $code = 0;
                $msg = '提交成功';
            }else{
                M()->rollback();
                $code = -1;
                if (!$taskUpdate){
                    $msg = '任务更新失败';
                }elseif(!$taskFlowUpdate){
                    $msg = '任务记录更新失败';
                }elseif(!$pushtask){
                    $msg = '推送失败';
                }elseif(!$adupdate){
                    $msg = '广告内容更新失败';
                }else{
                    $msg = '未知错误';
                }
            }
        }elseif($currentState == 2){//违法判定
            M()->startTrans();
            if ($wxId == $currentId && in_array($taskState, $allowTaskState)){// 录入员做任务
                
                if(!empty($formData['fillegaltype'])){//违法
                    if(empty($formData['fexpressioncodes']) || empty($formData['fillegalcontent'])){//判断是否填写违法信息
                        $this->ajaxReturn([
                            'code' => -1,
                            'msg' => '填写信息不完整，请检查'
                        ]);
                    }else{
                        // 取出违法表现内容及判定依据
                        foreach($formData['fexpressioncodes'] as $key=>$fcode){
                            $fexpressioncodes .= $fcode.';';
                            $fexpressionInfo = M('tillegal')->where(['fcode'=>$fcode])->find();
                            $fexpressions .= $fexpressionInfo['fexpression'].';';
                            $fconfirmations .= $fexpressionInfo['fconfirmation'].';';
                        }
                        $source['fillegaltypecode'] = (int)$formData['fillegaltype'];//判定
                        $source['fillegalcontent'] = $formData['fillegalcontent'];//违法内容
                        $source['fexpressioncodes'] = $fexpressioncodes;//违法表现代码
                        $source['fexpressions'] = $fexpressions;//违法表现内容
                        $source['fconfirmations'] = $fconfirmations;//认定依据
                    }
                }else{
                    $source['fillegaltypecode'] = 0;//判定
                    $source['fillegalcontent'] = '';//违法内容
                    $source['fexpressioncodes'] = '';//违法表现代码
                    $source['fexpressions'] = '';//违法表现内容
                    $source['fconfirmations'] = '';//认定依据
                }

                $data_ad['check'] = 12;//核查通过
                $data_task['task_state'] = 2;//违法判定环节完成
                $data_task['syn_state'] = 2;//推送成功
                
                //将要发送到第三方的数据
                $senddata = $source;
                $senddata['ad_id'] = $taskInfo['fadinfo_id'];//广告ID
                $senddata['check'] = $data_ad['check'];//核查状态
                $senddata['source_id'] = $taskInfo['fadsource_id'];

                $source['determine_time'] = date('Y-m-d H:i:s');//违法判定完成时间
                M('tod_lat_and_lon') -> where(['source_id' => $taskInfo['fadsource_id']]) -> save($source);

                //流程记录内容
                $flowData = [
                    'taskid' => $taskInfo['taskid'],
                    'tasklink' => $taskState,
                    'wx_id' => $wxId,
                    'logtype' => 22,
                    'log' => A('TaskInput/OutAd','Model')->getLogType(22),
                    'creator' => $wxId,
                    'createtime' => time()
                ];
            }else{
                $this->ajaxReturn([
                    'code' => -3,
                    'msg' => '当前任务状态非法',
                ]);
            }

            $taskUpdate = M('todtask')->where(['taskid' => ['EQ', $formData['taskid']]])->save($data_task) === false ? false : true;//任务进度保存
            $taskFlowUpdate = M('todtask_flowlog')->add($flowData) === false ? false : true;//添加任务日志
            $adupdate = M('tod_ad_info') -> where(['fid' => ['eq',$taskInfo['fadinfo_id']]]) ->save($data_ad) === false ? false : true;//广告处理结果保存
            if(in_array($taskState, $allowTaskState) && !empty($data_task['task_state'])){
                $pushtask = A('Open/Outad')->get_illegal_detail($senddata);//违法信息推送
            }else{
                $pushtask = true;
            }
            if ($taskUpdate && $taskFlowUpdate && $pushtask && $adupdate){
                $scoreModel = new TaskInputScoreModel();
                $scoreModel->addScoreByOdCheckTask($taskInfo['taskid'], 2,$source['fillegaltypecode']);
                M()->commit();
                $code = 0;
                $msg = '提交成功';
            }else{
                M()->rollback();
                $code = -1;
                if (!$taskUpdate){
                    $msg = '任务更新失败';
                }elseif(!$taskFlowUpdate){
                    $msg = '任务记录更新失败';
                }elseif(!$pushtask){
                    $msg = '推送失败';
                }elseif(!$adupdate){
                    $msg = '广告内容更新失败';
                }else{
                    $msg = '未知错误';
                }
            }

        }

        $this->ajaxReturn(compact('code', 'msg'));
    }



}