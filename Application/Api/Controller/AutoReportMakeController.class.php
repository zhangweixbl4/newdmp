<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;
import('Vendor.PHPExcel');


class AutoReportMakeController extends Controller {
	
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}
	
	
	public function chengyang(){
		
		ini_set('memory_limit','1024M');
		set_time_limit(600);
	    Vendor("PHPExcel.IOFactory");
		session_write_close();

		$regulatorpersonInfo = M('tregulatorperson')->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
		$regulatorInfo = M('tregulator')->where(array('fcode'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息

		
		$date = I('date');
		if($date){
			$date = date('Y-m-d',strtotime($date));
		}else{
			$date = date('Y-m-d',time()-86400);
		}
		
		
		$month = date('Y-m',strtotime($date));
		
		
		$data = array();
		$data[] = array('mediaid'=>11000010007240,'month'=>$month,'region'=>'370214','fmediaclassid'=>'0103','date'=>array($date));
		$data[] = array('mediaid'=>11000010007239,'month'=>$month,'region'=>'370214','fmediaclassid'=>'0103','date'=>array($date));
		$data[] = array('mediaid'=>11000010015405,'month'=>$month,'region'=>'370214','fmediaclassid'=>'02','date'=>array($date));
		
		
		$regionid = $data[0]['region'];//地区ID
		$month 	  = $data[0]['month'];//月份
		$regulatorRegionInfo = M('tregion')->field('fid,fname,ffullname,fname1')->where(array('fid'=>$regionid))->find();

		$region_id_rtrim = rtrim($regionid,'00');//去掉地区后面两位0，判断区
		$region_id_rtrim = rtrim($region_id_rtrim,'000');//去掉后面三位0，判断全国
		$region_id_rtrim = rtrim($region_id_rtrim,'00');//去掉后面两位0，判断市
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位
		if($tregion_len == 0){
			$area_name = '';
		}elseif($tregion_len == 2){
			$area_name = '省';
		}elseif($tregion_len == 4){
			$area_name = '市';
		}elseif($tregion_len == 6){
			$area_name = '（区/县）';
		}else{
			$area_name = '';
		}

		$report_start_date = date('Y-m-01', strtotime($month));//开始时间
		$report_end_date = date('Y-m-d', strtotime("$report_start_date +1 month -1 day"));//结束时间
		if($report_start_date == $report_end_date) $this->ajaxReturn(array('code'=>-1,'msg'=>'开始结束时间相同'));
		$startDate = date('Y-m-d H:i:s',strtotime($report_start_date));
		$endDate = date('Y-m-d H:i:s',strtotime($report_end_date)+86399);
		$report_month = date('n',strtotime($month));//报告时间
		$start_date = date('Y年m月d日',strtotime($report_start_date));//监测开始时间
		$end_date = date('Y年m月d日',strtotime($report_end_date));//监测结束时间

		
		$report_name = $regulatorRegionInfo['fname1'].' '.$report_month.'月份监测报告';//自动生成名字
		

		
		$issue_data = A('Gongshang/ReportMake','Model')->issue_data($data);//获取数据

		$tv_issue_data 	   	= $issue_data['tv'];							 //电视数据
		$tv_ill_data 	   	= $issue_data['tv_ill'];						 //电视违法数据
		$tv_media_group    	= $issue_data['tv_media_group'];				 //电视媒介
		$tv_count 		   	= count($tv_media_group);						 //统计电视媒介数
		$tv_sum 		   	= $issue_data['tv_sum'];						 //电视发布次数
		$tv_illegal_sum    	= $issue_data['tv_illegal_sum'];				 //电视违法发布次数
		$tv_rate			= round($tv_illegal_sum/$tv_sum*100,2).'%';		 //电视发布违法率
		$tv_length 		   	= $issue_data['tv_length'];						 //电视总时长
		$tv_illegal_length 	= $issue_data['tv_illegal_length'];				 //电视违法总时长
		$tv_length_rate		= round($tv_illegal_length/$tv_length*100,2).'%';//电视时长违法率

		$bc_issue_data 		= $issue_data['bc']; 							 //广播数据
		$bc_ill_data 	   	= $issue_data['bc_ill'];						 //广播违法数据
		$bc_media_group 	= $issue_data['bc_media_group'];				 //广播媒介
		$bc_count 			= count($bc_media_group);						 //统计广播媒介数
		$bc_sum				= $issue_data['bc_sum'];						 //广播发布次数
		$bc_illegal_sum		= $issue_data['bc_illegal_sum'];				 //广播违法发布次数
		$bc_rate			= round($bc_illegal_sum/$bc_sum*100,2).'%';		 //广播发布违法率
		$bc_length			= $issue_data['bc_length'];						 //广播总时长
		$bc_illegal_length  = $issue_data['bc_illegal_length'];				 //广播违法总时长
		$bc_length_rate		= round($bc_illegal_length/$bc_length*100,2).'%';//广播时长违法率

		$paper_issue_data 	= $issue_data['paper']; 						 //报纸数据
		$paper_ill_data 	= $issue_data['paper_ill'];						 //报纸违法数据
		$paper_media_group 	= $issue_data['paper_media_group'];				 //报纸媒介
		$paper_count 		= count($paper_media_group);					 //统计报纸媒介数
		$paper_sum			= $issue_data['paper_sum'];					 	 //报纸发布次数
		$paper_illegal_sum	= $issue_data['paper_illegal_sum'];			 	 //报纸违法发布次数
		$paper_rate			= round($paper_illegal_sum/$paper_sum*100,2).'%';//报纸发布违法率

		// //合计
		$total_sum 			= $tv_sum + $bc_sum + $paper_sum;				 
		$total_illegal_sum 	= $tv_illegal_sum + $bc_illegal_sum + $paper_illegal_sum;
		$total_rate			= round($total_illegal_sum/$total_sum*100,2).'%';
		$total_length		= $tv_length + $bc_length;
		$total_ill_length 	= $tv_illegal_length + $bc_illegal_length;
		$total_length_rate	= round($total_ill_length/$total_length*100,2).'%';

		$ad_class 			= array_values($issue_data['ad_class']);//广告类别统计
		$five_class_illegal = array_values($issue_data['five_class_illegal']);//违法广告五大类

		$template = $_SERVER['DOCUMENT_ROOT'].'/Application/Gongshang/Common/template.xls';	//使用模板  
	    $objPHPExcel = \PHPExcel_IOFactory::load($template);     	//加载excel文件,设置模板  	   
	    $objWriter = new \PHPExcel_Writer_Excel5($objPHPExcel);  	//设置保存版本格式  

	    //接下来就是写数据到表格里面去  
	    $sheet0 = $objPHPExcel->getSheet(0);	//表一
	    $sheet1 = $objPHPExcel->getSheet(1);	//表二
	    $sheet2 = $objPHPExcel->getSheet(2);	//表三
	    $sheet3 = $objPHPExcel->getSheet(3);	//表四
	    $sheet4 = $objPHPExcel->getSheet(4);	//表五
	    $sheet5 = $objPHPExcel->getSheet(5);	//表六
	    $sheet6 = $objPHPExcel->getSheet(6); 	//表七

	    //表一 
	    $highestRow = $sheet0->getHighestRow();//表一总行数
	    //设置表头
	    $headtitle = '表一:'.$regulatorRegionInfo['fname1'].$area_name.'级媒体广告监测情况及目录';
	    $sheet0->setCellValue('A1',$headtitle); 

	    $sheet0->setCellValue('A3',$tv_count); 
	    $sheet0->setCellValue('B3',$bc_count);
	    $sheet0->setCellValue('C3',$paper_count); 
	    //监测目录
	    //设置属性
	    $style = array(
	    	'alignment' => array(
	    		'vertical'		=> \PHPExcel_Style_Alignment::VERTICAL_CENTER,		//垂直居中
	    	),
	    	//边框
	    	'borders' => array(
	    		'bottom' 	=> array('style' => \PHPExcel_Style_Border::BORDER_THIN),
	    		'right' 	=> array('style' => \PHPExcel_Style_Border::BORDER_THIN),
	    	),
	    );
	    $top_boder = array('borders'=>array('top'=>array('style' => \PHPExcel_Style_Border::BORDER_THIN),));
	    $bottom_border = array('borders'=>array('bottom'=>array('style' => \PHPExcel_Style_Border::BORDER_THIN),));
	    //电视列表
	    $tv_end_row = $tv_start_row = $highestRow + 1;//设置位置	    
	    $sheet0->getStyle('A'.''.$tv_start_row)->applyFromArray($top_boder);
	    $sheet0->getStyle('B'.''.$tv_start_row)->applyFromArray($top_boder);
	    $sheet0->getStyle('C'.''.$tv_start_row)->applyFromArray($top_boder);
	    if($tv_count > 0){
	    	$sheet0->setCellValue('A'.$tv_start_row,$area_name.'级电视媒体（频道）');
	    	for ($i = 0;$i < $tv_count;$i++) { 
	    		$tv_end_row = $tv_start_row + $i;
	    		$sheet0->setCellValue('B'.$tv_end_row,$tv_media_group[$i]['fmedianame']);
	    		$sheet0->setCellValue('C'.$tv_end_row,$tv_media_group[$i]['day']);
	    		$sheet0->getStyle('B'.''.$tv_end_row)->applyFromArray($bottom_border);
	    		$sheet0->getStyle('C'.''.$tv_end_row)->applyFromArray($bottom_border);
	    	}
	    	$sheet0->mergeCells('A'.''.$tv_start_row.':'.'A'.''.$tv_end_row);//合并单元格
	    }else{
	    	$sheet0->setCellValue('A'.$tv_start_row,$area_name.'级电视媒体（频道）');
	    	$sheet0->setCellValue('B'.$tv_start_row,'');
	    	$sheet0->setCellValue('C'.$tv_start_row,'');
	    }
	    $sheet0->getStyle('A'.''.$tv_start_row.':'.'A'.''.$tv_end_row)->applyFromArray($style);
	    $sheet0->getStyle('B'.''.$tv_start_row.':'.'B'.''.$tv_end_row)->applyFromArray($style);
	    $sheet0->getStyle('C'.''.$tv_start_row.':'.'C'.''.$tv_end_row)->applyFromArray($style);
	    //广播列表
	    $bc_end_row = $bc_start_row = $tv_end_row + 1;
	    if($bc_count > 0){
	    	$sheet0->setCellValue('A'.$bc_start_row,$area_name.'级广播媒体（频率）');
	    	for ($i = 0;$i < $bc_count;$i++) { 
	    		$bc_end_row = $bc_start_row + $i;
	    		$sheet0->setCellValue('B'.$bc_end_row,$bc_media_group[$i]['fmedianame']);
	    		$sheet0->setCellValue('C'.$bc_end_row,$bc_media_group[$i]['day']);
	    		$sheet0->getStyle('B'.''.$bc_end_row)->applyFromArray($bottom_border);
	    		$sheet0->getStyle('C'.''.$bc_end_row)->applyFromArray($bottom_border);
	    	}
	    	$sheet0->mergeCells('A'.''.$bc_start_row.':'.'A'.''.$bc_end_row);//合并单元格
	    }else{
	    	$sheet0->setCellValue('A'.$bc_start_row,$area_name.'级广播媒体（频率）');
	    	$sheet0->setCellValue('B'.$bc_start_row,'');
	    	$sheet0->setCellValue('C'.$bc_start_row,'');
	    }
	    $sheet0->getStyle('A'.''.$bc_start_row.':'.'A'.''.$bc_end_row)->applyFromArray($style);
	    $sheet0->getStyle('B'.''.$bc_start_row.':'.'B'.''.$bc_end_row)->applyFromArray($style);
	    $sheet0->getStyle('C'.''.$bc_start_row.':'.'C'.''.$bc_end_row)->applyFromArray($style);
	    //报纸列表
	    $paper_end_row = $paper_start_row = $bc_end_row + 1;
	    if($paper_count > 0){
	    	$sheet0->setCellValue('A'.$paper_start_row,$area_name.'级报纸');
	    	for ($i = 0;$i < $paper_count;$i++) { 
	    		$paper_end_row = $paper_start_row + $i;
	    		$sheet0->setCellValue('B'.$paper_end_row,$paper_media_group[$i]['fmedianame']);
	    		$sheet0->setCellValue('C'.$paper_end_row,$paper_media_group[$i]['day']);
	    		$sheet0->getStyle('B'.''.$paper_end_row)->applyFromArray($bottom_border);
	    		$sheet0->getStyle('C'.''.$paper_end_row)->applyFromArray($bottom_border);
	    	}
	    	$sheet0->mergeCells('A'.''.$paper_start_row.':'.'A'.''.$paper_end_row);//合并单元格
	    }else{
	    	$sheet0->setCellValue('A'.$paper_start_row,$area_name.'级报纸');
	    	$sheet0->setCellValue('B'.$paper_start_row,'');
	    	$sheet0->setCellValue('C'.$paper_start_row,'');
	    }
	    $sheet0->getStyle('A'.''.$paper_start_row.':'.'A'.''.$paper_end_row)->applyFromArray($style);
	    $sheet0->getStyle('B'.''.$paper_start_row.':'.'B'.''.$paper_end_row)->applyFromArray($style);
	    $sheet0->getStyle('C'.''.$paper_start_row.':'.'C'.''.$paper_end_row)->applyFromArray($style);

	    //表二
	    //设置表头
	    $headtitle = '表二:'.$regulatorRegionInfo['fname1'].$area_name.'级媒体广告监测概表';
	    $sheet1->setCellValue('A1',$headtitle); 
	    //电视
	    $sheet1->setCellValue('B3',$tv_sum);
	    $sheet1->setCellValue('C3',$tv_illegal_sum);
	    $sheet1->setCellValue('D3',$tv_rate);
	    $sheet1->setCellValue('E3',$tv_illegal_length);
	    $sheet1->setCellValue('F3',$tv_length_rate);
	    //广播
	    $sheet1->setCellValue('B4',$bc_sum);
	    $sheet1->setCellValue('C4',$bc_illegal_sum);
	    $sheet1->setCellValue('D4',$bc_rate);
	    $sheet1->setCellValue('E4',$bc_illegal_length);
	    $sheet1->setCellValue('F4',$bc_length_rate);
	    //报纸
	    $sheet1->setCellValue('B5',$paper_sum);
	    $sheet1->setCellValue('C5',$paper_illegal_sum);
	    $sheet1->setCellValue('D5',$paper_rate);
	    //合计
	    $sheet1->setCellValue('B6',$total_sum);
	    $sheet1->setCellValue('C6',$total_illegal_sum);
	    $sheet1->setCellValue('D6',$total_rate);
	    $sheet1->setCellValue('E6',$total_ill_length);
	    $sheet1->setCellValue('F6',$total_length_rate);

	    //表三
	    //设置表头
	    $headtitle = '表三:'.$regulatorRegionInfo['fname1'].$area_name.'级电视媒体涉嫌违法广告统计表';
	    $sheet2->setCellValue('A1',$headtitle); 
	    if($tv_illegal_sum > 0){
	    	if($tv_illegal_sum > 7){
		    	$sheet2->setCellValue('A10',' ');
		    	$sheet2->setCellValue('A10',' ');
		    	$sheet2->unmergeCells('A10:G10');    // 拆分
		    	$sheet2->unmergeCells('A11:G11');    // 拆分
		    }
	    	for($i = 0;$i < $tv_illegal_sum;$i++){
	    		$row = $i + 3;
				
				$fexpressioncodes = $tv_ill_data[$i]['fexpressioncodes'];//违法表现代码
				$fexpression_c = '';
				if($fexpressioncodes){
					$fexpressioncodes_arr = explode(';',$fexpressioncodes);
					foreach($fexpressioncodes_arr as $fexpressioncode){
						$fexpression = M('tillegal')->cache(true,3600)->where(array('fcode'=>$fexpressioncode))->getField('fexpression');
						$fexpression_c .= $fexpressioncode.':'.$fexpression."\n";
					}
				}
				
				
				
	    		$sheet2->setCellValue('A'.$row,$tv_ill_data[$i]['fmedianame']);
	    		$sheet2->setCellValue('B'.$row,$tv_ill_data[$i]['fadname']);
	    		$sheet2->setCellValue('C'.$row,substr($tv_ill_data[$i]['fstarttime'],0,10));
	    		$sheet2->setCellValue('D'.$row,substr($tv_ill_data[$i]['fstarttime'],11,8));
	    		$sheet2->setCellValue('E'.$row,$tv_ill_data[$i]['flength']);
	    		$sheet2->setCellValue('F'.$row,$tv_ill_data[$i]['ffullname']);
	    		$sheet2->setCellValue('G'.$row,$fexpression_c); //违法表现代码
	    		$sheet2->getStyle('A'.$row)->applyFromArray($style);
	    		$sheet2->getStyle('B'.$row)->applyFromArray($style);
	    		$sheet2->getStyle('C'.$row)->applyFromArray($style);
	    		$sheet2->getStyle('D'.$row)->applyFromArray($style);
	    		$sheet2->getStyle('E'.$row)->applyFromArray($style);
	    		$sheet2->getStyle('F'.$row)->applyFromArray($style);
	    		$sheet2->getStyle('G'.$row)->applyFromArray($style);
	    		$sheet2->getStyle('A'.$row)->getAlignment()->setWrapText(false);
	    		$sheet2->getStyle('B'.$row)->getAlignment()->setWrapText(false);
	    		$sheet2->getStyle('C'.$row)->getAlignment()->setWrapText(false);
	    		$sheet2->getStyle('D'.$row)->getAlignment()->setWrapText(false);
	    		$sheet2->getStyle('E'.$row)->getAlignment()->setWrapText(false);
	    		$sheet2->getStyle('F'.$row)->getAlignment()->setWrapText(false);
	    		$sheet2->getStyle('G'.$row)->getAlignment()->setWrapText(false);
	    		$sheet2->getRowDimension($row)->setRowHeight(13.5);//设置行高
	    		$sheet2->getStyle('A'.$row.':'.'G'.$row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//设置水平居中
	    		$sheet2->getStyle('A'.$row.':'.'G'.$row)->getFont()->setSize(10);//字体大小
	    	}
	    	if($row > 9){
		    	$sheet2->setCellValue('A'.($row+1),'附1：广告类别包括，药品、医疗器械、化妆品、房地产、普通食品、保健食品、烟草、酒、电器、交通产品、知识产品、普通商品、医疗服务、金融服务、信息服务、互联网服务、商业招商投资、教育培训服务、会展文体活动服务、普通服务、形象宣传、非商业及其它。');
		    	$sheet2->setCellValue('A'.($row+2),'附2：按国家市场监督管理总局广告司2018年7月启用的《广告监测标准（2018年7月版）》规定的违法行为代码填写。');
		    	$sheet2->mergeCells('A'.''.($row+1).':'.'G'.''.($row+1));//合并单元格
		    	$sheet2->mergeCells('A'.''.($row+2).':'.'G'.''.($row+2));//合并单元格
		    	$sheet2->getStyle('A'.''.($row+1).':'.'G'.''.($row+1))->applyFromArray($style);
		    	$sheet2->getStyle('A'.''.($row+2).':'.'G'.''.($row+2))->applyFromArray($style);
		    	$sheet2->getStyle('A'.''.($row+1))->getFont()->setSize(10);//设置字体大小
		    	$sheet2->getStyle('A'.''.($row+2))->getFont()->setSize(10);//设置字体大小
		    	$sheet2->getStyle('A'.''.($row+1))->getAlignment()->setWrapText(true);//设置自动换行
		    	$sheet2->getStyle('A'.''.($row+2))->getAlignment()->setWrapText(true);//设置自动换行
		    	$sheet2->getRowDimension($row+1)->setRowHeight(30);//设置行高
		    	$sheet2->getRowDimension($row+2)->setRowHeight(30);//设置行高
	    	}
	    }
	    //表四
	    //设置表头
	    $headtitle = '表四:'.$regulatorRegionInfo['fname1'].$area_name.'级广播媒体涉嫌违法广告统计表';
	    $sheet3->setCellValue('A1',$headtitle);
	    if($bc_illegal_sum > 0){
	    	if($bc_illegal_sum > 7){
		    	$sheet3->setCellValue('A10',' ');
		    	$sheet3->setCellValue('A10',' ');
		    	$sheet3->unmergeCells('A10:G10');    // 拆分
		    	$sheet3->unmergeCells('A11:G11');    // 拆分
		    }
	    	for($i = 0;$i < $bc_illegal_sum;$i++){
	    		$row = $i + 3;
				
				$fexpressioncodes = $bc_ill_data[$i]['fexpressioncodes'];//违法表现代码
				$fexpression_c = '';
				if($fexpressioncodes){
					$fexpressioncodes_arr = explode(';',$fexpressioncodes);
					foreach($fexpressioncodes_arr as $fexpressioncode){
						$fexpression = M('tillegal')->cache(true,3600)->where(array('fcode'=>$fexpressioncode))->getField('fexpression');
						$fexpression_c .= $fexpressioncode.':'.$fexpression."\n";
					}
				}
				
	    		$sheet3->setCellValue('A'.$row,$bc_ill_data[$i]['fmedianame']);
	    		$sheet3->setCellValue('B'.$row,$bc_ill_data[$i]['fadname']);
	    		$sheet3->setCellValue('C'.$row,substr($bc_ill_data[$i]['fstarttime'],0,10));
	    		$sheet3->setCellValue('D'.$row,substr($bc_ill_data[$i]['fstarttime'],11,8));
	    		$sheet3->setCellValue('E'.$row,$bc_ill_data[$i]['flength']);
	    		$sheet3->setCellValue('F'.$row,$bc_ill_data[$i]['ffullname']);
	    		$sheet3->setCellValue('G'.$row,$fexpression_c);
	    		$sheet3->getStyle('A'.$row)->applyFromArray($style);
	    		$sheet3->getStyle('B'.$row)->applyFromArray($style);
	    		$sheet3->getStyle('C'.$row)->applyFromArray($style);
	    		$sheet3->getStyle('D'.$row)->applyFromArray($style);
	    		$sheet3->getStyle('E'.$row)->applyFromArray($style);
	    		$sheet3->getStyle('F'.$row)->applyFromArray($style);
	    		$sheet3->getStyle('G'.$row)->applyFromArray($style);
	    		$sheet3->getStyle('A'.$row)->getAlignment()->setWrapText(false);
	    		$sheet3->getStyle('B'.$row)->getAlignment()->setWrapText(false);
	    		$sheet3->getStyle('C'.$row)->getAlignment()->setWrapText(false);
	    		$sheet3->getStyle('D'.$row)->getAlignment()->setWrapText(false);
	    		$sheet3->getStyle('E'.$row)->getAlignment()->setWrapText(false);
	    		$sheet3->getStyle('F'.$row)->getAlignment()->setWrapText(false);
	    		$sheet3->getStyle('G'.$row)->getAlignment()->setWrapText(false);
	    		$sheet3->getRowDimension($row)->setRowHeight(13.5);//设置行高
	    		$sheet3->getStyle('A'.$row.':'.'G'.$row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//设置水平居中
	    		$sheet3->getStyle('A'.$row.':'.'G'.$row)->getFont()->setSize(10);//字体大小
	    	}
	    	if($row > 9){
		    	$sheet3->setCellValue('A'.($row+1),'附1：广告类别包括，药品、医疗器械、化妆品、房地产、普通食品、保健食品、烟草、酒、电器、交通产品、知识产品、普通商品、医疗服务、金融服务、信息服务、互联网服务、商业招商投资、教育培训服务、会展文体活动服务、普通服务、形象宣传、非商业及其它。');
		    	$sheet3->setCellValue('A'.($row+2),'附2：按国家市场监督管理总局广告司2018年7月启用的《广告监测标准（2018年7月版）》规定的违法行为代码填写。');
		    	$sheet3->mergeCells('A'.''.($row+1).':'.'G'.''.($row+1));//合并单元格
		    	$sheet3->mergeCells('A'.''.($row+2).':'.'G'.''.($row+2));//合并单元格
		    	$sheet3->getStyle('A'.''.($row+1).':'.'G'.''.($row+1))->applyFromArray($style);
		    	$sheet3->getStyle('A'.''.($row+2).':'.'G'.''.($row+2))->applyFromArray($style);
		    	$sheet3->getStyle('A'.''.($row+1))->getFont()->setSize(10);//设置字体大小
		    	$sheet3->getStyle('A'.''.($row+2))->getFont()->setSize(10);//设置字体大小
		    	$sheet3->getStyle('A'.''.($row+1))->getAlignment()->setWrapText(true);//设置自动换行
		    	$sheet3->getStyle('A'.''.($row+2))->getAlignment()->setWrapText(true);//设置自动换行
		    	$sheet3->getRowDimension($row+1)->setRowHeight(30);//设置行高
		    	$sheet3->getRowDimension($row+2)->setRowHeight(30);//设置行高
	    	}
	    }
	    //表五
	    //设置表头
	    $headtitle = '表五:'.$regulatorRegionInfo['fname1'].$area_name.'级报纸媒体涉嫌违法广告统计表';
	    $sheet4->setCellValue('A1',$headtitle);
	    if($paper_illegal_sum > 0){
	    	if($paper_illegal_sum > 7){
		    	$sheet4->setCellValue('A10',' ');
		    	$sheet4->setCellValue('A11',' ');
		    	$sheet4->unmergeCells('A10:E10');    // 拆分
		    	$sheet4->unmergeCells('A11:E11');    // 拆分
		    }
	    	for($i = 0;$i < $paper_illegal_sum;$i++){
	    		$row = $i + 3;
				
				$fexpressioncodes = $paper_ill_data[$i]['fexpressioncodes'];//违法表现代码
				$fexpression_c = '';
				if($fexpressioncodes){
					$fexpressioncodes_arr = explode(';',$fexpressioncodes);
					foreach($fexpressioncodes_arr as $fexpressioncode){
						$fexpression = M('tillegal')->cache(true,3600)->where(array('fcode'=>$fexpressioncode))->getField('fexpression');
						$fexpression_c .= $fexpressioncode.':'.$fexpression."\n";
					}
				}
				
				
	    		$sheet4->setCellValue('A'.$row,$paper_ill_data[$i]['fmedianame']);
	    		$sheet4->setCellValue('B'.$row,$paper_ill_data[$i]['fadname']);
	    		$sheet4->setCellValue('C'.$row,substr($paper_ill_data[$i]['fissuedate'],0,10));
	    		$sheet4->setCellValue('D'.$row,$paper_ill_data[$i]['ffullname']);
	    		$sheet4->setCellValue('E'.$row,$fexpression_c);
	    		$sheet4->getStyle('A'.$row)->applyFromArray($style);
	    		$sheet4->getStyle('B'.$row)->applyFromArray($style);
	    		$sheet4->getStyle('C'.$row)->applyFromArray($style);
	    		$sheet4->getStyle('D'.$row)->applyFromArray($style);
	    		$sheet4->getStyle('E'.$row)->applyFromArray($style);
	    		$sheet4->getStyle('A'.$row)->getAlignment()->setWrapText(false);
	    		$sheet4->getStyle('B'.$row)->getAlignment()->setWrapText(false);
	    		$sheet4->getStyle('C'.$row)->getAlignment()->setWrapText(false);
	    		$sheet4->getStyle('D'.$row)->getAlignment()->setWrapText(false);
	    		$sheet4->getStyle('E'.$row)->getAlignment()->setWrapText(false);
	    		$sheet4->getRowDimension($row)->setRowHeight(13.5);//设置行高
	    		$sheet4->getStyle('A'.$row.':'.'E'.$row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//设置水平居中
	    		$sheet4->getStyle('A'.$row.':'.'E'.$row)->getFont()->setSize(10);//字体大小
	    	}
	    	if($row > 9){
		    	$sheet4->setCellValue('A'.($row+1),'附1：广告类别包括，药品、医疗器械、化妆品、房地产、普通食品、保健食品、烟草、酒、电器、交通产品、知识产品、普通商品、医疗服务、金融服务、信息服务、互联网服务、商业招商投资、教育培训服务、会展文体活动服务、普通服务、形象宣传、非商业及其它。');
		    	$sheet4->setCellValue('A'.($row+2),'附2：按国家市场监督管理总局广告司2018年7月启用的《广告监测标准（2018年7月版）》规定的违法行为代码填写。');
		    	$sheet4->mergeCells('A'.''.($row+1).':'.'E'.''.($row+1));//合并单元格
		    	$sheet4->mergeCells('A'.''.($row+2).':'.'E'.''.($row+2));//合并单元格
		    	$sheet4->getStyle('A'.''.($row+1).':'.'E'.''.($row+1))->applyFromArray($style);
		    	$sheet4->getStyle('A'.''.($row+2).':'.'E'.''.($row+2))->applyFromArray($style);
		    	$sheet4->getStyle('A'.''.($row+1))->getFont()->setSize(10);//设置字体大小
		    	$sheet4->getStyle('A'.''.($row+2))->getFont()->setSize(10);//设置字体大小
		    	$sheet4->getStyle('A'.''.($row+1))->getAlignment()->setWrapText(true);//设置自动换行
		    	$sheet4->getStyle('A'.''.($row+2))->getAlignment()->setWrapText(true);//设置自动换行
		    	$sheet4->getRowDimension($row+1)->setRowHeight(30);//设置行高
		    	$sheet4->getRowDimension($row+2)->setRowHeight(30);//设置行高
	    	}
	    }
	    //表六
	    //设置表头
	    $headtitle = '表六:'.$regulatorRegionInfo['fname1'].$area_name.'级各类别广告发布数监测情况统计表';
	    $sheet5->setCellValue('A1',$headtitle);
	    foreach ($ad_class as $key => $value) {
	    	$row = $key + 4;
	    	$sheet5->setCellValue('A'.$row,$value['adclass']);
	    	$sheet5->setCellValue('B'.$row,$value['count']);
	    	$sheet5->setCellValue('C'.$row,$value['illegal_count']);	    	
	    	$sheet5->setCellValue('E'.$row,$value['total']);	    	
	    	$sheet5->setCellValue('F'.$row,$value['illegal_total']);
	    	if($value['count'] > 0){
		    	$class_count_ill_rate = round($value['illegal_count']/$value['count']*100,2).'%';//条次违法率
		    	$class_total_ill_rate = round($value['illegal_total']/$value['total']*100,2).'%';//时长违法率
		    	$sheet5->setCellValue('D'.$row,$class_count_ill_rate);
		    	$sheet5->setCellValue('G'.$row,$class_total_ill_rate);
		    }
		    $class_count += $value['count'];
		    $class_ill_count += $value['illegal_count'];
		    $class_total += $value['total'];
		    $class_ill_total += $value['illegal_total'];
	    }
	    if($class_count > 0){
	    	$sheet5->setCellValue('B27',$class_count);
	    	$sheet5->setCellValue('C27',$class_ill_count);
	    	$sheet5->setCellValue('D27',round($class_ill_count/$class_count*100,2).'%');
	    	$sheet5->setCellValue('E27',$class_total);	    	
	    	$sheet5->setCellValue('F27',$class_ill_total);
	    	$sheet5->setCellValue('G27',round($class_ill_total/$class_total*100,2).'%');
	    }
	    //表七
	    $headtitle = '表七:'.$regulatorRegionInfo['fname1'].$area_name.'级媒体涉嫌违法的典型广告统计表';
    	$sheet6->setCellValue('A1',$headtitle);
	    if($five_class_illegal){
	    	if(count($five_class_illegal) > 4){
	    		$sheet6->setCellValue('A10',' ');
		    	$sheet6->setCellValue('A11',' ');
		    	$sheet6->unmergeCells('A7:D7');    // 拆分
		    	$sheet6->unmergeCells('A8:D8');    // 拆分
	    	}
	    	foreach ($five_class_illegal as $key => $value) {
	    		$row = $key + 3;
				$fexpressioncodes = $value['fexpressioncodes'];//违法表现代码
				$fexpression_c = '';
				if($fexpressioncodes){
					$fexpressioncodes_arr = explode(';',$fexpressioncodes);
					foreach($fexpressioncodes_arr as $fexpressioncode){
						$fexpression = M('tillegal')->cache(true,3600)->where(array('fcode'=>$fexpressioncode))->getField('fexpression');
						$fexpression_c .= $fexpressioncode.':'.$fexpression."\n";
					}
				}
				
	    		$sheet6->setCellValue('A'.$row,($key+1));
	    		$sheet6->setCellValue('B'.$row,$value['fadname']);
	    		$sheet6->setCellValue('C'.$row,$value['ffullname']);
	    		$sheet6->setCellValue('D'.$row,$fexpression_c);
	    		$sheet6->getStyle('A'.$row)->applyFromArray($style);
	    		$sheet6->getStyle('B'.$row)->applyFromArray($style);
	    		$sheet6->getStyle('C'.$row)->applyFromArray($style);
	    		$sheet6->getStyle('D'.$row)->applyFromArray($style);
	    		$sheet6->getStyle('A'.$row)->getAlignment()->setWrapText(false);
	    		$sheet6->getStyle('B'.$row)->getAlignment()->setWrapText(false);
	    		$sheet6->getStyle('C'.$row)->getAlignment()->setWrapText(false);
	    		$sheet6->getStyle('D'.$row)->getAlignment()->setWrapText(false);
	    		$sheet6->getRowDimension($row)->setRowHeight(13.5);//设置行高
	    		$sheet6->getStyle('A'.$row.':'.'D'.$row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//设置水平居中
	    		$sheet6->getStyle('A'.$row.':'.'D'.$row)->getFont()->setSize(10);//字体大小
	    	}
	    	if($row > 6){
		    	$sheet6->setCellValue('A'.($row+1),'附1：广告类别包括，药品、医疗器械、化妆品、房地产、普通食品、保健食品、烟草、酒、电器、交通产品、知识产品、普通商品、医疗服务、金融服务、信息服务、互联网服务、商业招商投资、教育培训服务、会展文体活动服务、普通服务、形象宣传、非商业及其它。');
		    	$sheet6->setCellValue('A'.($row+2),'附2：按国家市场监督管理总局广告司2018年7月启用的《广告监测标准（2018年7月版）》规定的违法行为代码填写。');
		    	$sheet6->mergeCells('A'.''.($row+1).':'.'D'.''.($row+1));//合并单元格
		    	$sheet6->mergeCells('A'.''.($row+2).':'.'D'.''.($row+2));//合并单元格
		    	$sheet6->getStyle('A'.''.($row+1).':'.'D'.''.($row+1))->applyFromArray($style);
		    	$sheet6->getStyle('A'.''.($row+2).':'.'D'.''.($row+2))->applyFromArray($style);
		    	$sheet6->getStyle('A'.''.($row+1))->getFont()->setSize(10);//设置字体大小
		    	$sheet6->getStyle('A'.''.($row+2))->getFont()->setSize(10);//设置字体大小
		    	$sheet6->getStyle('A'.''.($row+1))->getAlignment()->setWrapText(true);//设置自动换行
		    	$sheet6->getStyle('A'.''.($row+2))->getAlignment()->setWrapText(true);//设置自动换行
		    	$sheet6->getRowDimension($row+1)->setRowHeight(30);//设置行高
		    	$sheet6->getRowDimension($row+2)->setRowHeight(30);//设置行高
	    	}
	    }
	    $objPHPExcel->setActiveSheetIndex(0);//设置打开表一
	    $doc_name = date(YmdHis).rand(10,99);
	    $objWriter->save($_SERVER['DOCUMENT_ROOT'].'/LOG/'.$doc_name.'.xls');
		$fileKey = $doc_name.'.xls';
	    $filePath = 'LOG/'.$doc_name.'.xls';
	    $rr = A('Common/SendMail','Model')
								->sent_to_adclue_media(
														array('caoyong@finesoft.com.cn','649937153@qq.com','yl88yy@163.com'),
														'城阳'.$date.'监测日报',
														'尊敬的领导，这是您的监测日报，请查看附件，该数据仅用于参考',
														array(array('path'=>$filePath,'name'=>$date.'日报表.xls')),
														'浙江华治数聚'
													);
		

		var_dump($rr);
		unlink($filePath);
		

	
		
		
	}
	
	
	
	
	



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
}