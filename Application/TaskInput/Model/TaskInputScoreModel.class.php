<?php
namespace TaskInput\Model;
use Think\Model;
class TaskInputScoreModel extends Model
{
    // 环节分数
    const INPUT_SCORE_GENERAL_TV    = 1;    // 录入电视一般广告
    const INPUT_SCORE_GENERAL_BC    = 2;    // 录入广播一般广告
    const INPUT_SCORE_GENERAL_PAPER = 0.7;  // 录入报纸一般广告
    const INPUT_SCORE_GENERAL_NET   = 2;    // 录入网络一般广告
    const INPUT_SCORE_LONG_TV       = 3;    // 录入电视长广告
    const INPUT_SCORE_LONG_BC       = 6;    // 录入广播长广告
    const INSPECT_SCORE_GENERAL_TV             = 1;     // 审核电视一般广告不违法
    const INSPECT_SCORE_GENERAL_TV_ILLEGAL     = 3;     // 审核电视一般广告违法
    const INSPECT_SCORE_GENERAL_BC             = 2;     // 审核广播一般广告不违法
    const INSPECT_SCORE_GENERAL_BC_ILLEGAL     = 6;     // 审核广播一般广告违法
    const INSPECT_SCORE_GENERAL_PAPER          = 0.7;   // 审核报纸一般广告不违法
    const INSPECT_SCORE_GENERAL_PAPER_ILLEGAL  = 2.1;   // 审核报纸一般广告违法
    const INSPECT_SCORE_GENERAL_NET            = 1.5;   // 审核网络一般广告不违法
    const INSPECT_SCORE_GENERAL_NET_ILLEGAL    = 4.5;   // 审核网络一般广告违法
    const INSPECT_SCORE_SPECIFIC_TV            = 3;     // 审核电视特殊(五大类)广告不违法
    const INSPECT_SCORE_SPECIFIC_TV_ILLEGAL    = 6;     // 审核电视特殊(五大类)广告违法
    const INSPECT_SCORE_SPECIFIC_BC            = 6;     // 审核广播特殊(五大类)广告不违法
    const INSPECT_SCORE_SPECIFIC_BC_ILLEGAL    = 12;    // 审核广播特殊(五大类)广告违法
    const INSPECT_SCORE_SPECIFIC_PAPER         = 2.1;   // 审核报纸特殊(五大类)广告不违法
    const INSPECT_SCORE_SPECIFIC_PAPER_ILLEGAL = 4.2;   // 审核报纸特殊(五大类)广告违法
    const INSPECT_SCORE_SPECIFIC_NET           = 4.5;   // 审核网络特殊(五大类)广告不违法
    const INSPECT_SCORE_SPECIFIC_NET_ILLEGAL   = 9;     // 审核网络特殊(五大类)广告违法
    const INSPECT_SCORE_LONG_TV                = 4;     // 审核电视长广告不违法
    const INSPECT_SCORE_LONG_TV_ILLEGAL        = 8;     // 审核电视长广告违法
    const INSPECT_SCORE_LONG_BC                = 8;     // 审核广播长广告不违法
    const INSPECT_SCORE_LONG_BC_ILLEGAL        = 12;    // 审核广播长广告违法

    const QUALITY_INSPECTION_SCORE             = 1.5;   // 质检每条分值 2019-07-16
    const NET_CHECK_SCORE                      = 1;     // 互联网数据检查任务每条分值 2019-05-17
    const NET_CHECKADD_SCORE                   = 5;     // 互联网数据补录任务每条分值 2019-07-26
    const NET_CHECK_SCORE_SH                   = 1;     // 互联网数据检查审核任务每条分值 2019-05-17
    const INSPECT_SCORE_GENERAL_YSOD           = 1;     // 户外广告预审任务不违法每条分值 2019-06-14
    const INSPECT_SCORE_GENERAL_YSOD_ILLEGAL   = 3;     // 户外广告预审任务违法每条分值 2019-06-14
    const INSPECT_SCORE_GENERAL_WPOD           = 1;     // 户外广告违法判定任务不违法每条分值 2019-06-14
    const INSPECT_SCORE_GENERAL_WPOD_ILLEGAL   = 3;     // 户外广告违法判定任务违法每条分值 2019-06-14
    const INSPECT_SCORE_SPECIFIC_YSOD          = 3;     // 户外特殊(五大类)广告预审任务不违法每条分值 2019-06-14
    const INSPECT_SCORE_SPECIFIC_YSOD_ILLEGAL  = 6;     // 户外特殊(五大类)广告预审任务违法每条分值 2019-06-14
    const INSPECT_SCORE_SPECIFIC_WPOD          = 3;     // 户外特殊(五大类)广告违法判定任务不违法每条分值 2019-06-14
    const INSPECT_SCORE_SPECIFIC_WPOD_ILLEGAL  = 6;     // 户外特殊(五大类)广告违法判定任务违法每条分值 2019-06-14

    const UPLOAD_PAPER                         = 2;     // 上传报纸版面 2020-03-01
    const ILLEGAL_REVIEW                       = 5;     // 违法广告确认 2020-03-01

    const LONG_AD_LENGTH = 300; // 长广告时长定义

    protected $specificAdClass          = [];                              // 特殊广告的分类(五大类), 下面定义的一级分类下的分类会自动包含其中
    protected $specificAdClassFirstLvl  = ['01', '02', '03', '06', '13'];  // 特殊广告分类的一级分类,
    protected $specificAdClassFirstLvl2 = ['药品类', '医疗器械类', '化妆品类', '保健食品类', '医疗服务类'];  // 特殊广告分类的一级分类,
    protected $ignoreAdClass            = ['2202', '2302'];                // 不计分的广告分类
    protected $ignoreAdClass2           = ['公益广告', '节目预告'];                // 不计分的广告分类

    public function _initialize()
    {
        // 合并需要特殊计分的广告分类
        $res = M('tadclass')
            ->where([
                'left(fcode, 2)' => ['IN', $this->specificAdClassFirstLvl]
            ])
            ->cache(true)
            ->getField('fcode', true);
        $this->specificAdClass = array_merge($this->specificAdClass, $res);
    }

    /**
     * 提交传统媒体任务加分
     * @param $taskid
     * @param $type
     * @return mixed
     */
    public function addScoreBySubmitTask($taskid, $type, $coment='')
    {
        $taskInfo = M('task_input')
            ->master(true)
            ->find($taskid);
        $taskInfo['fadclasscode'] = M('tad')->where(['fadid' => ['EQ', $taskInfo['fadid']]])->getField('fadclasscode');
        if ($type == 1){
            $res = $this->addScoreByInputTask($taskInfo, $coment);
        }else{
            $res = $this->addScoreByInspectTask($taskInfo);
        }
        return $res;
    }

    /**
     * 提交网络任务加分
     * @param $taskid
     * @param $type
     * @return mixed
     */
    public function addScoreBySubmitNetTask($taskid, $type, $coment='')
    {
        $taskInfo = M('tnettask')
            ->master(true)
            ->find($taskid);
        $taskInfo['lasttime'] = $taskInfo['modifytime'];
        if ($type == 1){
            $res = $this->addScoreByInputTask($taskInfo, $coment);
        }else{
            $res = $this->addScoreByInspectTask($taskInfo);
        }
        return $res;
    }

    /**
     * 录入加分
     */
    protected function addScoreByInputTask($taskInfo,$coment='')
    {
		if ($taskInfo['media_class'] == 3){
			$score = self::INPUT_SCORE_GENERAL_PAPER;
			$reason = '报纸一般广告录入';
		};
		if ($taskInfo['media_class'] == 13){
			$score = self::INPUT_SCORE_GENERAL_NET;
			$reason = '网络一般广告录入';
		};
        if ($this->isLongAd($taskInfo)){
			if ($taskInfo['media_class'] == 1){
				$score = self::INPUT_SCORE_LONG_TV;
				$reason = '电视长广告录入';
			};
			if ($taskInfo['media_class'] == 2){
				$score = self::INPUT_SCORE_LONG_BC;
				$reason = '广播长广告录入';
			};
		}else{
			if ($taskInfo['media_class'] == 1){
				$score = self::INPUT_SCORE_GENERAL_TV;
				$reason = '电视一般广告录入';
			};
			if ($taskInfo['media_class'] == 2){
				$score = self::INPUT_SCORE_GENERAL_BC;
				$reason = '广播一般广告录入';
			};
        }
        if($coment){
            $reason = $coment.$reason;
        }
        return $this->addScoreRow($taskInfo, $score, $reason, 1);
    }

    /**
     * 审核加分
     */
    protected function addScoreByInspectTask($taskInfo)
    {
        // 判断, 如果该广告是不计分广告, 并且是审核环节, 直接返回true
        if (in_array($taskInfo['fadclasscode'], $this->ignoreAdClass)){
            $score = 0;
            $reason = '不计分分类广告审核';
        }elseif ($this->isLongAd($taskInfo)){
			if ($taskInfo['media_class'] == 1){
				if ($this->isIllegal($taskInfo)){
					$score = self::INSPECT_SCORE_LONG_TV_ILLEGAL;
					$reason = '电视长广告违法';
				}else{
					$score = self::INSPECT_SCORE_LONG_TV;
					$reason = '电视长广告不违法';
				}
			};
			if ($taskInfo['media_class'] == 2){
				if ($this->isIllegal($taskInfo)){
					$score = self::INSPECT_SCORE_LONG_BC_ILLEGAL;
					$reason = '广播长广告违法';
				}else{
					$score = self::INSPECT_SCORE_LONG_BC;
					$reason = '广播长广告不违法';
				}
			};
        }elseif ($this->isSpecialClass($taskInfo)){
			if ($taskInfo['media_class'] == 1){
				if ($this->isIllegal($taskInfo)){
					$score = self::INSPECT_SCORE_SPECIFIC_TV_ILLEGAL;
					$reason = '电视特殊分类违法';
				}else{
					$score = self::INSPECT_SCORE_SPECIFIC_TV;
					$reason = '电视特殊分类不违法';
				}
			};
			if ($taskInfo['media_class'] == 2){
				if ($this->isIllegal($taskInfo)){
					$score = self::INSPECT_SCORE_SPECIFIC_BC_ILLEGAL;
					$reason = '广播特殊分类违法';
				}else{
					$score = self::INSPECT_SCORE_SPECIFIC_BC;
					$reason = '广播特殊分类不违法';
				}
			};
			if ($taskInfo['media_class'] == 3){
				if ($this->isIllegal($taskInfo)){
					$score = self::INSPECT_SCORE_SPECIFIC_PAPER_ILLEGAL;
					$reason = '报纸特殊分类违法';
				}else{
					$score = self::INSPECT_SCORE_SPECIFIC_PAPER;
					$reason = '报纸特殊分类不违法';
				}
			};
			if ($taskInfo['media_class'] == 13){
				if ($this->isIllegal($taskInfo)){
					$score = self::INSPECT_SCORE_SPECIFIC_NET_ILLEGAL;
					$reason = '网络特殊分类违法';
				}else{
					$score = self::INSPECT_SCORE_SPECIFIC_NET;
					$reason = '网络特殊分类不违法';
				}
			};
        }else{
            if ($taskInfo['media_class'] == 1){
                if ($this->isIllegal($taskInfo)){
                    $score = self::INSPECT_SCORE_GENERAL_TV_ILLEGAL;
                    $reason = '电视一般广告违法';
                }else{
                    $score = self::INSPECT_SCORE_GENERAL_TV;
                    $reason = '电视一般广告不违法';
                }
            };
            if ($taskInfo['media_class'] == 2){
                if ($this->isIllegal($taskInfo)){
                    $score = self::INSPECT_SCORE_GENERAL_BC_ILLEGAL;
                    $reason = '广播一般广告违法';
                }else{
                    $score = self::INSPECT_SCORE_GENERAL_BC;
                    $reason = '广播一般广告不违法';
                }
            };
            if ($taskInfo['media_class'] == 3){
                if ($this->isIllegal($taskInfo)){
                    $score = self::INSPECT_SCORE_GENERAL_PAPER_ILLEGAL;
                    $reason = '报纸一般广告违法';
                }else{
                    $score = self::INSPECT_SCORE_GENERAL_PAPER;
                    $reason = '报纸一般广告不违法';
                }
            };
            if ($taskInfo['media_class'] == 13){
                if ($this->isIllegal($taskInfo)){
                    $score = self::INSPECT_SCORE_GENERAL_NET_ILLEGAL;
                    $reason = '网络一般广告违法';
                }else{
                    $score = self::INSPECT_SCORE_GENERAL_NET;
                    $reason = '网络一般广告不违法';
                }
            }
        }
        return $this->addScoreRow($taskInfo, $score, $reason, 2);
    }

    /**
     * 质检加分
     * @param string $taskid 任务ID
     * @param string $media_class 媒体类别
     * @param string $wx_id 被加分人
     */
    public function addScoreByQuaIns($taskid,$media_class,$wx_id)
    {
        $taskTable = getTableNameByMediaClass($media_class);
        $taskInfo = M($taskTable)
            ->master(true)
            ->find($taskid);
        $taskInfo['fadclasscode'] = M('tad')->where(['fadid'=>$taskInfo['fadid']])->getField('fadclasscode');
        $taskInfo['taskid'] = $taskid;
        $taskInfo['fadid'] = $taskInfo['fadid'];
        $taskInfo['lasttime'] = time();
        $taskInfo['media_class'] = $media_class;
        $score = self::QUALITY_INSPECTION_SCORE;
        $reason = '质检加分';
        $wx_id = $wx_id ? $wx_id : session('wx_id');
        $res = $this->addScoreRow($taskInfo, $score, $reason, 6, $wx_id);
        return $res ;
    }

    /**
     * 互联网数据补录加分
     */
    public function addScoreByNetCheckTask($taskid)
    {
        $taskInfo = M('tnetissue_customer')
            ->master(true)
            ->find($taskid);
        $sampInfo = M('tnetissue')
            ->master(true)
            ->find($taskid);
        $taskInfo['fadclasscode'] = M('tad')->where(['fadid' => ['EQ', $sampInfo['fadid']]])->getField('fadclasscode');
        $taskInfo['taskid'] = $taskid;
        $taskInfo['fadid'] = $sampInfo['fadid'];
        $taskInfo['lasttime'] = $taskInfo['modifytime'];
        $taskInfo['media_class'] = 13; // TODO:媒体类型暂固定为互联网，后续若有扩展需另查询该媒体类型 
        $score = self::NET_CHECKADD_SCORE;
        $reason = '互联网数据补录加分';
        $res = $this->addScoreRow($taskInfo, $score, $reason, 4);
        return $res ;
    }

    /**
     * 互联网数据检查加分（原数据检查审核）
     * by zw
     */
    public function addScoreByNetCheckShTask($taskid,$fstatus = 0)
    {
        $taskInfo = M('tnetissue_customer')
            ->master(true)
            ->find($taskid);
        
        $taskInfo['wx_id'] = $taskInfo['check_wxid'];
        $taskInfo['taskid'] = $taskid;
        $taskInfo['lasttime'] = time();
        $taskInfo['media_class'] = 13; 
        $score = self::NET_CHECK_SCORE_SH;
        if($fstatus == 2){
            $reason = '互联网数据检查加分（一）';
        }elseif($fstatus == 8){
            $reason = '互联网数据检查加分（二）';
        }
        
        $res = $this->addScoreRow($taskInfo, $score, $reason, 5);
        return $res ;
    }

    /**
     * 互联网数据检查扣分（原数据检查审核）
     * by zw
     */
    public function deductionScoreByNetCheckShTask($taskid,$fstatus = 0,$reason)
    {
        $taskInfo = M('tnetissue_customer')
            ->master(true)
            ->find($taskid);

        $jsonInfo = json_encode($taskInfo);

        //判断是否补录过
        $taskCount = $this
            ->where([
                'taskid' => ['EQ', $taskid],
                'type' => ['EQ', 4],
                'media_class' => ['EQ', 13]
            ])
            ->count();

        if($fstatus == 2 && empty($taskCount)){//如果是第一次检查退回，需要扣除众包人员分
            $netaskid = M('tnettask')->where(['major_key'=>$taskid])->getField('taskid');//读取众包任务ID

            //根据退回原因，判定为录入问题
            if(strpos($reason,'广告信息有误') !== false && !empty($netaskid)){
                $scoreRow = $this
                    ->field('score, wx_id')
                    ->where([
                        'taskid' => ['EQ', $netaskid],
                        'type' => ['EQ', 1],
                        'score' => ['GT', 0],
                        'media_class' => ['EQ', 13]
                    ])
                    ->order('id desc')
                    ->find();

                if (!empty($scoreRow)){
                    $addData = [
                        'taskid' => $netaskid,
                        'wx_id' => $scoreRow['wx_id'],
                        'update_time' => time(),
                        'score' => -3*$scoreRow['score'],//3倍扣分
                        'reason' => $reason,
                        'task_info' => $jsonInfo,
                        'type' => 1,
                        'media_class' => 13,
                        'score_date'  => date('Y-m-d'),
                    ];
                    $res = $this->add($addData);
                    A('Common/AliyunLog','Model')->task_score_log($addData);
                }
            }

            //根据退回原因，判定为审核问题
            if(strpos($reason,'违法判定有误') !== false){
                $scoreRow = $this
                    ->field('score, wx_id')
                    ->where([
                        'taskid' => ['EQ', $netaskid],
                        'type' => ['EQ', 2],
                        'media_class' => ['EQ', 13]
                    ])
                    ->order('id desc')
                    ->find();
                if (!empty($scoreRow)){
                    $addData = [
                        'taskid' => $netaskid,
                        'wx_id' => $scoreRow['wx_id'],
                        'update_time' => time(),
                        'score' => -3*$scoreRow['score'],//3倍扣分
                        'reason' => $reason,
                        'task_info' => $jsonInfo,
                        'type' => 2,
                        'media_class' => 13,
                        'score_date'  => date('Y-m-d'),
                    ];
                    $res = $this->add($addData);
                    A('Common/AliyunLog','Model')->task_score_log($addData);
                }
            }
        }elseif($fstatus == 8){//如果是补录后被退回，需要扣除补录人员分
            if(strpos($reason,'广告信息有误') !== false || strpos($reason,'违法判定有误') !== false){
                $scoreRow = $this
                    ->field('score, wx_id')
                    ->where([
                        'taskid' => ['EQ', $taskid],
                        'type' => ['EQ', 4],
                        'score' => ['GT', 0],
                        'media_class' => ['EQ', 13]
                    ])
                    ->order('id desc')
                    ->find();

                if (!empty($scoreRow)){
                    $addData = [
                        'taskid' => $taskid,
                        'wx_id' => $scoreRow['wx_id'],
                        'update_time' => time(),
                        'score' => -3*$scoreRow['score'],//3倍扣分
                        'reason' => $reason,
                        'task_info' => $jsonInfo,
                        'type' => 4,
                        'media_class' => 13,
                        'score_date'  => date('Y-m-d'),
                    ];
                    $res = $this->add($addData);
                    A('Common/AliyunLog','Model')->task_score_log($addData);
                }
            }
        }
        
        return true;
    }

    /**
     * 户外广告加分
     */
    public function addScoreByOdCheckTask($taskid,$type,$ill)
    {
        $taskInfo = M('todtask')
            ->master(true)
            ->find($taskid);
        if (!$taskInfo['wx_id'] || $taskInfo['wx_id'] == '0'){
            return true;
        }

        $taskInfo['lasttime'] = $taskInfo['modifytime'];
        $taskInfo['media_class'] = 5; // TODO:媒体类型暂固定为户外，后续若有扩展需另查询该媒体类型 

        // 找到该任务最新的一条记录是否加分, 如果加过了, 就不加分了
        $res = $this
            ->where([
                'taskid' => ['EQ', $taskInfo['taskid']],
                'type' => ['EQ', $type],
                'media_class' => ['EQ', $taskInfo['media_class']],
            ])
            ->select();
        foreach ($res as $key => $value) {
            if ($value['score'] > 0){
                return true;
            }
        }

        $do_ad = M('tod_ad_info')->field('class_name')->find($taskInfo['fadinfo_id']);
        $classname = explode(' / ',$do_ad['class_name']);
        if($type == 1){//预审
            if(in_array($classname[1], $this->ignoreAdClass2)){
                $score = 0;
                $reason = '不计分分类户外广告预审';
            }elseif (in_array($classname[0], $this->specificAdClassFirstLvl2)){
                if($ill == 3){
                    $score = self::INSPECT_SCORE_SPECIFIC_YSOD_ILLEGAL;
                    $reason = '户外特殊分类广告预审不通过';
                }else{
                    $score = self::INSPECT_SCORE_SPECIFIC_YSOD;
                    $reason = '户外特殊分类广告预审通过';
                }
            }else{
               if($ill == 3){
                    $score = self::INSPECT_SCORE_GENERAL_YSOD_ILLEGAL;
                    $reason = '户外一般分类广告预审不通过';
                }else{
                    $score = self::INSPECT_SCORE_GENERAL_YSOD;
                    $reason = '户外一般分类广告预审通过';
                }
            }
        }else{//违法判定
            if(in_array($classname[1], $this->ignoreAdClass2)){
                $score = 0;
                $reason = '不计分分类户外广告审核';
            }elseif (in_array($classname[0], $this->specificAdClassFirstLvl2)){
                if(!empty($ill)){
                    $score = self::INSPECT_SCORE_SPECIFIC_WPOD_ILLEGAL;
                    $reason = '户外特殊分类广告审核违法';
                }else{
                    $score = self::INSPECT_SCORE_SPECIFIC_WPOD;
                    $reason = '户外特殊分类广告审核不违法';
                }
            }else{
               if(!empty($ill)){
                    $score = self::INSPECT_SCORE_GENERAL_WPOD_ILLEGAL;
                    $reason = '户外一般分类广告审核违法';
                }else{
                    $score = self::INSPECT_SCORE_GENERAL_WPOD;
                    $reason = '户外一般分类广告审核不违法';
                }
            }
        }
      
        $addData = [
            'taskid' => $taskInfo['taskid'],
            'wx_id' => $taskInfo['wx_id'],
            'update_time' => $taskInfo['lasttime'],
            'media_class' => $taskInfo['media_class'],
            'score' => $score,
            'reason' => $reason,
            'task_info' => '',
            'type' => $type,
            'score_date'  => date('Y-m-d'),
        ];
        $res = $this->add($addData);
        if ($res){
            A('Common/AliyunLog','Model')->task_score_log($addData);
        }
        return $res;
    }

    /**
     * 上传报纸版面加分
     */
    public function addScoreByUploadPaper($taskInfo){
        $score = self::UPLOAD_PAPER;
        $reason = '上传报纸版面加分';
        return $this->addScore(7,$taskInfo['wx_id'], $score,$taskInfo['update_time'], $reason, 3,$taskInfo['taskid'],json_encode($taskInfo));
    }
    /**
     * 上传报纸版面减分
     */
    public function minusScoreByUploadPaper($taskInfo){
        $score = self::UPLOAD_PAPER;
        $scoreInfo = M('task_input_score')->where(['type'=>7,'wx_id'=>$taskInfo['wx_id'],'media_class'=>3,'taskid'=>$taskInfo['taskid']])->find();
        if($scoreInfo['score'] > 0){
            $score = -$scoreInfo['score'];
            $reason = '删除报纸版面减分';
            return $this->addScore(7,$taskInfo['wx_id'], $score,$taskInfo['update_time'], $reason, 3,$taskInfo['taskid'],json_encode($taskInfo));
        }else{
            return true;
        }
    }
    /**
     * 违法广告确认加分
     */
    public function addScoreByIllegalReview($taskInfo){
        $score = self::ILLEGAL_REVIEW;
        $reason = '违法广告确认加分';
        return $this->addScore($taskInfo, $score, $reason, 8);
        return $this->addScore(8,$taskInfo['wx_id'], $score,$taskInfo['update_time'], $reason, $taskInfo['media_class'],$taskInfo['taskid']);
    }
    /**
     * @Des: 积分记录(加/减分)
     * @Edt: yuhou.wang
     * @Date: 2020-03-01 09:28:14
     * @param {type} 
     * @return: 
     */
    public function addScore($type,$wx_id=0,$score=0,$update_time=0,$reason='',$media_class=0,$taskid=0,$task_info='',$syn_flag=0,$score_date=''){
        if($type && $wx_id && $score){
            $addData = [
                'taskid'      => $taskid,
                'wx_id'       => $wx_id,
                'update_time' => $update_time ? $update_time : time(),
                'media_class' => $media_class,
                'score'       => $score,
                'reason'      => $reason,
                'task_info'   => $task_info,
                'type'        => $type,
                'score_date'  => $score_date ? $score_date : date('Y-m-d'),
            ];
            $res = $this->add($addData);
            if ($res){
                A('Common/AliyunLog','Model')->task_score_log($addData);
            }
            return $res;
        }else{
            return false;
        }
    }
    /**
     * 加分
     */
    protected function addScoreRow($taskInfo, $score, $reason, $type, $wx_id = 0)
    {
        $wx_id =  $wx_id ? $wx_id : $taskInfo['wx_id'];
        if (!$wx_id || $wx_id == '0'){
            return true;
        }
        // 找到该任务最新的一条记录是否加分, 如果加过了, 就不加分了
        $res = $this
            ->where([
                'taskid'      => ['EQ', $taskInfo['taskid']],
                'type'        => ['EQ', $type],
                'media_class' => ['EQ', $taskInfo['media_class']],
            ])
            ->order('update_time desc')
            ->find();
        if ($res['score'] > 0){
            return true;
        }
        $adInfo = M('tad')->master(true)->find($taskInfo['fadid']);
        $jsonInfo = [
            'task' => $taskInfo,
            'ad' => $adInfo,
        ];
        $jsonInfo = json_encode($jsonInfo);
        $addData = [
            'taskid'      => $taskInfo['taskid'],
            'wx_id'       => $wx_id,
            'update_time' => $taskInfo['lasttime'],
            'media_class' => $taskInfo['media_class'],
            'score'       => $score,
            'reason'      => $reason,
            'task_info'   => $jsonInfo,
            'type'        => $type,
            'score_date'  => date('Y-m-d'),
        ];
        $res = $this->add($addData);
        if ($res){
            A('Common/AliyunLog','Model')->task_score_log($addData);
        }
        return $res;

    }

    /**
     * 判断长广告
     * @param $taskInfo
     * @return bool
     */
    protected function isLongAd($taskInfo)
    {
        // 判断是否是长广告
        if ($taskInfo['media_class'] != 3 && $taskInfo['media_class'] != 13){
            $sampleTable = $taskInfo['media_class'] == 1 ? 'ttvsample' : 'tbcsample';
            $isLongAd = M($sampleTable)->where(['fid' => ['EQ', $taskInfo['sam_id']]])->getField('fadlen') >= self::LONG_AD_LENGTH;
            return $isLongAd;
        }
        return false;
    }

    /**
     * 判断特殊分类广告(五大类)
     * @param $taskInfo
     * @return bool
     */
    public function isSpecialClass($taskInfo)
    {
        if (in_array($taskInfo['fadclasscode'], $this->specificAdClass)){
            return true;
        }
        return false;
    }

    /**
     * 判断是否违法
     * @param $taskInfo
     * @return bool
     */
    protected function isIllegal($taskInfo)
    {
        return $taskInfo['fillegaltypecode'] != '0';
    }

    /**
     * 退回任务扣除分数
     * @param $taskid 任务ID
     * @param $media_class 媒体类别
     * @param $type 加分类别
     * @param $userId 任务负责人的ID(被退回扣分人)
     * @param $operatorId 退回操作人的ID
     * @return bool|mixed
     */
    public function rejectTask($taskid, $media_class, $type, $userId, $operatorId)
    {
        // 找到此类型提交时加的分数
        $scoreRow = $this
            ->field('score, reason')
            ->where([
                'taskid' => ['EQ', $taskid],
                'type' => ['EQ', $type],
                'media_class' => ['EQ', $media_class],
                'wx_id' => ['EQ', $userId],
            ])
            ->order('update_time desc')
            ->find();
        // 如果找不到, 就是没加分, 就不扣分了
        if (!$scoreRow){
            return true;
        }
        // 如果上一条数据是负数,就是已经扣过分了, 返回false
        if ($scoreRow['score'] < 0){
            return true;
        }
        // 自己退回的任务只扣除1倍积分
        if($userId == $operatorId){
            $time = -1;
        }else{
            $time = -3;
        }
        $score = $scoreRow['score'] * $time; // 任务被退回扣除3倍积分(自己退回除外)
        $taskTable = $media_class != 13 ? 'task_input' : 'tnettask';
        $taskInfo = M($taskTable)->find($taskid);
        $adInfo = M('tad')->find($taskInfo['fadid']);
        $jsonInfo = [
            'task' => $taskInfo,
            'ad' => $adInfo,
        ];
        $jsonInfo = json_encode($jsonInfo);
        $addData = [
            'taskid'      => $taskInfo['taskid'],
            'wx_id'       => $userId,
            'update_time' => time(),
            'score'       => $score,
            'reason'      => '任务退回扣分: '.$scoreRow['reason'],
            'task_info'   => $jsonInfo,
            'type'        => $type,
            'media_class' => $media_class,
            'score_date'  => date('Y-m-d'),
        ];
        $res = $this->add($addData);
        if ($res){
            A('Common/AliyunLog','Model')->task_score_log($addData);
        }
        return $res;
    }
    /**
     * 修改任务扣除分数，类似退回任务
     * @param $taskid 任务ID
     * @param $media_class 媒体类别
     * @param $type 加分类别
     * @param $userId 任务负责人的ID(被退回扣分人)
     * @param $operatorId 退回操作人的ID
     * @return bool|mixed
     */
    public function modifyTask($taskid, $media_class, $type, $userId, $operatorId)
    {
        // 找到此类型提交时加的分数
        $scoreRow = $this
            ->field('score, reason')
            ->where([
                'taskid' => ['EQ', $taskid],
                'type' => ['EQ', $type],
                'media_class' => ['EQ', $media_class],
                'wx_id' => ['EQ', $userId],
            ])
            ->order('update_time desc')
            ->find();
        // 如果找不到, 就是没加分, 就不扣分了
        if (!$scoreRow){
            return true;
        }
        // 如果上一条数据是负数,就是已经扣过分了, 返回false
        if ($scoreRow['score'] < 0){
            return true;
        }
        // 自己修改的任务只扣除1倍积分
        if($userId == $operatorId){
            $time = -1;
        }else{
            $time = -3;
        }
        $score = $scoreRow['score'] * $time; // 任务被退回扣除3倍积分(自己退回除外)
        $taskTable = $media_class != 13 ? 'task_input' : 'tnettask';
        $taskInfo = M($taskTable)->find($taskid);
        $adInfo = M('tad')->find($taskInfo['fadid']);
        $jsonInfo = [
            'task' => $taskInfo,
            'ad' => $adInfo,
        ];
        $jsonInfo = json_encode($jsonInfo);
        $addData = [
            'taskid'      => $taskInfo['taskid'],
            'wx_id'       => $userId,
            'update_time' => time(),
            'score'       => $score,
            'reason'      => '任务被修改扣分: '.$scoreRow['reason'],
            'task_info'   => $jsonInfo,
            'type'        => $type,
            'media_class' => $media_class,
            'score_date'  => date('Y-m-d'),
        ];
        $res = $this->add($addData);
        if ($res){
            A('Common/AliyunLog','Model')->task_score_log($addData);
        }
        return $res;
    }
}