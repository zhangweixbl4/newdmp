<?php
namespace InputPc\Controller;
use Think\Controller;
class PaperSourceController extends BaseController {
	

	

    public function index(){
		//var_dump(file_up());
		if(	!A('InputPc/Task','Model')->user_other_authority('1004')){
			exit('无权限');
		}
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->display();
	}
	
	public function add_papersource(){
		if(	!A('InputPc/Task','Model')->user_other_authority('1004')){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
		}
		$wx_id = intval(session('wx_id'));
		$userInfo = M('ad_input_user')->where(array('wx_id'=>$wx_id))->find();
		$fmediaid = I('fmediaid');//媒介id
		$fissuedate = date('Y-m-d',strtotime(I('fissuedate')));//发布日期
		$fpage = I('fpage');//版面
		$fpagetype = I('fpagetype');//版面类型
		$furl = I('furl');//存储位置
		
		$mediaInfo = M('tmedia')->where(array('fid'=>$fmediaid))->find();//查询是否存在该媒体
		if(!$mediaInfo) $this->ajaxReturn(array('code'=>-1,'msg'=>'添加或修改数据失败,媒体ID有错误'));
		if($furl == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'添加或修改数据失败,储存位置不能为空'));
		
		
		$a_e_data['fmediaid'] = $fmediaid;//媒体ID
		$a_e_data['fissuedate'] = $fissuedate;//发布日期
		$a_e_data['fpage'] = $fpage;//版面
		$a_e_data['priority'] = $mediaInfo['priority'] + 1000;//
		
		
		if(M('tpapersource')->where($a_e_data)->count() > 0){//判断是否存在数据
			$this->ajaxReturn(array('code'=>-1,'msg'=>'请勿重复添加'));
		}
		
		$a_e_data['fpagetype'] = $fpagetype;//版面类型
		$a_e_data['furl'] = $furl;//存储位置
		$a_e_data['fcreator'] = $userInfo['nickname'];//创建人
		$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$a_e_data['fmodifier'] = $userInfo['nickname'];//修改人
		$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		
		$sourceId = M('tpapersource')->add($a_e_data);//创建
		if($sourceId > 0){
			M('tmedia')->where(array('fid'=>$fmediaid))->save(array('fsort'=>array('exp','fsort+1')));//修改媒介排序
			A('InputPc/Task','Model')->change_task_count($wx_id,'paper_page_up',1);//增加上传版面数量
			$this->ajaxReturn(array('code'=>0,'msg'=>$mediaInfo['fmedianame'].'_'.$fissuedate.'_'.$fpage.'添加成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));
		}
		

	}
	
	
	
	
}