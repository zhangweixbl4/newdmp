<?php

namespace Admin\Controller;
use Think\Controller;
class SeperatetaskController extends BaseController {

    public function index(){
		
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$keyword = I('keyword');//搜索关键词
		$where = array();
		if($keyword != ''){
			$where['tmedia.fmedianame'] = array('like','%'.$keyword.'%');
		} 

		$count = M('tseperatetask')
									->join('tmedia on tmedia.fid = tseperatetask.fmediaid')
									->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$taskList = M('tseperatetask')
									->field('tseperatetask.*,tmedia.fmedianame')
									->join('tmedia on tmedia.fid = tseperatetask.fmediaid')
									->where($where)->limit($Page->firstRow.','.$Page->listRows)->order('tseperatetask.fissuedate desc,tmedia.fmedianame asc')->select();
		//var_dump($taskList);
		$taskList = list_k($taskList,$p,$pp);//为列表加上序号
		$this->assign('taskList',$taskList);
		$this->assign('page',$Page->show());//分页输出
		$this->display();
	}
	
	
	/*任务删除*/
	public function delete_task(){
		$fseperatetaskid = I('fseperatetaskid');//任务ID
		if(M('tseperatetask')->where(array('fseperatetaskid'=>$fseperatetaskid))->delete()){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败'));
		}
		
	}
	
	/*任务编辑*/
	public function edit_task(){
		$fstate = I('fstate');//任务状态
		$fseperatetaskid = I('fseperatetaskid');//任务ID
		
		if(M('tseperatetask')->where(array('fseperatetaskid'=>$fseperatetaskid))->save(array('fstate'=>$fstate))){
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败'));
		}
		
		
	}

	
	
	
	
	
		
	

}