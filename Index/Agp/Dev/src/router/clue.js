const clue = [{
    path: '/cluelist',
    name: 'cluelist',
    component: () => import('../components/clue/clueList'),
    meta: ['线索管理', '线索登记']
  },
  {
    path: '/registrationlist',
    name: 'registrationlist',
    component: () => import('../components/clue/registrationList'),
    meta: ['线索管理', '登记任务']
  },
  {
    path: '/resapprovallist',
    name: 'resapprovallist',
    component: () => import('../components/clue/resapprovalList'),
    meta: ['线索管理', '登记签批']
  },
  {
    path: '/resrecheck',
    name: 'resrecheck',
    component: () => import('../components/clue/resRecheck'),
    meta: ['线索管理', '线索处置']
  },
  {
    path: '/recheckapprovallist',
    name: 'recheckapprovallist',
    component: () => import('../components/clue/recheckapprovalList'),
    meta: ['线索管理', '处置签批']
  },
  {
    path: '/fxschenban',
    name: 'fxschenban',
    component: () => import('../components/clue/fxschenban'),
    meta: ['线索管理', '线索承办']
  },
  {
    path: '/cluefeedback',
    name: 'cluefeedback',
    component: () => import('../components/clue/clueFeedback'),
    meta: ['线索管理', '线索反馈']
  },
  {
    path: '/feedbackapprovallist',
    name: 'feedbackapprovallist',
    component: () => import('../components/clue/feedbackapprovalList'),
    meta: ['线索管理', '反馈签批']
  },
  {
    path: '/cluetrack',
    name: 'cluetrack',
    component: () => import('../components/clue/clueTrack'),
    meta: ['线索管理', '线索跟踪']
  },

  {
    path: '/approvalHistory',
    name: 'approvalHistory',
    component: () => import('../components/clue/approvalHistory'),
    meta: ['线索管理', '线索台账']
  }
]

export default clue