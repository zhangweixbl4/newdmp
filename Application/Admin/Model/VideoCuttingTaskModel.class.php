<?php
namespace Admin\Model;



class VideoCuttingTaskModel{


    /*遍历发布片段并在片段中间的空隙加入片段*/
    public function filling_part($taskInfo,$originalPartLast){
		
		$previous_start_time = $taskInfo['Start'];//上一个片段结束时间

		foreach($originalPartLast as $key => $part){//循环广告片段
			$differ_time = $part['Start'] - $previous_start_time ;//相差时间

			if($differ_time <= 3){//与上一片段相差3秒内
				
			}elseif($differ_time > 3 && $differ_time < 30){//与上一片段相差3到30秒
				$partLast[] = array(//中间塞入片段
									'Start_time'=>date('H:i:s',$previous_start_time),//开始时间是上一片段的结束时间
									'Stop_time'=>date('H:i:s',$part['Start']),//结束时间是当前片段的开始时间
									'Start'=>$previous_start_time,
									'Stop'=>$part['Start'],
									'name'=>'疑似广告',
									);
				
			}else{
				$partLast[] = array(//中间塞入片段
									'Start_time'=>date('H:i:s',$previous_start_time),//开始时间是上一片段的结束时间
									'Stop_time'=>date('H:i:s',$part['Start']),//结束时间是当前片段的开始时间
									'Start'=>$previous_start_time,
									'Stop'=>$part['Start'],
									'name'=>'查看素材',
									);
				
			}
			
			$partLast[] = $part;
			$previous_start_time = $part['Stop'];//设置上一片段结束时间
		}
		
		if($taskInfo['Stop'] - $previous_start_time <= 3){//任务的结束时间减去最后一个片段的结束时间
			
		}elseif($taskInfo['Stop'] - $previous_start_time > 3 && $taskInfo['Stop'] - $previous_start_time < 30){
			$partLast[] = array(//中间塞入片段
									'Start_time'=>date('H:i:s',$previous_start_time),//开始时间是上一片段的结束时间
									'Stop_time'=>date('H:i:s',$taskInfo['Stop']),//结束时间是任务的结束时间
									'Start'=>$previous_start_time,
									'Stop'=>$taskInfo['Stop'],
									'name'=>'疑似广告',
									);
		}else{
			$partLast[] = array(//中间塞入片段
									'Start_time'=>date('H:i:s',$previous_start_time),//开始时间是上一片段的结束时间
									'Stop_time'=>date('H:i:s',$taskInfo['Stop']),//结束时间是任务的结束时间
									'Start'=>$previous_start_time,
									'Stop'=>$taskInfo['Stop'],
									'name'=>'查看素材',
									);
		}
		return $partLast;

	}
	
	/*导入发布数据到数据库*/
	public function leading_in_issue(){
		
		$partList = M('video_cutting_part')->where(array('is_data'=>0))->select();//未导入的数据列表
		$in = 0;
		$de = 0;
		foreach($partList as $part ){//循环为导入的片段表
			$mediaInfo = M('tmedia')->where(array('fid'=>$part['Media']))->find();//查询媒介
			if(mb_substr($mediaInfo['fmediaclassid'],0,2) == '01'){//判断是电视
				
				$sampleInfo = M('ttvsample')->where(array('smp_id'=>$part['Sample']))->find();//样本信息
				if($sampleInfo){
					$rr = M('ttvissue')->add(array(
											'ftvsampleid'=>$sampleInfo['fid'],//样本ID
											'fmediaid'=>$mediaInfo['fid'],//媒介ID
											'fissuedate'=>date('Y-m-d',$part['Start']/10),//发布日期
											'fstarttime'=>date('Y-m-d H:i:s',$part['Start']/10),//发布开始时间
											'fendtime'=>date('Y-m-d H:i:s',$part['Stop']/10),//发布结束时间
											'flength'=> ($part['Stop'] - $part['Start']) / 10,//发布时长
											'fcreator'=>'平台监测',
											'fcreatetime'=>date('Y-m-d H:i:s'),
											'fmodifier'=>'平台监测',
											'fmodifytime'=>date('Y-m-d H:i:s'),
											'fstate'=>1,
											
											));
					if($rr > 0 ){
						M('video_cutting_part')->where(array('Index'=>$part['Index']))->save(array('is_data'=>1));
						$in++;
					} 						
				
				}else{
					if((time() - intval($part['Start'] / 10)) > 86400*15){
						
						M('video_cutting_part')->where(array('Index'=>$part['Index']))->delete();
						$de++;
					}  
				}
				
			}elseif(mb_substr($mediaInfo['fmediaclassid'],0,2) == '02'){
				$sampleInfo = M('tbcsample')->where(array('smp_id'=>$part['Sample']))->find();//样本信息
				if($sampleInfo){
					$rr = M('tbcissue')->add(array(
											'fbcsampleid'=>$sampleInfo['fid'],//样本ID
											'fmediaid'=>$mediaInfo['fid'],//媒介ID
											'fissuedate'=>date('Y-m-d',$part['Start']/10),//发布日期
											'fstarttime'=>date('Y-m-d H:i:s',$part['Start']/10),//发布开始时间
											'fendtime'=>date('Y-m-d H:i:s',$part['Stop']/10),//发布结束时间
											'flength'=> ($part['Stop'] - $part['Start']) / 10,//发布时长
											'fcreator'=>'平台监测',
											'fcreatetime'=>date('Y-m-d H:i:s'),
											'fmodifier'=>'平台监测',
											'fmodifytime'=>date('Y-m-d H:i:s'),
											'fstate'=>1,
											
											));
					if($rr > 0 ){
						M('video_cutting_part')->where(array('Index'=>$part['Index']))->save(array('is_data'=>1));
						$in++;
					} 						
				
				}else{
					if((time() - intval($part['Start'] / 10)) > 86400*15){
						
						M('video_cutting_part')->where(array('Index'=>$part['Index']))->delete();
						$de++;
					}  
				}
			}
			
			
		}
		return array('in'=>$in,'de'=>$de);
		
	}
	
	
	
	
	
	
	
	

}