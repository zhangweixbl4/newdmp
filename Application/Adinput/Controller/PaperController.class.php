<?php
namespace Adinput\Controller;
use Think\Controller;
class PaperController extends Controller {
	
    public function up_source(){
		
		
		get_openid();
		header('Content-Type:text/html;charset=utf-8');
		$wx_jssdk_config = A('Adinput/Weixin','Model')->wx_jssdk_config();
		$this->assign('wx_jssdk_config',$wx_jssdk_config);
        if(!session('wx_id') || M('ad_input_user')->where(['wx_id' => ['EQ', session('wx_id')]])->getField('subscribe') == 0){
            exit('请订阅公众号之后再次尝试');
        }

        if(	!A('InputPc/Task','Model')->user_other_authority('1004')){
            exit('无权限');
        }
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->display();
	}
	
	
	
	
	public function add_papersource(){
		if(	!A('InputPc/Task','Model')->user_other_authority('1004')){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
		}
		$wx_id = intval(session('wx_id'));
		$userInfo = M('ad_input_user')->where(array('wx_id'=>$wx_id))->find();
		$fmediaid = I('fmediaid');//媒介id
		$fissuedate = date('Y-m-d',strtotime(I('fissuedate')));//发布日期
		$fpage = I('fpage');//版面
		$fpagetype = I('fpagetype');//版面类型
		$serverId = I('serverId');//微信服务器素材id
        $noad = I('noad');
		
		

		$mediaInfo = M('tmedia')->where(array('fid'=>$fmediaid))->find();//查询是否存在该媒体
		if(!$mediaInfo) $this->ajaxReturn(array('code'=>-1,'msg'=>'添加或修改数据失败,媒体ID有错误'));
		if($noad == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'请选择是否有广告'));

		
		
		$a_e_data['fmediaid'] = $fmediaid;//媒体ID
		$a_e_data['fissuedate'] = $fissuedate;//发布日期
		$a_e_data['fpage'] = $fpage;//版面
		
		
		if(M('tpapersource')->where($a_e_data)->count() > 0){//判断是否存在数据
			$this->ajaxReturn(array('code'=>-1,'msg'=>'请勿重复添加'));
		}
		$wxServerUrl = 'https://api.weixin.qq.com/cgi-bin/media/get?access_token='.wx_token().'&media_id='.$serverId;//存储位置

		$furl = A('Common/Qiniu','Model')->fetch($wxServerUrl,'paper/'.$fmediaid.'_'.$fissuedate.'_'.base64_encode($fpage).'.jpg');
		if(!$furl){
            $logData = [
                'data' => $a_e_data, 
                'wxServerUrl' => $wxServerUrl,
                'fileName' => 'paper/'.$fmediaid.'_'.$fissuedate.'_'.base64_encode($fpage).'.jpg',
                'type' => 'add',
                'msg' => '获取微信服务器素材失败'
            ];
            A('Common/AliyunLog','Model')->paper_source_log($logData);
			$this->ajaxReturn(array('code'=>-1,'msg'=>'获取微信服务器素材失败,'.$wxServerUrl));
		}
		
		$a_e_data['fpagetype'] = $fpagetype;//版面类型
		$a_e_data['furl'] = $furl;//存储位置
		$a_e_data['noad'] = $noad;
		$a_e_data['fcreator'] = $userInfo['nickname'];//创建人
		$a_e_data['fcreatorid'] = $wx_id;//创建人ID
		$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$a_e_data['fmodifier'] = $userInfo['nickname'];//修改人
		$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		
		$sourceId = M('tpapersource')->add($a_e_data);//创建
		if($sourceId > 0){
			// 计入积分
			$taskInfo = [
				'wx_id'       => $wx_id,
				'taskid'      => $sourceId,
				'update_time' => time(),
				'info'        => $a_e_data
			];
			A('TaskInput/TaskInputScore','Model')->addScoreByUploadPaper($taskInfo);
            $logData = [
                'data' => $a_e_data,
                'type' => 'add',
                'msg' => '微信端报纸上传成功'
            ];
            A('Common/AliyunLog','Model')->paper_source_log($logData);
			M('tmedia')->where(array('fid'=>$fmediaid))->save(array('fsort'=>array('exp','fsort+1')));//修改媒介排序
			A('InputPc/Task','Model')->change_task_count($wx_id,'paper_page_up',1);//增加上传版面数量
			$this->ajaxReturn(array('code'=>0,'msg'=>$mediaInfo['fmedianame'].'_'.$fissuedate.'_'.$fpage.'		添加成功'));
		}else{
            $logData = [
                'data' => $a_e_data,
                'type' => 'add',
                'msg' => '微信端报纸上传失败'
            ];
            A('Common/AliyunLog','Model')->paper_source_log($logData);
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));
		}
		

	}

	
	
	
	

}