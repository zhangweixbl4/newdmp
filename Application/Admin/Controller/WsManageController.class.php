<?php
//电视样本审核控制器
namespace Admin\Controller;
use Think\Controller;
class WsManageController extends BaseController {

    /**
     * 文书类型列表
     * by jim
     */
    public function ws_type_list(){
        $document_type = M('document_type')->where(['de_state'=>0])->order('de_addtime desc')->select();//文书模板列表
        $this->assign('document_type',$document_type);
        $this->display();
    }

    /**
     * 文书类型添加页面
     * by jim
     */
    public function ws_type_add(){
        $this->display();
    }

    /**
     * 文书类型添加操作
     * by jim
     */
    public function ws_type_add_post(){
        $data['de_name'] = I('de_name');//文书类型名称
        $data['de_content'] = I('de_content');//文书内容
        $data['de_createpersonid'] = session('personInfo.fid');//创建人
        $data['de_addtime'] = date('Y-m-d H:i:s',time());//创建时间

        $document_type = M('document_type');
        $result = $document_type->add($data); // 写入数据到数据库
        if($result){
            $this->ajaxReturn(array('code' => 0,'msg'=>'添加成功'));
        }else{
            $this->ajaxReturn(array('code' => -1,'msg'=>'添加失败'));
        }
    }

    /**
     * 文书类型编辑页面
     * by jim
     */
    public function ws_type_edit(){

        $id = I('de_id');
        $ws_type = M("document_type")->field('de_id,de_name,de_content')->where(['de_state'=>0,'de_id'=>$id,'de_state'=>0])->find();
        if($ws_type){
            $this->ajaxReturn(array('code'=>0,'data'=>$ws_type));

        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'文书类型不存在！'));
        }

    }

    /**
     * 文书类型编辑操作
     * by jim
     */
    public function ws_type_edit_post(){
        $data['de_id'] = I('de_id');//文书类型id
        $data['de_name'] = I('de_name');//文书类型名称
        $data['de_content'] = I('de_content');//文书类型备注
        $data['de_editpersonid'] = session('personInfo.fid');//修改人
        $data['de_edittime'] = date('Y-m-d H:i:s',time());//修改时间

        $document_model = M('document_type');
        $result = $document_model->save($data); // 更新文书模板操作
        if($result !== false){
            $this->ajaxReturn(array('code' => 0,'msg'=>'保存成功'));
        }else{
            $this->ajaxReturn(array('code' => -1,'msg'=>'保存失败'));
        }
    }

    /**
     * 文书类型删除操作
     * by jim
     */
    public function ws_type_delete(){

        $data['de_id'] = I('de_id');//文书类型id
        $data['de_delpersonid'] = session('personInfo.fid');//文书类型删除人
        $data['de_deltime'] = date('Y-m-d H:i:s',time());//文书类型删除时间
        $data['de_state'] = 10;//删除时间

        $document_model = M('document_type');
        $result = $document_model->save($data); // 软删除文书模板操作
        if($result !== false){
            $this->ajaxReturn(array('code' => 0,'msg'=>'删除成功'));
        }else{
            $this->ajaxReturn(array('code' => -1,'msg'=>'删除失败'));
        }
    }

    /**
     * 文书模板列表//
     * by jim
     */
    public function ws_them_list(){
        $keyword = I('keyword');//搜索关键词
        $where = [];
        if($keyword != ''){
            $where['dl_name|dl_regulatorname'] = array('like','%'.$keyword.'%');
        }

        $document_model = M('document_model');
        $document_list = $document_model
            ->where($where)
            ->where(['dl_state'=>0])
            ->order('dl_createtime desc')
            ->page($_GET['p'].',15')
            ->select();//文书模板列表
        $count      = $document_model->where($where)->where(['dl_state'=>0])->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,15);// 实例化分页类 传入总记录数和每页显示的记录数
        $show       = $Page->show();// 分页显示输出

        $document_type = M('document_type')->where(['de_state'=>0])->order('de_addtime desc')->select();//文书类型列表
        $tregulator_list = M('tregulator')->field('fid,fname')->where(['fkind'=>1,'fstate'=>1])->select();//机构列表

        $this->assign('page',$show);// 赋值分页输出
        $this->assign('document_list',$document_list);
        $this->assign('document_type',$document_type);
        $this->assign('tregulator_list',$tregulator_list);
        $this->display();
    }

    /**
     * 文书模板添加
     * by jim
     */
    public function ws_them_add(){
        $this->display();
    }

    /**
     * 文书模板添加操作//TODO:文书模板添加页面ok
     * by jim
     */
    public function ws_them_add_post(){
        $is_apply_sub = I('is_apply_sub',0);//是否应用到下级
        $document_model = M('document_model');//实例化文书模板数据模型
        $dl_regulatorid = I('dl_regulatorid');//机构id
        $basic_data = [
            'dl_name' => I('dl_name'),//文书名称
            'dl_deid' => I('dl_deid'),//文书类型
            'dl_dename' => I('dl_dename'),//文书类型名称
            'dl_content' => I('dl_content'),//文书内容
            'dl_doctitle' => I('dl_doctitle'),//文档标题
            'dl_receiveregulatorname' => I('dl_receiveregulatorname'),//收文单位
            'dl_sendtregulatorname' => I('dl_sendtregulatorname'),//发送单位
            'dl_sendtime' => I('dl_sendtime'),//发送时间
            'dl_mark' => I('dl_mark'),//文书备注
            'dl_whrule' => I('dl_whrule'),//文书规则
            'dl_createpersonid' => session('personInfo.fid'),//创建人
            'dl_createtime' => date('Y-m-d H:i:s',time()),//创建时间
            'dl_editpersonid' => session('personInfo.fid'),//修改人
            'dl_edittime' => date('Y-m-d H:i:s',time())//修改时间
        ];
        //如果要应用到下级机构
        if($is_apply_sub == 1){

            $fregionid = M('tregulator')->where(['fid'=>$dl_regulatorid])->getField('fregionid');//查询当前机构所属的地域ID
            $fregionid = rtrim($fregionid,0);//去除为0的后缀

            //如果是国家局这种的特殊机构补0
            if(strlen($fregionid) == 1){
                $fregionid = $fregionid.'0';
            }

            //获取当前机构所在的地域的下级地域ID数组
            $regulatorids = M('tregulator')
                ->where([
                    'fregionid'=>['like',"$fregionid%"],
                    'fkind'=>1
                ])
                ->getField('fid,fname',true);

            //查询有的下级是否已经有此类文书模板存在
            $exist_ws_regulatorids = $document_model->where(['dl_regulatorid'=>['IN',array_keys($regulatorids)],'dl_deid'=>$basic_data['dl_deid']])->getField('dl_regulatorid',true);
            //循环机构id组装需要写入的数据
            foreach ($regulatorids as $regulatorid=>$fname){
                //如果存在的则跳出
                if(in_array($regulatorid,$exist_ws_regulatorids)){
                    continue;
                }else{
                    $basic_data['dl_regulatorid'] = $regulatorid;
                    $basic_data['dl_regulatorname'] = $fname;
                    $data[] = $basic_data;
                }
            }
            $result = $document_model->addAll($data);//批量写入模板数据到数据库
        }else{
            $basic_data['dl_regulatorid'] = $dl_regulatorid;
            $result = $document_model->add($basic_data);//批量写入模板数据到数据库
        }
        if($result > 0){
            $this->ajaxReturn(array('code' => 0,'msg'=>'添加成功'));
        }else{
            $this->ajaxReturn(array('code' => -1,'msg'=>'添加失败'));
        }
    }

    /**
     * 文书模板编辑页面//TODO:文书模板编辑页面
     * by jim
     */
    public function ws_them_edit(){
        $id = I('dl_id');
        $ws_them = M("document_model")->where(['dl_id'=>$id,'dl_state'=>0])->find();
        $ws_them['dl_content'] = htmlspecialchars_decode($ws_them['dl_content']);
        if($ws_them){
            $this->ajaxReturn(array('code'=>0,'data'=>$ws_them));

        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'文书模板不存在！'));
        }
    }

    /**
     * 文书模板编辑操作//TODO:文书模板编辑操作
     * by jim
     */
    public function ws_them_edit_post(){
        $save_type = I('save_type');
        $data['dl_id'] = I('dl_id');//文书id
        $data['dl_name'] = I('dl_name');//文书名称
        $data['dl_content'] = I('dl_content');//文书内容
        $data['dl_deid'] = I('dl_deid');//文书类型
        $data['dl_dename'] = I('dl_dename');//文书类型名称
        $data['dl_doctitle'] = I('dl_doctitle');//文档标题
        $data['dl_receiveregulatorname'] = I('dl_receiveregulatorname');//收文单位
        $data['dl_sendtregulatorname'] = I('dl_sendtregulatorname');//发送单位
        $data['dl_sendtime'] = I('dl_sendtime');//发送时间
        $data['dl_regulatorid'] = I('dl_regulatorid');//机构id
        $data['dl_regulatorname'] = I('dl_regulatorname');//机构名称
        $data['dl_mark'] = I('dl_mark');//文书备注
        $data['dl_whrule'] = I('dl_whrule');//文书规则
        $data['dl_editpersonid'] = session('personInfo.fid');//修改人
        $data['dl_edittime'] = date('Y-m-d H:i:s',time());//修改时间
        $document_model = M('document_model');
        $result = $document_model->save($data); // 更新文书模板操作
        if($result !== false){
            $this->ajaxReturn(array('code' => 0,'msg'=>'保存成功'));
        }else{
            $this->ajaxReturn(array('code' => -1,'msg'=>'保存失败'));
        }
    }

    /**
     * 文书模板删除操作
     * by jim
     */
    public function ws_them_delete(){

        $dl_id = I('dl_id');//文书id

        $document_model = M('document_model');
        $result = $document_model->where(['dl_id'=>$dl_id])->delete(); //删除文书末班
        //删除状态反馈
        if($result){
            $this->ajaxReturn(array('code' => 0,'msg'=>'删除成功'));
        }else{
            $this->ajaxReturn(array('code' => -1,'msg'=>'删除失败'));
        }
    }

}

