let illegalInfo = {
    name: 'illegalInfo',
    props: ['task', 'type'],
    template: `<div class="illegalInfo" v-loading="loading">
    <el-form ref="illegalInfo" :model="task" :label-width="this.task.media_class == 13 ? '80px' : '120px'" :rules="rule" :disabled="type !== 'input'">
        <el-form-item label="违法程度" prop="fillegaltypecode">
            <el-radio-group v-model="task.fillegaltypecode" @change="changeIllegalType">
                <el-radio :label="item.value" v-for="item in illegalTypeArr" :key="item.value">{{item.text}}</el-radio>
            </el-radio-group>
        </el-form-item>
        <div v-if="isIllegal || !task.fillegaltypecode">
            <el-form-item label="违法表现" prop="fexpressioncodes" class="illegalExpression">
                <el-popover
                        placement="left"
                        width="800"
                        trigger="click">
                    <div>
                        <el-input
                                placeholder="输入关键字进行过滤"
                                v-model="illegalKeyword">
                        </el-input>
                        <el-tree
                                class="filter-tree"
                                :data="illegalArr"
                                node-key="fcode"
                                accordion
                                show-checkbox
                                :filter-node-method="filterNode"
                                @check-change="selectIllegal"
                                ref="illegalSelecter">
                        </el-tree>
                    </div>
                    <el-button slot="reference">点击选择</el-button>
                </el-popover>
                <el-card>
                    <div v-for="(item,index) in task.checkedIllegalArr" :key="item.fcode" style="margin: 5px;" title="点击删除" @click="deleteIllegal(index)">
                        <b>{{index+1}}.</b>  {{item.fexpression}}
                    </div>
                </el-card>
            </el-form-item>
        </div>
        <div v-if="isIllegal" >
            <el-form-item label="违法内容" prop="fillegalcontent">
                <div ref="editor" v-if="task.media_class == 13 && type === 'input'"></div>
                <el-input
                        v-if="task.media_class != 13"
                        type="textarea"
                        :rows="3"
                        v-model="task.fillegalcontent">
                </el-input>
                <div v-html="task.fillegalcontent" v-if="task.media_class == 13 && type != 'input'" style="border: 1px solid #EBEEF5; overflow: auto;">
                    
                </div>
            </el-form-item>
        </div>
        
        <br><br>
        <el-form-item v-if="type === 'input'">
            <el-button type="primary" @click="onSubmit">提交</el-button>
        </el-form-item>
    </el-form>
    <el-dialog title="点击行即填充" :visible.sync="showFillDataList" width="80%" v-if="type === 'input'">
        <el-table :data="task.fillData"
                  highlight-current-row
                  @current-change="selectMoreInfoRow"
        >
            <el-table-column property="fent" label="生产企业（证件持有人）名称"></el-table-column>
            <el-table-column property="fadent" label="广告中标识的生产企业（证件持有人）名称"></el-table-column>
            <el-table-column property="fmanuno" label="生产批准文号"></el-table-column>
            <el-table-column property="fadmanuno" label="广告中生产批准文号"></el-table-column>
            <el-table-column property="fapprno" label="广告批准文号"></el-table-column>
            <el-table-column property="fadapprno" label="广告中广告批准文号"></el-table-column>
            <el-table-column property="fentzone" label="生产企业（证件持有人）所在地区"></el-table-column>
        </el-table>
    </el-dialog>
</div>`,
    data: function () {
        return {
            rule: {
                fillegaltypecode: [
                    {required: true, message: '请选择违法程度' , trigger: 'change' }
                ],
                fillegalcontent: [
                    {required: true, message: '请填写违法内容' , trigger: 'change'}
                ],
                fexpressioncodes: [
                    {required: true, message: '请选择违法表现' , trigger: 'change'}
                ],
            },
            loading: false,
            illegalTypeArr,
            illegalArr,
            illegalKeyword: '',
            showMoreInfo: false,
            showFillDataList: false,


        }
    },
    computed: {
        isIllegal: function () {
            return this.task.fillegaltypecode !== '0'
        },
        adType: function () {
            const obj = {
                '1': 'tv',
                '2': 'bc',
                '3': 'paper',
                '13': 'net',
            }
            return obj[this.task.media_class] || null
        },
    },
    watch: {
        illegalKeyword(val) {
            this.$refs.illegalSelecter.filter(val);
        }
    },
    methods: {
        selectMoreInfoRow: function (row) {
            Object.assign(this.task, row)
        },
        filterNode(value, data) {
            if (!value) return true;
            return data.label.indexOf(value) !== -1;
        },
        selectIllegal: function (data, isChecked) {
            if (data.fpcode === ''){
                return false
            }
            this.task.checkedIllegalArr = this.task.checkedIllegalArr || []
            var index = this.task.checkedIllegalArr.indexOf(data);
            if (isChecked && index === -1 && data.fpcode !== '') {
                this.task.checkedIllegalArr.push(data)
            }
            if (!isChecked && index !== -1) {
                this.task.checkedIllegalArr.splice(index, 1)
            }
            this.changeIllegal()
            if (this.task.checkedIllegalArr.length > 0){
                var maxCode = 0
                this.task.checkedIllegalArr.forEach((v) => {
                    if (v.fillegaltype > maxCode){
                        maxCode = v.fillegaltype
                    }
                })
                Vue.set(this.task, 'fillegaltypecode', maxCode)
            }
            this.$forceUpdate()
        },
        deleteIllegal: function (index) {
            if (this.type != 'input'){
                return false
            }
            this.task.checkedIllegalArr.splice(index, 1);
            this.$refs.illegalSelecter.setCheckedNodes(this.task.checkedIllegalArr);
            this.changeIllegal()
        },
        changeIllegal: function () {
            if (this.task.checkedIllegalArr.length === 0){
                Vue.set(this.task, 'fexpressioncodes', '')
            }
            var arr = this.task.checkedIllegalArr.map((v) => {
                return v.fcode;
            });
            var str = arr.join(';');
            Vue.set(this.task, 'fexpressioncodes', str)
        },
        toggleShowMoreInfo: function(){
            if (!this.showMoreInfo){
                this.loading = true
                const typeMap = {
                    paper: 'get_paper_ad_more_field',
                    tv: 'get_tv_ad_more_field',
                    bc: 'get_bc_ad_more_field',
                    net: 'get_net_ad_more_field',
                }
                $.post('/api/ad/' + typeMap[this.adType], {fadname: this.task.fadname}).then((res) => {
                    if  (res.code == 0){
                        this.task.fillData = res.moreFieldList
                        if(this.task.fillData.length !== 0){
                            this.showFillDataList = true
                        }
                    }
                    this.loading = false
                })
            }
            this.showMoreInfo = !this.showMoreInfo
        },
        changeIllegalType: function () {
            if (this.task.media_class == 13 && this.task.fillegaltypecode != 0) {
                this.$nextTick(() => {
                    const editor = new wangEditor(this.$refs.editor)
                    editor.customConfig.onchange = (html) => {
                        if (html === '<p><br></p>'){
                            this.task.fillegalcontent = ''
                        }else{
                            this.task.fillegalcontent = html
                        }
                    }
                    editor.customConfig.uploadImgServer = this.task.fileUp.up_server
                    editor.customConfig.uploadFileName = this.task.fileUp.file
                    editor.customConfig.uploadImgParams = {
                        token: this.task.fileUp.val.token
                    }
                    const vm = this
                    editor.customConfig.uploadImgHooks = {
                        before: function (xhr, editor, files) {
                            vm.$message('图片上传中')
                        },
                        fail: function (xhr, editor, result) {
                            vm.$message('图片上传失败')
                        },
                        error: function (xhr, editor) {
                            vm.$message('图片上传失败')
                        },
                        customInsert: function (insertImg, result, editor) {
                            vm.$message.success('图片上传成功')
                            const url = vm.task.fileUp.bucket_url + result[vm.task.fileUp.key]
                            editor.txt.append(`<a href="${url}" target="_blank" title="点击查看大图"><img src="${url}" alt=""></a>`)
                            // insertImg(vm.task.fileUp.bucket_url + url)
                        }
                    }
                    editor.customConfig.menus = []
                    editor.customConfig.zIndex = 100
                    editor.create()
                    editor.txt.html(this.task.fillegalcontent)
                    editor.$textElem.attr('contenteditable', this.type === 'input')

                })
            }
        },
        onSubmit: function () {
            if (this.adType === 'paper' && !this.task.paperIsSure) {
                this.$message.warning('请确认版面无误后点击报纸上方查看广告按钮')
                return false
            }
            this.$refs['illegalInfo'].validate((res) => {
                if (res){
                    let text = '确认提交?'
                    this.$confirm(text).then(() => {
                        if (this.loading){
                            this.$message.warning('正在提交中, 请不要重复提交')
                            return false
                        }
                        this.loading = true;
                        $.post('submitJudgeTask', {data: this.task}).then((res) => {
                            if (res.code == 0){
                                this.$confirm('提交成功, 是否继续做新任务?', '提交成功', {
                                    confirmButtonText: '继续',
                                    cancelButtonText: '取消',
                                    type: 'success',
                                }).then(() => {
                                    this.$emit('next')
                                }).catch(() => {
                                    this.$emit('back')
                                })
                            }else{
                                this.$message({
                                    message: res.msg,
                                    type: 'error'
                                })
                                this.$emit('error')
                            }
                            this.loading = false
                        })
                    }).catch(() => {})
                }else {
                    this.$alert('有必填字段未填写')
                }
            })
        },
    },
}