const listMixin = {
  data() {
    return {
      page: {
        pageIndex: 1,
        pageSize: 20,
      },
      total: 0,
      tableData: [],
      listData: {},
      tableHeight: '300px',
      loading: false,
    }
  },
  created() {
    this.getList()
  },
  mounted() {
    this.$nextTick(() => {
      let timer = null
      document.querySelector('#app').style.opacity = 1
      this.getTableHeight()
      window.addEventListener('resize', () => {
        clearTimeout(timer)
        timer = setTimeout(() => {
          this.getTableHeight()
        },500)
      })
    })
  },
  methods: {
    getList(page = 1) {
      this.loading = true
      this.page.pageIndex = page
      let data = {
        ...this.searchForm,
        ...this.page
      }
      $fly.post(this.listUrl, data).then(res => {
        this.tableData = res.data
        this.listData = res
        this.total = +res.count || 0
        this.loading = false
      }).catch((e) => {
        this.loading = false
        console.error(e)
      })
    },
    getTableHeight() {
      let formHeight = this.$refs.sform.$el.clientHeight
      this.tableHeight = document.documentElement.clientHeight - formHeight - (this.customHeight || 112) + 'px'
    },
    handleSizeChange(size) {
      this.page.pageSize = size
      this.getList(this.page.pageIndex)
    },
  },
}