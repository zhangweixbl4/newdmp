<?php

namespace Admin\Controller;
use Think\Controller;
use Think\Exception;


class TimeTaskController extends BaseController {

	#列表页
	public function index(){
		$this -> display();
		
	}
	
	
	#数据列表接口
	public function data_list(){
		
		$page = intval(I('page')); #页码
		if ($page <= 0) $page = 1;
		
		$pageSize = intval(I('pageSize')); #每页显示条数
		if ($pageSize <= 0) $pageSize = 5;
		if ($pageSize > 100) $pageSize = 100;
		
		$keyword = I('keyword');
		
		$where = array();
		if($keyword) $where['fname|furl'] = array('like','%'.$keyword.'%');
		
		$data_count = M('time_task')->where($where)->count(); #查询总数量
		$data_list = M('time_task')->where($where)->page($page,$pageSize)->order('fid desc')->select(); #查询列表
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','data_count'=>$data_count,'data_list'=>$data_list/* ,'sql'=>M('time_task')->getLastSql() */));
	}
	
	
	#数据详情接口
	public function data_details(){
		session_write_close();
		$fid = I('fid');
		
		$data_details = M('time_task')->where(array('fid'=>$fid))->find();
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','data_details'=>$data_details));
		
	
	}
	
	
	#创建、修改接口
	public function add_edit(){
		$fid = I('fid');
		$fname = I('fname');
		$furl = I('furl');
		
		$sleep_time = I('sleep_time');
		$on_off = I('on_off');
		
		$a_e_data = array();
		
		$a_e_data['fname'] = $fname ;
		$a_e_data['furl'] = $furl ;
		$a_e_data['sleep_time'] = $sleep_time ;
		$a_e_data['on_off'] = $on_off ;
		
		
		if($fid){ #判断是否为修改，有fid为修改
			$er = M('time_task')->where(array('fid'=>$fid))->save($a_e_data); #执行修改SQL
			if($er){ #判断是否修改成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败'));
			}
		}else{ #无fid为新增
			$a_e_data['create_time'] = date('Y-m-d H:i:s');
			$fid = M('time_task')->add($a_e_data); #执行新增SQL
			if($fid){ #判断是否新增成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功','fid'=>$fid));
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败'));
			}
		}
		


		
		
	}
	
	#删除接口
	public function del(){
		$fid = I('fid');
		
		$taskInfo = M('time_task')->where(array('fid'=>$fid))->find();
		
		if($taskInfo['on_off'] == 1){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'不允许删除开启状态的定时任务'));
		}
		
		
		$rr = M('time_task')->where(array('fid'=>$fid))->delete();
		
		if($rr){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败'));
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}