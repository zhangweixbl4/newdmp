Vue.prototype.$ELEMENT = {
	size: 'mini',
	zIndex: 3000
}
let vm = new Vue({
	el: '#app',
	data: function () {
		return {
			activeTab: '1',
			data1: '0',
			data2: '0'
		}
	},
	created() {
		this.getNum()
	},
	methods: {
		getNum() {
			$.post('getfhtask').then(res => {
				if (res.code === 0) {
					this.data1 = res.data.data1
					this.data2 = res.data.data2
				}
			})
		},
		searchAlias: function (queryString, cb) {
			$.post("/Api/User/searchUserByAlias", {
				keyword: queryString
			}, function (res) {
				cb(res.data);
			});
		},
	}
})