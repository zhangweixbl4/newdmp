<?php
namespace Agp\Controller;
use Think\Controller;

class NpreviewwordController extends BaseController{
	//在类初始化方法中，引入相关类库    
    public function _initialize() {
        header("Content-type:text/html;charset=utf-8");
        vendor('PHPWord.PHPWord');
        vendor('PHPWord.PHPWord.IOFactory');
    }

    /**
	 * 生成文书
	 * by zw
	 */
    function create_document_word($dt_id){
    	ini_set('memory_limit','1024M');
		ini_set('max_execution_time','86400');
		set_time_limit(120);//设置超时时间

		$db_ma = M('document');
		$where_ma['dt_id'] = $dt_id;
		$do_ma = $db_ma->field('dt_name,dt_number,dt_receiveregulatorname,dt_content,dt_sendregulatorname,dt_sendtime')->where($where_ma)->find();
		/* *************************正式开始文档编辑**************************** */
		$PHPWord = new \PHPWord();//引入组件
		$PHPWord->setDefaultFontSize(16);//默认字体大小
		$PHPWord->setDefaultFontName('仿宋_GB2312');//默认字体大小
		
		$sectionStyle = array(
			'orientation' => null,//文档方向
		);//页面样式

		$section = $PHPWord->createSection(
			array(
				'orientation' => null,//文档方向
			)
		);//创建文档

		$section->addText($do_ma['dt_name'],array('color'=>'000000', 'size'=>22, 'bold'=>true, 'name'=>'黑体'),array('align'=>'center','spacing'=>50));//文书名称
		$section->addText($do_ma['dt_number'],array('color'=>'000000', 'size'=>16),array('align'=>'center','spacing'=>50));//文号
		$section->addText($do_ma['dt_receiveregulatorname']."：",array('color'=>'000000', 'size'=>16),array('align'=>'left','spacing'=>50));//收文单位
		$section->addTextBreak(1);
		$section->addText($do_ma['dt_content'],array('color'=>'000000', 'size'=>16),array('align'=>'left','indentFirstLineChars'=>0,'spacing'=>50));//正文内容
		$section->addTextBreak(2);
		$section->addText($do_ma['dt_sendregulatorname'],array('color'=>'000000', 'size'=>16),array('align'=>'left','indentFirstLineChars'=>1400,'spacing'=>50));//发文单位
		$section->addText(date('Y',strtotime($do_ma['dt_sendtime'])).'年'.date('m',strtotime($do_ma['dt_sendtime'])).'月'.date('d',strtotime($do_ma['dt_sendtime'])).'日',array('color'=>'000000', 'size'=>16),array('align'=>'left','indentFirstLineChars'=>1600,'spacing'=>50));//发文时间
		
		// 保存文件
		$objWriter = \PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
        $date = date('Ymdhis');
        $savefile = './Public/Word/'.$date.'.docx';
        $objWriter->save($savefile);

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.docx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

        $db_ma->execute('update document set dt_downurl="'.$ret['url'].'" where dt_id='.$dt_id);
    	$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        
    }

    /**
	 * 福建局、南京局生成文书
	 * by zw
	 */
    function create_document_word2($dt_id){
    	header("Content-type: text/html; charset=utf-8"); 
        session_write_close();
        $do_dt = M('document')->where(['dt_id' => $dt_id])->find();

        $html .= '<!DOCTYPE html> <head> <title></title> </head> <body style="font-size:18px; line-height:24px;"> ';
        $html .= '<table style="border:0;margin:0;border-collapse:collapse;border-spacing:0;" width="100%">';
        $html .= '<tr><td align="center" style="padding:12px"><span style="font-size:28px;">'.$do_dt['dt_name'].'</span></td></tr>';
        $html .= '<tr><td align="center" style="padding:12px"><span style="font-size:22px;">'.$do_dt['dt_number'].'</span></td></tr>';
        $html .= '<tr><td align="left">'.$do_dt['dt_receiveregulatorname'].'：</td></tr>';
        $html .= '<tr><td align="left">'.$do_dt['dt_content'].'</td></tr>';
        $html .= '<tr><td align="right" style="padding-right:19%; padding-top:24px;">'.$do_dt['dt_sendregulatorname'].'</td></tr>';
        $html .= '<tr><td align="right" style="padding-right:20%">'.date('Y',strtotime($do_dt['dt_sendtime'])).'年'.date('m',strtotime($do_dt['dt_sendtime'])).'月'.date('d',strtotime($do_dt['dt_sendtime'])).'日'.'</td></tr>';
        $html .= '</table></body> </html>';
		$date = date('Ymdhis');
        $savefilename = $date.'.doc';
        $savefile = './Public/Word/'.$date.'.doc';

        //将文档保存
        word_start();
        echo $html;
        word_save($savefile);
        ob_flush();//每次执行前刷新缓存
        flush();

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.doc',$savefile,array('Content-Disposition' => 'attachment;filename='.$do_dt['dt_name'].'.doc'));//上传云
        unlink($savefile);//删除文件

        M()->execute('update document set dt_downurl="'.$ret['url'].'" where dt_id='.$dt_id);
    	return $ret['url'];
        
    }

}