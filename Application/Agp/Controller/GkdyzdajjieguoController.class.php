<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 国家局跨地域重点案件线索结果上报
 * by zw
 */

class GkdyzdajjieguoController extends BaseController{
  /**
  * 重点线索线索待处理列表
  * by zw
  */
  public function dclzdanjian_list(){
    Check_QuanXian(['kdyjsanjian']);
    session_write_close();

    $p  = I('page', 1);//当前第几页
    $pp = 20;//每页显示多少记录
    $system_num = getconfig('system_num');//客户编号

    $where_tia['a.fstatus']       = 20;
    $where_tia['a.fdomain_isout'] = 10;
    $where_tia['a.fcustomer'] = $system_num;
    $where_tia['a.fwaitregid']    = session('regulatorpersonInfo.fregulatorpid');
    $where_tia['_string']         = 'locate('.session('regulatorpersonInfo.fregulatorpid').',fparticipate_regids)>0';

    $count = M('tbn_illegal_zdad')
      ->alias('a')
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    
    $do_tia = M('tbn_illegal_zdad')
      ->alias('a')
      ->field('a.fid,a.fad_name,a.fill_type,a.fsend_time,a.fresult_datetime,a.fget_regname,a.fsend_regname,a.fview_status,a.fresult,a.fcreate_time,a.fill_content,a.fresult_endtime')
      ->where($where_tia)
      ->order('a.fid desc')
      ->page($p,$pp)
      ->select();
  
    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
  }

  /**
  * 重点案件线索上报
  * by zw
  */
  public function sbzdanjian(){
    $selfid     = I('selfid');//违法广告ID组
    $attachinfo = I('attachinfo');//上传的文件信息
    $selfresult = I('selfresult');//处理结果
    $attach     = [];//预添加的附件信息
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    if(empty($attachinfo)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请上传证据'));
    }
    if(!empty($selfresult)){
      foreach ($selfid as $value) {
        $where_tia['fid']           = $value;
        $where_tia['fstatus']       = 20;
        $where_tia['fdomain_isout'] = 10;
        $where_tia['fwaitregid']    = session('regulatorpersonInfo.fregulatorpid');
        $where_tia['_string']         = 'locate('.session('regulatorpersonInfo.fregulatorpid').',fparticipate_regids)>0';

        $data_tia['fstatus']          = 30;//已上报
        $data_tia['fview_status']     = 10;//已查看
        $data_tia['fresult']          = $selfresult;//处理结果
        $data_tia['fresult_datetime'] = date('Y-m-d H:i:s');//上报时间
        $data_tia['fresult_unit']     = session('regulatorpersonInfo.regulatorpname');//上报机构
        $data_tia['fresult_user']     = session('regulatorpersonInfo.fname');//上报人
        $data_tia['fwaitregid']       = 0;//等待处理机构id
        $data_tia['fwaituserid']      = 0;//等待处理人id
        $do_tia = M('tbn_illegal_zdad')->where($where_tia)->save($data_tia);
        
        if(!empty($do_tia)){
          M()->execute('update tbn_illegal_zdad set fview_status=10 where fid='.$value);//更新查看状态
          M()->execute('update tbn_zdcase_send set fstatus=10,freceiver="'.session('regulatorpersonInfo.fname').'",frece_time=CURDATE() where fillegal_ad_id='.$value.' and frece_reg_id='.session('regulatorpersonInfo.fregulatorpid').' and fstatus=0');//更新上级派发任务的处理状态

          //上传附件
          $attach_data['fillegal_ad_id']  = $value;//违法广告
          $attach_data['fuploader']       = session('regulatorpersonInfo.fname');//上传人
          $attach_data['fupload_time']    = date('Y-m-d H:i:s');//上传时间
          $attach_data['ftype']           = 10;//附件类型
          foreach ($attachinfo as $key2 => $value2){
            $attach_data['fattach']     = $value2['fattachname'];
            $attach_data['fattach_url'] = $value2['fattachurl'];
            array_push($attach,$attach_data);
          }
        }
      }
      if(!empty($attach)){
        M('tbn_illegal_zdad_attach')->addAll($attach);
        D('Function')->write_log('处理结果上报',1,'上报成功','tbn_illegal_zdad',0,M('tbn_illegal_zdad')->getlastsql());
        $this->ajaxReturn(array('code'=>0,'msg'=>'上报成功'));
      }else{
        D('Function')->write_log('处理结果上报',0,'上报失败','tbn_illegal_zdad',0,M('tbn_illegal_zdad')->getlastsql());
        $this->ajaxReturn(array('code'=>1,'msg'=>'上报失败'));
      }
    }else{
      D('Function')->write_log('处理结果上报',0,'请选择处理结果','tbn_illegal_zdad',0);
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择处理结果'));
    }
  }

  /**
  * 重点线索线索派发
  * by zw
  */
  public function pfzdanjian(){
    $selfid     = I('selfid');//违法广告ID组
    $tregionid  = I('tregionid');//行政区划ID

    $adddata    = [];
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    if(empty($tregionid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择需要派发的机构'));
    }
    $where_tr['fregionid'] = $tregionid;
    $where_tr['_string'] = 'fstate=1 and ftype=20 and fkind=1';
    $do_tr = M('tregulator')->field('fid,fname')->where($where_tr)->find();
    if(empty($do_tr)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'该区域还未建立相应机构'));
    }
    foreach ($selfid as $key => $value) {
      //保存当前待处理机构
      $do_tia = M()->execute('update tbn_illegal_zdad set fwaitregid='.$do_tr['fid'].',fparticipate_regids=concat(fparticipate_regids,",'.$do_tr['fid'].'") where fid='.$value.' and fstatus=20 and fdomain_isout=10 and fwaitregid='.session('regulatorpersonInfo.fregulatorpid').' and locate('.session('regulatorpersonInfo.fregulatorpid').',fparticipate_regids)>0');

      if(!empty($do_tia)){
        M()->execute('update tbn_illegal_zdad set fview_status=0 where fid='.$value);//更新查看状态
        M()->execute('update tbn_zdcase_send set fstatus=20,freceiver="'.session('regulatorpersonInfo.fname').'",frece_time=CURDATE() where fillegal_ad_id='.$value.' and frece_reg_id='.session('regulatorpersonInfo.fregulatorpid').' and fstatus=0');//更新上级派发任务的处理状态

        //将需要添加的派发记录放到数组中
        $data_tcs['fillegal_ad_id'] = $value;
        $data_tcs['frece_reg_id']   = $do_tr['fid'];
        $data_tcs['frece_reg']      = $do_tr['fname'];
        $data_tcs['fsend_reg_id']   = session('regulatorpersonInfo.fregulatorpid');
        $data_tcs['fsend_reg']      = session('regulatorpersonInfo.regulatorpname');
        $data_tcs['fsender']        = session('regulatorpersonInfo.fname');
        $data_tcs['fsend_time']     = date('Y-m-d H:i:s');
        array_push($adddata, $data_tcs);
      }
    }
    if(!empty($adddata)){
      M('tbn_case_send')->addAll($adddata);
      D('Function')->write_log('处理结果上报',1,'派发成功','tbn_zdcase_send',0);
      $this->ajaxReturn(array('code'=>0,'msg'=>'派发成功'));
    }else{
      D('Function')->write_log('处理结果上报',0,'派发失败','tbn_zdcase_send',0);
      $this->ajaxReturn(array('code'=>1,'msg'=>'派发失败'));
    }
  }

/**
  * 重点线索接收信息获取
  * by zw
  */
  public function view_jszdanjian(){
    $system_num = getconfig('system_num');//客户编号
    $fid = I('fid');//线索ID
    $where_tia['_string']     = 'locate('.session('regulatorpersonInfo.fregulatorpid').',fparticipate_regids)>0';
    $where_tia['fid']         = $fid;
    $where_tia['fstatus']     = array('gt',0);
    $where_tia['fdomain_isout'] = 10;
    $do_tia = M('tbn_illegal_zdad')->where($where_tia)->find();//接收信息

    //获取所有附件
    $where_tia2['b.fid']         = $fid;
    $where_tia2['b.fdomain_isout'] = 10;
    $where_tia2['b.fcustomer'] = $system_num;
    $where_tia2['a.fstate'] = 0;

    $do_tiza = M('tbn_illegal_zdad_attach')
      ->alias('a')
      ->field('a.fid,a.fattach,a.fattach_url,a.ftype')
      ->join('tbn_illegal_zdad b on a.fillegal_ad_id=b.fid')
      ->where($where_tia2)
      ->select();

    $do_tia['files']['jbfiles'] = [];
    $do_tia['files']['clfiles'] = [];
    $do_tia['files']['czfiles'] = [];
    foreach ($do_tiza as $key => $value) {
      if($value['ftype'] == 0){
        $do_tia['files']['jbfiles'][] = $value;
      }elseif($value['ftype'] == 10){
        $do_tia['files']['clfiles'][] = $value;
      }elseif($value['ftype'] == 20){
        $do_tia['files']['czfiles'][] = $value;
      }
    }

    if(!empty($do_tia)){
      M()->execute('update tbn_illegal_zdad set fview_status=10 where fid='.$fid);
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_tia));
    }else{
      $this->ajaxReturn(array('code'=>1,'msg'=>'获取失败'));
    }
  }

  /**
  * 重点线索接收信息附件信息获取
  * by zw
  */
  public function attach_zdanjian(){
    $fid    = I('fid');//线索ID
    $types  = I('types');//附件类型，0交办附件，10上报证据

    $where_tiza['fillegal_ad_id'] = $fid;
    $where_tiza['a.ftype'] = $types;
    $where_tiza['a.fstate'] = 0;
    $do_tiza = M('tbn_illegal_zdad_attach')
      ->alias('a')
      ->join('tbn_illegal_zdad b on a.fillegal_ad_id=b.fid')
      ->field('a.fid,a.fattach,a.fattach_url')
      ->where($where_tiza)
      ->select();

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','filedata'=>$do_tiza));
  }

}