<?php
//登录管理控制器
namespace Media\Controller;
use Think\Controller;
class LoginController extends BaseController {

    public function index(){
		$login = I('get.login');
		if($login == 'out_login') session('mediaPersonInfo',null);//如果传入退出参数，则退出
		if(session('mediaPersonInfo.fcode') != ''){
			header("Location:".U('Media/Index/index')); 
			exit;
		}

		$this->display();
	}
	
	
	
	/*登录处理*/
	public function ajax_login(){

		$allow_media_login_error_count = 3;//允许管理员登陆错误的次数
		$fcode = I('fcode');//获取登录用户名
		$fpassword = I('fpassword');//获取登录密码
		$verify = I('verify');//获取验证码
		$where = array();
		$where['fcode|fmobile|fmail'] = $fcode;//登录用户名查询条件
		$where['fpassword'] = md5($fpassword);//登录密码查询条件
		
		if (S($fcode.'_media_login_error_count') > $allow_media_login_error_count){//判断是否需要验证码
			$need_verify = 'true';
			if($verify == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'登录失败,需要验证码','need_verify'=>$need_verify));//返回ajax
			if(!$this->check_verify($verify)){
				S($fcode.'_media_login_error_count', intval(S($fcode.'_media_login_error_count'))+1,3600);//人员登录错误次数+1
				$this->ajaxReturn(array('code'=>-1,'msg'=>'登录失败,验证码错误','need_verify'=>$need_verify));//返回ajax
			} 
		}
		
		
		
		$mediaPersonInfo = M('tperson')->where($where)->find();//查询人员信息
		//var_dump(M('tperson')->getLastSql());
		if($mediaPersonInfo['fstate'] == 1){//判断人员是否正常状态
			session('mediaPersonInfo',$mediaPersonInfo);//人员信息写入session
			S($fcode.'_media_login_error_count',null,1);//重置人员登陆错误次数
			M('admin_login_log')->add(array('login_ip'=>get_client_ip(),'login_time'=>date('Y-m-d H:i:s'),'person_id'=>$mediaPersonInfo['fid']));//记录管理员登陆记录
			M('tperson')->where(array('fid'=>$mediaPersonInfo['fid']))->save(array('flogincount'=>array('exp','flogincount + 1')));//增加登陆次数
			$this->ajaxReturn(array('code'=>0,'msg'=>'登录成功','url'=>U('Admin/Index/index')));//返回ajax
		}else{
			S($fcode.'_media_login_error_count', intval(S($fcode.'_media_login_error_count'))+1,3600);//人员登录错误次数+1
			$this->ajaxReturn(array('code'=>-1,'msg'=>'登录失败,用户名或密码错误','need_verify'=>$need_verify));//返回ajax
		}
		
	}
	
	/*登录验证码*/
	public function verify(){
		
		$Verify = new \Think\Verify(array(
											
											'useCurve'	=>	false,
											'useNoise'	=>	false,
											'length'	=>	4,
									));
		$Verify->entry();
	}
	
	/*验证码验证*/
	function check_verify($code, $id = ''){
		$verify = new \Think\Verify();
		return $verify->check($code, $id);
	}

	

}