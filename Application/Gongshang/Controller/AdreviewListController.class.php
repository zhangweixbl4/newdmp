<?php
namespace Gongshang\Controller;
use Think\Controller;

/**
 * Class AdauditController
 * @package Gongshang\Controller
 * 复核任务
 */

class AdreviewListController extends BaseController
{
	public function index()
	{
		$p = I('p', 1);//当前第几页
		$pp = 20;//每页显示多少记录
//发布媒介、广告名称、广告内容类别、违法类型、涉嫌违法内容、发布次数、指导次数、状态、操作（详情）；

		$fadname = I('fadname');// 广告名称
		$fmediaclass = I('fmediaclass');//媒体类别
		$fcreatetime_s = I('createtime');// 创建时间  开始
		$fcreatetime_e = I('fcreatetime_e');// 创建时间  结束
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID

		if ($fcreatetime_s == '') {
			$fcreatetime_s = '2015-01-01';
		}
		if ($fcreatetime_e == '') {
			$fcreatetime_e = date('Y-m-d');
		}
		$where = array();
		$where['tillegaladflow.fcreatetime'] = array('between', $fcreatetime_s . ',' . $fcreatetime_e . ' 23:59:59'); //时间
		if ($fadname != '') {
			$where['tad.fadname'] = array('like', '%' . $fadname . '%');//广告名称
		}
		if ($fmediaclass != '') {
			$where['tillegalad.fmediaclassid'] = $fmediaclass;//媒体类别
		}
		if (is_array($fillegaltypecode)) {
			$where['tillegalad.fillegaltypecode'] = array('in', $fillegaltypecode);//违法类型代码
		}

		$count = M('tillegaladflow')
			->join('tillegalad on tillegaladflow.fillegaladid=tillegalad.fillegaladid', 'LEFT')//违法类型
//			->join('tmediaclass on tillegalad.fmediaclassid = tmediaclass.fid ', 'LEFT')//媒体列别
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on  tad.fadclasscode=tadclass.fcode ', 'LEFT')//广告内容列别
			->join('tmedia on  tillegalad.fmediaid=tmedia.fid', 'LEFT')//媒介信息
			->join('tillegaltype on tillegalad.fillegaltypecode=tillegaltype.fcode', 'LEFT')//违法类型
			->where($where)
			->where(array('tillegaladflow.fbizname'=>'广告复核','tillegaladflow.fflowname'=>'复核任务','tillegaladflow.fstate'=>array('in', '0,1'),'tillegaladflow.fregulatorid'=>session('regulatorpersonInfo.fregulatorid'),'tillegaladflow.fregulatorname'=>session('regulatorpersonInfo.regulatorname')))
			->count();// 查询满足要求的总记录数

		$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$data = M('tillegaladflow')
			->field('
						tillegaladflow.*,
			            tad.fadname,
			            tmedia.fmedianame,
			            tadclass.ffullname as adclass_fullname,
			            tillegalad.ffirstissuetime,
			            tillegaltype.fillegaltype
									')
			->join('tillegalad on tillegaladflow.fillegaladid=tillegalad.fillegaladid', 'LEFT')//违法类型
//			->join('tmediaclass on tillegalad.fmediaclassid = tmediaclass.fid ', 'LEFT')//媒体列别
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on  tad.fadclasscode=tadclass.fcode ', 'LEFT')//广告内容列别
			->join('tmedia on  tillegalad.fmediaid=tmedia.fid', 'LEFT')//媒介信息
			->join('tillegaltype on tillegalad.fillegaltypecode=tillegaltype.fcode', 'LEFT')//违法类型
			->where($where)
			->where(array('tillegaladflow.fbizname'=>'广告复核','tillegaladflow.fflowname'=>'复核任务','tillegaladflow.fstate'=>array('in', '0,1'),'tillegaladflow.fregulatorid'=>session('regulatorpersonInfo.fregulatorid'),'tillegaladflow.fregulatorname'=>session('regulatorpersonInfo.regulatorname')))
			->limit($Page->firstRow . ',' . $Page->listRows)
			->select();//查询违法广告
//		aa($data,1);
		$data = list_k($data, $p, $pp);//为列表加上序号
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate' => 1))->select();//查询违法类型
		$this->assign('illegaltype', $illegaltype); //违法类型
		$this->assign('data', $data); //列表信息
		$this->assign('page', $Page->show());//分页输出
		$this->display('retask');
	}

	/*线索详情*/
	public function details()
	{
		$where = array();
		if(I('fillegaladid')){
			$fillegaladid = I('fillegaladid');//线索ID
		}else{
			$this->error('缺失fillegaladid!');
		}
		if(I('fillegaladflowid')){
			$fillegaladflowid = I('fillegaladflowid');//线索ID
		}
		$where['tillegalad.fillegaladid'] = $fillegaladid;
		$data = M('tillegalad')
			->field('
						tillegalad.fillegaladid,
						tillegalad.fmediaclassid,
						tillegalad.fadid,
			            tad.fadname,
			            tadclass.ffullname as adclass_fullname,
			            tillegalad.fissuetimes,
			            tillegalad.fdisposetimes,
			            tillegalad.ffirstissuetime,
			            tillegalad.flastissuetime,

			            tillegaltype.fillegaltype,
			            tillegalad.fillegalcontent,
			            tillegalad.fexpressions,
			            tillegalad.fconfirmations,
			            tillegalad.fsampleid,
			            tmediaowner.fname as mediaowner_name

									')//id，<<媒介类别>>,广告id. 广告名称，广告内容列别，发布次数。指导次数。首次发布日期，末次发布日期
			// 违法类型：违法内容：违法表现：认定依据,样本id：
			->join('tmediaclass on tmediaclass.fid = tillegalad.fmediaclassid', 'LEFT')//媒体列别
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on tadclass.fcode = tad.fadclasscode', 'LEFT')//广告内容列别
			->join('tmedia on tmedia.fid = tillegalad.fmediaid', 'LEFT')//媒介信息
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid', 'LEFT')
			->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode', 'LEFT')//违法类型
			->where($where)
			->find();//查询广告信息
		$data['fillegaladflowid']= $fillegaladflowid;
		if ($data['fmediaclassid']) {
			switch ($data['fmediaclassid']) {
				case 'bc':
					$adclueInfo = M('tbcsample')->field('fid,favifilename')->where(array('fadid' => $data['fadid']))->find();
					break;
				case 'tv':
					$adclueInfo = M('ttvsample')->field('fid,favifilename')->where(array('fadid' => $data['fadid']))->find();
					break;
				case 'paper':
					$adclueInfo = M('tpapersample')->field('fpapersampleid,fjpgfilename')->where(array('fadid' => $data['fadid']))->find();
					break;
			}
		}
		//附件信息
		$attach = M('tillegaladattach')->where(array('fillegaladid' => $fillegaladid))->select();
		$attach_count = count($attach);
		//流转信息
		$flow = M('tillegaladflow')->where(array('fillegaladid' =>$fillegaladid))->select();
		//违法表现数据列表
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表

		$this->assign('IllegalList',$IllegalList);//违法表现数据列表
		$this->assign('data', $data); //违法信息
		$this->assign('adclueInfo', $adclueInfo); //视频信息
		$this->assign('attach',$attach);//附件
		$this->assign('attach_count',$attach_count);//附件数量
		$this->assign('flow', $flow); //流转信息
		$this->display('retaskDetails');
	}

	/*指导反馈*/
	public function results()
	{
		if(I('fillegaladid')){
			$fillegaladid = I('fillegaladid');
		}else{
		    $this->ajaxReturn(array('code'=>-1,'msg'=>'fillegaladid不存在！'));
		}
		if(I('fillegaladflowid')){
			$fillegaladflowid = I('fillegaladflowid');//线索流程id
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'fillegaladflowid不存在！'));
		}
		if(I('reg_type')){
			$reg_type = I('reg_type');//处理结果
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'请填写处理结果！'));
		}
		//是否已经操作过
		$is_dj = M('tillegaladflow')
			->where(array('fillegaladflowid' => $fillegaladflowid,'fstate'=>2))
			->find();//查询违法广告信息
		if($is_dj){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该广告已被操作过，请刷新~！'));
		}
		switch ($reg_type) {
			case 1://重新判定
				if(I('fcode')){
					$code = I('fcode');//处理结果
				}else{
					$this->ajaxReturn(array('code'=>-1,'msg'=>'fcode不存在！'));
				}
				$tillegal = M('tillegal')->where(array('fcode' =>array('in',$code)))->select();//对应违法表现信息

				$fcode = implode(";",array_column($tillegal, 'fcode'));//复核违法表现代码
				$fexpression = implode(";",array_column($tillegal, 'fexpression'));//复核违法表现
				$fconfirmation = implode(";",array_column($tillegal, 'fconfirmation'));//复核认定依据
				$fpunishment =implode(";",array_column($tillegal, 'fpunishment')); //复核处罚依据
				$fpunishmenttype= implode(";",array_column($tillegal, 'fpunishment'));//处罚种类及幅度
				$fillegaltype=array_column($tillegal, 'fillegaltype'); //默认违法类型
				$fillegalcontent=I('fillegalcontent'); // 复核违法内容

				$tadcheck_model=M("tadcheck");
				$tadcheck_model->startTrans();//开启事务
				$tillegaladflow_model=M("tillegaladflow");
				$tillegaladflow_model->startTrans();//开启事务
				$tillegalad_model=M("tillegalad");
				$tillegalad_model->startTrans();//开启事务

				/*更改复核表*/
				$tadcheck_data['fcheck'] = session('regulatorpersonInfo.fname');//复核人
				$tadcheck_data['fchecktime'] = date('Y-m-d H:i:s');//复核时间
				$tadcheck_data['fcheckresult'] = '重新判定';//todo 复核结果
				$tadcheck_data['fcheckreason'] = I('fcheckreason');//复核理由
				$tadcheck_data['fillegaltypecode'] = max($fillegaltype);//复核违法类型 暂时取最大的
				$tadcheck_data['fillegalcontent'] = $fillegalcontent;//复核违法内容
				$tadcheck_data['fexpressioncodes'] =$fcode;//复核违法表现代码
				$tadcheck_data['fexpressions'] = $fexpression;//复核违法表现
				$tadcheck_data['fconfirmations'] = $fconfirmation;//复核认定依据
				$tadcheck_data['fpunishments'] = $fpunishment;//复核处罚依据
				$tadcheck_data['fpunishmenttypes'] = $fpunishmenttype;//复核处罚种类及幅度
				$tadcheck_data['fstate'] = 2;//状态0-未复核1-正复核2-已复核
				$tadcheck_res = $tadcheck_model->where(array('fillegaladid' => $fillegaladid))->save($tadcheck_data);
				/*更改流程表*/
				$save_list['fregulatorpersonid'] = session('regulatorpersonInfo.fid');//处理人ID
				$save_list['fregulatorpersonname'] = session('regulatorpersonInfo.fname');//处理人
				$save_list['ffinishtime'] = date('Y-m-d H:i:s');//完成时间
				$save_list['fstate'] = 2;
				$save_res = M("tillegaladflow")->where(array('fillegaladflowid' => $fillegaladflowid))->save($save_list);

				/*更改违法广告表*/
				$tillegalad_data['fillegaltypecode'] = max($fillegaltype);//复核违法类型 暂时取最大的
				$tillegalad_data['fillegalcontent'] = $fillegalcontent;//复核违法内容
				$tillegalad_data['fexpressioncodes'] =$fcode;//复核违法表现代码
				$tillegalad_data['fexpressions'] = $fexpression;//复核违法表现
				$tillegalad_data['fconfirmations'] = $fconfirmation;//复核认定依据
				$tillegalad_data['fpunishments'] = $fpunishment;//复核处罚依据
				$tillegalad_data['fpunishmenttypes'] = $fpunishmenttype;//复核处罚种类及幅度
				$tillegalad_data['fmodifier'] = session('regulatorpersonInfo.fname');//修改人
				$tillegalad_data['fcreatetime'] = date('Y-m-d H:i:s');//修改时间
				$tillegalad_data['fdisposestyle'] = 0;//0-待定
				$tillegalad_data['fstate'] = 0;//0-待处理
				$tillegalad_res = M("tillegalad")->where(array('fillegaladid' => $fillegaladid))->save($tillegalad_data);
				if($tadcheck_res &&$save_res && $tillegalad_res){
					$tadcheck_model->commit();//成功则提交
					$tillegaladflow_model->commit();//成功则提交
					$tillegalad_model->commit();//成功则提交
					$this->ajaxReturn(array('code'=>0,'msg'=>'提交复核成功！'));
				}else{
					$tadcheck_model->rollback();
					$tillegaladflow_model->rollback();
					$tillegalad_model->rollback();
					$this->ajaxReturn(array('code'=>-1,'msg'=>'提交复核失败！'));
				}
				break;

			case 2://原判退回
				$tadcheck_model=M("tadcheck");
				$tadcheck_model->startTrans();//开启事务
				$tillegaladflow_model=M("tillegaladflow");
				$tillegaladflow_model->startTrans();//开启事务
				$tillegalad_model=M("tillegalad");
				$tillegalad_model->startTrans();//开启事务

				/*1，更改复核表*/
				$list=$tadcheck_model->where(array('fillegaladid' => $fillegaladid))->find();
				$tadcheck_data['fcheck'] = session('regulatorpersonInfo.fname');//复核人
				$tadcheck_data['fchecktime'] = date('Y-m-d H:i:s');//复核时间
				$tadcheck_data['fcheckresult'] = '原判退回';//todo 复核结果
				$tadcheck_data['fcheckreason'] = I('fcheckreason');//复核理由
				$tadcheck_data['fillegaltypecode'] =$list['fillegaltypecode1'];//复核违法类型 暂时取最大的
				$tadcheck_data['fillegalcontent'] = $list['fillegalcontent1'];//复核违法内容
				$tadcheck_data['fexpressioncodes'] =$list['fexpressioncodes1'];//复核违法表现代码
				$tadcheck_data['fexpressions'] = $list['fexpressions1'];//复核违法表现
				$tadcheck_data['fconfirmations'] = $list['fconfirmations1'];//复核认定依据
				$tadcheck_data['fpunishments'] = $list['fpunishments1'];//复核处罚依据
				$tadcheck_data['fpunishmenttypes'] = $list['fpunishmenttypes1'];//复核处罚种类及幅度
				$tadcheck_data['fstate'] = 2;//状态0-未复核1-正复核2-已复核
				$tadcheck_res = $tadcheck_model->where(array('fillegaladid' => $fillegaladid))->save($tadcheck_data);

				/*2,更改流程表*/
				$save_list['fregulatorpersonid'] = session('regulatorpersonInfo.fid');//处理人ID
				$save_list['fregulatorpersonname'] = session('regulatorpersonInfo.fname');//处理人
				$save_list['ffinishtime'] = date('Y-m-d H:i:s');//完成时间
				$save_list['fstate'] = 2;
				$save_res = M("tillegaladflow")->where(array('fillegaladflowid' => $fillegaladflowid))->save($save_list);
				/*3,更改违法广告表*/
				$tillegalad_data['fmodifier'] = session('regulatorpersonInfo.fname');//修改人
				$tillegalad_data['fcreatetime'] = date('Y-m-d H:i:s');//修改时间
				$tillegalad_data['fdisposestyle'] = 0;//0-待定
				$tillegalad_data['fstate'] = 0;//0-待处理
				$tillegalad_res = M("tillegalad")->where(array('fillegaladid' => $fillegaladid))->save($tillegalad_data);
				if($tadcheck_res &&$save_res && $tillegalad_res){
					$tadcheck_model->commit();//成功则提交
					$tillegaladflow_model->commit();//成功则提交
					$tillegalad_model->commit();//成功则提交
					$this->ajaxReturn(array('code'=>0,'msg'=>'提交复核成功！'));
				}else{
					$tadcheck_model->rollback();
					$tillegaladflow_model->rollback();
					$tillegalad_model->rollback();
					$this->ajaxReturn(array('code'=>-1,'msg'=>'提交复核失败！'));
				}
				break;

			case 3://不违法
				$tadcheck_model=M("tadcheck");
				$tadcheck_model->startTrans();//开启事务
				$tillegaladflow_model=M("tillegaladflow");
				$tillegaladflow_model->startTrans();//开启事务
				$tillegalad_model=M("tillegalad");
				$tillegalad_model->startTrans();//开启事务

				/*1，更改复核表*/
				$tadcheck_data['fcheck'] = session('regulatorpersonInfo.fname');//复核人
				$tadcheck_data['fcheckresult'] = '不违法';//todo 复核结果
				$tadcheck_data['fchecktime'] = date('Y-m-d H:i:s');//复核时间
				$tadcheck_data['fcheckreason'] = I('fcheckreason');//复核理由
				$tadcheck_data['fstate'] = 2;//状态0-未复核1-正复核2-已复核
				$tadcheck_res = $tadcheck_model->where(array('fillegaladid' => $fillegaladid))->save($tadcheck_data);

				/*2,更改流程表*/
				$save_list['fregulatorpersonid'] = session('regulatorpersonInfo.fid');//处理人ID
				$save_list['fregulatorpersonname'] = session('regulatorpersonInfo.fname');//处理人
				$save_list['ffinishtime'] = date('Y-m-d H:i:s');//完成时间
				$save_list['fstate'] = 2;
				$save_res = M("tillegaladflow")->where(array('fillegaladflowid' => $fillegaladflowid))->save($save_list);
				/*3,更改违法广告表*/
				$tillegalad_data['fmodifier'] = session('regulatorpersonInfo.fname');//修改人
				$tillegalad_data['fcreatetime'] = date('Y-m-d H:i:s');//修改时间
				$tillegalad_data['fdisposestyle'] = 10;//10，其他
				$tillegalad_data['fdisposedescribe'] = '广告复核后合法';//10，其他
				$tillegalad_data['fstate'] = 2;
				$tillegalad_res = M("tillegalad")->where(array('fillegaladid' => $fillegaladid))->save($tillegalad_data);
				if($tadcheck_res &&$save_res && $tillegalad_res){
					$tadcheck_model->commit();//成功则提交
					$tillegaladflow_model->commit();//成功则提交
					$tillegalad_model->commit();//成功则提交
					$this->ajaxReturn(array('code'=>0,'msg'=>'提交复核成功！'));
				}else{
					$tadcheck_model->rollback();
					$tillegaladflow_model->rollback();
					$tillegalad_model->rollback();
					$this->ajaxReturn(array('code'=>-1,'msg'=>'提交复核失败！'));
				}
				break;
			case 4://移交给上级单位
				$tillegaladflow_model=M("tillegaladflow");
				$tillegaladflow_model->startTrans();//开启事务

				/*1,更改流程表*/
				$save_list['fregulatorpersonid'] = session('regulatorpersonInfo.fid');//处理人ID
				$save_list['fregulatorpersonname'] = session('regulatorpersonInfo.fname');//处理人
				$save_list['ffinishtime'] = date('Y-m-d H:i:s');//完成时间
				$save_list['fstate'] = 2;
				$save_res = M("tillegaladflow")->where(array('fillegaladflowid' => $fillegaladflowid))->save($save_list);
				//2,新增流程表
				$data['fillegaladid'] = $fillegaladid;
				$data['fcreateinfo'] = I('fcheckreason'); //暂时存这里
				$data['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');
				$data['fcreateregualtorname'] =  session('regulatorpersonInfo.regulatorname');
				$data['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
				$data['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
				$data['fcreatetime'] = date('Y-m-d H:i:s');
				$data['fbizname'] = '广告复核';
				$data['fflowname'] = '复核任务';
				$data['fstate'] = 1;
				$tregulator= M("tregulator")->where(array('fid' =>session('regulatorpersonInfo.fregulatorid')))->find();
				if($tregulator['fpid']!= 0){
					$data['fregulatorid'] = $tregulator['fpid'];
					$up_tregulator= M("tregulator")->where(array('fid' =>$tregulator['fpid']))->find();
					$data['fregulatorname'] =  $up_tregulator['fname'];
					$reg =  M("tillegaladflow")->add($data);
					if($reg){
						$tillegaladflow_model->commit();//成功则提交
						$this->ajaxReturn(array('code'=>0,'msg'=>'移交成功！！'));
					}else{
						$tillegaladflow_model->rollback();//不成功，则回滚
						$this->ajaxReturn(array('code'=>-1,'msg'=>'申请复核失败！'));
					}
				}else{
					$tillegaladflow_model->rollback();//不成功，则回滚
					$this->ajaxReturn(array('code'=>-1,'msg'=>'您已没有上级！！'));
				}
				break;
		}
	}
}