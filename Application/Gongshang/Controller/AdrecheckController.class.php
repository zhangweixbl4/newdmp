<?php
namespace Gongshang\Controller;
use Think\Controller;

class AdrecheckController extends BaseController
{

	public function index(){

		if(I('media_class') == '' ) $_GET['media_class'] = '01';
		$media_class = I('media_class');//媒体类型，01电视，02广播，03报纸
		
		if($media_class == '01'){
			$this->tvList();
		}elseif($media_class == '02'){
			$this->bcList();
		}elseif($media_class == '03'){
			$this->paperList();
		}else{
			$this->tvList();
		}

	}

	public function tvList(){

		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$fadname = I('fadname');//广告名称
		$adclass_code = I('adclass_code');//类别
		$fmediaid = I('fmediaid');//媒体ID
		$fstarttime_s = I('fstarttime_s');// 建样时间
		if($fstarttime_s == '') $fstarttime_s = '2015-01-01';
		$fstarttime_e = I('fstarttime_e');// 建样时间
		if($fstarttime_e == '') $fstarttime_e = date('Y-m-d');
		$checkresult = I('checkresult');//状态
		if(in_array('-1',$checkresult)) $finputstate[] = 2;
		if(in_array('0',$checkresult)) $finputstate[] = 5;
		if(in_array('1',$checkresult)) $finputstate[] = 4;

		$region_id = session('regulatorpersonInfo.regionid');
		if($region_id == 100000 || $region_id == 0 || $region_id == '') $region_id = 0;//如果传值等于100000，相当于查全国
		$region_id_rtrim = rtrim($region_id,'00');//地区ID去掉末尾的0
		$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位

		//where条件
		$where['left(tmediaowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//默认地区条件
		$where['sample.finputstate'] = array('in',array(2,4,5));
		$where['sample.finspectstate'] = 2;//已初审状态
		if($fadname != ''){
			$where['tad.fadname'] = array('like','%'.trim($fadname,' ').'%');//媒体ID
		}
		if($adclass_code != ''){
			$adclass_code_strlen = strlen($adclass_code);//获取code长度
			$where['left(tad.fadclasscode,'.$adclass_code_strlen.')'] = $adclass_code;//广告分类搜索条件	
		}
		if($fmediaid != ''){
			$where['sample.fmediaid'] = $fmediaid;//媒体ID
		}
		if($fstarttime_s != '' || $fstarttime_e != ''){
			$where['sample.fissuedate'] = array('between',$fstarttime_s.','.$fstarttime_e.' 23:59:59');
		}
		if($finputstate){
			$where['sample.finputstate'] = array('in',$finputstate);
		}
		$count = M('ttvsample')
						->alias('sample')
						->join('tad on tad.fadid = sample.fadid')
						->join('tadclass on tadclass.fcode = tad.fadclasscode')
						->join('tmedia on tmedia.fid = sample.fmediaid')
						->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
						->where($where)->count();
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$samList = M('ttvsample')
						->alias('sample')
						->field('
								sample.fid,
								tad.fadname,
								sample.fversion,
								sample.fissuedate,
								tadclass.ffullname as adclass_fullname,
								tillegaltype.fillegaltype,
								tmedia.fmedianame,
								sample.finputstate,
								sample.finspectstate,
								sample.finputuser,
								sample.finspectuser								
								')
						->join('tad on tad.fadid = sample.fadid')
						->join('tadclass on tadclass.fcode = tad.fadclasscode')
						->join('tmedia on tmedia.fid = sample.fmediaid')
						->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
						->where($where)->order('sample.finputstate,sample.fissuedate desc')
						->limit($Page->firstRow.','.$Page->listRows)->select();
		// var_dump($tv);
		$samList = list_k($samList,$p,$pp);//为列表加上序号
		$this->assign('samList',$samList);
		$this->assign('page',$Page->show());//分页输出

		$this->display();
	}

	public function tvsample_edit(){

		$fid = I('fid');
		$tvsampleDetails = M('ttvsample')
									->field('ttvsample.*,tad.fadname,tad.fbrand,tad.fadclasscode,tad.fadowner,tadclass.ffullname,tadowner.fname as adowner_name,tillegaltype.fillegaltype,tmedia.fmedianame')
									->join('tad on tad.fadid = ttvsample.fadid')
									->join('tadclass on tadclass.fcode = tad.fadclasscode')
									->join('tmedia on tmedia.fid = ttvsample.fmediaid')
									->join('tadowner on tadowner.fid = tad.fadowner')
									->join('tillegaltype on tillegaltype.fcode = ttvsample.fillegaltypecode')
									->where(array('ttvsample.fid'=>$fid))
									->find();//查询样本详情
		$tvsampleillegal = M('ttvsampleillegal')
									->field('fillegalcode,fexpression')
									->where(array('fsampleid'=>$fid))->select();
		$this->assign('tvsampleDetails',$tvsampleDetails);
		$this->assign('tvsampleillegal',$tvsampleillegal);
		$illegaltypeList = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
		$this->assign('illegaltypeList',$illegaltypeList);//违法类型数据列表
		$this->assign('IllegalList',$IllegalList);//违法表现数据列表
		$this->display();

	}

	public function ajax_handle_tvsample(){

		$fid = I('fid');//样本ID
		$checkresult = I('checkresult');//复审结果

		$finputstate = M('ttvsample')->where(array('fid'=>$fid))->getField('finputstate');
		if($finputstate == 4 || $finputstate == 5){
			$this->ajaxReturn(array('code'=>200,'msg'=>'已被复审'));
		}

		$date = date('Y-m-d H:i:s');
		$data = array();
		//不通过
		if($checkresult == '0'){
			$data['finputstate'] = 5;
			$data['freason'] = I('reason');
			$data['frechecker'] = session('regulatorpersonInfo.fname');
			$data['frechecktime'] = $date;
		}
		//通过
		if($checkresult == '1'){
			$data['finputstate'] = 4;
			$data['frechecker'] = session('regulatorpersonInfo.fname');
			$data['frechecktime'] = $date;
		}
		M('ttvsample')->where(array('fid'=>$fid))->save($data);

		$this->ajaxReturn(array('code'=>0,'msg'=>'复审完成'));

	}

	public function bcList(){
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$fadname = I('fadname');//广告名称
		$adclass_code = I('adclass_code');//类别
		$fmediaid = I('fmediaid');//媒体ID
		$fstarttime_s = I('fstarttime_s');// 建样时间
		if($fstarttime_s == '') $fstarttime_s = '2015-01-01';
		$fstarttime_e = I('fstarttime_e');// 建样时间
		if($fstarttime_e == '') $fstarttime_e = date('Y-m-d');
		$checkresult = I('checkresult');//状态
		if(in_array('-1',$checkresult)) $finputstate[] = 2;
		if(in_array('0',$checkresult)) $finputstate[] = 5;
		if(in_array('1',$checkresult)) $finputstate[] = 4;

		$region_id = session('regulatorpersonInfo.regionid');
		if($region_id == 100000 || $region_id == 0 || $region_id == '') $region_id = 0;//如果传值等于100000，相当于查全国
		$region_id_rtrim = rtrim($region_id,'00');//地区ID去掉末尾的0
		$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位

		//where条件
		$where['left(tmediaowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//默认地区条件
		$where['sample.finputstate'] = array('in',array(2,4,5));
		$where['sample.finspectstate'] = 2;//已初审状态
		if($fadname != ''){
			$where['tad.fadname'] = array('like','%'.trim($fadname,' ').'%');//媒体ID
		}
		if($adclass_code != ''){
			$adclass_code_strlen = strlen($adclass_code);//获取code长度
			$where['left(tad.fadclasscode,'.$adclass_code_strlen.')'] = $adclass_code;//广告分类搜索条件	
		}
		if($fmediaid != ''){
			$where['sample.fmediaid'] = $fmediaid;//媒体ID
		}
		if($fstarttime_s != '' || $fstarttime_e != ''){
			$where['sample.fissuedate'] = array('between',$fstarttime_s.','.$fstarttime_e.' 23:59:59');
		}
		if($finputstate){
			$where['sample.finputstate'] = array('in',$finputstate);
		}

		$count = M('tbcsample')
						->alias('sample')
						->join('tad on tad.fadid = sample.fadid')
						->join('tadclass on tadclass.fcode = tad.fadclasscode')
						->join('tmedia on tmedia.fid = sample.fmediaid')
						->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
						->where($where)->count();
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$samList = M('tbcsample')
						->alias('sample')
						->field('
								sample.fid,
								tad.fadname,
								sample.fversion,
								sample.fissuedate,
								tadclass.ffullname as adclass_fullname,
								tillegaltype.fillegaltype,
								tmedia.fmedianame,
								sample.finputstate,
								sample.finspectstate,
								sample.finputuser,
								sample.finspectuser								
								')
						->join('tad on tad.fadid = sample.fadid')
						->join('tadclass on tadclass.fcode = tad.fadclasscode')
						->join('tmedia on tmedia.fid = sample.fmediaid')
						->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
						->where($where)->order('sample.finputstate,sample.fissuedate desc')
						->limit($Page->firstRow.','.$Page->listRows)->select();
		// var_dump($tv);
		$samList = list_k($samList,$p,$pp);//为列表加上序号
		$this->assign('samList',$samList);
		$this->assign('page',$Page->show());//分页输出

		$this->display();
	}

	public function bcsample_edit(){

		$fid = I('fid');
		$bcsampleDetails = M('tbcsample')
										->field('tbcsample.*,tad.fadname,tad.fbrand,tad.fadclasscode,tad.fadowner,tadclass.ffullname,tadowner.fname as adowner_name,tillegaltype.fillegaltype,tmedia.fmedianame')
										->join('tad on tad.fadid = tbcsample.fadid')
										->join('tadclass on tadclass.fcode = tad.fadclasscode')
										->join('tmedia on tmedia.fid = tbcsample.fmediaid')
										->join('tadowner on tadowner.fid = tad.fadowner')
										->join('tillegaltype on tillegaltype.fcode = tbcsample.fillegaltypecode')
										->where(array('tbcsample.fid'=>$fid))
										->find();//查询样本详情
		$bcsampleillegal = M('tbcsampleillegal')
												->field('fillegalcode,fexpression')
												->where(array('fsampleid'=>$fid))->select();
		// var_dump($bcsampleDetails);
		$this->assign('bcsampleDetails',$bcsampleDetails);
		$this->assign('bcsampleillegal',$bcsampleillegal);
		$illegaltypeList = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
		$this->assign('illegaltypeList',$illegaltypeList);//违法类型数据列表
		$this->assign('IllegalList',$IllegalList);//违法表现数据列表
		$this->display();

	}

	public function ajax_handle_bcsample(){

		$fid = I('fid');//样本ID
		$checkresult = I('checkresult');//复审结果

		$finputstate = M('tbcsample')->where(array('fid'=>$fid))->getField('finputstate');
		if($finputstate == 4 || $finputstate == 5){
			$this->ajaxReturn(array('code'=>200,'msg'=>'已被复审'));
		}

		$date = date('Y-m-d H:i:s');
		$data = array();
		//不通过
		if($checkresult == '0'){
			$data['finputstate'] = 5;
			$data['freason'] = I('reason');
			$data['frechecker'] = session('regulatorpersonInfo.fname');
			$data['frechecktime'] = $date;
		}
		//通过
		if($checkresult == '1'){
			$data['finputstate'] = 4;
			$data['frechecker'] = session('regulatorpersonInfo.fname');
			$data['frechecktime'] = $date;
		}
		M('tbcsample')->where(array('fid'=>$fid))->save($data);

		$this->ajaxReturn(array('code'=>0,'msg'=>'复审完成'));

	}

	public function paperList(){
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$fadname = I('fadname');//广告名称
		$adclass_code = I('adclass_code');//类别
		$fmediaid = I('fmediaid');//媒体ID
		$fstarttime_s = I('fstarttime_s');// 建样时间
		if($fstarttime_s == '') $fstarttime_s = '2015-01-01';
		$fstarttime_e = I('fstarttime_e');// 建样时间
		if($fstarttime_e == '') $fstarttime_e = date('Y-m-d');
		$checkresult = I('checkresult');//状态
		if(in_array('-1',$checkresult)) $finputstate[] = 2;
		if(in_array('0',$checkresult)) $finputstate[] = 5;
		if(in_array('1',$checkresult)) $finputstate[] = 4;

		$region_id = session('regulatorpersonInfo.regionid');
		if($region_id == 100000 || $region_id == 0 || $region_id == '') $region_id = 0;//如果传值等于100000，相当于查全国
		$region_id_rtrim = rtrim($region_id,'00');//地区ID去掉末尾的0
		$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位

		//where条件
		$where['left(tmediaowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//默认地区条件
		$where['sample.finputstate'] = array('in',array(2,4,5));
		$where['sample.finspectstate'] = 2;//已初审状态
		if($fadname != ''){
			$where['tad.fadname'] = array('like','%'.trim($fadname,' ').'%');//媒体ID
		}
		if($adclass_code != ''){
			$adclass_code_strlen = strlen($adclass_code);//获取code长度
			$where['left(tad.fadclasscode,'.$adclass_code_strlen.')'] = $adclass_code;//广告分类搜索条件	
		}
		if($fmediaid != ''){
			$where['sample.fmediaid'] = $fmediaid;//媒体ID
		}
		if($fstarttime_s != '' || $fstarttime_e != ''){
			$where['sample.fissuedate'] = array('between',$fstarttime_s.','.$fstarttime_e.' 23:59:59');
		}
		if($finputstate){
			$where['sample.finputstate'] = array('in',$finputstate);
		}

		$count = M('tpapersample')
						->alias('sample')
						->join('tad on tad.fadid = sample.fadid')
						->join('tadclass on tadclass.fcode = tad.fadclasscode')
						->join('tmedia on tmedia.fid = sample.fmediaid')
						->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
						->where($where)->count();
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$samList = M('tpapersample')
						->alias('sample')
						->field('
								sample.fpapersampleid as fid,
								tad.fadname,
								sample.fversion,
								sample.fissuedate,
								tadclass.ffullname as adclass_fullname,
								tillegaltype.fillegaltype,
								tmedia.fmedianame,
								sample.finputstate,
								sample.finspectstate,
								sample.finputuser,
								sample.finspectuser							
								')
						->join('tad on tad.fadid = sample.fadid')
						->join('tadclass on tadclass.fcode = tad.fadclasscode')
						->join('tmedia on tmedia.fid = sample.fmediaid')
						->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
						->where($where)->order('sample.finputstate,sample.fissuedate desc')
						->limit($Page->firstRow.','.$Page->listRows)->select();
		// var_dump($tv);
		$samList = list_k($samList,$p,$pp);//为列表加上序号
		$this->assign('samList',$samList);
		$this->assign('page',$Page->show());//分页输出

		$this->display();
	}

	public function papersample_edit(){
		
		$fid = I('fid');
		$papersampleDetails = M('tpapersample')
										->field('tpapersample.*,tad.fadname,tad.fbrand,tad.fadclasscode,tad.fadowner,tadclass.ffullname,tadowner.fname as adowner_name,tillegaltype.fillegaltype,tmedia.fmedianame')
										->join('tad on tad.fadid = tpapersample.fadid')
										->join('tadclass on tadclass.fcode = tad.fadclasscode')
										->join('tmedia on tmedia.fid = tpapersample.fmediaid')
										->join('tadowner on tadowner.fid = tad.fadowner')
										->join('tillegaltype on tillegaltype.fcode = tpapersample.fillegaltypecode')
										->where(array('tpapersample.fpapersampleid'=>$fid))
										->find();//查询样本详情
		$papersampleillegal = M('tpapersampleillegal')
												->field('fillegalcode,fexpression')
												->where(array('fsampleid'=>$fid))->select();
		// var_dump($papersampleDetails);
		$this->assign('papersampleDetails',$papersampleDetails);
		$this->assign('papersampleillegal',$papersampleillegal);
		$illegaltypeList = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
		$this->assign('illegaltypeList',$illegaltypeList);//违法类型数据列表
		$this->assign('IllegalList',$IllegalList);//违法表现数据列表
		$this->display();

	}

	public function ajax_handle_papersample(){

		$fid = I('fid');//样本ID
		$checkresult = I('checkresult');//复审结果

		$finputstate = M('tpapersample')->where(array('fpapersampleid'=>$fid))->getField('finputstate');
		if($finputstate == 4 || $finputstate == 5){
			$this->ajaxReturn(array('code'=>200,'msg'=>'已被复审'));
		}

		$date = date('Y-m-d H:i:s');
		$data = array();
		//不通过
		if($checkresult == '0'){
			$data['finputstate'] = 5;
			$data['freason'] = I('reason');
			$data['frechecker'] = session('regulatorpersonInfo.fname');
			$data['frechecktime'] = $date;
		}
		//通过
		if($checkresult == '1'){
			$data['finputstate'] = 4;
			$data['frechecker'] = session('regulatorpersonInfo.fname');
			$data['frechecktime'] = $date;
		}

		$a = M('tpapersample')->where(array('fpapersampleid'=>$fid))->save($data);

		$this->ajaxReturn(array('code'=>0,'msg'=>'复审完成'));

	}

}