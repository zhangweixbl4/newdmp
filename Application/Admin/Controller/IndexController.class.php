<?php
namespace Admin\Controller;
use Think\Controller;
class IndexController extends BaseController {
	

    public function index(){

		if(session('personInfo.fcode') == ''){
			cookie('admin_index_page',I('index_page'),60);
			header("Location:".U('Admin/Login/index'));
			exit;
		}
		header("COMPUTERNAME:".$_SERVER["COMPUTERNAME"]);
		header("SERVER_ADDR:".$_SERVER["SERVER_ADDR"]);
		
		$myInfo = M('tperson')->cache(true,600)->where(array('fid'=>session('personInfo.fid')))->find();
		$this->assign('myInfo',$myInfo);
		$this->assign('menu_list',A('Admin/Menu','Model')->menu_list());//管理菜单
		$this->display();
	}
	
	public function home(){
		header('location:http://serv1.report.adlife.com.cn/decision/view/form?viewlet=zgq%2Fywgl.frm');
		exit('首页调整中');

		session_write_close();
		set_time_limit(600);
		$now_date = strtotime(date('Y-m-d')) + 86400;//当前日期（时间戳）
		
		$lately_30 = $now_date - (30 * 86400);//30天前日期（时间戳）
		
		for($i=1;$i<15;$i++){
			
			$cache_time = 57600 + (7200 * $i);
			if($i == 1) $cache_time = 60;
			$monitorList[$i]['date'] = date('Y-m-d',($now_date - $i*86400));
			
			$monitorList[$i]['ad_count'] = M('tad')->cache(true,$cache_time)->where(array('fstate'=>array('neq',-1),'fcreatetime'=>array('between',$monitorList[$i]['date'].','.$monitorList[$i]['date'].' 23:59:59')))->count();//广告数;
			$tvsample_i_count = M('ttvsample')->cache(true,$cache_time)->where(array('fcreatetime'=>array('between',$monitorList[$i]['date'].','.$monitorList[$i]['date'].' 23:59:59')))->count();//电视样本数
			//$tvissue_i_count = M('ttvissue')->cache(true,$cache_time)->where(array('fstarttime'=>array('between',$monitorList[$i]['date'].','.$monitorList[$i]['date'].' 23:59:59')))->count();//电视发布数
			
			
			$bcsample_i_count = M('tbcsample')->cache(true,$cache_time)->where(array('fcreatetime'=>array('between',$monitorList[$i]['date'].','.$monitorList[$i]['date'].' 23:59:59')))->count();//广播样本数
			//$bcissue_i_count = M('tbcissue')->cache(true,$cache_time)->where(array('fstarttime'=>array('between',$monitorList[$i]['date'].','.$monitorList[$i]['date'].' 23:59:59')))->count();//广播发布数
			
			$papersample_i_count = M('tpapersample')->cache(true,$cache_time)->where(array('fcreatetime'=>array('between',$monitorList[$i]['date'].','.$monitorList[$i]['date'].' 23:59:59')))->count();//报纸样本数
			//$paperissue_i_count = M('tpaperissue')->cache(true,$cache_time)->where(array('fissuedate'=>array('between',$monitorList[$i]['date'].','.$monitorList[$i]['date'].' 23:59:59')))->count();//报纸发布数
			
			
			$monitorList[$i]['adsample_count'] = $tvsample_i_count + $bcsample_i_count + $papersample_i_count;
			$monitorList[$i]['adrelease_count'] = $tvissue_i_count + $bcissue_i_count + $paperissue_i_count;
			
			
		}
		sort($monitorList);
		
		$count['collect_count'] = M('tcollect')->cache(true,7200)->where()->count();//采集点总数
		$count['device_count'] = M('tdevice')->cache(true,7200)->where()->count();//设备总数
		$count['media_count'] = M('tmedia')->cache(true,3600)->where()->count();//媒介总数
		$count['mediaowner_count'] = M('tmediaowner')->cache(true,7200)->where()->count();//媒介机构总数

		$ad_count['ad_count'] = M('tad')->cache(true,36000)->where()->count();//广告总数
		
		$tvsample_count = M('ttvsample')->cache(true,36000)->where()->count();//电视样本总数
		$tvissue_count = M('ttvissue')->cache(true,36000)->where()->count();//电视发布总数
		
		$bcsample_count = M('tbcsample')->cache(true,36000)->where()->count();//广播样本总数
		$bcissue_count = M('tbcissue')->cache(true,36000)->where()->count();//广播发布总数
		
		$papersample_count = M('tpapersample')->cache(true,36000)->where()->count();//报纸样本总数
		$paperissue_count = M('tpaperissue')->cache(true,36000)->where()->count();//报纸发布总数
		
		$ad_count['sample_count'] = $tvsample_count + $bcsample_count + $papersample_count;//样本总数
		$ad_count['issue_count'] = $tvissue_count + $bcissue_count + $paperissue_count;//发布总数
		

		//$tvissue_30_count = M('ttvissue')->cache(true,57600)->where(array('fissuedate'=>array('between',date('Y-m-d',$lately_30).','.date('Y-m-d',$now_date))))->count();//电视近30天发布数
		//$bcissue_30_count = M('tbcissue')->cache(true,57600)->where(array('fissuedate'=>array('between',date('Y-m-d',$lately_30).','.date('Y-m-d',$now_date))))->count();//广播近30天发布数
		//$paperissue_30_count = M('tpaperissue')->cache(true,57600)->where(array('fissuedate'=>array('between',date('Y-m-d',$lately_30).','.date('Y-m-d',$now_date))))->count();//报纸近30天发布数
		$ad_count['issue_30_count'] = $tvissue_30_count + $bcissue_30_count + $paperissue_30_count;
/* 		$tv_ad_list = M('ttvissue')
								->cache(true,36000)
								->field('
											ttvissue.ftvsampleid,
											count(*) as count,
											ttvsample.fversion,
											(select fadname from tad where fadid = ttvsample.fadid) as fadname,
											(select tadclass.ffullname from tadclass join tad on tad.fadclasscode = tadclass.fcode where tad.fadid = ttvsample.fadid) as adclass_fullname,
											(select fmedianame from tmedia where fid = ttvissue.fmediaid) as fmedianame,
											(select fillegaltype from tillegaltype where fcode = ttvsample.fillegaltypecode) as fillegaltype
										')
								->join('ttvsample on ttvsample.fid = ttvissue.ftvsampleid')
								->join('tad on tad.fadid = ttvsample.fadid')
								
								->group('ttvissue.ftvsampleid')
								->where(array( 'left(tad.fadclasscode,2)'=>array('neq','23'), 'ttvissue.fissuedate'=>array('between',array(date('Y-m-d',time()-86400*30),date('Y-m-d')))))
								->order('count desc')
								->limit(10)
								->select(); */
		//var_dump($tv_ad_list);						
/* 		$bc_ad_list = M('tbcissue')
								->cache(true,36000)
								->field('
											tbcissue.fbcsampleid,
											count(*) as count,
											tbcsample.fversion,
											(select fadname from tad where fadid = tbcsample.fadid) as fadname,
											(select tadclass.ffullname from tadclass join tad on tad.fadclasscode = tadclass.fcode where tad.fadid = tbcsample.fadid) as adclass_fullname,
											(select fmedianame from tmedia where fid = tbcissue.fmediaid) as fmedianame,
											(select fillegaltype from tillegaltype where fcode = tbcsample.fillegaltypecode) as fillegaltype
										')
								->join('tbcsample on tbcsample.fid = tbcissue.fbcsampleid')
								->join('tad on tad.fadid = tbcsample.fadid')
								
								->group('tbcissue.fbcsampleid')
								->where(array( 'left(tad.fadclasscode,2)'=>array('neq','23'), 'tbcissue.fissuedate'=>array('between',array(date('Y-m-d',time()-86400*30),date('Y-m-d')))))
								->order('count desc')
								->limit(10)
								->select(); */

		$bc_ad_list = list_k($bc_ad_list,1,1);//为列表加上序号
		$tv_ad_list = list_k($tv_ad_list,1,1);//为列表加上序号
		
		$this->assign('tv_ad_list',$tv_ad_list);
		$this->assign('bc_ad_list',$bc_ad_list);
		
		$this->assign('count',$count);
		$this->assign('ad_count',$ad_count);
		
		$this->assign('monitorList',$monitorList);
		$this->display();
	}
	
	
	/*获取平台是否需要手机验证码*/
	public function pant(){
		
		$this->ajaxReturn(array('code'=>0,'need'=>0,'msg'=>''));
	}
	

}