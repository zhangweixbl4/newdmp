<?php
namespace Api\Model;
use Think\Exception;
use Think\Log;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;

class CustomerIssueModel{
	
	
	
	
	/*写入客户记录发布数据*/
    public function update_issue_data($month_date,$media_id,$datas,$forgid,$syn_illegal=true,$need_w_mysql=true){
		
		
		$iytVa = M('iyt')->cache(true,60)->where(array('fc'=>$forgid))->getField('va');
		$iytList = explode("\n",$iytVa);
		
		$forgid = intval($forgid);
		$fcustomer = M('tregulator')->cache(true,60)->where(array('fcode'=>$forgid))->getField('fregionid');
		$fcustomer = intval($fcustomer);
		
		
		
		if(!$fcustomer && $need_w_mysql) return array('openInputNum'=>0,'sqlInputNum'=>0, 'openDelNum'=>0,'sqlDelNum'=>0 );
		
		
		$delState = $this->delete_issue_data($month_date,$media_id,$forgid,$syn_illegal,$need_w_mysql);//删除当天该媒体的数据
		#if($forgid) $this->delete_issue_data($month_date,$media_id,0,$syn_illegal,$need_w_mysql);
		
		$mediaInfo = M('tmedia')
								->cache(true,600)
								->field('	
											tmedia.fid,
											left(tmedia.fmediaclassid,2) as media_class,
											tmediaowner.fregionid,
											tmedia.fmediaownerid
										')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>$media_id))
								->find();//媒介信息
		if(!$mediaInfo) return array('openInputNum'=>0,'sqlInputNum'=>0, 'openDelNum'=>0,'sqlDelNum'=>0 );
		
		
		$openInputNum = 0;//OpenSearch的写入数量
		$sqlInputNum = 0;//mMySQL的写入数量
		$samInputNum = 0;//汇总数据的写入数量
		$illNum = 0;
		
		$openDatas = array();//写入OpenSearch的数组初始化
		$mysqlDatas = array();//写入MySQL的数组初始化
	
		$ad_summary_day_adclass = array();
		
		$tbn_ad_summary_day_v3_gongyi = array();
		
		//公益广告统计初始化，by zw ************************************
		$tbn_ad_summary_day_v3_gongyi['fmediaid'] 			= $media_id;
		$tbn_ad_summary_day_v3_gongyi['fmediaownerid'] 	= $mediaInfo['fmediaownerid'];
		$tbn_ad_summary_day_v3_gongyi['fdate'] 			= $month_date;
		$tbn_ad_summary_day_v3_gongyi['fmedia_class_code'] = $mediaInfo['media_class'];
		$tbn_ad_summary_day_v3_gongyi['fregionid'] 		= $mediaInfo['fregionid'];
		$tbn_ad_summary_day_v3_gongyi['fad_times'] 		= 0;
		$tbn_ad_summary_day_v3_gongyi['fad_play_len'] 		= 0;
		$tbn_ad_summary_day_v3_gongyi['fad_times_0608'] 	= 0;
		$tbn_ad_summary_day_v3_gongyi['fad_play_len_0608'] = 0;
		$tbn_ad_summary_day_v3_gongyi['fad_times_1113'] 	= 0;
		$tbn_ad_summary_day_v3_gongyi['fad_play_len_1113'] = 0;
		$tbn_ad_summary_day_v3_gongyi['fad_times_1921'] 	= 0;
		$tbn_ad_summary_day_v3_gongyi['fad_play_len_1921'] = 0;
		$tbn_ad_summary_day_v3_gongyi['fcustomer'] = $fcustomer;//
		$tbn_ad_summary_day_v3_gongyi['forgid'] = $forgid;//
		//公益广告统计 ************************************
		
		foreach($datas as $dataValue){//循环传入的数据***********************************************************************************************************
			if($media_id == '11000010002941' && $dataValue['fissuedate'] >= strtotime('2020-04-13') && $dataValue['fissuedate'] <= strtotime('2020-04-23')){
				
				if($dataValue['fstarttime'] >= strtotime($month_date.' 09:20:00') && $dataValue['fstarttime'] <= strtotime($month_date.' 09:32:00')) continue;
				if($dataValue['fstarttime'] >= strtotime($month_date.' 10:05:00') && $dataValue['fstarttime'] <= strtotime($month_date.' 10:25:00')) continue;
				if($dataValue['fstarttime'] >= strtotime($month_date.' 11:05:00') && $dataValue['fstarttime'] <= strtotime($month_date.' 11:25:00')) continue;
				if($dataValue['fstarttime'] >= strtotime($month_date.' 12:00:00') && $dataValue['fstarttime'] <= strtotime($month_date.' 12:20:00')) continue;
				if($dataValue['fstarttime'] >= strtotime($month_date.' 13:00:00') && $dataValue['fstarttime'] <= strtotime($month_date.' 13:30:00')) continue;
				if($dataValue['fstarttime'] >= strtotime($month_date.' 14:20:00') && $dataValue['fstarttime'] <= strtotime($month_date.' 14:40:00')) continue;
				if($dataValue['fstarttime'] >= strtotime($month_date.' 15:20:00') && $dataValue['fstarttime'] <= strtotime($month_date.' 15:40:00')) continue;
				if($dataValue['fstarttime'] >= strtotime($month_date.' 16:15:00') && $dataValue['fstarttime'] <= strtotime($month_date.' 16:35:00')) continue;
				if($dataValue['fstarttime'] >= strtotime($month_date.' 17:15:00') && $dataValue['fstarttime'] <= strtotime($month_date.' 17:45:00')) continue;
				if($dataValue['fstarttime'] >= strtotime($month_date.' 00:10:00') && $dataValue['fstarttime'] <= strtotime($month_date.' 00:30:00')) continue;
				
				
			}
			
			$bre = false; #是否需要排除，初始值为不需要
			foreach($iytList as $iyt){#判断数据是否需要排除
				$iyt = trim($iyt); #去除前后空格
				if(strlen($iyt) > 2 && strstr($dataValue['fadname'],$iyt)){ #关键词大于2个字符，且模糊匹配成功
					$bre = true;
					break;
				}
			}
			if($bre && $need_w_mysql && $syn_illegal){
				$dmpLogs2 = A('Common/AliyunLog','Model')->dmpLogs2('extract_issue_err',array('forgid'=>$forgid,'dataValue'=>json_encode($dataValue)));		
				continue;
			}
			
			
			$left2_adclasscode = strval(substr($dataValue['fadclasscode'][0],0,2));
			$left4_adclasscode = strval(substr($dataValue['fadclasscode'][0],0,4));

			$flength = intval($dataValue['flength']);
			/* if($dataValue['fmediaclassid'] == 1){
				$flength = intval(M('ttvsample_370300')->cache(true,60)->where(array('fid'=>$dataValue['fsampleid']))->getField('fadlen'));
			}elseif($dataValue['fmediaclassid'] == 2){
				$flength = intval(M('tbcsample_370300')->cache(true,60)->where(array('fid'=>$dataValue['fsampleid']))->getField('fadlen'));
			} */

			//公益广告统计，by zw ************************************
			if($left4_adclasscode == 2202){
				$tbn_ad_summary_day_v3_gongyi['fad_times'] += 1;//全天条次
				$tbn_ad_summary_day_v3_gongyi['fad_play_len'] += $flength;//全天时长
				if((int)$mediaInfo['media_class'] == 1 || (int)$mediaInfo['media_class'] == 2){
					$gytimec = $dataValue['fstarttime'] - $dataValue['fissuedate'];//获取播出时间点
					if($gytimec>=21600 && $gytimec<=28800){//6点到8点公益广告
						$tbn_ad_summary_day_v3_gongyi['fad_times_0608'] += 1;
						$tbn_ad_summary_day_v3_gongyi['fad_play_len_0608'] += $flength;
					}
					if($gytimec>=39600 && $gytimec<=46800){//11点到13点公益广告
						$tbn_ad_summary_day_v3_gongyi['fad_times_1113'] += 1;
						$tbn_ad_summary_day_v3_gongyi['fad_play_len_1113'] += $flength;
					}
					if($gytimec>=68400 && $gytimec<=75600){//19点到21点公益广告
						$tbn_ad_summary_day_v3_gongyi['fad_times_1921'] += 1;
						$tbn_ad_summary_day_v3_gongyi['fad_play_len_1921'] += $flength;
					}
				}
			}
			//公益广告统计************************************
			
			$ad_summary_day_adclass[$left2_adclasscode]['fad_times_long_ad'] += 1;//包含长广告的发布条次
			$ad_summary_day_adclass[$left2_adclasscode]['fad_play_len_long_ad'] += $flength;//包含长广告的发布时长
			$ad_summary_day_adclass[$left2_adclasscode]['fsam_list_long_ad'][] = $dataValue['fsampleid'].'_'.$mediaInfo['media_class'];//包含长广告的发布样本id列表
			if(intval($dataValue['fillegaltypecode']) == 30){//判断是否违法广告
				$ad_summary_day_adclass[$left2_adclasscode]['fad_illegal_times_long_ad'] += 1;//包含长广告的违法发布条次
				$ad_summary_day_adclass[$left2_adclasscode]['fad_illegal_play_len_long_ad'] += $flength;//包含长广告的违法发布时长
				$ad_summary_day_adclass[$left2_adclasscode]['fsam_illegal_list_long_ad'][] = $dataValue['fsampleid'].'_'.$mediaInfo['media_class'];//包含长广告的违法发布样本
			
				if($syn_illegal && $forgid != 20100000){
					$illegal_issue_id = $this->handle_illegal_ad($dataValue,$mediaInfo,$forgid);//处理违法广告								
					if($illegal_issue_id) $illNum += 1;
				}
				
			}

			
			if($flength < 600){//判断是否长广告
				
				
				$ad_summary_day_adclass[$left2_adclasscode]['fad_times'] += 1;//广告的发布条次
				$ad_summary_day_adclass[$left2_adclasscode]['fad_play_len'] += $flength;//广告的发布时长
				$ad_summary_day_adclass[$left2_adclasscode]['fsam_list'][] = $dataValue['fsampleid'].'_'.$mediaInfo['media_class'];//广告的发布样本id列表
				if(intval($dataValue['fillegaltypecode']) == 30){//判断是否违法广告
					$ad_summary_day_adclass[$left2_adclasscode]['fad_illegal_times'] += 1;//广告的违法发布条次
					$ad_summary_day_adclass[$left2_adclasscode]['fad_illegal_play_len'] += $flength;//广告的违法发布时长
					$ad_summary_day_adclass[$left2_adclasscode]['fsam_illegal_list'][] = $dataValue['fsampleid'].'_'.$mediaInfo['media_class'];//广告的违法发布样本
				} 
				
			}
	
			$openData = array();//写入OpenSearch的数组初始化（单个）
			$mysqlData = array();//写入MySQL的数组初始化（单个）
			
			foreach($dataValue as $field => $value){//循环所有字段
				if(is_array($value)){//判断如果是数组
					$mysqlData[$field] = implode('|',$value);//如果是数组,在MySQL里面用竖线分隔
				}else{
					$mysqlData[$field] = $value;//
				}
				$openData[$field] = $value;//OpenSearch直接赋值
 				if($field == 'fprice') $openData[$field] = intval($value);
			}
			$openData['last_update_time'] = time();//OpenSearch的最后更新时间
			$mysqlData['last_update_time'] = time();//MySQL的最后更新时间
			

			$openData['fcustomer'] = $fcustomer;//
			$mysqlData['fcustomer'] = $fcustomer;//
			
			$openData['forgid'] = $forgid;//
			$mysqlData['forgid'] = $forgid;//
			
			
			
			

			
			$openDatas[] = array('cmd'=>'add','fields'=>$openData);
			$mysqlDatas[] = $mysqlData;
			
			
			if(count($openDatas) >= 600){
				$inputState_status = ''; #写入OpenSearch的状态
				$while_num = 0; #尝试写入的次数
				
				while($inputState_status != 'OK'){ #如果不是写入成功
					if($while_num > 5) break; #如果已经尝试5次以上
					$while_num += 1;
					$inputState = A('Common/OpenSearch','Model')->push('ad_issue',$openDatas,C('OPEN_SEARCH_CUSTOMER'));
					$inputState = json_decode($inputState->result,true);
					if($inputState['status'] == 'OK'){
						$inputState_status = 'OK';
						$openInputNum += COUNT($openDatas);
						$openDatas = array();
						break;
					}else{
						sleep(1);
					} 
				}
				
			}
			
		}//***************************循环传入的数结束************************************************************************************************************
		
		
		
		
		if(count($openDatas) > 0){
			$inputState_status = ''; #写入OpenSearch的状态
			$while_num = 0; #尝试写入的次数
			
			while($inputState_status != 'OK'){ #如果不是写入成功
				if($while_num > 5) break; #如果已经尝试5次以上
				$while_num += 1;
				$inputState = A('Common/OpenSearch','Model')->push('ad_issue',$openDatas,C('OPEN_SEARCH_CUSTOMER'));
				$inputState = json_decode($inputState->result,true);
				if($inputState['status'] == 'OK'){
					$inputState_status = 'OK';
					$openInputNum += COUNT($openDatas);
					$openDatas = array();
					break;
				}else{
					sleep(1);
				} 
			}
		}
		
		
		#M('hzsj_dmp_ad')->addAll($mysqlDatas);//写入数据库
		
		$sqlInputNum += M('hzsj_dmp_ad','',C('FENXI_DB'))->addAll($mysqlDatas);//写入分析型数据库
		
		
		$tbn_ad_summary_day_v3 = array();

		foreach($ad_summary_day_adclass as $adclass => $value){
			
			$wwData = array();
			$wwData['fmediaid'] = $media_id;
			$wwData['fmediaownerid'] = $mediaInfo['fmediaownerid'];
			$wwData['fdate'] = $month_date;
			$wwData['fmedia_class_code'] = $mediaInfo['media_class'];
			$wwData['fregionid'] = $mediaInfo['fregionid'];
			$wwData['fad_class_code'] = $adclass;
			$wwData['fad_times'] = intval($value['fad_times']);
			$wwData['fad_play_len'] = intval($value['fad_play_len']);
			$wwData['fsam_list'] = strval(implode(',',array_unique($value['fsam_list'])));//广告样本list
			$wwData['fad_illegal_times'] = intval($value['fad_illegal_times']);
			$wwData['fad_illegal_play'] = intval($value['fad_illegal_play']);
			$wwData['fsam_illegal_list'] = strval(implode(',',array_unique($value['fsam_illegal_list'])));//违法广告样本list
			$wwData['fad_times_long_ad'] = intval($value['fad_times_long_ad']);
			$wwData['fad_play_len_long_ad'] = intval($value['fad_play_len_long_ad']);
			$wwData['fsam_list_long_ad'] = strval(implode(',',array_unique($value['fsam_list_long_ad'])));//包含长广告的广告样本list
			$wwData['fad_illegal_times_long_ad'] = intval($value['fad_illegal_times_long_ad']);
			$wwData['fad_illegal_play_len_long_ad'] = intval($value['fad_illegal_play_len_long_ad']);
			$wwData['fsam_illegal_list_long_ad']  = strval(implode(',',array_unique($value['fsam_illegal_list_long_ad'])));//包含长广告的违法广告广告样本list
			$wwData['fcustomer'] = $fcustomer;//
			$wwData['forgid'] = $forgid;//

			$tbn_ad_summary_day_v3[] = $wwData;
		}
		
		
		$sumInputState = M('tbn_ad_summary_day_v3')->addAll($tbn_ad_summary_day_v3,array(),true);//写入汇总数据库

		
		$sumInputState_gongyi = M('tbn_ad_summary_day_v3_gongyi')->add($tbn_ad_summary_day_v3_gongyi,array(),true);//写入公益广告汇总数据库 by zw

		
		if($sumInputState) $sumInputNum += count($tbn_ad_summary_day_v3);
		
		return array('illNum'=>$illNum,'openInputNum'=>$openInputNum,'sqlInputNum'=>$sqlInputNum, 'openDelNum'=>$delState['openDelNum'],'sqlDelNum'=>$delState['sqlDelNum'] );
		
		
		
    }
	
	
	
	/*删除某天数据*/
    public function delete_issue_data($month_date,$media_id,$forgid,$need_del_illegal=true,$need_del_mysql=true){
		
		$openDelNum = 0;
		$sqlDelNum = 0;
		
		$OpenSearchClient = A('Common/OpenSearch','Model')->client();
		$searchClient = new SearchClient($OpenSearchClient);
		
		
		$params = new SearchParamsBuilder();
		$params->setHits(500);//返回数量
		$params->setAppName(C('OPEN_SEARCH_CUSTOMER'));
		$setQuery = A('Common/OpenSearch','Model')->setQuery(array('forgid'=>$forgid,'fmediaid'=>$media_id,'fissuedate'=>strtotime($month_date)));

		$params->setQuery($setQuery);//搜索
		$params->setFormat("json");//数据返回json格式
		$params->setFetchFields(array('identify'));//设置需返回哪些字段
		$params->setScrollExpire('3m');//设置下次scroll发送请求超时时间，用于scroll方法查询，此处为第一次访问，用于获取scrollId
		
			
			
		$ret = $searchClient->execute($params->build());//执行查询并返回信息
		$result = json_decode($ret->result,true);//第一次返回的数据，主要获取总数和scrollID
		$viewtotal = $result['result']['total'];//数据总量，也是剩余数量
		
		
		while($viewtotal > 0){//循环用scrollid查询数据
			
			$params->setScrollId($result['result']['scroll_id']);//通过上面第一次查询返回的 scrollId ，作为查询参数获取数据
			$ret = $searchClient->execute($params->build());//执行查询并返回信息
			$result = json_decode($ret->result,true);//
			$viewtotal -= $result['result']['num'];//计算剩余数据
			$delete_open_datas = array();//需要删除的数据初始化
			foreach($result['result']['items'] as $result_value){//循环需要删除的数据
				$delete_open_datas[] = array('cmd'=>'delete','fields'=>array('identify'=>$result_value['identify']));//组装需要删除的数据
			}
			
			for($try_num=6;$try_num>=0;$try_num-=1){
				$delete_state = A('Common/OpenSearch','Model')->push('ad_issue',$delete_open_datas,C('OPEN_SEARCH_CUSTOMER'));  //删除数据
				$delete_state = json_decode($delete_state->result,true);
				if($delete_state['status'] == 'OK'){
					$openDelNum += COUNT($delete_open_datas);
					break;
				}else{
					file_put_contents('LOG/delete_issue_data',date('Y-m-d H:i:s')."\n".$month_date.'	'.$media_id.'	'.$forgid ."\n". json_encode($delete_state) ."\n\n\n",FILE_APPEND);
					sleep(1);
				}
			}
		}
		
		#M('hzsj_dmp_ad')->where(array('fmediaid'=>$media_id,'fissuedate'=>strtotime($month_date)))->delete();//删除MySQL数据库的数据
		if($need_del_mysql){ #是否需要删除mysql的数据
			$del_sql_state = M('hzsj_dmp_ad','',C('FENXI_DB'))->where(array('forgid'=>$forgid,'fmediaid'=>$media_id,'fissuedate'=>strtotime($month_date)))->delete();//删除分析型数据库的数据
			$sqlDelNum += $del_sql_state;
		}
		$sumDelNum = M('tbn_ad_summary_day_v3')->where(array('forgid'=>$forgid,'fmediaid'=>$media_id,'fdate'=>$month_date))->delete();//删除汇总表里面该媒体的这一天的数据
		M('tbn_ad_summary_day_v3','',C('FENXI_DB'))->where(array('forgid'=>$forgid,'fmediaid'=>$media_id,'fdate'=>$month_date))->delete();//删除汇总表里面该媒体的这一天的数据
		
		if($need_del_illegal){ #是否需要删除违法
			$del_illegal_issue_id_list = M('tbn_illegal_ad_issue')
																
																->join('tbn_illegal_ad on tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id')
																->where(array(
																				'tbn_illegal_ad.fcustomer'=>array('neq',100000),
																				'tbn_illegal_ad.fmedia_id'=>$media_id,
																				'tbn_illegal_ad_issue.fissue_date'=>$month_date,
																				'tbn_illegal_ad.identify'=>'new_',
																				'tbn_illegal_ad.forgid'=>['in','0,'.$forgid],
																				))
																->getField('tbn_illegal_ad_issue.fid',true);
			if($del_illegal_issue_id_list){
				$illegalIssueDelNum = M('tbn_illegal_ad_issue')->where(array('fid'=>array('in',$del_illegal_issue_id_list)))->delete();//删除这一天的违法发布记录 */
			}
		}
		$sumDelNum_GY = M('tbn_ad_summary_day_v3_gongyi')->where(['forgid'=>$forgid,'fmediaid'=>$media_id,'fdate'=>$month_date])->delete();//删除公益广告汇总数据，by zw
		
		return array('openDelNum'=>$openDelNum,'sqlDelNum'=>$sqlDelNum);
			
    }
	
	
	
	/*处理违法广告*/
	public function handle_illegal_ad($dataValue,$mediaInfo,$forgid){
		if(!$forgid || $forgid == '20100000') return false;
 		$forgid = intval($forgid);
		$fcustomer = M('tregulator')->cache(true,60)->where(array('fcode'=>$forgid))->getField('fregionid');
		$fcustomer = intval($fcustomer);
		$tbn_illegal_ad_id = $this->add_illegal_ad($dataValue,$fcustomer,'new_',$forgid);//添加并获取违法广告id
	
		$illegal_issue_id = $this->add_illegal_issue(
														$tbn_illegal_ad_id,
														$mediaInfo,
														$dataValue['fissuedate'],
														$dataValue['fstarttime'],
														$dataValue['fendtime'],
														md5($tbn_illegal_ad_id.'_'.$dataValue['identify'])
													);
													
		return $illegal_issue_id;

			
	}
	
	
	/*新增违法广告(客户)*/
	public function add_illegal_ad($dataInfo,$fcustomer,$identify,$forgid=0){

		$mediaInfo = M('tmedia')->field('left(fmediaclassid,2) as media_class')->where(array('fid'=>$dataInfo['fmediaid']))->find();
		$media_class = intval($mediaInfo['media_class']);
		
		$fexpressioncodes = explode(';',str_replace(';;',';',$dataInfo['fexpressioncodes']));	
		
		$fexpressions = implode(';;',M('tillegal')->where(array('fcode'=>array('in',$fexpressioncodes)))->getField('fexpression',true));
		
		$tbn_illegal_ad_data = array();
		$tbn_illegal_ad_data['fregion_id'] = $dataInfo['fregionid'];//地区id
		$tbn_illegal_ad_data['fmedia_class'] = $dataInfo['fmediaclassid'];//媒介类别
		$tbn_illegal_ad_data['fmedia_id'] = $dataInfo['fmediaid'];//媒介id
		$tbn_illegal_ad_data['fad_class_code'] = $dataInfo['fadclasscode'][0];//广告类别
		
		$tbn_illegal_ad_data['fsample_id'] = $dataInfo['fsampleid'];//样本id
		
		$tbn_illegal_ad_data['fad_name'] = $dataInfo['fadname'];//广告名称
		$tbn_illegal_ad_data['fillegal_code'] = $dataInfo['fexpressioncodes'];//违法代码 
		$tbn_illegal_ad_data['fmediaownerid'] = $dataInfo['fmediaownerid'];//媒介机构id
		$tbn_illegal_ad_data['identify'] = $identify;//标识
		$tbn_illegal_ad_data['fadowner'] = $dataInfo['fadowner'];//广告主
		
		
		$tadownerInfo = M('tadowner')->cache(true,600)->where(array('fname'=>$dataInfo['fadowner']))->find();
		
		$tbn_illegal_ad_data['fadownerid'] = intval($tadownerInfo['fid']);//广告主
		$tbn_illegal_ad_data['adowner_regionid'] = intval($tadownerInfo['fregionid']);//广告主
		
		
		$tbn_illegal_ad_data['fillegal'] = $dataInfo['fillegalcontent'];//违法内容
		$tbn_illegal_ad_data['favifilename'] = $dataInfo['sam_source_path'];//视频、音频路径（电视、广播）
		$tbn_illegal_ad_data['fjpgfilename'] = $dataInfo['sam_source_path'];//图片路径（报刊）
		$tbn_illegal_ad_data['fexpressions'] = $fexpressions;//违法表现
		$tbn_illegal_ad_data['fillegaltypecode'] = $dataInfo['fillegaltypecode'];//违法程度
		$tbn_illegal_ad_data['fcustomer'] = $fcustomer ;//该违法广告的所属机构的地区
		$tbn_illegal_ad_data['create_time'] = date('Y-m-d H:i:s');//创建时间
		$tbn_illegal_ad_data['unique_k'] = date('Y-m-d');
		$tbn_illegal_ad_data['forgid'] = $forgid;
		
		
		if($dataInfo['fmediaclassid'] == 13){
			$tbn_illegal_ad_data['fplatform'] = $dataInfo['issue_platform_type'];
			$tbn_illegal_ad_data['favifilename'] = $dataInfo['target_url'];
			$tbn_illegal_ad_data['fjpgfilename'] = $dataInfo['landing_img'];
		}
		

		
		$tbn_illegal_ad_info = M('tbn_illegal_ad')
												->field('fid')
												->where(array('fcustomer'=>$fcustomer,'fmedia_class'=>$media_class,'fsample_id'=>$dataInfo['fsampleid'],'identify'=>$identify,'fstatus'=>array('in',array(0,30))))
												->find();//查询记录是否存在
		
	
		if(!$tbn_illegal_ad_info){//
			try{//尝试写入数据
				$tbn_illegal_ad_id = M('tbn_illegal_ad')->add($tbn_illegal_ad_data);//添加违法广告样本
				#sleep(1);//休息一秒钟，防止只读库数据不一致
				try{
					A('Api/IllegalAdIssue')->illegal_ad_sendlog([$tbn_illegal_ad_id]);//生成违法广告的派发记录
				}catch(Exception $error2) { 
				
				}
				
			}catch(Exception $error) { //
				$tbn_illegal_ad_id = M('tbn_illegal_ad')->where(array('fcustomer'=>$fcustomer,'fmedia_class'=>$media_class,'fsample_id'=>$dataInfo['fsampleid'],'identify'=>$identify))->getField('fid');//查询记录是否存在;//
			} 
			
		}else{
			$tbn_illegal_ad_id = $tbn_illegal_ad_info['fid'];//违法广告id
		}									
												
												
																					
												

		return $tbn_illegal_ad_id;

	}
	
	
	
	/*新增违法发布记录*/
	public function add_illegal_issue($tbn_illegal_ad_id,$mediaInfo,$fissue_date,$fstarttime,$fendtime,$funique){
		
		$fissue_date = date('Y-m-d',$fissue_date);//时间戳类型转换成日期类型
		
		if(!$fstarttime || !$fendtime){//判断日期是否正常
			$fstarttime = null;
			$fendtime = null;
		}else{
			$fstarttime = date('Y-m-d H:i:s',$fstarttime);
			$fendtime = date('Y-m-d H:i:s',$fendtime);
		}
		

		$media_class = intval($mediaInfo['media_class']);

		
		$illegal_issue_id = M('tbn_illegal_ad_issue')->where(array('funique'=>$funique))->getField('fid');
		
		if($illegal_issue_id){
			return $illegal_issue_id; 
		}
		
		try{//尝试写入数据
			$illegal_issue_id = M('tbn_illegal_ad_issue')->add(array(
																
																	'fillegal_ad_id'=>	$tbn_illegal_ad_id,
																	'fmedia_id'		=>	$mediaInfo['fid'],
																	'fissue_date'	=>	$fissue_date,
																	'fstarttime'	=>	$fstarttime,
																	'fendtime'		=>	$fendtime,
																	'fmedia_class'	=>	$media_class,
																	'fmediaownerid'	=>	$mediaInfo['fmediaownerid'],
																	'funique'		=>	$funique,
																	));	
		}catch(Exception $error) { //
			$illegal_issue_id = M('tbn_illegal_ad_issue')->where(array('funique'=>$funique))->getField('fid');
		} 
		
		
		

		
												
		return $illegal_issue_id;										
		
		
		
	}
	
	
	

	
	
	
	
	
	
	
	
}