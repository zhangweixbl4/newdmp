<?php
namespace Api\Controller;
use Think\Controller;

//获取疫情汇总数据临时接口
class YiqingDataTempController extends Controller {

	/**
	* 接口接收的JSON参数并解码为对象
	*/
	protected $oParam;

	/**
	 * 构造函数
	 * @Param	Mixed variable
	 * @Return	void 返回的数据
	 */
	public function _initialize(){
		header("Content-type:text/html;charset=utf-8");
		header('Access-Control-Allow-Origin:*');//允许所有域名跨域访问
		//请求方式为options时，不返回结果
		if(strtoupper($_SERVER['REQUEST_METHOD'])== 'OPTIONS'){
	        exit;
	    }
		$this->oParam = json_decode(file_get_contents('php://input'),true);
	}
	
	/*
	* 湖南疫情数据获取临时接口
	*/
	public function getYiQingData(){
		session_write_close();
		ini_set('memory_limit','2048M');
		set_time_limit(120);
		$st = I('st');
		$ed = I('ed');
		$regionid = I('rg')?I('rg'):430000;
		if(empty($st) || empty($ed)){
			echo '时间传参有误';
			die;
		}

		$tregion_len = $this->get_tregionlevel($regionid);
		if($tregion_len == 1){//国家级
			$wherestr = ' media_region_id ='.$regionid;	
		}elseif($tregion_len == 2){//省级
			$wherestr = ' media_region_id like "'.substr($regionid,0,2).'%"';
		}elseif($tregion_len == 4){//市级
			$wherestr = ' media_region_id like "'.substr($regionid,0,4).'%"';
		}elseif($tregion_len == 6){//县级
			$wherestr = ' media_region_id like "'.substr($regionid,0,6).'%"';
		}

		$st2 = date('Y-m-d',strtotime($st));
		$ed2 = date('Y-m-d',strtotime($ed));
		$edtime = date('H:i:s',strtotime($ed));
		if($edtime == '00:00:00'){
			$ed = date('Y-m-d',strtotime($ed)).' 23:59:59';
		}else{
			if(date('Y-m-d',strtotime($st)) != date('Y-m-d',strtotime($ed))){
				$st2 = $st;
				$ed2 = $ed;
			}
		}

		$sqlstr = '
			select * from (
			(
			SELECT fmedianame,mclass, SUM(acount) acount
			FROM (
			SELECT COUNT(*) AS acount,tmedia.fmedianame,"电视" mclass
			FROM ttvissue e
			INNER JOIN ttvsample ON e.ftvsampleid=ttvsample.fid AND ttvsample.fstate = 1
			INNER JOIN tad ON ttvsample.fadid=tad.fadid AND tad.fadid<>0 AND
			LEFT(tad.fadclasscode,2) <> "23"
			INNER JOIN tadowner ON tad.fadowner = tadowner.fid
			INNER JOIN tadclass ON tad.fadclasscode=tadclass.fcode
			INNER JOIN tmedia ON e.fmediaid=tmedia.fid
			INNER JOIN (
			SELECT DISTINCT(fmediaid)
			FROM tregulatormedia a
			WHERE a.fregulatorcode = "20'.$regionid.'" AND a.fstate = 1 AND a.fisoutmedia = 0 AND fcustomer = "'.$regionid.'") AS ttp ON tmedia.fid=ttp.fmediaid
			WHERE 1=1 AND tmedia.fid = tmedia.main_media_id AND '.$wherestr.' AND ttvsample.fversion LIKE "%病毒%"
			AND e.fstarttime between "'.$st.'" AND "'.$ed.'" 
			GROUP BY tmedia.fid
			) a
			GROUP BY fmedianame)

			union all(
			SELECT fmedianame,mclass, SUM(acount) acount
			FROM (
			SELECT COUNT(*) AS acount,tmedia.fmedianame,"广播" mclass
			FROM tbcissue e
			INNER JOIN tbcsample ON e.fbcsampleid=tbcsample.fid AND tbcsample.fstate = 1
			INNER JOIN tad ON tbcsample.fadid=tad.fadid AND tad.fadid<>0 AND
			LEFT(tad.fadclasscode,2) <> "23"
			INNER JOIN tadowner ON tad.fadowner = tadowner.fid
			INNER JOIN tadclass ON tad.fadclasscode=tadclass.fcode
			INNER JOIN tmedia ON e.fmediaid=tmedia.fid
			INNER JOIN (
			SELECT DISTINCT(fmediaid)
			FROM tregulatormedia a
			WHERE a.fregulatorcode = "20'.$regionid.'" AND a.fstate = 1 AND a.fisoutmedia = 0 AND fcustomer = "'.$regionid.'") AS ttp ON tmedia.fid=ttp.fmediaid
			WHERE 1=1 AND tmedia.fid = tmedia.main_media_id AND '.$wherestr.' AND tbcsample.fversion LIKE "%病毒%"
			AND e.fstarttime between "'.$st.'" AND "'.$ed.'"
			GROUP BY tmedia.fid
			) a
			GROUP BY fmedianame)

			union all(
			SELECT fmedianame,mclass, SUM(acount) acount
			FROM (
				(
			SELECT COUNT(*) AS acount,tmedia.fmedianame,"报纸" mclass
			FROM tpaperissue e
			INNER JOIN tpapersample ON e.fpapersampleid=tpapersample.fpapersampleid AND tpapersample.fstate = 1
			INNER JOIN tad ON tpapersample.fadid=tad.fadid AND tad.fadid<>0 AND
			LEFT(tad.fadclasscode,2) <> "23"
			INNER JOIN tadowner ON tad.fadowner = tadowner.fid
			INNER JOIN tadclass ON tad.fadclasscode=tadclass.fcode
			INNER JOIN tmedia ON e.fmediaid=tmedia.fid
			INNER JOIN (
			SELECT DISTINCT(fmediaid)
			FROM tregulatormedia a
			WHERE a.fregulatorcode = "20'.$regionid.'" AND a.fstate = 1 AND a.fisoutmedia = 0 AND fcustomer = "'.$regionid.'") AS ttp ON tmedia.fid=ttp.fmediaid
			WHERE 1=1 AND tmedia.fid = tmedia.main_media_id AND '.$wherestr.' AND tpapersample.fversion LIKE "%病毒%"
			AND e.fissuedate BETWEEN "'.$st2.'" AND "'.$ed2.'"
			GROUP BY tmedia.fid
				)
			) a
			GROUP BY fmedianame)
			) mm
		';
		$data = M()->query($sqlstr);

		$outdata['title'] = '传统媒体汇总'.$st.'至'.$ed;
		$outdata['datalie'] = [
          '序号'=>'key',
          '媒体名称'=>'fmedianame',
          '媒体类型'=>'mclass',
          '疫情广告发布量'=>'acount',
        ];

        $outdata['lists'] = $data;
        $ret = A('Api/Function')->outdata_xls($outdata);
        if(!empty($ret['url'])){
			echo $ret['url'];
        }else{
        	echo '暂无数据';
        }

	}

	/*
	* 湖南疫情数据获取临时接口
	*/
	public function getYiQingData_NET(){
		session_write_close();
		ini_set('memory_limit','2048M');
		set_time_limit(60);
		$st = I('st');
		$ed = I('ed');
		$regionid = I('rg')?I('rg'):430000;
		if(empty($st) || empty($ed)){
			echo '时间传参有误';
			die;
		}
		$st = date('Y-m-d',strtotime($st));
		$ed = date('Y-m-d',strtotime($ed));

		$tregion_len = $this->get_tregionlevel($regionid);
		if($tregion_len == 1){//国家级
			$wherestr = ' media_region_id ='.$regionid;	
		}elseif($tregion_len == 2){//省级
			$wherestr = ' media_region_id like "'.substr($regionid,0,2).'%"';
		}elseif($tregion_len == 4){//市级
			$wherestr = ' media_region_id like "'.substr($regionid,0,4).'%"';
		}elseif($tregion_len == 6){//县级
			$wherestr = ' media_region_id like "'.substr($regionid,0,6).'%"';
		}

		$sqlstr = "
			SELECT d.fclass,c.fmediacode,c.fmedianame, COUNT(*) AS tj, SUM(CASE WHEN fadtext LIKE '%肺炎%' OR fadtext LIKE '%防控%' OR fadtext LIKE '%诊%' OR b.fadname LIKE '%肺炎%' OR b.fadname LIKE '%防控%' OR b.fadname LIKE '%诊%' THEN 1 ELSE 0 END) acount
			FROM tnetissueputlog a
			INNER JOIN tnetissue b ON a.tid = b.major_key
			INNER JOIN tmedia c ON a.fmediaid = c.fid
			inner join tmediaclass d on left(c.fmediaclassid,4) = d.fid
			WHERE fissuedate between unix_timestamp('".$st."') AND unix_timestamp('".$ed."') AND a.fmediaid IN (
			SELECT fid
			FROM tmedia
			WHERE ".$wherestr." AND fmediaclassid LIKE '13%')
			GROUP BY c.fmediaclassid,c.fmediacode,c.fmedianame;
		";
		$data = M()->query($sqlstr);

		$outdata['title'] = '互联网媒体汇总'.$st.'至'.$ed;
		$outdata['datalie'] = [
          '序号'=>'key',
          '媒体类别'=>'fclass',
          '媒体代码'=>'fmediacode',
          '媒体名称'=>'fmedianame',
          '发布总量'=>'tj',
          '疫情广告发布量'=>'acount',
        ];

        $outdata['lists'] = $data;
        $ret = A('Api/Function')->outdata_xls($outdata);
        if(!empty($ret['url'])){
			echo $ret['url'];
        }else{
        	echo '暂无数据';
        }
	}

	private function get_tregionlevel($area){
        //判断机构级别，0区级，10市级，20省级，30国家级
        if((int)$area == 500000){
            return 2;
        }
        $region_id_rtrim = rtrim($area,'00');//去掉地区后面两位0，判断区
        $region_id_rtrim = rtrim($region_id_rtrim,'000');//去掉后面三位0，判断全国
        $region_id_rtrim = rtrim($region_id_rtrim,'00');//去掉后面两位0，判断市
        $tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位
        if($tregion_len == 3){
            $tregion_len = 4;
        }elseif($tregion_len == 5){
            $tregion_len = 6;
        }
        return $tregion_len;
    }

	/*
	* 湖南疫情数据获取临时接口
	*/
	public function getYiQingData_NETVIEW(){
		session_write_close();
		ini_set('memory_limit','2048M');
		set_time_limit(120);
		$st = I('st');
		$ed = I('ed');
		$version = I('version');
		$regionid = I('rg')?I('rg'):430000;
		if(empty($st) || empty($ed) || empty($version)){
			echo '时间传参有误';
			die;
		}
		$st = date('Y-m-d',strtotime($st));
		$ed = date('Y-m-d',strtotime($ed));

		$tregion_len = $this->get_tregionlevel($regionid);
		if($tregion_len == 1){//国家级
			$wherestr = ' media_region_id ='.$regionid;	
		}elseif($tregion_len == 2){//省级
			$wherestr = ' media_region_id like "'.substr($regionid,0,2).'%"';
		}elseif($tregion_len == 4){//市级
			$wherestr = ' media_region_id like "'.substr($regionid,0,4).'%"';
		}elseif($tregion_len == 6){//县级
			$wherestr = ' media_region_id like "'.substr($regionid,0,6).'%"';
		}

		$sqlstr = "
			select * from (
				SELECT c.fmedianame,d.fclass,c.fmediacode,b.*,a.net_created_date issuedate
				FROM tnetissueputlog a
				INNER JOIN tnetissue b ON a.tid = b.major_key
				INNER JOIN tmedia c ON a.fmediaid = c.fid
				inner join tmediaclass d on left(c.fmediaclassid,4) = d.fid
				WHERE fissuedate between unix_timestamp('".$st."') AND unix_timestamp('".$ed."') AND a.fmediaid IN (
				SELECT fid
				FROM tmedia
				WHERE ".$wherestr." AND fmediaclassid LIKE '13%' and left(fmediaclassid,4) not like '1303%')
				and b.fversion LIKE '%".$version."%'
				) aa
				
		";
		$data = M()->query($sqlstr);
		if(empty($data)){
			echo '暂无数据';
			die;
		}

		foreach ($data as $key => $value) {
			if($value['net_platform'] == 1){
				$data[$key]['net_platform'] = 'PC';
			}elseif($value['net_platform'] == 2){
				$data[$key]['net_platform'] = '移动端';
			}elseif($value['net_platform'] == 9){
				$data[$key]['net_platform'] = '微信端';
			}else{
				$data[$key]['net_platform'] = '';
			}
			$data[$key]['issuedate'] = date('Y-m-d H:i:s',$value['issuedate']/1000);
			$data[$key]['fillegaltypecode'] = !empty($value['fillegaltypecode'])?'违法':'不违法';
		}

		$outdata['title'] = '互联网媒体广告明细'.$st.'至'.$ed;
		$outdata['datalie'] = [
          '序号'=>'key',
          '媒体名称'=>'fmedianame',
          '媒体代码'=>'fmediacode',
          '媒体类别'=>'fclass',
          '广告位置x'=>'net_x',
          '广告位置y'=>'net_y',
          '广告宽度'=>'net_width',
          '广告高度'=>'net_height',
          '缩略图宽度'=>'net_thumb_width',
          '缩略图高度'=>'net_thumb_height',
          '广告URL'=>'net_url',
          '原始URL'=>'net_old_url',
          '缩略图url'=>'thumb_url_true',
          '文件大小'=>'net_size',
          '平台'=>'net_platform',
          '广告原始地址'=>'net_original_url',
          '落地页地址'=>'net_target_url',
          '广告发现时间'=>'issuedate',
          '广告主域名'=>'net_advertiser',
          '发布页截图地址'=>'net_original_snapshot',
          '广告快照'=>'net_snapshot',
          '广告名称'=>'fadname',
          '广告描述'=>'fadtext',
          '品牌'=>'fbrand',
          '代言人'=>'fspokesman',
          '审批号'=>'fapprovalid',
          '审批单位'=>'fapprovalunit',
          '违法类型代码'=>'fillegaltypecode',
          '涉嫌违法内容'=>'fillegalcontent',
          '违法表现代码'=>'fexpressioncodes',
          '违法表现'=>'fexpressions',
          '判定依据'=>'fconfirmations',
          '广告中标识的生产批准文号'=>'fadmanuno',
          '生产批准文号'=>'fmanuno',
          '广告中标识的广告批准文号'=>'fadapprno',
          '广告批准文号'=>'fapprno',
          '广告中标识的生产企业（证件持有人）名称'=>'fadent',
          '生产企业（证件持有人）名称'=>'fent',
          '生产企业（证件持有人）所在地区'=>'fentzone',
        ];

        $outdata['lists'] = $data;
        $ret = A('Api/Function')->outdata_xls($outdata);
        if(!empty($ret['url'])){
			echo $ret['url'];
        }else{
        	echo '暂无数据';
        }
	}

	/*
	* 国家领导人数据获取临时接口
	*/
	public function getLinDaoRenData_NETVIEW(){
		session_write_close();
		ini_set('memory_limit','2048M');
		set_time_limit(120);
		$st = I('st');
		$ed = I('ed');
		$fcustomer = I('rg')?I('rg'):430000;
		
		$wherestr = ' a.fcustomer = '.$fcustomer;

		$wherestr .= ' and (fexpressions like "%国家领导人%" or fexpressions like "%国家机关%")';

		if(!empty($st) && !empty($ed)){
			$wherestr .= ' and b.fissue_date between "'.$st.'" and "'.$ed.'"';
		}

		$sqlstr = "
			select a.fad_name,a.fmedia_class,d.ffullname adclassname,c.fmedianame,e.fname regionname,b.fissue_date,b.fstarttime,b.fendtime,a.fillegal_code,a.fexpressions,a.fillegal,a.favifilename,a.fjpgfilename
			from tbn_illegal_ad a 
			inner join tbn_illegal_ad_issue b on a.fid = b.fillegal_ad_id
			inner join tmedia c on a.fmedia_id = c.fid
			inner join tadclass d on a.fad_class_code = d.fcode
			inner join tregion e on a.fregion_id = e.fid
			where ".$wherestr."
		";
		$data = M()->query($sqlstr);
		if(empty($data)){
			echo '暂无数据';
			die;
		}

		foreach ($data as $key => $value) {
			if($value['net_platform'] == 1){
				$data[$key]['net_platform'] = 'PC';
			}elseif($value['net_platform'] == 2){
				$data[$key]['net_platform'] = '移动端';
			}elseif($value['net_platform'] == 9){
				$data[$key]['net_platform'] = '微信端';
			}else{
				$data[$key]['net_platform'] = '';
			}
			$data[$key]['issuedate'] = date('Y-m-d H:i:s',$value['issuedate']/1000);
			$data[$key]['fillegaltypecode'] = !empty($value['fillegaltypecode'])?'违法':'不违法';
		}

		$outdata['title'] = '互联网媒体疫情广告明细'.$st.'至'.$ed;
		$outdata['datalie'] = [
          '序号'=>'key',
          '媒体类型'=>[
	        'type'=>'zwif',
	        'data'=>[
	          ['{fmedia_class} == 1','电视'],
	          ['{fmedia_class} == 2','广播'],
	          ['{fmedia_class} == 3','报纸'],
	          ['{fmedia_class} == 13','互联网'],
	        ]
	      ],
          '广告名称'=>'fad_name',
          '广告类别'=>'adclassname',
          '发布媒体'=>'fmedianame',
          '所属地区'=>'regionname',
          '发布日期'=>'fissue_date',
          '开始时间'=>'fstarttime',
          '结束时间'=>'fendtime',
          '违法表现代码'=>'fillegal_code',
          '违法表现'=>'fexpressions',
          '违法内容'=>'fillegal',
          '素材地址'=>'favifilename',
        ];
        if($fmedia_class == 3){
        	$outdata['datalie']['素材地址'] = 'fjpgfilename';
        }elseif($mclass == 'net'){
        	$outdata['datalie']['素材地址'] = 'favifilename';
        }

        $outdata['lists'] = $data;
        $ret = A('Api/Function')->outdata_xls($outdata);
        if(!empty($ret['url'])){
			echo $ret['url'];
        }else{
        	echo '暂无数据';
        }
	}

	/*
	* 各地不同版本数据导出接口
	*/
	public function getVersionData(){
		session_write_close();
		ini_set('memory_limit','2048M');
		set_time_limit(120);
		$st = I('st');
		$ed = I('ed');
		$version = I('version');
		$regionid = I('rg')?I('rg'):430000;

		// $version = '病毒';
		// $st = '2020-02-01';
		// $ed = '2020-02-08';
		if(empty($st) || empty($ed) || empty($version)){
			echo '时间传参有误';
			die;
		}

		$tregion_len = $this->get_tregionlevel($regionid);
		if($tregion_len == 1){//国家级
			$wherestr = ' media_region_id ='.$regionid;	
		}elseif($tregion_len == 2){//省级
			$wherestr = ' media_region_id like "'.substr($regionid,0,2).'%"';
		}elseif($tregion_len == 4){//市级
			$wherestr = ' media_region_id like "'.substr($regionid,0,4).'%"';
		}elseif($tregion_len == 6){//县级
			$wherestr = ' media_region_id like "'.substr($regionid,0,6).'%"';
		}

		$st2 = date('Y-m-d',strtotime($st));
		$ed2 = date('Y-m-d',strtotime($ed));
		$edtime = date('H:i:s',strtotime($ed));
		if($edtime == '00:00:00'){
			$ed = date('Y-m-d',strtotime($ed)).' 23:59:59';
		}else{
			if(date('Y-m-d',strtotime($st)) != date('Y-m-d',strtotime($ed))){
				$st2 = $st;
				$ed2 = $ed;
			}
		}

		$sqlstr = '
			select * from (
			(
			SELECT fmedianame,fmediacode,mclass, SUM(acount) acount
			FROM (
			SELECT COUNT(*) AS acount,tmedia.fmedianame,fmediacode,"电视" mclass
			FROM ttvissue e
			INNER JOIN ttvsample ON e.ftvsampleid=ttvsample.fid AND ttvsample.fstate = 1
			INNER JOIN tad ON ttvsample.fadid=tad.fadid AND tad.fadid<>0 AND
			LEFT(tad.fadclasscode,2) <> "23"
			INNER JOIN tadowner ON tad.fadowner = tadowner.fid
			INNER JOIN tadclass ON tad.fadclasscode=tadclass.fcode
			INNER JOIN tmedia ON e.fmediaid=tmedia.fid
			INNER JOIN (
			SELECT DISTINCT(fmediaid)
			FROM tregulatormedia a
			WHERE a.fregulatorcode = "20'.$regionid.'" AND a.fstate = 1 AND a.fisoutmedia = 0 AND fcustomer = "'.$regionid.'") AS ttp ON tmedia.fid=ttp.fmediaid
			WHERE 1=1 AND tmedia.fid = tmedia.main_media_id AND '.$wherestr.' AND ttvsample.fversion LIKE "%'.$version.'%"
			AND e.fstarttime between "'.$st.'" AND "'.$ed.'" 
			GROUP BY tmedia.fid
			) a
			GROUP BY fmedianame)

			union all(
			SELECT fmedianame,fmediacode,mclass, SUM(acount) acount
			FROM (
			SELECT COUNT(*) AS acount,tmedia.fmedianame,fmediacode,"广播" mclass
			FROM tbcissue e
			INNER JOIN tbcsample ON e.fbcsampleid=tbcsample.fid AND tbcsample.fstate = 1
			INNER JOIN tad ON tbcsample.fadid=tad.fadid AND tad.fadid<>0 AND
			LEFT(tad.fadclasscode,2) <> "23"
			INNER JOIN tadowner ON tad.fadowner = tadowner.fid
			INNER JOIN tadclass ON tad.fadclasscode=tadclass.fcode
			INNER JOIN tmedia ON e.fmediaid=tmedia.fid
			INNER JOIN (
			SELECT DISTINCT(fmediaid)
			FROM tregulatormedia a
			WHERE a.fregulatorcode = "20'.$regionid.'" AND a.fstate = 1 AND a.fisoutmedia = 0 AND fcustomer = "'.$regionid.'") AS ttp ON tmedia.fid=ttp.fmediaid
			WHERE 1=1 AND tmedia.fid = tmedia.main_media_id AND '.$wherestr.' AND tbcsample.fversion LIKE "%'.$version.'%"
			AND e.fstarttime between "'.$st.'" AND "'.$ed.'"
			GROUP BY tmedia.fid
			) a
			GROUP BY fmedianame)

			union all(
			SELECT fmedianame,fmediacode,mclass, SUM(acount) acount
			FROM (
				(
			SELECT COUNT(*) AS acount,tmedia.fmedianame,fmediacode,"报纸" mclass
			FROM tpaperissue e
			INNER JOIN tpapersample ON e.fpapersampleid=tpapersample.fpapersampleid AND tpapersample.fstate = 1
			INNER JOIN tad ON tpapersample.fadid=tad.fadid AND tad.fadid<>0 AND
			LEFT(tad.fadclasscode,2) <> "23"
			INNER JOIN tadowner ON tad.fadowner = tadowner.fid
			INNER JOIN tadclass ON tad.fadclasscode=tadclass.fcode
			INNER JOIN tmedia ON e.fmediaid=tmedia.fid
			INNER JOIN (
			SELECT DISTINCT(fmediaid)
			FROM tregulatormedia a
			WHERE a.fregulatorcode = "20'.$regionid.'" AND a.fstate = 1 AND a.fisoutmedia = 0 AND fcustomer = "'.$regionid.'") AS ttp ON tmedia.fid=ttp.fmediaid
			WHERE 1=1 AND tmedia.fid = tmedia.main_media_id AND '.$wherestr.' AND tpapersample.fversion LIKE "%'.$version.'%"
			AND e.fissuedate BETWEEN "'.$st2.'" AND "'.$ed2.'"
			GROUP BY tmedia.fid
				)
			) a
			GROUP BY fmedianame)

			union all(
			SELECT fmedianame,fmediacode,mclass, SUM(acount) acount
			FROM (
				(
			SELECT COUNT(*) AS acount,tmedia.fmedianame,fmediacode,"互联网" mclass
			FROM tnetissueputlog e
			INNER JOIN tnetissue ON e.tid=tnetissue.major_key AND e.is_first_broadcast = 1
			INNER JOIN tad ON tnetissue.fadid=tad.fadid AND tad.fadid<>0 AND
			LEFT(tad.fadclasscode,2) <> "23"
			INNER JOIN tadowner ON tad.fadowner = tadowner.fid
			INNER JOIN tadclass ON tad.fadclasscode=tadclass.fcode
			INNER JOIN tmedia ON e.fmediaid=tmedia.fid
			INNER JOIN (
			SELECT DISTINCT(fmediaid)
			FROM tregulatormedia a
			WHERE a.fregulatorcode = "20'.$regionid.'" AND a.fstate = 1 AND a.fisoutmedia = 0 AND fcustomer = "'.$regionid.'") AS ttp ON tmedia.fid=ttp.fmediaid
			WHERE 1=1 AND tmedia.fid = tmedia.main_media_id AND '.$wherestr.' AND tnetissue.fversion LIKE "%'.$version.'%"
			AND e.net_created_date BETWEEN '.(strtotime($st2)*1000).' AND '.(strtotime($ed2)*1000).'
			GROUP BY tmedia.fid
				)
			) a
			GROUP BY fmedianame)
			) mm
		';
		$data = M()->query($sqlstr);

		$outdata['title'] = '各媒体版本情况汇总'.$st.'至'.$ed;
		$outdata['datalie'] = [
          '序号'=>'key',
          '媒体名称'=>'fmedianame',
          '媒体类型'=>'mclass',
          '广告发布量'=>'acount',
        ];

        $outdata['lists'] = $data;
        $ret = A('Api/Function')->outdata_xls($outdata);
        if(!empty($ret['url'])){
			echo $ret['url'];
        }else{
        	echo '暂无数据';
        }

	}
}