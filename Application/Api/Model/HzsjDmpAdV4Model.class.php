<?php
namespace Api\Model;
use Think\Exception;
use Think\Log;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;

class HzsjDmpAdV4Model{
	
	
	
	
	/*执行单个数据抽取*/
	public function handle_13($forgid,$fdate,$tissue_customer,$adnameSql){
		$mysqlInputNum = 0;
		$openInputNum = 0;
		$addDataList = [];
		$orgInfo = $this->getOrgInfo($forgid);
		$isoutmediaidList = M('tregulatormedia')->cache(true,60)->where(array('fregulatorcode'=>$forgid,'fisoutmedia'=>1))->getField('fmediaid',true);
		
		$org_region_id_rtrim = A('Common/System','Model')->get_region_left($orgInfo['fregionid']);//地区ID去掉末尾的0
		$org_region_id_strlen = strlen($org_region_id_rtrim);//去掉0后还有几位
		
		
		$adowneridList = M('tadowner')->cache(true,600)->where(array('left(fregionid,'.$org_region_id_strlen.')'=>$org_region_id_rtrim))->getField('fid',true);

		$netWhere = array('finputstate'=>2,'is_first_broadcast' => 1  , 'tissue.fissuedate'=>strtotime($fdate));
		
		if(count($adowneridList) > 0){
			$netWhere['tsample.fadid'] = array('exp','in (select fadid from tad where fadowner in ('.implode($adowneridList,',').'))');
		}else{
			$netWhere['tsample.fadid'] = -100000;
		}
		
		
		if (count($isoutmediaidList) > 0 ){
			$netWhere['tissue.fmediaid'] = array('in',$isoutmediaidList);
		}
		
		$netList = M('tnetissueputlog')
                ->alias('tissue')
                #->cache(true,1000)
                ->field('
						tissue.major_key as id,
						tissue.tid as fsampleid,
						tissue.fmediaid,
						tissue.net_platform as issue_platform_type,
						tissue.source_type,
						tsample.fadid,

						tsample.fspokesman,
						tsample.fillegalcontent,
						tissue.net_created_date as fissuedate,
						tsample.fillegaltypecode,
						tsample.fadmanuno,
						tsample.fmanuno,
						tsample.fadapprno,
						tsample.fapprno,
						tsample.fadent,
						tsample.fent,
						tsample.fentzone,
						tsample.fexpressioncodes,
						tsample.thumb_url_true as sam_source_path,
						tsample.net_old_url as landing_img,
						tsample.net_target_url as target_url,
						tsample.net_snapshot as issue_page_url,
						tsample.ftype as issue_ad_type
                ')
                ->join('tnetissue as tsample on tsample.major_key = tissue.tid')
                ->where($netWhere)
				->limit(100)
                ->select();
		
		foreach($netList as $net){
			
			$mediaInfo = $this->getMediaInfo($net['fmediaid']);
			$adownerInfo = $this->getAdownerInfo($net['fadid']);

			if(!$this->is_need($orgInfo,$net['fmediaid'],$net['fadid'])) continue;
			
			
			
			$e_a_data['fmediaclassid'] = 13;//媒介类型
			$e_a_data['fsampleid'] = $net['fsampleid']; //样本id
			$e_a_data['fmediaid'] = $net['fmediaid']; //媒介id
			$e_a_data['issue_platform_type'] = $net['issue_platform_type']; //平台类型
			$e_a_data['fmedianame'] = $mediaInfo['fmedianame'];//媒介名称
			$e_a_data['fregionid'] = $mediaInfo['media_region_id']; //地区id
			$e_a_data['regionid_array'] = implode(A('Common/Region','Model')->get_region_array($mediaInfo['media_region_id']),','); //地区id列表
			$e_a_data['fadclasscode_v2'] =  implode(A('Common/AdClass','Model')->get_adclass_v2_array($adownerInfo['fadclasscode_v2']),',');//商业广告分类
			$e_a_data['fadname'] = $adownerInfo['fadname'];//广告名称
			$e_a_data['fadowner'] = $adownerInfo['fname'];//广告主
			$e_a_data['fbrand'] = $adownerInfo['fbrand']; //品牌
			$e_a_data['fspokesman'] = $net['fspokesman']; //代言人
			$e_a_data['fadclasscode'] = implode(A('Common/AdClass','Model')->get_adclass_array($adownerInfo['fadclasscode']),',');
			
			$e_a_data['fstarttime'] = round($net['fissuedate']/1000); //发布开始时间戳
			$e_a_data['fendtime'] = round($net['fissuedate']/1000); //发布结束时间戳
			$e_a_data['fissuedate'] = strtotime(date("Y-m-d",$net['fissuedate']/1000)); //发布日期
			$e_a_data['flength'] = 0; //发布时长(秒)
			$e_a_data['fillegaltypecode'] = $net['fillegaltypecode']; //违法类型

			$e_a_data['fexpressioncodes'] = $net['fexpressioncodes']; //'违法表现代码，用“;”分隔
			$e_a_data['fillegalcontent'] = strval($net['fillegalcontent']); //'涉嫌违法内容
			$e_a_data['sam_source_path'] = strval($net['sam_source_path']); //'素材路径
			$e_a_data['landing_img'] = strval($net['landing_img']); //'落地页截图
			$e_a_data['target_url'] = strval($net['target_url']); //'落地页地址
			$e_a_data['issue_page_url'] = strval($net['issue_page_url']); //'发布地址截图
			$e_a_data['issue_ad_type'] = strval($net['issue_ad_type']); //'发布类型

			$e_a_data['adowner_regionid'] = $adownerInfo['fregionid']; //'广告主地区id
			$e_a_data['adowner_regionid_array'] = implode(A('Common/Region','Model')->get_region_array($adownerInfo['fregionid']),','); //'广告主地区列表
			
			$e_a_data['fmediaownername'] = $mediaInfo['fmediaownername']; //'媒介机构名称
			$e_a_data['fmediaownerid'] = $mediaInfo['fmediaownerid']; //'媒介机构id
			
			$e_a_data['fcustomer'] = $orgInfo['cr_num']; // '所属客户地域ID',
			$e_a_data['forgid'] = $forgid; //  '所属客户机构ID',
			$e_a_data['last_update_time'] = time();
			$trackerids = M('trackers_record')->cache(true,600)->where(array('tid'=>$net['fsampleid']))->getField('trackerid',true);
			if(count($trackerids) > 0){
				$tracker_names = M('tracker')->cache(true,600)->where(array('trackerid'=>array('in',$trackerids)))->getField('websitename',true);
			}else{
				$tracker_names = [];
			}
			$e_a_data['tracker_names'] = implode(array_unique($tracker_names),' ');
			$e_a_data['trackerids'] = implode($trackerids,',');
			$e_a_data['identify'] = md5($forgid.'_'.$e_a_data['fmediaid'].'_'.$e_a_data['fissuedate'].'_'.$net['id']);
			if($net['fillegaltypecode'] == 30){
				$this->handle_illegal_ad($e_a_data,$forgid);
			}
			$addDataList[] = $e_a_data;
			if(count($addDataList) >= 1000){
				$InputNum = A('Api/HzsjDmpAdV4','Model')->addOpenSearch($addDataList,$tissue_customer);
				$mysqlInputNum += $InputNum['mysqlInputNum'];
				$openInputNum += $InputNum['openInputNum'];
				$addDataList = [];
			}
			
		} 
		if(count($addDataList) > 0){
			$InputNum = A('Api/HzsjDmpAdV4','Model')->addOpenSearch($addDataList,$tissue_customer);
			$mysqlInputNum += $InputNum['mysqlInputNum'];
			$openInputNum += $InputNum['openInputNum'];
			$addDataList = [];
		}
		return array('openInputNum'=>$openInputNum,'mysqlInputNum'=>$mysqlInputNum);;
		
	}
	
	
	
	/*执行单个数据抽取*/
	public function handle_03($forgid,$fdate,$tissue_customer,$adnameSql){
		$mysqlInputNum = 0;
		$openInputNum = 0;
		$addDataList = [];
		$orgInfo = $this->getOrgInfo($forgid);
		$isoutmediaidList = M('tregulatormedia')->cache(true,60)->where(array('fregulatorcode'=>$forgid,'fisoutmedia'=>1))->getField('fmediaid',true);
		
		$org_region_id_rtrim = A('Common/System','Model')->get_region_left($orgInfo['fregionid']);//地区ID去掉末尾的0
		$org_region_id_strlen = strlen($org_region_id_rtrim);//去掉0后还有几位
		
		
		$adowneridList = M('tadowner')->cache(true,600)->where(array('left(fregionid,'.$org_region_id_strlen.')'=>$org_region_id_rtrim))->getField('fid',true);

		$paperWhere = array( 'tissue.fissuedate'=>$fdate);
		
		if(count($adowneridList) > 0){
			$paperWhere['tsample.fadid'] = array('exp','in (select fadid from tad where fadowner in ('.implode($adowneridList,',').'))');
		}else{
			$paperWhere['tsample.fadid'] = -100000;
		}
		
		
		
		if (count($isoutmediaidList) > 0 ){
			$paperWhere['tissue.fmediaid'] = array('in',$isoutmediaidList);
		}
		
		$paperList = M('tpaperissue')
							->alias('tissue')
							#->cache(true,60)
							->field('
									tissue.fpapersampleid as fsampleid,
									tissue.fmediaid,
									tsample.fadid,
									tsample.fspokesman,
									tissue.fissuedate,
									tsample.fillegaltypecode,
									tsample.fadmanuno,
									tsample.fmanuno,
									tsample.fadapprno,
									tsample.fapprno,
									tsample.fadent,
									tsample.fent,
									tsample.fentzone,
									tsample.fexpressioncodes,
									tsample.fillegalcontent,
									tsample.fjpgfilename as sam_source_path
									
									')

							->join('tpapersample as tsample on tsample.fpapersampleid = tissue.fpapersampleid')

							->where($paperWhere)
							->group('tissue.fmediaid,tissue.fissuedate,tsample.fjpgfilename')
							->limit(10)
							->select();	
		
		foreach($paperList as $paper){
			
			$mediaInfo = $this->getMediaInfo($paper['fmediaid']);
			$adownerInfo = $this->getAdownerInfo($paper['fadid']);
			
			if(!$this->is_need($orgInfo,$paper['fmediaid'],$paper['fadid'])) continue;
			
			$e_a_data['fmediaclassid'] = 3;//媒介类型
			$e_a_data['fsampleid'] = $paper['fsampleid']; //样本id
			$e_a_data['fmediaid'] = $paper['fmediaid']; //媒介id
			$e_a_data['fmedianame'] = $mediaInfo['fmedianame'];//媒介名称
			$e_a_data['fregionid'] = $mediaInfo['media_region_id']; //地区id
			$e_a_data['regionid_array'] = implode(A('Common/Region','Model')->get_region_array($mediaInfo['media_region_id']),','); //地区id列表
			$e_a_data['fadclasscode_v2'] =  implode(A('Common/AdClass','Model')->get_adclass_v2_array($adownerInfo['fadclasscode_v2']),',');//商业广告分类
					
			$e_a_data['fadname'] = $adownerInfo['fadname'];//广告名称
			$e_a_data['fadowner'] = $adownerInfo['fname'];//广告主
			$e_a_data['fbrand'] = $adownerInfo['fbrand']; //品牌
			$e_a_data['fspokesman'] = $paper['fspokesman']; //代言人
			$e_a_data['fadclasscode'] = implode(A('Common/AdClass','Model')->get_adclass_array($adownerInfo['fadclasscode']),',');
			$e_a_data['fstarttime'] = 0; //发布开始时间戳
			$e_a_data['fendtime'] = 0; //发布结束时间戳
			$e_a_data['fissuedate'] = strtotime(date('Y-m-d',strtotime($paper['fissuedate']))); //发布日期
			$e_a_data['flength'] = 0; //发布时长(秒)
			$e_a_data['fillegaltypecode'] = $paper['fillegaltypecode']; //违法类型
			$e_a_data['fexpressioncodes'] = $paper['fexpressioncodes']; //'违法表现代码，用“;”分隔
			$e_a_data['fillegalcontent'] = strval($paper['fillegalcontent']); //'涉嫌违法内容

			
			$e_a_data['sam_source_path'] = strval($paper['sam_source_path']); //'素材路径
			
			$e_a_data['adowner_regionid'] = $adownerInfo['fregionid']; //'广告主地区id

			$e_a_data['adowner_regionid_array'] = implode(A('Common/Region','Model')->get_region_array($adownerInfo['fregionid']),','); //'广告主地区列表
			
			$e_a_data['fmediaownername'] = $mediaInfo['fmediaownername']; //'媒介机构名称
			$e_a_data['fmediaownerid'] = $mediaInfo['fmediaownerid']; //'媒介机构id
			
			$e_a_data['fcustomer'] = $orgInfo['cr_num']; // '所属客户地域ID',
			$e_a_data['forgid'] = $forgid; //  '所属客户机构ID',
			$e_a_data['last_update_time'] = time();
			$e_a_data['identify'] = md5($forgid.'_'.$e_a_data['fmediaid'].'_'.$e_a_data['fissuedate'].'_'.$paper['sam_source_path']);
			if($paper['fillegaltypecode'] == 30){
				$this->handle_illegal_ad($e_a_data,$forgid);
			}
			$addDataList[] = $e_a_data;
			if(count($addDataList) >= 1000){
				$InputNum = A('Api/HzsjDmpAdV4','Model')->addOpenSearch($addDataList,$tissue_customer);
				$mysqlInputNum += $InputNum['mysqlInputNum'];
				$openInputNum += $InputNum['openInputNum'];
				$addDataList = [];
			}
			
		} 
		if(count($addDataList) > 0){
			$InputNum = A('Api/HzsjDmpAdV4','Model')->addOpenSearch($addDataList,$tissue_customer);
			$mysqlInputNum += $InputNum['mysqlInputNum'];
			$openInputNum += $InputNum['openInputNum'];
			$addDataList = [];
		}
		return array('openInputNum'=>$openInputNum,'mysqlInputNum'=>$mysqlInputNum);;
		
	}
	
	
	/*执行单个数据抽取*/
	public function handle_01($forgid,$fdate,$tissue_customer,$adnameSql){
		$mysqlInputNum = 0;
		$openInputNum = 0;
		$addDataList = [];
		$orgInfo = $this->getOrgInfo($forgid);
		$isoutmediaidList = M('tregulatormedia')->cache(true,60)->where(array('fregulatorcode'=>$forgid,'fisoutmedia'=>1))->getField('fmediaid',true);
		
		$org_region_id_rtrim = A('Common/System','Model')->get_region_left($orgInfo['fregionid']);//地区ID去掉末尾的0
		$org_region_id_strlen = strlen($org_region_id_rtrim);//去掉0后还有几位
		
		
		$adowneridList = M('tadowner')->cache(true,600)->where(array('left(fregionid,'.$org_region_id_strlen.')'=>$org_region_id_rtrim))->getField('fid',true);

		$dataWhere = array( 'tissue.fissuedate'=>strtotime($fdate));
		
		if(count($adowneridList) > 0){
			$dataWhere['tsample.fadid'] = array('exp','in (select fadid from tad where fadowner in ('.implode($adowneridList,',').'))');
		}else{
			$dataWhere['tsample.fadid'] = -100000;
		}
		
		
		
		if (count($isoutmediaidList) > 0 ){
			$dataWhere['tissue.fmediaid'] = array('in',$isoutmediaidList);
		}
		
		$regionIdList = M('tregion')->cache(true,3600)->where(array('fpid'=>100000))->getField('fid',true);
		$month = date('Ym',strtotime($fdate));

		foreach(['01','02'] as $tb){ #循环电视、广播 两种媒介类型
			if($tb == '01'){
				$media_tab = 'tv';
			}elseif($tb == '02'){
				$media_tab = 'bc';
			}else{
				continue;
			}
			$sam_table_name = 't'.$media_tab.'sample'; #样本表名称
			foreach($regionIdList as $regionId){ # 循环地区前缀

				$table_name = 't'.$media_tab.'issue_'.$month.'_'.substr($regionId,0,2);
				
				try{//
					$data2List = M($table_name)
								->alias('tissue')
								#->cache(true,1200)
								->field('
										tissue.fsampleid,
										tissue.fmediaid,
										tissue.fstarttime,
										tissue.fendtime,
										
										tsample.fspokesman,
										tsample.fadid,
										tissue.fissuedate,
										tsample.fillegaltypecode,
										tsample.fadmanuno,
										tsample.fmanuno,
										tsample.fadapprno,
										tsample.fapprno,
										tsample.fadent,
										tsample.fent,
										tsample.fentzone,
										tsample.fexpressioncodes,
										tsample.fillegalcontent,
										tsample.favifilename as sam_source_path
										
										
										')

								->join($sam_table_name.' as tsample on tsample.fid = tissue.fsampleid')
								
								->where($dataWhere)
								->group('tissue.fmediaid,tissue.fstarttime')
								->select();	

				}catch(Exception $error) {//
					$data2List = array();
				}
				
				foreach($data2List as $data){
					$mediaInfo = $this->getMediaInfo($data['fmediaid']);
					$adownerInfo = $this->getAdownerInfo($data['fadid']);
					
					if(!$this->is_need($orgInfo,$data['fmediaid'],$data['fadid'])) continue;
					
					$e_a_data['fmediaclassid'] = intval($tb);//媒介类型
					$e_a_data['fsampleid'] = $data['fsampleid']; //样本id
					$e_a_data['fmediaid'] = $data['fmediaid']; //媒介id
					$e_a_data['fmedianame'] = $mediaInfo['fmedianame'];//媒介名称
					$e_a_data['fregionid'] = $mediaInfo['media_region_id']; //地区id
					$e_a_data['regionid_array'] = implode(A('Common/Region','Model')->get_region_array($mediaInfo['media_region_id']),','); //地区id列表
					$e_a_data['fadclasscode_v2'] =  implode(A('Common/AdClass','Model')->get_adclass_v2_array($adownerInfo['fadclasscode_v2']),',');//商业广告分类
							
					$e_a_data['fadname'] = $adownerInfo['fadname'];//广告名称
					$e_a_data['fadowner'] = $adownerInfo['fname'];//广告主
					$e_a_data['fbrand'] = $adownerInfo['fbrand']; //品牌
					$e_a_data['fspokesman'] = $data['fspokesman']; //代言人
					$e_a_data['fadclasscode'] = implode(A('Common/AdClass','Model')->get_adclass_array($adownerInfo['fadclasscode']),',');
					$e_a_data['fstarttime'] = $data['fstarttime']; //发布开始时间戳
					$e_a_data['fendtime'] = $data['fendtime']; //发布结束时间戳
					$e_a_data['fissuedate'] = strtotime(date('Y-m-d',$data['fissuedate'])); //发布日期
					$e_a_data['flength'] = $data['fendtime'] - $data['fstarttime']; //发布时长(秒)
					$e_a_data['fillegaltypecode'] = $data['fillegaltypecode']; //违法类型
					$e_a_data['fexpressioncodes'] = $data['fexpressioncodes']; //'违法表现代码，用“;”分隔
					$e_a_data['fillegalcontent'] = strval($data['fillegalcontent']); //'涉嫌违法内容

					
					$e_a_data['sam_source_path'] = strval($data['sam_source_path']); //'素材路径
					
					$e_a_data['adowner_regionid'] = $adownerInfo['fregionid']; //'广告主地区id

					$e_a_data['adowner_regionid_array'] = implode(A('Common/Region','Model')->get_region_array($adownerInfo['fregionid']),','); //'广告主地区列表
					
					$e_a_data['fmediaownername'] = $mediaInfo['fmediaownername']; //'媒介机构名称
					$e_a_data['fmediaownerid'] = $mediaInfo['fmediaownerid']; //'媒介机构id
					
					$e_a_data['fcustomer'] = $orgInfo['cr_num']; // '所属客户地域ID',
					$e_a_data['forgid'] = $forgid; //  '所属客户机构ID',
					$e_a_data['last_update_time'] = time();
					$e_a_data['identify'] = md5($forgid.'_'.$e_a_data['fmediaid'].'_'.$e_a_data['fissuedate'].'_'.$e_a_data['fstarttime']);

					$addDataList[] = $e_a_data;
					
					if($data['fillegaltypecode'] == 30){
						$this->handle_illegal_ad($e_a_data,$forgid);
					}
					if(count($addDataList) >= 1000){
						$InputNum = A('Api/HzsjDmpAdV4','Model')->addOpenSearch($addDataList,$tissue_customer);
						$mysqlInputNum += $InputNum['mysqlInputNum'];
						$openInputNum += $InputNum['openInputNum'];
						$addDataList = [];
					}
				}
			}
		}
		if(count($addDataList) > 0){
			$InputNum = A('Api/HzsjDmpAdV4','Model')->addOpenSearch($addDataList,$tissue_customer);
			$mysqlInputNum += $InputNum['mysqlInputNum'];
			$openInputNum += $InputNum['openInputNum'];
			$addDataList = [];
		}
		return array('openInputNum'=>$openInputNum,'mysqlInputNum'=>$mysqlInputNum);;
	}
	
	
	#判断数据是否需要排除
	private function is_need($orgInfo,$mediaId,$adId){
		

		$mediaInfo = $this->getMediaInfo($mediaId);
		$adownerInfo = $this->getAdownerInfo($adId);
		
		$org_region_id_rtrim = A('Common/System','Model')->get_region_left($orgInfo['fregionid']);//地区ID去掉末尾的0
		$org_region_id_strlen = strlen($org_region_id_rtrim);//去掉0后还有几位
		
		
		if(in_array(strval($mediaInfo['media_region_id']),array('0','990000'))) return false;
		
		if ($org_region_id_rtrim != substr($adownerInfo['fregionid'],0,$org_region_id_strlen)) return false; //判断本机构的地区前缀是否等于广告主地区前缀
			
		if (!$mediaInfo || !$adownerInfo) return false;
		$tregulatormedia = M('tregulatormedia')->cache(true,60)->where(array('fregulatorcode'=>$orgInfo['fid'],'fisoutmedia'=>0))->getField('fmediaid',true);
		if(in_array($mediaId,$tregulatormedia)) return false; #判断是否自己的关联媒体，如果是自己的关联媒体就退出循环
		
		return true;

	}
	
	
	
	
	#查询媒介信息
	private function getMediaInfo($mediaId){
		$mediaInfo = M('tmedia')
								->cache(true,600)
								->field('tmedia.fid,media_region_id,fmedianame,tmediaowner.fname as fmediaownername,tmedia.fmediaownerid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>$mediaId))
								->find(); #查询媒介信息
		return $mediaInfo;
	}
	
	#查询广告主信息
	private function getAdownerInfo($adId){
		$adownerInfo = M('tadowner')
								->cache(true,600)
								->field('tad.fadclasscode,tad.fadclasscode_v2,tad.fbrand,tadowner.fid,tadowner.fregionid,tadowner.fname,tad.fadname,tad.fadid')
								->join('tad on tad.fadowner = tadowner.fid')
								->where(array('tad.fadid'=>$adId))
								->find(); # 查询广告主信息和广告信息
		return $adownerInfo;
	}
	
	#查询机构信息
	private function getOrgInfo($orgId){
		$orgInfo = M('tregulator')->cache(true,600)->field('fid,fcode,fregionid')->where(array('fid'=>$orgId))->find();
		$orgInfo['cr_num'] = M('customer')->where(array('cr_orgid'=>$orgId))->getField('cr_num');
		return $orgInfo;
	}
	
	
	
	/*处理违法广告*/
	private function handle_illegal_ad($dataValue,$forgid=0){
		
		$fregion_id = $dataValue['fcustomer'];
		$dataValue['fadclasscode'] = explode(',',$dataValue['fadclasscode']);
		$mediaInfo = array('media_class'=>$dataValue['fmediaclassid'],'fmediaownerid'=>$dataValue['fmediaownerid'],'fid'=>$dataValue['fmediaid']);
		
		$tbn_illegal_ad_id = A('Api/CustomerIssue','Model')->add_illegal_ad($dataValue,$fregion_id,'new_2',$forgid);//添加并获取违法广告id
	
		$illegal_issue_id = A('Api/CustomerIssue','Model')->add_illegal_issue(
														$tbn_illegal_ad_id,
														$mediaInfo,
														$dataValue['fissuedate'],
														$dataValue['fstarttime'],
														$dataValue['fendtime'],
														md5($tbn_illegal_ad_id.'_'.$dataValue['identify'])
													);

	
	}
	
	
	/*删除某天数据*/
    public function delete_issue_data($forgid,$fdate){
		
		$openDelNum = 0;
		$sqlDelNum = 0;
		$sql2DelNum = 0;
		$illegalIssueDelNum = 0;
		$orgInfo = M('tregulator')->cache(true,600)->where(array('fid'=>$forgid))->find();
		$fcustomer = $orgInfo['fregionid'];
		
		$OpenSearchClient = A('Common/OpenSearch','Model')->client();
		$searchClient = new SearchClient($OpenSearchClient);
		
		
		$params = new SearchParamsBuilder();
		$params->setHits(500);//返回数量
		$params->setAppName('hzsj_dmp_ad_v4');
		$setQuery = A('Common/OpenSearch','Model')->setQuery(array('forgid'=>$forgid,'fissuedate'=>strtotime($fdate)));

		$params->setQuery($setQuery);//搜索
		$params->setFormat("json");//数据返回json格式
		$params->setFetchFields(array('identify'));//设置需返回哪些字段
		$params->setScrollExpire('3m');//设置下次scroll发送请求超时时间，用于scroll方法查询，此处为第一次访问，用于获取scrollId
		
			
			
		$ret = $searchClient->execute($params->build());//执行查询并返回信息
		$result = json_decode($ret->result,true);//第一次返回的数据，主要获取总数和scrollID
		$viewtotal = $result['result']['viewtotal'];//数据总量，也是剩余数量
		
		
		while($viewtotal > 0){//循环用scrollid查询数据
			$params->setScrollId($result['result']['scroll_id']);//通过上面第一次查询返回的 scrollId ，作为查询参数获取数据
			$ret = $searchClient->execute($params->build());//执行查询并返回信息
			$result = json_decode($ret->result,true);//
			$viewtotal -= $result['result']['num'];//计算剩余数据
			$delete_open_datas = array();//需要删除的数据初始化
			foreach($result['result']['items'] as $result_value){//循环需要删除的数据
				$delete_open_datas[] = array('cmd'=>'delete','fields'=>array('identify'=>$result_value['identify']));//组装需要删除的数据
			}
			$delete_state = A('Common/OpenSearch','Model')->push('ad_issue',$delete_open_datas,'hzsj_dmp_ad_v4');  //删除数据
			$delete_state = json_decode($delete_state->result,true);
			if($delete_state['status'] == 'OK') $openDelNum += COUNT($delete_open_datas);
		}
		
		$del_sql_state = M('hzsj_dmp_ad_v4')->where(array('forgid'=>$forgid,'fissuedate'=>strtotime($fdate)))->delete();//删除MySQL数据库的数据
		#$del_sql_state_2 = M('tissue_customer')->where(array('fislook'=>1,'fcustomer'=>$forgid,'fissuedate'=>$fdate))->delete();//删除MySQL2数据库的数据
		
		
		$sqlDelNum += $del_sql_state;
		$sql2DelNum += $del_sql_state_2;
		
		
		
		$sumDelNum = M('tbn_ad_summary_day_v4')->where(array('forgid'=>$forgid,'fdate'=>$fdate))->delete();//删除汇总表里面该媒体的这一天的数据
		$del_illegal_issue_id_list = M('tbn_illegal_ad_issue')
															
															->join('tbn_illegal_ad on tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id')
															->where(array(
																			'tbn_illegal_ad.fcustomer'=>$fcustomer,
																			'tbn_illegal_ad_issue.fissue_date'=>$fdate,
																			'tbn_illegal_ad.identify'=>'new_2'
																			
																			))
															->getField('tbn_illegal_ad_issue.fid',true);
		if($del_illegal_issue_id_list && $fcustomer != 100000){
			$illegalIssueDelNum = M('tbn_illegal_ad_issue')->where(array('fid'=>array('in',$del_illegal_issue_id_list)))->delete();//删除这一天的违法发布记录 */
		}
		
		return array('openDelNum'=>$openDelNum,'sqlDelNum'=>$sqlDelNum,'sql2DelNum'=>$sql2DelNum,'illegalIssueDelNum'=>$illegalIssueDelNum,'sumDelNum'=>$sumDelNum);
			
    }
	
	
	#汇总数据计算
	public function tbn_ad_summary_day_v4($dataList){
		
		$maaList = array();
		foreach($dataList as $data){
			
			$left2_adclasscode = strval(substr(explode(',',$data['fadclasscode'])[0],0,2));
			$maaKey = $data['fmediaid'].'__'.$data['adowner_regionid'].'__'.$left2_adclasscode;
			
			
			$maaList[$maaKey]['fmediaid'] = $data['fmediaid'];
			$maaList[$maaKey]['fmediaownerid'] = $data['fmediaownerid'];
			$maaList[$maaKey]['fdate'] = date('Y-m-d',$data['fissuedate']);

			$maaList[$maaKey]['fmedia_class_code'] = $data['fmediaclassid'];
			$maaList[$maaKey]['fregionid'] = $data['adowner_regionid'];
			$maaList[$maaKey]['fad_class_code'] = $left2_adclasscode;
			$maaList[$maaKey]['fcustomer'] = $data['fcustomer'];
			$maaList[$maaKey]['forgid'] = $data['forgid'];
			
			
			$maaList[$maaKey]['fad_times_long_ad'] += 1;
			$maaList[$maaKey]['fad_play_len_long_ad'] += $data['flength'];
			$maaList[$maaKey]['fsam_list_long_ad'][] = $data['fsampleid'];
			
			
			
			
			
			if($data['flength'] < 600){
				$maaList[$maaKey]['fad_times'] += 1;
				$maaList[$maaKey]['fad_play_len'] += $data['flength'];
				$maaList[$maaKey]['fsam_list'][] = $data['fsampleid'];
				
			}else{
				$maaList[$maaKey]['fad_times'] += 0;
				$maaList[$maaKey]['fad_play_len'] += 0;
				$maaList[$maaKey]['fsam_list'][] = '';
			}
			

		}
		$addAllData = [];
		$fad_times_long_ad = 0;
		foreach($maaList as $maaKey => $maa){
			$fad_times_long_ad += $maa['fad_times_long_ad'];
			#$maaKeyL = explode('__',$maaKey);
			$maa['fsam_list'] = implode(',',array_filter(array_unique($maa['fsam_list'])));
			$maa['fsam_list_long_ad'] = implode(',',array_filter(array_unique($maa['fsam_list_long_ad'])));
			if($maa['fmedia_class_code'] < 10) $maa['fmedia_class_code'] = '0'.$maa['fmedia_class_code'];

			$addAllData[] = $maa; 
			#M('tbn_ad_summary_day_v4')->add($maa,[],true);
			
		}
		
		$rr = M('tbn_ad_summary_day_v4')->addAll($addAllData,[],true);


		return $fad_times_long_ad;
		
		
	}
	#添加opensearch 数据
	public function addOpenSearch($addDataList,$tissue_customer=1){
		
		$openDatas = [];
		$openInputNum = 0;
		$mysqlInputNum = 0;
		foreach($addDataList as $addData){
			
			if($tissue_customer){
				if($addData['fmediaclassid'] == 1){
					$fuuid = M('ttvsample')->cache(true,600)->where(array('fid'=>$addData['fsampleid']))->getField('uuid');
				}elseif($addData['fmediaclassid'] == 2){
					$fuuid = M('tbcsample')->cache(true,600)->where(array('fid'=>$addData['fsampleid']))->getField('uuid');
				}elseif($addData['fmediaclassid'] == 3){
					$fuuid = M('tpapersample')->cache(true,600)->where(array('fpapersampleid'=>$addData['fsampleid']))->getField('uuid');
				}elseif($addData['fmediaclassid'] == 13){
					$fuuid = M('tnetissue')->cache(true,600)->where(array('major_key'=>$addData['fsampleid']))->getField('identify_code');
				}
				
				$add2Data = array();
				$adInfo = M('tad')->cache(true,60)->where(array('fadname'=>$addData['fadname'],'fstate'=>['neq',-1]))->find();
				$add2Data['fmediaid'] = $addData['fmediaid'];
				$add2Data['fmediaclassid'] = $addData['fmediaclassid'];
				
				$add2Data['fsampleid'] = $addData['fsampleid'];
				$add2Data['fissuedate'] = date('Y-m-d',$addData['fissuedate']);
				$add2Data['fstarttime'] = date('Y-m-d H:i:s',$addData['fstarttime']);
				$add2Data['fendtime'] = date('Y-m-d H:i:s',$addData['fendtime']);
				$add2Data['fspecifications'] = $addData['flength'];
				$add2Data['fadid'] = $adInfo['fadid'];
				$add2Data['fadclasscode'] = $adInfo['fadclasscode'];
				$add2Data['fadownerid'] = $adInfo['fadowner'];
				$add2Data['fillegaltypecode'] = $addData['fillegaltypecode'];
				$add2Data['fexpressioncodes'] = $addData['fexpressioncodes'];
				$add2Data['fexpressions'] = '';
				$add2Data['fillegalcontent'] = $addData['fillegalcontent'];
				$add2Data['fconfirmations'] = '';
				$add2Data['fregulatorname'] = '';
				$add2Data['fuuid'] = $fuuid;
				$add2Data['tracker_names'] = '';
				$add2Data['sam_source_path'] = $addData['sam_source_path'];
				$add2Data['operator_name'] = '';
				$add2Data['issue_platform_type'] = '';
				$add2Data['landing_img'] = '';
				$add2Data['issue_page_url'] = '';
				$add2Data['target_url'] = '';
				$add2Data['fcustomer'] = $addData['forgid'];
				$add2Data['fidentify'] = $addData['identify'];
				$add2Data['create_time'] = date('Y-m-d H:i:s');
				$add2Data['fhandlestate'] = M('tsample_wine')->cache(true,600)->where(array('fuuid'=>$fuuid))->getField('fhandlestate');
				if(!$add2Data['fhandlestate']) $add2Data['fhandlestate'] = '待确认';
				
				
				
				try{//
					M('tissue_customer')->add($add2Data);	
				}catch(Exception $e) { 
					$eC = M('tissue_customer')
									->where(array('fidentify'=>$addData['identify']))
									->save(array(
													'fadownerid'=>$adInfo['fadowner'],
													'fadid'=>$adInfo['fadid'],
													
												));
					#file_put_contents('LOG/tissue_customer',M('tissue_customer')->getLastSql().';	'.$addData['fadname'].'	'.$eC."\n",FILE_APPEND);
												
				} 
			}

			
			$addData['fadclasscode'] = explode(',',$addData['fadclasscode']);
			$addData['regionid_array'] = explode(',',$addData['regionid_array']);
			$addData['fadclasscode_v2'] = explode(',',$addData['fadclasscode_v2']);
			$addData['adowner_regionid_array'] = explode(',',$addData['adowner_regionid_array']);
			$addData['trackerids'] = array_filter(explode(',',$addData['trackerids']));

			$openDatas[] = array('cmd'=>'add','fields'=>$addData);
			
			if(count($openDatas) >= 600){
				$inputState = A('Common/OpenSearch','Model')->push('ad_issue',$openDatas,'hzsj_dmp_ad_v4');
				$inputState = json_decode($inputState->result,true);
				if($inputState['status'] == 'OK') $openInputNum += COUNT($openDatas);
				$openDatas = array();
			}
			
		}
		
		if(count($openDatas) > 0){
			$inputState = A('Common/OpenSearch','Model')->push('ad_issue',$openDatas,'hzsj_dmp_ad_v4');
			$inputState = json_decode($inputState->result,true);
			if($inputState['status'] == 'OK') $openInputNum += COUNT($openDatas);
			$openDatas = array();
		}
		
		$in_m = M('hzsj_dmp_ad_v4')->addAll($addDataList,[],true);//添加数据到MySQL
		if($in_m) $mysqlInputNum += count($addDataList);
		
		return array('openInputNum'=>$openInputNum,'mysqlInputNum'=>$mysqlInputNum);
		
	}
	

	
	
	
	
	
	
	
	
}