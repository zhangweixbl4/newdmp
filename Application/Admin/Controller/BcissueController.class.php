<?php
//广播广告发行控制器
namespace Admin\Controller;
use Think\Controller;
use Think\Exception;


class BcissueController extends BaseController {

    public function index($data = []){
		session_write_close();
    	if(A('Admin/Authority','Model')->authority('200161') === 0){
			exit('没有权限');
		}

		if(!empty($data)){
			$_GET = $data;
		}
		
		if(I('region_id') != '') $region_id = I('region_id');
		$outtype = I('outtype');//导出类型
		if(!empty($outtype)){
			$p = 1;
			$pp = 999999;
			$addFields = '
				,tadowner.fname as fadownername
				,left(tmedia.fmediaclassid,2) mclass
				,tbcsample.favifilename ysscurl
				,tbcsample.fexpressions
				,tbcsample.fillegalcontent
				,tbcsample.fexpressioncodes
				,from_unixtime(tbcissue.fissuedate,"%Y-%m-%d") issuedate
				,from_unixtime(tbcissue.fstarttime,"%Y-%m-%d %H:%i:%s") starttime
				,from_unixtime(tbcissue.fendtime,"%Y-%m-%d %H:%i:%s") endtime
			';
		}else{
			$p = I('p',1);//当前第几页
			$pp = I('pageSize', 10);//每页显示多少记录
		}
		$keyword = I('keyword');//搜索关键词
		$fadname = I('fadname');// 广告名称
		$fversion = I('fversion');// 版本描述
		$fmediaid  = I('fmediaid');// 媒体ID
		$fmedianame = I('fmedianame');
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID
		$fstarttime_s = strtotime(I('fstarttime_s'));// 发布日期

		$fstarttime_e = strtotime(I('fstarttime_e'));// 发布日期

		$fcreator = I('fcreator');//创建人
		$sample_id = I('sample_id');
		$merge_media = I('merge_media');//是否合并媒介
		$adclass_code = I('adclass_code');//广告类别

		$medialabel = I('medialabel');//媒介标签
		$this_region_id = I('this_region_id');//是否只查询本级地区

		
		$provinceId = '10';

		$where = array();//查询条件
		if(intval($sample_id) > 0){
			$where['tbcissue.fsampleid'] = $sample_id;
		}

		if($region_id > 0 && $fmedianame == ''){//判断是否传入地区查询条件
			$provinceId = substr($region_id,0,2);
			if($this_region_id == 'true'){//判断是否只查询本级
				$where['tbcissue.fregionid'] = $region_id;//地区搜索条件
			}elseif($region_id != 100000){
				$region_id_rtrim = A('Common/System','Model')->get_region_left($region_id);//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tbcissue.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}

		}


		if($fcreator != ''){
			$where['tbcissue.fcreator'] = array('like','%'.$fcreator.'%');
		}
		if($keyword != ''){

		} 
	
		if($fadname != '' && $adclass_code != '' && $adclass_code != '0'){//既搜索广告名称，又搜索广告分类
			
			$adclass_code_strlen = strlen($adclass_code);//获取code长度
			$where['tbcsample.fadid'] = array('exp',"in (
															select fadid 
															from tad 
															where left(tad.fadclasscode,".$adclass_code_strlen.") = '".$adclass_code."'
															and fadname like '%".$fadname."%'
														)");//广告id
			
		}else{
			if($fadname != ''){//搜索广告名
				$where['tbcsample.fadid'] = array('exp',"in (select fadid from tad where fadname like '%".$fadname."%')");//广告名称
			}
			if($adclass_code != '' && $adclass_code != '0'){//搜索广告分类
				
				$adclass_code_strlen = strlen($adclass_code);//获取code长度
				$where['tbcsample.fadid'] = array('exp',"in (select fadid from tad where left(tad.fadclasscode,".$adclass_code_strlen.") = '".$adclass_code."')");//广告id
			}
			
		}
		if($fversion != ''){
			$where['tbcsample.fversion'] = array('like','%'.$fversion.'%');//版本描述
		}
		
		if($medialabel != ''){
			$mediaIdList = M('tmedialabel')
										->cache(true,60)
										->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
										->where(array('tlabel.flabel'=>$medialabel))
										->getField('fmediaid',true);
			if($mediaIdList){
				$where['tbcissue.fmediaid'] = array('in',$mediaIdList);	
			}else{
				$where['tbcissue.fmediaid'] = 0;
			}							
		}
		
		if($fmediaid != ''){//判断是否查询媒介
		
			$mediaInfo = M('tmedia')->cache(true,600)
									->field('tmedia.fid,fregionid')
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->where(array('tmedia.fid'=>$fmediaid))
									->find();//查询媒介信息，用于按媒介查询
			$provinceId = substr($mediaInfo['fregionid'],0,2);						
			$where['tbcissue.fmediaid'] = $fmediaid;//媒体ID
			$_GET['region_fullname'] = '全国';
			$_GET['region_id'] = 100000;

		}elseif($fmedianame != ''){

			$mediaInfo = M('tmedia')->cache(true,600)
									->field('tmedia.fid,fregionid')
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->where(array('tmedia.fmedianame'=>$fmedianame))
									->find();//查询媒介信息，用于按媒介查询
			$provinceId = substr($mediaInfo['fregionid'],0,2);						
			$where['tbcissue.fmediaid'] = $mediaInfo['fid'];//媒体ID
			$_GET['region_fullname'] = '全国';
			$_GET['region_id'] = 100000;
		}
		
		
		
		 
		if(is_array($fillegaltypecode)){
			$where['tbcsample.fillegaltypecode'] = array('in',$fillegaltypecode);//违法类型代码
		}


		$where['tbcissue.fissuedate'] = array('between',$fstarttime_s.','.$fstarttime_e);//查询时间段
		

		
		$issue_table_name = 'tbcissue_' . date('Ym',$fstarttime_s) . '_' . $provinceId ;//发布表的名称
	
		try{//尝试查询符合条件的记录总数
			$count = M($issue_table_name)
				->alias('tbcissue')
				->cache(true,60)
				->join('tbcsample on tbcsample.fid = tbcissue.fsampleid')
				->join('tad on tad.fadid = tbcsample.fadid')
				->join('tadclass on tad.fadclasscode = tadclass.fcode')
				->join('tadowner on tad.fadowner = tadowner.fid')
				->join('tmedia on tmedia.fid = tbcissue.fmediaid')
				->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
				->join('tillegaltype on tillegaltype.fcode = tbcsample.fillegaltypecode')
				->where($where) ->count();// 查询满足要求的总记录数
		}catch(Exception $error) { //如果创建失败
			$count = 0;
		} 
			
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数

		if($count > 0){
			$bcissueList = M($issue_table_name)
				->alias('tbcissue')
				->cache(true,60)
				->field('
					tbcissue.*,
					tad.fadname,
					tbcsample.fversion,
					tbcsample.favifilename,
					"'. $issue_table_name .'" as table_name,
					tmedia.fmedianame,
					tillegaltype.fillegaltype,
					tadclass.ffullname as fadclassfullname
				'.$addFields)
				->join('tbcsample on tbcsample.fid = tbcissue.fsampleid')
				->join('tad on tad.fadid = tbcsample.fadid')
				->join('tadclass on tad.fadclasscode = tadclass.fcode')
				->join('tadowner on tad.fadowner = tadowner.fid')
				->join('tmedia on tmedia.fid = tbcissue.fmediaid')
				->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
				->join('tillegaltype on tillegaltype.fcode = tbcsample.fillegaltypecode')
				->where($where)
				->order('tbcissue.fissuedate desc , tbcissue.fstarttime desc')
				->limit($Page->firstRow.','.$Page->listRows)->select();//查询广告样本列表
		}	

		//数据导出
		if(!empty($outtype)){
			if(empty($bcissueList)){
				$this->ajaxReturn(array('code'=>1,'msg'=>'生成失败，暂无数据'));
			}
	        $ret = A('Issue')->outIssueXls($bcissueList,'02');
	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}

		$bcissueList = list_k($bcissueList,$p,$pp);//为列表加上序号
		
		
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型

		$this->assign('bcissueList',$bcissueList);//广告发布列表
		$this->assign('illegaltype',$illegaltype);//违法类型
		$this->assign('page',$Page->show());//分页输出

	}

	
	
	/*广告发布详情*/
	public function ajax_bcissue_details(){
		
		$issueid = I('issueid');//获取发布ID
		$table_name = I('table_name');
		$bcissueDetails = M($table_name)
										->alias('tbcissue')
										->field(
												'tbcissue.*,
												tad.fadname,
												tbcsample.fversion,
												tbcsample.fillegalcontent,
												tbcsample.fexpressioncodes,
												tbcsample.fexpressions,
												tbcsample.fadmanuno,
												tbcsample.fmanuno,
												tbcsample.fadapprno,
												tbcsample.fapprno,
												tbcsample.fadent,
												tbcsample.fent,
												tbcsample.fentzone,
												
												tmedia.fmedianame,
												tillegaltype.fillegaltype,
												tadclass.ffullname as adclass_fullname
												')
										->join('tbcsample on tbcsample.fid = tbcissue.fsampleid')
										->join('tad on tad.fadid = tbcsample.fadid')
										->join('tadclass on tadclass.fcode = tad.fadclasscode')
										->join('tadowner on tadowner.fid = tad.fadowner')
										->join('tillegaltype on tillegaltype.fcode = tbcsample.fillegaltypecode')
										->join('tmedia on tmedia.fid = tbcissue.fmediaid')
										->where(array('tbcissue.fid'=>$issueid))
										->find();//查询样本详情

		$bcissueDetails['fstarttime'] = date('Y-m-d H:i:s',$bcissueDetails['fstarttime']);
		$bcissueDetails['fendtime'] = date('Y-m-d H:i:s',$bcissueDetails['fendtime']);
		
		$source_info = S('issue_m3u8_'.$table_name.'_'.$issueid);
		$source_url = $source_info['source_url'];
		$ad_part = array(
							'source_url'=>strval($source_url),

							);

		
		
		$this->ajaxReturn(array('code'=>0,'bcissueDetails'=>$bcissueDetails,'ad_part'=>$ad_part));
	}
	

	
	/*删除发布记录*/
	public function del_issue(){
		if(A('Admin/Authority','Model')->authority('200162') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));
		}
		M()->startTrans();//开启事务方法
		$issueid = I('issueid');//获取发布ID
		$table_name = I('table_name');
		
		
		$where['fid'] = $issueid;
		
		$issueInfo = M($table_name)->where($where)->find();
		

		$del_y_issue = M('tbcissue')->where(array('fmediaid'=>$issueInfo['fmediaid'],'fstarttime'=>date('Y-m-d H:i:s',$issueInfo['fstarttime'])))->delete();
		
	
		$rr = M($table_name)->where($where)->delete();
		M()->commit();//事务回滚方法


		if($rr){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功','del_y_issue'=>$del_y_issue));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败'));
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}