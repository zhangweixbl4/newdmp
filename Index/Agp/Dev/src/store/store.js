import Vue from 'vue'
import Vuex from 'vuex'
import ueditorConfig from './module/ueditorConfig'
import month from './module/month'
import mutations from './mutations'
import actions from './actions'
import dayjs from 'dayjs'
import getters from './getters'
import {
  levelchild,
  clueFrom,
  clueFrom2,
  fregulatorlevel,
  propsOptions,
  areaOptions,
  czfs,
  selectData,
  sfbj,
  rwzt,
  xsly,
  gzlx,
  wfcd,
  yjlx,
  czzt,
  timeDate,
  xsDealType,
  xsDealType2,
  seeStatus
} from './module/options'
import {
  pntype,
  pntype2,
  pnfiletype,
  pnfiletype2
} from './module/report'
import {
  netadtype,
  allmediatype,
  typeMap
} from './module/mediatype'
import {
  Gtime,
  Adata,
  NJadType
} from './module/condition'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    firstGetTask: true,
    userInfo: {
      fregulatorlevel: -1,
      thirdlink: []
    },
    showIncludeSub: true,
    bloading: false,
    leagalOptions: [],
    adClassOptions: [],
    allAdClass: [{
      children: []
    }],
    IllegalList: [],
    allArea: [{
      children: []
    }],
    areaList: [{
      children: []
    }],
    allArea2: [{
      fid: ''
    }],
    areaArr: [],
    GList: [],
    GData: [],
    wfqdarea: [],
    wfqdarea2: [],
    xinxiTree: [],
    xinxiList: [],
    isG: false,
    fregulatorlevel: fregulatorlevel,
    propsOptions: propsOptions,
    areaOptions: areaOptions,
    mediaType: [],
    mediaType3: [],
    mediaType5: [],
    netadtype: netadtype,
    allmediatype: allmediatype,
    typeMap: typeMap,
    levelchild: levelchild,
    clueFrom: clueFrom,
    clueFrom2: clueFrom2,
    pntype: pntype,
    pntype2: pntype2,
    pnfiletype: pnfiletype,
    pnfiletype2: pnfiletype2,
    month: month,
    czfs: czfs,
    timeDate: timeDate,
    selectData: selectData,
    menudata: [],
    menu: [{
      menu_id: '',
      menu_url: "",
      list: []
    }],
    cmenu: '',
    Gsearch: {
      years: '0',
      timetypes: 30,
      timeval: new Date().getMonth() + 1
    },
    Gtime: Gtime,
    Adata: Adata,
    NJadType: NJadType,
    Gtitle: '',
    loginwz: '',
    logowz: '',
    dbData: [],
    ActiveTab:{},
    tdata: {},
    fjxsgl: '',
    mediaArea: [{
      fid: ''
    }],
    selection: {
      year: dayjs().subtract(1, 'months').startOf('month').format('YYYY'),
      times_table: 'tbn_ad_summary_month',
      table_condition: dayjs().subtract(1, 'months').startOf('month').format('MM-DD'),
      is_show_longad: '1',
      is_show_fsend: '2',
      is_include_sub: '1',
      datatype: '1',
    },
    _selection: {
      is_show_longad: '1',
      is_show_fsend: '2',
      is_include_sub: '1',
      datatype: '1',
    },
    FJisstime: [],
    Gisstime: [],
    ccorgData: {},
    FJiscontain: '',
    havenet: false,
    ueditorConfig: ueditorConfig,
    fwidth: {
      width: '99%'
    },
    isgj: '',
    sfbj: sfbj,
    rwzt: rwzt,
    xsly: xsly,
    xsDealType: xsDealType,
    xsDealType2: xsDealType2,
    seeStatus: seeStatus,
    gzlx: gzlx,
    wfcd: wfcd,
    yjlx: yjlx,
    czzt: czzt,
    // logo图片
    loginurl: '',
    loginurl2: '',
    showError: false,
    safePWD: true, //密码安全登记符合要求
    trueNUM: true, //正确的手机号
    GTableHeight: null,
    activeMenu: {},

    userOptions: {},
    Customer: 0
  },
  mutations,
  actions,
  getters
})

export default store
