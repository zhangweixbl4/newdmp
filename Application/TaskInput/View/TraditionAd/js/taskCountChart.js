var taskCountChart = {
	template: `
		<div v-loading="loading">
			<el-row>
				<el-col :span="9">
					<div>
						<el-radio-group v-model="activeTag" @change='changeTag'>
							<el-radio :label="key" v-for="(item,key, index) in dataObj">{{key | fieldFilter}}</el-radio>
						</el-radio-group>
					</div>
				</el-col>
				<el-col :span="15">
					<div id="taskCountChart" style="width: 600px;height:400px;"></div>
				</el-col>
			</el-row>
		</div>`,
	props: ['wx_id', 'date-range'],
	data: function () {
		return {
			loading: false,
			xData: [],
			yData: [],
			chart: null,
			activeTag: '',
			dataObj: {},
			userName: ''
		}
	},
	computed: {
		options: function () {
			return {
				title: {
					text: '任务统计'
				},
				tooltip: {},
				legend: {
					data: ['任务量']
				},
				xAxis: {
					data: this.xData
				},
				yAxis: {},
				series: [{
					name: '任务量',
					type: 'line',
					data: this.yData
				}],
			}
		}
	},
	monted: function () {},
	methods: {
		reset: function () {
			this.chart = echarts.init(document.querySelector('#taskCountChart'))
			this.loading = false
			this.xData = []
			this.yData = []
			this.activeTag = ''
			this.dataObj = {}
			this.getData()
		},
		getData: function () {
			this.loading = true
			$.post('getUserTaskCountData', {
				wx_id: this.wx_id,
				dateRange: this.dateRange
			}).then((res) => {
				this.xData = res.dateArr
				this.dataObj = res.data
				this.loading = false
			})
		},
		updateChart: function () {
			this.chart.setOption(this.options)
		},
		changeTag: function (label) {
			this.activeTag = label
			this.yData = this.dataObj[label]
			this.updateChart()
		}
	},
	filters: {
		fieldFilter: function (value) {
			const arr = {
				'v3_paper_task': '新系统报纸录入审核量',
				'v3_bc_task': '新系统广播录入审核量',
				'v3_tv_task': '新系统电视录入审核量',
				'v3_sub_cut_err': '新系统剪辑错误提交',
				'v3_task_back': '新系统被退回任务',
				'v3_ad_sure': '广告确认',
				'v3_quality': '新系统质检任务',
				'input_err': '旧系统录入错误',
				'tv_input': '旧系统电视录入',
				'tv_inspect': '旧系统电视审核',
				'bc_input': '旧系统广播录入',
				'bc_inspect': '旧系统广播审核',
				'net_input': '旧系统网络录入',
				'net_inspect': '旧系统网络审核',
				'paper_input': '旧系统报纸录入',
				'paper_inspect': '旧系统报纸审核',
				'paper_page_cut': '剪辑版面',
				'paper_ad_cut': '剪辑广告',
				'paper_page_up': '报纸上传',
				'sub_paper_cut_err': '提交报纸剪辑错误',
				'paper_cut_err': '报纸剪辑错误',
				'inspect_err': '初审错误次数',
			}
			return arr[value]
		}
	}

}