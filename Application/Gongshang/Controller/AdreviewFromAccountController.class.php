<?php
namespace Gongshang\Controller;
use Think\Controller;

/**
 * Class AdreviewFromaccountController
 * @package Gongshang\Controller
 * 申请台账（我发出的）
 */

class AdreviewFromAccountController extends BaseController
{
	public function index()
	{
		$p = I('p', 1);//当前第几页
		$pp = 20;//每页显示多少记录
		$fcreatetime_s = I('fcreatetime_s');// 申请日期  开始
		$fcreatetime_e = I('fcreatetime_e');// 申请日期  结束
		$fchecktime_s = I('fchecktime_s');// 复核日期  开始
		$fchecktime_e = I('fchecktime_e');// 复核日期  结束

		if ($fcreatetime_s == '') {
			$fcreatetime_s = '2015-01-01';
		}
		if ($fcreatetime_e == '') {
			$fcreatetime_e = date('Y-m-d');
		}
		$where = array();
		$where['tadcheck.fapplytime'] = array('between', $fcreatetime_s . ',' . $fcreatetime_e . ' 23:59:59'); //时间
		if ($fchecktime_s && $fchecktime_e) {
			$where['tadcheck.fchecktime'] = array('between', $fchecktime_s . ',' . $fchecktime_e . ' 23:59:59'); //时间
		}
		$count = M('tadcheck')
			->join('tillegalad on tadcheck.fillegaladid=tillegalad.fillegaladid', 'LEFT')//违法类型
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on  tad.fadclasscode=tadclass.fcode ', 'LEFT')//广告内容列别
			->join('tmedia on  tillegalad.fmediaid=tmedia.fid', 'LEFT')//媒介信息
			->where($where)
			->where(array('tadcheck.fapplyregulatorid'=>session('regulatorpersonInfo.fregulatorid')))
			->count();// 查询满足要求的总记录数

		$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$data = M('tadcheck')
			->field('
						tadcheck.fillegaladid,
			            tad.fadname,
			            tmedia.fmedianame,
			            tadclass.ffullname as adclass_fullname,
			            tadcheck.fcheckresult,
			            tadcheck.fapply,
			            tadcheck.fapplytime,
			            tadcheck.fstate,
			            tadcheck.fchecktime
									')
			->join('tillegalad on tadcheck.fillegaladid=tillegalad.fillegaladid', 'LEFT')//违法类型
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on  tad.fadclasscode=tadclass.fcode ', 'LEFT')//广告内容列别
			->join('tmedia on  tillegalad.fmediaid=tmedia.fid', 'LEFT')//媒介信息
			->where($where)
			->where(array('tadcheck.fapplyregulatorid'=>session('regulatorpersonInfo.fregulatorid')))
			->limit($Page->firstRow . ',' . $Page->listRows)
			->select();//查询违法广告

		$data = list_k($data, $p, $pp);//为列表加上序号
		$this->assign('data', $data); //列表信息
		$this->assign('page', $Page->show());//分页输出
		$this->display('reapplication');
	}

	/*线索详情*/
	public function details()
	{
		$where = array();
		if(I('fillegaladid')){
			$fillegaladid = I('fillegaladid');//线索ID
		}else{
			$this->error('缺失fillegaladid!');
		}
		$where['tillegalad.fillegaladid'] = $fillegaladid;
		$data = M('tillegalad')
			->field('
						tillegalad.fillegaladid,
						tillegalad.fmediaclassid,
						tillegalad.fadid,
			            tad.fadname,
			            tadclass.ffullname as adclass_fullname,
			            tillegalad.fissuetimes,
			            tillegalad.fdisposetimes,
			            tillegalad.ffirstissuetime,
			            tillegalad.flastissuetime,
			            tillegalad.fconfirmations,
			            tillegalad.fpunishments,
			            tmediaowner.fname as mediaowner_name

									')//id，<<媒介类别>>,广告id. 广告名称，广告内容列别，发布次数。指导次数。首次发布日期，末次发布日期，认定依据，处罚依据，样本id：
			->join('tmediaclass on tmediaclass.fid = tillegalad.fmediaclassid', 'LEFT')//媒体列别
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on tadclass.fcode = tad.fadclasscode', 'LEFT')//广告内容列别
			->join('tmedia on tmedia.fid = tillegalad.fmediaid', 'LEFT')//媒介信息
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid', 'LEFT')
			->where($where)
			->find();//查询广告信息

		if ($data['fmediaclassid']) {
			switch ($data['fmediaclassid']) {
				case 'bc':
					$adclueInfo = M('tbcsample')->field('fid,favifilename')->where(array('fadid' => $data['fadid']))->find();
					break;
				case 'tv':
					$adclueInfo = M('ttvsample')->field('fid,favifilename')->where(array('fadid' => $data['fadid']))->find();
					break;
				case 'paper':
					$adclueInfo = M('tpapersample')->field('fpapersampleid,fjpgfilename')->where(array('fadid' => $data['fadid']))->find();
					break;
			}
		}
		//流转信息
		$flow = M('tillegaladflow')->where(array('fillegaladid' =>$fillegaladid))->select();
		//复核信息
		$check_data = M('tadcheck')->where(array('fillegaladid' =>$fillegaladid))->find();
		$check_data['fillegaltypecode1_name']=$this->get_fillegaltype($check_data['fillegaltypecode1']);
		$check_data['fillegaltypecode_name']=$this->get_fillegaltype($check_data['fillegaltypecode']);

		$this->assign('data', $data); //违法信息
		$this->assign('adclueInfo', $adclueInfo); //视频信息
		$this->assign('flow', $flow); //流转信息
		$this->assign('check_data', $check_data); //复核信息
		$this->display('reapplicationDetails');
	}
}