<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 线索处置
 */

class NcluelistController extends BaseController
{
	/**
	 * 获取线索登记列表
	 * by zw
	 */
	public function index() {
		$getData = $this->oParam;
		$pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
		$fnum = $getData['fnum'];//交办单号
		$fadname = $getData['fadname'];//广告名称
	    $finput_user = $getData['finput_user'];//登记人
	    $fregister_date = $getData['finput_date'];//登记日期，数组
	    $isself = $getData['isself'];//仅显示自己的数据，1是，0否
	    $outtype = $getData['outtype'];//导出类别

	    if(empty($outtype)){
        	$limitIndex = ($pageIndex-1)*$pageSize;
	    }else{
	    	$pageSize = 999999;
	    	$limitIndex = 0;
	    }
	    if(!empty($fnum)){
	    	$where['fnum'] = array('like','%'.$fnum.'%');
	    }
	    if(!empty($fadname)){
	    	$where['fadname'] = array('like','%'.$fadname.'%');
	    }
	    if(!empty($finput_user)){
	    	$where['finput_user'] = array('like','%'.$finput_user.'%');
	    }
	    if(!empty($fregister_date)){
	    	$where['finput_date'] = ['between',[$fregister_date[0],$fregister_date[1]]];
	    }
	    if(!empty($isself)){
	    	$where['finput_userid'] = session('regulatorpersonInfo.fid');
	    }

		$where['fstatus'] 	= 10;
		$count = M('nj_clue')
			->where($where)
			->count();
		
		$data = M('nj_clue')
			->field('*')
			->where($where)
			->order('finput_date desc,fid desc')
			->limit($limitIndex,$pageSize)
			->select();

		if(!empty($outtype)){
			if(empty($data)){
				$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
			}
	        $outdata['datalie'] = [
	          '序号'=>'fxuhao',
	          '交办单号'=>'fnum',
	          '交办日期'=>'fhandover_date',
	          '交办方式'=>'fhandover_types',
	          '线索来源'=>'fget_types',
	          '移送或提供线索单位'=>'ftipoffs_unit',
	          '投诉或举报方'=>'ftipoffs_user',
	          '投诉或举报方联系方式'=>'ftipoffs_phone',
	          '广告主'=>'fadowner_name',
	          '广告发布者（发布平台）'=>'fadsend_user',
	          '广告类型'=>'fadtypes',
	          '商品/服务类别'=>'fadclass_name',
	          '广告内容'=>'fadname',
	          '刊播时间'=>'fadissue_date',
	          '落地链接'=>'ftarget_url',
	          '发布条目数'=>'fadissue_times',
	          '反馈截止日期'=>'fendtime',
	          '接收机构'=>'fget_regulator',
	          '登记用户'=>'finput_user',
	          '登记日期'=>'finput_date',
	          '下发日期'=>'fpush_date'
	        ];

	        $outdata['lists'] = $data;
	        $ret = A('Api/Function')->outdata_xls($outdata);

	        D('Function')->write_log('南京线索登记导出',1,'excel导出成功');
	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
		}
	}

	/**线索导入
     * @return false|string
     */
	public function clueImport(){

        $upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize   =     3145728 ;// 设置附件上传大小
        $upload->exts      =     array('xlsx','xls','csv');// 设置附件上传类型
        $upload->rootPath  =     './Public/Xls/'; // 设置附件上传根目录
        $upload->savePath  =     ''; // 设置附件上传（子）目录
        $upload->autoSub = false;

        $info  =  $upload->upload();// 上传文件

        if(!$info) {
            // 上传错误提示错误信息
            $this->ajaxReturn(array('code'=>1,'msg'=>$upload->getError()));
        }else{
            // 上传成功
            foreach($info as $file){
                $excel_file = $file['savepath'].$file['savename'];
            }
        }

        $excel_file_name = './Public/Xls/'.$excel_file;//文件地址

        $excel_data = import_excel($excel_file_name);//获取excel信息

        unlink($excel_file_name);//删除excel

        $maxxuhao = M('nj_clue')->where(['fstatus'=>['neq',-1]])->getField('max(fid)');
	    if(empty($maxxuhao)){
	    	$maxxuhao = 0;
	    }

        $okData = [];//成功导入记录
        $errorData = [];//导入出错记录
        //循环获取写入数据
        foreach ($excel_data as $clue){
        	$maxxuhao += 1;
        	if(empty($clue[0])){
        		continue;
        	}

		    $fhandover_types = str_replace('+','；',str_replace(' ','',str_replace(')','）',str_replace('(','（',str_replace(';', '；', $clue[2])))));//交办方式
		    $fget_types = str_replace(' ','',str_replace(')','）',str_replace('(','（',str_replace(';', '；', $clue[3]))));//线索来源
		    $endtime = $clue[15]?date('Y-m-d',\PHPExcel_Shared_Date::ExcelToPHP($clue[15])):null;//反馈截止日期
		    $fregionname = trim($clue[16]);//地区
		    $doTr = M('tregulator')
		    	->alias('a')
		    	->field('a.fid,a.fname')
		    	// ->cache(86400)
		    	->join('tregion b on a.fregionid = b.fid')
		    	->where(['b.fname'=>['like',$fregionname.'%'],'b.fid'=>['like','3201%'],'a.fkind'=>1])
		    	->find();

		    if(!empty($doTr)){
		    	$dataCe = [
	                "fxuhao"            => $maxxuhao,//交办单号,
	                "fnum"            => trim($clue[0]),//交办单号,
	                "fhandover_date"  => date('Y-m-d',\PHPExcel_Shared_Date::ExcelToPHP($clue[1])),//交办日期,
	                "fhandover_types" => $fhandover_types,//交办方式,
	                "fget_types"      => $fget_types,//'线索来源',
	                "ftipoffs_unit"   => trim($clue[4]),//'移送或提供线索单位',
	                "ftipoffs_user"   => trim($clue[5]),//'投诉或举报方',
	                "ftipoffs_phone"  => trim($clue[6]),//'投诉或举报方联系方式',
	                "fadowner_name"   => trim($clue[7]),//'广告主',
	                "fadsend_user"    => trim($clue[8]),//'广告发布者（发布平台）',
	                "fadtypes"        => trim($clue[9]),//'广告类型（报纸、广播、电视、户外、互联网PC端、互联网移动端、其它） ',
	                "fadclass_name"   => str_replace(' ', '', $clue[10]),//'商品/服务类别',
	                "fadname"         => trim($clue[11]),//'广告内容',
	                "fadissue_date"   => trim($clue[12]),//'刊播时间',
	                "ftarget_url"     => trim($clue[13]),//'落地链接',
	                "fadissue_times"  => trim($clue[14]),//'广告条目数',
	                "fendtime"        => $endtime,//'反馈截止日期',
	                "fget_regulator"  => $doTr?$doTr['fname']:'',//'接收机构',
	                "fget_regulatorid"      => $doTr?$doTr['fid']:0,//'接收机构id',
	                "finput_regulator" => session('regulatorpersonInfo.regulatorpname'),
	                "finput_regulatorid" => session('regulatorpersonInfo.fregulatorpid'),
	                "finput_user"	=> session('regulatorpersonInfo.fname'),
	                "finput_userid" => session('regulatorpersonInfo.fid'),
	                "finput_date" => date('Y-m-d')
	            ];
		    }else{
		    	$errorData[] = $clue[0];
		    	continue;
		    }
    		
        	$addNce = M('nj_clue')->add($dataCe);
        	if(!empty($addNce)){
		   		//添加流程记录
		   		$dataNcfw = [];
		   		$dataNcfw['fclueid'] = $addNce;
		   		$dataNcfw['fprvflowid'] = 0;
		   		$dataNcfw['fcreatetrepid'] = session('regulatorpersonInfo.fregulatorpid');
		   		$dataNcfw['fcreatetrepname'] = session('regulatorpersonInfo.regulatorpname');
		   		$dataNcfw['fcreatepersonid'] = session('regulatorpersonInfo.fid');
		   		$dataNcfw['fcreateperson'] = session('regulatorpersonInfo.fname');
		   		$dataNcfw['fcreatetime'] = date('Y-m-d H:i:s');
		   		if(!empty($gotype)){
			   		$dataNcfw['ffinishtime'] = date('Y-m-d H:i:s');
		   			$dataNcfw['fsendstatus'] = 10;
		   			$dataNcfw['fchildstatus'] = 10;
		   		}else{
		   			$dataNcfw['fsendstatus'] = 0;
		   			$dataNcfw['fchildstatus'] = 0;
		   		}
		   		$dataNcfw['fcreateinfo'] = '';
	   			$dataNcfw['freason'] = '线索导入';
		   		$dataNcfw['fflowname'] = '线索登记';
	   			$dataNcfw['fstatus'] = 10;
		   		$addNcfw = M('nj_clueflow')->add($dataNcfw);
		   		if(!empty($addNcfw)){
		   			$okData[] = $clue[0];
		   		}else{
		   			$errorData[] = $clue[0];
		   		}
		   	}
        }

        if($okData){
        	if(!empty($errorData)){
        		$errorstr = '导入失败'.count($errorData).'条。导入错误记录的交办单号：'.implode(',', $errorData).'。注意：交办单号和接收机构必填，如未填将被直接过滤或记作失败导入项。';
        	}
            $this->ajaxReturn(array('code'=>0,'msg'=>'成功导入'.count($okData).'条。'.$errorstr));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'导入失败。注意：交办单号和接收机构必填，如未填将被直接过滤或记作失败导入项。'));
        }

    }

	/**
	 * 线索登记调整操作（包含添加、修改）
	 * by zw
	*/
	public function clueAction(){
		$getData = $this->oParam;
		$fid = $getData['fid'];//主键
		$fnum = $getData['fnum'];//交办单号
		$fhandover_date = $getData['fhandover_date'];//交办日期
	    $fhandover_types = $getData['fhandover_types'];//交办方式
	    $fget_types = $getData['fget_types'];//线索来源
	    $ftipoffs_unit = $getData['ftipoffs_unit'];//举报方
	    $ftipoffs_user = $getData['ftipoffs_user'];//举报方
	    $ftipoffs_phone = $getData['ftipoffs_phone'];//举报方联系方式
	    $fadowner_name = $getData['fadowner_name'];//广告主
	    $fadsend_user = $getData['fadsend_user'];//广告发布者（发布平台）
	    $fadtypes = $getData['fadtypes'];//广告类型（报纸、广播、电视、户外、互联网PC端、互联网移动端、其它） 
	    $fadclass_name = $getData['fadclass_name'];//广告类别
	    $fadname = $getData['fadname'];//广告名称
	    $fadissue_date = $getData['fadissue_date'];//刊播时间
	    $ftarget_url = $getData['ftarget_url'];//落地链接
	    $fadissue_times = $getData['fadissue_times'];//发布条目数
	    $fendtime = $getData['fendtime'];//反馈截止日期
	    $fhandle_regulatorid = $getData['fhandle_regulatorid'];//接收机构ID
	    $fhandle_regulatorname = $getData['fhandle_regulatorname'];//接收机构
	    $attachinfo = $getData['attachinfo'];//上传的附件材料
	    $gotype = $getData['gotype'];//提交方式，0保存，1下发

	    if(empty($fhandle_regulatorid)){
	    	$this->ajaxReturn(['code'=>1,'msg'=>'请选择接收机构']);
	    }

	   	$dataCe['fnum'] = $fnum;
	   	$dataCe['fhandover_date'] = $fhandover_date;
	   	$dataCe['fhandover_types'] = $fhandover_types;
	   	$dataCe['fget_types'] = $fget_types;
	   	$dataCe['ftipoffs_unit'] = $ftipoffs_unit;
	   	$dataCe['ftipoffs_user'] = $ftipoffs_user;
	   	$dataCe['ftipoffs_phone'] = $ftipoffs_phone;
	   	$dataCe['fadowner_name'] = $fadowner_name;
	   	$dataCe['fadsend_user'] = $fadsend_user;
	   	$dataCe['fadtypes'] = $fadtypes;
	   	$dataCe['fadclass_name'] = $fadclass_name;
	   	$dataCe['fadname'] = $fadname;
	   	$dataCe['fadissue_date'] = $fadissue_date;
	   	$dataCe['ftarget_url'] = $ftarget_url;
	   	$dataCe['fadissue_times'] = $fadissue_times;
	   	$dataCe['fget_regulator'] = $fhandle_regulatorname;
	   	$dataCe['fget_regulatorid'] = $fhandle_regulatorid;
	   	if(!empty($fendtime)){//截止时间
		   	$dataCe['fendtime'] = $fendtime;
	   	}else{
	   		$dataCe['fendtime'] = null;
	   	}

	   	if(!empty($gotype)){//下发
	   		$dataCe['fstatus'] = 20;
	   		$dataCe['fpush_date'] = date('Y-m-d');

	   		//未填截止日期会被默认为下发日之后10天
	   		if(empty($dataCe['fendtime'])){
	   			$dataCe['fendtime'] = date('Y-m-d',strtotime('+15 day'));
	   			$dataCe['fendtime_status'] = 0;
	   		}else{
	   			$dataCe['fendtime_status'] = 1;
	   		}
	   	}else{//保存
	   		$dataCe['fstatus'] = 10;
	   	}

	   	if(empty($fid)){
	   		$maxxuhao = M('nj_clue')->where(['fstatus'=>['neq',-1]])->getField('max(fid)');
		    if(empty($maxxuhao)){
		    	$maxxuhao = 0;
		    }

	    	$dataCe['fxuhao'] = $maxxuhao+1;
		   	$dataCe['finput_regulator'] = session('regulatorpersonInfo.regulatorpname');
		   	$dataCe['finput_regulatorid'] = session('regulatorpersonInfo.fregulatorpid');
		   	$dataCe['finput_user'] = session('regulatorpersonInfo.fname');
		   	$dataCe['finput_userid'] = session('regulatorpersonInfo.fid');
		   	$dataCe['finput_date'] = date('Y-m-d');

		   	$addNce = M('nj_clue')->add($dataCe);
		   	if(!empty($addNce)){
		   		//添加流程记录
		   		$dataNcfw = [];
		   		$dataNcfw['fclueid'] = $addNce;
		   		$dataNcfw['fprvflowid'] = 0;
		   		$dataNcfw['fcreatetrepid'] = session('regulatorpersonInfo.fregulatorpid');
		   		$dataNcfw['fcreatetrepname'] = session('regulatorpersonInfo.regulatorpname');
		   		$dataNcfw['fcreatepersonid'] = session('regulatorpersonInfo.fid');
		   		$dataNcfw['fcreateperson'] = session('regulatorpersonInfo.fname');
		   		$dataNcfw['fcreatetime'] = date('Y-m-d H:i:s');
		   		if(!empty($gotype)){
			   		$dataNcfw['ffinishtime'] = date('Y-m-d H:i:s');
		   			$dataNcfw['fsendstatus'] = 10;
		   			$dataNcfw['fchildstatus'] = 10;
		   		}else{
		   			$dataNcfw['fsendstatus'] = 0;
		   			$dataNcfw['fchildstatus'] = 0;
		   		}
		   		$dataNcfw['fcreateinfo'] = '';
	   			$dataNcfw['freason'] = '手动登记';
		   		$dataNcfw['fflowname'] = '线索登记';
	   			$dataNcfw['fstatus'] = 10;
		   		$addNcfw = M('nj_clueflow')->add($dataNcfw);

		   		if(!empty($attachinfo)){
	   				//附件材料添加
			   		$attach_data['fclueid'] = $addNce;//登记id
			        $attach_data['fflowid'] = $addNcfw;//流程ID
			        $attach_data['fuploadtime'] = date('Y-m-d H:i:s');//上传时间
			        $attach_data['ftype'] = 5;//附件类型
			        $attach_data['fcreateuserid'] = session('regulatorpersonInfo.fid');//上传人
			        $attach_data['fcreatetreid'] = session('regulatorpersonInfo.fregulatorpid');//上传机构
			        $attach = [];
			        $attach2 = [];
			        foreach ($attachinfo as $key => $value){
			          $attach_data['ffilename'] = $value['fattachname'];
			          $attach_data['fattachname'] = $value['fattachname'];
			          $attach_data['fattachurl'] = $value['fattachurl'];
			          if($value['fattachstatus'] == 2){
			            $attach[] = $attach_data;
			          }elseif($value['fattachstatus'] == 0){
			          	$attach2[] = $value['fattachurl'];
			          }
			        }
					if(!empty($attach)){
						M('nj_cluefile')->addAll($attach);
					}
					if(!empty($attach2)){
						$where_att['fattachurl'] = array('in',$attach2);
						$where_att['fclueid'] = $fid;
						$where_att['fflowid'] = $viewNcfw;
						M('nj_cluefile')->where($where_att)->save(['fstatus'=>0]);
					}
	   			}
		   		
		   		//下发流程添加
		   		if(!empty($gotype) && !empty($addNcfw)){
		   			//添加流程记录
			   		$dataNcfw = [];
			   		$dataNcfw['fclueid'] = $addNce;
			   		$dataNcfw['fprvflowid'] = $addNcfw;
			   		$dataNcfw['fcreatetrepid'] = $fhandle_regulatorid;
			   		$dataNcfw['fcreatetrepname'] = $fhandle_regulatorname;
			   		$dataNcfw['fcreatetime'] = date('Y-m-d H:i:s');
		   			$dataNcfw['fchildstatus'] = 0;
		   			$dataNcfw['fsendstatus'] = 0;
			   		$dataNcfw['fcreateinfo'] = '';
		   			$dataNcfw['freason'] = '';
			   		$dataNcfw['fflowname'] = '等待处理';
			   		$dataNcfw['fstatus'] = 20;
			   		$addNcfw = M('nj_clueflow')->add($dataNcfw);
		   			if(empty($addNcfw)){
		   				$this->ajaxReturn(['code'=>1,'msg'=>'操作失败，未知原因']);
		   			}else{
		   				$this->clueSendSms($fhandle_regulatorid,1,$fnum,$dataCe['fendtime'],0);//发送短信通知
		   			}
		   		}
		   		$this->ajaxReturn(['code'=>0,'msg'=>'操作成功']);
		   	}else{
		   		$this->ajaxReturn(['code'=>1,'msg'=>'操作失败，未知原因']);
		   	}
	   	}else{
	   		$dataCe['fmodify_user'] = session('regulatorpersonInfo.fname');
		   	$dataCe['fmodify_time'] = date('Y-m-d H:i:s');

	   		$whereNce['fid'] = $fid;
	   		$whereNce['finput_userid'] = session('regulatorpersonInfo.fid');
	   		$whereNce['fstatus'] = 10;
	   		$countNce = M('nj_clue')->where($whereNce)->count();
	   		if(empty($countNce)){
	   			$this->ajaxReturn(['code'=>1,'msg'=>'无法操作已流转或非您本人登记的线索']);
	   		}else{
		   		$viewNcfw = M('nj_clueflow')->where(['fclueid'=>$fid])->order('fid desc')->getField('fid');//当前流程ID
	   			if(!empty($attachinfo)){
	   				//附件材料添加
			   		$attach_data['fclueid'] = $fid;//登记id
			        $attach_data['fflowid'] = $viewNcfw;//流程ID
			        $attach_data['fuploadtime'] = date('Y-m-d H:i:s');//上传时间
			        $attach_data['ftype'] = 5;//附件类型
			        $attach_data['fcreateuserid'] = session('regulatorpersonInfo.fid');//上传人
			        $attach_data['fcreatetreid'] = session('regulatorpersonInfo.fregulatorpid');//上传机构
			        $attach = [];
			        $attach2 = [];
			        foreach ($attachinfo as $key => $value){
			          $attach_data['ffilename'] = $value['fattachname'];
			          $attach_data['fattachname'] = $value['fattachname'];
			          $attach_data['fattachurl'] = $value['fattachurl'];
			          if($value['fattachstatus'] == 2){
			            $attach[] = $attach_data;
			          }elseif($value['fattachstatus'] == 0){
			          	$attach2[] = $value['fattachurl'];
			          }
			        }
					if(!empty($attach)){
						M('nj_cluefile')->addAll($attach);
					}
					if(!empty($attach2)){
						$where_att['fattachurl'] = array('in',$attach2);
						$where_att['fclueid'] = $fid;
						$where_att['fflowid'] = $viewNcfw;
						M('nj_cluefile')->where($where_att)->save(['fstatus'=>0]);
					}
	   			}
	   			if(!empty($gotype)){//下发
					if(!empty($viewNcfw)){
						M('nj_clueflow')->where(['fid'=>$viewNcfw])->save(['fchildstatus'=>10,'fsendstatus'=>10,'ffinishtime'=>date('Y-m-d H:i:s')]);//保存当前流程状态
						//添加流程记录
				   		$dataNcfw = [];
				   		$dataNcfw['fclueid'] = $fid;
				   		$dataNcfw['fprvflowid'] = $viewNcfw;
				   		$dataNcfw['fcreatetrepid'] = $fhandle_regulatorid;
				   		$dataNcfw['fcreatetrepname'] = $fhandle_regulatorname;
				   		$dataNcfw['fcreatetime'] = date('Y-m-d H:i:s');
			   			$dataNcfw['fchildstatus'] = 0;
			   			$dataNcfw['fsendstatus'] = 0;
				   		$dataNcfw['fcreateinfo'] = '';
			   			$dataNcfw['freason'] = '';
				   		$dataNcfw['fflowname'] = '等待处理';
				   		$dataNcfw['fstatus'] = 20;
				   		$addNcfw = M('nj_clueflow')->add($dataNcfw);//添加下一流程
			   			if(empty($addNcfw)){
			   				$this->ajaxReturn(['code'=>1,'msg'=>'操作失败，未知原因']);
			   			}else{
			   				$this->clueSendSms($fhandle_regulatorid,1,$fnum,$dataCe['fendtime'],0);//发送短信通知
			   				$saveNce = M('nj_clue')->where($whereNce)->save($dataCe);
			   			}
					}else{
						$this->ajaxReturn(['code'=>1,'msg'=>'操作失败，未知原因']);
					}
			   	}else{//保存
	   				$saveNce = M('nj_clue')->where($whereNce)->save($dataCe);
			   	}
			   	if(empty($saveNce)){
			   		$this->ajaxReturn(['code'=>1,'msg'=>'操作失败，未知原因']);
			   	}else{
	   				$this->ajaxReturn(['code'=>0,'msg'=>'操作成功']);
			   	}
	   		}
	   	}
	}

	//短信通知办案用户
	public function clueSendSms($fwaittreid,$sl = 1,$jbh = '',$yxq = '',$istz = 0){
		$system_num = getconfig('system_num');
		$where['fregulatorid'] = $fwaittreid;
		$where['_string']  	= '(plbcount = 0 or plbcount is null or plbcount2 = 1)';
		$regulatorpersonInfo = M('tregulatorperson')
			->alias('a')
			->field('a.fmobile')
			->join('(select count(*) as plbcount,sum(case when fcustomer = "'.$system_num.'" then 1 else 0 end) as plbcount2,fpersonid from tpersonlabel  where fstate = 1 group by fpersonid) lb on a.fid = lb.fpersonid','left')
			->where($where)
			->select();
		foreach ($regulatorpersonInfo as $key => $value) {
			if(strlen($value['fmobile']) == 11 && is_numeric($value['fmobile'])){
				if(empty(S('nj_sendsms_'.$value['fmobile'])) || !empty($istz)){
					A('Common/Alitongxin','Model')->nj_illtask_sms($value['fmobile'],$sl,$jbh,$yxq);
					S('nj_sendsms_'.$value['fmobile'],1,86400);
				}
			}
		}
	}

	/**
	 * 线索批量下发
	 * by zw
	*/
	public function clueSelectPush(){
		$getData = $this->oParam;
		$fid = $getData['fid'];//线索ID
	    $fnums = [];
	   
   		if(is_array($fid)){
			$whereNce['fid'] = ['in',$fid];
		}else{
			$whereNce['fid'] = $fid;
		}
   		$whereNce['finput_userid'] = session('regulatorpersonInfo.fid');
   		$whereNce['fstatus'] = 10;
   		$isok = 0;//成功数量
   		$smsregulator = [];//需要通知的机构
   		$doNce = M('nj_clue')->field('fid,fnum,fendtime,fget_regulatorid,fget_regulator')->where($whereNce)->select();
   		if(empty($doNce)){
   			$this->ajaxReturn(['code'=>1,'msg'=>'无法操作已流转或非您本人登记的线索']);
   		}else{
   			$idsNce = array_column($doNce, 'fid');
   			$fnums = array_column($doNce, 'fnum');
   			$dataCe['fstatus'] = 20;
   			$dataCe['fpush_date'] = date('Y-m-d');
			foreach ($doNce as $key => $value) {
				if(empty($value['fendtime'])){
					$dataCe['fendtime'] = date('Y-m-d',strtotime('+15 day'));
	   				$dataCe['fendtime_status'] = 0;
				}else{
					$dataCe['fendtime'] = $value['fendtime'];
					$dataCe['fendtime_status'] = 1;
				}
				$saveNce = M('nj_clue')->where(['fid'=>$value['fid']])->save($dataCe);
				if(!empty($saveNce)) {
	   				$viewNcfw = M('nj_clueflow')->where(['fclueid'=>$value['fid']])->order('fid desc')->getField('fid');
					if(!empty($viewNcfw)){
						M('nj_clueflow')->where(['fid'=>$viewNcfw])->save(['fchildstatus'=>10,'fsendstatus'=>10,'fcreatepersonid'=>session('regulatorpersonInfo.fid'),'fcreateperson'=>session('regulatorpersonInfo.fname'),'ffinishtime'=>date('Y-m-d H:i:s')]);
						//添加流程记录
				   		$dataNcfw = [];
				   		$dataNcfw['fclueid'] = $value['fid'];
				   		$dataNcfw['fprvflowid'] = $viewNcfw;
				   		$dataNcfw['fcreatetrepid'] = $value['fget_regulatorid'];
				   		$dataNcfw['fcreatetrepname'] = $value['fget_regulator'];
				   		$dataNcfw['fcreatetime'] = date('Y-m-d H:i:s');
			   			$dataNcfw['fchildstatus'] = 0;
			   			$dataNcfw['fsendstatus'] = 0;
				   		$dataNcfw['fcreateinfo'] = '';
			   			$dataNcfw['freason'] = '';
				   		$dataNcfw['fflowname'] = '等待处理';
				   		$dataNcfw['fstatus'] = 20;
				   		$addNcfw = M('nj_clueflow')->add($dataNcfw);
				   		if(!empty($addNcfw)){
				   			$isok += 1;
				   			if(!in_array($value['fnum'], $smsregulator[(string)$value['fget_regulatorid']]['fnums'])){
				   				$smsregulator[(string)$value['fget_regulatorid']]['fnums'][] = $value['fnum'];
				   			}
			   				if(empty($smsregulator[(string)$value['fget_regulatorid']]['fendtime'])){
			   					$smsregulator[(string)$value['fget_regulatorid']]['fendtime'] = $dataCe['fendtime'];
			   				}else{
			   					$fendtime = $fendtime?$fendtime:$smsregulator[(string)$value['fget_regulatorid']]['fendtime'];
			   					if(strtotime($smsregulator[(string)$value['fget_regulatorid']]['fendtime'])>strtotime($dataCe['fendtime'])){
			   						$smsregulator[(string)$value['fget_regulatorid']]['fendtime'] = $dataCe['fendtime'];
			   					}
			   				}
				   		}
			   		}
				}
   			}

   			foreach ($smsregulator as $key => $value) {
				if(count($value['fnums'])>1 && count($value['fnums']) <= 5){
		   			$this->clueSendSms($key,count($value['fnums']),implode(',', $value['fnums']),$value['fendtime'],1);//发送短信通知
	   			}elseif(count($value['fnums']) >5){
	   				$this->clueSendSms($key,'多',$value['fnums'][0].','.$value['fnums'][1].'...',$value['fendtime'],1);//发送短信通知
	   			}elseif(count($value['fnums']) == 1){
	   				$this->clueSendSms($key,1,$value['fnums'][0],$value['fendtime'],0);//发送短信通知
	   			}
   			}

   			$this->ajaxReturn(array('code'=>0,'msg'=>'成功操作'.$isok.'条。注意：非您本人登记的线索无法操作'));
   		}
	}
}