<?php
namespace Agp\Controller;
use Think\Controller;
import('Vendor.PHPExcel');
/**
 * 国家局数据统计
 * by yjm
 */
class StatisticalDataController extends BaseController{

    /**
     * 违法广告统计
     * by yjm
     *http://www.ctcmc.com.cn/web/ggsjzx.asp
     * 1.通过选择时间，确认实例化哪张表
     * 2.通过点击选项卡，连接对应表
     * 3.条件
     *  ① '区域'=>['省','副省级市','市','县区']
     *  ② 媒体（媒体的种类）
     *  ③ 广告类别（广告的种类）
     *  ④ 综合得分、时长、时长违法率、违法条次和违法率
     */
    public function illegal_ad_monitor(){
        Check_QuanXian(['jchuizong','wfchachu']);
        ini_set('memory_limit','3072M');
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type: text/html; charset=utf-8");
        $user = session('regulatorpersonInfo');//获取用户信息

        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isrelease = $ALL_CONFIG['isrelease'];
        $use_open_search = $ALL_CONFIG['use_open_search'];
        $ischeck = $ALL_CONFIG['ischeck'];
        $is_manage_media = $ALL_CONFIG['set_manage_region'];//是否通过指定区域管理媒体统计

        $region_order = I('region_order',1);//是否地域排名

        $is_include_sub = I('is_include_sub',0);//是否包含下级

        $ad_pm_type = I('ad_pm_type',2);//确定是点击监测情况或者线索菜单

        $is_out_report = I('is_out_report',0);//确定是否点击导出EXCEL

        $is_have_count = I('is_have_count',0);//确定是否点击导出EXCEL包含条数

        $year = I('year',date('Y'));//默认当年

        $times_table = I('times_table','');//选表，默认月表

        $is_show_fsend = I('is_show_fsend',2);//是否发布前的数据

        $is_show_longad = I('is_show_longad',0);//是否包含长广告

        $permission_media = I('permission_media',1);//控制权限媒体

        $title = I('title','XX市');//描述标题
        $regio_area = I('regio_area','XX市各县');//选择区划文字描述

        $isfixeddata = I('isfixeddata');//是否实时数据

        $date_cycle = I('date_cycle');//日期周期文字描述

        $map = [];//定义查询条件
        $summary_table = "tbn_ad_summary_day_v4";
        $map[$summary_table.'.fcustomer'] = $system_num;
        if($is_show_longad == 1){
            //包含长广告
            $play_len_field = 'fad_play_len_long_ad';
            $fad_times_field = 'fad_times_long_ad';
            $fad_count_field = 'fsam_list_long_ad';
        }else{
            //不包含长广告
            $play_len_field = 'fad_play_len';
            $fad_times_field = 'fad_times';
            $fad_count_field = 'fsam_list';
        }

        //默认表
        if($times_table == ''){
            $times_table = 'tbn_ad_summary_month';
        }
        $table_condition = I('table_condition',date('m').'-01');//根据选取的不用表展示不同的条件  //默认当月
        //根据选择的不同的时间组合不同的条件
        switch ($times_table){
            case 'tbn_ad_summary_day':
                $sedays = explode(',',$table_condition);
                $cycle_stamp = [
                    'start' =>strtotime($sedays[0]),
                    'end' =>(strtotime($sedays[1])+86400)
                ];//周期首末时间戳
                break;
            case 'tbn_ad_summary_week':
                if($table_condition < 10){
                    $table_condition = '0'.$table_condition;
                }
                $cycle_stamp = $this->weekday($year,$table_condition);//周期首末时间戳
                $cycle_stamp['end'] = $cycle_stamp['end']+86400;
                break;
            case 'tbn_ad_summary_half_month':
            case 'tbn_ad_summary_month':
            case 'tbn_ad_summary_quarter':
                $table_condition = $year.'-'.$table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                break;
            case 'tbn_ad_summary_half_year':
            case 'tbn_ad_summary_year':
                $table_condition = $table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                break;
        }

        $s_time = date('Y-m-d',$cycle_stamp['start']);
        $e_time = date('Y-m-d',($cycle_stamp['end']-86400));

        $map[$summary_table.'.fdate'] = ['BETWEEN',[$s_time,$e_time]];
        $map['_string'] = 'tmedia.fid = tmedia.main_media_id';
        $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_time,$e_time]];
        $illegal_map['tbn_illegal_ad.fstatus3'] = ['NOT IN',[30,40]];
        $illegal_map['tbn_illegal_ad.identify'] = 'new_2';
        $s_timestamp = strtotime($s_time);//开始时间戳
        $e_timestamp = strtotime($e_time);//结束时间戳

        //获取上个周期的日期范围
        if(($e_timestamp - $s_timestamp) <= 0){
            $last_s_time = date('Y-m-d',($s_timestamp-86400));//上周期开始时间戳
            $last_e_time = $last_s_time;//上周期结束时间戳
        }else{
            $last_s_time = date('Y-m-d',($s_timestamp-($e_timestamp-$s_timestamp)-86400));
            $last_e_time = date('Y-m-d',($s_timestamp-86400));
        }
        //连表
        //分组统计条件1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
        $fztj = I('fztj','');//默认连接地域表
        if($fztj == ''){
            $fztj = 'fregionid';
        }

        //浏览权限设置start
        $re_level = $this->judgment_region($user['regionid']);//当前用户行政等级

        //是否需要抽查
        if(!empty($ischeck)){
            $spot_check_data = M('spot_check')->where(['fcustomer'=>$user['system_num']])->select();//查询抽查表数据
            //如果抽查表有数据
            if(!empty($spot_check_data)){
                $dates = [];//定义日期数组
                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                    $condition = trim($spot_check_data_val['condition']);
                    if(!empty($condition)){
                        $year_month = '';
                        $date_str = [];
                        $year_month = substr($spot_check_data_val['fmonth'],0,7);
                        if(!empty($spot_check_data_val['condition'])){
                            $date_str = explode(',',$spot_check_data_val['condition']);
                        }
                        foreach ($date_str as $date_str_val){

                            $dates[] = $year_month.'-'.$date_str_val;
                        }
                    }
                }
                $map[$summary_table.'.fdate'] = [['IN',$dates],['BETWEEN',[$s_time,$e_time]],'AND'];
                $illegal_map['tbn_illegal_ad_issue.fissue_date'] = [['IN',$dates],['BETWEEN',[$s_time,$e_time]],'AND'];
            }else{
                $map['_string'] = '1 = 0';
                $illegal_map['_string'] = '1 = 0';
            }
        }

        //如果是要看同级的用户，行政区划登记上升一级1210
        $is_show_tj = $ALL_CONFIG['is_show_tj'];//是否显示同级20181210
        $is_show_sj = $ALL_CONFIG['is_show_sj'];//是否显示省级20181210
        if(!empty($is_show_tj) && $re_level > 1){
            if($re_level == 2 || $re_level == 3 || $re_level == 4){
                $re_level = 1;
                $regionid = substr($user['regionid'],0,2).'0000';
            }else{
                $regionid = trim($user['regionid'],'0').'00';
                $re_level = $this->judgment_region($regionid);
            }
        }else{
            $regionid = $user['regionid'];
        }

        //判断$re_level用户点击哪个菜单
        //线索汇总
        if($ad_pm_type == 2){

            if(!empty($isrelease) || $system_num == '100000'){
                $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
            }
            $region = I('region2','');//区域
            if($region == ''){
                if($system_num == '100000' && session('regulatorpersonInfo.fregulatorlevel') != 30){
                    //$map['tregion.fpid|tregion.fid'] = $regionid;
                    $map["tregion.flevel"] = ['IN','1,2,3'];
                }else{
                    if($re_level == 2 || $re_level == 3 || $re_level == 4){
                        $region = 5;
                    }else{
                        $region = $re_level;
                    }
                }
            }
            $illegal_map['tbn_illegal_ad.fcustomer'] = $system_num;
            //如果是要查看同级的就不添加媒体权限限制
            if(empty($is_show_tj) && ($permission_media == 1 || $permission_media == 2)){
                switch ($permission_media){
                    case 1:
                        $exp = 'IN';
                        break;
                    case 2:
                        $exp = 'NOT IN';
                        break;
                }
            }
            if($user['regionid'] != '100000'){
                $map['tregion.fid'] = ['like',trim($user['regionid'],0).'%'];
                $illegal_map['tregion.fid'] = ['like',trim($user['regionid'],0).'%'];
                //线索
                switch ($region){
                    case 0:
                        $map['tregion.fid'] = ['like',substr($user['regionid'],0,2).'%'];
                        $illegal_map['tregion.fid'] = ['like',substr($user['regionid'],0,2).'%'];
                        break;
                    case 1://全国各省违法线索情况
                        if($re_level == 0){
                            $map["tregion.flevel"] = 1;
                            $illegal_map["tregion.flevel"] = 1;
                        }elseif($re_level == 1){
                            //是否有权限内的媒体是省级的
                            if(!empty($is_show_sj)){
                                $map['tregion.fid'] = substr($user['regionid'],0,2).'0000';
                                $illegal_map['tregion.fid'] = substr($user['regionid'],0,2).'0000';
                            }else{
                                if($is_include_sub == 1){
                                    $map['tregion.fid'] = ['like',substr($user['regionid'],0,2)."%"];
                                    $illegal_map['tregion.fid'] = ['like',substr($user['regionid'],0,2)."%"];
                                }else{
                                    $map['tregion.fid'] = $regionid;
                                    $illegal_map['tregion.fid'] = $regionid;
                                }

                            }
                        }elseif($re_level < 5 && $re_level > 1 && !empty($is_show_sj)){
                            //是否有权限内的媒体是省级的
                            $map['tregion.fid'] = substr($user['regionid'],0,2).'0000';
                            $illegal_map['tregion.fid'] = substr($user['regionid'],0,2).'0000';
                        }
                        break;
                    case 2:
                        if($re_level == 0){
                            //全国各副省级市违法线索情况
                            $map["tregion.flevel"] = 2;
                            $illegal_map["tregion.flevel"] = 2;
                        }elseif($re_level == 1){
                            //本省各副省级市违法线索情况
                            if($is_include_sub == 1){
                                $map["tregion.flevel"] = ['IN','2,5'];
                                $illegal_map["tregion.flevel"] = ['IN','2,5'];
                                $map['tregion.fid'] = ['like',substr($user['regionid'],0,2)."%"];
                                $illegal_map['tregion.fid'] = ['like',substr($user['regionid'],0,2)."%"];
                            }else{
                                $map["tregion.flevel"] = 2;
                                $illegal_map["tregion.flevel"] = 2;
                                $map["tregion.fpid"] = $regionid;
                                $illegal_map["tregion.fpid"] = $regionid;
                            }
                        }elseif($re_level == 2){

                            if($is_include_sub == 1){
                                $map["tregion.flevel"] = ['IN','2,5'];
                                $illegal_map["tregion.flevel"] = ['IN','2,5'];
                                $map['tregion.fid'] = ['like',substr($user['regionid'],0,4)."%"];
                                $illegal_map['tregion.fid'] = ['like',substr($user['regionid'],0,4)."%"];
                            }else{
                                $map["tregion.flevel"] = 2;
                                $illegal_map["tregion.flevel"] = 2;
                                $map["tregion.fid"] = $regionid;
                                $illegal_map["tregion.fid"] = $regionid;
                            }
                        }
                        break;
                    case 3://各计划单列市
                        if($re_level == 0){
                            //全国各计划单列市违法线索情况
                            $map["tregion.flevel"] = 3;
                            $illegal_map["tregion.flevel"] = 3;
                        }elseif($re_level == 1){
                            //本省各计划单列市违法线索情况

                            if($is_include_sub == 1){

                                $map["tregion.flevel"] = ['IN','3,5'];
                                $illegal_map["tregion.flevel"] = ['IN','3,5'];
                                $map['tregion.fid'] = ['like',substr($user['regionid'],0,2)."%"];
                                $illegal_map['tregion.fid'] = ['like',substr($user['regionid'],0,2)."%"];

                            }else{

                                $map["tregion.flevel"] = 3;
                                $illegal_map["tregion.flevel"] = 3;
                                $map["tregion.fpid"] = $regionid;
                                $illegal_map["tregion.fpid"] = $regionid;
                            }

                        }elseif($re_level == 3){
                            if($is_include_sub == 1){

                                $map["tregion.flevel"] = ['IN','3,5'];
                                $illegal_map["tregion.flevel"] = ['IN','3,5'];
                                $map['tregion.fid'] = ['like',substr($user['regionid'],0,4)."%"];
                                $illegal_map['tregion.fid'] = ['like',substr($user['regionid'],0,4)."%"];

                            }else{

                                $map["tregion.flevel"] = 3;
                                $illegal_map["tregion.flevel"] = 3;
                                $map["tregion.fid"] = $regionid;
                                $illegal_map["tregion.fid"] = $regionid;

                            }

                        }
                        break;
                    case 4://各市
                        if($re_level == 0){
                            //如果是全国各市违法线索情况
                            $map["tregion.flevel"] = 4;
                            $illegal_map["tregion.flevel"] = 4;

                        }elseif($re_level == 1){
                            //本省各市违法线索情况
                            if($is_include_sub == 1){

                                $map["tregion.flevel"] = ['IN','2,3,4,5'];
                                $illegal_map["tregion.flevel"] = ['IN','2,3,4,5'];
                                $map['tregion.fid'] = ['like',substr($user['regionid'],0,2)."%"];
                                $illegal_map['tregion.fid'] = ['like',substr($user['regionid'],0,2)."%"];

                            }else{
                                $map["tregion.flevel"] = ['IN','2,3,4'];
                                $illegal_map["tregion.flevel"] = ['IN','2,3,4'];
                                $map["tregion.fpid"] = $regionid;
                                $illegal_map["tregion.fpid"] = $regionid;
                            }
                        }elseif($re_level == 4){

                            if($is_include_sub == 1){

                                $map["tregion.flevel"] = ['IN','2,3,4,5'];
                                $illegal_map["tregion.flevel"] = ['IN','2,3,4,5'];
                                $map['tregion.fid'] = ['like',substr($user['regionid'],0,4)."%"];
                                $illegal_map['tregion.fid'] = ['like',substr($user['regionid'],0,4)."%"];

                            }else{

                                $map["tregion.flevel"] = ['IN','2,3,4'];
                                $illegal_map["tregion.flevel"] = ['IN','2,3,4'];
                                $map["tregion.fid"] = $regionid;
                                $illegal_map["tregion.fid"] = $regionid;

                            }



                        }elseif(!empty($is_show_sj) && $re_level == 5){
                            //是否有权限内的媒体是市级的
                            $map['tregion.fid'] = substr($user['regionid'],0,4).'00';
                            $illegal_map['tregion.fid'] = substr($user['regionid'],0,4).'00';
                        }
                        break;
                    case 5://各区县
                        if($re_level == 0){
                            //全国各区县违法线索情况
                            $map["tregion.flevel"] = 5;
                            $illegal_map["tregion.flevel"] = 5;
                        }elseif($re_level == 1){
                            //本省各区县违法线索情况
                            $fpid = substr($regionid,0,2);
                            $map["tregion.fpid"] = ['like',$fpid.'%'];
                            $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                            $map["tregion.flevel"] = 5;
                            $illegal_map["tregion.flevel"] = 5;
                        }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                            //本市各区县违法线索情况
                            $fpid = substr($regionid,0,2);
                            $map["tregion.fpid"] = ['like',$fpid.'%'];
                            $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                            $map["tregion.flevel"] = 5;
                            $illegal_map["tregion.flevel"] = 5;

                        }elseif($re_level == 5){
                            $fpid = substr($regionid,0,2);
                            $map["tregion.fpid"] = ['like',$fpid.'%'];
                            $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                            $map["tregion.flevel"] = 5;
                            $illegal_map["tregion.flevel"] = 5;
                        }
                        break;
                }
            }else{
                switch ($region){
                    case 1://全国各省违法线索情况
                        $map["tregion.flevel"] = 1;
                        $illegal_map["tregion.flevel"] = 1;
                        break;
                    case 2:
                        $map["tregion.flevel"] = 2;
                        $illegal_map["tregion.flevel"] = 2;
                        break;
                    case 3://各计划单列市
                        $map["tregion.flevel"] = 3;
                        $illegal_map["tregion.flevel"] = 3;
                        break;
                    case 4://各市
                        $map["tregion.flevel"] = 4;
                        $illegal_map["tregion.flevel"] = 4;
                        break;
                    case 5://各区县
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                        break;
                }
            }

        }else{
            $this->ajaxReturn(
                'Are you stupid? This name is ad_pm_type of this interface must be transmitted 2!'
            );
        }

        //浏览权限设置end


        //媒介类型
        $media_class = I('media_class','');
        if($media_class != ''){

            if($system_num == "120000" && $media_class == "13"){
                $map[$summary_table.'.fmedia_class_code'] = "999999999";//媒体
                $illegal_map['tbn_illegal_ad.fmedia_class'] = 99999999999;
            }else{
                $map[$summary_table.'.fmedia_class_code'] = $media_class;//媒体
                $illegal_map['tbn_illegal_ad.fmedia_class'] = intval($media_class);
            }

        }else{
            if($system_num == "120000"){
                $map[$summary_table.'.fmedia_class_code'] = ['IN',["01","02","03"]];//媒体
                $illegal_map['tbn_illegal_ad.fmedia_class'] = ['IN',[1,2,3]];
            }
        }

        $ad_class = I('ad_class');//广告大类I('ad_class',"")
        if(!empty($ad_class)){
            foreach ($ad_class as $ad_class_key=>$ad_class_val){
                if($ad_class_val < 10){
                    $ad_class[$ad_class_key] = '0'.$ad_class_val;
                }
            }
            $map[$summary_table.'.fad_class_code'] = ['IN',$ad_class];//广告类别
            $illegal_map['left(tbn_illegal_ad.fad_class_code,2)'] = ['IN',$ad_class];//广告类别
        }

        $onther_class = I('onther_class','');//排名条件
        $pmzd = $onther_class;
        if($onther_class == ''){
            if($fztj == 'fad_class_code'){
                //如果是广告类别排名的话,默认条次占比排名
                $onther_class =  'fad_illegal_times';
            }else{
                //如果是其他的话,默认条次评分排名
                $onther_class =  'fad_times';
                $pmzd = 'fad_times';
            }
        }else{
            if($onther_class == 'comprehensive_score'){
                $onther_class = 'fad_count';
                $pmzd = 'fad_count';
            }
        }

        //如果是地域排名 按行政区划排名
        $onther_class_val = $onther_class;
        if($fztj == 'fregionid' && $region_order == 1){
            $onther_class =  'forder';
        }

        $data = [];//定义统计数据空数组

        //按照选定时间实例化表
        $table_model = M($summary_table);

        $id = 'fid';//定义默认表ID
        $name = 'fname';//定义默认单元名称



        //匹配分组条件，连接不同表
        $tregion_name = "tregion.fname1 as tregion_name";
        $group_field = $summary_table.'.'.$fztj;
        $group_by = $summary_table.'.'.$fztj;

        //控制字段
        if($is_manage_media != 1){
            $ill_region_join = "tregion.fid = tbn_illegal_ad.adowner_regionid";
            $xs_group_manage = "tbn_illegal_ad.adowner_regionid";
            $manage_region_join = "tregion.fid = $summary_table.fregionid";
            $manage_id = "$summary_table.fregionid";
        }else{
            $ill_region_join = "tregion.fid = tmedia.manage_region_id";
            $xs_group_manage = "tmedia.manage_region_id";
            $manage_region_join = "tregion.fid = tmedia.manage_region_id";
            $manage_id = "tmedia.manage_region_id";
        }


        switch ($fztj){
            case 'fmedia_class_code':
                $join_table = 'tmediaclass';//fmedia_class_code媒介类别表fid,
                $name = 'fclass';
                $xs_group = 'tbn_illegal_ad.fmedia_class';
                $xs_group_field = 'tbn_illegal_ad.fmedia_class';
                $hb_field = 'fmedia_class';
                break;
            case 'fmediaownerid':
                $join_table = 'tmediaowner';//fmediaownerid媒介机构表fid
                $xs_group = 'tbn_illegal_ad.fmediaownerid';
                $xs_group_field = 'tbn_illegal_ad.fmediaownerid';
                $hb_field = 'fmediaownerid';
                $menu = '媒介机构';
                break;
            case 'fregionid':
                $join_table = 'tregion';//fregionid行政区划表fid
                $hb_field = 'fregion_id';
                $field_name = 'tregion_name';
                $menu = '地域';
                if($region == 0 && $ad_pm_type == 1){
                    $xs_group_field = "CONCAT(left($xs_group_manage,2),'0000') as fregion_id";//1102
                    $xs_group = "left($xs_group_manage,2)";
                    $tregion_name = "(SELECT tregion.fname1 FROM tregion WHERE tregion.fid = CONCAT(left($manage_id,2),'0000') LIMIT 1) AS tregion_name";
                    $group_field = "CONCAT(left($manage_id,2),'0000') as fregionid,".$join_table.".forder";
                    $group_by = "left($manage_id,2)";
                }else{
                    $group_field = $summary_table.'.'.$fztj.','.$join_table.'.forder';
                    $xs_group = $xs_group_manage;
                    $xs_group_field = $xs_group_manage." as fregion_id";
                    if($is_manage_media == 1){
                        $group_field = $manage_id.' as fregionid,'.$join_table.'.forder';
                        $group_by = $manage_id;
                    }
                    if($is_include_sub == 1){
                        switch ($region){

                            case 1:
                                $xs_group_field = "CONCAT(left($xs_group_manage,2),'0000') as fregion_id";//1102
                                $xs_group = "left($xs_group_manage,2)";
                                $tregion_name = "(SELECT tregion.fname1 FROM tregion WHERE tregion.fid = CONCAT(left($manage_id,2),'0000') LIMIT 1) AS tregion_name";
                                $group_field = "CONCAT(left($manage_id,2),'0000') as fregionid,".$join_table.".forder";
                                $group_by = "left($manage_id,2)";
                                break;

                            case 2:
                            case 3:
                            case 4:
                                $xs_group_field = "CONCAT(left($xs_group_manage,4),'00') as fregion_id";//1102
                                $xs_group = "left($xs_group_manage,4)";
                                $tregion_name = "(SELECT tregion.fname1 FROM tregion WHERE tregion.fid = CONCAT(left($manage_id,4),'00') LIMIT 1) AS tregion_name";
                                $group_field = "CONCAT(left($manage_id,4),'00') as fregionid,".$join_table.".forder";
                                $group_by = "left($manage_id,4)";
                                break;
                        }
                    }
                }
                break;
            case 'fmediaid':
                $join_table = 'tmedia';//fmediaid媒介表fid
                $name = 'fmedianame';
                $xs_group = 'tbn_illegal_ad.fmedia_id';
                $xs_group_field = 'tbn_illegal_ad.fmedia_id';
                $hb_field = 'fmedia_id';
                $menu = '媒体';
                break;
            case 'fad_class_code':
                $join_table = 'tadclass';//fad_class_code广告内容类别表fcode
                $id = 'fcode';
                $name = 'fadclass';
                $xs_group = 'left(tbn_illegal_ad.fad_class_code,2)';
                $xs_group_field = 'left(tbn_illegal_ad.fad_class_code,2) as fad_class_code';
                $hb_field = 'fad_class_code';
                $menu = '广告类别';
                $field_name = 'tadclass_name';
                break;
        }
        //违法广告条次与时长
        //是否包含违法长广告时长
        if($is_show_longad != 1){
            //$illegal_map['tbn_illegal_ad.is_long_ad'] = 0;
            $illegal_map['_string'] = '(unix_timestamp(tbn_illegal_ad_issue.fendtime) - unix_timestamp(tbn_illegal_ad_issue.fstarttime) < 600 OR tbn_illegal_ad.fmedia_class = 3)';

        }

        //审核通过的违法数据
        $isexamine = $ALL_CONFIG['isexamine'];
        if(in_array($isexamine, [10,20,30,40,50])){
            $illegal_map['tbn_illegal_ad.fexamine'] = 10;
        }

        $illegal_group = 'tbn_illegal_ad.'.$xs_group;
        $tbn_illegal_data = M('tbn_illegal_ad_issue')
            ->field("
              $xs_group_field,
              COUNT(DISTINCT tbn_illegal_ad.fsample_id) AS fad_illegal_count,
              SUM(
                UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fendtime
                ) - UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fstarttime
                )
                ) AS fad_illegal_play_len,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fstatus2 in(15,17) THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS yla,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fstatus2=16 THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS yja,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fresult like '%责令停止发布%' THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS tzfb,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fresult like '%立案%' THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS lian,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fresult like '%移送司法%' THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS yjsf,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fresult like '%其他%' THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS others,
              COUNT(1) AS fad_illegal_times
            ")
            ->where($illegal_map)
            ->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                tregion.fid = $xs_group_manage"
            )
            ->join("
                tadclass on 
                tadclass.fcode = tbn_illegal_ad.fad_class_code "
            )
            ->group($xs_group)
            ->select();
        $data = $table_model
            ->cache(true,600)
            ->field("
                $group_field,
                tmediaclass.fclass as tmediaclass_name,
                tmediaowner.fname as tmediaowner_name,
                $tregion_name,
                tregion.flevel as flevel,
                 (case when instr(tmedia.fmedianame,'（') > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,'（') -1) else tmedia.fmedianame end) as tmedia_name,
                tadclass.fadclass as tadclass_name,
                $summary_table.fmediaid as fmediaid,
                $summary_table.fmediaownerid as fmediaownerid,
                $summary_table.fmedia_class_code as fmedia_class_code,
                GROUP_CONCAT($fad_count_field SEPARATOR ',') as fad_sam_list,
                $summary_table.fad_class_code as fad_class_code,
                sum($summary_table.$fad_times_field) as fad_times,
                sum($summary_table.$play_len_field) as fad_play_len
           ")
            ->join("
                tmediaclass on 
                tmediaclass.fid = $summary_table.fmedia_class_code"
            )
            ->join("
                tmediaowner on 
                tmediaowner.fid = $summary_table.fmediaownerid"
            )
            ->join("
                tmedia on 
                tmedia.fid = $summary_table.fmediaid"
            )
            ->join("
                tregion on 
                tregion.fid = $manage_id"
            )
            ->join("
                tadclass on 
                tadclass.fcode = $summary_table.fad_class_code"
            )
            ->where($map)
            ->group($group_by)
            ->select();

        //循环计算条数
        foreach ($data as $data_key => $data_val){
            $sam_array = explode(',',$data_val['fad_sam_list']);
            $fad_count = count(array_unique($sam_array));
            unset($data[$data_key]['fad_sam_list']);
            $data[$data_key]['fad_count'] = $fad_count;//插入条数
        }


        //组合违法线索相关数据
        foreach ($tbn_illegal_data as $tbn_illegal_data_key=>$tbn_illegal_data_value){
            foreach ($data as $data_key=>$data_value){
                if($tbn_illegal_data_value[$hb_field] == $data_value[$fztj]){
                    unset($tbn_illegal_data_value[$hb_field]);
                    $data[$data_key]['counts_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_count']/$data_value['fad_count'];//条数违法率
                    $data[$data_key]['fad_illegal_count'] = $tbn_illegal_data_value['fad_illegal_count'];//违法条数
                    $data[$data_key]['times_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_times']/$data_value['fad_times'];//条次违法率
                    $data[$data_key]['lens_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_play_len']/$data_value['fad_play_len'];//时长违法率
                    $data[$data_key] = array_merge($tbn_illegal_data_value, $data[$data_key]);//合并数组
                }
            }
        }

        //循环计算
        foreach ($data as $data_key => $data_val){
            if(!isset($data_val['fad_illegal_count'])){
                $data[$data_key]['fad_illegal_count'] = 0;
            }
            if(!isset($data_val['counts_illegal_rate'])){
                $data[$data_key]['counts_illegal_rate'] = 0;
            }

            if(!isset($data_val['fad_illegal_times'])){
                $data[$data_key]['fad_illegal_times'] = 0;
            }
            if(!isset($data_val['times_illegal_rate'])){
                $data[$data_key]['times_illegal_rate'] = 0;
            }
            if(!isset($data_val['fad_illegal_play_len'])){
                $data[$data_key]['fad_illegal_play_len'] = 0;
            }
            if(!isset($data_val['lens_illegal_rate'])){
                $data[$data_key]['lens_illegal_rate'] = 0;
            }

            if(!isset($data_val['yla'])){
                $data[$data_key]['yla'] = 0;
            }
            if(!isset($data_val['yja'])){
                $data[$data_key]['yja'] = 0;
            }
            if(!isset($data_val['tzfb'])){
                $data[$data_key]['tzfb'] = 0;
            }
            if(!isset($data_val['lian'])){
                $data[$data_key]['lian'] = 0;
            }
            if(!isset($data_val['yjsf'])){
                $data[$data_key]['yjsf'] = 0;
            }
            if(!isset($data_val['others'])){
                $data[$data_key]['others'] = 0;
            }
            $all_count['fad_count'] += $data[$data_key]['fad_count'];
            $all_count['fad_illegal_count'] += $data[$data_key]['fad_illegal_count'];
            $all_count['fad_illegal_times'] += $data[$data_key]['fad_illegal_times'];
            $all_count['fad_times'] += $data[$data_key]['fad_times'];
            $all_count['fad_illegal_play_len'] += $data[$data_key]['fad_illegal_play_len'];
            $all_count['fad_play_len'] += $data[$data_key]['fad_play_len'];
            $all_count['yla'] += $data[$data_key]['yla'];
            $all_count['yja'] += $data[$data_key]['yja'];
            $all_count['tzfb'] += $data[$data_key]['tzfb'];
            $all_count['lian'] += $data[$data_key]['lian'];
            $all_count['yjsf'] += $data[$data_key]['yjsf'];
            $all_count['others'] += $data[$data_key]['others'];
        }



        //上个周期的违法广告条次与时长
        if(!empty($dates)){
            $illegal_map['tbn_illegal_ad_issue.fissue_date'] = [['IN',$dates],['BETWEEN',[$last_s_time,$last_e_time]],'AND'];
        }else{
            $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$last_s_time,$last_e_time]];
        }
        $tbn_prv_illegal_data = M('tbn_illegal_ad_issue')
            ->field("
              $xs_group_field,
              COUNT(DISTINCT tbn_illegal_ad.fsample_id) AS fad_illegal_count,
              SUM(
                UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fendtime
                ) - UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fstarttime
                )
                ) AS fad_illegal_play_len,
              COUNT(1) AS fad_illegal_times
            ")//,COUNT(DISTINCT tbn_illegal_ad.fid) AS fad_illegal_count
            ->where($illegal_map)
            ->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad.fmedia_id"
            )
            ->join("
                tregion on 
                tregion.fid = $xs_group_manage"
            )
            ->join("
                tadclass on 
                tadclass.fcode = tbn_illegal_ad.fad_class_code "
            )
            ->group($xs_group)
            ->select();

        //查询上个周期的数据
        if(!empty($dates)){
            $map[$summary_table.'.fdate'] = [['IN',$dates],['BETWEEN',[$last_s_time,$last_e_time]],'AND'];
        }else{
            $map[$summary_table.'.fdate'] = ['BETWEEN',[$last_s_time,$last_e_time]];
        }
        $illegal_prv_ad_mod = $table_model
            ->cache(true,600)
            ->field("
                $group_field,
                GROUP_CONCAT($fad_count_field SEPARATOR ',') as fad_sam_list,
                $summary_table.fad_class_code as fad_class_code,
                sum($summary_table.fad_times) as fad_times,
                sum($summary_table.$play_len_field) as fad_play_len
            ")
            ->join("
            tmediaclass on 
            tmediaclass.fid = $summary_table.fmedia_class_code"
            )
            ->join("
            tmediaowner on 
            tmediaowner.fid = $summary_table.fmediaownerid"
            )
            ->join("
            tmedia on 
            tmedia.fid = $summary_table.fmediaid"
            )
            ->join("
            tregion on 
            tregion.fid = $manage_id"
            )
            ->join("
            tadclass on 
            tadclass.fcode = $summary_table.fad_class_code"
            )
            ->where($map)
            ->group($group_by)
            ->select();

        //循环计算条数
        foreach ($illegal_prv_ad_mod as $illegal_prv_ad_mod_key => $illegal_prv_ad_mod_val){
            $sam_array = explode(',',$tbn_prv_illegal_data_val['fad_sam_list']);
            $fad_count = count(array_unique($sam_array));
            unset($illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_sam_list']);
            $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_count'] = $fad_count;//插入条数
        }

        //组合上个周期违法线索相关数据
        foreach ($tbn_prv_illegal_data as $tbn_illegal_data_key=>$tbn_illegal_data_value){
            foreach ($illegal_prv_ad_mod as $data_key=>$data_value){
                if($tbn_illegal_data_value[$hb_field] == $data_value[$fztj]){
                    unset($tbn_illegal_data_value[$hb_field]);
                    $illegal_prv_ad_mod[$data_key]['counts_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_count']/$data_value['fad_count'];//条数违法率
                    $illegal_prv_ad_mod[$data_key]['fad_illegal_count'] = $tbn_illegal_data_value['fad_illegal_count'];//违法条数
                    $illegal_prv_ad_mod[$data_key]['times_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_times']/$data_value['fad_times'];//合并数组
                    $illegal_prv_ad_mod[$data_key]['lens_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_play_len']/$data_value['fad_play_len'];//合并数组
                    $illegal_prv_ad_mod[$data_key] = array_merge($tbn_illegal_data_value, $illegal_prv_ad_mod[$data_key]);//合并数组
                }
            }
        }
        foreach ($illegal_prv_ad_mod as $illegal_prv_ad_mod_key => $illegal_prv_ad_mod_val){
            if(!isset($illegal_prv_ad_mod_val['fad_illegal_count'])){

                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_count'] = 0;
            }
            if(!isset($illegal_prv_ad_mod_val['counts_illegal_rate'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['counts_illegal_rate'] = 0;
            }

            if(!isset($illegal_prv_ad_mod_val['fad_illegal_times'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_times'] = 0;
            }

            if(!isset($illegal_prv_ad_mod_val['times_illegal_rate'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['times_illegal_rate'] = 0;
            }
            if(!isset($illegal_prv_ad_mod_val['fad_illegal_play_len'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_play_len'] = 0;
            }
            if(!isset($illegal_prv_ad_mod_val['fad_play_len'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_play_len'] = 0;
            }
            $prv_all_count['fad_count'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_count'];
            $prv_all_count['fad_illegal_count'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_count'];

            $prv_all_count['fad_illegal_times'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_times'];
            $prv_all_count['fad_times'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_times'];
            $prv_all_count['fad_illegal_play_len'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_play_len'];
            $prv_all_count['fad_play_len'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_play_len'];
        }
        //对比上个周期的数据
        foreach ($illegal_prv_ad_mod as $illegal_prv_ad_val) {
            foreach ($data as $data_key=>$data_value) {

                if ($illegal_prv_ad_val[$fztj] == $data_value[$fztj]) {

                    if (!$illegal_prv_ad_val['fad_illegal_count'] || ($data_value['fad_illegal_count'] - $illegal_prv_ad_val['fad_illegal_count']) == 0) {
                        $data[$data_key]['prv_fad_illegal_count'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_count = round(($data_value['fad_illegal_count'] - $illegal_prv_ad_val['fad_illegal_count']) / $illegal_prv_ad_val['fad_illegal_count'], 4) * 100;
                        if ($prv_fad_illegal_count < 0) {
                            $data[$data_key]['prv_fad_illegal_count'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_count . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_count'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_count . '%)';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_times'] || ($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) == 0) {
                        $data[$data_key]['prv_fad_illegal_times'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_times = round(($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) / $illegal_prv_ad_val['fad_illegal_times'], 4) * 100;
                        if ($prv_fad_illegal_times < 0) {
                            $data[$data_key]['prv_fad_illegal_times'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_times . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_times'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_times . '%)';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_play_len'] || ($data_value['fad_illegal_play_len'] - $illegal_prv_ad_val['fad_illegal_play_len']) == 0) {
                        $data[$data_key]['prv_fad_illegal_play_len'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_play_len = round(($data_value['fad_illegal_play_len'] - $illegal_prv_ad_val['fad_illegal_play_len']) / $illegal_prv_ad_val['fad_illegal_play_len'], 4) * 100;

                        if ($prv_fad_illegal_play_len < 0) {
                            $data[$data_key]['prv_fad_illegal_play_len'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_play_len . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_play_len'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_play_len . '%)';
                        }
                    }
                }
            }
        }

        //如果是地域排名 按行政区划排名
        if($fztj == 'fregionid' && $region_order == 1){

            $data = $this->pxsf($data,$onther_class,false);//顺序排序

        }else{

            $data = $this->pxsf($data,$onther_class);//倒叙排序

        }

        $array_num = 0;//定义限定数量
        $data_pm = [];//定义图表数据空数组
        $data_count = count($data);//获取数据条数

        $cl_count_all = 0;//线索全部数量
        $ck_count_all = 0;//线索全部数量
        $count_all = 0;//总数
        $times=0;

        //处理排序后的数据
        foreach ($data as $data_key=>$data_value){
            $data[$data_key]['counts_illegal_rate'] = round($data_value['counts_illegal_rate'],4)*100;
            $data[$data_key]['times_illegal_rate'] = round($data_value['times_illegal_rate'],4)*100;
            $data[$data_key]['lens_illegal_rate'] = round($data_value['lens_illegal_rate'],4)*100;
            //循环获取前35条数据
            if($array_num < 35 && $array_num < count($data) && $data_value[$onther_class] > 0){
                $data_pm['name'][] = $data_value[$join_table.'_name'];
                //不同排名条件显示各自的数值
                switch ($onther_class){
                    case 'forder':
                        $data_pm['num'][] = $data[$data_key][$onther_class_val];
                        break;
                    case 'fad_illegal_times':
                        $data_pm['num'][] = $data_value['fad_illegal_times'];//违法条次
                        break;
                    case 'times_illegal_rate':
                        $data_pm['num'][] = round($data_value['times_illegal_rate'],4)*100;//条次违法率
                        break;
                    case 'fad_times':
                        $data_pm['num'][] = $data[$data_key]['fad_times'];//条次
                        break;
                    case 'fad_illegal_count':
                        $data_pm['num'][] = $data[$data_key]['fad_illegal_count'];//条数
                        break;
                    case 'counts_illegal_rate':
                        $data_pm['num'][] = $data[$data_key]['counts_illegal_rate'];//条数违法率
                        break;
                    case 'fad_count':
                        $data_pm['num'][] = $data[$data_key]['fad_count'];//条数
                        break;
                    case 'fad_play_len':
                        $data_pm['num'][] = $data[$data_key]['fad_play_len'];//时长
                        break;
                    case 'fad_illegal_play_len':
                        $data_pm['num'][] = $data_value['fad_illegal_play_len'];//违法时长
                        break;
                    case 'lens_illegal_rate':
                        $data_pm['num'][] = round($data_value['lens_illegal_rate'],4)*100;//时长违法率
                        break;
                }
                $array_num++;//fad_count  广告条次：fad_times  广告时长：fad_play_len
            }

        }

        /*线索统计模块*/
        //线索查看处理情况tbn_illegal_ad
        //已处理量
        $tbn_illegal_ad_mod = M('tbn_illegal_ad');
        if(!empty($isrelease) || $system_num == '100000'){
            $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
        }
        if(!empty($dates)){
            $illegal_map['tbn_illegal_ad_issue.fissue_date'] = [['IN',$dates],['BETWEEN',[$s_time,$e_time]],'AND'];
        }else{
            $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_time,$e_time]];
        }
        $ycl_count = $tbn_illegal_ad_mod
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as cll,
                $xs_group_field
           ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                tregion.fid = $xs_group_manage"
            )
            ->where($illegal_map)
            ->where(['tbn_illegal_ad.fstatus'=>['gt',0]])
            ->group($xs_group)
            ->select();
        //全部数量
        $cl_count = $tbn_illegal_ad_mod
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as clall,
                $xs_group_field
            ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                tregion.fid = $xs_group_manage"
            )
            ->where($illegal_map)
            ->group($xs_group)
            ->select();
        $xs_data = [];//定义线索统计情况相关数据

        //如果没有被处理的数据
        if(empty($ycl_count)){
            foreach ($cl_count as $cl_val){
                $xs_data[$cl_val[$hb_field]]['cll'] = '0%';
                $xs_data[$cl_val[$hb_field]][$hb_field] = $cl_val[$hb_field];
                $xs_data[$cl_val[$hb_field]]['cl_count'] = 0;
                $xs_data[$cl_val[$hb_field]]['all_count'] = $cl_val['clall'];
            }
        }else{
            //循环获取
            foreach ($ycl_count as $ycl_val){
                foreach ($cl_count as $cl_val){
                    if($ycl_val[$hb_field] == $cl_val[$hb_field]){
                        if($ycl_val['cll'] != 0){
                            $xs_data[$cl_val[$hb_field]][$hb_field] = $cl_val[$hb_field];
                            $xs_data[$cl_val[$hb_field]]['cll'] = (round($ycl_val['cll']/$cl_val['clall'],4)*100).'%';
                            $xs_data[$cl_val[$hb_field]]['cl_count'] = $ycl_val['cll'];
                        }else{
                            $xs_data[$cl_val[$hb_field]]['cll'] = '0%';
                            $xs_data[$cl_val[$hb_field]][$hb_field] = $cl_val[$hb_field];
                            $xs_data[$cl_val[$hb_field]]['cl_count'] = 0;
                        }
                        $xs_data[$cl_val[$hb_field]]['all_count'] = $cl_val['clall'];
                    }
                }
            }
        }
        //已查看量
        $yck_count = $tbn_illegal_ad_mod
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as ckl,
                $xs_group_field
            ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                tregion.fid = $xs_group_manage"
            )
            ->where($illegal_map)
            ->where(['tbn_illegal_ad.fview_status'=>['gt',0]])
            ->group($xs_group)
            ->select();

        //全部数量
        $ck_count = $tbn_illegal_ad_mod
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as ckall,
                $xs_group_field
            ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                tregion.fid = $xs_group_manage"
            )
            ->where($illegal_map)
            ->group($xs_group)
            ->select();

        //如果没有一查看的数量
        if(empty($yck_count)){
            foreach ($ck_count as $ck_val){
                $xs_data[$ck_val[$hb_field]]['ckl'] = '0%';
                $xs_data[$ck_val[$hb_field]]['ck_count'] = 0;
                $xs_data[$ck_val[$hb_field]]['all_count'] = $ck_val['ckall'];
            }
        }else{
            //循环获取
            foreach ($yck_count as $yck_val){
                foreach ($ck_count as $ck_val){
                    if($yck_val[$hb_field] == $ck_val[$hb_field]){
                        if($yck_val['ckl'] != 0){
                            $xs_data[$ck_val[$hb_field]]['ck_count'] = $yck_val['ckl'];
                        }else{
                            $xs_data[$ck_val[$hb_field]]['ck_count'] = 0;
                        }
                        $xs_data[$ck_val[$hb_field]]['ckl'] = (round($yck_val['ckl']/$ck_val['ckall'],4)*100).'%';
                    }
                    $xs_data[$ck_val[$hb_field]]['all_count'] = $ck_val['ckall'];
                }
            }//组建案件查看与处理情况
        }


        //组合线索相关数据
        foreach ($xs_data as $xs_key=>$xs_value){
            foreach ($data as $data_key=>$data_value){
                if($xs_key == $data_value[$fztj]){
                    unset($xs_value[$hb_field]);
                    $cl_count_all += $xs_value['cl_count'];
                    $ck_count_all += $xs_value['ck_count'];
                    $count_all += $xs_value['all_count'];
                    $data[$data_key] = array_merge($xs_value, $data[$data_key]);//合并数组
                }
            }
        }

        //组合广告类别百分比图表数据
        $ad_class_num = 0;
        if($fztj == 'fad_class_code') {
            $data_pm = [];
            $data = $this->pxsf($data, $onther_class);//排序
            foreach ($data as $ad_class_key=>$ad_class_value){
                if($ad_class_value[$onther_class] != 0){
                    $data_pm['data'][] = [
                        'name' => $ad_class_value[$join_table.'_name'],
                        'value' => $ad_class_value[$onther_class]
                    ];
                    $data_pm['name'][] = $ad_class_value[$join_table.'_name'];
                }
            }
        }

        $all_prv_fad_illegal_count = round(($all_count['fad_illegal_count'] - $prv_all_count['fad_illegal_count']) / $prv_all_count['fad_illegal_count'], 4) * 100;
        $all_prv_fad_illegal_play_len = round(($all_count['fad_illegal_play_len'] - $prv_all_count['fad_illegal_play_len']) / $prv_all_count['fad_illegal_play_len'], 4) * 100;
        $all_prv_fad_illegal_times = round(($all_count['fad_illegal_times'] - $prv_all_count['fad_illegal_times']) / $prv_all_count['fad_illegal_times'], 4) * 100;

        $counts_illegal_rate = round($all_count['fad_illegal_count']/$all_count['fad_count'],4)*100;
        $times_illegal_rate = round($all_count['fad_illegal_times']/$all_count['fad_times'],4)*100;
        $lens_illegal_rate = round($all_count['fad_illegal_play_len']/$all_count['fad_play_len'],4)*100;

        $prv_counts_illegal_rate = round($prv_all_count['fad_illegal_count']/$prv_all_count['fad_count'],4)*100;
        $prv_times_illegal_rate = round($prv_all_count['fad_illegal_times']/$prv_all_count['fad_times'],4)*100;
        $prv_lens_illegal_rate = round($prv_all_count['fad_illegal_play_len']/$prv_all_count['fad_play_len'],4)*100;

        if ($all_prv_fad_illegal_count < 0) {
            $all_prv_fad_illegal_count = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_count . '%)';
        } else {
            if($all_prv_fad_illegal_count == 0){
                $all_prv_fad_illegal_count = '(0.00%)';

            }else{
                $all_prv_fad_illegal_count = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_count . '%)';
            }
        }

        if ($all_prv_fad_illegal_play_len < 0) {
            $all_prv_fad_illegal_play_len = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_play_len . '%)';
        } else {
            if($all_prv_fad_illegal_play_len == 0){
                $all_prv_fad_illegal_play_len = '(0.00%)';

            }else{
                $all_prv_fad_illegal_play_len = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_play_len . '%)';
            }
        }

        if ($all_prv_fad_illegal_times < 0) {
            $all_prv_fad_illegal_times = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_times . '%)';
        } else {
            if($all_prv_fad_illegal_times == 0){
                $all_prv_fad_illegal_times = '(0.00%)';

            }else{
                $all_prv_fad_illegal_times = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_times . '%)';
            }
        }

        $compar_fad_count = $all_count['fad_count'] - $prv_all_count['fad_count'];
        if ($compar_fad_count < 0) {
            $compar_fad_count = '减少'.($compar_fad_count*-1);
        } else {
            if($compar_fad_count == 0){
                $compar_fad_count = '无变化';

            }else{
                $compar_fad_count = '增加'.$compar_fad_count;
            }
        }
        $compar_fad_illegal_count = $all_count['fad_illegal_count'] - $prv_all_count['fad_illegal_count'];
        if ($compar_fad_illegal_count < 0) {
            $compar_fad_illegal_count = '减少'.($compar_fad_illegal_count*-1);
        } else {
            if($compar_fad_illegal_count == 0){
                $compar_fad_illegal_count = '无变化';

            }else{
                $compar_fad_illegal_count = '增加'.$compar_fad_illegal_count;
            }
        }
        $compar_illegal_count_rate = $counts_illegal_rate - $prv_counts_illegal_rate;
        if ($compar_illegal_count_rate < 0) {
            $compar_illegal_count_rate = '下降'.($compar_illegal_count_rate*-1).'%';
        } else {
            if($compar_illegal_count_rate == 0){
                $compar_illegal_count_rate = '无变化';

            }else{
                $compar_illegal_count_rate = '上升'.$compar_illegal_count_rate.'%';
            }
        }
        $compar_fad_times = $all_count['fad_times'] - $prv_all_count['fad_times'];
        if ($compar_fad_times < 0) {
            $compar_fad_times = '减少'.($compar_fad_times*-1);
        } else {
            if($compar_fad_times == 0){
                $compar_fad_times = '无变化';

            }else{
                $compar_fad_times = '增加'.$compar_fad_times;
            }
        }
        $compar_fad_illegal_times = $all_count['fad_illegal_times'] - $prv_all_count['fad_illegal_times'];
        if ($compar_fad_illegal_times < 0) {
            $compar_fad_illegal_times = '减少'.($compar_fad_illegal_times*-1);
        } else {
            if($compar_fad_illegal_times == 0){
                $compar_fad_illegal_times = '无变化';

            }else{
                $compar_fad_illegal_times = '增加'.$compar_fad_illegal_times;
            }
        }
        $compar_illegal_times_rate = $times_illegal_rate - $prv_times_illegal_rate;
        if ($compar_illegal_times_rate < 0) {
            $compar_illegal_times_rate = '下降'.($compar_illegal_times_rate*-1).'%';
        } else {
            if($compar_illegal_times_rate == 0){
                $compar_illegal_times_rate = '无变化';
            }else{
                $compar_illegal_times_rate = '上升'.$compar_illegal_times_rate.'%';
            }
        }
        $compar_fad_play_len = $all_count['fad_play_len'] - $prv_all_count['fad_play_len'];
        if ($compar_fad_play_len < 0) {
            $compar_fad_play_len = '减少'.($compar_fad_play_len*-1);
        } else {
            if($compar_fad_play_len == 0){
                $compar_fad_play_len = '无变化';

            }else{
                $compar_fad_play_len = '增加'.$compar_fad_play_len;
            }
        }
        $compar_fad_illegal_play_len = $all_count['fad_illegal_play_len'] - $prv_all_count['fad_illegal_play_len'];
        if ($compar_fad_illegal_play_len < 0) {
            $compar_fad_illegal_play_len = '减少'.($compar_fad_illegal_play_len*-1);
        } else {
            if($compar_fad_illegal_play_len == 0){
                $compar_fad_illegal_play_len = '无变化';

            }else{
                $compar_fad_illegal_play_len = '增加'.$compar_fad_illegal_play_len;
            }
        }
        $compar_illegal_play_len_rate = $lens_illegal_rate - $prv_lens_illegal_rate;
        if ($compar_illegal_play_len_rate < 0) {
            $compar_illegal_play_len_rate = '下降'.($compar_illegal_play_len_rate*-1).'%';
        } else {
            if($compar_illegal_play_len_rate == 0){
                $compar_illegal_play_len_rate = '无变化';
            }else{
                $compar_illegal_play_len_rate = '上升'.$compar_illegal_play_len_rate.'%';
            }
        }
        $compar_prv_com_score = round($all_count['com_score'] - $prv_all_count['prv_com_score'],2);
        if ($compar_prv_com_score < 0) {
            $compar_prv_com_score = '减少'.($compar_prv_com_score*-1);
        } else {
            if($compar_prv_com_score == 0){
                $compar_prv_com_score = '无变化';

            }else{
                $compar_prv_com_score = '增加'.$compar_prv_com_score;
            }
        }

        /*
        已立案 yla  fstatus = 10 and fstatus2 in(15,17)
        已结案 yja fstatus = 10 and fstatus2=16
        责令停止发布 tzfb fstatus = 10 and fresult like '%责令停止发布%'
        立案 lian fstatus = 10 and fresult like '%立案%'
        移交司法机关 yjsf fstatus = 10 and fresult like '%移送司法%'
        其他  others fstatus = 10 and fresult like '%其他%'
         * */

        //统计放在最后一条$all_count     $prv_all_count
        $data[] = [
            'all_xs_count' =>$count_all,
            'titlename'=>'全部',
            "cll"=>(round($cl_count_all/$count_all,4)*100).'%',
            "cl_count" =>$cl_count_all,
            "ck_count"=>$ck_count_all,
            "yla"=>$all_count['yla'],
            "yja"=>$all_count['yja'],
            "tzfb"=>$all_count['tzfb'],
            "lian"=>$all_count['lian'],
            "yjsf"=>$all_count['yjsf'],
            "others"=>$all_count['others'],
            "ckl"=>(round($ck_count_all/$count_all,4)*100).'%',
            "prv_fad_illegal_count"=>$all_prv_fad_illegal_count,
            "prv_fad_illegal_play_len"=>$all_prv_fad_illegal_play_len,
            "prv_fad_illegal_times"=>$all_prv_fad_illegal_times,

            'fad_count'=>$all_count['fad_count'],//广告条数
            'fad_illegal_count'=>$all_count['fad_illegal_count'],//违法广告数量
            'counts_illegal_rate'=>round($all_count['fad_illegal_count']/$all_count['fad_count'],4)*100,
            'fad_times'=>$all_count['fad_times'],//广告条次
            'fad_illegal_times'=>$all_count['fad_illegal_times'],//违法广告条次
            'times_illegal_rate'=>round($all_count['fad_illegal_times']/$all_count['fad_times'],4)*100,
            'fad_play_len'=>$all_count['fad_play_len'],//总时长
            'fad_illegal_play_len'=>$all_count['fad_illegal_play_len'],//违法时长
            'lens_illegal_rate'=>round($all_count['fad_illegal_play_len']/$all_count['fad_play_len'],4)*100,
            "com_score"=>round($all_count['com_score'],2),//综合分

            "compar_fad_count"=>$compar_fad_count,//对比上个周期广告条数
            "compar_fad_illegal_count"=>$compar_fad_illegal_count,//对比上个周期违法广告数量
            "compar_illegal_count_rate"=>$compar_illegal_count_rate,//对比上个周期条数违法率
            "compar_fad_times"=>$compar_fad_times,//对比上个周期广告条次
            "compar_fad_illegal_times"=>$compar_fad_illegal_times,//对比上个周期违法广告条次
            "compar_illegal_times_rate"=>$compar_illegal_times_rate,//对比上个周期条次违法率
            "compar_fad_play_len"=>$compar_fad_play_len,//对比上个周期总时长
            "compar_fad_illegal_play_len"=>$compar_fad_illegal_play_len,//对比上个周期违法时长
            "compar_illegal_play_len_rate"=>$compar_illegal_play_len_rate,//对比上个周期时长违法率
            "compar_prv_com_score"=>$compar_prv_com_score,//对比上个周期综合分
        ];

        //是否展示条数相关
        /*        if(!$isshow_count){
                    foreach ($data as $data_key=>$data_val){
                        unset($data[$data_key]['fad_count']);
                        unset($data[$data_key]['fad_illegal_count']);
                        unset($data[$data_key]['counts_illegal_rate']);
                    }
                }*/

        $finally_data = [
            'count'=> count($data)-1,
            'data_pm'=>$data_pm,//图表数据
            'data_list'=>$data//列表数据
        ];

        if($is_out_report == 1){

            if($is_have_count == 1){
                //如果是线索菜单并且是点击了导出按钮
                $objPHPExcel = new \PHPExcel();
                $objPHPExcel
                    ->getProperties()  //获得文件属性对象，给下文提供设置资源
                    ->setCreator( "MaartenBalliauw")             //设置文件的创建者
                    ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
                    ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
                    ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
                    ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
                    ->setKeywords( "office 2007 openxmlphp")        //设置标记
                    ->setCategory( "Test resultfile");                //设置类别

                $sheet = $objPHPExcel->setActiveSheetIndex(0);
                //分组导出数据1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
                switch ($fztj){
                    case 'fmediaid':
                        $sheet ->mergeCells("D1:F1");
                        $sheet ->mergeCells("G1:I1");
                        $sheet ->mergeCells("J1:L1");
                        $sheet ->mergeCells("M1:P1");
                        $sheet ->setCellValue('A1','');
                        $sheet ->setCellValue('B1','');
                        $sheet ->setCellValue('C1','');
                        $sheet ->setCellValue('D1','');
                        $sheet ->setCellValue('E1','广告条数');
                        $sheet ->setCellValue('H1','广告条次');
                        $sheet ->setCellValue('K1','广告时长');
                        $sheet ->setCellValue('N1','线索查看及处理情况');

                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','媒体所在地');
                        $sheet ->setCellValue('C2','媒体名称');
                        $sheet ->setCellValue('D2','总条数');
                        $sheet ->setCellValue('E2','违法条数');
                        $sheet ->setCellValue('F2','条数违法率');
                        $sheet ->setCellValue('G2','总条次');
                        $sheet ->setCellValue('H2','违法条次');
                        $sheet ->setCellValue('I2','条次违法率');
                        $sheet ->setCellValue('J2','总时长');
                        $sheet ->setCellValue('K2','违法时长');
                        $sheet ->setCellValue('L2','时长违法率');
                        $sheet ->setCellValue('M2','查看量');
                        $sheet ->setCellValue('N2','查看率');
                        $sheet ->setCellValue('O2','处理量');
                        $sheet ->setCellValue('P2','处理率');

                        foreach ($data as $key => $value) {

                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                                $sheet ->setCellValue('C'.($key+3),'全部媒体');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                                $sheet ->setCellValue('C'.($key+3),$value['tmedia_name']);
                            }

                            $sheet ->setCellValue('D'.($key+3),$value['fad_count']);
                            $sheet ->setCellValue('E'.($key+3),$value['fad_illegal_count']);
                            $sheet ->setCellValue('F'.($key+3),$value['counts_illegal_rate'].'%');
                            $sheet ->setCellValue('G'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('H'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('I'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('j'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('K'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('L'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('M'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('N'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('O'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('P'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fregionid':
                        $sheet ->mergeCells("C1:E1");
                        $sheet ->mergeCells("F1:H1");
                        $sheet ->mergeCells("I1:K1");
                        $sheet ->mergeCells("L1:O1");
                        $sheet ->setCellValue('A1','');
                        $sheet ->setCellValue('B1','');
                        $sheet ->setCellValue('C1','广告条数');
                        $sheet ->setCellValue('F1','广告条次');
                        $sheet ->setCellValue('I1','广告时长');
                        $sheet ->setCellValue('L1','线索查看及处理情况');
                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','地域名称');
                        $sheet ->setCellValue('C2','总条数');
                        $sheet ->setCellValue('D2','违法条数');
                        $sheet ->setCellValue('E2','条数违法率');
                        $sheet ->setCellValue('F2','总条次');
                        $sheet ->setCellValue('G2','违法条次');
                        $sheet ->setCellValue('H2','条次违法率');
                        $sheet ->setCellValue('I2','总时长');
                        $sheet ->setCellValue('J2','违法时长');
                        $sheet ->setCellValue('K2','时长违法率');
                        $sheet ->setCellValue('L2','查看量');
                        $sheet ->setCellValue('M2','查看率');
                        $sheet ->setCellValue('N2','处理量');
                        $sheet ->setCellValue('O2','处理率');

                        foreach ($data as $key => $value) {


                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                            }

                            $sheet ->setCellValue('C'.($key+3),$value['fad_count']);
                            $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_count']);
                            $sheet ->setCellValue('E'.($key+3),$value['counts_illegal_rate'].'%');
                            $sheet ->setCellValue('F'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('H'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('I'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('J'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('K'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('L'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('M'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('N'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('O'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fmediaownerid':
                        $sheet ->mergeCells("E1:G1");
                        $sheet ->mergeCells("H1:J1");
                        $sheet ->mergeCells("K1:M1");
                        $sheet ->mergeCells("N1:Q1");
                        $sheet ->setCellValue('A1','');
                        $sheet ->setCellValue('B1','');
                        $sheet ->setCellValue('C1','');
                        $sheet ->setCellValue('D1','');
                        $sheet ->setCellValue('E1','广告条数');
                        $sheet ->setCellValue('H1','广告条次');
                        $sheet ->setCellValue('K1','广告时长');
                        $sheet ->setCellValue('N1','线索查看及处理情况');

                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','媒介机构所在地');
                        $sheet ->setCellValue('C2','媒介机构名称');
                        $sheet ->setCellValue('D2','媒介类型');
                        $sheet ->setCellValue('E2','总条数');
                        $sheet ->setCellValue('F2','违法条数');
                        $sheet ->setCellValue('G2','条数违法率');
                        $sheet ->setCellValue('H2','总条次');
                        $sheet ->setCellValue('I2','违法条次');
                        $sheet ->setCellValue('J2','条次违法率');
                        $sheet ->setCellValue('K2','总时长');
                        $sheet ->setCellValue('L2','违法时长');
                        $sheet ->setCellValue('M2','时长违法率');
                        $sheet ->setCellValue('N2','查看量');
                        $sheet ->setCellValue('O2','查看率');
                        $sheet ->setCellValue('P2','处理量');
                        $sheet ->setCellValue('Q2','处理率');

                        foreach ($data as $key => $value) {

                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                                $sheet ->setCellValue('C'.($key+3),'全部媒介机构');
                                $sheet ->setCellValue('D'.($key+3),'全部类型');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                                $sheet ->setCellValue('C'.($key+3),$value['tmediaowner_name']);
                                $sheet ->setCellValue('D'.($key+3),$value['tmediaclass_name']);
                            }

                            $sheet ->setCellValue('E'.($key+3),$value['fad_count']);
                            $sheet ->setCellValue('F'.($key+3),$value['fad_illegal_count']);
                            $sheet ->setCellValue('G'.($key+3),$value['counts_illegal_rate'].'%');
                            $sheet ->setCellValue('H'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('I'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('J'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('K'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('L'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('M'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('N'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('O'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('P'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('Q'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fad_class_code':
                        $sheet ->mergeCells("C1:E1");
                        $sheet ->mergeCells("F1:H1");
                        $sheet ->mergeCells("I1:K1");
                        $sheet ->mergeCells("L1:O1");
                        $sheet ->setCellValue('A1','');
                        $sheet ->setCellValue('B1','');
                        $sheet ->setCellValue('C1','广告条数');
                        $sheet ->setCellValue('F1','广告条次');
                        $sheet ->setCellValue('I1','广告时长');
                        $sheet ->setCellValue('L1','线索查看及处理情况');

                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','广告类别');
                        $sheet ->setCellValue('C2','总条数');
                        $sheet ->setCellValue('D2','违法条数');
                        $sheet ->setCellValue('E2','条数违法率');
                        $sheet ->setCellValue('F2','总条次');
                        $sheet ->setCellValue('G2','违法条次');
                        $sheet ->setCellValue('H2','条次违法率');
                        $sheet ->setCellValue('I2','总时长');
                        $sheet ->setCellValue('J2','违法时长');
                        $sheet ->setCellValue('K2','时长违法率');
                        $sheet ->setCellValue('L2','查看量');
                        $sheet ->setCellValue('M2','查看率');
                        $sheet ->setCellValue('N2','处理量');
                        $sheet ->setCellValue('O2','处理率');

                        foreach ($data as $key => $value) {


                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部类别');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tadclass_name']);
                            }

                            $sheet ->setCellValue('C'.($key+3),$value['fad_count']);
                            $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_count']);
                            $sheet ->setCellValue('E'.($key+3),$value['counts_illegal_rate'].'%');
                            $sheet ->setCellValue('F'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('H'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('I'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('J'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('K'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('L'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('M'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('N'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('O'.($key+3),$value['cll']);

                        }
                        break;
                }
                $objActSheet =$objPHPExcel->getActiveSheet();
                $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
                $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

                $date = date('Ymdhis');
                $savefile = './Public/Xls/'.$date.'.xlsx';
                $savefile2 = '/Public/Xls/'.$date.'.xlsx';
                $objWriter->save($savefile);
                //即时导出下载
                /*          header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                            header('Content-Disposition:attachment;filename="'.$date.'.xlsx"');
                            header('Cache-Control:max-age=0');
                            $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
                            $objWriter->save( 'php://output');*/
                $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
                unlink($savefile);//删除文件
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
            }else{
                //如果是线索菜单并且是点击了导出按钮
                $objPHPExcel = new \PHPExcel();
                $objPHPExcel
                    ->getProperties()  //获得文件属性对象，给下文提供设置资源
                    ->setCreator( "MaartenBalliauw")             //设置文件的创建者
                    ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
                    ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
                    ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
                    ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
                    ->setKeywords( "office 2007 openxmlphp")        //设置标记
                    ->setCategory( "Test resultfile");                //设置类别

                $sheet = $objPHPExcel->setActiveSheetIndex(0);
                //分组导出数据1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
                switch ($fztj){
                    case 'fmediaid':
                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','媒体所在地');
                        $sheet ->setCellValue('C2','媒体名称');
                        $sheet ->setCellValue('D2','总条次');
                        $sheet ->setCellValue('E2','违法条次');
                        $sheet ->setCellValue('F2','条次违法率');
                        $sheet ->setCellValue('G2','总时长');
                        $sheet ->setCellValue('H2','违法时长');
                        $sheet ->setCellValue('I2','时长违法率');
                        $sheet ->setCellValue('J2','查看量');
                        $sheet ->setCellValue('K2','查看率');
                        $sheet ->setCellValue('L2','处理量');
                        $sheet ->setCellValue('M2','处理率');
                        foreach ($data as $key => $value) {

                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                                $sheet ->setCellValue('C'.($key+3),'全部媒体');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                                $sheet ->setCellValue('C'.($key+3),$value['tmedia_name']);
                            }
                            $sheet ->setCellValue('D'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('E'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('F'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('G'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('H'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('I'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('J'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('K'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('L'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('M'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fregionid':
                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','地域名称');
                        $sheet ->setCellValue('C2','总条次');
                        $sheet ->setCellValue('D2','违法条次');
                        $sheet ->setCellValue('E2','条次违法率');
                        $sheet ->setCellValue('F2','总时长');
                        $sheet ->setCellValue('G2','违法时长');
                        $sheet ->setCellValue('H2','时长违法率');
                        $sheet ->setCellValue('I2','查看量');
                        $sheet ->setCellValue('J2','查看率');
                        $sheet ->setCellValue('K2','处理量');
                        $sheet ->setCellValue('L2','处理率');

                        foreach ($data as $key => $value) {


                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                            }
                            $sheet ->setCellValue('C'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('E'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('F'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('H'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('I'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('J'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('K'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('L'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fmediaownerid':
                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','媒介机构所在地');
                        $sheet ->setCellValue('C2','媒介机构名称');
                        $sheet ->setCellValue('D2','媒介类型');
                        $sheet ->setCellValue('E2','总条次');
                        $sheet ->setCellValue('F2','违法条次');
                        $sheet ->setCellValue('G2','条次违法率');
                        $sheet ->setCellValue('H2','总时长');
                        $sheet ->setCellValue('I2','违法时长');
                        $sheet ->setCellValue('J2','时长违法率');
                        $sheet ->setCellValue('K2','查看量');
                        $sheet ->setCellValue('L2','查看率');
                        $sheet ->setCellValue('M2','处理量');
                        $sheet ->setCellValue('N2','处理率');

                        foreach ($data as $key => $value) {

                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                                $sheet ->setCellValue('C'.($key+3),'全部媒介机构');
                                $sheet ->setCellValue('D'.($key+3),'全部类型');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                                $sheet ->setCellValue('C'.($key+3),$value['tmediaowner_name']);
                                $sheet ->setCellValue('D'.($key+3),$value['tmediaclass_name']);
                            }
                            $sheet ->setCellValue('E'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('F'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('G'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('H'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('I'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('J'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('K'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('L'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('M'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('N'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fad_class_code':
                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','广告类别');
                        $sheet ->setCellValue('C2','总条次');
                        $sheet ->setCellValue('D2','违法条次');
                        $sheet ->setCellValue('E2','条次违法率');
                        $sheet ->setCellValue('F2','总时长');
                        $sheet ->setCellValue('G2','违法时长');
                        $sheet ->setCellValue('H2','时长违法率');
                        $sheet ->setCellValue('I2','查看量');
                        $sheet ->setCellValue('J2','查看率');
                        $sheet ->setCellValue('K2','处理量');
                        $sheet ->setCellValue('L2','处理率');

                        foreach ($data as $key => $value) {
                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部类别');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tadclass_name']);
                            }
                            $sheet ->setCellValue('C'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('E'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('F'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('H'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('I'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('J'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('K'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('L'.($key+3),$value['cll']);
                        }
                        break;
                }
                $objActSheet =$objPHPExcel->getActiveSheet();
                $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
                $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

                $date = date('Ymdhis');
                $savefile = './Public/Xls/'.$date.'.xlsx';
                $savefile2 = '/Public/Xls/'.$date.'.xlsx';
                $objWriter->save($savefile);
                //即时导出下载
                /*          header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                            header('Content-Disposition:attachment;filename="'.$date.'.xlsx"');
                            header('Cache-Control:max-age=0');
                            $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
                            $objWriter->save( 'php://output');*/
                $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
                unlink($savefile);//删除文件
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
            }

        }elseif($is_out_report == 2){
            $this->jchz_out_word($menu,$field_name,$title,$data,$date_cycle,$s_time,$e_time,$regio_area);
        }else{
            $this->ajaxReturn($finally_data);
        }

    }

    /**
     * 违法条数列表
     * by zw
     */
    public function tiaoshu_list(){
        session_write_close();
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isrelease = $ALL_CONFIG['isrelease'];
        $ischeck = $ALL_CONFIG['ischeck'];
        $is_manage_media = $ALL_CONFIG['set_manage_region'];//是否通过指定区域管理媒体统计

        $user = session('regulatorpersonInfo');//获取用户信息
        $fad_class_code   = I('fad_class_code');//广告类别，用于汇总中违法广告数据的查看
        $fmediaid         = I('fmediaid');//媒体ID，用于汇总中违法广告数据的查看
        $fmediaownerid    = I('fmediaownerid');//媒体机构ID，用于汇总中违法广告数据的查看
        $mclass           = I('mclass');//媒体类别，用于汇总中违法广告数据的查看
        $fad_class_code2  = I('fad_class_code2');//广告类别组，用于汇总中违法广告数据的查看
        $flevel           = I('flevel');//地域级别，用于汇总中违法广告数据的查看
        $is_show_longad   = I('is_show_longad');//是否有长广告，用于汇总中违法广告数据的查看
        $is_show_fsend    = I('is_show_fsend');//是否包含确认广告，1包含未发布，其他值已发布
        $is_have_self    = I('is_have_self',0);//是否包含本级
        $is_include_sub    = I('is_include_sub',0);//是否包含下级
        $ad_pm_type = I('ad_pm_type',2);
        $outtype = I('outtype');//导出类型
        if(!empty($outtype)){
            $p  = I('page', 1);//当前第几页ks
            $pp = 50000;//每页显示多少记录
        }else{
            $p  = I('page', 1);//当前第几页ks
            $pp = 10;//每页显示多少记录
        }
        $area = I('area');

        //时间条件筛选
        $years      = I('year');//选择年份
        $times_table = I('times_table');
        $table_condition = I('table_condition');//选择时间
        if($times_table == 'tbn_ad_summary_day'){
            $timetypes = 0;
            $timeval = $table_condition;
        }elseif($times_table == 'tbn_ad_summary_week'){
            $timetypes = 10;
            $timeval = $table_condition;
        }elseif($times_table == 'tbn_ad_summary_half_month'){
            $timetypes = 20;
            $table_condition2 = explode('-', $table_condition);
            $table_condition3 = ((int)$table_condition2[0]-1)*2;
            $table_condition4 = ((int)$table_condition2[1])==1?1:2;
            $timeval = $table_condition3+$table_condition4;
        }elseif($times_table == 'tbn_ad_summary_month'){
            $timetypes = 30;
            $table_condition2 = explode('-', $table_condition);
            $timeval = (int)$table_condition2[0];
        }elseif($times_table == 'tbn_ad_summary_quarter'){
            $timetypes = 40;
            if($table_condition == '01-01'){
                $timeval = 1;
            }elseif($table_condition == '04-01'){
                $timeval = 2;
            }elseif($table_condition == '07-01'){
                $timeval = 3;
            }else{
                $timeval = 4;
            }
        }elseif($times_table == 'tbn_ad_summary_half_year'){
            $timetypes = 50;
            $table_condition2 = explode('-', $table_condition);
            if($table_condition2[2] == '01'){
                $timeval = 1;
            }else{
                $timeval = 2;
            }
        }elseif($times_table == 'tbn_ad_summary_year'){
            $timetypes = 60;
            $timeval = 1;
        }
        if($is_show_fsend==1){
            $is_show_fsend = -2;
        }else{
            $is_show_fsend = 0;
        }
        $iszjdata = I('iszjdata')?I('iszjdata'):0;//是否总局数据
        if(!empty($iszjdata)){
            $system_num = 100000;
            $is_show_fsend = 2;
        }else{
            $system_num = $system_num;
        }
        $where_tia['tn.fid'] = ['like',trim($user['regionid'],0).'%'];

        $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date',$is_show_fsend,$isrelease);

        $search_type  = I('search_type');//搜索类别
        $search_val   = I('search_val');//搜索值
        if(!empty($search_val)){
            if($search_type == 10){//媒体分类
                $where_tia['a.fmedia_class'] = $search_val;
            }elseif($search_type == 20){//广告类别
                $where_tia['b.ffullname'] = array('like','%'.$search_val.'%');
            }elseif($search_type == 30){//广告名称
                $where_tia['a.fad_name'] = array('like','%'.$search_val.'%');
            }elseif($search_type == 40){//发布媒体
                $where_tia['c.fmedianame'] = array('like','%'.$search_val.'%');
            }elseif($search_type == 50){//违法原因
                $where_tia['a.fillegal'] = array('like','%'.$search_val.'%');
            }
        }

        if(!empty($mclass)){
            $where_tia['a.fmedia_class'] = $mclass;
        }

        //国家局系统只显示有打国家局标签媒体的数据
        if($system_num == '100000'){
            $wherestr = ' and tn.flevel in (1,2,3)';
        }

        if(!empty($flevel)){
            $wherestr = ' and tn.flevel in('.$flevel.')';
        }

        if(!empty($fad_class_code2)){
            $fadclass = [];
            foreach ($fad_class_code2 as $key => $value) {
                if($value<10){
                    $fadclass[] = '0'.$value;
                }else{
                    $fadclass[] = $value;
                }
            }
            $where_tia['left(a.fad_class_code,2)'] = array('in',$fadclass);
        }

        if((!empty($fmediaownerid) || !empty($fmediaid) || !empty($fad_class_code)) && empty($area)){
            if($system_num == '100000' && session('regulatorpersonInfo.fregulatorlevel')==30){
                $area = '100000';
            }else{
                $area = session('regulatorpersonInfo.regionid');
            }
        }


        if($is_manage_media == 1){
            $region_field = 'c.manage_region_id';
            $map_region_field = 'tn.fid';//a.fregion_id
        }else{
            $region_field = 'adowner_regionid';
            $map_region_field = 'a.adowner_regionid';//
        }


        if(!empty($area)){//所属地区
            if($area != '100000'){
                if(($flevel==1 || $flevel == 2 || $flevel==3 || $flevel==4 || $flevel==5) && is_numeric($flevel)){

                    $where_tia[$map_region_field]  = $area;
                }else{
                    $where_tia["left($map_region_field,2)"] = substr($area, 0,2);
                }
            }
        }

        if(!empty($fmediaid)){//媒体排名查询
            $where_tia['a.fmedia_id'] = $fmediaid;
            $wherestr = ' and tn.flevel in (1,2,3,4,5)';
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==0 && $flevel == 5){
                $where_tia[$map_region_field]  = $area;
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==10&&$flevel == 5){
                $where_tia[$map_region_field]  = array('like',substr($area, 0,4).'%');
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==20&&$flevel == 4){
                $where_tia[$map_region_field]  = array('like',substr($area, 0,2).'%');
            }
        }

        if(!empty($fmediaownerid)){//机构排名查询
            $where_tia['a.fmediaownerid'] = $fmediaownerid;
            $wherestr = ' and tn.flevel in (1,2,3,4,5)';
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==0 && $flevel == 5){
                $where_tia[$map_region_field]  = $area;
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==10&&$flevel == 5){
                $where_tia[$map_region_field]  = array('like',substr($area, 0,4).'%');
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==20&&$flevel == 4){
                $where_tia[$map_region_field]  = array('like',substr($area, 0,2).'%');
            }
        }

        if(!empty($fad_class_code)){//广告类别排名查询
            $where_tia['left(a.fad_class_code,2)'] = $fad_class_code;
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==0 && $flevel == 5){
                $where_tia[$map_region_field]  = $area;
            }

            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==10&&$flevel == 5){
                if(!empty($is_have_self)){
                    $wherestr = ' and tn.flevel in (2,3,4,5)';
                }else{
                    $wherestr = ' and tn.flevel = 5';
                }
                $where_tia[$map_region_field]  = array('like',substr($area, 0,4).'%');
            }

            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==20&&$flevel == 4){
                if(!empty($is_have_self)){
                    $wherestr = ' and tn.flevel in (1,2,3,4)';
                }else{
                    $wherestr = ' and tn.flevel in (2,3,4)';
                }
                $where_tia[$map_region_field]  = array('like',substr($area, 0,2).'%');
            }

        }

        $where_tia['a.fcustomer']  = $system_num;
        //是否抽查模式
        if(!empty($ischeck)){
            $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
            //如果抽查表有数据
            if(!empty($spot_check_data)){
                $dates = [];//定义日期数组
                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                    $cc_condition = trim($spot_check_data_val['condition']);
                    if(!empty($cc_condition)){
                        $year_month = '';
                        $date_str = [];
                        $year_month = substr($spot_check_data_val['fmonth'],0,7);
                        if(!empty($spot_check_data_val['condition'])){
                            $date_str = explode(',',$spot_check_data_val['condition']);
                        }
                        foreach ($date_str as $date_str_val){
                            $dates[] = $year_month.'-'.$date_str_val;
                        }
                    }

                }

                $where_time  .= ' and a.fissue_date in ("'.implode('","', $dates).'")';
            }else{
                $where_time  .= ' and 1=0';
            }
        }

        if($is_show_longad === '0'){
            $where_time .= ' and (TIMESTAMPDIFF(SECOND,a.fstarttime,a.fendtime)<600 or a.fmedia_class>2)';
        }

        //审核通过的违法数据
        $isexamine = $ALL_CONFIG['isexamine'];
        if(in_array($isexamine, [10,20,30,40,50])){
            $where_tia['a.fexamine'] = 10;
        }


        if($ad_pm_type == 2){
            $region = I('region2','');//区域
            if($is_include_sub == 1 && $region != 5 && $region != 0){
                switch ($region){
                    case 1:
                        $where_tia[$map_region_field] = ['like',substr($area,0,2).'%'];
                        $wherestr = ' and tn.flevel in (1,2,3,4,5)';
                        break;
                    case 2:
                    case 3:
                    case 4:
                        $where_tia[$map_region_field] = ['like',substr($area,0,4).'%'];
                        $wherestr = ' and tn.flevel in (2,3,4,5)';
                        break;
                }
            }
        }

        $where_tia['a.fstatus3'] = ['NOT IN',[30,40]];
        $where_tia['a.identify'] = 'new_2';
        $count = M('tbn_illegal_ad')
            ->alias('a')
            ->join('tadclass b on a.fad_class_code=b.fcode')
            ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
            ->join('tregion tn on '.$region_field.' = tn.fid'.$wherestr)
            ->join('(select count(*) as fcount,max(fillegal_ad_id) as fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue a,tbn_illegal_ad b where '.$where_time.' and a.fillegal_ad_id=b.fid and b.fcustomer = '.$system_num.' group by fsample_id,b.fmedia_class) as x on a.fid=x.fillegal_ad_id')
            //->join('tmedia_temp ttp on c.fid=ttp.fmediaid and ttp.ftype in(1,2) and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
            ->where($where_tia)
            ->count();//查询满足条件的总记录数

        $do_tia = M('tbn_illegal_ad')
            ->alias('a')
            ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name,(case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus2,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fstatus,a.fregion_id')
            ->join('tadclass b on a.fad_class_code=b.fcode')
            ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
            ->join('tregion tn on '.$region_field.' =tn.fid'.$wherestr)
            ->join('(select count(*) as fcount,max(fillegal_ad_id) as fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue a,tbn_illegal_ad b where '.$where_time.' and a.fillegal_ad_id=b.fid and b.fcustomer = '.$system_num.' group by fsample_id,b.fmedia_class) as x on a.fid=x.fillegal_ad_id')
            //->join('tmedia_temp ttp on c.fid=ttp.fmediaid and ttp.ftype in(1,2) and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
            ->where($where_tia)
            ->order('x.fstarttime desc')
            ->page($p,$pp)
            ->select();
        if(!empty($outtype)){
            if(empty($do_tia)){
                $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
            }

            $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-数据汇总违法条数列表';//文档内部标题名称
            $outdata['datalie'] = [
                '序号'=>'key',
                '媒体分类'=>[
                    'type'=>'zwif',
                    'data'=>[
                        ['{fmedia_class} == 1','电视'],
                        ['{fmedia_class} == 2','广播'],
                        ['{fmedia_class} == 3','报纸'],
                        ['{fmedia_class} == 13','互联网'],
                    ]
                ],
                '广告类别'=>'fadclass',
                '广告名称'=>'fad_name',
                '发布媒体'=>'fmedianame',
                '播出总条次'=>'fcount',
                '播出周数'=>'diffweek',
                '播出时间段'=>[
                    'type'=>'zwhebing',
                    'data'=>'{fstarttime}~{fendtime}'
                ],
                '违法表现代码'=>'fillegal_code',
                '违法表现'=>'fexpressions',
                '涉嫌违法原因'=>'fillegal',
            ];
            $outdata['lists'] = $do_tia;
            $ret = A('Api/Function')->outdata_xls($outdata);

            D('Function')->write_log('数据汇总违法条数',1,'导出成功');
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

        }else{
            $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
        }
    }

    /**
     *违法条次列表
     */

    public function tiaoci_list()
    {
        $user = session('regulatorpersonInfo');//fregulatorid机构ID
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $ischeck = $ALL_CONFIG['ischeck'];
        $is_manage_media = $ALL_CONFIG['set_manage_region'];//是否通过指定区域管理媒体统计

        $fad_class_code   = I('fad_class_code');//广告类别，用于汇总中违法广告数据的查看
        $fmediaid         = I('fmediaid');//媒体ID，用于汇总中违法广告数据的查看
        $fmediaownerid    = I('fmediaownerid');//媒体机构ID，用于汇总中违法广告数据的查看
        $mclass           = I('mclass');//媒体类别，用于汇总中违法广告数据的查看
        $fad_class_code2  = I('fad_class_code2');//广告类别组，用于汇总中违法广告数据的查看
        $area           = I('area');//地域ID，用于汇总中违法广告数据的查看
        $flevel         = I('flevel');//地域级别，用于汇总中违法广告数据的查看
        $is_show_longad   = I('is_show_longad');//是否有长广告，用于汇总中违法广告数据的查看
        $is_show_fsend    = I('is_show_fsend');//是否包含确认广告，1包含未发布，其他值已发布
        $is_have_self    = I('is_have_self',0);//是否包含本级
        $ad_pm_type    = I('ad_pm_type',2);//
        $is_include_sub    = I('is_include_sub',0);//是否包含下级
        $outtype = I('outtype');//导出类型

        if($is_manage_media == 1){
            $region_field = 'tm.manage_region_id';
            $map_region_field = 'tn.fid';//a.fregion_id
        }else{
            $region_field = 'tia.adowner_regionid';
            $map_region_field = 'tia.adowner_regionid';//
        }
        if(!empty($outtype)){
            $p  = I('page', 1);//当前第几页ks
            $pp = 50000;//每页显示多少记录
        }else{
            $p  = I('page', 1);//当前第几页ks
            $pp = 10;//每页显示多少记录
        }

        //时间条件筛选
        $years      = I('year');//选择年份
        $times_table = I('times_table');
        $table_condition = I('table_condition');//选择时间
        if($times_table == 'tbn_ad_summary_day'){
            $timetypes = 0;
            $timeval = $table_condition;
        }elseif($times_table == 'tbn_ad_summary_week'){
            $timetypes = 10;
            $timeval = $table_condition;
        }elseif($times_table == 'tbn_ad_summary_half_month'){
            $timetypes = 20;
            $table_condition2 = explode('-', $table_condition);
            $table_condition3 = ((int)$table_condition2[0]-1)*2;
            $table_condition4 = ((int)$table_condition2[1])==1?1:2;
            $timeval = $table_condition3+$table_condition4;
        }elseif($times_table == 'tbn_ad_summary_month'){
            $timetypes = 30;
            $table_condition2 = explode('-', $table_condition);
            $timeval = (int)$table_condition2[0];
        }elseif($times_table == 'tbn_ad_summary_quarter'){
            $timetypes = 40;
            if($table_condition == '01-01'){
                $timeval = 1;
            }elseif($table_condition == '04-01'){
                $timeval = 2;
            }elseif($table_condition == '07-01'){
                $timeval = 3;
            }else{
                $timeval = 4;
            }
        }elseif($times_table == 'tbn_ad_summary_half_year'){
            $timetypes = 50;
            $table_condition2 = explode('-', $table_condition);
            if($table_condition2[2] == '01'){
                $timeval = 1;
            }else{
                $timeval = 2;
            }
        }elseif($times_table == 'tbn_ad_summary_year'){
            $timetypes = 60;
            $timeval = 1;
        }
        if($is_show_fsend==1){
            $is_show_fsend = -2;
        }else{
            $is_show_fsend = 0;
        }

        $where['tn.fid'] = ['like',trim($user['regionid'],0).'%'];


        //如果要求用总局数据，而平台不属于总局平台时，发布状态要等于2，即已确认发布
        $iszjdata = I('iszjdata')?I('iszjdata'):0;//是否总局数据
        if(!empty($iszjdata)){
            $system_num = 100000;
            $is_show_fsend = 2;
        }
        $where_time = gettimecondition($years,$timetypes,$timeval,'tiai.fissue_date',$is_show_fsend);


        if($system_num == '100000'){
            if(!empty($area)){
                if(($flevel==1 || $flevel == 2 || $flevel==3 || $flevel==4 || $flevel==5) && is_numeric($flevel)){
                    $where[$map_region_field]  = $area;
                }else{
                    $where["left($map_region_field,2)"] = substr($area, 0,2);
                }
            }else{
                if(session('regulatorpersonInfo.fregulatorlevel')!=30){
                    $regionid = session('regulatorpersonInfo.regionid');//机构行政区划ID
                    $regionid = substr($regionid , 0 , 2);
                    $where[$map_region_field] = array('like',$regionid.'%');
                }
            }
        }else{
            if(!empty($area)){
                $where[$map_region_field] = $area;
            }
        }

        if(!empty($mclass)){
            $where['tia.fmedia_class'] = $mclass;
        }

        //国家局系统只显示有打国家局标签媒体的数据
        if($system_num == '100000'){
            $wherestr = ' and tn.flevel in (1,2,3)';
        }

        if(!empty($flevel)){
            $wherestr = ' and tn.flevel in('.$flevel.')';
        }

        if(!empty($fad_class_code)){//广告类别排名查询
            $where['left(tia.fad_class_code,2)'] = $fad_class_code;

            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==0 && $flevel == 5){
                $where_tia[$map_region_field]  = $area;
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==10&&$flevel == 5){
                if(!empty($is_have_self)){
                    $wherestr = ' and tn.flevel in (2,3,4,5)';
                }else{
                    $wherestr = ' and tn.flevel = 5';
                }
                $where_tia[$map_region_field]  = array('like',substr($area, 0,4).'%');
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==20&&$flevel == 4){
                if(!empty($is_have_self)){
                    $wherestr = ' and tn.flevel in (1,2,3,4)';
                }else{
                    $wherestr = ' and tn.flevel in (2,3,4)';
                }
                $where_tia[$map_region_field]  = array('like',substr($area, 0,2).'%');
            }

        }

        if(!empty($fmediaid)){
            $where['tia.fmedia_id'] = $fmediaid;
            $wherestr = ' and tn.flevel in (1,2,3,4,5)';
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==0 && $flevel == 5){
                $where_tia[$map_region_field]  = $area;
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==10&&$flevel == 5){
                $where_tia[$map_region_field]  = array('like',substr($area, 0,4).'%');
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==20&&$flevel == 4){
                $where_tia[$map_region_field]  = array('like',substr($area, 0,2).'%');
            }
        }

        if(!empty($fmediaownerid)){
            $where['tia.fmediaownerid'] = $fmediaownerid;
            $wherestr = ' and tn.flevel in (1,2,3,4,5)';
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==0 && $flevel == 5){
                $where_tia[$map_region_field]  = $area;
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==10&&$flevel == 5){
                $where_tia[$map_region_field]  = array('like',substr($area, 0,4).'%');
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==20&&$flevel == 4){
                $where_tia[$map_region_field]  = array('like',substr($area, 0,2).'%');
            }
        }

        if(!empty($fad_class_code2)){
            $fadclass = [];
            foreach ($fad_class_code2 as $key => $value) {
                if($value<10){
                    $fadclass[] = '0'.$value;
                }else{
                    $fadclass[] = $value;
                }
            }
            $where['left(tia.fad_class_code,2)'] = array('in',$fadclass);
        }

        $ad_name    = I('name');//广告名
        $start_date = I('start_date');//开始日期
        $end_date   = I('end_date');//结束日期

        if($ad_name){
            $where['tia.fad_name'] = array('like','%'.$ad_name.'%');
        }
        if($start_date && $end_date){
            $where['tiai.fissue_date'] = array('between',array($start_date,$end_date));
        }

        //是否抽查模式
        if(!empty($ischeck)){
            $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
            //如果抽查表有数据
            if(!empty($spot_check_data)){
                $dates = [];//定义日期数组
                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                    $year_month = '';
                    $date_str = [];
                    $year_month = substr($spot_check_data_val['fmonth'],0,7);
                    if(!empty($spot_check_data_val['condition'])){
                        $date_str = explode(',',$spot_check_data_val['condition']);
                    }
                    foreach ($date_str as $date_str_val){
                        $dates[] = $year_month.'-'.$date_str_val;
                    }
                }
                $where_time     .= ' and tiai.fissue_date in ("'.implode('","', $dates).'")';
            }else{
                $where_time     .= ' and 1=0';
            }
        }

        if($is_show_longad === '0'){
            $where_time .= ' and (TIMESTAMPDIFF(SECOND,tiai.fstarttime,tiai.fendtime)<600 or tiai.fmedia_class>2)';
        }

        $where['_string'] .= $where_time;
        //审核通过的违法数据
        $isexamine = $ALL_CONFIG['isexamine'];
        if(in_array($isexamine, [10,20,30,40,50])){
            $where['tia.fexamine'] = 10;
        }

        if($ad_pm_type == 2){
            $region = I('region2','');//区域
            if($is_include_sub == 1 && $region != 5 && $region != 0){
                switch ($region){
                    case 1:
                        $where[$map_region_field] = ['like',substr($area,0,2).'%'];
                        $wherestr = ' and tn.flevel in (1,2,3,4,5)';
                        break;
                    case 2:
                    case 3:
                    case 4:
                        $where[$map_region_field] = ['like',substr($area,0,4).'%'];
                        $wherestr = ' and tn.flevel in (2,3,4,5)';
                        break;
                }
            }
        }


        $where['tia.fstatus3'] = ['NOT IN',[30,40]];
        $where['tia.identify'] = 'new_2';
        $count = M('tbn_illegal_ad_issue')
            ->alias('tiai')
            ->join('tbn_illegal_ad as tia on tiai.fillegal_ad_id = tia.fid and tia.fcustomer="'.$system_num.'"')
            ->join('tmedia as tm on tiai.fmedia_id = tm.fid and tm.fid=tm.main_media_id')
            ->join('tadclass as tc on tia.fad_class_code = tc.fcode')
            ->join('tregion tn on '.$region_field.' = tn.fid'.$wherestr)
            //->join('tmedia_temp ttp on tm.fid=ttp.fmediaid and ttp.ftype in(1,2) and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
            ->where($where)
            ->count();
        $res = M('tbn_illegal_ad_issue')
            ->alias('tiai')
            ->field('
                tia.fid as sid,
                tia.fmedia_class,
                tc.ffullname as fadclass,
                tia.fad_name,
                 (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,
                tiai.fissue_date,
                tiai.fstarttime,
                tiai.fendtime,
                tiai.fpage,
                tia.fillegal,
                tia.fexpressions,
                tia.fillegal_code,
                tia.favifilename,
                tia.fjpgfilename,
                (UNIX_TIMESTAMP(tiai.fendtime)-UNIX_TIMESTAMP(tiai.fstarttime)) as difftime
            ')
            ->join('tbn_illegal_ad as tia on tiai.fillegal_ad_id = tia.fid and tia.fcustomer="'.$system_num.'"')
            ->join('tmedia as tm on tiai.fmedia_id = tm.fid  and tm.fid=tm.main_media_id')
            ->join('tadclass as tc on tia.fad_class_code = tc.fcode')
            ->join('tregion tn on '.$region_field.' = tn.fid'.$wherestr)
            //->join('tmedia_temp ttp on tm.fid=ttp.fmediaid and ttp.ftype in(1,2) and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
            ->where($where)
            ->page($p,$pp)
            ->select();

        if(empty($outtype)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$res)));
        }else{
            if(empty($res)){
                $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
            }

            $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-数据汇总违法条次列表';//文档内部标题名称
            $outdata['datalie'] = [
                '序号'=>'key',
                '媒体分类'=>[
                    'type'=>'zwif',
                    'data'=>[
                        ['{fmedia_class} == 1','电视'],
                        ['{fmedia_class} == 2','广播'],
                        ['{fmedia_class} == 3','报纸'],
                        ['{fmedia_class} == 13','互联网'],
                    ]
                ],
                '广告分类'=>'fadclass',
                '广告名称'=>'fad_name',
                '发布媒体'=>'fmedianame',
                '播出日期'=>'fissue_date',
                '开始时间'=>'fstarttime',
                '结束时间'=>'fendtime',
                '时长/版面'=>[
                    'type'=>'zwif',
                    'data'=>[
                        ['{fmedia_class} == 1','{difftime}'],
                        ['{fmedia_class} == 2','{difftime}'],
                        ['{fmedia_class} == 3','{fpage}']
                    ]
                ],
                '违法表现代码'=>'fillegal_code',
                '违法表现'=>'fexpressions',
                '涉嫌违法原因'=>'fillegal'
            ];
            $outdata['lists'] = $res;
            $ret = A('Api/Function')->outdata_xls($outdata);

            D('Function')->write_log('数据汇总违法条次',1,'导出成功');
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }
    }

    /*
     * 排序
     * */
    public function pxsf($data = [],$score_rule = 'com_score',$is_desc = true){
        $data_count = count($data);
        for($k=0;$k<=$data_count;$k++)
        {
            for($j=$data_count-1;$j>$k;$j--){
                if($is_desc){
                    if($data[$j][$score_rule]>$data[$j-1][$score_rule]){
                        $temp = $data[$j];
                        $data[$j] = $data[$j-1];
                        $data[$j-1] = $temp;
                    }
                }else{
                    if($data[$j][$score_rule]<$data[$j-1][$score_rule]){
                        $temp = $data[$j];
                        $data[$j] = $data[$j-1];
                        $data[$j-1] = $temp;
                    }
                }

            }
        }
        return $data;
    }

    //生成数字
    public function create_number($num){
        $n = 0;
        $m = [];
        while ($n < $num){
            $n++;
            $m[] = $n;
        }
        return $m;
    }

    //生成半月
    public function create_month($half = true,$kv = true){
        $n = 0;
        $m = [];
        while ($n < 12){
            $n++;
            $str = $n;
            if($n<10){
                $str = '0'.$n;
            }
            if($kv){
                if($half){
                    $m[$str.'-01'] = $n.'月上半月';
                    if($n == 2){
                        $m[$str.'-15'] = $n.'月下半月';
                    }else{
                        $m[$str.'-16'] = $n.'月下半月';
                    }
                }else{
                    $m[$str.'-01'] = $n.'月';
                }
            }else{
                if($half){
                    $m[] = $str.'-01';
                    if($n == 2){
                        $m[] = $str.'-15';
                    }else{
                        $m[] = $str.'-16';
                    }
                }else{
                    $m[] = $str.'-01';
                }
            }

        }
        return $m;
    }

    //判断当前用户行政等级,并返回
    public function judgment_region($regionid){
        $level = M('tregion')->where(['fid'=>$regionid])->getField('flevel');
        /*        if($level == 2 || $level == 3){
                    $data['region'] = [
                        '1' =>'省',
                        '2' =>'副省级市',
                        '3' =>'计划单列市',
                        '4' =>'市',
                        '5' =>'区县'
                    ];//选区域

                    $data['region_level'] = [
                        '1' =>'全国',
                        '2' =>'省级',
                        '3' =>'市级',
                        '4' =>'县级'
                    ];//行政级别
                }*/
        return $level;
    }


    //获取行政等级下的所有id
    public function get_region_ids($region_level){
        $region_ids = M('tregion')->where(['flevel'=>$region_level])->getField('fid',true);
        return $region_ids;
    }

    //获取上个周期
    public function get_previous_cycles($times_table ='tbn_ad_summary_week',$year='2018',$table_condition='17'){
        if($times_table == 'tbn_ad_summary_week'){
            $year_mod = M('tbn_ad_summary_week')->distinct(true)->getField('fweek',true);
        }else{
            $year_mod = M($times_table)->distinct(true)->getField('fdate',true);
        }
        $years = [];
        foreach ($year_mod as $year_date){
            $years[] = substr($year_date,0,4);
        }
        $years = array_unique($years);//选年
        $select_half_year = [];
        foreach ($years as $v_half_year){
            $select_half_year[] = $v_half_year.'-01-01';//$v_year.'上半年'
            $select_half_year[] = $v_half_year.'-07-01';//$v_year.'下半年';
        }

        $select_year = [];
        foreach ($years as $v_year){
            $select_year[] = $v_year.'-01-01';
        }
        switch ($times_table){
            case 'tbn_ad_summary_week':
                $cycles = $this->create_number(52);
                break;
            case 'tbn_ad_summary_half_month':
                $cycles = $this->create_month(true,false);
                break;
            case 'tbn_ad_summary_month':
                $cycles = $this->create_month(false,false);
                break;
            case 'tbn_ad_summary_quarter':
                $cycles = ['01-01','04-01','07-01','10-01'];
                break;
            case 'tbn_ad_summary_half_year':
                $cycles = $select_half_year;
                break;
            case 'tbn_ad_summary_year':
                $cycles = $select_year;
                break;
        }

        $year_array = explode('-',$table_condition);
        $cycles_count = count($cycles);
        foreach ($cycles as $cy_key => $cycle){
            if($cycle == $table_condition){
                if($times_table == 'tbn_ad_summary_half_year' || $times_table == 'tbn_ad_summary_year'){
                    if($cy_key == 0){
                        $year_array[0] -= 1;
                        $prv = explode('-',$cycles[$cycles_count-1]);
                        return $year_array[0].'-'.$prv[1].'-'.$prv[2];
                    }else{
                        return $cycles[$cy_key-1];
                    }
                }
                if($cy_key == 0){
                    $year -= 1;
                    $condition = $cycles[$cycles_count-1];
                }else{
                    $condition = $cycles[$cy_key-1];
                }
                if($times_table == 'tbn_ad_summary_week'){
                    if($condition < 10){
                        $condition = '0'.$condition;
                    }
                    return $year.$condition;
                }else{
                    return $year.'-'.$condition;
                }

            }
        }
    }

    /**
     * 获取某年第几周的开始日期和结束日期
     * @param int $year
     * @param int $week 第几周;
     */
    public function weekday($year,$week=0){
        $year_start = mktime(0,0,0,1,1,$year);
        $year_end = mktime(0,0,0,12,31,$year);

        // 判断第一天是否为第一周的开始
        if (intval(date('W',$year_start))===1){
            $start = $year_start;//把第一天做为第一周的开始
        }else{
            $week++;
            $start = strtotime('+1 monday',$year_start);//把第一个周一作为开始
        }

        // 第几周的开始时间
        if ($week===1){
            $weekday['start'] = $start;
        }else{
            $weekday['start'] = strtotime('+'.($week-0).' monday',$start);
        }

        // 第几周的结束时间
        $weekday['end'] = strtotime('+1 sunday',$weekday['start']);
        if (date('Y',$weekday['end'])!=$year){
            $weekday['end'] = $year_end;
        }
        return $weekday;
    }

    //获取首末周期时间戳
    public function get_stemp($cycles='2018-01-01',$time_type='tbn_ad_summary_half_month'){
        switch($time_type){
            case 'tbn_ad_summary_half_month':
                $day = substr($cycles,-2);
                $month = substr($cycles,5,2);
                $year = substr($cycles,0,4);
                if($day == '01'){
                    if($month == '02'){
                        $time_string = $year.'-'.$month.'-15';
                    }else{
                        $time_string = $year.'-'.$month.'-16';
                    }
                }else{
                    if($month == '12'){
                        $year = $year+1;
                        $time_string = $year.'-01-01';
                    }else{
                        if($month == '02'){
                            $cycles = $year.'-'.$month.'-15';
                        }
                        $month = ($month+1) < 10 ? '0'.($month+1):($month+1);
                        $time_string = $year.'-'.$month.'-01';
                    }
                }
                break;
            case 'tbn_ad_summary_month':
                $month = substr($cycles,5,2);
                $year = substr($cycles,0,4);

                if($month == '12'){
                    $year = $year+1;
                    $time_string = $year.'-01-01';
                }else{
                    $month = ($month+1) < 10 ? '0'.($month+1):($month+1);
                    $time_string = $year.'-'.$month.'-01';
                }
                break;
            case 'tbn_ad_summary_quarter':
                $quarter = substr($cycles,5,2);
                $year = substr($cycles,0,4);
                if($quarter == '10'){
                    $year = $year+1;
                    $time_string = $year.'-01-01';
                }else{
                    $quarter = ($quarter+3) < 10 ? '0'.($quarter+3):($quarter+3);
                    $time_string = $year.'-'.$quarter.'-01';
                }
                break;
            case 'tbn_ad_summary_half_year':
                $half_year = substr($cycles,5,2);
                $year = substr($cycles,0,4);
                if($half_year == '07'){
                    $year = $year+1;
                    $time_string = $year.'-01-01';
                }else{
                    $time_string = $year.'-07-01';
                }
                break;
            case 'tbn_ad_summary_year':
                $year = substr($cycles,0,4);
                $year = $year+1;
                $cycles = $cycles.'-01-01';
                $time_string = $year.'-01-01';
                break;
        }

        $cycle['start'] = strtotime($cycles);
        $cycle['end'] = strtotime($time_string);
        return $cycle;
    }

    /**
     * @return array
     */
    public function get_gjj_label_media(){
        $gjj_label_media_map = [];
        $gjj_label_media_map['tmedialabel.flabelid'] = ['IN',[243,244,245,248]];
        $gjj_label_media_map['tregion.flevel'] = ['IN',[1,2,3]];
        $gjj_label_media_map['tmedialabel.fstate'] = ['IN',[1,0]];
        $gjj_label_media = M('tmedialabel')
            ->join("tmedia on tmedia.fid = tmedialabel.fmediaid and tmedia.fid = tmedia.main_media_id and tmedia.fstate =1")
            ->join("tregion on tregion.fid = tmedia.media_region_id")
            ->where($gjj_label_media_map)
            ->group('fmediaid')
            ->getField('fmediaid',true);
        $gjj_label_media = array_unique($gjj_label_media);


        return $gjj_label_media;
    }

    function prDates($start,$end,$date_num){
        $date_array = [];
        $dt_start = strtotime($start);
        $dt_end = strtotime($end);
        while ($dt_start<=$dt_end){
            $date_array[] = date('Y-m-d',$dt_start);
            $dt_start = strtotime('+1 day',$dt_start);
        }
        return array_chunk($date_array,$date_num);
    }

}