<?php
namespace TaskInput\Model;
use Think\Model;
use Open\Model\EvidenceModel;

class NetAdModel{

    /**
     * 互联网广告任务状态码。两位数组成:第一位表示环节 9-表示不针对环节，第二位表示状态
     */
    protected $TaskState = [
        0,  //初始
        11, //环节1进行中
        12, //环节1完成
        13, //环节1退回
        21, //环节2进行中
        22, //环节2完成
        23, //环节2退回
        2,  //完成
        9,  //标记为非广告
    ];

    /**
     * 互联网广告任务流程日志类型码。两位数组成:第一位表示环节 9-表示不针对环节，第二位表示状态 1-领取任务 2-提交任务 3-退回 4-超时释放 5-定时同步 6-提交非广告
     */
    public $LogType = [
        "11" => "领取任务环节1(信息录入)",
        "21" => "领取任务环节2(违法审核)",
        "12" => "提交环节1(信息录入)",
        "22" => "提交环节2(违法审核)",
        "13" => "退回环节1(信息录入)",
        "23" => "退回环节2(违法审核)",
        "14" => "退回环节1(信息录入)",//暂保留该状态码，以兼容原状态为4的退回状态
        "24" => "退回环节2(违法审核)",//暂保留该状态码，以兼容原状态为4的退回状态
        "95" => "完成定时同步",
        "96" => "提交非广告",
        "17" => "质检修改提交环节1(信息录入)",
        "27" => "质检修改提交环节2(违法审核)",
        "18" => "超时释放环节1(信息录入)",
        "28" => "超时释放环节2(违法审核)",
        "2"  => "完成任务",
    ];

    protected $task_count_field_map = [
        '1' => [
            '1' => 'tv_input',
            '2' => 'tv_inspect',
        ],
        '2' => [
            '1' => 'bc_input',
            '2' => 'bc_inspect',
        ],
        '3' => [
            '1' => 'paper_input',
            '2' => 'paper_inspect',
        ],
        '13' => [
            '1' => 'net_input',
            '2' => 'net_inspect',
        ],
    ]; // 第一层是媒体类型, 第二层是环节类型

    protected $v3_task_count_map = [
        '1' => 'v3_tv_task',
        '2' => 'v3_bc_task',
        '3' => 'v3_paper_task',
        '13' => 'v3_net_task',
    ];

    /**
     * 错误信息
     */
    protected $error = '';

    /**
     * 新增互联网广告众包任务
     * @Param   Array $tasks    任务数据数组
     * @Return  Bool            返回True成功,False失败
     */
    public function addTask($tasks = []){
        if(is_array($tasks) && !empty($tasks)){
            foreach($tasks as $key => $row){
				$isExist = M('tnettask')->where(['major_key'=>$row['major_key']])->count();
				if($isExist > 0) continue;
                $fadclasscode = $row['fadclasscode'];
                // 广告类别为默认值或空才需要添加到众包任务
                if($fadclasscode == '2301' || empty($fadclasscode)){
                    // 媒体优先级同时赋给任务优先级
                    $priority = M('tmedia')->cache(true,120)->where(['fid'=>$row['fmediaid']])->getField('priority');
                    $platform = $row['net_platform'];
                    $issueInfo = M('tnetissue')->where(['major_key'=>$row['major_key']])->find();
                    $tUrl = $issueInfo['net_target_url'];
                    // if($priority <= 10){
                    //     if($platform == 2 || $platform == 9){
                    //         // 移动端或微信端，将优先级设为10
                    //         $priority = 10;
                    //     }else{
                    //         $priority = 0;
                    //     }
                    // }
                    // 包含如下地址的优先级设置为-10
                    $arrLike = [
                        'https://m.baidu.com/from=',
                        'https://wap.sogou.com/web/searchList.jsp?keyword=%',
                        'http://aden.baidu.com/?word=',
                        'http://cpro.baidu.com/cpro/ui/uijs.php',
                    ];
                    foreach($arrLike as $val){
                        $exist = strpos($tUrl,$val) !== false;
                        if($exist) $priority = -10;
                    }
                    // 临时注释$platform判断，避免传入参数$platform为空优先级异常BUG
                    // if($platform == 2 || $platform == 9){
                    //     $strLikeA = 'https://m.baidu.com/from=';
                    //     $strLikeB = 'https://wap.sogou.com/web/searchList.jsp?keyword=%';
                    //     $strLikeC = 'http://aden.baidu.com/?word=';
                    //     $strLikeD = 'http://cpro.baidu.com/cpro/ui/uijs.php';
                    //     $strLikeE = 'https://m.baidu.com/from=';
                    //     if(strpos($tUrl,$strLikeA) !== false || strpos($tUrl,$strLikeB) !== false || strpos($tUrl,$strLikeC) !== false || strpos($tUrl,$strLikeD) !== false || strpos($tUrl,$strLikeE) !== false){
                    //         $priority = -10;
                    //     }
                    // }

                    $data = [
                        'wx_id'            => '',
                        'major_key'        => $row['major_key'],
                        'media_class'      => '13',
                        'media_id'         => $row['fmediaid'],
                        'net_id'           => $row['net_id'],
                        'net_created_date' => round($row['net_created_date']*0.001),//众包任务表中四舍五入到秒
                        'issue_date'       => date('Y-m-d',round($row['net_created_date']*0.001)),
                        'source_path'      => '',
                        'ftype'            => $row['ftype'],
                        'fadclasscode'     => $fadclasscode,
                        'fversion'         => '',
                        'fspokesman'       => $row['fspokesman'],
                        'fillegaltypecode' => $row['fillegaltypecode'],
                        'fillegalcontent'  => $row['fillegalcontent'],
                        'fexpressioncodes' => $row['fexpressioncodes'],
                        'fadmanuno'        => $row['fadmanuno'],
                        'fmanuno'          => $row['fmanuno'],
                        'fadapprno'        => $row['fadapprno'],
                        'fapprno'          => $row['fapprno'],
                        'fadent'           => $row['fadent'],
                        'fent'             => $row['fent'],
                        'fentzone'         => $row['fentzone'],
                        'priority'         => $priority,
                        'back_reason'      => '',
                        'sam_uuid'         => '',
                        'cut_err_state'    => 0,
                        'cut_err_reason'   => '',
                        'quality_wx_id'    => '',
                        'quality_state'    => 0,
                        'syn_state'        => 0,
                        'task_state'       => 0,
                        'creator'          => '系统',
                        'createtime'       => time(),
                        'modifier'         => '',
                        'modifytime'       => time(),
                    ];
                    $isExist = M('tnettask')->where(['major_key'=>$row['major_key']])->count() > 0 ? true : false;
                    if(!$isExist) $ret = M('tnettask')->add($data);
                }
            }
            return true;
        }
        return false;
    }

    /**
     * 获取互联网广告任务 2019-02-20
     */
    public function getTask()
    {
        $wx_id = intval(session('wx_id'));
        // 获取用户环节权限，如:['1','2']
        $linkTypes = (new AdInputUserModel())->getTaskLinkAuth($wx_id);
        // 任务日期排序,默认倒序,做环节2时正序,以处理积压的环节2任务
        $orderType = 'DESC';
        // 根据用户环节权限得到任务状态搜索条件
        $ArrState = [0];
        // 任务过滤条件
        $searchWhere = [
            'task.isad' => ['EQ', 1],
        ];
        if($linkTypes === ['1']){
            $ArrState = 0;
            $searchWhere['task.task_state'] = 0;
        }elseif($linkTypes === ['2']){
            $ArrState = 12;
            $orderType = 'ASC';
            $searchWhere['task.task_state'] = 12;
        }elseif($linkTypes === ['1','2']){
            $ArrState = [0,12];
            $orderType = 'ASC';
            $searchWhere['task.task_state'] = ['IN', [0,12]];
        }else{
            $ArrState = 0;
            $searchWhere['task.task_state'] = 0;
        }
        // 人员数据分组
        $have_group_id = M('ad_input_user')->cache(true,86400)->where(['wx_id'=>$wx_id])->getField('have_group_id');
        if(!empty($have_group_id)){
            $task_have_group_id = explode(',',$have_group_id);
        }
        $task_have_group_id[] = '0';//增加未分组0
        $searchWhere['task.group_id'] = ['IN',$task_have_group_id];
        // 如果有正在执行的任务，直接返回正在执行的任务id
        $taskid = M('tnettask')
            ->alias('task')
            ->where([
                'task.isad' => ['EQ', 1],
                'task.wx_id' => ['EQ', $wx_id],
                'task.task_state' => ['IN',[11,21]]
            ])
            ->getField('task.taskid');
        if($taskid) return $taskid;
        // 被退回的任务，优先分配给负责人
        $taskid = M('tnettask')
            ->alias('task')
            ->where([
                'task.isad' => ['EQ', 1],
                'task.wx_id' => ['EQ', $wx_id],
                'task.task_state' => ['IN',[13,23]]
            ])
            ->getField('task.taskid');
        if($taskid){
            // 更新时间
            M('tnettask')->where(['taskid'=>$taskid])->save(['modifytime' => time()]);
            return $taskid;
        }
        $while_num = 0;
        $e_task_state = false;
        // 排序依据条件
        $orderArr = [];
        $orderArr[] = 'task.group_id DESC';
        $orderArr[] = 'task.priority DESC';
        $orderArr[] = 'task.net_created_date '.$orderType;
        $order = implode(',', $orderArr);
        // 尝试获取任务
        while(!$e_task_state){
            $while_num++;
            if(in_array('2',$linkTypes)){
                // 有环节2权限则优先处理环节2待处理的任务
                $searchWhere['task.task_state'] = 12;
                $taskList = M('tnettask')->alias('task')
                    ->field('task.taskid,task.task_state')
                    ->where($searchWhere)
                    ->order($order)
                    ->limit(5)
                    ->select();
                if(empty($taskList)){
                    $searchWhere['task.task_state'] = 0;
                    $searchWhere['task.priority'] = ['GT', 0];
                    $taskList = M('tnettask')->alias('task')
                        ->field('task.taskid,task.task_state')
                        ->where($searchWhere)
                        ->order($order)
                        ->limit(5)
                        ->select();
                }
            }else{
                $searchWhere['task.priority'] = ['GT', 0];
                $taskList = M('tnettask')->alias('task')
                    ->field('task.taskid,task.task_state')
                    ->where($searchWhere)
                    ->order($order)
                    ->limit(5)
                    ->select();
            }
            if(empty($taskList)){
                return false;
            }
            shuffle($taskList);
            foreach ($taskList as $taskInfo) {
                $taskid = $taskInfo['taskid'];
                $currentState = $taskInfo['task_state'];
                if($currentState == 0){
                    $nextTaskState = 11;
                }elseif($currentState == 12){
                    $nextTaskState = 21;
                }
                M()->startTrans();
                $e_task_state = M('tnettask')
                    ->where([
                        'taskid' => ['EQ', $taskid],
                        'task_state' => $currentState
                    ])
                    ->save([
                        'wx_id' => $wx_id,
                        'task_state' => $nextTaskState,
                        'modifytime' => time()
                    ]);
                if($e_task_state){
                    $e_task_state = M('tnettask_flowlog')
                        ->add([
                            'taskid' => $taskid,
                            'tasklink' => substr($currentState,0,1),
                            'wx_id' => $wx_id,
                            'logtype' => $nextTaskState,
                            'log' => $this->LogType[$nextTaskState],
                            'creator' => $wx_id,
                            'createtime' => time()
                        ]);
                    if($e_task_state){
                        M()->commit();
                        return $taskid;
                    }
                }
                M()->rollback();
            }
            if($while_num >= 3) return false;//如果抢了3次还没有任务，那就返回false
        }
    }
    /**
     * @Des: 获取任务
     * @Edt: yuhou.wang
     * @Date: 2020-01-19 17:15:33
     * @param {type} 
     * @return: 
     */
    public function getTask_Demo()
    {
        $wx_id = intval(session('wx_id'));
        // 获取用户环节权限，如:['1','2']
        $linkTypes = (new AdInputUserModel())->getTaskLinkAuth($wx_id);
        // 任务日期排序,默认倒序,做环节2时正序,以处理积压的环节2任务
        $orderType = 'DESC';
        // 根据用户环节权限得到任务状态搜索条件
        $ArrState = [0];
        // 任务过滤条件
        $searchWhere = [
            'task.isad' => ['EQ', 1],
        ];
        if($linkTypes === ['1']){
            $ArrState = 0;
            $searchWhere['task.task_state'] = 0;
        }elseif($linkTypes === ['2']){
            $ArrState = 12;
            $orderType = 'ASC';
            $searchWhere['task.task_state'] = 12;
        }elseif($linkTypes === ['1','2']){
            $ArrState = [0,12];
            $orderType = 'ASC';
            $searchWhere['task.task_state'] = ['IN', [0,12]];
        }else{
            $ArrState = 0;
            $searchWhere['task.task_state'] = 0;
        }
        // 人员数据分组
        $have_group_id = M('ad_input_user')->cache(true,120)->where(['wx_id'=>$wx_id])->getField('have_group_id');
        if(!empty($have_group_id)){
            $task_have_group_id = explode(',',$have_group_id);
        }
        $task_have_group_id[] = '0';//增加未分组0
        $searchWhere['task.group_id'] = ['IN',$task_have_group_id];
        // 如果有正在执行的任务，直接返回正在执行的任务id
        $taskid = M('tnettask')
            ->alias('task')
            ->where([
                'task.isad' => ['EQ', 1],
                'task.wx_id' => ['EQ', $wx_id],
                'task.task_state' => ['IN',[11,21]]
            ])
            ->getField('task.taskid');
        if($taskid) return $taskid;
        // 被退回的任务，优先分配给负责人
        $taskid = M('tnettask')
            ->alias('task')
            ->where([
                'task.isad' => ['EQ', 1],
                'task.wx_id' => ['EQ', $wx_id],
                'task.task_state' => ['IN',[13,23]]
            ])
            ->getField('task.taskid');
        if($taskid){
            // 更新时间
            M('tnettask')->where(['taskid'=>$taskid])->save(['modifytime' => time()]);
            return $taskid;
        }

        // 获取优先级偏好
        $priorityList = $userGroupInfo['priority_list'];
        // 获取媒体偏好
        $mediaPrefer = $userGroupInfo['prefer_list'];
        $mediaIds = [];
        // 媒体限制的条件
        if ($userGroupInfo['mediaauthority'] !== 'unrestricted'){
            if (!$userGroupInfo['mediaauthority'] || $userGroupInfo['mediaauthority'] === '""'){
                return false;
            }
            $mediaIds = array_merge($mediaIds, json_decode($userGroupInfo['mediaauthority'], true));
        }
        // $searchWhere['task.media_id'] = ['IN', [11000010008084]];
        // $searchWhere['task.issue_date'] = '2019-02-28';
        // 地区限制的条件
        if ($userGroupInfo['regionauthority'] !== 'unrestricted'){
            if (!$userGroupInfo['regionauthority'] || $userGroupInfo['regionauthority'] === '""'){
                return false;
            }
            $userGroupInfo['regionauthority'] = json_decode($userGroupInfo['regionauthority'], true);
            // 区分并组合查找本级的和下属的条件, 生成mediaOwner的子查询搜索条件
            $inArr = [];
            $leftArr = [];
            foreach ($userGroupInfo['regionauthority'] as $item) {
                $len = strlen($item);
                if ($len == 6){
                    $inArr[] = $item;
                }else{
                    $leftArr[] = "(LEFT(fregionid, $len) = $item)";
                }
            }
            // 搜索条件有2个部分, 一个是全部地区id的本级媒体, 一个是部分地区id本级及下属媒体,互相之间的关系应该是OR
            $_mediaOwnerWhere = [
                '_logic' => 'OR',
            ];
            if (count($inArr) != 0){
                $_mediaOwnerWhere['fregionid'] = ['IN', $inArr];
            }
            if (count($leftArr) != 0){
                $_string = implode(' OR ', $leftArr);
                $_mediaOwnerWhere['_string'] = $_string;
            }
            // 生成mediaOwner子查询
            $subQuery = M('tmediaowner')->field('fid')->where($_mediaOwnerWhere)->buildSql();
            $_mediaWhere = [
                'fmediaownerid' => ['EXP', 'IN '. $subQuery]
            ];
            $res = M('tmedia')->where($_mediaWhere)->getField('fid', true);
            // 得到允许的媒体id
            $mediaIds = array_merge($mediaIds, $res);
            // $searchWhere['task.media_id'] = ['IN', $mediaIds];
        }
        // 加入媒体偏好元素
        if ($mediaPrefer){
            $mediaPrefer = explode(',', $mediaPrefer);
            if ($userGroupInfo['mediaauthority'] !== 'unrestricted') {
                $preferList = array_intersect($mediaPrefer, $mediaIds);
            }else{
                $preferList = $mediaPrefer;
            }
            $preferList = implode(',', $preferList);
        }
        if (!$preferList){
            $preferList = "null";
        }
        // 加入优先级偏好元素
        if (!$priorityList){
            $priorityList = "null";
        }
        // 发布日期限制条件
        if ($userGroupInfo['dateauthority'] !== 'unrestricted'){
            if (!$userGroupInfo['dateauthority'] || $userGroupInfo['dateauthority'] === '""'){
                return false;
            }
            // 保留，日期限制条件
            // $dateauthority = json_decode($userGroupInfo['dateauthority'], true);
            // 临时，日期限制条件固化为近30天，屏蔽众包分组日期限制条件，设置对互联网广告的失效但不影响传统广告
            $dateauthority = [date('Y-m-d',strtotime('-30 day')).' , '.date('Y-m-d')]; //日期偏好数组每个元素为一个时间段，每个时间段起始用,逗号分隔，注意逗号前后有空格

            $dateWhere = [];
            foreach ($dateauthority as $key => $value) {
                $date_a_e = explode(' , ', $value);
                foreach($date_a_e as $k=>$v){
                    $date_a_e[$k] = strtotime($v);
                }
                $dateWhere[] = ['BETWEEN', $date_a_e];
            }
            $dateWhere[] = 'OR';
            // $searchWhere['task.net_created_date'] = $dateWhere;
        }
        // 暂时使用classauthority这个字段限制互联网广告的优先级, 后期考虑加个字段限制
        if (!in_array($userGroupInfo['classauthority'], [null, '', '""', 'unrestricted'])){
            // $searchWhere['priority'] = ['ELT', $userGroupInfo['classauthority']];
        }
        // 临时处理，强制只处理高于指定优先级的任务，以减少数据量，加快任务页面加载速度,覆盖上面优先级处理 2019-01-04
        $net_ad_priority = sys_c('net_ad_priority') ? sys_c('net_ad_priority') : 10;
        // $searchWhere['priority'] = ['EGT', $net_ad_priority];

        $while_num = 0;
        $e_task_state = false;
        // 排序依据条件
        $orderArr = [];
        if ($priorityList !== 'null'){
            // $orderArr[] = "field(task.priority, $priorityList) desc";
        }
        if ($preferList !== 'null'){
            // $orderArr[] = "field(task.media_id, $preferList) desc";
        }
        $orderArr[] = 'task.group_id DESC';
        $orderArr[] = 'task.priority DESC';
        $orderArr[] = 'task.net_created_date '.$orderType;
        // $orderArr[] = 'task.task_state desc';
        $order = implode(',', $orderArr);
        // 尝试获取任务
        while(!$e_task_state){
            $while_num++;
            if(in_array('2',$linkTypes)){
                // 有环节2权限则优先处理环节2待处理的任务
                $searchWhere['task.task_state'] = 12;
                $taskList = M('tnettask')->alias('task')
                    ->field('task.taskid,task.task_state')
                    ->where($searchWhere)
                    ->order($order)
                    ->limit(5)
                    ->select();
                if(empty($taskList)){
                    $searchWhere['task.task_state'] = 0;
                    $searchWhere['task.priority'] = ['GT', 0];
                    $taskList = M('tnettask')->alias('task')
                        ->field('task.taskid,task.task_state')
                        ->where($searchWhere)
                        ->order($order)
                        ->limit(5)
                        ->select();
                }
            }else{
                $searchWhere['task.priority'] = ['GT', 0];
                $taskList = M('tnettask')->alias('task')
                    ->field('task.taskid,task.task_state')
                    ->where($searchWhere)
                    ->order($order)
                    ->limit(5)
                    ->select();
            }
            if(empty($taskList)){
                return false;
            }

            // if(empty($taskList)){
            //     $changeResult = $this->change_net_priority();
            //     if($changeResult) continue;
            // }
            // if(empty($taskList)){
            //     $searchWhere['task.priority'] = 0;
            //     $searchWhere['task.net_created_date'] = ['BETWEEN',[strtotime('-1 day'),time()]];
            //     $taskList = M('tnettask')->alias('task')
            //         ->field('task.taskid,task.task_state')
            //         ->where($searchWhere)
            //         ->order($order)
            //         ->limit(100)
            //         ->select();
            // }
            shuffle($taskList);
            foreach ($taskList as $taskInfo) {
                $taskid = $taskInfo['taskid'];
                $currentState = $taskInfo['task_state'];
                if($currentState == 0){
                    $nextTaskState = 11;
                }elseif($currentState == 12){
                    $nextTaskState = 21;
                }
                M()->startTrans();
                $e_task_state = M('tnettask')
                    ->where([
                        'taskid' => ['EQ', $taskid],
                        'task_state' => $currentState
                    ])
                    ->save([
                        'wx_id' => $wx_id,
                        'task_state' => $nextTaskState,
                        'modifytime' => time()
                    ]);
                if($e_task_state){
                    $e_task_state = M('tnettask_flowlog')
                        ->add([
                            'taskid' => $taskid,
                            'tasklink' => substr($currentState,0,1),
                            'wx_id' => $wx_id,
                            'logtype' => $nextTaskState,
                            'log' => $this->LogType[$nextTaskState],
                            'creator' => $wx_id,
                            'createtime' => time()
                        ]);
                    if($e_task_state){
                        M()->commit();
                        return $taskid;
                    }
                }
                M()->rollback();
            }
            if($while_num >= 3) return false;//如果抢了3次还没有任务，那就返回false
        }
    }

    /**
     * 提交广告信息录入任务
     * @param $data
     * @param $wx_id
     * @return bool
     */
    public function submitAdInputTask($data, $wx_id)
    {
        if ($data['fadclasscode'] == 2301){
            $this->error = '广告分类不允许';
            return false;
        }
        $wxInfo = M('ad_input_user')
            ->where(['wx_id' => ['EQ', $wx_id]])
            ->cache(true, 60)
            ->find();
        // 新广告主题则加入质检任务
        if(empty($data['fadid']) && !empty($data['fadname'])){
            $addRes = A('TaskInput/QualityInspection','Model')->addInsTask($data,true);
        }
        if ($data['media_class'] != 13){
            $data['fadid'] = A('Common/Ad','Model')->getAdId($data['fadname'],$data['fbrand'],$data['fadclasscode'],$data['fname'],$wxInfo['alias'], $data['fadclasscode_v2'],1);//获取广告ID
        }else{
            $data['fadid'] = A('Common/Ad','Model')->getAdId($data['fadname'],$data['fbrand'],$data['fadclasscode'],$data['fname'],$wxInfo['alias'], $data['fadclasscode_v2'],13);//获取广告ID
            $data['fadowner'] = A('Common/Ad','Model')->getAdOwnerId($data['fname'],$wxInfo['alias']);
            $issueData = $data;
            $issueData['finputstate'] = 2;//更新为已录入状态 录入状态，0-无需录入，1-未录入，2-已录入
            $issueData['finputuser'] = $wx_id;
            $issueData['fmodifier'] = $wx_id;
            $issueData['fmodifytime'] = date('Y-m-d H:i:s');
			$netIssueRes = M('tnetissue')->where(['major_key' => ['EQ', $data['major_key']]])->save($issueData);
			try{
				$identify_code = M('tnetissue_identify')->where(array('major_key'=>$data['major_key']))->getField('identify_code');
				$major_key_list = M('tnetissue_identify')->where(array('identify_code'=>$identify_code,'major_key'=>array('neq',$data['major_key'])))->getField('major_key',true);
				$add_tnetsam_handle = [array('major_key'=>$data['major_key'],'create_time'=>date('Y-m-d H:i:s'))];
				foreach($major_key_list as $mj){
					$add_tnetsam_handle[] = array('major_key'=>$mj,'create_time'=>date('Y-m-d H:i:s'));
				}
				M('tnetsam_handle')->addAll($add_tnetsam_handle);
				#把这些样本id的任务改为录入已完成 $major_key_list
                if (count($major_key_list) > 0) {
                    $issueData['finputuser'] = 32;
                    $issueData['fmodifier'] = 32;
					unset($issueData['major_key']);
                    $eee = M('tnetissue')->where(['major_key' => ['in', $major_key_list]])->save($issueData);
					#file_put_contents('LOG/NetAdRi',date('Y-m-d H:i:s').'	'.$eee."\n",FILE_APPEND);
                }
				$issueData = array();
			}catch( \Exception $e) { //
				file_put_contents('LOG/NetAdErr',date('Y-m-d H:i:s').'	'. ($e->getMessage()) ."\n",FILE_APPEND);
			}
            if (!$netIssueRes){
                $this->error = '样本更新失败';
                return false;
            }
        }
        return $this->submitTask($data['taskid'], $data, 1 ,$wx_id);
    }

    /**
     * 提交违法判定
     */
    public function submitJudgeTask($data, $wx_id)
    {
        // 如果广告不违法, 过滤掉其他字段
        if ($data['fillegaltypecode'] == '0'){
            unset($data['fillegalcontent']);
            unset($data['fexpressioncodes']);
            unset($data['fexpressions']);
            unset($data['fconfirmations']);
        }
        if ($data['media_class'] == 13) {
            $issueData = $data;
            $issueData['net_snapshot'] = $data['source_path']; // 截图地址
            $issueData['finspectstate'] = 2;//更新为已录入状态 录入状态，0-无需录入，1-未录入，2-已录入
            $issueData['finspectuser'] = $wx_id;
            $issueData['fmodifier'] = $wx_id;
            $issueData['fmodifytime'] = date('Y-m-d H:i:s');

            // 审核时修改录入信息时，减扣录入人积分及记录历史
            if($wx_id){
                $tableName = 'tnettask';
                $isChangeInput = false;
                $wxInfo = M('ad_input_user')->cache(true, 60)->where(['wx_id'=>['EQ', $wx_id]])->find();
                $data['fadid'] = A('Common/Ad','Model')->getAdId($data['fadname'],$data['fbrand'],$data['fadclasscode'],$data['fname'],$wxInfo['alias'], $data['fadclasscode_v2'],13);//获取广告ID
                $beforeInfo = M($tableName)->where(['taskid'=>$data['taskid']])->find();
                $afterInfo = $beforeInfo;
                $fields = ['fspokesman','fversion','fadid'];
                foreach($fields as $field){
                    $isChangeInput = $beforeInfo[$field] != $data[$field];
                    if($isChangeInput == true) break;//有一项不同则修改过录入信息
                }
                // 录入人
                $userId = M('tnettask_flowlog')->where(['taskid'=>$data['taskid'],'logtype'=>12])->order('createtime DESC')->getField('wx_id');
                // 记录变更
                if($isChangeInput == true && $userId != $wx_id){
                    $historyTable = $tableName.'_history';
                    $addRet = M($historyTable)->add($beforeInfo);
                    $afterInfo['fadid']           = $data['fadid'];
                    $afterInfo['fadname']         = $data['fadname'];
                    $afterInfo['fadclasscode']    = $data['fadclasscode'];
                    $afterInfo['fadclasscode_v2'] = $data['fadclasscode_v2'];
                    $afterInfo['fbrand']          = $data['fbrand'];
                    $afterInfo['fadowner']        = $data['fadowner'];
                    $afterInfo['fspokesman']      = $data['fspokesman'];
                    $afterInfo['fversion']        = $data['fversion'];
                    $afterInfo['modifier']        = $wxInfo['alias'];
                    $afterInfo['modifytime']      = time();
                    $addAfterRet = M($historyTable)->add($afterInfo);
                    // 减扣积分
                    $retScore = A('TaskInput/TaskInputScore','Model')->modifyTask($data['taskid'], $data['media_class'], 1, $userId, $wx_id);
                    // 增加录入积分
                    $scoreRes = A('TaskInput/TaskInputScore','Model')->addScoreBySubmitNetTask($data['taskid'], 1, '任务修改加分:');
                } 
            }
            
			$netIssueRes = M('tnetissue')->where(['major_key' => ['EQ', $data['major_key']]])->save($issueData);
			
			try{
				$identify_code = M('tnetissue_identify')->where(array('major_key'=>$data['major_key']))->getField('identify_code');
				$major_key_list = M('tnetissue_identify')->where(array('identify_code'=>$identify_code,'major_key'=>array('neq',$data['major_key'])))->getField('major_key',true);
				$add_tnetsam_handle = [array('major_key'=>$data['major_key'],'create_time'=>date('Y-m-d H:i:s'))];
				foreach($major_key_list as $mj){
					$add_tnetsam_handle[] = array('major_key'=>$mj,'create_time'=>date('Y-m-d H:i:s'));
				}
				M('tnetsam_handle')->addAll($add_tnetsam_handle);
				# 把这些样本id的任务改为违法判定已完成  $major_key_list
				if(count($major_key_list) > 0){
                    $issueData['finputuser'] = 32;
                    $issueData['fmodifier'] = 32;
					unset($issueData['major_key']);
                    M('tnetissue')->where(['major_key' => ['in', $major_key_list]])->save($issueData);
                }
				$issueData = array();
			}catch( \Exception $e) {
				
			}
            if (!$netIssueRes){
                $this->error = '样本更新失败';
                return false;
            }
        }
        return $this->submitTask($data['taskid'], $data, 2 ,$wx_id);
    }

    /**
     * @param $taskid
     * @param $data array   任务的广告录入信息或者违法判定信息, 不包括状态信息
     * @param $type int     环节类型
     * @param $wx_id
     * @return bool
     */
    private function submitTask($taskid, $data, $type, $wx_id)
    {
        $logType = $type.'2';
        // 更新流程
        $flowTableName = 'tnettask_flowlog';
        $flowData = [
            'taskid'     => $taskid,
            'tasklink'   => $type,
            'wx_id'      => $wx_id,
            'logtype'    => $logType,
            'log'        => $this->LogType[$logType],
            'creator'    => $wx_id,
            'createtime' => time()
        ];
        $flowRes = M($flowTableName)->add($flowData);
        // 获取用户环节权限,如:['1','2'],并判断下一环节状态
        if($wx_id != 0){
            $linkTypes = (new AdInputUserModel())->getTaskLinkAuth($wx_id);
            if($type < 2 && is_array($linkTypes) && count($linkTypes) > 1){
                $logType = ($type+1).'1';//下一环节进行中状态
            }
        }
        $data['wx_id'] = $wx_id;
        // 判断任务是否完成结束，如公益广告不需进行审核等条件 TODO:补充条件
        if($type == 2 || $wx_id == 0){
            // 整个任务完成状态2或wx_id为0时表示自动提交的审核环节
            $logType = 2;
            $data['priority'] = 0;//任务完成时将提升的优先级降为0，优化任务速度
        }
        $data['task_state'] = $logType;
        $where = [
            'taskid' => ['EQ', $taskid]
        ];
        // 更新任务的广告录入信息或者违法判定信息
        $saveRes = M('tnettask')->where($where)->save($data);
        // 任务最终完成时推送至客户(上海)样本表，仅上海地区(条件LEFT(fregionid,2) = 31)任务需要推送
        // $customerSampleId = A('Api/ShIssue')->push_netsample($taskid);
        // 任务完成时，质检、存证
        if($type == 2 && $wx_id != 0){
            // 是否质检
            $addRes = A('TaskInput/QualityInspection','Model')->addInsTask($data);
            // 违法广告自动存证
            if($data['fillegaltypecode'] != '0'){
                $url         = $data['net_target_url'];
                $title       = $data['taskid'];
                $fsample_id  = $data['major_key'];
                $fmediaclass = $data['media_class'];
                $result = A('Open/Evidence','Model')->addEvidence($url,$title,$fsample_id,$fmediaclass);
            }
        }
        // 第二次更新流程
        $flowData = [
            'taskid'     => $taskid,
            'tasklink'   => $type,
            'wx_id'      => $wx_id,
            'logtype'    => $logType,
            'log'        => $this->LogType[$logType],
            'creator'    => $wx_id,
            'createtime' => time()
        ];
        $flowRes = M($flowTableName)->add($flowData);
        // 日志
        $aliyunlog = [
            'type'      => 'save',
            'taskid'    => $taskid,
            'data'      => $data,
            'link_type' => $type,
            'msg'       => '提交任务成功;'
        ];
        A('Common/AliyunLog','Model')->net_task_log($aliyunlog);
        // 获取媒介类型, 计数
        $media_class = $data['media_class'];
        A('InputPc/Task','Model')->change_task_count($wx_id,$this->task_count_field_map[$media_class][$type],1);
        A('InputPc/Task','Model')->change_task_count($wx_id,$this->v3_task_count_map[$media_class],1);
        return true;
    }

    /**
     * @param $taskid
     * @param $data array   任务的广告录入信息或者违法判定信息, 不包括状态信息
     * @param $type int     环节类型
     * @param $wx_id
     * @return bool
     */
    private function submitTask_old_20191025($taskid, $data, $type, $wx_id)
    {
        $logType = $type.'2';
        // 更新流程
        $flowTableName = 'tnettask_flowlog';
        $flowData = [
            'taskid' => $taskid,
            'tasklink' => $type,
            'wx_id' => $wx_id,
            'logtype' => $logType,
            'log' => $this->LogType[$logType],
            'creator' => $wx_id,
            'createtime' => time()
        ];
        $flowRes = M($flowTableName)
            ->add($flowData);
        // if (!$flowRes){
        //     $this->error = '流程更新失败';
        //     return false;
        // }
        // 如果继续做环节2，则状态变更为21，避免12状态被多人抢到审核任务
        // $linkTypeArr = $data['linkTypeArr'];
        // 获取用户环节权限，如:['1','2']
        if($wx_id != 0){
            $linkTypes = (new AdInputUserModel())->getTaskLinkAuth($wx_id);
            if($type < 2 && is_array($linkTypes) && count($linkTypes) > 1){
                $logType = ($type+1).'1';//下一环节进行中状态
            }
        }
        $data['wx_id'] = $wx_id;
        // 判断任务是否完成结束，如公益广告不需进行审核等条件 TODO:补充条件
        if($type == 2 || $wx_id == 0){
            // 整个任务完成状态2或wx_id为0时表示自动提交的审核环节
            $logType = 2;
            $data['priority'] = 0;//任务完成时将提升的优先级降为0，优先任务速度
        }
        $data['task_state'] = $logType;
        $where = [
            'taskid' => ['EQ', $taskid]
        ];
        // 更新任务的广告录入信息或者违法判定信息
        $tableName = 'tnettask';
        $saveRes = M($tableName)
                ->where($where)
                ->save($data);
        // 任务最终完成时推送至客户(上海)样本表，仅上海地区(条件LEFT(fregionid,2) = 31)任务需要推送
        // $customerSampleId = A('Api/ShIssue')->push_netsample($taskid);
        file_put_contents('LOG/push_netsample.txt', date('c')."\n".implode("\n", $customerSampleId), FILE_APPEND | LOCK_EX);
        // 是否质检
        if($type == 2 && $wx_id != 0){
            $addRes = A('TaskInput/QualityInspection','Model')->addInsTask($data);
            file_put_contents('LOG/addInsTask.txt', date('c')."\n".implode("\n", $data), FILE_APPEND | LOCK_EX);
            // 违法广告自动存证
            if($data['fillegaltypecode'] != '0'){
                $url = $data['net_target_url'];
                $result = A('Open/Evidence','Model')->addEvidence($url);
            }
        }
        // 第二次更新流程
        $flowData = [
            'taskid' => $taskid,
            'tasklink' => $type,
            'wx_id' => $wx_id,
            'logtype' => $logType,
            'log' => $this->LogType[$logType],
            'creator' => $wx_id,
            'createtime' => time()
        ];
        $flowRes = M($flowTableName)->add($flowData);
        // 日志
        A('Common/AliyunLog','Model')->net_task_log([
            'type' => 'save',
            'taskid' => $taskid,
            'data' => $data,
            'link_type' => $type,
            'msg' => '提交任务成功;'
        ]);
        // 获取媒介类型, 计数
        $media_class = $data['media_class'];
        A('InputPc/Task','Model')->change_task_count($wx_id,$this->task_count_field_map[$media_class][$type],1);
        A('InputPc/Task','Model')->change_task_count($wx_id,$this->v3_task_count_map[$media_class],1);
        return true;
    }

    /**
     * @param $taskid
     * @param int $wx_id            当前用户id
     * @param int $mediaClass       媒体类型 1:电视 2: 广播 3: 报纸 13: 网络
     * @param int $linkType         环节类型 1:广告录入环节  2: 违法审核环节
     * @param string $reason        退回原因 如果以 '客户反馈' 开头, 则不计入统计
     * @return bool
     */
    public function rejectTask($taskid, $wx_id, $mediaClass, $linkType=1, $reason = '') //$linkType环节类型默认为1处理 2019-02-12
    {
        $time = time();
        $taskTable = 'tnettask';
        $taskInfo = M($taskTable)->find($taskid);
        // $userId = $taskInfo['wx_id'];
        // 退回环节负责人 TODO:linkType=0退回整个任务所有环节
        $userId = M('tnettask_flowlog')->where(['taskid'=>$taskid,'logtype'=>$linkType.'2'])->order('createtime DESC')->getField('wx_id');
        $taskState = $linkType.'3';
        $taskData = [
            'quality_wx_id' => $wx_id,
            'task_state' => $taskState,
            'syn_state' => 0,
            'lasttime' => $time,
            'modifytime' => $time,
            'wx_id' => $userId,
            'back_reason' => $this->LogType[$taskState] .$reason. ' 退回人:' .M('ad_input_user')->where(['wx_id' => ['EQ', $wx_id]])->cache(true, 60)->getField('alias'),
        ];
        $flowData = [
            'taskid' => $taskid,
            'tasklink' => $linkType,
            'wx_id' => $userId,
            'logtype' => $taskState,
            'log' => $this->LogType[$taskState],
            'creator' => $wx_id,
            'createtime' => time()
        ];
        $where = [
            'taskid' => ['EQ', $taskid],
        ];
        $taskUpdate = M($taskTable)->where($where)->save($taskData);
        if (!$taskUpdate){
            $this->error = '任务更新失败';
            return false;
        }
        $flowTable = 'tnettask_flowlog';
        $flowUpdate = M($flowTable)->add($flowData);
        if (!$flowUpdate){
            $this->error = '任务流程更新失败';
            return false;
        }
        if (mb_substr($reason, 0, 4) !== '客户反馈'){
            if ($linkType == 1){
                A('InputPc/Task','Model')->change_task_count($userId, 'v3_task_back', 1);
                A('InputPc/Task','Model')->change_task_count($userId, 'input_err', 1);
                A('InputPc/Task','Model')->change_task_count($wx_id,'v3_quality', 1);
            }elseif ($linkType == 2){
                A('InputPc/Task','Model')->change_task_count($userId, 'inspect_err', 1);
                A('InputPc/Task','Model')->change_task_count($userId, 'v3_task_back', 1);
                A('InputPc/Task','Model')->change_task_count($wx_id,'v3_quality', 1);
            }else{
                A('InputPc/Task','Model')->change_task_count($userId, 'v3_task_back', 2);
                A('InputPc/Task','Model')->change_task_count($wx_id,'v3_quality', 2);
            }
            // 更新积分
            $scoreModel = new TaskInputScoreModel();
            if ($linkType == 0){
                $scoreUpdate = $scoreModel->rejectTask($taskid, $mediaClass, 1, $userId, $wx_id);
                $scoreUpdate = $scoreModel->rejectTask($taskid, $mediaClass, 2, $userId, $wx_id);
            }else{
                $scoreUpdate = $scoreModel->rejectTask($taskid, $mediaClass, $linkType, $userId, $wx_id);
            }
        }
        $adName = M('tad')->where(['fadid' => ['EQ', $taskInfo['fadid']]])->getField('fadname');
        A('Adinput/Weixin','Model')->task_back($userId,$reason,$adName);
        A('Common/AliyunLog','Model')->task_input_log([
            'type' => 'save',
            'task' => $taskInfo,
            'media_class' => $mediaClass,
            'reason' =>  $reason,
            'linkType' => $linkType,
            'msg' => '任务被退回',
        ]);
        return true;
    }

    /**
     * 获取日志类型属性
     * @Param String $code 类型代码，不传则返回列表数组
     */
    public function getLogType($code){
        if($code){
            return $this->LogType[$code];
        }else{
            return $this->LogType;
        }
    }

    /**
     * 错误信息
     */
    public function getError(){
        return $this->error;
    }

    /**
     * (即将弃用)
     */
    public function getNetTaskTrans()
    {
        $wx_id = intval(session('wx_id'));
        // 获取用户所在组的权限信息
        $userGroupInfo = M('ad_input_user_group')
            ->field('ad_input_user_group.other_authority, ad_input_user_group.classauthority, ad_input_user_group.mediaauthority, ad_input_user_group.regionauthority, ad_input_user_group.dateauthority, ad_input_user.prefer_list, ad_input_user.priority_list')
            ->join('ad_input_user on ad_input_user.group_id = ad_input_user_group.group_id')
            ->where(array('ad_input_user.wx_id'=>$wx_id))
            ->find();
        $linkTypes = (new AdInputUserModel())->getTaskLinkAuth($wx_id); // 录入权限
        // 判断广告信息录入权限
        $taskid = M('tnettask')
            ->alias('task')
            ->join('task_input_link as link on task.taskid = link.taskid')
            ->where([
                'link.type' => ['IN', $linkTypes],
                'link.state' => ['EQ', 1],
                'link.media_class' => ['EQ', 13],
                'task.task_state' => ['EQ', 1],
                'task.wx_id' => ['EQ', $wx_id],
            ])
            ->getField('task.taskid');
        if($taskid) return $taskid;//如果有正在执行的任务，直接返回正在执行的任务id
        // 获取媒体偏好
        $mediaPrefer = $userGroupInfo['prefer_list'];
        // 获取优先级偏好
        $priorityList = $userGroupInfo['priority_list'];

        $searchWhere = [
            'link.type' => ['IN', $linkTypes],
            'link.state' => ['EQ', 0],
            'link.media_class' => ['EQ', 13],
            'task.isad' => ['EQ', 1],
            'task.task_state' => ['EQ', 0],
        ];
        $mediaIds = [];
        // 生成搜索条件及子查询
        if ($userGroupInfo['mediaauthority'] !== 'unrestricted'){
            if (!$userGroupInfo['mediaauthority'] || $userGroupInfo['mediaauthority'] === '""'){
                return false;
            }
            $mediaIds = array_merge($mediaIds, json_decode($userGroupInfo['mediaauthority'], true));
            $searchWhere['task.media_id'] = ['IN', $mediaIds];
        }
        // 地区限制的条件
        if ($userGroupInfo['regionauthority'] !== 'unrestricted'){
            if (!$userGroupInfo['regionauthority'] || $userGroupInfo['regionauthority'] === '""'){
                return false;
            }
            $userGroupInfo['regionauthority'] = json_decode($userGroupInfo['regionauthority'], true);
            // 区分并组合查找本级的和下属的条件, 生成mediaOwner的子查询搜索条件
            $inArr = [];
            $leftArr = [];
            foreach ($userGroupInfo['regionauthority'] as $item) {
                $len = strlen($item);
                if ($len == 6){
                    $inArr[] = $item;
                }else{
                    $leftArr[] = "(LEFT(fregionid, $len) = $item)";
                }
            }
            // 搜索条件有2个部分, 一个是全部地区id的本级媒体, 一个是部分地区id本级及下属媒体,互相之间的关系应该是OR
            $_mediaOwnerWhere = [
                '_logic' => 'OR',
            ];
            if (count($inArr) != 0){
                $_mediaOwnerWhere['fregionid'] = ['IN', $inArr];
            }
            if (count($leftArr) != 0){
                $_string = implode(' OR ', $leftArr);
                $_mediaOwnerWhere['_string'] = $_string;
            }
            // 生成mediaOwner子查询
            $subQuery = M('tmediaowner')->field('fid')->where($_mediaOwnerWhere)->buildSql();
            $_mediaWhere = [
                'fmediaownerid' => ['EXP', 'IN '. $subQuery]
            ];
            $res = M('tmedia')->where($_mediaWhere)->getField('fid', true);
            // 得到允许的媒体id
            $mediaIds = array_merge($mediaIds, $res);
            $searchWhere['task.media_id'] = ['IN', $mediaIds];
        }
        // 加入媒体偏好元素
        if ($mediaPrefer){
            $mediaPrefer = explode(',', $mediaPrefer);
            if ($userGroupInfo['mediaauthority'] !== 'unrestricted') {
                $preferList = array_intersect($mediaPrefer, $mediaIds);
            }else{
                $preferList = $mediaPrefer;
            }
            $preferList = implode(',', $preferList);
        }
        if (!$preferList){
            $preferList = "null";
        }
        // 加入优先级偏好元素
        if (!$priorityList){
            $priorityList = "null";
        }
        // 发布日期限制条件
        if ($userGroupInfo['dateauthority'] !== 'unrestricted'){
            if (!$userGroupInfo['dateauthority'] || $userGroupInfo['dateauthority'] === '""'){
                return false;
            }
            // 保留，日期限制条件
            // $dateauthority = json_decode($userGroupInfo['dateauthority'], true);
            // 临时，日期限制条件固化为近为30天，屏蔽众包分组日期限制条件，设置对互联网广告的失效但不影响传统广告
            $dateauthority = [date('Y-m-d',strtotime('-30 day')).' , '.date('Y-m-d')]; //日期偏好数组每个元素为一个时间段，每个时间段起始用,逗号分隔，注意逗号前后有空格

            $dateWhere = [];
            foreach ($dateauthority as $key => $value) {
                $date_a_e = explode(' , ', $value);
                foreach($date_a_e as $k=>$v){
                    $date_a_e[$k] = strtotime($v);
                }
                $dateWhere[] = ['BETWEEN', $date_a_e];
            }
            $dateWhere[] = 'OR';
            $searchWhere['task.net_created_date'] = $dateWhere;
        }
        // 暂时使用classauthority这个字段限制互联网广告的优先级, 后期考虑加个字段限制
        if (!in_array($userGroupInfo['classauthority'], [null, '', '""', 'unrestricted'])){
            $searchWhere['priority'] = ['ELT', $userGroupInfo['classauthority']];
        }
        //临时处理，强制只处理高于指定优先级的任务，以减少数据量，加快任务页面加载速度,覆盖上面优先级处理 2019-01-04
        $net_ad_priority = sys_c('net_ad_priority') ? sys_c('net_ad_priority') : 10;
        // $searchWhere['priority'] = ['EGT', $net_ad_priority];

        $while_num = 0;
        $e_task_state = false;
        while(!$e_task_state){
            $while_num++;
            $orderArr = [];
            if ($priorityList !== 'null'){
                $orderArr[] = "field(task.priority, $priorityList) desc";
            }
            if ($preferList !== 'null'){
                $orderArr[] = "field(task.media_id, $preferList) desc";
            }
            $orderArr[] = 'task.priority desc';
            $orderArr[] = 'task.net_created_date desc';
            $order = implode(',', $orderArr);
            $taskids = M('tnettask')
                ->alias('task')
                ->field('task.taskid')
                ->join('task_input_link as link on task.taskid = link.taskid')
                ->where($searchWhere)
                ->group('task.taskid')
                ->order($order)
                ->limit(150)
                ->select();
            if(!$taskids) return false;//如果没有获取到任务，就返回false
            shuffle($taskids);
            foreach ($taskids as $taskid) {
                $taskid = $taskid['taskid'];
                M()->startTrans();
                $e_task_state = M('task_input_link')
                    ->where([
                        'taskid'      => ['EQ', $taskid],
                        'media_class' => ['EQ', 13],
                        'state'       => ['EQ', 0],
                        'type'        => ['IN',$linkTypes],//解决无提交按钮的BUG 2019-01-19
                    ])
                    ->save([
                        'wx_id' => $wx_id,
                        'state' => 1,
                        'update_time' => time()
                    ]);
                if (!empty($e_task_state)){
                    $e_task_state = M('tnettask')
                        ->where([
                            'taskid' => ['EQ', $taskid],
                            'task_state' => ['EQ', 0],
                        ])
                        ->save([
                            'wx_id' => $wx_id,
                            'task_state' => 1,
                            'modifytime' => time()
                        ]);
                }
                if($e_task_state){
                    M()->commit();
                    return $taskid;
                }
                M()->rollback();
            }
            if($while_num > 10) return false;//如果抢了10次还没有任务，那就返回false
        }

    }

	/*修改网络广告优先级*/
	public function change_net_priority(){
		$msectime = msectime();
		$scount = M('tnettask')->where(array('priority'=>array('gt',0),'task_state'=>0))->count();
		if($scount > 100){
			return 0;
		}
		$dataList = M('tnettask')->where(array('task_state'=>0,'priority'=>0))->order('net_created_date desc')->limit(100)->getField('taskid',true);
        if(!empty($dataList)){
            $e_state = M('tnettask')->where(array('taskid'=>array('in',$dataList)))->save(array('priority'=>9));
            file_put_contents('LOG/tnettask2',date('Y-m-d H:i:s') . '	'.$scount.'	'.(msectime() - $msectime).'	'.implode(',',$dataList)."\n",FILE_APPEND);
            return $e_state;
        }else{
            return false;
        }
	}
}
