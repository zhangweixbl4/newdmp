<?php
namespace Adinput\Controller;
use Think\Controller;
use Think\Exception;

class VideoCuttingController extends Controller {
	
	/*登录引导页面（pc）*/
    public function login($customer=''){
		if($_SERVER['HTTP_HOST'] == 'dmp.goldclippers.com'){
			header("Location:http://dmp.hz-data.com".U('')); 
			exit;
		}
		$createNoncestr = createNoncestr();//生成随机数
		$login_url = 'http://'.$_SERVER['HTTP_HOST'].U('Adinput/VideoCutting/wx_login',array('createNoncestr'=>$createNoncestr));//二维码地址
		
		S('jjz_wx_createNoncestr_'.$createNoncestr,rand(1000,9999),120);//创建登录随机数缓存

		$wx_qr = http(
						'https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token='.wx_token(),
						'{"expire_seconds": 120, "action_name": "QR_STR_SCENE", "action_info": {"scene": {"scene_str": "confirm_login_'.$createNoncestr.'"}}}',
						'POST'
						);
		#file_put_contents('LOG/wx_qr',$wx_qr."\n\n",FILE_APPEND);
		$wx_qr = json_decode($wx_qr,true);
		
		//var_dump($wx_qr['url']);		
		$login_url = $wx_qr['url'];
		//var_dump($login_url);
        $this->assign('tag', $customer ? 1 : 0);
		$this->assign('createNoncestr',$createNoncestr);//随机数
		$this->assign('login_url',$login_url);//二维码地址
		$this->display();
	}
	
	/*登录引导*/
    public function get_wx_login(){

		$createNoncestr = createNoncestr();//生成随机数
		
		S('jjz_wx_createNoncestr_'.$createNoncestr,rand(1000,9999),120);//创建登录随机数缓存

		$wx_qr = http(
						'https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token='.wx_token(),
						'{"expire_seconds": 120, "action_name": "QR_STR_SCENE", "action_info": {"scene": {"scene_str": "confirm_login_'.$createNoncestr.'"}}}',
						'POST'
						);
		$wx_qr = json_decode($wx_qr,true);

		$this->ajaxReturn(array('createNoncestr'=>$createNoncestr,'qr_url'=>$wx_qr['url']));


	}
	
	
	
	
	
	
	/*手机登录提交页面*/
	public function wx_login(){
		$user_info = get_user_info();//获取用户信息
		$createNoncestr = I('createNoncestr');//获取get传递的随机数
		M('ad_input_user')->where(array('wx_id'=>$user_info['wx_id']))->save(array('video_cutting_token'=>$createNoncestr,'video_cutting_login_state'=>0));//记录手机已扫码
		
		$wx_jssdk_config = A('Adinput/Weixin','Model')->wx_jssdk_config();
		$this->assign('user_info',$user_info);
		$this->assign('wx_jssdk_config',$wx_jssdk_config);
		$this->display();
	}
	
	/*pc获取登录状态*/
	public function get_login(){
		$createNoncestr = I('createNoncestr');
		$group_id = intval(I('get.group_id'));//分组id
		$user_info = M('ad_input_user')->field('subscribe,video_cutting_login_state,nickname,headimgurl,wx_id,have_group_id')->where(array('video_cutting_token'=>$createNoncestr))->find();//数据库里面查询用户信息
		
		
		if($user_info && $group_id > 0 && in_array($group_id,explode(',',$user_info['have_group_id']))){//查询到用户且分组id大于0且该用户拥有该组权限
			$rr = M('ad_input_user')->where(array('wx_id'=>$user_info['wx_id']))->save(array('group_id'=>$group_id));//修改分组 
			if($rr > 0){
				S('user_wx_id' . $user_info['wx_id'] . 'group_id_key', null);
				sleep(1);
			}	
		}
		
		
		
		if($user_info['video_cutting_login_state'] === '1'){//登录成功

			session('wx_id',$user_info['wx_id']);
			$this->ajaxReturn(array('code'=>0,'msg'=>'登录成功,可以开始在PC端操作','user_info'=>$user_info,'url'=>C('VIDEO_CUTTING_URL').'index.php?token='.$createNoncestr));
		}elseif($user_info['video_cutting_login_state'] === '0'){//扫码成功，等待手机确认
			if($user_info['subscribe'] === '0'){
				$user_info['nickname'] = '您未关注公众号,请扫码关注';
				$user_info['headimgurl'] = C('WX_QRCODE_URL');//公众号的二维码地址
				$this->ajaxReturn(array('code'=>100,'msg'=>'请扫码以上二维码关注','url'=>'','user_info'=>$user_info));
			}
			$this->ajaxReturn(array('code'=>100,'msg'=>'扫码成功,请在手机上确认','url'=>'','user_info'=>$user_info));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'','url'=>''));
		}
		
		
	}
	/*用户名密码登录*/
	public function user_login(){
		$createNoncestr = I('createNoncestr');
		$group_id = intval(I('get.group_id'));//分组id
		if($createNoncestr == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'请求参数不完整','url'=>''));
		$user = I('user');//用户名密码登录
		$pass = I('pass');//密码
		$where = array();
		$where['mobile|mail'] = $user;
		$where['password'] = md5($pass);
		$user_info = M('ad_input_user')->field('subscribe,video_cutting_login_state,nickname,headimgurl,wx_id, alias, mobile,have_group_id')->where($where)->find();//数据库里面查询用户信息
		
		if($user_info && $group_id > 0 && in_array($group_id,explode(',',$user_info['have_group_id']))){//查询到用户且分组id大于0且该用户拥有该组权限
			M('ad_input_user')->where(array('wx_id'=>$user_info['wx_id']))->save(array('group_id'=>$group_id));//修改分组
		}
		//var_dump($user_info);
		if($user_info){//登录成功
		    // 密码简单并且没有发过短信, 发送验证码短信
            if ($pass == '123456' && preg_match('/^1[34578]\d{9}$/', $user_info['mobile']) && !S('taskInputLoginLock_'.$user)){
                $captchaCode = mt_rand(0, 9).mt_rand(0, 9).mt_rand(0, 9).mt_rand(0, 9);
                S('taskInputLoginCaptcha_'.$user, $captchaCode, 60);
                try{
                    $sendSmsRes = A('Common/Alitongxin','Model')->identifying_sms($user_info['mobile'],$captchaCode,'登录验证码');
                }catch (Exception $e){

                }
                A('Adinput/Weixin','Model')->wx_captcha($user_info['wx_id'],$captchaCode);
                S('taskInputLoginLock_'.$user, 'true', 60);
                $this->ajaxReturn(array('code'=>-2,'msg'=>'验证码短信已发送, 请输入手机验证码, 有效时间60秒'));
            }
            $captcha = I('captcha');
            if (S('taskInputLoginLock_'.$user) && !$captcha){
                $this->ajaxReturn(array('code'=>-2,'msg'=>'请输入手机验证码','url'=>''));
            }
            if (S('taskInputLoginLock_'.$user) && $captcha != S('taskInputLoginCaptcha_'.$user)){
                $this->ajaxReturn(array('code'=>-2,'msg'=>'验证码错误','url'=>''));
            }
            $personInfoCookie = array('wx_id'=>sys_auth($user_info['wx_id']),'alias'=>sys_auth($user_info['alias']));
            cookie('wxInfo',$personInfoCookie,array('expire'=>3600*24*30));//保存用户cookie
            session('wx_id',$user_info['wx_id']);
			M('ad_input_user')->where(array('wx_id'=>$user_info['wx_id']))->save(array('video_cutting_token'=>$createNoncestr,'video_cutting_login_state'=>1));//
			$this->ajaxReturn(array('code'=>0,'msg'=>'登录成功','user_info'=>$user_info,'url'=>C('VIDEO_CUTTING_URL').'index.php?token='.$createNoncestr));

		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'用户名或密码错误','url'=>''));
		}
		
		
		
	}
	
	/*手机确认登录*/
	public function enter_wx_login(){
		$user_info = get_user_info();//获取用户信息
		$enter = I('enter');
		$createNoncestr = I('createNoncestr');
		if(!S('jjz_wx_createNoncestr_'.$createNoncestr)){//判断登录随机数缓存是否存在
			$this->ajaxReturn(array('code'=>-1,'msg'=>'登录失败,请重新扫描'));
		}
		if($user_info['mobile'] == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'您未绑定手机'));
		if(M('ad_input_user')->where(array('video_cutting_token'=>$createNoncestr))->count() == 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'登录失败,请重新扫描'));
		}
		
		if($enter == 'true'){
			$rr = M('ad_input_user')->where(array('wx_id'=>$user_info['wx_id']))->save(array('video_cutting_token'=>$createNoncestr,'video_cutting_login_state'=>1));
		}

		
		if($rr > 0){
			S('jjz_wx_createNoncestr_'.$createNoncestr,null);//登录成功需要清空缓存
			$this->ajaxReturn(array('code'=>0,'msg'=>'登录成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'登录失败'));
		}
		
		
	}
	
	/*发送绑定手机验证码*/
	public function send_sms(){
		$user_info = get_user_info();//获取用户信息
		if(!$user_info){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'无用户信息,发送失败'));
		}
		
		$telephone = I('telephone');
		$code = rand(100000,999999);
		$rr = A('Common/Alitongxin','Model')->identifying_sms($telephone,$code,'绑定手机');
		if($rr['code'] == 0){
			S('identifying_sms_'.$telephone,$code,600);
			$this->ajaxReturn(array('code'=>0,'msg'=>'发送成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>$rr['msg']));
		}	
		
	}
	
	/*绑定手机号*/
	public function bound_telephone(){
		$user_info = get_user_info();//获取用户信息
		if(!$user_info){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'无用户信息,失败'));
		}
		$telcode = I('telcode');
		$telephone = I('telephone');
		
		if($telcode != S('identifying_sms_'.$telephone)){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'验证码错误'));
		}
		
		if(M('ad_input_user')->where(array('mobile'=>$telephone))->count() > 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'你不能绑定该手机号,请联系管理员'));
		}
		
		$rr = M('ad_input_user')->where(array('wx_id'=>$user_info['wx_id']))->save(array('mobile'=>$telephone));
		
		if($rr){
			$this->ajaxReturn(array('code'=>0,'msg'=>'手机绑定成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'手机绑定失败'));
		}
		

		
		
	}
	
	/*外部登录*/
	public function wbLogin(){
		
		$wbUrl = I('get.wbUrl');
		$wbUrl = urldecode($wbUrl);
		$createNoncestr = createNoncestr();//生成随机数
		$wx_id = intval(session('wx_id'));	
		$loginInfo = M('ad_input_user')->field('wx_id,nickname,mobile,alias')->where(array('wx_id'=>$wx_id))->find();
		S('wxLogin_'.$createNoncestr,$loginInfo,60);
		$targetUrl = $wbUrl.$createNoncestr;
		header('Location:'.$targetUrl);
		#var_dump($targetUrl);

	}
	
	/*获取登录信息*/
	public function getLoginInfo(){
		$createNoncestr = I('get.createNoncestr');
		
		$loginInfo = S('wxLogin_'.$createNoncestr);
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','loginInfo'=>$loginInfo));
	}
	
	#通过手机号获取用户id
	public function mobile_to_wxid(){
		$mobile = I('get.mobile');
		if(!$mobile) $this->ajaxReturn(array('code'=>-1,'msg'=>'手机号不能为空'));
		$loginInfo = M('ad_input_user')->field('wx_id,nickname,mobile,alias')->where(array('mobile'=>$mobile))->find();
		if(!$loginInfo) $this->ajaxReturn(array('code'=>-1,'msg'=>'不存在该用户'));
		$this->ajaxReturn(array('code'=>0,'msg'=>'','info'=>$loginInfo));
	}
	
	
	
	
	
	
	
	
	
	

}