<?php
namespace Agp\Controller;
use Think\Controller;
use Agp\Model\StatisticalReportModel;
class OutWordHzController extends BaseController{

    /**
     * 获取历史报告列表
     * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（count-记录总数，list-列表数据）
     * by zw
     */
    public function index() {
        session_write_close();
        header("Content-type:text/html;charset=utf-8");

        $p                  = I('page', 1);//当前第几页
        $pp                 = 20;//每页显示多少记录
        $pnname             = I('pnname');// 报告名称
        $pntype             = I('pntype');// 报告类型
        $pnfiletype         = I('pnfiletype');// 文件类型
        $pncreatetime       = I('pnendtime');//报告时间
        $system_num = getconfig('system_num');

        if (!empty($pnname)) {
            $where['pnname'] = array('like', '%' . $pnname . '%');//报告名称
        }
        if (!empty($pntype)) {
            $where['pntype'] = $pntype;//报告类型
        }else{
            $where['pntype'] = array('in',array(10,20,30,70,80));//报告类型
        }
        if (!empty($pnfiletype)) {
            $where['pnfiletype'] = $pnfiletype;//文件类型
        }
        if(!empty($pncreatetime)){
            $where['pnendtime'] = array('between',array($pncreatetime[0],$pncreatetime[1]));
        }   
        $where['pntrepid'] = session('regulatorpersonInfo.fregulatorpid');
        $where['fcustomer']  = $system_num;

        $count = M('tpresentation')
            ->where($where)
            ->count();

        
        $data = M('tpresentation')
            ->where($where)
            ->order('pncreatetime desc')
            ->page($p,$pp)
            ->select();
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
    }

    //报告删除接口
    public function tpresentation_delete() {

        $pnid         = I('pnid');//报告ID
        $user = session('regulatorpersonInfo');//获取用户信息
        $system_num = getconfig('system_num');

        $where['pnid'] = $pnid;
        $where['pntrepid'] = $user['fregulatorpid'];
        $where['fcustomer']  = $system_num;

        $data = M('tpresentation')->where($where)->delete();

        if($data > 0){
            $this->ajaxReturn(array('code'=>0,'msg'=>'删除成功！'));
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'您无权删除此报告！'));
        }
    }



    //菏泽月报下级
    public function create_month_xj(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型
        $month = I('daytime',date('Y-m',time()));//接收时间  TODO::时间改2018
        //$month = I('daytime','2018-11');//接收时间  TODO::时间改
        $month_num = strval(substr($month,5,2));//月份
        $year_num = strval(substr($month,0,4));//年份
        if($month_num < 10 && $year_num <= 2018){
            $this->ajaxReturn([
                "code" => 1,
                "msg" => '抱歉，只能导出2018年10月以及十月之后的报告！',
            ]);
        }
        if($month == '2018-10'){

            $report_start_date = strtotime('2018-10-17');
        }else{
            $report_start_date = strtotime($month);
        }
        //$report_end_date = mktime(23, 59, 59, date('m', strtotime($month))+1, 00);//指定月份月末时间戳
        $report_end_date = strtotime($month.'-'.date('t', strtotime($month)).' 23:59:59');

        if($report_end_date > time()){
            $report_end_date = time();

        }

        $date_ym = date('Y年m月',$report_start_date);//年月字符

        $user = session('regulatorpersonInfo');//获取用户信息
        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级
        if($re_level == 2 || $re_level == 3 || $re_level == 4){
            $dq_name = '市级';
            $r_name = '全市';
            $xj_name = '区县';
        }elseif($re_level == 1){
            $dq_name = '全省';
            $xj_name = '市级';
        }else{
            $dq_name = '全'.$user['regionname1'];
            $xj_name = '地方';
        }
        $date_table = date('Ym',strtotime($month)).'_'.substr($user['regionid'],0,2);//组合表

        if(!table_exist("ttvissue_".$date_table) || !table_exist("tbcissue_".$date_table)){
            $this->ajaxReturn([
                'code'=>1,
                'msg'=>'生成失败，时间选择有误！'
            ]);
        }
        $check_datas = get_check_dates();
        $xj_dates = 0;
        $check_days = [];
        foreach ($check_datas as $check_datas_val){
            if(strtotime($check_datas_val) >= $report_start_date && strtotime($check_datas_val)<= $report_end_date){
                if(date('m',strtotime($check_datas_val)) == 10 && count($check_days) < 10){
                    $check_days[] = $check_datas_val;
                }
                $xj_dates++;
            }
        }
        if(!empty($check_days)){
            $check_datas = $check_days;
            $xj_dates = count($check_datas);
        }

        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数
        $owner_media_ids = get_owner_media_ids();

        /* 各大媒体类型数量 */
        $ct_media_tv_count_hz = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//菏泽市局电视媒体数量

        $ct_media_bc_count_hz = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//菏泽市局广播媒体数量

        $ct_media_paper_count_hz = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//菏泽市局报纸媒体数量

        $ct_media_tv_count_xj = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//菏泽各区县电视媒体数量

        $ct_media_bc_count_xj = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//菏泽各区县广播媒体数量

        $ct_media_paper_count_xj = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//菏泽各区县报纸媒体数量

        $ct_media_count = $ct_media_tv_count_hz + $ct_media_bc_count_hz + $ct_media_paper_count_hz + $ct_media_tv_count_xj + $ct_media_bc_count_xj + $ct_media_paper_count_xj;//全部有数据的媒体数量


        /*
        * @市级各媒体监测总情况
        *
        **/

        $ct_jczqk_bj = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_count_rate','fmediaclass',false,2);//本市传统媒体监测总情况

        $ct_fad_times_bj = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times_bj = 0;//定义传统媒体发布违法广告条次
        $ct_fad_count_bj = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_count_bj = 0;//定义传统媒体发布违法广告条次
        $every_media_class_bj_data = [];//定义媒体发布监测情况数组
//循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk_bj as $ct_jczqk_bj_val){
            $ct_fad_times_bj += $ct_jczqk_bj_val['fad_times'];
            $ct_fad_illegal_times_bj += $ct_jczqk_bj_val['fad_illegal_times'];
            $ct_fad_count_bj += $ct_jczqk_bj_val['fad_count'];
            $ct_fad_illegal_count_bj += $ct_jczqk_bj_val['fad_illegal_count'];
            $ct_data_val = [
                $ct_jczqk_bj_val['tmediaclass_name'],
                $ct_jczqk_bj_val['fad_times'],
                $ct_jczqk_bj_val['fad_count'],
                $ct_jczqk_bj_val['fad_illegal_times'],
                $ct_jczqk_bj_val['fad_illegal_count'],
                $ct_jczqk_bj_val['fad_illegal_times_rate'].'%',
                $ct_jczqk_bj_val['fad_illegal_count_rate'].'%'
            ];
            if($ct_jczqk_bj_val['fmedia_class_code'] == '01'){
                $every_media_class_bj_data[0] = $ct_data_val;
            }elseif($ct_jczqk_bj_val['fmedia_class_code'] == '02'){
                $every_media_class_bj_data[1] = $ct_data_val;
            }else{
                $every_media_class_bj_data[2] = $ct_data_val;
            }
        }

        $every_media_class_bj_data[3] = [
            '合计',
            strval($ct_fad_times_bj),
            strval($ct_fad_count_bj),
            strval($ct_fad_illegal_times_bj),
            strval($ct_fad_illegal_count_bj),
            round((($ct_fad_illegal_times_bj)/($ct_fad_times_bj))*100,2).'%',
            round((($ct_fad_illegal_count_bj)/($ct_fad_count_bj))*100,2).'%'
        ];//二、各类媒体广告发布情况
        $every_media_class_bj_data = array_values($every_media_class_bj_data);


                /*
        * @各县各媒体监测总情况
        *
        **/
        $ct_fad_times_xj = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times_xj = 0;//定义传统媒体发布违法广告条次
        $ct_fad_count_xj = 0;
        $ct_fad_illegal_count_xj = 0;
        $every_media_class_xj_data = [];//定义媒体发布监测情况数组

        $ct_jczqk_xj = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_count_rate','fmediaclass',false,3,$check_datas);//传统媒体监测总情况
//循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk_xj as $ct_jczqk_xj_val){
            $ct_fad_times_xj += $ct_jczqk_xj_val['fad_times'];
            $ct_fad_illegal_times_xj += $ct_jczqk_xj_val['fad_illegal_times'];
            $ct_fad_count_xj += $ct_jczqk_xj_val['fad_count'];
            $ct_fad_illegal_count_xj += $ct_jczqk_xj_val['fad_illegal_count'];
            $ct_data_val = [
                $ct_jczqk_xj_val['tmediaclass_name'],
                $ct_jczqk_xj_val['fad_times'],
                $ct_jczqk_xj_val['fad_count'],
                $ct_jczqk_xj_val['fad_illegal_times'],
                $ct_jczqk_xj_val['fad_illegal_count'],
                $ct_jczqk_xj_val['fad_illegal_times_rate'].'%',
                $ct_jczqk_xj_val['fad_illegal_count_rate'].'%'
            ];
            if($ct_jczqk_xj_val['fmedia_class_code'] == '01'){
                $every_media_class_xj_data[0] = $ct_data_val;
            }elseif($ct_jczqk_xj_val['fmedia_class_code'] == '02'){
                $every_media_class_xj_data[1] = $ct_data_val;
            }else{
                $every_media_class_xj_data[2] = $ct_data_val;
            }
        }

        $every_media_class_xj_data[3] = [
            '合计',
            strval($ct_fad_times_xj),
            strval($ct_fad_count_xj),
            strval($ct_fad_illegal_times_xj),
            strval($ct_fad_illegal_count_xj),
            round((($ct_fad_illegal_times_xj)/($ct_fad_times_xj))*100,2).'%',
            round((($ct_fad_illegal_count_xj)/($ct_fad_count_xj))*100,2).'%'
        ];//二、各类媒体广告发布情况
        $every_media_class_xj_data = array_values($every_media_class_xj_data);


        /*
        * @各地域监测总情况
        *
        **/
        $every_region_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times_rate','fregionid',false,3,$check_datas);//各地域监测总情况
        $every_region_data_xj = [];//定义各地区发布监测情况数组
//循环赋值
        $every_region_illegal_times_data = [
            [''],
            ['']
        ];
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){
            $every_region_fad_times += $every_region_jczqk_val['fad_times'];
            $every_region_fad_illegal_times += $every_region_jczqk_val['fad_illegal_times'];
            $every_region_fad_count += $every_region_jczqk_val['fad_count'];
            $every_region_fad_illegal_count += $every_region_jczqk_val['fad_illegal_count'];
            //if($every_region_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_region_illegal_times_data[0],$every_region_jczqk_val['tregion_name']);
                array_push($every_region_illegal_times_data[1],$every_region_jczqk_val['fad_illegal_times_rate'].'%');
            //}

            $every_region_data_xj[] = [
                strval($every_region_jczqk_key+1),
                $every_region_jczqk_val['tregion_name'],
                $every_region_jczqk_val['fad_times'],
                $every_region_jczqk_val['fad_count'],
                $every_region_jczqk_val['fad_illegal_times'],
                $every_region_jczqk_val['fad_illegal_count'],
                $every_region_jczqk_val['fad_illegal_times_rate'].'%',
                $every_region_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_region_data_xj[] = [
            '',
            '合计',
            strval($every_region_fad_times),
            strval($every_region_fad_count),
            strval($every_region_fad_illegal_times),
            strval($every_region_fad_illegal_count),
            round((($every_region_fad_illegal_times)/($every_region_fad_times))*100,2).'%',
            round((($every_region_fad_illegal_count)/($every_region_fad_count))*100,2).'%'
        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）



        /*
* @各地域电视广播报纸监测总情况
*
**/
        $every_media_all_data = [];
        $every_media_illegal_times_all_data = [];
        foreach (['01','02','03'] as $every_media_val){
            $every_media_data = [];
            $every_media_illegal_times_data = [];
            $every_media_jczqk = [];
            //循环赋值
            $every_media_fad_times = 0;
            $every_media_fad_illegal_times = 0;
            $every_media_fad_count = 0;
            $every_media_fad_illegal_count = 0;
            $every_media_illegal_times_data = [
                [''],
                ['']
            ];
            $every_media_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,$every_media_val,'','fad_illegal_times_rate','fmediaid',false,3,$check_datas);//各地域监测总情况
            foreach ($every_media_jczqk as $every_media_jczqk_key => $every_media_jczqk_val){
                $every_media_fad_times += $every_media_jczqk_val['fad_times'];
                $every_media_fad_illegal_times += $every_media_jczqk_val['fad_illegal_times'];
                $every_media_fad_count += $every_media_jczqk_val['fad_count'];
                $every_media_fad_illegal_count += $every_media_jczqk_val['fad_illegal_count'];
                if($every_media_jczqk_val['fad_illegal_times'] > 0){
                    array_push($every_media_illegal_times_data[0],$every_media_jczqk_val['tmedia_name']);
                    array_push($every_media_illegal_times_data[1],$every_media_jczqk_val['fad_illegal_times_rate'].'%');
                }

                $every_media_data[] = [
                    strval($every_media_jczqk_key+1),
                    $every_media_jczqk_val['tregion_name'],
                    $every_media_jczqk_val['tmedia_name'],
                    $every_media_jczqk_val['fad_times'],
                    $every_media_jczqk_val['fad_count'],
                    $every_media_jczqk_val['fad_illegal_times'],
                    $every_media_jczqk_val['fad_illegal_count'],
                    $every_media_jczqk_val['fad_illegal_times_rate'].'%',
                    $every_media_jczqk_val['fad_illegal_count_rate'].'%'
                ];
            }
            /*            $every_media_data[] = [
                            '',
                            '合计',
                            strval($every_media_fad_times),
                            strval($every_media_fad_count),
                            strval($every_media_fad_illegal_times),
                            strval($every_media_fad_illegal_count),
                            round((($every_media_fad_illegal_times)/($every_media_fad_times))*100,2).'%',
                            round((($every_media_fad_illegal_count)/($every_media_fad_count))*100,2).'%'
                        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）*/
            $every_media_all_data[] = $every_media_data;
            $every_media_illegal_times_all_data[] = $every_media_illegal_times_data;
        }
        $every_media_tv_data_xj = $every_media_all_data[0];//电视各媒体
        $every_media_tb_data_xj = $every_media_all_data[1];//广播各媒体
        $every_media_tp_data_xj = $every_media_all_data[2];//报纸各媒体
        $every_media_illegal_times_tv_data_xj = $every_media_illegal_times_all_data[0];//电视各媒体
        $every_media_illegal_times_tb_data_xj = $every_media_illegal_times_all_data[1];//广播各媒体
        $every_media_illegal_times_tp_data_xj =$every_media_illegal_times_all_data[2];//报纸各媒体

        //本级电视
        $hz_tv_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',0);
        $hz_tv_illegal_ad_data = $this->get_table_data($hz_tv_illegal_ad_data);
        //本级广播
        $hz_tb_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',0);
        $hz_tb_illegal_ad_data = $this->get_table_data($hz_tb_illegal_ad_data);

        //本级报纸
        $hz_tp_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',0);
        $hz_tp_illegal_ad_data = $this->get_table_data($hz_tp_illegal_ad_data);

        //下级电视
        $xj_tv_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',2,$check_datas);
        $xj_tv_illegal_ad_data = $this->get_table_data($xj_tv_illegal_ad_data);

        //下级广播
        $xj_tb_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',2,$check_datas);
        $xj_tb_illegal_ad_data = $this->get_table_data($xj_tb_illegal_ad_data);

        //下级报纸
        $xj_tp_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',2,$check_datas);
        $xj_tp_illegal_ad_data = $this->get_table_data($xj_tp_illegal_ad_data);



        /*
* @各广告类型监测总情况下级
*
**/
        $every_ad_class_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times_rate','fcode',false,3,$check_datas);//各地域监测总情况
        $every_ad_class_data_xj = [];//定义各地区发布监测情况数组
//循环赋值
        $every_ad_class_illegal_times_data_xj = [
            [''],
            ['']
        ];
        foreach ($every_ad_class_jczqk as $every_ad_class_jczqk_key => $every_ad_class_jczqk_val){
            $every_ad_class_fad_times += $every_ad_class_jczqk_val['fad_times'];
            $every_ad_class_fad_illegal_times += $every_ad_class_jczqk_val['fad_illegal_times'];
            $every_ad_class_fad_count += $every_ad_class_jczqk_val['fad_count'];
            $every_ad_class_fad_illegal_count += $every_ad_class_jczqk_val['fad_illegal_count'];
            if($every_ad_class_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_ad_class_illegal_times_data_xj[0],$every_ad_class_jczqk_val['tadclass_name']);
                array_push($every_ad_class_illegal_times_data_xj[1],strval($every_ad_class_jczqk_val['fad_illegal_times']));
            }

            $every_ad_class_data_xj[] = [
                strval($every_ad_class_jczqk_key+1),
                $every_ad_class_jczqk_val['tadclass_name'],
                $every_ad_class_jczqk_val['fad_times'],
                $every_ad_class_jczqk_val['fad_count'],
                $every_ad_class_jczqk_val['fad_illegal_times'],
                $every_ad_class_jczqk_val['fad_illegal_count'],
                $every_ad_class_jczqk_val['fad_illegal_times_rate'].'%',
                $every_ad_class_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_ad_class_data_xj[] = [
            '',
            '合计',
            strval($every_ad_class_fad_times),
            strval($every_ad_class_fad_count),
            strval($every_ad_class_fad_illegal_times),
            strval($every_ad_class_fad_illegal_count),
            round((($every_ad_class_fad_illegal_times)/($every_ad_class_fad_times))*100,2).'%',
            round((($every_ad_class_fad_illegal_count)/($every_ad_class_fad_count))*100,2).'%'
        ];

        //四大类 药品、医疗服务、医疗器械、保健食品
        $hz_zdclass_tv_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,['01','02','08','13','05','06'],1);
        $hz_zdclass_tb_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,['01','02','08','13','05','06'],1);
        $hz_zdclass_tp_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,['01','02','08','13','05','06'],1);
        $typical_illegal_ad_list_data = [];
        if(!empty($hz_zdclass_tv_illegal_ad_data) || !empty($hz_zdclass_tb_illegal_ad_data) || !empty($hz_zdclass_tp_illegal_ad_data)){
            $typical_illegal_ad_list = array_merge($hz_zdclass_tv_illegal_ad_data,$hz_zdclass_tb_illegal_ad_data,$hz_zdclass_tp_illegal_ad_data);
            $illegal_ad_list_data = [];
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_val){
                $md5 = MD5($typical_illegal_ad_list_val['fad_name'].$typical_illegal_ad_list_val['fmedianame']);
                if(isset($illegal_ad_list_data[$md5])){
                    $illegal_ad_list_data[$md5]['illegal_times'] += $typical_illegal_ad_list_val['illegal_times'];
                    $illegal_ad_list_data[$md5]['illegal_times'] = strval($illegal_ad_list_data[$md5]['illegal_times']);
                }else{

                    $illegal_ad_list_data[$md5] = $typical_illegal_ad_list_val;
                }
            }

            $typical_illegal_ad_list = array_values($illegal_ad_list_data);
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_key => $typical_illegal_ad_list_val){
                $typical_illegal_ad_list_data[] = [
                    strval($typical_illegal_ad_list_key+1),
                    $typical_illegal_ad_list_val['fmedianame'],
                    $typical_illegal_ad_list_val['fad_name'],
                    $typical_illegal_ad_list_val['fadclass'],
                    $typical_illegal_ad_list_val['illegal_times']
                ];
            }
        }



//定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号加粗居中",
            "text" => $user['regionname1']."工商行政管理局"
        ];



        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号加粗居中",
            "text" => $date_ym."份广告监测通报"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号加粗居中",
            "text" => "    "
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $user['regulatorname']."广告监测中心对".$date_ym."份菏泽市内各区县级部分电视、广播、报刊等".($ct_media_tv_count_xj+$ct_media_bc_count_xj+$ct_media_paper_count_xj)."家媒体发布广告的情况进行了抽查监测，现将监测情况通报如下："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "一、监测范围及对象"
        ];
/*        if($ct_media_tv_count_hz != 0){
            $hz_media_count_str .= $ct_media_tv_count_hz."个电视监测".$days."天;";
        }
        if($ct_media_bc_count_hz != 0){
            $hz_media_count_str .= $ct_media_bc_count_hz."个广播监测".$days."天;";
        }
        if($ct_media_paper_count_hz != 0){
            $hz_media_count_str .= $ct_media_paper_count_hz."个报纸监测".$days."天;";
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）".$dq_name."媒体：".$hz_media_count_str
        ];*/

        if($ct_media_tv_count_xj != 0){
            $xj_media_count_str .= $ct_media_tv_count_xj."个电视监测".$xj_dates."天;";
        }
        if($ct_media_bc_count_xj != 0){
            $xj_media_count_str .= $ct_media_bc_count_xj."个广播监测".$xj_dates."天;";
        }
        if($ct_media_paper_count_xj != 0){
            $xj_media_count_str .= $ct_media_paper_count_xj."个报纸监测".$xj_dates."天;";
        }
        $tregion_count = M('tregion')->where(['fpid'=>$user['regionid']])->count();

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "菏泽市 ".$tregion_count."个区、县：".$xj_media_count_str
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "二、区县级各类媒体广告发布情况"
        ];

        /*$data['content'][] = [
             "type" => "text",
             "bookmark" => "仿宋三号段落",
             "text" => "（一）市级媒体广告发布情况"
         ];


         $data['content'][] = [
             "type" => "text",
             "bookmark" => "仿宋三号段落",
             "text" => "本月共监测广告".$every_media_class_bj_data[count($every_media_class_bj_data)-1][1]."条次，".$every_media_class_bj_data[count($every_media_class_bj_data)-1][2]."条数，其中监测发现各类涉嫌违法广告".$every_media_class_bj_data[count($every_media_class_bj_data)-1][3]."条次，".$every_media_class_bj_data[count($every_media_class_bj_data)-1][4]."条数。具体情况详见下表："
         ];

         $data['content'][] = [
             "type" => "table",
             "bookmark" => "媒介类型汇总表格hz",
             "data" => $every_media_class_bj_data
         ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "各区县媒体广告发布情况"
        ];*/


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "本月共监测广告".$every_media_class_xj_data[count($every_media_class_xj_data)-1][1]."条次，".$every_media_class_xj_data[count($every_media_class_xj_data)-1][2]."条数，其中监测发现各类涉嫌违法广告".$every_media_class_xj_data[count($every_media_class_xj_data)-1][3]."条次，".$every_media_class_xj_data[count($every_media_class_xj_data)-1][4]."条数。具体情况详见下表："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒介类型汇总表格hz",
            "data" => $every_media_class_xj_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "三、各区县媒体广告发布总体情况"
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "地域排名汇总表格hz",
            "data" => $every_region_data_xj
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "条次违法率chart",
            "data" => $every_region_illegal_times_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "四、各区县电视、广播、报纸广告发布情况（排名按广告条次违法率由高到底排列）"
        ];

        if(!empty($every_media_tv_data_xj)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "1、电视"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tv_data_xj
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "条次违法率chart",
                "data" => $every_media_illegal_times_tv_data_xj
            ];
        }
        if(!empty($every_media_tb_data_xj)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "2、广播"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tb_data_xj
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "条次违法率chart",
                "data" => $every_media_illegal_times_tb_data_xj
            ];
        }
        if(!empty($every_media_tp_data_xj)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "3、报纸"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tp_data_xj
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "条次违法率chart",
                "data" => $every_media_illegal_times_tp_data_xj
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、区县各类违法广告比重结构图"
        ];
        if(!empty($every_ad_class_data_xj)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表hz",
                "data" => $every_ad_class_data_xj
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "各大类别广告违法占比表hz",
                "data" => $every_ad_class_illegal_times_data_xj
            ];

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "六、各类媒体发布的违法广告（全部类别）通报"
        ];

/*        $illegal_ad_list_tv_bj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',0);
        $illegal_ad_list_tb_bj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',0);
        $illegal_ad_list_tp_bj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',0);
        $illegal_ad_list_bj = [];
        if(!empty($illegal_ad_list_tv_bj) || !empty($illegal_ad_list_tb_bj) || !empty($illegal_ad_list_tp_bj)) {
            $typical_illegal_ad_list = array_merge($illegal_ad_list_tv_bj, $illegal_ad_list_tb_bj, $illegal_ad_list_tp_bj);
            $illegal_ad_list_bj = pxsf($typical_illegal_ad_list, 'illegal_times');//排序
        }
        if(!empty($illegal_ad_list_bj)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "（一）菏泽市违法广告"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告详情列表hz",
                "data" => $this->pmaddkey($this->get_table_data($illegal_ad_list_bj),6)
            ];
        }*/

        $illegal_ad_list_tv_xj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',2,$check_datas);
        $illegal_ad_list_tb_xj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',2,$check_datas);
        $illegal_ad_list_tp_xj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',2,$check_datas);

        $illegal_ad_list_xj = [];
        if(!empty($illegal_ad_list_tv_xj) || !empty($illegal_ad_list_tb_xj) || !empty($illegal_ad_list_tp_xj)) {
            $typical_illegal_ad_list = array_merge($illegal_ad_list_tv_xj, $illegal_ad_list_tb_xj, $illegal_ad_list_tp_xj);
            $illegal_ad_list_xj = pxsf($typical_illegal_ad_list, 'illegal_times');//排序

        }
        if(!empty($illegal_ad_list_xj)){
/*            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "（二）各区县违法广告"
            ];*/
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告详情列表hz",
                "data" => $this->pmaddkey($this->get_table_data($illegal_ad_list_xj),6)
            ];
        }
        //1104
        /*$data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、区县各类违法广告比重结构图"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）市级媒体"
        ];


        if(!empty($hz_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tv_illegal_ad_data)
            ];
        }
        if(!empty($hz_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tb_illegal_ad_data)
            ];
        }
        if(!empty($hz_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tp_illegal_ad_data)
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）区（县）级媒体"
        ];

        if(!empty($xj_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tv_illegal_ad_data)
            ];
        }

        if(!empty($xj_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tb_illegal_ad_data)
            ];
        }

        if(!empty($xj_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tp_illegal_ad_data)
            ];
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "六、全市各类违法广告比重结构图"
        ];
        if(!empty($every_ad_class_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表hz",
                "data" => $every_ad_class_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "各大类别广告违法占比表hz",
                "data" => $every_ad_class_illegal_times_data
            ];

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "七、药品、医疗服务、医疗器械、保健食品违法广告发布情况"
        ];

        if(!empty($typical_illegal_ad_list_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $typical_illegal_ad_list_data
            ];
        }*/
        $report_data = json_encode($data);
        //echo $report_data;exit;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $system_num = getconfig('system_num');

//将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].date('Y年m月',strtotime($month)).'县级月报';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 30;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }

    //菏泽月报本级
    public function create_month_bj(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型
        $month = I('daytime',date('Y-m',time()));//接收时间  TODO::时间改
        //$month = I('daytime','2018-10');//接收时间  TODO::时间改
        $month_num = strval(substr($month,5,2));//月份
        $year_num = strval(substr($month,0,4));//年份
        if($month_num < 10 && $year_num <= 2018){
            $this->ajaxReturn([
                "code" => 1,
                "msg" => '抱歉，只能导出2018年10月以及十月之后的报告！',
            ]);
        }
        if($month == '2018-10'){

            $report_start_date = strtotime('2018-10-17');
        }else{
            $report_start_date = strtotime($month);
        }
        //$report_end_date = mktime(23, 59, 59, date('m', strtotime($month))+1, 00);//指定月份月末时间戳
        $report_end_date = strtotime($month.'-'.date('t', strtotime($month)).' 23:59:59');

        if($report_end_date > time()){
            $report_end_date = time();

        }


        $date_ym = date('Y年m月',$report_start_date);//年月字符

        $user = session('regulatorpersonInfo');//获取用户信息
        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级
        if($re_level == 2 || $re_level == 3 || $re_level == 4){
            $dq_name = '市级';
            $r_name = '全市';
            $xj_name = '区县';
        }elseif($re_level == 1){
            $dq_name = '全省';
            $xj_name = '市级';
        }else{
            $dq_name = '全'.$user['regionname1'];
            $xj_name = '地方';
        }
        $date_table = date('Ym',strtotime($month)).'_'.substr($user['regionid'],0,2);//组合表

        if(!table_exist("ttvissue_".$date_table) || !table_exist("tbcissue_".$date_table)){
            $this->ajaxReturn([
                'code'=>1,
                'msg'=>'生成失败，时间选择有误！'
            ]);
        }
        $check_datas = get_check_dates();
        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数
        $owner_media_ids = get_owner_media_ids();

        /* 各大媒体类型数量 */
        $ct_media_tv_count_hz = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//菏泽市局电视媒体数量

        $ct_media_bc_count_hz = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//菏泽市局广播媒体数量

        $ct_media_paper_count_hz = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//菏泽市局报纸媒体数量

        $ct_media_tv_count_xj = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//菏泽各区县电视媒体数量

        $ct_media_bc_count_xj = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//菏泽各区县广播媒体数量

        $ct_media_paper_count_xj = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//菏泽各区县报纸媒体数量

        $ct_media_count = $ct_media_tv_count_hz + $ct_media_bc_count_hz + $ct_media_paper_count_hz + $ct_media_tv_count_xj + $ct_media_bc_count_xj + $ct_media_paper_count_xj;//全部有数据的媒体数量


        /*
        * @市级各媒体监测总情况
        *
        **/

        $ct_jczqk_bj = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_count_rate','fmediaclass',false,2);//本市传统媒体监测总情况

        $ct_fad_times_bj = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times_bj = 0;//定义传统媒体发布违法广告条次
        $ct_fad_count_bj = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_count_bj = 0;//定义传统媒体发布违法广告条次
        $every_media_class_bj_data = [];//定义媒体发布监测情况数组
//循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk_bj as $ct_jczqk_bj_val){
            $ct_fad_times_bj += $ct_jczqk_bj_val['fad_times'];
            $ct_fad_illegal_times_bj += $ct_jczqk_bj_val['fad_illegal_times'];
            $ct_fad_count_bj += $ct_jczqk_bj_val['fad_count'];
            $ct_fad_illegal_count_bj += $ct_jczqk_bj_val['fad_illegal_count'];
            $ct_data_val = [
                $ct_jczqk_bj_val['tmediaclass_name'],
                $ct_jczqk_bj_val['fad_times'],
                $ct_jczqk_bj_val['fad_count'],
                $ct_jczqk_bj_val['fad_illegal_times'],
                $ct_jczqk_bj_val['fad_illegal_count'],
                $ct_jczqk_bj_val['fad_illegal_times_rate'].'%',
                $ct_jczqk_bj_val['fad_illegal_count_rate'].'%'
            ];
            if($ct_jczqk_bj_val['fmedia_class_code'] == '01'){
                $every_media_class_bj_data[0] = $ct_data_val;
            }elseif($ct_jczqk_bj_val['fmedia_class_code'] == '02'){
                $every_media_class_bj_data[1] = $ct_data_val;
            }else{
                $every_media_class_bj_data[2] = $ct_data_val;
            }
        }

        $every_media_class_bj_data[3] = [
            '合计',
            strval($ct_fad_times_bj),
            strval($ct_fad_count_bj),
            strval($ct_fad_illegal_times_bj),
            strval($ct_fad_illegal_count_bj),
            round((($ct_fad_illegal_times_bj)/($ct_fad_times_bj))*100,2).'%',
            round((($ct_fad_illegal_count_bj)/($ct_fad_count_bj))*100,2).'%'
        ];//二、各类媒体广告发布情况
        $every_media_class_bj_data = array_values($every_media_class_bj_data);


        /*
        * @各县各媒体监测总情况
        *
        **/
        $ct_fad_times_xj = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times_xj = 0;//定义传统媒体发布违法广告条次
        $ct_fad_count_xj = 0;
        $ct_fad_illegal_count_xj = 0;
        $every_media_class_xj_data = [];//定义媒体发布监测情况数组

        $ct_jczqk_xj = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_count_rate','fmediaclass',false,3,$check_datas);//传统媒体监测总情况
        //循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk_xj as $ct_jczqk_xj_val){
            $ct_fad_times_xj += $ct_jczqk_xj_val['fad_times'];
            $ct_fad_illegal_times_xj += $ct_jczqk_xj_val['fad_illegal_times'];
            $ct_fad_count_xj += $ct_jczqk_xj_val['fad_count'];
            $ct_fad_illegal_count_xj += $ct_jczqk_xj_val['fad_illegal_count'];
            $ct_data_val = [
                $ct_jczqk_xj_val['tmediaclass_name'],
                $ct_jczqk_xj_val['fad_times'],
                $ct_jczqk_xj_val['fad_count'],
                $ct_jczqk_xj_val['fad_illegal_times'],
                $ct_jczqk_xj_val['fad_illegal_count'],
                $ct_jczqk_xj_val['fad_illegal_times_rate'].'%',
                $ct_jczqk_xj_val['fad_illegal_count_rate'].'%'
            ];
            if($ct_jczqk_xj_val['fmedia_class_code'] == '01'){
                $every_media_class_xj_data[0] = $ct_data_val;
            }elseif($ct_jczqk_xj_val['fmedia_class_code'] == '02'){
                $every_media_class_xj_data[1] = $ct_data_val;
            }else{
                $every_media_class_xj_data[2] = $ct_data_val;
            }
        }

        $every_media_class_xj_data[3] = [
            '合计',
            strval($ct_fad_times_xj),
            strval($ct_fad_count_xj),
            strval($ct_fad_illegal_times_xj),
            strval($ct_fad_illegal_count_xj),
            round((($ct_fad_illegal_times_xj)/($ct_fad_times_xj))*100,2).'%',
            round((($ct_fad_illegal_count_xj)/($ct_fad_count_xj))*100,2).'%'
        ];//二、各类媒体广告发布情况
        $every_media_class_xj_data = array_values($every_media_class_xj_data);


        /*
        * @各地域监测总情况
        *
        **/
        $every_region_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times_rate','fregionid',false,3,$check_datas);//各地域监测总情况
        $every_region_data_xj = [];//定义各地区发布监测情况数组
//循环赋值
        $every_region_illegal_times_data = [
            [''],
            ['']
        ];
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){
            $every_region_fad_times += $every_region_jczqk_val['fad_times'];
            $every_region_fad_illegal_times += $every_region_jczqk_val['fad_illegal_times'];
            $every_region_fad_count += $every_region_jczqk_val['fad_count'];
            $every_region_fad_illegal_count += $every_region_jczqk_val['fad_illegal_count'];
            //if($every_region_jczqk_val['fad_illegal_times'] > 0){
            array_push($every_region_illegal_times_data[0],$every_region_jczqk_val['tregion_name']);
            array_push($every_region_illegal_times_data[1],$every_region_jczqk_val['fad_illegal_times_rate'].'%');
            //}

            $every_region_data_xj[] = [
                strval($every_region_jczqk_key+1),
                $every_region_jczqk_val['tregion_name'],
                $every_region_jczqk_val['fad_times'],
                $every_region_jczqk_val['fad_count'],
                $every_region_jczqk_val['fad_illegal_times'],
                $every_region_jczqk_val['fad_illegal_count'],
                $every_region_jczqk_val['fad_illegal_times_rate'].'%',
                $every_region_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_region_data_xj[] = [
            '',
            '合计',
            strval($every_region_fad_times),
            strval($every_region_fad_count),
            strval($every_region_fad_illegal_times),
            strval($every_region_fad_illegal_count),
            round((($every_region_fad_illegal_times)/($every_region_fad_times))*100,2).'%',
            round((($every_region_fad_illegal_count)/($every_region_fad_count))*100,2).'%'
        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）



        /*
* @各地域电视广播报纸监测总情况
*
**/
        $every_media_all_data = [];
        $every_media_illegal_times_all_data = [];
        foreach (['01','02','03'] as $every_media_val){
            $every_media_data = [];
            $every_media_illegal_times_data = [];
            $every_media_jczqk = [];
            //循环赋值
            $every_media_fad_times = 0;
            $every_media_fad_illegal_times = 0;
            $every_media_fad_count = 0;
            $every_media_fad_illegal_count = 0;
            $every_media_illegal_times_data = [
                [''],
                ['']
            ];
            $every_media_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,$every_media_val,'','fad_illegal_times_rate','fmediaid',false,2);//市级各地域监测总情况
            foreach ($every_media_jczqk as $every_media_jczqk_key => $every_media_jczqk_val){
                $every_media_fad_times += $every_media_jczqk_val['fad_times'];
                $every_media_fad_illegal_times += $every_media_jczqk_val['fad_illegal_times'];
                $every_media_fad_count += $every_media_jczqk_val['fad_count'];
                $every_media_fad_illegal_count += $every_media_jczqk_val['fad_illegal_count'];
                if($every_media_jczqk_val['fad_illegal_times'] > 0){
                    array_push($every_media_illegal_times_data[0],$every_media_jczqk_val['tmedia_name']);
                    array_push($every_media_illegal_times_data[1],$every_media_jczqk_val['fad_illegal_times_rate'].'%');
                }

                $every_media_data[] = [
                    strval($every_media_jczqk_key+1),
                    $every_media_jczqk_val['tregion_name'],
                    $every_media_jczqk_val['tmedia_name'],
                    $every_media_jczqk_val['fad_times'],
                    $every_media_jczqk_val['fad_count'],
                    $every_media_jczqk_val['fad_illegal_times'],
                    $every_media_jczqk_val['fad_illegal_count'],
                    $every_media_jczqk_val['fad_illegal_times_rate'].'%',
                    $every_media_jczqk_val['fad_illegal_count_rate'].'%'
                ];
            }
            /*            $every_media_data[] = [
                            '',
                            '合计',
                            strval($every_media_fad_times),
                            strval($every_media_fad_count),
                            strval($every_media_fad_illegal_times),
                            strval($every_media_fad_illegal_count),
                            round((($every_media_fad_illegal_times)/($every_media_fad_times))*100,2).'%',
                            round((($every_media_fad_illegal_count)/($every_media_fad_count))*100,2).'%'
                        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）*/
            $every_media_all_data[] = $every_media_data;
            $every_media_illegal_times_all_data[] = $every_media_illegal_times_data;
        }
        $every_media_tv_data_xj = $every_media_all_data[0];//电视各媒体
        $every_media_tb_data_xj = $every_media_all_data[1];//广播各媒体
        $every_media_tp_data_xj = $every_media_all_data[2];//报纸各媒体
        $every_media_illegal_times_tv_data_xj = $every_media_illegal_times_all_data[0];//电视各媒体
        $every_media_illegal_times_tb_data_xj = $every_media_illegal_times_all_data[1];//广播各媒体
        $every_media_illegal_times_tp_data_xj =$every_media_illegal_times_all_data[2];//报纸各媒体

        //本级电视
        $hz_tv_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',0);
        $hz_tv_illegal_ad_data = $this->get_table_data($hz_tv_illegal_ad_data);
        //本级广播
        $hz_tb_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',0);
        $hz_tb_illegal_ad_data = $this->get_table_data($hz_tb_illegal_ad_data);

        //本级报纸
        $hz_tp_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',0);
        $hz_tp_illegal_ad_data = $this->get_table_data($hz_tp_illegal_ad_data);

        //下级电视
        $xj_tv_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',2,$check_datas);
        $xj_tv_illegal_ad_data = $this->get_table_data($xj_tv_illegal_ad_data);

        //下级广播
        $xj_tb_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',2,$check_datas);
        $xj_tb_illegal_ad_data = $this->get_table_data($xj_tb_illegal_ad_data);

        //下级报纸
        $xj_tp_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',2,$check_datas);
        $xj_tp_illegal_ad_data = $this->get_table_data($xj_tp_illegal_ad_data);



        /*
* @各广告类型监测总情况下级
*
**/
        $every_ad_class_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times_rate','fcode',false,2);//市属各地域监测总情况
        $every_ad_class_data_xj = [];//定义各地区发布监测情况数组
//循环赋值
        $every_ad_class_illegal_times_data_xj = [
            [''],
            ['']
        ];
        foreach ($every_ad_class_jczqk as $every_ad_class_jczqk_key => $every_ad_class_jczqk_val){
            $every_ad_class_fad_times += $every_ad_class_jczqk_val['fad_times'];
            $every_ad_class_fad_illegal_times += $every_ad_class_jczqk_val['fad_illegal_times'];
            $every_ad_class_fad_count += $every_ad_class_jczqk_val['fad_count'];
            $every_ad_class_fad_illegal_count += $every_ad_class_jczqk_val['fad_illegal_count'];
            if($every_ad_class_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_ad_class_illegal_times_data_xj[0],$every_ad_class_jczqk_val['tadclass_name']);
                array_push($every_ad_class_illegal_times_data_xj[1],strval($every_ad_class_jczqk_val['fad_illegal_times']));
            }

            $every_ad_class_data_xj[] = [
                strval($every_ad_class_jczqk_key+1),
                $every_ad_class_jczqk_val['tadclass_name'],
                $every_ad_class_jczqk_val['fad_times'],
                $every_ad_class_jczqk_val['fad_count'],
                $every_ad_class_jczqk_val['fad_illegal_times'],
                $every_ad_class_jczqk_val['fad_illegal_count'],
                $every_ad_class_jczqk_val['fad_illegal_times_rate'].'%',
                $every_ad_class_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_ad_class_data_xj[] = [
            '',
            '合计',
            strval($every_ad_class_fad_times),
            strval($every_ad_class_fad_count),
            strval($every_ad_class_fad_illegal_times),
            strval($every_ad_class_fad_illegal_count),
            round((($every_ad_class_fad_illegal_times)/($every_ad_class_fad_times))*100,2).'%',
            round((($every_ad_class_fad_illegal_count)/($every_ad_class_fad_count))*100,2).'%'
        ];

        //四大类 药品、医疗服务、医疗器械、保健食品
        $hz_zdclass_tv_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,['01','02','08','13','05','06'],1);
        $hz_zdclass_tb_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,['01','02','08','13','05','06'],1);
        $hz_zdclass_tp_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,['01','02','08','13','05','06'],1);
        $typical_illegal_ad_list_data = [];
        if(!empty($hz_zdclass_tv_illegal_ad_data) || !empty($hz_zdclass_tb_illegal_ad_data) || !empty($hz_zdclass_tp_illegal_ad_data)){
            $typical_illegal_ad_list = array_merge($hz_zdclass_tv_illegal_ad_data,$hz_zdclass_tb_illegal_ad_data,$hz_zdclass_tp_illegal_ad_data);
            $illegal_ad_list_data = [];
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_val){
                $md5 = MD5($typical_illegal_ad_list_val['fad_name'].$typical_illegal_ad_list_val['fmedianame']);
                if(isset($illegal_ad_list_data[$md5])){
                    $illegal_ad_list_data[$md5]['illegal_times'] += $typical_illegal_ad_list_val['illegal_times'];
                    $illegal_ad_list_data[$md5]['illegal_times'] = strval($illegal_ad_list_data[$md5]['illegal_times']);
                }else{

                    $illegal_ad_list_data[$md5] = $typical_illegal_ad_list_val;
                }
            }

            $typical_illegal_ad_list = array_values($illegal_ad_list_data);
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_key => $typical_illegal_ad_list_val){
                $typical_illegal_ad_list_data[] = [
                    strval($typical_illegal_ad_list_key+1),
                    $typical_illegal_ad_list_val['fmedianame'],
                    $typical_illegal_ad_list_val['fad_name'],
                    $typical_illegal_ad_list_val['fadclass'],
                    $typical_illegal_ad_list_val['illegal_times']
                ];
            }
        }



//定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号加粗居中",
            "text" => $user['regionname1']."工商行政管理局"
        ];


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号加粗居中",
            "text" => $date_ym."份广告监测通报"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号加粗居中",
            "text" => "    "
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $user['regulatorname']."广告监测中心对".$date_ym."份".$dq_name."部分电视、广播、报刊等".($ct_media_tv_count_hz+$ct_media_bc_count_hz+$ct_media_paper_count_hz)."家媒体发布广告的情况进行了抽查监测，现将监测情况通报如下："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "一、监测范围及对象"
        ];
        if($ct_media_tv_count_hz != 0){
            $hz_media_count_str .= $ct_media_tv_count_hz."个电视监测".$days."天;";
        }
        if($ct_media_bc_count_hz != 0){
            $hz_media_count_str .= $ct_media_bc_count_hz."个广播监测".$days."天;";
        }
        if($ct_media_paper_count_hz != 0){
            $hz_media_count_str .= $ct_media_paper_count_hz."个报纸监测".$days."天;";
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $dq_name."媒体：".$hz_media_count_str
        ];
/*        $xj_dates = count($check_datas);
        if($ct_media_tv_count_xj != 0){
            $xj_media_count_str .= $ct_media_tv_count_xj."个电视监测".$xj_dates."天;";
        }
        if($ct_media_bc_count_xj != 0){
            $xj_media_count_str .= $ct_media_bc_count_xj."个广播监测".$xj_dates."天;";
        }
        if($ct_media_paper_count_xj != 0){
            $xj_media_count_str .= $ct_media_paper_count_xj."个报纸监测".$xj_dates."天;";
        }
        $tregion_count = M('tregion')->where(['fpid'=>$user['regionid']])->count();

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）其他".$tregion_count."个区、县：".$xj_media_count_str
        ];*/

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "二、市属各类媒体广告发布情况"
        ];

/*        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）市级媒体广告发布情况"
        ];*/


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "本月共监测广告".$every_media_class_bj_data[count($every_media_class_bj_data)-1][1]."条次，".$every_media_class_bj_data[count($every_media_class_bj_data)-1][2]."条数，其中监测发现各类涉嫌违法广告".$every_media_class_bj_data[count($every_media_class_bj_data)-1][3]."条次，".$every_media_class_bj_data[count($every_media_class_bj_data)-1][4]."条数。具体情况详见下表："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒介类型汇总表格hz",
            "data" => $every_media_class_bj_data
        ];

/*        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）各区县媒体广告发布情况"
        ];


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "本月共监测广告".$every_media_class_xj_data[count($every_media_class_xj_data)-1][1]."条次，".$every_media_class_xj_data[count($every_media_class_xj_data)-1][2]."条数，其中监测发现各类涉嫌违法广告".$every_media_class_xj_data[count($every_media_class_xj_data)-1][3]."条次，".$every_media_class_xj_data[count($every_media_class_xj_data)-1][4]."条数。具体情况详见下表："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒介类型汇总表格hz",
            "data" => $every_media_class_xj_data
        ];*/

       /* $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "三、各区县媒体广告发布总体情况"
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "地域排名汇总表格hz",
            "data" => $every_region_data_xj
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "条次违法率chart",
            "data" => $every_region_illegal_times_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "四、各区县电视、广播、报纸广告发布情况（排名按广告条次违法率由高到底排列）"
        ];

        if(!empty($every_media_tv_data_xj)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "1、电视"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tv_data_xj
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "条次违法率chart",
                "data" => $every_media_illegal_times_tv_data_xj
            ];
        }
        if(!empty($every_media_tb_data_xj)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "2、广播"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tb_data_xj
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "条次违法率chart",
                "data" => $every_media_illegal_times_tb_data_xj
            ];
        }
        if(!empty($every_media_tp_data_xj)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "3、报纸"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tp_data_xj
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "条次违法率chart",
                "data" => $every_media_illegal_times_tp_data_xj
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、区县各类违法广告比重结构图"
        ];
        if(!empty($every_ad_class_data_xj)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表hz",
                "data" => $every_ad_class_data_xj
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "各大类别广告违法占比表hz",
                "data" => $every_ad_class_illegal_times_data_xj
            ];

        }*/
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "三、市属 电视、广播、报纸广告发布情况（排名按广告条次违法率由高到底排列）"
        ];

        if(!empty($every_media_tv_data_xj)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "1、电视"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tv_data_xj
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "条次违法率chart",
                "data" => $every_media_illegal_times_tv_data_xj
            ];
        }
        if(!empty($every_media_tb_data_xj)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "2、广播"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tb_data_xj
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "条次违法率chart",
                "data" => $every_media_illegal_times_tb_data_xj
            ];
        }
        if(!empty($every_media_tp_data_xj)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "3、报纸"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tp_data_xj
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "条次违法率chart",
                "data" => $every_media_illegal_times_tp_data_xj
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "四、市属媒体各类违法广告比重结构图"
        ];
        if(!empty($every_ad_class_data_xj)) {
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表hz",
                "data" => $every_ad_class_data_xj
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "各大类别广告违法占比表hz",
                "data" => $every_ad_class_illegal_times_data_xj
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、市属各类媒体发布的违法广告（全部类别）通报"
        ];

        $illegal_ad_list_tv_bj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',0);
        $illegal_ad_list_tb_bj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',0);
        $illegal_ad_list_tp_bj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',0);
        $illegal_ad_list_bj = [];
        if(!empty($illegal_ad_list_tv_bj) || !empty($illegal_ad_list_tb_bj) || !empty($illegal_ad_list_tp_bj)) {
            $typical_illegal_ad_list = array_merge($illegal_ad_list_tv_bj, $illegal_ad_list_tb_bj, $illegal_ad_list_tp_bj);
            $illegal_ad_list_bj = pxsf($typical_illegal_ad_list, 'illegal_times');//排序
        }
        if(!empty($illegal_ad_list_bj)){
/*            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "（一）菏泽市违法广告"
            ];*/
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告详情列表hz",
                "data" => $this->pmaddkey($this->get_table_data($illegal_ad_list_bj),6)
            ];
        }

/*        $illegal_ad_list_tv_xj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',2,$check_datas1);
        $illegal_ad_list_tb_xj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',2,$check_datas1);
        $illegal_ad_list_tp_xj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',2,$check_datas1);

        $illegal_ad_list_xj = [];
        if(!empty($illegal_ad_list_tv_xj) || !empty($illegal_ad_list_tb_xj) || !empty($illegal_ad_list_tp_xj)) {
            $typical_illegal_ad_list = array_merge($illegal_ad_list_tv_xj, $illegal_ad_list_tb_xj, $illegal_ad_list_tp_xj);
            $illegal_ad_list_xj = pxsf($typical_illegal_ad_list, 'illegal_times');//排序

        }
        if(!empty($illegal_ad_list_xj)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "（二）各区县违法广告"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告详情列表hz",
                "data" => $this->pmaddkey($this->get_table_data($illegal_ad_list_xj),6)
            ];
        }*/
        //1104
        /*$data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、区县各类违法广告比重结构图"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）市级媒体"
        ];


        if(!empty($hz_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tv_illegal_ad_data)
            ];
        }
        if(!empty($hz_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tb_illegal_ad_data)
            ];
        }
        if(!empty($hz_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tp_illegal_ad_data)
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）区（县）级媒体"
        ];

        if(!empty($xj_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tv_illegal_ad_data)
            ];
        }

        if(!empty($xj_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tb_illegal_ad_data)
            ];
        }

        if(!empty($xj_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tp_illegal_ad_data)
            ];
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "六、全市各类违法广告比重结构图"
        ];
        if(!empty($every_ad_class_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表hz",
                "data" => $every_ad_class_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "各大类别广告违法占比表hz",
                "data" => $every_ad_class_illegal_times_data
            ];

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "七、药品、医疗服务、医疗器械、保健食品违法广告发布情况"
        ];

        if(!empty($typical_illegal_ad_list_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $typical_illegal_ad_list_data
            ];
        }*/
        $report_data = json_encode($data);
        //echo $report_data;exit;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $system_num = getconfig('system_num');
            
//将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].date('Y年m月',strtotime($month)).'市级月报';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 30;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }

    //菏泽月报下级
    public function create_month(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型
        $month = I('daytime',date('Y-m',time()));//接收时间  TODO::时间改
        //$month = I('daytime','2018-10');//接收时间  TODO::时间改
        $month_num = strval(substr($month,5,2));//月份
        if($month_num < 10){
            $this->ajaxReturn([
                "code" => 1,
                "msg" => '抱歉，只能到处十月以及十月之后的报告！',
            ]);
        }
        if($month == '2018-10'){

            $report_start_date = strtotime('2018-10-17');
        }else{
            $report_start_date = strtotime($month);
        }
        $report_end_date = mktime(23, 59, 59, date('m', strtotime($month))+1, 00);//指定月份月末时间戳

        if($report_end_date > time()){
            $report_end_date = time();

        }


        $date_ym = date('Y年m月',$report_start_date);//年月字符

        $user = session('regulatorpersonInfo');//获取用户信息
        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级
        if($re_level == 2 || $re_level == 3 || $re_level == 4){
            $dq_name = '市级';
            $r_name = '全市';
            $xj_name = '区县';
        }elseif($re_level == 1){
            $dq_name = '全省';
            $xj_name = '市级';
        }else{
            $dq_name = '全'.$user['regionname1'];
            $xj_name = '地方';
        }
        $date_table = date('Ym',strtotime($month)).'_'.substr($user['regionid'],0,2);//组合表

        if(!table_exist("ttvissue_".$date_table) || !table_exist("tbcissue_".$date_table)){
            $this->ajaxReturn([
                'code'=>1,
                'msg'=>'生成失败，时间选择有误！'
            ]);
        }
        $check_datas1 = [
            '"2018-10-17"',
            '"2018-10-18"',
            '"2018-10-19"',
            '"2018-10-20"',
            '"2018-10-21"',
            '"2018-10-22"',
            '"2018-10-23"',
            '"2018-10-24"',
            '"2018-10-25"',
            '"2018-10-26"'
        ];
        $check_datas = [
            '2018-10-17',
            '2018-10-18',
            '2018-10-19',
            '2018-10-20',
            '2018-10-21',
            '2018-10-22',
            '2018-10-23',
            '2018-10-24',
            '2018-10-25',
            '2018-10-26'
        ];
        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数
        $owner_media_ids = get_owner_media_ids();

        /* 各大媒体类型数量 */
        $ct_media_tv_count_hz = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//菏泽市局电视媒体数量

        $ct_media_bc_count_hz = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//菏泽市局广播媒体数量

        $ct_media_paper_count_hz = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//菏泽市局报纸媒体数量

        $ct_media_tv_count_xj = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//菏泽各区县电视媒体数量

        $ct_media_bc_count_xj = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//菏泽各区县广播媒体数量

        $ct_media_paper_count_xj = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//菏泽各区县报纸媒体数量

        $ct_media_count = $ct_media_tv_count_hz + $ct_media_bc_count_hz + $ct_media_paper_count_hz + $ct_media_tv_count_xj + $ct_media_bc_count_xj + $ct_media_paper_count_xj;//全部有数据的媒体数量


        /*
        * @市级各媒体监测总情况
        *
        **/

        $ct_jczqk_bj = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_count_rate','fmediaclass',false,2);//本市传统媒体监测总情况

        $ct_fad_times_bj = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times_bj = 0;//定义传统媒体发布违法广告条次
        $ct_fad_count_bj = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_count_bj = 0;//定义传统媒体发布违法广告条次
        $every_media_class_bj_data = [];//定义媒体发布监测情况数组
//循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk_bj as $ct_jczqk_bj_val){
            $ct_fad_times_bj += $ct_jczqk_bj_val['fad_times'];
            $ct_fad_illegal_times_bj += $ct_jczqk_bj_val['fad_illegal_times'];
            $ct_fad_count_bj += $ct_jczqk_bj_val['fad_count'];
            $ct_fad_illegal_count_bj += $ct_jczqk_bj_val['fad_illegal_count'];
            $ct_data_val = [
                $ct_jczqk_bj_val['tmediaclass_name'],
                $ct_jczqk_bj_val['fad_times'],
                $ct_jczqk_bj_val['fad_count'],
                $ct_jczqk_bj_val['fad_illegal_times'],
                $ct_jczqk_bj_val['fad_illegal_count'],
                $ct_jczqk_bj_val['fad_illegal_times_rate'].'%',
                $ct_jczqk_bj_val['fad_illegal_count_rate'].'%'
            ];
            if($ct_jczqk_bj_val['fmedia_class_code'] == '01'){
                $every_media_class_bj_data[0] = $ct_data_val;
            }elseif($ct_jczqk_bj_val['fmedia_class_code'] == '02'){
                $every_media_class_bj_data[1] = $ct_data_val;
            }else{
                $every_media_class_bj_data[2] = $ct_data_val;
            }
        }

        $every_media_class_bj_data[3] = [
            '合计',
            strval($ct_fad_times_bj),
            strval($ct_fad_count_bj),
            strval($ct_fad_illegal_times_bj),
            strval($ct_fad_illegal_count_bj),
            round((($ct_fad_illegal_times_bj)/($ct_fad_times_bj))*100,2).'%',
            round((($ct_fad_illegal_count_bj)/($ct_fad_count_bj))*100,2).'%'
        ];//二、各类媒体广告发布情况
        $every_media_class_bj_data = array_values($every_media_class_bj_data);


        /*
* @各县各媒体监测总情况
*
**/
        $ct_fad_times_xj = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times_xj = 0;//定义传统媒体发布违法广告条次
        $ct_fad_count_xj = 0;
        $ct_fad_illegal_count_xj = 0;
        $every_media_class_xj_data = [];//定义媒体发布监测情况数组

        $ct_jczqk_xj = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_count_rate','fmediaclass',false,3,$check_datas);//传统媒体监测总情况
//循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk_xj as $ct_jczqk_xj_val){
            $ct_fad_times_xj += $ct_jczqk_xj_val['fad_times'];
            $ct_fad_illegal_times_xj += $ct_jczqk_xj_val['fad_illegal_times'];
            $ct_fad_count_xj += $ct_jczqk_xj_val['fad_count'];
            $ct_fad_illegal_count_xj += $ct_jczqk_xj_val['fad_illegal_count'];
            $ct_data_val = [
                $ct_jczqk_xj_val['tmediaclass_name'],
                $ct_jczqk_xj_val['fad_times'],
                $ct_jczqk_xj_val['fad_count'],
                $ct_jczqk_xj_val['fad_illegal_times'],
                $ct_jczqk_xj_val['fad_illegal_count'],
                $ct_jczqk_xj_val['fad_illegal_times_rate'].'%',
                $ct_jczqk_xj_val['fad_illegal_count_rate'].'%'
            ];
            if($ct_jczqk_xj_val['fmedia_class_code'] == '01'){
                $every_media_class_xj_data[0] = $ct_data_val;
            }elseif($ct_jczqk_xj_val['fmedia_class_code'] == '02'){
                $every_media_class_xj_data[1] = $ct_data_val;
            }else{
                $every_media_class_xj_data[2] = $ct_data_val;
            }
        }

        $every_media_class_xj_data[3] = [
            '合计',
            strval($ct_fad_times_xj),
            strval($ct_fad_count_xj),
            strval($ct_fad_illegal_times_xj),
            strval($ct_fad_illegal_count_xj),
            round((($ct_fad_illegal_times_xj)/($ct_fad_times_xj))*100,2).'%',
            round((($ct_fad_illegal_count_xj)/($ct_fad_count_xj))*100,2).'%'
        ];//二、各类媒体广告发布情况
        $every_media_class_xj_data = array_values($every_media_class_xj_data);


        /*
        * @各地域监测总情况
        *
        **/
        $every_region_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times_rate','fregionid',false,3,$check_datas);//各地域监测总情况
        $every_region_data_xj = [];//定义各地区发布监测情况数组
//循环赋值
        $every_region_illegal_times_data = [
            [''],
            ['']
        ];
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){
            $every_region_fad_times += $every_region_jczqk_val['fad_times'];
            $every_region_fad_illegal_times += $every_region_jczqk_val['fad_illegal_times'];
            $every_region_fad_count += $every_region_jczqk_val['fad_count'];
            $every_region_fad_illegal_count += $every_region_jczqk_val['fad_illegal_count'];
            //if($every_region_jczqk_val['fad_illegal_times'] > 0){
            array_push($every_region_illegal_times_data[0],$every_region_jczqk_val['tregion_name']);
            array_push($every_region_illegal_times_data[1],$every_region_jczqk_val['fad_illegal_times_rate'].'%');
            //}

            $every_region_data_xj[] = [
                strval($every_region_jczqk_key+1),
                $every_region_jczqk_val['tregion_name'],
                $every_region_jczqk_val['fad_times'],
                $every_region_jczqk_val['fad_count'],
                $every_region_jczqk_val['fad_illegal_times'],
                $every_region_jczqk_val['fad_illegal_count'],
                $every_region_jczqk_val['fad_illegal_times_rate'].'%',
                $every_region_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_region_data_xj[] = [
            '',
            '合计',
            strval($every_region_fad_times),
            strval($every_region_fad_count),
            strval($every_region_fad_illegal_times),
            strval($every_region_fad_illegal_count),
            round((($every_region_fad_illegal_times)/($every_region_fad_times))*100,2).'%',
            round((($every_region_fad_illegal_count)/($every_region_fad_count))*100,2).'%'
        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）



        /*
* @各地域电视广播报纸监测总情况
*
**/
        $every_media_all_data = [];
        $every_media_illegal_times_all_data = [];
        foreach (['01','02','03'] as $every_media_val){
            $every_media_data = [];
            $every_media_illegal_times_data = [];
            $every_media_jczqk = [];
            //循环赋值
            $every_media_fad_times = 0;
            $every_media_fad_illegal_times = 0;
            $every_media_fad_count = 0;
            $every_media_fad_illegal_count = 0;
            $every_media_illegal_times_data = [
                [''],
                ['']
            ];
            $every_media_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,$every_media_val,'','fad_illegal_times_rate','fmediaid',false,3,$check_datas);//各地域监测总情况
            foreach ($every_media_jczqk as $every_media_jczqk_key => $every_media_jczqk_val){
                $every_media_fad_times += $every_media_jczqk_val['fad_times'];
                $every_media_fad_illegal_times += $every_media_jczqk_val['fad_illegal_times'];
                $every_media_fad_count += $every_media_jczqk_val['fad_count'];
                $every_media_fad_illegal_count += $every_media_jczqk_val['fad_illegal_count'];
                if($every_media_jczqk_val['fad_illegal_times'] > 0){
                    array_push($every_media_illegal_times_data[0],$every_media_jczqk_val['tmedia_name']);
                    array_push($every_media_illegal_times_data[1],$every_media_jczqk_val['fad_illegal_times_rate'].'%');
                }

                $every_media_data[] = [
                    strval($every_media_jczqk_key+1),
                    $every_media_jczqk_val['tregion_name'],
                    $every_media_jczqk_val['tmedia_name'],
                    $every_media_jczqk_val['fad_times'],
                    $every_media_jczqk_val['fad_count'],
                    $every_media_jczqk_val['fad_illegal_times'],
                    $every_media_jczqk_val['fad_illegal_count'],
                    $every_media_jczqk_val['fad_illegal_times_rate'].'%',
                    $every_media_jczqk_val['fad_illegal_count_rate'].'%'
                ];
            }
            /*            $every_media_data[] = [
                            '',
                            '合计',
                            strval($every_media_fad_times),
                            strval($every_media_fad_count),
                            strval($every_media_fad_illegal_times),
                            strval($every_media_fad_illegal_count),
                            round((($every_media_fad_illegal_times)/($every_media_fad_times))*100,2).'%',
                            round((($every_media_fad_illegal_count)/($every_media_fad_count))*100,2).'%'
                        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）*/
            $every_media_all_data[] = $every_media_data;
            $every_media_illegal_times_all_data[] = $every_media_illegal_times_data;
        }
        $every_media_tv_data_xj = $every_media_all_data[0];//电视各媒体
        $every_media_tb_data_xj = $every_media_all_data[1];//广播各媒体
        $every_media_tp_data_xj = $every_media_all_data[2];//报纸各媒体
        $every_media_illegal_times_tv_data_xj = $every_media_illegal_times_all_data[0];//电视各媒体
        $every_media_illegal_times_tb_data_xj = $every_media_illegal_times_all_data[1];//广播各媒体
        $every_media_illegal_times_tp_data_xj =$every_media_illegal_times_all_data[2];//报纸各媒体

        //本级电视
        $hz_tv_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',0);
        $hz_tv_illegal_ad_data = $this->get_table_data($hz_tv_illegal_ad_data);
        //本级广播
        $hz_tb_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',0);
        $hz_tb_illegal_ad_data = $this->get_table_data($hz_tb_illegal_ad_data);

        //本级报纸
        $hz_tp_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',0);
        $hz_tp_illegal_ad_data = $this->get_table_data($hz_tp_illegal_ad_data);

        //下级电视
        $xj_tv_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',2,$check_datas);
        $xj_tv_illegal_ad_data = $this->get_table_data($xj_tv_illegal_ad_data);

        //下级广播
        $xj_tb_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',2,$check_datas);
        $xj_tb_illegal_ad_data = $this->get_table_data($xj_tb_illegal_ad_data);

        //下级报纸
        $xj_tp_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',2,$check_datas);
        $xj_tp_illegal_ad_data = $this->get_table_data($xj_tp_illegal_ad_data);



        /*
* @各广告类型监测总情况下级
*
**/
        $every_ad_class_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times_rate','fcode',false,3,$check_datas);//各地域监测总情况
        $every_ad_class_data_xj = [];//定义各地区发布监测情况数组
//循环赋值
        $every_ad_class_illegal_times_data_xj = [
            [''],
            ['']
        ];
        foreach ($every_ad_class_jczqk as $every_ad_class_jczqk_key => $every_ad_class_jczqk_val){
            $every_ad_class_fad_times += $every_ad_class_jczqk_val['fad_times'];
            $every_ad_class_fad_illegal_times += $every_ad_class_jczqk_val['fad_illegal_times'];
            $every_ad_class_fad_count += $every_ad_class_jczqk_val['fad_count'];
            $every_ad_class_fad_illegal_count += $every_ad_class_jczqk_val['fad_illegal_count'];
            if($every_ad_class_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_ad_class_illegal_times_data_xj[0],$every_ad_class_jczqk_val['tadclass_name']);
                array_push($every_ad_class_illegal_times_data_xj[1],strval($every_ad_class_jczqk_val['fad_illegal_times']));
            }

            $every_ad_class_data_xj[] = [
                strval($every_ad_class_jczqk_key+1),
                $every_ad_class_jczqk_val['tadclass_name'],
                $every_ad_class_jczqk_val['fad_times'],
                $every_ad_class_jczqk_val['fad_count'],
                $every_ad_class_jczqk_val['fad_illegal_times'],
                $every_ad_class_jczqk_val['fad_illegal_count'],
                $every_ad_class_jczqk_val['fad_illegal_times_rate'].'%',
                $every_ad_class_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_ad_class_data_xj[] = [
            '',
            '合计',
            strval($every_ad_class_fad_times),
            strval($every_ad_class_fad_count),
            strval($every_ad_class_fad_illegal_times),
            strval($every_ad_class_fad_illegal_count),
            round((($every_ad_class_fad_illegal_times)/($every_ad_class_fad_times))*100,2).'%',
            round((($every_ad_class_fad_illegal_count)/($every_ad_class_fad_count))*100,2).'%'
        ];

        //四大类 药品、医疗服务、医疗器械、保健食品
        $hz_zdclass_tv_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,['01','02','08','13','05','06'],1);
        $hz_zdclass_tb_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,['01','02','08','13','05','06'],1);
        $hz_zdclass_tp_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,['01','02','08','13','05','06'],1);
        $typical_illegal_ad_list_data = [];
        if(!empty($hz_zdclass_tv_illegal_ad_data) || !empty($hz_zdclass_tb_illegal_ad_data) || !empty($hz_zdclass_tp_illegal_ad_data)){
            $typical_illegal_ad_list = array_merge($hz_zdclass_tv_illegal_ad_data,$hz_zdclass_tb_illegal_ad_data,$hz_zdclass_tp_illegal_ad_data);
            $illegal_ad_list_data = [];
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_val){
                $md5 = MD5($typical_illegal_ad_list_val['fad_name'].$typical_illegal_ad_list_val['fmedianame']);
                if(isset($illegal_ad_list_data[$md5])){
                    $illegal_ad_list_data[$md5]['illegal_times'] += $typical_illegal_ad_list_val['illegal_times'];
                    $illegal_ad_list_data[$md5]['illegal_times'] = strval($illegal_ad_list_data[$md5]['illegal_times']);
                }else{

                    $illegal_ad_list_data[$md5] = $typical_illegal_ad_list_val;
                }
            }

            $typical_illegal_ad_list = array_values($illegal_ad_list_data);
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_key => $typical_illegal_ad_list_val){
                $typical_illegal_ad_list_data[] = [
                    strval($typical_illegal_ad_list_key+1),
                    $typical_illegal_ad_list_val['fmedianame'],
                    $typical_illegal_ad_list_val['fad_name'],
                    $typical_illegal_ad_list_val['fadclass'],
                    $typical_illegal_ad_list_val['illegal_times']
                ];
            }
        }



//定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "大标题楷体60",
            "text" => "广告监测月报"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号加粗",
            "text" => $user['regionname1']."工商行政管理局"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号加粗带红线",
            "text" => "广告监测中心               ".date('Y年m月d日')
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $user['regionname1']."工商行政管理局广告监测中心对".$date_ym."份".$r_name."部分电视、广播、报刊等".$ct_media_count."家媒体发布广告的情况进行了抽查监测，现将监测情况通报如下："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "一、监测范围及对象"
        ];
        if($ct_media_tv_count_hz != 0){
            $hz_media_count_str .= $ct_media_tv_count_hz."个电视监测".$days."天;";
        }
        if($ct_media_bc_count_hz != 0){
            $hz_media_count_str .= $ct_media_bc_count_hz."个广播监测".$days."天;";
        }
        if($ct_media_paper_count_hz != 0){
            $hz_media_count_str .= $ct_media_paper_count_hz."个报纸监测".$days."天;";
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）".$dq_name."媒体：".$hz_media_count_str
        ];
        $xj_dates = count($check_datas);
        if($ct_media_tv_count_xj != 0){
            $xj_media_count_str .= $ct_media_tv_count_xj."个电视监测".$xj_dates."天;";
        }
        if($ct_media_bc_count_xj != 0){
            $xj_media_count_str .= $ct_media_bc_count_xj."个广播监测".$xj_dates."天;";
        }
        if($ct_media_paper_count_xj != 0){
            $xj_media_count_str .= $ct_media_paper_count_xj."个报纸监测".$xj_dates."天;";
        }
        $tregion_count = M('tregion')->where(['fpid'=>$user['regionid']])->count();

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）其他".$tregion_count."个区、县：".$xj_media_count_str
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "二、各类媒体广告发布情况"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）市级媒体广告发布情况"
        ];


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "本月共监测广告".$every_media_class_bj_data[count($every_media_class_bj_data)-1][1]."条次，".$every_media_class_bj_data[count($every_media_class_bj_data)-1][2]."条数，其中监测发现各类涉嫌违法广告".$every_media_class_bj_data[count($every_media_class_bj_data)-1][3]."条次，".$every_media_class_bj_data[count($every_media_class_bj_data)-1][4]."条数。具体情况详见下表："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒介类型汇总表格hz",
            "data" => $every_media_class_bj_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）各区县媒体广告发布情况"
        ];


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "本月共监测广告".$every_media_class_xj_data[count($every_media_class_xj_data)-1][1]."条次，".$every_media_class_xj_data[count($every_media_class_xj_data)-1][2]."条数，其中监测发现各类涉嫌违法广告".$every_media_class_xj_data[count($every_media_class_xj_data)-1][3]."条次，".$every_media_class_xj_data[count($every_media_class_xj_data)-1][4]."条数。具体情况详见下表："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒介类型汇总表格hz",
            "data" => $every_media_class_xj_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "三、各区县媒体广告发布总体情况"
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "地域排名汇总表格hz",
            "data" => $every_region_data_xj
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "条次违法率chart",
            "data" => $every_region_illegal_times_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "四、各区县电视、广播、报纸广告发布情况（排名按广告条次违法率由高到底排列）"
        ];

        if(!empty($every_media_tv_data_xj)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "1、电视"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tv_data_xj
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "条次违法率chart",
                "data" => $every_media_illegal_times_tv_data_xj
            ];
        }
        if(!empty($every_media_tb_data_xj)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "2、广播"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tb_data_xj
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "条次违法率chart",
                "data" => $every_media_illegal_times_tb_data_xj
            ];
        }
        if(!empty($every_media_tp_data_xj)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "3、报纸"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tp_data_xj
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "条次违法率chart",
                "data" => $every_media_illegal_times_tp_data_xj
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、区县各类违法广告比重结构图"
        ];
        if(!empty($every_ad_class_data_xj)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表hz",
                "data" => $every_ad_class_data_xj
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "各大类别广告违法占比表hz",
                "data" => $every_ad_class_illegal_times_data_xj
            ];

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "六、全市各类媒体发布的违法广告（全部类别）通报"
        ];

        $illegal_ad_list_tv_bj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',0);
        $illegal_ad_list_tb_bj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',0);
        $illegal_ad_list_tp_bj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',0);
        $illegal_ad_list_bj = [];
        if(!empty($illegal_ad_list_tv_bj) || !empty($illegal_ad_list_tb_bj) || !empty($illegal_ad_list_tp_bj)) {
            $typical_illegal_ad_list = array_merge($illegal_ad_list_tv_bj, $illegal_ad_list_tb_bj, $illegal_ad_list_tp_bj);
            $illegal_ad_list_bj = pxsf($typical_illegal_ad_list, 'illegal_times');//排序
        }
        if(!empty($illegal_ad_list_bj)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "（一）菏泽市违法广告"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告详情列表hz",
                "data" => $this->pmaddkey($this->get_table_data($illegal_ad_list_bj),6)
            ];
        }

        $illegal_ad_list_tv_xj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',2,$check_datas1);
        $illegal_ad_list_tb_xj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',2,$check_datas1);
        $illegal_ad_list_tp_xj = $StatisticalReport->typical_illegal_ad_heze_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',2,$check_datas1);

        $illegal_ad_list_xj = [];
        if(!empty($illegal_ad_list_tv_xj) || !empty($illegal_ad_list_tb_xj) || !empty($illegal_ad_list_tp_xj)) {
            $typical_illegal_ad_list = array_merge($illegal_ad_list_tv_xj, $illegal_ad_list_tb_xj, $illegal_ad_list_tp_xj);
            $illegal_ad_list_xj = pxsf($typical_illegal_ad_list, 'illegal_times');//排序

        }
        if(!empty($illegal_ad_list_xj)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "（二）各区县违法广告"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告详情列表hz",
                "data" => $this->pmaddkey($this->get_table_data($illegal_ad_list_xj),6)
            ];
        }
        //1104
        /*$data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、区县各类违法广告比重结构图"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）市级媒体"
        ];


        if(!empty($hz_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tv_illegal_ad_data)
            ];
        }
        if(!empty($hz_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tb_illegal_ad_data)
            ];
        }
        if(!empty($hz_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tp_illegal_ad_data)
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）区（县）级媒体"
        ];

        if(!empty($xj_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tv_illegal_ad_data)
            ];
        }

        if(!empty($xj_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tb_illegal_ad_data)
            ];
        }

        if(!empty($xj_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tp_illegal_ad_data)
            ];
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "六、全市各类违法广告比重结构图"
        ];
        if(!empty($every_ad_class_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表hz",
                "data" => $every_ad_class_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "各大类别广告违法占比表hz",
                "data" => $every_ad_class_illegal_times_data
            ];

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "七、药品、医疗服务、医疗器械、保健食品违法广告发布情况"
        ];

        if(!empty($typical_illegal_ad_list_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $typical_illegal_ad_list_data
            ];
        }*/
        $report_data = json_encode($data);
        //echo $report_data;exit;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $system_num = getconfig('system_num');

//将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].date('Y年m月',strtotime($month)).'月报';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 30;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }
    //菏泽月报备份
    public function create_month_201811032021(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型
        $month = I('daytime',date('Y-m',time()));//接收时间  TODO::时间改
        //$month = I('daytime','2018-07');//接收时间  TODO::时间改

        $month_num = strval(substr($month,5,2));//月份
        if($month_num < 10){
            $this->ajaxReturn([
                "code" => 1,
                "code" => '抱歉，只能到处十月以及十月之后的报告！',
            ]);
        }
        if($month == '2018-10'){

            $report_start_date = strtotime('2018-10-18');
        }else{
            $report_start_date = strtotime($month);
        }
        $report_end_date = mktime(23, 59, 59, date('m', strtotime($month))+1, 00);//指定月份月末时间戳

        if($report_end_date > time()){
            $report_end_date = time();

        }

        $date_ym = date('Y年m月',$report_start_date);//年月字符

        $user = session('regulatorpersonInfo');//获取用户信息
        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级
        if($re_level == 2 || $re_level == 3 || $re_level == 4){
            $dq_name = '市级';
            $r_name = '全市';
            $xj_name = '区县';
        }elseif($re_level == 1){
            $dq_name = '全省';
            $xj_name = '市级';
        }else{
            $dq_name = '全'.$user['regionname1'];
            $xj_name = '地方';
        }
        $date_table = date('Ym',strtotime($month)).'_'.substr($user['regionid'],0,2);//组合表

        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数
        $owner_media_ids = get_owner_media_ids();

        /* 各大媒体类型数量 */
        $ct_media_tv_count_hz = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//菏泽市局电视媒体数量

        $ct_media_bc_count_hz = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//菏泽市局广播媒体数量

        $ct_media_paper_count_hz = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//菏泽市局报纸媒体数量

        $ct_media_tv_count_xj = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//菏泽各区县电视媒体数量

        $ct_media_bc_count_xj = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//菏泽各区县广播媒体数量

        $ct_media_paper_count_xj = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//菏泽各区县报纸媒体数量

        $ct_media_count = $ct_media_tv_count_hz + $ct_media_bc_count_hz + $ct_media_paper_count_hz + $ct_media_tv_count_xj + $ct_media_bc_count_xj + $ct_media_paper_count_xj;//全部有数据的媒体数量


        /*
        * @各媒体监测总情况
        *
        **/

        $ct_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_count_rate','fmediaclass');//传统媒体监测总情况

        $ct_fad_times = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times = 0;//定义传统媒体发布违法广告条次
        $every_media_class_data = [];//定义媒体发布监测情况数组
//循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk as $ct_jczqk_val){
            $ct_fad_times += $ct_jczqk_val['fad_times'];
            $ct_fad_illegal_times += $ct_jczqk_val['fad_illegal_times'];
            $ct_fad_count += $ct_jczqk_val['fad_count'];
            $ct_fad_illegal_count += $ct_jczqk_val['fad_illegal_count'];
            $ct_data_val = [
                $ct_jczqk_val['tmediaclass_name'],
                $ct_jczqk_val['fad_times'],
                $ct_jczqk_val['fad_count'],
                $ct_jczqk_val['fad_illegal_times'],
                $ct_jczqk_val['fad_illegal_count'],
                $ct_jczqk_val['fad_illegal_times_rate'].'%',
                $ct_jczqk_val['fad_illegal_count_rate'].'%'
            ];
            if($ct_jczqk_val['fmedia_class_code'] == '01'){
                $every_media_class_data[0] = $ct_data_val;
            }elseif($ct_jczqk_val['fmedia_class_code'] == '02'){
                $every_media_class_data[1] = $ct_data_val;
            }else{
                $every_media_class_data[2] = $ct_data_val;
            }
        }

        $every_media_class_data[3] = [
            '合计',
            strval($ct_fad_times),
            strval($ct_fad_count),
            strval($ct_fad_illegal_times),
            strval($ct_fad_illegal_count),
            round((($ct_fad_illegal_times)/($ct_fad_times))*100,2).'%',
            round((($ct_fad_illegal_count)/($ct_fad_count))*100,2).'%'
        ];//二、各类媒体广告发布情况
        $every_media_class_data = array_values($every_media_class_data);

        /*
        * @各地域监测总情况
        *
        **/
        $every_region_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times','fregionid');//各地域监测总情况
        $every_region_data = [];//定义各地区发布监测情况数组
//循环赋值
        $every_region_illegal_times_data = [
            [''],
            ['']
        ];
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){
            $every_region_fad_times += $every_region_jczqk_val['fad_times'];
            $every_region_fad_illegal_times += $every_region_jczqk_val['fad_illegal_times'];
            $every_region_fad_count += $every_region_jczqk_val['fad_count'];
            $every_region_fad_illegal_count += $every_region_jczqk_val['fad_illegal_count'];
            if($every_region_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_region_illegal_times_data[0],$every_region_jczqk_val['tregion_name']);
                array_push($every_region_illegal_times_data[1],strval($every_region_jczqk_val['fad_illegal_times']));
            }

            $every_region_data[] = [
                strval($every_region_jczqk_key+1),
                $every_region_jczqk_val['tregion_name'],
                $every_region_jczqk_val['fad_times'],
                $every_region_jczqk_val['fad_count'],
                $every_region_jczqk_val['fad_illegal_times'],
                $every_region_jczqk_val['fad_illegal_count'],
                $every_region_jczqk_val['fad_illegal_times_rate'].'%',
                $every_region_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_region_data[] = [
            '',
            '合计',
            strval($every_region_fad_times),
            strval($every_region_fad_count),
            strval($every_region_fad_illegal_times),
            strval($every_region_fad_illegal_count),
            round((($every_region_fad_illegal_times)/($every_region_fad_times))*100,2).'%',
            round((($every_region_fad_illegal_count)/($every_region_fad_count))*100,2).'%'
        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）



        /*
* @各地域电视广播报纸监测总情况
*
**/
        $every_media_all_data = [];
        $every_media_illegal_times_all_data = [];
        foreach (['01','02','03'] as $every_media_val){
            $every_media_data = [];
            $every_media_illegal_times_data = [];
            $every_media_jczqk = [];
            //循环赋值
            $every_media_fad_times = 0;
            $every_media_fad_illegal_times = 0;
            $every_media_fad_count = 0;
            $every_media_fad_illegal_count = 0;
            $every_media_illegal_times_data = [
                [''],
                ['']
            ];
            $every_media_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,$every_media_val,'','fad_illegal_times','fmediaid');//各地域监测总情况
            foreach ($every_media_jczqk as $every_media_jczqk_key => $every_media_jczqk_val){
                $every_media_fad_times += $every_media_jczqk_val['fad_times'];
                $every_media_fad_illegal_times += $every_media_jczqk_val['fad_illegal_times'];
                $every_media_fad_count += $every_media_jczqk_val['fad_count'];
                $every_media_fad_illegal_count += $every_media_jczqk_val['fad_illegal_count'];
                if($every_media_jczqk_val['fad_illegal_times'] > 0){
                    array_push($every_media_illegal_times_data[0],$every_media_jczqk_val['tmedia_name']);
                    array_push($every_media_illegal_times_data[1],strval($every_media_jczqk_val['fad_illegal_times']));
                }

                $every_media_data[] = [
                    strval($every_media_jczqk_key+1),
                    $every_media_jczqk_val['tregion_name'],
                    $every_media_jczqk_val['tmedia_name'],
                    $every_media_jczqk_val['fad_times'],
                    $every_media_jczqk_val['fad_count'],
                    $every_media_jczqk_val['fad_illegal_times'],
                    $every_media_jczqk_val['fad_illegal_count'],
                    $every_media_jczqk_val['fad_illegal_times_rate'].'%',
                    $every_media_jczqk_val['fad_illegal_count_rate'].'%'
                ];
            }
/*            $every_media_data[] = [
                '',
                '合计',
                strval($every_media_fad_times),
                strval($every_media_fad_count),
                strval($every_media_fad_illegal_times),
                strval($every_media_fad_illegal_count),
                round((($every_media_fad_illegal_times)/($every_media_fad_times))*100,2).'%',
                round((($every_media_fad_illegal_count)/($every_media_fad_count))*100,2).'%'
            ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）*/
            $every_media_all_data[] = $every_media_data;
            $every_media_illegal_times_all_data[] = $every_media_illegal_times_data;
        }
        $every_media_tv_data = $every_media_all_data[0];//电视各媒体
        $every_media_tb_data = $every_media_all_data[1];//广播各媒体
        $every_media_tp_data = $every_media_all_data[2];//报纸各媒体
        $every_media_illegal_times_tv_data = $every_media_illegal_times_all_data[0];//电视各媒体
        $every_media_illegal_times_tb_data = $every_media_illegal_times_all_data[1];//广播各媒体
        $every_media_illegal_times_tp_data =$every_media_illegal_times_all_data[2];//报纸各媒体

        //本级电视
        $hz_tv_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',0);
        $hz_tv_illegal_ad_data = $this->get_table_data($hz_tv_illegal_ad_data);
        //本级广播
        $hz_tb_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',0);
        $hz_tb_illegal_ad_data = $this->get_table_data($hz_tb_illegal_ad_data);

        //本级报纸
        $hz_tp_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',0);
        $hz_tp_illegal_ad_data = $this->get_table_data($hz_tp_illegal_ad_data);

        //下级电视
        $xj_tv_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',2);
        $xj_tv_illegal_ad_data = $this->get_table_data($xj_tv_illegal_ad_data);

        //下级广播
        $xj_tb_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',2);
        $xj_tb_illegal_ad_data = $this->get_table_data($xj_tb_illegal_ad_data);

        //下级报纸
        $xj_tp_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',2);
        $xj_tp_illegal_ad_data = $this->get_table_data($xj_tp_illegal_ad_data);



        /*
* @各广告类型监测总情况
*
**/
        $every_ad_class_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_times','fcode');//各地域监测总情况
        $every_ad_class_data = [];//定义各地区发布监测情况数组
//循环赋值
        $every_ad_class_illegal_times_data = [
            [''],
            ['']
        ];
        foreach ($every_ad_class_jczqk as $every_ad_class_jczqk_key => $every_ad_class_jczqk_val){
            $every_ad_class_fad_times += $every_ad_class_jczqk_val['fad_times'];
            $every_ad_class_fad_illegal_times += $every_ad_class_jczqk_val['fad_illegal_times'];
            $every_ad_class_fad_count += $every_ad_class_jczqk_val['fad_count'];
            $every_ad_class_fad_illegal_count += $every_ad_class_jczqk_val['fad_illegal_count'];
            if($every_ad_class_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_ad_class_illegal_times_data[0],$every_ad_class_jczqk_val['tadclass_name']);
                array_push($every_ad_class_illegal_times_data[1],strval($every_ad_class_jczqk_val['fad_illegal_times']));
            }

            $every_ad_class_data[] = [
                strval($every_ad_class_jczqk_key+1),
                $every_ad_class_jczqk_val['tadclass_name'],
                $every_ad_class_jczqk_val['fad_times'],
                $every_ad_class_jczqk_val['fad_count'],
                $every_ad_class_jczqk_val['fad_illegal_times'],
                $every_ad_class_jczqk_val['fad_illegal_count'],
                $every_ad_class_jczqk_val['fad_illegal_times_rate'].'%',
                $every_ad_class_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_ad_class_data[] = [
            '',
            '合计',
            strval($every_ad_class_fad_times),
            strval($every_ad_class_fad_count),
            strval($every_ad_class_fad_illegal_times),
            strval($every_ad_class_fad_illegal_count),
            round((($every_ad_class_fad_illegal_times)/($every_ad_class_fad_times))*100,2).'%',
            round((($every_ad_class_fad_illegal_count)/($every_ad_class_fad_count))*100,2).'%'
            ];

        //四大类 药品、医疗服务、医疗器械、保健食品
        $hz_zdclass_tv_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,['01','02','08','13','05','06'],1);
        $hz_zdclass_tb_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,['01','02','08','13','05','06'],1);
        $hz_zdclass_tp_illegal_ad_data = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,['01','02','08','13','05','06'],1);
        $typical_illegal_ad_list_data = [];
        if(!empty($hz_zdclass_tv_illegal_ad_data) || !empty($hz_zdclass_tb_illegal_ad_data) || !empty($hz_zdclass_tp_illegal_ad_data)){
            $typical_illegal_ad_list = array_merge($hz_zdclass_tv_illegal_ad_data,$hz_zdclass_tb_illegal_ad_data,$hz_zdclass_tp_illegal_ad_data);
            $illegal_ad_list_data = [];
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_val){
                $md5 = MD5($typical_illegal_ad_list_val['fad_name'].$typical_illegal_ad_list_val['fmedianame']);
                if(isset($illegal_ad_list_data[$md5])){
                    $illegal_ad_list_data[$md5]['illegal_times'] += $typical_illegal_ad_list_val['illegal_times'];
                    $illegal_ad_list_data[$md5]['illegal_times'] = strval($illegal_ad_list_data[$md5]['illegal_times']);
                }else{

                    $illegal_ad_list_data[$md5] = $typical_illegal_ad_list_val;
                }
            }

            $typical_illegal_ad_list = array_values($illegal_ad_list_data);
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_key => $typical_illegal_ad_list_val){
                $typical_illegal_ad_list_data[] = [
                    strval($typical_illegal_ad_list_key+1),
                    $typical_illegal_ad_list_val['fmedianame'],
                    $typical_illegal_ad_list_val['fad_name'],
                    $typical_illegal_ad_list_val['fadclass'],
                    $typical_illegal_ad_list_val['illegal_times']
                ];
            }
        }



//定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "大标题楷体60",
            "text" => "广告监测月报"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号加粗",
            "text" => $user['regionname1']."工商行政管理局"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号加粗带红线",
            "text" => "广告监测中心               ".date('Y年m月d日')
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $user['regionname1']."工商行政管理局广告监测中心对".$date_ym."份".$r_name."部分电视、广播、报刊等".$ct_media_count."家媒体发布广告的情况进行了抽查监测，现将监测情况通报如下："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "一、监测范围及对象"
        ];
        if($ct_media_tv_count_hz != 0){
            $hz_media_count_str .= $ct_media_tv_count_hz."个电视监测".$days."天;";
        }
        if($ct_media_bc_count_hz != 0){
            $hz_media_count_str .= $ct_media_bc_count_hz."个广播监测".$days."天;";
        }
        if($ct_media_paper_count_hz != 0){
            $hz_media_count_str .= $ct_media_paper_count_hz."个报纸监测".$days."天;";
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）".$dq_name."媒体：".$hz_media_count_str
        ];
        if($ct_media_tv_count_xj != 0){
            $xj_media_count_str .= $ct_media_tv_count_xj."个电视监测".$days."天;";
        }
        if($ct_media_bc_count_xj != 0){
            $xj_media_count_str .= $ct_media_bc_count_xj."个广播监测".$days."天;";
        }
        if($ct_media_paper_count_xj != 0){
            $xj_media_count_str .= $ct_media_paper_count_xj."个报纸监测".$days."天;";
        }
        $tregion_count = M('tregion')->where(['fpid'=>$user['regionid']])->count();

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）其他".$tregion_count."个区、县：".$xj_media_count_str
        ];
        
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "二、各类媒体广告发布情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "本月共监测广告".$every_media_class_data[3][1]."条次，".$every_media_class_data[3][2]."条数，其中监测发现各类涉嫌违法广告".$every_media_class_data[3][3]."条次，".$every_media_class_data[3][4]."条数。具体情况详见下表："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒介类型汇总表格hz",
            "data" => $every_media_class_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "三、市（各区县）媒体广告发布总体情况"
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "地域排名汇总表格hz",
            "data" => $every_region_data
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各市区县违法广告发布量排名条形图hz",
            "data" => $every_region_illegal_times_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "四、全市电视、广播、报纸广告发布情况（排名按发布违法广告条次由高到底排列）"
        ];

        if(!empty($every_media_tv_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "1、电视"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tv_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tv_data
            ];
        }
        if(!empty($every_media_tb_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "2、广播"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tb_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tb_data
            ];
        }
        if(!empty($every_media_tp_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "3、报纸"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tp_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tp_data
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、全市各类媒体发布的违法广告（全部类别）通报"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）市级媒体"
        ];


        if(!empty($hz_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tv_illegal_ad_data)
            ];
        }
        if(!empty($hz_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tb_illegal_ad_data)
            ];
        }
        if(!empty($hz_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tp_illegal_ad_data)
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）区（县）级媒体"
        ];

        if(!empty($xj_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tv_illegal_ad_data)
            ];
        }
        if(!empty($xj_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tb_illegal_ad_data)
            ];
        }

        if(!empty($xj_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tp_illegal_ad_data)
            ];
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "六、全市各类违法广告比重结构图"
        ];
        if(!empty($every_ad_class_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表hz",
                "data" => $every_ad_class_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "各大类别广告违法占比表hz",
                "data" => $every_ad_class_illegal_times_data
            ];

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "七、药品、医疗服务、医疗器械、保健食品违法广告发布情况"
        ];

        if(!empty($typical_illegal_ad_list_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $typical_illegal_ad_list_data
            ];
        }


        $report_data = json_encode($data);
        //echo $report_data;exit;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $system_num = getconfig('system_num');

//将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].date('Y年m月',strtotime($month)).'月报';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 30;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }


    public function get_table_data($data){
        $data_array = [];
        foreach ($data as $data_val){
            $data_array[] = array_values($data_val);
        }
        return $data_array;

    }

    public function pmaddkey($data,$score_rule = 4){
        $data = pxsf($data,$score_rule);
        $res = [];
        foreach ($data as $data_key=>$data_val){
            $array_res = [
                strval($data_key+1),
            ];
            foreach ($data_val as $data_val2){
                $array_res[] = $data_val2;
            }
            $res[] = $array_res;
        }
        return $res;
    }
    

}