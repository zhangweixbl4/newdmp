<?php
namespace TaskInput\Controller;

use TaskInput\Model\TaskInputModel;

class IllegalAdReviewController extends BaseController {

    public function _initialize(){
        
		if(	!A('InputPc/Task','Model')->user_other_authority('3001')){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
		}
    }



	/*违法广告确认页面*/
    public function illegalReview(){
        

		$this->assign('getRegionTree',A('Common/Region','Model')->getRegionTree());
		$this->assign('getAdClassArr',A('Common/AdClass','Model')->getAdClassArr());
		$this->display();
    }
	
	/*搜索列表*/
	public function search_issue(){
		$p = I('p',1);//当前第几页
		$pp = I('pp',20);//每页显示多少记录
		
		$issuedate_min = I('issuedate_min');//查询开始日期
		$issuedate_max = I('issuedate_max');//查询结束日期
		$sx = I('sx');
		$fadname = I('fadname');
		$fmedianame = I('fmedianame');
		$fregionid = I('fregionid');
		$this_region_id = I('this_region_id');
		
		
		$regionIds = M('ad_input_user')->cache(true,60)
            ->join('ad_input_user_group on ad_input_user.group_id = ad_input_user_group.group_id')
            ->where(['wx_id' => session('wx_id')])
            ->getField('regionauthority');
		if(!is_array($regionIds)){
			$regionIds = json_decode($regionIds,true);
			
		}
		
		$where = array();
		

		$where['sam_id'] = array('gt',0);
		$where['issue_date'] = array('between',array($issuedate_min,$issuedate_max));
		
		if($sx !== ''){
			$where['type'] = $sx;
		}
		if($fadname){
			$where['illegal_ad_issue_review.ad_name'] = array('like','%'.$fadname.'%');
		}
		if($fmedianame){
			$where['tmedia.fmedianame'] = array('like','%'.$fmedianame.'%');
		}
		
		if($fregionid){
			if($this_region_id == 'true'){
				$where['tmedia.media_region_id'] = $fregionid;//地区搜索条件
			}elseif($fregionid != 100000){
				$region_id_rtrim = A('Common/System','Model')->get_region_left($fregionid);//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmedia.media_region_id,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}
		}
		
		$regionIds = M('ad_input_user')->cache(true,1)
            ->join('ad_input_user_group on ad_input_user.group_id = ad_input_user_group.group_id')
            ->where(['wx_id' => session('wx_id')])
            ->getField('regionauthority');//查询地区权限
	
		if(!is_array($regionIds) && $regionIds != 'unrestricted'){//判断是否数组，如果不是数组则转为数组
			$regionIds = json_decode($regionIds,true);
			
		}
		
		if($regionIds != 'unrestricted'){
			$regionAuth = array('0');
			foreach($regionIds as $re){
				$regionAuth = array_merge($regionAuth,M('tregion')->where(array('fid'=>array('like',$re.'%')))->getField('fid',true));
			}
			
			$where['media_region_id'] = array('in',$regionAuth);
		}
		
		//var_dump($where);
		
		
		$dataList0 = M('illegal_ad_issue_review')
											->field('
														tmedia.fmedianame,
														illegal_ad_issue_review.media_id,
														illegal_ad_issue_review.ad_name,
														illegal_ad_issue_review.sam_id,
														illegal_ad_issue_review.fadclasscode,
														
														count(*) as count,
														count(case when type = 0 then 1 else null end) as count0,
														count(case when type = 1 then 1 else null end) as count1,
														count(case when type = -1 then 1 else null end) as count_1
														
														
														
														')
											->join('tmedia on tmedia.fid = illegal_ad_issue_review.media_id')
											->where($where)
											->group('illegal_ad_issue_review.media_id,illegal_ad_issue_review.sam_id')
											
											->page($p,$pp)
											->select();
		$dataList = array();
		foreach($dataList0 as $data0){
			//$data0['remarkCount'] = M('illegal_ad_issue_review_remark')->cache(true,1)->where(array('media_id'=>$data0['media_id'],'sam_id'=>$data0['sam_id']))->count();
			
			$data0['fadclassname'] = M('tadclass')->where(array('fcode'=>$data0['fadclasscode']))->getField('ffullname');
			$dataList[] = $data0;
		}
			
		//var_dump(M('illegal_ad_issue_review')->getLastSql());									
		$total = M('illegal_ad_issue_review')
											->field('count(distinct illegal_ad_issue_review.media_id,illegal_ad_issue_review.sam_id) as total')
											->join('tmedia on tmedia.fid = illegal_ad_issue_review.media_id')
											->where($where)
											
											
											->select();									
		//var_dump( M('illegal_ad_issue_review')->getLastSql());
		//var_dump($total[0]['total']);
		$this->ajaxReturn(array('code'=>0,'msg'=>'','dataList'=>$dataList,'total'=>intval($total[0]['total']),'issuedate_min'=>$issuedate_min,'issuedate_max'=>$issuedate_max));
	
	}
	
	
	/*违法广告详情*/
	public function info(){
		
		$media_id = I('media_id');
		$sam_id = I('sam_id');
		
		$issuedate_min = I('issuedate_min');//查询开始日期
		$issuedate_max = I('issuedate_max');//查询结束日期
		
		
		$mediaInfo = M('tmedia')->field('tmedia.fid,fmedianame,left(tmedia.fmediaclassid,2) as media_class')->where(array('fid'=>$media_id))->find();
		

		
		$where = array();
		$where['media_id'] = $media_id;
		$where['sam_id'] = $sam_id;
		$where['issue_date'] = array('between',array($issuedate_min,$issuedate_max));
		
		$issueList = M('illegal_ad_issue_review')
												->field('
															id,
															start_time,
															end_time,
															FROM_UNIXTIME(start_time) as starttime,
															FROM_UNIXTIME(end_time) as endtime,
															
															remark,
															issue_date,
															type
														')
												->where($where)
												->select();
		$remarkList = M('illegal_ad_issue_review_remark')->where(array('media_id'=>$media_id,'sam_id'=>$sam_id))->select();
												
		
		if($mediaInfo['media_class'] == '01'){
			$samInfo = M('ttvsample')
								
								->field('
											ttvsample.fid,
											ttvsample.fillegaltypecode,
											ttvsample.fillegalcontent,
											
											ttvsample.fexpressioncodes,
											tadclass.ffullname as adclass,
											tad.fadname,
											tad.fadclasscode,
											ttvsample.favifilename as sam_filename

										')
								->join('tad on tad.fadid = ttvsample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->where(array('fid'=>$sam_id))
								->find();
		}elseif($mediaInfo['media_class'] == '02'){
			$samInfo = M('tbcsample')
								
								->field('
											tbcsample.fid,
											tbcsample.fillegaltypecode,
											tbcsample.fillegalcontent,
											tbcsample.fexpressioncodes,
											tadclass.ffullname as adclass,
											tad.fadname,
											tad.fadclasscode,
											tbcsample.favifilename as sam_filename

										')
								->join('tad on tad.fadid = tbcsample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->where(array('fid'=>$sam_id))
								->find();
		}elseif($mediaInfo['media_class'] == '03'){
			$samInfo = M('tpapersample')
								
								->field('
											tpapersample.fpapersampleid as fid,
											tpapersample.fillegaltypecode,
											tpapersample.fillegalcontent,
											tpapersample.fexpressioncodes,
											tadclass.ffullname as adclass,
											tad.fadname,
											tad.fadclasscode,
											tpapersample.fjpgfilename as sam_filename

										')
								->join('tad on tad.fadid = tpapersample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->where(array('fpapersampleid'=>$sam_id))
								->find();
		}else{
			
		}
		
		//$this->ajaxReturn(array('code'=>-1,'msg'=>'与原始数据不一致,该数据已删除,请刷新列表'));
		
		
		
		
		
		if(!$mediaInfo || !$issueList || !$samInfo){
			M('illegal_ad_issue_review')->where($where)->delete();
			$this->ajaxReturn(array('code'=>-1,'msg'=>'与原始数据不一致,该数据已删除,请刷新列表'));
		}
		
		
		if($samInfo['fillegaltypecode'] != 30){
			M('illegal_ad_issue_review')->where($where)->delete();
			$this->ajaxReturn(array('code'=>-1,'msg'=>'原样本已不违法,该数据已删除,请刷新列表'));
		}
		M('illegal_ad_issue_review')->where($where)->save(array('ad_name'=>$samInfo['fadname']));
									
		$this->ajaxReturn(array('code'=>0,'msg'=>'','mediaInfo'=>$mediaInfo,'issueList'=>$issueList,'samInfo'=>$samInfo,'remarkList'=>$remarkList));								
		
		
		
		
		
	}
	
	/*增加备注列表*/
	public function add_remark_list(){
		$media_id = I('media_id');
		$sam_id = I('sam_id');
		$content = I('content');
		if(!$media_id || !$sam_id || !$content){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'信息错误,提交失败'));
		}
		
		
		$addData = array();
		$addData['media_id'] = $media_id;
		$addData['sam_id'] = $sam_id;
		$addData['content'] = $content;
		$addData['createor'] = session('wx_id');
		$addData['createtime'] = date('Y-m-d H:i:s');
		$fid = M('illegal_ad_issue_review_remark')->add($addData);
		
		
		if($fid){
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败'));
		}
		
		
	}
	
	
	/*修改备注*/
	public function edit_remark(){
		
		$media_id = I('media_id');
		$sam_id = I('sam_id');
		
		$issuedate_min = I('issuedate_min');//查询开始日期
		$issuedate_max = I('issuedate_max');//查询结束日期
		$remark = I('remark');//备注信息
		
		$where = array();
		$where['media_id'] = $media_id;
		$where['sam_id'] = $sam_id;
		$where['issue_date'] = array('between',array($issuedate_min,$issuedate_max));
		
		$editCount = M('illegal_ad_issue_review')->where($where)->save(array('remark'=>$remark));
		//echo M('illegal_ad_issue_review')->getLastSql();
		if($editCount > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败'));
		}
		
	}	
	
	/*修改确认状态*/
	public function edit(){
		if(	!A('InputPc/Task','Model')->user_other_authority('3002')){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
		}
		$idList = I('idList');
		$idList = explode(',',$idList);
		
		$type = I('type');
		
		$editCount = 0;
		if(count($idList) > 0){
			$editCount = M('illegal_ad_issue_review')->where(array('id'=>array('in',$idList)))->save(array('type'=>$type,'wx_id'=>session('wx_id')));//执行修改
			//var_dump(M('illegal_ad_issue_review')->getLastSql());
		}
		$editCount = intval($editCount);
		$this->ajaxReturn(array('code'=>0,'msg'=>'修改了'.$editCount.'条记录'));
		
		
	}
	
	/*批量修改确认状态*/
	public function batch_edit(){
		if(	!A('InputPc/Task','Model')->user_other_authority('3002')){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
		}
		$media_id = I('media_id');
		$sam_id = I('sam_id');
		
		$issuedate_min = I('issuedate_min');//查询开始日期
		$issuedate_max = I('issuedate_max');//查询结束日期
		$type = I('type');
		
		$where = array();
		$where['media_id'] = $media_id;
		$where['sam_id'] = $sam_id;
		$where['issue_date'] = array('between',array($issuedate_min,$issuedate_max));
		
		$editCount = M('illegal_ad_issue_review')->where($where)->save(array('type'=>$type,'wx_id'=>session('wx_id')));
			
		$editCount = intval($editCount);
		$this->ajaxReturn(array('code'=>0,'msg'=>'修改了'.$editCount.'条记录'));
		
		
	}
	
	/*修改样本*/
	public function edit_sam(){
		$media_class = I('media_class');
		$sam_id = I('sam_id');
		
		$fillegalcontent = I('fillegalcontent');//涉嫌违法内容
		$fexpressioncodes = I('fexpressioncodes');//违法表现代码，分隔符 分隔
		$fadname = I('fadname');//广告名称
		
		
		
		
		$fexpressioncodes_arr = explode(',',$fexpressioncodes);
		
	
		
		$illegalList = M('tillegal')->where(array('fcode'=>array('in',$fexpressioncodes_arr)))->select();
		
		
		$fexpressioncodes_array = array();
		$fexpressions_array = array();
		$fconfirmations_array = array();
		$fpunishments_array = array();
		$fpunishmenttypes_array = array();
		
		
		foreach($illegalList as $illegal){
			$fexpressioncodes_array[] = $illegal['fcode'];//违法表现代码
			$fexpressions_array[] = $illegal['fexpression'];//违法表现
			$fconfirmations_array[] = $illegal['fconfirmation'];//认定依据
			$fpunishments_array[] = $illegal['fpunishment'];//处罚依据
			$fpunishmenttypes_array[] = $illegal['fpunishmenttype'];//处罚种类及幅度
			
		}
		
		
		
		
	}
	
	
	
		
}