<?php
namespace Api\Model;
use Think\Exception;

class CoreModel{
	
	/*
	* 添加业务请求日志
	* by ze
	*/
	public function addLog($nowdata){
		if(empty($nowdata[0])){
			$cldata[0] = $nowdata;
		}else{
			$cldata = $nowdata;
		}
		foreach ($cldata as $key => $data) {
			$fpostcontent = is_array($data['fpostcontent'])?json_encode($data['fpostcontent']):$data['fpostcontent'];
			$freturncontent = is_array($data['freturncontent'])?json_encode($data['freturncontent']):$data['freturncontent'];
			
			$addData['fbizname'] = $data['fbizname'];
			$addData['ftype'] = $data['ftype'];
			$addData['furl'] = $data['furl'];
			$addData['fmain_id'] = $data['fmain_id']?$data['fmain_id']:0;
			$addData['fpostcontent'] = $fpostcontent;
			$addData['freturncontent'] = $freturncontent;
			$allData[] = $addData;
		}
		$do_ncl = M('tbiz_log')->addAll($allData);
		return count($allData);
	}
	
}