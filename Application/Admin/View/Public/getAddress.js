function getAddress(url) {
    $.post(url, function (req) {
        var $provinces_li = $('.region>ul');
        $.each(req.regionList, function (e) {
            $provinces_li.append('<li><a href="javascript:void(0);" class="provinces" fid="' + this.fid + '">' + this.fname + '</a></li>');
        });
        $('.area')
            .tabs()
            .on('click', '.provinces', function () {
                //获取当前对象
                var fid = $(this).attr('fid'),
                    $this = $(this),		//追加DIV
                    $tabs = $this.parents('.selectAddress'),		//追加DIV
                    $div = $('<div id="tabs-2"></div>'),
                    $ul = $('<ul></ul>');		//追加ul
                $('.address').attr('fid', fid);
                $('.address').val($this.text());
                $tabs.children(':eq(0)').children(':gt(0)').remove();
                $tabs.children('div:gt(0)').remove();
                $.post(url, { "fid": fid }, function (req) {
                    $.each(req.regionList, function () {
                        $ul.append('<li><a href="javascript:void(0);" class="city" fid="' + this.fid + '">' + this.fname + '</a></li>');
                    });
                })
                //each遍历，赋值
                $tabs.children('ul').append('<li><a href="#tabs-2">市区</a></li>');
                $tabs.append($div.addClass('region clearfix').append($ul));
                $tabs
                    .tabs("refresh")
                    .tabs('option', 'active', 1)
                    .data('address', $this.text());
            })
            .on('click', '.city', function () {
                var fid = $(this).attr('fid');
                $this = $(this),
                    $tabs = $this.parents('.selectAddress'),
                    $div = $('<div id="tabs-3"></div>'),
                    $ul = $('<ul></ul>');
                $('.address').attr('fid', fid);
                $('.address').val($tabs.data('address') + '/' + $this.text());
                $tabs.children('ul').children(':eq(2)').remove();
                $tabs.children('div:eq(2)').remove();
                $.post(url, { "fid": fid }, function (req) {
                    if (req.regionList.length != 0) {			//判断是否有下级
                        $.each(req.regionList, function () {
                            $ul.append('<li><a href="javascript:void(0);" class="county"  fid="' + this.fid + '">' + this.fname + '</a></li>');
                        });

                        $tabs.children('ul').append('<li><a href="#tabs-3">县区</a></li>');
                        $tabs.append($div.addClass('region clearfix').append($ul));
                        $tabs
                            .tabs("refresh")
                            .tabs('option', 'active', 2)
                            .data('address', $tabs.data('address') + '/' + $this.text());
                    } else {
                        //获取值并赋值至文本框中
                        $('.address').attr('fid', fid);
                        $('.address').val($tabs.data('address') + '/' + $this.text());
                        $tabs.parent().hide();
                    }
                });
            })
            .on('click', '.county', function () {
                var $this = $(this),
                    fid = $(this).attr('fid'),
                    $tabs = $this.parents('.selectAddress');
                $('.address').attr('fid', fid);
                $('.address').val($tabs.data('address') + '/' + $this.text());
                $tabs.parent().hide();
            })
    })
    $(document).bind('click', function (e) {
        var $target = $(e.target),
            addressInfo = $('#addressInfo');
        if (!$target.hasClass('selectAddress')
            && $target.parents('.selectAddress').size() == 0
            && !$target.is($('.address'))
            && addressInfo.is(':visible')) {
            $('#addressInfo').hide();
        }
    });
    $('.address').bind('focus', function () {
        var $this = $(this);
        $this.val('');
        $('#addressInfo').css({
            top: $this.offset().top + $this.outerHeight(),
            left: $this.offset().left
        }).show();
    });
    $('.area').on('click', '#addrClose', function () {
        $('#addressInfo').hide();
    })
}