<?php
namespace Gongshang\Controller;
use Think\Controller;

class AdguideListController extends BaseController
{
	public function index()
	{
		$p = I('p', 1);//当前第几页
		$pp = 20;//每页显示多少记录
//发布媒介、广告名称、广告内容类别、违法类型、涉嫌违法内容、发布次数、指导次数、状态、操作（详情）；
		$fadname = I('fadname');// 广告名称
		$fmediaclass = I('fmediaclass');//媒体类别
		$fcreatetime_s = I('fcreatetime_s');// 创建时间  开始
		$fcreatetime_e = I('fcreatetime_e');// 创建时间  结束
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID
		$fadclasscode = I('fadclasscode');// 广告内容类别
		$fmediaid = I('fmediaid');// 发布媒介
		if ($fcreatetime_s == '') {
			$fcreatetime_s = '2015-01-01';
		}
		if ($fcreatetime_e == '') {
			$fcreatetime_e = date('Y-m-d');
		}
		$where = array();
		$where['tillegalad.fdisposestyle'] = array('in', '0,1');
		$where['tillegalad.fstate'] = array('in', '0,1');
		$where['tillegalad.fcreatetime'] = array('between', $fcreatetime_s . ',' . $fcreatetime_e . ' 23:59:59'); //时间
		if ($fadname != '') {
			$where['tad.fadname'] = array('like', '%' . $fadname . '%');//广告名称
		}
		if ($fmediaclass != '') {
			$where['tillegalad.fmediaclassid'] = $fmediaclass;
		}
		if (is_array($fillegaltypecode)) {
			$where['tillegalad.fillegaltypecode'] = array('in', $fillegaltypecode);//违法类型代码
		}
		if (isset($fadclasscode) && !empty($fadclasscode)) {
			$arr_code=$this->get_next_tadclasscode($fadclasscode,true);//获取下级广告类别代码
			if($arr_code){
				$where['tad.fadclasscode'] = array('in', $arr_code);
			}else{
				$where['tad.fadclasscode'] = $fadclasscode;
			}
		}
		if($fmediaid == ''){
			//是否有指定的媒体
			if(session('regulatorpersonInfo.special_media')){
				$media=json_decode(session('regulatorpersonInfo.special_media'),true);
			}else{
				$media=$this->get_regulator_mediaid(session('regulatorpersonInfo.fregulatorid'));//监管机构对应的媒体
				//如果没有对应的媒体，用地区id取媒体列表
				if(!$media){
					$media=$this->get_region_mediaid(session('regulatorpersonInfo.regulator_regionid'));
				}
			}
			$where['tillegalad.fmediaid'] = array('in', $media);//发布媒介
		}else{
			$where['tillegalad.fmediaid'] = $fmediaid;//发布媒介
		}
		$where['tmediaowner.fregionid'] = session('regulatorpersonInfo.regulator_regionid');//地区搜索条件

		$count = M('tillegalad')
			->join('tmediaclass on tillegalad.fmediaclassid = tmediaclass.fid ', 'LEFT')//媒体列别
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on  tad.fadclasscode=tadclass.fcode ', 'LEFT')//广告内容列别
			->join('tmedia on  tillegalad.fmediaid=tmedia.fid', 'LEFT')//媒介信息
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//媒介机构
			->join('(select * from taddirect group by fdirecttimes order by fdirecttimes desc limit 1) as ta  on  tillegalad.fillegaladid=ta.fillegaladid', 'LEFT')
//			->join('ta  on  tillegalad.fillegaladid=ta.fillegaladid', 'LEFT')//视图写法
			->join('tillegaltype on tillegalad.fillegaltypecode=tillegaltype.fcode', 'LEFT')//违法类型
			->join('(SELECT * FROM `tillegaladflow` where fbizname=\'广告指导\' and fflowname=\'广告指导\') as f  on  tillegalad.fillegaladid=f.fillegaladid ', 'LEFT')//违法表
			->where($where)
			->where('f.fstate IN (\'0\',\'1\') or f.fstate is NULL')
			->order('tillegalad.fstate asc')
			->count();// 查询满足要求的总记录数

		$Page = new \Think\Page($count, $pp);//, 实例化分页类 传入总记录数和每页显示的记录数
		$data = M('tillegalad')
			->field('
						tillegalad.fillegaladid,
			            tad.fadname,
			            tmedia.fmedianame,
			            tadclass.ffullname as adclass_fullname,
			            tillegaltype.fillegaltype,
			            tillegalad.fillegalcontent,
			            tillegalad.fissuetimes,
			            tillegalad.ffirstissuetime,
			            ta.fdirecttimes,
			            tillegalad.fstate
									')
			->join('tmediaclass on tillegalad.fmediaclassid = tmediaclass.fid ', 'LEFT')//媒体列别
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on  tad.fadclasscode=tadclass.fcode ', 'LEFT')//广告内容列别
			->join('tmedia on  tillegalad.fmediaid=tmedia.fid', 'LEFT')//媒介信息
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//媒介机构
			->join('(select * from taddirect group by fdirecttimes order by fdirecttimes desc limit 1) as ta  on  tillegalad.fillegaladid=ta.fillegaladid', 'LEFT')
//			->join('ta  on  tillegalad.fillegaladid=ta.fillegaladid', 'LEFT')//视图写法
			->join('tillegaltype on tillegalad.fillegaltypecode=tillegaltype.fcode', 'LEFT')//违法类型
			->join('(SELECT * FROM `tillegaladflow` where fbizname=\'广告指导\' and fflowname=\'广告指导\') as f  on  tillegalad.fillegaladid=f.fillegaladid ', 'LEFT')//违法表
			->where($where)
			->where('f.fstate IN (\'0\',\'1\') or f.fstate is NULL')
			->order('tillegalad.fstate asc')
			->limit($Page->firstRow . ',' . $Page->listRows)
			->select();//查询违法广告
		$data = list_k($data, $p, $pp);//为列表加上序号
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate' => 1))->select();//查询违法类型

		$this->assign('illegaltype', $illegaltype); //违法类型
		$this->assign('data', $data); //列表信息
		$this->assign('page', $Page->show());//分页输出
		$this->display('ad_guide');
	}

	/*指导详情*/
	public function ad_guide_details()
	{
		$where = array();
		if(I('fillegaladid')){
			$fillegaladid = I('fillegaladid');//线索ID
		}else{
			$this->error('缺失fillegaladid!');
		}
		$arr['fstate'] = 1;
		$res = M("tillegalad")->where(array('fillegaladid' => $fillegaladid))->save($arr);//待处理改正处理 埋点

		$where['tillegalad.fillegaladid'] = $fillegaladid;

		$data = M('tillegalad')
			->field('
						tillegalad.fillegaladid,
						tillegalad.fmediaclassid,
						tillegalad.fadid,
			            tad.fadname,
			            tmedia.fmedianame,
			            tadclass.ffullname as adclass_fullname,
			            tillegalad.fissuetimes,
			            taddirect.fdirecttimes,
			            tillegalad.ffirstissuetime,
			            tillegalad.flastissuetime,

			            tillegaltype.fillegaltype,
			            tillegalad.fillegalcontent,
			            tillegalad.fexpressions,
			            tillegalad.fconfirmations
									')//id，广告名称，<<媒介类别>>,广告id.广告内容列别，发布次数。指导次数。首次发布日期，末次发布日期
			// 违法类型：违法内容：违法表现：认定依据：
			->join('tmediaclass on tmediaclass.fid = tillegalad.fmediaclassid', 'LEFT')//媒体列别
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on tadclass.fcode = tad.fadclasscode', 'LEFT')//广告内容列别
			->join('tmedia on tmedia.fid = tillegalad.fmediaid', 'LEFT')//媒介信息
			->join('taddirect on taddirect.fillegaladid = tillegalad.fillegaladid', 'LEFT')//广告指导
			->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode', 'LEFT')//违法类型
			->where($where)
			->find();//查询广告信息
		if ($data['fmediaclassid']) {
			switch ($data['fmediaclassid']) {
				case 'bc':
					$adclueInfo = M('tbcsample')->field('fid,favifilename')->where(array('fadid' => $data['fadid']))->find();
					break;
				case 'tv':
					$adclueInfo = M('ttvsample')->field('fid,favifilename')->where(array('fadid' => $data['fadid']))->find();
					break;
				case 'paper':
					$adclueInfo = M('tpapersample')->field('fpapersampleid,fjpgfilename')->where(array('fadid' => $data['fadid']))->find();
					break;
			}
		}
		$ttemplate = M('ttemplate')->where(array('ftemplatetype' =>'建议' ))->find();//建议模板
		$re_contents = html_entity_decode($ttemplate['fcontents']);

		$flow = M('tillegaladflow')->where(array('fillegaladid' =>$fillegaladid))->select();//流程信息

		//模板的变量信息
		$moban_data['fadname']=$data['fadname'];
		$moban_data['fmedianame']=$data['fmedianame'];
		$moban_data['ffirstissuetime']=$data['ffirstissuetime'];
		$moban_data['now_time']=date('Y-m-d');;
		$moban_data['regulatorname']=session('regulatorpersonInfo.regulatorname');


		$this->assign('flow', $flow);
		$this->assign('data', $data); //违法类型
		$this->assign('adclueInfo', $adclueInfo); //视频信息
		$this->assign('ttemplate', $ttemplate); //模板
		$this->assign('re_contents', $re_contents); //列表信息
		$this->assign('moban_data', $moban_data); //模板 变量信息
		$this->display();
	}

	/*指导反馈*/
	public function results()
	{
		if(I('fillegaladid')){
			$fillegaladid = I('fillegaladid');

		}else{
		    $this->ajaxReturn(array('code'=>-1,'msg'=>'fillegaladid不存在！'));
		}
		if(I('ad_guide')){
			$ad_guide = I('ad_guide');//处理结果

		}else{

			$this->ajaxReturn(array('code'=>-1,'msg'=>'请填写处理结果！'));
		}

		$tillegalad = M('tillegalad')->where(array('fillegaladid' => $fillegaladid))->find();//查询违法广告信息
		switch ($ad_guide) {
			case 1:
				/*添加指导表记录*/
				$data['fillegaladid'] = $fillegaladid;
				$data['fdirecttype'] = I('guideType');//指导类型
				$data['fdirectcontent'] = I('content'); //描述
				$data['fdirectregulatorid'] = session('regulatorpersonInfo.fregulatorid');
				$data['fdirectregulator'] = session('regulatorpersonInfo.regulatorname');
				$data['fdirector'] = session('regulatorpersonInfo.fname');
				$data['fdirecttime'] = date('Y-m-d H:i:s');
				$data['fstate'] = 1;
				$taddirect = M('taddirect')->where(array('fillegaladid' => $fillegaladid))->find();//是否指导表已经有信息
				if(!$taddirect){
					$data['fmediaid'] = $tillegalad['fmediaid'];
					$data['fsampleid'] = $tillegalad['fsampleid'];
					$data['fdirecttimes'] = M('taddirect')->where(array('fsampleid' => $tillegalad['fsampleid']))->count();//指导次数根据样本id
					$reg = M("taddirect")->add($data);
				}else{
					$reg = M("taddirect")->where(array('fillegaladid' => $fillegaladid))->save($data);
				}
				if(!$reg){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'指导失败！'));
				}
				$flow_data = M('tillegaladflow')->where(array('fillegaladid' => $fillegaladid,'fflowname' => '广告指导','fstate' =>1))->find();//是否指导表已经有信息
				if($flow_data){
					$save_list['fregulatorpersonid'] = session('regulatorpersonInfo.fid');//处理人ID
					$save_list['fregulatorpersonname'] = session('regulatorpersonInfo.fname');//处理人
					$save_list['ffinishtime'] = date('Y-m-d H:i:s');//完成时间
					$save_list['fstate'] = 2;
					$save_reg = M("tillegaladflow")->where(array('fillegaladflowid' => $flow_data['fillegaladflowid']))->save($save_list);
					if(!$save_reg){
						$this->ajaxReturn(array('code'=>-1,'msg'=>'广告指导失败！！'));
					}

				}else{
					/*新增流程表_广告指导*/
					$zd_list['fillegaladid'] = $fillegaladid;
					$zd_list['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');
					$zd_list['fcreateregualtorname'] =  session('regulatorpersonInfo.regulatorname');
					$zd_list['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
					$zd_list['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
					$zd_list['fcreatetime'] = date('Y-m-d H:i:s');
					$zd_list['fbizname'] = '广告指导';
					$zd_list['fflowname'] = '广告指导';
					$zd_list['fregulatorid'] = session('regulatorpersonInfo.fregulatorid');
					$zd_list['fregulatorname'] =  session('regulatorpersonInfo.regulatorname');
					$zd_list['fregulatorpersonid'] = session('regulatorpersonInfo.fid');
					$zd_list['fregulatorpersonname'] = session('regulatorpersonInfo.fname');
					$zd_list['ffinishtime'] = date('Y-m-d H:i:s');
					$zd_list['fstate'] = 2;
					$zd_res = M("tillegaladflow")->add($zd_list);
					if(!$zd_res){
						$this->ajaxReturn(array('code'=>-1,'msg'=>'广告指导失败！！'));
					}

				}

				/*新增流程表_签发流程*/
				$qf_list['fillegaladid'] = $fillegaladid;
				$qf_list['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');
				$qf_list['fcreateregualtorname'] =  session('regulatorpersonInfo.regulatorname');
				$qf_list['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
				$qf_list['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
				$qf_list['fcreatetime'] = date('Y-m-d H:i:s');
				$qf_list['fbizname'] = '广告指导';
				$qf_list['fflowname'] = '指导签发';
				$qf_list['fregulatorid'] = session('regulatorpersonInfo.fregulatorid');
				$qf_list['fregulatorname'] =  session('regulatorpersonInfo.regulatorname');
				$qf_list['fstate'] = 1;
				$qf_res = M("tillegaladflow")->add($qf_list);
				if(!$qf_res){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'签发失败！！'));
				}
				/*更改违法广告表*/
				$arr['fmodifier'] = session('regulatorpersonInfo.fname');
				$arr['fmodifytime'] = date('Y-m-d H:i:s');
				$arr['fdisposestyle'] = 1;
				$arr['fstate'] = 1;
				$res = M("tillegalad")->where(array('fillegaladid' => $fillegaladid))->save($arr);

				if(!$res){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'指导失败！！！'));
				}
				$this->ajaxReturn(array('code'=>0,'msg'=>'指导成功！'));
				break;
			case 2:
				/*复核
				 * 新增流程表*/
				$model=M("tillegaladflow");
				$model->startTrans();//开启事务

				$flow_data = $model->where(array('fillegaladid' => $fillegaladid,'fflowname' => '广告指导','fstate' =>1))->find();//是否指导表已经有信息
				if($flow_data) {
					$save_list['fregulatorpersonid'] = session('regulatorpersonInfo.fid');//处理人ID
					$save_list['fregulatorpersonname'] = session('regulatorpersonInfo.fname');//处理人
					$save_list['ffinishtime'] = date('Y-m-d H:i:s');//完成时间
					$save_list['fstate'] = 2;
					$save_reg = M("tillegaladflow")->where(array('fillegaladflowid' => $flow_data['fillegaladflowid']))->save($save_list);
					if(!$save_reg){
						$model->rollback();//不成功，则回滚
						$this->ajaxReturn(array('code'=>-1,'msg'=>'申请复核失败！！'));
					}

				}
				$data['fillegaladid'] = $fillegaladid;
				$data['fcreateinfo'] = I('fdescribe'); //暂时存这里
				$data['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');
				$data['fcreateregualtorname'] =  session('regulatorpersonInfo.regulatorname');
				$data['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
				$data['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
				$data['fcreatetime'] = date('Y-m-d H:i:s');
				$data['fbizname'] = '广告复核';
				$data['fflowname'] = '复核任务';
				$data['fstate'] = 1;
				$tregulator= M("tregulator")->where(array('fid' =>session('regulatorpersonInfo.fregulatorid')))->find();
				if($tregulator['fpid']!= 0){
					$data['fregulatorid'] = $tregulator['fpid'];
					$up_tregulator= M("tregulator")->where(array('fid' =>$tregulator['fpid']))->find();
					$data['fregulatorname'] =  $up_tregulator['fname'];
					$reg = $model->add($data);
					if($reg){
						$model->commit();//成功则提交
					}else{
						$model->rollback();//不成功，则回滚
						$this->ajaxReturn(array('code'=>-1,'msg'=>'申请复核失败！'));
					}
				}else{
					$model->rollback();//不成功，则回滚
					$this->ajaxReturn(array('code'=>-1,'msg'=>'您已没有上级！！'));
				}
				if(!$reg){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'申请复核失败！'));
				}

				/*更改违法广告表*/
				$arr['fmodifier'] = session('regulatorpersonInfo.fname');
				$arr['fmodifytime'] = date('Y-m-d H:i:s');
				$arr['fdisposestyle'] = 3;
				$arr['fstate'] = 0;
				$res = M("tillegalad")->where(array('fillegaladid' => $fillegaladid))->save($arr);
				if(!$res){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'申请复核失败！！'));
				}
				$check_data['fillegaladid'] = $fillegaladid;
				$check_data['fapplyregulatorid'] = session('regulatorpersonInfo.fregulatorid');
				$check_data['fapplyregulator'] = session('regulatorpersonInfo.regulatorname');
				$check_data['fapply'] =  session('regulatorpersonInfo.fname');
				$check_data['fapplytime'] = date('Y-m-d H:i:s');
				$check_data['fapplyreason'] = I('fdescribe');
				$check_data['fcheckregulatorid'] = $tregulator['fpid'];
				$check_data['fcheckregulator'] = $up_tregulator['fname'];
				$check_data['fillegaltypecode1'] = $tillegalad['fillegaltypecode'];//复核前违法类型
				$check_data['fillegalcontent1'] = $tillegalad['fillegalcontent'];//复核前涉嫌违法内容
				$check_data['fexpressioncodes1'] = $tillegalad['fexpressioncodes'];//复核前违法表现代码
				$check_data['fexpressions1'] = $tillegalad['fexpressions'];//复核前违法表现
				$check_data['fconfirmations1'] = $tillegalad['fconfirmations'];//复核前认定依据
				$check_data['fpunishments1'] = $tillegalad['fpunishments'];//复核前处罚依据
				$check_data['fpunishmenttypes1'] = $tillegalad['fpunishmenttypes'];//复核前处罚种类及幅度
				$check_data['fstate'] = 0;
				$check_reg = M("tadcheck")->add($check_data);
				if(!$check_reg){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'申请复核失败！！'));
				}
				$this->ajaxReturn(array('code'=>0,'msg'=>'申请复核成功！'));
				break;
			case 3:
				//转线索

				$flow_data = M('tillegaladflow')->where(array('fillegaladid' => $fillegaladid,'fflowname' => '广告指导','fstate' =>1))->find();//是否指导表已经有信息
				if($flow_data){
					$save_list['fregulatorpersonid'] = session('regulatorpersonInfo.fid');//处理人ID
					$save_list['fregulatorpersonname'] = session('regulatorpersonInfo.fname');//处理人
					$save_list['ffinishtime'] = date('Y-m-d H:i:s');//完成时间
					$save_list['fstate'] = 2;
					$save_reg = M("tillegaladflow")->where(array('fillegaladflowid' => $flow_data['fillegaladflowid']))->save($save_list);
					if(!$save_reg){
						$this->ajaxReturn(array('code'=>-1,'msg'=>'广告指导失败！！'));
					}
				}
				/*转线索新增流程表*/
				$data['fillegaladid'] = $fillegaladid;
				$data['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');
				$data['fcreateregualtorname'] =  session('regulatorpersonInfo.regulatorname');
				$data['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
				$data['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
				$data['fcreatetime'] = date('Y-m-d H:i:s');
				$data['fbizname'] = '线索处理';
				$data['fflowname'] = '线索登记';
				$data['fregulatorid'] = session('regulatorpersonInfo.fregulatorid');
				$data['fregulatorname'] =  session('regulatorpersonInfo.regulatorname');
				$data['ffinishtime'] = date('Y-m-d H:i:s');
				$data['fstate'] = 1;
				$reg = M("tillegaladflow")->add($data);
				if(!$reg){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'转为线索失败！'));
				}
				/*更改违法广告表*/
				$arr['fmodifier'] = session('regulatorpersonInfo.fname');
				$arr['fmodifytime'] = date('Y-m-d H:i:s');
				$arr['fdisposestyle'] = 4;
				$arr['fstate'] = 0;
				$res = M("tillegalad")->where(array('fillegaladid' => $fillegaladid))->save($arr);
				if(!$res){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'转为线索失败！'));
				}
				$this->ajaxReturn(array('code'=>0,'msg'=>'转为线索成功！'));
				break;
		}
	}
	/*类型*/
	public function ad_guide_type()
	{
		if(I('guide_type')){
			$guide_type = I('guide_type');
			$ttemplate = M('ttemplate')->where(array('ftemplatetype' => $guide_type ))->find();//建议模板

			$ttemplate['re_fcontents']=html_entity_decode($ttemplate['fcontents']);

//			aa($ttemplate,1);
			$this->ajaxReturn(array('code'=>0,'msg'=>'请求成功','data'=>$ttemplate));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'指导类型不存在'));
		}
	}
	/*模板设置*/
	public function update_guide_type()
	{
		$data['ftemplatetype'] = I('guide_type');//指导类型
		$data['fcontents'] = I('content'); //描述
		$data['fcreatetime'] = date('Y-m-d H:i:s');
		$data['fstate'] = 1;
		$res = M('ttemplate')->where(array('ftemplatetype' => $data['ftemplatetype']))->find();//建议模板
		if($res){
//			aa($data,1);
			$reg= M('ttemplate')->where(array('ftemplatetype' => $data['ftemplatetype']))->save($data);
//			aa($reg,1);
		}else{
			$reg = M('ttemplate')->add($data);
		}
		if(!$reg){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'保存失败！'));
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'保存成功！'));

	}
}