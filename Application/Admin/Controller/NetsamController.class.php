<?php
namespace Admin\Controller;
use Think\Controller;
use Think\Page;
use OpenSearch\Client\OpenSearchClient;
use OpenSearch\Client\DocumentClient;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;
class NetsamController extends BaseController {
	

	

    public function index(){
		
		if(A('Admin/Authority','Model')->authority('200301') === 0){
			exit('没有权限');//验证是否有新增权限
		}	
		$getRegionTree = A('Common/Region','Model')->getRegionTree();
		//var_dump($getRegionTree);
		
		$getAdClassArr = A('Common/AdClass','Model')->getAdClassArr();
		//var_dump($getAdClassArr);
		
		
		$this->assign('getRegionTree',$getRegionTree);
		$this->assign('getAdClassArr',$getAdClassArr);
		
		$this->display();
	}
	
	
	/*应用数据管理(搜索)*/
	public function search_issue(){
		header('Access-Control-Allow-Origin:*');//允许跨域

		$p = I('p',1);//当前第几页
		$pp = I('pp',10);//每页显示多少记录
		
		if(strtotime(I('issuedate_min')) < strtotime('2017-07-01')) $_GET['issuedate_min'] = date('Y-m-d',time() - 86400*360);
		if(strtotime(I('issuedate_max')) < strtotime('2017-07-01')) $_GET['issuedate_max'] = date('Y-m-d');
		
		
		
		
		$client = A('Common/OpenSearch','Model')->client();//获得OpenSearchClient
		$searchClient = new SearchClient($client);

		$paramsResule = new SearchParamsBuilder();
		$paramsResule->setStart(($p*$pp) - $pp);//起始位置

		$paramsResule->setHits($pp);//返回数量

		//$paramsResule->setAppName(C('OPEN_SEARCH_CUSTOMER'));//设置应用名称
		$paramsResule->setAppName('hzsj_dmp_ad');//设置应用名称
		
		
		
		$where = array();
		
		if(I('fmedianame') != '') $where['fmedianame'] = I('fmedianame');//搜索媒介名称
		if(I('fadname') != '') $where['fadname'] = I('fadname');//搜索广告名称
		if(I('fadclasscode') != '') $where['fadclasscode'] = I('fadclasscode');//搜索广告类别
		if(I('fillegaltypecode') != '') $where['fillegaltypecode'] = I('fillegaltypecode');//搜索违法类别
		if(I('net_platform') != '') $where['issue_platform_type'] = I('net_platform');//搜索发布平台
		
		
		
		$where['fmediaclassid'] = 13;//搜索媒介类别
		if(I('fregionid') != ''){//如果有传入地区搜索条件
			if(I('this_region_id') == 'true'){
				$where['fregionid'] = I('fregionid');
			}else{
				$where['regionid_array'] = I('fregionid');
			}
			
		}
		
		$setQuery = A('Common/OpenSearch','Model')->setQuery($where);//组装搜索字句
		

		
		$paramsResule->setQuery($setQuery);//设置搜索
		
		
		$whereFilter = array();
		$whereFilter['fissuedate'] = array('between',array(strtotime(I('issuedate_min')),strtotime(I('issuedate_max'))));


		$setFilter = A('Common/OpenSearch','Model')->setFilter($whereFilter);//组装过滤条件


		
		$paramsResule->addSort('fissuedate', SearchParamsBuilder::SORT_DECREASE);//排序
		$paramsResule->setFilter($setFilter);//设置文档过滤条件
		$paramsResule->setFormat("json");//数据返回json格式
		
		
		
		
		

		
		$paramsResule->setFetchFields(array(
										'identify',//识别码（哈希）

										'fmediaclassid',
										
										'fmediaid', //媒介id
										'fmedianame',//媒介名称
										'issue_platform_type',//发布平台类型
										
										'fregionid', //地区id
										'fadname',//广告名称
										'fadowner',//广告主
										
										'fbrand', //品牌
										'fspokesman', //代言人
										'fadclasscode', //广告类别id

										'fissuedate', //发布日期

										'fillegaltypecode', //违法类型
										'landing_img',
										'target_url',
										'sam_source_path',
										
										
									));//设置需返回哪些字段
		
		
		$result = json_decode($searchClient->execute($paramsResule->build())->result,true);//
		
		$issueList0 = $result['result']['items'];
		
		$issueList = array();
		
		
		if(!S('adClass')){
			$cacheAdClass = M('tadclass')->cache(true,600)->field('fcode,fadclass,ffullname')->select();
			$adClass = array();
			foreach($cacheAdClass as $cacheAdClassV){
				$adClass[$cacheAdClassV['fcode']] = $cacheAdClassV;
			}
			S('adClass',$adClass,86400);
			
		}else{
			$adClass = S('adClass');
			
		}
		
		
		//var_dump($adClass);

		foreach($issueList0 as $issu){

			$issu['issuedate'] = date('Y-m-d',$issu['fissuedate']);
			
			$issu['thumbnail'] = $issu['sam_source_path'];
			if($issu['issue_platform_type'] == 9){
				$issu['thumbnail'] = str_replace('mmbiz.qpic.cn','hz-weixin-img.oss-cn-hangzhou.aliyuncs.com',$issu['sam_source_path']);
			}
			 
			$adclasscode_arr = explode('	',$issu['fadclasscode']);
			
			$issu['adclasscode'] = $adClass[$adclasscode_arr[0]]['ffullname'];
			
			$issueList[] = $issu;
		}
		
		
		
		
		$paramsMediaCount = new SearchParamsBuilder();

		$paramsMediaCount->setAppName('hzsj_dmp_ad');//设置应用名称

		$paramsMediaCount->setQuery($setQuery);//设置搜索
		$paramsMediaCount->addDistinct(array('key' => 'fmediaid', 'distTimes' => 1, 'distCount' => 1, 'reserved' => false,'update_total_hit'=>true));
		$paramsMediaCount->setKvPairs("duniqfield:fmediaid");
		$paramsMediaCount->setHits(0);//返回数量

		$paramsMediaCount->addSort('fissuedate', SearchParamsBuilder::SORT_DECREASE);//排序
		$paramsMediaCount->setFilter($setFilter);//设置文档过滤条件
		$paramsMediaCount->setFormat("json");//数据返回json格式
		

		$retMediaCount = json_decode($searchClient->execute($paramsMediaCount->build())->result,true);//
		

		

		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','mediaCount'=>$retMediaCount['result']['total'],'total'=>$result['result']['total'],'issueList'=>$issueList));//返回数据
		
		
		
		
		
		
		
	}
	
	/*发布数据详情*/
	public function issue_info(){
		
		$identify = I('identify');
		
		header('Access-Control-Allow-Origin:*');//允许跨域


	
		
		
		
		$client = A('Common/OpenSearch','Model')->client();//获得OpenSearchClient
		$searchClient = new SearchClient($client);

		$params = new SearchParamsBuilder();


		$params->setHits(1);//返回数量

		//$params->setAppName(C('OPEN_SEARCH_CUSTOMER'));//设置应用名称
		$params->setAppName('hzsj_dmp_ad');//设置应用名称
		
		
		$setQuery = A('Common/OpenSearch','Model')->setQuery(array('id'=>$identify));//组装搜索字句
		

		
		$params->setQuery($setQuery);//设置搜索

		$params->setFormat("json");//数据返回json格式
		
		$params->setFetchFields(array(
										'identify',		
										'fmediaclassid',	
										'fsampleid',	
										'fmediaid',	
										'fmedianame',			
										'fregionid',	
										'fadname',			
										'fadowner',		
										'adowner_regionid',		
										'fbrand',
										'fspokesman',
										'fadclasscode',
										'fstarttime',	
										'fendtime',
										'fissuedate',
										'flength',
										'fillegaltypecode',	
										'fexpressioncodes',
										'fillegalcontent',
										'sam_source_path',
										'fprice',
										'issue_platform_type',
										'landing_img',
										'issue_page_url',
										'target_url',		
										'operator_name',
										'operator_id',
										'issue_ad_type',
										'search_other',	
										'fmediaownername',
										'fmediaownerid',
										'regionid_array',
										'last_update_time',
										
	
									));//设置需返回哪些字段
		
		
		$ret = $searchClient->execute($params->build());//执行查询并返回信息
		

		$result = json_decode($ret->result,true);//
		
		$issueInfo0 = $result['result']['items'][0];
		$issueInfo0['issuedate'] = date('Y-m-d',$issueInfo0['fissuedate']);
		$issueInfo0['adclasscode'] = M('tadclass')->cache(true,3600)->where(array('fcode'=>explode('	',$issueInfo0['fadclasscode'])[0]))->getField('ffullname');
		
		$issueInfo0['fexpressions'] = M('tillegal')
												->where(array('fcode'=>array('in',explode(';',$issueInfo0['fexpressioncodes']))))
												->getField('fexpression',true);
		$issueInfo0['fexpressions']	= implode(";\n",$issueInfo0['fexpressions']);									
		
		$issueInfo = array();
		
		
		foreach($issueInfo0 as $field => $value){
			if($value) $issueInfo[$field] = $value;
			
		}
		
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','issueInfo'=>$issueInfo));//返回数据
		
		
		
		
		
		
		
	}
	
	
	
	
	
	
	public function sample_edit(){
		
		if(A('Admin/Authority','Model')->authority('200301') === 0){
			exit('没有权限');//验证是否有新增权限
		}	
		$major_key = I('major_key');//获取样本ID


		$NetAdDetails = M('tnetissue')
										->field('
													tnetissue.*,
													tadowner.fname as adowner_name,
													tadclass.ffullname,
													tillegaltype.fillegaltype,
													tmedia.fmedianame,
													tmedia.fmediacode,
													(select nickname from ad_input_user where wx_id = tnetissue.finputuser) as finputusername
													')
										->join('tadclass on tadclass.fcode = tnetissue.fadclasscode')
										->join('tmedia on tmedia.fid = tnetissue.fmediaid')
										->join('tadowner on tadowner.fid = tnetissue.fadowner')
										->join('tillegaltype on tillegaltype.fcode = tnetissue.fillegaltypecode')
										->where(array('tnetissue.major_key'=>$major_key))
										->find();//查询样本详情
		$NetAdDetails['adowner_name'] = M('tdomain')->where(array('fdomain'=>$NetAdDetails['net_advertiser']))->getField('fname');
		$NetAdDetails['fmedianame2'] = M('tdomain')->where(array('fdomain'=>$NetAdDetails['fmediacode']))->getField('fname');
		
		//var_dump(M('tdomain')->getLastSql());


		$NetAdDetails['article_content'] = str_replace(array('mmbiz.qpic.cn','width: auto !important;'),array('hz-weixin-img.oss-cn-hangzhou.aliyuncs.com',''),$NetAdDetails['article_content']);
		
		
		$NetAdillegal = M('tillegal')
												->field('fcode as fillegalcode,fexpression')
												->where(array('fcode'=>array('in',explode(',',$NetAdDetails['fexpressioncodes']))))->select();

		
		
		
		$this->assign('NetAdDetails',$NetAdDetails);
		$this->assign('NetAdillegal',$NetAdillegal);
		$illegaltypeList = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
		$this->assign('illegaltypeList',$illegaltypeList);//违法类型数据列表
		$this->assign('IllegalList',$IllegalList);//违法表现数据列表
		$this->display();
	}
	
	
	/*信息录入*/
	public function edit_sample(){
		if(A('Admin/Authority','Model')->authority('200302') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'没有权限'));//验证是否有新增权限
		}
		$major_key = intval(I('major_key'));//获取样本ID
	
		$netAdInfo = M('tnetissue')->where(array('major_key'=>$major_key))->find();
		
		
		
		
		$fadname = I('fadname');//广告名
		$fbrand = I('fbrand');// 广告品牌
		$adclass_code = I('adclass_code');//广告分类code
		$adowner_name = I('adowner_name');//广告主
		$fspokesman = I('fspokesman');//代言人

		$fadmanuno = I('fadmanuno');//广告中标识的生产批准文号
		$fmanuno = I('fmanuno');//生产批准文号
		$fadapprno = I('fadapprno');//广告中标识的广告批准文号
		$fapprno = I('fapprno');//广告批准文号
		$fadent = I('fadent');//广告中标识的生产企业（证件持有人）名称
		$fent = I('fent');//生产企业（证件持有人）名称
		$fentzone = I('fentzone');//生产企业（证件持有人）所在地区
		
		$fmedianame2 = I('fmedianame2');
		
		
		
		$a_e_data['fadname'] = $fadname;//广告名称
		$a_e_data['fbrand'] = $fbrand;//品牌
		$a_e_data['fspokesman'] = $fspokesman;//代言人
		$a_e_data['fadclasscode'] = $adclass_code;//代言人
		$a_e_data['fadowner'] = A('Common/Adowner','Model')->get_ad_owner_id($adowner_name,$wxInfo['nickname']);

		$a_e_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
		$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		
		$a_e_data['fadmanuno'] = $fadmanuno;//广告中标识的生产批准文号
		$a_e_data['fmanuno'] = $fmanuno;//生产批准文号
		$a_e_data['fadapprno'] = $fadapprno;//广告中标识的广告批准文号
		$a_e_data['fapprno'] = $fapprno;//广告批准文号
		$a_e_data['fadent'] = $fadent;//广告中标识的生产企业（证件持有人）名称
		$a_e_data['fent'] = $fent;//生产企业（证件持有人）名称
		$a_e_data['fentzone'] = $fentzone;//生产企业（证件持有人）所在地区
		if(M('tadclass')->cache(true,86400)->where(array('fcode'=>$adclass_code))->count() == 0){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'广告类别错误'));
		} 

		if($fadname == ''){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'请输入广告名称'));
		}

		$rr = M('tnetissue')->where(array('major_key'=>$major_key))->save($a_e_data);//修改数据		
		//var_dump(M('tnetissue')->getLastSql());
		
		
		if(M('tdomain')->where(array('fdomain'=>$netAdInfo['net_advertiser']))->count() == 0){
			M('tdomain')->add(array('fname'=>$adowner_name,'fdomain'=>$netAdInfo['net_advertiser']));
		}else{
			M('tdomain')->where(array('fdomain'=>$netAdInfo['net_advertiser']))->save(array('fname'=>$adowner_name));
		}
		
		
		
		if($fmedianame2 != ''){
			$mediaInfo = M('tmedia')->cache(true,5)->where(array('fid'=>$netAdInfo['fmediaid']))->find();//查询媒介信息
			
			if(M('tdomain')->where(array('fdomain'=>$mediaInfo['fmediacode']))->count() == 0){
				M('tdomain')->add(array('fname'=>$fmedianame2,'fdomain'=>$mediaInfo['fmediacode']));
			}else{
				M('tdomain')->where(array('fdomain'=>$mediaInfo['fmediacode']))->save(array('fname'=>$fmedianame2));
			}
			
			if($mediaInfo['fmedianame'] !=  $fmedianame2){
				$e_media_state = M('tmedia')->where(array('fmediacode'=>$mediaInfo['fmediacode']))->save(array('fmedianame'=>$fmedianame2));
			}
		}

		if($rr > 0){
			
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功'));
		}else{

			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败,原因未知d'));
		}

	}
	
	/*编辑电视广告样本违法*/
	public function edit_sample_illegal(){
		if(A('Admin/Authority','Model')->authority('200302') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'没有权限'));//验证是否有新增权限
		}
		$major_key = intval(I('major_key'));//获取样本ID


		$fillegalcontent = I('fillegalcontent');//违法内容
		$fexpressioncodes = I('fexpressioncodes');//违法代码
		$fillegaltypecode = I('fillegaltype');//违法类型

		if(intval($fillegaltypecode) > 0 && ($fillegalcontent == '' || $fexpressioncodes == '')){//选择了违法程度，没有填写违法内容或者没有选择违法表现
			$this->ajaxReturn(array('code'=>-1,'msg'=>'选择错误,没有填写违法内容或没有选择违法表现'));
		}
		
		if(intval($fillegaltypecode) == 0 && ($fillegalcontent != '' || $fexpressioncodes != '')){//选择了不违法，但是填写了违法内容或者选择了违法表现
			$this->ajaxReturn(array('code'=>-1,'msg'=>'选择错误,不违法不允许填写违法内容或选择违法表现'));
		}
		

		$illegal['fillegalcontent'] = $fillegalcontent;//违法内容
		$illegal['fexpressioncodes'] = $fexpressioncodes;//违法代码
		

		$illegal['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
		$illegal['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$illegal['fillegaltypecode'] = $fillegaltypecode;//违法类型
		
		
		$rr_illegal = M('tnetissue')->where(array('major_key'=>$major_key))->save($illegal);//修改数据

		if($rr_illegal > 0){
			
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功','next_url'=>strval($next_url)));
		}else{

			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败,原因未知','next_url'=>''));
		}

	}
	
	/*退回重新录入*/
	public function back_sample(){
		if(A('Admin/Authority','Model')->authority('200302') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'没有权限'));//验证是否有新增权限
		}
		$major_key = intval(I('major_key'));//广告样本ID
		$samInfo = M('tnetissue')->field('finputuser,finspectuser')->where(array('major_key'=>$major_key))->find();
		$where = array();
		$where['finputuser'] = $samInfo['finputuser'];
		$where['major_key'] = $major_key;
		
		$rr = M('tnetissue')->where($where)->save(array('finputstate'=>1,'finputuser'=>0));
		if($rr > 0){
			A('InputPc/Task','Model')->change_task_count($samInfo['finputuser'],'net_input',-1);
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败'));
		}
		
	}
	
	/*退回重新初审*/
	public function back_sample_illegal(){
		if(A('Admin/Authority','Model')->authority('200302') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'没有权限'));//验证是否有新增权限
		}
		
		$major_key = intval(I('major_key'));//广告样本ID
		$samInfo = M('tnetissue')->field('finputuser,finspectuser')->where(array('major_key'=>$major_key))->find();
		$where = array();
		$where['finspectuser'] = $samInfo['finspectuser'];
		$where['major_key'] = $major_key;
		
		$rr = M('tnetissue')->where($where)->save(array('finspectstate'=>1,'finspectuser'=>0));
		if($rr > 0){
			A('InputPc/Task','Model')->change_task_count($samInfo['finspectuser'],'net_inspect',-1);
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败'));
		}
		
	}
	
	
	/*标记为非广告*/
	public function is_not_ad(){
		if(A('Admin/Authority','Model')->authority('200302') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'没有权限'));//验证是否有新增权限
		}
		$major_key = intval(I('major_key'));//广告样本ID
		
		$where = array();
		$where['major_key'] = $major_key;
		$where['isad'] = 1;

		
		$e_data = array();
		$e_data['isad'] = 0;
		$e_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
		$e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		
		$e_state = M('tnetissue')->where($where)->save($e_data);
		
		if($e_state){
			$this->ajaxReturn(array('code'=>0,'msg'=>'标记成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'标记失败'));
		}
		
	}


}