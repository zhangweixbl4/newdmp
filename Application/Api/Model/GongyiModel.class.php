<?php
namespace Api\Model;
use Think\Exception;
use Think\Log;


class GongyiModel{
	
	
	public $mubiaoDb = 'mysql://webapp:Webapp123@rm-bp11ke7uus98107nxzo.mysql.rds.aliyuncs.com:3306/datav';
	
	
	
	
	
	public function gyzl(){
		$gyzlSql = "select fmediaclassid as mjlx,count(*) as count 
							from hzsj_dmp_ad 
							where left(fadclasscode,4) = '2202' and fissuedate BETWEEN ".strtotime('-11 month',strtotime(date('Y-m-01',time())))." and ". time() ." 
							GROUP BY fmediaclassid;"; ## -- 公益广告总量
		$dataList = M('','',C('FENXI_DB'))->query($gyzlSql);
		foreach($dataList as $data){

			$mjlx = '';
			if($data['mjlx'] == 1){
				$mjlx = '电视';
			}elseif($data['mjlx'] == 2){
				$mjlx = '广播';
			}elseif($data['mjlx'] == 3){
				$mjlx = '报纸';
			}elseif($data['mjlx'] == 13){
				$mjlx = '互联网';
			}
			
			if(M('gy_mjsl','',$this->mubiaoDb)->where(array('mjlx'=>$mjlx))->count() == 0){
				M('gy_mjsl','',$this->mubiaoDb)->add(array('mjlx'=>$mjlx,'fbsl'=>$data['count']));
			}else{
				M('gy_mjsl','',$this->mubiaoDb)->where(array('mjlx'=>$mjlx))->save(array('fbsl'=>$data['count']));
			}
		}
	}
	
	
	public function mtsl(){
	
		$mtslSql = "select 
								left(fmediaclassid,2) media_class,count(*) as count 
								from tmedia 
								where priority >= 0 and left(fmediaclassid,2) in('01','02','03','13') 
								GROUP BY left(fmediaclassid,2) ;"; ## -- 采集媒体量
	
		$dataList = M('','',C('FENXI_DB'))->query($mtslSql);
		foreach($dataList as $data){

			$mjlx = '';
			if($data['media_class'] == '01'){
				$mjlx = '电视';
			}elseif($data['media_class'] == '02'){
				$mjlx = '广播';
			}elseif($data['media_class'] == '03'){
				$mjlx = '报纸';
			}elseif($data['media_class'] == '13'){
				$mjlx = '互联网';
			}
			
			if(M('gy_mjsl','',$this->mubiaoDb)->where(array('mjlx'=>$mjlx))->count() == 0){
				M('gy_mjsl','',$this->mubiaoDb)->add(array('mjlx'=>$mjlx,'mjsl'=>$data['count']));
			}else{
				M('gy_mjsl','',$this->mubiaoDb)->where(array('mjlx'=>$mjlx))->save(array('mjsl'=>$data['count']));
			}
		}
	
	}
	
	
	public function yuefen(){
		
		
		$yuefenSql = "
					select DATE_FORMAT(FROM_UNIXTIME(fissuedate),'%y-%m') as yuefen,count(*) tc,sum(cast(flength as signed)) sc
					from hzsj_dmp_ad 
					where left(fadclasscode,4) = '2202' and fissuedate BETWEEN ".strtotime('-11 month',strtotime(date('Y-m-01',time())))." and ". time() ."  
					GROUP BY DATE_FORMAT(FROM_UNIXTIME(fissuedate),'%y-%m')  ORDER BY yuefen;"; ## -- 按照月份统计


		$dataList = M('','',C('FENXI_DB'))->query($yuefenSql);
		$yuefenList = [];
		foreach($dataList as $data){
			$yuefenList[] = $data['yuefen'];
			M('gy_yuefen','',$this->mubiaoDb)->add(array('yuefen'=>$data['yuefen'],'fbsl'=>$data['tc'],'fbsc'=>$data['sc']),array(),true);
		}
		
		
		M('gy_yuefen','',$this->mubiaoDb)->where(array('yuefen'=>array('not in',$yuefenList)))->delete();
	}
	
	
	public function ztongji(){
		
		$ztongjiSql = "select count(*) as tc ,count(DISTINCT fsampleid) as ts,sum(cast(flength as signed)) as sc  
						from hzsj_dmp_ad 
						where left(fadclasscode,4) = '2202' and fissuedate BETWEEN ".strtotime('-11 month',strtotime(date('Y-m-01',time())))." and ". time() .";"; ## -- 条次、条数、时长

		$dataList = M('','',C('FENXI_DB'))->query($ztongjiSql);
		
		M('gy_tongji','',$this->mubiaoDb)->add(array('fid'=>1,'tc'=>$dataList[0]['tc'],'ts'=>$dataList[0]['ts'],'sc'=>$dataList[0]['sc']),array(),true);
		M('gy_tongji','',$this->mubiaoDb)->where(array('fid'=>array('neq',1)))->delete();
		
	}
	
	public function lbtongji(){
	
		$lbtongjiSql = "
						select substring_index( fadclasscode,'|',1) as lbdm
								,(select fadclass from tadclass where fcode = substring_index( hzsj_dmp_ad.fadclasscode,'|',1)) as lbmc
								,count(*) tc
						from hzsj_dmp_ad 
						where left(fadclasscode,4) = '2202'  and fissuedate BETWEEN ".strtotime('-11 month',strtotime(date('Y-m-01',time())))." and ". time() ."
						GROUP BY substring_index( fadclasscode,'|',1),(select fadclass from tadclass where fcode = substring_index( hzsj_dmp_ad.fadclasscode,'|',1));" ; ## -- 类别分布
		
		$dataList = M('','',C('FENXI_DB'))->query($lbtongjiSql);
		$lbList = array();
		foreach($dataList as $data){
			if($data['lbdm'] == '220210'){
				$data['lbdm'] = '220203';
				$data['lbmc'] = '环境保护';
			}
			
			if($data['lbdm'] == '2202'){
				$data['lbdm'] = '220202';
				$data['lbmc'] = '时政宣传';
			}
			
			
			
			$lbList[$data['lbdm']]['lb_mingc'] = $data['lbmc'];
			$lbList[$data['lbdm']]['fbsl'] += $data['tc'];
			
		}
		$lbCodeS = [];
		foreach($lbList as $lbCode => $lb){
			$lbCodeS[] = $lbCode;
			M('gy_lb','',$this->mubiaoDb)->add(array('lb_bianma'=>$lbCode,'lb_mingc'=>$lb['lb_mingc'],'fbsl'=>$lb['fbsl']),array(),true);

		}
		M('gy_lb','',$this->mubiaoDb)->where(array('lb_bianma'=>array('not in',$lbCodeS)))->delete();
		
		
		
	}	
	
	public function diqu(){
		
		$diquSql = "
					select left(fregionid,2) as diqu_code
							,(select fname from tregion where fid = concat(left(hzsj_dmp_ad.fregionid,2),'0000')) as diqu_mc
							,count(*) as tc
					from hzsj_dmp_ad 
					where left(fadclasscode,4) = '2202'  and fissuedate BETWEEN ".strtotime('-11 month',strtotime(date('Y-m-01',time())))." and ". time() ."

					GROUP BY left(fregionid,2),(select fname from tregion where fid = concat(left(hzsj_dmp_ad.fregionid,2),'0000'));"; ## -- 地区
				
		$dataList = M('','',C('FENXI_DB'))->query($diquSql);
		$diquList = [];
		foreach($dataList as $data){
			$diquList[] = $data['diqu_code'];
			M('gy_dq','',$this->mubiaoDb)->add(array('dq_bianma'=>$data['diqu_code'],'dq_mingcheng'=>$data['diqu_mc'],'fbsl'=>$data['tc']),array(),true);
		}
		
		
		M('gy_dq','',$this->mubiaoDb)->where(array('dq_bianma'=>array('not in',$diquList)))->delete();
	}
	
	
	
	public function mj_fbsl(){
		
		
		$mj_fbslSql = "
						select fmedianame,count(*) as fbsl
							from hzsj_dmp_ad 
							where left(fadclasscode,4) = '2202'  and fissuedate BETWEEN ".strtotime('-11 month',strtotime(date('Y-m-01',time())))." and ". time() ."
							GROUP BY fmedianame
							ORDER BY fbsl desc 
							limit 100;"; ##


		$dataList = M('','',C('FENXI_DB'))->query($mj_fbslSql);
		$mjList = [];
		foreach($dataList as $data){
			$mjList[] = $data['fmedianame'];
			M('gy_mj_fbsl','',$this->mubiaoDb)->add(array('fmedianame'=>$data['fmedianame'],'fbsl'=>$data['fbsl']),array(),true);
		}
		
		
		M('gy_mj_fbsl','',$this->mubiaoDb)->where(array('fmedianame'=>array('not in',$mjList)))->delete();
	}
	
	public function mjjg_fbsl(){
		
		
		$mj_fbslSql = "
						select fmediaownername,count(*) as fbsl
						from hzsj_dmp_ad 
						where left(fadclasscode,4) = '2202'  and fissuedate BETWEEN ".strtotime('-11 month',strtotime(date('Y-m-01',time())))." and ". time() ."
						GROUP BY fmediaownername
						ORDER BY fbsl desc 
						limit 100; "; ##


		$dataList = M('','',C('FENXI_DB'))->query($mj_fbslSql);
		$mjList = [];
		foreach($dataList as $data){
			$mjList[] = $data['fmediaownername'];
			M('gy_mjjg_fbsl','',$this->mubiaoDb)->add(array('fmediaownername'=>$data['fmediaownername'],'fbsl'=>$data['fbsl']),array(),true);
		}
		
		
		M('gy_mjjg_fbsl','',$this->mubiaoDb)->where(array('fmediaownername'=>array('not in',$mjList)))->delete();
	}
	
	
	
	
	
	
	
	
	
	
}