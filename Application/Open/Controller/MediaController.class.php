<?php
namespace Open\Controller;
use Think\Controller;

class MediaController extends BaseController {
	
	/*获取媒介发布记录列表*/
    public function get_issue_list(){
		
		$post_data = file_get_contents('php://input');//获取post数据
		$post_data = json_decode($post_data,true);//把json转化成数组
		
		if($post_data['start_time'] == '' || $post_data['end_time'] == ''){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'开始时间或结束时间错误'));
		}
		
		
		$mediaid = $post_data['mediaid'];//媒体ID
		$start_time = date('Y-m-d H:i:s',strtotime($post_data['start_time'])-15);//开始时间
		$end_time = date('Y-m-d H:i:s',strtotime($post_data['end_time'])+15);//结束时间
		
		$mediaInfo = M('tmedia')
								->field('fid,left(fmediaclassid,2) as media_class,fmedianame')
								->where(array('fid'=>$mediaid))
								->find();//查询媒介信息

		if(intval($mediaInfo['fid']) == 0){
			$this->ajaxReturn(array('code'=>10027,'msg'=>'查询不到这个媒体'));
		}
		

		$where = array();
		$issueList = array();
		if($mediaInfo['media_class'] == '01'){//电视
			
			$where['ttvissue.fmediaid'] = $mediaInfo['fid'];
			$where['ttvissue.fstarttime'] = array('lt',$end_time);//开始时间小于查询的结束时间
			$where['ttvissue.fendtime'] = array('gt',$start_time);//结束时间大于查询的开始时间
			$issueList = M('ttvissue')
									->field('ftvissueid as issueid,ftvsampleid as sampleid,ftvmodelid as modelid,ttvissue.fcreator,
												ttvissue.fstarttime as starttime,ttvissue.fendtime as endtime,faudiosimilar as audiosimilar,
												fvideosimilar as videosimilar,tad.fadname as adname,ttvsample.fversion as version,ttvsample.fspokesman as spokesman,
												ttvissue.fu0 as u0,ttvissue.fissample as issample,ttvissue.fduration as duration,
												tad.fbrand as brand,tad.fadclasscode as adclass_code,tadclass.fadclass as adclass,
												tadclass.ffullname as adclass_fullname,tadowner.fname as adowner_name,
												ttvsample.fapprovalunit as approvalunit,ttvsample.fapprovalid as approvalid,ttvsample.fillegaltypecode as illegaltypecode
											')
									->join('ttvsample on ttvsample.fid = ttvissue.ftvsampleid')
									->join('tad on tad.fadid = ttvsample.fadid')
									->join('tadowner on tadowner.fid = tad.fadowner')
									->join('tadclass on tadclass.fcode = tad.fadclasscode')
									->where($where)->select();
					
		}elseif($mediaInfo['media_class'] == '02'){//广播
			$where['tbcissue.fmediaid'] = $mediaInfo['fid'];
			$where['tbcissue.fstarttime'] = array('lt',$end_time);//开始时间小于查询的结束时间
			$where['tbcissue.fendtime'] = array('gt',$start_time);//结束时间大于查询的开始时间
			$issueList = M('tbcissue')
									->field('fbcissueid as issueid,fbcsampleid as sampleid,fbcmodelid as modelid,tbcissue.fcreator,
												tbcissue.fstarttime as starttime,tbcissue.fendtime as endtime,faudiosimilar as audiosimilar,
												fvideosimilar as videosimilar,tad.fadname as adname,tbcsample.fversion as version,tbcsample.fspokesman as spokesman,
												tbcissue.fu0 as u0,tbcissue.fissample as issample,tbcissue.fduration as duration,
												tad.fbrand as brand,tad.fadclasscode as adclass_code,tadclass.fadclass as adclass,
												tadclass.ffullname as adclass_fullname,tadowner.fname as adowner_name,
												tbcsample.fapprovalunit as approvalunit,tbcsample.fapprovalid as approvalid,tbcsample.fillegaltypecode as illegaltypecode
											')
									->join('tbcsample on tbcsample.fid = tbcissue.fbcsampleid')
									->join('tad on tad.fadid = tbcsample.fadid')
									->join('tadowner on tadowner.fid = tad.fadowner')
									->join('tadclass on tadclass.fcode = tad.fadclasscode')
									->where($where)->select();
		}else{//其它情况
			$this->ajaxReturn(array('code'=>10027,'msg'=>'这个媒体既不是电视也不是广播'));
		}
		
		
		
		
		
		if(count($issueList) == 0) $issueList = array();
		if( $post_data['has_closing_time'] == 1){
			$naList = $this->get_na_time($mediaInfo['fid'],strtotime($post_data['start_time']),strtotime($post_data['end_time']));
			//var_dump($naList);
			
			foreach($naList as $na){
				$issueList[] = array(
									'issueid'=> '0',
									'sampleid'=> '0',
									'modelid'=> '0',
									'starttime'=> date('Y-m-d H:i:s',$na['start']),
									'endtime'=> date('Y-m-d H:i:s',$na['end']),
									'audiosimilar'=> '0',
									'videosimilar'=> '0',
									'adname'=> '闭台时间段',
									'version'=> '',
									'spokesman'=> '',
									'u0'=> strval(($na['start'] - strtotime(date('Y-m-d',$na['start']))) * 1000),
									'issample'=> '0',
									'duration'=> strval(($na['end']-$na['start']) * 1000),
									'brand'=> '闭台时间段',
									'adclass_code'=> '2301',
									'adclass'=> '其它',
									'adclass_fullname'=> '其它类>其它',
									'adowner_name'=> '无',
									'approvalunit'=> '',
									'approvalid'=> '',
									'illegaltypecode'=> '0',
									'fcreator'=>''
									
										);
			} 
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'','issueCount'=>count($issueList),'issueList'=>$issueList));
		
	}
	
	/*修改发布记录*/
	public function edit_issue_time(){
		
		$post_data = file_get_contents('php://input');//获取post数据
		$post_data = json_decode($post_data,true);//把json转化成数组
		
		
		
		$mediaInfo = M('tmedia')->field('fid,left(fmediaclassid,2) as media_class')->where(array('fid'=>$post_data['media_id']))->find();//查询媒介信息
		if(intval($mediaInfo['fid']) == 0){//判断媒体是否存在
			$this->ajaxReturn(array('code'=>10027,'msg'=>'查询不到这个媒体'));
		}
		
		
		$fstarttime = $post_data['fstarttime'];//新的开始时间
		$fendtime = $post_data['fendtime'];//新的结束时间
		
		if((strtotime($fendtime) - strtotime($fstarttime)) < 3){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'不能小于3秒'));
		} 
		
		if((strtotime($fendtime) - strtotime($fstarttime)) > 86400){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'不能大于86400秒'));
		} 
		
		
		
		$fissuedate = date('Y-m-d',strtotime($fstarttime));//新的发布日期
		$fu0 = $post_data['fu0'];//新的
		$fduration = $post_data['fduration'];//新的
		
		if(abs(((strtotime($fstarttime) - strtotime($fissuedate)) * 1000) - $fu0) > 1000 ){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'FU0错误'));
		}
		
		if(abs(((strtotime($fendtime) - strtotime($fstarttime)) * 1000) - $fduration) > 1000 ){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'DURATION错误'));
		}
		
		
		if($mediaInfo['media_class'] == '01'){//电视
			$media_class_tab = 'tv';
		}elseif($mediaInfo['media_class'] == '02'){//广播
			$media_class_tab = 'bc';
		}
		

		$issueInfo = M('t'.$media_class_tab.'issue')->field('fissuedate,fstarttime,fendtime,fregionid')->where(array('f'.$media_class_tab.'issueid'=>$post_data['issueid']))->find();//查询旧的发布记录
		
		if(date('Y-m-d',strtotime($issueInfo['fissuedate'])) != $fissuedate){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'不允许跨天修改'));
		}
		
		if(!$issueInfo){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'没有这条数据'));
		}
		
		
		$s_issue_table = 't'.$media_class_tab.'issue_'.date('Ym',strtotime($issueInfo['fissuedate'])).'_'.substr($issueInfo['fregionid'],0,2);//分表的名称
		
		
		try{//
			$s_del_state = M($s_issue_table)->where(array(
														'fmediaid'=>$post_data['media_id'],
														'fissuddate'=>strtotime($issueInfo['fissuedate']),
														'fstarttime'=>strtotime($issueInfo['fstarttime']),
														'fendtime'=>strtotime($issueInfo['fendtime']),
														
														))
										->delete();
		}catch( \Exception $error) { 
			$s_del_state = 0;
		} 
		
		
		$l_edit_state = M('t'.$media_class_tab.'issue')
										->where(array('f'.$media_class_tab.'issueid'=>$post_data['issueid']))
										->save(array(
														'fissuddate'=>$fissuedate,
														'fstarttime'=>$fstarttime,
														'fendtime'=>$fendtime,
														'fu0'=>$fu0,
														'fduration'=>$fduration,
														'flength'=>strtotime($fendtime) - strtotime($fstarttime),
														'f'.$media_class_tab.'modelid'=>0,
													));	

		$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		
	}
	
	
	
	
	public function get_media_list(){
		$post_data = file_get_contents('php://input');//获取post数据
		$post_data = json_decode($post_data,true);//把json转化成数组
		
		
		$media_name = $post_data['media_name'];//获取媒介名称
		$fmediaclassid = $post_data['fmediaclassid'];//媒介类型
		$select_count = intval($post_data['select_count']);
		if($select_count <= 0 ) $select_count = 10;
		if($select_count >= 200 ) $select_count = 200;
		
		$where = array();
		$where['fstate'] = array('neq',-1);
		$where['fmedianame'] = array('like','%'.$media_name.'%');
		if($fmediaclassid != ''){
			$where['left(fmediaclassid,2)'] = $fmediaclassid;//媒介类型
		}
		$mediaList = M('tmedia')->field('fid as media_id,fmedianame as media_name')->where($where)->order('media_id')->limit($select_count)->select();//查询媒介列表

		$this->ajaxReturn(array('code'=>0,'msg'=>'','media_count'=>count($mediaList),'mediaList'=>$mediaList));
		
	}
	
	/*接收媒介上传ts*/
	public function media_drift(){
		$post_data = file_get_contents('php://input');//获取post数据
		//file_put_contents('LOG/A',$post_data);
		$post_data = json_decode($post_data,true);//把json转化成数组
		
		$a_data = array();
		$a_data['media_id'] = intval($post_data['media_id']);//媒介ID
		$a_data['start_time'] = intval($post_data['start_time']);//开始时间戳，毫秒级别
		$a_data['end_time'] = intval($post_data['end_time']);//结束时间戳，毫秒级别
		$a_data['url'] = $post_data['url'];//流地址
		$a_data['length'] = intval($post_data['length']);//流长度，单位毫秒
		if(S(md5(json_encode($a_data))) == 1) $this->ajaxReturn(array('code'=>-1,'msg'=>'请勿重复提交'));
		S(md5(json_encode($a_data)),1,360);
		$a_data['create_time'] = date('Y-m-d H:i:s');//创建时间
		
		
		$mediaInfo = M('tmedia')->field('left(fmediaclassid,2) as media_class')->where(array('fid'=>$a_data['media_id']))->find();
		
		if($mediaInfo['media_class'] != '01' && $mediaInfo['media_class'] != '02'){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介不存在'));
		}
		if($a_data['url'] == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'流地址不能为空'));
		
		$drift_id = M('media_drift')->add($a_data);
		if($drift_id > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>''));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>''));
		}
		
	}
	
	
	/*提交媒介异常*/
	/*添加媒介异常类型*/
	public function add_media_abnormal(){
			
			
		$post_data = file_get_contents('php://input');//获取post数据
		//file_put_contents('LOG/A',$post_data);
		$post_data = json_decode($post_data,true);//把json转化成数组
		
		
		$media_id = $post_data['media_id'];//媒介id
		$mediaInfo = M('tmedia')->cache(true,600)->where(array('fid'=>$media_id))->find();
		if(!$mediaInfo){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介ID有误'));
		}
		$abnormal_type = $post_data['abnormal_type'];//异常类型
		
		if($abnormal_type == ''){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'异常类型不能为空'));
		}
		$creator = $post_data['creator'];//创建人
		if($creator == ''){
			$creator = '空';
		}
		
		$abnormal_code = A('Common/Media','Model')->get_abnormal_code($abnormal_type);//异常代码
		
		$a_data = array();
		$a_data['media_id'] = $media_id;
		$a_data['abnormal_code'] = $abnormal_code;
		$a_data['creator'] = $creator;
		$a_data['createtime'] = date('Y-m-d H:i:s');
		$a_data['abnormal_start_time'] = intval($post_data['abnormal_start_time']);
		$a_data['abnormal_end_time'] = intval($post_data['abnormal_end_time']);
		
		
		$a_ret = M('media_abnormal')->add($a_data);
		$save_data = array();
		$save_data['abnormal_code'] = $abnormal_code;

		$e_ret = M('tmedia')->where(array('fid'=>$media_id))->save($save_data);
		
		if($a_ret > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败'));
		}
	}
	
	/*接收切换过流的频道时间段*/
	public function change_drift(){
		
		$php_input = file_get_contents('php://input');
		
		
		$postData = json_decode($php_input,true);
		
		
		$where = array();
		$where['fstarttime'] = array('lt',$postData['end_time']);//查询条件，开始时间小于结束时间
		$where['fendtime'] = array('gt',$postData['start_time']);//查询条件，结束时间大于开始时间
		$where['fmediaid'] = $postData['media_id'];
		$where['fstate'] = 2;
		
		$edit_count = M('cut_task')->where($where)->save(array('fstate'=>0,'make_error_count'=>0));
		
		file_put_contents('LOG/change_drift',$php_input.'	'.$edit_count."\n",FILE_APPEND);
		
		$this->ajaxReturn(array('code'=>0,'msg'=>''));
	}
	
	
	/*查询媒介的当前采集状态*/
	public function now_collect_state(){
		
		$media_id = I('media_id');
		
		$where = array();
		$where['left(fmediaclassid,2)'] = array('in','01,02');
		$where['priority'] = array('egt',-1);
		
		if($media_id) $where['fid'] = $media_id;
		
		$mediaList = M('tmedia')->cache(true,2)
								->field('fid,fmedianame,now_collect_state,collect_abnormal_remark')
								->where($where)
								->select();//查询媒体信息
		//var_dump(M('tmedia')->getLastSql());
		$this->ajaxReturn(array(
									'code'=>0,
									'msg'=>'',
									'mediaList'=>$mediaList,
									
								));
		
	}
	
	
	/*接收媒介异常的回调*/
	public function feedback_media_abnormal_2(){
		
		$channelcheck_db_link = 'mysql://root:Aadmin18003435512@192.168.0.152:3306/channelcheck';
		$postData = file_get_contents('php://input');
		
		$postData = json_decode($postData,true);
		$remote_id = $postData['id'];//异常ID
		$status = $postData['status'];//状态，1确认为异常，-1确认为非异常
		
		$save = array();
		$save['update_time'] = time();
		if($status == 1){//确认异常
			$save['fstate'] = 2;
		}elseif($status == -1){//确认为非异常
			$save['fstate'] = -1;
			
			$media_abnormal_2_info = M('media_abnormal_2')
												->where(array('remote_id'=>$remote_id))
												->order('start_time desc')
												->find();//查询最新的异常信息

			$aa = array();
			
			$aa['sid'] = $media_abnormal_2_info['fmediaid'];
			$aa['channel'] = M('tvchannelinfo','',$channelcheck_db_link)->where(array('sid'=>$media_abnormal_2_info['fmediaid']))->getField('channel');
			$aa['title'] = M('tmedia')->cache(true,600)->where(array('fid'=>$media_abnormal_2_info['fmediaid']))->getField('fmedianame');
			$aa['type'] = 1;

			$aa['url'] = A('Common/Media','Model')->get_m3u8($media_abnormal_2_info['fmediaid'],$media_abnormal_2_info['start_time'],($media_abnormal_2_info['start_time'] + 60),1);
			$aa['start'] = $media_abnormal_2_info['start_time'];
			$aa['create_time'] = date('Y-m-d H:i:s');
			M('tvlogofeedback','',$channelcheck_db_link)->where(array('sid'=>$media_abnormal_2_info['fmediaid']))->delete();//删除旧数据
			M('tvlogofeedback','',$channelcheck_db_link)->add($aa);//新增新数据
		}
		M('media_abnormal_2')->where(array('remote_id'=>$remote_id))->save($save);
		
		$this->ajaxReturn(array('code'=>0,'msg'=>''));
		
	}
	
	/*修改媒体采集状态*/
	public function change_collect_state(){
		$channelcheck_db_link = 'mysql://root:Aadmin18003435512@192.168.0.152:3306/channelcheck';
		$postData = file_get_contents('php://input');
		$postData = json_decode($postData,true);
		
		$media_id = $postData['media_id'];
		$now_collect_state = $postData['now_collect_state']; //'当前采集状态，1正常，0疑似异常，-1确认异常',
		$collect_abnormal_remark = $postData['collect_abnormal_remark']; //'采集异常说明',
		if(intval($now_collect_state) == 1){
			M('tvchannelinfo','',$channelcheck_db_link)
									->where(array('sid'=>$media_id,'status'=>array('not in','0,3')))
									->save(array('status'=>1));//修改为通道正常
		}elseif(intval($now_collect_state) == -1){
			M('tvchannelinfo','',$channelcheck_db_link)
									->where(array('sid'=>$media_id,'status'=>array('not in','0,3')))
									->save(array('status'=>2));//暂时修改为不检测
		}
		
		
		
		$save = array();
		$save['now_collect_state'] = $now_collect_state;//'当前采集状态，1正常，0疑似异常，-1确认异常',
		$save['collect_abnormal_remark'] = $collect_abnormal_remark;//'采集异常说明',
		
		$e_state = M('tmedia')->where(array('fid'=>$media_id))->save($save);//修改媒体采集信息
		
		if($e_state){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败'));
		}
		
		
		
		
		
		
	}
	
	
	
	/*查询媒体的毕台时间段*/
	public function get_na_time($mediaId,$startTime,$endTime){
		
		$date = date('Y-m-d',$startTime);//转化为日期
		$na_url = 'http://47.96.182.117/manage/channel/getChannelServiceTime?channel='.$mediaId.'&date='.$date;
		$ret = http($na_url);//去远程查询

		$ret = json_decode($ret,true);//查询结果转化为数组
		$naList = array();
		foreach($ret['dataList'] as $rr){//循环查到的结果，判断是否包含查询时间
			$ss = $rr['start'];//开始时间
			$ee = $rr['end'];//结束时间
			if($ss < $startTime) $ss = $startTime;//如果开始时间小于需要查询的开始时间，则让它等于查询的开始时间
			if($ss > $endTime) continue;//如果开始时间大于查询的结束时间，则丢弃本条毕台时间段
			if($ee > $endTime) $ee = $endTime;//如果结束时间大于查询的结束时间，则让它等于查询的结束时间
			if($ee < $startTime) continue;//如果结束时间小于查询的结束时间，则丢弃本条毕台时间段
			$naList[] = array('type'=>$rr['type'],'start'=>$ss,'end'=>$ee);
		}
		
		
		return $naList;
		
	}
	
	
	
	
	/*查询媒体生产异常*/
	public function produce_abnormal(){
		$mediaId = I('get.mediaId');
		$date = I('get.date');
		
		$abnormalList = M('media_abnormal_4')->where(array('fissuedate'=>$date,'fmediaid'=>$mediaId))->select();
		
		$dataList = array();
		
		foreach($abnormalList as $abnormal){
			$dataList[] = array(
								'date_time'=>date('Y-m-d H:i:s',$abnormal['fstarttime']),
								'duration'=>$abnormal['duration'],
								'ts'=>$abnormal['fstarttime'],
								'type'=>$abnormal['type'],
								'info'=>array('msg'=>$abnormal['fremark']),
									);
		}
		
		$this->ajaxReturn($dataList);
		
	}
	
	
	/*查询数据可用时间*/
	public function available_time(){
		
		set_time_limit(600);
		$postData = file_get_contents('php://input');
		$postData = json_decode($postData,true);
		$mediaIdList = $postData;
		
		$available_time = array();
		foreach($mediaIdList as $mediaId){
			
			$available_time[] = array('mediaId'=>$mediaId,'available_time'=>$this->available_time2($mediaId));
			
		}
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','available_time'=>$available_time));
	}
	
	
	
	
	/*查询数据可用时间*/
	private function available_time2($mediaId){
		
	
		

		$mediaInfo = M('tmedia')
								->cache(true,600)
								->field('fid,left(fmediaclassid,2) as media_class')
								->where(array('fid'=>$mediaId))
								->find();//查询媒介详情
		
		$labelList = M('tmedialabel')
								->cache(true,600)
								->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
								->where(array('fmediaid'=>$mediaId))
								->getField('tlabel.flabel',true);//查询标签列表


		
		if(in_array('快剪',$labelList)){//判断是否快剪频道
			$fastCut = M('cut_task')
									->where(array(
													'fmediaid'=>$mediaId,
													//'fissuddate'=>array('gt',date('Y-m-d',time()-86400*30)),
													'fstate'=>array('in','0,1,2,3')
												))
									->order('fissuedate')
									->getField('fissuedate');//查询未完成的快剪任务
			if(!$fastCut) $fastCut = date('Y-m-d',time()-86400*2);						
			$available_time = strtotime($fastCut);					
		}else{
			if($mediaInfo['media_class'] == '01') $samTable = 'ttvsample';
			if($mediaInfo['media_class'] == '02') $samTable = 'tbcsample';
			if($mediaInfo['media_class'] == '03') $samTable = 'tpapersample';
			if(empty($samTable)){
				// TODO:网络媒体数据可用时间待完善
				// ...
				return 0;
			}
			$editTime = M($samTable)
									->where(array(
													'fmediaid'=>$mediaId,
													'fissuedate'=>array('gt',date('Y-m-d',time()-86400*30)),
													'fadid'=>0
													
													))
									->order('fissuedate')
									->getField('fissuedate');//查询最早的未编辑完的样本
									//var_dump($editTime);
			if(!$editTime){//如果没有未编辑完的样本
				$editTime = time()-86400; //把最后编辑时间设为当前	
			}else{
				$editTime = strtotime($editTime);//把最后编辑时间转为时间戳
			}	

			$available_time = $editTime;//使用编辑时间

		}
		return $available_time;
		
		
	}
	
	
	/*查询审核进度*/
	public function sam_progress(){
		
		
		$dataList = M('task_input')
									->field('media_id,UNIX_TIMESTAMP(min(issue_date)) as min_issuedate,count(*) as count')
									->where(array(
													'issue_date'=>array('gt',date('Y-m-d',time()-86400*30)),
													'media_class'=>array('in','1,2'),
													'task_state'=>0
													))
									->group('media_id')
									->select();
									
		$this->ajaxReturn(array('code'=>0,'msg'=>'','dataList'=>$dataList));
	}
	
	
	
	
	/*删除ts流索引*/
	public function del_ts(){
		$mediaId = I('mediaId');
		$startTime = I('startTime');
		$endTime = I('endTime');
		$start_time = $startTime;
		$end_time = $endTime;
		$now_time = I('now_time');
		
		if(abs(time()-$now_time) > 300) $this->ajaxReturn(array('code'=>-1,'msg'=>'时间戳错误'));

		//查询当前媒体id的ts列表********************************************************************************************/
		$startPK = array (
			array('fmediaid', intval($mediaId)),
			array('start_time',$start_time - 10),
			array('end_time',$start_time + 0),
			array('fdate', date('Y-m-d',$start_time)),
		);
		$endPK = array (
			array('fmediaid', intval($mediaId)),
			array('start_time',$end_time - 0),
			array('end_time',$end_time + 10),
			array('fdate', date('Y-m-d',$start_time)),
		);

		$limit = intval((($end_time - $start_time) / 10 ) + 1000);
		$m3u8_list = A('Common/TableStore','Model')->getRange('ts_key',$startPK,$endPK,$limit);
		
		#var_dump($startPK);
		//查询当前媒体id的ts列表***************************************************************************结束***************/
		
		$DdataList = array();
		$ots_d_num = 0;
		foreach($m3u8_list as $M3u8){
			$DdataList[] = array ( // 第一行
							"operation_type" => 'DELETE',            //操作是PUT
							'condition' => 'IGNORE',
							'primary_key' => array (
								array('fmediaid',intval($mediaId)),
								array('start_time',intval($M3u8['start_time'])),
								array('end_time',intval($M3u8['end_time'])),
								array('fdate',date('Y-m-d',$M3u8['start_time'])),
								
							),
							
						);//组装写入tablestore的数据结构
			if(	count($DdataList) == 200){//判断数据是否达到200条，因为table的最大限制是一次性写入200条
				$response = A('Common/TableStore','Model')->batchWriteRow('ts_key',$DdataList);//写入数据到tablestore
				$ots_d_num += $response['successNum'];//写入ots成功的数量
				$DdataList = array();	
			}				
								
		}
		if(	count($DdataList) > 0){//
			$response = A('Common/TableStore','Model')->batchWriteRow('ts_key',$DdataList);//写入数据到tablestore
			$ots_d_num += $response['successNum'];//写入ots成功的数量
			$DdataList = array();	
		}
	

		

		
		$this->ajaxReturn(array('code'=>0,'msg'=>'删除:'.$ots_d_num,'ots_d_num'=>$ots_d_num));
		
	}
	
	
	
	
	
	
	
	
	
	
}