<?php
//样本控制器
namespace Gongshang\Controller;
use Think\Controller;
class SampleController extends BaseController {

	public function index(){
		if(I('media_class') == '' ) $_GET['media_class'] = '01';
		$media_class = I('media_class');//媒体类型，01电视，02广播，03报纸
		if($media_class == '01'){
			A('Gongshang/Tvsample')->index();
		}elseif($media_class == '02'){
			A('Gongshang/Bcsample')->index();
		}elseif($media_class == '03'){
			A('Gongshang/Papersample')->index();
		}else{
			A('Gongshang/Tvsample')->index();
		}
		
		
		$this->display();
	}
	
	
	/*导出到文件*/
	public function export(){
		$region_id = session('regulatorpersonInfo.regionid');//地区ID
		if(I('get.region_id') > 100000){
			$region_id = I('get.region_id');
		}
		$csv_url = P_U('Gongshang/Sample/export_csv',array_merge(I(''),array('region_id'=>$region_id)));
		$this->ajaxReturn(array('code'=>0,'msg'=>'','csv_url'=>$csv_url));
	}
	
	
	/*导出到文件*/
	public function export_csv(){
		$file_name = I('file_name');//文件名称
		if($file_name == '') $file_name = '样本记录导出文件';
		$file_type = I('file_type');//文件类型
		if($file_type != 'EXCEL' && $file_type != 'XML' && $file_type != 'TXT'){
			$file_type = 'EXCEL';
		}
		if(I('media_class') == '' ) $_GET['media_class'] = '01';

		$media_class = I('media_class');//媒体类型，01电视，02广播，03报纸
		$keyword = I('keyword');//搜索关键词
		$fadname = I('fadname');// 广告名称
		$fversion = I('fversion');// 版本描述
		$fmediaid  = I('fmediaid');// 媒体ID
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID
		$fcreatetime_s = I('fcreatetime_s');// 建样时间
		if($fcreatetime_s == '') $fcreatetime_s = '2015-01-01';
		$fcreatetime_e = I('fcreatetime_e');// 建样时间
		if($fcreatetime_e == '') $fcreatetime_e = date('Y-m-d');
		$this_region_id = I('this_region_id');//是否只查询本级地区
		
		$region_id = I('region_id'); 
		$where = array();//查询条件
		if($region_id == 100000 || $region_id == 0 || $region_id == '') $region_id = 0;//如果传值等于100000，相当于查全国
		if($region_id > 0){
			if($this_region_id == 'true'){
				$where['tmediaowner.fregionid'] = $region_id;//地区搜索条件
			}else{
				$region_id_rtrim = rtrim($region_id,'00');//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmediaowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}
		}
		$where['sample.fstate'] = array('neq',-1);
		
		if($keyword != ''){
			$where['sample.fversion|tad.fadname'] = array('like','%'.$keyword.'%');
		} 
		if($fadname != ''){
			$where['tad.fadname'] = array('like','%'.$fadname.'%');//广告名称
		}
		if($fversion != ''){
			$where['sample.fversion'] = array('like','%'.$fversion.'%');//版本描述
		}
		if($fmediaid != ''){
			$where['sample.fmediaid'] = $fmediaid;//媒体ID
		}
		if(is_array($fillegaltypecode)){
			$where['sample.fillegaltypecode'] = array('in',$fillegaltypecode);//违法类型代码
		}
		if($fcreatetime_s != '' || $fcreatetime_e != ''){
			$where['sample.fcreatetime'] = array('between',$fcreatetime_s.','.$fcreatetime_e.' 23:59:59');
		}
		
		
		if($media_class == '01'){

			$tvsampleList = M('ttvsample')->alias('sample')
											->field(
													'sample.fid,
													tad.fadname,
													sample.fversion,
													sample.fspokesman,
													sample.fcreator,
													tillegaltype.fillegaltype,
													sample.fissuedate,
													tadowner.fname as adowner_name,
													tmedia.fmedianame,
													sample.favifilename
													')
											->join('tmedia on tmedia.fid = sample.fmediaid')
											->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
											->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
											->join('tad on tad.fadid = sample.fadid')
											->join('tadowner on tadowner.fid = tad.fadowner')
											->where($where)
											->order('sample.fid desc')
											->select();//查询广告样本列表
			if($file_type == 'XML'){
				header("Content-Disposition: attachment;filename=".$file_name.".xml"); 
				echo arrayToXml($tvsampleList);
				exit;
			}
			$csvContent = A('Common/Csv','Model')->make($tvsampleList,array('样本ID','广告名称','版本说明','代言人','创建人','违法类型','发布日期','广告主','发布媒介','素材路径'));
	
		}elseif($media_class == '02'){
			$bcsampleList = M('tbcsample')->alias('sample')
											->field(
													'sample.fid,
													tad.fadname,
													sample.fversion,
													sample.fspokesman,
													sample.fcreator,
													tillegaltype.fillegaltype,
													sample.fissuedate,
													tadowner.fname as adowner_name,
													tmedia.fmedianame,
													sample.favifilename
													')
											->join('tmedia on tmedia.fid = sample.fmediaid')
											->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
											->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
											->join('tad on tad.fadid = sample.fadid')
											->join('tadowner on tadowner.fid = tad.fadowner')
											->where($where)
											->order('sample.fid desc')
											->select();//查询广告样本列表
			if($file_type == 'XML'){
				header("Content-Disposition: attachment;filename=".$file_name.".xml"); 
				echo arrayToXml($bcsampleList);
				exit;
			}
			$csvContent = A('Common/Csv','Model')->make($bcsampleList,array('样本ID','广告名称','版本说明','代言人','创建人','违法类型','发布日期','广告主','发布媒介','素材路径'));
				
			
		}elseif($media_class == '03'){
			$papersampleList = M('tpapersample')->alias('sample')
											->field(
													'sample.fpapersampleid,
													tad.fadname,
													sample.fversion,
													sample.fspokesman,
													sample.fcreator,
													tillegaltype.fillegaltype,
													sample.fissuedate,
													tadowner.fname as adowner_name,
													tmedia.fmedianame,
													sample.fjpgfilename
													')
											->join('tmedia on tmedia.fid = sample.fmediaid')
											->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
											->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
											->join('tad on tad.fadid = sample.fadid')
											->join('tadowner on tadowner.fid = tad.fadowner')
											->where($where)
											->order('sample.fpapersampleid desc')
											->select();//查询广告样本列表
			if($file_type == 'XML'){
				header("Content-Disposition: attachment;filename=".$file_name.".xml"); 
				echo arrayToXml($papersampleList);
				exit;
			}								
			$csvContent = A('Common/Csv','Model')->make($papersampleList,array('样本ID','广告名称','版本说明','代言人','创建人','违法类型','发布日期','广告主','发布媒介','素材路径'));
			
			
		}else{
			
			
		}
		
		
		//var_dump($csvContent);



		if($file_type == 'EXCEL'){
			header("Content-Disposition: attachment;filename=".$file_name.".csv"); 
		}elseif($file_type == 'TXT'){
			header("Content-Disposition: attachment;filename=".$file_name.".txt"); 
		}

		echo $csvContent;
		exit;
		
	}
	
	
	/*转办线索*/
	public function send_to_media(){

		$media_mail = I('media_mail');//邮件地址
		
		
		$sample_id = I('sample_id');//样本ID

		

		$sampleInfo = M('ttvsample')->alias('sample')
								  ->field('
										sample.*,
										sample.fid as fsampleid,
										left(tmedia.fmediaclassid,2) as fmediaclass,
										tmediaclass.fclass as mediaclass,
										tad.fadname,
										tadclass.ffullname as adclass_fullname,
										tmedia.fmedianame,
										tmedia.femail as media_mail,
										tillegaltype.fillegaltype
										')
								
								->join('tad on tad.fadid = sample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = sample.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构
								->join('tmediaclass on tmediaclass.fid = tmedia.fmediaclassid')
								->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')

								->where(array('sample.fid'=>$sample_id))
								->find();//查询样本详情
		
		//var_dump($sampleInfo);
		A('Gongshang/Adclue','Model')->sent_to_adclue_media($sampleInfo,$media_mail);//给媒介发送邮件通知

		$this->ajaxReturn(array('code'=>0,'msg'=>'发送成功'));
		
	}
	
	
	
	
	


	
	
	
	
}