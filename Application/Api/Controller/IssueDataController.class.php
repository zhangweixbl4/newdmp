<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;
class IssueDataController extends Controller {
	
	
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}
	

	
	public function change_customer1(){

		$time = time();
		set_time_limit(600);
		session_write_close();
		$cc = I('cc');
		if(!$cc){
			if(S('change_customer1_cc') == '1'){
				S('change_customer1_cc','2',3600);
				$cc = 2;
			}else{
				S('change_customer1_cc','1',3600);
				$cc = 1;
			}
			
		}  
		$OpenSearchClient = A('Common/OpenSearch','Model')->client();
		$searchClient = new SearchClient($OpenSearchClient);
		
		
		$params = new SearchParamsBuilder();
		$params->setHits(500);//返回数量
		$params->setAppName(C('OPEN_SEARCH_CUSTOMER'));

		$params->setQuery('forgid:"0"');//搜索
		$params->setFormat("json");//数据返回json格式
		$params->setFetchFields(['identify']);//设置需返回哪些字段
		$params->setScrollExpire('1m');//设置下次scroll发送请求超时时间，用于scroll方法查询，此处为第一次访问，用于获取scrollId
		
			
			
		$ret = $searchClient->execute($params->build());//执行查询并返回信息
		$result = json_decode($ret->result,true);//第一次返回的数据，主要获取总数和scrollID
		$viewtotal = $result['result']['total'];//数据总量，也是剩余数量
		#var_dump($result['result']);
		$inputCount = 0;
		for($i=1;$i<=100;$i++){//循环用scrollid查询数据
			#var_dump($viewtotal);
			#var_dump($result['result']['scroll_id']);
			if($viewtotal == 0) break;
			
			
			$params->setScrollId($result['result']['scroll_id']);//通过上面第一次查询返回的 scrollId ，作为查询参数获取数据
			$ret = $searchClient->execute($params->build());//执行查询并返回信息
			$result = json_decode($ret->result,true);//
			$viewtotal -= $result['result']['num'];//计算剩余数据
			
			#var_dump($result['result']['items']);
			$openDatas = [];
			
			foreach($result['result']['items'] as $items){
			
				#echo date('Y-m-d',$items['fissuedate']) ."<br />\n";
				$openData = array('identify'=>$items['identify']);
				$openDatas[] = array('cmd'=>'delete','fields'=>$openData);
				
			}
			
			if(count($openDatas) > 0){
				$inputState = A('Common/OpenSearch','Model')->push('ad_issue',$openDatas,C('OPEN_SEARCH_CUSTOMER'));
				$inputState = json_decode($inputState->result,true);
			}else{
				sleep(1);
				continue;
			}

			if($inputState['status'] == 'OK'){
				$inputCount += count($openDatas);
			}else{
				#echo $cc.'	'.$inputState['status'] .'	'."\n";
				sleep(1);
			}
			sleep(1);
		}
		
		echo '类别代码：'.$cc.',执行时间：' . (time() - $time) .',删除成功：'.$inputCount.',还剩：'.$viewtotal."\n" ;
		
		

	}


	public function issue_data(){
		ini_set('memory_limit','512M');
		session_write_close();
		set_time_limit(600);
		for($i=0;$i<50;$i++){
			$this->issue_data_do();
		}

	}
	
	/*媒介*/
	public function issue_data_do(){
		ini_set('memory_limit','512M');
		session_write_close();
		set_time_limit(600);
		$dateInfo = A('Common/System','Model')->important_data('issue_data');//获取处理过的日期
		$dateInfo = json_decode($dateInfo,true);//转为数组
		if(!$dateInfo){//如果是空的
			$month_date = date('Y-m-d',time() - 86400 * 8);
			$media_id = 0;
		}else{
			$month_date = $dateInfo['month_date'];//处理月份
			$media_id = $dateInfo['media_id'];//处理媒介id
		}
		
		$where = array();

		$where['tmedia.fid'] = array('gt',$media_id);
		$where['left(tmedia.fmediaclassid,2)'] = array('in','01,02,03');
		$where['priority'] = array('egt',0);
		
		
		$mediaInfo = M('tmedia')->cache(true,120)
			->field('tmedia.fid ')
			->where($where)
			->order('tmedia.fid asc')
			->find();
		
		if(!$mediaInfo){//判断能否查到媒介
			if(strtotime($month_date) > time()){//判断日期是否处理到了最近一天
				A('Common/System','Model')->important_data('issue_data','{}');
			}else{
				A('Common/System','Model')->important_data('issue_data',
					json_encode(array(
						'month_date'=>date("Y-m-d",strtotime($month_date) + 86400),//时间戳加一天
						'media_id'=>0)
					));//不是最近一天
			}
			exit;
		}else{
			$ff = A('Common/System','Model')->important_data('issue_data',
				json_encode(array('month_date'=>$month_date,'media_id'=>$mediaInfo['fid'])));
		}
		//var_dump($month_date);
		//var_dump($mediaInfo['fid']);
		
		A('Api/IssueData','Model')->issue_data($month_date,$mediaInfo['fid']);
	}
	
	public function make_customer_ad_issue_correct(){
		set_time_limit(600);
		session_write_close();
		ini_set('memory_limit','1024M');
		$array_filed_list = ['fexpression_codes','trackerids','fadclasscode_v2','regionid_array','fadclasscode'];
		
		
		$needCorrectList = M('make_customer_ad_issue')->field('fid,fmediaid,fdate,forgid,fmediaclass')->where(array('fstate'=>10))->limit(5)->select();
		
		foreach($needCorrectList as $needCorrect){
			$dataList_mysql = M('hzsj_dmp_ad','',C('FENXI_DB'))->where(array('forgid'=>$needCorrect['forgid'],'fmediaid'=>$needCorrect['fmediaid'],'fissuedate'=>strtotime($needCorrect['fdate'])))->select();
			$datas = [];
			foreach($dataList_mysql as $data) {
				#var_dump($data);
				foreach($array_filed_list as $array_filed){
					$data[$array_filed] = explode('|',$data[$array_filed]);

				}
				$datas[] = $data;
			}
			$aa = A('Api/CustomerIssue','Model')->update_issue_data($needCorrect['fdate'],$needCorrect['fmediaid'],$datas,$needCorrect['forgid'],true,false);
			var_dump($aa);
			if($aa['openInputNum'] == $aa['sqlInputNum']){
				M('make_customer_ad_issue')->where(array('fid'=>$needCorrect['fid']))->save(array('fstate'=>1));
			}
		}
		
		echo date('Y-m-d H:i:s');
		
		
		
	}
	
	
	/*写入客户发布数据控制端（应用数据）*/
	public function make_customer_ad_issue(){

        set_time_limit(600);
        session_write_close();
		
		M('make_customer_ad_issue')->where(array('fmediaclass'=>['notin','01,02,03,05,13']))->delete();
		var_dump(M('make_customer_ad_issue')->getLastSql());
		$make_customer_ad_issue_list = M('make_customer_ad_issue')->field('fid')->where(array('fstate'=>array('in','0,9'),'fmediaclass'=>array('in','01,02,03,05,13')))->order('fid desc')->limit(10)->select();//查询抽取任务
		//var_dump($make_customer_ad_issue_list);
		$urlArr = array();
		foreach($make_customer_ad_issue_list as $make_customer_ad_issue){//循环抽取任务
			$urlArr[] = urlencode(U('Api/IssueData/make_customer_ad_issue_do@'.$_SERVER['HTTP_HOST'],array('fid'=>$make_customer_ad_issue['fid'])));
			
		}
		if($urlArr){
			$retArr = A('Common/System','Model')->async_get_node($urlArr);
		}

		var_dump($retArr);

	}
	
	
	/*写入客户发布数据执行端（应用数据）*/
	public function make_customer_ad_issue_do(){
		lock('make_customer_ad_issue_do_'.I('get.fid'),null);
		ini_set('memory_limit','2048M');//设置内存
		//lock('make_customer_ad_issue_do_'.I('get.fid'),true,600);//获得线程锁
        set_time_limit(600);
        session_write_close();
		
		$make_customer_ad_issue = M('make_customer_ad_issue')
									->where(array('fmediaclass'=>['in','01,02,03,05,13'],'fstate'=>array('in','0,9'),'fid'=>I('get.fid')))->find();//查询抽取任务

		

		if($make_customer_ad_issue['fmediaclass'] == '13'){//如果是互联网的
			$add_state = A('Api/IssueData','Model')->make_customer_net_ad_issue($make_customer_ad_issue['fdate'],$make_customer_ad_issue['fmediaid'],$make_customer_ad_issue['forgid']);//添加数据
			
		}elseif(in_array($make_customer_ad_issue['fmediaclass'],array('01','02','03'))){//如果是电视、广播、报纸的
			$add_state = A('Api/IssueData','Model')->make_customer_ad_issue($make_customer_ad_issue['fdate'],$make_customer_ad_issue['fmediaid'],$make_customer_ad_issue['forgid']);//添加数据
		}elseif($make_customer_ad_issue['fmediaclass'] == '05'){//如果是户外的
			$add_state = A('Api/IssueData','Model')->make_customer_hw_ad_issue($make_customer_ad_issue['fdate'],$make_customer_ad_issue['fmediaid'],$make_customer_ad_issue['forgid']);//添加数据
		}
		
		
		
		if($add_state['openInputNum'] == $add_state['sqlInputNum']){
			
			$ed_yewu_s = M('tcustomer_inspect_plan','',C('ZIZHI_DB'))
												->where(array(
															'fcustomer_code'=>$make_customer_ad_issue['forgid'],
															'fmedia_id'=>$make_customer_ad_issue['fmediaid'],
															'fissue_date'=>$make_customer_ad_issue['fdate'],
														))
												->save(array(
															'fstatus'=>2,
															'fdelivery_date'=>date('Y-m-d H:i:s'),
															'fprogress'=>'已完成交付',
															'ffinish_ads'=>$add_state['sqlInputNum'],
															'fillegal_ads'=>$add_state['illNum'],
															));
			if($ed_yewu_s || true){ #中台修改成功才修改队列
				M('make_customer_ad_issue')
									->where(array('fid'=>$make_customer_ad_issue['fid']))
									->save(array(
													'fremark'=>json_encode($add_state),
													'fstate'=>1,
													'ffinishtime'=>date('Y-m-d H:i:s')
												));//修改抽取数据任务状态
			}									
			
		}
		
		
		echo date('Y-m-d H:i:s');
		//lock('make_customer_ad_issue_do_'.I('get.fid'),null);//释放线程锁

	}
	
	
	
	
	
	/*创建自动抽取任务*/
	public function auto_ext(){
		exit;
		set_time_limit(600);
		session_write_close();
		$today = I('today');
		if(!$today) $today = date('Y-m-d');
		$mediaList = M('tmedia')->field('fid,auto_ext_day_ago,left(fmediaclassid,2) as media_class')->where(array('auto_ext_day_ago'=>array('gt',0)))->select();

		$auto_ext_list = array();
		foreach($mediaList as $media){//按天数组装数据
			$auto_ext_list[$media['auto_ext_day_ago']][] = $media;
		}
		
		foreach($auto_ext_list as $day_ago => $auto_ext){
			$ago_day = date('Y-m-d',strtotime($today) - ($day_ago * 86400));
			
			#$task_extract_id = M('customer_ad_issue_task')->add(array('task_date'=>$ago_day,'wx_id'=>0,'create_date'=>date('Y-m-d H:i:s'),'remark'=>'自动任务'));

			foreach($auto_ext as $autoExt){
				$get_state = A('Admin/FixedMediaData','Model')->get_state($autoExt['fid'],$ago_day);
				#var_dump($get_state,$autoExt['fid'],$ago_day);
				if($get_state != 0) continue; #如果归档状态不是未归档，则不处理
				A('Admin/FixedMediaData','Model')->fixed($autoExt['fid'],$ago_day,'-1','等待自动归档','system'); #归档状态修改
			}
		}
		
		echo 'SUCCESS';
		
		
	}
	
	/**
	 * 清理河北传统广告数据
	 * by zw
	 */
	public function clearHeBeiIssueData(){
		$customer_url = ['hebeict.hz-data.xyz'];//需要处理的平台

		if(time()>=strtotime('Y-m-06')){//每月6号之后可执行
			$where_cr['a.cr_url'] = ['in',$customer_url];
			$where_cr['a.cr_status'] = 1;
			$where_cr['b.cst_name'] = 'clearIssueData';
			$where_cr['b.cst_val'] = ['lt',date('Y-m-01')];
			$do_cr = M('customer')
				->alias('a')
				->field('b.cst_val,a.cr_num,a.cr_id,b.cst_id')
				->join('customer_systemset b on a.cr_id = b.cst_crid')
				->where($where_cr)
				->find();
			$medias = S('finish_media'.strtotime($do_cr['cst_val']))?S('finish_media'.strtotime($do_cr['cst_val'])):[];//获取需要的媒体
			if(!empty($do_cr) && empty($medias)){
				//获取当前平台所拥有的媒体类型
				$cst_val = M('customer_systemset')->cache(true,600)->where(['cst_crid'=>$do_cr['cr_id'],'cst_name'=>'media_class'])->getField('cst_val');
				if(!empty($cst_val)){
					$mclass = $cst_val;
				}else{
					$mclass = ['01','02','03'];
				}

				$where_rma['left(b.fmediaclassid,2)'] = ['in',$mclass];
				$where_rma['a.fstate'] = 1;
				$where_rma['a.fcustomer'] = $do_cr['cr_num'];
				$where_rma['a.fregulatorcode'] = '20'.$do_cr['cr_num'];
				$do_rma = M('tregulatormedia')
					->alias('a')
					->join('tmedia b on a.fmediaid = b.fid')
					->where($where_rma)
					->getField('b.fid',true);
				if(!empty($do_rma)){
					S('finish_media'.strtotime($do_cr['cst_val']),$do_rma,86400);
					$medias = $do_rma;
				}
			}

			if(!empty($do_cr) && !empty($medias)){
				$nowmedia = array_splice($medias,0,1)[0];//去除当前执行检索的媒体
				if(!empty($medias)){
					S('finish_media'.strtotime($do_cr['cst_val']),$medias,86400);
				}else{
					S('finish_media'.strtotime($do_cr['cst_val']),null);

					//更新截止时间点
					$task_date = date('Y-m-d',strtotime($do_cr['cst_val'].' +1 day'));
					M('customer_systemset')->where(['cst_id'=>$do_cr['cst_id']])->save(['cst_val'=>$task_date]);
				}

				if(!empty($nowmedia) && !empty($do_cr['cst_id'])){
					A('Api/CustomerIssue','Model')->delete_issue_data($do_cr['cst_val'],$nowmedia);
				}
				$data['mediaid'] = $nowmedia;
				$data['issuedata'] = $do_cr['cst_val'];
			}
		}

		$this->ajaxReturn(['code'=>0,'msg'=>'执行完成','data'=>$data]);
	}

	/**
	 * 清理广西传统广告数据，保留7、8、9月份数据
	 * by zw
	 */
	public function clearGuangXiIssueData(){
		$customer_url = ['guangxi.hz-data.xyz'];//需要处理的平台

		$where_cr['a.cr_url'] = ['in',$customer_url];
		$where_cr['a.cr_status'] = 1;
		$where_cr['b.cst_name'] = 'clearIssueData';
		$where_cr['b.cst_val'] = ['lt',date("Y-m-d", strtotime("-4 day"))];
		$do_cr = M('customer')
			->alias('a')
			->field('b.cst_val,a.cr_num,a.cr_id,b.cst_id')
			->join('customer_systemset b on a.cr_id = b.cst_crid')
			->where($where_cr)
			->find();
		if(strtotime($do_cr['cst_val']) >= strtotime('2019-07-01') && strtotime($do_cr['cst_val']) <= strtotime('2019-09-30')){
			$do_cr['cst_val'] = '2019-10-01';
		}
		$medias = S('finish_media'.strtotime($do_cr['cst_val']))?S('finish_media'.strtotime($do_cr['cst_val'])):[];//获取需要的媒体
		if(!empty($do_cr) && empty($medias)){
			//获取当前平台所拥有的媒体类型
			$cst_val = M('customer_systemset')->cache(true,600)->where(['cst_crid'=>$do_cr['cr_id'],'cst_name'=>'media_class'])->getField('cst_val');
			if(!empty($cst_val)){
				$mclass = $cst_val;
			}else{
				$mclass = ['01','02','03'];
			}

			$where_rma['left(b.fmediaclassid,2)'] = ['in',$mclass];
			$where_rma['a.fstate'] = 1;
			$where_rma['a.fcustomer'] = $do_cr['cr_num'];
			$where_rma['a.fregulatorcode'] = '20'.$do_cr['cr_num'];
			$do_rma = M('tregulatormedia')
				->alias('a')
				->join('tmedia b on a.fmediaid = b.fid')
				->where($where_rma)
				->getField('b.fid',true);
			if(!empty($do_rma)){
				S('finish_media'.strtotime($do_cr['cst_val']),$do_rma,86400);
				$medias = $do_rma;
			}
		}

		if(!empty($do_cr) && !empty($medias)){
			$nowmedia = array_splice($medias,0,1)[0];//去除当前执行检索的媒体
			if(!empty($medias)){
				S('finish_media'.strtotime($do_cr['cst_val']),$medias,86400);
			}else{
				S('finish_media'.strtotime($do_cr['cst_val']),null);

				//更新截止时间点
				$task_date = date('Y-m-d',strtotime($do_cr['cst_val'].' +1 day'));
				if(strtotime($task_date) >= strtotime('2019-07-01') && strtotime($task_date) <= strtotime('2019-09-30')){
					$task_date = '2019-10-01';
				}
				M('customer_systemset')->where(['cst_id'=>$do_cr['cst_id']])->save(['cst_val'=>$task_date]);
			}

			if(!empty($nowmedia) && !empty($do_cr['cst_id'])){
				A('Api/CustomerIssue','Model')->delete_issue_data($do_cr['cst_val'],$nowmedia);
			}
			$data['mediaid'] = $nowmedia;
			$data['issuedata'] = $do_cr['cst_val'];
		}

		$this->ajaxReturn(['code'=>0,'msg'=>'执行完成','data'=>$data]);
	}
	
	public function iss(){
		
		set_time_limit(600);
		session_write_close();
		ini_set('memory_limit','2048M');
		
		$ff = I('ff','0');
		
		$time = time();
		$array_filed_list = ['fexpression_codes','trackerids','fadclasscode_v2','regionid_array','fadclasscode'];
		$sql = 'select  DISTINCT fmediaid,fissuedate from hzsj_dmp_ad_2 where forgid = 0 limit 10;';
		echo $sql."\n";
		$huizong0List = M('','',C('FENXI_DB'))->query($sql);
		

		foreach($huizong0List as $huizong){
			$is_wc = true;
			$regulatorcodeList = M('tregulatormedia')
											->cache(true,120)
											->join('tregulator on tregulator.fcode = tregulatormedia.fregulatorcode')
											->where(array('fmediaid'=>$huizong['fmediaid'],'tregulatormedia.fstate'=>1,'fisoutmedia'=>0,'left(fregulatorcode,4)'=>['not in','2010,3010,9010,2061']))
											
											->group('tregulatormedia.fregulatorcode')
											->getField('tregulatormedia.fregulatorcode',true);//
											
			$dataList_mysql = M('hzsj_dmp_ad_2','',C('FENXI_DB'))->where(array('fmediaid'=>$huizong['fmediaid'],'fissuedate'=>$huizong['fissuedate']))->select();
			
			
			#var_dump($datas);
			
			
			foreach($regulatorcodeList as $regulatorcode){
				
				$datas = [];
				foreach($dataList_mysql as $data) {
					
					foreach($array_filed_list as $array_filed){
						$data[$array_filed] = explode('|',$data[$array_filed]);

					}
					unset($data['fcustomer']);
					unset($data['forgid']);
					$data['identify'] = $data['identify'].'_'.$regulatorcode;
					$datas[] = $data;
				}
				#var_dump($datas);
				#var_dump(date('Y-m-d',$huizong['fissuedate']),$huizong['fmediaid'],$datas,$regulatorcode,false);
				$aa = A('Api/CustomerIssue','Model')->update_issue_data(date('Y-m-d',$huizong['fissuedate']),$huizong['fmediaid'],$datas,$regulatorcode,false);
				echo $regulatorcode .'	'. $aa['openInputNum'] .'	'. $aa['sqlInputNum']."\n";
				if($aa['openInputNum'] != $aa['sqlInputNum']){
					$is_wc = false;
				}
			}
			
			if($is_wc){
				$delCount = M('hzsj_dmp_ad_2','',C('FENXI_DB'))->where(array('fmediaid'=>$huizong['fmediaid'],'fissuedate'=>$huizong['fissuedate']))->delete();
				$del1 = M('tbn_ad_summary_day_v3','',C('FENXI_DB'))->where(array('fmediaid'=>$huizong['fmediaid'],'fdate'=>date('Y-m-d',$huizong['fissuedate']),'forgid'=>0))->delete();
				$del2 = M('tbn_ad_summary_day_v3')->where(array('fmediaid'=>$huizong['fmediaid'],'fdate'=>date('Y-m-d',$huizong['fissuedate']),'forgid'=>0))->delete();
				$del3 = M('tbn_ad_summary_day_v3_gongyi')->where(array('fmediaid'=>$huizong['fmediaid'],'fdate'=>date('Y-m-d',$huizong['fissuedate']),'forgid'=>0))->delete();
				
				echo date('Y-m-d',$huizong['fissuedate']).'	'.$huizong['fmediaid'].'	'.$delCount.'	'.$del1.'	'.$del2.'	'.$del3."\n\n\n";
			}
			
		}
		
		echo time()-$time;

	}
	

	

	
	
	
	
}