<?php
/**
 * User: yjn
 * Date: 2019/7/1 0001
 * Time: 22:38
 */

namespace Agp\Controller;
use Think\Controller;
import('Vendor.PHPExcel');

class StaIllDataController extends BaseController{

    /*by yjn*/
    public function sta_cule_data(){

        $fmedia_class = I("fmedia_class");//媒体类型

        $is_out_report = I("is_out_report",0);//是否是导出EXCEl

        $region = I("region",0);//查看级别

        $s_fsend_datetime = I("s_fsend_datetime");//派发日期开始
        $e_fsend_datetime = I("e_fsend_datetime");//派发日期结束

        $fresult_date = I("fresult_date");//处理结束时间

        $s_fissue_date = I("s_fissue_date");//发布时间开始
        $e_fissue_date = I("e_fissue_date");//发布时间结束

        $where = " ";
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isexamine = $ALL_CONFIG['isexamine'];
        $ischeck = $ALL_CONFIG['ischeck'];

        if(in_array($isexamine, [10,20,30,40,50])){
            $where .= " AND a.fexamine = 10 ";
        }

        //是否抽查模式
        if(!empty($ischeck)){
            $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
            //如果抽查表有数据
            if(!empty($spot_check_data)){
                $dates = [];//定义日期数组
                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                    $year_month = '';
                    $date_str = [];
                    $year_month = substr($spot_check_data_val['fmonth'],0,7);
                    if(!empty($spot_check_data_val['condition'])){
                        $date_str = explode(',',$spot_check_data_val['condition']);
                    }
                    foreach ($date_str as $date_str_val){
                        $dates[] = $year_month.'-'.$date_str_val;
                    }
                }
                $where  .= ' AND d.fissue_date IN ("'.implode('","', $dates).'")';
            }else{
                $where  .= ' AND 1 = 0';
            }
        }

        switch ($region){
            case 0:
                $where .= " AND b.flevel IN(1,2,3,4,5)";
                break;
            case 1:
                $where .= " AND b.flevel IN(1)";
                break;
            case 2:
                $where .= " AND b.flevel IN(2,3,4)";
                break;
            case 3:
                $where .= " AND b.flevel = 5";
                break;
        }

        //媒体类型
        if(!empty($fmedia_class)){
            $where .= " AND a.fmedia_class = ".$fmedia_class." ";
        }

        //派发日期
        if(!empty($s_fsend_datetime) && !empty($e_fsend_datetime)){
            $where .= " AND a.fsend_datetime BETWEEN '".$s_fsend_datetime."' AND '".$e_fsend_datetime."' ";
        }

        //发布时间
        if(!empty($s_fissue_date) && !empty($e_fissue_date)){
            $where .= " AND d.fissue_date BETWEEN '".$s_fissue_date."' AND '".$e_fissue_date."' ";
        }


        if(!empty($fresult_date)){
            $fresult_date_map = " AND a.fresult_datetime <= '".$fresult_date."'";
        }else{
            $fresult_date_map = '';
        }

        //统计线索立案以及处理情况 dump($res);exit;
        $res = M("")->query("
        SELECT a.fregion_id,IF (b.flevel = 1,'省级',b.fname1) AS region_name,COUNT(DISTINCT a.fmedia_id) AS media_count,
        GROUP_CONCAT(DISTINCT (SELECT CASE WHEN instr(fmedianame, '（') > 0 THEN LEFT (fmedianame,instr(fmedianame, '（') - 1) ELSE fmedianame
		END as fmedianame from tmedia WHERE fid = a.fmedia_id) SEPARATOR '；<br/>') as medias,COUNT(DISTINCT a.fid) AS cule_count,
        COUNT(DISTINCT (CASE WHEN a.fview_status = 10 THEN a.fid END)) AS ck_count,
        ROUND(COUNT(DISTINCT (CASE WHEN a.fview_status = 10 THEN a.fid END)) / COUNT(DISTINCT a.fid) * 100,2) AS ckl,
        COUNT(DISTINCT (CASE WHEN a.fstatus3 IN (30, 40) THEN a.fid END)) AS pass_count,
        COUNT(DISTINCT (CASE WHEN a.fstatus3 IN (10, 20, 50) THEN a.fid END ) ) AS unpass_count,
        COUNT(DISTINCT ( CASE WHEN a.fstatus = 10 ".$fresult_date_map." THEN a.fid END ) ) AS cl_count,
        ROUND( COUNT( DISTINCT ( CASE WHEN a.fstatus = 10 ".$fresult_date_map."  THEN a.fid END ) ) / 
        (COUNT(DISTINCT a.fid) - COUNT(DISTINCT (CASE WHEN a.fstatus3 IN (30, 40) THEN a.fid END))) * 100,2) AS cll,
        COUNT(DISTINCT ( CASE WHEN fstatus = 10 AND fstatus2 IN (15, 17) THEN a.fid END ) ) AS yla,
        COUNT(DISTINCT (CASE WHEN fstatus = 10 AND fstatus2 = 16 THEN a.fid END ) ) AS yja,
        COUNT(DISTINCT (CASE WHEN fstatus = 10 AND fresult LIKE '%责令停止发布%' THEN a.fid END ) ) AS tzfb,
        COUNT(DISTINCT (CASE WHEN fstatus = 10 AND fresult LIKE '%立案%' THEN a.fid END ) ) AS lian,
        COUNT(DISTINCT (CASE WHEN fstatus = 10 AND fresult LIKE '%移送司法%' THEN a.fid END ) ) AS yjsf,
        COUNT(DISTINCT (CASE WHEN fstatus = 10 AND fresult LIKE '%其他%' THEN a.fid END )) AS others
        FROM tbn_illegal_ad a
        JOIN tregion b ON a.fregion_id = b.fid
        JOIN tmedia c ON c.fid = a.fmedia_id
        JOIN tbn_illegal_ad_issue d ON d.fillegal_ad_id = a.fid
        WHERE 1 = 1
        AND a.fcustomer = ".$system_num.$where."
        GROUP BY fregion_id ORDER BY d.fissue_date DESC;");//fsend_datetime

        $res = pxsf($res,"fregion_id",false);

        $all = [];
        $all['media_count'] = 0;
        $all['medias'] = '';
        $all['cule_count'] = 0;
        $all['ck_count'] = 0;
        $all['pass_count'] = 0;
        $all['unpass_count'] = 0;
        $all['cl_count'] = 0;
        $all['yla'] = 0;
        $all['yja'] = 0;
        $all['tzfb'] = 0;
        $all['lian'] = 0;
        $all['yjsf'] = 0;
        $all['others'] = 0;
        $all['region_name'] = "合计";
        if(!empty($res)){
            foreach ($res as $r_key => $r_val){

                $res[$r_key]['ckl'] =str_replace(".00","", $r_val['ckl']."%");
                $res[$r_key]['cll'] = str_replace(".00","",$r_val['cll']."%");

                $all['media_count'] += $r_val['media_count'];
                $all['medias'] .= $r_val['medias'];
                $all['cule_count'] += $r_val['cule_count'];
                $all['ck_count'] += $r_val['ck_count'];
                $all['pass_count'] += $r_val['pass_count'];
                $all['unpass_count'] += $r_val['unpass_count'];
                $all['cl_count'] += $r_val['cl_count'];
                $all['yla'] += $r_val['yla'];
                $all['yja'] += $r_val['yja'];
                $all['tzfb'] += $r_val['tzfb'];
                $all['lian'] += $r_val['lian'];
                $all['yjsf'] += $r_val['yjsf'];
                $all['others'] += $r_val['others'];
            }
        }

        $all['ckl'] = (round($all['ck_count']/$all['cule_count'],4)*100)."%";
        $all['cll'] =  (round($all['cl_count']/$all['cule_count'],4)*100)."%";
        $res[] = $all;
        //如果是线索菜单并且是点击了导出按钮
        if($is_out_report == 1){

            $objPHPExcel = new \PHPExcel();
            $objPHPExcel
                ->getProperties()  //获得文件属性对象，给下文提供设置资源
                ->setCreator( "MaartenBalliauw")             //设置文件的创建者
                ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
                ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
                ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
                ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
                ->setKeywords( "office 2007 openxmlphp")        //设置标记
                ->setCategory( "Test resultfile");                //设置类别

            $sheet = $objPHPExcel->setActiveSheetIndex(0);

            $sheet ->mergeCells("A1:A3");//序号
            $sheet ->mergeCells("B1:B3");//管辖机构
            $sheet ->mergeCells("C1:C3");//媒体数量
            $sheet ->mergeCells("D1:D3");//线索数量

            //线索查看
            $sheet ->mergeCells("E1:F1");
            $sheet ->mergeCells("E1:E2");
            $sheet ->mergeCells("E1:F2");

            //线索复核
            $sheet ->mergeCells("G1:H1");
            $sheet ->mergeCells("G1:G2");
            $sheet ->mergeCells("G1:H2");

            $sheet ->mergeCells("I1:P1");//线索处理情况
            $sheet ->mergeCells("I2:J2");//线索处理
            $sheet ->mergeCells("K2:L2");//立案
            $sheet ->mergeCells("M2:P2");//线索处理方式


            $sheet ->setCellValue('A1','序号');
            $sheet ->setCellValue('B1','地区名称');
            $sheet ->setCellValue('C1','媒体数量');
            $sheet ->setCellValue('D1','线索数量');
            $sheet ->setCellValue('E1','线索查看');
            $sheet ->setCellValue('E3','查看量');
            $sheet ->setCellValue('F3','查看率');

            $sheet ->setCellValue('G1','线索复核');
            $sheet ->setCellValue('G3','通过');
            $sheet ->setCellValue('H3','未通过');

            $sheet ->setCellValue('I1','线索处理情况');
            $sheet ->setCellValue('I2','线索处理');
            $sheet ->setCellValue('I3','处理量');
            $sheet ->setCellValue('j3','处理率');

            $sheet ->setCellValue('K2','立案');
            $sheet ->setCellValue('K3','已立案');
            $sheet ->setCellValue('L3','已结案');

            $sheet ->setCellValue('M2','线索处理方式');
            $sheet ->setCellValue('M3','责令停止发布');
            $sheet ->setCellValue('N3','立案');
            $sheet ->setCellValue('O3','移交司法机关');
            $sheet ->setCellValue('p3','其他');
            $data_count = count($res);
            foreach ($res as $key => $value) {

                $sheet ->setCellValue('A'.($key+4),($key+1));
                if($data_count == $key){
                    $sheet ->setCellValue('B'.($key+4),'合计');
                }else{
                    $sheet ->setCellValue('B'.($key+4),$value['region_name']);
                }
                /*"region_name": 管辖机构,
                "media_count": 媒体数量,
                "medias": "媒体名称（预留，现在可不用管）",
                "cule_count": 线索数量,
                "ck_count": 查看量,
                "ckl": 查看率,
                "pass_count": 通过,
                "unpass_count": 未通过,
                "cl_count": 处理量,
                "cll": 处理率,
                "yla": 已立案,
                "yja": 已结案,
                "tzfb": 责令停止发布,
                "lian": 立案,
                "yjsf": 移交司法机关,
                "others": 其他*/
                $sheet ->setCellValue('C'.($key+4),$value['media_count']);
                $sheet ->setCellValue('D'.($key+4),$value['cule_count']);
                $sheet ->setCellValue('E'.($key+4),$value['ck_count']);
                $sheet ->setCellValue('F'.($key+4),$value['ckl']);
                $sheet ->setCellValue('G'.($key+4),$value['pass_count']);
                $sheet ->setCellValue('H'.($key+4),$value['unpass_count']);
                $sheet ->setCellValue('I'.($key+4),$value['cl_count']);
                $sheet ->setCellValue('J'.($key+4),$value['cll']);
                $sheet ->setCellValue('K'.($key+4),$value['yla']);
                $sheet ->setCellValue('L'.($key+4),$value['yja']);
                $sheet ->setCellValue('M'.($key+4),$value['tzfb']);
                $sheet ->setCellValue('N'.($key+4),$value['lian']);
                $sheet ->setCellValue('O'.($key+4),$value['yjsf']);
                $sheet ->setCellValue('P'.($key+4),$value['others']);

            }
            $objActSheet =$objPHPExcel->getActiveSheet();
            $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
            $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            $date = date('Ymdhis');
            $savefile = './Public/Xls/'.$date.'.xlsx';
            $savefile2 = '/Public/Xls/'.$date.'.xlsx';
            $objWriter->save($savefile);
            //即时导出下载
            /*          header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        header('Content-Disposition:attachment;filename="'.$date.'.xlsx"');
                        header('Cache-Control:max-age=0');
                        $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
                        $objWriter->save( 'php://output');*/
            $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
            unlink($savefile);//删除文件
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn([
                "code" => 0,
                "msg" => "获取成功！",
                "data" => $res
            ]);
        }
    }
}