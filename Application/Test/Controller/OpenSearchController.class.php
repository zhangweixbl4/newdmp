<?php

namespace Test\Controller;
use Think\Controller;
use Think\Exception;
use OpenSearch\Client\OpenSearchClient;
use OpenSearch\Client\DocumentClient;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;

import('Vendor.OpenSearch.Autoloader.Autoloader', '', '.php');//载入sdk



class OpenSearchController extends Controller{

	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}	
		
		
		
		
	public function ccc(){
		
		$dataList = M('hzsj_dmp_ad')->where(array('zz'=>0))->limit(300)->getField('identify',true);
		
		$urlArr = array();
		foreach($dataList as $identify){
			$urlArr[] = urlencode(U('Test/OpenSearch/identify@'.$_SERVER['HTTP_HOST'],array('identify'=>$identify)));
		}
		if($urlArr){
			$retArr = A('Common/System','Model')->async_get_node($urlArr);
		}

		//var_dump($retArr);
	}
		
		
		
	public function identify(){
		
		$identify = I('identify');

		$dataInfo = M('hzsj_dmp_ad')->where(array('identify'=>$identify))->find();
		$openData = array();
		foreach($dataInfo as $field => $data){
			if($field == 'zz') continue;
			if(in_array($field,array('fadclasscode','regionid_array','fadclasscode_v2'))){
				$openData[$field] = explode('|',$data);
			}else{
				$openData[$field] = $data;
				if($field == 'fprice') $openData[$field] = intval($data);
			}
			
		}
		//var_dump($openData);
		$inputState = A('Common/OpenSearch','Model')->push('ad_issue',array(array('cmd'=>'add','fields'=>$openData)),C('OPEN_SEARCH_CUSTOMER'));
		M('hzsj_dmp_ad')->where(array('identify'=>$identify))->save(array('zz'=>1));
		echo date('Y-m-d H:i:s').'	'.(json_decode($inputState->result,true)['status']);
	
	}
	
	
	/*搜索测试*/
	
	public function s(){
		
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		
		header("Content-Type:text/html;charset=utf-8");


		$client = A('Common/OpenSearch','Model')->client();
		$searchClient = new SearchClient($client);

		$params = new SearchParamsBuilder();
		$params->setStart(($p*$pp) - $pp);//起始位置
		//var_dump(($p*$pp) - $pp);
		$params->setHits($pp);//返回数量

		$params->setAppName('hzsj_dmp');
		
		$setQuery = I('keyword');
		

		$params->setQuery($setQuery);

		
		
		//$params->setFilter('fstarttime>1538407035');//搜索条件

		$params->addSort('fstarttime', SearchParamsBuilder::SORT_DECREASE);//SORT_INCREASE,SORT_DECREASE,排序字段
		
		$params->addAggregate(array('groupKey' => 'fmediaid', 'aggFun' => 'count()'));//添加aggregate子句
		
		$params->setFormat("fulljson");//数据返回json格式
		

		$params->setFetchFields(array('primarykey','fmedianame','fadname','identify','fissuedate','fstarttime'));//设置需返回哪些字段
		
		
		$ret = $searchClient->execute($params->build());//执行查询并返回信息
		

		$result = json_decode($ret->result,true);//
		
		$Page = new \Think\Page($result['result']['total'],$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		//var_dump($result);
		$this->assign('issueList',$result['result']['items']);

		$this->assign('page',$Page->show());//分页输出
		$this->display();
				
		
		
		
		
		
	}
	
	
	/*group测试*/
	public function gr(){
		header("Content-Type:text/html;charset=utf-8");


		$client = A('Common/OpenSearch','Model')->client();
		$searchClient = new SearchClient($client);

		
		$params = new SearchParamsBuilder();
		$params->setStart(($p*$pp) - $pp);//起始位置

		$params->setHits($pp);//返回数量
		$params->setAppName('hzsj_dmp');
		#$params->addAggregate(array('groupKey' => 'fmediaid', 'aggFun' => 'count()'));//添加aggregate子句
		
		$params->setFormat("fulljson");//数据返回json格式
		

		$params->setFetchFields(array('primarykey','fmedianame','fadname','identify','fissuedate','fstarttime'));//设置需返回哪些字段
		
		
		$ret = $searchClient->execute($params->build());//执行查询并返回信息
		var_dump($ret);
		
		
		
		
		
	}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}