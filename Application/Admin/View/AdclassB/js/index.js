Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}
var vm = new Vue({
  el: '#app',
  data: {
    loading: false,
    adClassATree: adClassATree,
    adClassBTree: adClassBTree,
    updateList: [],
    deleteList: [],
    addList: [],
    filterText: '',
    activeId: '',
    classInfo: {},
    adCateArr: []
  },
  watch: {
    filterText(val) {
      this.$refs.adClassBTree.filter(val);
    }
  },
  methods: {
    append(data) {
      if (!data.children) {
        this.$set(data, 'children', []);
      }
      let maxId = 0;
      data.children.forEach((v, i) => {
        if (+v.id > +maxId) {
          maxId = +v.id
        }
      })
      let newId = maxId + 1
      if (data.children.length === 0) {
        newId = +(data.id + '01')
      }
      this.$prompt('请输入新类型名称', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        inputPattern: /\S/,
        inputErrorMessage: '请输入新类型名称'
      }).then((obj) => {
        let newChild = {
          id: newId,
          label: obj.value,
          children: [],
          fpcode: data.id
        };
        data.children.push(newChild);
        this.addList.push(newChild)
      }).catch(() => {});

    },
    remove(node, data) {
      const parent = node.parent;
      const children = parent.data.children || parent.data;
      const index = children.findIndex(d => d.id === data.id);
      children.splice(index, 1);
      this.deleteList.push(data.id)
    },
    edit(node, data) {
      this.$prompt('请输入修改后的名称', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        inputPattern: /\S/,
        inputErrorMessage: '请输入新类型名称'
      }).then((obj) => {
        data.label = obj.value
        this.updateList.push(data)
      }).catch(() => {});
    },
    save() {
      this.$confirm('是否保存?').then(() => {
        this.loading = true
        $.post('', {
          update: this.updateList,
          remove: this.deleteList,
          add: this.addList,
        }).then((res) => {
          this.loading = false
        })
      }).catch(() => {})
    },
    filterNode(value, data) {
      if (!value) return true;
      return data.label.toLowerCase().indexOf(value) !== -1 || data.id == value;
    },
    clickClass: function (data) {
      this.loading = false
      this.adCateArr = []
      this.activeId = data.id
      $.post('getClassInfo', {
        fcode: data.id
      }).then((res) => {
        this.loading = false
        this.classInfo = res.data
        if (this.classInfo.fcode1) {
          this.adCateArr.push(this.classInfo.fcode1.substr(0, 2))
          this.adCateArr.push(this.classInfo.fcode1.substr(0, 4))
        }
      })
    },
    saveClassA: function () {
      if (this.adCateArr.length < 1) {
        this.$message.error('请选择关联广告分类')
        return
      }
      this.$confirm('确认保存?').then(() => {
        // this.loading = true
        console.log($.post);
        $.post('saveClassA', {
          fcode: this.classInfo.fcode,
          classA: this.adCateArr[this.adCateArr.length - 1]
        }).then((res) => {
          if (res.code == 0) {
            this.$message.success(res.msg)
          } else {
            this.$message.error(res.msg)
          }
          this.loading = false
        })

      }).catch(() => {})
    }
  }
})