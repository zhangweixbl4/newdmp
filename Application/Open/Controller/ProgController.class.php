<?php
namespace Open\Controller;
use Think\Controller;

class ProgController extends BaseController {
	
	/*节目类别列表*/
	public function prog_class(){
		$post_data = file_get_contents('php://input');//获取post数据
		$post_data = json_decode($post_data,true);//把json转化成数组
		$where = array();
		if($post_data['fmediaclass'] == 'tv') $where['fmediaclass'] = 'tv';
		if($post_data['fmediaclass'] == 'bc') $where['fmediaclass'] = 'bc';
		if($post_data['fprogclass'] != '') $where['fprogclass'] = array('like','%'.$post_data['fprogclass'].'%');
		$prog_class_list = M('tprogclass')
							->field('fprogclasscode,fmediaclass,fprogclass')
							->where($where)->select();//查询界面类型列表
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','prog_class_list'=>$prog_class_list));
	}

	/*添加广播、电视节目*/
	public function add_prog(){
		$post_data = file_get_contents('php://input');//获取post数据

		if(S(md5($post_data)) == 1) $this->ajaxReturn(array('code'=>-1,'msg'=>'请勿重复提交')); //判断是否重复提交
		//S(md5($post_data),1,10);//写入缓存
		$post_data = json_decode($post_data,true);//把json转化成数组
		$issueList = $post_data['issueList'];//发布记录
		$wx_id = $post_data['wx_id'];
		$wxInfo = M('ad_input_user')->where(array('wx_id'=>$wx_id))->find();//查询用户
		if(!$wxInfo) $this->ajaxReturn(array('code'=>-1,'msg'=>'查询不到此用户'));
		
		$mediaInfo = M('tmedia')->where(array('fid'=>intval($post_data['fmediaid'])))->find();//查询媒介
		if		(substr($mediaInfo['fmediaclassid'],0,2) == '01'){
			$tb = 'tv';
		}elseif	(substr($mediaInfo['fmediaclassid'],0,2) == '02'){
			$tb = 'bc';
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介ID错误'));
		}
		
		
		if($post_data['fsamplefilename'] == ''){//判断样本文件名是否为空
			
		}else{
		
			if(M('tprogclass')->where(array('fmediaclass'=>$tb,'fprogclasscode'=>strval($post_data['fclasscode'])))->count() == 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'节目类型CODE错误'));
			}

			$a_data = array();
			$a_data['fmediaid'] = intval($post_data['fmediaid']);//媒介ID
			$a_data['fissuedate'] = date('Y-m-d',strtotime($post_data['fstart']));//发布日期
			$a_data['fcontent'] = $post_data['fcontent'];//节目内容
			$a_data['fclasscode'] = $post_data['fclasscode'];//节目类别代码
			$a_data['fstart'] = date('Y-m-d H:i:s',strtotime($post_data['fstart']));//开始播放时间
			$a_data['fend'] = date('Y-m-d H:i:s',strtotime($post_data['fend']));//结束播放时间
			$a_data['faudiosimliar'] = 100;//音频相似度
			$a_data['fvideosimliar'] = 100;//视频相似度
			$a_data['fu0'] = intval($post_data['fu0']);//位置，单位毫秒
			$a_data['fduration'] = intval($post_data['fduration']);//位置，单位秒
			
			$a_data['fsamplefilename'] = $post_data['fsamplefilename'];//样本文件名，样本文件名用于判断是否需要写入新样本
			$a_data['flastissuedate'] = date('Y-m-d',strtotime($post_data['fstart']));//最后发布日期
			$a_data['fcreator'] = $wxInfo['nickname'];//创建人
			$a_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_data['fmodifier'] = $wxInfo['nickname'];//修改人
			$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			$fprogid = M('t'.$tb.'prog')->add($a_data);//添加数据,并取得样本ID
			M('t'.$tb.'prog')->where(array('fprogid'=>$fprogid))->save(array('fsampleprogid'=>$fprogid));//直接修改为自己的样本ID
		}
		$ret_issue = array();
		foreach($issueList as $issue){
			$ia_data = array();

			$ia_data['fmediaid'] = intval($post_data['fmediaid']);//媒介ID
			$ia_data['fissuedate'] = date('Y-m-d',strtotime($issue['fstart']));//发布日期
			$ia_data['fcontent'] = $post_data['fcontent'];//节目内容
			$ia_data['fclasscode'] = $post_data['fclasscode'];//节目类别代码
			
			$ia_data['fstart'] = date('Y-m-d H:i:s',strtotime($issue['fstart']));//开始播放时间
			$ia_data['fend'] = date('Y-m-d H:i:s',strtotime($issue['fend']));//结束播放时间
			$ia_data['faudiosimliar'] = intval($issue['faudiosimliar']);//音频相似度
			$ia_data['fvideosimliar'] = intval($issue['fvideosimliar']);//视频相似度
			$ia_data['fu0'] = intval($issue['fu0']);//位置，单位毫秒
			$ia_data['fduration'] = intval($issue['fduration']);//位置，单位秒
			if(intval($fprogid) == 0){
				$ia_data['fsampleprogid'] = intval($issue['fsampleprogid']);//样本ID
				M('t'.$tb.'prog')->where(array('fprogid'=>intval($issue['fsampleprogid'])))->save(array('flastissuedate'=>date('Y-m-d H:i:s',strtotime($issue['fstart']))));//修改对应样本的最后发布日期
			}else{
				$ia_data['fsampleprogid'] = intval($fprogid);//样本ID
				M('t'.$tb.'prog')->where(array('fprogid'=>intval($fprogid)))->save(array('flastissuedate'=>date('Y-m-d H:i:s',strtotime($issue['fstart']))));//修改对应样本的最后发布日期
			}
			
			$ia_data['flastissuedate'] = date('Y-m-d',strtotime($ia_data['fstart']));//最后发布日期
			$ia_data['fcreator'] = $wxInfo['nickname'];//创建人
			$ia_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$ia_data['fmodifier'] = $wxInfo['nickname'];//修改人
			$ia_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$issueId = M('t'.$tb.'prog')->add($ia_data);//添加数据
			$issueInfo = M('t'.$tb.'prog')
										->master(true)
										->field('fprogid,fmediaid,fcontent,fclasscode,fstart,fend,fu0,fduration,faudiosimliar,fvideosimliar,fsamplefilename,fsampleprogid')
										->where(array('fprogid'=>$issueId))->find();//节目发布信息（单条）
			if($issueInfo) $ret_issue[] = $issueInfo;
		}
		
		
		if($fprogid > 0 || count($ret_issue) > 0){
			$sam_info_arr = array();
			if($fprogid > 0 ){
				$sam_info_arr = M('t'.$tb.'prog')
											->field('fprogid,fmediaid,fcontent,fclasscode,fstart,fend,fu0,fduration,faudiosimliar,fvideosimliar,fsamplefilename,fsampleprogid')
											->where(array('fprogid'=>$fprogid))->select();//样本发布信息（单条）
			}							
			$this->ajaxReturn(array('code'=>0,'msg'=>'','fprogid'=>intval($fprogid),'ret_issue'=>array_merge($sam_info_arr,$ret_issue)));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败','fprogid'=>intval($fprogid)));
		}
		

	}
	
	
	
	/*修改广播、电视节目*/
	public function edit_prog(){
		$post_data = file_get_contents('php://input');//获取post数据

		if(S(md5($post_data)) == 1) $this->ajaxReturn(array('code'=>-1,'msg'=>'请勿重复提交')); //判断是否重复提交
		//S(md5($post_data),1,10);//写入缓存
		$post_data = json_decode($post_data,true);//把json转化成数组

		$wx_id = $post_data['wx_id'];
		$wxInfo = M('ad_input_user')->where(array('wx_id'=>$wx_id))->find();//查询用户
		if(!$wxInfo) $this->ajaxReturn(array('code'=>-1,'msg'=>'查询不到此用户'));
		
		$mediaInfo = M('tmedia')->where(array('fid'=>$post_data['fmediaid']))->find();//查询媒介
		#var_dump($mediaInfo);
		if		(substr($mediaInfo['fmediaclassid'],0,2) == '01'){
			$tb = 'tv';
		}elseif	(substr($mediaInfo['fmediaclassid'],0,2) == '02'){
			$tb = 'bc';
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介ID错误'));
		}
		
		

		if ($post_data['fclasscode']) {
			if(M('tprogclass')->where(array('fmediaclass'=>$tb,'fprogclasscode'=>strval($post_data['fclasscode'])))->count() == 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'节目类型CODE错误'));
			}
		}

		$a_data = array();
		if ($post_data['fstart']) $a_data['fissuedate'] = date('Y-m-d',strtotime($post_data['fstart']));//发布日期
		if ($post_data['fcontent']) $a_data['fcontent'] = $post_data['fcontent'];//节目内容
		if ($post_data['fclasscode']) $a_data['fclasscode'] = $post_data['fclasscode'];//节目类别代码
		if ($post_data['fstart']) $a_data['fstart'] = date('Y-m-d H:i:s',strtotime($post_data['fstart']));//开始播放时间
		if ($post_data['fend']) $a_data['fend'] = date('Y-m-d H:i:s',strtotime($post_data['fend']));//结束播放时间
		
		
		if ($post_data['faudiosimliar']) $a_data['faudiosimliar'] = $post_data['faudiosimliar'];//音频相似度
		if ($post_data['fvideosimliar']) $a_data['fvideosimliar'] = $post_data['fvideosimliar'];//视频相似度
		
		
		if ($post_data['fu0']) $a_data['fu0'] = intval($post_data['fu0']);//位置，单位毫秒
		if ($post_data['fduration']) $a_data['fduration'] = intval($post_data['fduration']);//位置，单位秒
		
		if ($post_data['fsamplefilename']) $a_data['fsamplefilename'] = $post_data['fsamplefilename'];//样本文件名
		if ($post_data['fstart']) $a_data['flastissuedate'] = date('Y-m-d',strtotime($post_data['fstart']));//最后发布日期

		$a_data['fmodifier'] = $wxInfo['nickname'];//修改人
		$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间

		$rr = M('t'.$tb.'prog')->where(array('fprogid'=>$post_data['fprogid'],'fmediaid'=>$post_data['fmediaid']))->save($a_data);//
		
		if($rr){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败'));	
		}
		
		
		

	}
	
	
	
	/*获取节目列表*/
	public function get_prog_list(){
		$post_data = file_get_contents('php://input');//获取post数据

		$post_data = json_decode($post_data,true);//把json转化成数组
		
		$mediaid = intval($post_data['mediaid']);
		$start_time = date('Y-m-d H:i:s',strtotime($post_data['start_time']));
		$end_time = date('Y-m-d H:i:s',strtotime($post_data['end_time']));
		
		$mediaInfo = M('tmedia')->where(array('fid'=>$mediaid))->find();//查询媒介
		if		(substr($mediaInfo['fmediaclassid'],0,2) == '01'){
			$tb = 'tv';
		}elseif	(substr($mediaInfo['fmediaclassid'],0,2) == '02'){
			$tb = 'bc';
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介ID错误'));
		}
		
		$where = array();
		$where['fmediaid'] = $mediaid;
		$where['fstart'] = array('lt',$end_time);
		$where['fend'] = array('gt',$start_time);
		if(intval($post_data['only_sample']) == 1) $where['fprogid'] = array('exp',' = fsampleprogid');
		
		$progList = M('t'.$tb.'prog')
								->field('
										fprogid,
										fmediaid,
										fcontent,
										fclasscode,
										tprogclass.fprogclass,
										fstart,
										fend,
										fu0,
										fduration,
										faudiosimliar,
										fvideosimliar,
										fsamplefilename,
										fsampleprogid,
										flastissuedate')
								->join('tprogclass on tprogclass.fprogclasscode = t'.$tb.'prog.fclasscode')
								->where($where)->select();
		//var_dump(M('t'.$tb.'prog')->getLastSql());
		$this->ajaxReturn(array('code'=>0,'msg'=>'','progList'=>$progList));
		
	}
	
	/*删除节目*/
	public function del_prog(){
		$post_data = file_get_contents('php://input');//获取post数据
		$post_data = json_decode($post_data,true);//把json转化成数组
		$mediaid = intval($post_data['mediaid']);
		$fprogid_arr = $post_data['fprogid_arr'];
		
		
		$mediaInfo = M('tmedia')->where(array('fid'=>$mediaid))->find();//查询媒介
		if		(substr($mediaInfo['fmediaclassid'],0,2) == '01'){
			$tb = 'tv';
		}elseif	(substr($mediaInfo['fmediaclassid'],0,2) == '02'){
			$tb = 'bc';
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介ID错误'));
		}
		
		$deletedList = array();
		foreach($fprogid_arr as $fprogid){
			if(M('t'.$tb.'prog')->where(array('fmediaid'=>$mediaid,'fprogid'=>$fprogid))->delete()){
				$deletedList[] = $fprogid;
			}
		}
			
		if(count($deletedList) > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'','deletedList'=>$deletedList));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'没有删除数据'));
		}

		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}