<?php
namespace Api\Controller;
use Think\Controller;
class AdownerController extends Controller {
	// 接收JSON参数
    protected $P;
	/**
	 * @Des: 初始化
	 * @Edt: yuhou.wang
	 */	
    public function _initialize(){
        $this->P = json_decode(file_get_contents('php://input'),true);
    }
	/*获取广告主列表*/
    public function get_adowner_list(){
        header('Access-Control-Allow-Origin:*');//允许跨域
		$fname = I('fname');//获取广告主名称
		$where = array();
		$where['fstate'] = array('neq',-1);
		$where['fid'] = array('neq',0);
		$where['fname'] = array('like','%'.$fname.'%');
		$adownerList = M('tadowner')->field('fid,fname')->where($where)->limit(10)->select();//查询广告主列表
		$this->ajaxReturn(array('code'=>0,'value'=>$adownerList));
	}
    /**
     * 搜索广告主
     * @param $name string 广告主部分或全部名称
     */
    public function searchAdOwner($name = '')
    {
		header('Access-Control-Allow-Origin:*');//允许跨域
		$name = $name ? $name : $this->P['name'];
		$name = trim($name);
        $where = [
            'fstate' => ['NEQ', -1],
		];
		if(!empty($name)){
			$where['fname'] = ['LIKE', '%'.$name.'%'];
		}
        $data = M('tadowner')->field('fname value, fid')->where($where)->limit(6)->select();
		$opsearchUrl = 'http://47.98.142.150:8080/opsearch?name='.urlencode($name).'&start=1&hits=4';
		$opsearchData = http($opsearchUrl);
		$opsearchData = json_decode($opsearchData,true);
		$retData = array();
		foreach($data as $d1){
			$retData[] = array('fid'=>$d1['fid'],'value'=>$d1['value']);
		}
		foreach($opsearchData as $d2){
			$retData[] = array('value'=>$d2['name']);
		}
        $this->ajaxReturn([
            'code' => 0,
            'data' => $retData
        ]);
	}
	/**
	 * @Des: 获取广告主列表
	 * @Edt: yuhou.wang
	 * @param {type} 
	 * @return: 
	 */
	public function getAdownerList(){
        header('Access-Control-Allow-Origin:*');//允许跨域
		$data = A('Common/Adowner','Model')->getList();
		$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$data]);
	}
}