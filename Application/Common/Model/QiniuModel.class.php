<?php
namespace Common\Model;

use Qiniu\Auth;
use Qiniu\Storage\UploadManager;
use Qiniu\Storage\BucketManager;
use Qiniu\Config;
use Qiniu\Zone;
use Qiniu\Processing\PersistentFop;


import('Vendor.qiniu_sdk.autoload', '', '.php');//载入七牛sdk

class QiniuModel {
    private static $instance;
	
	/*设置文件的有效期*/
	function buildBatchDeleteAfterDays($bucket,$key,$days){
		
		
		$config = new \Qiniu\Config();
		$accessKey = C('QINIU_ACCESSKEY');
        $secretKey = C('QINIU_SECRETKEY');

		
		$auth = new Auth($accessKey, $secretKey);
		
		$bucketManager = new BucketManager($auth, $config);

		$ops = $bucketManager->buildBatchDeleteAfterDays($bucket, array($key=>$days));
		list($ret, $err) = $bucketManager->batch($ops);
		//file_put_contents('ret.log',json_encode($ret));
		//file_put_contents('err.log',json_encode($err));
		
		if ($err) {
			print_r($err);
		} else {
			return true;
		}
	}
	
	public function qiniu_compress(){
		
		$bucket = C('QINIU_BUCKET');
        $accessKey = C('QINIU_ACCESSKEY');
        $secretKey = C('QINIU_SECRETKEY');
		
		$auth = new Auth($accessKey, $secretKey);
		
		
		//要转码的文件所在的空间和文件名。
		$key = '00167e851a579d265ca46f76e6e2d397.jpg';

		$config = new \Qiniu\Config();

		$pfop = new PersistentFop($auth, $config);

		
		$fops = 'mkzip/2/url/' . \Qiniu\base64_urlSafeEncode('http://or7inlt8f.bkt.clouddn.com/00167e851a579d265ca46f76e6e2d397.jpg') . '/url/'. \Qiniu\base64_urlSafeEncode('http://or7inlt8f.bkt.clouddn.com/007a705b6090aadfd25f45b703ac9677.jpg') . '';
		
		
		list($id, $err) = $pfop->execute($bucket, $key, $fops);


		var_dump($id);
		var_dump($err);
		
		
	}
	
	
	
	
    /*获取上传文件到七牛的token*/
    public function qiniu_updload_token($prefix = ''){
        $bucket = C('QINIU_BUCKET');
        $accessKey = C('QINIU_ACCESSKEY');
        $secretKey = C('QINIU_SECRETKEY');
        $auth = new Auth($accessKey, $secretKey);
        $policy = array(
            'saveKey' => $prefix.'$(etag)$(ext)',
			//'scope'	=>C('QINIU_BUCKET').':0',
        );
        $token = $auth->uploadToken($bucket, null, 3600, $policy);
        return $token;
    }
	
	/*上传crossdomain.xml文件到七牛云*/
	public function up_crossdomain($bucket){
		
        $accessKey = C('QINIU_ACCESSKEY');
        $secretKey = C('QINIU_SECRETKEY');
        $auth = new Auth($accessKey, $secretKey);

        $token = $auth->uploadToken($bucket, null, 3600, $policy);
		
		$uploadMgr = new UploadManager();
		
		$rr = $uploadMgr->putFile($token, 'crossdomain.xml', 'crossdomain.xml');// 调用 UploadManager 的 putFile 方法进行文件的上传。
		return $rr;
	}
	
	
	/*上传base64的图片的token*/
	public function qiniu_up_base64_img_token(){
		$bucket = C('QINIU_BUCKET');
        $accessKey = C('QINIU_ACCESSKEY');
        $secretKey = C('QINIU_SECRETKEY');
        $auth = new Auth($accessKey, $secretKey);
        $policy = array(
            'saveKey' => $prefix.'paperimg/$(year)$(mon)$(day)$(hour)$(min)$(sec)'.rand(100,999).'.jpg',
        );
        $token = $auth->uploadToken($bucket, null, 3600, $policy);
        return $token;
	}
	
	/*抓取第三方URL*/
	public function fetch($url,$key){
		$bucket = C('QINIU_BUCKET');
		$auth = new Auth(C('QINIU_ACCESSKEY'), C('QINIU_SECRETKEY'));
		$bucketManager = new BucketManager($auth);
		$extension_name = pathinfo($url, PATHINFO_EXTENSION);//扩展名
		if($extension_name != ''){
			$extension_name = '.'.$extension_name;
		} 
		if($key == ''){
			$key = md5($url) . $extension_name;// 指定抓取的文件保存名称
		}
		list($ret, $err) = $bucketManager->fetch($url, $bucket, $key);
		if ($err !== null) {
			//var_dump($err);
			return false;
		} else {
			$key_url = C('QINIU_ENDPOINT').$key;
			return $key_url;
		}
	}
	
	/*上传文件到七牛*/
	function qiniu_up($filePath,$key){

		$config = new Config();

		
		$uptoken = $this->qiniu_updload_token();


		$uploadMgr = new UploadManager($config);

		list($ret, $err) = $uploadMgr->putFile($uptoken, $key, $filePath);

		if ($err !== null) {
			var_dump($err);
		} else {
			
			return C('QINIU_ENDPOINT').$key;
		}
	}
	
	/*上传字符串到七牛*/
	function qiniu_up_str($fileStr,$key){

		$config = new Config();

		
		$uptoken = $this->qiniu_updload_token();


		$uploadMgr = new UploadManager($config);

		
		list($ret, $err) = $uploadMgr->put($uptoken, $key, $fileStr);

		if ($err !== null) {
			var_dump($err);
		} else {
			
			return C('QINIU_ENDPOINT').$key;
		}
	}
	
	/*批量删除文件*/
	function delete_keys($keys){

		$config = new Config();
		$accessKey = C('QINIU_ACCESSKEY');
        $secretKey = C('QINIU_SECRETKEY');
        $auth = new Auth($accessKey, $secretKey);
		$bucket = C('QINIU_BUCKET');
		$bucketManager = new \Qiniu\Storage\BucketManager($auth, $config);

		
		$ops = $bucketManager->buildBatchDelete($bucket, $keys);
		list($ret, $err) = $bucketManager->batch($ops);

		if ($err !== null) {
			var_dump($err);
		} else {
			
			return true;
		}
	}
	
	
	
	
	
	public function get_list_files($bucket,$marker,$limit,$prefix = null){
		
		
		$auth = new Auth(C('QINIU_ACCESSKEY'), C('QINIU_SECRETKEY'));
		$bucketManager = new BucketManager($auth);
		
		

		

		$delimiter = null;
		
		list($ret, $err) = $bucketManager->listFiles($bucket, $prefix, $marker, $limit, $delimiter);
		if ($err !== null) {
			var_dump('error:');
			var_dump($err);
		} else {
			//var_dump('success:');
			return $ret;

		}
		

	}
	
	
	/*新增生命周期规则*/
	public function rules_add($bucket,$prefix,$save_days){
		
        $accessKey = C('QINIU_ACCESSKEY');
        $secretKey = C('QINIU_SECRETKEY');
		
		
		$auth = new Auth($accessKey,$secretKey);
		$url = "http://uc.qbox.me/rules/add";
		
		
		
		$body = 'bucket='.$bucket.'&name='.$prefix.'&prefix='.$prefix.'&delete_after_days='.$save_days.'&to_line_after_days=0';
		$sign = $auth->authorization($url, $body, "application/x-www-form-urlencoded");
		
		$header = array('Content-Type: application/x-www-form-urlencoded','Authorization: '.$sign["Authorization"]);

		$rr = http($url,$body,'POST',$header);//请求
		$rr = json_decode($rr,true);
		
		if($rr['error'] == 'rule name exists'){
			
			$url = "http://uc.qbox.me/rules/update";
			
			$body = 'bucket='.$bucket.'&name='.$prefix.'&prefix='.$prefix.'&delete_after_days='.$save_days.'&to_line_after_days=0';
			
			$sign = $auth->authorization($url, $body, "application/x-www-form-urlencoded");
			
			$header = array('Content-Type: application/x-www-form-urlencoded','Authorization: '.$sign["Authorization"]);

			$rr = http($url,$body,'POST',$header);//请求
			$rr = json_decode($rr,true);
		}
		//var_dump($rr);

		return true;
		
	}
	
	
	/*删除生命周期规则*/
	public function rules_delete($bucket,$prefix){
		$accessKey = C('QINIU_ACCESSKEY');
        $secretKey = C('QINIU_SECRETKEY');
		
		
		$auth = new Auth($accessKey,$secretKey);
		$url = "http://uc.qbox.me/rules/delete";
		
		
		
		$body = 'bucket='.$bucket.'&name='.$prefix;
		$sign = $auth->authorization($url, $body, "application/x-www-form-urlencoded");
		
		$header = array('Content-Type: application/x-www-form-urlencoded','Authorization: '.$sign["Authorization"]);

		$rr = http($url,$body,'POST',$header);//请求
		$rr = json_decode($rr,true);
		
		if(!$rr){
			return true;
		}else{
			return false;
		}
	}	
	

    public static function __callStatic($method, $arg){
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance->$method($arg);
    }

}