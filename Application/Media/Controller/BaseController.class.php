<?php

namespace Media\Controller;
use Think\Controller;

class BaseController extends Controller {


	public function _initialize() {
		
		$direct = array(//设置无须验证的页面
					'Media/Login/ajax_login',
					'Media/Index/index',
					'Media/Login/index',
					'Media/Login/verify',

					
				
						);

		if(!session('personInfo.fid') && !in_array(MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME ,$direct)){
			$this->ajaxReturn(array('code'=>-2,'msg'=>'need log in again'));
		}
	}
}