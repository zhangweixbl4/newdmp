<?php
namespace Agp\Controller;
use Think\Controller;
use Agp\Model\StatisticalReportModel;
use Agp\Model\StatisticalModel;
class OutWordNJController extends BaseController{

    /**
     * 生成opensearch数据监测日报（南京）
     * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
     * by zw
     */
    public function create_daypresentation(){
        session_write_close();
        header("Content-type:text/html;charset=utf-8");
        $system_num = getconfig('system_num');
        $usermedia = M('tmedia_temp')->where('ftype=1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid'))->getField('fmediaid',true);

        $daytime = I('daytime')?I('daytime'):date('Y-m-d');//报告时间
        $fid     = I('fid',1);//机构ID

        $regulatorpersonInfop = session('regulatorpersonInfo');
        if($fid != 'netad'){
            $where_tr['fid']    = $fid;
            $where_tr['fstate'] = 1;
            $do_tr = M('tmediaowner')->field('fname')->where($where_tr)->find();//获取媒介机构名称

            $where_ma['fid'] = array('in',$usermedia);
            $where_ma['fmediaownerid']  = $fid;
            $do_ma = M('tmedia')->field('fid,fmediaclassid, (case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')->where($where_ma)->select();//获取媒体信息
        }else{
            $do_tr["fname"] = "南京互联网媒介";
            $where_ma['fmediaclassid'] = '13';
            $where_ma['tmediaowner.fregionid'] = array('like',substr($regulatorpersonInfop['regionid'],0,4)."%");
            $do_ma = M('tmedia')->field('tmedia.fid,tmedia.fmediaclassid, (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame')
                ->join("tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid")
                ->where($where_ma)
                ->select();//获取媒体信息
        }
        if(substr($do_ma[0]['fmediaclassid'],0,2) == '01' || substr($do_ma[0]['fmediaclassid'],0,2) == '02'){
            $html = '<div align="center" style="font-size:18px; font-weight:bold; padding:10px;">'.$do_tr["fname"].date("Y.m.d",strtotime($daytime)).'涉嫌发布违法广告情况表</div>
            <table style="width:100%;border-top:1px solid #666666; border-left:1px solid #666666; margin-top:20px;" cellpadding="6" cellspacing="0" align="center" class="tb">
            <tr>
             <td>序号</td>
             <td>媒体名称</td>
             <td>时长（秒）</td>
             <td>广告发布数（条次）</td>
             <td>违法数</td>
             <td>总违法率</td>
             <td>违法广告明细</td>
             <td>涉嫌违法点</td>
            </tr>';
        }else{
            $html = '<div align="center" style="font-size:18px; font-weight:bold; padding:10px;">'.$do_tr["fname"].date("Y.m.d",strtotime($daytime)).'涉嫌发布违法广告情况表</div>
            <table style="width:100%;border-top:1px solid #666666; border-left:1px solid #666666; margin-top:20px;" cellpadding="6" cellspacing="0" align="center" class="tb">
            <tr>
             <td>序号</td>
             <td>媒体名称</td>
             <td>广告发布数（条次）</td>
             <td>违法数</td>
             <td>总违法率</td>
             <td>违法广告明细</td>
             <td>涉嫌违法点</td>
            </tr>';
        }
        
        foreach ($do_ma as $key => $value) {
            $mediaids[] = $value['fid'];
            $mediadata[$value['fid']]['fmediaid'] = $value['fid'];
            $mediadata[$value['fid']]['fmedianame'] = $value['fmedianame'];
            $mediadata[$value['fid']]['ztccount'] = 0 ;
            $mediadata[$value['fid']]['zsccount'] = 0 ;
            $mediadata[$value['fid']]['wftccount'] = 0 ;
            $mediadata[$value['fid']]['illAdIds'] = [];
            $mediadata[$value['fid']]['illAd'] = [];
            $mediadata[$value['fid']]['illAdExpressions'] = [];
        }

        //广告总量
        $where_sy['fmediaid'] = ['in',$mediaids];
        $where_sy['a.fdate'] = $daytime;
        $where_sy['a.fcustomer'] = ['in',[0,$system_num]];
        $do_sy = M('tbn_ad_summary_day_v3')
            ->alias('a')
            ->field('fmediaid,sum(fad_times_long_ad) ztccount,sum(fad_play_len_long_ad) zsccount')
            ->where($where_sy)
            ->group('fmediaid')
            ->select();
        foreach ($do_sy as $key => $value) {
            $mediadata[$value['fmediaid']]['ztccount'] = $value['ztccount'];
            $mediadata[$value['fmediaid']]['zsccount'] = $value['zsccount'];
        }

        //违法广告量
        $where_wf['n.fissue_date'] = $daytime;
        $where_wf['m.fcustomer'] = $system_num;
        $where_wf['m.fstatus3'] = ['notin',[30,40]];
        $where_wf['m.fmedia_id'] = ['in',$mediaids];
        $do_wf = M('tbn_illegal_ad')
            ->alias('m')
            ->field('m.fmedia_id,m.fid,m.fad_name,m.fexpressions')
            ->join('tbn_illegal_ad_issue n on m.fid = n.fillegal_ad_id ')
            ->where($where_wf)
            ->select();
        foreach ($do_wf as $key => $value) {
            $mediadata[$value['fmedia_id']]['wftccount'] += 1;
            if(!in_array($value['fid'], $mediadata[$value['fmedia_id']]['IllAdIds'])){
                $mediadata[$value['fmedia_id']]['illAdIds'][] = $value['fid'];
                $mediadata[$value['fmedia_id']]['illAd'][] = $value['fad_name'];
                $mediadata[$value['fmedia_id']]['illAdExpressions'][] = $value['fexpressions'];
            }
        }

        $zsccount    = 0;
        $ztccount    = 0;
        $wftccount   = 0;
        foreach ($mediadata as $key => $value) {
            $allcount   = $allcount+1;
            $html .= '<tr>
            <td>'.$allcount.'</td>
            <td>'.$value['fmedianame'].'</td>';
            if(substr($do_ma[0]['fmediaclassid'],0,2) == '01' || substr($do_ma[0]['fmediaclassid'],0,2) == '02'){
                $html .= '<td>'.$value['zsccount'].'</td>';
            }
            $html .= '
            <td>'.$value['ztccount'].'</td>
            <td>'.$value['wftccount'].'</td>
            <td>'.round($value['wftccount']/$value['ztccount']*100,2).'%</td>
            <td>'.implode('<br>', $value['illAd']).'</td>
            <td>'.implode('。<br>', $value['illAdExpressions']).'</td>
            ';

            //合计数据
            $zsccount   = $zsccount+$value['zsccount'];
            $ztccount   = $ztccount+$value['ztccount'];
            $wftccount  = $wftccount+$value['wftccount'];
        }
        
        $html .= '<tr>
        <td colspan="2">合计</td>';
        if(substr($do_ma[0]['fmediaclassid'],0,2) == '01' || substr($do_ma[0]['fmediaclassid'],0,2) == '02'){
            $html .= '<td>'.$zsccount.'</td>';
        }
        $html .= '
        <td>'.$ztccount.'</td>
        <td>'.$wftccount.'</td>
        <td>'.round($wftccount/$ztccount*100,2).'%</td>
        <td></td>
        <td></td>
        ';
        $html .= '</table>';

        $date = date('Ymdhis');
        $savefilename = $date.'.doc';
        $savefile = './Public/Word/'.$date.'.doc';

        //将文档保存
        word_start();
        echo $html;
        word_save($savefile);
        ob_flush();//每次执行前刷新缓存
        flush();

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.doc',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

        //将生成记录保存
        $pnname = $do_tr['fname'].date('Y.m.d',strtotime($daytime)).'涉嫌发布违法广告情况表';
        $data['pnname']             = $pnname;
        $data['pntype']             = 10;
        $data['pnfiletype']         = 10;
        $data['pnstarttime']        = $daytime;
        $data['pncreatetime']       = date('Y-m-d H:i:s');
        $data['pnurl']              = $ret['url'];
        $data['pnhtml']             = $html;
        $data['pntrepid']   = session('regulatorpersonInfo.fregulatorpid');
        $data['pncreatepersonid']   = session('regulatorpersonInfo.fid');
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
        }
    }

	/**
	 * 生成监测日报（南京）
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	 */
    public function create_daypresentation2(){
        session_write_close();
        header("Content-type:text/html;charset=utf-8");
        $system_num = getconfig('system_num');
        $usermedia = M('tmedia_temp')->where('ftype=1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid'))->getField('fmediaid',true);

        $daytime = I('daytime')?I('daytime'):date('Y-m-d');//报告时间

        $fid 	 = I('fid',1);

        $tb_tvissue_nowmonth = gettable('tv',strtotime($daytime));

        $tb_tvissue_nevmonth = gettable('tv',strtotime("$daytime -1 month"));

        $tb_bcissue_nowmonth = gettable('bc',strtotime($daytime));

        $tb_bcissue_nevmonth = gettable('bc',strtotime("$daytime -1 month"));

        $where 		= ' where 1=1';
        $regulatorpersonInfop = session('regulatorpersonInfo');
        $db_tr 		= M('tmediaowner');
        $db_ma = M('tmedia');
        if($fid != 'netad'){
            $where_tr['fid'] 	= $fid;
            $where_ma['fid'] = array('in',$usermedia);
            $where_tr['fstate'] = 1;
            $do_tr = $db_tr->field('fname')->where($where_tr)->find();//获取媒介机构名称
            $where_ma['fmediaownerid'] 	= $fid;
            $do_ma = $db_ma->field('fid,fmediaclassid, (case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')->where($where_ma)->select();//获取媒体信息
            //dump($do_ma);exit;
        }else{
            $do_tr["fname"] = "南京互联网媒介";
            $where_ma['fmediaclassid'] = '13';
            $where_ma['tmediaowner.fregionid'] = array('like',substr($regulatorpersonInfop['regionid'],0,4)."%");
            $do_ma = $db_ma->field('tmedia.fid,tmedia.fmediaclassid, (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame')
                ->join("tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid")
                ->where($where_ma)
                ->select();//获取媒体信息
        }
        $html = '<div align="center" style="font-size:18px; font-weight:bold; padding:10px;">'.$do_tr["fname"].date("Y.m.d",strtotime($daytime)).'涉嫌发布违法广告情况表</div>
			<table style="width:100%;border-top:1px solid #666666; border-left:1px solid #666666; margin-top:20px;" cellpadding="6" cellspacing="0" align="center" class="tb">
			<tr>
			 <td>序号</td>
			 <td>媒体名称</td>
			 <td>时长（秒）</td>
			 <td>广告发布数（条次）</td>
			 <td>违法数</td>
			 <td>总违法率</td>
			 <td>违法广告明细</td>
			 <td>涉嫌违法点</td>
			</tr>';

        /*			<tr>
				<td  >一般违法</td>
				<td  >严重违法</td>
				<td  >严重违法率</td>
				<td  >违法合计</td>
			</tr>*/
        $ggscmj 	= 0;
        $ggsl 		= 0;
        $ybwfggsl 	= 0;
        $yzwfggsl 	= 0;
        $ggslyzwfl 	= 0;
        $wfggsl 	= 0;
        $ggslwfl 	= 0;
        $is_paper = 0;
        foreach ($do_ma as $key => $value2) {
            $where ='';
            $where .= ' and a.fmediaid='.$value2['fid'];

            if(substr($value2['fmediaclassid'],0,2)=='01'){//如果媒体类型是电视
                $mediatype = 'tv';
            }elseif(substr($value2['fmediaclassid'],0,2)=='02'){//如果媒体类型是广播
                $mediatype = 'bc';
            }elseif(substr($value2['fmediaclassid'],0,2)=='03'){//如果媒体类型是报纸
                $mediatype = 'paper';
                $is_paper++;
            }elseif(substr($value2['fmediaclassid'],0,2)=='13'){//如果媒体类型是互联网
                $mediatype = 'net';
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
            }

            if($mediatype == 'tv'){
                $where .= ' and datediff("'.$daytime.'",FROM_UNIXTIME(a.fissuedate, "%Y-%m-%d"))=0 ';
                $sql = 'select
					FROM_UNIXTIME(a.fissuedate,"%Y-%m-%d") as fbtime,
					m.fid as fmediaid,
					sum(flength) as ggscmj,
					count(*) as ggsl,
					sum(case when b.fillegaltypecode=20 then 1 else 0 end) as ybwfggsl,
					sum(case when b.fillegaltypecode=30 then 1 else 0 end) as yzwfggsl,
					ifnull(sum(case when b.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl,
					sum(case when b.fillegaltypecode>0 then 1 else 0 end) as wfggsl,
					ifnull(sum(case when b.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl
					from tmedia m,((select fsampleid,fmediaid,flength,fissuedate from '.$tb_tvissue_nevmonth.') union all (select fsampleid,fmediaid,flength,fissuedate from '.$tb_tvissue_nowmonth.')) as a,ttvsample b
					where m.fid=a.fmediaid and a.fsampleid=b.fid '.$where.'
					group by fmediaid,fbtime ';
            }elseif($mediatype == 'bc'){
                $where .= ' and datediff("'.$daytime.'",FROM_UNIXTIME(a.fissuedate, "%Y-%m-%d"))=0 ';

                $sql = 'select
					FROM_UNIXTIME(a.fissuedate,"%Y-%m-%d") as fbtime,
					m.fid as fmediaid,
					sum(flength) as ggscmj,
					count(*) as ggsl,
					sum(case when b.fillegaltypecode=20 then 1 else 0 end) as ybwfggsl,
					sum(case when b.fillegaltypecode=30 then 1 else 0 end) as yzwfggsl,
					ifnull(sum(case when b.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl,
					sum(case when b.fillegaltypecode>0 then 1 else 0 end) as wfggsl,
					ifnull(sum(case when b.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl
					from tmedia m,((select fsampleid,fmediaid,flength,fissuedate from '.$tb_bcissue_nevmonth.') union all (select fsampleid,fmediaid,flength,fissuedate from '.$tb_bcissue_nowmonth.')) as a,tbcsample b
					where m.fid=a.fmediaid and a.fsampleid=b.fid '.$where.'
					group by fmediaid,fmedianame,fbtime ';
            }elseif($mediatype == 'paper'){
                $where .= ' and datediff("'.$daytime.'",FROM_UNIXTIME(UNIX_TIMESTAMP(a.fissuedate),"%Y-%m-%d"))=0 ';
                $sql = 'select
					FROM_UNIXTIME(UNIX_TIMESTAMP(a.fissuedate),"%Y-%m-%d") as fbtime,
					m.fid as fmediaid,
					0 as ggscmj,
					count(*) as ggsl,
					sum(case when b.fillegaltypecode=20 then 1 else 0 end) as ybwfggsl,
					sum(case when b.fillegaltypecode=30 then 1 else 0 end) as yzwfggsl,
					ifnull(sum(case when b.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl,
					sum(case when b.fillegaltypecode>0 then 1 else 0 end) as wfggsl,
					ifnull(sum(case when b.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl
					from tmedia m,tpaperissue a,tpapersample b
					where m.fid=a.fmediaid and a.fpapersampleid=b.fpapersampleid '.$where.'
					group by fmediaid,fbtime ';
            }elseif($mediatype == 'net'){
                $sql = 'select
					FROM_UNIXTIME(a.net_created_date/1000,"%Y-%m-%d") as fbtime,
					a.fmediaid,
					0 as ggscmj,
					count(*) as ggsl,
					sum(case when b.fillegaltypecode=20 then 1 else 0 end) as ybwfggsl,
					sum(case when b.fillegaltypecode=30 then 1 else 0 end) as yzwfggsl,
					ifnull(sum(case when b.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl,
					sum(case when b.fillegaltypecode>0 then 1 else 0 end) as wfggsl,
					ifnull(sum(case when b.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl
					from tmedia m,tnetissueputlog a,tnetissue b
					where m.fid=a.fmediaid and a.fmediaid='.$value2['fid'].'  and finputstate=2 and datediff("'.$daytime.'",FROM_UNIXTIME(a.net_created_date/1000))=0 group by a.fmediaid';//互联网统计数据
            }//根据媒体类型查询结果
            $do_ie = M()->query($sql);//获取统计结果
            if(!empty($do_ie[0]['ggsl'])){
                $addhtml 	= '';
                if($mediatype == 'tv'){
                    $sesql = 'select
                        b.fadname,
                        a.fexpressions
                        from ttvsample a join (select fsampleid from ((select fsampleid,fmediaid,fissuedate from '.$tb_tvissue_nevmonth.') union all (select fsampleid,fmediaid,fissuedate from '.$tb_tvissue_nowmonth.')) as x where x.fmediaid='.$value2['fid'].' and datediff("'.$daytime.'",FROM_UNIXTIME(x.fissuedate))=0 group by x.fsampleid) as c on a.fid=c.fsampleid join tad b on a.fadid=b.fadid and b.fadid<>0 where a.fillegaltypecode>0';


                    $do_se = M()->query($sesql);//获取所有违法信息
                    if(!empty($do_se)){
                        $addfadname = '';
                        $secount = count($do_se)?count($do_se):1;
                        foreach ($do_se as $key3 => $value3) {
                            if(!strstr($addfadname,$value3['fadname'])){
                                $addfadname .= $value3['fadname'].'<br />';
                            }
                            $addfexpressions .= $value3['fexpressions'].'<br />';
                        }
                        $addhtml .= '<td>'.$addfadname.'</td>';
                        $addhtml .= '<td>'.$addfexpressions.'</td></tr>';

                    }else{
                        $addhtml .= '<td></td>';
                        $addhtml .= '<td></td></tr>';
                        $secount = 1;
                    }
                }elseif($mediatype == 'bc'){
                    $sesql = 'select b.fadname,a.fexpressions 
                              from tbcsample a 
                              join (
                                select fsampleid,fmediaid,fissuedate 
                                from (
                                  (
                                    select fsampleid,fmediaid,fissuedate 
                                    from '.$tb_bcissue_nevmonth.') 
                                    union all (
                                        select fsampleid,fmediaid,fissuedate 
                                        from '.$tb_bcissue_nowmonth.')
                                    ) as x where x.fmediaid='.$value2['fid'].' 
                                    and datediff("'.$daytime.'",FROM_UNIXTIME(x.fissuedate))=0 
                                    group by x.fsampleid) as c on a.fid=c.fsampleid 
                                    join tad b 
                                    on a.fadid=b.fadid and b.fadid<>0 
                                    where a.fillegaltypecode>0';
                    $do_se = M()->query($sesql);//获取所有违法信息
                    if(!empty($do_se)){
                        $addfadname = '';
                        $secount = count($do_se)?count($do_se):1;
                        foreach ($do_se as $key3 => $value3) {
                            if(!strstr($addfadname,$value3['fadname'])){
                                $addfadname .= $value3['fadname'].'<br />';
                            }
                            $addfexpressions .= $value3['fexpressions'].'<br />';
                        }
                        $addhtml .= '<td>'.$addfadname.'</td>';
                        $addhtml .= '<td>'.$addfexpressions.'</td></tr>';

                    }else{
                        $addhtml .= '<td></td>';
                        $addhtml .= '<td></td></tr>';
                        $secount = 1;
                    }
                }elseif($mediatype == 'paper'){
                    $sesql ='SELECT
                                b.fadname,
                                a.fexpressions
                            FROM
                                tpapersample a
                            JOIN (
                                SELECT
                                    fpapersampleid,
                                    fmediaid,
                                    fissuedate
                                FROM
                                    tpaperissue b
                                WHERE
                                    fmediaid = '.$value2['fid'].'
                                AND datediff(
                                    "'.$daytime.'",
                                    FROM_UNIXTIME(UNIX_TIMESTAMP(b.fissuedate),"%Y-%m-%d")
                                ) = 0
                                GROUP BY
                                    b.fpapersampleid
                            ) AS c ON a.fpapersampleid = c.fpapersampleid
                            JOIN tad b ON a.fadid = b.fadid and b.fadid<>0
                            WHERE
                                a.fillegaltypecode > 0';
                    $do_se = M()->query($sesql);//获取所有违法信息
                    if(!empty($do_se)){
                        $addfadname = '';
                        $secount = count($do_se)?count($do_se):1;
                        foreach ($do_se as $key3 => $value3) {
                            if(!strstr($addfadname,$value3['fadname'])){
                                $addfadname .= $value3['fadname'].'<br />';
                            }
                            $addfexpressions .= $value3['fexpressions'].'<br />';
                        }
                        $addhtml .= '<td>'.$addfadname.'</td>';
                        $addhtml .= '<td>'.$addfexpressions.'</td></tr>';

                    }else{
                        $addhtml .= '<td></td>';
                        $addhtml .= '<td></td></tr>';
                        $secount = 1;
                    }
                }elseif($mediatype == 'net'){
                    $sesql = 'select fadname,fexpressioncodes from tnetissue where fillegaltypecode>0  and finputstate=2 group by fadname,fexpressioncodes';
                    $do_se = M()->query($sesql);//获取所有违法信息
                    if(!empty($do_se)){
                        $addfadname = '';
                        $secount = count($do_se)?count($do_se):1;
                        foreach ($do_se as $key3 => $value3) {

                            if(!strstr($addfadname,$value3['fadname'])){
                                $addfadname .= $value3['fadname'].'<br />';
                            }
                            if(!empty($value3['fexpressioncodes'])){
                                $do_tl = M('tillegal')->where(['_string'=>'fcode in ('.$value3['fexpressioncodes'].')','fstate'=>1])->getField('fexpression',true);
                                foreach ($do_tl as $value) {
                                    $addfexpressions .= $value.';<br />';
                                }
                            }else{
                                $addfexpressions = '';
                            }
                        }
                        $addhtml .= '<td>'.$addfadname.'</td>';
                        $addhtml .= '<td>'.$addfexpressions.'</td></tr>';

                    }else{
                        $addhtml .= '<td></td>';
                        $addhtml .= '<td></td></tr>';
                        $secount = 1;
                    }
                }

                //合计数据
                $ggscmj 	= $ggscmj+$do_ie[0]['ggscmj'];
                $ggsl 		= $ggsl+$do_ie[0]['ggsl'];
                $ybwfggsl 	= $ybwfggsl+$do_ie[0]['ybwfggsl'];
                $yzwfggsl 	= $yzwfggsl+$do_ie[0]['yzwfggsl'];
                $ggslyzwfl 	= $ggslyzwfl+$do_ie[0]['ggslyzwfl']*100;
                $wfggsl 	= $wfggsl+$do_ie[0]['wfggsl'];
                $ggslwfl 	= $ggslwfl+$do_ie[0]['ggslwfl']*100;
                $allcount 	= $allcount+1;

                if($mediatype == 'paper'){
                    $html .= '<tr>
					<td>'.$allcount.'</td>
					<td>'.$value2['fmedianame'].'</td>
					<td>'.$do_ie[0]['ggsl'].'</td>
					<td>'.$do_ie[0]['wfggsl'].'</td>
					<td>'.round($do_ie[0]['ggslwfl']*100,2).'%</td>
				'.$addhtml;
                }else{
                    $html .= '<tr>
					<td>'.$allcount.'</td>
					<td>'.$value2['fmedianame'].'</td>
					<td>'.$do_ie[0]['ggscmj'].'</td>
					<td>'.$do_ie[0]['ggsl'].'</td>
					<td>'.$do_ie[0]['wfggsl'].'</td>
					<td>'.round($do_ie[0]['ggslwfl']*100,2).'%</td>
				'.$addhtml;
                }


            }

        }

        /*		 rowspan="'.$secount.'"			<td rowspan="'.$secount.'">'.$do_ie[0]['ybwfggsl'].'</td>
					<td rowspan="'.$secount.'">'.$do_ie[0]['yzwfggsl'].'</td>
					<td rowspan="'.$secount.'">'.round($do_ie[0]['ggslyzwfl']*100,2).'%</td>

        				<td>'.$ybwfggsl.'</td>
				<td>'.$yzwfggsl.'</td>
				<td>'.round(($ggslyzwfl/$allcount),2).'%</td>


        */

        if(!empty($allcount)){
            if($is_paper != 0 && $is_paper == count($do_ma)){
                $html = str_replace('<td>时长（秒）</td>','',$html);
                $html .= '<tr>
				<td colspan="2">合计</td>
				<td>'.$ggsl.'</td>
				<td>'.$wfggsl.'</td>
				<td>'.round(($ggslwfl/$allcount),2).'%</td>
				<td></td>
				<td></td>
			';
            }else{
                $html .= '<tr>
				<td colspan="2">合计</td>
				<td>'.$ggscmj.'</td>
				<td>'.$ggsl.'</td>
				<td>'.$wfggsl.'</td>
				<td>'.round(($ggslwfl/$allcount),2).'%</td>
				<td></td>
				<td></td>
			';
            }

        }else{
            if($is_paper){
                $html = str_replace('<td>时长（秒）</td>','',$html);
                $html .= '<tr>
				<td colspan="2">合计</td>
				<td>0</td>
				<td>0</td>
				<td>0.00%</td>
				<td></td>
				<td></td>
			';
            }else{
                $html .= '<tr>
				<td colspan="2">合计</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>0.00%</td>
				<td></td>
				<td></td>
			';
            }

        }
        $html .= '</table>';

        $date = date('Ymdhis');
        $savefilename = $date.'.doc';
        $savefile = './Public/Word/'.$date.'.doc';

        //将文档保存
        word_start();
        echo $html;
        word_save($savefile);
        ob_flush();//每次执行前刷新缓存
        flush();

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.doc',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

        $system_num = getconfig('system_num');

        //将生成记录保存
        $pnname = $do_tr['fname'].date('Y.m.d',strtotime($daytime)).'涉嫌发布违法广告情况表';
        $data['pnname'] 			= $pnname;
        $data['pntype'] 			= 10;
        $data['pnfiletype'] 		= 10;
        $data['pnstarttime'] 		= $daytime;
        $data['pncreatetime'] 		= date('Y-m-d H:i:s');
        $data['pnurl'] 				= $ret['url'];
        $data['pnhtml'] 			= $html;
        $data['pntrepid']   = session('regulatorpersonInfo.fregulatorpid');
        $data['pncreatepersonid']   = session('regulatorpersonInfo.fid');
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
        }
    }

    /**
	 * 获取历史报告列表
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function index() {
		
		$p 					= I('page', 1);//当前第几页
		$pp 				= 20;//每页显示多少记录
		$pnname 			= I('pnname');// 报告名称
		$pntype 			= I('pntype');// 报告类型
		$pnfiletype 		= I('pnfiletype');// 文件类型
		$pncreatetime 		= I('pnendtime');//生成时间
        $system_num = getconfig('system_num');

		if (!empty($pnname)) {
			$where['pnname'] = array('like', '%' . $pnname . '%');//报告名称
		}
		if (!empty($pntype)) {
			$where['pntype'] = $pntype;//报告类型
		}
		if (!empty($pnfiletype)) {
			$where['pnfiletype'] = $pnfiletype;//文件类型
		}
		if(!empty($pncreatetime)){
            $where['pnendtime'] = array('between',array($pncreatetime[0],$pncreatetime[1]));
		}

        $where['pntrepid'] = session('regulatorpersonInfo.fregulatorpid');
		$where['fcustomer']  = $system_num;

		$count = M('tpresentation')
			->where($where)
			->count();

		
		$data = M('tpresentation')
			->field('pnid,pnname,pntype,pnfiletype,pnstarttime,pnendtime,pnurl')
			->where($where)
			->order('pncreatetime desc')
			->page($p,$pp)
			->select();
		if(!empty($data)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据'));
		}
	}

	/**
	 * 获取报告信息
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	 */
    public function get_pnview(){
        $system_num = getconfig('system_num');

        $where['pnid'] = I('pnid');
    	$where['fcustomer']  = $system_num;
    	$do_pn = M('tpresentation')
			->field('pnhtml')
			->where($where)
			->find();
		if(!empty($do_pn)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_pn));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据'));
		}
    }

    /**
	 * 获取南京的媒介机构
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	 */
    public function get_njtmedia(){
        $system_num = getconfig('system_num');
        $usermedia = M('tmedia_temp')->where('ftype=1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid'))->getField('fmediaid',true);
    	$where['tmedia.fstate'] 	= 1;
    	$where['tmediaowner.fstate'] 	= 1;
    	$where['tmedia.fid'] = array('in',$usermedia);
    	$do_tr = M('tmedia')->field('tmediaowner.fname,tmediaowner.fid')->join('tmediaowner on tmedia.fmediaownerid=tmediaowner.fid')->where($where)->group('tmediaowner.fname,tmediaowner.fid')->select();
    	 if(!empty($do_tr)){
        	$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_tr));
        }else{
        	$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
        }
    }

    public function create_netpresentation(){
        session_write_close();
        header("Content-type: text/html; charset=utf-8");
        $daytime = I('daytime')?I('daytime'):date('Y-m-d');//报告时间


        $where 		= ' where 1=1';

/*      $db_tr 		= M('tmediaowner');
        $where_tr['fid'] 	= $fid;
        $where_tr['isad'] = 1;
        $do_tr = $db_tr->field('fname')->where($where_tr)->find();//获取媒介机构名称*/

        $db_ma = M('tmedia');
        //$where_ma['fmediaownerid'] 	= $fid;
        $where_ma['fid'] 			= array('in',$usermedia);

        $do_ma = $db_ma->field('fid,fmediaclassid, (case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')->where($where_ma)->select();//获取媒体信息
        $html = '<div align="center" style="font-size:18px; font-weight:bold; padding:10px;">南京市互联网'.date("Y.m.d",strtotime($daytime)).'涉嫌发布违法广告情况表</div>
			<table style="width:100%;border-top:1px solid #666666; border-left:1px solid #666666; margin-top:20px;" cellpadding="6" cellspacing="0" align="center" class="tb">
			<tr>
			 <td rowspan=2  >序号</td>
			 <td rowspan=2  >媒体名称</td>
			 <td rowspan=2  >时长（秒）</td>
			 <td rowspan=2  >广告发布数（条次）</td>
			 <td colspan=4  style="border-bottom:1px solid #333333;">违法数</td>
			 <td rowspan=2  >总违法率</td>
			 <td rowspan=2  >违法广告明细</td>
			 <td rowspan=2  >涉嫌违法点</td>
			</tr>
			<tr>
				<td  >一般违法</td>
				<td  >严重违法</td>
				<td  >严重违法率</td>
				<td  >违法合计</td>
			</tr>

		';
        $ggscmj 	= 0;
        $ggsl 		= 0;
        $ybwfggsl 	= 0;
        $yzwfggsl 	= 0;
        $ggslyzwfl 	= 0;
        $wfggsl 	= 0;
        $ggslwfl 	= 0;
        foreach ($do_ma as $key => $value2) {
            $where ='';
            $where .= ' and datediff("'.$daytime.'",FROM_UNIXTIME(a.net_created_date, "%Y-%m-%d"))=0 ';
            $where .= ' and a.fmediaid='.$value2['fid'];

            $sql = 'select
                FROM_UNIXTIME(a.net_created_date/1000,"%Y-%m-%d") as fbtime,
                fmediaid,
                0 as ggscmj,
                count(*) as ggsl,
                sum(case when fillegaltypecode=20 then 1 else 0 end) as ybwfggsl,
                sum(case when fillegaltypecode=30 then 1 else 0 end) as yzwfggsl,
                ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl,
                sum(case when fillegaltypecode>0 then 1 else 0 end) as wfggsl,
                ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl
                from tmedia m,tnetissue a
                where m.fid=a.fmediaid and fmediaid='.$value2['fid'].' and datediff("'.$daytime.'",FROM_UNIXTIME(net_created_date/1000))=0 group by fmediaid,fbtime';

            $do_ie = M()->query($sql);//获取统计结果
            if(!empty($do_ie[0]['ggsl'])){
                $addhtml 	= '';
                if($mediatype == 'tv'){
                    $sesql = 'select
                        b.fadname,
                        a.fexpressions
                        from ttvsample a join (select fsampleid from ((select fsampleid,fmediaid,fissuedate from '.$tb_tvissue_nevmonth.') union all (select fsampleid,fmediaid,fissuedate from '.$tb_tvissue_nowmonth.')) as x where x.fmediaid='.$value2['fid'].' and datediff("'.$daytime.'",FROM_UNIXTIME(x.fissuedate))=0 group by x.fsampleid) as c on a.fid=c.fsampleid join tad b on a.fadid=b.fadid and b.fadid<>0 where a.fillegaltypecode>0';


                    $do_se = M()->query($sesql);//获取所有违法信息
                    if(!empty($do_se)){
                        $secount = count($do_se)?count($do_se):1;
                        foreach ($do_se as $key3 => $value3) {
                            if($key3>=1){
                                $addhtml .= '<tr>';
                            }
                            $addhtml .= '<td>'.$value3['fadname'].'</td>';
                            $addhtml .= '<td>'.$value3['fexpressions'].'</td></tr>';
                        }
                    }else{
                        $addhtml .= '<td></td>';
                        $addhtml .= '<td></td></tr>';
                        $secount = 1;
                    }
                }elseif($mediatype == 'bc'){
                    $sesql = 'select b.fadname,a.fexpressions 
                              from tbcsample a 
                              join (
                                select fsampleid,fmediaid,fissuedate 
                                from (
                                  (
                                    select fsampleid,fmediaid,fissuedate 
                                    from '.$tb_bcissue_nevmonth.') 
                                    union all (
                                        select fsampleid,fmediaid,fissuedate 
                                        from '.$tb_bcissue_nowmonth.')
                                    ) as x where x.fmediaid='.$value2['fid'].' 
                                    and datediff("'.$daytime.'",FROM_UNIXTIME(x.fissuedate))=0 
                                    group by x.fsampleid) as c on a.fid=c.fsampleid 
                                    join tad b 
                                    on a.fadid=b.fadid and b.fadid<>0 
                                    where a.fillegaltypecode>0';
                    $do_se = M()->query($sesql);//获取所有违法信息
                    if(!empty($do_se)){
                        $secount = count($do_se)?count($do_se):1;
                        foreach ($do_se as $key3 => $value3) {
                            if($key3>=1){
                                $addhtml .= '<tr>';
                            }
                            $addhtml .= '<td>'.$value3['fadname'].'</td>';
                            $addhtml .= '<td>'.$value3['fexpressions'].'</td></tr>';
                        }
                    }else{
                        $addhtml .= '<td></td>';
                        $addhtml .= '<td></td></tr>';
                        $secount = 1;
                    }
                }elseif($mediatype == 'paper'){
                    $sesql = 'select b.fadname,a.fexpressions from tpapersample a join (select fpapersampleid,fmediaid,fissuedate from tpaperissue where fmediaid='.$value2['fid'].' and datediff("'.$daytime.'",FROM_UNIXTIME(x.fissuedate))=0 group by x.fpapersampleid) as c on a.fpapersampleid=c.fpapersampleid join tad b on a.fadid=b.fadid and b.fadid<>0 where a.fillegaltypecode>0';
                    $do_se = M()->query($sesql);//获取所有违法信息
                    if(!empty($do_se)){
                        $secount = count($do_se)?count($do_se):1;
                        foreach ($do_se as $key3 => $value3) {
                            if($key3>=1){
                                $addhtml .= '<tr>';
                            }
                            $addhtml .= '<td>'.$value3['fadname'].'</td>';
                            $addhtml .= '<td>'.$value3['fexpressions'].'</td></tr>';
                        }
                    }else{
                        $addhtml .= '<td></td>';
                        $addhtml .= '<td></td></tr>';
                        $secount = 1;
                    }
                }elseif($mediatype == 'net'){
                    $sesql = 'select fadname,fexpressioncodes from tnetissue where fillegaltypecode>0 and finputstate=2 group by fadname,fexpressioncodes';
                    $do_se = M()->query($sesql);//获取所有违法信息
                    if(!empty($do_se)){
                        $secount = count($do_se)?count($do_se):1;
                        foreach ($do_se as $key3 => $value3) {
                            if($key3>=1){
                                $addhtml .= '<tr>';
                            }
                            if(!empty($value3['fexpressioncodes'])){
                                $do_tl = M('tillegal')->where(['_string'=>'fcode in ('.$value3['fexpressioncodes'].')','fstate'=>1])->getField('fexpression',true);
                                foreach ($do_tl as $value) {
                                    $fexpressioncodes .= $value.';';
                                }
                            }else{
                                $fexpressioncodes = '';
                            }
                            $addhtml .= '<td>'.$value3['fadname'].'</td>';
                            $addhtml .= '<td>'.$fexpressioncodes.'</td></tr>';
                        }
                    }else{
                        $addhtml .= '<td></td>';
                        $addhtml .= '<td></td></tr>';
                        $secount = 1;
                    }
                }

                //合计数据
                $ggscmj 	= $ggscmj+$do_ie[0]['ggscmj'];
                $ggsl 		= $ggsl+$do_ie[0]['ggsl'];
                $ybwfggsl 	= $ybwfggsl+$do_ie[0]['ybwfggsl'];
                $yzwfggsl 	= $yzwfggsl+$do_ie[0]['yzwfggsl'];
                $ggslyzwfl 	= $ggslyzwfl+$do_ie[0]['ggslyzwfl']*100;
                $wfggsl 	= $wfggsl+$do_ie[0]['wfggsl'];
                $ggslwfl 	= $ggslwfl+$do_ie[0]['ggslwfl']*100;
                $allcount 	= $allcount+1;

                $html .= '<tr>
					<td rowspan="'.$secount.'">'.$allcount.'</td>
					<td rowspan="'.$secount.'">'.$value2['fmedianame'].'</td>
					<td rowspan="'.$secount.'">'.$do_ie[0]['ggscmj'].'</td>
					<td rowspan="'.$secount.'">'.$do_ie[0]['ggsl'].'</td>
					<td rowspan="'.$secount.'">'.$do_ie[0]['ybwfggsl'].'</td>
					<td rowspan="'.$secount.'">'.$do_ie[0]['yzwfggsl'].'</td>
					<td rowspan="'.$secount.'">'.round($do_ie[0]['ggslyzwfl']*100,2).'%</td>
					<td rowspan="'.$secount.'">'.$do_ie[0]['wfggsl'].'</td>
					<td rowspan="'.$secount.'">'.round($do_ie[0]['ggslwfl']*100,2).'%</td>
				'.$addhtml;

            }

        }
        if(!empty($allcount)){
            $html .= '<tr>
				<td colspan="2">合计</td>
				<td>'.$ggscmj.'</td>
				<td>'.$ggsl.'</td>
				<td>'.$ybwfggsl.'</td>
				<td>'.$yzwfggsl.'</td>
				<td>'.round(($ggslyzwfl/$allcount),2).'%</td>
				<td>'.$wfggsl.'</td>
				<td>'.round(($ggslwfl/$allcount),2).'%</td>
				<td></td>
				<td></td>
			';
        }else{
            $html .= '<tr>
				<td colspan="2">合计</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>0.00%</td>
				<td>0</td>
				<td>0.00%</td>
				<td></td>
				<td></td>
			';
        }
        $html .= '</table>';

        $date = date('Ymdhis');
        $savefilename = $date.'.doc';
        $savefile = './Public/Word/'.$date.'.doc';

        //将文档保存
        word_start();
        echo $html;
        word_save($savefile);
        ob_flush();//每次执行前刷新缓存
        flush();

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.doc',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

        $system_num = getconfig('system_num');

        //将生成记录保存
        $pnname = $do_tr['fname'].date('Y.m.d',strtotime($daytime)).'涉嫌发布违法广告情况表';
        $data['pnname'] 			= $pnname;
        $data['pntype'] 			= 10;
        $data['pnfiletype'] 		= 10;
        $data['pnstarttime'] 		= $daytime;
        $data['pncreatetime'] 		= date('Y-m-d H:i:s');
        $data['pnurl'] 				= $ret['url'];
        $data['pnhtml'] 			= $html;
        $data['pntrepid']   = session('regulatorpersonInfo.fregulatorpid');
        $data['pncreatepersonid']   = session('regulatorpersonInfo.fid');
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
        }
    }

    /**
     * 生成广告监测报告月报
     * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
     * by zw
     */

    public function create_monthpresentation(){
        ini_set('max_execution_time','86400');
        set_time_limit(0);
        //$date = I('daytime','2018-07');
        $date = I('daytime',date("Y-m"));
        $focus = I('focus',0);
        $this->create_monthpresentation2($date,$focus,session('regulatorpersonInfo.fid'));
    }

    public function create_monthpresentation2($date,$focus,$userid) {

    header("Content-type: text/html; charset=utf-8");
    ini_set('memory_limit','1024M');
    ini_set('serialize_precision',-1);
    ini_set('max_execution_time','86400');
    set_time_limit(0);
    $report_start_date = strtotime($date);//指定月份月初时间戳
    $report_end_date = strtotime($date.'-'.date('t', strtotime($date)).' 23:59:59');
    $month_last = date('m',$report_start_date) - 1;
    $year_last = date('Y',$report_start_date);
    if($month_last == 0){
        $year_last = $year_last - 1;
    }
    //$report_end_date = mktime(23, 59, 59, date('m', strtotime($date))+1, 00);//指定月份月末时间戳

    if($report_end_date > time()){
        $report_end_date = time();
    }

    //获取上个周期的日期范围
    $last_date = date('Y-m',strtotime(date("Y",strtotime($date)).'-'.(date("m",strtotime($date))-1)));
    $last_s_time = strtotime($last_date);
    $last_e_time = strtotime($year_last.'-'.$month_last.'-'.date('t', strtotime($year_last.'-'.$month_last)).' 23:59:59');

    //获取同期的日期范围
    $last_year_date = date('Y-m',strtotime((date("Y",strtotime($date))-1).'-'.(date("m",strtotime($date)))));
    $last_year_s_time = strtotime($last_year_date);
    $last_year_e_time = mktime(23, 59, 59, date('m', strtotime($last_year_date))+1, 00,(date("Y",strtotime($date))-1));

    $date_ym = date('Y年m月',$report_start_date);//年月字符

    $regulatorpersonInfo = M('tregulatorperson')
        ->field('fregulatorid as bumen_id,fid')
        ->where('fid='.$userid)->find();
    if(!empty($regulatorpersonInfo)){
        $regulatorInfo = M('tregulator')->where(array('fid' => $regulatorpersonInfo['bumen_id']))->find();//监管机构信息
        $regulatorpersonInfo['fregulatorid']        = $regulatorInfo['fid'];//部门ID
        $regulatorpersonInfo['fregulatorpid']       = D('Function')->get_tregulatoraction($regulatorInfo['fid']);//获取当前单位所属机构ID
        $do_tr = M('tregulator')->field('tregulator.fname,tregion.fname as regionname,tregion.fname1 as regionname1,tregulator.fregionid')->join('tregion on tregulator.fregionid=tregion.fid')->where(array('tregulator.fid' => $regulatorpersonInfo['fregulatorpid']))->find();//获取机构信息
        $regulatorpersonInfo['regulatorpname']      = $do_tr['fname'];//所属机构名称
        $regulatorpersonInfo['regionid']            = $do_tr['fregionid'];//机构行政区划ID
        $regulatorpersonInfo['regionname']          = $do_tr['regionname'];//机构行政区划名称
        $regulatorpersonInfo['regionname1']         = $do_tr['regionname1'];//机构行政区划全名称
        $regulatorpersonInfo['regulator_regionid']  = $regulatorInfo['fregionid'];//部门行政区划ID
        $regulatorpersonInfo['regulatorname']       = $regulatorInfo['fname'];//部门名称
        //用户媒介权限
        $mediajurisdiction = D('Function')->get_mediajurisdiction($regulatorInfo['fid'],[],[],$regulatorpersonInfo['fid']);//获取媒体权限组
        $nj_media   = $mediajurisdiction?$mediajurisdiction:array('0');

        $do_tt = M('tbn_media_grant')->field('fmedia_id')->where('freg_id='.$regulatorpersonInfo['fregulatorpid'])->select();
        foreach ($do_tt as $key => $value) {
            array_push($nj_media, $value['fmedia_id']);
        }
    }





    $regulatorpname = $regulatorpersonInfo['regulatorpname'];//机构名
    $regionid = $regulatorpersonInfo['regionid'];//机构行政区划ID

    $date_table = date('Ym',strtotime($date)).'_'.substr($regionid,0,2);//组合表
    $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
    $date_end_ymd = date('Y年m月d日',$report_end_date);
    $date_md = date('m月d日',$report_end_date);//结束月日字符
    $phase_num = date('（Y年第m期）',$report_start_date);//第几期
    $phase_ym = date('Y年m月',$report_start_date);//第几期
    $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数


    $StatisticalReport1 = New StatisticalReportModel();//实例化数据统计模型
    $StatisticalReport = New StatisticalModel();//实例化数据统计模型



    $system_num = getconfig('system_num');//获取国家局标记

    $nj_media = M('tmedia_temp')
        ->join("tmedia on tmedia.fid = tmedia_temp.fmediaid")
        ->where([
            'LEFT(tmedia.fmediaclassid, 2)'=>['IN',"01,02,03"],
            'tmedia_temp.ftype'=>1,
            'tmedia_temp.fcustomer'=>$system_num,
            'tmedia_temp.fuserid'=>session('regulatorpersonInfo.fid')
        ])
        ->getField('fmediaid',true);

    $usermedia = $StatisticalReport->distinguish_media(implode(',',$nj_media),$regionid);//是否是选择了省直属date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date)
    $all_data = $StatisticalReport->report_ad_monitor('fmedia_class_code',$nj_media,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate');//当前客户全部互联网广告数据统计
    $all_data_last = $StatisticalReport->report_ad_monitor('fmedia_class_code',$nj_media,$user['regionid'],date('Y-m-d',$last_s_time),date('Y-m-d',$last_e_time),'times_illegal_rate');//当前客户全部互联网广告数据统计
    $all_data_last_year = $StatisticalReport->report_ad_monitor('fmedia_class_code',$nj_media,$user['regionid'],date('Y-m-d',$last_year_s_time),date('Y-m-d',$last_year_e_time),'times_illegal_rate');//当前客户全部互联网广告数据统计

    $data_all_list = [];
    $all_fad_times = 0;
    $all_fad_illegal_times = 0;
    $all_last_fad_times = 0;
    $all_last_fad_illegal_times = 0;
    foreach ($all_data as $all_data_key=>$all_data_val){
        if(!empty($all_data_val['tmediaclass_name'])){
            $hb_illegal_times_rate = 0;
            if(!isset($all_data_val['times_illegal_rate']) || !isset($all_data_val['prv_times_illegal_rate'])){
                $hb_illegal_times_rate = 0;
            }else{
                $hb_illegal_times_rate = number_format($all_data_val['times_illegal_rate'] - $all_data_val['prv_times_illegal_rate'],2);//本周期总违法率减去上周期总违法率
            }
            //环比违法率
            if($hb_illegal_times_rate == 0){
                $hb_illegal_times_rate_array[] = $all_data_val['tmediaclass_name'].'无变化';
            }elseif($hb_illegal_times_rate > 0){
                $hb_illegal_times_rate_array[] = $all_data_val['tmediaclass_name'].'环比上升'.$hb_illegal_times_rate.'个百分点';
            }elseif($hb_illegal_times_rate < 0){
                $hb_illegal_times_rate_array[] = $all_data_val['tmediaclass_name'].'环比下降'.($hb_illegal_times_rate * -1).'个百分点';
            }
            $all_fad_times += $all_data_val['fad_times'];
            $all_fad_illegal_times += $all_data_val['fad_illegal_times'];
        }

    }

    $hb_illegal_times_rate_array = implode('、',$hb_illegal_times_rate_array);
    foreach ($all_data_last as $all_data_last_key => $all_data_last_val){
        if(!empty($all_data_last_val['tmediaclass_name'])) {
            $all_last_fad_times += $all_data_last_val['fad_times'];
            $all_last_fad_illegal_times += $all_data_last_val['fad_illegal_times'];
        }
    }
    foreach ($all_data_last_year as $all_data_last_year_key => $all_data_last_year_val){
        if(!empty($all_data_last_year_val['tmediaclass_name'])) {
            $all_last_year_fad_times += $all_data_last_year_val['fad_times'];
            $all_last_year_fad_illegal_times += $all_data_last_year_val['fad_illegal_times'];
        }
    }

    $a = number_format(($all_fad_illegal_times/$all_fad_times)*100,2);//本周期总违法率
    $a1 = number_format(($all_last_fad_illegal_times/$all_last_fad_times)*100,2);//上周期总违法率
    $hb_times_illegal_rate = $a - $a1;//本周期总违法率减去上周期总违法率
    $hb_fad_times = $all_fad_times - $all_last_fad_times;//本周期发布量减去上周期发布量
    //环比违法率
    if($hb_times_illegal_rate == 0){
        $hb_times_illegal_rate = '环比未有明显变化，';
    }elseif($hb_times_illegal_rate > 0){
        $hb_times_illegal_rate = '环比上升'.$hb_times_illegal_rate.'个百分点，';
    }elseif($hb_times_illegal_rate < 0){
        $hb_times_illegal_rate = '环比下降'.($hb_times_illegal_rate*-1).'个百分点，';
    }
    //环比发布量
    if($hb_fad_times == 0){
        $hb_fad_times = '环比无变化，';
    }elseif($hb_fad_times > 0){
        $hb_fad_times = '环比增加'.$hb_fad_times.'条次，';
    }elseif($hb_fad_times < 0){
        $hb_fad_times = '环比减少'.($hb_fad_times*-1).'条次，';
    }

    if(!empty($all_data_last_year)){
        //同比违法率
        $c = number_format(($all_last_year_fad_illegal_times/$all_last_year_fad_times)*100,2);//本月总违法率
        $tb_times_illegal_rate = $a - $c;//本月总违法率减去去年本月总违法率
        if($tb_times_illegal_rate == 0){
            $tb_times_illegal_rate = '同比无变化，';
        }elseif($tb_times_illegal_rate > 0){
            $tb_times_illegal_rate = '同比上升'.$tb_times_illegal_rate.'个百分点，';
        }elseif($tb_times_illegal_rate < 0){
            $tb_times_illegal_rate = '同比下降'.($tb_times_illegal_rate*-1).'个百分点，';
        }
    }else{
        $tb_times_illegal_rate = '';
    }

    $province_data = $StatisticalReport->report_ad_monitor('fmediaid',$usermedia[1],$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate');////省媒体
    $province_tv_data = $StatisticalReport->report_ad_monitor('fmediaid',array_merge($usermedia[1],$usermedia[0]),$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'fregionid','01');//省媒体电视
    $province_tb_data = $StatisticalReport->report_ad_monitor('fmediaid',array_merge($usermedia[1],$usermedia[0]),$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'fregionid','02');//省媒体广播
    $province_tp_data = $StatisticalReport->report_ad_monitor('fmediaid',array_merge($usermedia[1],$usermedia[0]),$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'fregionid','03');//省媒体报纸

    //电视
    $province_media_tv_data = [];
    foreach ($province_tv_data as $province_tv_data_key => $province_tv_data_val){
        $province_media_tv_data[] = [
            strval($province_tv_data_key+1),
            $province_tv_data_val['tmedia_name'] ? $province_tv_data_val['tmedia_name'] : $province_tv_data_val['titlename'],
            $province_tv_data_val['fad_times'],
            $province_tv_data_val['fad_illegal_times'],
            $province_tv_data_val['times_illegal_rate'].'%'
        ];
    }

    //广播
    $province_media_tb_data = [];
    foreach ($province_tb_data as $province_tb_data_key => $province_tb_data_val){
        $province_media_tb_data[] = [
            strval($province_tb_data_key+1),
            $province_tb_data_val['tmedia_name'] ? $province_tb_data_val['tmedia_name'] : $province_tb_data_val['titlename'],
            $province_tb_data_val['fad_times'],
            $province_tb_data_val['fad_illegal_times'],
            $province_tb_data_val['times_illegal_rate'].'%'
        ];
    }

    //报纸
    $province_media_tp_data = [];
    foreach ($province_tp_data as $province_tp_data_key => $province_tp_data_val){
        $province_media_tp_data[] = [
            strval($province_tp_data_key+1),
            $province_tp_data_val['tmedia_name'] ? $province_tp_data_val['tmedia_name'] : $province_tp_data_val['titlename'],
            $province_tp_data_val['fad_times'],
            $province_tp_data_val['fad_illegal_times'],
            $province_tp_data_val['times_illegal_rate'].'%'
        ];

    }

    $province_fmediaclass_data = $StatisticalReport->report_ad_monitor(
        'fmedia_class_code',
        array_merge($usermedia[1],$usermedia[0]),
        $user['regionid'],
        date('Y-m-d',$report_start_date),
        date('Y-m-d',$report_end_date),
        'times_illegal_rate'
    );//省媒介类型

    $province_media_class_data = [];
    $province_media_class_times_data[] = ['','发布量'];
    $province_media_class_illegal_times_data[] = ['','违法发布量'];
    $province_fad_illegal_times = 0;
    foreach ($province_fmediaclass_data as $province_fmediaclass_data_key=>$province_fmediaclass_data_val){

        $province_media_class_data[] = [
            $province_fmediaclass_data_val['tmediaclass_name'] ? $province_fmediaclass_data_val['tmediaclass_name'] : $province_fmediaclass_data_val['titlename'],
            $province_fmediaclass_data_val['fad_times'],
            $province_fmediaclass_data_val['hb_fad_times'] ? $province_fmediaclass_data_val['hb_fad_times']:'无变化',
            $province_fmediaclass_data_val['fad_illegal_times'],
            $province_fmediaclass_data_val['hb_fad_illegal_times'] ? $province_fmediaclass_data_val['hb_fad_illegal_times']:'无变化',
            $province_fmediaclass_data_val['times_illegal_rate'].'%',
            $province_fmediaclass_data_val['hb_times_illegal_rate'] ? $province_fmediaclass_data_val['hb_times_illegal_rate']:'无变化',
        ];

        if(!empty($province_fmediaclass_data_val['tmediaclass_name'])){
            $province_media_class_times_data[] = [
                $province_fmediaclass_data_val['tmediaclass_name'],
                $province_fmediaclass_data_val['fad_times']
            ];
            if($province_fmediaclass_data_val['fad_illegal_times'] > 0){
                $province_media_class_illegal_times_data[] = [
                    $province_fmediaclass_data_val['tmediaclass_name'],
                    $province_fmediaclass_data_val['fad_illegal_times']
                ];
            }
        }




        $province_fad_illegal_times += $province_fmediaclass_data_val['fad_illegal_times'];
    }

    $city_data = $StatisticalReport->report_ad_monitor('fmediaid',$usermedia[0],$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate');//市媒体全部
    $city_tv_data = $StatisticalReport->report_ad_monitor('fmediaid',$usermedia[0],$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','01');//市媒体电视
    $city_tb_data = $StatisticalReport->report_ad_monitor('fmediaid',$usermedia[0],$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','02');//市媒体广播
    $city_tp_data = $StatisticalReport->report_ad_monitor('fmediaid',$usermedia[0],$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','03');//市媒体报纸

//电视
    $city_media_tv_data = [];
    foreach ($city_tv_data as $city_tv_data_key => $city_tv_data_val){
        $city_media_tv_data[] = [
            strval($city_tv_data_key+1),
            $city_tv_data_val['tmedia_name'] ? $city_tv_data_val['tmedia_name'] : $city_tv_data_val['titlename'],
            $city_tv_data_val['fad_times'],
            $city_tv_data_val['fad_illegal_times'],
            $city_tv_data_val['times_illegal_rate'].'%'
        ];
    }

//广播
    $city_media_tb_data = [];
    foreach ($city_tb_data as $city_tb_data_key => $city_tb_data_val){
        $city_media_tb_data[] = [
            strval($city_tb_data_key+1),
            $city_tb_data_val['tmedia_name'] ? $city_tb_data_val['tmedia_name'] : $city_tb_data_val['titlename'],
            $city_tb_data_val['fad_times'],
            $city_tb_data_val['fad_illegal_times'],
            $city_tb_data_val['times_illegal_rate'].'%'
        ];
    }
    $city_tb_illegal_times_rate_last = round(($city_tb_illegal_times_last/$city_tb_times_last)*100,2).'%';
    $city_media_tb_data[] = [strval(count($city_media_tb_data)+1),'小计',strval($city_tb_times_last),strval($city_tb_illegal_times_last),$city_tb_illegal_times_rate_last];//最后统计

//报纸
    $city_media_tp_data = [];
    foreach ($city_tp_data as $city_tp_data_key => $city_tp_data_val){
        $city_media_tp_data[] = [
            strval($city_tp_data_key+1),
            $city_tp_data_val['tmedia_name'] ? $city_tp_data_val['tmedia_name'] : $city_tp_data_val['titlename'],
            $city_tp_data_val['fad_times'],
            $city_tp_data_val['fad_illegal_times'],
            $city_tp_data_val['times_illegal_rate'].'%'
        ];
    }

    $city_fmediaclass_data = $StatisticalReport->report_ad_monitor('fmedia_class_code',$usermedia[0],$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate');//市媒介类型

    $city_media_class_data = [];
    $city_media_class_times_data[] = ['','发布量'];
    $city_media_class_illegal_times_data[] = ['','违法发布量'];
    $city_fad_illegal_times = 0;
    foreach ($city_fmediaclass_data as $city_fmediaclass_data_key=>$city_fmediaclass_data_val){

        $city_media_class_data[] = [
            $city_fmediaclass_data_val['tmediaclass_name'] ? $city_fmediaclass_data_val['tmediaclass_name'] : $city_fmediaclass_data_val['titlename'],
            $city_fmediaclass_data_val['fad_times'],
            $city_fmediaclass_data_val['hb_fad_times']?$city_fmediaclass_data_val['hb_fad_times']:'无变化',
            $city_fmediaclass_data_val['fad_illegal_times'],
            $city_fmediaclass_data_val['hb_fad_illegal_times']?$city_fmediaclass_data_val['hb_fad_illegal_times']:'无变化',
            $city_fmediaclass_data_val['times_illegal_rate'].'%',
            $city_fmediaclass_data_val['hb_times_illegal_rate']?$city_fmediaclass_data_val['hb_times_illegal_rate']:'无变化',
        ];

        if(!empty($city_fmediaclass_data_val['tmediaclass_name'])){
            $city_media_class_times_data[] = [
                $city_fmediaclass_data_val['tmediaclass_name'],
                $city_fmediaclass_data_val['fad_times']
            ];
            if($city_fmediaclass_data_val['fad_illegal_times'] > 0){
                $city_media_class_illegal_times_data[] = [
                    $city_fmediaclass_data_val['fclass'],
                    $city_fmediaclass_data_val['fad_illegal_times']
                ];
            }
        }



        $city_fad_illegal_times += $city_fmediaclass_data_val['fad_illegal_times'];
    }


    $area_data = $StatisticalReport->report_ad_monitor('fmediaid',$usermedia[2],$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate');//区媒体全部
    $area_tv_data = $StatisticalReport->report_ad_monitor('fmediaid',$usermedia[2],$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','01');//区媒体电视
    $area_tb_data = $StatisticalReport->report_ad_monitor('fmediaid',$usermedia[2],$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','02');//区媒体广播
    $area_tp_data = $StatisticalReport->report_ad_monitor('fmediaid',$usermedia[2],$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','03');//区媒体报纸


//电视
    $area_media_tv_data = [];
    foreach ($area_tv_data as $area_tv_data_key => $area_tv_data_val){
        $area_media_tv_data[] = [
            strval($area_tv_data_key+1),
            $area_tv_data_val['tmedia_name'] ? $area_tv_data_val['tmedia_name'] : $area_tv_data_val['titlename'],
            $area_tv_data_val['fad_times'],
            $area_tv_data_val['fad_illegal_times'],
            $area_tv_data_val['times_illegal_rate'].'%'
        ];
    }

//广播
    $area_media_tb_data = [];
    foreach ($area_tb_data as $area_tb_data_key => $area_tb_data_val){
        $area_media_tb_data[] = [
            strval($area_tb_data_key+1),
            $area_tb_data_val['tmedia_name'] ? $area_tb_data_val['tmedia_name'] : $area_tb_data_val['titlename'],
            $area_tb_data_val['fad_times'],
            $area_tb_data_val['fad_illegal_times'],
            $area_tb_data_val['times_illegal_rate'].'%'
        ];

    }

//报纸
    $area_media_tp_data = [];
    foreach ($area_tp_data as $area_tp_data_key => $area_tp_data_val){
        $area_media_tp_data[] = [
            strval($area_tp_data_key+1),
            $area_tp_data_val['tmedia_name'] ? $area_tp_data_val['tmedia_name'] : $area_tp_data_val['titlename'],
            $area_tp_data_val['fad_times'],
            $area_tp_data_val['fad_illegal_times'],
            $area_tp_data_val['times_illegal_rate'].'%'
        ];
    }

    $area_fmediaclass_data = $StatisticalReport->report_ad_monitor('fmedia_class_code',$usermedia[2],$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate');//市媒介区媒介类型类型

    $area_media_class_data = [];
    $area_media_class_times_data[] = ['','发布量'];
    $area_media_class_illegal_times_data[] = ['','违法发布量'];
    $area_fad_illegal_times = 0;
    foreach ($area_fmediaclass_data as $area_fmediaclass_data_key=>$area_fmediaclass_data_val){
        $area_media_class_data[] = [
            $area_fmediaclass_data_val['tmediaclass_name'] ? $area_fmediaclass_data_val['tmediaclass_name'] : $area_fmediaclass_data_val['titlename'],
            $area_fmediaclass_data_val['fad_times'],
            $area_fmediaclass_data_val['hb_fad_times']?$area_fmediaclass_data_val['hb_fad_times']:'无变化',
            $area_fmediaclass_data_val['fad_illegal_times'],
            $area_fmediaclass_data_val['hb_fad_illegal_times']?$area_fmediaclass_data_val['hb_fad_illegal_times']:'无变化',
            $area_fmediaclass_data_val['times_illegal_rate'].'%',
            $area_fmediaclass_data_val['hb_times_illegal_rate']?$area_fmediaclass_data_val['hb_times_illegal_rate']:'无变化',
        ];

        if(!empty($area_fmediaclass_data_val['tmediaclass_name'])){
            $area_media_class_times_data[] = [
                $area_fmediaclass_data_val['tmediaclass_name'],
                $area_fmediaclass_data_val['fad_times']
            ];
            if($area_fmediaclass_data_val['fad_illegal_times'] > 0){
                $area_media_class_illegal_times_data[] = [
                    $area_fmediaclass_data_val['tmediaclass_name'],
                    $area_fmediaclass_data_val['fad_illegal_times']
                ];
            }
        }



        $area_fad_illegal_times += $area_fmediaclass_data_val['fad_illegal_times'];
    }

    $province_media_names = [];//省级媒体名称
    $city_media_names = [];//市级媒体名称
    $area_media_names = [];//区级媒体名称

    foreach ($province_data as $province_data_key=>$province_data_val){
        if($province_data_val['tmedia_name']){
            $province_media_names[] = $province_data_val['tmedia_name'];
        }
    }

    foreach ($city_data as $city_data_key => $city_data_val){
        if($city_data_val['tmedia_name']){
            $city_media_names[] = $city_data_val['tmedia_name'];
        }
    }

    foreach ($area_data as $area_data_key => $area_data_val){
        if($area_data_val['tmedia_name']){
            $area_media_names[] = $area_data_val['tmedia_name'];
        }
    }

    $cus_media_count = $StatisticalReport->cus_media_count('tregion.flevel',['01','02','03']);

    foreach ($cus_media_count as $cus_media){
        switch ($cus_media['flevel']){
            case 1:
                $province_media_count = $cus_media['m_count'];//省级媒体数量
                $province_media_names = $cus_media['tmedia_names'];//省级媒体名称
                break;
            case 2:
            case 3:
            case 4:
                $city_media_count = $cus_media['m_count'];//市级媒体数量
                $city_media_names = $cus_media['tmedia_names'];//市级媒体名称
                break;
            case 5:
                $area_media_count = $cus_media['m_count'];//区级媒体数量
                $area_media_names = $cus_media['tmedia_names'];//区级媒体名称
                break;
        }
    }


    $data = [
        'dotfilename' => 'Template.dotx',
        'ossupload' => 'yes',
        'reportfilename' => $regulatorpersonInfo['regulator_regionid'].'_'.time().'.docx',
        'content' => []
    ];
    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体45号居中",
        "text" => $regulatorpersonInfo['regulatorpname']
    ];
    $data['content'][] = [
        "type" => "text",
        "bookmark" => "红色宋体小一居中",
        "text" => "广告监测报告"
    ];
    $data['content'][] = [
        "type" => "text",
        "bookmark" => "红色宋体四号居中",
        "text" => $phase_num
    ];
    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三加粗靠右",
        "text" => $phase_ym
    ];
    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三加粗靠左",
        "text" => "一、广告监测情况综述"
    ];
    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三正文",
        "text" => "广告监测中心".$phase_ym."共监测".($city_media_count+$province_media_count+$area_media_count)."家传统媒体，其中省属媒体".$province_media_count."家(".$province_media_names.")。市属媒体".$city_media_count."家(".$city_media_names.")。区属媒体".$area_media_count."家(".$area_media_names.")。"
    ];


    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三正文",
        "text" => "本月，监测中心监测媒体发布的各类广告".$all_fad_times."条次，".$hb_fad_times."，其中涉嫌违法广告".$all_fad_illegal_times."条次，监测违法率为".$a."%，".$hb_times_illegal_rate."。"
    ];

    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三加粗靠左",
        "text" => "(一)分类监测情况"
    ];



    if(!empty($province_media_class_data)){
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "省市媒体："
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒体类别监测列表",
            "data" => to_string($province_media_class_data)
        ];
    }

    $data['content'][] = [
        "type" => "chart",
        "bookmark" => "媒体广告监测量分布图",
        "data" => to_string($province_media_class_times_data)
    ];

    if($province_fad_illegal_times == 0){
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三正文",
            "text" => "暂未监测到违法广告"
        ];
    }else{
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "媒体广告违法情况分布图",
            "data" => to_string($province_media_class_illegal_times_data)
        ];
    }

    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三加粗靠左",
        "text" => "省市报刊媒体："
    ];

    $data['content'][] = [
        "type" => "table",
        "bookmark" => "媒体监测情况列表",
        "data" => to_string($province_media_tp_data)
    ];

    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三加粗靠左",
        "text" => "省市电视媒体："
    ];

    $data['content'][] = [
        "type" => "table",
        "bookmark" => "媒体监测情况列表",
        "data" => to_string($province_media_tv_data)
    ];

    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三加粗靠左",
        "text" => "省市广播媒体："
    ];

    $data['content'][] = [
        "type" => "table",
        "bookmark" => "媒体监测情况列表",
        "data" => to_string($province_media_tb_data)
    ];

    $tv_public_welfare_data_province = $StatisticalReport->os_tv_public_welfare(array_merge($usermedia[1],$usermedia[0]),date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));

    $tv_public_welfare_province_list = [];
    foreach ($tv_public_welfare_data_province as $tv_public_welfare_data_key=>$tv_public_welfare_data_val){
        $tv_public_welfare_province_list[] = [
            $tv_public_welfare_data_val['tmname'],
            $tv_public_welfare_data_val['gysl_1921'],
            number_format(($tv_public_welfare_data_val['gysl_1921']/120)*100,2).'%'
        ];
    }

    if(!empty($tv_public_welfare_province_list)){
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "省市电视媒体公益广告发布情况表："
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "电视公益监测列表",
            "data" => to_string($tv_public_welfare_province_list)
        ];
    }

    $tb_public_welfare_data_province = $StatisticalReport->os_tb_public_welfare(array_merge($usermedia[1],$usermedia[0]),date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));
    $tb_public_welfare_province_list = [];
    foreach ($tb_public_welfare_data_province as $tb_public_welfare_data_key=>$tb_public_welfare_data_val){
        $tb_public_welfare_province_list[] = [
            $tb_public_welfare_data_val['tmname'],
            $tb_public_welfare_data_val['gysl_0608'],
            $tb_public_welfare_data_val['gysl_1113'],
            number_format((($tb_public_welfare_data_val['gysl_0608']+$tb_public_welfare_data_val['gysl_1113'])/120)*100,2).'%'
        ];
    }

    if(!empty($tb_public_welfare_province_list)){
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "省市广播媒体公益广告发布情况表："
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "广播公益监测列表",
            "data" => to_string($tb_public_welfare_province_list)
        ];
    }

    $tp_public_welfare_data_province = $StatisticalReport->os_tp_public_welfare(array_merge($usermedia[1],$usermedia[0]),date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));
    $tp_public_welfare_province_list = [];
    foreach ($tp_public_welfare_data_province as $tp_public_welfare_data_key=>$tp_public_welfare_data_val){
        $tp_public_welfare_province_list[] = [
            $tp_public_welfare_data_val['tmname'],
            $tp_public_welfare_data_val['gysl'],
            '',
            ''
        ];
    }
    if(!empty($tp_public_welfare_province_list)){
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "省市报刊媒体公益广告发布情况表："
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "报刊公益广告列表",
            "data" => to_string($tp_public_welfare_province_list)
        ];
    }

    /*        if(!empty($city_media_class_data)){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => "市属媒体："
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "媒体类别监测列表",
                    "data" => $city_media_class_data
                ];
            }

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "媒体广告监测量分布图",
                "data" => $city_media_class_times_data
            ];

            if($city_fad_illegal_times == 0){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三正文",
                    "text" => "暂未监测到违法广告"
                ];
            }else{
                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "媒体广告违法情况分布图",
                    "data" => $city_media_class_illegal_times_data
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "报刊媒体："
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "媒体监测情况列表",
                "data" => $city_media_tp_data
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "电视媒体："
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "媒体监测情况列表",
                "data" => $city_media_tv_data
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "广播媒体："
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "媒体监测情况列表",
                "data" => $city_media_tb_data
            ];
            $tv_public_welfare_data = $StatisticalReport->tv_public_welfare($usermedia[0],$date_table);
            $tv_public_welfare_list = [];
            foreach ($tv_public_welfare_data as $tv_public_welfare_data_key=>$tv_public_welfare_data_val){
                $tv_public_welfare_list[] = [
                    $tv_public_welfare_data_val['tmname'],
                    strval($tv_public_welfare_data_val['gysl']),
                ];
            }

            if(!empty($tv_public_welfare_list)){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => "电视媒体公益广告发布情况表："
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "电视公益监测列表",
                    "data" => $tv_public_welfare_list
                ];
            }
            $tb_public_welfare_data = $StatisticalReport->tb_public_welfare($usermedia[0],$date_table);
            $tb_public_welfare_list = [];
            foreach ($tb_public_welfare_data as $tb_public_welfare_data_key=>$tb_public_welfare_data_val){
                $tb_public_welfare_list[] = [
                    $tb_public_welfare_data_val['tmname'],
                    strval($tb_public_welfare_data_val['gysl1']),
                    strval($tb_public_welfare_data_val['gysl2']),
                ];
            }

            if(!empty($tb_public_welfare_list)){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => "广播媒体公益广告发布情况表："
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "广播公益监测列表",
                    "data" => $tb_public_welfare_list
                ];
            }

            $tp_public_welfare_data = $StatisticalReport->tp_public_welfare($usermedia[0],$report_start_date,$report_end_date);
            $tp_public_welfare_list = [];
            foreach ($tp_public_welfare_data as $tp_public_welfare_data_key=>$tp_public_welfare_data_val){
                $tp_public_welfare_list[] = [
                    $tp_public_welfare_data_val['tmname'],
                    strval($tp_public_welfare_data_val['gysl']),
                ];
            }
            if(!empty($tp_public_welfare_list)){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => "报刊媒体公益广告发布情况表："
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "报刊公益广告列表",
                    "data" => $tp_public_welfare_list
                ];
            }*/

    //1008
    if(!empty($area_media_class_data)){
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "区属媒体："
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒体类别监测列表",
            "data" => to_string($area_media_class_data)
        ];
    }

    $data['content'][] = [
        "type" => "chart",
        "bookmark" => "媒体广告监测量分布图",
        "data" => to_string($area_media_class_times_data)
    ];

    if($area_fad_illegal_times == 0){
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三正文",
            "text" => "暂未监测到违法广告"
        ];
    }else{
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "媒体广告违法情况分布图",
            "data" => to_string($area_media_class_illegal_times_data)
        ];
    }

/*    if(!empty($area_media_tp_data)) {
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "报刊媒体："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒体监测情况列表",
            "data" => to_string($area_media_tp_data)
        ];
    }*/

    if(!empty($area_media_tv_data)) {

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "电视媒体："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒体监测情况列表",
            "data" => to_string($area_media_tv_data)
        ];
    }

    if(!empty($area_media_tb_data)) {
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "广播媒体："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒体监测情况列表",
            "data" => to_string($area_media_tb_data)
        ];
    }


    $tv_public_welfare_data = $StatisticalReport->os_tv_public_welfare($usermedia[2],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));
    $tv_public_welfare_list = [];
    foreach ($tv_public_welfare_data as $tv_public_welfare_data_key=>$tv_public_welfare_data_val){
        $tv_public_welfare_list[] = [
            $tv_public_welfare_data_val['tmname'],
            strval($tv_public_welfare_data_val['gysl_1921']),
            number_format(($tv_public_welfare_data_val['gysl_1921']/120)*100,2).'%'
        ];
    }

    if(!empty($tv_public_welfare_list)){
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "区县级电视媒体公益广告发布情况表："
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "电视公益监测列表",
            "data" => to_string($tv_public_welfare_list)
        ];
    }/*else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "本期未监测到电视公益广告"
            ];
        }*/
    $tb_public_welfare_data = $StatisticalReport->os_tb_public_welfare($usermedia[2],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));
    $tb_public_welfare_list = [];
    foreach ($tb_public_welfare_data as $tb_public_welfare_data_key=>$tb_public_welfare_data_val){
        $tb_public_welfare_list[] = [
            $tb_public_welfare_data_val['tmname'],
            strval($tb_public_welfare_data_val['gysl_0608']),
            strval($tb_public_welfare_data_val['gysl_1113']),
            number_format((($tb_public_welfare_data_val['gysl_0608']+$tb_public_welfare_data_val['gysl_1113'])/120)*100,2).'%'
        ];
    }

    if(!empty($tb_public_welfare_list)){
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "区县级广播媒体公益广告发布情况表："
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "广播公益监测列表",
            "data" => to_string($tb_public_welfare_list)
        ];
    }/*else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "本期未监测到广播公益广告"
            ];
        }*/

/*    $tp_public_welfare_data = $StatisticalReport->os_tp_public_welfare($usermedia[2],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));
    $tp_public_welfare_list = [];
    foreach ($tp_public_welfare_data as $tp_public_welfare_data_key=>$tp_public_welfare_data_val){
        $tp_public_welfare_list[] = [
            $tp_public_welfare_data_val['tmname'],
            strval($tp_public_welfare_data_val['gysl']),
            '',
            ''
        ];
    }
    if(!empty($tp_public_welfare_list)){
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "区县级报刊媒体公益广告发布情况表："
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "报刊公益广告列表",
            "data" => to_string($tp_public_welfare_list)
        ];
    }*/
    /*else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "本期未监测到报刊公益广告"
            ];
        }*/

    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三加粗靠左",
        "text" => "（二）行业监测情况"
    ];

    $eight_categories = ['01','02','03','12','13','06','04','08'];//重点整治七大类
    //$categories_1201 = ['1201'];//小类
    $eight_categories_data = $StatisticalReport->report_ad_monitor('fad_class_code',$nj_media,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','',$eight_categories);//市媒介区媒介类型类型

  /*  $eight_categories_data_7 = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$nj_media,'',$eight_categories,'times_illegal_rate','fcode');//
    $eight_categories_data_1201 = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$nj_media,'',$categories_1201,'times_illegal_rate','fcode',true);//

    $eight_categories_data_7[] = $eight_categories_data_1201[0];
    $eight_categories_data = $this->pxsf($eight_categories_data_7,'times_illegal_rate');*/
    $eight_categories_data_last = $StatisticalReport->report_ad_monitor('fad_class_code',$nj_media,$user['regionid'],date('Y-m-d',$last_s_time),date('Y-m-d',$last_e_time),'times_illegal_rate','',$eight_categories);//市媒介区媒介类型类型

   /* $eight_categories_data_last_7 = $StatisticalReport->ad_monitor_customize($date_table,$last_s_time,$last_e_time,$nj_media,'',$eight_categories,'times_illegal_rate','fcode');//
    $eight_categories_data_last_1201 = $StatisticalReport->ad_monitor_customize($date_table,$last_s_time,$last_e_time,$nj_media,'',$categories_1201,'times_illegal_rate','fcode',true);//

    $eight_categories_data_last_7[] = $eight_categories_data_last_1201[0];
    $eight_categories_data_last = $this->pxsf($eight_categories_data_last_7,'times_illegal_rate');*/

    $eight_ad_class_data = [];
    $eight_ad_class_hb_data[] = ['','本期','上期'];
    $eight_ad_class_illegal_data[] = ['','违法发布量'];
    $eight_ad_class_illegal_times = 0;

    foreach ($eight_categories_data as $eight_categories_data_key=>$eight_categories_data_val){
        if(!empty($eight_categories_data_val['tadclass_name'])){
            $eight_ad_class_data[] = [
                $eight_categories_data_val['tadclass_name'],
                $eight_categories_data_val['fad_times'],
                $eight_categories_data_val['hb_fad_times'] ? $eight_categories_data_val['hb_fad_times']:'无变化',
                $eight_categories_data_val['fad_illegal_times'],
                $eight_categories_data_val['hb_fad_illegal_times'] ? $eight_categories_data_val['hb_fad_illegal_times']:'无变化',
                $eight_categories_data_val['times_illegal_rate'].'%',
                $eight_categories_data_val['hb_times_illegal_rate'] ? $eight_categories_data_val['hb_times_illegal_rate']:'无变化',
            ];
            $eight_ad_class_hb_data[] = [
                $eight_categories_data_val['tadclass_name'],
                $eight_categories_data_val['times_illegal_rate'].'%',
                $eight_categories_data_val['prv_times_illegal_rate'].'%'
            ];
            $eight_ad_class_illegal_data[] = [
                $eight_categories_data_val['tadclass_name'],
                $eight_categories_data_val['fad_illegal_times']
            ];
            $eight_ad_class_times += $eight_categories_data_val['fad_times'];
            $eight_ad_class_illegal_times += $eight_categories_data_val['fad_illegal_times'];
        }

    }

    foreach ($eight_categories_data_last as $eight_categories_data_last_key=>$eight_categories_data_last_val){
        if(!empty($eight_categories_data_last_val['tadclass_name'])){
            $eight_ad_class_times_last += $eight_categories_data_last_val['fad_times'];
            $eight_ad_class_illegal_times_last += $eight_categories_data_last_val['fad_illegal_times'];
        }

    }

    $b = number_format(($eight_ad_class_illegal_times/$eight_ad_class_times)*100,2);//本周期总违法率
    $b1 = number_format(($eight_ad_class_illegal_times_last/$eight_ad_class_times_last)*100,2);//上周期总违法率
    $eight_hb_times_illegal_rate = $b - $b1;//本周期总违法率减去上周期总违法率
    $hb_fad_times = $eight_ad_class_times - $eight_ad_class_times_last;//本周期发布量减去上周期发布量
    $hb_fad_illegal_times = $eight_ad_class_illegal_times - $eight_ad_class_illegal_times_last;//本周期发布量减去上周期违法发布量
    //环比违法率
    if($eight_hb_times_illegal_rate == 0){
        $eight_hb_times_illegal_rate = '无变化';
    }elseif($eight_hb_times_illegal_rate > 0){
        $eight_hb_times_illegal_rate = $eight_hb_times_illegal_rate.'↑';
    }elseif($eight_hb_times_illegal_rate < 0){
        $eight_hb_times_illegal_rate = ($eight_hb_times_illegal_rate*-1).'↓';
    }
    //环比发布量
    if($hb_fad_times == 0){
        $hb_fad_times = '无变化';
    }elseif($hb_fad_times > 0){
        $hb_fad_times = $hb_fad_times.'↑';
    }elseif($hb_fad_times < 0){
        $hb_fad_times = ($hb_fad_times*-1).'↓';
    }
    //环比违法发布量
    if($hb_fad_illegal_times == 0){
        $hb_fad_illegal_times = '无变化';
    }elseif($hb_fad_illegal_times > 0){
        $hb_fad_illegal_times = $hb_fad_illegal_times.'↑';
    }elseif($hb_fad_illegal_times < 0){
        $hb_fad_illegal_times = ($hb_fad_illegal_times*-1).'↓';
    }
    $eight_ad_class_data[] = ['八类重点行业累计',strval($eight_ad_class_times),$hb_fad_times,strval($eight_ad_class_illegal_times),$hb_fad_illegal_times,$b.'%',$eight_hb_times_illegal_rate];

    $data['content'][] = [
        "type" => "chart",
        "bookmark" => "重点行业违法情况分布图",
        "data" => to_string($eight_ad_class_illegal_data)
    ];
    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三加粗靠左",
        "text" => "8大重点行业情况表："
    ];
    $data['content'][] = [
        "type" => "table",
        "bookmark" => "媒体类别监测列表",
        "data" => to_string($eight_ad_class_data)
    ];//
    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三加粗靠左",
        "text" => "重点整治行业广告违法率环比图："
    ];
    $data['content'][] = [
        "type" => "chart",
        "bookmark" => "重点整治行业广告违法率环比图",
        "data" => to_string($eight_ad_class_hb_data)
    ];


    $twenty_three_categories_data = $StatisticalReport->report_ad_monitor('fad_class_code',$nj_media,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate');//市媒介区媒介类型类型

    //$twenty_three_categories_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$nj_media,'','','times_illegal_rate','fcode');
    $twenty_three_categories_list = [];
    $illegal_categories_name = [];
    foreach ($twenty_three_categories_data as $twenty_three_categories_data_key => $twenty_three_categories_data_val){
        if(!empty($twenty_three_categories_data_val['tadclass_name'])){

            if($twenty_three_categories_data_val['fad_illegal_times'] > 0){
                $illegal_categories_name['name'][] = $twenty_three_categories_data_val['tadclass_name'];
                $illegal_categories_name['fad_illegal_times'] += $twenty_three_categories_data_val['fad_illegal_times'];
            }
            $twenty_three_categories_list[] = [
                strval($twenty_three_categories_data_key+1),
                $twenty_three_categories_data_val['tadclass_name'] ? $twenty_three_categories_data_val['tadclass_name'] : $twenty_three_categories_data_val['titlename'],
                $twenty_three_categories_data_val['fad_times'],
                $twenty_three_categories_data_val['fad_illegal_times'],
                $twenty_three_categories_data_val['times_illegal_rate'].'%'
            ];
        }

    }
    $illegal_categories_name['count'] = count($illegal_categories_name['name']);
    $illegal_categories_name['name'] = implode('、',$illegal_categories_name['name']);

    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三加粗靠左",
        "text" => "23大类广告情况列表（按违法率排名）："
    ];
    $data['content'][] = [
        "type" => "table",
        "bookmark" => "二十三大类广告情况列表",
        "data" => to_string($twenty_three_categories_list)
    ];

    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三加粗靠左",
        "text" => "二、广告监测警示"
    ];

    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三正文",
        "text" => "（一）本月广告监测违法率"
    ];

    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三正文",
        "text" => "本月，市省媒体广告监测违法率".$tb_times_illegal_rate.$hb_times_illegal_rate."其中，".$hb_illegal_times_rate_array.
            "，本月违法广告类别主要集中在".$illegal_categories_name['name'].$illegal_categories_name['count']."个行业，累计涉嫌违法".
            $illegal_categories_name['fad_illegal_times']."条次"
    ];//0809

    $all_three_categories_data = $StatisticalReport->report_ad_monitor('fad_class_code',$nj_media,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'fad_illegal_times');//市媒介区媒介类型类型

    //$all_three_categories_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$nj_media,'','','fad_illegal_times','fcode',true);
    //$all_three_categories_data = $this->pxsf($all_three_categories_data,'fad_illegal_times');
    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三正文",
        "text" => "（二）".$all_three_categories_data[0]['tadclass_name']."广告违法情况较严重"
    ];

    //查询第一违法类型的违法表现
    $tv_typical_illegal_ad_zd = $StatisticalReport->ill_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$nj_media,1,$all_three_categories_data[0]['fcode']);

    $typical_illegal = [];
    foreach ($tv_typical_illegal_ad_zd as $value){
        $typical_illegal[] = $value['fexpressions'];
    }
    $tb_typical_illegal_ad_zd = $StatisticalReport->ill_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$nj_media,2,$all_three_categories_data[0]['fcode']);
    foreach ($tb_typical_illegal_ad_zd as $value){
        $typical_illegal[] = $value['fexpressions'];
    }
    $tp_typical_illegal_ad_zd = $StatisticalReport->ill_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$nj_media,3,$all_three_categories_data[0]['fcode']);
    foreach ($tp_typical_illegal_ad_zd as $value){
        $typical_illegal[] = $value['fexpressions'];
    }
    $typical_illegal = implode(';',array_unique($typical_illegal));
    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三正文",
        "text" => "本月共监测".$all_three_categories_data[0]['tadclass_name']."广告".$all_three_categories_data[0]['fad_times']."条次，其中涉嫌违法广告".$all_three_categories_data[0]['fad_illegal_times']."条次，监测违法率为".$all_three_categories_data[0]['times_illegal_rate']."%，高居各行业榜首。( ".$typical_illegal.")"
    ];

    $data['content'][] = [
        "type" => "text",
        "bookmark" => "宋体小三加粗靠左",
        "text" => "三、典型违法广告案例"
    ];

    $typical_illegal_ad_tv = $StatisticalReport->ill_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$nj_media,1);

    $illegal_ad_tv_data = [];
    foreach ($typical_illegal_ad_tv as $illegal_ad_tv_val){
        $md5 = MD5($illegal_ad_tv_val['fad_name']);
        if(isset($illegal_ad_tv_data[$md5])){
            $illegal_ad_tv_data[$md5]['fmedianame'] .= '、'.$illegal_ad_tv_val['fmedianame'];
            $illegal_ad_tv_data[$md5]['image'] .= ';'.$illegal_ad_tv_val['image'];
            $illegal_ad_tv_data[$md5]['fexpressions'] .= ';'.$illegal_ad_tv_val['fexpressions'];
            $illegal_ad_tv_data[$md5]['fad_illegal_times'] += $illegal_ad_tv_val['fad_illegal_times'];
        }else{
            $illegal_ad_tv_data[$md5] = $illegal_ad_tv_val;
        }
    }
    $typical_illegal_ad_tv = array_values($illegal_ad_tv_data);

    if(!empty($typical_illegal_ad_tv)){
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "电视"
        ];

        foreach ($typical_illegal_ad_tv as $typical_illegal_ad_tv_key=>$typical_illegal_ad_tv_val){
            if($typical_illegal_ad_tv_key > 9){
                break;
            }
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => ($typical_illegal_ad_tv_key+1)."、".$typical_illegal_ad_tv_val['fad_name']." (".$typical_illegal_ad_tv_val['ffullname'].")"
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "发布媒体：".$typical_illegal_ad_tv_val['fmedianame']
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "违法表现：“".$typical_illegal_ad_tv_val['fexpressions']."。"
            ];

            $images = explode(';',$typical_illegal_ad_tv_val['image']);

            foreach ($images as $images_val){
                $str_image = str_replace("/h/100","/h/400",$images_val);
                if(trim($str_image) != ""){
                    $data['content'][] = [
                        "type"=>"filltable",
                        "bookmark"=>"单个图片",
                        "data"=>[
                            ["index"=>1,"type"=>"image","content"=>$str_image]
                        ]
                    ];
                }
            }
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "违法次数：该广告累计违法".$typical_illegal_ad_tv_val['fad_illegal_times']."条次。"
            ];
        }
    }

    $typical_illegal_ad_tb = $StatisticalReport->ill_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$nj_media,2);

    $illegal_ad_tb_data = [];
    foreach ($typical_illegal_ad_tb as $illegal_ad_tb_val){
        $md5 = MD5($illegal_ad_tb_val['fad_name']);
        if(isset($illegal_ad_tb_data[$md5])){
            $illegal_ad_tb_data[$md5]['fmedianame'] .= '、'.$illegal_ad_tb_val['fmedianame'];
            $illegal_ad_tb_data[$md5]['fexpressions'] .= ';'.$illegal_ad_tb_val['fexpressions'];
            $illegal_ad_tb_data[$md5]['fad_illegal_times'] += $illegal_ad_tb_val['fad_illegal_times'];
        }else{
            $illegal_ad_tb_data[$md5] = $illegal_ad_tb_val;
        }
    }
    $typical_illegal_ad_tb = array_values($illegal_ad_tb_data);
    if(!empty($typical_illegal_ad_tb)){
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "广播"
        ];
        foreach ($typical_illegal_ad_tb as $typical_illegal_ad_tb_key=>$typical_illegal_ad_tb_val){
            if($typical_illegal_ad_tb_key > 9){
                break;
            }
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => ($typical_illegal_ad_tb_key+1)."、".$typical_illegal_ad_tb_val['fad_name']." (".$typical_illegal_ad_tb_val['ffullname'].")"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "发布媒体：".$typical_illegal_ad_tb_val['fmedianame']
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "违法表现：“".$typical_illegal_ad_tb_val['fexpressions']."。"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "违法次数：该广告累计违法".$typical_illegal_ad_tb_val['fad_illegal_times']."条次。"
            ];
        }
    }

    $typical_illegal_ad_tp = $StatisticalReport->ill_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$nj_media,3);
    $illegal_ad_tp_data = [];
    foreach ($typical_illegal_ad_tp as $illegal_ad_tp_val){
        $md5 = MD5($illegal_ad_tp_val['fad_name']);
        if(isset($illegal_ad_tp_data[$md5])){
            $illegal_ad_tp_data[$md5]['fmedianame'] .= '、'.$illegal_ad_tp_val['fmedianame'];
            $illegal_ad_tp_data[$md5]['image'] .= ';'.$illegal_ad_tp_val['image'];
            $illegal_ad_tp_data[$md5]['fexpressions'] .= ';'.$illegal_ad_tp_val['fexpressions'];
            $illegal_ad_tp_data[$md5]['fad_illegal_times'] += $illegal_ad_tp_val['fad_illegal_times'];
        }else{
            $illegal_ad_tp_data[$md5] = $illegal_ad_tp_val;
        }
    }
    $typical_illegal_ad_tp = array_values($illegal_ad_tp_data);
    if(!empty($typical_illegal_ad_tp)){
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "报纸"
        ];
        foreach ($typical_illegal_ad_tp as $typical_illegal_ad_tp_key=>$typical_illegal_ad_tp_val){
            if($typical_illegal_ad_tp_key > 9){
                break;
            }
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => ($typical_illegal_ad_tp_key+1)."、".$typical_illegal_ad_tp_val['fad_name']." (".$typical_illegal_ad_tp_val['ffullname'].")"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "发布媒体：".$typical_illegal_ad_tp_val['fmedianame']
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "违法表现：“".$typical_illegal_ad_tp_val['fexpressions']."。"
            ];
            $images = explode(';',$typical_illegal_ad_tp_val['image']);

            foreach ($images as $images_val){

                if(trim($images_val) != ""){
                    $data['content'][] = [
                        "type"=>"filltable",
                        "bookmark"=>"单个图片",
                        "data"=>[
                            ["index"=>1,"type"=>"image","content"=>$images_val]
                        ]
                    ];
                }
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "违法次数：该广告累计违法".$typical_illegal_ad_tp_val['fad_illegal_times']."条次。"
            ];
        }
    }

    $top_ten_data = array_merge($typical_illegal_ad_tv,$typical_illegal_ad_tb,$typical_illegal_ad_tp);
    $top_ten_data = $this->pxsf($top_ten_data,'fad_illegal_times');

    $top_ten = [];
    foreach ($top_ten_data as $top_ten_data_key=>$top_ten_data_val){
        if($top_ten_data_key != 0){

            if($top_ten_data_val['fad_illegal_times'] == $top_ten[$index]['fad_illegal_times']){
                $top_ten[$index]['fad_name'] .= '、'.$top_ten_data_val['fad_name'].'('.$top_ten_data_val['fmedianames'].')';
            }else{
                $index++;
                $top_ten[$index]['key'] = strval(count($top_ten)+1);
                $top_ten[$index]['fad_name'] = $top_ten_data_val['fad_name'].'('.$top_ten_data_val['fmedianame'].')';
                $top_ten[$index]['fad_illegal_times'] = strval($top_ten_data_val['fad_illegal_times']);
            }

        }else{
            $index = 0;
            $top_ten[$index]['key'] = '1';
            $top_ten[$index]['fad_name'] = $top_ten_data_val['fad_name'].'('.$top_ten_data_val['fmedianame'].')';
            $top_ten[$index]['fad_illegal_times'] = strval($top_ten_data_val['fad_illegal_times']);
        }
    }

    $top_ten = array_values($top_ten);
    $top_ten_data = [];
    foreach ($top_ten as $top_ten_key=>$top_ten_val){
        if($top_ten_key < 10){
            $top_ten_data[] = [$top_ten_val['key'],$top_ten_val['fad_name'],$top_ten_val['fad_illegal_times']];
        }
    }

    if(!empty($top_ten_data)){

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "四、违法广告前十名"
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "违法广告前十名",
            "data" => to_string($top_ten_data)
        ];

    }

    $typical_illegal_p_tv = $StatisticalReport->ill_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$usermedia[1],1,'',0,-1,1);//省级电视违法广告明细
    $typical_illegal_p_bc = $StatisticalReport->ill_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$usermedia[1],2,'',0,-1,1);//省级广播违法广告明细
    $typical_illegal_p_paper = $StatisticalReport->ill_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$usermedia[1],3,'',0,-1,1);//省级报纸违法广告明细

    $typical_illegal_c_tv = $StatisticalReport->ill_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$usermedia[0],1,'',0,-1,1);//市级电视违法广告明细
    $typical_illegal_c_bc = $StatisticalReport->ill_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$usermedia[0],2,'',0,-1,1);//市级广播违法广告明细
    $typical_illegal_c_paper = $StatisticalReport->ill_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$usermedia[0],3,'',0,-1,1);//市级报纸违法广告明细

    $typical_illegal_a_tv = $StatisticalReport->ill_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$usermedia[2],1,'',0,-1,1);//区级电视违法广告明细
    $typical_illegal_a_bc = $StatisticalReport->ill_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$usermedia[2],2,'',0,-1,1);//区级广播违法广告明细
    $typical_illegal_a_paper = $StatisticalReport->ill_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$usermedia[2],3,'',0,-1,1);//区级报纸违法广告明细

    $all_count = count($typical_illegal_p_tv) +
        count($typical_illegal_p_bc) +
        count($typical_illegal_p_paper) +
        count($typical_illegal_c_tv) +
        count($typical_illegal_c_bc) +
        count($typical_illegal_c_paper) +
        count($typical_illegal_a_tv) +
        count($typical_illegal_a_bc) +
        count($typical_illegal_a_paper);

    $p_count = count($typical_illegal_p_tv) +
        count($typical_illegal_p_bc) +
        count($typical_illegal_p_paper);

    $c_count = count($typical_illegal_c_tv) +
        count($typical_illegal_c_bc) +
        count($typical_illegal_c_paper);

    $a_count = count($typical_illegal_a_tv) +
        count($typical_illegal_a_bc) +
        count($typical_illegal_a_paper);

    if($all_count > 0){
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "五、违法广告明细"
        ];

        if($p_count > 0){
            //省
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "省级媒体发布的违法广告明细"
            ];

            if(!empty($typical_illegal_p_tv)){

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => "省级电视媒体发布的违法广告明细列表"
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "违法广告明细",
                    "data" => to_string($this->get_table_data($typical_illegal_p_tv))
                ];
            }

            if(!empty($typical_illegal_p_bc)) {

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => "省级广播媒体发布的违法广告明细"
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "违法广告明细",
                    "data" => to_string($this->get_table_data($typical_illegal_p_bc))
                ];
            }

            if(!empty($typical_illegal_p_paper)) {

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => "省级报纸媒体发布的违法广告明细"
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "违法广告明细",
                    "data" => to_string($this->get_table_data($typical_illegal_p_paper))
                ];
            }

        }


        //市
        if($p_count > 0){
            //省
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "市级媒体发布的违法广告明细"
            ];

            if(!empty($typical_illegal_c_tv)){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => "市级电视媒体发布的违法广告明细列表"
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "违法广告明细",
                    "data" => to_string($this->get_table_data($typical_illegal_c_tv))
                ];
            }

            if(!empty($typical_illegal_c_bc)) {

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => "市级广播媒体发布的违法广告明细"
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "违法广告明细",
                    "data" => to_string($this->get_table_data($typical_illegal_c_bc))
                ];
            }

            if(!empty($typical_illegal_c_paper)) {

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => "市级报纸媒体发布的违法广告明细"
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "违法广告明细",
                    "data" => to_string($this->get_table_data($typical_illegal_c_paper))
                ];
            }

        }


        //区
        if($p_count > 0){
            //省
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "区县级媒体发布的违法广告明细"
            ];

            if(!empty($typical_illegal_a_tv)){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => "区县级电视媒体发布的违法广告明细列表"
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "违法广告明细",
                    "data" => to_string($this->get_table_data($typical_illegal_a_tv))
                ];
            }

            if(!empty($typical_illegal_a_bc)) {

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => "区县级广播媒体发布的违法广告明细"
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "违法广告明细",
                    "data" => to_string($this->get_table_data($typical_illegal_a_bc))
                ];
            }

            if(!empty($typical_illegal_a_paper)) {

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => "区县级报纸媒体发布的违法广告明细"
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "违法广告明细",
                    "data" => to_string($this->get_table_data($typical_illegal_a_paper))
                ];
            }

        }
    }

    $report_data = json_encode($data);
    //echo $report_data;exit;
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_PORT => "8081",
        CURLOPT_URL => C('REPORT_SERVER'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 6000,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
        CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
    } else {
        $response = json_decode($response,true);
        if(empty($response['ReportFileName'])){
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！错误信息：'.$response));
        }

        $system_num = getconfig('system_num');

        //将生成记录保存
        $focus = I('focus',0);
        $pnname = $regulatorpersonInfo['regulatorname'].$phase_ym.'月报';
        $data['pnname'] 			= $pnname;
        $data['pntype'] 			= 30;
        $data['pnfiletype'] 		= 10;
        $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
        $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
        $data['pncreatetime'] 		= date('Y-m-d H:i:s');
        $data['pnurl'] 				= $response['ReportFileName'];
        $data['pnhtml'] 			= '';
        $data['pnfocus']            = $focus;
        $data['pntrepid']           = $regulatorpersonInfo['fregulatorpid'];
        $data['pncreatepersonid']   = $regulatorpersonInfo['fid'];
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
        }
    }

}

    public function create_monthpresentation2_bak_20190719($date,$focus,$userid) {

        header("Content-type: text/html; charset=utf-8");
        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);
        $report_start_date = strtotime($date);//指定月份月初时间戳
        $report_end_date = strtotime($date.'-'.date('t', strtotime($date)).' 23:59:59');
        $month_last = date('m',$report_start_date) - 1;
        $year_last = date('Y',$report_start_date);
        if($month_last == 0){
            $year_last = $year_last - 1;
        }
        //$report_end_date = mktime(23, 59, 59, date('m', strtotime($date))+1, 00);//指定月份月末时间戳

        if($report_end_date > time()){
            $report_end_date = time();
        }

        //获取上个周期的日期范围
        $last_date = date('Y-m',strtotime(date("Y",strtotime($date)).'-'.(date("m",strtotime($date))-1)));
        $last_s_time = strtotime($last_date);
        $last_e_time = strtotime($year_last.'-'.$month_last.'-'.date('t', strtotime($year_last.'-'.$month_last)).' 23:59:59');

        //获取同期的日期范围
        $last_year_date = date('Y-m',strtotime((date("Y",strtotime($date))-1).'-'.(date("m",strtotime($date)))));
        $last_year_s_time = strtotime($last_year_date);
        $last_year_e_time = mktime(23, 59, 59, date('m', strtotime($last_year_date))+1, 00,(date("Y",strtotime($date))-1));

        $date_ym = date('Y年m月',$report_start_date);//年月字符

        $regulatorpersonInfo = M('tregulatorperson')
            ->field('fregulatorid as bumen_id,fid')
            ->where('fid='.$userid)->find();
        if(!empty($regulatorpersonInfo)){
            $regulatorInfo = M('tregulator')->where(array('fid' => $regulatorpersonInfo['bumen_id']))->find();//监管机构信息
            $regulatorpersonInfo['fregulatorid']        = $regulatorInfo['fid'];//部门ID
            $regulatorpersonInfo['fregulatorpid']       = D('Function')->get_tregulatoraction($regulatorInfo['fid']);//获取当前单位所属机构ID
            $do_tr = M('tregulator')->field('tregulator.fname,tregion.fname as regionname,tregion.fname1 as regionname1,tregulator.fregionid')->join('tregion on tregulator.fregionid=tregion.fid')->where(array('tregulator.fid' => $regulatorpersonInfo['fregulatorpid']))->find();//获取机构信息
            $regulatorpersonInfo['regulatorpname']      = $do_tr['fname'];//所属机构名称
            $regulatorpersonInfo['regionid']            = $do_tr['fregionid'];//机构行政区划ID
            $regulatorpersonInfo['regionname']          = $do_tr['regionname'];//机构行政区划名称
            $regulatorpersonInfo['regionname1']         = $do_tr['regionname1'];//机构行政区划全名称
            $regulatorpersonInfo['regulator_regionid']  = $regulatorInfo['fregionid'];//部门行政区划ID
            $regulatorpersonInfo['regulatorname']       = $regulatorInfo['fname'];//部门名称
            //用户媒介权限
            $mediajurisdiction = D('Function')->get_mediajurisdiction($regulatorInfo['fid'],[],[],$regulatorpersonInfo['fid']);//获取媒体权限组
            $nj_media   = $mediajurisdiction?$mediajurisdiction:array('0');

            $do_tt = M('tbn_media_grant')->field('fmedia_id')->where('freg_id='.$regulatorpersonInfo['fregulatorpid'])->select();
            foreach ($do_tt as $key => $value) {
                array_push($nj_media, $value['fmedia_id']);
            }
        }
        $regulatorpname = $regulatorpersonInfo['regulatorpname'];//机构名
        $regionid = $regulatorpersonInfo['regionid'];//机构行政区划ID

        $date_table = date('Ym',strtotime($date)).'_'.substr($regionid,0,2);//组合表
        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);
        $date_md = date('m月d日',$report_end_date);//结束月日字符
        $phase_num = date('（Y年第m期）',$report_start_date);//第几期
        $phase_ym = date('Y年m月',$report_start_date);//第几期
        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数


        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型

        $system_num = getconfig('system_num');//获取国家局标记

        $nj_media = M('tmedia_temp')->where(['ftype'=>1,'fcustomer'=>$system_num,'fuserid'=>session('regulatorpersonInfo.fid')])->getField('fmediaid',true);
        $usermedia = $StatisticalReport->distinguish_media(implode(',',$nj_media),$regionid);//是否是选择了省直属

        $all_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$nj_media,'','','fad_illegal_times_rate','fmediaclass');//0809
        $all_data_last = $StatisticalReport->ad_monitor_customize($date_table,$last_s_time,$last_e_time,$nj_media,'','','fad_illegal_times_rate','fmediaclass');

        $all_data_last_year = $StatisticalReport->ad_monitor_customize($date_table,$last_year_s_time,$last_year_e_time,$nj_media,'','','fad_illegal_times_rate','fmediaclass');//0809

        $data_all_list = [];
        $all_fad_times = 0;
        $all_fad_illegal_times = 0;
        $all_last_fad_times = 0;
        $all_last_fad_illegal_times = 0;
        foreach ($all_data as $all_data_key=>$all_data_val){
            $hb_illegal_times_rate = 0;
            if(!isset($all_data_val['fad_illegal_times_rate']) || !isset($all_data_val['prv_fad_illegal_times_rate'])){
                $hb_illegal_times_rate == 0;
            }else{
                $hb_illegal_times_rate = $all_data_val['fad_illegal_times_rate'] - $all_data_val['prv_fad_illegal_times_rate'];//本周期总违法率减去上周期总违法率
            }
            //环比违法率
            if($hb_illegal_times_rate == 0){
                $hb_illegal_times_rate_array[] = $all_data_val['fclass'].'无变化';
            }elseif($hb_illegal_times_rate > 0){
                $hb_illegal_times_rate_array[] = $all_data_val['fclass'].'环比上升'.$hb_illegal_times_rate.'个百分点';
            }elseif($hb_illegal_times_rate < 0){
                $hb_illegal_times_rate_array[] = $all_data_val['fclass'].'环比下降'.($hb_illegal_times_rate*-1).'个百分点';
            }

            $all_fad_times += $all_data_val['fad_times'];
            $all_fad_illegal_times += $all_data_val['fad_illegal_times'];
        }

        $hb_illegal_times_rate_array = implode('、',$hb_illegal_times_rate_array);
        foreach ($all_data_last as $all_data_last_key => $all_data_last_val){
            $all_last_fad_times += $all_data_last_val['fad_times'];
            $all_last_fad_illegal_times += $all_data_last_val['fad_illegal_times'];
        }
        foreach ($all_data_last_year as $all_data_last_year_key => $all_data_last_year_val){
            $all_last_year_fad_times += $all_data_last_year_val['fad_times'];
            $all_last_year_fad_illegal_times += $all_data_last_year_val['fad_illegal_times'];
        }

        $a = round(($all_fad_illegal_times/$all_fad_times)*100,2);//本周期总违法率
        $a1 = round(($all_last_fad_illegal_times/$all_last_fad_times)*100,2);//上周期总违法率
        $hb_fad_illegal_times_rate = $a - $a1;//本周期总违法率减去上周期总违法率
        $hb_fad_times = $all_fad_times - $all_last_fad_times;//本周期发布量减去上周期发布量
        //环比违法率
        if($hb_fad_illegal_times_rate == 0){
            $hb_fad_illegal_times_rate = '环比未有明显变化';
        }elseif($hb_fad_illegal_times_rate > 0){
            $hb_fad_illegal_times_rate = '环比上升'.$hb_fad_illegal_times_rate.'个百分点';
        }elseif($hb_fad_illegal_times_rate < 0){
            $hb_fad_illegal_times_rate = '环比下降'.($hb_fad_illegal_times_rate*-1).'个百分点';
        }
        //环比发布量
        if($hb_fad_times == 0){
            $hb_fad_times = '环比无变化';
        }elseif($hb_fad_times > 0){
            $hb_fad_times = '环比增加'.$hb_fad_times.'条次';
        }elseif($hb_fad_times < 0){
            $hb_fad_times = '环比减少'.($hb_fad_times*-1).'条次';
        }

        if(!empty($all_data_last_year)){
            //同比违法率
            $c = round(($all_last_year_fad_illegal_times/$all_last_year_fad_times)*100,2);//本月总违法率
            $tb_fad_illegal_times_rate = $a - $c;//本月总违法率减去去年本月总违法率
            if($tb_fad_illegal_times_rate == 0){
                $tb_fad_illegal_times_rate = '同比无变化';
            }elseif($tb_fad_illegal_times_rate > 0){
                $tb_fad_illegal_times_rate = '同比上升'.$tb_fad_illegal_times_rate.'个百分点，';
            }elseif($tb_fad_illegal_times_rate < 0){
                $tb_fad_illegal_times_rate = '同比下降'.($tb_fad_illegal_times_rate*-1).'个百分点，';
            }
        }else{
            $tb_fad_illegal_times_rate = '';
        }



        $province_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$usermedia[1],'','','fad_illegal_times_rate','fmediaid');//省媒体
        $province_tv_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$usermedia[1],'01','','fad_illegal_times_rate','fmediaid');//省媒体电视
        $province_tb_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$usermedia[1],'02','','fad_illegal_times_rate','fmediaid');//省媒体广播
        $province_tp_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$usermedia[1],'03','','fad_illegal_times_rate','fmediaid');//省媒体报纸
        //电视
        $province_media_tv_data = [];
        foreach ($province_tv_data as $province_tv_data_key => $province_tv_data_val){
            $province_media_tv_data[] = [
                strval($province_tv_data_key+1),
                $province_tv_data_val['tmedia_name'],
                $province_tv_data_val['fad_times'],
                $province_tv_data_val['fad_illegal_times'],
                $province_tv_data_val['fad_illegal_times_rate'].'%'
            ];
            $province_tv_times_last += $province_tv_data_val['fad_times'];
            $province_tv_illegal_times_last += $province_tv_data_val['fad_illegal_times'];
        }
        $province_tv_illegal_times_rate_last = round(($province_tv_illegal_times_last/$province_tv_times_last)*100,2).'%';
        $province_media_tv_data[] = [strval(count($province_media_tv_data)+1),'小计',strval($province_tv_times_last),strval($province_tv_illegal_times_last),$province_tv_illegal_times_rate_last];//最后统计

        //广播
        $province_media_tb_data = [];
        foreach ($province_tb_data as $province_tb_data_key => $province_tb_data_val){
            $province_media_tb_data[] = [
                strval($province_tb_data_key+1),
                $province_tb_data_val['tmedia_name'],
                $province_tb_data_val['fad_times'],
                $province_tb_data_val['fad_illegal_times'],
                $province_tb_data_val['fad_illegal_times_rate'].'%'
            ];
            $province_tb_times_last += $province_tb_data_val['fad_times'];
            $province_tb_illegal_times_last += $province_tb_data_val['fad_illegal_times'];
        }
        $province_tb_illegal_times_rate_last = round(($province_tb_illegal_times_last/$province_tb_times_last)*100,2).'%';
        $province_media_tb_data[] = [strval(count($province_media_tb_data)+1),'小计',strval($province_tb_times_last),strval($province_tb_illegal_times_last),$province_tb_illegal_times_rate_last];//最后统计
        //报纸
        $province_media_tp_data = [];
        foreach ($province_tp_data as $province_tp_data_key => $province_tp_data_val){
            $province_media_tp_data[] = [
                strval($province_tp_data_key+1),
                $province_tp_data_val['tmedia_name'],
                $province_tp_data_val['fad_times'],
                $province_tp_data_val['fad_illegal_times'],
                $province_tp_data_val['fad_illegal_times_rate'].'%'
            ];
            $province_tp_times_last += $province_tp_data_val['fad_times'];
            $province_tp_illegal_times_last += $province_tp_data_val['fad_illegal_times'];
        }
        $province_tp_illegal_times_rate_last = round(($province_tp_illegal_times_last/$province_tp_times_last)*100,2).'%';
        $province_media_tp_data[] = [strval(count($province_media_tp_data)+1),'小计',strval($province_tp_times_last),strval($province_tp_illegal_times_last),$province_tp_illegal_times_rate_last];//最后统计


        $province_fmediaclass_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$usermedia[1],'','','fad_illegal_times_rate','fmediaclass');//省媒介类型
        $province_media_class_data = [];
        $province_media_class_times_data[] = ['','发布量'];
        $province_media_class_illegal_times_data[] = ['','违法发布量'];
        $province_fad_illegal_times = 0;
        foreach ($province_fmediaclass_data as $province_fmediaclass_data_key=>$province_fmediaclass_data_val){
            $province_media_class_data[] = [
                $province_fmediaclass_data_val['fclass'],
                $province_fmediaclass_data_val['fad_times'],
                $province_fmediaclass_data_val['hb_fad_times'] ? $province_fmediaclass_data_val['hb_fad_times']:'无变化',
                $province_fmediaclass_data_val['fad_illegal_times'],
                $province_fmediaclass_data_val['hb_fad_illegal_times'] ? $province_fmediaclass_data_val['hb_fad_illegal_times']:'无变化',
                $province_fmediaclass_data_val['fad_illegal_times_rate'].'%',
                $province_fmediaclass_data_val['hb_fad_illegal_times_rate'] ? $province_fmediaclass_data_val['hb_fad_illegal_times_rate']:'无变化',
            ];
            $province_media_class_times_data[] = [
                $province_fmediaclass_data_val['fclass'],
                $province_fmediaclass_data_val['fad_times']
            ];
            $province_media_class_illegal_times_data[] = [
                $province_fmediaclass_data_val['fclass'],
                $province_fmediaclass_data_val['fad_illegal_times']
            ];
            $province_fad_illegal_times += $province_fmediaclass_data_val['fad_illegal_times'];
        }



        $city_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$usermedia[0],'','','fad_illegal_times_rate','fmediaid');//市媒体全部
        $city_tv_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$usermedia[0],'01','','fad_illegal_times_rate','fmediaid');//市媒体电视
        $city_tb_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$usermedia[0],'02','','fad_illegal_times_rate','fmediaid');//市媒体广播
        $city_tp_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$usermedia[0],'03','','fad_illegal_times_rate','fmediaid');//市媒体报纸

//电视
        $city_media_tv_data = [];
        foreach ($city_tv_data as $city_tv_data_key => $city_tv_data_val){
            $city_media_tv_data[] = [
                strval($city_tv_data_key+1),
                $city_tv_data_val['tmedia_name'],
                $city_tv_data_val['fad_times'],
                $city_tv_data_val['fad_illegal_times'],
                $city_tv_data_val['fad_illegal_times_rate'].'%'
            ];
            $city_tv_times_last += $city_tv_data_val['fad_times'];
            $city_tv_illegal_times_last += $city_tv_data_val['fad_illegal_times'];
        }
        $city_tv_illegal_times_rate_last = round(($city_tv_illegal_times_last/$city_tv_times_last)*100,2).'%';
        $city_media_tv_data[] = [strval(count($city_media_tv_data)+1),'小计',strval($city_tv_times_last),strval($city_tv_illegal_times_last),$city_tv_illegal_times_rate_last];//最后统计

//广播
        $city_media_tb_data = [];
        foreach ($city_tb_data as $city_tb_data_key => $city_tb_data_val){
            $city_media_tb_data[] = [
                strval($city_tb_data_key+1),
                $city_tb_data_val['tmedia_name'],
                $city_tb_data_val['fad_times'],
                $city_tb_data_val['fad_illegal_times'],
                $city_tb_data_val['fad_illegal_times_rate'].'%'
            ];
            $city_tb_times_last += $city_tb_data_val['fad_times'];
            $city_tb_illegal_times_last += $city_tb_data_val['fad_illegal_times'];
        }
        $city_tb_illegal_times_rate_last = round(($city_tb_illegal_times_last/$city_tb_times_last)*100,2).'%';
        $city_media_tb_data[] = [strval(count($city_media_tb_data)+1),'小计',strval($city_tb_times_last),strval($city_tb_illegal_times_last),$city_tb_illegal_times_rate_last];//最后统计

//报纸
        $city_media_tp_data = [];
        foreach ($city_tp_data as $city_tp_data_key => $city_tp_data_val){
            $city_media_tp_data[] = [
                strval($city_tp_data_key+1),
                $city_tp_data_val['tmedia_name'],
                $city_tp_data_val['fad_times'],
                $city_tp_data_val['fad_illegal_times'],
                $city_tp_data_val['fad_illegal_times_rate'].'%'
            ];
            $city_tp_times_last += $city_tp_data_val['fad_times'];
            $city_tp_illegal_times_last += $city_tp_data_val['fad_illegal_times'];
        }
        $city_tp_illegal_times_rate_last = round(($city_tp_illegal_times_last/$city_tp_times_last)*100,2).'%';
        $city_media_tp_data[] = [strval(count($city_media_tp_data)+1),'小计',strval($city_tp_times_last),strval($city_tp_illegal_times_last),$city_tp_illegal_times_rate_last];//最后统计

        $city_fmediaclass_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$usermedia[0],'','','fad_illegal_times_rate','fmediaclass');//市媒介类型
        $city_media_class_data = [];
        $city_media_class_times_data[] = ['','发布量'];
        $city_media_class_illegal_times_data[] = ['','违法发布量'];
        $city_fad_illegal_times = 0;
        foreach ($city_fmediaclass_data as $city_fmediaclass_data_key=>$city_fmediaclass_data_val){
            $city_media_class_data[] = [
                $city_fmediaclass_data_val['fclass'],
                $city_fmediaclass_data_val['fad_times'],
                $city_fmediaclass_data_val['hb_fad_times']?$city_fmediaclass_data_val['hb_fad_times']:'无变化',
                $city_fmediaclass_data_val['fad_illegal_times'],
                $city_fmediaclass_data_val['hb_fad_illegal_times']?$city_fmediaclass_data_val['hb_fad_illegal_times']:'无变化',
                $city_fmediaclass_data_val['fad_illegal_times_rate'].'%',
                $city_fmediaclass_data_val['hb_fad_illegal_times_rate']?$city_fmediaclass_data_val['hb_fad_illegal_times_rate']:'无变化',
            ];
            $city_media_class_times_data[] = [
                $city_fmediaclass_data_val['fclass'],
                $city_fmediaclass_data_val['fad_times']
            ];
            $city_media_class_illegal_times_data[] = [
                $city_fmediaclass_data_val['fclass'],
                $city_fmediaclass_data_val['fad_illegal_times']
            ];
            $city_fad_illegal_times += $city_fmediaclass_data_val['fad_illegal_times'];
        }

        $area_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$usermedia[2],'','','fad_illegal_times_rate','fmediaid');//区媒体全部
        $area_tv_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$usermedia[2],'01','','fad_illegal_times_rate','fmediaid');//区媒体电视
        $area_tb_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$usermedia[2],'02','','fad_illegal_times_rate','fmediaid');//区媒体广播
        $area_tp_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$usermedia[2],'03','','fad_illegal_times_rate','fmediaid');//区媒体报纸

//电视
        $area_media_tv_data = [];
        foreach ($area_tv_data as $area_tv_data_key => $area_tv_data_val){
            $area_media_tv_data[] = [
                strval($area_tv_data_key+1),
                $area_tv_data_val['tmedia_name'],
                $area_tv_data_val['fad_times'],
                $area_tv_data_val['fad_illegal_times'],
                $area_tv_data_val['fad_illegal_times_rate'].'%'
            ];
            $area_tv_times_last += $area_tv_data_val['fad_times'];
            $area_tv_illegal_times_last += $area_tv_data_val['fad_illegal_times'];
        }
        $area_tv_illegal_times_rate_last = round(($area_tv_illegal_times_last/$area_tv_times_last)*100,2).'%';
        if(!empty($area_media_tv_data)) {

            $area_media_tv_data[] = [strval(count($area_media_tv_data) + 1), '小计', strval($area_tv_times_last), strval($area_tv_illegal_times_last), $area_tv_illegal_times_rate_last];//最后统计
        }

//广播
        $area_media_tb_data = [];
        foreach ($area_tb_data as $area_tb_data_key => $area_tb_data_val){
            $area_media_tb_data[] = [
                strval($area_tb_data_key+1),
                $area_tb_data_val['tmedia_name'],
                $area_tb_data_val['fad_times'],
                $area_tb_data_val['fad_illegal_times'],
                $area_tb_data_val['fad_illegal_times_rate'].'%'
            ];
            $area_tb_times_last += $area_tb_data_val['fad_times'];
            $area_tb_illegal_times_last += $area_tb_data_val['fad_illegal_times'];
        }
        $area_tb_illegal_times_rate_last = round(($area_tb_illegal_times_last/$area_tb_times_last)*100,2).'%';
        if(!empty($area_media_tb_data)) {
            $area_media_tb_data[] = [strval(count($area_media_tb_data) + 1), '小计', strval($area_tb_times_last), strval($area_tb_illegal_times_last), $area_tb_illegal_times_rate_last];//最后统计
        }
//报纸
        $area_media_tp_data = [];
        foreach ($area_tp_data as $area_tp_data_key => $area_tp_data_val){
            $area_media_tp_data[] = [
                strval($area_tp_data_key+1),
                $area_tp_data_val['tmedia_name'],
                $area_tp_data_val['fad_times'],
                $area_tp_data_val['fad_illegal_times'],
                $area_tp_data_val['fad_illegal_times_rate'].'%'
            ];
            $area_tp_times_last += $area_tp_data_val['fad_times'];
            $area_tp_illegal_times_last += $area_tp_data_val['fad_illegal_times'];
        }
        $area_tp_illegal_times_rate_last = round(($area_tp_illegal_times_last/$area_tp_times_last)*100,2).'%';
        if(!empty($area_media_tp_data)){
            $area_media_tp_data[] = [strval(count($area_media_tp_data)+1),'小计',strval($area_tp_times_last),strval($area_tp_illegal_times_last),$area_tp_illegal_times_rate_last];//最后统计
        }

        $area_fmediaclass_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$usermedia[2],'','','fad_illegal_times_rate','fmediaclass');//区媒介类型
        $area_media_class_data = [];
        $area_media_class_times_data[] = ['','发布量'];
        $area_media_class_illegal_times_data[] = ['','违法发布量'];
        $area_fad_illegal_times = 0;
        foreach ($area_fmediaclass_data as $area_fmediaclass_data_key=>$area_fmediaclass_data_val){
            $area_media_class_data[] = [
                $area_fmediaclass_data_val['fclass'],
                $area_fmediaclass_data_val['fad_times'],
                $area_fmediaclass_data_val['hb_fad_times']?$area_fmediaclass_data_val['hb_fad_times']:'无变化',
                $area_fmediaclass_data_val['fad_illegal_times'],
                $area_fmediaclass_data_val['hb_fad_illegal_times']?$area_fmediaclass_data_val['hb_fad_illegal_times']:'无变化',
                $area_fmediaclass_data_val['fad_illegal_times_rate'].'%',
                $area_fmediaclass_data_val['hb_fad_illegal_times_rate']?$area_fmediaclass_data_val['hb_fad_illegal_times_rate']:'无变化',
            ];
            $area_media_class_times_data[] = [
                $area_fmediaclass_data_val['fclass'],
                $area_fmediaclass_data_val['fad_times']
            ];
            $area_media_class_illegal_times_data[] = [
                $area_fmediaclass_data_val['fclass'],
                $area_fmediaclass_data_val['fad_illegal_times']
            ];
            $area_fad_illegal_times += $area_fmediaclass_data_val['fad_illegal_times'];
        }

        $province_media_names = [];//省级媒体名称
        $city_media_names = [];//市级媒体名称
        $area_media_names = [];//区级媒体名称

        foreach ($province_data as $province_data_key=>$province_data_val){
            $province_media_names[] = $province_data_val['tmedia_name'];
        }

        foreach ($city_data as $city_data_key => $city_data_val){
            $city_media_names[] = $city_data_val['tmedia_name'];
        }

        foreach ($area_data as $area_data_key => $area_data_val){
            $area_media_names[] = $area_data_val['tmedia_name'];
        }

        $province_media_count = count($province_media_names);//省级媒体数量
        $city_media_count = count($city_media_names);//市级媒体数量
        $area_media_count = count($area_media_names);//区级媒体数量

        $province_media_names = implode('、',$province_media_names);//省级媒体名称
        $city_media_names = implode('、',$city_media_names);//市级媒体名称
        $area_media_names = implode('、',$area_media_names);//市级媒体名称

        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $regulatorpersonInfo['regulator_regionid'].'_'.time().'.docx',
            'content' => []
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体45号居中",
            "text" => $regulatorpersonInfo['regulatorpname']
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "红色宋体小一居中",
            "text" => "广告监测报告"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "红色宋体四号居中",
            "text" => $phase_num
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠右",
            "text" => $phase_ym
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "一、广告监测情况综述"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三正文",
            "text" => "广告监测中心".$phase_ym."共监测".($city_media_count+$province_media_count+$area_media_count)."家媒体，其中省属媒体".$province_media_count."家(".$province_media_names.")。市属媒体".$city_media_count."家(".$city_media_names.")。区属媒体".$area_media_count."家(".$area_media_names.")。"
        ];


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三正文",
            "text" => "本月，监测中心监测媒体发布的各类广告".$all_fad_times."条次，".$hb_fad_times."，其中涉嫌违法广告".$all_fad_illegal_times."条次，监测违法率为".$a."%，".$hb_fad_illegal_times_rate."。"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "(一)分类监测情况"
        ];



        if(!empty($province_media_class_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "省属媒体："
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "媒体类别监测列表",
                "data" => $province_media_class_data
            ];
        }

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "媒体广告监测量分布图",
            "data" => $province_media_class_times_data
        ];

        if($province_fad_illegal_times == 0){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "暂未监测到违法广告"
            ];
        }else{
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "媒体广告违法情况分布图",
                "data" => $province_media_class_illegal_times_data
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "报刊媒体："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒体监测情况列表",
            "data" => $province_media_tp_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "电视媒体："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒体监测情况列表",
            "data" => $province_media_tv_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "广播媒体："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒体监测情况列表",
            "data" => $province_media_tb_data
        ];
        $tv_public_welfare_data_province = $StatisticalReport->tv_public_welfare($usermedia[1],$date_table);
        $tv_public_welfare_province_list = [];
        foreach ($tv_public_welfare_data_province as $tv_public_welfare_data_key=>$tv_public_welfare_data_val){
            $tv_public_welfare_province_list[] = [
                $tv_public_welfare_data_val['tmname'],
                $tv_public_welfare_data_val['gysl'],
            ];
        }

        if(!empty($tv_public_welfare_province_list)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "省属电视媒体公益广告发布情况表："
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "电视公益监测列表",
                "data" => $tv_public_welfare_province_list
            ];
        }/*else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "本期未监测到电视公益广告"
            ];
        }*/
        $tb_public_welfare_data_province = $StatisticalReport->tb_public_welfare($usermedia[1],$date_table);
        $tb_public_welfare_province_list = [];
        foreach ($tb_public_welfare_data_province as $tb_public_welfare_data_key=>$tb_public_welfare_data_val){
            $tb_public_welfare_province_list[] = [
                $tb_public_welfare_data_val['tmname'],
                $tb_public_welfare_data_val['gysl1'],
                $tb_public_welfare_data_val['gysl2'],
            ];
        }

        if(!empty($tb_public_welfare_province_list)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "省属广播媒体公益广告发布情况表："
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "广播公益监测列表",
                "data" => $tb_public_welfare_province_list
            ];
        }/*else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "本期未监测到广播公益广告"
            ];
        }*/
        $tp_public_welfare_data_province = $StatisticalReport->tp_public_welfare($usermedia[0],$report_start_date,$report_end_date);
        $tp_public_welfare_province_list = [];
        foreach ($tp_public_welfare_data_province as $tp_public_welfare_data_key=>$tp_public_welfare_data_val){
            $tp_public_welfare_province_list[] = [
                $tp_public_welfare_data_val['tmname'],
                $tp_public_welfare_data_val['gysl'],
            ];
        }
        if(!empty($tp_public_welfare_province_list)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "省属报刊媒体公益广告发布情况表："
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "报刊公益广告列表",
                "data" => $tp_public_welfare_province_list
            ];
        }/*else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "本期未监测到报刊公益广告"
            ];
        }*/




        if(!empty($city_media_class_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "市属媒体："
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "媒体类别监测列表",
                "data" => $city_media_class_data
            ];
        }

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "媒体广告监测量分布图",
            "data" => $city_media_class_times_data
        ];

        if($city_fad_illegal_times == 0){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "暂未监测到违法广告"
            ];
        }else{
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "媒体广告违法情况分布图",
                "data" => $city_media_class_illegal_times_data
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "报刊媒体："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒体监测情况列表",
            "data" => $city_media_tp_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "电视媒体："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒体监测情况列表",
            "data" => $city_media_tv_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "广播媒体："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒体监测情况列表",
            "data" => $city_media_tb_data
        ];
        $tv_public_welfare_data = $StatisticalReport->tv_public_welfare($usermedia[0],$date_table);
        $tv_public_welfare_list = [];
        foreach ($tv_public_welfare_data as $tv_public_welfare_data_key=>$tv_public_welfare_data_val){
            $tv_public_welfare_list[] = [
                $tv_public_welfare_data_val['tmname'],
                strval($tv_public_welfare_data_val['gysl']),
            ];
        }

        if(!empty($tv_public_welfare_list)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "电视媒体公益广告发布情况表："
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "电视公益监测列表",
                "data" => $tv_public_welfare_list
            ];
        }/*else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "本期未监测到电视公益广告"
            ];
        }*/
        $tb_public_welfare_data = $StatisticalReport->tb_public_welfare($usermedia[0],$date_table);
        $tb_public_welfare_list = [];
        foreach ($tb_public_welfare_data as $tb_public_welfare_data_key=>$tb_public_welfare_data_val){
            $tb_public_welfare_list[] = [
                $tb_public_welfare_data_val['tmname'],
                strval($tb_public_welfare_data_val['gysl1']),
                strval($tb_public_welfare_data_val['gysl2']),
            ];
        }

        if(!empty($tb_public_welfare_list)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "广播媒体公益广告发布情况表："
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "广播公益监测列表",
                "data" => $tb_public_welfare_list
            ];
        }/*else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "本期未监测到广播公益广告"
            ];
        }*/

        $tp_public_welfare_data = $StatisticalReport->tp_public_welfare($usermedia[0],$report_start_date,$report_end_date);
        $tp_public_welfare_list = [];
        foreach ($tp_public_welfare_data as $tp_public_welfare_data_key=>$tp_public_welfare_data_val){
            $tp_public_welfare_list[] = [
                $tp_public_welfare_data_val['tmname'],
                strval($tp_public_welfare_data_val['gysl']),
            ];
        }
        if(!empty($tp_public_welfare_list)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "报刊媒体公益广告发布情况表："
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "报刊公益广告列表",
                "data" => $tp_public_welfare_list
            ];
        }/*else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "本期未监测到报刊公益广告"
            ];
        }*/





        //1008
        if(!empty($area_media_class_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "区属媒体："
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "媒体类别监测列表",
                "data" => $area_media_class_data
            ];
        }

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "媒体广告监测量分布图",
            "data" => $area_media_class_times_data
        ];

        if($area_fad_illegal_times == 0){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "暂未监测到违法广告"
            ];
        }else{
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "媒体广告违法情况分布图",
                "data" => $area_media_class_illegal_times_data
            ];
        }

        if(!empty($area_media_tp_data)) {
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "报刊媒体："
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "媒体监测情况列表",
                "data" => $area_media_tp_data
            ];
        }

        if(!empty($area_media_tv_data)) {

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "电视媒体："
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "媒体监测情况列表",
                "data" => $area_media_tv_data
            ];
        }

        if(!empty($area_media_tb_data)) {
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "广播媒体："
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "媒体监测情况列表",
                "data" => $area_media_tb_data
            ];
        }
        $tv_public_welfare_data = $StatisticalReport->tv_public_welfare($usermedia[2],$date_table);
        $tv_public_welfare_list = [];
        foreach ($tv_public_welfare_data as $tv_public_welfare_data_key=>$tv_public_welfare_data_val){
            $tv_public_welfare_list[] = [
                $tv_public_welfare_data_val['tmname'],
                strval($tv_public_welfare_data_val['gysl']),
            ];
        }

        if(!empty($tv_public_welfare_list)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "区县级电视媒体公益广告发布情况表："
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "电视公益监测列表",
                "data" => $tv_public_welfare_list
            ];
        }/*else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "本期未监测到电视公益广告"
            ];
        }*/
        $tb_public_welfare_data = $StatisticalReport->tb_public_welfare($usermedia[2],$date_table);
        $tb_public_welfare_list = [];
        foreach ($tb_public_welfare_data as $tb_public_welfare_data_key=>$tb_public_welfare_data_val){
            $tb_public_welfare_list[] = [
                $tb_public_welfare_data_val['tmname'],
                strval($tb_public_welfare_data_val['gysl1']),
                strval($tb_public_welfare_data_val['gysl2']),
            ];
        }

        if(!empty($tb_public_welfare_list)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "区县级广播媒体公益广告发布情况表："
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "广播公益监测列表",
                "data" => $tb_public_welfare_list
            ];
        }/*else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "本期未监测到广播公益广告"
            ];
        }*/

        $tp_public_welfare_data = $StatisticalReport->tp_public_welfare($usermedia[2],$report_start_date,$report_end_date);
        $tp_public_welfare_list = [];
        foreach ($tp_public_welfare_data as $tp_public_welfare_data_key=>$tp_public_welfare_data_val){
            $tp_public_welfare_list[] = [
                $tp_public_welfare_data_val['tmname'],
                strval($tp_public_welfare_data_val['gysl']),
            ];
        }
        if(!empty($tp_public_welfare_list)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "区县级报刊媒体公益广告发布情况表："
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "报刊公益广告列表",
                "data" => $tp_public_welfare_list
            ];
        }/*else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三正文",
                "text" => "本期未监测到报刊公益广告"
            ];
        }*/





        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "（二）行业监测情况"
        ];

        $eight_categories = ['01','02','03','13','06','1201','04','08'];//重点整治八大类
        $eight_categories_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$nj_media,'',$eight_categories,'fad_illegal_times_rate','fcode');//
        $eight_categories_data_last = $StatisticalReport->ad_monitor_customize($date_table,$last_s_time,$last_e_time,$nj_media,'',$eight_categories,'fad_illegal_times_rate','fcode');//
        $eight_ad_class_data = [];
        $eight_ad_class_hb_data[] = ['','本期','上期'];
        $eight_ad_class_illegal_data[] = ['','违法发布量'];
        $eight_ad_class_illegal_times = 0;

        foreach ($eight_categories_data as $eight_categories_data_key=>$eight_categories_data_val){
            $eight_ad_class_data[] = [
                $eight_categories_data_val['tadclass_name'],
                $eight_categories_data_val['fad_times'],
                $eight_categories_data_val['hb_fad_times'] ? $eight_categories_data_val['hb_fad_times']:'无变化',
                $eight_categories_data_val['fad_illegal_times'],
                $eight_categories_data_val['hb_fad_illegal_times'] ? $eight_categories_data_val['hb_fad_illegal_times']:'无变化',
                $eight_categories_data_val['fad_illegal_times_rate'].'%',
                $eight_categories_data_val['hb_fad_illegal_times_rate'] ? $eight_categories_data_val['hb_fad_illegal_times_rate']:'无变化',
            ];
            $eight_ad_class_hb_data[] = [
                $eight_categories_data_val['tadclass_name'],
                $eight_categories_data_val['fad_illegal_times_rate'].'%',
                $eight_categories_data_val['prv_fad_illegal_times_rate'].'%'
            ];
            $eight_ad_class_illegal_data[] = [
                $eight_categories_data_val['tadclass_name'],
                $eight_categories_data_val['fad_illegal_times']
            ];
            $eight_ad_class_times += $eight_categories_data_val['fad_times'];
            $eight_ad_class_illegal_times += $eight_categories_data_val['fad_illegal_times'];
        }

        foreach ($eight_categories_data_last as $eight_categories_data_last_key=>$eight_categories_data_last_val){
            $eight_ad_class_times_last += $eight_categories_data_last_val['fad_times'];
            $eight_ad_class_illegal_times_last += $eight_categories_data_last_val['fad_illegal_times'];
        }

        $b = round(($eight_ad_class_illegal_times/$eight_ad_class_times)*100,2);//本周期总违法率
        $b1 = round(($eight_ad_class_illegal_times_last/$eight_ad_class_times_last)*100,2);//上周期总违法率
        $eight_hb_fad_illegal_times_rate = $b - $b1;//本周期总违法率减去上周期总违法率
        $hb_fad_times = $eight_ad_class_times - $eight_ad_class_times_last;//本周期发布量减去上周期发布量
        $hb_fad_illegal_times = $eight_ad_class_illegal_times - $eight_ad_class_illegal_times_last;//本周期发布量减去上周期违法发布量
        //环比违法率
        if($eight_hb_fad_illegal_times_rate == 0){
            $eight_hb_fad_illegal_times_rate = '无变化';
        }elseif($eight_hb_fad_illegal_times_rate > 0){
            $eight_hb_fad_illegal_times_rate = $eight_hb_fad_illegal_times_rate.'↑';
        }elseif($eight_hb_fad_illegal_times_rate < 0){
            $eight_hb_fad_illegal_times_rate = ($eight_hb_fad_illegal_times_rate*-1).'↓';
        }
        //环比发布量
        if($hb_fad_times == 0){
            $hb_fad_times = '无变化';
        }elseif($hb_fad_times > 0){
            $hb_fad_times = $hb_fad_times.'↑';
        }elseif($hb_fad_times < 0){
            $hb_fad_times = ($hb_fad_times*-1).'↓';
        }
        //环比违法发布量
        if($hb_fad_illegal_times == 0){
            $hb_fad_illegal_times = '无变化';
        }elseif($hb_fad_illegal_times > 0){
            $hb_fad_illegal_times = $hb_fad_illegal_times.'↑';
        }elseif($hb_fad_illegal_times < 0){
            $hb_fad_illegal_times = ($hb_fad_illegal_times*-1).'↓';
        }
        $eight_ad_class_data[] = ['八类重点行业累计',strval($eight_ad_class_times),$hb_fad_times,strval($eight_ad_class_illegal_times),$hb_fad_illegal_times,$b.'%',$eight_hb_fad_illegal_times_rate];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "重点行业违法情况分布图",
            "data" => $eight_ad_class_illegal_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "8大重点行业情况表："
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒体类别监测列表",
            "data" => $eight_ad_class_data
        ];//
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "重点整治行业广告违法率环比图："
        ];
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "重点整治行业广告违法率环比图",
            "data" => $eight_ad_class_hb_data
        ];
        $twenty_three_categories_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$nj_media,'','','fad_illegal_times_rate','fcode');
        $twenty_three_categories_list = [];
        $illegal_categories_name = [];
        foreach ($twenty_three_categories_data as $twenty_three_categories_data_key => $twenty_three_categories_data_val){
            if($twenty_three_categories_data_val['fad_illegal_times'] > 0){
                $illegal_categories_name['name'][] = $twenty_three_categories_data_val['tadclass_name'];
                $illegal_categories_name['fad_illegal_times'] += $twenty_three_categories_data_val['fad_illegal_times'];
            }
            $twenty_three_categories_list[] = [
                strval($twenty_three_categories_data_key+1),
                $twenty_three_categories_data_val['tadclass_name'],
                $twenty_three_categories_data_val['fad_times'],
                $twenty_three_categories_data_val['fad_illegal_times'],
                $twenty_three_categories_data_val['fad_illegal_times_rate'].'%'
            ];
            $twenty_three_times_last += $province_data_val['fad_times'];
            $twenty_three_illegal_times_last += $province_data_val['fad_illegal_times'];
        }
        $illegal_categories_name['count'] = count($illegal_categories_name['name']);
        $illegal_categories_name['name'] = implode('、',$illegal_categories_name['name']);

        $twenty_three_illegal_times_rate_last = round(($twenty_three_illegal_times_last/$twenty_three_times_last)*100,2).'%';
        $twenty_three_categories_list[] = [strval(count($twenty_three_categories_list)+1),'小计',strval($twenty_three_times_last),strval($twenty_three_illegal_times_last),$twenty_three_illegal_times_rate_last];//最后统计
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "23大类广告情况列表（按违法率排名）："
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "二十三大类广告情况列表",
            "data" => $twenty_three_categories_list
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "二、广告监测警示"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三正文",
            "text" => "（一）本月广告监测违法率"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三正文",
            "text" => "本月，市省媒体广告监测违法率".$tb_fad_illegal_times_rate.$hb_fad_illegal_times_rate."其中，".$hb_illegal_times_rate_array."，本月违法广告类别主要集中在".$illegal_categories_name['name'].$illegal_categories_name['count']."个行业，累计涉嫌违法".$illegal_categories_name['fad_illegal_times']."条次"
        ];//0809
        $all_three_categories_data = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$nj_media,'','','fad_illegal_times','fcode',true);
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三正文",
            "text" => "（二）".$all_three_categories_data[0]['tadclass_name']."广告违法情况较严重"
        ];

        //查询第一违法类型的违法表现

        $tv_typical_illegal_ad_zd = $StatisticalReport->typical_illegal_ad($date_table,$nj_media,1,$report_start_date,$report_end_date,$all_three_categories_data[0]['fcode']);

        $typical_illegal = [];
        foreach ($tv_typical_illegal_ad_zd as $value){
            $typical_illegal[] = $value['fillegal'];
        }
        $tb_typical_illegal_ad_zd = $StatisticalReport->typical_illegal_ad($date_table,$nj_media,2,$report_start_date,$report_end_date,$all_three_categories_data[0]['fcode']);
        foreach ($tb_typical_illegal_ad_zd as $value){
            $typical_illegal[] = $value['fillegal'];
        }
        $tp_typical_illegal_ad_zd = $StatisticalReport->typical_illegal_ad($date_table,$nj_media,3,$report_start_date,$report_end_date,$all_three_categories_data[0]['fcode']);
        foreach ($tp_typical_illegal_ad_zd as $value){
            $typical_illegal[] = $value['fillegal'];
        }
        $typical_illegal = implode(';',array_unique($typical_illegal));
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三正文",
            "text" => "本月共监测".$all_three_categories_data[0]['tadclass_name']."广告".$all_three_categories_data[0]['fad_times']."条次，其中涉嫌违法广告".$all_three_categories_data[0]['fad_illegal_times']."条次，监测违法率为".$all_three_categories_data[0]['fad_illegal_times_rate']."%，高居各行业榜首。( ".$typical_illegal.")"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体小三加粗靠左",
            "text" => "三、典型违法广告案例"
        ];

        $typical_illegal_ad_tv = $StatisticalReport->typical_illegal_ad($date_table,$nj_media,1,$report_start_date,$report_end_date);

        $illegal_ad_tv_data = [];
        foreach ($typical_illegal_ad_tv as $illegal_ad_tv_val){
            $md5 = MD5($illegal_ad_tv_val['fad_name']);
            if(isset($illegal_ad_tv_data[$md5])){
                $illegal_ad_tv_data[$md5]['fmedianame'] .= '、'.$illegal_ad_tv_val['fmedianame'];
                $illegal_ad_tv_data[$md5]['image'] .= ';'.$illegal_ad_tv_val['image'];
                $illegal_ad_tv_data[$md5]['fillegal'] .= ';'.$illegal_ad_tv_val['fillegal'];
                $illegal_ad_tv_data[$md5]['illegal_times'] += $illegal_ad_tv_val['illegal_times'];
            }else{
                $illegal_ad_tv_data[$md5] = $illegal_ad_tv_val;
            }
        }
        $typical_illegal_ad_tv = array_values($illegal_ad_tv_data);

        if(!empty($typical_illegal_ad_tv)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "电视"
            ];

            foreach ($typical_illegal_ad_tv as $typical_illegal_ad_tv_key=>$typical_illegal_ad_tv_val){
                if($typical_illegal_ad_tv_key > 9){
                    break;
                }
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => ($typical_illegal_ad_tv_key+1)."、".$typical_illegal_ad_tv_val['fad_name']." (".$typical_illegal_ad_tv_val['fadclass'].")"
                ];

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三正文",
                    "text" => "发布媒体：".$typical_illegal_ad_tv_val['fmedianame']
                ];

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三正文",
                    "text" => "违法表现：“".$typical_illegal_ad_tv_val['fillegal']."。"
                ];

                $images = explode(';',$typical_illegal_ad_tv_val['image']);

                foreach ($images as $images_val){
                    $str_image = str_replace("/h/100","/h/400",$images_val);
                    if(trim($str_image) != ""){
                        $data['content'][] = [
                            "type"=>"filltable",
                            "bookmark"=>"单个图片",
                            "data"=>[
                                ["index"=>1,"type"=>"image","content"=>$str_image]
                            ]
                        ];
                    }
                }
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三正文",
                    "text" => "违法次数：该广告累计违法".$typical_illegal_ad_tv_val['illegal_times']."条次。"
                ];
            }
        }

        $typical_illegal_ad_tb = $StatisticalReport->typical_illegal_ad($date_table,$nj_media,2,$report_start_date,$report_end_date);

        $illegal_ad_tb_data = [];
        foreach ($typical_illegal_ad_tb as $illegal_ad_tb_val){
            $md5 = MD5($illegal_ad_tb_val['fad_name']);
            if(isset($illegal_ad_tb_data[$md5])){
                $illegal_ad_tb_data[$md5]['fmedianame'] .= '、'.$illegal_ad_tb_val['fmedianame'];
                $illegal_ad_tb_data[$md5]['fillegal'] .= ';'.$illegal_ad_tb_val['fillegal'];
                $illegal_ad_tb_data[$md5]['illegal_times'] += $illegal_ad_tb_val['illegal_times'];
            }else{
                $illegal_ad_tb_data[$md5] = $illegal_ad_tb_val;
            }
        }
        $typical_illegal_ad_tb = array_values($illegal_ad_tb_data);
        if(!empty($typical_illegal_ad_tb)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "广播"
            ];
            foreach ($typical_illegal_ad_tb as $typical_illegal_ad_tb_key=>$typical_illegal_ad_tb_val){
                if($typical_illegal_ad_tb_key > 9){
                    break;
                }
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => ($typical_illegal_ad_tb_key+1)."、".$typical_illegal_ad_tb_val['fad_name']." (".$typical_illegal_ad_tb_val['fadclass'].")"
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三正文",
                    "text" => "发布媒体：".$typical_illegal_ad_tb_val['fmedianame']
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三正文",
                    "text" => "违法表现：“".$typical_illegal_ad_tb_val['fillegal']."。"
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三正文",
                    "text" => "违法次数：该广告累计违法".$typical_illegal_ad_tb_val['illegal_times']."条次。"
                ];
            }
        }

        $typical_illegal_ad_tp = $StatisticalReport->typical_illegal_ad($date_table,$nj_media,3,$report_start_date,$report_end_date);
        $illegal_ad_tp_data = [];
        foreach ($typical_illegal_ad_tp as $illegal_ad_tp_val){
            $md5 = MD5($illegal_ad_tp_val['fad_name']);
            if(isset($illegal_ad_tp_data[$md5])){
                $illegal_ad_tp_data[$md5]['fmedianame'] .= '、'.$illegal_ad_tp_val['fmedianame'];
                $illegal_ad_tp_data[$md5]['image'] .= ';'.$illegal_ad_tp_val['image'];
                $illegal_ad_tp_data[$md5]['fillegal'] .= ';'.$illegal_ad_tp_val['fillegal'];
                $illegal_ad_tp_data[$md5]['illegal_times'] += $illegal_ad_tp_val['illegal_times'];
            }else{
                $illegal_ad_tp_data[$md5] = $illegal_ad_tp_val;
            }
        }
        $typical_illegal_ad_tp = array_values($illegal_ad_tp_data);
        if(!empty($typical_illegal_ad_tp)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "报纸"
            ];
            foreach ($typical_illegal_ad_tp as $typical_illegal_ad_tp_key=>$typical_illegal_ad_tp_val){
                if($typical_illegal_ad_tp_key > 9){
                    break;
                }
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => ($typical_illegal_ad_tp_key+1)."、".$typical_illegal_ad_tp_val['fad_name']." (".$typical_illegal_ad_tp_val['fadclass'].")"
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三正文",
                    "text" => "发布媒体：".$typical_illegal_ad_tp_val['fmedianame']
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三正文",
                    "text" => "违法表现：“".$typical_illegal_ad_tp_val['fillegal']."。"
                ];
                $images = explode(';',$typical_illegal_ad_tp_val['image']);

                foreach ($images as $images_val){

                    if(trim($images_val) != ""){
                        $data['content'][] = [
                            "type"=>"filltable",
                            "bookmark"=>"单个图片",
                            "data"=>[
                                ["index"=>1,"type"=>"image","content"=>$images_val]
                            ]
                        ];
                    }
                }

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三正文",
                    "text" => "违法次数：该广告累计违法".$typical_illegal_ad_tp_val['illegal_times']."条次。"
                ];
            }
        }
        $top_ten_data = array_merge($typical_illegal_ad_tv,$typical_illegal_ad_tb,$typical_illegal_ad_tp);
        $top_ten_data = $this->pxsf($top_ten_data,'illegal_times');

        $top_ten = [];
        foreach ($top_ten_data as $top_ten_data_key=>$top_ten_data_val){
            if($top_ten_data_key != 0){

                if($top_ten_data_val['illegal_times'] == $top_ten[$index]['illegal_times']){
                    $top_ten[$index]['fad_name'] .= '、'.$top_ten_data_val['fad_name'].'('.$top_ten_data_val['fmedianame'].')';
                }else{
                    $index++;
                    $top_ten[$index]['key'] = strval(count($top_ten)+1);
                    $top_ten[$index]['fad_name'] = $top_ten_data_val['fad_name'].'('.$top_ten_data_val['fmedianame'].')';
                    $top_ten[$index]['illegal_times'] = strval($top_ten_data_val['illegal_times']);
                }

            }else{
                $index = 0;
                $top_ten[$index]['key'] = '1';
                $top_ten[$index]['fad_name'] = $top_ten_data_val['fad_name'].'('.$top_ten_data_val['fmedianame'].')';
                $top_ten[$index]['illegal_times'] = strval($top_ten_data_val['illegal_times']);
            }
        }

        $top_ten = array_values($top_ten);
        $top_ten_data = [];
        foreach ($top_ten as $top_ten_key=>$top_ten_val){
            if($top_ten_key < 10){
                $top_ten_data[] = [$top_ten_val['key'],$top_ten_val['fad_name'],$top_ten_val['illegal_times']];
            }
        }

        if(!empty($top_ten_data)){

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "四、违法广告前十名"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告前十名",
                "data" => $top_ten_data
            ];

        }

        $typical_illegal_p_tv = $StatisticalReport->typical_illegal_ad_list($date_table,$usermedia[1],1,$report_start_date,$report_end_date);//省级电视违法广告明细
        $typical_illegal_p_bc = $StatisticalReport->typical_illegal_ad_list($date_table,$usermedia[1],2,$report_start_date,$report_end_date);//省级广播违法广告明细
        $typical_illegal_p_paper = $StatisticalReport->typical_illegal_ad_list($date_table,$usermedia[1],3,$report_start_date,$report_end_date);//省级报纸违法广告明细

        $typical_illegal_c_tv = $StatisticalReport->typical_illegal_ad_list($date_table,$usermedia[0],1,$report_start_date,$report_end_date);//市级电视违法广告明细
        $typical_illegal_c_bc = $StatisticalReport->typical_illegal_ad_list($date_table,$usermedia[0],2,$report_start_date,$report_end_date);//市级广播违法广告明细
        $typical_illegal_c_paper = $StatisticalReport->typical_illegal_ad_list($date_table,$usermedia[0],3,$report_start_date,$report_end_date);//市级报纸违法广告明细

        $typical_illegal_a_tv = $StatisticalReport->typical_illegal_ad_list($date_table,$usermedia[2],1,$report_start_date,$report_end_date);//区级电视违法广告明细
        $typical_illegal_a_bc = $StatisticalReport->typical_illegal_ad_list($date_table,$usermedia[2],2,$report_start_date,$report_end_date);//区级广播违法广告明细
        $typical_illegal_a_paper = $StatisticalReport->typical_illegal_ad_list($date_table,$usermedia[2],3,$report_start_date,$report_end_date);//区级报纸违法广告明细

        $all_count = count($typical_illegal_p_tv) +
            count($typical_illegal_p_bc) +
            count($typical_illegal_p_paper) +
            count($typical_illegal_c_tv) +
            count($typical_illegal_c_bc) +
            count($typical_illegal_c_paper) +
            count($typical_illegal_a_tv) +
            count($typical_illegal_a_bc) +
            count($typical_illegal_a_paper);

        $p_count = count($typical_illegal_p_tv) +
            count($typical_illegal_p_bc) +
            count($typical_illegal_p_paper);

        $c_count = count($typical_illegal_c_tv) +
            count($typical_illegal_c_bc) +
            count($typical_illegal_c_paper);

        $a_count = count($typical_illegal_a_tv) +
            count($typical_illegal_a_bc) +
            count($typical_illegal_a_paper);

        if($all_count > 0){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体小三加粗靠左",
                "text" => "五、违法广告明细"
            ];

            if($p_count > 0){
                //省
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => "省级媒体发布的违法广告明细"
                ];

                if(!empty($typical_illegal_p_tv)){

                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "宋体小三加粗靠左",
                        "text" => "省级电视媒体发布的违法广告明细列表"
                    ];
                    $data['content'][] = [
                        "type" => "table",
                        "bookmark" => "违法广告明细",
                        "data" => $this->get_table_data($typical_illegal_p_tv)
                    ];
                }

                if(!empty($typical_illegal_p_bc)) {

                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "宋体小三加粗靠左",
                        "text" => "省级广播媒体发布的违法广告明细"
                    ];
                    $data['content'][] = [
                        "type" => "table",
                        "bookmark" => "违法广告明细",
                        "data" => $this->get_table_data($typical_illegal_p_bc)
                    ];
                }

                if(!empty($typical_illegal_p_paper)) {

                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "宋体小三加粗靠左",
                        "text" => "省级报纸媒体发布的违法广告明细"
                    ];
                    $data['content'][] = [
                        "type" => "table",
                        "bookmark" => "违法广告明细",
                        "data" => $this->get_table_data($typical_illegal_p_paper)
                    ];
                }

            }


            //市
            if($p_count > 0){
                //省
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => "市级媒体发布的违法广告明细"
                ];

                if(!empty($typical_illegal_c_tv)){
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "宋体小三加粗靠左",
                        "text" => "市级电视媒体发布的违法广告明细列表"
                    ];
                    $data['content'][] = [
                        "type" => "table",
                        "bookmark" => "违法广告明细",
                        "data" => $this->get_table_data($typical_illegal_c_tv)
                    ];
                }

                if(!empty($typical_illegal_c_bc)) {

                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "宋体小三加粗靠左",
                        "text" => "市级广播媒体发布的违法广告明细"
                    ];
                    $data['content'][] = [
                        "type" => "table",
                        "bookmark" => "违法广告明细",
                        "data" => $this->get_table_data($typical_illegal_c_bc)
                    ];
                }

                if(!empty($typical_illegal_c_paper)) {

                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "宋体小三加粗靠左",
                        "text" => "市级报纸媒体发布的违法广告明细"
                    ];
                    $data['content'][] = [
                        "type" => "table",
                        "bookmark" => "违法广告明细",
                        "data" => $this->get_table_data($typical_illegal_c_paper)
                    ];
                }

            }


            //区
            if($p_count > 0){
                //省
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体小三加粗靠左",
                    "text" => "区县级媒体发布的违法广告明细"
                ];

                if(!empty($typical_illegal_a_tv)){
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "宋体小三加粗靠左",
                        "text" => "区县级电视媒体发布的违法广告明细列表"
                    ];
                    $data['content'][] = [
                        "type" => "table",
                        "bookmark" => "违法广告明细",
                        "data" => $this->get_table_data($typical_illegal_a_tv)
                    ];
                }

                if(!empty($typical_illegal_a_bc)) {

                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "宋体小三加粗靠左",
                        "text" => "区县级广播媒体发布的违法广告明细"
                    ];
                    $data['content'][] = [
                        "type" => "table",
                        "bookmark" => "违法广告明细",
                        "data" => $this->get_table_data($typical_illegal_a_bc)
                    ];
                }

                if(!empty($typical_illegal_a_paper)) {

                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "宋体小三加粗靠左",
                        "text" => "区县级报纸媒体发布的违法广告明细"
                    ];
                    $data['content'][] = [
                        "type" => "table",
                        "bookmark" => "违法广告明细",
                        "data" => $this->get_table_data($typical_illegal_a_paper)
                    ];
                }

            }
        }

        $report_data = json_encode($data);

        //echo $report_data;exit;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！错误信息：'.$response));
            }

            $system_num = getconfig('system_num');

            //将生成记录保存
            $focus = I('focus',0);
            $pnname = $regulatorpersonInfo['regulatorname'].$phase_ym.'月报';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 30;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $regulatorpersonInfo['fregulatorpid'];
            $data['pncreatepersonid']   = $regulatorpersonInfo['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }

    public function create_monthpresentation2_20180807($date,$focus,$userid) {
        header("Content-type: text/html; charset=utf-8");
        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);
        $regulatorpersonInfo = M('tregulatorperson')
            ->field('fregulatorid as bumen_id,fid')
            ->where('fid='.$userid)->find();
        if(!empty($regulatorpersonInfo)){
            $regulatorInfo = M('tregulator')->where(array('fid' => $regulatorpersonInfo['bumen_id']))->find();//监管机构信息
            $regulatorpersonInfo['fregulatorid']        = $regulatorInfo['fid'];//部门ID
            $regulatorpersonInfo['fregulatorpid']       = D('Function')->get_tregulatoraction($regulatorInfo['fid']);//获取当前单位所属机构ID
            $do_tr = M('tregulator')->field('tregulator.fname,tregion.fname as regionname,tregion.fname1 as regionname1,tregulator.fregionid')->join('tregion on tregulator.fregionid=tregion.fid')->where(array('tregulator.fid' => $regulatorpersonInfo['fregulatorpid']))->find();//获取机构信息
            $regulatorpersonInfo['regulatorpname']      = $do_tr['fname'];//所属机构名称
            $regulatorpersonInfo['regionid']            = $do_tr['fregionid'];//机构行政区划ID
            $regulatorpersonInfo['regionname']          = $do_tr['regionname'];//机构行政区划名称
            $regulatorpersonInfo['regionname1']         = $do_tr['regionname1'];//机构行政区划全名称
            $regulatorpersonInfo['regulator_regionid']  = $regulatorInfo['fregionid'];//部门行政区划ID
            $regulatorpersonInfo['regulatorname']       = $regulatorInfo['fname'];//部门名称
            //用户媒介权限
            $mediajurisdiction = D('Function')->get_mediajurisdiction($regulatorInfo['fid'],[],[],$regulatorpersonInfo['fid']);//获取媒体权限组
            $nj_media   = $mediajurisdiction?$mediajurisdiction:array('0');

            $do_tt = M('tbn_media_grant')->field('fmedia_id')->where('freg_id='.$regulatorpersonInfo['fregulatorpid'])->select();
            foreach ($do_tt as $key => $value) {
                array_push($nj_media, $value['fmedia_id']);
            }
        }

        $regulatorpname = $regulatorpersonInfo['regulatorpname'];//机构名
        $regionid = $regulatorpersonInfo['regionid'];//机构行政区划ID
        $regionid = substr($regionid , 0 , 2);
        $arr = explode("-" ,$date);
        $date1 = date('Y年m月',strtotime($date));
        $html = '<div style="margin:0 auto;"> <style>.tb tr td{ background:#ffffff; text-align:center; border-bottom:1px solid #333333;border-right:1px solid #333333;}</style>';
        $html.= '<div align="center"><p style="color:red;font-size:30px;">'.$regulatorpname.'</p></div>';
        $html.= '<div align="center"><p style="color:red;font-size:18px;">广告监测报告</p></div>';
        $html.= '<p align="center" style="color:red;font-size:12px;">（'.	$arr[0].'年第'.intval($arr[1]).'期）</p>';
        $html.= '<p align="right">'.$date1.'</p>';
        $html.= '<p>一、广告监测情况综述</p>';

        $system_num = getconfig('system_num');
        $usermedia = M('tmedia_temp')->where('ftype=1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid'))->getField('fmediaid',true);
        $where['t.fid'] = array('in',$usermedia);

        //所有媒体
        $res = M('tmedia')
            ->alias('t')
            ->field('t.fid,t.fmediaownerid,t.fmediaclassid, (case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,tw.fregionid')
            ->join('tmediaowner as tw on t.fmediaownerid = tw.fid')
            ->where($where)
            ->select();
        $count = count($res);
        $attach1 = [];
        $attach2 = [];
        $attach4 = [];
        $attach6 = [];
        foreach($res as $key => $val){
            $tregion_len = get_tregionlevel($val['fregionid']);

            if($tregion_len == 1){//国家级
                array_push($attach1, $val);
                $str1 .= $val['fmedianame'].'、';
            }elseif($tregion_len == 2){//省级
                array_push($attach2, $val);
                $str2 .= $val['fmedianame'].'、';
            }elseif($tregion_len == 4){//市级
                array_push($attach4, $val);
                $str4 .= $val['fmedianame'].'、';
            }elseif($tregion_len == 6){//县级
                array_push($attach6, $val);
                $str6 .= $val['fmedianame'].'、';
            }
        }


        $html.= '<p>
					广告监测中心'.intval($arr[1]).'月共监测'.$count.'家媒体，其中市属媒体'.count($attach4).'家('.$str4.')。省属媒体'.count($attach2).'家('.$str2.')。
		         </p>';
        $regionids = [];
        foreach($res as $val){
            array_push($regionids, $val['fid']);//用户具有权限的媒介id
        }

        $now_date = $arr[0].$arr[1].'_'.$regionid;//当月
        $date_last = date("Y-m", strtotime("-1 months", strtotime($date)));//上个月份
        $last_date = date("Ym", strtotime("-1 months", strtotime($date))).'_'.$regionid;//上月

        if($this->is_have_table('ttvissue_'.$now_date)){
            $this->ajaxReturn([
                'code'=>-1,
                'msg'=>'当月无数据'
            ]);
        }
        $sql = 'select tv.fmediaid,tv.fsampleid from ttvissue_'.$now_date.' as tv where tv.fmediaid in ('.join(',',$regionids).')
				union all (select tb.fmediaid,tb.fsampleid from tbcissue_'.$now_date.' as tb where tb.fmediaid in ('.join(',',$regionids).'))
				union all (select tp.fmediaid,tp.fpapersampleid as fsampleid from tpaperissue as tp where tp.fmediaid in ('.join(',',$regionids).'))
				';
        $sql1 = 'select tv.fmediaid,tv.fsampleid from ttvissue_'.$last_date.' as tv where tv.fmediaid in ('.join(',',$regionids).')
				union all (select tb.fmediaid,tb.fsampleid from tbcissue_'.$last_date.' as tb where tb.fmediaid in ('.join(',',$regionids).'))
				union all (select tp.fmediaid,tp.fpapersampleid as fsampleid from tpaperissue as tp where tp.fmediaid in ('.join(',',$regionids).'))
				';
        $n_res = M()->query($sql);//当月总发布量
        $l_res = M()->query($sql1);//上月总发布量
        //dump($l_res);exit;

        $zdwf = 0;
        $yzwf = 0;
        $ybwf = 0;
        $arr_fmediaid = [];
        $arr_fsampleid = [];
        $larr_fmediaid = [];
        $larr_fsampleid = [];

        foreach($n_res as $v){
            $arr_fmediaid[] = $v['fmediaid'];
            $arr_fsampleid[] = $v['fsampleid'];
        }

        foreach($l_res as $v1){
            $larr_fmediaid[] = $v1['fmediaid'];
            $larr_fsampleid[] = $v1['fsampleid'];
        }

        //上月违法总量
        if(empty($larr_fmediaid) || empty($larr_fsampleid)){
            $l_zdwf = 0;
        }else{


            $last_count_sql = 'SELECT sum(all_illegal_count) as all_illegal_count FROM (SELECT
                                    count(*) as all_illegal_count
                                FROM
                                    ttvissue_'.$last_date.' AS tv
                                    JOIN ttvsample tvs on tvs.fid = tv.fsampleid
                                WHERE
                                    tv.fmediaid IN ('.join(',',$regionids).')
                                AND
                                    tvs.fillegaltypecode > 10
                                UNION ALL
                                    (
                                        SELECT
                                            count(*) as all_illegal_count
                                        FROM
                                            tbcissue_'.$last_date.' AS tb
                                        JOIN tbcsample tbs on tbs.fid = tb.fsampleid
                                        WHERE
                                            tb.fmediaid IN ('.join(',',$regionids).')
                                AND
                                    tbs.fillegaltypecode > 10
                                    )
                                UNION ALL
                                    (
                                        SELECT
                                            count(*) as all_illegal_count
                                        FROM
                                            tpaperissue AS tp
                                        JOIN tpapersample tps on tps.fpapersampleid = tp.fpapersampleid
                                        WHERE
                                            tp.fmediaid IN ('.join(',',$regionids).')
                                        AND
                                            tps.fillegaltypecode > 10
                                        AND date_format(tp.`fissuedate`,"%Y-%m")='.'"'.$date_last.'"'.'
                                    )) as b';

            $l_zdwf = M()->query($last_count_sql);//上月违法总量
            $l_zdwf = $l_zdwf[0]['all_illegal_count'];

        }
        $l_illegality = round(($l_zdwf/count($l_res))*100,2);//上月违法率


        $now_count_sql_wf = 'SELECT sum(all_illegal_count) as all_illegal_count FROM (SELECT
                                    count(*) as all_illegal_count
                                FROM
                                    ttvissue_'.$now_date.' AS tv
                                    JOIN ttvsample tvs on tvs.fid = tv.fsampleid
                                WHERE
                                    tv.fmediaid IN ('.join(',',$regionids).')
                                AND
                                    tvs.fillegaltypecode > 10
                                UNION ALL
                                    (
                                        SELECT
                                            count(*) as all_illegal_count
                                        FROM
                                            tbcissue_'.$now_date.' AS tb
                                        JOIN tbcsample tbs on tbs.fid = tb.fsampleid
                                        WHERE
                                            tb.fmediaid IN ('.join(',',$regionids).')
                                AND
                                    tbs.fillegaltypecode > 10
                                    )
                                UNION ALL
                                    (
                                        SELECT
                                            count(*) as all_illegal_count
                                        FROM
                                            tpaperissue AS tp
                                        JOIN tpapersample tps on tps.fpapersampleid = tp.fpapersampleid
                                        WHERE
                                            tp.fmediaid IN ('.join(',',$regionids).')
                                        AND
                                            tps.fillegaltypecode > 10
                                        AND date_format(tp.`fissuedate`,"%Y-%m")='.'"'.$date_last.'"'.'
                                    )) as b';



        $now_count_sql_yz = 'SELECT sum(all_illegal_count) as all_illegal_count FROM (SELECT
                                    count(*) as all_illegal_count
                                FROM
                                    ttvissue_'.$now_date.' AS tv
                                    JOIN ttvsample tvs on tvs.fid = tv.fsampleid
                                WHERE
                                    tv.fmediaid IN ('.join(',',$regionids).')
                                AND
                                    tvs.fillegaltypecode = 30
                                UNION ALL
                                    (
                                        SELECT
                                            count(*) as all_illegal_count
                                        FROM
                                            tbcissue_'.$now_date.' AS tb
                                        JOIN tbcsample tbs on tbs.fid = tb.fsampleid
                                        WHERE
                                            tb.fmediaid IN ('.join(',',$regionids).')
                                AND
                                    tbs.fillegaltypecode = 30
                                    )
                                UNION ALL
                                    (
                                        SELECT
                                            count(*) as all_illegal_count
                                        FROM
                                            tpaperissue AS tp
                                        JOIN tpapersample tps on tps.fpapersampleid = tp.fpapersampleid
                                        WHERE
                                            tp.fmediaid IN ('.join(',',$regionids).')
                                        AND
                                            tps.fillegaltypecode = 30
                                        AND date_format(tp.`fissuedate`,"%Y-%m")='.'"'.$date_last.'"'.'
                                    )) as b';

        $now_count_sql_yb = 'SELECT sum(all_illegal_count) as all_illegal_count FROM (SELECT
                                    count(*) as all_illegal_count
                                FROM
                                    ttvissue_'.$now_date.' AS tv
                                    JOIN ttvsample tvs on tvs.fid = tv.fsampleid
                                WHERE
                                    tv.fmediaid IN ('.join(',',$regionids).')
                                AND
                                    tvs.fillegaltypecode = 20
                                UNION ALL
                                    (
                                        SELECT
                                            count(*) as all_illegal_count
                                        FROM
                                            tbcissue_'.$now_date.' AS tb
                                        JOIN tbcsample tbs on tbs.fid = tb.fsampleid
                                        WHERE
                                            tb.fmediaid IN ('.join(',',$regionids).')
                                AND
                                    tbs.fillegaltypecode = 20
                                    )
                                UNION ALL
                                    (
                                        SELECT
                                            count(*) as all_illegal_count
                                        FROM
                                            tpaperissue AS tp
                                        JOIN tpapersample tps on tps.fpapersampleid = tp.fpapersampleid
                                        WHERE
                                            tp.fmediaid IN ('.join(',',$regionids).')
                                        AND
                                            tps.fillegaltypecode = 20
                                        AND date_format(tp.`fissuedate`,"%Y-%m")='.'"'.$date_last.'"'.'
                                    )) as b';

        $zdwf = M()->query($now_count_sql_wf);//当月违法总量
        $zdwf = $zdwf[0]['all_illegal_count'];

        $yzwf = M()->query($now_count_sql_yz);//当月严重违法总量
        $yzwf = $yzwf[0]['all_illegal_count'];

        $ybwf = M()->query($now_count_sql_yb);//当月一般违法总量
        $ybwf = $ybwf[0]['all_illegal_count'];
        //当月违法率
        $illegality = round(($zdwf/count($n_res))*100,2);

        //环比增加
        $hb_ = count($n_res)-count($l_res);
        if($hb_==0){
            $hb_str='无变化';
        }elseif($hb_>0){
            $hb_str='增加'.$hb_.'条次';
        }else{
            $hb_str='下降'.abs($hb_).'条次';
        }

        //违法率环比
        $hb_illegality = $illegality - $l_illegality;
        if($hb_illegality==0){
            $hb_illegality_str='无变化';
        }elseif($hb_illegality>0){
            $hb_illegality_str='增加'.$hb_illegality.'个百分点';
        }else{
            $hb_illegality_str='下降'.abs($hb_illegality).'个百分点';
        }

        //违法率同比
        $tb_illegality = ($illegality - $l_illegality)/$illegality;
        if($tb_illegality==0){
            $tb_illegality_str='无变化';
        }elseif($tb_illegality>0){
            $tb_illegality_str='增加'.$tb_illegality.'个百分点';
        }else{
            $tb_illegality_str='下降'.abs($tb_illegality).'个百分点';
        }


        $html.= '<p>本月，监测中心监测媒体发布的各类广告'.count($n_res).'条次，环比'.$hb_str.'，其中涉嫌违法广告'.$zdwf.'条次（严重违法广告'.$yzwf.'条次，一般违法广告'.$ybwf.'条次），监测违法率为'.$illegality.'%，环比'.$hb_illegality_str.'。</p>';
        $html.= '<h4>(一)分类监测情况</h4>';

        $html .= ' 
    		<div align="center" border="1" style="font-size:18px; font-weight:bold; padding:10px;">市属媒体：</div> 
			<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
			<tr> 
			 <td>媒体类别</td> 
			 <td>监测条次</td> 
			 <td>环比</td> 
			 <td>涉嫌违法广告条次</td> 
			 <td>环比</td> 
			 <td>违法率</td> 
			 <td>环比</td> 
			 <td>严重违法</td>
			 <td>环比</td>
			 <td>严重违法率</td>
			 <td>环比</td>
			</tr>';
        $arr2 = ['↑','↓'];
        $regionids4 = [];
        foreach($attach4 as $v){
            array_push($regionids4, $v['fid']);//用户具有权限的市媒体id
        }
        $tv_regionid = [];
        $bc_regionid = [];
        $paper_regionid = [];


        foreach($attach4 as $val){
            //市属电视媒体id
            if(substr($val['fmediaclassid'], 0,2)=='01'){
                array_push($tv_regionid, $val['fid']);
            }
            //市属广播媒体id
            if(substr($val['fmediaclassid'], 0,2)=='02'){
                array_push($bc_regionid, $val['fid']);
            }
            //市属报刊媒体id
            if(substr($val['fmediaclassid'], 0,2)=='03'){
                array_push($paper_regionid, $val['fid']);
            }
        }


        $wfggsl = 0;
        $ggslwfl = 0;
        $yzwfggsl = 0;
        $ggslyzwfl = 0;

        //当月报刊监测情况
        if($paper_regionid){
            $sql_p_count = 'select count(*) total from tpaperissue bc where bc.fmediaid in ('.join(',',$paper_regionid).') and date_format(bc.`fissuedate`,"%Y-%m")='.'"'.$date.'"';
            $npa_total = M()->query($sql_p_count);//当月广播监测数
            $sql_p = 'select ifnull(sum(case when tps.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when tps.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when tps.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when tps.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from tpaperissue tp join tpapersample tps on tp.fpapersampleid=tps.fpapersampleid where tp.fmediaid in ('.join(',',$paper_regionid).') and date_format(tp.`fissuedate`,"%Y-%m")='.'"'.$date.'"';

            $npa_list = M()->query($sql_p);
            $npa_list[0]['total'] = $npa_total[0]['total'];

            //上月报刊监测情况
            $sql_p_l_count = 'select count(*) total from tpaperissue bc where bc.fmediaid in ('.join(',',$paper_regionid).') and date_format(bc.`fissuedate`,"%Y-%m")='.'"'.$date_last.'"';
            $lpa_total = M()->query($sql_p_l_count);//上月广播监测数
            $sql_p_l = 'select ifnull(sum(case when tps.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when tps.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when tps.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when tps.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from tpaperissue tp join tpapersample tps on tp.fpapersampleid=tps.fpapersampleid where tp.fmediaid in ('.join(',',$paper_regionid).') and date_format(tp.`fissuedate`,"%Y-%m")='.'"'.$date_last.'"';

            $lpa_list = M()->query($sql_p_l);
            $lpa_list[0]['total'] = $npa_total[0]['total'];
            $hb_total = $npa_list[0]['total'] - $lpa_list[0]['total'];
            if($hb_total==0){
                $hb_total = '无变化';
            }elseif ($hb_total>0) {
                $hb_total = $hb_total.$arr2[0];
            }else{
                $hb_total = $hb_total.$arr2[1];
            }
            $hb_wfggsl = $npa_list[0]['wfggsl'] - $lpa_list[0]['wfggsl'];
            if($hb_wfggsl==0){
                $hb_wfggsl = '无变化';
            }elseif ($hb_wfggsl>0) {
                $hb_wfggsl = $hb_wfggsl.$arr2[0];
            }else{
                $hb_wfggsl = $hb_wfggsl.$arr2[1];
            }
            $hb_ggslwfl = $npa_list[0]['ggslwfl']-$lpa_list[0]['ggslwfl'];
            if($hb_ggslwfl==0){
                $hb_ggslwfl = '无变化';
            }elseif ($hb_ggslwfl>0) {
                $hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[0];
            }else{
                $hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[1];
            }

            $hb_yzwfggsl = $npa_list[0]['yzwfggsl'] - $lpa_list[0]['yzwfggsl'];
            if($hb_yzwfggsl==0){
                $hb_yzwfggsl = '无变化';
            }elseif ($hb_yzwfggsl>0) {
                $hb_yzwfggsl = $hb_yzwfggsl.$arr2[0];
            }else{
                $hb_yzwfggsl = $hb_yzwfggsl.$arr2[1];
            }

            $hb_ggslyzwfl = $npa_list[0]['ggslyzwfl'] - $lpa_list[0]['ggslyzwfl'];
            if($hb_ggslyzwfl==0){
                $hb_ggslyzwfl = '无变化';
            }elseif ($hb_ggslyzwfl>0) {
                $hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[0];
            }else{
                $hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[1];
            }
            $html .= '<tr>';
            $html .= '<td>报刊</td>';
            $html .= '<td>'.$npa_list[0]['total'].'</td>';
            $html .= '<td>'.$hb_total.'</td>';
            $html .= '<td>'.$npa_list[0]['wfggsl'].'</td>';
            $html .= '<td>'.$hb_wfggsl.'</td>';
            $html .= '<td>'.round($npa_list[0]['ggslwfl']*100,2).'%</td>';
            $html .= '<td>'.$hb_ggslwfl.'</td>';
            $html .= '<td>'.$npa_list[0]['yzwfggsl'].'</td>';
            $html .= '<td>'.$hb_yzwfggsl.'</td>';
            $html .= '<td>'.round($npa_list[0]['ggslyzwfl']*100,2).'%</td>';
            $html .= '<td>'.$hb_ggslyzwfl.'</td></tr>';
        }


        //当月电视监测情况0613
        if($tv_regionid){
            $sql_tv_count = 'select count(*) total from ttvissue_'.$now_date.' tv where tv.fmediaid in ('.join(',',$tv_regionid).')';
            $ntv_total = M()->query($sql_tv_count);//当月广播监测数
            $sql_tv = 'select ifnull(sum(case when tvs.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when tvs.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when tvs.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when tvs.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from ttvissue_'.$now_date.' tv join ttvsample tvs on tv.fsampleid=tvs.fid where tv.fmediaid in ('.join(',',$tv_regionid).')';

            $ntv_list = M()->query($sql_tv);
            $ntv_list[0]['total'] = $ntv_total[0]['total'];
            //dump($sql_tv);exit;

            //上月电视监测情况
            $sql_tv_l_count = 'select count(*) total from ttvissue_'.$last_date.' bc where bc.fmediaid in ('.join(',',$tv_regionid).')';
            $ltv_total = M()->query($sql_tv_l_count);//上月电视监测数
            $sql_tv_l = 'select ifnull(sum(case when tvs.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when tvs.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when tvs.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when tvs.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from ttvissue_'.$last_date.' tv join ttvsample tvs on tv.fsampleid=tvs.fid where tv.fmediaid in ('.join(',',$tv_regionid).')';
            $ltv_list = M()->query($sql_tv_l);

            $ltv_list[0]['total'] = $ltv_total[0]['total'];
            $hb_total = $ntv_list[0]['total'] - $ltv_list[0]['total'];
            if($hb_total==0){
                $hb_total = '无变化';
            }elseif ($hb_total>0) {
                $hb_total = $hb_total.$arr2[0];
            }else{
                $hb_total = $hb_total.$arr2[1];
            }
            $hb_wfggsl = $ntv_list[0]['wfggsl'] - $ltv_list[0]['wfggsl'];
            if($hb_wfggsl==0){
                $hb_wfggsl = '无变化';
            }elseif ($hb_wfggsl>0) {
                $hb_wfggsl = $hb_wfggsl.$arr2[0];
            }else{
                $hb_wfggsl = $hb_wfggsl.$arr2[1];
            }
            $hb_ggslwfl = $ntv_list[0]['ggslwfl']-$ltv_list[0]['ggslwfl'];
            if($hb_ggslwfl==0){
                $hb_ggslwfl = '无变化';
            }elseif ($hb_ggslwfl>0) {
                $hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[0];
            }else{
                $hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[1];
            }

            $hb_yzwfggsl = $ntv_list[0]['yzwfggsl'] - $ltv_list[0]['yzwfggsl'];
            if($hb_yzwfggsl==0){
                $hb_yzwfggsl = '无变化';
            }elseif ($hb_yzwfggsl>0) {
                $hb_yzwfggsl = $hb_yzwfggsl.$arr2[0];
            }else{
                $hb_yzwfggsl = $hb_yzwfggsl.$arr2[1];
            }

            $hb_ggslyzwfl = $ntv_list[0]['ggslyzwfl'] - $ltv_list[0]['ggslyzwfl'];
            if($hb_ggslyzwfl==0){
                $hb_ggslyzwfl = '无变化';
            }elseif ($hb_ggslyzwfl>0) {
                $hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[0];
            }else{
                $hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[1];
            }

            $html .= '<tr>';
            $html .= '<td>电视</td>';
            $html .= '<td>'.$ntv_list[0]['total'].'</td>';
            $html .= '<td>'.$hb_total.'</td>';
            $html .= '<td>'.$ntv_list[0]['wfggsl'].'</td>';
            $html .= '<td>'.$hb_wfggsl.'</td>';
            $html .= '<td>'.round($ntv_list[0]['ggslwfl']*100,2).'%</td>';
            $html .= '<td>'.$hb_ggslwfl.'</td>';
            $html .= '<td>'.$ntv_list[0]['yzwfggsl'].'</td>';
            $html .= '<td>'.$hb_yzwfggsl.'</td>';
            $html .= '<td>'.round($ntv_list[0]['ggslyzwfl']*100,2).'%</td>';
            $html .= '<td>'.$hb_ggslyzwfl.'</td></tr>';
        }

        //当月广播监测情况
        if($bc_regionid){
            $sql_bc_count = 'select count(*) total from tbcissue_'.$now_date.' bc where bc.fmediaid in ('.join(',',$bc_regionid).')';
            $nbc_total = M()->query($sql_bc_count);//当月广播监测数
            $sql_bc = 'select ifnull(sum(case when tbs.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when tbs.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when tbs.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when tbs.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from tbcissue_'.$now_date.' tb join tbcsample tbs on tb.fsampleid=tbs.fid where tb.fmediaid in ('.join(',',$bc_regionid).')';
            $nbc_list = M()->query($sql_bc);
            $nbc_list[0]['total'] = $nbc_total[0]['total'];

            //上月广播监测情况
            $sql_count_l = 'select count(*) total from tbcissue_'.$last_date.' bc where bc.fmediaid in ('.join(',',$bc_regionid).')';
            $lbc_total = M()->query($sql_count_l);//上月广播监测数
            $sql_bc_l = 'select ifnull(sum(case when tbs.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when tbs.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when tbs.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when tbs.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from tbcissue_'.$last_date.' tb join tbcsample tbs on tb.fsampleid=tbs.fid where tb.fmediaid in ('.join(',',$bc_regionid).')';
            $lbc_list = M()->query($sql_bc_l);
            $lbc_list[0]['total'] = $lbc_total[0]['total'];

            $hb_total = $nbc_list[0]['total'] - $lbc_list[0]['total'];
            if($hb_total==0){
                $hb_total = '无变化';
            }elseif ($hb_total>0) {
                $hb_total = $hb_total.$arr2[0];
            }else{
                $hb_total = $hb_total.$arr2[1];
            }
            $hb_wfggsl = $nbc_list[0]['wfggsl'] - $lbc_list[0]['wfggsl'];
            if($hb_wfggsl==0){
                $hb_wfggsl = '无变化';
            }elseif ($hb_wfggsl>0) {
                $hb_wfggsl = $hb_wfggsl.$arr2[0];
            }else{
                $hb_wfggsl = $hb_wfggsl.$arr2[1];
            }
            $hb_ggslwfl = $nbc_list[0]['ggslwfl']-$lbc_list[0]['ggslwfl'];
            if($hb_ggslwfl==0){
                $hb_ggslwfl = '无变化';
            }elseif ($hb_ggslwfl>0) {
                $hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[0];
            }else{
                $hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[1];
            }

            $hb_yzwfggsl = $nbc_list[0]['yzwfggsl'] - $lbc_list[0]['yzwfggsl'];
            if($hb_yzwfggsl==0){
                $hb_yzwfggsl = '无变化';
            }elseif ($hb_yzwfggsl>0) {
                $hb_yzwfggsl = $hb_yzwfggsl.$arr2[0];
            }else{
                $hb_yzwfggsl = $hb_yzwfggsl.$arr2[1];
            }

            $hb_ggslyzwfl = $nbc_list[0]['ggslyzwfl'] - $lbc_list[0]['ggslyzwfl'];
            if($hb_ggslyzwfl==0){
                $hb_ggslyzwfl = '无变化';
            }elseif ($hb_ggslyzwfl>0) {
                $hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[0];
            }else{
                $hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[1];
            }

            $html .= '<tr>';
            $html .= '<td>广播</td>';
            $html .= '<td>'.$nbc_list[0]['total'].'</td>';
            $html .= '<td>'.$hb_total.'</td>';
            $html .= '<td>'.$nbc_list[0]['wfggsl'].'</td>';
            $html .= '<td>'.$hb_wfggsl.'</td>';
            $html .= '<td>'.round($nbc_list[0]['ggslwfl']*100,2).'%</td>';
            $html .= '<td>'.$hb_ggslwfl.'</td>';
            $html .= '<td>'.$nbc_list[0]['yzwfggsl'].'</td>';
            $html .= '<td>'.$hb_yzwfggsl.'</td>';
            $html .= '<td>'.round($nbc_list[0]['ggslyzwfl']*100,2).'%</td>';
            $html .= '<td>'.$hb_ggslyzwfl.'</td></tr>';
        }

        $html .= '</table>';

        foreach ($attach4 as $key => &$value2) {

            if(substr($value2['fmediaclassid'], 0,2)=='01'){

                $value2['adv_totals'] += M('ttvissue_'.$now_date)->where('fmediaid='.$value2['fid'])->count();//电视媒体当月监测数量

                $value2['illegal_total'] += M('ttvissue_'.$now_date)
                    ->alias('tv')
                    ->join("
                        JOIN ttvsample tvs ON tv.fsampleid = tvs.fid
                    ")
                    ->where(array('tv.fmediaid'=>$value2['fid'],'tvs.fillegaltypecode'=>array('gt',10)))
                    ->count();//违法数

                $value2['serious_total'] += M('ttvissue_'.$now_date)
                    ->alias('tv')
                    ->join("
                        JOIN ttvsample tvs ON tv.fsampleid = tvs.fid
                    ")
                    ->where(array('tv.fmediaid'=>$value2['fid'],'tvs.fillegaltypecode'=>array('eq',30)))
                    ->count();//严重违法数

                $value2['commonly_total'] += M('ttvissue_'.$now_date)
                    ->alias('tv')
                    ->join("
                        JOIN ttvsample tvs ON tv.fsampleid = tvs.fid
                    ")
                    ->where(array('tv.fmediaid'=>$value2['fid'],'tvs.fillegaltypecode'=>array('eq',20)))
                    ->count();//一般违法数


            }
            if(substr($value2['fmediaclassid'], 0,2)=='02'){

                $value2['adv_totals'] += M('tbcissue_'.$now_date)->where('fmediaid='.$value2['fid'])->count();//广播媒体当月监测数量

                $value2['illegal_total'] += M('tbcissue_'.$now_date)
                    ->alias('tb')
                    ->join("
                        JOIN tbcsample tbs ON tb.fsampleid = tbs.fid
                    ")
                    ->where(array('tb.fmediaid'=>$value2['fid'],'tbs.fillegaltypecode'=>array('gt',10)))
                    ->count();//违法数

                $value2['serious_total'] += M('tbcissue_'.$now_date)
                    ->alias('tb')
                    ->join("
                        JOIN tbcsample tbs ON tb.fsampleid = tbs.fid
                    ")
                    ->where(array('tb.fmediaid'=>$value2['fid'],'tbs.fillegaltypecode'=>array('eq',30)))
                    ->count();//严重违法数

                $value2['commonly_total'] += M('tbcissue_'.$now_date)
                    ->alias('tb')
                    ->join("
                        JOIN tbcsample tbs ON tb.fsampleid = tbs.fid
                    ")
                    ->where(array('tb.fmediaid'=>$value2['fid'],'tbs.fillegaltypecode'=>array('eq',20)))
                    ->count();//一般违法数

            }
            if(substr($value2['fmediaclassid'], 0,2)=='03'){

                $value2['adv_totals'] += M('tpaperissue')->where('fmediaid='.$value2['fid'].' and date_format(fissuedate,"%Y-%m")='.'"'.$date.'"')->count();//报纸媒体当月监测数量

                $value2['illegal_total'] += M('tpaperissue')->alias('tp')
                    ->join("
                        JOIN tpapersample tps ON tp.fpapersampleid = tps.fpapersampleid
                    ")
                    ->where(array('tp.fmediaid'=>$value2['fid'],'tps.fillegaltypecode'=>array('gt',10),'date_format(tp.`fissuedate`,"%Y-%m")'=>'"'.$date.'"'))
                    ->count();//违法数

                $value2['serious_total'] += M('tpaperissue')->alias('tp')
                    ->join("
                        JOIN tpapersample tps ON tp.fpapersampleid = tps.fpapersampleid
                    ")
                    ->where(array('tp.fmediaid'=>$value2['fid'],'tps.fillegaltypecode'=>array('eq',30),'date_format(tp.`fissuedate`,"%Y-%m")'=>'"'.$date.'"'))
                    ->count();//严重违法数

                $value2['commonly_total'] += M('tpaperissue')->alias('tp')
                    ->join("
                        JOIN tpapersample tps ON tp.fpapersampleid = tps.fpapersampleid
                    ")
                    ->where(array('tp.fmediaid'=>$value2['fid'],'tps.fillegaltypecode'=>array('eq',20),'date_format(tp.`fissuedate`,"%Y-%m")'=>'"'.$date.'"'))
                    ->count();//一般违法数

            }



        }

        $arr1 = array('01'=>'电视','02'=>'广播','03'=>'报刊');

        foreach($attach4 as $value4){
            $mlist1[substr($value4['fmediaclassid'], 0,2)][] = $value4;
        }
        $html.= '<p>'.$arr1[$key].'媒体：</p>';
        $html .= ' 
			<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
			<tr> 
			 <td>序号</td> 
			 <td>媒体名称</td> 
			 <td>监测数</td> 
			 <td>总违法数</td> 
			 <td>严重违法数</td> 
			 <td>严重违法率</td> 
			 <td>一般违法</td> 
			 <td>监测违法率</td>
			</tr>';
        foreach($mlist1 as $mlist1_key => $value){

            foreach($value as $value_key => $val){
                $adv_totals1 += $val['adv_totals'];
                $illegal_total1 += $val['illegal_total'];
                $serious_total1 += $val['serious_total'];
                $commonly_total1 += $val['commonly_total'];
                $num++;
                $html .= '<tr>';
                $html .= '<td>'.$num.'</td>';
                $html .= '<td>'.$val['fmedianame'].'</td>';
                $html .= '<td>'.$val['adv_totals'].'</td>';
                $html .= '<td>'.$val['illegal_total'].'</td>';
                $html .= '<td>'.$val['serious_total'].'</td>';
                $html .= '<td>'.round(($val['serious_total']/$val['adv_totals'])*100,2).'%</td>';
                $html .= '<td>'.$val['commonly_total'].'</td>';
                $html .= '<td>'.round(($val['illegal_total']/$val['adv_totals'])*100,2).'%</td></tr>';
            }
        }
        $num++;
        $html .= '<tr>';
        $html .= '<td>'.$num.'</td>';
        $html .= '<td>小计</td>';
        $html .= '<td>'.$adv_totals1.'</td>';
        $html .= '<td>'.$illegal_total1.'</td>';
        $html .= '<td>'.$serious_total1.'</td>';
        $html .= '<td>'.(round(($serious_total1/$adv_totals1),4)*100).'%</td>';
        $html .= '<td>'.$commonly_total1.'</td>';
        $html .= '<td>'.(round(($illegal_total1/$adv_totals1),4)*100).'%</td></tr>';
        $html .= '</table>';

        if($tv_regionid){
            //电视媒体公益广告发布情况$tv_regionid $bc_regionid $paper_regionid
            $sql_tv = 'SELECT
	            	count(
                    CASE
                    WHEN DATE_FORMAT(tad.fcreatetime, "%H:%i:%S") >= "19:00:00"
                    AND DATE_FORMAT(tad.fcreatetime, "%H:%i:%S") <= "21:00:00" THEN
                        1
                    ELSE
                        NULL
                    END
                ) AS gyhjsl,
                count(*) - count(
                    CASE
                    WHEN DATE_FORMAT(tad.fcreatetime, "%H:%i:%S") >= "19:00:00"
                    AND DATE_FORMAT(tad.fcreatetime, "%H:%i:%S") <= "21:00:00" THEN
                        1
                    ELSE
                        NULL
                    END
                ) AS othersl,
                 (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as tmname
                FROM
	            ttvsample as ts
                JOIN ttvissue_'.$now_date.' AS tv ON tv.fsampleid = ts.fid
                JOIN tmedia AS tm ON tm.fid = ts.fmediaid
                JOIN tad ON tad.fadid = ts.fadid and tad.fadid<>0
                WHERE
	            tad.fadclasscode = 2202
                AND tv.fmediaid IN ('.join(',',$tv_regionid).')
                group by tv.fmediaid';
            $result_tv = M()->query($sql_tv);
            if($result_tv){
                $html .= '<p>电视媒体公益广告发布情况表</p>';
                $html.= '  
				<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
				<tr> 
				 <td rowspan=2  >媒体名称</td>  
				 <td colspan=4  style="border-bottom:1px solid #333333;">时间段（发布条次）</td>  
				</tr> 
				<tr>
					<td>黄金时段（19:00-21:00）</td>
					<td>其他时段</td>
				</tr>
				';
                foreach($result_tv as $v_tv){
                    $html.= '   
					<tr>
					<td>'.$v_tv['tmname'].'</td>
					<td>'.$v_tv['gyhjsl'].'</td>
					<td>'.$v_tv['othersl'].'</td>
					</tr>
					';
                }
                $html .= '</table>';
            }
        }



        if($bc_regionid){
            //广播媒体公益广告发布情况
            $sql_bc = 'SELECT
	            	count(
                    CASE
                    WHEN DATE_FORMAT(tad.fcreatetime, "%H:%i:%S") >= "11:00:00"
                    AND DATE_FORMAT(tad.fcreatetime, "%H:%i:%S") <= "13:00:00" THEN
                        1
                    ELSE
                        NULL
                    END
                ) AS gyhjsl,
                count(*) - count(
                    CASE
                    WHEN DATE_FORMAT(tad.fcreatetime, "%H:%i:%S") >= "11:00:00"
                    AND DATE_FORMAT(tad.fcreatetime, "%H:%i:%S") <= "13:00:00" THEN
                        1
                    ELSE
                        NULL
                    END
                ) AS othersl,
                 (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as tmname
                FROM
	            tbcsample as tb
                JOIN tbcissue_'.$now_date.' AS bc ON bc.fsampleid = tb.fid
                JOIN tmedia AS tm ON tm.fid = tb.fmediaid
                JOIN tad ON tad.fadid = tb.fadid and tad.fadid<>0
                WHERE
	            tad.fadclasscode = 2202 
                AND bc.fmediaid IN ('.join(',',$bc_regionid).')
                group by bc.fmediaid';
            $result_bc = M()->query($sql_bc);
            if($result_bc){
                $html .= '<p>广播媒体公益广告发布情况表</p>';
                $html.= '  
				<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
				<tr> 
				 <td rowspan=2  >媒体名称</td>  
				 <td colspan=4  style="border-bottom:1px solid #333333;">时间段（发布条次）</td>  
				</tr> 
				<tr>
					<td>黄金时段（11:00-13:00）</td>
					<td>其他时段</td>
				</tr>
			';
                foreach($result_bc as $v_bc){
                    $html.= '   
				<tr>
					<td>'.$v_bc['tmname'].'</td>
					<td>'.$v_bc['gyhjsl'].'</td>
					<td>'.$v_bc['othersl'].'</td>
				</tr>
			';
                }
                $html .= '</table>';
            }
        }


        //报刊媒体公益广告发布情况
        if($paper_regionid){
            $sql_paper = 'SELECT
                count(*) AS gyggsl,
                 (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) AS tmname
                FROM
	            tpapersample as tpaper
                JOIN tpaperissue AS paper ON paper.fpapersampleid = tpaper.fpapersampleid
                JOIN tmedia AS tm ON tm.fid = tpaper.fmediaid
                JOIN tad ON tad.fadid = tpaper.fadid and tad.fadid<>0
                WHERE
	            tad.fadclasscode = 2202
                AND paper.fmediaid IN ('.join(',',$paper_regionid).')
                group by paper.fmediaid';

            $result_paper = M()->query($sql_paper);
            if($result_paper){
                $html .= '<p>报刊媒体公益广告发布情况表</p>';
                $html.= '  
				<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
				<tr> 
				<td>媒体名称</td>  
				<td>公益广告条次</td>
				</tr>
			';
                foreach($result_paper as $v_paper){
                    $html.= '   
				<tr>
					<td>'.$v_paper['tmname'].'</td>
					<td>'.$v_paper['gyggsl'].'</td>
				</tr>
			';
                }

                $html .= '</table>';
            }
        }


        //省属媒体信息

        $html .= ' 
    		<div align="center" style="font-size:18px; font-weight:bold; padding:10px;">省属媒体：</div> 
			<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
			<tr> 
			 <td>媒体类别</td> 
			 <td>监测条次</td> 
			 <td>环比</td> 
			 <td>涉嫌违法广告条次</td> 
			 <td>环比</td> 
			 <td>违法率</td> 
			 <td>环比</td> 
			 <td>严重违法</td>
			 <td>环比</td>
			 <td>严重违法率</td>
			 <td>环比</td>
			</tr>';
        $tv_regionid2 = [];
        $bc_regionid2 = [];
        $paper_regionid2 = [];
        foreach($attach2 as $val){
            //省属电视媒体id
            if(substr($val['fmediaclassid'], 0,2)=='01'){
                array_push($tv_regionid2, $val['fid']);
            }
            //省属广播媒体id
            if(substr($val['fmediaclassid'], 0,2)=='02'){
                array_push($bc_regionid2, $val['fid']);
            }

            //省属报刊媒体id
            if(substr($val['fmediaclassid'], 0,2)=='03'){
                array_push($paper_regionid2, $val['fid']);
            }
        }
        //dump($tv_regionid2);dump($bc_regionid2);dump($paper_regionid2);exit;

        //当月报刊监测情况
        if($paper_regionid2){
            $sql = 'select count(*) total from tpaperissue bc where bc.fmediaid in ('.join(',',$paper_regionid2).') and date_format(bc.`fissuedate`,"%Y-%m")='.'"'.$date.'"';
            $npa_total = M()->query($sql);//当月报刊监测数
            $sql = 'select ifnull(sum(case when tps.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when tps.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when tps.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when tps.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from tpaperissue tp join tpapersample tps on tp.fpapersampleid=tps.fpapersampleid where tp.fmediaid in ('.join(',',$paper_regionid2).') and date_format(tp.`fissuedate`,"%Y-%m")='.'"'.$date.'"';
            $npa_list = M()->query($sql);
            $npa_list[0]['total'] = $npa_total[0]['total'];

            //上月报刊监测情况
            $sql = 'select count(*) total from tpaperissue bc where bc.fmediaid in ('.join(',',$paper_regionid2).') and date_format(bc.`fissuedate`,"%Y-%m")='.'"'.$date_last.'"';
            $lpa_total = M()->query($sql);//上月报刊监测数
            $sql = 'select ifnull(sum(case when tps.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when tps.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when tps.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when tps.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from tpaperissue tp join tpapersample tps on tp.fpapersampleid=tps.fpapersampleid where tp.fmediaid in ('.join(',',$paper_regionid2).') and date_format(tp.`fissuedate`,"%Y-%m")='.'"'.$date_last.'"';
            $lpa_list1 = M()->query($sql);
            $lpa_list1[0]['total'] = $npa_total[0]['total'];
            $hb_total = $npa_list1[0]['total'] - $lpa_list1[0]['total'];
            if($hb_total==0){
                $hb_total = '无变化';
            }elseif ($hb_total>0) {
                $hb_total = $hb_total.$arr2[0];
            }else{
                $hb_total = $hb_total.$arr2[1];
            }
            $hb_wfggsl = $npa_list1[0]['wfggsl'] - $lpa_list1[0]['wfggsl'];
            if($hb_wfggsl==0){
                $hb_wfggsl = '无变化';
            }elseif ($hb_wfggsl>0) {
                $hb_wfggsl = $hb_wfggsl.$arr2[0];
            }else{
                $hb_wfggsl = $hb_wfggsl.$arr2[1];
            }
            $hb_ggslwfl = $npa_list1[0]['ggslwfl']-$lpa_list1[0]['ggslwfl'];
            if($hb_ggslwfl==0){
                $hb_ggslwfl = '无变化';
            }elseif ($hb_ggslwfl>0) {
                $hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[0];
            }else{
                $hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[1];
            }

            $hb_yzwfggsl = $npa_list1[0]['yzwfggsl'] - $lpa_list1[0]['yzwfggsl'];
            if($hb_yzwfggsl==0){
                $hb_yzwfggsl = '无变化';
            }elseif ($hb_yzwfggsl>0) {
                $hb_yzwfggsl = $hb_yzwfggsl.$arr2[0];
            }else{
                $hb_yzwfggsl = $hb_yzwfggsl.$arr2[1];
            }

            $hb_ggslyzwfl = $npa_list1[0]['ggslyzwfl'] - $lpa_list1[0]['ggslyzwfl'];
            if($hb_ggslyzwfl==0){
                $hb_ggslyzwfl = '无变化';
            }elseif ($hb_ggslyzwfl>0) {
                $hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[0];
            }else{
                $hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[1];
            }

            $html .= '<tr>';
            $html .= '<td>报刊</td>';
            $html .= '<td>'.$npa_list[0]['total'].'</td>';
            $html .= '<td>'.$hb_total.'</td>';
            $html .= '<td>'.$npa_list[0]['wfggsl'].'</td>';
            $html .= '<td>'.$hb_wfggsl.'</td>';
            $html .= '<td>'.round($npa_list[0]['ggslwfl']*100,2).'%</td>';
            $html .= '<td>'.$hb_ggslwfl.'</td>';
            $html .= '<td>'.$npa_list[0]['yzwfggsl'].'</td>';
            $html .= '<td>'.$hb_yzwfggsl.'</td>';
            $html .= '<td>'.round($npa_list[0]['ggslyzwfl']*100,2).'%</td>';
            $html .= '<td>'.$hb_ggslyzwfl.'</td></tr>';
        }


        //当月电视监测情况
        if($tv_regionid2){


            $sql_tv_count = 'select count(*) total from ttvissue_'.$now_date.' tv where tv.fmediaid in ('.join(',',$tv_regionid2).')';
            $ntv_total = M()->query($sql_tv_count);//当月电视监测数

            $sql_tv = 'select ifnull(sum(case when tvs.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when tvs.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when tvs.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when tvs.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from ttvissue_'.$now_date.' tv join ttvsample tvs on tv.fsampleid=tvs.fid where tv.fmediaid in ('.join(',',$tv_regionid2).')';

            $ntv_list1 = M()->query($sql_tv);
            $ntv_list1[0]['total'] = $ntv_total[0]['total'];
            //dump($sql_tv);exit;

            //上月电视监测情况
            $sql_tv_l_count = 'select count(*) total from ttvissue_'.$last_date.' bc where bc.fmediaid in ('.join(',',$tv_regionid2).')';
            $ltv_total = M()->query($sql_tv_l_count);//上月电视监测数
            $sql_tv_l = 'select ifnull(sum(case when tvs.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when tvs.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when tvs.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when tvs.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from ttvissue_'.$last_date.' tv join ttvsample tvs on tv.fsampleid=tvs.fid where tv.fmediaid in ('.join(',',$tv_regionid2).')';
            $ltv_list1 = M()->query($sql_tv_l);

            $hb_total = $ntv_list1[0]['total'] - $ltv_total[0]['total'];
            if($hb_total==0){
                $hb_total = '无变化';
            }elseif ($hb_total>0) {
                $hb_total = $hb_total.$arr2[0];
            }else{
                $hb_total = $hb_total.$arr2[1];
            }
            $hb_wfggsl = $ntv_list1[0]['wfggsl'] - $ltv_list1[0]['wfggsl'];
            if($hb_wfggsl==0){
                $hb_wfggsl = '无变化';
            }elseif ($hb_wfggsl>0) {
                $hb_wfggsl = $hb_wfggsl.$arr2[0];
            }else{
                $hb_wfggsl = $hb_wfggsl.$arr2[1];
            }
            $hb_ggslwfl = $ntv_list1[0]['ggslwfl']-$ltv_list1[0]['ggslwfl'];
            if($hb_ggslwfl==0){
                $hb_ggslwfl = '无变化';
            }elseif ($hb_ggslwfl>0) {
                $hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[0];
            }else{
                $hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[1];
            }

            $hb_yzwfggsl = $ntv_list1[0]['yzwfggsl'] - $ltv_list1[0]['yzwfggsl'];
            if($hb_yzwfggsl==0){
                $hb_yzwfggsl = '无变化';
            }elseif ($hb_yzwfggsl>0) {
                $hb_yzwfggsl = $hb_yzwfggsl.$arr2[0];
            }else{
                $hb_yzwfggsl = $hb_yzwfggsl.$arr2[1];
            }

            $hb_ggslyzwfl = $ntv_list1[0]['ggslyzwfl'] - $ltv_list1[0]['ggslyzwfl'];
            if($hb_ggslyzwfl==0){
                $hb_ggslyzwfl = '无变化';
            }elseif ($hb_ggslyzwfl>0) {
                $hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[0];
            }else{
                $hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[1];
            }

            $html .= '<tr>';
            $html .= '<td>电视</td>';
            $html .= '<td>'.$ntv_list1[0]['total'].'</td>';
            $html .= '<td>'.$hb_total.'</td>';
            $html .= '<td>'.$ntv_list1[0]['wfggsl'].'</td>';
            $html .= '<td>'.$hb_wfggsl.'</td>';
            $html .= '<td>'.round($ntv_list1[0]['ggslwfl']*100,2).'%</td>';
            $html .= '<td>'.$hb_ggslwfl.'</td>';
            $html .= '<td>'.$ntv_list1[0]['yzwfggsl'].'</td>';
            $html .= '<td>'.$hb_yzwfggsl.'</td>';
            $html .= '<td>'.round($ntv_list1[0]['ggslyzwfl']*100,2).'%</td>';
            $html .= '<td>'.$hb_ggslyzwfl.'</td></tr>';
        }

        //当月广播测试情况
        if($bc_regionid2){

            $sql = 'select count(*) total from tbcissue_'.$now_date.' bc where bc.fmediaid in ('.join(',',$bc_regionid2).')';

            $nbc_total = M()->query($sql);//当月广播监测数
            $sql = 'select ifnull(sum(case when tbs.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when tbs.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when tbs.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when tbs.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from tbcissue_'.$now_date.' tb join tbcsample tbs on tb.fsampleid=tbs.fid where tb.fmediaid in ('.join(',',$bc_regionid2).')';
            $nbc_list = M()->query($sql);

            $nbc_list[0]['total'] = $nbc_total[0]['total'];

            //上月广播监测情况
            $sql = 'select count(*) total from tbcissue_'.$last_date.' bc where bc.fmediaid in ('.join(',',$bc_regionid2).')';
            $lbc_total = M()->query($sql);//上月广播监测数
            $sql = 'select ifnull(sum(case when tbs.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when tbs.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when tbs.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when tbs.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from tbcissue_'.$last_date.' tb join tbcsample tbs on tb.fsampleid=tbs.fid where tb.fmediaid in ('.join(',',$bc_regionid2).')';
            $lbc_list1 = M()->query($sql);
            $lbc_list1[0]['total'] = $lbc_total[0]['total'];
            $hb_total = $nbc_list1[0]['total'] - $lbc_total[0]['total'];
            if($hb_total==0){
                $hb_total = '无变化';
            }elseif ($hb_total>0) {
                $hb_total = $hb_total.$arr2[0];
            }else{
                $hb_total = $hb_total.$arr2[1];
            }
            $hb_wfggsl = $nbc_list1[0]['wfggsl'] - $lbc_list1[0]['wfggsl'];
            if($hb_wfggsl==0){
                $hb_wfggsl = '无变化';
            }elseif ($hb_wfggsl>0) {
                $hb_wfggsl = $hb_wfggsl.$arr2[0];
            }else{
                $hb_wfggsl = $hb_wfggsl.$arr2[1];
            }
            $hb_ggslwfl = $nbc_list1[0]['ggslwfl']-$lbc_list1[0]['ggslwfl'];
            if($hb_ggslwfl==0){
                $hb_ggslwfl = '无变化';
            }elseif ($hb_ggslwfl>0) {
                $hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[0];
            }else{
                $hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[1];
            }

            $hb_yzwfggsl = $nbc_list1[0]['yzwfggsl'] - $lbc_list1[0]['yzwfggsl'];
            if($hb_yzwfggsl==0){
                $hb_yzwfggsl = '无变化';
            }elseif ($hb_yzwfggsl>0) {
                $hb_yzwfggsl = $hb_yzwfggsl.$arr2[0];
            }else{
                $hb_yzwfggsl = $hb_yzwfggsl.$arr2[1];
            }

            $hb_ggslyzwfl = $nbc_list1[0]['ggslyzwfl'] - $lbc_list1[0]['ggslyzwfl'];
            if($hb_ggslyzwfl==0){
                $hb_ggslyzwfl = '无变化';
            }elseif ($hb_ggslyzwfl>0) {
                $hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[0];
            }else{
                $hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[1];
            }

            $html .= '<tr>';
            $html .= '<td>广播</td>';
            $html .= '<td>'.$nbc_list[0]['total'].'</td>';
            $html .= '<td>'.$hb_total.'</td>';
            $html .= '<td>'.$nbc_list[0]['wfggsl'].'</td>';
            $html .= '<td>'.$hb_wfggsl.'</td>';
            $html .= '<td>'.round($nbc_list[0]['ggslwfl']*100,2).'%</td>';
            $html .= '<td>'.$hb_ggslwfl.'</td>';
            $html .= '<td>'.$nbc_list[0]['yzwfggsl'].'</td>';
            $html .= '<td>'.$hb_yzwfggsl.'</td>';
            $html .= '<td>'.round($nbc_list[0]['ggslyzwfl']*100,2).'%</td>';
            $html .= '<td>'.$hb_ggslyzwfl.'</td></tr>';
        }

        $html .= '</table>';

        $adv_totals1 = $npa_list[0]['total'] + $ntv_list1[0]['total'] + $nbc_list[0]['total'];
        foreach ($attach2 as $key => &$value2) {

            if(substr($value2['fmediaclassid'], 0,2)=='01'){

                $value2['adv_totals'] += M('ttvissue_'.$now_date)->where('fmediaid='.$value2['fid'])->count();//电视媒体当月监测数量

                $value2['illegal_total'] += M('ttvissue_'.$now_date)
                    ->alias('tv')
                    ->join("
                        JOIN ttvsample tvs ON tv.fsampleid = tvs.fid
                    ")
                    ->where(array('tv.fmediaid'=>$value2['fid'],'tvs.fillegaltypecode'=>array('gt',10)))
                    ->count();//违法数

                $value2['serious_total'] += M('ttvissue_'.$now_date)
                    ->alias('tv')
                    ->join("
                        JOIN ttvsample tvs ON tv.fsampleid = tvs.fid
                    ")
                    ->where(array('tv.fmediaid'=>$value2['fid'],'tvs.fillegaltypecode'=>array('eq',30)))
                    ->count();//严重违法数

                $value2['commonly_total'] += M('ttvissue_'.$now_date)
                    ->alias('tv')
                    ->join("
                        JOIN ttvsample tvs ON tv.fsampleid = tvs.fid
                    ")
                    ->where(array('tv.fmediaid'=>$value2['fid'],'tvs.fillegaltypecode'=>array('eq',20)))
                    ->count();//一般违法数


            }
            if(substr($value2['fmediaclassid'], 0,2)=='02'){

                $value2['adv_totals'] += M('tbcissue_'.$now_date)->where('fmediaid='.$value2['fid'])->count();//广播媒体当月监测数量

                $value2['illegal_total'] += M('tbcissue_'.$now_date)
                    ->alias('tb')
                    ->join("
                        JOIN tbcsample tbs ON tb.fsampleid = tbs.fid
                    ")
                    ->where(array('tb.fmediaid'=>$value2['fid'],'tbs.fillegaltypecode'=>array('gt',10)))
                    ->count();//违法数

                $value2['serious_total'] += M('tbcissue_'.$now_date)
                    ->alias('tb')
                    ->join("
                        JOIN tbcsample tbs ON tb.fsampleid = tbs.fid
                    ")
                    ->where(array('tb.fmediaid'=>$value2['fid'],'tbs.fillegaltypecode'=>array('eq',30)))
                    ->count();//严重违法数

                $value2['commonly_total'] += M('tbcissue_'.$now_date)
                    ->alias('tb')
                    ->join("
                        JOIN tbcsample tbs ON tb.fsampleid = tbs.fid
                    ")
                    ->where(array('tb.fmediaid'=>$value2['fid'],'tbs.fillegaltypecode'=>array('eq',20)))
                    ->count();//一般违法数

            }
            if(substr($value2['fmediaclassid'], 0,2)=='03'){

                $value2['adv_totals'] += M('tpaperissue')
                    ->where('fmediaid='.$value2['fid'].' and DATE_FORMAT(fissuedate,"%Y-%m") = '.'"'.$date.'"')
                    ->count();//报纸媒体当月监测数量

                $value2['illegal_total'] += M('tpaperissue')->alias('tp')
                    ->join("
                        JOIN tpapersample tps ON tp.fpapersampleid = tps.fpapersampleid
                    ")
                    ->where(array('tp.fmediaid'=>$value2['fid'],'tps.fillegaltypecode'=>array('gt',10),'DATE_FORMAT(tp.fissuedate,"%Y-%m")'=>'"'.$date.'"'))
                    ->count();//违法数

                $value2['serious_total'] += M('tpaperissue')->alias('tp')
                    ->join("
                        JOIN tpapersample tps ON tp.fpapersampleid = tps.fpapersampleid
                    ")
                    ->where(array('tp.fmediaid'=>$value2['fid'],'tps.fillegaltypecode'=>array('eq',30),'DATE_FORMAT(tp.fissuedate,"%Y-%m")'=>'"'.$date.'"'))
                    ->count();//严重违法数

                $value2['commonly_total'] += M('tpaperissue')->alias('tp')
                    ->join("
                        JOIN tpapersample tps ON tp.fpapersampleid = tps.fpapersampleid
                    ")
                    ->where(array('tp.fmediaid'=>$value2['fid'],'tps.fillegaltypecode'=>array('eq',20),'DATE_FORMAT(tp.fissuedate,"%Y-%m")'=>'"'.$date.'"'))
                    ->count();//一般违法数

            }



        }

        $arr1 = array('01'=>'电视','02'=>'广播','03'=>'报刊');

        foreach($attach2 as $value4){
            $mlist2[substr($value4['fmediaclassid'], 0,2)][] = $value4;
        }
        $html.= '<p>'.$arr1[$key].'媒体：</p>';
        $html .= ' 
			<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
			<tr> 
			 <td>序号</td> 
			 <td>媒体名称</td> 
			 <td>监测数</td> 
			 <td>总违法数</td> 
			 <td>严重违法数</td> 
			 <td>严重违法率</td> 
			 <td>一般违法</td> 
			 <td>监测违法率</td>
			</tr>';
        foreach($mlist2 as $mlist1_key => $value){

            foreach($value as $value_key => $val){
                //$adv_totals1 += $val['adv_totals'];
                $illegal_total1 += $val['illegal_total'];
                $serious_total1 += $val['serious_total'];
                $commonly_total1 += $val['commonly_total'];
                $num++;
                $html .= '<tr>';
                $html .= '<td>'.$num.'</td>';
                $html .= '<td>'.$val['fmedianame'].'</td>';
                $html .= '<td>'.$val['adv_totals'].'</td>';
                $html .= '<td>'.$val['illegal_total'].'</td>';
                $html .= '<td>'.$val['serious_total'].'</td>';
                $html .= '<td>'.round(($val['serious_total']/$val['adv_totals'])*100,2).'%</td>';
                $html .= '<td>'.$val['commonly_total'].'</td>';
                $html .= '<td>'.round(($val['illegal_total']/$val['adv_totals'])*100,2).'%</td></tr>';
            }
        }
        $num++;
        $html .= '<tr>';
        $html .= '<td>'.$num.'</td>';
        $html .= '<td>小计</td>';
        $html .= '<td>'.$adv_totals1.'</td>';
        $html .= '<td>'.$illegal_total1.'</td>';
        $html .= '<td>'.$serious_total1.'</td>';
        $html .= '<td>'.(round(($serious_total1/$adv_totals1),4)*100).'%</td>';
        $html .= '<td>'.$commonly_total1.'</td>';
        $html .= '<td>'.(round(($illegal_total1/$adv_totals1),4)*100).'%</td></tr>';
        $html .= '</table>';


        $html.= '<h3>（二）行业监测情况</h3>';
        $where1 = 'where ac.fpcode=" "';

        $sql = 'SELECT
                adc.ffullname as fadclass,
                b.fpcode as fcode,
                total,
                ybwfggsl,
                yzwfggsl,
                ggslyzwfl,
                wfggsl,
                ggslwfl
                FROM
                (
                    SELECT
                        fcode,
                        CASE
                    WHEN fpcode = "" THEN
                        fcode
                    ELSE
                        fpcode
                    END fpcode,
                    sum(total) AS total,
                    sum(ybwfggsl) AS ybwfggsl,
                    sum(yzwfggsl) AS yzwfggsl,
                    sum(yzwfggsl) / sum(total) AS ggslyzwfl,
                    sum(wfggsl) AS wfggsl,
                    sum(wfggsl) / sum(total) AS ggslwfl
                FROM
                    (
                        (
                            SELECT
                                count(*) total,
                                ac.fadclassid,
                                ac.fcode,
                                ac.fpcode,
                                sum(
                                    CASE
                                    WHEN ts.fillegaltypecode = 20 THEN
                                        1
                                    ELSE
                                        0
                                    END
                                ) AS ybwfggsl,
                                sum(
                                    CASE
                                    WHEN ts.fillegaltypecode = 30 THEN
                                        1
                                    ELSE
                                        0
                                    END
                                ) AS yzwfggsl,
                                ifnull(
                                    sum(
                                        CASE
                                        WHEN ts.fillegaltypecode = 30 THEN
                                            1
                                        ELSE
                                            0
                                        END
                                    ) / count(*),
                                    0
                                ) AS ggslyzwfl,
                                sum(
                                    CASE
                                    WHEN ts.fillegaltypecode > 0 THEN
                                        1
                                    ELSE
                                        0
                                    END
                                ) AS wfggsl,
                                ifnull(
                                    sum(
                                        CASE
                                        WHEN ts.fillegaltypecode > 0 THEN
                                            1
                                        ELSE
                                            0
                                        END
                                    ) / count(*),
                                    0
                                ) AS ggslwfl
                            FROM
                                ttvissue_'.$now_date.' AS tv
                            JOIN ttvsample AS ts ON tv.fsampleid = ts.fid
                            JOIN tad ON tad.fadid = ts.fadid and tad.fadid<>0
                            JOIN tadclass AS ac ON ac.fcode = tad.fadclasscode
                            WHERE
                                tv.fmediaid IN ('.join(',',$regionids).')
                            GROUP BY
                                ac.fcode
                        )
                        UNION ALL
                            (
                                SELECT
                                    count(*) total,
                                    ac.fadclassid,
                                    ac.fcode,
                                    ac.fpcode,
                                    sum(
                                        CASE
                                        WHEN ts.fillegaltypecode = 20 THEN
                                            1
                                        ELSE
                                            0
                                        END
                                    ) AS ybwfggsl,
                                    sum(
                                        CASE
                                        WHEN ts.fillegaltypecode = 30 THEN
                                            1
                                        ELSE
                                            0
                                        END
                                    ) AS yzwfggsl,
                                    ifnull(
                                        sum(
                                            CASE
                                            WHEN ts.fillegaltypecode = 30 THEN
                                                1
                                            ELSE
                                                0
                                            END
                                        ) / count(*),
                                        0
                                    ) AS ggslyzwfl,
                                    sum(
                                        CASE
                                        WHEN ts.fillegaltypecode > 0 THEN
                                            1
                                        ELSE
                                            0
                                        END
                                    ) AS wfggsl,
                                    ifnull(
                                        sum(
                                            CASE
                                            WHEN ts.fillegaltypecode > 0 THEN
                                                1
                                            ELSE
                                                0
                                            END
                                        ) / count(*),
                                        0
                                    ) AS ggslwfl
                                FROM
                                    tbcissue_'.$now_date.' AS tv
                                JOIN ttvsample AS ts ON tv.fsampleid = ts.fid
                                JOIN tad ON tad.fadid = ts.fadid and tad.fadid<>0
                                JOIN tadclass AS ac ON ac.fcode = tad.fadclasscode
                                WHERE
                                    tv.fmediaid IN ('.join(',',$regionids).')
                                GROUP BY
                                    ac.fcode
                            )
                        UNION ALL
                            (
                                SELECT
                                    count(*) total,
                                    ac.fadclassid,
                                    ac.fcode,
                                    ac.fpcode,
                                    sum(
                                        CASE
                                        WHEN ts.fillegaltypecode = 20 THEN
                                            1
                                        ELSE
                                            0
                                        END
                                    ) AS ybwfggsl,
                                    sum(
                                        CASE
                                        WHEN ts.fillegaltypecode = 30 THEN
                                            1
                                        ELSE
                                            0
                                        END
                                    ) AS yzwfggsl,
                                    ifnull(
                                        sum(
                                            CASE
                                            WHEN ts.fillegaltypecode = 30 THEN
                                                1
                                            ELSE
                                                0
                                            END
                                        ) / count(*),
                                        0
                                    ) AS ggslyzwfl,
                                    sum(
                                        CASE
                                        WHEN ts.fillegaltypecode > 0 THEN
                                            1
                                        ELSE
                                            0
                                        END
                                    ) AS wfggsl,
                                    ifnull(
                                        sum(
                                            CASE
                                            WHEN ts.fillegaltypecode > 0 THEN
                                                1
                                            ELSE
                                                0
                                            END
                                        ) / count(*),
                                        0
                                    ) AS ggslwfl
                                FROM
                                    tpaperissue AS tv
                                JOIN tpapersample AS ts ON tv.fpapersampleid = ts.fpapersampleid
                                JOIN tad ON tad.fadid = ts.fadid and tad.fadid<>0
                                JOIN tadclass AS ac ON ac.fcode = tad.fadclasscode
                                WHERE
                                    tv.fmediaid IN ('.join(',',$regionids).')
                                AND DATE_FORMAT(tv.fissuedate, "%Y-%m") = "'.$date.'"
                                GROUP BY
                                    ac.fcode
                            )
                                ) AS a
                            GROUP BY
                                CASE
                            WHEN fpcode = "" THEN
                                fcode
                            ELSE
                                fpcode
                            END
                            ) AS b
                            JOIN tadclass AS adc ON b.fpcode = adc.fcode';
        $sql1 = 'SELECT
                adc.ffullname as fadclass,
                b.fpcode as fcode,
                total,
                ybwfggsl,
                yzwfggsl,
                ggslyzwfl,
                wfggsl,
                ggslwfl
                FROM
                (
                    SELECT
                        fcode,
                        CASE
                    WHEN fpcode = "" THEN
                        fcode
                    ELSE
                        fpcode
                    END fpcode,
                    sum(total) AS total,
                    sum(ybwfggsl) AS ybwfggsl,
                    sum(yzwfggsl) AS yzwfggsl,
                    sum(yzwfggsl) / sum(total) AS ggslyzwfl,
                    sum(wfggsl) AS wfggsl,
                    sum(wfggsl) / sum(total) AS ggslwfl
                FROM
                    (
                        (
                            SELECT
                                count(*) total,
                                ac.fadclassid,
                                ac.fcode,
                                ac.fpcode,
                                sum(
                                    CASE
                                    WHEN ts.fillegaltypecode = 20 THEN
                                        1
                                    ELSE
                                        0
                                    END
                                ) AS ybwfggsl,
                                sum(
                                    CASE
                                    WHEN ts.fillegaltypecode = 30 THEN
                                        1
                                    ELSE
                                        0
                                    END
                                ) AS yzwfggsl,
                                ifnull(
                                    sum(
                                        CASE
                                        WHEN ts.fillegaltypecode = 30 THEN
                                            1
                                        ELSE
                                            0
                                        END
                                    ) / count(*),
                                    0
                                ) AS ggslyzwfl,
                                sum(
                                    CASE
                                    WHEN ts.fillegaltypecode > 0 THEN
                                        1
                                    ELSE
                                        0
                                    END
                                ) AS wfggsl,
                                ifnull(
                                    sum(
                                        CASE
                                        WHEN ts.fillegaltypecode > 0 THEN
                                            1
                                        ELSE
                                            0
                                        END
                                    ) / count(*),
                                    0
                                ) AS ggslwfl
                            FROM
                                ttvissue_'.$last_date.' AS tv
                            JOIN ttvsample AS ts ON tv.fsampleid = ts.fid
                            JOIN tad ON tad.fadid = ts.fadid and tad.fadid<>0
                            JOIN tadclass AS ac ON ac.fcode = tad.fadclasscode
                            WHERE
                                tv.fmediaid IN ('.join(',',$regionids).')
                            GROUP BY
                                ac.fcode
                        )
                        UNION ALL
                            (
                                SELECT
                                    count(*) total,
                                    ac.fadclassid,
                                    ac.fcode,
                                    ac.fpcode,
                                    sum(
                                        CASE
                                        WHEN ts.fillegaltypecode = 20 THEN
                                            1
                                        ELSE
                                            0
                                        END
                                    ) AS ybwfggsl,
                                    sum(
                                        CASE
                                        WHEN ts.fillegaltypecode = 30 THEN
                                            1
                                        ELSE
                                            0
                                        END
                                    ) AS yzwfggsl,
                                    ifnull(
                                        sum(
                                            CASE
                                            WHEN ts.fillegaltypecode = 30 THEN
                                                1
                                            ELSE
                                                0
                                            END
                                        ) / count(*),
                                        0
                                    ) AS ggslyzwfl,
                                    sum(
                                        CASE
                                        WHEN ts.fillegaltypecode > 0 THEN
                                            1
                                        ELSE
                                            0
                                        END
                                    ) AS wfggsl,
                                    ifnull(
                                        sum(
                                            CASE
                                            WHEN ts.fillegaltypecode > 0 THEN
                                                1
                                            ELSE
                                                0
                                            END
                                        ) / count(*),
                                        0
                                    ) AS ggslwfl
                                FROM
                                    tbcissue_'.$last_date.' AS tv
                                JOIN ttvsample AS ts ON tv.fsampleid = ts.fid
                                JOIN tad ON tad.fadid = ts.fadid and tad.fadid<>0
                                JOIN tadclass AS ac ON ac.fcode = tad.fadclasscode
                                WHERE
                                    tv.fmediaid IN ('.join(',',$regionids).')
                                GROUP BY
                                    ac.fcode
                            )
                        UNION ALL
                            (
                                SELECT
                                    count(*) total,
                                    ac.fadclassid,
                                    ac.fcode,
                                    ac.fpcode,
                                    sum(
                                        CASE
                                        WHEN ts.fillegaltypecode = 20 THEN
                                            1
                                        ELSE
                                            0
                                        END
                                    ) AS ybwfggsl,
                                    sum(
                                        CASE
                                        WHEN ts.fillegaltypecode = 30 THEN
                                            1
                                        ELSE
                                            0
                                        END
                                    ) AS yzwfggsl,
                                    ifnull(
                                        sum(
                                            CASE
                                            WHEN ts.fillegaltypecode = 30 THEN
                                                1
                                            ELSE
                                                0
                                            END
                                        ) / count(*),
                                        0
                                    ) AS ggslyzwfl,
                                    sum(
                                        CASE
                                        WHEN ts.fillegaltypecode > 0 THEN
                                            1
                                        ELSE
                                            0
                                        END
                                    ) AS wfggsl,
                                    ifnull(
                                        sum(
                                            CASE
                                            WHEN ts.fillegaltypecode > 0 THEN
                                                1
                                            ELSE
                                                0
                                            END
                                        ) / count(*),
                                        0
                                    ) AS ggslwfl
                                FROM
                                    tpaperissue AS tv
                                JOIN tpapersample AS ts ON tv.fpapersampleid = ts.fpapersampleid
                                JOIN tad ON tad.fadid = ts.fadid and tad.fadid<>0
                                JOIN tadclass AS ac ON ac.fcode = tad.fadclasscode
                                WHERE
                                    tv.fmediaid IN ('.join(',',$regionids).')
                                AND DATE_FORMAT(tv.fissuedate, "%Y-%m") = "'.$date_last.'"
                                GROUP BY
                                    ac.fcode
                            )
                                ) AS a
                            GROUP BY
                                CASE
                            WHEN fpcode = "" THEN
                                fcode
                            ELSE
                                fpcode
                            END
                            ) AS b
                            JOIN tadclass AS adc ON b.fpcode = adc.fcode';

        $do_ie = M()->query($sql);//获取统计结果
        $last_doie = M()->query($sql1);
        $html.= '8大重点行业情况表：';
        $array1 = ['13','16','08','06','04','03','02','01'];
        // dump($do_ie);
        // dump($array1);
        $array2 = ['↑','↓'];
        $do_ie1 = [];
        $last_doie1 = [];
        foreach($do_ie as $k=>$val){
            if(in_array($val['fcode'],$array1)){
                array_push($do_ie1, $val);
            }
        }
        foreach($last_doie as $k=>$val){
            if(in_array($val['fcode'],$array1)){

                array_push($last_doie1, $val);
            }
        }
        foreach($last_doie1 as $v2){
            $lastd_totals += $v2['total'];//上月监测总量
            $lastd_wfggsls += $v2['wfggsl'];//上月违法总量
        }
        $lastd_ggslwfl = $lastd_wfggsls/$lastd_totals;//上月总违法率


        foreach($do_ie1 as &$v){
            foreach($last_doie1 as $v1){
                if($v['fcode']==$v1['fcode']){

                    $v['total_'] = ($v['total'] - $v1['total']==0)?'无变化':(($v['total'] - $v1['total'])>0?$v['total'] - $v1['total'].$array2[0]:(abs($v['total'] - $v1['total'])).$array2[1]);
                    $v['wfggsl_'] = ($v['wfggsl'] - $v1['wfggsl']==0)?'无变化':(($v['wfggsl'] - $v1['wfggsl'])>0?$v['wfggsl'] - $v1['wfggsl'].$array2[0]:(abs($v['wfggsl'] - $v1['wfggsl'])).$array2[1]);
                    $v['ggslwfl_'] = $v['ggslwfl'] - $v1['ggslwfl'];
                }
            }
        }
        $html .= ' 
			<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
			<tr>
			<td>主类别</td> 
			<td>监测条次</td> 
			<td>环比</td> 
			<td>涉嫌违法广告条次</td> 
			<td>环比</td> 
			<td>违法率</td> 
			<td>环比</td>
			</tr>';

        foreach($do_ie1 as $key => $val){
            $ad_totals += $val['total'];
            $ad_wfggsls += $val['wfggsl'];
            $html .= '
				<tr> 
					<td>'.$val['fadclass'].'</td> 
					<td>'.$val['total'].'</td> 
					<td>'.$val['total_'].'</td> 
					<td>'.$val['wfggsl'].'</td> 
					<td>'.$val['wfggsl_'].'</td> 
					<td>'.round($val['ggslwfl']*100,2).'%</td> ';
            if($val['ggslwfl_']==0){
                $hb_ggslwfl_ = '无变化';
            }elseif($val['ggslwfl_']>0){
                $hb_ggslwfl_ = round($val['ggslwfl_']*100,2).'%'.$array2[0];
            }else{
                $hb_ggslwfl_ = round(abs($val['ggslwfl_'])*100,2).'%'.$array2[1];
            }
            $html.='
					<td>'.$hb_ggslwfl_.'</td>
				</tr>';
        }
        $ad_ggslwfl = $ad_wfggsls/$ad_totals;//当月违法率
        $hb_ggslwfl = $ad_ggslwfl - $lastd_ggslwfl;//违法率环比

        if($hb_ggslwfl==0){
            $hb_ggslwfl = '无变化';
        }elseif($hb_ggslwfl>0){
            $hb_ggslwfl = round($hb_ggslwfl*100,2).'%'.$array2[0];
        }else{
            $hb_ggslwfl = round(abs($hb_ggslwfl)*100,2).'%'.$array2[1];
        }

        if($ad_totals - $lastd_totals==0){
            $hb_totals = '无变化';
        }elseif($ad_totals - $lastd_totals>0){
            $hb_totals = $ad_totals - $lastd_totals.$array2[0];
        }else{
            $hb_totals = abs($ad_totals - $lastd_totals).$array2[1];
        }

        if($ad_wfggsls - $lastd_wfggsls==0){
            $hb_wfggsls = '无变化';
        }elseif($ad_wfggsls - $lastd_wfggsls>0){
            $hb_wfggsls = $ad_wfggsls - $lastd_wfggsls.$array2[0];
        }else{
            $hb_wfggsls = abs($ad_wfggsls - $lastd_wfggsls).$array2[1];
        }

        $html .= '<tr>';
        $html .= '<td>八类重点行业累计</td>';
        $html .= '<td>'.$ad_totals.'</td>';
        $html .= '<td>'.$hb_totals.'</td>';
        $html .= '<td>'.$ad_wfggsls.'</td>';
        $html .= '<td>'.$hb_wfggsls.'</td>';
        $html .= '<td>'.round($ad_ggslwfl*100,2).'%</td>';
        $html .= '<td>'.$hb_ggslwfl.'</td></tr>';


        $html .= '</table>';

        $html.= '23大类广告情况表（按违法率排名）：';

        $html .= ' 
			<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
			<tr>
			<td>序号</td>
			<td>主类别</td> 
			<td>监测数</td> 
			<td>总违法数</td> 
			<td>严重违法数</td> 
			<td>严重违法率</td> 
			<td>一般违法</td> 
			<td>监测违法率</td>
			</tr>';
        foreach($do_ie as $key => $val){
            $number = $key + 1;
            $ad_totals1 += $val['total'];
            $ad_wfggsls1 += $val['wfggsl'];
            $ad_yzwfggsls1 += $val['yzwfggsl'];
            $ad_ybwfggsls1 += $val['ybwfggsl'];
            $html .= '
				<tr> 
					<td>'.$number.'</td> 
					<td>'.$val['fadclass'].'</td> 
					<td>'.$val['total'].'</td> 
					<td>'.$val['wfggsl'].'</td> 
					<td>'.$val['yzwfggsl'].'</td> 
					<td>'.round($val['ggslyzwfl']*100,2).'%</td> 
					<td>'.$val['ybwfggsl'].'</td> 
					<td>'.round($val['ggslwfl']*100,2).'%</td>
				</tr>';
        }

        $num = $number+1;
        $html .= '<tr>';
        $html .= '<td>'.$num.'</td>';
        $html .= '<td>小计</td>';
        $html .= '<td>'.$ad_totals1.'</td>';
        $html .= '<td>'.$ad_wfggsls1.'</td>';
        $html .= '<td>'.$ad_yzwfggsls1.'</td>';
        $html .= '<td>'.round(($ad_yzwfggsls1/$ad_totals1)*100,2).'%</td>';
        $html .= '<td>'.$ad_ybwfggsls1.'</td>';
        $html .= '<td>'.round(($ad_wfggsls1/$ad_totals1)*100,2).'%</td></tr>';


        $html .= '</table>';
        $html .='<h3>二、广告监测警示</h3>';

        foreach ($do_ie as $val){
            $ages[] = $val['wfggsl'];
        }
        array_multisort($ages, SORT_DESC, $do_ie);//按照违法数降序
        $ll_wfggsl = $do_ie[0]['wfggsl']+$do_ie[1]['wfggsl'];

        //本、上月市省报刊媒体违法率总和
        $npa_ggslwfl = $npa_list[0]['ggslwfl'] + $npa_list1[0]['ggslwfl'];
        $lpa_ggslwfl = $lpa_list[0]['ggslwfl'] + $lpa_list1[0]['ggslwfl'];
        $pa_wfl = $npa_ggslwfl - $lpa_ggslwfl;
        if($pa_wfl==0){
            $pa_wfl = '无变化';
        }elseif($pa_wfl > 0){
            $pa_wfl = '上升'.$pa_wfl.'个百分点';
        }else{
            $pa_wfl = '下降'.$pa_wfl.'个百分点';
        }
        //电视
        $ntv_ggslwfl = $ntv_list[0]['ggslwfl'] + $ntv_list1[0]['ggslwfl'];
        $ltv_ggslwfl = $ltv_list[0]['ggslwfl'] + $ltv_list1[0]['ggslwfl'];
        $tv_wfl = $ntv_ggslwfl - $ltv_ggslwfl;

        if($tv_wfl==0){
            $tv_wfl = '无变化';
        }elseif($tv_wfl > 0){
            $tv_wfl = '上升'.$tv_wfl.'个百分点';
        }else{
            $tv_wfl = '下降'.$tv_wfl.'个百分点';
        }
        //广播
        $nbc_ggslwfl = $nbc_list[0]['ggslwfl'] + $nbc_list1[0]['ggslwfl'];
        $lbc_ggslwfl = $lbc_list[0]['ggslwfl'] + $lbc_list1[0]['ggslwfl'];
        $bc_wfl = $nbc_ggslwfl - $lbc_ggslwfl;

        if($bc_wfl==0){
            $bc_wfl = '无变化';
        }elseif($bc_wfl > 0){
            $bc_wfl = '上升'.$bc_wfl.'个百分点';
        }else{
            $bc_wfl = '下降'.$bc_wfl.'个百分点';
        }

        $html .='<p>（一）本月广告监测违法率</p>
				<p>本月，市省媒体广告监测违法率同比'.$tb_illegality_str.'，环比'.$hb_illegality_str.'，其中报刊'.$pa_wfl.'、电视'.$tv_wfl.'、广播'.$bc_wfl.'、本月违法广告类别主要集中在'.$do_ie[0]['fadclass'].'和'.$do_ie[1]['fadclass'].'两个行业，累计涉嫌违法'.$ll_wfggsl.'条次<p>';


        foreach ($do_ie as $val){
            $ages1[] = $val['ggslwfl'];
        }
        array_multisort($ages1, SORT_DESC, $do_ie);//按照违法率数降序
        // dump($do_ie);die;
        $html .='<p>（二）'.$do_ie[0]['fadclass'].'广告违法情况较严重</p>
				<p>本月共监测'.$do_ie[0]['fadclass'].'广告'.$do_ie[0]['total'].'条次，其中涉嫌违法广告'.$do_ie[0]['wfggsl'].'条次，监测违法率为'.round($do_ie[0]['ggslwfl']*100,2).'%，高居各行业榜首。</p>';

        $html .='<h3>三、典型违法广告案例</h3>';


        $map = ' where bs.fillegaltypecode=30';
        $map .= ' and bc.fmediaid in ('.join(',',$regionids).')';
        $array = ['一','二','三','四','五'];
        //电视类信息
        $sql = 'select 
                count(*) as total,
                bc.fid,
                bc.fmediaid,
                bc.fsampleid,
                bc.fsampleid,
                bs.fillegaltypecode,
                bs.fexpressions,
                bs.fadid,
                ad.fadname,
                ad.fbrand,
                 (case when instr(m.fmedianame,"（") > 0 then left(m.fmedianame,instr(m.fmedianame,"（") -1) else m.fmedianame end) as fmedianame,
                dc.ffullname as fadclass
                from ttvissue_'.$now_date.' as bc 
                join ttvsample as bs on bc.fsampleid=bs.fid 
                join tad ad on bs.fadid=ad.fadid and ad.fadid<>0 
                join tmedia as m on bc.fmediaid=m.fid 
                join tadclass dc on ad.fadclasscode=dc.fcode'.$map.' group by bs.fadid order by total desc limit 5';

        $result = M()->query($sql);//获取统计结果

        if($result){
            $html .='<h3>电视</h3>';
            foreach($result as $k => $val){
                $html.='<p>('.$array[$k].')、'.$val['fadname'].'  ('.$val['fadclass'].')</p>';
                $html.='<p>发布媒体：'.$val['fmedianame'].'</p>';
                $html.='<p>违法表现：“'.$val['fexpressions'].'”</p>';
                $html.='<p>违法次数：该广告累计违法'.$val['total'].'条次</p>';
            }
        }


        //广播类信息
        $sql = 'select 
                count(*) as total,
                bc.fid,
                bc.fmediaid,
                bc.fsampleid,
                bc.fsampleid,
                bs.fillegaltypecode,
                bs.fexpressions,
                bs.fadid,
                ad.fadname,
                ad.fbrand,
                (case when instr(m.fmedianame,"（") > 0 then left(m.fmedianame,instr(m.fmedianame,"（") -1) else m.fmedianame end) as fmedianame,
                dc.ffullname as fadclass
                from tbcissue_'.$now_date.' as bc 
                join tbcsample as bs on bc.fsampleid=bs.fid 
                join tad ad on bs.fadid=ad.fadid and ad.fadid<>0 
                join tmedia as m on bc.fmediaid=m.fid 
                join tadclass dc on ad.fadclasscode=dc.fcode'.$map.' group by bs.fadid order by total desc limit 5';

        $result = M()->query($sql);//获取统计结果
        if($result){
            $html .='<h3>广播</h3>';
            foreach($result as $k => $val){
                $html.='<p>('.$array[$k].')、'.$val['fadname'].'  ('.$val['fadclass'].')</p>';
                $html.='<p>发布媒体：'.$val['fmedianame'].'</p>';
                $html.='<p>违法表现：“'.$val['fexpressions'].'”</p>';
                $html.='<p>违法次数：该广告累计违法'.$val['total'].'条次</p>';
            }
        }

        //报纸类信息
        $map.= ' and date_format(bc.`fissuedate`, "%Y-%m")='.'"'.$date.'"';

        $sql = 'select count(*) as total,
                bc.fpaperissueid as fid,
                bc.fmediaid,
                bc.fpapersampleid as fsampleid,
                bs.fjpgfilename,
                bs.fpapersampleid,
                bs.fillegaltypecode,
                bs.fexpressions,
                bs.fadid,
                ad.fadname,
                ad.fbrand,
                (case when instr(m.fmedianame,"（") > 0 then left(m.fmedianame,instr(m.fmedianame,"（") -1) else m.fmedianame end) as fmedianame,
                dc.ffullname as fadclass
                from tpaperissue as bc 
                join tpapersample as bs on bc.fpapersampleid = bs.fpapersampleid 
                join tad ad on bs.fadid=ad.fadid and ad.fadid<>0 
                join tmedia as m on bc.fmediaid=m.fid 
                join tadclass dc on ad.fadclasscode=dc.fcode'.$map.' group by bs.fadid order by total desc limit 5';

        $result = M()->query($sql);//获取统计结果

        if($result){
            $html .='<h3>报纸</h3>';
            foreach($result as $k => $val){
                $html.='<p>('.$array[$k].')、'.$val['fadname'].'  ('.$val['fadclass'].')</p>';
                $html.='<p>发布媒体：'.$val['fmedianame'].'</p>';
                $html.='<p>违法表现：“'.$val['fexpressions'].'”</p>';
                $html.='<p>违法次数：该广告累计违法'.$val['total'].'条次</p>';
            }
        }

        $html .= '<h3>四、广告监测情况统计表</h3>';
        $html .= '<p>前十位涉嫌违法广告排名表</p>';


        $sql = 'SELECT 
                SUM(b.wfggsl) AS wfggsl,
                SUM(b.yzwfggsl) AS yzwfggsl,
                (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,
                (SUM(b.wfggsl) + SUM(b.yzwfggsl)) AS all_count,
                b.aofname,
                b.fadname
                FROM (SELECT
                        sum(
                        CASE
                        WHEN bs.fillegaltypecode = 20 THEN
                            1
                        ELSE
                            0
                        END
                    ) AS wfggsl,
                        sum(
                        CASE
                        WHEN bs.fillegaltypecode = 30 THEN
                            1
                        ELSE
                            0
                        END
                    ) AS yzwfggsl,
                    tb.fid,
                    tb.fmediaid,
                    bs.fid AS bsfid,
                    a.fadid,
                    a.fadname,
                    ao.fname AS aofname
                FROM
                    tbcissue_'.$now_date.' AS tb
                JOIN tbcsample AS bs ON tb.fsampleid = bs.fid
                JOIN tad a ON bs.fadid = a.fadid and a.fadid<>0
                JOIN tadowner ao ON a.fadowner = ao.fid
                WHERE
                    bs.fillegaltypecode > 0
                AND tb.fmediaid IN ('.join(',',$regionids).')
                GROUP BY
                    fadid
                UNION ALL
                    SELECT
                        sum(
                        CASE
                        WHEN bs.fillegaltypecode = 20 THEN
                            1
                        ELSE
                            0
                        END
                    ) AS wfggsl,
                        sum(
                        CASE
                        WHEN bs.fillegaltypecode = 30 THEN
                            1
                        ELSE
                            0
                        END
                    ) AS yzwfggsl,
                        tb.fid,
                        tb.fmediaid,
                        bs.fid AS bsfid,
                        a.fadid,
                        a.fadname,
                        ao.fname AS aofname
                    FROM
                        ttvissue_'.$now_date.' AS tb
                    JOIN tbcsample AS bs ON tb.fsampleid = bs.fid
                    JOIN tad a ON bs.fadid = a.fadid and a.fadid<>0
                    JOIN tadowner ao ON a.fadowner = ao.fid
                    WHERE
                        bs.fillegaltypecode > 0
                    AND tb.fmediaid IN ('.join(',',$regionids).')
                    GROUP BY
                        fadid
                    UNION ALL
                        SELECT
                        sum(
                        CASE
                        WHEN bs.fillegaltypecode = 20 THEN
                            1
                        ELSE
                            0
                        END
                    ) AS wfggsl,
                        sum(
                        CASE
                        WHEN bs.fillegaltypecode = 30 THEN
                            1
                        ELSE
                            0
                        END
                    ) AS yzwfggsl,
                            tb.fpaperissueid AS fid,
                            tb.fmediaid,
                            bs.fid AS bsfid,
                            a.fadid,
                            a.fadname,
                            ao.fname AS aofname
                        FROM
                            tpaperissue AS tb
                        JOIN tbcsample AS bs ON tb.fpapersampleid = bs.fid
                        JOIN tad a ON bs.fadid = a.fadid and a.fadid<>0
                        JOIN tadowner ao ON a.fadowner = ao.fid
                        WHERE
                        bs.fillegaltypecode > 0
                        AND date_format(tb.`fissuedate`, "%Y-%m") = '.'"'.$date.'"'.'
                        AND tb.fmediaid IN ('.join(',',$regionids).')
                        GROUP BY
                            fadid) as b JOIN tmedia as tm ON tm.fid = b.fmediaid
                GROUP BY
                            fadid ORDER BY wfggsl DESC LIMIT 10';

        $result1 = M()->query($sql);//获取广告主监测信息
        $html .= ' 
			<table style="width:100%;border:1px solid #666666; margin-top:20px;" border="1" align="center" class="tb"> 
			<tr>
			<td>序号</td>
			<td>广告名称</td> 
			<td>广告主</td> 
			<td>媒体名称</td> 
			<td>一般违法数量</td> 
			<td>严重违法数量</td>
			<td>合计</td>
			</tr>';

        foreach($result1 as $key => $val){
            $num = $key + 1;
            $html .= '
			<tr> 
				<td>'.$num.'</td> 
				<td>'.$val['fadname'].'</td>
				<td>'.$val['aofname'].'</td>
				<td>'.$val['fmedianame'].'</td>
				<td>'.$val['wfggsl'].'</td>
				<td>'.$val['yzwfggsl'].'</td>
				<td>'.$val['all_count'].'</td>
			</tr>';
        }

        $html .= '</table>';
        $html .= '</div>';

        $month_date = $date;
        $date = date('Ymdhis');
        $savefilename = $date.'.doc';
        $savefile = './Public/Word/'.$date.'.doc';

        //将文档保存
        word_start();
        echo $html;
        word_save($savefile);
        ob_flush();//每次执行前刷新缓存
        flush();

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.doc',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

        $system_num = getconfig('system_num');

        //将生成记录保存
        $pnname = $regulatorpname.date('Y年m月',strtotime($month_date)).'月报';
        $data['pnname'] 			= $pnname;
        $data['pntype'] 			= 30;
        $data['pnfiletype'] 		= 10;
        $data['pnstarttime'] 		= date('Y-m-1',strtotime($month_date));
        $data['pncreatetime'] 		= date('Y-m-d H:i:s');
        $data['pnurl'] 				= $ret['url'];
        $data['pnhtml'] 			= $html;
        $data['pnfocus']            = $focus;
        $data['pntrepid']           = $regulatorpersonInfo['fregulatorpid'];
        $data['pncreatepersonid']   = $regulatorpersonInfo['fid'];
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
        }
    }

    public function is_have_table($table_name){
        $isTable = M()->query('SHOW TABLES LIKE "'.$table_name.'"');
        if(empty($isTable)){
            return true;
        }else{
            return false;
        }
    }

    public function create_NJmonth(){

        set_time_limit(0);

        $regulatorpersonInfo = M('tregulatorperson')->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
        $regulatorInfo = M('tregulator')->where(array('fcode'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息
        $regulatorRegionInfo = M('tregion')->field('fid,fname')->where(array('fid'=>$regulatorInfo['fregionid']))->find();//监管地区信息信息
        session_write_close();//停止使用session

        $report_name = I('report_name','测试05');//报告名称
        $report_start_date = I('report_start_date','2018-05-01');//开始时间
        $report_end_date = I('report_end_date','2018-05-31');//结束时间

        $start_date = date('Y年m月d日',strtotime($report_start_date));//监测开始时间
        $end_date = date('Y年m月d日',strtotime($report_end_date));//监测结束时间
        $year_month =  date('Y年m月',strtotime($report_start_date));
        $start_month_day = date('m月d日',strtotime($report_start_date));
        $end_month_day = date('m月d日',strtotime($report_end_date));
        $days = (strtotime($report_end_date) - strtotime($report_start_date))/86400 + 1;

        //按行政级别区分
        $regionid = $regulatorRegionInfo['fid'];
        if($regionid == 100000) $regionid = '000000';
        $region_id_rtrim = rtrim($regionid,'00');//去掉地区后面的00
        $tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位
        if($tregion_len == 0){
            $area_name = '全国';
            $jur_area = '省';
        }elseif($tregion_len == 2) {
            $area_name = '全省';
            $jur_area = '市';
        }elseif($tregion_len == 4){
            $area_name = '全市';
            $jur_area = '县';
        }else{
            $area_name = '';
            $jur_area = '';
        }

        //名字为空自动生成名字
        if(empty($report_name)){
            $report_name = $regulatorInfo['fname'].' '. $start_date.'至'.$end_date.' 监测报告';
        }
        $a_data['report_name'] 	= $report_name;//报告名称
        $a_data['report_type'] 	= 99;//报告类型
        $a_data['fcreator'] 	= $regulatorpersonInfo['fname'];//创建人
        $a_data['fcreatetime'] 	= date('Y-m-d H:i:s');//创建时间
        $a_data['fmodifier'] 	= $regulatorpersonInfo['fname'];//修改人
        $a_data['fmodifytime'] 	= date('Y-m-d H:i:s');//修改时间
        $a_data['regulator_id'] = $regulatorInfo['fid'];//监管机构的ID
/*        $report_id = M('monitor_report')->add($a_data);

        $this->ajaxReturn(array('code'=>0,'msg'=>'已提交生成报告,请稍后查看'),'',0,'CC');*/

        $startDate = str_replace(array('年','月','日'),array('-','-',''),$start_date);
        $endDate = str_replace(array('年','月','日'),array('-','-',''),$end_date);
        //获取指定时间内的发布统计
        $issue_count = A('Gongshang/ReportMake','Model')->region_issue($regulatorRegionInfo['fid'],$startDate,$endDate);
        $adclass 	 = A('Gongshang/ReportMake','Model')->count_adclass($regulatorRegionInfo['fid'],$startDate,$endDate);
        $adcaseInfo  = A('Gongshang/ReportMake','Model')->getAdcase($regulatorInfo['fid'],$startDate,$endDate);
        $mediatype       = $issue_count['mediatype'];
        $tv_mediatype    = $issue_count['tv_mediatype'];
        $bc_mediatype    = $issue_count['bc_mediatype'];
        $paper_mediatype = $issue_count['paper_mediatype'];
        $issue       = $issue_count['issue'];
        $tv_issue 	 = $issue_count['tv'];
        $bc_issue 	 = $issue_count['bc'];
        $paper_issue = $issue_count['paper'];
        $adcase = $adcaseInfo['adcase'];
        $media_count = $issue_count['media_count'];//媒介数量

        //报告内容
        $report_data['summary']['reportfilename'] = 'C:\Users\Administrator\Desktop\1.docx';//保存路径
        $report_data['summary']['reportname'] = '关于'.$year_month.'广告监测情况的通报';
        $report_data['summary']['department'] = $regulatorInfo['fname'];
        $report_data['summary']['printdate'] = date('Y年m月d日');
        $report_data['summary']['sendto'] = $area_name.'市场监督管理部门：';
        $report_data['summary']['text'] = $year_month.'，'.$regulatorRegionInfo['fname'].'市场监督管理局广告监测中心对'.$area_name.'所属的媒介广告发布情况进行了监测，现将有关情况通报如下：';

        $report_data['part1']['title'] = '一、监测范围及对象';
        $report_data['part1']['part1.1']['title'] = '（一）传统媒体广告监测';
        $report_data['part1']['part1.1']['text'] = $year_month.'，共对'.$area_name.'所属的'.$media_count.'家电视、广播、报纸等传统媒体广告发布情况进行了监测，广告监测范围为'.$start_month_day.'-'.$end_month_day.'（共'.$days.'天）';
        $report_data['part1']['part1.2']['title'] = '（二）互联网广告监测';
        $report_data['part1']['part1.2']['text'] = '由国家互联网广告监测中心提供相关数据。';

        $report_data['part2']['title'] = '二、监测总体情况';
        $report_data['part2']['text1'] = $year_month.'，共监测到电视、广播、报纸的各类广告'.$mediatype['total'].'条次，涉嫌严重违法广告'.$mediatype['illegal'].'条次，条次违法率为'.$mediatype['percent'].'。';
        $report_data['part2']['table']['title'] = '表1各类媒体广告监测统计情况';
        $report_data['part2']['table']['list'] = array($tv_mediatype,$bc_mediatype,$paper_mediatype,$mediatype);
        $report_data['part2']['text2'] = '涉嫌严重违法广告类别主要集中在'.$adclass[0]['name'].'、'.$adclass[1]['name'].'、'.$adclass[2]['name'].'、'.$adclass[3]['name'].'、'.$adclass[4]['name'].'，其中'.$adclass[0]['name'].'广告占涉嫌严重违法广告总量的'.$adclass[0]['ratio'].'、'.$adclass[1]['name'].'广告占'.$adclass[1]['ratio'].'、'.$adclass[2]['name'].'广告占'.$adclass[2]['ratio'].'、'.$adclass[3]['name'].'广告占'.$adclass[3]['ratio'].'、'.$adclass[4]['name'].'广告占'.$adclass[4]['ratio'].'。';

        $report_data['part3']['title'] = '三、传统媒体广告监测情况';
        $report_data['part3']['part3.1']['title'] = '（一）区域广告市场秩序情况';
        $report_data['part3']['part3.1']['part3.1.1']['title'] = '1.广告市场秩序总体情况';
        $region_str = A('Gongshang/ReportMake','Model')->getRegionStr($issue);
        $report_data['part3']['part3.1']['part3.1.1']['text'] = '监测数据显示，'.$year_month.'，发布涉嫌严重违法广告条次违法率最为严重的'.$region_str['region_count'].'个'.$jur_area.'依次为：'.$region_str['str'].'。';
        $report_data['part3']['part3.1']['part3.1.1']['table']['title'] = '表2 发布涉嫌严重违法广告情况';
        $report_data['part3']['part3.1']['part3.1.1']['table']['title1'] = '（注：按发布涉嫌严重违法广告情况严重程度从高到低排列）';
        $report_data['part3']['part3.1']['part3.1.1']['table']['list'] = $issue;
        $report_data['part3']['part3.1']['part3.1.2']['title'] = '2.各'.$jur_area.'电视发布涉嫌严重违法广告情况';
        $tv_region_str = A('Gongshang/ReportMake','Model')->getRegionStr($tv_issue);
        $report_data['part3']['part3.1']['part3.1.2']['text'] = $year_month.'，电视发布涉嫌严重违法广告条次违法率最为严重的'.$tv_region_str['region_count'].'个地区依次为：'.$tv_region_str['str'].'。';
        $report_data['part3']['part3.1']['part3.1.2']['table']['title'] = '表3 电视发布涉嫌严重违法广告情况';
        $report_data['part3']['part3.1']['part3.1.2']['table']['title1'] = '（注：按发布涉嫌严重违法广告情况严重程度从高到低排列）';
        $report_data['part3']['part3.1']['part3.1.2']['table']['list'] = $tv_issue;
        $report_data['part3']['part3.1']['part3.1.3']['title'] = '3.各'.$jur_area.'广播发布涉嫌严重违法广告情况';
        $bc_region_str = A('Gongshang/ReportMake','Model')->getRegionStr($bc_issue);
        $report_data['part3']['part3.1']['part3.1.3']['text'] = $year_month.'，广播发布涉嫌严重违法广告条次违法率最为严重的'.$bc_region_str['region_count'].'个地区依次为：'.$bc_region_str['str'].'。';
        $report_data['part3']['part3.1']['part3.1.3']['table']['title'] = '表4 广播发布涉嫌严重违法广告情况';
        $report_data['part3']['part3.1']['part3.1.3']['table']['title1'] = '（注：按发布涉嫌严重违法广告情况严重程度从高到低排列）';
        $report_data['part3']['part3.1']['part3.1.3']['table']['list'] = $bc_issue;
        $report_data['part3']['part3.1']['part3.1.4']['title'] = '4.各'.$jur_area.'报纸发布涉嫌严重违法广告情况';
        $paper_region_str = A('Gongshang/ReportMake','Model')->getRegionStr($paper_issue);
        $report_data['part3']['part3.1']['part3.1.4']['text'] = $year_month.'，报纸发布涉嫌严重违法广告条次违法率最为严重的'.$paper_region_str['region_count'].'个地区依次为：'.$paper_region_str['str'].'。';
        $report_data['part3']['part3.1']['part3.1.4']['table']['title'] = '表5 报纸发布涉嫌严重违法广告情况';
        $report_data['part3']['part3.1']['part3.1.4']['table']['title1'] = '（注：按发布涉嫌严重违法广告情况严重程度从高到低排列）';
        $report_data['part3']['part3.1']['part3.1.4']['table']['list'] = $paper_issue;

        $report_data['part4']['title'] = '四、互联网广告监测情况';
        $report_data['part4']['text'] = '由国家互联网广告监测中心提供相关数据！';

        $report_data['part5']['title'] = '五、'.$area_name.'广告监管效能';
        $report_data['part5']['text'] = $year_month.'，'.$regulatorInfo['fname'].'广告监管系统共派发涉嫌严重违法广告线索'.$adcaseInfo['adcase_sum'].'条，截止'.$end_month_day.'，'.$jur_area.'单位共查看'.$adcaseInfo['adcase_check_sum'].'条，查看率为'.$adcaseInfo['adcase_check_ratio'].'，停播'.$adcaseInfo['adcase_xstb'].'条，停播率为'.$adcaseInfo['adcase_xstb_ratio'].'；立案调查'.$adcaseInfo['adcase_xsla'].'条，立案率为'.$adcaseInfo['adcase_xsla_ratio'].'。';
        $report_data['part5']['table']['title'] = '表14 涉嫌严重违法广告线索处理情况统计表（截止'.$end_month_day.'）';
        $report_data['part5']['table']['title1'] = '（注：按发布涉嫌严重违法广告线索处理率从低到高排列）';
        $report_data['part5']['table']['list'] = $adcase;

        $report_data = json_encode($report_data);
        $reporttype = 'sichuanreport';

        dump($report_data);exit;
        $rr = http(C('REPORT_MAKE_URL'),'reporttype='.$reporttype.'&reportparam='.$report_data,'POST',false,6000);

        // var_dump($rr);
        $rr = json_decode($rr,true);
        $e_data = array();
        if($rr['code'] === 0){
            $e_data['report_url'] = $rr['ReportFileName'];//报告URL
            $e_data['make_state'] = 1;//生成成功
        }else{
            $e_data['make_state'] = 2;//生成失败
        }
        M('monitor_report')->where(array('report_id'=>$report_id))->save($e_data);//修改数据库

    }

    public function pxsf($data = [],$score_rule = 'com_score'){
        $data_count = count($data);
        for($k=0;$k<=$data_count;$k++)
        {
            for($j=$data_count-1;$j>$k;$j--){
                if($data[$j][$score_rule]>$data[$j-1][$score_rule]){
                    $temp = $data[$j];
                    $data[$j] = $data[$j-1];
                    $data[$j-1] = $temp;
                }
            }
        }
        return $data;
    }

    public function get_table_data($data){
        $data_array = [];
        foreach ($data as $data_val){
            if(isset($data_val['fissue_date'])){
                $issue_date_arr = [];
                $issue_dates = explode(',',$data_val['fissue_date']);
                foreach ($issue_dates as $issue_date){
                    $issue_date_arr[] = strtotime($issue_date);
                }
                sort($issue_date_arr);
                $fissue_date = [];
                foreach ($issue_date_arr as $v){
                    $fissue_date[] = date("Y-m-d",$v);
                }
                $data_val['fissue_date'] = implode($fissue_date,',');
            }
            $data_array[] = array_values($data_val);

        }
        return $data_array;

    }


}