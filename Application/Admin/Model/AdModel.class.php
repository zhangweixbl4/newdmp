<?php
namespace Admin\Model;
class AdModel{
	// 引用fadid的表名及主键
	public $relatedTables = [
		'ttvsample'    => 'fid',
		'tbcsample'    => 'fid',
		'tpapersample' => 'fpapersampleid',
		'tnetissue'    => 'major_key',
		'task_input'   => 'taskid',
		'tnettask'     => 'taskid'
	];
	// 广告主题修改原因
	protected $editAdReason = [
		'10'=>[
			'11'=>'广告名称',
			'12'=>'品牌',
			'13'=>'类别',
			'14'=>'商业类别',
			'15'=>'广告主',
		],
		'20'=>[
			'21'=>'格式错误',
			'22'=>'不符合规则',
			'23'=>'错别字',
			'24'=>'与库中主题不统一',
		],
	];
	/**
	 * @Des: 编辑广告主题
	 * @Edt: yuhou.wang
	 * @Date: 2019-12-29 03:23:34
	 * @param {Array} $info 更新数据
	 * @return: {Array} $data 影响结果
	 */
    public function editAd($info){
		if(empty($info['fadname'])){
			$data = ['code'=>-1,'msg'=>'缺少必要参数'];
			return $data;
		}else{
			$userId = session('personInfo.fid');
			$userName = session('personInfo.fname');
			$sameNameList = M('tad')
				->alias('a')
				->field('a.fadid,a.fadname,a.fadowner,b.fname fadowner_name,a.fadclasscode,c.ffullname,a.fadclasscode_v2,d.ffullname fadclasscode_v2name,a.fstate,a.fcreator,a.fcreatetime,a.fmodifier,a.fmodifytime')
				->join('tadowner b on b.fid=a.fadowner','LEFT')
				->join('tadclass c ON c.fcode=a.fadclasscode','LEFT')
				->join('hz_ad_class d ON d.fcode=a.fadclasscode_v2','LEFT')
				->where(['a.fadid'=>['NEQ',$info['fadid']],'a.fadname'=>$info['fadname'],'a.fstate'=>['GT',0]])
				->order('a.fadowner DESC,a.fbrand DESC,a.fadclasscode DESC,a.fadclasscode_v2 DESC,a.fcreatetime ASC')
				->select();
			$sql = M('tad')->getLastSql();
			if(count($sameNameList) >= 1){
				$sameNameList[] = [
					'fadid'               => $info['fadid'],
					'fadname'             => $info['fadname'],
					'fadowner'            => $info['fadowner'],
					'fadowner_name'       => $info['fadowner_name'],
					'fadclasscode'        => $info['fadclasscode'],
					'ffullname'           => $info['adclass_fullname'],
					'fadclasscode_v2'     => $info['fadclasscode_v2'],
					'fadclasscode_v2name' => $info['adclass_v2_fullname'],
					'fstate'              => $info['fstate'],
					'fcreator'            => $info['fcreator'],
					'fcreatetime'         => $info['fcreatetime'],
					'fmodifier'           => $info['fmodifier'],
					'fmodifytime'         => $info['fmodifytime'],
				];
				if(empty($info['mainfadid'])){
					$data = ['code'=>2,'msg'=>'存在同名主题，请确认合并目标','data'=>$sameNameList];
					return $data;
				}else{
					// 同名自动合并,创建合并任务
					foreach($sameNameList as $row){
						$mergefids[] = $row['fadid'];
					}
					$ret = $this->mergeAd(implode(',',$mergefids),$info['mainfadid'],'修改主题同名自动合并');
				}
			}
			$info['fadowner'] = A('Common/Ad','Model')->getAdOwnerId($info['adowner_name'], $userName);
			$beforeInfo = M('tad')->find($info['fadid']);
			$before = json_encode($beforeInfo);
			// 更新主题
			$saveInfo = [
				'fadname'            => $info['fadname'],
				'fadowner'           => $info['fadowner'],
				'fbrand'             => $info['fbrand'],
				'fadclasscode'       => $info['fadclasscode'],
				'fadclasscode_v2'    => $info['fadclasscode_v2'],
				'is_sure'            => $info['is_sure'],
				'fmodifier_id'		 => $userId,
				'fmodifier'          => $userName,
				'fmodifytime'        => date('Y-m-d H:i:s'),
				'to_opensearch_time' => 0,
			];
			if($beforeInfo['fstate'] == 2 || $beforeInfo['fstate'] == 9){
				$saveInfo['fstate'] = 1;
			}
			$where = ['fadid'=>$info['fadid']];
			$res = M('tad')->where($where)->save($saveInfo);
			// 记录
			$logData = [
				'old' => $before,
				'new' => $info
			];
			if ($res !== false){
				$freason = $info['freason'];
				$after = json_encode(M('tad')->find($info['fadid']));
				$recordData = [
					'fadid'       => $info['fadid'],
					'wx_id'       => $userId,
					'before'      => $before,
					'after'       => $after,
					'update_time' => time(),
					'user_name'   => $userName,
					'log'         => $freason,
					'type'		  => 2,
				];
				M('ad_record')->add($recordData);
				if($beforeInfo['fstate'] == 2 || $beforeInfo['fstate'] == 9){
					// 若为2待确认状态，则记录“主题确认”记录
					$recordData = [
						'fadid'       => $info['fadid'],
						'wx_id'       => $userId,
						'before'      => $before,
						'after'       => $after,
						'update_time' => time(),
						'user_name'   => $userName,
						'log'         => '主题确认',
						'type'		  => 3,
					];
					M('ad_record')->add($recordData);
				}
				$logData['msg'] = '修改成功';
				$data = ['code'=>0,'msg'=>'修改成功'.$res.'条广告主题','data'=>$info];
			}else{
				$logData['msg'] = '修改失败';
				$data = ['code'=>-1,'msg'=>'修改失败'.$res.'条广告主题','data'=>$info];
			}
			A('Common/AliyunLog','Model')->ad_log($logData);
			return $data;
		}
    }
	/**
	 * @Des: 编辑广告主题_v2
	 * @Edt: yuhou.wang
	 * @Date: 2019-12-29 03:23:34
	 * @param {Array} $info 更新字段及数据
	 * @param {String|Array} $where 更新条件(主键或条件数组)
	 * @return: {Array} $data 影响结果
	 */
    public function editAd_v2($info,$where){
        $userName = session('personInfo.fname');
		$info['fadowner'] = A('Common/Ad','Model')->getAdOwnerId($info['adowner_name'], $userName);
		$before = json_encode(M('tad')->find($info['fadid']));
		// 更新主题
		$saveInfo = [
			'fadname'            => $info['fadname'],
			'fbrand'             => $info['fbrand'],
			'fadowner'           => $info['fadowner'],
			'fadclasscode'       => $info['fadclasscode'],
			'fadclasscode_v2'    => $info['fadclasscode_v2'],
			'is_sure'            => $info['is_sure'],
			'fstate'             => $info['fstate'],
			'fmodifier'          => $userName,
			'fmodifytime'        => date('Y-m-d H:i:s'),
			'to_opensearch_time' => 0,
		];
        if (!is_array($where)) {
            $where = ['fadid'=>$where];
        }
		$res = M('tad')->where($where)->save($saveInfo);
		// 记录
        $logData = [
            'old' => M('tad')->where($where)->select(),
            'new' => $info
        ];
        if ($res !== false){
			$freason = $info['freason'];
			$after = json_encode(M('tad')->find($info['fadid']));
			$recordData = [
				'fadid'       => $info['fadid'],
				'wx_id'       => 0,
				'before'      => $before,
				'after'       => $after,
				'update_time' => time(),
				'user_name'   => $userName,
				'log'         => $freason,
			];
            M('ad_record')->add($recordData);
            $logData['msg'] = '修改成功';
			$data = ['code'=>0,'msg'=>'修改成功'];
        }else{
			$logData['msg'] = '修改失败';
			$data = ['code'=>-1,'msg'=>'修改失败'];
        }
		A('Common/AliyunLog','Model')->ad_log($logData);
		return $data;
    }
	/**
	 * @Des: 合并广告主题-逻辑删除并记录日志
	 * @Edt: yuhou.wang
	 * @Date: 2019-12-29 03:23:34
	 * @param {String|Array} $fadids 需删除的fadid，支持数组或字符串(多个以半角逗号分隔) 
	 * @return: {Int} $data 删除条数
	 */
	public function delAd($fadids,$comment=''){
		$data = 0;
		// 逻辑删除
        $adRow = [
            'fstate'       => -1,
            'comment'      => $comment,
            'fmodifier_id' => session('personInfo.fid'),
            'fmodifier'    => session('personInfo.fname'),
            'fmodifytime'  => date('Y-m-d H:i:s'),
            'to_opensearch_time'=>0,
        ];
		$data = M('tad')->where(['fadid'=>['IN',$fadids]])->save($adRow);
		// 记录日志
		if($data){
			if(!is_array($fadids)) $fadids = explode(',',$fadids);
			foreach($fadids as $fadid){
                $log[] = [
                    'fadid'       => $fadid,
                    'wx_id'       => session('personInfo.fid'),
                    'update_time' => time(),
                    'user_name'   => session('personInfo.fname'),
					'log'         => $comment,
					'type'		  => -1,
                ];
			}
			M('ad_record')->addAll($log);
		}
		return $data;
	}
	/**
	 * @Des: 合并广告主题-隐藏显示
	 * @Edt: yuhou.wang
	 * @Date: 2019-12-29 03:23:34
	 * @param {String|Array} $fadids 需隐藏的fadid，支持数组或字符串(多个以半角逗号分隔) 
	 * @return: {Int} $data 删除条数
	 */
	public function hideAd($fadids,$comment=''){
		$data = 0;
		// 隐藏不显示
        $adRow = [
            'fshow'        => 0,
            'comment'      => $comment,
            'fmodifier_id' => session('personInfo.fid'),
            'fmodifier'    => session('personInfo.fname'),
            'fmodifytime'  => date('Y-m-d H:i:s'),
            'to_opensearch_time'=>0,
        ];
		$data = M('tad')->where(['fadid'=>['IN',$fadids]])->save($adRow);
		return $data;
	}
    /**
     * @Des: 合并广告主题
     * @Edt: yuhou.wang
     * @param {String} fadids 广告ID,多个以半角逗号分隔
     * @param {String} mainfadid 目标广告ID,以此广告为合并目标，其它将被删除
     * @return: {JSON}
     */
    public function mergeAd($fadids,$mainfadid,$log='广告主题合并'){
        if((!empty($fadids) || $fadids == 0) && (!empty($mainfadid) || $mainfadid == 0)){
            $mergefids = explode(',',$fadids);
            array_splice($mergefids,array_search($mainfadid,$mergefids),1);// 排除目标fadids
            // 校验目标主题是否待合并或正合并
            $mergeRecord = M('ad_record')->where(['source_fadid'=>['LIKE','%'.$mainfadid.'%'],'merge_state'=>['IN',[0,1]]])->find();
            if(empty($mergeRecord)){
                // 隐藏目标外其它广告记录
                $comment = '正在被合并至:'.$mainfadid;
                $retHide = A('Admin/Ad','Model')->hideAd($mergefids,$comment);
                // 删除OpenSearch广告主题联想，页面隐藏不显示已提交合并的主题
                $delOpenAdRet = ['success'=>0,'failure'=>0];
                foreach($mergefids as $key=>$fadid){
                    if($_SERVER['SERVER_NAME'] == 'dmp' || strpos($_SERVER['SERVER_NAME'],'debug') !== false){
                        // 避免测试环境删除正式OpenSearch数据
                        $ret = true;
                    }else{
                        $ret = A('Api/Ad','Model')->delOpenAd($fadid);
                    }
                    if($ret === true){
                        $delOpenAdRet['success']++;
                    }else{
                        $delOpenAdRet['failure']++;
                    }
                }
                // 创建更新引用异步处理任务
                if($delOpenAdRet['success'] == count($mergefids)){
                    // 全部删除成功
                    $task = [
                        'fadid'       => $mainfadid,
                        'wx_id'       => session('personInfo.fid'),
                        'update_time' => time(),
                        'user_name'   => session('personInfo.fname'),
						'type'        => 4,
                        'log'         => $log,
                        'source_fadid'=> implode(',',$mergefids),
                        'merge_state' => 0,
                    ];
                    M('ad_record')->add($task);
                    return ['code'=>0,'msg'=>'合并提交成功'];
                }else{
                    // 未全部删除成功，则回滚
                    return ['code'=>1,'msg'=>'操作不成功，请联系管理员','data'=>$delOpenAdRet];
                }
            }else{
                return ['code'=>1,'msg'=>'目标主题正被合并中，请稍候重试','data'=>$mergeRecord];
            }
        }else{
            return ['code'=>1,'msg'=>'缺少必要参数'];
        }
    }
	/**
	 * @Des: 合并广告主题-更新引用_V20200118
	 * @Edt: yuhou.wang
	 * @Date: 2019-12-29 02:57:36
	 * @param {Int} $targetId 目标ID
	 * @param {String|Array} $sourceIds 源ID，支持数组和字符串(多个以半角逗号分隔)
	 * @return: {Array} $data 更新的表名和影响行数
	 */
	public function updateRelated_V20200118($targetId,$sourceIds){
		$data = [];
		// 锁定合并任务为1-进行中
        M('ad_record')->master(true)->where(['fadid'=>$targetId,'$source_fadid'=>$sourceIds,'merge_state'=>0])->save(['merge_state'=>1]);
        // 更新所有对源fadid引用
        foreach($this->relatedTables as $table=>$key){
			$ret = 0;
			// 更新引用
			// $ret = M($table)->where(['fadid'=>['IN',$sourceIds]])->save(['fadid'=>$targetId]);
			$keys = M($table)->where(['fadid'=>['IN',$sourceIds]])->getField($key,true);
			if($keys) $ret = M($table)->where([$key=>['IN',$keys]])->save(['fadid'=>$targetId]);
			$data[] = [$table=>$ret];
		}
		// 更新合并任务为2-合并完成
        M('ad_record')->master(true)->where(['fadid'=>$targetId,'$source_fadid'=>$sourceIds,'merge_state'=>1])->save(['merge_state'=>2,'merge_time'=>date('Y-m-d H:i:s')]);
		// 删除目标外其它广告记录
		$comment = '已被合并至:'.$targetId;
		$retDel = $this->delAd($sourceIds,$comment);
		return $data;
	}
	/**
	 * @Des: 合并广告主题-更新引用
	 * @Edt: yuhou.wang
	 * @Date: 2019-12-29 02:57:36
	 * @param {Int} $id 合并任务ID
	 * @return: {Array} $data 更新的表名和影响行数
	 */
	public function updateRelated($id){
		$data = [];
        $taskInfo = M('ad_record')->master(true)->where(['id'=>$id])->find();
		// 锁定合并任务为1-进行中
		M('ad_record')->master(true)->where(['id'=>$id])->save(['merge_state'=>1]);
        // 更新所有对源fadid引用
        foreach($this->relatedTables as $table=>$key){
			$ret = 0;
			// 更新引用
			$keys = M($table)->where(['fadid'=>['IN',$taskInfo['source_fadid']]])->getField($key,true);
			if($keys) {
				$ret = M($table)->where([$key=>['IN',$keys]])->save(['fadid'=>$taskInfo['fadid']]);
			}
			$data[] = [$table=>$ret];
		}
		// 更新合并任务为2-合并完成
		M('ad_record')->master(true)->where(['id'=>$id])->save(['merge_state'=>2,'merge_time'=>date('Y-m-d H:i:s')]);
		// 删除目标外其它广告记录
		$comment = '已被合并至:'.$taskInfo['fadid'];
		$retDel = $this->delAd($taskInfo['source_fadid'],$comment);
		return $data;
	}
    /**
     * @Des: 合并广告主题-更新引用补丁
     * @Edt: yuhou.wang
     * @Date: 2019-12-29 05:33:41
     * @param {type} 
     * @return: 
     */
    public function mergeAdUpdateRelatedSp_V20200224(){
		$data = [];
		$sourceAds = M('tad')->field('fadid,SUBSTR(comment,7) targetId')->where(['fstate'=>-1,'comment'=>['LIKE','已被合并至:%']])->select();
		if(!empty($sourceAds)){
			// 更新所有已被合并的fadid引用表
			foreach($this->relatedTables as $table=>$key){
				// 更新引用
				$saveNum = 0;
				foreach($sourceAds as $ad){
					$ret = M($table)->where(['fadid'=>$ad['fadid']])->save(['fadid'=>$ad['targetId']]);
					if($ret) $saveNum++;
				}
				$data[] = [$table=>$saveNum];
			}
		}
		return $data;
	}
	/**
     * @Des: 合并广告主题-更新引用补丁
     * @Edt: yuhou.wang
     * @Date: 2020-02-24 16:28:18
     * @param {type} 
     * @return: 
     */
    public function mergeAdUpdateRelatedSp(){
		$data = [];
		// 更新所有已被合并的fadid引用表
		$comment = '已被合并至%';
		foreach($this->relatedTables as $table=>$key){
			$num = M()->query('update '.$table.' a,tad b SET a.fadid=SUBSTR(b.comment,7) WHERE a.fadid=b.fadid AND b.fstate=-1 AND b.comment LIKE '.$comment);
			// $ids = M($table)->alias('a')->join('tad b ON b.fadid=a.fadid')->where(['b.fstate'=>-1,'b.comment'=>['LIKE','已被合并至:%']])->getField($key,true);
			// $num = M($table)->alias('a')->join('tad b ON b.fadid=a.fadid')->where(['b.fstate'=>-1,'b.comment'=>['LIKE','已被合并至:%']])->save(['a.fadid'=>"SUBSTR('b.comment',7)"]);
			$data[] = [$table=>$num];
		}
		return ['code'=>0,'msg'=>'更新合并主题引用补丁完成','data'=>$data];
	}
	/**
	 * @Des: 获取修改广告原因清单
	 * @Edt: yuhou.wang
	 * @Date: 2020-01-13 14:06:13
	 * @param {type} 
	 * @return: 
	 */	
	public function getEditAdReason(){
		$data = $this->editAdReason;
		foreach($data['10'] as $k=>$v){
			foreach($data['20'] as $k2=>$v2){
				$data['tile'][$k.$k2] = $v.$v2;
			}
		}
		foreach($data['10'] as $k=>$v){
			$data['level'][$k]['fname'] = $v;
			$data['level'][$k]['children'][] = $data['20'];
		}
		return $data;
	}
    /**
     * @Des: 获取广告内容(广告主题)操作记录
     * @Edt: yuhou.wang
     * @param {type} $fadid 广告主题ID
     * @return: 
     */   
    public function getHistory($fadid){
		if($fadid || $fadid == 0){
			$dataSet = M('ad_record')->where(['fadid'=>$fadid])->select();
			foreach($dataSet as $key=>$row){
				$before = $row['before'] ? json_decode($row['before'],true) : [];
				$after = $row['after'] ? json_decode($row['after'],true) : [];
				$data[$key]['user_name'] = $row['user_name'];
				$data[$key]['update_time'] = date('Y-m-d H:i:s',$row['update_time']);
				$data[$key]['log'] = $row['log'];
				$data[$key]['before'] = [
					'fadname' => $before['fadname'],
					'fbrand' => $before['fbrand'],
					'adowner_name' => M('tadowner')->cache(true,60)->where(['fid'=>$before['fadowner']])->getField('fname'),
					'adclass_fullname' => M('tadclass')->cache(true,60)->where(['fcode'=>$before['fadclasscode']])->getField('ffullname'),
					'adclass_v2_fullname' => M('hz_ad_class')->cache(true,60)->where(['fcode'=>$before['fadclasscode_v2']])->getField('ffullname'),
					'fstate' => $before['fstate'],
				];
				$fadowner = $after['fadowner'] ? $after['fadowner'] : $before['fadowner'];
				$fadclasscode = $after['fadclasscode'] ? $after['fadclasscode'] : $before['fadclasscode'];
				$fadclasscode_v2 = $after['fadclasscode_v2'] ? $after['fadclasscode_v2'] : $before['fadclasscode_v2'];
				$data[$key]['after'] = [
					'fadname' => $after['fadname'] ? $after['fadname'] : $before['fadname'],
					'fbrand' => $after['fbrand'] ? $after['fbrand'] : $before['fbrand'],
					'adowner_name' => M('tadowner')->cache(true,60)->where(['fid'=>$fadowner])->getField('fname'),
					'adclass_fullname' => M('tadclass')->cache(true,60)->where(['fcode'=>$fadclasscode])->getField('ffullname'),
					'adclass_v2_fullname' => M('hz_ad_class')->cache(true,60)->where(['fcode'=>$fadclasscode_v2])->getField('ffullname'),
					'fstate' => $after['fstate'] ? $after['fstate'] : $before['fstate'],
				];
			}
			$ret = ['code'=>0,'msg'=>'获取成功','data'=>$data];
		}else{
			$ret = ['code'=>1,'msg'=>'缺少必要参数'];
		}
		return $ret;
	}
}