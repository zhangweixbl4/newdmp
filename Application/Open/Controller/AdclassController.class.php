<?php
namespace Open\Controller;
use Think\Controller;

class AdclassController extends BaseController {
	
	/*获取广告类别列表*/
    public function get_adclass_list(){
		$adclass_data = file_get_contents('php://input');//获取post数据
		$adclass_data = json_decode($adclass_data,true);//把json转化成数组
		if($adclass_data['model'] == 'get_all'){//判断是否获取全部
			$adclassList = M('tadclass')->field('')->where()->select();//查询地区列表
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取的是全部列表','adclassList'=>$adclassList));
		}
		$fcode = intval($adclass_data['fcode']);//广告分类code

		if($fcode == 0) $fcode = '';
		$adclassList = M('tadclass')->field('fcode,fadclass')->where(array('fpcode'=>$fcode))->select();//查询广告分类列表
		
		$adclassDetails = M('tadclass')->field('fcode,left(fadclass,4) as fadclass,ffullname')->where(array('fcode'=>$fcode))->find();//查询广告分类详情
		
		if(!$adclassDetails) $adclassDetails = array('fcode'=>'','fadclass'=>'全部','ffullname'=>'全部');
		$this->ajaxReturn(array('code'=>0,'msg'=>'','adclassList'=>$adclassList,'adclassDetails'=>$adclassDetails));
		
	}
	
	
	/*获取商业广告类别列表*/
    public function get_adclass_v2_list(){
		$adclass_data = file_get_contents('php://input');//获取post数据
		$adclass_data = json_decode($adclass_data,true);//把json转化成数组
		if($adclass_data['model'] == 'get_all'){//判断是否获取全部
			$adclassList = M('hz_ad_class')->field('')->where()->select();//查询地区列表
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取的是全部列表','adclassList'=>$adclassList));
		}
		$fcode = intval($adclass_data['fcode']);//广告分类code

		if($fcode == 0) $fcode = '';
		$adclassList = M('hz_ad_class')->field('fcode,fname')->where(array('fpcode'=>$fcode))->select();//查询广告分类列表
		
		$adclassDetails = M('hz_ad_class')->field('fcode,fname,ffullname')->where(array('fcode'=>$fcode))->find();//查询广告分类详情
		
		if(!$adclassDetails) $adclassDetails = array('fcode'=>'','fadclass'=>'全部','ffullname'=>'全部');
		$this->ajaxReturn(array('code'=>0,'msg'=>'','adclassList'=>$adclassList,'adclassDetails'=>$adclassDetails));
		
	}
	
	
	
	
	
	
}