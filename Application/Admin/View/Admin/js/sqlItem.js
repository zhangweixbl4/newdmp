let sqlItem = {
    template: `<div v-loading="loading">
    <el-form
            :model="sqlInfo"
            :rules="rules" 
            ref="sqlForm"
            label-width="100px"
            :disabled="type != 'input'"
    >
        <el-form-item label="类别" prop="type">
            <el-radio-group v-model="sqlInfo.type">
                <el-radio label="0">修改</el-radio>
                <el-radio label="1">新增</el-radio>
                <el-radio label="2">删除</el-radio>
				<el-radio label="9">其它</el-radio>
				
            </el-radio-group>
			
			
        </el-form-item>
		
		
		<el-form-item label="数据库标识" prop="db_flag">
            <el-radio-group v-model="sqlInfo.db_flag">
                <el-radio label="0">默认数据库(0)</el-radio>
                <el-radio label="1">中台业务库(1)</el-radio>
                <el-radio label="2">中台分析库(2)</el-radio>

				
            </el-radio-group>
			
        </el-form-item>
		
		
        <el-form-item label="说明" prop="reason">
            <el-input
                    type="textarea"
                    :rows="2"
                    placeholder="请输入说明"
                    :autosize="{ minRows: 2}"
                    v-model="sqlInfo.reason">
            </el-input>
        </el-form-item>
        <el-form-item label="SQL内容" prop="sql_content">
            <el-input
                    type="textarea"
                    :rows="2"
                    placeholder="请输入SQL内容"
                    :autosize="{ minRows: 2}"
                    v-model="sqlInfo.sql_content">
            </el-input>
        </el-form-item>
        <el-form-item>
            <div v-if="type == 'input'">
                <el-button type="primary" @click="submit">提交</el-button>
            </div>
        </el-form-item>
    </el-form>
       <div v-if="type == 'check' && sqlInfo.state == 0" style="margin-left: 100px;">
            <el-button type="info" @click="testSql">测试SQL影响行数(修改表结构语句请直接执行)</el-button>
            <el-button type="primary" @click="execSql">执行SQL</el-button>
            <el-button type="warning" @click="reject">驳回</el-button>
        </div>
</div>`,
    props: ['id'],
    data: function () {
        return{
            loading: true,
            sqlInfo: {},
            rules: {
                type: [
                    { required: true, message: '请选择类别', trigger: 'change' }
                ],
                reason: [
                    { required: true, message: '请输入说明', trigger: 'change' }
                ],
                sql_content: [
                    { required: true, message: '请输入SQL内容', trigger: 'change' }
                ],
				db_flag: [
                    { required: true, message: '请选择数据库', trigger: 'change' }
                ],
				
            },
        }
    },
    computed: {
        type: function () {
            if (this.id){
                return 'check'
            } else {
                return 'input'
            }
        }
    },
    methods:{
        dataInit: function (){
            this.loading = false
            this.sqlInfo = {}
            // hljs.initHighlightingOnLoad();
            if (this.type === 'input'){
                return false
            }
            this.getInfo()
        },
        getInfo: function () {
            this.loading = true
            $.post('getSqlInfo', {id: this.id}).then((res) => {
                this.loading = false
                this.sqlInfo = res.data
            })
        },
        submit: function () {
            this.$refs.sqlForm.validate((res) => {
                if (!res){
                    return false
                }
                this.$confirm('确认提交?').then(() => {
                    this.loading = true
                    $.post('addSqlItem', {
                        sqlInfo: this.sqlInfo,
                    }).then((res) => {
                        this.loading = false
                        if (res.code == 0){
                            this.$message.success(res.msg)
                        } else{
                            this.$message.error(res.msg)
                        }
                        this.$emit('back')
                    })
                })
            })
        },
        execSql: function () {
            this.$confirm('确认执行SQL?').then(() => {
                this.loading = true
                $.post('execSqlItem', {id: this.id}).then((res) => {
                    this.loading = false
                    if (res.code == 0){
                        this.$alert('受影响行数 '+ res.data)
                    } else{
                        this.$alert(res.msg, '错误')
                    }
                    this.$emit('back')
                })
            })
        },
        reject: function () {
            this.$confirm('确认退回SQL?').then(() => {
                this.loading = true
                $.post('rejectSqlItem', {id: this.id}).then((res) => {
                    this.loading = false
                    if (res.code == 0){
                        this.$message.success(res.msg)
                    } else{
                        this.$message.error(res.msg)
                    }
                    this.$emit('back')
                })
            })
        },
        testSql: function () {
            this.loading = true
            $.post('testAffectedRows', {id: this.id}).then((res) => {
                this.loading = false
                if (res.code == 0){
                    this.$alert('受影响行数 '+ res.data)
                } else{
                    this.$alert(res.msg, '错误')
                }
            })
        },
    }
}