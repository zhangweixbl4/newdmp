<?php
namespace Adinput\Model;



class InputTaskModel{


    /*获取广告ID*/
    public function get_ad_id($fadname,$fbrand,$fadclasscode,$adowner_name,$fadclasscode_v2='3203',$type=1){
		if(in_array(MODULE_NAME,array('Adinput'))){
			$user_info = get_user_info();//微信用户
		}
		$person_info = session('personInfo');//后台用户
		$creator = $user_info['nickname'];//微信用户
		if($creator == '' ) $creator = $person_info['fname'];//后台用户
		
		if($creator == '' ){
			$wx_id = session('wx_id');
			$user_info = M('ad_input_user')->where(array('wx_id'=>$wx_id))->find();
			$creator = $user_info['nickname'];
		}
		if($creator == '' ) $creator = '系统';
		return A('Common/Ad','Model')->getAdId($fadname,$fbrand,$fadclasscode,$adowner_name,$creator,$fadclasscode_v2,$type);
//		if(M('tadclass')->where(array('fcode'=>$fadclasscode))->count() == 0 && $fadname == ''){
//			if($fadname == ''){
//				return 0;
//			}else{
//				$fadclasscode = '2301';
//			}
//
//
//		}
//		//if($fbrand == '') return 0;
//		if($fadname == ''){
//			return 0;
//		}else{
//			$adInfo = M('tad')->where(array('fadname'=>$fadname))->find();//通过广告名称查询广告信息
//
//			if($adowner_name == ''){//判断是否有输入广告主
//				$fadowner = 0;//如果没有输入广告主，广告主ID等于0
//			}else{
//				$adownerInfo = M('tadowner')->where(array('fname'=>$adowner_name))->find();//查询广告主信息
//				if($adownerInfo){//如果有广告主信息
//					$fadowner = $adownerInfo['fid'];//赋值广告主ID
//				}else{//如果没有广告主ID
//					$fadowner = M('tadowner')->add(array(
//														'fname'			=> $adowner_name,
//														'fregionid'		=> 100000,
//														'fcreator' 		=> $creator,//创建人
//														'fcreatetime' 	=> date('Y-m-d H:i:s'),//创建时间
//														'fmodifier' 	=> $creator,//修改人
//														'fmodifytime' 	=> date('Y-m-d H:i:s'),//修改时间
//														'fstate'		=> 1,
//													));//添加广告主
//				}
//			}
//
//
//
//			if($adInfo){
//				$ad_id = $adInfo['fadid'];
//				$v_tad = M('tad')->where(array('fadid'=>$ad_id,'fbrand'=>$fbrand,'fadclasscode'=>$fadclasscode,'fadowner'=>$fadowner,'fstate'=>1))->count();
//				if($ad_id > 0 && $v_tad == 0){
//					$s_data = array();
//					$s_data['fbrand'] = $fbrand;
//					if($fadclasscode != '2301'){
//						$s_data['fadclasscode'] = $fadclasscode;
//					}
//					$s_data['fadowner'] = $fadowner;
//					$s_data['fmodifier'] = $creator;
//					$s_data['fmodifytime'] = date('Y-m-d H:i:s');
//					$s_data['fstate'] = 1;
//
//
//					M('tad')->where(array('fadid'=>$ad_id))->save($s_data);//修改广告
//					unset($s_data);
//				}
//
//			}else{
//
//
//				$ad_id = M('tad')->add(array(
//											'fadname'		=> $fadname,//广告名称
//											'fbrand'		=> $fbrand,//品牌
//											'fadclasscode'	=> $fadclasscode,//广告类别代码
//											'fadowner'		=> $fadowner,//广告主ID
//											'fcreator' 		=> $creator,//创建人
//											'fcreatetime' 	=> date('Y-m-d H:i:s'),//创建时间
//											'fmodifier' 	=> $creator,//修改人
//											'fmodifytime' 	=> date('Y-m-d H:i:s'),//修改时间
//											'fstate'		=> 1,
//										));//添加广告
//			}
//
//		}
//		return $ad_id;

    }
	
	
	/*新增电视任务*/
	public function add_tv_task($tvTaskInfo){
		$user_info = get_user_info();//获取用户信息
		$rr = M('ttvsample')->where(array('fid'=>$tvTaskInfo['fid']))->save(array('finputstate'=>2));//修改电视任务为已接收状态
		if($rr == 0) return 0;
		$a_taskInfo = array();
		$a_taskInfo['mediaclass'] = '电视';//媒体类型
		$a_taskInfo['sample_table_name'] = 'ttvsample';//样本表的名字
		$a_taskInfo['sample_id'] = $tvTaskInfo['fid'];//样本ID
		$a_taskInfo['task_state'] = 1;//任务状态
		$a_taskInfo['money'] = C('INPUT_PRICE_N') + (C('INPUT_PRICE_L') * $tvTaskInfo['fadlen']);//任务金额
		$a_taskInfo['receive_time'] = date('Y-m-d H:i:s');//接收任务时间
		$a_taskInfo['user_id'] = $user_info['wx_id'];//用户ID
		$task_id = M('ad_input_task')->add($a_taskInfo);//添加任务表
		return $task_id;//返回任务ID
	}
	
	/*新增广播任务*/
	public function add_bc_task($bcTaskInfo){
		$user_info = get_user_info();//获取用户信息
		$rr = M('tbcsample')->where(array('fid'=>$bcTaskInfo['fid']))->save(array('finputstate'=>2));//修改电视任务为已接收状态
		if($rr == 0) return 0;
		$a_taskInfo = array();
		$a_taskInfo['mediaclass'] = '广播';//媒体类型
		$a_taskInfo['sample_table_name'] = 'tbcsample';//样本表的名字
		$a_taskInfo['sample_id'] = $bcTaskInfo['fid'];//样本ID
		$a_taskInfo['task_state'] = 1;//任务状态
		$a_taskInfo['money'] = C('INPUT_PRICE_N') + (C('INPUT_PRICE_L') * $bcTaskInfo['fadlen']);//任务金额
		$a_taskInfo['receive_time'] = date('Y-m-d H:i:s');//接收任务时间
		$a_taskInfo['user_id'] = $user_info['wx_id'];//用户ID
		$task_id = M('ad_input_task')->add($a_taskInfo);//添加任务表
		return $task_id;//返回任务ID
	}
	
	/*新增报纸任务*/
	public function add_paper_task($paperTaskInfo){
		$user_info = get_user_info();//获取用户信息
		$rr = M('tpapersample')->where(array('fpapersampleid'=>$paperTaskInfo['fpapersampleid']))->save(array('finputstate'=>2));//修改电视任务为已接收状态
		if($rr == 0) return 0;
		$a_taskInfo = array();
		$a_taskInfo['mediaclass'] = '报纸';//媒体类型
		$a_taskInfo['sample_table_name'] = 'tpapersample';//样本表的名字
		$a_taskInfo['sample_id'] = $paperTaskInfo['fpapersampleid'];//样本ID
		$a_taskInfo['task_state'] = 1;//任务状态
		$a_taskInfo['money'] = C('INPUT_PRICE_N');//任务金额
		$a_taskInfo['receive_time'] = date('Y-m-d H:i:s');//接收任务时间
		$a_taskInfo['user_id'] = $user_info['wx_id'];//用户ID
		$task_id = M('ad_input_task')->add($a_taskInfo);//添加任务表
		return $task_id;//返回任务ID
	}
	
	
	
	



}