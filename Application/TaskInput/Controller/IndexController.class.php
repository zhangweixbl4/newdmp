<?php
namespace TaskInput\Controller;
use Think\Controller;
use Think\Exception;

class IndexController extends BaseController {


    /**
     * 返回统计数组, 第一层是行, 第二层是列
     * @return array
     */
    private function taskCountContent()
    {
        $date = date('Y-m-d');
        $content = [];
        $wx_id = session('wx_id');
        if(A('InputPc/Task','Model')->user_other_authority('1006')) {
//            $todayCount = M('user_task_count')
//                ->field('(v3_paper_task + v3_bc_task + v3_tv_task) as count, v3_task_back as back')
//                ->where([
//                    'user_id' => ['EQ', session('wx_id')],
//                    'count_date' => ['EQ', $date]
//                ])
//                ->select()[0];
//            $yesterdayCount = M('user_task_count')
//                ->field('(v3_paper_task + v3_bc_task + v3_tv_task) as count, v3_task_back as back')
//                ->where([
//                    'user_id' => ['EQ', session('wx_id')],
//                    'count_date' => ['EQ', date('Y-m-d', strtotime('-1 day', strtotime($date)))]
//                ])
//                ->select()[0];
            $todayBetween = [
                strtotime(date('Y-m-d')),
                strtotime('+1 day',strtotime(date('Y-m-d'))) - 1
            ];
            $currentMonthBetween = [
                strtotime(date('Y-m')),
                strtotime('+1 month',strtotime(date('Y-m'))) - 1
            ];
            $todayScore = M('task_input_score')->where([
                'update_time' => ['BETWEEN', $todayBetween],
                'wx_id' => ['EQ', $wx_id],
            ])->sum('score');
            $monthScore = M('task_input_score')->where([
                'update_time' => ['BETWEEN', $currentMonthBetween],
                'wx_id' => ['EQ', $wx_id],
            ])->sum('score');
//            $content[] = [
//                [
//                    'text' => '今日完成任务:',
//                    'count' => intval($todayCount['count']),
//                ],
//                [
//                    'text' => '今日退回任务:',
//                    'count' => intval($todayCount['back']),
//                ],
//            ];
//            $content[] = [
//                [
//                    'text' => '昨日完成任务:',
//                    'count' => intval($yesterdayCount['count']),
//                ],
//                [
//                    'text' => '昨日退回任务:',
//                    'count' => intval($yesterdayCount['back']),
//                ],
//            ];
            $content[] = [
                [
                    'text' => '今日任务积分',
                    'count' => $todayScore ? $todayScore : 0,
                ],
                [
                    'text' => '本月任务积分',
                    'count' => $monthScore ? $monthScore : 0,
                ],
            ];
        }
        if(A('InputPc/Task','Model')->user_other_authority('1007')) {
//            $yesterdayQualityCount = M('user_task_count')
//                ->field('v3_quality as count')
//                ->where([
//                    'user_id' => ['EQ', session('wx_id')],
//                    'count_date' => ['EQ', date('Y-m-d', strtotime('-1 day', strtotime($date)))]
//                ])
//                ->select()[0];
//            $todayQualityCount = M('user_task_count')
//                ->field('v3_quality as count')
//                ->where([
//                    'user_id' => ['EQ', session('wx_id')],
//                    'count_date' => ['EQ', $date]
//                ])
//                ->select()[0];
//            $content[] = [
//                [
//                    'text' => '昨日质检任务:',
//                    'count' => intval($yesterdayQualityCount['count']),
//                ],
//                [
//                    'text' => '今日质检任务:',
//                    'count' => intval($todayQualityCount['back']),
//                ],
//            ];
        }
        return $content;

	}

    public function index(){
		if(!session('wx_id')){
			header("Location:".U('Adinput/VideoCutting/login'));
			exit;
		}
        $wxInfo = M('ad_input_user')->where(array('wx_id'=>session('wx_id')))->find();
        $this->assign('wxInfo',$wxInfo);
		$this->assign('menu_list',A('TaskInput/Menu','Model')->menu_list());//管理菜单
        $content = $this->taskCountContent();
        $this->assign(compact('content'));
		
		$this->display();
	}
	
	public function home()
    {
        $wx_id = session('wx_id');
        $userInfo = M('ad_input_user')->find($wx_id);
        $userName = $userInfo['alias'];
        $userGroup = M('ad_input_user_group')->where(['group_id' => ['EQ', $userInfo['group_id']]])->getField('group_name');
        $backNumber = M('task_input')->where([
            'wx_id' => ['EQ', $wx_id],
            'cut_err_state' => ['EQ', 0],
            'task_state' => ['EQ', 4],
        ])->count();
        $content = $this->taskCountContent();
        $have_group_id = M('ad_input_user')
            ->where([
                'wx_id' => ['EQ', $wx_id]
            ])
            ->getField('have_group_id');
        if ($have_group_id){
            $have_group_list = M('ad_input_user_group')
                ->field('group_id, group_name')
                ->where("field(group_id, $have_group_id) AND group_name NOT LIKE '数据_%'")
                ->cache(true, 60)
                ->select();
        }else{
            $have_group_list = [];
        }
        $this->assign(compact('content'));
        $this->assign(compact('userName', 'userGroup', 'backNumber','have_group_list' ));

		$this->display();
	}

    public function changeGroup($group_id)
    {
        $wx_id = session('wx_id');
        $cacheKey = 'user_wx_id' . $wx_id . 'group_id_key';
        $res = M('ad_input_user')
            ->where([
                'wx_id' => ['EQ', $wx_id],
            ])
            ->save([
                'group_id' => $group_id
            ]);
        if ($res){
            S($cacheKey, null);
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功',
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败',
            ]);
        }
	}

    /**
     * 修改众包用户密码
     */
    public function changePassword()
    {
        if (IS_POST){
            $wx_id = session('wx_id');
            $oldPassword = I('oldPassword');
            $newPassword = I('newPassword');
            $repeatNewPassword = I('repeatNewPassword');
            $captcha = I('captcha');
            if (!$captcha){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '请输入验证码',
                ]);
            }else{
                if (S('task_input_change_password_captcha_user_'.$wx_id) != $captcha){
                    $this->ajaxReturn([
                        'code' => -1,
                        'msg' => '验证码错误',
                    ]);
                }
            }
            if (!$oldPassword){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '请输入原密码',
                ]);
            }else{
                $where['password'] = md5($oldPassword);
                $passWordIsRight = M('ad_input_user')->where(['wx_id' => ['EQ', $wx_id], 'password' => ['EQ', md5($oldPassword)]])->count() != 0;
                if (!$passWordIsRight){
                    $this->ajaxReturn([
                        'code' => -1,
                        'msg' => '原密码错误',
                    ]);
                }
            }
            if (!$newPassword){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '请输入新密码',
                ]);
            }elseif (strlen($newPassword) < 6 || strlen($newPassword) > 18){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '密码长度在6位和18位之间',
                ]);
            }
            if (!$repeatNewPassword){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '请重复输入新密码',
                ]);
            }
            if ($newPassword !== $repeatNewPassword){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '两次输入密码不一致',
                ]);
            }
            // 保存新密码
            $res = M('ad_input_user')
                ->where([
                    'wx_id' => ['EQ', $wx_id],
                ])
                ->save([
                    'password' => md5($newPassword)
                ]) !== false;
            if ($res){
                $this->ajaxReturn([
                    'code' => 0,
                    'msg' => '修改成功, 请关闭此窗口',
                ]);
            }else{
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '修改失败',
                ]);
            }
        }else{
            $this->display();
        }
	}

    /**
     * 修改密码时发送验证码
     */
    public function sendCaptcha()
    {
        $wx_id = session('wx_id');
        $key = 'task_input_change_password_captcha_user_'.$wx_id;
        if (!S($key)){
            $user_info = M('ad_input_user')->find($wx_id);
            if (!$user_info['mobile']){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '当前账号没有绑定手机, 请联系管理员修改',
                ]);
            }
            $captchaCode = mt_rand(0, 9).mt_rand(0, 9).mt_rand(0, 9).mt_rand(0, 9);
            try{
                $sendSmsRes = A('Common/Alitongxin','Model')->identifying_sms($user_info['mobile'],$captchaCode,'密码修改验证码');
                A('Adinput/Weixin','Model')->wx_captcha($wx_id,$captchaCode);
            }catch (Exception $e){

            }
            S($key, $captchaCode, 120);
        }
        $this->ajaxReturn([
            'code' => 0,
            'msg' => '发送成功, 请查看手机或微信公众号, 有效期120秒',
        ]);
	}
}