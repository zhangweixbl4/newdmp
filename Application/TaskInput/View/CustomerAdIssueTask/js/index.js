Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}
let vm = new Vue({
  el: '#app',
  components: {
    task
  },
  data: function () {
    return {
      loading: false,
      table: [],
      page: 1,
      pageSize: 10,
      total: 0,
      where: {},
      currentId: 0,
      showTaskInfo: false
    }
  },
  created: function () {
    this.getTable()
  },
  methods: {
    getMediaList(row) {
      if (row.mediaList) return false
      $.post('/TaskInput/CustomerAdIssueTask/searchCustomerMedia', { task_extract_id: row.id }).then(res => {
        this.table = this.table.map(item => {
          if (item.id === row.id) {
            item.mediaList = res.mediaList
          }
          return item
        })
      })
    },
    getTable: function () {
      this.page = 1
      this.searchReq()
    },
    searchReq: function () {
      this.loading = true
      $.post('', {
        where: this.where,
        page: this.page,
        pageSize: this.pageSize,
      }).then((res) => {
        this.loading = false
        this.table = res.table
        this.total = +res.total
        this.updateProgress()
      })
    },
    resetWhere: function () {
      this.where = {
        userTag: [],
      }
      this.getTable()
    },
    pageSizeChange: function (val) {
      this.pageSize = val
      this.getTable()
    },
    pageChange: function (page) {
      this.page = page
      this.searchReq()
    },

    searchAlias: function (queryString, cb) {
      $.post("/Api/User/searchUserByAlias", {
        keyword: queryString
      }, function (res) {
        cb(res.data);
      });
    },
    updateSuccess: function (msg) {
      this.showTaskInfo = false
      this.$message.success(msg)
      this.searchReq()
    },
    updateField: function (msg) {
      this.showTaskInfo = false
      this.$message.error(msg)
      this.searchReq()
    },
    addTask: function () {
      this.currentId = 0
      this.showTaskInfo = true
      this.$nextTick(() => {
        this.$refs.taskInfo.dataInit()
      })
    },
    checkProgress: function (row) {
      let index
      this.table.forEach((v, i) => {
        if (v.id === row.id) {
          index = i
        }
      })
      return new Promise((resolve, rej) => {
        $.post('getTaskProgress', {
          id: row.id
        }).then((res) => {
          if (res.data == 100) {
            Vue.set(this.table[index], 'state', '完成')
          }
          Vue.set(this.table[index], 'progress', res.data)
          resolve(1)
        })
      })
    },
    updateProgress: function () {
      let promiseAllArr = [];
      this.table.forEach((v, i) => {
        if (v.progress != 100 && v.state !== '完成') {
          promiseAllArr.push(this.checkProgress(v))
        }
        if (v.state === '完成') {
          v.progress = 100
        }
      })
      let promiseAll = Promise.all(promiseAllArr)
      promiseAll.then(() => {
        let timeId = setTimeout(this.updateProgress, 5000)
        let tempTimeId = 0
        while (tempTimeId < timeId) {
          clearTimeout(tempTimeId)
          tempTimeId++
        }
      })
    }
  }
})