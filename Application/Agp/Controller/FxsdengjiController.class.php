<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 线索登记
 */

class FxsdengjiController extends BaseController
{
	/**
	 * 获取待登记线索列表
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function index() {
		session_write_close();
		$system_num = getconfig('system_num');

		$p 					= I('page', 1);//当前第几页
		$pp 				= 20;//每页显示多少记录
		$mclass 			= I('mclass')?I('mclass'):0;// 媒体类型
		$fadname 			= I('fadname');// 广告名称
		$fmedianame 		= I('fmedianame');// 发布媒介
		$fadclasscode 		= I('fadclasscode');// 广告内容类别
		$fissuedate 		= I('fissuedate');//发布时间

		$where['_string'] = '1=1';
		if ($fadname != '') {
			$where['a.fad_name'] = array('like', '%' . $fadname . '%');//广告名称
		}
		if ($fmedianame != '') {
			$where['d.fmedianame'] = array('like', '%' . $fmedianame . '%');//发布媒介
		}
		if (!empty($fadclasscode)) {
			$arr_code=D('Function')->get_next_tadclasscode($fadclasscode,true);//获取下级广告类别代码
			if($arr_code){
				$where['a.fad_class_code'] = array('in', $arr_code);
			}else{
				$where['a.fad_class_code'] = $fadclasscode;
			}
		}
		if(!empty($fissuedate)){
			$where['_string'] .= ' and datediff("'.$fissuedate.'",a.create_time)=0';
		}
		//机构数据查看权限范围
		$where['a.fstatus'] = 0;
		if(!empty($mclass)){
			$where['a.fmedia_class'] 	= $mclass;
		}
		$where['a.fregisterid'] 	= 0;
		$where['a.fcustomer'] 	= $system_num;

		$count = M('tbn_illegal_ad')
			->alias('a')
			->join('tmedia d on  a.fmedia_id=d.fid and d.fid=d.main_media_id')//媒介信息
			->join('tbn_case_send snd on a.fid = snd.fillegal_ad_id and snd.fstatus = 0 and snd.frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid'))
			->join('tadclass e on a.fad_class_code=e.fcode ')//广告内容列别
			->where($where)
			->count();// 查询满足要求的总记录数

		
		$data = M('tbn_illegal_ad')
			->alias('a')
			->field('
				a.fid,
				a.fmedia_class as mclass,
	            a.fad_name as fadname,
	             (case when instr(d.fmedianame,"（") > 0 then left(d.fmedianame,instr(d.fmedianame,"（") -1) else d.fmedianame end) as fmedianame,
	            e.ffullname as adclass_fullname,
	            a.fillegal,
	            a.create_time
			')
			->join('tmedia d on  a.fmedia_id=d.fid and d.fid=d.main_media_id')//媒介信息
			->join('tbn_case_send snd on a.fid = snd.fillegal_ad_id and snd.fstatus = 0 and snd.frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid'))
			->join('tadclass e on a.fad_class_code=e.fcode ')//广告内容列别
			->where($where)
			->order('a.create_time desc')
			->page($p,$pp)
			->select();//查询违法广告
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	/**
	 *(手工/系统(合并/单个))登记线索操作
	 * @return array|string code-状态（0成功1失败），msg-提示信息）
	 * by zw
	 */
	public function create_register()
	{
		$system_num = getconfig('system_num');
		$submit 		= I('submit')?I('submit'):2;//1保存，2提交
		$fillegaladid 	= I('fillegaladid');//违法广告ID组，非手工登记必要参数
		
		$res['fpersonmobile']			= I('fpersonmobile');//举报人联系电话
		$res['fpersonadress']			= I('fpersonadress');//举报人联系地址
		$res['fpersonpostcode']			= I('fpersonpostcode');//举报人邮政编码
		$res['fregisterregulatorid']	= session('regulatorpersonInfo.fregulatorpid');//登记机构id
		$res['fregisterregulatorname']	= session('regulatorpersonInfo.regulatorpname');//登记机构名称
		$res['fregisterpersonid']		= session('regulatorpersonInfo.fid');//登记人id
		$res['fregisterpersonname']		= session('regulatorpersonInfo.fname');//登记人姓名
		$res['fnumber']					= I('fnumber');//编号
		$res['fname']					= I('fname');//标题
		$res['fcreatetime'] 			= date('Y-m-d H:i:s');//创建时间
		$res['ffromtype']				= I('ffromtype');//来源分类
		$res['fpersontype']				= I('fpersontype');//举报人类型
		$res['fregistercontents']		= I('fregistercontents');//登记摘要
		$res['fwaitregulatorid']		= I('fwaitregulatorid');//下一处理部门默认无
		$res['fwaitpersonid']			= I('fwaitpersonid');//下一处理人默认无
		$res['fusersid'] 				= ','.session('regulatorpersonInfo.fid').',';//加入到处理用户组
		$res['fregistertime']			= I('fcreatetime');//登记时间
		if($res['fpersontype']==10){//个人
			$res['fpersonname']		= I('fpersonname');//举报人姓名
			$res['fpersoncard']		= I('fpersoncard');//举报人身份证号
		}elseif($res['fpersontype']==20){//单位
			$res['fpersonname']		= I('fpersonname');//举报单位法人姓名
			$res['fpersoncompany']	= I('fpersoncompany');//举报单位名称
		}elseif($res['fpersontype']==30){//移送机关
			$res['fpersonname']		= I('fpersonname');//移送机关联系人
			$res['fpersoncompany']	= I('fpersoncompany');//移送机关名称
		}else{
			$res['fpersontype'] 	= 0;
		}
		if(is_array($ffillegaladid)){
			$res['ffillegaladid'] = implode(',', $ffillegaladid);
		}

		$tregister_model = M("tregister");
		if($submit==2){//提交
			$res['fstate'] 			= 11;//待登记签批
			$fregisterid = $tregister_model->add($res);//添加登记信息

			/*新增流程记录*/
			$sh_list['fregisterid'] 				= $fregisterid;
			$sh_list['fupflowid'] 					= 0;
			$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
			$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
			$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
			$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
			$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
			$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
			$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
			$sh_list['fflowname'] 					= '线索登记';
			$sh_list['freason'] 					= '线索登记';
			$sh_list['fregulatorid'] 				= I('fwaitregulatorid');
			$sh_list['fstate'] 						= 11;
			$flowid = M("tregisterflow")->add($sh_list);
		}else{
			$res['fstate'] 	= 10;//草稿箱
			$fregisterid = $tregister_model->add($res);//添加草稿箱
		}

		//将登记表id写入违法广告表，用以标记已处理的违法广告（非手工登记方式）
		if(!empty($fillegaladid)){
			$tddata['fregisterid'] 	= $fregisterid;
			$tddata['fstatus'] 		= 10;
			if(is_array($fillegaladid)){
				$tdwhere['fid'] 	= array('in',$fillegaladid);
			}else{
				$tdwhere['fid']	= $fillegaladid;
			}
			$tdwhere['fcustomer']	= $system_num;
			M('tbn_illegal_ad')->where($tdwhere)->save($tddata);
		}

		//如果上传附件
		if(I('attachinfo')&&!empty($fregisterid)){
			$attachinfo=I('attachinfo');
			$attach_data['fregisterid'] 			= $fregisterid;//线索登记id
			if(!empty($flowid)){
				$attach_data['fillegaladflowid'] 	= $flowid;//流程ID
			}
			$attach_data['fuploadtime'] 			= date('Y-m-d H:i:s');//上传时间
			$attach_data['fattachuser'] 			= 1;//1:工商附件 2.媒体附件
			$attach_data['fcreateregualtorid'] 		= session('regulatorpersonInfo.fregulatorid');//上传机构id
			$attach_data['fflowname'] 				='线索登记';//环节名称
			$attach_data['is_file'] 				=1;//1 真文件 2 假文件
			foreach ($attachinfo as $key => $value){
				$attach_data['fattachname'] 		= $value['fattachname'];
				$attach_data['fattachurl'] 			= $value['fattachurl'];
				$attach_data['ffilename'] 			= preg_replace('/\..*/','',$value['fattachurl']);
				$attach_data['ffiletype'] 			= preg_replace('/.*\./','',$value['fattachname']);
				$attach[$key] 						= $attach_data;
			}
			$attachid = M('tregisterfile')->addAll($attach);
		}

		if($fregisterid){
			D('Function')->write_log('线索登记',1,'操作成功','tregister',$fregisterid,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
		}else{
			D('Function')->write_log('线索登记',0,'操作失败','tregister',0,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
		}
	}

	/**
	 *获取登记任务列表
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function get_registerlist(){
		session_write_close();

		$p 	= I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录

		$fname = I('fname');//登记表名
		$fmodifytime = I('fmodifytime');//登记时间
		$status = I('status');//登录来源
		if(!empty($fname)){
			$where['fname'] = array('like','%'.$fname.'%');
		}
		if(!empty($fmodifytime)){
			$where['_string'] = ' datediff("'.$fmodifytime.'",fregistertime)=0';
		}
		if(!empty($status)){
			if($status==1){
				$where['fstate'] = 10;//草稿箱
			}elseif($status==2){
				$where['fstate'] = array('in',array(12,13));//登记签批退回、初核退回
			}else{
				$where['fstate'] = array('in',array(10,12,13));//草稿箱、登记签批退回、初核退回
			}
		}else{
			$where['fstate'] 	= array('in',array(10,12,13));//草稿箱、登记签批退回、初核退回
		}

		$where['fregisterregulatorid'] 	= session('regulatorpersonInfo.fregulatorpid');//登记机关id
		$where['fregisterpersonid'] 	= session('regulatorpersonInfo.fid');//登记人id

		$count = M('tregister')
			->where($where)
			->count();//查询满足条件的总记录数

		
		$data = M('tregister')
			->where($where)
			->order('fregistertime desc,fid desc')
			->page($p,$pp)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	

	/**
	 *登记任务保存或提交操作
	 * @return array|string code-状态（0成功1失败），msg-提示信息）
	 * by zw
	 */
	public function update_register()
	{

		if(I('fid')){
			$fid = I('fid');//线索登记id
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}
		$submit = I('submit')?I('submit'):2;//1保存，2登记

		$res['fpersonmobile']			= I('fpersonmobile');//举报人联系电话
		$res['fpersonadress']			= I('fpersonadress');//举报人联系地址
		$res['fpersonpostcode']			= I('fpersonpostcode');//举报人邮政编码
		$res['fregisterregulatorid']	= session('regulatorpersonInfo.fregulatorpid');//登记机构id
		$res['fregisterregulatorname']	= session('regulatorpersonInfo.regulatorpname');//登记机构名称
		$res['fregisterpersonid']		= session('regulatorpersonInfo.fid');//登记人id
		$res['fregisterpersonname']		= session('regulatorpersonInfo.fname');//登记人姓名
		$res['fnumber']					= I('fnumber');//编号
		$res['fname']					= I('fname');//标题
		$res['fcreatetime'] 			= date('Y-m-d H:i:s');//创建时间
		$res['ffromtype']				= I('ffromtype');//来源分类
		$res['fpersontype']				= I('fpersontype');//举报人类型
		$res['fregistercontents']		= I('fregistercontents');//登记摘要
		$res['fregistertime']			= I('fcreatetime');//登记时间

		if($res['fpersontype']==10){//个人
			$res['fpersonname']		= I('fpersonname');//举报人姓名
			$res['fpersoncard']		= I('fpersoncard');//举报人身份证号
		}elseif($res['fpersontype']==20){//单位
			$res['fpersonname']		= I('fpersonname');//举报单位法人姓名
			$res['fpersoncompany']	= I('fpersoncompany');//举报单位名称
		}elseif($res['fpersontype']==30){//移送机关
			$res['fpersonname']		= I('fpersonname');//移送机关联系人
			$res['fpersoncompany']	= I('fpersoncompany');//移送机关名称
		}else{
			$res['fpersontype'] 	= 0;
		}

		$tregister_model = M("tregister");

		$where['fid'] = $fid;
		$where['fstate'] = array('in',array(10,12,13));

		$trdo = $tregister_model->field('fstate')->where($where)->find();
		if(!empty($trdo)){
			if($submit==2){//登记
				$res['fwaitregulatorid']		= I('fwaitregulatorid');//下一处理部门默认无
				$res['fwaitpersonid']			= I('fwaitpersonid');//下一处理人默认无
				$res['fstate'] 					= 11;//待登记签批
				if($trdo['fstate']==10){//如果草稿箱提交
					$fregisterid = $tregister_model->where($where)->save($res);//变更到登记信息

					/*新增流程记录*/
					$sh_list['fregisterid'] 				= $fid;
					$sh_list['fupflowid'] 					= 0;
					$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
					$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
					$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
					$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
					$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
					$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
					$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
					$sh_list['fflowname'] 					= '线索登记';
					$sh_list['freason'] 					= '线索登记';
					$sh_list['fregulatorid'] 				= I('fwaitregulatorid');
					$sh_list['fstate'] 						= 11;
					$flowid = M("tregisterflow")->add($sh_list);

					M('tregisterfile')->where(['fregisterid'=>$fid,'fillegaladflowid'=>0])->save(array('fillegaladflowid'=>$flowid));//草稿箱的附件文件变成实际登记表附件文件
				}elseif($trdo['fstate']==12){//如果登记签批退回提交
					//签批退回，将原有的登记签批信息清除
					$res['fregistersignpersonid'] 		= 0;//登记签批人id
					$res['fregistersignpersonname'] 	= '';//登记签批人
					$res['fregistersigntime'] 			= array('exp','null');//登记签批时间
					$res['fregistersigncontent'] 		= '';//登记签批建议
					$fregisterid = $tregister_model->where($where)->save($res);//变更到登记信息

					$twdo = M('tregisterflow')->field('flowid')->where(['fregisterid'=>$fid,'_string'=>'fstate=12 and ffinishtime is null'])->find();//获取前一流程id
					if(!empty($twdo)){
						//更新上一流程的处理人
						$twdata['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
						$twdata['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
						$twdata['ffinishtime']					= date('Y-m-d H:i:s');//处理时间
						$twdo1 = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'fstate=12 and ffinishtime is null'])->save($twdata);
						if(!empty($twdo1)){
							/*新增流程记录*/
							$sh_list['fregisterid'] 				= $fid;
							$sh_list['fupflowid'] 					= $twdo['flowid'];
							$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
							$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
							$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
							$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
							$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
							$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
							$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
							$sh_list['fflowname'] 					= '线索登记';
							$sh_list['freason'] 					= '签批退回重新登记';
							$sh_list['fregulatorid'] 				= I('fwaitregulatorid');
							$sh_list['fstate'] 						= 11;
							$flowid = M("tregisterflow")->add($sh_list);
						}
					}
				}elseif($trdo['fstate']==13){//如果处置退回提交
					//签记退回，将原有的登记签批及处置信息清除
					$res['fregistersignpersonid'] 		= 0;//登记签批人id
					$res['fregistersignpersonname'] 	= '';//登记签批人
					$res['fregistersigntime'] 			= array('exp','null');//登记签批时间
					$res['fregistersigncontent'] 		= '';//登记签批建议

					$res['fcheckregulatorid'] 			= 0;//初核机构id
					$res['fcheckregulatorname'] 		= '';//初核机构名称
					$res['fcheckpersonid'] 				= 0;//初核人id
					$res['fcheckpersonname'] 			= '';//初核人姓名
					$res['fcheckcontents'] 				= '';//初核办理意见
					$res['fchecktype'] 					= 0;//初核处理方式
					$res['fchecktime'] 					= array('exp','null');//初核时间

					$res['fapprovalpersonid'] 			= 0;//初核签批人id
					$res['fapprovalpersonname'] 		= '';//初核签批人姓名
					$res['fapprovalcontents'] 			= '';//初核签批人意见
					$res['fapprovaltime'] 				= array('exp','null');//初核签批时间

					$fregisterid = $tregister_model->where($where)->save($res);//变更到登记信息

					$twdo = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'fstate=13 and ffinishtime is null'])->find();//获取前一流程id
					if(!empty($twdo)){
						//更新上一流程的处理人
						$twdata['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
						$twdata['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
						$twdata['ffinishtime']					= date('Y-m-d H:i:s');//处理时间
						$twdo1 = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'fstate=13 and ffinishtime is null'])->save($twdata);
						if(!empty($twdo1)){
							/*新增流程记录*/
							$sh_list['fregisterid'] 				= $fid;
							$sh_list['fupflowid'] 					= $twdo['flowid'];
							$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
							$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
							$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
							$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
							$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
							$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
							$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
							$sh_list['fflowname'] 					= '线索登记';
							$sh_list['freason'] 					= '处置退回重新登记';
							$sh_list['fregulatorid'] 				= I('fwaitregulatorid');
							$sh_list['fstate'] 						= 11;
							$flowid = M("tregisterflow")->add($sh_list);
						}
					}
				}
				
			}else{
				$fregisterid = $tregister_model->where($where)->save($res);//保存草稿箱
			}

			//如果上传附件
			if(I('attachinfo')&&!empty($fregisterid)){
				$attachinfo=I('attachinfo');
				$attach_data['fregisterid'] 			= $fid;//线索登记id
				if(!empty($flowid)){
					$attach_data['fillegaladflowid'] 	= $flowid;//流程ID
				}
				$attach_data['fuploadtime'] 			= date('Y-m-d H:i:s');//上传时间
				$attach_data['fattachuser'] 			= 1;//1:工商附件 2.媒体附件
				$attach_data['fcreateregualtorid'] 		= session('regulatorpersonInfo.fregulatorid');//上传机构id
				$attach_data['fflowname'] 				='线索登记';//环节名称
				$attach_data['is_file'] 				=1;//1 真文件 2 假文件
				foreach ($attachinfo as $key => $value){
					$attach_data['fattachname'] 		= $value['fattachname'];
					$attach_data['fattachurl'] 			= $value['fattachurl'];
					$attach_data['ffilename'] 			= preg_replace('/\..*/','',$value['fattachurl']);
					$attach_data['ffiletype'] 			= preg_replace('/.*\./','',$value['fattachname']);
					$attach[$key] 						= $attach_data;
				}
				$attachid = M('tregisterfile')->addAll($attach);
			}
		}

		if($fregisterid){
			D('Function')->write_log('登记任务',1,'操作成功','tregister',$fregisterid,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
		}else{
			D('Function')->write_log('登记任务',0,'操作失败','tregister',0,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
		}
	}

	/**
	 *获取待签批列表
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function get_dqpregisterlist(){
		session_write_close();
		
		$p 	= I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录

		$where['fstate'] = 11;//待签批状态
		$where['_string'] 	= '((fwaitregulatorid='.session('regulatorpersonInfo.fregulatorpid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid='.session('regulatorpersonInfo.fid').'))';//用户处理权限范围

		$fname 			= I('fname');//登记表名
		$fmodifytime 	= I('fmodifytime');//登记时间
		if(!empty($fname)){
			$where['fname'] = array('like','%'.$fname.'%');
		}
		if(!empty($fmodifytime)){
			$where['_string'] .= ' and datediff("'.$fmodifytime.'",fregistertime)=0';
		}

		$count = M('tregister')
			->where($where)
			->count();//查询满足条件的总记录数

		
		$data = M('tregister')
			->where($where)
			->order('fregistertime desc,fid desc')
			->page($p,$pp)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	/**
	 * 签批操作
	 * @return array|string code-状态（0成功1失败），msg-提示信息）
	 * by zw
	*/
	public function action_qpregister(){

		if(I('fid')){
			$fid = I('fid');
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}
		$submit = I('submit')?I('submit'):1;//1签批2退回
		
		$where['fid'] 			= $fid;//登记表ID
		$where['fstate'] 		= 11;//登记待签批状态
		$where['_string'] 	= '((fwaitregulatorid='.session('regulatorpersonInfo.fregulatorpid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid='.session('regulatorpersonInfo.fid').'))';//用户处理权限范围
		$data = M('tregister')->where($where)->find();
		if(!empty($data)){
			if($submit==1){//签批
				//保存签批信息
				$trdata['fregistersigncontent'] 		= I('result');//签批建议
				$trdata['fregistersignpersonid'] 		= session('regulatorpersonInfo.fid');//签批人ID
				$trdata['fregistersignpersonname'] 		= session('regulatorpersonInfo.fname');//签批人
				$trdata['fregistersigntime'] 			= date('Y-m-d H:i:s');//签批时间
				$trdata['fstate'] 						= 20;//签批通过，流转至线索待初核
				$trdata['fwaitregulatorid'] 			= I('fwaitregulatorid');//下一处理部门
				$trdata['fwaitpersonid'] 				= I('fwaitpersonid');//下一处理人
				$trdata['fusersid'] 					= $data['fusersid'].session('regulatorpersonInfo.fid').',';//加入到处理用户组
				$trdo = M('tregister')->where($where)->save($trdata);

				$twdo = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'fstate=11 and ffinishtime is null'])->find();//获取前一流程id
				if(!empty($twdo)){
					//更新上一流程的处理人
					$twdata['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
					$twdata['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
					$twdata['fistree'] 						= 10;//设置上一流程为主流程
					$twdata['ffinishtime']					= date('Y-m-d H:i:s');//处理时间
					$twdo1 = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'fstate=11 and ffinishtime is null'])->save($twdata);

					if(!empty($twdo1)){
						//新增流程记录
						$sh_list['fregisterid'] 				= $fid;
						$sh_list['fupflowid'] 					= $twdo['flowid'];
						$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
						$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
						$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
						$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
						$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
						$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
						$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
						$sh_list['fflowname'] 					= '线索登记';
						$sh_list['freason'] 					= '登记签批';
						$sh_list['fregulatorid'] 				= I('fwaitregulatorid');
						$sh_list['fcreateinfo'] 				= I('result');
						$sh_list['fstate'] 						= 20;
						$flowid = M("tregisterflow")->add($sh_list);
					}
				}
			}else{

				$twdo = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'fstate=11 and ffinishtime is null'])->find();//获取前一流程id
				if(!empty($twdo)){
					//保存签批信息
					$trdata['fregistersigncontent'] 	= I('result');//签批建议
					$trdata['fregistersignpersonid'] 	= session('regulatorpersonInfo.fid');//签批人ID
					$trdata['fregistersignpersonname'] 	= session('regulatorpersonInfo.fname');//签批人
					$trdata['fregistersigntime'] 		= date('Y-m-d H:i:s');//签批时间
					$trdata['fstate'] 					= 12;//签批退回
					$trdata['fwaitregulatorid'] 		= $twdo['fcreateregualtorid'];//下一处理部门
					$trdata['fwaitpersonid'] 			= $twdo['fcreateregualtorpersonid'];//下一处理人
					$trdata['fusersid'] 				= $data['fusersid'].session('regulatorpersonInfo.fid').',';//加入到处理用户组
					$trdo = M('tregister')->where($where)->save($trdata);

					//更新上一流程的处理人
					$twdata['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
					$twdata['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
					$twdata['ffinishtime']					= date('Y-m-d H:i:s');//处理时间
					$twdo1 = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'fstate=11 and ffinishtime is null'])->save($twdata);

					if(!empty($twdo1)){
						//新增流程记录
						$sh_list['fregisterid'] 				= $fid;
						$sh_list['fupflowid'] 					= $twdo['flowid'];
						$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
						$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
						$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
						$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
						$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
						$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
						$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
						$sh_list['fflowname'] 					= '线索登记';
						$sh_list['freason'] 					= '登记签批退回';
						$sh_list['fregulatorid'] 				= $twdo['fcreateregualtorid'];
						$sh_list['fcreateinfo'] 				= I('result');
						$sh_list['fstate'] 						= 12;
						$flowid = M("tregisterflow")->add($sh_list);
					}
				}
			}
			D('Function')->write_log('登记签批',1,'签批成功','tregister',$fid,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'签批成功'));
		}else{
			D('Function')->write_log('登记签批',0,'签批失败','tregister',0,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'签批失败'));
		}
	}

	/**
	 *获取待登记违法广告详情（单条/列表）
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function get_tbn_illegal_ad_details(){
		
		$fillegaladid 	= I('fillegaladid');//违法广告id
		if(empty($fillegaladid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}
		if(is_array($fillegaladid)){
			foreach ($fillegaladid as $key => $value) {
				$data[$key] = $this->get_tbn_illegal_adlist_details($value);
			}
		}else{
			$data[0] = $this->get_tbn_illegal_adlist_details($fillegaladid);
		}

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>count($data),'list'=>$data)));
	}

	/**
	 *获取待登记违法广告
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（data-违法广告信息，adclueInfo-样本信息，issue-广告发布信息）
	 * by zw
	 */
	public function get_tbn_illegal_adlist_details($fillegaladid)
	{
		$system_num = getconfig('system_num');
		$where['a.fid'] = $fillegaladid;
		$where['a.fcustomer'] = $system_num;

		$data = M('tbn_illegal_ad')
		->alias('a')
		->field('
			a.fid, a.fmedia_class, a.fsample_id, a.fmedia_id, a.create_time,
			(case when instr(d.fmedianame,"（") > 0 then left(d.fmedianame,instr(d.fmedianame,"（") -1) else d.fmedianame end) as fmedianame,
            e.ffullname as adclass_fullname,
            f.fcreditcode, f.fregaddr, f.flinkman, f.ftel, f.fname as mediaowner_name
		')
		->join('tmedia d on  a.fmedia_id=d.fid and d.fid=d.main_media_id')//媒介信息
		->join('tadclass e on a.fad_class_code=e.fcode ')//广告内容列别
		->join('tmediaowner f on f.fid = d.fmediaownerid')
		->where($where)
		->find();
		if(!empty($data)){
			if($data['fmedia_class']==1){
				$data_se = M('ttvsample')
				->field('
					b.fadid, b.favifilepng, b.fadlen, b.fversion, b.fadmanuno, b.fmanuno, b.fadapprno, b.fapprno, b.fadent, b.fent, b.fentzone, b.fillegalcontent, b.fexpressioncodes, b.fexpressions, b.fconfirmations, b.fpunishments, b.favifilename,b.fissuedate,
            		c.fadname, c.fbrand
            	')
				->alias('b')
				->join('tad c on b.fadid = c.fadid and c.fadid<>0')
				->where('fid='.$data['fsample_id'])
				->find();
			}elseif($data['fmedia_class']==2){
				$data_se = M('tbcsample')
				->field('
					b.fadid, b.favifilepng, b.fadlen, b.fversion, b.fadmanuno, b.fmanuno, b.fadapprno, b.fapprno, b.fadent, b.fent, b.fentzone, b.fillegalcontent, b.fexpressioncodes, b.fexpressions, b.fconfirmations, b.fpunishments, b.favifilename,b.fissuedate,
            		c.fadname, c.fbrand
            	')
				->alias('b')
				->join('tad c on b.fadid = c.fadid and c.fadid<>0')
				->where('fid='.$data['fsample_id'])
				->find();
			}elseif($data['fmedia_class']==3){
				$data_se = M('tpapersample')
				->field('
					b.fadid, fjpgfilename as favifilepng,fjpgfilename, b.fadmanuno, b.fmanuno, b.fadapprno, b.fapprno, b.fadent, b.fent, b.fentzone, b.fillegalcontent, b.fexpressioncodes, b.fexpressions, b.fconfirmations, b.fpunishments,b.fissuedate,
	            	c.fadname, c.fbrand,
	            	y.fpage as fadlen
            	')
				->alias('b')
				->join('tad c on b.fadid = c.fadid and c.fadid<>0')
				->join('tpaperissue y on b.fpapersampleid=y.fpapersampleid')
				->where('b.fpapersampleid='.$data['fsample_id'])
				->find();
			}
			$data = array_merge($data,$data_se);
		}

		return $data;
	}

	/**
	 *获取登记任务详情
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function get_registerview(){
		$system_num = getconfig('system_num');

		if(I('fid')){
			$fid = I('fid');
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}
		$where['fid'] = $fid;//登记表ID
		$data = M('tregister')->where($where)->find();
		if(!empty($data)){
			$data['flow']=M('tregisterflow')
				->field('
					fflowname,
					fcreateregualtorpname,
					fcreateregualtorname,
					fcreateregualtorpersonname,
					freason,
					fcreatetime,
					fcreateinfo,
					fstate
				')
				->where(['fregisterid'=>$fid])
				->order('flowid desc')
				->select();//获取流转信息
			$data['refiles'] = M('tregisterfile')->where(['fregisterid'=>$fid])->order('fid asc')->select();

			$do_dt = M('document')->field('dt_id,dt_name,dt_showstate,dt_createpersonid as createpersonid')->join('tregisterflow on tregisterflow.flowid=document.dt_flowid','left')->where(['dt_tregisterid'=>$fid])->order('dt_id desc')->select();//获取文书列表
			$aa = [];
			foreach ($do_dt as $key => $value) {
				$isf = 1;
				if (($value['dt_showstate']==10 && $value['dt_createpersonid']==session('regulatorpersonInfo.fid'))||$value['dt_flowid']==0) {//仅自己可见
					array_push($aa, $value);
				}elseif($value['dt_showstate']==0){//下级可见
					if($value['fregulatorpersonid']!=session('regulatorpersonInfo.fid')&&$value['fregulatorpersonid']!=0){
						$isf = 2;
					}
					if($value['fregulatorid']!=session('regulatorpersonInfo.fregulatorid')&&$value['fregulatorid']!=0||$value['fregulatorid']==0){
						$isf = 2;
					}
					if($isf == 1){
						array_push($aa, $value);
					}
				}
			}
			$data['document'] = $aa;

			$data['fillegaladid'] = M('tbn_illegal_ad')->field('fid,fmedia_class')->where(['fregisterid'=>$fid,'fcustomer'=>$system_num])->order('fid desc')->select();
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('data'=>$data)));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据'));
		}

	}



}