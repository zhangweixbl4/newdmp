<?php
namespace Api\Model;
use Think\Exception;


class SamModel{


	/*同步样本表最后发布日期*/
	public function last_issue_date($date){
		

		$month = date("Ym", strtotime($date));//月份
		$regionList = M('tregion')->cache(true,86400)->field('left(fid,2) as region_prefix')->where(array('right(fid,4)'=>'0000'))->select();
		
		
		
		
		foreach($regionList as $region){//循环地区
			$table_postfix = '_'.$month.'_'.$region['region_prefix'];//表后缀
			
			$dmp_tv_where = array();
			$dmp_tv_where['ttvissue.fissuedate'] = strtotime($date);
			
			
			
			
			$dmp_bc_where = array();
			$dmp_bc_where['tbcissue.fissuedate'] = strtotime($date);
			
			
			

			try{
				$dmp_tv = M('ttvissue'.$table_postfix)//按媒体查询
								->alias('ttvissue')
								->field('ttvissue.fsampleid')
	
								->where($dmp_tv_where)
								->group('ttvissue.fsampleid')
								->select();
				//var_dump($dmp_tv);
			}catch(Exception $error) {
				//var_dump($error);
				$dmp_tv = array();
			} 
			
			try{ 
				$dmp_bc = M('tbcissue'.$table_postfix)//按媒体查询
								->alias('tbcissue')
								->field('tbcissue.fsampleid')
	
								->where($dmp_bc_where)
								->group('tbcissue.fsampleid')
								->select();
				//var_dump($dmp_bc);
								
			}catch(Exception $error) { 
				//var_dump($error);
				$dmp_bc = array();
			} 	 
			foreach($dmp_tv as $tv){
				$samInfo = M('ttvsample')->field('last_issue_date')->where(array('fid'=>$tv['fsampleid']))->find();
				if(strtotime($samInfo['last_issue_date']) < strtotime($date) && $samInfo){
					M('ttvsample')->where(array('fid'=>$tv['fsampleid']))->save(array('last_issue_date'=>$date));
				}elseif(!$samInfo){
					M('ttvissue'.$table_postfix)->where(array('fsampleid'=>$bc['fsampleid']))->delete();//删除发布记录
					M('ttvissue')->where(array('ftvsampleid'=>$bc['fsampleid']))->delete();//删除发布记录
					file_put_contents('LOG/del_issue'.date('Ymd').'.log','类型:电视	时间:'.date('Y-m-d H:i:s').'	样本id:'.$bc['fsampleid']."\n",FILE_APPEND);
				}
			}
			foreach($dmp_bc as $bc){
				$samInfo = M('tbcsample')->field('last_issue_date')->where(array('fid'=>$bc['fsampleid']))->find();
				if(strtotime($samInfo['last_issue_date']) < strtotime($date) && $samInfo){
					M('tbcsample')->where(array('fid'=>$bc['fsampleid']))->save(array('last_issue_date'=>$date));
				}elseif(!$samInfo){
					M('tbcissue'.$table_postfix)->where(array('fsampleid'=>$bc['fsampleid']))->delete();//删除发布记录
					M('tbcissue')->where(array('fbcsampleid'=>$bc['fsampleid']))->delete();//删除发布记录
					file_put_contents('LOG/del_issue'.date('Ymd').'.log','类型:广播	时间:'.date('Y-m-d H:i:s').'	样本id:'.$bc['fsampleid']."\n",FILE_APPEND);
				}
			}
			
			
		}//循环地区结束
		
		
		
		return true;
		
		
	}	
	
	

}



