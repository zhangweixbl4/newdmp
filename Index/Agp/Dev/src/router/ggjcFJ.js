const ggjcFJ = [{
    path: '/televisionFJ',
    name: 'televisionFJ',
    component: () => import('../components/ggjcFJ/televisionFJ'),
    meta: ['广告监测', '电视']
  },
  {
    path: '/tadsearchFJ',
    name: 'tadsearchFJ',
    component: () => import('../components/ggjcFJ/tadsearchFJ'),
    meta: ['广告监测', '广告查询']
  },
  {
    path: '/newspaperFJ',
    name: 'newspaperFJ',
    component: () => import('../components/ggjcFJ/newspaperFJ'),
    meta: ['广告监测', '报纸']
  },
  {
    path: '/illegaltadFJ',
    name: 'illegaltadFJ',
    component: () => import('../components/ggjcFJ/illegaltadFJ'),
    meta: ['广告监测', '违法广告']
  },
  {
    path: '/evidenceFJ',
    name: 'evidenceFJ',
    component: () => import('../components/ggjcFJ/evidenceFJ'),
    meta: ['广告监测', '证据下载']
  },
  {
    path: '/broadcastFJ',
    name: 'broadcastFJ',
    component: () => import('../components/ggjcFJ/broadcastFJ'),
    meta: ['广告监测', '广播']
  }
]
export default ggjcFJ
