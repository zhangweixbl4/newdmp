<?php

namespace Test\Controller;
use Think\Controller;
use Think\Exception;

class Test4Controller extends Controller{

	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		//echo date('Y-m-d H:i:s')."<br />\n";
		
	}	
	public function bcc(){
		
		
		
		$dataList = M('tbcsample')->field('fid')->where(array('fsource'=>3,'fcuted'=>-1))->limit(500)->select();
		$urlArr = array();
		foreach($dataList as $data) {
			$urlArr[] = U('Test/Test4/bcc2@'.$_SERVER['HTTP_HOST'],array('fid'=>$data['fid']));
		}
		


		
		if($urlArr){
			$retArr = A('Common/System','Model')->async_get_node($urlArr);
		}
		
		//var_dump($retArr);
		
	}
	
	public function bcc2(){
		$fid = I('fid');
		$samInfo = M('tbcsample')->field('fid,favifilename')->where(array('fid'=>$fid))->find();
		
		$avinfo = json_decode(file_get_contents($samInfo['favifilename'].'?avinfo'),true);
		if($avinfo['streams']){
			$fcuted = 2;
		}else{
			$fcuted = 0;
		}
		
		M('tbcsample')->where(array('fid'=>$fid))->save(array('fcuted'=>$fcuted));
		echo $fcuted;
		
		
	}
	
	public function ali_up(){
		
		$ret = A('Common/AliyunOss','Model')->file_up('LOG/http_log20180911.log','LOG/http_log20180911.log',array('Content-Disposition' => 'attachment;filename=555555555.log'));
		
		var_dump($ret);
		
	}
	  
	public function tv(){

		session_write_close();
		set_time_limit(6000);
		//M()->startTrans();//开始事务
		$issueList = M('ttvissue')
									->field('
												ttvissue.ftvissueid as fid,
												ttvissue.ftvsampleid as samid,
												ttvissue.fstarttime,
												ttvissue.fmediaid,
												ttvsample.fmediaid as sam_mediaid,
												ttvissue.fregionid
											')
									->join('ttvsample on ttvsample.fid = ttvissue.ftvsampleid and ttvissue.fmediaid <> ttvsample.fmediaid')
									->where('ttvissue.fissuedate  BETWEEN "2018-06-01" and "2018-06-31"')
									->limit(20000)
									->select();//查询有问题的发布记录
		//echo M('ttvissue')->getLastSql();
		
		foreach($issueList as $issue){
			$uuid = M('ttvsample')->where(array('fid'=>$issue['samid']))->getField('uuid');//查询该发布记录的uuid
			$newSamId = M('ttvsample')->where(array('uuid'=>$uuid,'fmediaid'=>$issue['fmediaid']))->getField('fid');//查询uuid和媒介id能对应上的样本id
			
			if($newSamId){//判断是否有获取到样本id
				
			}else{
				$samInfo = M('ttvsample')->where(array('uuid'=>$uuid))->find();//通过uuid去找到另一个样本
				unset($samInfo['fid']);
				$samInfo['fmediaid'] = $issue['fmediaid'];//修改媒介id
				$newSamId = M('ttvsample')->add($samInfo);//添加新的样本
				file_put_contents('LOG/newSamId',$newSamId."\n",FILE_APPEND);
			}
			$issue_table = 'ttvissue_'.date('Ym',strtotime($issue['fstarttime'])).'_'.substr($issue['fregionid'],0,2);
			$e_l = M('ttvissue')->where(array('fmediaid'=>$issue['fmediaid'],'fstarttime'=>$issue['fstarttime']))->save(array('ftvsampleid'=>$newSamId));
			
			$e_m = M($issue_table)->where(array('fmediaid'=>$issue['fmediaid'],'fstarttime'=>strtotime($issue['fstarttime'])))->save(array('fsampleid'=>$newSamId));
			//echo M($issue_table)->getLastSql()."\n";
			
			echo $issue['fid'].'	修改了	'.$e_l.'	条大表记录,修改了	'.$e_m.'	条小表记录	'."<br />\n";
			
		}
		
		//M()->rollback();//回滚事务
		//M()->commit();//提交事务
		
		
	}


	public function bc(){

		session_write_close();
		set_time_limit(6000);
		//M()->startTrans();//开始事务
		$issueList = M('tbcissue')
									->field('
												tbcissue.fbcissueid as fid,
												tbcissue.fbcsampleid as samid,
												tbcissue.fstarttime,
												tbcissue.fmediaid,
												tbcsample.fmediaid as sam_mediaid,
												tbcissue.fregionid
											')
									->join('tbcsample on tbcsample.fid = tbcissue.fbcsampleid and tbcissue.fmediaid <> tbcsample.fmediaid')
									->where('tbcissue.fissuedate BETWEEN "2018-06-01" and "2018-06-31"')
									->limit(20000)
									->select();//查询有问题的发布记录
		//echo M('tbcissue')->getLastSql();
		
		foreach($issueList as $issue){
			$uuid = M('tbcsample')->where(array('fid'=>$issue['samid']))->getField('uuid');//查询该发布记录的uuid
			$newSamId = M('tbcsample')->where(array('uuid'=>$uuid,'fmediaid'=>$issue['fmediaid']))->getField('fid');//查询uuid和媒介id能对应上的样本id
			
			if($newSamId){//判断是否有获取到样本id
				
			}else{
				$samInfo = M('tbcsample')->where(array('uuid'=>$uuid))->find();//通过uuid去找到另一个样本
				unset($samInfo['fid']);
				$samInfo['fmediaid'] = $issue['fmediaid'];//修改媒介id
				$newSamId = M('tbcsample')->add($samInfo);//添加新的样本
				file_put_contents('LOG/newSamId',$newSamId."\n",FILE_APPEND);
			}
			$issue_table = 'tbcissue_'.date('Ym',strtotime($issue['fstarttime'])).'_'.substr($issue['fregionid'],0,2);
			$e_l = M('tbcissue')->where(array('fmediaid'=>$issue['fmediaid'],'fstarttime'=>$issue['fstarttime']))->save(array('fbcsampleid'=>$newSamId));
			
			$e_m = M($issue_table)->where(array('fmediaid'=>$issue['fmediaid'],'fstarttime'=>strtotime($issue['fstarttime'])))->save(array('fsampleid'=>$newSamId));
			//echo M($issue_table)->getLastSql()."\n";
			
			echo $issue['fid'].'	修改了	'.$e_l.'	条大表记录,修改了	'.$e_m.'	条小表记录	'."<br />\n";
			
		}
		
		//M()->rollback();//回滚事务
		//M()->commit();//提交事务
		
		
	}


	
	
	
	
	
	

}