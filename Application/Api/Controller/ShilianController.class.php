<?php
namespace Api\Controller;
use Think\Controller;

class ShilianController extends Controller {
	

	
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}
		
	/*接收拾联的m3u8索引消息推送2*/
	public function m3u8_index(){
		ini_set('memory_limit','1024M');
		
		$msectime = msectime();
		$get_client_ip = get_client_ip();

		session_write_close();

		$ots_w_num = 0;//初始化写入成功的条数
		$post_data = file_get_contents('php://input');//获取post数据
		#file_put_contents('LOG/m3u8_index',$post_data);
		$post_data = json_decode($post_data,true);//转为数组
		if($post_data['type'] != 'ts_list'){//判断推送类型
			exit('type error');
		}
		
		$ts_list = $post_data['itemes'];
		$m3u8_index_count = count($ts_list);//发送过来的数据总条数
		

		$dataList = array();//写入tablestore的数据
		$getMediaIdTime = 0;
		foreach($ts_list as $ts_list_k=>$ts){//循环list
			
			$a_data = array();
			$issue_date = date('Y-m-d',$ts['start_ts']);//发布日期
			$getMediaId_msectime = msectime();
			#$fmediaid = M('tmedia')->cache(true,600)->where(array('fdpid'=>$ts['chan_id']))->getField('fid');//查询媒介id
			$fmediaid = A('Common/Media','Model')->fdpid_to_fmediaid($ts['chan_id']);
			$getMediaIdTime += msectime() - $getMediaId_msectime;
			if(!$fmediaid) continue;

			
			$ts['url'] = str_replace('ovsc84o05.com0.z0.glb.clouddn.com','qiniusource.hz-data.xyz',$ts['url']);
			$ts['url'] = str_replace('shilian-yunhe2.oss-cn-hangzhou.aliyuncs.com','shilian-yunhe2.oss-accelerate.aliyuncs.com',$ts['url']);
			$dataList[] = array ( // 第一行
							"operation_type" => 'PUT',            //操作是PUT
							'condition' => 'IGNORE',
							'primary_key' => array (
								array('fmediaid',intval($fmediaid)),
								array('start_time',intval($ts['start_ts'])),
								array('end_time',intval($ts['end_ts'])),
								array('fdate',$issue_date),
								
							),
							'attribute_columns' => array(array('ts_url',$ts['url']),array('create_time',date('Y-m-d H:i:s'))),
						);//组装写入tablestore的数据结构
			if(	count($dataList) == 200){//判断数据是否达到200条，因为table的最大限制是一次性写入200条
				$response = A('Common/TableStore','Model')->batchWriteRow('ts_key',$dataList);//写入数据到tablestore
				$ots_w_num += $response['successNum'];//写入ots成功的数量
				$dataList = array();
				
			}		

		}
		
		if(	count($dataList) > 0){
			$response = A('Common/TableStore','Model')->batchWriteRow('ts_key',$dataList);//写入数据到tablestore
			$ots_w_num += $response['successNum'];//写入ots成功的数量
			$dataList = array();
			
		}

		
		$e_time = msectime() - $msectime;
		
		if($m3u8_index_count < 5000){
			$wait_time = 0;
		}else{
			$wait_time = 0;
		}
		

		
		A('Common/AliyunLog','Model')->dmpLogs2('m3u8_index_count',array(
																'm3u8_index_count'=>$m3u8_index_count,
																
																'ots_w_num'=>$ots_w_num,
																'getMediaIdTime'=>$getMediaIdTime,
																'get_client_ip'=>$get_client_ip,
																'e_time'=>$e_time,
																
																));//记录日志
		
		
		if($ots_w_num > 0){//判断写入成功数量是否大于0
			$this->ajaxReturn(array('ret'=>0,'wait_time'=>$wait_time));
		}else{
			$this->ajaxReturn(array('ret'=>-1,'wait_time'=>$wait_time));
		}
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	/*接收来自拾联的消息推送*/
	public function shilian_msg(){
		
		
		$data = file_get_contents('php://input');
		//file_put_contents('LOG/shilian_msg',$data."\n",FILE_APPEND);
		
		$data = json_decode($data,true);
		if($data['alarm_content'] == '视频丢失') exit;
		
		if($data['type'] == 'state'){//设备上下线通知推送
			$dev_serial = $data['dev_serial'];
			$state = $data['state'];
			M('tdevice')->where(array('fcode'=>$dev_serial))->save(array('fonline'=>$state));
			
			$a_data = array();
			$a_data['media_id'] = 0;
			$a_data['abnormal_code'] = 0;//异常代码
			$a_data['creator'] = 'shilianApi';//创建人
			$a_data['createtime'] = date('Y-m-d H:i:s');

			$a_data['abnormal_time'] = time();
			if($a_data['state'] == 1) $abnormal_type = '云盒上线';
			if($a_data['state'] == 0) $abnormal_type = '云盒下线';
			
			$a_data['abnormal_content'] = $abnormal_type;
			$a_data['device_code'] = $data['dev_serial'];

			$a_ret = M('media_abnormal')->add($a_data);

		}elseif($data['type'] == 'alarm' && $data['chan_id']){

			
			$mediaInfo = M('tmedia')->cache(true,600)->where(array('fdpid'=>$data['chan_id']))->find();
			$media_id = $mediaInfo['fid'];//媒介id
			
			if(!$mediaInfo){
				return false;
			}
			$abnormal_type = $data['alarm_content'];//异常类型
			
			if($abnormal_type == ''){
				return false;
			}

			
			$a_data = array();
			$a_data['media_id'] = $media_id;
			$a_data['abnormal_code'] = $data['alarm_type'];//异常代码
			$a_data['creator'] = 'shilianApi';//创建人
			$a_data['createtime'] = date('Y-m-d H:i:s');

			$a_data['abnormal_time'] = strtotime($data['alarmTime']);
			$a_data['abnormal_content'] = $abnormal_type;
			$a_data['device_code'] = $data['dev_serial'];
			
			

			$a_ret = M('media_abnormal')->add($a_data);
			
			$media_abnormal_4 = array();
			
			$media_abnormal_4['fmediaid'] = $media_id;
			$media_abnormal_4['fissuedate'] = date('Y-m-d',strtotime($data['alarmTime']));
			$media_abnormal_4['fstarttime'] = strtotime($data['alarmTime']);
			$media_abnormal_4['duration'] = 30;
			$media_abnormal_4['fremark'] = $abnormal_type;
			if(strstr($abnormal_type,'系统时间变化')){
				$media_abnormal_4['type'] = 33;
			}elseif(strstr($abnormal_type,'网络出现故障')){
				$media_abnormal_4['type'] = 34;
			}elseif(strstr($abnormal_type,'向平台获取token失败')){
				$media_abnormal_4['type'] = 35;
			}elseif(strstr($abnormal_type,'上传失败')){
				$media_abnormal_4['type'] = 36;
			}else{
				$media_abnormal_4['type'] = -1;
			}
			
			M('media_abnormal_4')->add($media_abnormal_4);
			
			
			
			
			
			
			//$dmpLogs2 = A('Common/AliyunLog','Model')->dmpLogs2('media_abnormal',$a_data);//写入日志库			
			
			
		}
		
	}
	


	
	/*注册消息推送接收地址*/
	public function set_msg_api(){
		$push_url = 'http://'.$_SERVER['HTTP_HOST'].'/Api/Shilian/shilian_msg';
		$rr = A('Common/Shilian','Model')->set_msg_api($push_url);
		var_dump($rr);
	}
	
	
	
	/*获取 不在线设备*/
	public function device_state(){
		header("Content-type: text/html; charset=utf-8");
		
		$deviceList = M('tdevice')->field('fid,fcode,fname,device_no')->where(array('fdevicetypeid'=>3))->select();
		
		
		$dpids = array();
		
		foreach($deviceList as $device){
			
			$dpids[] = $device['device_no'];
			//echo $device['fcode'].'	'.$device['fname'].'	'."\n";

		}
		
		//var_dump($dpids);
		$device_checkonline = A('Common/Shilian','Model')->device_checkonline($dpids);
		//var_dump($device_checkonline);
		foreach($device_checkonline['state'] as $key => $device_online){
			if($device_online){
				$online = 1;
			}else{
				$online = 0;
			}
			echo $deviceList[$key]['fcode'].'	'.$deviceList[$key]['fname'].'	'.$online."\n";
			M('tdevice')->where(array('fid'=>$deviceList[$key]['fid']))->save(array('fonline'=>$online));
		}
		
	}
	
	
	
	/*获取拾联存放在七牛的文件列表*/
	public function get_list_files(){
		session_write_close();
		$msectime = msectime();
		$prefix = I('prefix');

		if($prefix == ''){
			exit;
			$prefix = null;
		}
		$fileList = array();
		$limit = 1000;
		for($qi=0;$qi<1;$qi++){

			$marker = A('Common/System','Model')->important_data('qiniu_ts_marker'.$prefix);
			$qiniu_ret = A('Common/Qiniu','Model')->get_list_files('hzsj-video-nb',$marker,$limit,$prefix);
			$marker = $qiniu_ret['marker'];
			//echo base64_decode($marker);
			
			
			if(!$qiniu_ret['marker']){
				exit();
			}
			A('Common/System','Model')->important_data('qiniu_ts_marker'.$prefix,$marker);
			$fileList = array_merge($fileList,$qiniu_ret['items']);
		}	


		
		
		$dataList = array();//写入tablestore的数据
		$ots_w_num = 0;
		foreach($fileList as $file){//循环list
			
			$tem1 = explode('/',$file['key']);//按斜杠转数组
			$fdpid = $tem1[0];
			$tem1 = array_reverse($tem1);//按斜杠转数组
			
		
			
			
			
			$tem1 = explode('.',$tem1[0]);//按点转数组
			$tem1 = explode('-',$tem1[0]);//按横转数组

			$startTime = $tem1[0];//开始时间
			$endTime = $tem1[1];//结束时间
			
			
			$a_data = array();
			$issue_date = date('Y-m-d',$startTime);//发布日期
			if($startTime > strtotime('2018-08-17')){
				file_put_contents('LOG/get_list_files',$fdpid."\n",FILE_APPEND);
				exit;
				
			}
			
			$fmediaid = M('tmedia')->cache(true,60)->where(array('fdpid'=>$fdpid))->getField('fid');//查询媒介id
			if(!$fmediaid) continue;

			
			
			$dataList[] = array ( // 第一行
							"operation_type" => 'PUT',            //操作是PUT
							'condition' => 'IGNORE',
							'primary_key' => array (
								array('fmediaid',intval($fmediaid)),
								array('start_time',intval($startTime)),
								array('end_time',intval($endTime)),
								array('fdate',$issue_date),
								
							),
							'attribute_columns' => array(array('ts_url','http://ovsc84o05.com0.z0.glb.clouddn.com/'.$file['key']),array('create_time',date('Y-m-d H:i:s'))),
						);//组装写入tablestore的数据结构
			if(	count($dataList) == 200){//判断数据是否达到200条，因为table的最大限制是一次性写入200条
				$response = A('Common/TableStore','Model')->batchWriteRow('ts_key',$dataList);//写入数据到tablestore
				$ots_w_num += $response['successNum'];//写入ots成功的数量
				$dataList = array();
				
			}		

		}
		
		if(	count($dataList) > 0){
			$response = A('Common/TableStore','Model')->batchWriteRow('ts_key',$dataList);//写入数据到tablestore
			$ots_w_num += $response['successNum'];//写入ots成功的数量
			$dataList = array();
			
		}
		
		
		
		
		
		
		echo '执行时间:'.(msectime() - $msectime) . "毫秒\n" ;
		
		echo '写入成功的数量:'.$ots_w_num."\n";

		echo base64_decode($marker);
		
		
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}