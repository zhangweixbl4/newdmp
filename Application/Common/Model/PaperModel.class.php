<?php

namespace Common\Model;
use Think\Exception;

class PaperModel{
	public function get_source($samId){
		$sourceInfo = M('tpapersample')->field('tpapersource.furl')->join('tpapersource on tpapersource.fid = tpapersample.sourceid')->where(array('tpapersample.fpapersampleid'=>$samId))->find();
		return $sourceInfo['furl'];
	}

    public function get_slim_source($samId)
    {
        return $this->get_source($samId) . '?imageView2/2/w/1600';
	}
}