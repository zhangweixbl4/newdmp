<?php
namespace Api\Controller;
use Think\Controller;
class AdclassController extends Controller {
	/*获取广告类别列表*/
    public function get_adclass_list(){
		$fcode = I('fcode','');//获取fcode
		if($fcode == 0) $fcode = '';
		$adclassList = M('tadclass')->cache(true,86400)->field('fcode,fadclass')->where(array('fpcode'=>$fcode,'fcode'=>array('neq','0')))->select();//查询广告分类列表
		$adclassDetails = M('tadclass')->cache(true,86400)->field('fcode,left(fadclass,4) as fadclass,ffullname')->where(array('fcode'=>$fcode))->find();//查询广告分类详情
		if(!$adclassDetails) $adclassDetails = array('fcode'=>'','fadclass'=>'全部','ffullname'=>'全部');
		$this->ajaxReturn(array('code'=>0,'adclassList'=>$adclassList,'adclassDetails'=>$adclassDetails));
	}
    /**
     * 根据商业分类获取广告分类
     * @param $code     int     商业广告分类代码
     */
    public function getAdClassA($code)
    {
        $data = M('hz_ad_class')->where(['fcode' => ['EQ', $code]])->getField('fcode1');
        if ($data){
            $this->ajaxReturn([
                'code' => 0,
                'data' => $data
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
            ]);
        }
    }
    /**
     * @Des: 获取广告类别树
     * @Edt: yuhou.wang
     * @param {type} 
     * @return: 
     */
    public function getAdClassTree(){
        header('Access-Control-Allow-Origin:*');//允许跨域
        $adClassTree = A('Common/AdClass','Model')->getAdClassList();
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$adClassTree]);
    }
    /**
     * @Des: 获取商业类别树
     * @Edt: yuhou.wang
     * @param {type} 
     * @return: 
     */
    public function getAdClassBTree(){
        header('Access-Control-Allow-Origin:*');//允许跨域
        $adClassTree = A('Common/AdClass','Model')->getAdClassBList();
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$adClassTree]);
    }
}