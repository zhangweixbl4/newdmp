<?php
/**
 * 基础控制器
 */
namespace Open\Controller;
use Think\Controller;

class BaseController extends Controller {
	public function _initialize() {
		A('Common/AliyunLog','Model')->dmpLogs();//写入日志
	}
}