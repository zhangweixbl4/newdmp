<?php
namespace Admin\Controller;
use Think\Controller;
class FankuiquestionController extends BaseController {

    //反馈列表
	public function index(){
        $title = I('title');//标题
        $fstatus = I('fstatus');//状态（0未处理，10已处理，20已回复）
        $s_time = I('s_time');//开始时间
        $e_time = I('e_time');//结束时间
        $where = [];
        if($title != ''){
            $where['ftitle'] = array('like','%'.$title.'%');
        }

        if($fstatus != ''){
            $where['fstatus'] = $fstatus;
        }

        if($s_time != '' && $e_time != ''){
            $where['fcreatetime'] = array('BETWEEN',array($s_time,$e_time));
        }else{
            if($s_time != ''){
                $where['fcreatetime'] = array('GT',$s_time);
            }
            if($e_time != ''){
                $where['fcreatetime'] = array('LT',$e_time);
            }
        }
        $fankui_model = M('tbn_question');
        $fankui_list = $fankui_model
            ->where($where)
            ->order('fcreatetime desc')
            ->page($_GET['p'].',15')
            ->select();//文书模板列表
        $count      = $fankui_model->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,15);// 实例化分页类 传入总记录数和每页显示的记录数
        $show       = $Page->show();// 分页显示输出
        $this->assign('page',$show);// 赋值分页输出
        $this->assign('fankui_list',$fankui_list);
        $this->assign('file_up',file_up());
        $this->display();
	}

	//反馈回复
    public function reply(){
        $id = I('fid');//结束时间
        $question = M('tbn_question')->where(['fid'=>$id])->find();
        if($question){
            $data = [
                'code'=>0,
                'question'=>$question,
            ];
        }else{
            $data = [
                'code'=>-1,
                'msg'=>'数据不存在!'
            ];
        }

        $this->ajaxReturn($data);
    }

    //修改回复
    public function post_reply(){
        $date = I('post.');//
        $tbn_question = M('tbn_question');

        /*dump();exit;
        $question = $tbn_question->where(['fid'=>$date['fid']])->find();*/

        $date['fretime'] = date('Y-m-d H:i:s');
        $date['fstatus'] = 20;
        $user = session('personInfo');
        $date['freuserid'] =  $user['fid'];
        $date['freusername'] = $user['fname'];
        $res = $tbn_question->where(['fid'=>$date['fid']])->save($date);
        if($res !== false){
            $this->ajaxReturn([
                'code'=>0,
                'msg'=>'回复成功！',
            ]);
        }else{
            $this->ajaxReturn([
                'code'=>-1,
                'msg'=>'回复失败！',
            ]);
        }
    }

    //处理
    public function deal_with(){
        $id = I('fid');
        $tbn_question = M('tbn_question');
        $res = $tbn_question->where(['fid'=>$id])->save(['fstatus'=>10]);
        if($res !== false){
            $this->ajaxReturn([
                'code'=>0,
                'msg'=>'操作成功！',
            ]);
        }else{
            $this->ajaxReturn([
                'code'=>-1,
                'msg'=>'操作失败！',
            ]);
        }
    }

    //删除反馈
    public function delete(){
        $id = I('fid');//id
        $tbn_question = M('tbn_question');
        $question = $tbn_question->where(['fid'=>$id])->find();
        if(!$question){
            $this->ajaxReturn([
                'code'=>-1,
                'msg'=>'数据不存在！',
            ]);
        }
        if($question['fstatus'] == 0){
            $this->ajaxReturn([
                'code'=>-1,
                'msg'=>'未处理不能删除！',
            ]);
        }else{
            $res = $tbn_question->where(['fid'=>$id])->save(['delete_time'=>date('Y-m-d H:i:s')]);
            if($res !== false){
                $this->ajaxReturn([
                    'code'=>0,
                    'msg'=>'删除成功！',
                ]);
            }else{
                $this->ajaxReturn([
                    'code'=>-1,
                    'msg'=>'删除失败！',
                ]);
            }
        }
    }

}