<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 线索跟踪
 */

class FxsgenzongController extends BaseController
{
	/**
	 * 获取进行中的线索列表
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function index() {
		session_write_close();

		$p 	= I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录

		$where['fstate'] 	= array(array('neq',10),array('neq',40),'and');
		$where['_string'] 	= 'fid in (select fregisterid from tregisterflow where fcreateregualtorpersonid='.session('regulatorpersonInfo.fid').' group by fregisterid)';//用户处理权限范围

		$fname 			= I('fname');//登记表名
		$fmodifytime 	= I('fmodifytime');//登记时间
		if(!empty($fname)){
			$where['fname'] = array('like','%'.$fname.'%');
		}
		if(!empty($fmodifytime)){
			$where['_string'] .= ' and datediff("'.$fmodifytime.'",fregistertime)=0';
		}

		$count = M('tregister')
			->where($where)
			->count();//查询满足条件的总记录数

		
		$data = M('tregister')
			->where($where)
			->order('fid desc')
			->page($p,$pp)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	/**
	 *线索督办操作
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function supervise_register(){

		$fid = I('fid');//登记信息ID
		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}

		$where['fid'] 		= $fid;//登记表ID
		$where['fstate'] 	= array(array('neq',10),array('neq',40),'and');
		$where['_string'] 	= 'fid in (select fregisterid from tregisterflow where fcreateregualtorpersonid='.session('regulatorpersonInfo.fid').' group by fregisterid)';//用户处理权限范围

		$data = M('tregister')->where($where)->find();//获取当前登记信息
		if(!empty($data)){
			/*新增流程记录*/
			$sh_list['fregisterid'] 				= $fid;
			$sh_list['fupflowid'] 					= 0;
			$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
			$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
			$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
			$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
			$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
			$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
			$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
			$sh_list['fflowname'] 					= '线索跟踪';
			$sh_list['freason'] 					= '处置催办';
			$sh_list['fregulatorid'] 				= session('regulatorpersonInfo.fregulatorid');
			$sh_list['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
			$sh_list['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
			$sh_list['ffinishtime']					= date('Y-m-d H:i:s');//处理时间
			$sh_list['fstate'] 						= 50;
			$flowid = M('tregisterflow')->add($sh_list);
			if(!empty($flowid)){
				M()->execute('update document set dt_flowid='.$flowid.' where dt_tregisterid='.$fid.' and dt_flowid=0 and dt_createpersonid='.session('regulatorpersonInfo.fid'));
				M()->execute('update tregisterfile set fillegaladflowid='.$flowid.' where fregisterid='.$fid.' and fillegaladflowid=0 and fcreateregualtorid='.session('regulatorpersonInfo.fregulatorid'));
			}

			//待写入的登记信息--公共
			$res['fsupervisestate'] 	= $data['fsupervisestate']+1;//该案件被监督量
			$trdo =  M('tregister')->where(['fid'=>$fid])->save($res);
			if(!empty($trdo) && !empty($flowid)){
				D('Function')->write_log('线索跟踪',1,'督办操作成功','tregister',$trdo,M('tregister')->getlastsql());
				$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
			}else{
				D('Function')->write_log('线索跟踪',0,'督办操作失败','tregister',$fid,M('tregister')->getlastsql());
				$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
			}
		}else{
			D('Function')->write_log('线索跟踪',0,'督办操作失败','tregister',$fid,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'.M('tregister')->getlastsql()));
		}
	}

	/**
	 *线索处置催办操作
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function reminder_register(){

		$fid = I('fid');//登记信息ID
		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}

		$where['fid'] 		= $fid;//登记表ID
		$where['fstate'] 	= array(array('neq',10),array('neq',40),'and');
		$where['_string'] 	= 'fid in (select fregisterid from tregisterflow where fcreateregualtorpersonid='.session('regulatorpersonInfo.fid').' group by fregisterid)';//用户处理权限范围

		$data = M('tregister')->where($where)->find();//获取当前登记信息
		if(!empty($data)){
			/*新增流程记录*/
			$sh_list['fregisterid'] 				= $fid;
			$sh_list['fupflowid'] 					= 0;
			$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
			$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
			$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
			$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
			$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
			$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
			$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
			$sh_list['fflowname'] 					= '线索跟踪';
			$sh_list['freason'] 					= '处置催办';
			$sh_list['fregulatorid'] 				= session('regulatorpersonInfo.fregulatorid');
			$sh_list['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
			$sh_list['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
			$sh_list['ffinishtime']					= date('Y-m-d H:i:s');//处理时间
			$sh_list['fstate'] 						= 60;
			$flowid = M('tregisterflow')->add($sh_list);
			if(!empty($flowid)){
				M()->execute('update document set dt_flowid='.$flowid.' where dt_tregisterid='.$fid.' and dt_flowid=0 and dt_createpersonid='.session('regulatorpersonInfo.fid'));
				M()->execute('update tregisterfile set fillegaladflowid='.$flowid.' where fregisterid='.$fid.' and fillegaladflowid=0 and fcreateregualtorid='.session('regulatorpersonInfo.fregulatorid'));
			}

			//待写入的登记信息--公共
			$res['freminderstate'] 	= $data['freminderstate']+1;//该案件被催办量
			$trdo =  M('tregister')->where(['fid'=>$fid])->save($res);
			if(!empty($trdo) && !empty($flowid)){
				D('Function')->write_log('线索跟踪',1,'催办操作成功','tregister',$trdo,M('tregister')->getlastsql());
				$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
			}else{
				D('Function')->write_log('线索跟踪',0,'催办操作失败','tregister',0,M('tregister')->getlastsql());
				$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
			}
		}else{
			D('Function')->write_log('线索跟踪',0,'催办操作失败','tregister',0,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
		}
	}

	/**
	 *线索执法监督操作
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function enforcement_register(){

		$fid = I('fid');//登记信息ID
		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}

		$where['fid'] 		= $fid;//登记表ID
		$where['fstate'] 	= array(array('neq',10),array('neq',40),'and');
		$where['_string'] 	= 'fid in (select fregisterid from tregisterflow where fcreateregualtorpersonid='.session('regulatorpersonInfo.fid').' group by fregisterid)';//用户处理权限范围

		$data = M('tregister')->where($where)->find();//获取当前登记信息
		if(!empty($data)){
			/*新增流程记录*/
			$sh_list['fregisterid'] 				= $fid;
			$sh_list['fupflowid'] 					= 0;
			$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
			$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
			$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
			$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
			$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
			$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
			$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
			$sh_list['fflowname'] 					= '线索跟踪';
			$sh_list['freason'] 					= '处置催办';
			$sh_list['fregulatorid'] 				= session('regulatorpersonInfo.fregulatorid');
			$sh_list['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
			$sh_list['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
			$sh_list['ffinishtime']					= date('Y-m-d H:i:s');//处理时间
			$sh_list['fstate'] 						= 70;
			$flowid = M('tregisterflow')->add($sh_list);

			if(!empty($flowid)){
				M()->execute('update document set dt_flowid='.$flowid.' where dt_tregisterid='.$fid.' and dt_flowid=0 and dt_createpersonid='.session('regulatorpersonInfo.fid'));
				M()->execute('update tregisterfile set fillegaladflowid='.$flowid.' where fregisterid='.$fid.' and fillegaladflowid=0 and fcreateregualtorid='.session('regulatorpersonInfo.fregulatorid'));
			}

			//待写入的登记信息--公共
			$res['fenforcementstate'] 	= $data['fenforcementstate']+1;//该案件被执法监督量
			$trdo =  M('tregister')->where(['fid'=>$fid])->save($res);
			if(!empty($trdo) && !empty($flowid)){
				D('Function')->write_log('线索跟踪',1,'执法监督操作成功','tregister',$trdo,M('tregister')->getlastsql());
				$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
			}else{
				D('Function')->write_log('线索跟踪',0,'执法监督操作失败','tregister',0,M('tregister')->getlastsql());
				$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
			}
		}else{
			D('Function')->write_log('线索跟踪',0,'执法监督操作失败','tregister',0,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
		}
	}

	/**
	 * 获取线索台账列表
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function getledger() {
		session_write_close();
		
		$p 	= I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录

		$where['fstate'] 	= array('neq',10);
		$where['_string'] 	= 'fid in (select fregisterid from tregisterflow where fcreateregualtorpersonid='.session('regulatorpersonInfo.fid').' group by fregisterid)';//用户处理权限范围

		$fname 			= I('fname');//登记表名
		$fmodifytime 	= I('fmodifytime');//登记时间
		if(!empty($fname)){
			$where['fname'] = array('like','%'.$fname.'%');
		}
		if(!empty($fmodifytime)){
			$where['_string'] .= ' and datediff("'.$fmodifytime.'",fregistertime)=0';
		}
		
		$count = M('tregister')
			->where($where)
			->count();//查询满足条件的总记录数

		
		$data = M('tregister')
			->where($where)
			->order('fid desc')
			->page($p,$pp)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

}