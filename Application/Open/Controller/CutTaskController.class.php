<?php
namespace Open\Controller;
use Think\Controller;

class CutTaskController extends BaseController {

	public function index(){



	}
	
	
	
	/*获取任务*/
	public function getTask(){
		
		$fstate = I('get.fstate');
		$fmedianame = I('get.fmedianame');
		$cut_task_id = I('get.cut_task_id');
		$source_class = I('get.source_class');
		$wx_id = intval(I('get.wx_id'));
		$limit = I('get.limit');
		if($limit < 1) $limit = 1;
		if($limit > 100) $limit = 100;
		
		if($wx_id > 0){//是否传入wxid
			
			$userGroupInfo = M('ad_input_user_group')
												->cache(true,60)
												->field('
														ad_input_user_group.mediaauthority
												
														')
												->join('ad_input_user on ad_input_user.group_id = ad_input_user_group.group_id')
												->where(array('ad_input_user.wx_id'=>$wx_id))
												->find();

			
		}
		
		
		
		
		
		if($fstate == ''){//判断参数是否正确
			$this->ajaxReturn(array('code'=>-1,'msg'=>'fstate error'));
		}
			
		
		
		
		
		$where = array();
		$where['cut_task.fstate'] = $fstate;
		if($fstate == 0){
			$where['cut_task.start_make_time'] = array('lt',time());
			
			
			$count2 = M('cut_task')
								->where(array('cut_complete_time'=>array('lt',time()-3600*5),'fstate'=>1))
								->save(array('fstate'=>0));//修改任务状态为1的任务
		}
		
		
		
		if($cut_task_id != ''){
			$where['cut_task_id'] = $cut_task_id;//任务ID
		}
		
		if($source_class){
			$where['source_class'] = $source_class;//素材类型
			
		}elseif(!$wx_id){
			$where['source_class'] = 'aac';//素材类型
		}
		
		

		if($fmedianame != ''){
			$fmedianame = str_replace(' ','%',$fmedianame);
			$where_media_id_list = M('tmedia')->cache(true,600)->where(array('fmedianame'=>array('like','%'.$fmedianame.'%')))->getField('fid',true);
			//var_dump($where_media_id_list);
			if(count($where_media_id_list) > 0){
				$where['fmediaid'][] = array('in',$where_media_id_list);
			}else{
				$where['fmediaid'][] = array('eq',0);
			}
			
		} 
		
		
		
		/* if ($userGroupInfo['mediaauthority'] !== 'unrestricted' && $wx_id > 0 && in_array(intval($fstate),array(2,3))){
            if (!$userGroupInfo['mediaauthority'] || $userGroupInfo['mediaauthority'] === '""'){
                return false;
            }
            $mediaIds = array_filter(json_decode($userGroupInfo['mediaauthority'], true));
			if(count($mediaIds) > 0){
				$where['fmediaid'][] = ['IN', $mediaIds];
			}else{
				$where['fmediaid'][] = array('eq',0);	
			}			
        } */
		
		/* if($wx_id > 0){
			$wxInfo = M('ad_input_user')->cache(true,1)->field('wx_id,prefer_list')->where(array('wx_id'=>$wx_id))->find();
			$prefer_list = array_filter(explode(',',$wxInfo['prefer_list']));
		}
		if(count($prefer_list) > 0){
			$where['fmediaid'][] = ['IN', $prefer_list];
		} */
		
		
		if($wx_id > 0 && in_array(intval($fstate),array(2,3))){
			$tqc_media_permission = array_filter( M('tqc_media_permission ')->cache(true,60)->where(array('wx_id'=>$wx_id))->order('fpriority desc')->getField('fmediaid',true));
		
			if(count($tqc_media_permission) > 0){
				$where['fmediaid'][] = ['IN', $tqc_media_permission];
			}else{
				$where['fmediaid'][] = ['eq', 0];
			}
		}
		
		
		
		
		
		
		$taskList = M('cut_task')
								->field('
											cut_task.cut_task_id,
											cut_task.fmediaid,
											cut_task.fissuedate,
											cut_task.fstarttime,
											cut_task.fendtime,
											cut_task.fsourcefilename,
											cut_task.fstate,
											cut_task.source_class,
											cut_task.time_list,
											cut_task.m3u8_url,
											cut_task.fremark,
											cut_task.cut_wx_id,
											cut_task.cut_remark,
											cut_task.cut_complete_time,
											cut_task.priority,
											cut_task.time_list_length,
											cut_task.source_length,
											cut_task.start_make_time,
											cut_task.make_error_count,
											cut_task.make_reco_state,
											cut_task.score_id,
											cut_task.frecfilename,
											
											tmedia.fmedianame,
											0 as fseperatetaskid,
											
											FROM_UNIXTIME(cut_task.fstarttime) as fstarttime,
											FROM_UNIXTIME(cut_task.fendtime) as fendtime,
											
											frecfilename,
											cut_task.fsourcefilename as favifilename,
											
											left(tmedia.fmediaclassid,2) as fmediaclassid

										')
								->join('tmedia on tmedia.fid = cut_task.fmediaid')
								->where($where)
								->order('cut_task.priority desc , cut_task.fissuedate asc')
								->limit($limit)
								->select();
		//var_dump(M('cut_task')->getLastSql());
		
		
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','taskList'=>$taskList,'sql'=>M('cut_task')->getLastSql()));
		
	}
	
	
	/*修改任务状态*/
	public function editTask(){
		$msectime = msectime();
		$postData = file_get_contents('php://input');
		$postData = json_decode($postData,true);
		$cut_task_id = $postData['cut_task_id'];
		$fstate = $postData['fstate'];
		
		$editData = $postData['editData'];
		$editData['cut_complete_time'] = time();
		$lot_content = $editData;
		
		if($lot_content['m3u8_content']) $lot_content['m3u8_content'] = '这里省略....m3u8.....内容';

		
		$taskInfo = M('cut_task')
								->field('
											fmediaid,
											fissuedate,
											fstarttime,
											fendtime,
											start_make_time,
											time_list_length,
											source_length,
											make_error_count,
											fstate
										')
								->where(array('cut_task_id'=>$cut_task_id))->find();//查询任务详情
		
		$flow_id = 0;
		if($editData['fstate'] == 4){//快剪人员提交任务
			//$this->ajaxReturn(array('code'=>-1,'msg'=>'系统正在调整积分，为保证积分一致性，暂停10分钟提交任务'));
			$flow_content = json_encode($lot_content,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
			$flow_id = A('Open/CutTask','Model')->add_flow($cut_task_id,$flow_content,$editData['cut_wx_id'],'快剪人员提交完成任务');//创建流程
		}elseif($editData['fstate'] == 3){//领取任务
			
			$stream_update_time_url = 'http://ay.hz-data.xyz:8002/stream_update_time/'.$taskInfo['fmediaid'].'/'.$taskInfo['fstarttime'].'/'.$taskInfo['fendtime'].'/';	//查询流更新时间的URL				
			
			$stream_update_time = json_decode(http($stream_update_time_url),true)['ts'];//获得流更新时间
			
			if($stream_update_time > $taskInfo['start_make_time']){//如果流更新时间大于任务生成时间
				$editData = array();
				$editData['fstate'] = 0;
				$editData['start_make_time'] = time();
				$flow_content = 'M3U8已更新,触发任务重新生成. 流更新时间:'.date('Y-m-d H:i:s',$stream_update_time).',任务原生成时间:'.date('Y-m-d H:i:s',$taskInfo['start_make_time']);
				$flow_id = A('Open/CutTask','Model')->add_flow($cut_task_id,$flow_content,$editData['cut_wx_id'],'发现流已更新,服务器重新生成');//创建流程
				$rest = true;
			}else{
				$flow_content = json_encode($lot_content,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT).',流更新时间:'.date('Y-m-d H:i:s',$stream_update_time).',任务生成时间:'.date('Y-m-d H:i:s',$taskInfo['start_make_time']);
				$flow_id = A('Open/CutTask','Model')->add_flow($cut_task_id,$flow_content,$editData['cut_wx_id'],'快剪任务领取任务');//创建流程
			}
				
			
			
		}elseif($editData['fstate'] == 1){
			$flow_content = json_encode($lot_content,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
			$flow_id = A('Open/CutTask','Model')->add_flow($cut_task_id,$flow_content,0,'服务器开始生成任务');//创建流程
		}elseif($editData['fstate'] == 2){
			$flow_content = json_encode($lot_content,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
			
			if($editData['cut_wx_id']){
				$flow_short = '快剪任务退出';
			}else{
				$flow_short = '服务器生成完成';
			}
			$flow_id = A('Open/CutTask','Model')->add_flow($cut_task_id,$flow_content,0,$flow_short);//创建流程
		}elseif($editData['fstate'] == -1){//服务器生成失败
			
			$flow_content = json_encode($lot_content,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
			
			$flow_id = A('Open/CutTask','Model')->add_flow($cut_task_id,$flow_content,0,'服务器生成任务失败');//创建流程
			
			$editData['make_error_count'] = array('exp','make_error_count + 1');//生成错误次数加一
			if($taskInfo['make_error_count'] <= 3){//如果错误次数小于等于3次
				$editData['fstate'] = 0;//重新生成任务
				$editData['start_make_time'] = time() + 3600*5;//重置下次生成任务时间
			}elseif($taskInfo['make_error_count'] > 3 && $taskInfo['time_list_length'] > 0){
				$editData['fstate'] = 2; 
			}else{
				$editData['fstate'] = -1; //最终失败
			}
			
			
			
			if(strstr($editData['fremark'],'时长误差过大')){
				$media_abnormal_4_error_type = 32;
			}elseif(strstr($editData['fremark'],'拼接错误')){
				$media_abnormal_4_error_type = 31;
			}else{
				$media_abnormal_4_error_type = -1;
			}
			M('media_abnormal_4')
								->where(array(
												'fmediaid'=>$taskInfo['fmediaid'],
												'fissuedate'=>date('Y-m-d',$taskInfo['fstarttime']),
												'fstarttime'=>$taskInfo['fstarttime'],
												'duration'=>$taskInfo['fendtime'] - $taskInfo['fstarttime']
											))
								->delete();			
			M('media_abnormal_4')->add(array(
											  
											  'fmediaid'=>$taskInfo['fmediaid'],
											  'fissuedate'=>date('Y-m-d',$taskInfo['fstarttime']),
											  'fstarttime'=>$taskInfo['fstarttime'],
											  'duration'=>$taskInfo['fendtime'] - $taskInfo['fstarttime'],
											  'fremark'=>$editData['fremark'],
											  'type'=>$media_abnormal_4_error_type,
											));//写入异常记录表
		}elseif($editData['fstate'] == 5){//改为需要质检的状态
			
		}elseif($editData['fstate'] == 6){//正在质检的状态									
			$flow_content = json_encode($lot_content,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
			$flow_id = A('Open/CutTask','Model')->add_flow($cut_task_id,$flow_content,$editData['inspector'],'获取质检任务');//创建流程
		}elseif($editData['fstate'] == 7){//质检完成									
			$flow_content = json_encode($lot_content,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
			$flow_id = A('Open/CutTask','Model')->add_flow($cut_task_id,$flow_content,$editData['inspector'],'提交质检任务');//创建流程
		}else{
			
		}
		

		
		if($editData['fstate'] == 4){
			
			$source_score_id = M('cut_task')->where(array('cut_task_id'=>$cut_task_id))->getField('score_id');//查询该任务原来的积分id，如果有
			$source_score_info = M('task_input_score')->where(array('id'=>$source_score_id))->find();
			
			if($source_score_info){
				$source_score_info['score'] = 0 - $source_score_info['score'];
				$source_score_info['update_time'] = time();
				$source_score_info['reason'] ='快剪任务退回重新计算积分';
				unset($source_score_info['id']);
				$minus_score_id = M('task_input_score')->add($source_score_info);//添加负分记录
			}
			
			$editData['score_id'] = $this->add_score($cut_task_id,$editData['cut_wx_id']);//重新创建积分 
		}
		

		
		
		$editState = M('cut_task')->where(array('cut_task_id'=>$cut_task_id,'fstate'=>$fstate))->save($editData);

		
		
		$e_time = msectime() - $msectime;
		if($e_time > 1000){//判断执行时间是否大于一秒
			A('Common/AliyunLog','Model')->dmpLogs2('editcuttasktime',array(
																	'cut_task_id'=>$cut_task_id,
																	
																	'editData'=>json_encode($lot_content),
																	
																	'get_client_ip'=>$get_client_ip,
																	'e_time'=>$e_time,
																	
																	));//记录日志
		}
		
		if($editState){//判断是否修改成功
			if($editData['fstate'] == 4 && A('Open/CutTask','Model')->is_task_finish($taskInfo['fmediaid'],$taskInfo['fissuedate'])){
				
				$wwqUrl = 'http://47.96.182.117/index/checkChannelFinish';
				
				$wwqReq = http($wwqUrl,array('channel'=>$taskInfo['fmediaid'],'date'=>$taskInfo['fissuedate']),'POST');
				$wwqLog = '';
				$wwqLog .= date('Y-m-d H:i:s') ."\n";
				$wwqLog .= 'channel:'.$taskInfo['fmediaid'].',date:'.$taskInfo['fissuedate'] ."\n";
				$wwqLog .= $wwqReq . "\n";
				$wwqLog .= "\n\n\n";
				
				
				file_put_contents('LOG/checkChannelFinish',$wwqLog,FILE_APPEND);
				
			
			}
			if($rest) $this->ajaxReturn(array('code'=>-1,'msg'=>'任务需要重新生成,过程可能需要15到30分钟,请先做其它任务','e_time'=>$e_time));
			
			
			
			
			
			if($editData['fstate'] == 4 && rand(0,5) == 3){
				$editState2 = M('cut_task')->where(array('cut_task_id'=>$cut_task_id))->save(array('fstate'=>5));
				if($editState2){
					$flow_id = A('Open/CutTask','Model')->add_flow($cut_task_id,'随机进入抽检库',$editData['cut_wx_id'],'随机进入抽检库');//创建流程
					$this->ajaxReturn(array('code'=>0,'msg'=>'提交成功，根据20%的随机抽检率，该任务已进入抽检库等待抽检','e_time'=>$e_time));
				}
			}
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功','e_time'=>$e_time));
			
			
		}else{
			M('cut_task_flow')->where(array('flow_id'=>$flow_id))->delete();//删除日志
			if($editData['score_id'] || $minus_score_id){
				M('task_input_score')->where(array('id'=>array('in',array($editData['score_id'],$minus_score_id))))->delete();//如果修改失败就要删除积分记录
			}
			
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败','e_time'=>$e_time));
		}
		
		
		
		
		
	}
	
	
	/*获取指定媒体天的任务状态*/
	public function get_media_date(){
		$mediaId = I('mediaId');
		$date = I('date');
		
		$taskList = M('cut_task')->where(array('fmediaid'=>$mediaId,'fissuedate'=>$date))->select();
		
		//var_dump(M('cut_task')->getLastSql());
		
		$media_name = M('tmedia')->cache(true,600)->where(array('fid'=>$mediaId))->getField('fmedianame');
		
		$retData = array();
		foreach($taskList as $task){
			
			$data = array();
			$data['media_name'] = $media_name;
			$data['taskid'] = $task['cut_task_id'];
			$data['time_interval'] = date('H:i',$task['fstarttime']) .'('. (($task['fendtime'] - $task['fstarttime']) / 3600) .'小时)';
			$data['fstate'] = $task['fstate'];
			$data['source_length'] = $task['source_length'];
			$data['time_list_length'] = $task['time_list_length'];
			$data['make_error_count'] = $task['make_error_count'];
			$data['fremark'] = $task['fremark'];
			
			
			$retData[] = $data;
		}
		
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','retData'=>$retData));
	}
	
	#按媒体按时间段查询快剪任务
	public function get_media_dates(){
		$mediaId = I('mediaId');
		$sdate = I('sdate');
		$edate = I('edate');
		$mediaInfo = M('tmedia')->field('fid,left(fmediaclassid,2) as media_class,media_region_id')->where(array('fid'=>$mediaId))->find(); #查询媒介信息
		$cutList = M('cut_task')
						->field()
						->where(array('fmediaid'=>$mediaId,'fissuedate'=>array('between',array($sdate,$edate)),'fstate'=>array('in','0,1,2,3')))
						->group('fissuedate')
						->getField(' fissuedate as date, count(*) as pending_count '); #查询未完成的快剪任务数量
		
		$ssDate = strtotime($sdate); #开始日期时间戳
		$eeDate = strtotime($edate); #结束日期时间戳

		$monthList = []; #涉及的月份列表
		$fdateList = []; #涉及的日期列表
		for($ti=$ssDate;$ti<=$eeDate;$ti+=86400){
			
			$monthList[] = date('Ym',$ti);#涉及的月份列表
			$fdateList[] = date('Y-m-d',$ti);#涉及的日期列表
			
		}
		$monthList = array_unique($monthList); #月份列表去重
		
		
		if($mediaInfo['media_class'] == '01'){
			$tab = 'tv';
		}elseif($mediaInfo['media_class'] == '02'){
			$tab = 'bc';
		}else{
			$tab = '';
		}
		
		
		$issueDateList = []; #发布记录数量列表
		foreach($monthList as $month){
			$issueTable = 't'.$tab.'issue_'.$month.'_'.substr($mediaInfo['media_region_id'],0,2);
			try{//
				$issueList = M($issueTable)
										->where(array('fissuedate'=>array('between',array($ssDate,$eeDate)),'fmediaid'=>$mediaId))
										->group('fissuedate')
										->getField('fissuedate,count(*)'); #查询发布记录列表
			}catch( \Exception $error) { //
				$issueList = [];
			}
			
			if($issueList){
				$issueDateList += $issueList; #拼接发布记录列表
			}
			
		}
		
		$retDataList = []; #返回的数据列表
		foreach($fdateList as $fdate){ #遍历涉及的日期列表
 			$retData = array();
			$retData['date'] = $fdate; #日期
			$retData['pending_count'] = intval($cutList[$fdate]); #还在处理的快剪任务
			$retData['issue_count'] = intval($issueDateList[strtotime($fdate)]); #发布记录数量
			
			$retDataList[] = $retData;
		}
						

						
						
		$this->ajaxReturn(array('code'=>0,'msg'=>'','dateList'=>$retDataList));
		
	}
	
	

	/*获取截取文件任务*/
	public function get_up_task_bc(){
		$limit = I('limit');
		if($limit > 20) $limit = 20;
		if($limit < 1) $limit = 1;
		
		$fid = I('fid');
		$where = array();
		$where['fcuted'] = 0;
		$where['fsource'] = 3; 
		if($fid){
			$where['fid'] = $fid;
		}
		
		$samList = M('tbcsample')->field('fid,fmediaid,fissuedate,fsourcefilename,favifilename,fcuted,foffsetstart,foffsetend,uuid')->where($where)->limit($limit)->select();
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','samList'=>$samList));
	}
	
	/*获取截取文件任务*/
	public function get_up_task_tv(){
		$limit = I('limit');
		if($limit > 20) $limit = 20;
		if($limit < 1) $limit = 1;
		
		$fid = I('fid');
		$where = array();
		$where['fcuted'] = 0;
		$where['fsource'] = 3; 
		if($fid){
			$where['fid'] = $fid;
		}
		
		$samList = M('ttvsample')->field('fid,fmediaid,fissuedate,fsourcefilename,favifilename,fcuted,foffsetstart,foffsetend,uuid,sstarttime,sendtime')->where($where)->limit($limit)->select();
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','samList'=>$samList));
	}
	
	
	
	
	/*修改截取文件任务*/
	public function edit_up_task_bc(){
		
		
		$fid = I('get.fid');
		$fcuted = I('get.fcuted');
		
		$postData = file_get_contents('php://input');
		$postData = json_decode($postData,true);
		
		$editData = $postData['editData'];
		
		if($editData['fcuted'] == 2){
			$samInfo = M('tbcsample')->where(array('fid'=>$fid))->find();
			$postData = array(
								'start'=>$samInfo['sstarttime'],
								'end'=>$samInfo['sendtime'],
								'name'=>M('tad')->where(array('fadid'=>$samInfo['fadid']))->getField('fadname'),
								'channel'=>$samInfo['fmediaid'],
								'uuid'=>$samInfo['uuid'],
								'editor_id'=>$samInfo['fcreator'],
								'sample_path'=>$samInfo['favifilename'],
								);
			$ret = http('http://47.96.182.117/index/saveAdInfo',$postData,'POST');
			
			//file_put_contents('LOG/4796182117indexsaveAdInfo',date('Y-m-d H:i:s')."\n".json_encode($postData)."\n".$ret."\n\n\n",FILE_APPEND);
			if($samInfo['fillegaltypecode'] == -1){
				$taskid = A('Common/InputTask','Model')->add_task(2,$fid,[2]);//添加到任务表
			}
		}

		$editState = M('tbcsample')->where(array('fid'=>$fid,'fcuted'=>$fcuted)) ->save($editData);
		
		
		if($editState){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败，'.M('tbcsample')->getLastSql()));
		}
		
		
	}
	
	/*修改截取文件任务*/
	public function edit_up_task_tv(){
		
		
		$fid = I('get.fid');
		$fcuted = I('get.fcuted');
		
		$postData = file_get_contents('php://input');
		$postData = json_decode($postData,true);
		
		$editData = $postData['editData'];
		
		if($editData['fcuted'] == 2){
			$samInfo = M('ttvsample')->where(array('fid'=>$fid))->find();
			$postData = array(
								'start'=>$samInfo['sstarttime'],
								'end'=>$samInfo['sendtime'],
								'name'=>M('tad')->where(array('fadid'=>$samInfo['fadid']))->getField('fadname'),
								'channel'=>$samInfo['fmediaid'],
								'uuid'=>$samInfo['uuid'],
								'editor_id'=>$samInfo['fcreator'],
								'sample_path'=>$samInfo['favifilename'],
								);
			$ret = http('http://47.96.182.117/index/saveAdInfo',$postData,'POST');
			
			//file_put_contents('LOG/4796182117indexsaveAdInfo',date('Y-m-d H:i:s')."\n".json_encode($postData)."\n".$ret."\n\n\n",FILE_APPEND);
			if($samInfo['fillegaltypecode'] == -1){
				$taskid = A('Common/InputTask','Model')->add_task(1,$fid,[2]);//添加到任务表
			}
		}

		$editState = M('ttvsample')->where(array('fid'=>$fid,'fcuted'=>$fcuted)) ->save($editData);
		
		#$editState = true;
		if($editState){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败，'.M('ttvsample')->getLastSql()));
		}
		
		
	}
	
	
	
	
	/*获取需要生成识别文件的任务*/
	public function get_need_make_reco(){
		
		$limit = I('get.limit');
		if($limit > 24) $limit = 24;
		if($limit < 1) $limit = 1;
		$make_reco_state = I('get.make_reco_state');
		if($make_reco_state == '') $make_reco_state = 0;
		$cut_task_id = I('get.cut_task_id');
		
		
		
		$where = array();
		$where['make_reco_state'] = 0;
		$where['fstate'] = 2;
		if($cut_task_id){
			$where['cut_task_id'] = $cut_task_id;
		}
		
		$taskList = M('cut_task')->field('cut_task_id,fsourcefilename,make_reco_state,fstarttime')->where($where)->order('priority desc , cut_task.fissuedate desc')->limit($limit)->select();
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','taskList'=>$taskList));		
	}
	
	/*修改生成识别文件任务状态*/
	public function edit_make_reco_state(){
		
		$cut_task_id = I('get.cut_task_id');
		$make_reco_state = I('get.make_reco_state');
		
		
		$postData = file_get_contents('php://input');
		$postData = json_decode($postData,true);
		
		$editData = $postData['editData'];
		


		$editState = M('cut_task')->where(array('cut_task_id'=>$cut_task_id,'make_reco_state'=>$make_reco_state)) ->save($editData);
		
		
		if($editState){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败，'.M('cut_task')->getLastSql()));
		}
		
		
		
		
		
		
	}
	
	
	/*增加积分*/
	private function add_score($cut_task_id,$wx_id){
		
		$cutTaskInfo = M('cut_task')->field('fissuedate,fmediaid,fstarttime,fendtime')->where(array('cut_task_id'=>$cut_task_id))->find();//查询任务详情
		$mediaInfo = M('tmedia')->field('fid,left(fmediaclassid,2) as media_class')->where(array('fid'=>$cutTaskInfo['fmediaid']))->find();//查询媒介详情
		
		if(intval($mediaInfo['media_class']) == 1) $tab = 'tv';
		if(intval($mediaInfo['media_class']) == 2) $tab = 'bc';
		
		$taskLength = ($cutTaskInfo['fendtime'] - $cutTaskInfo['fstarttime']);//任务长度
		$samListWhere = array();
		$samListWhere['fmediaid'] = $cutTaskInfo['fmediaid'];
		$samListWhere['fissuedate'] = $cutTaskInfo['fissuedate'];
		$samListWhere['fid'] = ['exp','in(select f'.$tab.'sampleid from t'.$tab.'issue where fmediaid = '.$cutTaskInfo['fmediaid'].' and fissuedate = "'.$cutTaskInfo['fissuedate'].'")'];
		
		if($taskLength == (3600*9)){
			$samListWhere['sstarttime'] =array('between',array(date('Y-m-d H:i:s',$cutTaskInfo['fstarttime']),date('Y-m-d H:i:s',$cutTaskInfo['fendtime'] - 3600)));
		}else{
			$samListWhere['sstarttime'] =array('between',array(date('Y-m-d H:i:s',$cutTaskInfo['fstarttime']),date('Y-m-d H:i:s',$cutTaskInfo['fendtime'])));
		}
		
		
		
		
		$samList = M('t'.$tab.'sample')
									->field('
												fillegaltypecode,
												(select fadclasscode from tad where fadid = t'.$tab.'sample.fadid) as fadclasscode,
												fadlen
											')
									->where($samListWhere)
									->select();
		#file_put_contents('LOG/cutScoreSql',M('t'.$tab.'sample')->getLastSql()."\n",FILE_APPEND);

		
		$longIllegalNum = 0;//长违法广告
		$longNum = 0;//长不违法广告
		$shortIllegalNum = 0;//短违法广告
		$shortNum = 0;//短不违法广告
		foreach($samList as $sam){
			
			if(substr($sam['fadclasscode'],0,2) == '23'){
				#echo '排除一条数据';
				continue;
			}

			if($sam['fadlen'] >= 600 && $sam['fillegaltypecode'] > 0){//长违法广告
				$longIllegalNum++;
			}elseif($sam['fadlen'] >= 600 && $sam['fillegaltypecode'] == 0){//长不违法广告
				$longNum++;
			}elseif($sam['fadlen'] <= 600 && $sam['fillegaltypecode'] > 0){//短违法广告
				$shortIllegalNum++;
			}elseif($sam['fadlen'] <= 600 && $sam['fillegaltypecode'] == 0){//短不违法广告
				$shortNum++;
			}
	
		}
		$issueCountWhere = array();
		$issueCountWhere['fmediaid'] = $cutTaskInfo['fmediaid'];
		$issueCountWhere['fstarttime'] = array('between',array(date('Y-m-d H:i:s',$cutTaskInfo['fstarttime']),date('Y-m-d H:i:s',$cutTaskInfo['fendtime'])));
		$issueCountWhere['fissuedate'] = $cutTaskInfo['fissuedate'];
		
		$issueCount = M('t'.$tab.'issue')->where($issueCountWhere)->count();

		$issueCount = intval($issueCount);

		
		
		$dataBase_task_info = array();
		$dataBase_task_info['cut_task_id'] = $cut_task_id;
		$dataBase_task_info['longIllegalNum'] = $longIllegalNum;
		$dataBase_task_info['longNum'] = $longNum;
		$dataBase_task_info['shortIllegalNum'] = $shortIllegalNum;
		$dataBase_task_info['shortNum'] = $shortNum;
		$dataBase_task_info['issueCount'] = $issueCount;
		
		
		
		$add_data = array();
		$add_data['taskid'] = 0;//这里填固定值0是因为怕与task_input里面的taskid冲突
		$add_data['wx_id'] = $wx_id;//
		
		#$add_data['score'] = 15+($longIllegalNum*12)+($longNum*8)+($shortIllegalNum*9)+($shortNum*4)+($issueCount/20);
		#$add_data['score'] = 10+($longIllegalNum*10)+($longNum*6)+($shortIllegalNum*8)+($shortNum*3)+($issueCount/40);//该积分规则调整于2019-06-20，但执行时间开始于2019-06-01
		$add_data['score'] = 12+($longIllegalNum*11)+($longNum*7)+($shortIllegalNum*8)+($shortNum*4)+($issueCount/40);//该积分规则调整于2019-06-25
		if($add_data['score'] > 170) $add_data['score'] = 170;
		
		$add_data['reason'] = '快剪任务积分';
		$add_data['update_time'] = time();
		$add_data['type'] = 3;
		$add_data['task_info'] = json_encode($dataBase_task_info);
		$add_data['media_class'] = intval($mediaInfo['media_class']);
		$add_data['score_date'] = date('Y-m-d',$add_data['update_time']);
		
		
		
		
		$score_id = M('task_input_score')->add($add_data);
		
		
		return $score_id;
		
	}
	
	#接受快剪任务生成完成通知
	public function task_state_change(){
		$this->ajaxReturn(array('code'=>-1,'msg'=>'不再使用此方式'));
		$mediaId = I('mediaId');
		$date = I('date');
		#file_put_contents('LOG/CutTask_task_state_change',implode(',',$_GET).'	'.file_get_contents('php://input')."\n",FILE_APPEND);
		
		$mediaInfo = M('tmedia')->cache(true,600)->field('left(fmediaclassid,2) as media_class,priority')->where(array('fid'=>$mediaId))->find();
		#$cutTaskCount = M('cut_task')->where(array('fmediaid'=>$mediaId,'fissuedate'=>$date))->count();
		
		$getTaskUrl = 'http://118.31.55.177:8002/fast/combine_mp3/'.$mediaId.'/'.$date.'/';
		
		#var_dump($getTaskUrl);
		$getTaskJson = http($getTaskUrl);
		$taskArr = json_decode($getTaskJson,true);
		#var_dump($taskArr);
		
		foreach($taskArr['data'] as $key => $task){ #循环获取到的任务
			if ($key > 3) break; 
			#var_dump($task['mp4']);
			$te1 = explode('_',$task['mp4']);
			$te1 = $te1[count($te1)-1];
			$te1 = explode('.',$te1);
			$source_class = $te1[1];
			$timeArr = explode('-',$te1[0]);

			
			if($key == 0){
				$startTime = strtotime($date)+0;
				$endTime = $startTime + 3600*9;
			}elseif($key == 1){
				$startTime = strtotime($date)+3600*8;
				$endTime = $startTime + 3600*9;
			}elseif($key == 2){
				$startTime = strtotime($date)+3600*16;
				$endTime = $startTime + 3600*8;
			}
			
			
			$sCutInfo = M('cut_task')->field('cut_task_id,source_md5')->where(array('fmediaid'=>$mediaId,'fissuedate'=>$date,'fstarttime'=>$startTime))->find();

			if(!$sCutInfo){

				M('cut_task')->add(array(
								
								'fmediaid' => $mediaId,
								'fissuedate' => $date,
								'fstarttime' => $startTime,
								'fendtime' => $endTime,
								'priority' => $mediaInfo['priority'],
								'fstate' => 2,
								'source_class' => $source_class,
								'time_list' => json_encode([array('start'=>$timeArr[0],'end'=>$timeArr[1])]),
								'source_md5' => $task['mp4_md5'],
								'fsourcefilename'=>$task['mp4'],
								'start_make_time'=>$startTime+86400*365,
								'cut_wx_id'=> M('tqc_media_permission')->cache(true,300)->where(array('fmediaid'=>$mediaId))->getField('wx_id'),

								'm3u8_url' => ''
							));
			}elseif($sCutInfo['source_md5'] != $task['mp4_md5']){
				$e_data = array();
				$e_data['fsourcefilename'] = $task['mp4'];
				$e_data['source_md5'] = $task['mp4_md5'];
				$e_data['time_list'] = json_encode([array('start'=>$timeArr[0],'end'=>$timeArr[1])]);
				
				
				$eState = M('cut_task')->where(array('cut_task_id'=>$sCutInfo['cut_task_id']))->save($e_data);
				if($eState){
					$flow_id = A('Open/CutTask','Model')->add_flow($sCutInfo['cut_task_id'],'源文件md5发生变化，已替换源文件',0,'源文件md5发生变化，已替换源文件');//创建流程
				}
				
			}
			
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>''));
		
	}
	
	
	
	
	/*计算积分*/
	public function score(){
		header("Content-type: text/html; charset=utf-8");
		set_time_limit(600);
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		#$mod = I('mod');
		#if(!$mod) $mod = 0;
		$taskList = M('cut_task')
							->field('cut_task_id,cut_wx_id')
							->where(array(
											'cut_complete_time'=>array('between',array(strtotime('2019-06-01 00:00:00'),strtotime('2019-06-30 23:59:59'))),
											'fstate'=>4,
											'score_id'=>0,
											'cut_wx_id'=>array('gt',0),
											
											#'_string'=>'cut_task_id mod 10 = '.$mod
											
											))
											->limit(100)->select();
		#echo M('cut_task')->getLastSql();
		echo "<br />\n";


		foreach($taskList as $task){
			$score_id = $this->add_score($task['cut_task_id'],$task['cut_wx_id']);//重新创建积分
			M('cut_task')->where(array('cut_task_id'=>$task['cut_task_id']))->save(array('score_id'=>$score_id));
			echo $task['cut_task_id'];
			echo "\n";
			
		}
		
		
	}
	
	
	public function get_sc(){
		set_time_limit(600);
		ini_set('memory_limit','2048M');
		
		$sd = I('sd');
		$ed = I('ed');
		
		$dataList = M('cut_task')
							->field('cut_task_id,fmediaid,fissuedate,score_id,cut_wx_id')
							->where('fissuedate BETWEEN "'.$sd.'" and "'.$ed.'" and fstate in( 4,5,6)')
							->select();
		$mdList = array();
		
		foreach($dataList as $data){
			$scoreInf = M('task_input_score')->field('task_info,score')->where(array('id'=>$data['score_id']))->find();
			$scoreInfo = json_decode($scoreInf['task_info'],true);

			$mdList[$data['fmediaid'].'_'.$data['fissuedate']]['longIllegalNum'] += $scoreInfo['longIllegalNum'];
			$mdList[$data['fmediaid'].'_'.$data['fissuedate']]['longNum'] += $scoreInfo['longNum'];
			$mdList[$data['fmediaid'].'_'.$data['fissuedate']]['shortIllegalNum'] += $scoreInfo['shortIllegalNum'];
			$mdList[$data['fmediaid'].'_'.$data['fissuedate']]['shortNum'] += $scoreInfo['shortNum'];
			$mdList[$data['fmediaid'].'_'.$data['fissuedate']]['issueCount'] += $scoreInfo['issueCount'];
			$mdList[$data['fmediaid'].'_'.$data['fissuedate']]['score'] += $scoreInf['score'];
			$mdList[$data['fmediaid'].'_'.$data['fissuedate']]['taskCount'] ++;
			$mdList[$data['fmediaid'].'_'.$data['fissuedate']]['cut_wx_id'] = $data['cut_wx_id'];
			
			
			
			
		}
		$filename = 'LOG/'. createNoncestr(8) .'.CSV';
		file_put_contents($filename,iconv('utf-8','gb2312','媒体id,媒体名称,发布日期,任务数量,短广告数量,短广告违法量,长广告数量,长广告违法量,发布记录数量,积分,人员ID,人员名称'."\n"));
		foreach($mdList as $md => $mdInfo){
			$md = explode('_',$md);
			$mediaId = $md[0];
			$fissuedate = $md[1];
			$mediaName = M('tmedia')->cache(true,60)->where(array('fid'=>$mediaId))->getField('fmedianame');
			$wxName = M('ad_input_user')->where(array('wx_id'=>$mdInfo['cut_wx_id']))->getField('alias');
			
			$rowE = $mediaId.','.$mediaName.','.$fissuedate.','.$mdInfo['taskCount'].','.$mdInfo['shortNum'].','.$mdInfo['shortIllegalNum'].','.$mdInfo['longNum'].','.$mdInfo['longIllegalNum'].','.$mdInfo['issueCount'].','.$mdInfo['score'].','.$mdInfo['cut_wx_id'].','.$wxName."\n";
			
			//echo $rowE;
			file_put_contents($filename,iconv('utf-8','gb2312',$rowE),FILE_APPEND);
		}
		
		echo 'http://'.$_SERVER['HTTP_HOST'].'/'.$filename;
		header('location:'.'http://'.$_SERVER['HTTP_HOST'].'/'.$filename);
	}
	
	
	public function fzr(){
		
		$postData = file_get_contents('php://input');
		$postData = json_decode($postData,true);
		
		$mediaIdList = $postData['mediaIdList'];
		$dateList = $postData['dateList'];
		$dataList = M('cut_task')
								->field('
											fmediaid
											,fissuedate
											,cut_wx_id
											,(select alias from ad_input_user where wx_id = cut_task.cut_wx_id) as fzr
											
										')
								->where(array('fmediaid'=>['in',$mediaIdList],'fissuedate'=>['in',$dateList]))
								->group('fmediaid,fissuedate')
								->select();
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','dataList'=>$dataList));
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}