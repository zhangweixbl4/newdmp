<?php
namespace Api\Controller;
use Think\Controller;

class DeviceController extends Controller {
	
		/*获取设备列表*/
    public function get_device_list(){
		
		$device_name = I('device_name');//获取设备名称
		
		$where = array();
		$where['fname'] = array('like','%'.$device_name.'%');
		$where['fstate'] = array('neq',-1);
		
		$deviceList = M('tdevice')->field('fid,fname,fcode')->where($where)->limit(10)->select();//查询设备列表

		$this->ajaxReturn(array('code'=>0,'value'=>$deviceList));
		
	}
	
	/*采集设备运行状态*/
	public function device_run_param(){
		$data = file_get_contents('php://input');//获取post数据
		$data_arr = json_decode($data,true);//json转为数组
		$fcode = $data_arr['sn'];//序列号
		$device_count =  M('tdevice')->where(array('fcode'=>$fcode))->count();
		if($device_count == 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'no such device'));//没有找到设备
		
		$frunparamupdatetime = date('Y-m-d H:i:s');
		$foperatingsystem = $data_arr['os'];
		$e_data = array(
						'frunparam'=>$data,
						'foperatingsystem'=>$foperatingsystem,
						'frunparamupdatetime'=>$frunparamupdatetime
					);
		
		
		$rr = M('tdevice')->where(array('fcode'=>$fcode))->save($e_data);
		
		if($rr > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'success'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'fail'));
		}
	}
	
}