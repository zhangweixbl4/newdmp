<?php
/*
 * @Description: 存证公证模型
 * @Author: yuhou.wang
 * @Date: 2019-10-10 14:28:16
 * @LastEditors: yuhou.wang
 * @LastEditTime: 2019-11-05 13:31:46
 */
namespace Open\Model;
use Common\Model;
class EvidenceModel{
	/**
	 * @Des: 保存单条证据
	 * @Edt: yuhou.wang
	 * @param {String} $url 证据URL
	 * @param {String} $title 存证名称 
	 * @param {Int}    $fsample_id 存证的样本ID 
	 * @param {String} $fmediaclass 存证媒介类别 01-电视 02-广播 03-报纸 05-户外 13-互联网
	 * @return: 
	 * @Date: 2019-10-10 14:31:18
	 */
	public function addEvidence($url,$title='',$fsample_id=0,$fmediaclass='13'){
        if($url){
			$repeatEvi = M('tb_evidence')->where(['url'=>$url])->find();
			if(!empty($repeatEvi)){
				$data = ['code'=>0,'msg'=>'成功','data'=>['rid' => (int)$repeatEvi['rid']]];
				return $data;
			}else{
				// 保存待发送
				$result = $this->addEvidenceInfo($url,$title);
				// 发送存证请求
				$resInfo = $this->sendEvidence($url);
				// 更新存证结果
				if($resInfo['type'] == 'succ'){
					$objectId = $resInfo['id'];
					// 更新存证码
					$id = ['id' => $result];
					$data = ['rid' => $objectId];
					$saveReturn = $this->updateEvidence($id,$data);
					// 添加样本存证关系记录
					if(!empty($fsample_id)){
						$retEviSam = $this->addEvidenceSam($fmediaclass,$fsample_id,$objectId);
					}
					// 更新存证快照及结果
					// $resUpdate = $this->updateInfo($objectId);
					$data = ['code'=>0,'msg'=>'成功','data'=>['rid' => $objectId]];
					return $data;
				}else{
					$data = ['code'=>1,'msg'=>'出错','data'=>$resInfo];
					return $data;
				}
			}
        }else{
			$data = ['code'=>1,'msg'=>'缺少必要参数'];
            return $data;
        }
	}
	/**
	 * @Des: 保存批量证据
	 * @Edt: yuhou.wang
	 * @param {Array} $evidences 证据
	 * @return: 
	 * @Date: 2019-10-10 14:31:18
	 */
	public function addEvidences($evidences){
		$saveReturn = [];
        if(is_array($evidences) && !empty($evidences)){
			foreach($evidences as $evi){
				$ret = $this->addEvidence($evi['url'],$evi['title'],$evi['fsample_id'],$evi['fmediaclass']);
				$data['rid'][] = $ret['data']['rid'];
			}
			$saveReturn = ['code'=>0,'msg'=>'成功','data'=>$data];
		}elseif(!empty($evidences)){
			$ret = $this->addEvidence($evidences['url'],$evidences['title'],$evidences['fsample_id'],$evidences['fmediaclass']);
			$saveReturn = $ret;
		}
        return $saveReturn;
	}
	/**
	 * @Des: 发送存证请求(通过第三方代理接口)
	 * @Edt: yuhou.wang
	 * @param {type} 
	 * @return: 
	 * @Date: 2019-10-10 14:43:28
	 */
	public function sendEvidence($url){
		$sUrl = 'http://interface.e-chains.cn:8309/ecues/delivery';
		$accessId = '15565244962745QrTgfuzvUc55e4f7a7ecbe1d76e94ead56cfa4bdbe9195';
		$accessKey = '15565244962745YyjvSTuHZae1eef14722421f4f9f9ae1937a3e1db32628';
		$uts = time();
		$accessToken = md5($uts.$accessKey);
		$reqUrl = base64_encode($url);
		$aData = [
			"url"    => $reqUrl,
			"render" => "1",
			"flash"  => "1",
			"wsize"  => "1920x1080",
			"ua"     => "0"
		];
		$aHeader = [
			'Content-Type: application/json',
			'Uts: '.$uts,
			'Access-id: '.$accessId,
			'Access-token: '.$accessToken,
			'Type: url',
		];
		$resInfo = http($sUrl,json_encode($aData),'POST',$aHeader,10);
		$resInfo = json_decode($resInfo,true);
		return $resInfo;
	}
	/**
	 * @Des: 提交存证URL
	 * @Edt: yuhou.wang
	 * @param {String} $url 存证URL 
	 * @param {String} $title 存证名称 
	 * @param {Int}    $userid 存证发起者ID 
	 * @param {String} $user 存证发起者名称 
	 * @param {String} $usertrepid 存证发起者机构 
	 * @param {Int}    $fsample_id 存证的样本ID 
	 * @param {String} $fmediaclass 存证媒介类型 
	 * @return: 
	 * @Date: 2019-10-10 15:31:27
	 */
	public function addEvidenceInfo($url,$title){
		$dataSet = [
			'url'         => $url,
			'title'       => $title,
			'rid'         => '',
			'md5'         => '',
			'screenshot'  => '',
			'wsize'       => '1920x1080',
			'cdate'       => date('Y-m-d H:i:s'),
			'ua'          => 0,
			'render'      => 0,
			'flash'       => 1,
			'status'      => 0,
			'fcreator'    => '',
			'fcreatetime' => date('Y-m-d H:i:s'),
		];
		$result = M('tb_evidence')->add($dataSet);
		return $result;
	}
	/**
	 * @Des: 更新证据
	 * @Edt: yuhou.wang
	 * @param {Array} $key 主键数组(允许组合主键)
	 * @param {Array} $data 证据数据
	 * @return: 
	 * @Date: 2019-10-10 14:31:18
	 */
	public function updateEvidence($key,$data){
		$ret = M('tb_evidence')->where($key)->save($data);
		return $ret;
	}
	/**
	 * @Des: 添加样本存证关联记录
	 * @Edt: yuhou.wang
	 * @param {String} $fmediaclass 媒介类别
	 * @param {Int} $fsample_id 样本ID
	 * @param {Int} $frid 存证ID
	 * @return: 
	 * @Date: 2019-10-10 14:31:18
	 */
	public function addEvidenceSam($fmediaclass,$fsample_id,$frid,$fcreator='API'){
		$dataSet = [
			'fmediaclass' => $fmediaclass,
			'fsample_id'  => $fsample_id,
			'frid'        => $frid,
			'fcreator'    => $fcreator,
			'fcreatetime' => date('Y-m-d H:i:s'),
		];
		$result = M('tb_evidence_sam')->add($dataSet);
		return $result;
	}
	/**
	 * @Des: 等待存证上传OSS成功
	 * @Edt: yuhou.wang
	 * @param {type} 
	 * @return: 
	 * @Date: 2019-10-10 15:18:38
	 */
    protected function updateInfo($objectId){
        $isObjectExist = A('Common/AliyunOss','Model')->isObjectExist($objectId);
        if($isObjectExist){
            $this->processOssFile($objectId);
            return true;
        }else{
            sleep(1);
            $this->updateInfo($objectId);
        }
        return true;
	}
	/**
	 * @Des: 补全存证信息
	 * @Edt: yuhou.wang
	 * @param {String|Array} $objectIds OSS文件名称即存证ID,支持单条或批量
	 * @return: 
	 * @Date: 2019-10-10 15:20:31
	 */
    public function appendInfo($objectIds){
        if(is_array($objectIds) && !empty($objectIds)){
			foreach($objectIds as $objectId){
				$ret = $this->processOssFile($objectId);
			}
		}elseif(!empty($objectIds)){
			$ret = $this->processOssFile($objectIds);
		}
		$saveReturn = $ret;
        return $saveReturn;
	}
	/**
	 * @Des: 下载解压上传快照
	 * @Edt: yuhou.wang
	 * @param {String} $objectId OSS文件名称即存证ID
	 * @return: 
	 * @Date: 2019-10-10 15:20:31
	 */
    protected function processOssFile($objectId){
        // 从OSS下载至本地
        $object = A('Common/AliyunOss','Model')->getObject($objectId);
        // 从OSS加载至内存
		// $objectContent = A('Common/AliyunOss','Model')->getObjectToMem($objectId);
		
        // 从本地解压上传快照并返回快照url
        $ret = $this->tarObject($objectId);
        // 从内存解压 TODO:内存不足
		// $ret = $this->tarObjectFromMem($object);
		
        // 获取元信息
        $objectMeta = $this->getObjectMeta($objectId);

        $dataSet = [
            'md5'         => $objectMeta['eTag'],
            'screenshot'  => $ret['url'],
            'status'      => 1,
            'fmodifier'   => '',
            'fmodifytime' => date('Y-m-d H:i:s')
        ];
        $saveReturn = M('tb_evidence')->where(['rid'=>$objectId])->save($dataSet);
        return $saveReturn;
	}
	/**
     * @Des: 从磁盘解压缩.tar.gz文件，并将其中截图图片上传OSS
     * @Edt: yuhou.wang
     * @param {String} $objname 文件名
     * @return: {String} $ret 资源链接
     * @Date: 2019-10-10 15:22:23
     */
    public function tarObject($objname){
        exec('mkdir -p ./LOG/temp/'.$objname,$out,$ret);
        exec('tar -xzvf ./LOG/'.$objname.'.tar.gz -C ./LOG/temp/'.$objname,$output,$execret);
        $filename = $objname.'_screenshot_'.date('YmdHis').'.png';
        $dir = './LOG/temp/'.$objname.'/screenshot.png';
        if(!file_exists($dir)){
            mkdir($dir);
        }
		$savefile = $dir;
		$ret = A('Common/AliyunOss','Model')->file_up('tem/'.$filename,$savefile,['Content-Disposition' => 'attachment;filename=']);//上传OSS(ddmmpp)
		if($ret){
			// 清理本地临时文件及文件夹
			unlink('./LOG/'.$objname.'.tar.gz');
			$this->deldir('./LOG/temp/'.$objname.'/');
			rmdir('./LOG/temp/'.$objname.'/');
		}
        return $ret;
	}
	//清空文件夹函数和清空文件夹后删除空文件夹函数的处理
	public function deldir($path){
		//如果是目录则继续
		if(is_dir($path)){
			//扫描一个文件夹内的所有文件夹和文件并返回数组
			$p = scandir($path);
			foreach($p as $val){
				//排除目录中的.和..
				if($val !="." && $val !=".."){
					//如果是目录则递归子目录，继续操作
					if(is_dir($path.$val)){
						//子目录中操作删除文件夹和文件
						$this->deldir($path.$val.'/');
						//目录清空后删除空文件夹
						@rmdir($path.$val.'/');
					}else{
						//如果是文件直接删除
						unlink($path.$val);
					}
				}
			}
		}
	}
	//删除指定文件夹以及文件夹下的所有文件
	public function deldir2($dir) {
		//先删除目录下的文件：
		$dh=opendir($dir);
		while ($file=readdir($dh)) {
			if($file!="." && $file!="..") {
				$fullpath=$dir."/".$file;
				if(!is_dir($fullpath)) {
					unlink($fullpath);
				} else {
					deldir($fullpath);
					break;
				}
			}
		}
		closedir($dh);
		//删除当前文件夹：
		if(rmdir($dir)){
			return true;
		}else{
			return false;
		}
	}
	/**
	 * @Des: 获取指定对象元信息
	 * @Edt: yuhou.wang
	 * @param {type} 
	 * @return: 
	 * @Date: 2019-10-10 15:59:50
	 */	
    public function getObjectMeta($object){
        $objectList = A('Common/AliyunOss','Model')->getObjects($object);
        foreach($objectList as $objectInfo){
            $objectMeta = [
                'key' => $objectInfo->getKey(),
                'lastModified' => date('Y-m-d H:i:s',strtotime($objectInfo->getLastModified())),
                'eTag' => str_replace('"','',$objectInfo->getEtag()),//$objectInfo->getEtag(),
                'type' => $objectInfo->getType(),
                'size' => $objectInfo->getSize(),
                'storageClass' => $objectInfo->getStorageClass()
            ];
        }
        return $objectMeta;
	}
	/**
	 * @Des: 获取存管函(下载为PDF文件版)
	 * @Edt: yuhou.wang
	 * @Date: 2019-11-01 06:17:14
	 * @param {Int} $rid 存证码 
	 * @return: 
	 */	
	public function getEvidenceLetterPDF($rid){
		if(!empty($rid)){
			$req = 'https://app-test.e-chains.cn/api/cert/download-pdf/'.$rid;
			return $req;
		}else{
			return ['code'=>1,'msg'=>'缺少必要参数'];
		}
	}
	/**
	 * @Des: 获取存管函(输出HTML源文件版)
	 * @Edt: yuhou.wang
	 * @Date: 2019-11-01 06:17:14
	 * @param {Int} $rid 存证码 
	 * @return: 
	 */	
    public function getEvidenceLetterHTML($rid){
        if (!empty($rid)) {
            $sUrl = 'https://evidence-management.e-chains.cn/api/check_certificate';
            $aData = [
				'tab'        => 'check',
				'type'       => 'wpes',
				'id'         => $rid,
				'access_id'  => '15565244962745QrTgfuzvUc55e4f7a7ecbe1d76e94ead56cfa4bdbe9195',
				'access_key' => base64_encode('15565244962745YyjvSTuHZae1eef14722421f4f9f9ae1937a3e1db32628')
			];
            $resInfo = http($sUrl, $aData, 'POST', false, 10, false);
            return $resInfo;
        }else{
			return ['code'=>1,'msg'=>'缺少必要参数'];
		}
	}
	/**
	 * @Des: 获取存证资源链接
	 * @Edt: yuhou.wang
	 * @Date: 2019-11-01 06:51:22
	 * @param {Int} $rid 存证码
	 * @return: 
	 */	
    public function getEvidenceObjextOSS($rid){
        if (!empty($rid)) {
			// header("Content-Disposition:attachment;filename='".$rid."'.tar.gz");
            $ret = A('Common/AliyunOss','Model')->signUrl('cunzheng', $rid, $timeout=86400);
            return $ret;
        }else{
			return ['code'=>1,'msg'=>'缺少必要参数'];
		}
	}
	/**
	 * @Des: 获取存证资源链接-Local
	 * @Edt: yuhou.wang
	 * @Date: 2019-11-01 06:51:22
	 * @param {Int} $rid 存证码
	 * @return: 
	 */	
    public function getEvidenceObjext($rid){
        if (!empty($rid)) {
			// 从OSS下载至本地
			$object = A('Common/AliyunOss','Model')->getObject($rid);
			$ret = 'http://'.$_SERVER['HTTP_HOST'].'/LOG/'.$rid.'.tar.gz';
            return $ret;
        }else{
			return ['code'=>1,'msg'=>'缺少必要参数'];
		}
	}
}