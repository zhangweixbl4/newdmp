const reasonInfo = {
    template: `<div v-loading="loading">
    <el-form ref="form" :model="info" label-width="80px">
        <el-form-item label="错误类型">
            <el-input v-model="info.reason"></el-input>
        </el-form-item>
        <el-form-item label="错误描述">
            <el-input v-model="info.description"></el-input>
        </el-form-item>
        <el-form-item label="媒体类型">
            <el-checkbox-group v-model="info.mediaClass">
                <el-checkbox label="1">电视</el-checkbox>
                <el-checkbox label="2">广播</el-checkbox>
                <el-checkbox label="3">报纸</el-checkbox>
                <el-checkbox label="13">网络</el-checkbox>
            </el-checkbox-group>
        </el-form-item>
        <el-form-item>
            <el-button type="primary" @click="save">保存</el-button>
        </el-form-item>
    </el-form>
</div>`,
    props: ['currentId'],
    data: function () {
        return {
            loading: false,
            info: {}
        }
    },
    methods: {
        dataInit: function () {
            this.info = {
                mediaClass: []
            }
            if (this.currentId){
                this.loading = true
                $.post('getCutErrorReason', {id: this.currentId}).then((res) => {
                    this.info = res
                    this.info.mediaClass = this.info.media_class.split(',')
                    this.loading = false
                })
            }
        },
        save: function () {
            let text = ''
            let url = ''
            let data = {}
            this.info.media_class = this.info.mediaClass.join(',')
            if (this.currentId){
                text = '确认保存?'
                url = 'putCutErrorReason'
                data = {
                    id: this.currentId,
                    reason: this.info
                }
            } else {
                text = '确认新增?'
                url = 'postCutErrorReason'
                data = {
                    reason: this.info
                }
            }
            this.$confirm(text).then(() => {
                this.loading = true
                $.post(url, data).then((res) => {
                    this.loading = false
                    if (res.code == 0){
                       this.$message.success(res.msg)
                        this.$emit('done')
                    } else {
                        this.$message.error(res.msg)
                    }
                })
            })

        }
    }

}