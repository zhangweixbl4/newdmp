<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;


class SynIssueController extends Controller {
	
	
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}
	
	public function bak_issue(){

		foreach(array('tv','bc') as $media_type){
			$issueList = M('t'.$media_type.'issue')
										->field(' 
												`f'.$media_type.'issueid`,
												`f'.$media_type.'sampleid`,
												`fmediaid`,
												`fissuedate`,
												`fstarttime`,
												`fendtime`,
												`flength`,
												`famounts`,
												`fcreatetime`,
												`fstate`,
												`fregionid`
												')
										->where(array('fissuedate'=>array('lt',date('Y-m-01',time()-86400*62))))
										->limit(1000)
										->select();
			$idList = [];
			foreach($issueList as $issue){ 
				$idList[] = $issue['f'.$media_type.'issueid'] ;
			}
			
			$rowState = M('t'.$media_type.'issue_bak')->addAll($issueList,array(),true);
			
			if($rowState){
				$delState = M('t'.$media_type.'issue')->where(array('f'.$media_type.'issueid'=>array('in',$idList)))->delete();
				#var_dump($delState);
			}
			
			
		
		}
		echo date('Y-m-d H:i:s') ." end \n";
	}
	
	
	public function syn_tv(){
		set_time_limit(600);
		session_write_close();
		$issueList = M('ttvissue')->master(true)
									->field('
											ftvissueid
											,fmediaid
											,ftvsampleid
											,fstarttime
											,fendtime
											,faudiosimilar
											
											,fcreatetime
											,fcreator
											
											')
									->where(array('ftvmodelid'=>0))
									->limit(1000)->select();
									
		M()->startTrans();//开启事务方法	
		$add_data = array();
		foreach($issueList as $issue){//循环发布记录
		
			$mediaInfo = M('tmedia')
									->cache(true,86400)
									->field('tmediaowner.fregionid')
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->where(array('tmedia.fid'=>$issue['fmediaid']))
									->find();//查询媒介信息
			$table_name = 'ttvissue_' . date('Ym',strtotime($issue['fstarttime'])) . '_' . substr($mediaInfo['fregionid'],0,2);//拼接分表名称
			
			$e_state = M('ttvissue')->where(array('ftvmodelid'=>0,'ftvissueid'=>$issue['ftvissueid']))->save(array('ftvmodelid'=>20000,'fregionid'=>$mediaInfo['fregionid']));//修改同步状态
			
			if($e_state){
				
				$add_data[$table_name][] = array(
											'fmediaid' => $issue['fmediaid'],
											'fsampleid' => $issue['ftvsampleid'],
											'fstarttime' => strtotime($issue['fstarttime']),
											'fendtime' => strtotime($issue['fendtime']),
											'flength' => strtotime($issue['fendtime']) - strtotime($issue['fstarttime']),
											'fissuedate' => strtotime(date('Y-m-d',strtotime($issue['fstarttime']))),
											'faudiosimilar' => $issue['faudiosimilar'],
											'fregionid' => $mediaInfo['fregionid'],
											'fcreatetime' => strtotime($issue['fcreatetime']),
											'fcreator' => $issue['fcreator']
											);
				A('Api/Media','Model')->sam_relation_issue('tv',$issue['ftvsampleid'],$table_name);	
				//S('synIssue_'.$table_name.'_'.$issue['fmediaid'].'_'.strtotime($issue['fstarttime']),1,600);//写入缓存，用于排除在冲突
				echo 'e_state tv success'."\n";
			}else{
				
				echo 'e_state tv error'."\n";
			}

			
		}
		
		foreach($add_data as $table_name => $addData){
			
			try{//尝试写入数据，如果写入失败则可能是没有创建表
				M($table_name)->addAll($addData);
			}catch(Exception $error) { //如果写入失败
				A('Common/SynIssue','Model')->create_issue_part_table($table_name);	//创建表
				M($table_name)->addAll($addData);
				
			} 		
		}
		M()->commit();//事务提交方法
		
	}
	
	
	public function syn_bc(){
		set_time_limit(600);
		session_write_close();
		$issueList = M('tbcissue')->master(true)
									->field('
											fbcissueid
											,fmediaid
											,fbcsampleid
											,fstarttime
											,fendtime
											,faudiosimilar
											
											,fcreatetime
											,fcreator
											
											')
									->where(array('fbcmodelid'=>0))
									->limit(1000)->select();
		M()->startTrans();//开启事务方法
		$add_data = array();
		foreach($issueList as $issue){
			$mediaInfo = M('tmedia')
									->cache(true,86400)
									->field('tmediaowner.fregionid')
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->where(array('tmedia.fid'=>$issue['fmediaid']))
									->find();//查询媒介信息
			$table_name = 'tbcissue_' . date('Ym',strtotime($issue['fstarttime'])) . '_' . substr($mediaInfo['fregionid'],0,2);
	
			$e_state = M('tbcissue')->where(array('fbcmodelid'=>0,'fbcissueid'=>$issue['fbcissueid']))->save(array('fbcmodelid'=>20000,'fregionid'=>$mediaInfo['fregionid']));

			if($e_state){
				$add_data[$table_name][] = array(
											'fmediaid' => $issue['fmediaid'],
											'fsampleid' => $issue['fbcsampleid'],
											'fstarttime' => strtotime($issue['fstarttime']),
											'fendtime' => strtotime($issue['fendtime']),
											'flength' => strtotime($issue['fendtime']) - strtotime($issue['fstarttime']),
											'fissuedate' => strtotime(date('Y-m-d',strtotime($issue['fstarttime']))),
											'faudiosimilar' => $issue['faudiosimilar'],
											'fregionid' => $mediaInfo['fregionid'],
											'fcreatetime' => strtotime($issue['fcreatetime']),
											'fcreator' => $issue['fcreator']
											);
				A('Api/Media','Model')->sam_relation_issue('bc',$issue['fbcsampleid'],$table_name);
				//S('synIssue_'.$table_name.'_'.$issue['fmediaid'].'_'.strtotime($issue['fstarttime']),1,600);//写入缓存，用于排除在冲突
				echo 'e_state bc success'."\n";
				
			}else{
				file_put_contents('LOG/synIssueBc',$issue['fmediaid'].'	'.$issue['fbcissueid']."\n",FILE_APPEND);
				echo 'e_state bc error'."\n";
				
				
			}
		}
		
		foreach($add_data as $table_name => $addData){
			
			try{//尝试写入数据，如果写入失败则可能是没有创建表
				M($table_name)->addAll($addData);
			}catch(Exception $error) { //如果写入失败
				A('Common/SynIssue','Model')->create_issue_part_table($table_name);	//创建表
				M($table_name)->addAll($addData);
				
			} 		
		}
		M()->commit();//事务提交方法		
		
	}
	
	public function check_issue(){
		set_time_limit(600);
		session_write_close();
		$fmediaid = I('fmediaid');
		$fdate = I('fdate');
		
		
		$count = A('Api/SynIssue','Model')->check_issue($fmediaid,$fdate);
		
		echo $count;
		
		

		
	}
	
	public function del_repeat_issue(){
		set_time_limit(600);
		session_write_close();
		$type = I('get.type');
		
		
		$month = I('get.month');//月份
		if($month == ''){
			$month = date('Ym');
		}
		
		
		$regionList = M('tregion')->cache(true,86400)->field('left(fid,2) as region_prefix')->where(array('right(fid,4)'=>'0000'))->select();
		
		M()->startTrans();//开启事务方法
		foreach($regionList as $region){//循环地区
			$table_postfix = '_'.$month.'_'.$region['region_prefix'];//表后缀
			$issue_table = 't'.$type.'issue'.$table_postfix;
			try{//
				$issueList = M($issue_table)
									->field('fid,fmediaid,fstarttime,count(*) as count')
									->where()
									->group('fmediaid,fstarttime')
									->having('count(*) > 1')
									->limit(100000)
									->select();
			}catch(Exception $error) { //
				
			} 
			
			
			foreach($issueList as $issue){
				try{//
					$del_state = M($issue_table)->where(array('fmediaid'=>$issue['fmediaid'],'fstarttime'=>$issue['fstarttime'],'fid'=>array('neq',$issue['fid'])))->delete();
					//var_dump(M($issue_table)->getLastSql());
				}catch(Exception $error) { //
					
				} 
				
				echo $issue_table.','.$issue['count'].','.$del_state."\n";
				
				
			}
		
		}
		M()->commit();//事务提交方法
		//M()->rollback();//事务回滚方法
		
	}
	

	
	
	
	public function media_issue_check(){
		session_write_close();
		set_time_limit(1200);
		for($i=0;$i<50;$i++){
			$this->media_issue_check_do();
		}
		
		
		
	}	
	
	
	/*媒介发布数据校验*/
	public function media_issue_check_do(){
		
		session_write_close();
		set_time_limit(80);

		$dateInfo = A('Common/System','Model')->important_data('media_issue_check');//获取处理过的日期
		$dateInfo = json_decode($dateInfo,true);//转为数组

		
		if(!$dateInfo){//如果是空的
			$month = date('Y-m-01',time() - 86400 * 30 * 12);
			$media_id = 0;
		}else{

			$month = $dateInfo['month'];//处理月份
			$media_id = $dateInfo['media_id'];//处理媒介id
		}
		
		
		$mediaInfo = M('tmedia')->cache(true,120)->field('fid')->where(array('left(fmediaclassid,2)'=>array('in','01,02'),'fid'=>array('gt',$media_id)))->order('fid')->find();//取出一个媒介
		

		if(!$mediaInfo){//判断能否查到媒介
			if(strtotime($month) > time()){//判断日期是否处理到了最近一天
				A('Common/System','Model')->important_data('media_issue_check','{}');
			}else{
				A('Common/System','Model')->important_data('media_issue_check',
																				json_encode(array(
																									'month'=>date("Y-m-01", strtotime("+1 months", strtotime($month))),//时间戳加一个月
																									'media_id'=>0
																									)
																							)
															);//不是最近一天
			}
			exit;
		}else{
			$ff = A('Common/System','Model')->important_data('media_issue_check',json_encode(array('month'=>$month,'media_id'=>$mediaInfo['fid'])));


		}
	
		
		//var_dump($month);
		//var_dump($mediaInfo['fid']);
		A('Api/SynIssue','Model')->media_issue_check_do($month,$mediaInfo['fid']);
		
		
	}
	
	
	

	
	
	
	
	
	
	
	
	
	
	
}