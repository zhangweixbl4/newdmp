<?php
namespace Open\Controller;
use Think\Controller;

class SampleModelController extends BaseController {
	
	
	
	/*获取上传任务*/
	public function get_upload_sample_task(){

		$post_data = file_get_contents('php://input');//获取post数据
		$post_data = json_decode($post_data,true);//把json转化成数组
		$ftype = I('ftype');
		
		
		

		$where = array();
		$where['sample.fcuted'] = 3;
		if(I('mm') == '1'){
			$where['sample.favifilename'] = array('neq','');
		}
		
		
		if($ftype == 'q'){
			$where['tmedia.fcollecttype'] = 10;
		}elseif($ftype == 'a'){
			$where['tmedia.fcollecttype'] = 25;
		}
		
		$samInfo = M('ttvsample')//查询电视样本
								->alias('sample')
								->field('sample.fid,favifilename,fcuted,"tv" as mediaclass,fsourcefilename,fmediaid,uuid')
								->join('tmedia on tmedia.fid = sample.fmediaid')
								->where($where)->order('fid desc')->find();//电视
		if($samInfo  && count(explode(',',$samInfo['fsourcefilename'])) == 2 && $samInfo['favifilename'] == ''){
			$issueTime = explode(',',$samInfo['fsourcefilename']);
			$samInfo['m3u8_url'] = A('Common/Media','Model')->get_m3u8($samInfo['fmediaid'],$issueTime[0],$issueTime[1]);

		}
	
		if(!$samInfo){

			$samInfo = M('tbcsample')
									->alias('sample')
									->field('sample.fid,favifilename,fcuted,"bc" as mediaclass,fsourcefilename,fmediaid,uuid')
									->join('tmedia on tmedia.fid = sample.fmediaid')
									->where($where)->order('fid desc')->find();//广播
			if($samInfo  && count(explode(',',$samInfo['fsourcefilename'])) == 2 && $samInfo['favifilename'] == ''){
				$issueTime = explode(',',$samInfo['fsourcefilename']);
				$samInfo['m3u8_url'] = A('Common/Media','Model')->get_m3u8($samInfo['fmediaid'],$issueTime[0],$issueTime[1]);
						
				/* $mediaInfo = M('tmedia')->cache(true,86400)->where(array('fid'=>$samInfo['fmediaid']))->find();
				if($mediaInfo['fcollecttype'] == 25){
					$samInfo['m3u8_url'] = 'http://118.31.14.6/test/vod?channel='. $samInfo['fmediaid'] . '&start='. $issueTime[0] .'&end='. $issueTime[1];

				} */
				
			}
		}

			
			
		
		


		$this->ajaxReturn(array('code'=>0,'msg'=>'','samInfo'=>$samInfo/* ,'mediaInfo'=>$mediaInfo */));
		
	}
	
	
	


	
	/*修改剪辑任务状态*/
	public function edit_cutting_sample_task(){
		$post_data = file_get_contents('php://input');//获取post数据
		
		$post_data = json_decode($post_data,true);//把json转化成数组
		
		$fid = $post_data['fid'];//样本的ID
		
		$fcuted = intval($post_data['fcuted']);//状态
		$favifilename = $post_data['favifilename'];//视频文件名，剪辑后的
		$favifilepng = $post_data['favifilepng'];//视频文件名，剪辑后的
		$fadlen = $post_data['fadlen'];
		
		
		$mediaclass = $post_data['mediaclass'];//媒介类型，电视tv，广播bc
		
		if(in_array($fcuted,array(0,1,2,9,3,4,5))){
			
		}else{
			$this->ajaxReturn(array('code'=>10027,'msg'=>'参数 fcuted 错误'));
		}

		if($mediaclass != 'tv' && $mediaclass != 'bc'){
			$this->ajaxReturn(array('code'=>10027,'msg'=>'参数 mediaclass 错误，只能是tv或bc'));
		}
		
		$sampleInfo = M('t'.$mediaclass.'sample')->where(array('fid'=>$fid))->find();
		
		if(!$sampleInfo){
			$this->ajaxReturn(array('code'=>10027,'msg'=>'不存在该样本'));
		}
		$e_data = array();
		$e_data['fcuted'] = $fcuted;
		if($fcuted == 2 && $favifilename != ''){
			$e_data['favifilename'] = $favifilename;//视频文件名
			$e_data['favifilepng'] = $favifilepng;//视频缩略图文件名
		}
		if(intval($fadlen) != 0){
			$e_data['fadlen'] = $fadlen;//视频文件长度
		}
		
		$rr = M('t'.$mediaclass.'sample')->where(array('fid'=>$fid))->save($e_data);
		
		
		
	
		/* if($mediaclass == 'tv' && $fcuted == 2){
			$taskid = A('Common/InputTask','Model')->add_task(1,$fid);//添加到任务表
			
		}elseif($mediaclass == 'bc' && $fcuted == 2){
			$taskid = A('Common/InputTask','Model')->add_task(2,$fid);//添加到任务表
		} */
		
		if($fcuted == 5){//判断是否处理样本出错
			A('TaskInput/CutErr','Model')->CutErr($mediaclass,$fid,'处理样本文件时错误');
		}
		
		
		
		
		
		if($rr > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));
		}
		
		
		
	}
	
	
	
	
	/*新增样本模型、发布记录*/
	public function add_sample_model_issue(){
		$msectime = msectime();
		$post_data = file_get_contents('php://input');//获取post数据
		//file_put_contents('LOG/add_sample_model_issue',$post_data);
		$post_data = json_decode($post_data,true);//把json转化成数组

		$mediaid = $post_data['mediaid'];//媒体ID
		$mediaInfo = M('tmedia')->field('fid,left(fmediaclassid,2) as media_class,fmedianame')->where(array('fid'=>$mediaid))->find();
		//var_dump($mediaid);
		//var_dump(M('tmedia')->getLastSql());
		if(intval($mediaInfo['fid']) == 0){
			$this->ajaxReturn(array('code'=>10027,'msg'=>'查询不到这个媒体','e_time'=>msectime() - $msectime));
		}
		
		$wx_id = $post_data['wx_id'];//微信ID
		if(!$wx_id) $wx_id = $post_data['inspector'];
		$wxInfo = M('ad_input_user')->field('wx_id,nickname,alias')->where(array('wx_id'=>$wx_id))->find();//查询微信用户信息
		if(intval($wxInfo['wx_id']) == 0){
			$this->ajaxReturn(array('code'=>10027,'msg'=>'查询不到这个微信用户'));
		}
		
		if(!$post_data['startime']) $post_data['startime'] = $post_data['starttime'];
		$issuedate = date('Y-m-d',strtotime($post_data['startime']));//发布日期
		$startime = date('Y-m-d H:i:s',strtotime($post_data['startime']));//开始时间
		$endtime = date('Y-m-d H:i:s',strtotime($post_data['endtime']));//结束时间

		$client_version = $post_data['client_version'];
		
		
		
		$u0 = intval($post_data['u0']);//发布位置，相对当天0点的毫秒数
		$duration = intval($post_data['duration']);//发布长度，单位毫秒
		if($duration < 2000) $this->ajaxReturn(array('code'=>-1,'msg'=>'样本长度不能小于2秒'));
		$samfilename = $post_data['samfilename'];//模型文件名
		$mp4filename = $post_data['mp4filename'];//视频文件名
		$pngfilename = $post_data['pngfilename'];//缩略图文件名
		
		$fadname = $post_data['fadname'];//广告名称
		$fbrand = $post_data['fbrand'];//品牌名称
		$fadclasscode = $post_data['fadclasscode'];//广告分类code
		$fadclasscode_v2 = $post_data['fadclasscode_v2'];//商业广告分类code
		
		$adowner_name = strval($post_data['adowner_name']);//广告主名称
		$fversion = $post_data['fversion'];//版本说明
		$fspokesman = $post_data['fspokesman'];//代言人
		
		$fapprovalid = $post_data['fapprovalid'];//审批号
		$fapprovalunit = $post_data['fapprovalunit'];//审批单位
		
		$fexpressioncodes = $post_data['fexpressioncodes'];//违法表现代码，数组
		$fillegalcontent = $post_data['fillegalcontent'];//违法内容
		$fillegaltypecode = $post_data['fillegaltypecode'];//违法程度
		$offsetstart = intval($post_data['offsetstart']);//开始位置，海康偏移开始位置字节,其它文件偏移开始位置毫秒
		$offsetend = intval($post_data['offsetend']);//结束位置，海康偏移结束位置字节,其它文件偏移结束位置毫秒
		
		$sourcefilename = $post_data['sourcefilename'];//原始素材文件名
		
		if($mediaInfo['media_class'] == '01'){//电视
			$media_class_tab = 'tv';
		}elseif($mediaInfo['media_class'] == '02'){//广播
			$media_class_tab = 'bc';
		}else{//其它情况
			$this->ajaxReturn(array('code'=>10027,'msg'=>'这个媒体既不是电视也不是广播'));
		}
		
		/* if(M('t'.$media_class_tab.'model')->where(array('fmediaid'=>$mediaid,'fstarttime'=>$startime,'fendtime'=>$endtime))->count() > 0){
			echo M('t'.$media_class_tab.'model')->getLastSql();
			$this->ajaxReturn(array('code'=>-1,'msg'=>'请勿重复添加'));
		} */
		
		
		
		M()->startTrans();//开启事务方法
		$sample_data = array();/*开始添加样本表数据*************************************************************开始添加样本表数据*/
		if(count($fexpressioncodes) > 0){
			$illegalList = M('tillegal')->where(array('fcode'=>array('in',$fexpressioncodes)))->select();
			
			
			$fexpressioncodes_array = array();
			$fexpressions_array = array();
			$fconfirmations_array = array();
			$fpunishments_array = array();
			$fpunishmenttypes_array = array();
			
			
			foreach($illegalList as $illegal){
				$fexpressioncodes_array[] = $illegal['fcode'];//违法表现代码
				$fexpressions_array[] = $illegal['fexpression'];//违法表现
				$fconfirmations_array[] = $illegal['fconfirmation'];//认定依据
				$fpunishments_array[] = $illegal['fpunishment'];//处罚依据
				$fpunishmenttypes_array[] = $illegal['fpunishmenttype'];//处罚种类及幅度
				
			}
			
			$sample_data['fillegalcontent'] = $fillegalcontent;////
			$sample_data['fillegaltypecode'] = $fillegaltypecode;////
			$sample_data['fexpressioncodes'] = implode(';',$fexpressioncodes_array);
			$sample_data['fexpressions'] = implode("\n",$fexpressions_array);
			$sample_data['fconfirmations'] = implode("\n",$fconfirmations_array);
			$sample_data['fpunishments'] = implode("\n",$fpunishments_array);
			$sample_data['fpunishmenttypes'] = implode("\n",$fpunishmenttypes_array);
		}
		
		
		
		
		
		
		$sample_data['fmediaid'] = $mediaid;//媒介ID
		$sample_data['fissuedate'] = $issuedate;//发布日期
		$sample_data['last_issue_date'] = $issuedate;//最后发布日期
		
		$sample_data['favifilename'] = $mp4filename;//视频文件名
		//$adid = A('Adinput/InputTask','Model')->get_ad_id($fadname,$fbrand,$fadclasscode,$adowner_name);//获取广告ID
		$adid = A('Common/Ad','Model')->getAdId($fadname,$fbrand,$fadclasscode,$adowner_name,$wxInfo['alias'], $fadclasscode_v2);//获取广告ID
		
		
		$sample_data['fadid'] = intval($adid);//广告ID

		$sample_data['fversion'] = $post_data['fversion'];//版本说明
		$sample_data['fspokesman'] = $post_data['fspokesman'];//代言人

		
		if($pngfilename != '') $sample_data['favifilepng'] = $pngfilename;//缩略图文件名
		$sample_data['fsourcefilename'] = $sourcefilename;//原始视频文件名
		$sample_data['fduration'] = $duration;////发布长度，单位毫秒
		$sample_data['fadlen'] = $duration / 1000;////发布长度，单位秒
		

		$sample_data['sstarttime'] = $post_data['startime'];//// starttime
		$sample_data['sendtime'] = $post_data['endtime'];////
		
		$sample_data['fsource'] = 3;////
		$sample_data['uuid'] = $post_data['uuid'];////
		
		if($client_version >= 1.1){
			$sample_data['tem_ad_name'] = '1.1';
			$sample_data['fcuted'] = 0;//剪切状态
		}
		
		
		$sample_data['fcreator'] = $wxInfo['alias'];//创建人
		$sample_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$sample_data['fmodifier'] = $wxInfo['alias'];//修改人
		$sample_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		
		$sample_data['foffsetstart'] = $offsetstart;//开始位置 海康偏移开始位置字节,其它文件偏移开始位置毫秒
		$sample_data['foffsetend'] = $offsetend;//结束位置 海康偏移结束位置字节,其它文件偏移结束位置毫秒
		
		$sample_data['fadmanuno'] = $post_data['fadmanuno'];// 广告中标识的生产批准文号 
		$sample_data['fmanuno'] = $post_data['fmanuno'];// 生产批准文号 
		$sample_data['fadapprno'] = $post_data['fadapprno'];//广告中标识的广告批准文号 
		$sample_data['fapprno'] = $post_data['fapprno'];//广告批准文号 
		$sample_data['fadent'] = $post_data['fadent'];//广告中标识的生产企业（证件持有人）名称 
		$sample_data['fent'] = $post_data['fent'];//生产企业（证件持有人）名称 
		$sample_data['fentzone'] = $post_data['fentzone'];//生产企业（证件持有人）所在地区 
		
		


		$sampleid = M('t'.$media_class_tab.'sample')->add($sample_data);//添加样本表数据****************************************结束添加样本表数据*/
		
			
			
		if(intval($sampleid) == 0){
			M()->rollback();//事务回滚方法
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加样本表数据失败,数据已回滚'));
		}
		

		$issue_list = $post_data['issueList'];//发布记录，数组
		M()->startTrans();//开启事务方法,再次启用事务
		foreach($issue_list as $ke => $issue){//循环发布记录/*******************************************************开始添加发布记录*/
			//var_dump($issue);
			$issue_data = array();
			$issue_data['f'.$media_class_tab.'sampleid'] = $sampleid;//样本ID
			$issue_data['f'.$media_class_tab.'modelid'] = 0;//模型ID
			$issue_data['fmediaid'] = $mediaid;//媒介ID
			$issue_data['fissuedate'] = date('Y-m-d',strtotime($issue['starttime']));//发布日期
			$issue_data['fu0'] = intval($issue['u0']);//发布位置，相对当天0点的毫秒数
			$issue_data['fduration'] = intval($issue['duration']);//发布长度，单位毫秒
			$issue_data['flength'] = intval($issue['duration']) / 1000;//发布长度，单位秒
			if(!$issue_data['flength']){
				$issue_data['flength'] = strtotime($issue['endtime']) - strtotime($issue['starttime']);
			}
			
			$issue_data['fstarttime'] = date('Y-m-d H:i:s',strtotime($issue['starttime']));//开始时间
			$issue_data['fendtime'] = date('Y-m-d H:i:s',strtotime($issue['endtime']));//结束时间
			$issue_data['faudiosimilar'] = intval($issue['audiosimilar']);//音频相似度
			$issue_data['fvideosimilar'] = intval($issue['videosimilar']);//视频相似度相似度
			
			$issue_data['fcreator'] = $wxInfo['alias'];//创建人
			$issue_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$issue_data['fmodifier'] = $wxInfo['alias'];//修改人
			$issue_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			if(intval($issue['issample']) == 1) $issue_data['fissample'] = 1;//是否样本，1是，0不是
			
			if($media_class_tab == 'tv'){//电视
				$issue_add = A('Open/SampleModel','Model')->add_tvissue($issue_data);//新增电视发布并获取信息
				
				if(is_array($issue_add) && $issue_add){
					$issueList[] = $issue_add;
				}else{
					$issueError[] = $issue_add;
				}	
			}elseif($media_class_tab == 'bc'){//广播
				$issue_add = A('Open/SampleModel','Model')->add_bcissue($issue_data);//新增广播发布并获取信息
				
				if(is_array($issue_add) && $issue_add){
					$issueList[] = $issue_add;
				}else{
					$issueError[] = $issue_add;
				}
			}
			
		}/*结束添加发布记录*****************************************************************************************结束添加发布记录*/
		
		
		if(count($issueList) == 0){
			#file_put_contents('LOG/issueList_rollback',date('Y-m-d H:i:s') . "\n" . file_get_contents('php://input') . "\n\n\n\n\n",FILE_APPEND);
			M()->rollback();//事务回滚方法
			$this->ajaxReturn(array('code'=>-1,'msg'=>'发布记录全部失败,数据已回滚。' . (implode(',',$issueError)) ));
		}
		
		M()->commit();//事务提交方法
		/* if($post_data['fillegaltypecode'] == -1){
			$taskid = A('Common/InputTask','Model')->add_task(intval(substr($mediaInfo['media_class'],0,2)),$sampleid,[2]);//添加到任务表
		} */
		$e_time = msectime() - $msectime;
		
		#file_put_contents('LOG/kuaijian','新增样本和发布记录,执行时间:'.$e_time.'毫秒,写入条数:'.(count($issueList))."\n",FILE_APPEND);
		$this->ajaxReturn(array('code'=>0,'msg'=>implode(',',$issueError),'issueCount'=>count($issueList),'issueList'=>$issueList,'e_time'=>$e_time));



	}
	
	
	
	
	/*获取模型*/
    public function get_model_list(){
		$msectime = msectime();
		$post_data = file_get_contents('php://input');//获取post数据

		$post_data = json_decode($post_data,true);//把json转化成数组
		if($post_data['start_date'] == '' || $post_data['end_date'] == ''){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'开始时间或结束时间错误'));
		}
		$mediaid = $post_data['mediaid'];//媒体ID
		$start_date = $post_data['start_date'];//开始时间
		$end_date = $post_data['end_date'];//结束时间
		$end_date = date('Y-m-d',strtotime($end_date)+86400);//把结束日期加一天

		
		$mediaInfo = M('tmedia')
							->field('tmedia.fid,left(fmediaclassid,2) as media_class,fmedianame,tmediaowner.fregionid')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->where(array('tmedia.fid'=>$mediaid))->find();//媒介信息

		if(intval($mediaInfo['fid']) == 0){
			$this->ajaxReturn(array('code'=>10027,'msg'=>'查询不到这个媒体'));
		}


		$self_where = array();
		if($mediaInfo['media_class'] == '01'){//电视

			
			$self_where['ttvsample.fmediaid'] = $mediaid;
			$self_where['ttvsample.last_issue_date'] = array('between',$start_date.','.$end_date);

			$self_where['ttvsample.fstate'] = 1;
						
			$modelList = M('ttvsample')//本频道模型
									->field('
											
											ttvsample.fid as fsampleid,
											ttvsample.fmediaid,
											ttvsample.favifilename as ffilename,
											ttvsample.fadlen as fsize,		
											tad.fadname,
											ttvsample.fversion
											')
									->join('tad on tad.fadid = ttvsample.fadid')
									->where($self_where)->select();
		}elseif($mediaInfo['media_class'] == '02'){//广播

			$self_where['tbcsample.fmediaid'] = $mediaid;
			$self_where['tbcsample.last_issue_date'] = array('between',$start_date.','.$end_date);

			$self_where['tbcsample.fstate'] = 1;
							
			$modelList = M('tbcsample')//本频道模型
									->field('
											tbcsample.fid as fsampleid,
											tbcsample.fmediaid,
											tbcsample.favifilename as ffilename,
											tbcsample.fadlen as fsize,	
											tad.fadname,
											tbcsample.fversion
											')
									
									->join('tad on tad.fadid = tbcsample.fadid')
									->where($self_where)->select();						
		}else{//其它情况
			$this->ajaxReturn(array('code'=>10027,'msg'=>'这个媒体既不是电视也不是广播'));
		}
		


		$e_time = msectime() - $msectime;
		file_put_contents('LOG/kuaijian','查询识别模型,执行时间:'.$e_time."毫秒\n",FILE_APPEND);
		$this->ajaxReturn(array('code'=>0,'msg'=>'','modelCount'=>count($modelList),'modelList'=>$modelList));
		
	}
	
	
	
	/*新增广告发布记录*/
	public function add_issue(){
		$msectime = msectime();
		$post_data = file_get_contents('php://input');//获取post数据

		$post_data = json_decode($post_data,true);//把json转化成数组
		
		$mediaid = intval($post_data['mediaid']);//媒体ID
		$mediaInfo = M('tmedia')->field('fid,left(fmediaclassid,2) as media_class,fmedianame')->where(array('fid'=>$mediaid))->find();
		if(intval($mediaInfo['fid']) == 0){
			$this->ajaxReturn(array('code'=>10027,'msg'=>'查询不到这个媒体'));
		}
		

		$wx_id = $post_data['wx_id'];//微信ID
		if(!$wx_id) $wx_id = $post_data['inspector'];
		$wxInfo = M('ad_input_user')->field('wx_id,nickname,alias')->where(array('wx_id'=>$wx_id))->find();//查询微信用户信息
		if(intval($wxInfo['wx_id']) == 0){
			$this->ajaxReturn(array('code'=>10027,'msg'=>'查询不到这个微信用户'));
		}
		
		//$issuedate = date('Y-m-d',strtotime($post_data['starttime']));//发布日期
		
		$issue_list = $post_data['issueList'];//发布记录，数组
		if($mediaInfo['media_class'] == '01'){//电视
			$media_class_tab = 'tv';
		}elseif($mediaInfo['media_class'] == '02'){//广播
			$media_class_tab = 'bc';
		}else{//其它情况
			$this->ajaxReturn(array('code'=>10027,'msg'=>'这个媒体既不是电视也不是广播'));
		}
		$issueList = array();
		foreach($issue_list as $ke => $issue){//循环发布记录/*******************************************************开始添加发布记录*/
			$fcreator = $issue['fcreator'];
			if(!$fcreator) $fcreator = $wxInfo['alias'];
			$issue_data = array();
			$issue_data['f'.$media_class_tab.'sampleid'] = $issue['sampleid'];//样本ID
			$issue_data['f'.$media_class_tab.'modelid'] = 0;//模型ID 
			$issue_data['fmediaid'] = $mediaid;//媒介ID
			$issue_data['fissuedate'] = date('Y-m-d',strtotime($issue['starttime']));//发布日期
			
			
			$issue_data['flength'] = intval($issue['duration']) / 1000;//发布长度，单位秒
			$issue_data['fstarttime'] = date('Y-m-d H:i:s',strtotime($issue['starttime']));//开始时间
			$issue_data['fendtime'] = date('Y-m-d H:i:s',strtotime($issue['endtime']));//结束时间

			$issue_data['fu0'] = intval($issue['u0']);//发布位置，相对当天0点的毫秒数
			$issue_data['fduration'] = intval($issue['duration']);//发布长度，单位毫秒

			$issue_data['faudiosimilar'] = intval($issue['audiosimilar']);//音频相似度
			$issue_data['fvideosimilar'] = intval($issue['videosimilar']);//视频相似度相似度
			
			$issue_data['fcreator'] = $fcreator;//创建人
			$issue_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$issue_data['fmodifier'] = $fcreator;//修改人
			$issue_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			if(intval($issue['issample']) == 1) $issue_data['fissample'] = 1;//是否样本，1是，0不是
			
			
			$sampleInfo = M('t'.$media_class_tab.'sample')->field('fid')->where(array('fid'=>$issue['sampleid']))->find();

			//var_dump($sampleInfo);
			//var_dump($modelInfo);
			
			
			if($sampleInfo){
				
				if($media_class_tab == 'tv'){//电视
				
					$issue_add = A('Open/SampleModel','Model')->add_tvissue($issue_data);//新增电视发布并获取信息
					
					if($issue_add){
						$issueList[] = $issue_add;
						
					} 
				}elseif($media_class_tab == 'bc'){//广播
					$issue_add = A('Open/SampleModel','Model')->add_bcissue($issue_data);//新增广播发布并获取信息
					//var_dump($issue_add);
					if($issue_add){
						$issueList[] = $issue_add;
					}
				}
				
				
			}
			
		}/*结束添加发布记录*****************************************************************************************结束添加发布记录*/
		$e_time = msectime() - $msectime;
		file_put_contents('LOG/kuaijian','新增发布记录,执行时间:'.$e_time.'毫秒,写入条数:'.(count($issueList))."\n",FILE_APPEND);
		$this->ajaxReturn(array('code'=>0,'msg'=>'','issueCount'=>count($issueList),'issueList'=>$issueList));
		
	}
	
	
	/*删除发布记录*/
	public function del_issue(){
		
		$post_data = file_get_contents('php://input');//获取post数据
		//file_put_contents('LOG/aaaaa',$post_data);
		//$this->ajaxReturn(array('code'=>0,'msg'=>''));
		$post_data = json_decode($post_data,true);//把json转化成数组
		
		
		$mediaid = $post_data['mediaid'];//媒体ID
		$mediaInfo = M('tmedia')
								->field('tmedia.fid,left(tmedia.fmediaclassid,2) as media_class,tmedia.fmedianame,tmediaowner.fregionid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>$mediaid))->find();
		//var_dump(M('tmedia')->getLastSql());						
		if(intval($mediaInfo['fid']) == 0){
			$this->ajaxReturn(array('code'=>10027,'msg'=>'查询不到这个媒体'));
		}
		
		$wx_id = $post_data['wx_id'];//微信ID
		if(!$wx_id) $wx_id = $post_data['inspector'];
		$wxInfo = M('ad_input_user')->field('wx_id,nickname')->where(array('wx_id'=>$wx_id))->find();//查询微信用户信息
		if(intval($wxInfo['wx_id']) == 0){
			$this->ajaxReturn(array('code'=>10027,'msg'=>'wx_id错误,查询不到这个用户'));
		}
		

		
		$issue_list = $post_data['issueList'];//发布记录，数组
		
		if($mediaInfo['media_class'] == '01'){//电视
			$media_class_tab = 'tv';
		}elseif($mediaInfo['media_class'] == '02'){//广播
			$media_class_tab = 'bc';
		}else{//其它情况
			$this->ajaxReturn(array('code'=>10027,'msg'=>'这个媒介既不是电视也不是广播'));
		}
		$delCount = 0;
		$del2Count = 0;
		foreach($issue_list as $issue){//循环发布记录/*******************************************************开始添加发布记录*/
			$issueInfo = M('t'.$media_class_tab.'issue')->field('fmediaid,fstarttime,fendtime')->where(array('f'.$media_class_tab.'issueid'=>$issue))->find();//查询发布记录
			M('del_issue')->add(array(
										'fmediaid'=>$mediaInfo['fid'],
										'fstarttime'=>strtotime($issueInfo['fstarttime']),
										'fendtime'=>strtotime($issueInfo['fendtime']),
										'del_time'=>date('Y-m-d H:i:s')
										
										));
			
			$s_issue_table = 't'.$media_class_tab.'issue_'.date('Ym',strtotime($issueInfo['fstarttime'])).'_'.substr($mediaInfo['fregionid'],0,2);
			
			M()->startTrans();//开启事务方法
			try{//
				$del2Count += M($s_issue_table)->where(array('fmediaid'=>$mediaid,'fstarttime'=>strtotime($issueInfo['fstarttime'])))->delete();
			}catch( \Exception $error) { 
				
			} 
			if( M('t'.$media_class_tab.'issue')->where(array('f'.$media_class_tab.'issueid'=>$issue))->delete() > 0) $delCount++;
			M()->commit();//事务提交方法
			
			
			
			
		}/*结束添加发布记录*****************************************************************************************结束添加发布记录*/
		$this->ajaxReturn(array('code'=>0,'msg'=>'','delCount'=>$delCount,'del2Count'=>$del2Count));
		
		
	}
	
	
	/*查询样本详情*/
	public function sampleInfo(){
		
		$post_data = file_get_contents('php://input');//获取post数据
		$post_data = json_decode($post_data,true);//把json转化成数组

		$media_class_tab = $post_data['media_class_tab'];//电视、广播标记，电视:tv,广播:bc
		$sample_id = intval($post_data['sample_id']);//样本ID
		if($sample_id == 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'sample_id 不能为空'));
		
		
		if($media_class_tab != 'tv' && $media_class_tab != 'bc') $this->ajaxReturn(array('code'=>-1,'msg'=>'media_class_tab 错误,只能是tv或bc'));//电视、广播标记错误
		
		$sampleInfo0 = M('t'.$media_class_tab.'sample')->alias('sample')
										->field('
												    sample.fid as sample_id,
													sample.fissuedate as issuedate,
													sample.fversion as version,
													sample.fspokesman as spokesman,
													sample.fapprovalid as approvalid,
													sample.fapprovalunit as approvalunit,
													sample.fillegalcontent as illegalcontent,
													sample.favifilename as avifilename,
													sample.fcreator as creator,
													sample.fcreatetime as createtime,
													sample.fmodifier as modifier,
													sample.fmodifytime as modifytime,
													sample.fexpressioncodes,
													sample.fadmanuno,
													sample.fmanuno,
													sample.fadapprno,
													sample.fapprno,
													sample.fadent,
													sample.fent,
													sample.fentzone,
													sample.fstate,
													
													tmedia.fmedianame as media_name,
													tad.fadname as ad_name,
													tad.fbrand as brand,
													tad.fadclasscode as adclasscode,
													tad.fadclasscode_v2 as adclasscode_v2,
													
													
													tadclass.fadclass as adclass,
													tadclass.ffullname as adclass_fullname,
													tadowner.fname as adowner_name,
													tillegaltype.fillegaltype as illegaltype
													
													
												')
										->join('tmedia on tmedia.fid = sample.fmediaid')
										->join('tad on tad.fadid = sample.fadid')
										->join('tadclass on tadclass.fcode = tad.fadclasscode')
										->join('tadowner on tadowner.fid = tad.fadowner')	
										->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
										
										->where(array('sample.fid'=>$sample_id))->find();
		$sampleInf0 = array();								
		foreach($sampleInfo0 as $field => $sam){
			
			$sampleInf0[$field] = strval($sam);
		}		
		
		
		
		$fexpressioncodes_array = explode(';',$sampleInf0['fexpressioncodes']);
		if(count($fexpressioncodes_array > 0)){
			$sampleInf0['illegalList'] = M('tillegal')->cache(true,600)->field('fcode as fillegalcode,fexpression,fconfirmation')->where(array('fcode'=>array('in',$fexpressioncodes_array)))->select();
			
		}else{
			$sampleInf0['illegalList'] = array();
		}

		
		$sampleInf0['modelList'] = array();	//模型信息赋值
			
		$this->ajaxReturn(array('code'=>0,'msg'=>'','sampleInfo'=>$sampleInf0));								
		
		
		
	}
	
	
	/*修改广告样本信息*/
	public function edit_sample(){
		$post_data = file_get_contents('php://input');//获取post数据
		//file_put_contents('LOG/F',$post_data);
		$post_data = json_decode($post_data,true);//把json转化成数组

		$mediaid = intval($post_data['mediaid']);//媒体ID
		$sampleid = intval($post_data['sampleid']);//媒体ID
		$mediaInfo = M('tmedia')->field('fid,left(fmediaclassid,2) as media_class,fmedianame')->where(array('fid'=>$mediaid))->find();

		if(intval($mediaInfo['fid']) == 0){
			$this->ajaxReturn(array('code'=>10027,'msg'=>'查询不到这个媒体'));
		}
		
		$wx_id = $post_data['wx_id'];//微信ID
		$wxInfo = M('ad_input_user')->field('wx_id,nickname,alias')->where(array('wx_id'=>$wx_id))->find();//查询微信用户信息
		if(intval($wxInfo['wx_id']) == 0){
			$this->ajaxReturn(array('code'=>10027,'msg'=>'查询不到这个微信用户'));
		}

		
		
		$fadname = $post_data['fadname'];//广告名称
		$fbrand = $post_data['fbrand'];//品牌名称
		$fadclasscode = $post_data['fadclasscode'];//广告分类code
		$fadclasscode_v2 = $post_data['fadclasscode_v2'];//广告分类code
		
		$adowner_name = $post_data['adowner_name'];//广告主名称
		$fversion = $post_data['fversion'];//版本说明
		$fspokesman = $post_data['fspokesman'];//代言人
		$fapprovalid = $post_data['fapprovalid'];//审批号
		$fapprovalunit = $post_data['fapprovalunit'];//审批单位
		$fexpressioncodes = $post_data['fexpressioncodes'];//违法表现代码，数组
		$fillegalcontent = $post_data['fillegalcontent'];//违法内容
		$fillegaltypecode = $post_data['fillegaltypecode'];//违法程度

		
		
		if($mediaInfo['media_class'] == '01'){//电视
			$media_class_tab = 'tv';
		}elseif($mediaInfo['media_class'] == '02'){//广播
			$media_class_tab = 'bc';
		}else{//其它情况
			$this->ajaxReturn(array('code'=>10027,'msg'=>'这个媒体既不是电视也不是广播'));
		}

		M()->startTrans();//开启事务方法
		$sample_data = array();/*开始修改样本表数据*************************************************************开始修改样本表数据*/

		$adid = A('Common/Ad','Model')->getAdId($fadname,$fbrand,$fadclasscode,$adowner_name,$wxInfo['alias'], $fadclasscode_v2);//获取广告ID
		
		
		$sample_data['fadid'] = intval($adid);//广告ID

		$sample_data['fversion'] = $fversion;//版本说明
		$sample_data['fspokesman'] = $fspokesman;//代言人
		$sample_data['fapprovalid'] = $fapprovalid;//审批号
		$sample_data['fapprovalunit'] = $fapprovalunit;//审批单位

		$sample_data['fmodifier'] = $wxInfo['alias'];//修改人
		$sample_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		
		
		$sample_data['fadmanuno'] = $post_data['fadmanuno'];// 广告中标识的生产批准文号 
		$sample_data['fmanuno'] = $post_data['fmanuno'];// 生产批准文号 
		$sample_data['fadapprno'] = $post_data['fadapprno'];//广告中标识的广告批准文号 
		$sample_data['fapprno'] = $post_data['fapprno'];//广告批准文号 
		$sample_data['fadent'] = $post_data['fadent'];//广告中标识的生产企业（证件持有人）名称 
		$sample_data['fent'] = $post_data['fent'];//生产企业（证件持有人）名称 
		$sample_data['fentzone'] = $post_data['fentzone'];//生产企业（证件持有人）所在地区 
		
		if($post_data['fstate'] != ''){
			$sample_data['fstate'] = $post_data['fstate'];//修改样板状态
		}
		

		if(count($fexpressioncodes) > 0){
			$illegalList = M('tillegal')->where(array('fcode'=>array('in',$fexpressioncodes)))->select();
			
			
			$fexpressioncodes_array = array();
			$fexpressions_array = array();
			$fconfirmations_array = array();
			$fpunishments_array = array();
			$fpunishmenttypes_array = array();
			
			
			foreach($illegalList as $illegal){
				$fexpressioncodes_array[] = $illegal['fcode'];//违法表现代码
				$fexpressions_array[] = $illegal['fexpression'];//违法表现
				$fconfirmations_array[] = $illegal['fconfirmation'];//认定依据
				$fpunishments_array[] = $illegal['fpunishment'];//处罚依据
				$fpunishmenttypes_array[] = $illegal['fpunishmenttype'];//处罚种类及幅度
				
			}
			
			$sample_data['fillegalcontent'] = $fillegalcontent;////
			$sample_data['fillegaltypecode'] = $fillegaltypecode;////
			$sample_data['fexpressioncodes'] = implode(';',$fexpressioncodes_array);
			$sample_data['fexpressions'] = implode("\n",$fexpressions_array);
			$sample_data['fconfirmations'] = implode("\n",$fconfirmations_array);
			$sample_data['fpunishments'] = implode("\n",$fpunishments_array);
			$sample_data['fpunishmenttypes'] = implode("\n",$fpunishmenttypes_array);
		}else{
			$sample_data['fillegalcontent'] = '';////
			$sample_data['fillegaltypecode'] = 0;////
			$sample_data['fexpressioncodes'] = '';
			$sample_data['fexpressions'] = '';
			$sample_data['fconfirmations'] = '';
			$sample_data['fpunishments'] = '';
			$sample_data['fpunishmenttypes'] = '';
		}
		
		$edit_state = M('t'.$media_class_tab.'sample')->where(array('fid'=>$sampleid))->save($sample_data);//修改样本表数据****************************************结束修改样本表数据*/
		
		if(intval($post_data['fstate']) <= 0){ #样本修改为删除状态
			$uuid = M('t'.$media_class_tab.'sample')->where(array('fid'=>$sampleid))->getField('uuid');
			
			/* 
			$postData = array();
			$postData['fid'] = $mediaid;
			$postData['uuid'] = $uuid;
			$postData['source'] = 2;
			$postData['error_cause'] = '修改人:'.$wxInfo['alias'];
			$rett = http('http://47.96.182.117/index/saveUuid',$postData,'POST');
			 */	
			#file_put_contents('LOG/sam_saveUuid.log',date('Y-m-d H:i:s')."\n" . json_encode($postData) . "\n" . $rett . "\n\n\n",FILE_APPEND);
			
			$gbUrl = 'http://47.96.163.56:8002/sp_time_out/'.$mediaid.'/'.$uuid.'/'. time() .'/';
			$postData = array();

			$postData['auther'] = '修改人:'.$wxInfo['wx_id'].'_'.$wxInfo['alias'];
			$rett = http($gbUrl,$postData,'POST');
			#file_put_contents('LOG/sam_saveUuid.log',date('Y-m-d H:i:s')."\n" .$gbUrl."\n". json_encode($postData) . "\n" . $rett . "\n\n\n",FILE_APPEND);
		}
			
			
		if(intval($edit_state) == 0){//判断是否修改成功
			M()->rollback();//事务回滚方法
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改样本表数据失败'));
		}
		
		M()->commit();//事务提交方法
		$this->ajaxReturn(array('code'=>0,'msg'=>''));


		
		
	}
	
	
	
	
	
}