<?php

namespace Common\Model;
use Think\Exception;


class AdClassModel{

	
	 /*获取广告分类*/
    public function getAdClassCode($fcode = ''){
		


		$adclassList = M('tadclass')->where(array('fpcode'=>$fcode))->getField('fcode',true);//查询广告分类列表
		
		return $adclassList;
	}
	
	/**
     * 返回广告类型树
     * @return array
     */
    public function getAdClassArr()
    {
        $data = M('tadclass')
            ->field('fcode, fpcode, fadclass')
            ->where(['fstate' => ['EQ', 1], 'fcode' => ['NEQ', 2301]])
            ->cache(true)
            ->select();
        $res = [];
        foreach ($data as $key => &$value) {
            if (strlen($value['fcode']) === 2){
                $res[] = [
                    'value' => $value['fcode'],
                    'label' => $value['fadclass'],
                ];
            }
        }
        foreach ($data as $key => &$value2) {
            $value2['value'] = $value2['fcode'];
            $value2['label'] = $value2['fadclass'];
            if (strlen($value2['fcode']) === 4){
                foreach ($res as &$item) {
                    if ($value2['fpcode'] === $item['value']){
                        $item['children'][] = $value2;
                        break;
                    }
                }
            }
        }
        return $res;
    }
	
	
	
	/*获取商业分类列表（从下到上）*/
	public function get_adclass_v2_array($fcode = '',$adclass_v2_array = array(),$num = 0){
		$adclass_v2_array[] = $fcode;
		$vv = M('hz_ad_class')->cache(true,600)->where(array('fcode'=>$fcode))->find();
		if(!$vv['fpcode'] || $num > 5){
			return $adclass_v2_array;
		}else{
			return $this->get_adclass_v2_array($vv['fpcode'],$adclass_v2_array,$num+1 );
		}	
			
	}
	
	/*获取商业分类列表（从下到上）*/
	public function get_adclass_array($fcode = '',$adclass_array = array(),$num = 0){
		$adclass_array[] = $fcode;
		$vv = M('tadclass')->cache(true,600)->where(array('fcode'=>$fcode))->find();
		
		if(!$vv['fpcode'] || $num > 5){
			#var_dump($adclass_array);
			return $adclass_array;
		}else{
			#var_dump($vv['fpcode']);
			return $this->get_adclass_array($vv['fpcode'],$adclass_array,$num+1 );
		}	
			
	}
    /**
     * @Des: 获取广告类别列表(固定3级，多级需扩展)
     * @Edt: yuhou.wang
     * @param {type} 
     * @return: 
     */
    public function getAdClassList(){
        $data = M('tadclass')
            ->field('fcode, fpcode, fadclass')
            ->where(['fstate' => ['EQ', 1]])
            ->cache(true,86400)
            ->select();
        $res = [];
        foreach($data as $key => $value){
            $fcodelen = strlen($value['fcode']);
            $node = [
                'value' => $value['fcode'],
                'label' => $value['fadclass'],
            ];
            switch($fcodelen){
                case 2:
                    $i = $value['fcode'];
                    $res[$i] = $node;
                    break;
                case 4:
                    $i = $value['fpcode'];
                    $j = $value['fcode'];
                    $res[$i]['children'][$j] = $node;
                    break;
                case 6:
                    $i = substr($value['fpcode'],0,2);
                    $j = $value['fpcode'];
                    $k = $value['fcode'];
                    $res[$i]['children'][$j]['children'][$k] = $node;
                    break;
            }
        }
        // 重新整理成顺序数字索引 
        $res = array_values($res);
        foreach($res as &$val){
            if($val['children']){
                $val['children'] = array_values($val['children']);
                foreach($val['children'] as &$val2){
                    if($val2['children']){
                        $val2['children'] = array_values($val2['children']);
                    }
                }
            }
        }
        return $res;
    }
    /**
     * @Des: 获取商业类别列表(固定3级，多级需扩展)
     * @Edt: yuhou.wang
     * @param {type} 
     * @return: 
     */
    public function getAdClassBList(){
        $data = M('hz_ad_class')
            ->field('fcode, fpcode, fname')
            ->where(['fstatus' => ['EQ', 1]])
            ->cache(true,86400)
            ->select();
        $res = [];
        foreach($data as $key => $value){
            $fcodelen = strlen($value['fcode']);
            $node = [
                'value' => $value['fcode'],
                'label' => $value['fname'],
            ];
            switch($fcodelen){
                case 2:
                    $i = $value['fcode'];
                    $res[$i] = $node;
                    break;
                case 4:
                    $i = $value['fpcode'];
                    $j = $value['fcode'];
                    $res[$i]['children'][$j] = $node;
                    break;
                case 6:
                    $i = substr($value['fpcode'],0,2);
                    $j = $value['fpcode'];
                    $k = $value['fcode'];
                    $res[$i]['children'][$j]['children'][$k] = $node;
                    break;
            }
        }
        // 重新整理成顺序数字索引 
        $res = array_values($res);
        foreach($res as &$val){
            if($val['children']){
                $val['children'] = array_values($val['children']);
                foreach($val['children'] as &$val2){
                    if($val2['children']){
                        $val2['children'] = array_values($val2['children']);
                    }
                }
            }
        }
        return $res;
    }
}