<?php
namespace TaskInput\Model;
use Think\Model;

/**
 * 众包质检任务模型
 */
class QualityInspectionModel{
    /**
     * 抽检率
     * 1、新广告主题抽检100%；
     * 2、违法广告抽检率100%；
     * 3、五大类广告抽检率50%；
     * 4、其他广告抽检率10%；
     */
    protected $rate = [
        'newad'     => 100,
        'illegalad' => 100,
        'special'   => 50,
        'others'    => 10,
    ];

    /**
     * 添加任务
     * @param Array $taskData 任务数据
     * @param Boolean $isNewAd 是否新广告主题 true-是 false-否
     */
    public function addInsTask($taskData = [],$isNewAd){
        if(!empty($taskData)){
            // 依据质检率设置判断是否进入质检
            $isTaskInspect = false;
            // 是否为新广告主题，有传参则以传参为准，未传参则判断
            $newad = false;
            if(isset($isNewAd)){
                $newad = $isNewAd;
            }else{
                $fcreatetime = M('tad')->where(['fadid'=>$taskData['fadid']])->getField('fcreatetime');
                if($fcreatetime >= date('Y-m-d 00:00:00') && $fcreatetime <= date('Y-m-d 23:59:59')){
                    // 任务的广告主题是否当天新建
                    $newad = true;
                }
            }
            // 是否需要质检
            if($newad){
                // 新广告主题
                $isTaskInspect = $this->isInspectByRate($this->rate['newad']);
            }elseif($taskData['fillegaltypecode'] > 0){
                // 违法广告
                $isTaskInspect = $this->isInspectByRate($this->rate['illegalad']);
            }elseif(A('TaskInput/TaskInputScore','Model')->isSpecialClass($taskData)){
                // 五大类
                $isTaskInspect = $this->isInspectByRate($this->rate['special']);
            }else{
                // 其它
                $isTaskInspect = $this->isInspectByRate($this->rate['others']);
            }
            // 任务来源表名
            $ftablename = '';
            switch($taskData['media_class']){
                case '01':
                case '02':
                case '03':
                case '1':
                case '2':
                case '3':
                    $ftablename = 'task_input';
                    break;
                case '05':
                case '5':
                    $ftablename = 'todtask';
                    break;
                case '13':
                    $ftablename = 'tnettask';
                    break;
            }
            // 添加质检
            if($isTaskInspect){
                // 更新来源任务表质检状态 0待质检
                // $this->setTaskQualityIns($ftablename,$taskData['taskid'],0);
                $existWhere = [
                    'ftablename'  => $ftablename,
                    'ftaskid'     => $taskData['taskid'],
                    'fmediaclass' => $taskData['media_class'],
                ];
                $isExist = M('ttask_quality_inspection')->where($existWhere)->count() > 0 ? 1 : 0;
                if(!$isExist){
                    // 质检任务数据
                    $taskData = [
                        'ftablename'   => $ftablename,
                        'ftaskid'      => $taskData['taskid'],
                        'fmediaclass'  => $taskData['media_class'],
                        'foperator_id' => 0,
                        'fstate'       => 0,
                        'fcreator'     => $taskData['wx_id'],
                        'fcreatetime'  => time(),
                    ];
                    $ftask_quality_inspection_id = M('ttask_quality_inspection')->add($taskData);
                    // 流程日志
                    $flowData = [
                        'ftask_quality_inspection_id' => $ftask_quality_inspection_id,
                        'foperator_id'                => $taskData['wx_id'],
                        'log'                         => '创建质检任务',
                        'logtype'                     => 1,
                        'creator'                     => 'API',
                        'createtime'                  => time(),
                    ];
                    M('ttask_quality_inspection_flowlog')->add($flowData);
                }
            }else{
                // 更新来源任务表质检状态,-1无需质检
                // $this->setTaskQualityIns($ftablename,$taskData['taskid'],-1);
            }
            return true;
        }
        return false;
    }

    /**
     * 依据抽检率概率判断是否抽检
     */
    public function isInspectByRate($rate = 0){
        if($rate > 0){
            $rand = mt_rand(1,100);
            if(1 <= $rand && $rand <= $rate){
                return true;
            }
        }
        return false;
    }

    /**
     * 更新来源任务的质检状态
     */
    public function setTaskQualityIns($tablename,$id,$state = 0,$operator = 0,$reason = ''){
        $saveData = [
            'quality_state' => $state,
        ];
        if($operator){
            $saveData['quality_wx_id'] = $operator;
        }
        if($reason){
            $saveData['back_reason'] = $reason;
        }
        $res = M($tablename)->where(['taskid'=>$id])->save($saveData);
        if($res){
            return true;
        }
        return false;
    }

    /**
     * 流程日志
     */
    public function log($logArr = []){
        $wx_id = session('wx_id');
        $name = M('ad_input_user')->cache(true,86400)->where(['wx_id'=>$wx_id])->getField('alias');
        $logArr = [
            'ftask_quality_inspection_id' => $logArr['ftask_quality_inspection_id'],
            'foperator_id'                => $logArr['foperator_id'] ? $logArr['foperator_id'] : session('wx_id'),
            'foperator'                   => $logArr['foperator'] ? $logArr['foperator'] : $name,
            'log'                         => $logArr['log'],
            'logtype'                     => $logArr['logtype'],
            'taskinfo'                    => $logArr['taskinfo'],
            'creator'                     => $logArr['creator'] ? $logArr['creator'] : getClientIp(),
            'createtime'                  => $logArr['createtime'] ? $logArr['createtime'] : time(),
        ];
        $res = M('ttask_quality_inspection_flowlog')->add($logArr);
        if(!$res){
            return false;
        }
        return true;
    }
}
