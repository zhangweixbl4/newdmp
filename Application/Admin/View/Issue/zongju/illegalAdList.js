var illegalAdList = {
    template: `<div v-loading="loading">
    <el-table
            :data="table"
            style="width: 100%"
            @row-click="openAdInfo"
            >
        <el-table-column
                prop="fad_name"
                label="广告名称">
        </el-table-column>
        <el-table-column
                prop="adClass"
                label="广告类别">
        </el-table-column>
        <el-table-column
                prop="stateText"
                label="确认状态">
        </el-table-column>
    </el-table>
    <el-pagination
            layout="total, prev, pager, next"
            @current-change="pageChange"
            :page-size="10"
            :current-page="page"
            :total="total">
    </el-pagination>
    <el-dialog
    :title="medianame + ', 广告详情'"
    :visible.sync="showAdInfo"
    :modal="false"
    :before-close="closeAdInfo"
    width="100%"
    >
        <ad-info :adid="activeAd" @close="closeAdInfo" ref="adInfo"></ad-info>
    </el-dialog>
</div>
    `,
    props: ['mediaid', 'date', 'medianame'],
    components:{
        adInfo: adInfo
    },
    data: function(){
        return {
            loading: false,
            table: [],
            total: 0,
            page: 1,
            activeAd: null,
            showAdInfo: false
        }
    },
    methods:{
        getTable: function () {
            this.loading = true
            $.post('getIllegalAdList', {
                mediaId: this.mediaid,
                date: this.date,
                page: this.page
            }).then((res) => {
                this.table = res.data
                this.total = +res.total
                this.loading = false
            })
        },
        dataInit: function () {
            this.table = []
            this.loading = false
            this.page = 1
            this.total = 0
        },
        pageChange: function (page) {
            this.page = page
            this.getTable()
        },
        closeAdInfo: function () {
            this.activeAd = null
            this.showAdInfo = false
            this.$nextTick(() => {
                this.$refs.adInfo.dataInit()
            })
            this.getTable()
        },
        openAdInfo: function (row) {
            this.activeAd = row.fid
            this.showAdInfo = true
            this.$nextTick(() => {
                this.$refs.adInfo.getAdInfo()
            })

        }
    }

}