const timeSelect = {
	
  template: `
  <div class="timecontainer"
    v-loading="loading"
    id="timecontainer">
    <div class="swrap" @mouseleave="activeDay = ''">
      <div class="mainbox">
        <div class="_boxlist" @click.stop>
          <div class="_list" v-for="i in tableHead" :key="i.day">{{i.day}}</div>
        </div>
        <div id="listWrap">
          <div class="boxlist"  v-for="(item,index0) in list" :key="item.fid"><!-- @mousedown="mousedownfn" -->
            <div class="list" :title=i.msg
              v-for="(i,index) in item.days"
              :class="{
                selected: i.selected === 1,
                archive0: i.fixed_state == '0',
                archive1: i.fixed_state == '1',
                'archive-1': i.fixed_state == '-1',
                archive99: i.fixed_state == '99',
                hover: i.day === activeDay,
                overtime: isOverTime(i)
              }"
			        :source_collect_intact = "i.source_collect_intact"
					:data="item.fid + ',' + index + ',' + i.fday"
              
			  
			  @click.stop="showInfo(item,i)"
			  :xx="index"
			  :yy="index0"
              @mouseenter="setActiveDate(i)"
              @mouseleave="i.showNum = false"
              :key="index">
              <div class="dayNum" :xx="index" :yy="index0">
                {{i.day}}
                <div class="source_collect_intact" v-if="i.source_collect_intact" :style="{width: 'calc(34px * ' + i.source_collect_intact + ')'}"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <el-dialog
      class="info-dialog"
      title=""
      :visible.sync="showDialog"
      :show-close="false"
      width="400px">
      <div v-html="activeDateData"></div>
    </el-dialog>
    <el-drawer
      :title="title"
      :visible.sync="drawer"
      direction="rtl">
      <div style="height: calc(100vh - 100px); overflow: auto;">
        <el-table
          :data="logData"
          style="width: 100%">
          <el-table-column
            type="index"
            width="50">
          </el-table-column>
          <el-table-column
            prop="fvalue"
            label="操作内容">
            <template slot-scope="scope">
              <el-tooltip effect="dark" placement="left">
                <div slot="content" v-html="scope.row._fvalue"></div>
                <div v-html="scope.row.fvalue" style="width:10em;overflow: hidden;white-space:nowrap;text-overflow:ellipsis;"></div>
              </el-tooltip>
            </template>
          </el-table-column>
          <el-table-column
            prop="fperson"
            label="操作人"
            width="100">
          </el-table-column>
          <el-table-column
            prop="ftime"
            label="操作时间"
            width="140">
          </el-table-column>
        </el-table>
      </div>
    </el-drawer>
  </div>`,




  props: {
    tableData: {
      type: Array,
      required: true,
    },
    tableHead: {
      type: Array,
      required: true,
    }
  },
  data() {
    return {
      drawer: false,
      title: '',
      list: [],
      selectAll: false,
      isSelect: true,
      selectBoxDashed: null,
      startX: null,
      startY: null,
      initx: null,
      inity: null,
      scrollX: null,
      scrollY: null,
      logData: [],
      loading: false,
      timer: null,
      isMove: false,
      showDialog: false,
      activeDay: '',
      activeDateData: '',
      state: {
        '0': {
          label: '未归档',
          class: 'archive0'
        },
        '1': {
          label: '已归档',
          class: 'archive1'
        },
        '-1': {
          label: '逾期未归档',
          class: 'archive-1'
        },
        '99': {
          label: '特殊',
          class: 'archive99'
        },
      }
    }
  },
  watch: {
    tableData: {
      handler() {
        this.list = [...this.tableData]
      },
      deep: true
    }
  },
  created() {
    this.list = [...this.tableData]
  },
  methods: {
    isOverTime({last_time = 0, last_change_time = 0}) {
      return Number(last_change_time) > Number(last_time)
    },
    clearData() {
      this.checkData = []
    },
    showInfo( media,row) {
		
		//console.log(event.ctrlKey )

		if(event.ctrlKey){
			
			//console.log(event.target.getAttribute('xx'),event.target.getAttribute('yy'))
			xx = event.target.getAttribute('xx')
			yy = event.target.getAttribute('yy')
			$('.list').each(function(){
				
				myxx = $(this).attr('xx')
				myyy = $(this).attr('yy')
				console.log(myxx,myyy)
				if(xx == myxx && yy == myyy){
					$(this).addClass('selected')
				}
			});
			
		}else if(event.layerY < 20){
				mediaid = media.fid
			  //if (this.isMove) return;
			  this.loading = true
			  //clearTimeout(this.timer)
			  //this.timer = setTimeout(() => {
				//this.$set(row, 'visible', true)
				//if (row.msg) return;
				let data = {
				  fmediaid: mediaid,
				  fdate: row.fday
				}
				fly.post('/Admin/FixedMediaData/get_msg', Qs.stringify(data)).then(({ data }) => {
				  this.showDialog = true
				  this.loading = false
				  this.activeDateData = data.msg // + `<br/>采集完整率：${(row.source_collect_intact * 100).toFixed(2)}%`
				}).catch(() => {
				  this.loading = false
				})
			  //},1)
		}else if(event.layerY > 20){
			this.getLog(media,row)
		}
	  
    },
    getLog(item, i) {
      clearTimeout(this.timer)
      this.loading = true
      this.title = `${item.fmedianame} ${i.fday} 的归档日志`
      fly.get(`/Admin/FixedMediaData/s_log?fmediaid=${item.fid}&fdate=${i.fday}`).then(({data}) => {
        if (data.fixed_media_data_log_list.length) {
          this.logData = data.fixed_media_data_log_list.map(i => {
            i._fvalue = i.fvalue
            i.fvalue = i.fvalue.replace(/<\/br>/g, '')
            return i
          })
          this.drawer = true
        } else {
          $h.notify.info('该日无归档日志！',this)
        }
        this.loading = false
      })
    },
    clearBubble(e) {
      if (e.stopPropagation) {
        e.stopPropagation()
      } else {
        e.cancelBubble = true
      }
      if (e.preventDefault) {
        e.preventDefault()
      } else {
        e.returnValue = false
      }
    },
    mousedownfn(e) {
      //  创建选框节点
      this.selectBoxDashed = document.createElement('div')
      this.selectBoxDashed.className = 'select-box-dashed'

      document.getElementById('timecontainer').appendChild(this.selectBoxDashed)
      this.scrollX = document.documentElement.scrollLeft || document.body.scrollLeft
      this.scrollY = document.documentElement.scrollTop || document.body.scrollTop
      //  设置选框的初始位置
      this.startX = e.x + this.scrollX || e.clientX + this.scrollX
      this.startY = e.y + this.scrollY || e.clientY + this.scrollY
      this.selectBoxDashed.style.cssText = `left:${this.startX}px;top:${this.startY}px;pointer-events: none;`
      //  清除事件冒泡、捕获
      this.clearBubble(e)
      document.getElementById('timecontainer').addEventListener('mousemove', this.mousemovefn)
      document.getElementById('timecontainer').addEventListener('mouseup', this.mouseUpfn)
    },
    mousemovefn(e) {
      //  根据鼠标移动，设置选框的位置、宽高
      this.initx = e.x + this.scrollX || e.clientX + this.scrollX
      this.inity = e.y + this.scrollY || e.clientY + this.scrollY
      //  暂存选框的位置及宽高，用于将 select-item 选中
      this.left = Math.min(this.initx, this.startX)
      this.top = Math.min(this.inity, this.startY)
      this.width = Math.abs(this.initx - this.startX)
      this.height = Math.abs(this.inity - this.startY)

      // 当框选范围大于一定值时才认定为是选择
      if (this.width <= 10 && this.height <= 10) {
        this.clearBubble(e)
        return false
      } else {
        // 全局变量
        this.isMove = true
        //  设置选框可见
        this.selectBoxDashed.style.display = 'block'
      }

      this.selectBoxDashed.style.left = `${this.left}px`
      this.selectBoxDashed.style.top = `${this.top}px`
      this.selectBoxDashed.style.width = `${this.width}px`
      this.selectBoxDashed.style.height = `${this.height}px`
      let fileDivs = document.getElementsByClassName('list')
      for (let i = 0; i < fileDivs.length; i++) {
        let itemX_pos = fileDivs[i].offsetWidth + fileDivs[i].offsetLeft
        let itemY_pos = fileDivs[i].offsetHeight + fileDivs[i].offsetTop
        let condition1 = itemX_pos > this.left
        let condition2 = itemY_pos > this.top
        let condition3 = fileDivs[i].offsetLeft < (this.left + this.width)
        let condition4 = fileDivs[i].offsetTop < (this.top + this.height)
        if (condition1 && condition2 && condition3 && condition4) {
          fileDivs[i].classList.add('temp-selected')
        } else {
          fileDivs[i].classList.remove('temp-selected')
        }
      }
      this.clearBubble(e)
    },
    mouseUpfn(e) {
      this.selectAllornot(0).then(() => {
        document.getElementById('timecontainer').removeEventListener('mousemove', this.mousemovefn)
        let selectDom = document.getElementsByClassName('temp-selected');
        [].slice.call(selectDom).forEach(item => {
          if (item.classList.contains('selected')) {
            item.classList.remove('selected')
          } else {
            item.classList.add('selected')
          }
          item.classList.remove('temp-selected')
        })
        if (this.selectBoxDashed) {
          try {
            this.selectBoxDashed.parentNode.removeChild(this.selectBoxDashed)
            this.selectBoxDashed = null
          } catch (err) {
            console.log(err)
          }
        }
        let fileDivs = document.getElementsByClassName('list')
        for (let i = 0; i < fileDivs.length; i++) {
          let data = fileDivs[i].getAttribute('data').split(',')
          if (fileDivs[i].classList.contains('selected')) {
            this.toggleSelected(1, data)
          } else {
            this.toggleSelected(0, data)
          }
        }
        this.clearBubble(e)
        setTimeout(() => {
          this.isMove = false
        }, 200)
        this.$nextTick(() => {
          this.getData()
          this.$emit('triggertime', this.getData())
        })
      })
    },
    getData() {
      let fileDivs = document.getElementsByClassName('list')
      let selected = Array.prototype.map.call(fileDivs, i => {
        if (i.classList.contains('selected')) {
          return i.getAttribute('data').split(',')
        }
        return null
      }).filter(i => i)
      let keys = Array.from(new Set(selected.map(i => i[0])))
      let result = {}
      keys.forEach(i => {
        let _days = selected.filter(v => v[0] === i).map(c => c[2]).join(',')
        result[i] = _days
      })
      return result
    },
    toggleSelected(value, data){
      let [fid, _index] = data
      let index = this.getIndex(this.list, 'fid', fid)
      this.list[index].days = this.list[index].days.map((i,index) => {
        if(index === +_index){
          i.selected = value
        }
        return i
      })
    },
    setcurrent(item) {
      this.$set(item, 'selected', item.selected ? 0 : 1)
    },
    selectAllornot(e) {
      return new Promise((resolve, reject) => {
        let value = e ? 1 : 0
        this.list = this.list.map(i => {
          i.days.map(v => {
            v.selected = value
            return v
          })
          return i
        })
        this.$emit('triggertime', this.getData())
        resolve()
      })
    },
    getIndex(box, name, objectId) {
      let index = box.map(e => e[name]).indexOf(objectId)
      return index
    },
    setActiveDate(i){
      this.$set(i, 'showNum', true)
      this.activeDay = i.day
    }
  }
}