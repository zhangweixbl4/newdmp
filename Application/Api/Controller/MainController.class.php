<?php
namespace Api\Controller;
use Think\Controller;
class MainController extends Controller {

    public function up_main(){

        $up_main_data = json_decode($_POST['JsonData'],true);
        if(empty($up_main_data)) return $this->ajaxReturn(['code'=>-1,'message'=>'未接收到数据']);

        ini_set('max_execution_time', 0);

        $tables = [1 => 'tmediaowner' ,2 => 'tadowner',3 => 'tracker'];
        $names = [1 => 'fname' ,2 => 'fname',3 => 'entname'];

        foreach ($up_main_data as $main_data){
            M()->startTrans();

            if(empty($main_data['mainType'])) return $this->ajaxReturn(['code'=>-1,'message'=>'mainType must be set!']);

            if(empty($main_data['operate'])) return $this->ajaxReturn(['code'=>-1,'message'=>'operate must be set!']);

            if(empty($main_data['mainData'])) return $this->ajaxReturn(['code'=>-1,'message'=>'mainData must be set!']);

            if($main_data['operate'] == 1){

                $if_exists = M('tb_ent_info')->where(['fentname' => $main_data['entData']['fentname']])->find();

                if(empty($if_exists)){
                    $add_ent_res = M('tb_ent_info')->add($main_data['entData']);
                    if(empty($add_ent_res)){
                        M()->rollback();
                    }else{
                        $main_data['mainData']['fent_id'] = $add_ent_res;
                    }
                }

            }elseif($main_data['operate'] == 2){

                $up_ent_res = M('tb_ent_info')->save($main_data['entData']);

                if($up_ent_res === false){

                    M()->rollback();

                }else{
                    $main_data['mainData']['fent_id'] = $main_data['entData']['fid'];
                }

            }

            if(!empty($main_data['mainData']['fent_id']) || $main_data['operate'] == 3){

                $up_main_res = M($tables[$main_data['mainType']])
                    ->where([$names[$main_data['mainType']]=>$main_data['mainData'][$names[$main_data['mainType']]]])
                    ->save($main_data['mainData']);

                if($up_main_res === false){
                    M()->rollback();
                }else{
                    M()->commit();
                }
            }

        }

        return  $this->ajaxReturn([
                    'code'=>0,
                    'msg'=> 'Sucsess!'
                ]);

    }

    //批量新增企业
    public function batch_add_ent(){

        $EntData = json_decode($_POST['EntData'],true);

        if(empty($EntData)) return $this->ajaxReturn(['code'=>-1,'message'=>'未接收到数据']);

        ini_set('max_execution_time', 0);

        $num = 0;
        foreach ($EntData as $ent){
            $res = M('tb_ent_info')->add($ent);
            if ($res){
                $num++;
            }
        }

        $this->ajaxReturn(array('code'=>0,'insert_count'=>$num));

    }

    //批量更新企业表
    public function batch_up_ent(){

        $EntData = json_decode($_POST['EntData'],true);

        if(empty($EntData)) return $this->ajaxReturn(['code'=>-1,'message'=>'未接收到数据']);

        ini_set('max_execution_time', 0);

        $num = 0;

        foreach ($EntData as $ent){

            if(isset($ent['fcreditid'])){
                $res = M('tb_ent_info')->where(['fcreditid' => $ent['fcreditid']])->save($ent);
                if ($res !== false){
                    $num++;
                }
            }else{
                continue;
            }

        }

        $this->ajaxReturn(array('code'=>0,'update_count'=>$num));
    }

}