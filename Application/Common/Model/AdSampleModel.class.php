<?php
namespace Common\Model;
use Think\Exception;

class AdSampleModel{
	
	/**
	 * 获取广告样本ID，没有则新建
	 * @Param Array $oAdSample 广告样本对象
	 */
    public function getAdSampleId($oAdSample){
		$fmediaid = $oAdSample['fmediaid'];
		$fissuedate = $oAdSample['fissuedate'];
		$fadid = $oAdSample['fadid'];
		$fspokesman = $oAdSample['fspokesman'];
		$fadlen = $oAdSample['fadlen'];
		$fillegaltypecode = $oAdSample['fillegaltypecode'];
		$fillegalcontent = $oAdSample['fillegalcontent'];
		$fexpressioncodes = $oAdSample['fexpressioncodes'];
		$fexpressions = $oAdSample['fexpressions'];
		$fconfirmations = $oAdSample['fconfirmations'];
		$fpunishments = $oAdSample['fpunishments'];
		$fpunishmenttypes = $oAdSample['fpunishmenttypes'];
		$favifilename = $oAdSample['favifilename'];
		$creator = $oAdSample['creator'];

		$mediaInfo = M('tmedia')->where(['fid'=>$fmediaid])->find();
		$mediaClass = substr($mediaInfo['fmediaclassid'],0,2);
		$adInfo = M('tad')->where(['fadid'=>$fadid])->find();
		$uuid = md5($mediaClass.$fmediaid.$fissuedate.$fadid.$creator);
        $pre = 'tv';
        switch($mediaClass){
            case '01':
                $pre = 'tv';
                break;
            case '02':
                $pre = 'bc';
                break;
            case '03':
                $pre = 'paper';
                break;
            case '13':
                $pre = 'net';
				break;
			default:
				$pre = 'tv';
				break;
		}
		$samTable = 't'.$pre.'sample';
		$whereSam = [
			'fmediaid' => $fmediaid,
			'fissuedate' => $fissuedate,
			'fadid' => $fadid
		];
		$adSamInfo = M($samTable)->where($whereSam)->find();
		if($adSamInfo){
			$samId = $adSamInfo['fid'];
		}else{//无数据则新建样本
			if(M('tadclass')->cache(true,3600)->where(['fcode'=>$adInfo['fadclasscode']])->count() == 0){
				$adClassCode = '2301';
			}
			$samId = M($samTable)
				->add([
					'fmediaid' => $fmediaid,//媒介ID
					'fissuedate' => $fissuedate,//发布时间
					'fadid' => $fadid,//广告ID
					// 'fversion' => $fversion,//广告版本
					'fspokesman' => $fspokesman,//代言人
					'fadlen' => $fadlen,//样本时长
					'fillegaltypecode' => $fillegaltypecode,//违法类型代码
					// 'fapprovalid' => $fapprovalid,//审批号
					// 'fapprovalunit' => $fapprovalunit,//审批单位
					'fillegalcontent' => $fillegalcontent,//违法内容
					'fexpressioncodes' => $fexpressioncodes,//违法表现代码
					'fexpressions' => $fexpressions,//违法表现
					'fconfirmations' => $fconfirmations,//认定依据
					'fpunishments' => $fpunishments,//处罚依据
					'fpunishmenttypes' => $fpunishmenttypes,//处罚种类及幅度
					'favifilename' => $favifilename,//视频或音频文件名
					'fstate' => 1,//状态（-2源异常，-1删除，0无效，1-有效，2抽查）
					'fsource' => 1,//来源（0-监测，1抽查）
					'uuid' => $uuid,
					'fcreator' => $creator,//创建人
					'fcreatetime' => date('Y-m-d H:i:s'),//创建时间
					'fmodifier' => $creator,//修改人
					'fmodifytime' => date('Y-m-d H:i:s')//修改时间
				]);//添加广告样本
		}
		return $samId;
    }
}