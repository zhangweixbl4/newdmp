<?php
namespace Open\Controller;
use Think\Controller;

class DeviceController extends BaseController {


	/*获取设备列表*/
	public function get_device(){

		$post_data = file_get_contents('php://input');
		$post_data = json_decode($post_data,true);
		$fid = $post_data['fid'];
		$fname = $post_data['fname'];
		
		$where = array();
		$where['tdevice.fdevicetypeid'] = 3;
		if(intval($fid) > 0){
			$where['tdevice.fid'] = $fid;
		}
		
		if($fname){
			$where['tdevice.fcode|tdevice.fname'] = array('like','%'.$fname.'%');
		}
		
		$deviceList = M('tdevice')
								->field('
											tdevice.fid,
											tdevice.fcode,
											tdevice.fname,
											tdevice.fonline,
											tcollect.fid as collect_id,
											tcollect.fcode as collect_code,
											tcollect.faddress,
											tcollect.fname as collect_fname,
											tcollect.fregionid as collect_regionid,
											tcollect.fmanager,
											tcollect.ftel,
											tcollect.ftype as collect_type
											
											
										')
								->join('tcollect on tcollect.fid = tdevice.fcollectid')
								
								
								->where($where)->select();
		//var_dump(M('tdevice')->getLastSql());
		$this->ajaxReturn(array('code'=>0,'msg'=>0,'deviceList'=>$deviceList));
		
	}

}