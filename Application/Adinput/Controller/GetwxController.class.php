<?php
namespace Adinput\Controller;
use Think\Controller;
class GetwxController extends Controller {
	
    public function test(){
		$openid = get_openid();
		var_dump($openid);
	}
	
	/*获取openid第一步*/
	public function wx_openid(){
		$back_url = urlencode(I('get.back_url'));//授权成功后跳转回的最终网址
		$in_url = urlencode('http://'.$_SERVER['HTTP_HOST'].U('Adinput/Getwx/wx_openid_code'));//微信授权成功后的跳转网址
		$appid = C('WX_APPID');//微信appid
		$secret = C('WX_APPSECRET');//微信秘钥
		$wx_url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$appid}&redirect_uri={$in_url}&response_type=code&scope=snsapi_base&state={$back_url}#wechat_redirect";
		//var_dump($_SERVER);
		//exit($in_url);
		header("location:{$wx_url}");//跳转的微信请求授权
		exit;
	}
	
	
	/*获取openid第二部*/
	public function wx_openid_code(){
		
		$code = I('get.code');//微信返回回来的code
		$back_url = cookie('back_url');//第一步的跳转回网址
		cookie('back_url',null);
		if ($back_url == '') exit;//如果没有网址就退出

		$appid = C('WX_APPID');//微信appid
		$secret = C('WX_APPSECRET');//微信秘钥
		$openid_url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={$appid}&secret={$secret}&code={$code}&grant_type=authorization_code";//使用code请求openid
		//exit($openid_url);
		$ret = http($openid_url);//获取到返回结果，josn
		$ret = json_decode($ret,true);//把josn转为数组
		session('wx_openid',$ret['openid']);//把openid存入session
		$this->in_wx_info($ret['openid']);
		if(session('wx_openid')=='') session('wx_openid',0);
		$back_url = str_replace("&amp;amp;","&",$back_url);
		header("location:".$back_url);//跳转回第一步提交的回跳网址
		exit;
	}
	
	/*写入数据库，微信资料*/
	private function in_wx_info($openid){
		//exit($openid);
		$info = M('ad_input_user')->where(array('openid'=>$openid))->find();
		$wx_id = $info['wx_id'];
		if (!$info){
			$url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token='.wx_token().'&openid='.$openid.'&lang=zh_CN';
			$ret = http($url);
			$ret = json_decode($ret,true);
			$ret['nickname'] = wx_nickname_filter($ret['nickname']);
			$ret['alias'] = $ret['nickname'];
			$wx_id = M('ad_input_user')->add($ret);
		}elseif($info['subscribe'] == 0){
			$url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token='.wx_token().'&openid='.$openid.'&lang=zh_CN';
			$ret = http($url);
			$ret = json_decode($ret,true);
			$ret['nickname'] = wx_nickname_filter($ret['nickname']);
			M('ad_input_user')->where(array('openid'=>$openid))->save($ret);
		}
		
		session('wx_id',$wx_id);
		
		return;
	}

	
	
	
	

}