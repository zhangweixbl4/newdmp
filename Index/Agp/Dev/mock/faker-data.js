module.exports = function () {
  var faker = require("faker");
  faker.locale = "zh_CN";
  var _ = require("lodash");
  return {
      peopleLists: _.times(37, function (n) {
          return {
              id: n,
              name: faker.name.findName(),
              avatar: faker.internet.avatar(),
              address: faker.address.county(),
              date: faker.date.past()
          }
      }),
      api: _.times(55, function (n) {
          return faker.helpers.userCard()
      })
  }
}