<?php
namespace TaskInput\Model;
use Think\Model;
class IllegalAdConfirmScoreModel extends TaskInputScoreModel
{

    /**
     * @param $id   int     illegal_ad_issue_review主键
     * @return bool|mixed
     */
    public function getSampleId($id)
    {
        // 确认媒体类型
        $reviewItem = M('illegal_ad_issue_review')
            ->find($id);
        if ($reviewItem['paper_jpg']){
            $mediaClass = '3';
            $sampleId = M('tpapersample')->where(['fjpgfilename' => ['EQ', $reviewItem['paper_jpg']]])->getField('fpapersampleid');
            if (!$sampleId){
                return true;
            }
        }else{
            // 判断媒体类型
            $mediaClass = M('tmedia')->cache(true)->where(['fid' => ['EQ', $reviewItem['media_id']]])->getField('fmediaclassid');
            $mediaClass = $mediaClass[1];
            $issueTable = $mediaClass == 1 ? 'ttvissue' : 'tbcissue';
            $sampleIdField = $mediaClass == 1 ? 'ftvsampleid' : 'fbcsampleid';
            $sampleId = M($issueTable)
                ->where([
                    'fmediaid' => ['EQ', $reviewItem['media_id']],
                    'fstarttime' => ['EQ', date('Y-m-d H:i:s', $reviewItem['start_time'])]
                ])
                ->getField($sampleIdField);
        }
        return $this->addScoreBySample($sampleId, $mediaClass, $reviewItem['wx_id']);

    }

    /**
     * 确认传统媒体违法广告
     * @param $sampleId
     * @param $mediaClass
     * @return mixed
     */
    public function addScoreBySample($sampleId, $mediaClass, $wx_id)
    {
        $sampleTableMap = [
            '1' => 'ttvsample',
            '2' => 'tbcsample',
            '3' => 'tpapersample',
        ];
        $sampleInfo = M($sampleTableMap[$mediaClass])
            ->find($sampleId);
        $sampleInfo['media_class'] = $mediaClass;
        $sampleInfo['wx_id'] = $wx_id;
        $sampleInfo['fadclasscode'] = M('tad')->where(['fadid' => ['EQ', $sampleInfo['fadid']]])->getField('fadclasscode');
        $res = $this->addScoreByInspectTask($sampleInfo);
        return $res;
    }

    protected function addScoreByInspectTask($sampleInfo)
    {
        if (in_array($sampleInfo['fadclasscode'], $this->ignoreAdClass)){
            $score = 0;
            $reason = '不计分分类广告审核';
        }elseif ($this->isLongAd($sampleInfo)){
            $score = self::ILLEGAL_LONG_AD_INSPECT_SCORE;
            $reason = '长广告违法';
        }elseif ($this->isSpecialClass($sampleInfo)){
            $score = self::ILLEGAL_SPECIFIC_CLASS_INSPECT_SCORE;
            $reason = '特殊分类违法';
        }else{
            $score = self::ILLEGAL_INSPECT_SCORE;
            $reason = '一般广告违法';
        }
        return $this->addScoreRow($sampleInfo, $score, $reason, 2);
    }

    protected function addScoreRow($taskInfo, $score, $reason, $type)
    {
        $times = 1;
        if ($taskInfo['media_class'] == 1){
            $times = self::TV_TASK_TIMES;
        }elseif ($taskInfo['media_class'] == 2){
            $times = self::BC_TASK_TIMES;
        }elseif ($taskInfo['media_class'] == 3){
            $times = self::PAPER_TASK_TIMES;
        }elseif ($taskInfo['media_class'] == 13){
            $times = self::NET_TASK_TIMES;
        }
        $score = $score * $times;
        $addData = [
            'wx_id' => $taskInfo['wx_id'],
            'update_time' => time(),
            'media_class' => $taskInfo['media_class'],
            'score' => $score,
            'reason' => $reason,
        ];
        $res = $this->add($addData);
        return $res;

    }

    /**
     * 判断长广告
     * @param $sampleInfo
     * @return bool
     */
    protected function isLongAd($sampleInfo)
    {
        // 判断是否是长广告
        if ($sampleInfo['media_class'] != 3 && $sampleInfo['media_class'] != 13){
            return $sampleInfo['fadlen'] >= self::LONG_AD_LENGTH;
        }
        return false;
    }
}