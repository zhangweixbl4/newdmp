const outdoorAdCheck = {
  template: `
  <div class="outdoor-ad-check">
    <div class="left-block">
      <el-card :body-style="{ padding: '0px 6px' }" style="height:100%;overflow: auto;" v-if="ischeck">
        <el-alert
          :title="'任务创建时间：' + taskCreatedTime"
          type="warning"
          :description="'任务将在 ' + remainingTime + ' 秒后被回收'"
          :closable="false"
          show-icon>
        </el-alert>
        <div v-if="checkType === '1'">
          <div>
            <span class="card-title">预审结果</span>
          </div>
          <el-form :model="auditForm" label-width="90px">
            <el-form-item label="预审结果：">
              <el-radio-group v-model="auditForm.faudit">
                <el-radio :label="item.value" v-for="item in auditType" :key="item.value">{{item.label}}</el-radio>
              </el-radio-group>
            </el-form-item>
            <el-form-item label="原因：">
              <el-input :disabled="auditForm.faudit === '1'" type="textarea" v-model="auditForm.freason" :rows="5" ></el-input>
            </el-form-item>
            <el-form-item>
              <el-button type="primary" @click="auditSubmit">提 交</el-button>
            </el-form-item>
          </el-form>
        </div>
        <div v-if="checkType === '2'">
          <div>
            <span class="card-title">违法判定</span>
          </div>
          <el-form :model="illForm" ref="illForm" :rules="rules" label-width="100px">
            <el-form-item label="违法程度：" prop="fillegaltype">
              <el-radio-group v-model="illForm.fillegaltype">
                <el-radio :label="item.value" v-for="item in illegalTypeArr" :key="item.value">{{item.text}}</el-radio>
              </el-radio-group>
            </el-form-item>
            <el-form-item label="违法表现：" v-if="illForm.fillegaltype !== '0'">
              <el-button @click="showIllegal = true">选择违法表现</el-button>
              <div class="alert-box" v-if="selectedList.length">
                <el-alert v-for="item in selectedList" :key="item.id" type="error" @close="alertClose(item)">
                  <div class="alert-title">{{item.label}}</div>
                </el-alert>
              </div>
            </el-form-item>
            <el-form-item label="违法内容：" prop="fillegalcontent" v-if="illForm.fillegaltype !== '0'">
              <el-input v-model="illForm.fillegalcontent" type="textarea" :rows="5"></el-input>
            </el-form-item>
            <el-form-item>
              <el-button type="primary" @click="illSubmit">提 交</el-button>
            </el-form-item>
          </el-form>
        </div>
      </el-card>
      <el-card :body-style="{ padding: '0px 6px' }" style="height:100%;overflow: auto;" v-else>
        <div v-if="task.task_state ==='0'">未处理</div>
        <div v-if="task.task_state ==='11'">预审中</div>
        <div v-if="task.task_state ==='12'">
          <el-form label-width="100px">
            <el-form-item label="预审结果：">
              <span v-if="Number(task.pretrial.check) === 2 || Number(task.pretrial.check) > 10">预审通过</span>
              <span v-if="Number(task.pretrial.check) === 1 || Number(task.pretrial.check) === 4">未处理</span>
              <span v-if="Number(task.pretrial.check) === 3">预审不通过</span>
            </el-form-item>
            <el-form-item label="预审结果：" v-if="Number(task.pretrial.check) === 3">
              {{task.pretrial.pretrial_result}}
            </el-form-item>
            <el-form-item label="预审次数：">
              {{task.pretrial.pretrial_count}}
            </el-form-item>
            <el-form-item label="完成时间：">
              {{task.pretrial.pretrial_time}}
            </el-form-item>
          </el-form>
        </div>
        <div v-if="task.task_state ==='21'">审核中</div>
        <div v-if="task.task_state ==='22'">
          <el-form label-width="100px">
            <el-form-item label="违法类型：">
              <span v-if="task.illdata.fillegaltypecode === '-1'">未判定</span>
              <span v-else-if="task.illdata.fillegaltypecode === '0'">不违法</span>
              <span v-else-if="task.illdata.fillegaltypecode === '30'">违法</span>
            </el-form-item>
            <el-form-item label="涉嫌违法内容：" v-if="task.illdata.fillegaltypecode !== '-1'">
            </el-form-item>
            <el-form-item label="违法表现：" v-if="task.illdata.fillegaltypecode !== '-1'">
            </el-form-item>
            <el-form-item label="违法表现代码：" v-if="task.illdata.fillegaltypecode !== '-1'">
            </el-form-item>
            <el-form-item label="判定依据：" v-if="task.illdata.fillegaltypecode !== '-1'">
            </el-form-item>
            <el-form-item label="完成时间：" v-if="task.illdata.fillegaltypecode !== '-1'">
            </el-form-item>
          </el-form>
        </div>
      </el-card>
    </div>
    <div class="right-block">
      <el-card :body-style="{ padding: '10px' }" style="height:100%;overflow: auto;">
        <div>
          <span class="card-title">企业信息</span>
        </div>
        <el-form label-width="120px">
          <el-row>
            <el-col :span="12" v-for="(item,index) in task.upunit_info" :key="index">
              <el-form-item :label="item.fieldname + '：'">{{item.fieldvalue}}</el-form-item>
            </el-col>
          </el-row>
        </el-form>
        <div>
          <span class="card-title">广告主信息</span>
        </div>
        <el-form label-width="120px">
          <el-row>
            <el-col :span="12" v-for="(item,index) in task.owner_info" :key="index">
              <el-form-item :label="item.fieldname + '：'">{{item.fieldvalue}}</el-form-item>
            </el-col>
          </el-row>
        </el-form>
        <div>
          <span class="card-title">广告信息</span>
        </div>
        <el-form label-width="120px">
          <el-row>
            <el-col :span="12" v-for="(item,index) in task.ad_info" :key="index">
              <el-form-item :label="item.fieldname + '：'">{{item.fieldvalue}}</el-form-item>
            </el-col>
          </el-row>
          <el-row>
            <el-col :span="12" v-for="(item,index) in task.ad_lat" :key="index">
              <el-form-item :label="item.fieldname + '：'">{{item.fieldvalue}}</el-form-item>
            </el-col>
          </el-row>
        </el-form>
        <div v-if="haveImgFile">
          <div>
            <span class="card-title">图片小样</span>
          </div>
          <div v-viewer="{zIndex: 9999}" v-for="img in task.imgfile" :key="img.material_url" class="line-box">
            <div class="box-title">第 {{img.check_sn}} 版</div>
            <div class="img-box" v-for="item in img.material_url.split(',')" :key="item">
              <template>
                <img :src="'http://bj-hw.oss-cn-qingdao.aliyuncs.com/' + item">
              </template>
            </div>
          </div>
        </div>
        <div v-if="haveVideoFile">
          <div>
            <span class="card-title">视频小样</span>
          </div>
          <div>
            <div class="line-box" v-for="v in task.videofile" :key="v.material_url">
              <div class="box-title">第 {{v.check_sn}} 版</div>
              <div>
                <div v-for="item in v.material_url.split(',')" :key="item" class="video-box">
                  <video width="100%" controls :src="item"></video>
                </div>
              </div>
            </div>
            <!-- <video width="100%" :src="'http://bj-hw.oss-cn-qingdao.aliyuncs.com/' + v.material_url" v-for="v in task.videofile" :key="v.material_url"></video> -->
          </div>
        </div>
        <div v-if="haveOtherFile">
          <div>
            <span class="card-title">其他样件</span>
          </div>
          <div v-viewer="{zIndex: 9999}">
            <div class="img-box" v-for="(img,index) in task.otherfile" :key="index">
              <template>
                <img :src="'http://bj-hw.oss-cn-qingdao.aliyuncs.com/' + img.material_url" :key="img.material_url">
              </template>
            </div>
          </div>
        </div>
        <div v-if="checkType !== '1'">
          <div>
            <span class="card-title">现场核查</span>
          </div>
          <el-form label-width="120px">
            <el-form-item :label="item.fieldname + '：'" v-for="(item,index) in task.check_data" :key="index">{{item.fieldvalue}}</el-form-item>
            <el-form-item label="公证照片：">
              <div v-viewer="{zIndex: 9999}" class="line-box">
                <div class="img-box" v-for="(img,index) in task.check_imgfile" :key="index">
                  <template>
                    <img :src="img.material_url" :key="img.material_url">
                  </template>
                </div>
              </div>
            </el-form-item>
          </el-form>
        </div>
      </el-card>
    </div>
    <el-dialog
      title="认定依据"
      :visible.sync="showIllegal"
      center
      :close-on-click-modal="false"
      :close-on-press-escape="false"
      width="1000px">
      <el-input
        placeholder="关键字搜索"
        v-model="filterText">
      </el-input>
      <div class="tree-block">
        <el-tree
          show-checkbox
          node-key="id"
          :data="illegalArr"
          :props="treeProps"
          default-expand-all
          @check="nodeCheck"
          :filter-node-method="filterNode"
          ref="tree">
          <div slot-scope="{ node, data }">
            <el-tooltip v-if="node.label.length > 60" effect="dark" :content="node.label" placement="top-start">
              <div class="tree-node-slot">{{ node.label }}</div>
            </el-tooltip>
            <div v-else class="tree-node-slot">{{ node.label }}</div>
          </div>
        </el-tree>
      </div>
      <span slot="footer">
        <el-button type="primary" @click="showIllegal = false">确 定</el-button>
        <el-button type="danger" @click="reset">清 空</el-button>
      </span>
    </el-dialog>
  </div>
  `,
  props: {
    task: {
      type: Object,
      required: true,
    },
    ischeck: {
      type: Boolean,
      default: true
    }
  },
  data() {
    return {
      illForm: {
        fillegaltype: '0',
        fillegalcontent: '',
      },
      auditForm: {
        faudit: '1',
        freason: ''
      },
      filterText: '',
      illegalTypeArr: illegalTypeArr,
      auditType: [
        {
          label: '通过',
          value: '1'
        },
        {
          label: '不通过',
          value: '0'
        },
      ],
      rules: {
        fillegaltype: [{ required: true, message: '必填', trigger: 'change' }],
        fillegalcontent: [{ required: true, message: '必填', trigger: 'change' }],
      },
      illegalArr: illegalArr,
      showIllegal: false,
      selectedList: [],
      treeProps: {
        label: 'label',
        children: 'children'
      },
      timer: null,
      remainingTime: 1200,
    }
  },
  computed: {
    taskCreatedTime() {
      return moment(this.task.modifytime * 1000).format('YYYY-MM-DD HH:mm:ss')
    },
    checkNodes(){
      return this.$refs.tree.getCheckedNodes()
    },
    haveImgFile() {
      return (this.task.imgfile && this.task.imgfile.length)
    },
    haveVideoFile() {
      return (this.task.videofile && this.task.videofile.length)
    },
    haveOtherFile() {
      return (this.task.otherfile && this.task.otherfile.length)
    },
    checkType() {
      // !'1'预审，'2'，违法判定，'3'其他
      if (this.task.task_state === '11' || this.task.task_state === '13') {
        return '1'
      } else if(this.task.task_state === '21' || this.task.task_state === '23') {
        return '2'
      } else {
        return '3'
      }
    }
  },
  watch: {
    filterText(val) {
      this.$refs.tree2.filter(val)
    }
  },
  methods: {
    dataInit() {
      if (this.ischeck) {
        this.resetForm()
        this.RemainingTime()
      } else {
        return false
      }
    },
    RemainingTime() {
      clearInterval(this.timer)
      let end = (Number(this.task.modifytime) + 1200) * 1000
      let now = +new Date()
      this.remainingTime = Math.floor((end - now) / 1000)
      if (this.remainingTime > 0) {
        this.timer = setInterval(() => {
          if (this.remainingTime > 0) {
            this.remainingTime -= 1
          } else {
            clearInterval(this.timer)
            this.$confirm('任务超时, 是否重新领取任务?', '任务超时', {
              confirmButtonText: '领取',
              cancelButtonText: '取消',
              type: 'warning',
            }).then(() => {
              this.$emit('next')
            }).catch(() => {
              this.$emit('back')
            })
          }
        },1000)
      } else {
        window.location.reload()
      }
    },
    resetForm() {
      this.illForm = {
        fillegaltype: '0',
        fillegalcontent: '',
      }
      this.auditForm = {
        faudit: '1',
        freason: ''
      }
    },
    auditSubmit() {
      let data = {
        taskid: this.task.taskid,
        ...this.auditForm
      }
      $.post('/TaskInput/OutAd/submitTask', data).then(res => {
        if (res.code === 0) {
          this.$confirm('提交成功, 是否继续做新任务?', '提交成功', {
            confirmButtonText: '继续',
            cancelButtonText: '取消',
            type: 'success',
          }).then(() => {
            clearInterval(this.timer)
            this.$emit('next')
          }).catch(() => {
            clearInterval(this.timer)
            this.$emit('back')
          })
        } else {
          this.$message.error('提交失败')
        }
      })
    },
    illSubmit() {
      let fexpressioncodes = this.selectedList.map(i => {
        return i.fcode
      })
      if (this.illForm.fillegaltype === '30' && !fexpressioncodes.length) {
        this.$message.error('请选择认定依据！')
        return false
      }
      this.$refs.illForm.validate((valid) => {
        if (valid) {
          let data = {
            taskid: this.task.taskid,
            fexpressioncodes,
            ...this.illForm
          }
          $.post('/TaskInput/OutAd/submitTask', data).then(res => {
            if (res.code === 0) {
              this.$confirm('提交成功, 是否继续做新任务?', '提交成功', {
                confirmButtonText: '继续',
                cancelButtonText: '取消',
                type: 'success',
              }).then(() => {
                this.$emit('next')
              }).catch(() => {
                this.$emit('back')
              })
            } else {
              this.$message.error('提交失败')
            }
          })
        } else {
          this.$message.error('表单验证失败！')
        return false
        }
      })
    },
    reset(){
      this.selectedList = []
      this.$refs.tree.setCheckedKeys([])
    },
    alertClose(obj){
      let keys = []
      let selectedList = this.selectedList.filter(item => {
        if(item.id !== obj.id){
          keys.push(item.id)
        }
        return item.id !== obj.id
      })
      this.selectedList = selectedList
      this.$refs.tree.setCheckedKeys(keys)
    },
    nodeCheck(node,data){
      this.selectedList = data.checkedNodes
    },
    filterNode(value, data) {
      if (!value) return true
      return data.label.indexOf(value) !== -1
    }
  }
}