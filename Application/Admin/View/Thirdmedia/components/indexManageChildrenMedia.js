const childrenMediaDialog = {
  name: 'childrenMediaDialog',
  props: {
    data: {
      type: Object,
      default: {}
    }
  },
  template: `
    <el-dialog
      :title="data.fmediacodename"
      :visible.sync="show"
      center
      width="800px"
      :close-on-click-modal="false"
      :close-on-press-escape="false"
      @close="() => $emit('close')">
      <el-form :model="form" ref="form" inline>
        <el-form-item label="用户媒体编号">
          <el-input v-model="form.fmediaid" clearable></el-input>
        </el-form-item>
        <el-form-item>
          <el-button type="primary" @click="onSubmit" :disabled="!form.fmediaid">新 增</el-button>
        </el-form-item>
      </el-form>
      <el-table :data="tableData" border stripe height="360">
        <el-table-column label="媒体名称">
          <template slot-scope="scope">
            <span class="copy-text" v-clipboard:copy="scope.row.medianame" v-clipboard:success="() => $message.success('复制成功')"><i class="el-icon-document"></i> {{scope.row.medianame}}</span>
          </template>
        </el-table-column>
        <el-table-column label="媒体 id">
          <template slot-scope="scope">
            <span class="copy-text" v-clipboard:copy="scope.row.mediaid" v-clipboard:success="() => $message.success('复制成功')"><i class="el-icon-document"></i> {{scope.row.mediaid}}</span>
          </template>
        </el-table-column>
        <el-table-column label="操作" width="90">
          <template slot-scope="scope">
            <el-popover
              placement="top"
              width="160"
              v-model="scope.row.visible">
              <p>确认删除 <span class="text-danger">{{scope.row.medianame}}</span>？</p>
              <div style="text-align: right; margin: 0">
                <el-button size="mini" type="text" @click="scope.row.visible = false">取消</el-button>
                <el-button type="primary" size="mini" @click="deleteMedia(scope.row)">确定</el-button>
              </div>
              <el-button type="danger" slot="reference" @click="e => {scope.row.visible = true;e.stopPropagation()}">删 除</el-button>
          </el-popover>
          </template>
        </el-table-column>
      </el-table>
    </el-dialog>
  `,
  data() {
    return {
      show: true,
      tableData: [],
      form: {
        fmediaid: '',
        fid: '',
      }
    }
  },
  watch:{
    data(){
      this.initData()
    }
  },
  created() {
    this.initData()
  },
  methods: {
    initData(){
      this.tableData = this.data.children_media
      this.form.fid = this.data.fid
    },
    deleteMedia({ cid }){
      $.post('/Admin/Thirdmedia/delete_media', { fid: cid }).then(res => {
        if (res.code === 0) {
          this.$message.success(res.msg)
          this.$emit('refresh')
        } else {
          this.$message.error(res.msg)
        }
      })
    },
    onSubmit(){
      $.post('/Admin/Thirdmedia/addedit_childmedia', this.form).then(res => {
        if(res.code === 0){
          this.$emit('refresh')
          this.form.fmediaid = ''
        }else{
          this.$message.error(res.msg)
        }
      })
    }
  },
}