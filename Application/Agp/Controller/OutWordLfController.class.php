<?php
/**
 * Created by PhpStorm.
 * User: yaojunnan
 * Date: 2019-10-07
 * Time: 21:12
 */
namespace Agp\Controller;
use Agp\Model\StatisticalReportModel;
use Think\Controller;
use Agp\Model\StatisticalModel;

class OutWordLfController extends BaseController{

    public function get_owner_media_ids($level = -1,$media_type = '',$out_media_type = ''){
        $owner_media = get_owner_media_ids(-1,'',$out_media_type);//获取权限内媒体
        /*        $owner_media_ids = M('tmedia')->where([
                    'fid' => ['IN',$owner_media],
                    'media_region_id' => ['NEQ',session('regulatorpersonInfo.regionid')]
                ])->getField('fid',true);*/
        //return $owner_media_ids;
        return $owner_media;
    }

    /*生成图表数据*/
    function create_chart_data($array,$index1 = 1,$index2 = 2,$index3 = 3,$index4 = 4){
        $array1[] = ["类型","发布量","违法发布量","违法率"];
        $array2[] = ["类型","发布量"];
        $array3[] = ["类型","违法发布量"];
        foreach ($array as $array_key=>$array_val){
            $array1[] = [
                $array_val[$index1],
                $array_val[$index2],
                $array_val[$index3],
                $array_val[$index4]
            ];
            $array2[] = [
                $array_val[$index1],
                $array_val[$index2]
            ];
            if($array_val[3] != 0){
                $array3[] = [
                    $array_val[$index1],
                    $array_val[$index3]
                ];
            }
        }
        return [$array1,$array2,$array3];
    }

    //廊坊月报
    public function create_month(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport = New StatisticalModel();//实例化数据统计模型
        $StatisticalReport1 = New StatisticalReportModel();//实例化数据统计模型
        $month = I('daytime','2019-07');//接收时间  TODO::时间改
        $days = date('t', strtotime($month));
        $report_start_date = strtotime($month);//指定月份月初时间戳
        $report_end_date = strtotime($month.'-'.date('t', strtotime($month)).' 23:59:59');

        if($report_end_date > time()){
            $report_end_date = time();
        }

/*        $report_start_date = strtotime("2011-01-01");
        $report_end_date = strtotime("2019-10-09");*/

        $date_ym = date('Y年m月',$report_start_date);//年月字符
        $user = session('regulatorpersonInfo');//获取用户信息
        $zxs = ['110000','120000','310000','500000'];//直辖市
        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级

        //文字描述匹配
        switch ($re_level){
            case 2:
            case 3:
            case 4:
                $regionid_str = substr($user['regionid'],0,4);
                $dq_name = '全市';
                $xj_name = '区县';
                $self_name = '市属';
                break;
            case 1:
                $regionid_str = substr($user['regionid'],0,2);
                if(!in_array($user['regionid'],$zxs)){
                    $dq_name = '全省';
                    $xj_name = '市级';
                    $self_name = '省属';
                }else{
                    $dq_name = '全市';
                    $xj_name = '区级';
                    $self_name = '市属';
                }
                break;
            case 5:
                $regionid_str = $user['regionid'];
                $dq_name = '全县';
                break;
        }

        $date_table = date('Y',strtotime($month)).'_'.$user['regionid'];//组合表

        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);//结束时间
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数

        //线索统计列表
        $clue_statistics = $StatisticalReport->sta_cule_data(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));
        $clue_statistics_data = [];
        //循环赋值
        foreach ($clue_statistics as $clue_key => $clue_val){

            $clue_statistics_data[] = [
                $clue_key+1,
                $clue_val['region_name'],
                $clue_val['cule_count'],
                $clue_val['ck_count'],
                $clue_val['ckl'],
                $clue_val['cl_count'],
                $clue_val['cll']
            ];
        }
        $clue_statistics_data = to_string($clue_statistics_data);



        $owner_media_ids = $this->get_owner_media_ids(5,'','13');//获取权限内媒体

        //违法广告列表
        $illegal_ad_list = $StatisticalReport->ill_ad_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$owner_media_ids);
        $illegal_ad_list_data = [];
        foreach ($illegal_ad_list as $illegal_ad_key => $illegal_ad_val){

            $illegal_ad_list_data[] = [
                $illegal_ad_key+1,
                $illegal_ad_val['region_name'],
                $illegal_ad_val['fmedianame'],
                $illegal_ad_val['fad_name'],
                $illegal_ad_val['ffullname'],
                $illegal_ad_val['fexpressions'],
                $illegal_ad_val['fad_illegal_times']
            ];

        }
        $illegal_ad_list_data = to_string($illegal_ad_list_data);

        $ct_media_tv_count = $StatisticalReport->ct_media_count('tv',$owner_media_ids);//电视媒体数量

        $ct_media_bc_count = $StatisticalReport->ct_media_count('bc',$owner_media_ids);//广播媒体数量

        $ct_media_paper_count = $StatisticalReport->ct_media_count('paper',$owner_media_ids);//报纸媒体数量

        $ct_media_count = $ct_media_tv_count + $ct_media_bc_count + $ct_media_paper_count;

        $pc_media_count = $StatisticalReport->net_media_counts($regionid_str,'1301');//pc媒体数量
        $gzh_media_count = $StatisticalReport->net_media_counts($regionid_str,'1303');//公众号数量

        $pc_illegal_situation = $StatisticalReport->net_illegal_situation_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));//pc违法情形列表
        $pc_medias = $StatisticalReport->net_media_type_ids('1301');
        $wx_medias = $StatisticalReport->net_media_type_ids('1303');

        $ct_jczqk = $StatisticalReport->report_ad_monitor('fmedia_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));

        $ct_fad_times = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times = 0;//定义传统媒体发布违法广告条次
        $every_media_data = [];//定义媒体发布监测情况数组

        //循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk as $ct_jczqk_key => $ct_jczqk_val){
            $every_media_data_array = [];
            if(!$ct_jczqk_val['titlename']){
                if($ct_jczqk_val['fmedia_class_code'] != '13'){
                    $ct_fad_times += $ct_jczqk_val['fad_times'];
                    $ct_fad_illegal_times += $ct_jczqk_val['fad_illegal_times'];
                    $ct_fad_play_len += $ct_jczqk_val['fad_play_len'];
                    $ct_fad_illegal_play_len += $ct_jczqk_val['fad_illegal_play_len'];
                }
                $fmedia_class_code = $ct_jczqk_val['fmedia_class_code'];
                $tmediaclass_name = $ct_jczqk_val['tmediaclass_name'];
            }else{
                $fmedia_class_code = 'all';
                $tmediaclass_name = '合计';
            }
            $every_media_data_array = [
                $tmediaclass_name,
                $ct_jczqk_val['fad_times'],
                $ct_jczqk_val['fad_illegal_times'],
                $ct_jczqk_val['times_illegal_rate'].'%',
                $ct_jczqk_val['fad_play_len'],
                $ct_jczqk_val['fad_illegal_play_len'],
                $ct_jczqk_val['lens_illegal_rate']?$ct_jczqk_val['lens_illegal_rate'].'%':'0.00'.'%'
            ];
            switch ($fmedia_class_code){
                case '01':
                    $every_media_data[0] = $every_media_data_array;
                    break;
                case '02':
                    $every_media_data[1] = $every_media_data_array;
                    break;
                case '03':
                    $every_media_data[2] = $every_media_data_array;
                    break;
                case '13':
                    //$every_media_data[3] = $every_media_data_array;
                    break;
                case 'all':
                    $every_media_data[3] = $every_media_data_array;
                    break;
            }

        }
        $every_media_data = [
            $every_media_data[0],
            $every_media_data[1],
            $every_media_data[2],
            //$every_media_data[3],
            $every_media_data[3],
        ];
        $every_media_data = to_string($every_media_data);
        foreach ($every_media_data as $every_media_k=>$every_media_v){
            if(empty($every_media_v)){
                unset($every_media_data[$every_media_k]);
            }
        }


        /*
        * @各地域监测总情况
        *
        **/
        $every_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));


        $every_region_data = [];//定义各地区发布监测情况数组
        $ext_reg_ids = [];
        $every_region_num = 0;

        $reg_ids = M("tregion")->where([
            'fpid' => 131000,
            'flevel' => 5
        ])->getField("fid",true);
        //循环赋值
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){


            if(!$every_region_jczqk_val['titlename']){
                if($every_region_jczqk_val['fregionid'] == $user['regionid']){
                    continue;
                    $every_region_jczqk_tregion_name = $self_name;
                }else{
                    $ext_reg_ids[] = $every_region_jczqk_val['fregionid'];
                    $every_region_jczqk_tregion_name = $every_region_jczqk_val['tregion_name'];
                }
                if($every_region_jczqk_val['fad_illegal_times'] != 0){
                    $every_region_num++;
                    $every_region_name[] = $every_region_jczqk_tregion_name;
                }
                $every_region_data[] = [
                    strval($every_region_jczqk_key+1),
                    $every_region_jczqk_tregion_name,
                    $every_region_jczqk_val['fad_times'],
                    $every_region_jczqk_val['fad_illegal_times'],
                    $every_region_jczqk_val['times_illegal_rate'].'%',
                    $every_region_jczqk_val['fad_play_len'],
                    $every_region_jczqk_val['fad_illegal_play_len'],
                    $every_region_jczqk_val['lens_illegal_rate']?$every_region_jczqk_val['lens_illegal_rate'].'%':'0%'
                ];
            }

        }

        $no_data_ids = array_diff($reg_ids,$ext_reg_ids);
        $has_count = count($every_region_data);
        if(!empty($no_data_ids)){
            $reg_names = M("tregion")->where([

                'fid' => ['IN',$no_data_ids],
                'flevel' => 5

            ])->getField("fname1",true);
            foreach ($reg_names as $rename){
                $every_region_data[] = [
                    strval($has_count++),
                    $rename,
                    "0",
                    "0",
                    "0%",
                    "0",
                    "0",
                    "0%",
                    "0",
                    "0%",
                    "0",
                    "0%"
                ];
            }

        }

        $every_region_data = to_string($every_region_data);

        /*
        * @各广告类型监测总情况
        *
        **/
        $every_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));

        $every_adclass_data = [];//定义各大类发布监测情况数组
        $adclass_illegal_times = 0;//各违法地域数量
        //循环赋值
        foreach ($every_adclass_jczqk as $every_adclass_jczqk_key => $every_adclass_jczqk_val){
            if(!$every_adclass_jczqk_val['titlename']){
                if($every_adclass_jczqk_val['fad_illegal_times'] > 0){
                    $adclass_illegal_times += $every_adclass_jczqk_val['fad_illegal_times'];
                }
                $every_adclass_data[] = [
                    strval($every_adclass_jczqk_key+1),
                    $every_adclass_jczqk_val['tadclass_name'],
                    $every_adclass_jczqk_val['fad_times'],
                    $every_adclass_jczqk_val['fad_illegal_times'],
                    $every_adclass_jczqk_val['times_illegal_rate'].'%',
                    $every_adclass_jczqk_val['fad_play_len'],
                    $every_adclass_jczqk_val['fad_illegal_play_len'],
                    $every_adclass_jczqk_val['lens_illegal_rate']?$every_adclass_jczqk_val['lens_illegal_rate'].'%':'0.00'.'%'
                ];
            }
        }
        $every_adclass_data = to_string($every_adclass_data);

        $every_adclass_name = [];
        $every_adclass_rate_des = [];
        foreach ($every_adclass_data as $every_adclass_data_key=>$every_adclass_data_val){
            if($every_adclass_data_val[3] > 0){
                $every_adclass_name[] = $every_adclass_data_val[1];
                if($every_adclass_data_key == 0){
                    $every_adclass_rate_des[] = $every_adclass_data_val[1].'广告占涉嫌违法广告总量的'.round(($every_adclass_data_val[3]/$adclass_illegal_times)*100,2).'％';
                }else{
                    $every_adclass_rate_des[] .= $every_adclass_data_val[1].'广告占'.round(($every_adclass_data_val[3]/$adclass_illegal_times)*100,2).'％';
                }
            }
        }

        /*
        * @电视各地域监测总情况
        *
        **/
        $every_tv_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','01');

        $every_tv_region_data = [];//定义各地区发布监测情况数组
        $every_tv_region_num = 0;//电视各违法地域数量
        //循环赋值
        foreach ($every_tv_region_jczqk as $every_tv_region_jczqk_key => $every_tv_region_jczqk_val){
            if(!$every_tv_region_jczqk_val['titlename']){
                if($every_tv_region_jczqk_val['fregionid'] == $user['regionid']){
                    $every_tv_region_jczqk_tregion_name = $self_name;
                }else{
                    $every_tv_region_jczqk_tregion_name = $every_tv_region_jczqk_val['tregion_name'];
                }
                if($every_tv_region_jczqk_val['fad_illegal_times'] != 0){
                    $every_tv_region_num++;
                    $every_tv_region_name[] = $every_tv_region_jczqk_tregion_name;
                }
                $every_tv_region_data[] = [
                    strval($every_tv_region_jczqk_key+1),
                    $every_tv_region_jczqk_tregion_name,
                    $every_tv_region_jczqk_val['fad_times'],
                    $every_tv_region_jczqk_val['fad_illegal_times'],
                    $every_tv_region_jczqk_val['times_illegal_rate'].'%',
                    $every_tv_region_jczqk_val['fad_play_len'],
                    $every_tv_region_jczqk_val['fad_illegal_play_len'],
                    $every_tv_region_jczqk_val['lens_illegal_rate'].'%'
                ];
            }
        }
        $every_tv_region_data = to_string($every_tv_region_data);


        /*
        * @电视各广告类型监测总情况
        *
        **/
        $every_tv_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','01');

        $every_tv_adclass_data = [];//定义各地区发布监测情况数组
        //循环赋值
        foreach ($every_tv_adclass_jczqk as $every_tv_adclass_jczqk_key => $every_tv_adclass_jczqk_val){

            if(!$every_tv_adclass_jczqk_val['titlename']){
                $every_tv_adclass_data[] = [
                    strval($every_tv_adclass_jczqk_key+1),
                    $every_tv_adclass_jczqk_val['tadclass_name'],
                    $every_tv_adclass_jczqk_val['fad_times'],
                    $every_tv_adclass_jczqk_val['fad_illegal_times'],
                    $every_tv_adclass_jczqk_val['times_illegal_rate'].'%',
                    $every_tv_adclass_jczqk_val['fad_play_len'],
                    $every_tv_adclass_jczqk_val['fad_illegal_play_len'],
                    $every_tv_adclass_jczqk_val['lens_illegal_rate'].'%'
                ];
            }

        }
        $every_tv_adclass_data = to_string($every_tv_adclass_data);

        /*
        * @广播各地域监测总情况
        *
        **/
        $every_tb_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','02');
        //$every_tb_region_jczqk1 = $StatisticalReport1->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'02','','fad_illegal_times_rate','fregionid',false,1,get_check_dates(),true);//广播各地域监测总情况
        $every_tb_region_data = [];//定义各地区发布监测情况数组
        $every_tb_region_num = 0;//广播各违法地域数量
        //循环赋值
        foreach ($every_tb_region_jczqk as $every_tb_region_jczqk_key => $every_tb_region_jczqk_val){
            if(!$every_tb_region_jczqk_val['titlename']){
                if($every_tb_region_jczqk_val['fregionid'] == $user['regionid']){
                    $every_tb_region_jczqk_tregion_name = $self_name;
                }else{
                    $every_tb_region_jczqk_tregion_name = $every_tb_region_jczqk_val['tregion_name'];
                }

                if($every_tb_region_jczqk_val['fad_illegal_times'] != 0){
                    $every_tb_region_num++;
                    $every_tb_region_name[] = $every_tb_region_jczqk_tregion_name;
                }

                $every_tb_region_data[] = [
                    strval($every_tb_region_jczqk_key+1),
                    $every_tb_region_jczqk_tregion_name,
                    $every_tb_region_jczqk_val['fad_times'],
                    $every_tb_region_jczqk_val['fad_illegal_times'],
                    $every_tb_region_jczqk_val['times_illegal_rate'].'%',
                    $every_tb_region_jczqk_val['fad_play_len'],
                    $every_tb_region_jczqk_val['fad_illegal_play_len'],
                    $every_tb_region_jczqk_val['lens_illegal_rate'].'%'
                ];
            }
        }
        $every_tb_region_data = to_string($every_tb_region_data);

        /*
        * @广播各广告类型监测总情况
        *
        **/
        $every_tb_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','02');
        $every_tb_adclass_data = [];//定义各地区发布监测情况数组

        //循环赋值
        foreach ($every_tb_adclass_jczqk as $every_tb_adclass_jczqk_key => $every_tb_adclass_jczqk_val){

            if(!$every_tb_adclass_jczqk_val['titlename']) {
                $every_tb_adclass_data[] = [
                    strval($every_tb_adclass_jczqk_key+1),
                    $every_tb_adclass_jczqk_val['tadclass_name'],
                    $every_tb_adclass_jczqk_val['fad_times'],
                    $every_tb_adclass_jczqk_val['fad_illegal_times'],
                    $every_tb_adclass_jczqk_val['times_illegal_rate'].'%',
                    $every_tb_adclass_jczqk_val['fad_play_len'],
                    $every_tb_adclass_jczqk_val['fad_illegal_play_len'],
                    $every_tb_adclass_jczqk_val['lens_illegal_rate'].'%'
                ];
            }

        }
        $every_tb_adclass_data = to_string($every_tb_adclass_data);

        /*
        * @报纸各地域监测总情况
        *
        **/
        $every_tp_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','03');
        $every_tp_region_data = [];//定义各地区发布监测情况数组
        $every_tp_region_num = 0;//报纸各违法地域数量

        //循环赋值
        foreach ($every_tp_region_jczqk as $every_tp_region_jczqk_key => $every_tp_region_jczqk_val){

            if(!$every_tp_region_jczqk_val['titlename']) {

                if($every_tp_region_jczqk_val['fregionid'] == $user['regionid']){
                    $every_tp_region_jczqk_tregion_name = $self_name;
                }else{
                    $every_tp_region_jczqk_tregion_name = $every_tp_region_jczqk_val['tregion_name'];
                }
                if($every_tp_region_jczqk_val['fad_illegal_times'] != 0){
                    $every_tp_region_num++;
                    $every_tp_region_name[] = $every_tp_region_jczqk_tregion_name;
                }
                $every_tp_region_data[] = [
                    strval($every_tp_region_jczqk_key+1),
                    $every_tp_region_jczqk_tregion_name,
                    $every_tp_region_jczqk_val['fad_times'],
                    $every_tp_region_jczqk_val['fad_illegal_times'],
                    $every_tp_region_jczqk_val['times_illegal_rate'].'%'
                ];
            }
        }
        $every_tp_region_data = to_string($every_tp_region_data);


        /*
        * @报纸各广告类型监测总情况
        *
        **/
        $every_tp_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','03');
        $every_tp_adclass_data = [];//定义各地区发布监测情况数组
        //循环赋值
        foreach ($every_tp_adclass_jczqk as $every_tp_adclass_jczqk_key => $every_tp_adclass_jczqk_val){

            if(!$every_tp_adclass_jczqk_val['titlename']) {
                $every_tp_adclass_data[] = [
                    strval($every_tp_adclass_jczqk_key+1),
                    $every_tp_adclass_jczqk_val['tadclass_name'],
                    $every_tp_adclass_jczqk_val['fad_times'],
                    $every_tp_adclass_jczqk_val['fad_illegal_times'],
                    $every_tp_adclass_jczqk_val['times_illegal_rate'].'%'
                ];
            }
        }
        $every_tp_adclass_data = to_string($every_tp_adclass_data);

        /*
        * @PC各媒体监测总情况
        *
        **/
        $every_pc_media_data = [];

        $every_pc_media_jczqk = $StatisticalReport->report_ad_monitor('fmediaid',$pc_medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));
        foreach ($every_pc_media_jczqk as $every_pc_media_jczqk_key=>$every_pc_media_jczqk_val){
            if(!$every_pc_media_jczqk_val['titlename']) {
                $every_pc_media_data[] = [
                    strval($every_pc_media_jczqk_key+1),
                    $every_pc_media_jczqk_val['tmedia_name'],
                    $every_pc_media_jczqk_val['fad_times'],
                    $every_pc_media_jczqk_val['fad_illegal_times'],
                    $every_pc_media_jczqk_val['times_illegal_rate'].'%',
                    $every_pc_media_jczqk_val['ck_count']?$every_pc_media_jczqk_val['ck_count']:'0',
                    $every_pc_media_jczqk_val['ckl']?$every_pc_media_jczqk_val['ckl']:'0.00'.'%',
                    $every_pc_media_jczqk_val['cl_count']?$every_pc_media_jczqk_val['cl_count']:'0',
                    $every_pc_media_jczqk_val['cll']?$every_pc_media_jczqk_val['cll']:'0.00'.'%'
                ];
            }
        }

        $every_pc_media_data = to_string($every_pc_media_data);


        //定义数据数组
        $data = [
            'dotfilename' => 'ReportTemplate.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报告标题一",
            "text" => "廊坊市县级传统媒体"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报告标题二",
            "text" => $date_ym."广告监测通报"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报表通报描述",
            "text" => $date_ymd."-".$date_md."，廊坊市县级传统媒体广告监测平台对全市所属的媒介广告发布情况进行了监测，现将有关情况通报如下："
        ];//1008

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "一、监测范围及对象",
            "text" => "一、监测范围及对象"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "传统媒体广告监测描述",
            "text" => "本期共对全市各区".$ct_media_count."家电视、广播、报纸等传统媒体广告发布情况进行了监测，其中电视媒体".$ct_media_tv_count.
                "家、广播媒体".$ct_media_bc_count."家、报纸媒体".$ct_media_paper_count."家，广告监测范围为".$date_ymd."-".$date_md."（共".$days."天）"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "二、监测总体情况",
            "text" => "二、监测总体情况"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "传统媒体广告监测情况描述",
            "text" => "本期共监测到电视、广播、报纸的各类广告".$ct_fad_times."条次，涉嫌违法广告".$ct_fad_illegal_times."条次，条次违法率为".round(($ct_fad_illegal_times/$ct_fad_times)*100,2)."％。"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表1各类媒体广告监测统计情况标题",
            "text" => "表1各类媒体广告监测统计情况"
        ];
        $every_media_data = array_values($every_media_data);

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表1各类媒体广告监测统计情况列表含时长",
            "data" => $every_media_data
        ];

        array_pop($every_media_data);
        $every_media_datas = $this->create_chart_data($every_media_data,0,1,2,3);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告监测情况条状图",
            "data" => $every_media_datas[0]
        ];
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告发布量饼状图",
            "data" => $every_media_datas[1]
        ];

        if(count($every_media_datas[2]) >1){
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "各类媒体广告违法发布量饼状图",
                "data" => $every_media_datas[2]
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "三、传统媒体广告监测情况",
            "text" => "三、传统媒体广告监测情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（一）广告总体监测情况",
            "text" => "（一）广告总体监测情况"
        ];

        if($every_region_num > 0){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "广告总体监测情况描述",
                "text" => "本期监测数据显示，发布涉嫌违法广告条次违法率最为严重的".$every_region_num."个".$xj_name."依次为：".implode('、',$every_region_name)."。"
            ];
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "广告总体监测情况描述",
                "text" => "本期监测数据显示，暂未发现违法广告。"
            ];
        }



        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表2本期各地区发布涉嫌违法广告情况标题",
            "text" => "表2 本期各地区发布涉嫌违法广告情况"
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表2本期各地区发布涉嫌违法广告情况列表含时长",
            "data" => $every_region_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表2注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];

        $every_region_data_chart = $this->create_chart_data($every_region_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告监测情况条状图",
            "data" => $every_region_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告发布量饼状图",
            "data" => $every_region_data_chart[1]
        ];

        if($every_region_num > 0) {
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "本期各地区广告违法发布量饼状图",
                "data" => $every_region_data_chart[2]
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表3本期各大类违法广告发布总情况标题",
            "text" => '表3本期各大类违法广告发布总情况'
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表3本期各大类违法广告发布总情况列表含时长",
            "data" => $every_adclass_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表3注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];

        $every_adclass_data_chart = $this->create_chart_data($every_adclass_data);

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告监测情况列条状图",
            "data" => $every_adclass_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告发布量饼状图",
            "data" => $every_adclass_data_chart[1]
        ];

        if(!empty($every_adclass_name)){
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "本期各大类广告违法发布量饼状图",
                "data" => $every_adclass_data_chart[2]
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "各类媒体广告发布情况描述",
                "text" => '涉嫌违法广告类别主要集中在'.implode('、',$every_adclass_name).'，其中'.implode("、",$every_adclass_rate_des).'。'
            ];

        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "各类媒体广告发布情况描述",
                "text" => '本期各大类别暂未发现违法广告。'
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（二）电视发布涉嫌违法广告情况",
            "text" => '（二）电视发布涉嫌违法广告情况'
        ];
        if(!empty($every_tv_region_data)){
            if($every_tv_region_num > 0){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "电视发布涉嫌违法广告情况描述",
                    "text" => '本期电视发布涉嫌违法广告条次违法率最为严重的'.$every_tv_region_num.'个地区依次为：'.implode('、',$every_tv_region_name).'。'
                ];
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "电视发布涉嫌违法广告情况描述",
                    "text" => '本期电视暂未发现违法广告。'
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况标题",
                "text" => '表4 电视发布涉嫌广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况列表",
                "data" => $every_tv_region_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表4注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];
            $every_tv_region_data_chart = $this->create_chart_data($every_tv_region_data);
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "电视广告监测情况条状图",
                "data" => $every_tv_region_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "电视广告发布量饼状图",
                "data" => $every_tv_region_data_chart[1]
            ];
            if($every_tv_region_num > 0){
                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "电视广告违法发布量饼状图",
                    "data" => $every_tv_region_data_chart[2]
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表5电视各广告类型发布涉嫌违法广告情况标题",
                "text" => '表5 电视各广告类型发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况列表",
                "data" => $every_tv_adclass_data
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表5注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];

            $every_tv_adclass_data_chart = $this->create_chart_data($every_tv_adclass_data);
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "电视各广告类型广告监测情况条状图",
                "data" => $every_tv_adclass_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "电视各广告类型广告发布量饼状图",
                "data" => $every_tv_adclass_data_chart[1]
            ];
            if($every_tv_region_num > 0){
                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "电视各广告类型广告违法发布量饼状图",
                    "data" => $every_tv_adclass_data_chart[2]
                ];
            }
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "电视发布涉嫌违法广告情况描述",
                "text" => '本期电视暂未监测到广告。'
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（三）广播发布涉嫌违法广告情况",
            "text" => '（三）广播发布涉嫌违法广告情况'
        ];
        if(!empty($every_tb_region_data)){
            if($every_tb_region_num > 0){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "广播发布涉嫌违法广告情况描述",
                    "text" => '本期广播发布涉嫌违法广告条次违法率最为严重的'.$every_tb_region_num.'个地区依次为：'.implode('、',$every_tb_region_name).'。'
                ];
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "广播发布涉嫌违法广告情况描述",
                    "text" => '本期广播暂未发现违法广告。'
                ];
            }
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表6广播发布涉嫌违法广告情况标题",
                "text" => '表6 广播发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况列表",
                "data" => $every_tb_region_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表6注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];

            $every_tb_region_data_chart = $this->create_chart_data($every_tb_region_data);
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广播广告监测情况条状图",
                "data" => $every_tb_region_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广播广告发布量饼状图",
                "data" => $every_tb_region_data_chart[1]
            ];
            if($every_tb_region_num > 0){

                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "广播广告违法发布量饼状图",
                    "data" => $every_tb_region_data_chart[2]
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表7广播各广告类别发布涉嫌违法广告情况标题",
                "text" => '表7 广播各广告类别发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况列表",
                "data" => $every_tb_adclass_data
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表7注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];

            $every_tb_adclass_data_chart = $this->create_chart_data($every_tb_adclass_data);

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广播各广告类型广告监测情况条状图",
                "data" => $every_tb_adclass_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广播各广告类型广告发布量饼状图",
                "data" => $every_tb_adclass_data_chart[1]
            ];
            if($every_tb_region_num > 0){

                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "广播各广告类型广告违法发布量饼状图",
                    "data" => $every_tb_adclass_data_chart[2]
                ];
            }
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "广播发布涉嫌违法广告情况描述",
                "text" => '本期广播暂未监测到广告。'
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（四）报纸发布涉嫌违法广告情况",
            "text" => '（四）报纸发布涉嫌违法广告情况'
        ];
        if(!empty($every_tp_region_data)){
            if($every_tp_region_num > 0){
                $every_tp_region_str = '本期报纸发布涉嫌违法广告条次违法率最为严重的'.$every_tp_region_num.'个地区依次为：'.implode('、',$every_tp_region_name).'。';
            }else{
                $every_tp_region_str = '本期各地区报纸暂未发布违法广告。';
            }//1119e
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "报纸发布涉嫌违法广告情况描述",
                "text" => $every_tp_region_str
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表8报纸发布涉嫌违法广告情况标题",
                "text" => '表8 报纸发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表8报纸发布涉嫌违法广告情况列表",
                "data" => $every_tp_region_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表8注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];
            $every_tp_region_data_chart = $this->create_chart_data($every_tp_region_data);

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "报纸广告监测情况条状图",
                "data" => $every_tp_region_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "报纸广告发布量饼状图",
                "data" => $every_tp_region_data_chart[1]
            ];
            if($every_tp_region_num > 0){

                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "报纸广告违法发布量饼状图",
                    "data" => $every_tp_region_data_chart[2]
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表9报纸各广告类型发布涉嫌违法广告情况标题",
                "text" => '表9 报纸各广告类型发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表9报纸各广告类型发布涉嫌违法广告情况列表",
                "data" => $every_tp_adclass_data
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表9注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];
            $every_tp_adclass_data_chart = $this->create_chart_data($every_tp_adclass_data);

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "报纸各广告类型广告监测情况条状图",
                "data" => $every_tp_adclass_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "报纸各广告类型广告发布量饼状图",
                "data" => $every_tp_adclass_data_chart[1]
            ];
            if($every_tp_region_num > 0){

                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "报纸各广告类型广告违法发布量饼状图",
                    "data" => $every_tp_adclass_data_chart[2]
                ];
            }
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "报纸发布涉嫌违法广告情况描述",
                "text" => '本期报纸暂未监测到广告'
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "三、传统媒体广告监测情况",
            "text" => "四、广告数据线索查看处置情况"
        ];

        if(count($clue_statistics_data) > 0 && empty($illegal_ad_list_data)){

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "广告总体监测情况描述",
                "text" => '本期未发现违法广告'
            ];

        }else{

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表8报纸发布涉嫌违法广告情况标题",
                "text" => '表10 线索汇总列表'
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "线索统计表格",
                "data" => $clue_statistics_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表9注释",
                "text" => '（注：按行政区划排列）'
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表8报纸发布涉嫌违法广告情况标题",
                "text" => '表11 违法广告列表'
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表按名称",
                "data" => $illegal_ad_list_data
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表9注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表8报纸发布涉嫌违法广告情况标题",
            "text" => "廊坊市整治虚假违法广告联席会议办公室"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表8报纸发布涉嫌违法广告情况标题",
            "text" => $date_end_ymd
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报纸发布涉嫌违法广告情况描述",
            "text" => "主送：市、县级整治虚假违法广告联席会议成员单位，各县（市）市场监督管理局，各县（市）广播电视台"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报纸发布涉嫌违法广告情况描述",
            "text" => "抄送：市、县（市）审计局，各县（市）派驻市场监督管理局和广播电视台的纪检监察机构"
        ];

        $report_data = json_encode($data);
        //echo $report_data;exit;//20190103

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $system_num = getconfig('system_num');

            //将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].date('Y年m月',strtotime($month)).'月报';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 30;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }

}