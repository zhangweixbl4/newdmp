Vue.prototype.$ELEMENT = { size: "mini", zIndex: 3000 };

var vm = new Vue({
  el: '#app',
  data () {
    return{
      loading: false,
      form:{
        page: 1,
        pageSize: 20,
        keyword: '',
      },
      tag: 1,
      title: '',
      addForm: {
        on_off: true,
      },
      fid: '',
      activeRow: {},
      tableData:[],
      total: 0,
      drawer: false,
    }
  },
  created(){
    this.search()
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight()
      document.querySelector('#app').style.opacity = 1
    })
    window.onresize = () => {
      this.getTableHeight()
    }
  },
  methods:{
    search() {
      this.loading = true
      fly.post('/Admin/TimeTask/data_list', Qs.stringify(this.form)).then(({ data }) => {
        if (data.code === 0) {
          this.tableData = data.data_list
          this.total = Number(data.data_count)
		      this.keyword = this.form.keyword
        } else {
          this.$message.error(data.msg)
        }
        this.loading = false
      }).catch(e => {
        this.$message.error(e)
      })
    },
    openWindow(val,row) {
      this.tag =  val
      if (val === 2){
        this.title = '添 加'
      } else if(val === 3){
        this.title = '编 辑'
        this.addForm = {...row}
        row.on_off === '1' ? this.addForm.on_off  = true : this.addForm.on_off =  false
        this.fid  = row.fid
      }
      this.drawer = true
    },
    detail(val, id){
      this.tag = val
      this.title = '详 情'
      fly.post('/Admin/TimeTask/data_details', Qs.stringify({fid: id})).then(({ data }) => {
        if (data.code === 0) {
          this.drawer = true
          this.addForm = {...data.data_details}
        } else {
          this.$message.error(data.msg)
        }
        this.loading = false
      }).catch(e => {
        this.$message.error(e)
      })
    },
    onSubmit(){
      const data ={
        ...this.addForm
      }
      data.on_off = this.addForm.on_off == true ? 1 :0
      if(this.title === '编辑'){
        data.fid = this.fid
      }
      fly.post('/Admin/TimeTask/add_edit', Qs.stringify(data)).then(({ data }) => {
        if (data.code === 0) {
          this.$notify({
            title: '成功',
            message: data.msg,
            type: 'success',
            duration: 1000,
          });
          this.search()
        } else {
          this.$message.error(data.msg)
        }
        this.handleClose()
      }).catch(e => {
        console.error(e)
      })
    },
    deleted(row) {
      fly.post('/Admin/TimeTask/del', Qs.stringify({fid: row.fid})).then(({ data }) => {
        if (data.code === 0) {
          this.$notify({
            title: '成功',
            message: data.msg,
            type: 'success',
            duration: 1000,
          });
          row.visible = false
        } else {
          this.$message.error(data.msg)
        }
        this.search()
      }).catch(e => {
        this.$message.error(e)
      })
    },
    handleCurrentChange(page) {
      this.form.page = page
      this.search()
    },
    handleSizeChange(val) {
      this.form.pageSize = val
      this.search()
    },
    handleClose() {
      this.drawer = false
      this.addForm = {}
    },
    getTableHeight() {
      let formHeight = this.$refs.sform.$el.clientHeight
      this.tableHeight = `${document.body.clientHeight - formHeight}px`
    },
  }
})