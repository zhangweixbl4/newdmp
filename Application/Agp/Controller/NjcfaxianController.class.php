<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 监测发现
 */

class NjcfaxianController extends BaseController
{
	/**
	 * 获取监测发现列表
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function index() {
		session_write_close();
		$ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $ischeck = $ALL_CONFIG['ischeck'];

		$p 					= I('page', 1);//当前第几页
		$pp 				= 20;//每页显示多少记录
		$mclass 			= I('mclass')?I('mclass'):0;// 媒体类型
		$fadname 			= I('fadname');// 广告名称
		$fmedianame 		= I('fmedianame');// 发布媒介
		$fadclasscode 		= I('fadclasscode');// 广告内容类别
		$gxrqst 		= I('gxrqst');//更新开始日期
		$gxrqed 		= I('gxrqed');//更新结束日期
		$area 			= I('area');//所属地域

		$where['_string'] = '1=1';
		if ($fadname != '') {
			$where['a.fad_name'] = array('like', '%' . $fadname . '%');//广告名称
		}
		if ($fmedianame != '') {
			$where['d.fmedianame'] = array('like', '%' . $fmedianame . '%');//发布媒介
		}
		if (!empty($fadclasscode)) {
			$arr_code=D('Function')->get_next_tadclasscode($fadclasscode,true);//获取下级广告类别代码
			if($arr_code){
				$where['a.fad_class_code'] = array('in', $arr_code);
			}else{
				$where['a.fad_class_code'] = $fadclasscode;
			}
		}
		$where_time = '1=1';
		if(!empty($gxrqst)&&!empty($gxrqed)){
			$where_time = 'fissue_date between "'.$gxrqst.'" and "'.$gxrqed.'"';
		}
		
		//是否抽查模式
		if(!empty($ischeck)){
		    $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
		    //如果抽查表有数据
		    if(!empty($spot_check_data)){
		        $dates = [];//定义日期数组
		        foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
		            $year_month = '';
		            $date_str = [];
		            $year_month = substr($spot_check_data_val['fmonth'],0,7);
		            if(!empty($spot_check_data_val['condition'])){
		            	$date_str = explode(',',$spot_check_data_val['condition']);
		            }
		            foreach ($date_str as $date_str_val){
		                $dates[] = $year_month.'-'.$date_str_val;
		            }
		        }
				$where_time 	.= ' and tbn_illegal_ad_issue.fissue_date in ("'.implode('","', $dates).'")';
		    }else{
		        $where_time 	.= ' and 1=0';
		    }
		}

		$where['a.fstatus'] = 0;
		if(!empty($mclass)){
			$where['a.fmedia_class'] 	= $mclass;
		}
		if(!empty($area)){
			$where['fregion_id'] = $area;
		}
		$where['a.fregisterid'] = 0;
		$where['a.fcustomer'] 	= $system_num;

		$count = M('tbn_illegal_ad')
			->alias('a')
			->join('tmedia d on  a.fmedia_id=d.fid and d.fid=d.main_media_id')//媒介信息
			->join('tmedia_temp ttp on d.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->join('tadclass e on a.fad_class_code=e.fcode ')//广告内容列别
			->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
			->where($where)
			->count();// 查询满足要求的总记录数

		
		$data = M('tbn_illegal_ad')
			->alias('a')
			->field('
				a.fid,
				a.fmedia_class as mclass,
	            a.fad_name as fadname,
	            d.fmedianame,
	            e.ffullname as adclass_fullname,
	            a.fillegal,
	            a.create_time,
	            x.fstarttime,
	            x.fendtime,
	            x.fcount,
	            (case when fmedia_class=1 then (select fexpressioncodes from ttvsample where fid=fsample_id) when fmedia_class=2 then (select fexpressioncodes from tbcsample where fid=fsample_id ) when fmedia_class=3 then (select fexpressioncodes from tpapersample where fpapersampleid=fsample_id ) end) as fexpressioncodes,
	            (case when fmedia_class=1 then (select fexpressions from ttvsample where fid=fsample_id) when fmedia_class=2 then (select fexpressions from tbcsample where fid=fsample_id ) when fmedia_class=3 then (select fexpressions from tpapersample where fpapersampleid=fsample_id ) end) as fexpressions,
	            (case when fmedia_class=1 then (select fconfirmations from ttvsample where fid=fsample_id) when fmedia_class=2 then (select fconfirmations from tbcsample where fid=fsample_id ) when fmedia_class=3 then (select fconfirmations from tpapersample where fpapersampleid=fsample_id ) end) as fconfirmations
			')
			->join('tmedia d on  a.fmedia_id=d.fid and d.fid=d.main_media_id')//媒介信息
			->join('tmedia_temp ttp on d.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->join('tadclass e on a.fad_class_code=e.fcode ')//广告内容列别
			->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
			->where($where)
			->order('x.fstarttime desc')
			->page($p,$pp)
			->select();//查询违法广告

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	/**
	 *(手工/系统(合并/单个))登记线索操作
	 * by zw
	 */
	public function create_register()
	{

		$fillegaladid 	= I('fillegaladid');//违法广告ID，非手工登记必要参数
		$fid 			= I('fid');//登记ID
		$fchecktype 	= I('fchecktype');//处理方式
		
		$where['fid'] 		= $fid;//登记表ID
		$where['fstate'] 	= 0;
		$tregister_model = M("tregister");
		$where['_string'] 	= '((fwaitregulatorid='.session('regulatorpersonInfo.fregulatorpid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid='.session('regulatorpersonInfo.fid').'))';//用户处理权限范围
		$data = $tregister_model->where(['fid'=>$fid])->find();

		if(!empty($data) && ($fchecktype==10 || $fchecktype==40 )){
			
			if($fchecktype==10){//线索办结
				$res['fwaitregulatortype']	= 0;//下一处理单位类型，部门
				$res['fwaitregulatorid']	= 0;//下一处理部门默认无
				$res['fwaitpersonid']		= 0;//下一处理人默认无
				$res['fstate']				= 40;//办结状态
				$res['fdealcontents']		= I('fcheckcontents');//办结说明

				//新增流程记录
				$sh_list['fregisterid'] 				= $fid;
				$sh_list['fupflowid'] 					= 0;
				$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
				$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
				$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
				$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
				$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
				$sh_list['fflowname'] 					= '监测发现';
				$sh_list['freason'] 					= '线索办结';
				$sh_list['fregulatorid'] 				= session('regulatorpersonInfo.fregulatorid');//处理机构id
				$sh_list['fregulatorname'] 				= session('regulatorpersonInfo.regulatorname');//处理机构名称
				$sh_list['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');//处理人id
				$sh_list['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');//处理人名
				$sh_list['fcreateinfo'] 				= I('fcheckcontents');//处理建议
				$sh_list['fstate'] 						= 40;
				$sh_list['fchildstate'] 				= 10;
				$flowid = M("tregisterflow")->add($sh_list);

			}elseif($fchecktype==40){//线索交办
				$fwaitregulatorid 					= I('fwaitregulatorid');//移交机关ID
				if(empty($fwaitregulatorid)){
					$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
				}
				$res['fwaitregulatortype']			= 10;//下一处理单位类型，机构
				$res['fwaitregulatorid']			= I('fwaitregulatorid');//下一处理部门默认无
				$res['fwaitpersonid']				= 0;//下一处理人默认无
				$res['fstate'] 						= 20;//线索待处置
				
				//新增流程记录
				$sh_list['fregisterid'] 				= $fid;
				$sh_list['fupflowid'] 					= 0;
				$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
				$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
				$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
				$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
				$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
				$sh_list['fflowname'] 					= '监测发现';
				$sh_list['freason'] 					= '线索交办';
				$sh_list['fregulatorid'] 				= I('fwaitregulatorid');//下一处理机构id
				$sh_list['fregulatorname'] 				= I('fwaitregulatorname');//下一处理机构名称
				$sh_list['fcreateinfo'] 				= I('fcheckcontents');//处理建议
				$sh_list['fstate'] 						= 20;
				$sh_list['fchildstate'] 				= 40;
				$flowid = M("tregisterflow")->add($sh_list);

			}
			if(!empty($flowid)){
				//将登记表id写入违法广告表，用以标记已处理的违法广告（非手工登记方式）
				if(!empty($fillegaladid)){
					//更改的数据
					$tddata['fstate'] 			= 1;
					$tddata['fregisterid'] 		= $fid;

					$tdwhere['fstate'] 			= 0;
					if(is_array($fillegaladid)){
						$tdwhere['fillegaladid'] 	= array('in',$fillegaladid);
					}else{
						$tdwhere['fillegaladid']	= $fillegaladid;
					}
					M('tillegalad')->where($tdwhere)->save($tddata);
				}

				M()->execute('update document set dt_flowid='.$flowid.' where dt_tregisterid='.$fid);
				M()->execute('update tregisterfile set fillegaladflowid='.$flowid.' where fregisterid='.$fid);
			}

			//待写入的初核信息--公共
			$reg['fmodifier']			= session('regulatorpersonInfo.fname');//修改人
			$reg['fmodifytime']			= date('Y-m-d H:i:s');//修改时间
			$trdo = M('tregister')->where(['fid'=>$fid])->save($res);
			if(!empty($trdo) && !empty($flowid)){
				D('Function')->write_log('监测发现',1,'操作成功','tregister',$trdo,M('tregister')->getlastsql());
				$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
			}else{
				D('Function')->write_log('监测发现',0,'操作失败','tregister',0,M('tregister')->getlastsql());
				$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
			}
		}else{
			D('Function')->write_log('监测发现',0,'操作失败','tregister',0,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
		}
	}

	/**
	 *获取待登记违法广告详情（单条/列表）
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function get_tillegalad_details(){
		
		$fillegaladid = I('fillegaladid');//违法广告id
		
		if(is_array($fillegaladid)){
			foreach ($fillegaladid as $key => $value) {
				$data[$key] = $this->get_tillegaladlist_details($value);
			}
		}else{
			$data[0] = $this->get_tillegaladlist_details($fillegaladid);
		}

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>count($data),'list'=>$data,'data'=>$do_tr)));
	}

	/**
	 *获取待登记违法广告
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（data-违法广告信息，adclueInfo-样本信息，issue-广告发布信息）
	 * by zw
	 */
	public function get_tillegaladlist_details($fillegaladid)
	{
		$where['tillegalad.fillegaladid'] = $fillegaladid;
		$data = M('tillegalad')
			->field('
				tillegalad.fillegaladid,
				tillegalad.fmediaclassid,
				tillegalad.fadid,
				tillegalad.fmediaid,
	            tad.fadname,
	            tadclass.ffullname as adclass_fullname,
	            tillegalad.fissuetimes,
	            tillegalad.fdisposetimes,
	            tillegalad.ffirstissuetime,
	            tillegalad.flastissuetime,
	            tad.fbrand,
	             (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fname,
	            tillegalad.fversion,
	            tillegalad.fillegalcontent,
	            tillegalad.fexpressioncodes,
	            tillegalad.fspokesman,
	            tillegalad.fexpressions,
	            tillegalad.fconfirmations,
	            tillegalad.fsampleid,
	            tmediaowner. fcreditcode,
	            tmediaowner.fregaddr,
	            tmediaowner. flinkman,
	            tmediaowner.ftel,
	            tmediaowner.fname as mediaowner_name
			')
			->join('tad on tillegalad.fadid = tad.fadid and tad.fadid<>0')//广告信息
			->join('tadclass on tadclass.fcode = tad.fadclasscode')//广告内容列别
			->join('tmedia on tmedia.fid = tillegalad.fmediaid and tmedia.fid=tmedia.main_media_id')//媒介信息
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
			->where($where)
			->find();//查询违法广告信息
		if ($data['fmediaclassid']) {
			switch ($data['fmediaclassid']) {
				case 'bc':
					$adclueInfo = M('tbcsample')->field('fid,favifilepng,favifilename,fadlen,fadmanuno,fmanuno,fadapprno,fapprno,fadent,fent,fentzone')->where(array('fadid' => $data['fadid']))->find();
					// $issue = M('tbcissue')
					// 	->field('tmedia.fmedianame,tbcissue.fstarttime,tbcissue.fendtime,tbcissue.fissuedate')
					// 	->join('tmedia on  tbcissue.fmediaid=tmedia.fid', 'LEFT')//媒介信息
					// 	->where(array('fbcsampleid'=>$data['fsampleid']))
					// 	->select();//发布记录
					break;
				case 'tv':
					$adclueInfo = M('ttvsample')->field('fid,favifilepng,favifilename,fadlen,fadmanuno,fmanuno,fadapprno,fapprno,fadent,fent,fentzone')->where(array('fadid' => $data['fadid']))->find();
					// $issue = M('ttvissue')
					// 	->field('tmedia.fmedianame,ttvissue.fstarttime,ttvissue.fendtime,ttvissue.fissuedate')
					// 	->join('tmedia on  ttvissue.fmediaid=tmedia.fid', 'LEFT')//媒介信息
					// 	->where(array('ftvsampleid'=>$data['fsampleid']))
					// 	->select();//发布记录
					break;
				case 'paper':
					$adclueInfo = M('tpapersample')->field('fpapersampleid,fjpgfilename as favifilepng,fjpgfilename,fadmanuno,fmanuno,fadapprno,fapprno,fadent,fent,fentzone')->where(array('fadid' => $data['fadid']))->find();
					// $issue = M('tpaperissue')
					// 	->field('tmedia.fmedianame,tpaperissue.fissuedate')
					// 	->join('tmedia on  tpaperissue.fmediaid=tmedia.fid', 'LEFT')//媒介信息
					// 	->where(array('fpapersampleid'=>$data['fsampleid']))
					// 	->select();//发布记录
					break;
			}
		}
		$data2['data'] 			= $data;
		$data2['adclueInfo'] 	= $adclueInfo;
		// $data2['issue'] 		= $issue;
		return $data2;
	}

	/**
	 *获取登记详情
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function get_registerview(){

		$fillegaladid = I('fillegaladid');//违法广告id
		if(empty($fillegaladid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}
		// if(is_array($fillegaladid)){
			$fillegaladid2	= implode(',',$fillegaladid);//违法广告ID组
			$fadid = $fillegaladid[0];
			
		// }else{
		// 	$fillegaladid2	=$fillegaladid;//违法广告ID
		// 	$fadid = $fillegaladid2;
		// }
		
		$tregister_model = M("tregister");
		$data = $tregister_model->where('ffillegaladid="'.$fillegaladid2.'" and fregisterregulatorid='.session('regulatorpersonInfo.fregulatorpid').' and fregisterpersonid='.session('regulatorpersonInfo.fid').' and ffromtype=10 and fstate=0')->find();
		if(empty($data)){
			$do_td = M('tillegalad')->field('tad.fadname')->join('tad on tillegalad.fadid=tad.fadid and tad.fadid<>0')->where('tillegalad.fillegaladid='.$fadid)->find();
			$fname = $do_td['fadname']?$do_td['fadname']:'';
			$res['fname']					= $fname;//登记表名称
			$res['fregisterregulatorid']	= session('regulatorpersonInfo.fregulatorpid');//登记机构id
			$res['fregisterregulatorname']	= session('regulatorpersonInfo.regulatorpname');//登记机构名称
			$res['fregisterpersonid']		= session('regulatorpersonInfo.fid');//登记人id
			$res['fregisterpersonname']		= session('regulatorpersonInfo.fname');//登记人姓名
			$res['fcreatetime'] 			= date('Y-m-d H:i:s');//创建时间
			$res['fregistertime']			= date('Y-m-d H:i:s');//登记时间
			$res['ffromtype']				= 10;//来源分类
			$res['fpersontype']				= 0;//举报人类型
			$res['fwaitregulatortype']		= 0;//下一处理机构，部门
			$res['fwaitregulatorid']		= 0;//下一处理部门默认无
			$res['fwaitpersonid']			= 0;//下一处理人默认无
			$res['ffillegaladid']			=$fillegaladid2;//违法广告ID
			$res['fstate'] 					= 0;//待登记
			$fregisterid = $tregister_model->add($res);//添加登记信息
			$data = $tregister_model->where('fid='.$fregisterid)->find();
		}

		if(!empty($data)){
			$data['flow'] = M('tregisterflow')->field('fflowname,fcreateregualtorpersonname,fcreatetime,fcreateregualtorid,fcreateregualtorname,freason,fcreateinfo')->where('fregisterid='.$data['fid'])->order('flowid desc')->select();//流转信息
			foreach ($data['flow'] as $key => $value) {
				$data['flow'][$key]['fcreateregualtorpname'] = D('Function')->get_tregulatornameaction($value['fcreateregualtorid']);
			}
			$data['refiles'] = M('tregisterfile')->field('fid,fattachname,fattachurl,ffilename,tregisterfile.fflowname,fcreateregualtorpersonid as createpersonid')->join('tregisterflow on tregisterfile.fillegaladflowid=tregisterflow.flowid','left')->where('is_file=1 and tregisterfile.fregisterid='.$data['fid'])->order('fid asc')->select();//获取附件列表

			$do_dt = M('document')->field('dt_id,dt_name,dt_showstate,dt_createpersonid as createpersonid')->join('tregisterflow on tregisterflow.flowid=document.dt_flowid','left')->where('dt_tregisterid='.$data['fid'])->order('dt_id desc')->select();//获取文书列表
			$aa = [];
			foreach ($do_dt as $key => $value) {
				$isf = 1;
				if (($value['dt_showstate']==10 && $value['dt_createpersonid']==session('regulatorpersonInfo.fid'))||$value['dt_flowid']==0) {//仅自己可见
					array_push($aa, $value);
				}elseif($value['dt_showstate']==0){//下级可见
					if($value['fregulatorpersonid']!=session('regulatorpersonInfo.fid')&&$value['fregulatorpersonid']!=0){
						$isf = 2;
					}
					if($value['fregulatorid']!=session('regulatorpersonInfo.fregulatorid')&&$value['fregulatorid']!=0||$value['fregulatorid']==0){
						$isf = 2;
					}
					if($isf == 1){
						array_push($aa, $value);
					}
				}
			}
			$data['document'] = $aa;
			$data['fillegaladid'] = M('tillegalad')->where('fregisterid='.$data['fid'])->order('fillegaladid desc')->getField('fillegaladid',true);
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('data'=>$data)));
		}

	}

	/**
	 *获取待登记违法广告详情（单条/列表）
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function get_tbn_illegal_ad_details(){
		
		$fillegaladid 	= I('fillegaladid');//违法广告id
		if(empty($fillegaladid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}
		if(is_array($fillegaladid)){
			foreach ($fillegaladid as $key => $value) {
				$data[$key] = $this->get_tbn_illegal_adlist_details($value);
			}
		}else{
			$data[0] = $this->get_tbn_illegal_adlist_details($fillegaladid);
		}

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>count($data),'list'=>$data)));
	}

	/**
	 *获取待登记违法广告
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（data-违法广告信息，adclueInfo-样本信息，issue-广告发布信息）
	 * by zw
	 */
	public function get_tbn_illegal_adlist_details($fillegaladid)
	{
		$system_num = getconfig('system_num');
		$where['a.fid'] = $fillegaladid;
		$where['a.fcustomer'] = $system_num;

		$data = M('tbn_illegal_ad')
		->alias('a')
		->field('
			a.fid, a.fmedia_class, a.fsample_id, a.fmedia_id, a.create_time,
			(case when instr(d.fmedianame,"（") > 0 then left(d.fmedianame,instr(d.fmedianame,"（") -1) else d.fmedianame end) as fmedianame,
            e.ffullname as adclass_fullname,
            f.fcreditcode, f.fregaddr, f.flinkman, f.ftel, f.fname as mediaowner_name,a.fillegaltypecode
		')
		->join('tmedia d on  a.fmedia_id=d.fid and d.fid=d.main_media_id')//媒介信息
		->join('tadclass e on a.fad_class_code=e.fcode ')//广告内容列别
		->join('tmediaowner f on f.fid = d.fmediaownerid')
		->where($where)
		->find();
		if(!empty($data)){
			if($data['fmedia_class']==1){
				$data_se = M('ttvsample')
				->field('
					b.fadid, b.favifilepng, b.fadlen, b.fversion, b.fadmanuno, b.fmanuno, b.fadapprno, b.fapprno, b.fadent, b.fent, b.fentzone, b.fillegalcontent, b.fexpressioncodes, b.fexpressions, b.fconfirmations, b.fpunishments, b.favifilename,b.fissuedate,
            		c.fadname, c.fbrand
            	')
				->alias('b')
				->join('tad c on b.fadid = c.fadid and c.fadid<>0')
				->where('fid='.$data['fsample_id'])
				->find();
			}elseif($data['fmedia_class']==2){
				$data_se = M('tbcsample')
				->field('
					b.fadid, b.favifilepng, b.fadlen, b.fversion, b.fadmanuno, b.fmanuno, b.fadapprno, b.fapprno, b.fadent, b.fent, b.fentzone, b.fillegalcontent, b.fexpressioncodes, b.fexpressions, b.fconfirmations, b.fpunishments, b.favifilename,b.fissuedate,
            		c.fadname, c.fbrand
            	')
				->alias('b')
				->join('tad c on b.fadid = c.fadid and c.fadid<>0')
				->where('fid='.$data['fsample_id'])
				->find();
			}elseif($data['fmedia_class']==3){
				$data_se = M('tpapersample')
				->field('
					b.fadid, fjpgfilename as favifilepng,fjpgfilename, b.fadmanuno, b.fmanuno, b.fadapprno, b.fapprno, b.fadent, b.fent, b.fentzone, b.fillegalcontent, b.fexpressioncodes, b.fexpressions, b.fconfirmations, b.fpunishments,b.fissuedate,
	            	c.fadname, c.fbrand,
	            	y.fpage as fadlen
            	')
				->alias('b')
				->join('tad c on b.fadid = c.fadid and c.fadid<>0')
				->join('tpaperissue y on b.fpapersampleid=y.fpapersampleid')
				->where('b.fpapersampleid='.$data['fsample_id'])
				->find();
			}

			if($data['fmedia_class']==1 || $data['fmedia_class']==2){
				$where_te['fillegal_ad_id'] = $fillegaladid;
				$do_te = M('tbn_illegal_ad_issue')
				->alias('a')
				->field('fid as tid,fmedia_id,fstarttime,fendtime')
				->where($where_te)
				->order('fstarttime asc')
				->select();
				$data_se['issue_list'] = $do_te;

				$where2['source_type'] = 20;
				$where2['source_tid'] = $fillegaladid;
				if($data['fmedia_class']==1){
					$where2['source_mediaclass'] = '01';
				}else{
					$where2['source_mediaclass'] = '02';
				}
				$where2['validity_time'] = ['gt',time()];
				$data2 = M('source_make')->field('source_url,source_id,source_state')->where($where2)->order('source_id desc')->find();
				if(!empty($data2)){
					$data_se['source_url'] = $data2['source_url'];
					$data_se['source_id']  = $data2['source_id'];
					$data_se['source_state'] = $data2['source_state'];
				}
			}
			
			$data = array_merge($data,$data_se);
		}

		return $data;
	}
	

}