<?php
namespace Open\Model;



class CutTaskModel{



	/*快剪任务流程*/
	public function add_flow($cut_task_id,$flow_content,$wx_id = 0,$flow_short = ''){
		
		
		$flow_id = M('cut_task_flow')->add(array(
													'cut_task_id'=>$cut_task_id,//任务ID
													'flow_content'=>$flow_content,//流程详情
													'wx_id'=>$wx_id,//微信ID
													'flow_short'=>$flow_short,//流程简写
													'flow_ip'=>get_client_ip(),
													'flow_time'=>time()
													
													));
		
		
		return $flow_id;
		
		
	}
	
	#查询某一天的任务是否处理完毕
	public function is_task_finish($fmediaid,$fdate){
		$count = M('cut_task')->master(true)->where(array('fmediaid'=>$fmediaid,'fissuedate'=>$fdate,'fstate'=>array('in','0,1,2,3')))->count();
		
		if($count == 0){
			return true;
		}else{
			return false;
		}
	}














}