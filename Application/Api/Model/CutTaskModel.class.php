<?php
namespace Api\Model;
use Think\Exception;
use Think\Log;


class CutTaskModel{
	
	
	
	
	
	public function make_task($mediaId,$date){
		$mediaInfo = M('tmedia')->cache(true,600)->field('left(fmediaclassid,2) as media_class,priority')->where(array('fid'=>$mediaId))->find();
		if(!$mediaInfo) return 0;
		if($mediaInfo['media_class'] == '01'){
			$source_class = 'mp4';
		}elseif($mediaInfo['media_class'] == '02'){
			$source_class = 'aac';
		}else{
			return 0;
		}
		
		$insertCount = 0;
		for($i=0;$i<86400;$i+=3600*8){
			$startTime = strtotime($date) + $i;
			$endTime = $startTime + 3600*9;
			if($endTime > (strtotime($date) + 86400)){
				$endTime = strtotime($date) + 86400;
			}
			$m3u8Url = A('Common/Media','Model')->get_m3u8($mediaId,$startTime,$endTime);
			$ck = M('cut_task')->where(array('fmediaid' => $mediaId,'fissuedate' => $date,'fstarttime' => $startTime,'fendtime' => $endTime))->count();
			if($ck == 0){
				$cut_id = M('cut_task')->add(array(
											
											'fmediaid' => $mediaId,
											'fissuedate' => $date,
											'fstarttime' => $startTime,
											'fendtime' => $endTime,
											'priority' => $mediaInfo['priority'],
											'fstate' => 0,
											'source_class' => $source_class,
											'time_list' => '[]',
											
											'start_make_time'=>$endTime + 3600*5,
											'cut_wx_id'=> M('tqc_media_permission')->cache(true,300)->where(array('fmediaid'=>$mediaId))->getField('wx_id'),

											'm3u8_url' => $m3u8Url
										));
				if($cut_id) $insertCount += 1;
			}
			
		}
		
		return $insertCount;
	
	}
	
	
	
}