<?php
namespace TaskInput\Controller;

use Think\Exception;

class TbnDataCheckController extends TraditionAdController
{
    public function _initialize()
    {
        if(!A('InputPc/Task','Model')->checkTbnDataCheckAuth()){
            $this->ajaxReturn(array('msg'=>'无权限'));
        }
    }

    private $sampleTableMap = [
        '1' => 'ttvsample',
        '2' => 'tbcsample',
        '3' => 'tpapersample'
    ];

    public function index()
    {
        if (IS_POST) {
            $page = I('page', 1);
            $pageSize = I('pageSize', 10);
            $formData = I('where');
            $where = [];
            if ($formData['fstatus3'] != ''){
                $where['ad.fstatus3'] = ['EQ', $formData['fstatus3']];
            }
            if ($formData['media_class'] != ''){
                $where['ad.fmedia_class'] = ['EQ', $formData['media_class']];
            }
            if ($formData['adName'] != ''){
                $where['ad.fad_name'] = ['EQ', $formData['adName']];
            }
            if ($formData['adClass'] != ''){
                $where['ad.fad_class_code'] = ['EQ', $formData['adClass']];
            }
            if ($formData['customerId'] != ''){
                $where['ad.fcustomer'] = ['EQ', $formData['customerId']];
            }
            // tmedia表
            $mediaWhere = [];
            if ($formData['mediaName'] != ''){
                $mediaWhere['fmedianame'] = ['EQ', $formData['mediaName']];
            }
            // tmediaowner表
            $mediaOwnerWhere = [];
            if ($formData['regionId'] != ''){
                if ($formData['onlyThisLevel'] != 1){
                    $region_id_rtrim = A('Common/System','Model')->get_region_left($formData['regionId']);//地区ID去掉末尾的0
                    $region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
                    $mediaOwnerWhere["LEFT(fregionid, $region_id_strlen)"] = ['EQ', $region_id_rtrim];
                }else{
                    $mediaOwnerWhere['fregionid'] = ['LIKE', '%'.$formData['regionId'].'%'];
                }
                $mediaOwnerSubQuery = M('tmediaowner')
                    ->field('fid')
                    ->where($mediaOwnerWhere)
                    ->buildSql();
                $mediaWhere['fmediaownerid'] = ['EXP', 'IN '. $mediaOwnerSubQuery];//地区搜索条件
            }
            if ($mediaWhere != []){
                $mediaSubQuery = M('tmedia')
                    ->field('fid')
                    ->where($mediaWhere)
                    ->buildSql();
                $where['fmedia_id'] = ['EXP', 'IN '. $mediaSubQuery]; // 媒体搜索条件
            }
            $field = implode(',', array_keys($this->getIndexTableField()));
            $field .= " , tgj_result, tgj_time, tgj_username, tshengj_result, tshengj_time, tshengj_username, tshij_result, tshij_time, tshij_username,tquj_result, tquj_time, tquj_username, check.id";
            $table = M('tbn_illegal_ad')
                // ->field($field)
                ->alias('ad')
                ->join('tbn_data_check as `check` on ad.fid = check.fillegal_ad_id')
                ->where($where)
                ->order('check.tcheck_createtime desc')
                ->page($page, $pageSize)
                ->select();
            $mediaMap = [
                '1' => '电视',
                '2' => '广播',
                '3' => '报纸',
            ];
            foreach ($table as $key => $value) {
                $table[$key]['fad_class_code'] = M('tadclass')->cache(true)->where(['fcode' => ['EQ', $value['fad_class_code']]])->getField('ffullname');
                $table[$key]['fmedia_id'] = M('tmedia')->cache(true)->where(['fid' => ['EQ', $value['fmedia_id']]])->getField('fmedianame');
                $table[$key]['fmedia_class'] = $mediaMap[$value['fmedia_class']];
                $table[$key]['fcustomer'] = M('customer')->cache(60)->where(['cr_num' => ['EQ', $value['fcustomer']]])->getField('cr_name');
            }
            $total = M('tbn_illegal_ad')
                ->alias('ad')
                ->join('tbn_data_check as `check` on ad.fid = check.fillegal_ad_id')
                ->where($where)
                ->count();
            $this->ajaxReturn(compact('table', 'total'));
        } else {
            $tableField = json_encode($this->getIndexTableField2());
            $stateMap = json_encode($this->getfstate3Map2());
            $regionTree = json_encode($this->getRegionTree());
            $customerArr = json_encode($this->getCustomerArr());
            $adClassArr = json_encode($this->getAdClassArr());
            $adClassTree = json_encode($this->getAdClassTree());
            $illegalArr = json_encode($this->getIllegalArr());
            $illegalTypeArr = M('tillegaltype')->field('fcode value, fillegaltype text')->cache(true)->where(['fstate' => ['EQ', 1]])->select();
            $illegalTypeArr = json_encode($illegalTypeArr);
            $this->assign(compact('stateMap', 'tableField', 'adClassArr', 'regionTree', 'adClassTree', 'illegalArr', 'illegalTypeArr','customerArr'));
            $this->display();
        }
    }

    public function index2()
    {
        if (1==1) {
            $page = I('page', 1);
            $pageSize = I('pageSize', 10);
            $formData = I('where');
            $where_tia = [];

            if(!empty($formData['mediaName'])){
                $where_tia['f.fmedianame'] = ['like','%'.$formData['mediaName'].'%'];
            }
            if(!empty($formData['adClass'])){
                $where_tia['c.fad_class_code'] = ['EQ', $formData['adClass']];
            }
            if(!empty($formData['adName'])){
                $where_tia['b.fadname'] = ['like', '%'.$formData['adName'].'%'];
            }
            if ($formData['fstatus3'] != ''){
                $where_tia['a.tcheck_status'] = ['EQ', $formData['fstatus3']];
            }else{
                $where_tia['a.tcheck_status'] = ['in', [10,30,40]];
            }
            if (!empty($formData['media_class'])){
                $mclass = $formData['media_class']<10?'0'.$formData['media_class']:$formData['media_class'];
                $where_tia['left(f.fmediaclassid,2)'] = ['EQ', $mclass];
            }

            if($formData['media_class']==2){
                $total = M('fj_data_check')
                    ->alias('a')
                    ->join('tbcsample g on g.fid=a.fsample_id')
                    ->join('tad b on g.fadid = b.fadid and b.fadid<>0 ')//广告
                    ->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
                    ->join('tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id')
                    ->where($where_tia)
                    ->count();
                $table = M('fj_data_check')
                    ->alias('a')
                    ->field('a.tcheck_status,a.fid,a.fid id,a.fsample_id,"广播" as fmedia_class,c.ffullname as fad_class_code,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedia_id,g.fillegalcontent,a.tshengj_result,a.tshengj_time,a.tshengj_username,a.tshengj_trename,a.tshij_result,a.tshij_time,a.tshij_username,a.tshij_trename,a.tquj_result,a.tquj_time,a.tquj_username,a.tquj_trename')
                    ->join('tbcsample g on g.fid=a.fsample_id')
                    ->join('tad b on g.fadid = b.fadid  and b.fadid<>0')//广告
                    ->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
                    ->join('tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id')
                    ->where($where_tia)
                    ->order('a.fid desc')
                    ->page($page,$pageSize)
                    ->select();//查询复核数据
            }elseif($formData['media_class']==3){
                $total = M('fj_data_check')
                    ->alias('a')
                    ->join('tpapersample g on g.fpapersampleid=a.fsample_id')
                    ->join('tad b on g.fadid = b.fadid  and b.fadid<>0')//广告
                    ->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
                    ->join('tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id')
                    ->where($where_tia)
                    ->count();
                $table = M('fj_data_check')
                    ->alias('a')
                    ->field('a.tcheck_status,a.fid,a.fid id,a.fsample_id,"报纸" as fmedia_class,c.ffullname as fad_class_code,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedia_id,g.fillegalcontent,a.tshengj_result,a.tshengj_time,a.tshengj_username,a.tshengj_trename,a.tshij_result,a.tshij_time,a.tshij_username,a.tshij_trename,a.tquj_result,a.tquj_time,a.tquj_username,a.tquj_trename')
                    ->join('tpapersample g on g.fpapersampleid=a.fsample_id')
                    ->join('tad b on g.fadid = b.fadid  and b.fadid<>0')//广告
                    ->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
                    ->join('tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id')
                    ->where($where_tia)
                    ->order('a.fid desc')
                    ->page($page,$pageSize)
                    ->select();//查询复核数据
            }else{
                $total = M('fj_data_check')
                    ->alias('a')
                    ->join('ttvsample g on g.fid=a.fsample_id')
                    ->join('tad b on g.fadid = b.fadid and b.fadid<>0 ')//广告
                    ->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
                    ->join('tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id')
                    ->where($where_tia)
                    ->count();
                $table = M('fj_data_check')
                    ->alias('a')
                    ->field('a.tcheck_status,a.fid,a.fid id,a.fsample_id,"电视" as fmedia_class,c.ffullname as fad_class_code,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedia_id,g.fillegalcontent,a.tshengj_result,a.tshengj_time,a.tshengj_username,a.tshengj_trename,a.tshij_result,a.tshij_time,a.tshij_username,a.tshij_trename,a.tquj_result,a.tquj_time,a.tquj_username,a.tquj_trename')
                    ->join('ttvsample g on g.fid=a.fsample_id')
                    ->join('tad b on g.fadid = b.fadid and b.fadid<>0 ')//广告
                    ->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
                    ->join('tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id')
                    ->where($where_tia)
                    ->order('a.fid desc')
                    ->page($page,$pageSize)
                    ->select();//查询复核数据
            }
            $this->ajaxReturn(compact('table', 'total'));
        } else {
            $tableField = json_encode($this->getIndexTableField2());
            $stateMap = json_encode($this->getfstate3Map2());
            $adClassArr = json_encode($this->getAdClassArr());
            $adClassTree = json_encode($this->getAdClassTree());
            $illegalArr = json_encode($this->getIllegalArr());
            $illegalTypeArr = M('tillegaltype')->field('fcode value, fillegaltype text')->cache(true)->where(['fstate' => ['EQ', 1]])->select();
            $illegalTypeArr = json_encode($illegalTypeArr);
            $this->assign(compact('stateMap', 'tableField', 'adClassArr', 'adClassTree', 'illegalArr', 'illegalTypeArr'));
            $this->display();
        }
    }

    private function getIndexTableField()
    {
        $tableFieldArr = [
            'check.tcheck_createtime' => '申请复核时间',
            'ad.fad_name' => '广告名称',
            'ad.fad_class_code' => '广告类别',
            'ad.fmedia_id' => '媒体',
            'ad.fmedia_class' => '媒介',
            'ad.fcustomer' => '平台',
        ];
        return $tableFieldArr;
    }

    private function getIndexTableField2()
    {
        $arr = $this->getIndexTableField();
        $res = [];
        foreach ($arr as $key => $value) {
            $res[] = [
                'value' => explode('.',$key)[1],
                'label' => $value,
            ];
        }
        return $res;
    }


    private function getfstate3Map1()
    {
        return [
            '0' => '未申请',
            '10' => '复核申请',
            '20' => '取消申请',
            '30' => '复核成功',
            '40' => '复核通过',
            '50' => '复核不通过',
        ];
    }

    private function getfstate3Map2()
    {
        $arr = $this->getfstate3Map1();
        $res = [];
        foreach ($arr as $key => $value) {
            $res[] = [
                'value' => $key,
                'label' => $value,
            ];
        }
        return $res;
    }

    public function getAttachList($id, $lvl)
    {
        $where = [
            'fcheck_id' => ['EQ', $id],
            'fupload_trelevel' => ['EQ', $lvl],
        ];
        $res = M('tbn_data_check_attach')
            ->where($where)
            ->select();
        $this->ajaxReturn($res);
    }

    public function getAttachList2($id, $lvl)
    {
        $where = [
            'fcheck_id' => ['EQ', $id],
            'fstate' => ['EQ', 0],
            'fupload_trelevel' => ['EQ', $lvl],
        ];
        $res = M('fj_data_check_attach')
            ->field('fattach,fattach_url')
            ->where($where)
            ->select();
        $this->ajaxReturn($res);
    }

    public function getInfo($id)
    {
        $illegalAd = M('tbn_illegal_ad')
            ->find($id);
        $sample = M($this->sampleTableMap[$illegalAd['fmedia_class']])
            ->find($illegalAd['fsample_id']);
        if (!$sample){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '找不到样本'
            ]);
        }
        if ($illegalAd['fmedia_class'] == 3){
            $sample['fid'] = $sample['fpapersampleid'];
        }
        $data = [];
        $taskData = $sample;
        // 查找相关的记录, 组装成最终数据
        $recordList = M('tbn_illegal_ad_record')
            ->where([
                'illegal_ad_id' => ['EQ', $id]
            ])
            ->select();
        if ($recordList){
            // 将样本信息更新到记录上的最新状态
            foreach ($recordList as $item) {
                foreach ($item as $key => $value) {
                    if (!$value && $value !== '0'){
                        unset($item[$key]);
                    }
                }
                $taskData = array_merge($taskData, $item);
            }
        }
        // 补全广告信息
        $data['ad'] = M('tad')->field('fadname, fbrand, fadclasscode, fadclasscode_v2, fadowner')->find($taskData['fadid']);
        $data['adOwner'] = M('tadowner')->field('fname')->find($data['ad']['fadowner']);
        $taskData['adCateText1'] = M('tadclass')->where(['fcode' => ['EQ', $data['ad']['fadclasscode']]])->cache(true)->getField('ffullname');
        $taskData['adCateText2'] = M('hz_ad_class')->where(['fcode' => ['EQ', $data['ad']['fadclasscode_v2']]])->cache(true)->getField('ffullname');
        $taskData['adCateText2'] = $taskData['adCateText2'] ? $taskData['adCateText2'] : '';
        foreach ($data as $key => $value) {
            if ($value){
                $taskData = array_merge($taskData, $value);
            }
        }
        // 查找已选择的违法表现
        if ($taskData['fexpressioncodes']){
            $taskData['checkedIllegalArr'] = M('tillegal')->field('fcode, fconfirmation, fexpression, fillegaltype, fpcode, fcode id, fexpression label')->cache(true)->where(['fcode' => ['IN', explode(';',$taskData['fexpressioncodes'])]])->select();
        }
        if ($illegalAd['fmedia_class'] == 3){
            $taskData['paperUrl'] = M('tpapersource')->where(['fid' => ['EQ', $taskData['sourceid']]])->getField('furl');
        }
        $taskData['source_path'] = $illegalAd['favifilename'] ? $illegalAd['favifilename'] : $illegalAd['fjpgfilename'];
        $taskData['media_class'] = $illegalAd['fmedia_class'];
        $taskData['media_name'] = M('tmedia')->cache(60)->where([
            'fid' => ['EQ', $illegalAd['fmedia_id']]
        ])->getField('fmedianame');
        $taskData['illegalAdId'] = $id;
        $this->ajaxReturn([
            'code' => 0,
            'data' => $taskData
        ]);
    }


    public function cutErr($id, $reason)
    {
        $samInfo = M('tbn_illegal_ad')->field('fsample_id, fmedia_class')->find($id);
        $recordData = $this->getRecordData($id, '剪辑错误', [], $samInfo['fmedia_class'], $samInfo['fsample_id']);
        $mediaClassMap = [
            '1' => 'tv',
            '2' => 'bc',
            '3' => 'paper',
        ];
        M()->startTrans();
        $res = A('TaskInput/CutErr', 'Model')->cutErr($mediaClassMap[$samInfo['fmedia_class']], $samInfo['fsample_id'], $reason);
        $recordUpdate = M('tbn_illegal_ad_record')->add($recordData);
        if ($res && $recordUpdate){
            M()->commit();
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '提交成功',
            ]);
        }else{
            M()->rollback();
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '提交失败',
            ]);
        }
    }

    public function submitAdInputTask()
    {
        $wx_id = session('wx_id');
        $formData = I('post.data');
        $taskInputTableField = [
            'fspokesman',
            'fversion',
            'is_long_ad',
            'fadclasscode',
            'fadclasscode_v2',
            'fname',
            'fadname',
            'media_class',
            'fbrand',
        ];
        $taskInputData = [];
        $requiredField = [
            'fadclasscode',
            'fadclasscode_v2',
            'fname',
            'fadname',
            'fbrand',
        ];
        if ($formData['fadname'] == '社会公益'){
            $adInfo = M('tad')->where(['fadname' => ['EQ', '社会公益']])->cache(true, 60)->find();
            $formData['fadclasscode'] = $adInfo['fadclasscode'];
            $formData['fadclasscode_v2'] = $adInfo['fadclasscode_v2'];
            $formData['fname'] = '广告主不详';
            $formData['fbrand'] = $adInfo['fbrand'];
        }
        foreach ($taskInputTableField as $key => $value) {
            if (isset($formData[$value])){
                $taskInputData[$value] = $formData[$value];
            }elseif (in_array($value, $requiredField) && $formData['fadname'] !== '社会公益'){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '有必填字段没有填写, 请重新填写',
                    'v' => $value
                ]);
            }
        }
        $taskInputData['fadownername'] = $taskInputData['fname'];
        $alias = M('ad_input_user')->where(['wx_id' => ['EQ', $wx_id]])->cache(60)->getField('alias');
        $sampleData = $taskInputData;
        M()->startTrans();
        $sampleData['fadid'] = A('Common/Ad','Model')->getAdId($taskInputData['fadname'],$taskInputData['fbrand'],$taskInputData['fadclasscode'],$taskInputData['fname'],$alias, $taskInputData['fadclasscode_v2']);
        $sampleData['finputuser'] = $alias;
        $sampleData['fmodifier'] = $alias;
        $sampleData['fmodifytime'] = date('Y-m-d H:i:s');
        $sampleData['need_sync'] = 0;


        if ($formData['media_class'] != 3){
            $where = [
                'fid' => ['EQ', $formData['fid']],
            ];
        }else{
            $where = [
                'fpapersampleid' => ['EQ', $formData['fid']],
            ];
        }
        $sampleUpdate = M($this->sampleTableMap[$formData['media_class']])
            ->where($where)
            ->save($sampleData);
        $illegalAdInfo = M('tbn_illegal_ad')
            ->find($formData['illegalAdId']);
        $recordData = $this->getRecordData($formData['illegalAdId'], '修改广告信息', $sampleData, $illegalAdInfo['fmedia_class'], $illegalAdInfo['fsample_id']);
        $recordUpdate = M('tbn_illegal_ad_record')
            ->add($recordData);

        if ($sampleUpdate && $recordUpdate){
            M()->commit();
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '提交成功'
            ]);
        }else{
            M()->rollback();
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '提交失败'
            ]);
        }
    }

    public function submitJudgeTask()
    {
        $wx_id = session('wx_id');
        $formData = I('post.data');
        $taskInputTableField = [
            'fillegaltypecode',
            'fillegalcontent',
            'fexpressioncodes',
            'fmanuno',
            'fadmanuno',
            'fapprno',
            'fadapprno',
            'fent',
            'fadent' ,
            'media_class',
            'fentzone',
        ];
        $taskInputData = [];
        $requiredField = [
            'fillegaltypecode',
        ];
        foreach ($taskInputTableField as $key => $value) {
            if (isset($formData[$value])){
                $taskInputData[$value] = $formData[$value];
            }elseif (in_array($value, $requiredField)){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '有必填字段没有填写, 请重新填写',
                    'v' => $value
                ]);
            }
        }
        $expressionCodes = explode(';', $taskInputData['fexpressioncodes']);
        $expressionsAndConfirmation = M('tillegal')->field('fexpression, fconfirmation')->cache(true)->where(['fcode' => ['IN', $expressionCodes]])->select();
        $taskInputData['fexpressions'] = '';
        $taskInputData['fconfirmations'] = '';
        foreach ($expressionsAndConfirmation as $item) {
            $taskInputData['fexpressions'] .= $item['fexpression'] . ' ;; ' ;
            $taskInputData['fconfirmations'] .= $item['fconfirmation'] . ' ;; ' ;
        }
        $sampleData = $taskInputData;
        $alias = M('ad_input_user')->where(['wx_id' => ['EQ', $wx_id]])->cache(60)->getField('alias');
        $sampleData['need_sync'] = 0;
        $sampleData['finspectuser'] = $alias;
        $sampleData['fmodifier'] = $alias;
        $sampleData['fmodifytime'] = date('Y-m-d H:i:s');
        M()->startTrans();
        if ($formData['media_class'] != 3){
            $where = [
                'fid' => ['EQ', $formData['fid']],
            ];
        }else{
            $where = [
                'fpapersampleid' => ['EQ', $formData['fid']],
            ];
        }
        $sampleUpdate = M($this->sampleTableMap[$formData['media_class']])
            ->where($where)
            ->save($sampleData);
        $illegalAdInfo = M('tbn_illegal_ad')
            ->find($formData['illegalAdId']);
        $recordData = $this->getRecordData($formData['illegalAdId'], '修改违法判定', $sampleData, $illegalAdInfo['fmedia_class'], $illegalAdInfo['fsample_id']);
        $recordUpdate = M('tbn_illegal_ad_record')
            ->add($recordData);
        if ($sampleUpdate && $recordUpdate){
            M()->commit();
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '提交成功'
            ]);
        }else{
            M()->rollback();
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '提交失败'
            ]);
        }
    }

    //违法广告复审
    public function passCheck($id,$reson)
    {
        M()->startTrans();
        $illegalAdInfo = M('tbn_illegal_ad')
            ->find($id);
        $recordData = $this->getRecordData($id, '通过复核', [], $illegalAdInfo['fmedia_class'], $illegalAdInfo['fsample_id']);
        $recordUpdate = M('tbn_illegal_ad_record')
            ->add($recordData);
        $res = M('tbn_illegal_ad')
            ->where([
                'fid' => ['EQ', $id],
                'fstatus3' => ['EQ', 30],
            ])
            ->save([
                'fstatus3' => 40,
                'fh_reson' => $reson,
            ]);
        if ($res && $recordUpdate){
            M()->commit();
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '提交成功'
            ]);
        }else{
            M()->rollback();
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '提交失败'
            ]);
        }
    }

    //样本复审
    public function passCheck2($id,$reson)
    {
        M()->startTrans();
        $res = M('fj_data_check')
            ->where([
                'fid' => ['EQ', $id],
                'tcheck_status' => ['EQ', 10],
            ])
            ->save([
                'tcheck_status' => 30,
                'tcheck_fhreson' => $reson,
            ]);
        if ($res){
            M()->commit();
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '提交成功'
            ]);
        }else{
            M()->rollback();
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '提交失败'
            ]);
        }
    }

    //违法广告复审
    public function rejectCheck($id,$reson)
    {
        M()->startTrans();
        $illegalAdInfo = M('tbn_illegal_ad')
            ->find($id);
        $recordData = $this->getRecordData($id, '不通过复核', [], $illegalAdInfo['fmedia_class'], $illegalAdInfo['fsample_id']);
        $recordUpdate = M('tbn_illegal_ad_record')
            ->add($recordData);
        $res = M('tbn_illegal_ad')
            ->where([
                'fid' => ['EQ', $id],
                'fstatus3' => ['EQ', 30],
            ])
            ->save([
                'fstatus3' => 50,
                'fstatus' => 0,
                'fh_reson' => $reson,
            ]);
        if ($res && $recordUpdate){
            M()->commit();
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '提交成功'
            ]);
        }else{
            M()->rollback();
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '提交失败'
            ]);
        }
    }

    //样本复审
    public function rejectCheck2($id,$reson)
    {
        M()->startTrans();
        $illegalAdInfo = M('fj_data_check')
            ->find($id);
        $res = M('fj_data_check')
            ->where([
                'fid' => ['EQ', $id],
                'tcheck_status' => ['EQ', 10],
            ])
            ->save([
                'tcheck_status' => 40,
                'tcheck_fhreson' => $reson,
            ]);
        if ($res){
            M()->commit();
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '提交成功'
            ]);
        }else{
            M()->rollback();
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '提交失败'
            ]);
        }
    }

    private function getRecordData($illegalAdId, $remark, $data, $mediaClass, $samId)
    {
        $data['wx_id'] = session('wx_id');
        $data['update_time'] = date('Y-m-d H:i:s');
        $data['remark'] = $remark;
        $data['illegal_ad_id'] = $illegalAdId;
        $data['media_class'] = $mediaClass;
        $data['sam_id'] = $samId;
        return $data;
    }

    public function getfhtask(){
        $data['data1'] = M('tbn_illegal_ad')
            ->alias('ad')
            ->join('tbn_data_check as `check` on ad.fid = check.fillegal_ad_id')
            ->where('fstatus3 = 30')
            ->count();

        $where_tia['a.tcheck_status'] = 10;
        $total1 = M('fj_data_check')
            ->alias('a')
            ->join('ttvsample g on g.fid=a.fsample_id')
            ->join('tad b on g.fadid = b.fadid and b.fadid<>0 ')//广告
            ->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
            ->join('tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id')
            ->where($where_tia)
            ->count();
        $total2 = M('fj_data_check')
            ->alias('a')
            ->join('tbcsample g on g.fid=a.fsample_id')
            ->join('tad b on g.fadid = b.fadid and b.fadid<>0 ')//广告
            ->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
            ->join('tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id')
            ->where($where_tia)
            ->count();
        $total3 = M('fj_data_check')
            ->alias('a')
            ->join('tpapersample g on g.fpapersampleid=a.fsample_id')
            ->join('tad b on g.fadid = b.fadid  and b.fadid<>0')//广告
            ->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
            ->join('tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id')
            ->where($where_tia)
            ->count();

        $data['data2'] = $total1 + $total2 + $total3;

        $this->ajaxReturn([
                'code' => 0,
                'msg' => '提交失败',
                'data' => $data
            ]);

    }

}