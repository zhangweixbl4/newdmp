<?php
namespace Api\Controller;
use Think\Controller;

class IssueAnalysisController extends Controller {

    public $oParam;//用户信息

    public function _initialize() {
        header("Content-type: text/html; charset=utf-8");
        
        $token = $_SERVER['HTTP_TOKEN'];
        $token = '3d0e2f4f0e7e483eb62e1ddd5dbd34bb';
        $action = '/api-system/personManage/checkToken/'.$token;
        $micData = accessService($action,false,'GET');
        if($micData['code'] == 200){
          session('person',$micData['data']);
        }
    }

    /*
    * by zw
    * 发布数据列表
    */
    public function issueDataList(){
        $getData = $this->oParam;
        $outtype = $getData['outtype']?$getData['outtype']:0;//导出类型
        if(empty($outtype)){
            $pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
            $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
        }else{
            $pageIndex = 1;
            $pageSize = 9999;
        }

        $limitIndex = ($pageIndex-1)*$pageSize;
        $mediaId = $getData['mediaId'];//媒体ID
        $issueDate = $getData['issueDate'];//任务日期，数组
        $dataType = $getData['dataType']?$getData['dataType']:1;//数据类别（传值，1 全部发布广告，2 播出广告，3 新广告及违法广告）
        $adType = $getData['adType']?$getData['adType']:-1;//节目类型，（传值，1 广告，2 节目，-1 全部）
        $orderType = $getData['orderType'];//排序方式，（传值，0默认时间，1 广告名，2 节目类别）
        $isNullTime = $getData['isNullTime']?$getData['isNullTime']:1;//是否含空时段（1包含，0不含）

        $mediaId = 11000010002365;
        $issueDate = ['2018-11-20 00:00:00','2018-11-20 23:59:59'];

        //任务详情
        $where_mie['b.fid'] = $mediaId;
        $where_mie['b.fstate'] = 1;
        $do_mie = M('tmedia')
            ->alias('b')
            ->field('b.fmediaclassid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')
            ->where($where_mie)
            ->find();

        if(empty($do_mie)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'媒体不存在'));
        }

        //根据媒体类型获取相关表
        $mclass = substr($do_mie['fmediaclassid'],0,2);
        if($mclass == '01'){
            $tb_str = 'tv';
        }elseif($mclass == '02'){
            $tb_str = 'bc';
        }elseif($mclass == '03' || $mclass == '04'){
            $tb_str = 'paper';
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'暂不支持该媒体类型'));
        }

        switch ($orderType) {
            case 1:
                if($mclass == '01' || $mclass == '02'){
                    $order = 'a.fstarttime desc';
                }else{
                    $order = 'a.fissuedate desc';
                }
                break;
            case 10:
                $order = 'CONVERT(c.fadname using gbk)  asc';
                break;
            case 11:
                $order = 'CONVERT(c.fadname using gbk) desc';
                break;
            case 20:
                $order = 'c.fadclasscode asc';
                break;
            case 21:
                $order = 'c.fadclasscode desc';
                break;
            case 30:
                $order = 'a.flength asc';
                break;
            case 31:
                $order = 'a.flength desc';
                break;
            case 40:
                $order = 'b.fillegaltypecode asc';
                break;
            case 41:
                $order = 'b.fillegaltypecode desc';
                break;
            case 50:
                $order = 'CONVERT(a.fpage using gbk) asc';
                break;
            case 51:
                $order = 'CONVERT(a.fpage using gbk) desc';
                break;
            case 60:
                $order = 'CONVERT(a.fpagetype using gbk) asc';
                break;
            case 61:
                $order = 'CONVERT(a.fpagetype using gbk) desc';
                break;
            case 70:
                $order = 'a.fquantity asc';
                break;
            case 71:
                $order = 'a.fquantity desc';
                break;
            case 80:
                $order = 'CONVERT(a.fissuetype using gbk) asc';
                break;
            case 81:
                $order = 'CONVERT(a.fissuetype using gbk) desc';
                break;
            default:
                if($mclass == '01' || $mclass == '02'){
                    $order = 'a.fstarttime asc';
                }else{
                    $order = 'a.fissuedate asc';
                }
                break;
        }

        //需要读取的字段
        $fields = '1 adType,a.f'.$tb_str.'issueid issueid,a.fissuedate,a.f'.$tb_str.'sampleid fsampleid,b.fadid,b.fillegaltypecode,b.fillegalcontent,b.fexpressioncodes,b.fapprno,b.fmanuno,b.fspokesman,b.fexpressions,b.fversion,c.fadowner,c.fadname,c.fbrand,c.fadclasscode,d.ffullname fadclassname,e.fname fadownername,b.fissuedate sampleissuedate,b.fcreatetime samplecreatetime';
        if($mclass == '03' || $mclass == '04'){
            $fields2 = ',a.fpage,a.fpagetype,a.fsize,b.fjpgfilename,b.sourceid,a.fissuetype,a.fquantity,a.fproportion';
        }else{
            $fields2 = ',a.fendtime,a.flength,b.is_long_ad,a.fstarttime,b.favifilename';
        }

        if($dataType == 1){
            $count1 = 0;
            $count2 = 0;
            $data1 = [];
            $data2 = [];
            if($adType == -1 || $adType == 1){
                if($mclass=='01' || $mclass == '02'){
                    $where_issue['a.fstarttime'] = ['between',[$issueDate[0],$issueDate[1]]];
                    $limits = "";
                }else{
                    $limits = "$limitIndex,$pageSize";
                }
                $where_issue['a.fissuedate'] = date('Y-m-d',strtotime($issueDate[0]));
                $where_issue['a.fmediaid'] = $mediaId;

                $count1 = M('t'.$tb_str.'issue a')
                    ->join('t'.$tb_str.'sample b on a.f'.$tb_str.'sampleid = b.'.($tb_str == 'tv' || $tb_str == 'bc'?'fid':'fpapersampleid').' and b.fstate in(0,1)')
                    ->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
                    ->join('tadclass d on c.fadclasscode = d.fcode')
                    ->join('tadowner e on c.fadowner = e.fid')
                    ->fetchsql(true)
                    ->where($where_issue)
                    ->count();

                $data1 = M('t'.$tb_str.'issue a')
                    ->field($fields.$fields2)
                    ->join('t'.$tb_str.'sample b on a.f'.$tb_str.'sampleid = b.'.($tb_str == 'tv' || $tb_str == 'bc'?'fid':'fpapersampleid').' and b.fstate in(0,1)')
                    ->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
                    ->join('tadclass d on c.fadclasscode = d.fcode')
                    ->join('tadowner e on c.fadowner = e.fid')
                    ->where($where_issue)
                    ->order($order)
                    ->limit($limits)
                    ->select();
            }
            if(($adType == -1 || $adType == 2) && ($mclass=='01' || $mclass == '02')){
                $where_prog['a.fstart'] = ['between',[$issueDate[0],$issueDate[1]]];
                $where_prog['fissuedate'] = date('Y-m-d',strtotime($issueDate[0]));
                $where_prog['fmediaid'] = $mediaId;
                $where_prog['a.fstate'] = 1;
                $count2 = M('t'.$tb_str.'prog')
                    ->alias('a')
                    ->join('tprogclass d on a.fclasscode = d.fprogclasscode')
                    ->where($where_prog)
                    ->count();

                $data2 = M('t'.$tb_str.'prog')
                    ->alias('a')
                    ->field('2 adType,a.fprogid issueid,a.fissuedate,0 fsampleid,0 fadid,0 fillegaltypecode,"" fillegalcontent,"" fexpressioncodes,"" fapprno,"" fmanuno,"" fspokesman,"" fexpressions,"" fversion,"" fadowner,fcontent fadname,"" fbrand,fclasscode fadclasscode,d.fprogclass fadclassname,"" fadownername,a.fend fendtime,(unix_timestamp(a.fend)-unix_timestamp(a.fstart)) flength,0 is_long_ad,a.fstart fstarttime,a.fsamplefilename favifilename')
                    ->join('tprogclass d on a.fclasscode = d.fprogclasscode')
                    ->where($where_prog)
                    ->select();
            }
            $count = $count1+$count2;
            $data = array_merge($data1,$data2);
        }elseif($dataType == 2){
            $where_astr = ' fmediaid = "'.$mediaId.'" and fissuedate = "'.date('Y-m-d',strtotime($issueDate[0])).'"';

            $count = M('t'.$tb_str.'sample')
                ->alias('b')
                ->field($fields.$fields2)
                ->join('(select * from t'.$tb_str.'issue where '.$where_astr.' group by f'.$tb_str.'sampleid) a on a.f'.$tb_str.'sampleid = b.'.($tb_str == 'tv' || $tb_str == 'bc'?'fid':'fpapersampleid').' and b.fstate in(0,1)')
                ->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
                ->join('tadclass d on c.fadclasscode = d.fcode')
                ->join('tadowner e on c.fadowner = e.fid')
                ->where($where_issue)
                ->count();

            $data = M('t'.$tb_str.'sample')
                ->alias('b')
                ->field($fields.$fields2)
                ->join('(select * from  t'.$tb_str.'issue where '.$where_astr.' group by f'.$tb_str.'sampleid) a on a.f'.$tb_str.'sampleid = b.'.($tb_str == 'tv' || $tb_str == 'bc'?'fid':'fpapersampleid').' and b.fstate in(0,1)')
                ->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
                ->join('tadclass d on c.fadclasscode = d.fcode')
                ->join('tadowner e on c.fadowner = e.fid')
                ->where($where_issue)
                ->order($order)
                ->limit($limitIndex,$pageSize)
                ->select();
        }elseif($dataType == 3){
            $where_astr = 'fmediaid = "'.$mediaId.'" and fissuedate = "'.date('Y-m-d',strtotime($issueDate[0])).'"';

            $where_issue['a.fissuedate'] = date('Y-m-d',strtotime($issueDate[0]));
            $where_issue['a.fmediaid'] = $mediaId;
            if($mclass == '01' || $mclass == '02'){
                $where_issue['_string'] = 'b.fissuedate = "'.date('Y-m-d',strtotime($issueDate[0])).'" or b.fillegaltypecode > 0';
            }else{
                $where_issue['_string'] = 'b.fillegaltypecode > 0';
            }

            $count = M('t'.$tb_str.'sample')
                ->alias('b')
                ->field($fields.$fields2)
                ->join('(select * from  t'.$tb_str.'issue where '.$where_astr.' group by fsampleid) a on a.f'.$tb_str.'sampleid = b.'.($tb_str == 'tv' || $tb_str == 'bc'?'fid':'fpapersampleid').' and b.fstate in(0,1)')
                ->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
                ->join('tadclass d on c.fadclasscode = d.fcode')
                ->join('tadowner e on c.fadowner = e.fid')
                ->where($where_issue)
                ->count();

            $data = M('t'.$tb_str.'sample')
                ->alias('b')
                ->field($fields.$fields2)
                ->join('(select * from  t'.$tb_str.'issue where '.$where_astr.' group by fsampleid) a on a.f'.$tb_str.'sampleid = b.'.($tb_str == 'tv' || $tb_str == 'bc'?'fid':'fpapersampleid').' and b.fstate in(0,1)')
                ->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
                ->join('tadclass d on c.fadclasscode = d.fcode')
                ->join('tadowner e on c.fadowner = e.fid')
                ->where($where_issue)
                ->order($order)
                ->limit($limitIndex,$pageSize)
                ->select();
        }

        if(!empty($outtype)){
            $title = $do_mie['fmedianame'].'  '.date('Y-m-d',strtotime($issueDate[0]));
            if(date('H:i:s',strtotime($issueDate[0])) <> '00:00:00' || date('H:i:s',strtotime($issueDate[1])) <> '23:59:59'){
                $title .= '（'.date('H:i:s',strtotime($issueDate[0])).'~'.date('H:i:s',strtotime($issueDate[1])).'）';
            }
            if($adType == -1){
                $title .= '串播单';
            }elseif($adType == 1){
                if($dataType == 1){
                    $title .= '广告清单';
                }elseif($dataType == 2){
                    $title .= '播出广告';
                }elseif($dataType == 2){
                    $title .= '新广告及违法广告';
                }
            }elseif($adType == 1){
                $title .= '节目清单';
            }
        }

        if($dataType == 1 && ($mclass == '01' || $mclass == '02')){
            if($adType == -1){
                $resultdata = $this->adListOrder($data1,$issueDate,$limitIndex,$pageSize,$data2,0,$orderType,$isNullTime);
            }else{
                $resultdata = $this->adListOrder($data,$issueDate,$limitIndex,$pageSize,[],0,$orderType,$isNullTime);
            }
            if(!empty($outtype)){
                $endtitle = '共'.$resultdata['count'].'条记录（包含空时段），其中广告'.$resultdata['guanggaocount'].'条，节目'.$resultdata['jiemucount'].'条';
                $this->createXls($resultdata['resultList'],$title,$endtitle);
            }else{
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$resultdata['count'], 'jiemucount'=>$resultdata['jiemucount'], 'guanggaocount'=>$resultdata['guanggaocount'], 'data'=>$resultdata['resultList']?$resultdata['resultList']:[]]);
            }
        }elseif($mclass == '03' || $mclass == '04'){
            $where_psource['fmediaid'] = $mediaId;
            $where_psource['fissuedate'] = date('Y-m-d',strtotime($issueDate[0]));
            $do_psource = M('tpapersource')->field('fid,fpage,fpagetype,furl,fstatus fstate')->where($where_psource)->order('fpage asc')->select();
            $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$data?$data:[],'pagedata'=>$do_psource));
        }else{
            if(!empty($outtype)){
                $this->createXls($data,$title,'');
            }else{
                $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$data?$data:[]));
            }
        }
    }

    /*
    * 生成串播单xls
    * zw
    * $data 数据，$title 表头
    */
    private function createXls($data,$title,$endtitle){
        foreach ($data as $key => $value) {
            if($value['adType'] == 1){
                $data[$key]['fadname'] = '  '.$value['fadname'];
                $data[$key]['adTypeName'] = '广告';
            }elseif($value['adType'] == 2){
                $data[$key]['adTypeName'] = '节目';
            }else{
                $data[$key]['adTypeName'] = '空时段';
            }
            if($value['fillegaltypecode'] > 0){
                $data[$key]['fillegaltype'] = '违法';
            }elseif($value['fillegaltypecode'] <= 0 && $value['adType'] == 1){
                $data[$key]['fillegaltype'] = '不违法';
            }else{
                $data[$key]['fillegaltype'] = '';
            }
        }
        $outdata['datalie'] = [
          '序号'=>'key',
          '广告/节目名称'=>'fadname',
          '广告/节目类别'=>'fadclassname',
          '开始时间'=>'fstarttime',
          '结束时间'=>'fendtime',
          '时长/版面'=>'flength',
          '品牌'=>'fbrand',
          '类型'=>'adTypeName',
          '广告主'=>'fadownername',
          '违法类型'=>'fillegaltype',
          '违法内容'=>'fillegalcontent',
          '违法表现代码'=>'fexpressioncodes',
          '违法表现'=>'fexpressions',
          '版本说明'=>'fversion',
          '生产批准文号'=>'fmanuno',
          '广告批准文号'=>'fapprno',
          '代言人'=>'fspokesman',
        ];
        if($mclass == '01' || $mclass == '02' || $mclass == '03'){
            $outdata['datalie']['素材地址'] = 'ysscurl';
        }elseif($mclass == 'net'){
            $outdata['datalie']['发布页'] = 'fbyurl';
            $outdata['datalie']['落地页'] = 'ldyurl';
        }else{
            $outdata['datalie']['素材地址'] = 'ysscurl';
            $outdata['datalie']['发布页'] = 'fbyurl';
            $outdata['datalie']['落地页'] = 'ldyurl';
        }

        $outdata['title'] = $title;
        $outdata['endtitle'] = $endtitle;
        $outdata['lists'] = $data;
        $ret = A('Api/Function')->outdata_xls($outdata);
        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
    }

    /*
    * by zw
    * 任务数据保存
    */
    public function issueDataSave(){
        $getData = $this->oParam;
        $mediaId = $getData['mediaId'];//媒体ID
        $issueDate = $getData['issueDate'];//任务日期
        // $taskType = $getData['taskType']?$getData['taskType']:'ggjc';//任务类别（传值，ggfl广告分离，ggjc 广告监测，ggfs 广告复审，ggzs广告终审,，bzfl 报纸分离）
        $issueId = $getData['issueid'];//发布记录ID
        $adType = $getData['adType'];//节目类型，1广告，2节目
        $sampleIssueDate = $getData['sampleIssueDate'];//样本发布日期
        $sampleIsCreate = $getData['sampleIsCreate']?$getData['sampleIsCreate']:0;//样本类型，1新样本，0原样本
        $logData['fcontent'] = [];//初始化日志内容

        if(!empty($getData['fstarttime'])){
            $fstarttime = $getData['fstarttime'];
            $fendtime = $getData['fendtime'];
            $getData['fstarttime'] = date('Y-m-d H:i:s',strtotime($getData['fstarttime']));
            $getData['fendtime'] = date('Y-m-d H:i:s',strtotime($getData['fendtime']));
        }

        // $adType = 1;
        // $issueId = 58630102;
        // $mediaId = 11000010002365;
        // $issueDate = ['2018-11-20 00:00:00','2018-11-20 23:59:59'];
        // $getData = [
        //     'fadname'=>'古方红糖',
        //     'fadclasscode'=>'0510',
        //     'fillegaltypecode'=>0,
        //     'fstarttime'=>'2018-11-20 03:08:02',
        //     'fendtime'=>'2018-11-20 03:08:11',
        //     'fadownername'=>'黔西南古方红糖有限责任公司',
        // ];

        if(empty($mediaId) || empty($issueDate) || empty($adType)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
        }

        //广告名
        if(empty($getData['fadname']) || $getData['fadname'] == '未命名广告'){
            $this->ajaxReturn(['code'=>1, 'msg'=>'请正确输入广告名']);
        }
        //广告类别
        if(empty($getData['fadclasscode'])){
            $this->ajaxReturn(['code'=>1, 'msg'=>'请选择广告类别']);
        }
        if($getData['fillegaltypecode'] > 0 && (empty($getData['fillegalcontent']) || empty($getData['fexpressioncodes']) || empty($getData['fexpressions']))){
            $this->ajaxReturn(['code'=>1, 'msg'=>'请选择违法表现']);
        }

        //媒体详情
        $where_mie['b.fid'] = $mediaId;
        $where_mie['b.fstate'] = 1;
        $do_mie = M('tmedia b')
            ->where($where_mie)
            ->find();
        if(empty($do_mie)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'媒体不存在'));
        }
        $mclass = substr($do_mie['fmediaclassid'],0,2);

        if($adType == 1){
            //根据媒体类型获取相关表
            if($mclass == '01'){
                $tb_str = 'tv';
            }elseif($mclass == '02'){
                $tb_str = 'bc';
            }elseif($mclass == '03' || $mclass == '04'){
                $tb_str = 'paper';
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'暂不支持该媒体类型'));
            }

            //发布信息更新
            if($mclass == '01' || $mclass == '02'){
                //判断时长传参
                $length = strtotime($getData['fendtime']) - strtotime($getData['fstarttime']);
                if($length<1 || $length>86400){
                    $this->ajaxReturn(['code'=>1, 'msg'=>'播出时段选择有误']);
                }

                if(!empty($getData['fstarttime']) && !empty($getData['fendtime'])){
                    $save_issue['fstarttime'] = $getData['fstarttime'];
                    $save_issue['fendtime'] = $getData['fendtime'];
                    $save_issue['fstarttime2'] = $fstarttime;
                    $save_issue['fendtime2'] = $fendtime;
                    $save_issue['flength'] = $length;

                    $adList = $this->getAdList($mclass,$mediaId,$issueDate,',a.*',1);//获取节目与广告的合并数据

                    $nowAdData['issueid'] = $issueId;
                    $nowAdData['adtype'] = 1;
                    $nowAdData['fstarttime'] = $getData['fstarttime'];
                    $nowAdData['fendtime'] = $getData['fendtime'];
                    $checkData = $this->overlappingData($nowAdData,$adList,'fstarttime','fendtime');//验证时段是否包含及交叉
                    if(!empty($checkData)){
                        $this->ajaxReturn(array('code'=>1,'msg'=>'您提交的广告与【'.$checkData['title'].'（'.$checkData['fstarttime'].'-'.$checkData['fendtime'].'）】时段冲突'));
                    }
                }
            }else{
                $save_issue['fpage'] = trim($getData['fpage']);
                $save_issue['fsize'] = trim($getData['fsize']);
                $save_issue['fpagetype'] = trim($getData['fpagetype']);
                $save_issue['fproportion'] = trim($getData['fproportion']);
                $save_issue['fissuetype'] = $getData['fissuetype'];
                $save_issue['fquantity'] = $getData['fquantity'];
            }

            //广告主
            $adOwnerId = 0;
            if(!empty($getData['fadownername'])){
                $adOwnerId = M('tadowner')->where(['fname'=>trim($getData['fadownername'])])->getfield('fid');
                if(!isset($adOwnerId)){
                    $add_owner['fname'] = $getData['fadownername'];
                    $add_owner['fregionid'] = $do_mie['media_region_id'];
                    $add_owner['fcreatetime'] = date('Y-m-d H:i:s');
                    $add_owner['fcreator'] = session('person.fname');
                    $adOwnerId = M('tadowner')->add($add_owner);
                }
            }

            //广告名更新，新增的广告用新广告信息，原广告已有的话用原广告的信息
            $adId = 0;
            if(!empty($getData['fadname'])){
                $doAd = M('tad')->where(['fadname'=>trim($getData['fadname']),'fstate'=>['in',[1,2,9]]])->find();
                if(empty($doAd)){
                    $add_ad['fadname'] = trim($getData['fadname']);
                    $add_ad['fbrand'] = trim($getData['fbrand'])?trim($getData['fbrand']):'无突出品牌';
                    $add_ad['fadowner'] = $adOwnerId;
                    $add_ad['fadclasscode'] = $getData['fadclasscode'];
                    $add_ad['fcreator'] = session('person.fname');
                    $add_ad['fcreatetime'] = date('Y-m-d H:i:s');
                    $add_ad['fstate'] = 2;
                    $adId = M('tad')->add($add_ad);
                    $doAd = $add_ad;
                    $logData['fcontent']['新广告信息'] = $add_ad;//广告信息日志赋值
                }else{
                    $adId = $doAd['fadid'];
                }
            }

            //报纸版面获取
            $sourceid = 0;
            if(!empty($save_issue['fpage'])){
                $viewPse = M('tpapersource')->field('fid,furl')->where(['fpage'=>trim($save_issue['fpage']),'fmediaid'=>$mediaId,'fissuedate'=>$issueDate])->find();
                if(!empty($viewPse)){
                    $sourceid = $viewPse['fid'];
                    $sourceurl = $viewPse['furl'];
                }
            }

            $oldData['issue'] = M('t'.$tb_str.'issue')->where(['f'.$tb_str.'issueid'=>$issueId])->find();//原发布数据日志
            $oldData['sample'] = M('t'.$tb_str.'sample')->where([($tb_str == 'paper'?'fpapersampleid':'fid')=>$oldData['issue']['fsampleid']])->find();//原样本日志

            if(!empty($sampleIssueDate) && strtotime($sampleIssueDate) == strtotime($oldData['sample']['fissuedate'])){
                $sampleIssueDate = '';
            }
            //样本发布日期修改时的操作
            if((!empty($sampleIssueDate) || !empty($sampleIsCreate)) && ($mclass == '01' || $mclass == '02')){
                $save_sample = $oldData['sample'];
                unset($save_sample[($tb_str == 'paper'?'fpapersampleid':'fid')]);
            }

            //样本更新
            $save_sample['fadid'] = $adId;
            $save_sample['fversion'] = $getData['fversion'];
            $save_sample['fspokesman'] = $getData['fspokesman'];
            $save_sample['fillegaltypecode'] = $getData['fillegaltypecode'];
            if($getData['fillegaltypecode'] > 0){
                $illegalData = $this->sampleillegal($getData['fexpressioncodes']);//获取违法信息

                $save_sample['fexpressioncodes'] = $getData['fexpressioncodes'];
                $save_sample['fexpressions'] = $illegalData['fexpressions'];
                $save_sample['fconfirmations'] = $illegalData['fconfirmations'];
                $save_sample['fillegalcontent'] = $getData['fillegalcontent'];
            }else{
                $save_sample['fexpressioncodes'] = '';
                $save_sample['fexpressions'] = '';
                $save_sample['fconfirmations'] = '';
                $save_sample['fillegalcontent'] = $getData['fillegalcontent'];
            }
            $save_sample['fmanuno'] = $getData['fmanuno'];
            $save_sample['fapprno'] = $getData['fapprno'];
            if($mclass == '01' || $mclass == '02'){
                $save_sample['is_long_ad'] = $getData['is_long_ad'];
            }else{
                if(!empty($getData['fillegaltypecode']) && empty($sourceid)){
                    $this->ajaxReturn(array('code'=>1,'msg'=>'违法广告的版面图片必须上传'));
                }
                if(!empty($sourceurl)){
                    $save_sample['fjpgfilename'] = $sourceurl;
                }
                $save_sample['sourceid'] = $sourceid;
            }

            $save_issue['fmodifier'] = session('person.fname');
            $save_issue['fmodifytime'] = date('Y-m-d H:i:s');

            //发布信息更改对比
            $checkEditData = checkEditData($oldData['issue'],$save_issue,['fstarttime','fendtime','flength','fpage','fsize','fpagetype','fissuetype','fquantity','fproportion']);//获取修改前后差异数据
            if (!empty($checkEditData)) $logData['fcontent']['发布信息'] = $checkEditData;//发布信息日志赋值

            //样本信息更改对比
            if(!empty($sampleIsCreate) && ($mclass == '01' || $mclass == '02')){ //如果样本发布日期调整时，新建样本，原样本的UUID变更
                $save_sample['fmodifier'] = session('person.fname');
                $save_sample['fmodifytime'] = date('Y-m-d H:i:s');
                $save_sample['fcreator'] = session('person.fname');
                $save_sample['fcreatetime'] = date('Y-m-d H:i:s');
                $save_sample['uuid'] = $oldData['sample']['uuid'].'-'.date('YmdHis');
                $save_sample['fissuedate'] = $issueDate;
                $newSampleId = M('t'.$tb_str.'sample')->add($save_sample);
                if(!empty($newSampleId)){
                    $save_issue['fsampleid'] = $newSampleId;
                    $logData['fcontent']['新建样本信息'] = $save_sample;
                }
                $checkEditData = checkEditData($oldData['sample'],$save_sample,['uuid']);//获取修改前后差异数据
            }elseif(!empty($sampleIssueDate) && ($mclass == '01' || $mclass == '02')){ //如果样本发布日期调整时，新建样本，原样本的UUID变更
                $save_sample['fmodifier'] = session('person.fname');
                $save_sample['fmodifytime'] = date('Y-m-d H:i:s');
                $save_sample['fcreator'] = session('person.fname');
                $save_sample['fcreatetime'] = date('Y-m-d H:i:s');
                $save_sample['fissuedate'] = $sampleIssueDate;
                $newSampleId = M('t'.$tb_str.'sample')->add($save_sample);
                if(!empty($newSampleId)){
                    $sampleIsSt = date('Y',strtotime($sampleIssueDate));
                    while($sampleIsSt<=date('Y')){
                        $issueids = M('t'.$tb_str.'issue')->where(['f'.$tb_str.'sampleid'=>$getData['fsampleid'],'fissuedate'=>['between',[$sampleIssueDate,date('Y-m-d')]]])->getfield('f'.$tb_str.'issueid',true);
                        if(!empty($issueids)){
                            M('t'.$tb_str.'issue')->where(['f'.$tb_str.'sampleid'=>$getData['fsampleid'],'fissuedate'=>['between',[$sampleIssueDate,date('Y-m-d')]]])->save(['f'.$tb_str.'sampleid'=>$newSampleId]);
                            $save_sample['issueids'] = $issueids;
                        }
                        $sampleIsSt += 1;
                    }
                    $logData['fcontent']['新建样本信息'] = $save_sample;
                }

                $save_sample = [];
                $save_sample['uuid'] = $oldData['sample']['uuid'].'-'.date('YmdHis');
                $checkEditData = checkEditData($oldData['sample'],$save_sample,['uuid']);//获取修改前后差异数据
            }else{
                $checkEditData = checkEditData($oldData['sample'],$save_sample,['fissuedate','fadid','fversion','fspokesman','fillegaltypecode','fillegalcontent','fexpressioncodes','fexpressions','fmanuno','fapprno','is_long_ad','fjpgfilename']);//获取修改前后差异数据
            }

            //违法判定日志记录
            if(!empty($checkEditData['fillegaltypecode'])){
                if($oldData['sample']['fillegaltypecode'] == -30){
                    $silData['违法类型'] = '待核实';
                }elseif($oldData['sample']['fillegaltypecode'] == 0){
                    $silData['违法类型'] = '不违法';
                }elseif($oldData['sample']['fillegaltypecode'] == 0){
                    $silData['违法类型'] = '违法';
                }

                if($save_sample['fillegaltypecode'] == -30){
                    $silData['违法类型'] .= ' >>调整为>> 待核实';
                }elseif($save_sample['fillegaltypecode'] == 0){
                    $silData['违法类型'] .= ' >>调整为>> 不违法';
                }elseif($save_sample['fillegaltypecode'] == 30){
                    $silData['违法类型'] .= ' >>调整为>> 违法';
                }
            }
            if(!empty($checkEditData['fexpressioncodes'])){
                $silData['违法表现代码'] = $checkEditData['fexpressioncodes'];
            }
            if(!empty($checkEditData['fexpressions'])){
                $silData['违法表现'] = $checkEditData['fexpressions'];
            }
            if(!empty($checkEditData['fillegalcontent'])){
                $silData['违法内容'] = $checkEditData['fillegalcontent'];
            }

            if (!empty($checkEditData)) $logData['fcontent']['样本信息'] = $checkEditData;//样本信息日志赋值
            if(!empty($checkEditData['fadid'])){//广告信息更改对比
                $oldAd = M('tad')->where(['fadid'=>$oldData['sample']['fadid']])->find();
                if(!empty($oldAd)){
                    $checkEditData = checkEditData($oldAd,$doAd,['fadname']);//获取修改前后差异数据
                    if (!empty($checkEditData)) $logData['fcontent']['广告信息'] = $checkEditData;//广告信息日志赋值
                }
            }

            if(!empty($logData['fcontent'])){
                //非新建样本情况下执行
                if(empty($sampleIsCreate)){
                    $do_sample = M('t'.$tb_str.'sample')
                        ->where([($tb_str == 'tv' || $tb_str == 'bc'?'fid':'fpapersampleid')=>$oldData['issue']['fsampleid']])
                        ->save($save_sample);
                }

                $do_issue = M('t'.$tb_str.'issue')
                    ->where(['f'.$tb_str.'issueid'=>$issueId])
                    ->save($save_issue);

                $logData['ftype'] = 2;
                $logData['fmainid'] = $issueId;
                $logData['fcontent'] = json_encode($logData['fcontent'],JSON_UNESCAPED_UNICODE);
                $logData['fmediaid'] = $mediaId;
                $logData['fissuedate'] = $issueDate;
                $logData['fmediatype'] = $mclass;
                $logId = $this->createTaskFlowLog($logData);
                $this->ajaxReturn(array('code'=>0,'msg'=>'已保存','log'=>$logId));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'保存失败，未调整'));
            }
        }else{
            if($mclass == '01'){
                $tb_name = 'ttvprog';
            }elseif($mclass == '02'){
                $tb_name = 'tbcprog';
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'保存失败，媒体类别有误'));
            }
            $oldData = M($tb_name)->where(['fprogid'=>$issueId])->find();//旧数据

            //判断时长传参
            $length = strtotime($getData['fendtime']) - strtotime($getData['fstarttime']);
            if($length<1 || $length>86400){
                $this->ajaxReturn(['code'=>1, 'msg'=>'播出时段选择有误']);
            }

            if(!empty($getData['fstarttime']) && !empty($getData['fendtime'])){
                $add_ad['fstart'] = $getData['fstarttime'];
                $add_ad['fend'] = $getData['fendtime'];
                $add_ad['fduration'] = strtotime($getData['fendtime'])-strtotime($getData['fstarttime']);

                $adList = $this->getAdList($mclass,$mediaId,$issueDate,',a.*',2);//获取节目与广告的合并数据
                $nowAdData['issueid'] = $issueId;
                $nowAdData['adtype'] = 2;
                $nowAdData['fstarttime'] = $getData['fstarttime'];
                $nowAdData['fendtime'] = $getData['fendtime'];
                $checkData = $this->overlappingData($nowAdData,$adList,'fstarttime','fendtime');//验证时段是否包含及交叉
                if(!empty($checkData)){
                    $this->ajaxReturn(array('code'=>1,'msg'=>'您提交的节目与【'.$checkData['title'].'（'.$checkData['fstarttime'].'-'.$checkData['fendtime'].'）】时段冲突'));
                }
            }

            $add_ad['fcontent'] = $getData['fadname'];
            $add_ad['fclasscode'] = $getData['fadclasscode'];
            $add_ad['fmodifier'] = session('person.fname');
            $add_ad['fmodifytime'] = date('Y-m-d H:i:s');

            $checkEditData = checkEditData($oldData,$add_ad,['fstart','fend','fduration','fcontent','fclasscode']);//获取修改前后差异数据
            if (!empty($checkEditData)) $logData['fcontent']['节目信息'] = $checkEditData;

            if(!empty($logData['fcontent'])){
                M($tb_name)->where(['fprogid'=>$issueId])->save($add_ad);

                //日志记录
                $logData['ftype'] = 12;
                $logData['fmainid'] = $issueId;
                $logData['fcontent'] = json_encode($logData['fcontent'],JSON_UNESCAPED_UNICODE);
                $logData['fmediaid'] = $mediaId;
                $logData['fissuedate'] = $issueDate;
                $logData['fmediatype'] = $mclass;
                $logId = $this->createTaskFlowLog($logData);
                $this->ajaxReturn(array('code'=>0,'msg'=>'已保存','log'=>$logId));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'保存失败，未调整'));
            }
        }
    }

    /*
    * by zw
    * 任务数据添加
    */
    public function issueDataAdd(){
        $getData = $this->oParam;
        $mediaId = $getData['mediaId'];//媒体ID
        $issueDate = $getData['issueDate'];//任务日期
        $adType = $getData['adType'];//节目类型，1广告，2节目
        if(!empty($getData['fstarttime'])){
            $fstarttime = $getData['fstarttime'];
            $fendtime = $getData['fendtime'];
            $getData['fstarttime'] = date('Y-m-d H:i:s',strtotime($getData['fstarttime']));
            $getData['fendtime'] = date('Y-m-d H:i:s',strtotime($getData['fendtime']));
        }

        if(empty($mediaId) || empty($issueDate) || empty($adType)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
        }

        //广告名
        if(empty($getData['fadname']) || $getData['fadname'] == '未命名广告'){
            $this->ajaxReturn(['code'=>1, 'msg'=>'请正确输入广告名']);
        }
        //广告类别
        if(empty($getData['fadclasscode'])){
            $this->ajaxReturn(['code'=>1, 'msg'=>'请选择广告类别']);
        }

        //媒体详情
        $where_mie['b.fid'] = $mediaId;
        $where_mie['b.fstate'] = 1;
        $do_mie = M('tmedia b')
            ->where($where_mie)
            ->find();
        if(empty($do_mie)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'媒体不存在'));
        }

        $mclass = substr($do_mie['fmediaclassid'],0,2);

        if($mclass == '03' || $mclass == '04'){
            if(empty(trim($getData['fpage'])) || empty($getData['fsize']) || empty($getData['fissuetype']) || empty($getData['fquantity'])){
                $this->ajaxReturn(['code'=>1, 'msg'=>'报纸必要参数有误']);
            }
        }

        if($adType == 1){
            if($mclass == '01'){
                $tb_str = 'tv';
            }elseif($mclass == '02'){
                $tb_str = 'bc';
            }elseif($mclass == '03' || $mclass == '04'){
                $tb_str = 'paper';
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'暂不支持该媒体类型'));
            }

            //发布信息更新
            if($mclass == '01' || $mclass == '02'){
                //判断时长传参
                $length = strtotime($getData['fendtime']) - strtotime($getData['fstarttime']);
                if($length<1 || $length>86400){
                    $this->ajaxReturn(['code'=>1, 'msg'=>'播出时段选择有误']);
                }

                if(!empty($getData['fstarttime']) && !empty($getData['fendtime'])){
                    $save_issue['fstarttime'] = $getData['fstarttime'];
                    $save_issue['fendtime'] = $getData['fendtime'];
                    $save_issue['fstarttime2'] = $fstarttime;
                    $save_issue['fendtime2'] = $fendtime;
                    $save_issue['flength'] = $length;

                    $adList = $this->getAdList($mclass,$mediaId,$issueDate,',a.*',1);//获取节目与广告的合并数据
                    $nowAdData['issueid'] = 0;
                    $nowAdData['adtype'] = 1;
                    $nowAdData['fstarttime'] = $getData['fstarttime'];
                    $nowAdData['fendtime'] = $getData['fendtime'];
                    $checkData = $this->overlappingData($nowAdData,$adList,'fstarttime','fendtime');//验证时段是否包含及交叉
                    if(!empty($checkData)){
                        $this->ajaxReturn(array('code'=>1,'msg'=>'您提交的广告与【'.$checkData['title'].'（'.$checkData['fstarttime'].'-'.$checkData['fendtime'].'）】时段冲突'));
                    }
                }else{
                    $this->ajaxReturn(array('code'=>1,'msg'=>'请选择播出开始和结束时间'));
                }
            }else{
                $save_issue['fpage'] = trim($getData['fpage']);
                $save_issue['fsize'] = trim($getData['fsize']);
                $save_issue['fpagetype'] = trim($getData['fpagetype']);
                $save_issue['fproportion'] = trim($getData['fproportion']);
                $save_issue['fissuetype'] = $getData['fissuetype'];
                $save_issue['fquantity'] = $getData['fquantity'];
            }

            //广告主
            $adOwnerId = 0;
            if(!empty($getData['fadownername'])){
                $adOwnerId = M('tadowner')->where(['fname'=>trim($getData['fadownername'])])->getfield('fid');
                if(!isset($adOwnerId)){
                    $add_owner['fname'] = trim($getData['fadownername']);
                    $add_owner['fregionid'] = $do_mie['media_region_id'];
                    $add_owner['fcreatetime'] = date('Y-m-d H:i:s');
                    $add_owner['fcreator'] = session('person.fname');
                    $adOwnerId = M('tadowner')->add($add_owner);
                }
            }

            //广告名更新
            $adId = 0;
            if(!empty($getData['fadname'])){
                $add_ad = M('tad')->where(['fadname'=>trim($getData['fadname']),'fstate'=>['in',[1,2,9]]])->find();
                if(empty($add_ad)){
                    $add_ad['fadname'] = trim($getData['fadname']);
                    $add_ad['fbrand'] = trim($getData['fbrand'])?trim($getData['fbrand']):'无突出品牌';
                    $add_ad['fadowner'] = $adOwnerId;
                    $add_ad['fadclasscode'] = $getData['fadclasscode'];
                    $add_ad['fcreator'] = session('person.fname');
                    $add_ad['fcreatetime'] = date('Y-m-d H:i:s');
                    $add_ad['fstate'] = 1;
                    $adId = M('tad')->add($add_ad);
                }else{
                    $adId = $add_ad['fadid'];
                }
            }

            //报纸版面获取
            $sourceid = 0;
            if(!empty($save_issue['fpage'])){
                $viewPse = M('tpapersource')->field('fid,furl')->where(['fpage'=>trim($save_issue['fpage']),'fmediaid'=>$mediaId,'fissuedate'=>$issueDate])->find();
                if(!empty($viewPse)){
                    $sourceid = $viewPse['fid'];
                    $sourceurl = $viewPse['furl'];
                }
            }

            //样本更新
            $save_sample['fadid'] = $adId;
            $save_sample['fversion'] = $getData['fversion'];
            $save_sample['fspokesman'] = $getData['fspokesman'];
            $save_sample['fillegaltypecode'] = $getData['fillegaltypecode'];
            if($getData['fillegaltypecode'] > 0){
                $illegalData = $this->sampleillegal($getData['fexpressioncodes']);//获取违法信息

                $save_sample['fexpressioncodes'] = $getData['fexpressioncodes'];
                $save_sample['fexpressions'] = $illegalData['fexpressions'];
                $save_sample['fconfirmations'] = $illegalData['fconfirmations'];
                $save_sample['fillegalcontent'] = $getData['fillegalcontent'];
            }else{
                $save_sample['fexpressioncodes'] = '';
                $save_sample['fexpressions'] = '';
                $save_sample['fconfirmations'] = '';
                $save_sample['fillegalcontent'] = $getData['fillegalcontent'];
            }
            $save_sample['fmanuno'] = $getData['fmanuno'];
            $save_sample['fapprno'] = $getData['fapprno'];
            // $fuuid = strtolower(createNoncestr(8).'-'.createNoncestr(4).'-'.createNoncestr(4).'-'.createNoncestr(4).'-'.createNoncestr(12));
            $save_sample['fmediaid'] = $mediaId;
            $save_sample['fstate'] = 1;
            if($mclass == '01' || $mclass == '02'){
                $save_sample['fadlen'] = $length;
                $save_sample['is_long_ad'] = $getData['is_long_ad'];
            }else{
                if($getData['fillegaltypecode'] > 0 && empty($sourceid)){
                    $this->ajaxReturn(array('code'=>1,'msg'=>'违法广告的版面图片必须上传'));
                }
                if(!empty($sourceurl)){
                    $save_sample['fjpgfilename'] = $sourceurl;
                }
                $save_sample['sourceid'] = $sourceid;
            }

            $save_sample['fissuedate'] = $issueDate;
            $save_sample['fcreator'] = session('person.fname');
            $save_sample['fcreatetime'] = date('Y-m-d H:i:s');
            M()->startTrans();
            $sample = M('t'.$tb_str.'sample') ->add($save_sample);
            if($mclass == '01' || $mclass == '02'){
                if(!empty(C('SAMPLEDETECT'))){//识别状态开启时
                    //样本识别
                    $detectPushData['channel'] = $mediaId;
                    $detectPushData['name'] = $add_ad['fadname'];
                    $detectPushData['start'] = strripos($fstarttime,".")?strtotime($fstarttime).substr($fstarttime,strripos($fstarttime,".")):strtotime($fstarttime);
                    $detectPushData['end'] = strripos($fendtime,".")?strtotime($fendtime).substr($fendtime,strripos($fendtime,".")):strtotime($fendtime);
                    $detectPushData['editor_id'] = 'zhejiang_'.session('person.fid');
                    $detectPushData['ad_type'] = $add_ad['fadclasscode'];
                    $detectPushData['ad_brand'] = $add_ad['fbrand'];
                    $detectPushData['ad_owner'] = trim($getData['fadownername']);
                    $detectPushData['ad_in_show'] = false;
                    $detectPushData['ad_speaker'] = $getData['fspokesman'];
                    $detectPushData['ver_info'] = $getData['fversion'];
                    $resDetect = A('Jiance/SampleManage')->sampleDetect($detectPushData,1);
                    if(!empty($resDetect['code'])){
                        M()->rollback();
                        $this->ajaxReturn(array('code'=>1,'msg'=>$resDetect['msg']));
                    }else{
                        M()->commit();
                        M('t'.$tb_str.'sample')->where([($tb_str == 'tv' || $tb_str == 'bc'?'fid':'fpapersampleid')=>$sample])->save(['uuid'=>$resDetect['data'],'fdetectstatus'=>1,'fdetecttime'=>time()]);
                    }
                }else{
                    M()->commit();
                }

                //剪辑任务
                $pushData = [
                    'mediaId'=>$mediaId,
                    'source_name'=>'样本素材生成_'.$mediaId.'_'.$sample.'_'.$getData['fstarttime'].'~'.$getData['fendtime'],
                    'source_type'=>30,
                    'Sstart'=>$getData['fstarttime'],
                    'Send'=>$getData['fendtime'],
                    'tid'=>$sample
                ];
                $res = A('Api/Media')->add_source_make($pushData,1);
            }else{
                M()->commit();
            }
            $newData['sample'] = $save_sample;//日志

            //发布表更新
            $save_issue['fsampleid'] = $sample;
            $save_issue['fmediaid'] = $mediaId;
            $save_issue['fissuedate'] = $issueDate;
            $save_issue['fcreator'] = session('person.fname');
            $do_issue = M('t'.$tb_str.'issue') ->add($save_issue);
            $newData['issue'] = $save_issue;//日志

            $logData['ftype'] = 1;
            $logData['fmainid'] = $do_issue;
            $logData['fcontent'] = json_encode($newData,JSON_UNESCAPED_UNICODE);
        }else{
            if($mclass == '01'){
                $tb_name = 'ttvprog';
            }elseif($mclass == '02'){
                $tb_name = 'tbcprog';
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'添加失败，媒体类别有误'));
            }

            //判断时长传参
            $length = strtotime($getData['fendtime']) - strtotime($getData['fstarttime']);
            if($length<1 || $length>86400){
                $this->ajaxReturn(['code'=>1, 'msg'=>'播出时段选择有误']);
            }

            if(!empty($getData['fstarttime']) && !empty($getData['fendtime'])){
                $add_ad['fstart'] = $getData['fstarttime'];
                $add_ad['fend'] = $getData['fendtime'];
                $add_ad['fduration'] = $length;

                $adList = $this->getAdList($mclass,$mediaId,$issueDate,',a.*',2);//获取节目与广告的合并数据
                $nowAdData['issueid'] = 0;
                $nowAdData['adtype'] = 2;
                $nowAdData['fstarttime'] = $getData['fstarttime'];
                $nowAdData['fendtime'] = $getData['fendtime'];
                $checkData = $this->overlappingData($nowAdData,$adList,'fstarttime','fendtime');//验证时段是否包含及交叉
                if(!empty($checkData)){
                    $this->ajaxReturn(array('code'=>1,'msg'=>'您提交的节目与【'.$checkData['title'].'（'.$checkData['fstarttime'].'-'.$checkData['fendtime'].'）】时段冲突'));
                }
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'请选择播出开始和结束时间'));
            }

            $add_ad['fmediaid'] = $mediaId;
            $add_ad['fissuedate'] = $issueDate;
            $add_ad['fstate'] = 1;
            $add_ad['fcontent'] = $getData['fadname'];
            $add_ad['fclasscode'] = $getData['fadclasscode'];
            $add_ad['fcreator'] = session('person.fname');
            $add_ad['fcreatetime'] = date('Y-m-d H:i:s');
            M($tb_name)->add($add_ad);

            $logData['ftype'] = 11;
            $logData['fmainid'] = 0;
            $logData['fcontent'] = json_encode($add_ad,JSON_UNESCAPED_UNICODE);
        }
        $logData['fmediaid'] = $mediaId;
        $logData['fissuedate'] = $issueDate;
        $logData['fmediatype'] = $mclass;
        $logId = $this->createTaskFlowLog($logData);
        $this->ajaxReturn(array('code'=>0,'msg'=>'添加成功','log'=>$logId));
    }

    /*
    * by zw
    * 发布数据删除
    */
    public function issueDataDel(){
        $getData = $this->oParam;
        $issueId = $getData['issueId'];//发布ID
        $adType = $getData['adType'];//节目类型，1广告，2节目
        $delData = $getData['delData']?$getData['delData']:[];//删除的数据
        $mediaId = $getData['mediaId'];//媒体ID
        $issueDate = $getData['issueDate'];//任务日期

        if(!empty($issueId)){
            $delData[] = [
                'adType'=>$adType,
                'issueId'=>$issueId,
            ];
        }
        if(empty($delData) && !is_array($delData)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
        }

        //媒体详情
        $where_mie['b.fid'] = $mediaId;
        $where_mie['b.fstate'] = 1;
        $do_mie = M('tmedia b')
            ->where($where_mie)
            ->find();
        if(empty($do_mie)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'媒体不存在'));
        }

        $mclass = substr($do_mie['fmediaclassid'],0,2);

        //根据媒体类型获取相关表
        if($mclass == '01'){
            $tb_str = 'tv';
        }elseif($mclass == '02'){
            $tb_str = 'bc';
        }elseif($mclass == '03' || $mclass == '04'){
            $tb_str = 'paper';
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'暂不支持该媒体类型'));
        }

        foreach ($delData as $key => $value) {
            if($value['adType'] == 1){
                $oldData =  M('t'.$tb_str.'issue')->where(['f'.$tb_str.'issueid'=>$value['issueId']])->find();//日志
                $logData['ftype'] = 3;

                $del_issue =  M('t'.$tb_str.'issue')->where(['f'.$tb_str.'issueid'=>$value['issueId']])->save(['fstate'=>-1,'fmodifier'=>session('person.fname'),'fmodifytime'=>date('Y-m-d H:i:s')]);
            }else{
                $oldData =  M('t'.$tb_str.'prog')->where(['fprogid'=>$value['issueId']])->find();//日志
                $logData['ftype'] = 13;

                $del_issue =  M('t'.$tb_str.'prog')->where(['fprogid'=>$value['issueId']])->save(['fstate'=>-1,'fmodifier'=>session('person.fname'),'fmodifytime'=>date('Y-m-d H:i:s')]);
            }
            $logData['fmainid'] = $value['issueId'];
            $logData['fcontent'] = json_encode($oldData,JSON_UNESCAPED_UNICODE);
            $logData['fmediaid'] = $mediaId;
            $logData['fissuedate'] = $issueDate;
            $logData['fmediatype'] = $mclass;
            $logId = $this->createTaskFlowLog($logData);
        }

        if(!empty(count($delData))){
            $this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'删除失败，记录不存在，请刷新后重试'));
        }
    }

    /*
    * 时间偏差调整
    * by zw
    */
    public function mediaIssueTimeEdit(){
        $getData = $this->oParam;
        $mediaId = $getData['mediaId'];//媒体ID
        $issueDate = $getData['issueDate'];//任务日期
        $issueData = $getData['issueData'];//发布数据
        $eType = $getData['eType'] === ''?-1:$getData['eType'];//调整方式，0前进，1后退
        $second = $getData['second'];//偏差秒数
        // $issueDate = '2019-11-1';
        // $mediaId = 11000010002389;
        // $issueData = [
        //  ["issueId"=>328060,"adType"=>1],
        //  ["issueId"=>328061,"adType"=>1],
        //  ["issueId"=>1284,"adType"=>1],
        // ];

        if(empty($second) || $eType == -1 || empty($issueData) || empty($issueDate) || empty($mediaId)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
        }
        if(!empty($eType)){
            $second = - (int)($second);
        }

        //媒体详情
        $where_mie['b.fid'] = $mediaId;
        $where_mie['b.fstate'] = 1;
        $do_mie = M('tmedia b')
            ->where($where_mie)
            ->find();
        if(empty($do_mie)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'媒体不存在'));
        }

        $mclass = substr($do_mie['fmediaclassid'],0,2);
        if($mclass == '01'){
            $tb_str = 'tv';
        }elseif($mclass == '02'){
            $tb_str = 'bc';
        }elseif($mclass == '03' || $mclass == '04'){
            $tb_str = 'paper';
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'暂不支持该媒体类型'));
        }

        $ids = [];
        foreach ($issueData as $key => $value) {
            if($value['adType'] == 1){
                $ids[] = $value['issueId'];
            }
        }
        if(!empty($ids)){
            $where_issue['a.fstate'] = 1;//发布数据必须是有效的
            $where_issue['a.f'.$tb_str.'issueid'] = ['in',$ids];
            $where_issue['a.fmediaid'] = $mediaId;
            $where_issue['a.fissuedate'] = $issueDate;
            $data1 = M('t'.$tb_str.'issue')
                ->alias('a')
                ->field('a.fstarttime,a.fendtime,c.fadname,a.f'.$tb_str.'issueid')
                ->join('t'.$tb_str.'sample b on a.f'.$tb_str.'sampleid = b.'.($tb_str == 'tv' || $tb_str == 'bc'?'fid':'fpapersampleid').' and b.fstate in(0,1)')
                ->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
                ->join('tadclass d on c.fadclasscode = d.fcode')
                ->join('tadowner e on c.fadowner = e.fid')
                ->where($where_issue)
                ->select();

            $where_issue['a.f'.$tb_str.'issueid'] = ['notin',$ids];
            $data2 = M('t'.$tb_str.'issue')
                ->alias('a')
                ->field('a.fstarttime,a.fendtime,c.fadname,a.f'.$tb_str.'issueid')
                ->join('t'.$tb_str.'sample b on a.f'.$tb_str.'sampleid = b.'.($tb_str == 'tv' || $tb_str == 'bc'?'fid':'fpapersampleid').' and b.fstate in(0,1)')
                ->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
                ->join('tadclass d on c.fadclasscode = d.fcode')
                ->join('tadowner e on c.fadowner = e.fid')
                ->where($where_issue)
                ->select();

            $checkData = [];
            foreach ($data1 as $key => $value) {
                $isCheck = 0;
                foreach ($data2 as $key2 => $value2) {
                    $isCheck = isTimeContain((strtotime($value['fstarttime'])+$second),(strtotime($value['fendtime'])+$second),strtotime($value2['fstarttime']),strtotime($value2['fendtime']));
                    if(!empty($isCheck)){
                        break;
                    }
                }
                if(!empty($isCheck)){
                    $checkData[] = $value;
                }
            }

            if(!empty(count($checkData))){
                $this->ajaxReturn(array('code'=>1,'msg'=>'调整失败，请检查调整后的时间是否与前后广告交叉','data'=>$checkData));
            }else{
                $do_save = M()->execute('update t'.$tb_str.'issue'.' set fstarttime = from_unixtime((unix_timestamp(fstarttime)+('.(int)$second.'))),fendtime = from_unixtime((unix_timestamp(fendtime)+('.(int)$second.'))),fmodifier = "时间偏差调整",fmodifytime = now() where fmediaid = '.$mediaId.' and fissuedate = "'.$issueDate.'" and f'.$tb_str.'issueid in('.implode(',', $ids).')');
                $this->ajaxReturn(array('code'=>0,'msg'=>'成功调整'.$do_save.'条'));
            }
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'调整失败，节目不允许批量调整'));
        }

    }

    /*
    * by zw
    * 报纸版面确认
    */
    public function issuePageConfirm(){
        $getData = $this->oParam;
        $fid = $getData['fid'];//ID
        $state = $getData['state']?$getData['state']:0;//更改的状态，0待处理，1处理中，2已处理
        if(empty($fid)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
        }

        $do_pse = M('tpapersource')->where(['fid'=>$fid])->save(['fstatus'=>$state]);
        if(!empty($do_pse)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'更新成功'));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'更新失败'));
        }
    }

    /**
     * 对广告串播单进行重新排序
     * by zw
     */
    private function adListOrder($adData = [], $issueDate = [],$limitIndex = 20,$pageSize = 1,$adData2 = [],$again = 0,$orderType = 0,$isNullTime = 1){
        $adData = pxsf($adData,'fstarttime',false);
        if(!empty($adData)){
            $adList = [];
            foreach($adData as $key=>$row){
                if($key == 0){//第一条
                    $max_endtime = $row['fendtime'];
                    $interval = strtotime($row['fstarttime']) - strtotime($issueDate[0]); //与起始时间间隔大于3秒,则创建一个空时间段
                    if($interval > 3 && !empty($isNullTime)){
                        $adNull = [
                            'adType'        => 0,
                            'fadname'       => '',
                            'fadclasscode'  => 0,
                            'fstarttime'    => $issueDate[0],
                            'fendtime'      => $row['fstarttime'],
                            'flength'       => $interval
                        ];
                        array_push($adList,$adNull);
                    }
                }elseif($key > 0 && $key <= (count($adData)-1)){
                    //两个广告时间间隔大于3秒,则创建一个空时间段
                    if($max_endtime<=$adData[$key-1]['fendtime']){
                        $max_endtime = $adData[$key-1]['fendtime'];
                    }

                    $interval = strtotime($row['fstarttime']) - strtotime($max_endtime);
                    if($interval > 3 && !empty($isNullTime)){
                        $adNull = [
                            'adType'        => 0,
                            'fadname'       => '',
                            'fadclasscode'  => 0,
                            'fstarttime'    => $max_endtime,
                            'fendtime'      => $row['fstarttime'],
                            'flength'       => $interval
                        ];
                        array_push($adList,$adNull);
                    }
                }
                $row['nevtime'] = $max_endtime;
                array_push($adList,$row);
                //最后一条后的空时间段
                if($key == (count($adData)-1)){
                    $interval = strtotime($issueDate[1]) - strtotime($row['fendtime']); //与结束时间间隔大于3秒,则创建一个空时间段
                    if($interval > 3 && !empty($isNullTime)){
                        $adNull = [
                            'adType'        => 0,
                            'fadname'       => '',
                            'fadclasscode'  => 0,
                            'fstarttime'    => $row['fendtime'],
                            'fendtime'      => $issueDate[1],
                            'flength'       => $interval
                        ];
                        array_push($adList,$adNull);
                    }
                }
            }
        }else{
            $adNull = [
                'adType'        => 0,
                'fadname'       => '',
                'fadclasscode'  => 0,
                'fstarttime'    => $issueDate[0],
                'fendtime'      => $issueDate[1],
                'flength'       => $interval
            ];
            if(!empty($isNullTime)){
                array_push($adList,$adNull);
            }
        }


        //如果有节目的话
        if(!empty($adData2)){
            $adData2 = pxsf($adData2,'fendtime',false);
            $addData = [];
            if(!empty($adList)){
                foreach ($adList as $key => $value) {
                    if(!empty($value['adType'])){//非空时段跳出本次循环
                        continue;
                    }else{
                        unset($adList[$key]);
                    }
                    foreach ($adData2 as $key2 => $value2) {
                        $timeContain = timeContain(strtotime($value['fstarttime']),strtotime($value['fendtime']),strtotime($value2['fstarttime']),strtotime($value2['fendtime']));
                        if(!empty($timeContain['use']) && ($timeContain['ed']-$timeContain['st'])>=3){
                            $longData = $value2;
                            $longData['fstarttime_long'] = $value2['fstarttime'];
                            $longData['fendtime_long'] = $value2['fendtime'];
                            $longData['fstarttime'] = date('Y-m-d H:i:s',$timeContain['st']);
                            $longData['fendtime'] = date('Y-m-d H:i:s',$timeContain['ed']);
                            $longData['flength'] = $timeContain['ed']-$timeContain['st'];
                            $addData[] = $longData;
                        }
                    }
                }
                $adList = array_merge($adList,$addData);
                if(empty($again)){
                    $adList = $this->adListOrder($adList,$issueDate,0,0,[],1,$orderType,$isNullTime);
                }
            }else{
                $adList = $adData2;
            }
        }

        if(empty($again)){
            $adList = $this->adPxsf($adList,$orderType);

            $data['jiemucount'] = count($adData2);
            $data['guanggaocount'] = count($adData);
            $data['count'] = count($adList);
            $data['resultList'] = array_slice($adList,$limitIndex,$pageSize);
        }else{
            return $adList;
        }
        return $data;
    }

    /*
    * by zw
    * 广告排序
    */
    private function adPxsf($data,$orderType){
        switch ($orderType) {
            case 1:
                $data = pxsf($data,'fstarttime',true);
                break;
            case 10:
                $data = pxsf($data,'fadname',false,'cn');
                break;
            case 11:
                $data = pxsf($data,'fadname',true,'cn');
                break;
            case 20:
                $data = pxsf($data,'fadclasscode',false);
                break;
            case 21:
                $data = pxsf($data,'fadclasscode',true);
                break;
            case 30:
                $data = pxsf($data,'flength',false);
                break;
            case 31:
                $data = pxsf($data,'flength',true);
                break;
            case 40:
                $data = pxsf($data,'fillegaltypecode',false);
                break;
            case 41:
                $data = pxsf($data,'fillegaltypecode',true);
                break;
            default:
                $data = pxsf($data,'fstarttime',false);
                break;
        }
        return $data;
    }

    /*
    * by zw
    * 交叉数据获取,
    * $data1 需要比较的开始、结束、节目类别数据，$data2 所有数据的集合
    */
    private function overlappingData($data1 = [],$data2 = [],$startname,$endname){
        foreach ($data2 as $key => $value) {
            $ischeck = true;
            //修改的，属于当前修改的记录不需要验证
            if(!empty($value['issueid'])){
                if(($data1['issueid'] == $value['issueid'] && $data1['adType'] == $value['adType'])){
                    $ischeck = false;
                }
            }

            $baohan = isTimeContain(strtotime($data1[$startname]),strtotime($data1[$endname]),strtotime($value[$startname]),strtotime($value[$endname]));

            if($baohan>0 && $ischeck){
                return $value;
            }
        }
        return false;
    }

    /**
     * 获取广告明细数据（包含节目）
     * by zw
     * $datatype 默认所有数据（广告、节目），1广告，2节目，3节目片断和广告
     */
    public function getAdList($mclass,$mediaId,$issueDate,$fields = '',$datatype = 0){
        $data1 = [];
        $data2 = [];
        $data3 = [];
        if($mclass == '01'){
            $tb_str = 'tv';
        }elseif($mclass == '02'){
            $tb_str = 'bc';
        }elseif($mclass == '03' || $mclass == '04'){
            $tb_str = 'paper';
        }else{
            return false;
        }

        if(is_array($issueDate)){
            $tbYear = date('Y',strtotime($issueDate[0]));
            $where_issue['a.fissuedate'] = ['between',[date('Y-m-d',strtotime($issueDate[0])),date('Y-m-d',strtotime($issueDate[1]))]];
            $where_prog['a.fissuedate'] = ['between',[date('Y-m-d',strtotime($issueDate[0])),date('Y-m-d',strtotime($issueDate[1]))]];
            $where_prog_detail['a.fissuedate'] = ['between',[date('Y-m-d',strtotime($issueDate[0])),date('Y-m-d',strtotime($issueDate[1]))]];
        }else{
            $tbYear = date('Y',strtotime($issueDate));
            $where_issue['a.fissuedate'] = date('Y-m-d',strtotime($issueDate));
            $where_prog['a.fissuedate'] = date('Y-m-d',strtotime($issueDate));
            $where_prog_detail['a.fissuedate'] = date('Y-m-d',strtotime($issueDate));
        }

        //广告
        if($datatype == 0 || $datatype == 1){
            $where_issue['a.fmediaid'] = $mediaId;
            $where_issue['a.fstate'] = 1;
            $where_issue['b.fstate'] = 1;
            $data1 = M('t'.$tb_str.'issue a')
                ->field('1 adtype,c.fadname title,a.f'.$tb_str.'issueid issueid'.$fields)
                ->join('t'.$tb_str.'sample b on a.f'.$tb_str.'sampleid = b.'.($tb_str == 'tv' || $tb_str == 'bc'?'fid':'fpapersampleid').' and b.fstate in(0,1)')
                ->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
                ->join('tadclass d on c.fadclasscode = d.fcode')
                ->join('tadowner e on c.fadowner = e.fid')
                ->where($where_issue)
                ->select();
        }

        if($mclass == '01' || $mclass == '02'){
            //节目
            if($datatype == 0 || $datatype == 2){
                $where_prog['a.fmediaid'] = $mediaId;
                $where_prog['a.fstate'] = 1;
                $data2 = M('t'.$tb_str.'prog')
                    ->alias('a')
                    ->field('2 adtype,a.fprogid issueid,a.fissuedate,a.fstart fstarttime,a.fend fendtime,a.fcontent title'.$fields)
                    ->join('tprogclass d on a.fclasscode = d.fprogclasscode')
                    ->where($where_prog)
                    ->select();
            }
        }

        $data = array_merge($data1,$data2);
        return $data;
    }

    private function createTaskFlowLog($data) {
        $data['fcreaterid'] = session('person.fid');
        $data['fcreater'] = session('person.fname');
        $data['fcreatetime'] = date('Y-m-d H:i:s');
        $logId = M('tissue_checklog')->add($data);
        return $logId;
    }

    /**
     * 处理违法表现
     * $fexpressioncodes 违法表现代码
     * by zw
     */
    public function sampleillegal($fexpressioncodes){
        if(empty($fexpressioncodes)){
            return false;
        }
        $codes = explode(';', $fexpressioncodes);
        foreach($codes as $v){//循环违法表现代码
            if(!empty($v)){
                $illegal_info = M('tillegal')->cache(true,86400)->where(array('fcode'=>$v,'fstate'=>1))->find();
                if($illegal_info){
                    $fexpression .= $illegal_info['fexpression'].';'."\n";//把合规的违法表现加入字符串
                    $fconfirmation .= $illegal_info['fconfirmation'].';'."\n";//把合规的认定依据加入字符串
                    $fpunishment .= $illegal_info['fpunishment'].';'."\n";//把合规的处罚依据加入字符串
                    $fpunishmenttype .= $illegal_info['fpunishmenttype'].';'."\n";//把合规的处罚种类及幅度加入字符串
                }
            }
        }
        $return_data['fexpressioncodes'] = rtrim($fexpressioncodes,';');//去掉末尾分号
        $return_data['fexpressions'] = rtrim($fexpression,';'."\n");//去掉末尾分号
        $return_data['fconfirmations'] = rtrim($fconfirmation,';'."\n");//去掉末尾分号
        $return_data['fpunishments'] = rtrim($fpunishment,';'."\n");//去掉末尾分号
        $return_data['fpunishmenttypes'] = rtrim($fpunishmenttype,';'."\n");//去掉末尾分号
        return $return_data;
    }

}
