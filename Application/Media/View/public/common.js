var var_func;
var var_func2;
function confirmModal(text, func, func2) {
    if ($('#confirmModal')) {
        $('#confirmModal').remove();
    }
    $('body').append('<div class="modal inmodal fade" id="confirmModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">\
        <div class="modal-dialog">\
            <div class="modal-content" style="padding:40px 0px 10px 0px;">\
                <h2 class="h2 text-center">'+ text + '</h2>\
                <div class="container-fluid">\
                    <div class="row text-center" style="margin:30px 0">\
                        <button type="button" class="btn btn-primary" id="confirm_yes" style="margin-right:20px;">确认</button>\
                        <button type="button" class="btn btn-danger" id="confirm_no" style="margin-left:20px;">取消</button>\
                    </div>\
                </div>\
            </div>\
        </div>\
    </div>')
    $('#confirmModal').modal('show');
    var_func = func;
    if (func2) {
        var_func2 = func2;
    }
}
$(document).on('click', '#confirm_yes', function () {
    var_func()
})
$(document).on('click', '#confirm_no', function () {
    if (var_func2) {
        var_func2();
    }
    $('#confirmModal').modal("hide").on('hidden.bs.modal', function () {
        $('#confirmModal').remove();
    });
    return false;
})

$(document).on("click",'.J_menuItem2',function () {
    let href = $(this).attr('href');
    let name = $(this).text();
    $(this).attr("title2") ? (name = $(this).attr("title2")) : name = $(this).text();

    let have = false;
    $.each($("body", parent.document).find('.page-tabs-content a'), function () {
        if ($(this).attr('data-id') == href) {
            $(this).addClass('active');
            $("body", parent.document).find('.J_tabShowActive').click();
            have = true;
        } else {
            $(this).removeClass('active');
        }
    });

    if (have == true) {
        for (let i = 0; i < $("body", parent.document).find('#content-main iframe').length; i++) {
            if ($("body", parent.document).find('#content-main iframe').eq(i).attr('data-id') == href) {
                $("body", parent.document).find('#content-main iframe').hide();
                $("body", parent.document).find('#content-main iframe').eq(i).css('display', 'inline');
            }
        }
    } else {
        $("body", parent.document).find('.page-tabs-content a').removeClass('active');
        $("body", parent.document).find('.page-tabs-content').append('<a href="javascript:;" class="J_menuTab active" data-id="' + href + '">' + name + ' <i class="fa fa-times-circle"></i></a>');
        $("body", parent.document).find('.J_tabShowActive').click();
        $("body", parent.document).find('#content-main iframe').hide();
        $("body", parent.document).find('#content-main').append('<iframe style="display:inline" class="J_iframe" name="iframe0" width="100%" height="100%" src="' + href + '" frameborder="0" data-id="' + href + '" seamless></iframe>')
    }
})
$(function(){
    $("#check_no").on("ifChecked",function(){
        $("#check_wrong").show();
    })
    $("#check_yes").on("ifChecked",function(){
        $("#check_wrong").find("input[type=checkbox]").iCheck("uncheck");
        $("#check_wrong").hide();
    })
})
// 关闭窗口并清空内容函数
function closeModal(obj1, obj2) {
    $(document).on('click', obj1, function () {
        if ($('#mediaclass-select')) {
            $('#mediaclass-select').hide();
        }
        if ($('#region-select')) {
            $('#region-select').hide();
        }
        if ($('input[type=radio]').length > 0) {
            $('input[type=radio]').iCheck('uncheck');
        }
        if ($('textarea').length > 0) {
            $('textarea').val('');
        }
        if ($('#infoDiv') && $('#ctrlDiv')) {
            $('#infoDiv').show();
            $('#ctrlDiv').hide();
        }
        if ($('#liveIframe')) {
            $('#liveIframe').attr('src', '');
        }
        if ($('#backMsg')) {
            $('#backMsg').hide();
        }
        if ($('#videoModal').length > 0) {
            $('#videoModal').find('video').attr('src', '');
        }
        if ($('.vidoeBlock').length > 0) {
            $('.vidoeBlock').find('video').attr('src', '');
        }

        $(obj2).modal('hide');
        $(obj2).find('input[type=text],input[type=password],textarea,select').val('');
    })
}
function alertMSG(msg, type, time, callback) {
    if (!msg) {
        alert('请输入打印信息!')
        return false;
    }
    $("#alertMsg").remove();
    $('body').append(`
    <div style="position: fixed;width: 50%;bottom: 10%;left: 50%;transform: translateX(-50%);z-index: 9999;" class="alert alert-`+ type +` text-center" id="alertMsg">`+ msg +`</div>
    `)
    if (callback) {
        $('#alertMsg').fadeOut(time, callback)
    } else {
        $('#alertMsg').fadeOut(time)
    }
}

function closePage() {
    var thispage = window.location.pathname;
    $("body", parent.document).find('.page-tabs-content a[data-id="'+ thispage +'"] .fa-times-circle').click();
}

function tagJump(addr) {
    var thispage = window.location.pathname;
    if ($("body", parent.document).find("iframe[data-id='" + addr + "']").length) {
        $("body", parent.document).find("iframe[data-id='"+ addr +"']").attr('src',addr).show();
        $("body", parent.document).find('.page-tabs-content a[data-id="'+ addr +'"]').addClass('active').siblings().removeClass('active');
    }
    $("body", parent.document).find('.page-tabs-content a[data-id="'+ thispage +'"] .fa-times-circle').click();
}