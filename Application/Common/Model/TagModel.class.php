<?php
/*
 * @Description: 通用标签(分组)
 * @Author: yuhou.wang
 * @Date: 2019-12-04 11:32:55
 * @LastEditors: yuhou.wang
 * @LastEditTime: 2019-12-04 11:57:24
 */
namespace Common\Model;
class TagModel{
    /**
     * @Des: 获取标签(分组)列表
     * @Edt: yuhou.wang
     * @Date: 2019-12-04 11:34:55
     * @param {String} $fields 返回字段
     * @param {Array} $filter 过滤条件 
     * @return: 
     */
    public function getList($fields='*',$filter=[]){
        $dataSet = [];
        $dataSet = M('ad_input_user_group')->field($fields)->where($filter)->select();
        return $dataSet;
    }
    /**
     * @Des: 添加标签
     * @Edt: yuhou.wang
     * @Date: 2019-12-04 11:44:39
     * @param {type} 
     * @return: 
     */
    public function addTag(){

    }
    /**
     * @Des: 删除标签
     * @Edt: yuhou.wang
     * @Date: 2019-12-04 11:44:39
     * @param {type} 
     * @return: 
     */
    public function delTag(){
        
    }
    /**
     * @Des: 更新标签
     * @Edt: yuhou.wang
     * @Date: 2019-12-04 11:44:39
     * @param {type} 
     * @return: 
     */
    public function setTag(){
        
    }
}