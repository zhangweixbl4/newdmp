let workScoreItem = {
    template: `<div v-loading="loading">
    <el-form ref="form" :model="info" label-width="80px" :rules="rules" :disabled="type != 'input'">
        <el-form-item label="加分人" prop="wx_id">
            <el-select
                    v-model="info.wx_id"
                    filterable
                    remote
                    placeholder="请输入加分人"
                    :remote-method="searchAdInputUser"
                    :loading="loading">
                <el-option
                        v-for="item in wxUserArr"
                        :key="item.value"
                        :label="item.label"
                        :value="item.value">
                </el-option>
            </el-select>
        </el-form-item>
        <el-form-item label="分数" prop="score">
            <el-input-number v-model="info.score"></el-input-number>
        </el-form-item>
        <el-form-item label="加分日期" prop="score_date">
            <el-date-picker
                    v-model="info.score_date"
                    type="date"
                    placeholder="选择日期"
                    format="yyyy 年 MM 月 dd 日"
                    value-format="timestamp"
                    clearable
            >
            </el-date-picker>
        </el-form-item>
        <el-form-item label="加分类型" prop="type">
            <el-select v-model="info.type" filterable clearable>
                <el-option
                        v-for="item in workScoreType.slice(2)"
                        :key="item.value"
                        :label="item.label"
                        :value="item.value">
                </el-option>
            </el-select>
        </el-form-item>
        <el-form-item label="备注" prop="remark">
            <el-input v-model="info.remark" autosize type="textarea"></el-input>
        </el-form-item>
        <el-form-item>
            <div v-if="type==='input'">
                <el-button type="primary" @click="submit">提交</el-button>
            </div>
        </el-form-item>
        <div v-if="type!=='input'">
            <el-form-item label="申请人">
                {{info.creator}}
            </el-form-item>
            <el-form-item label="申请时间">
                {{info.create_time}}
            </el-form-item>
        </div>
    </el-form>
    <div v-if="type != 'input' && info.state==0" style="display: flex;justify-content: space-around;">
        <el-button type="success" @click="pass">通过</el-button>
        <el-button type="danger" @click="reject">驳回</el-button>
    </div>
</div>`,
    props: ['id'],
    data: function () {
        return {
            loading: false,
            rules: {
                wx_id: [
                    { required: true, message: '请输入加分人', trigger: 'blur' },
                ],
                score: [
                    { required: true, message: '请输入分数', trigger: 'blur' },
                ],
                type: [
                    { required: true, message: '选择类型', trigger: 'blur' },
                ],
                remark: [
                    { required: true, message: '请输入加分说明', trigger: 'blur' },
                ],
                score_date: [
                    { required: true, message: '请输入加分说明', trigger: 'blur' },
                ],
            },
            workScoreType,
            info: {},
            type: 'input',
            wxUserArr: [],
            userSearchLoading: false,
        }
    },
    methods: {
        dataInit: function () {
            this.loading = false
            this.info = {}
            this.wxUserArr = []
            this.userSearchLoading = false
            this.type = 'input'
            if (this.id != 0){
                this.type = 'check'
                this.getInfo()
            }
        },
        getInfo: function () {
            this.loading = true
            $.post('getItemInfo', {id: this.id}).then((res) => {
                this.loading = false
                this.info = res
            })
        },
        searchAdInputUser: function (queryString, cb) {
            this.userSearchLoading = true
            $.post("/Api/User/searchUserValueLabelByAlias", {keyword:queryString}).then((res) => {
                this.wxUserArr = res.data
                this.userSearchLoading = false
            });
        },
        submit: function () {
            this.$refs.form.validate((res) => {
                if (res){
                  this.$confirm('确认提交?').then(() => {
                      $.post('addWorkScoreItem', {info: this.info}).then((res) => {
                          if (res.code == 0){
                              this.$emit('success', res.msg)
                          } else {
                              this.$message.error(res.msg)
                          }
                      })
                  })
                }
            })
        },
        pass: function () {
            this.$confirm('确认通过?').then(() => {
                $.post('passScoreItem', {id: this.id}).then((res) => {
                    if (res.code == 0){
                        this.$emit('success', res.msg)
                    } else {
                        this.$emit('error', res.msg)
                    }
                })
            })
        },
        reject: function () {
            this.$confirm('确认驳回?').then(() => {
                $.post('rejectScoreItem', {id: this.id}).then((res) => {
                    if (res.code == 0){
                        this.$emit('success', res.msg)
                    } else {
                        this.$emit('error', res.msg)
                    }
                })
            })
        },
    }
}