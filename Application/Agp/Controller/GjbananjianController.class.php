<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 国家局交办案件
 * by zw
 */

class GjbananjianController extends BaseController{
  /**
  * 交办案件列表
  * by zw
  */
  public function jiaoban_list(){
    Check_QuanXian(['jbananjian']);
    session_write_close();
    $system_num = getconfig('system_num');
    
    $p  = I('page', 1);//当前第几页
    $pp = 20;//每页显示多少记录

    $mclass = I('mclass');//媒体类型
    $medianame = I('medianame');//媒体名称
    $area = I('area');//所属工商局

    if(!empty($mclass)){
      $where_tmg['left(tmedia.fmediaclassid,2)'] = $mclass;
    }
    if(!empty($medianame)){
      $where_tmg['tmedia.fmedianame'] = array('like','%'.$medianame.'%');
    }
    if(!empty($area)){
      $where_tmg['tregulator.fregionid'] = $area;
    }

    $where_tmg['tbn_media_grant.fgrant_reg_id']   = session('regulatorpersonInfo.fregulatorpid');
    $where_tmg['tbn_media_grant.fcustomer']   = $system_num;

    $count = M('tbn_media_grant')
      ->join('tmedia on tmedia.fid=tbn_media_grant.fmedia_id and tmedia.fid=tmedia.main_media_id')
      ->join('(select fmediaid from tmedia_temp where fstate = 1 and ftype in (0,1) and fcustomer = "'.$system_num.'" and fuserid = '.session('regulatorpersonInfo.fid').' group by fmediaid) ttp on tmedia.fid = ttp.fmediaid')
      ->join('tregulator on tregulator.fid = tbn_media_grant.freg_id')
      ->where($where_tmg)
      ->count();//查询满足条件的总记录数

    
    $do_tmg = M('tbn_media_grant')
      ->field('tbn_media_grant.fid,fgrant_user,left(fmediaclassid,2) fmedia_class,freg,fgrant_time, (case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')
      ->join('tmedia on tmedia.fid=tbn_media_grant.fmedia_id and tmedia.fid=tmedia.main_media_id')
      ->join('(select fmediaid from tmedia_temp where fstate = 1 and ftype in (0,1) and fcustomer = "'.$system_num.'" and fuserid = '.session('regulatorpersonInfo.fid').' group by fmediaid) ttp on tmedia.fid = ttp.fmediaid')
      ->join('tregulator on tregulator.fid = tbn_media_grant.freg_id')
      ->where($where_tmg)
      ->order('tbn_media_grant.fid desc')
      ->page($p,$pp)
      ->select();
  
    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tmg)));
  }

  /**
  * 交办
  * by zw
  */
  public function ajjiaoban(){
    $system_num = getconfig('system_num');

    $fmedia_ids   = I('fmedia_ids');//媒体ID组
    $tregionid    = I('tregionid');//行政区划ID
    $fmedia_class = I('fmedia_class');//媒体类型

    if(empty($tregionid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择需要派发的机构'));
    }
    $do_tr = M('tregulator')->field('fid,fname')->where(['fregionid'=>$tregionid,'_string'=>'fstate=1 and ftype=20 and fkind=1'])->find();
    if(empty($do_tr)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'该区域还未建立相应机构'));
    }
    $attach = [];
    $where_tia['freg_id']       = $do_tr['fid'];
    $where_tia['fcustomer']     = $system_num;
    $where_tia['fgrant_reg_id'] = session('regulatorpersonInfo.fregulatorpid');
    $data_tia['freg']           = $do_tr['fname'];
    $data_tia['freg_id']        = $do_tr['fid'];
    $data_tia['fcustomer']      = $system_num;
    $data_tia['fgrant_reg_id']  = session('regulatorpersonInfo.fregulatorpid');
    $data_tia['fgrant_reg']     = session('regulatorpersonInfo.regulatorpname');
    $data_tia['fgrant_user']    = session('regulatorpersonInfo.fname');
    $data_tia['fgrant_time']    = date('Y-m-d H:i:s');
    foreach ($fmedia_ids as $key => $value) {
      $where_tia['fmedia_id']     = $value;
      $do_tia = M('tbn_media_grant')->where($where_tia)->find();
      if(empty($do_tia)){
        $data_tia['fmedia_id']      = $value;
        array_push($attach,$data_tia);
      }
    }
    if(!empty($attach)){
      M('tbn_media_grant')->addAll($attach);
      D('Function')->write_log('交办案件',1,'交办成功','tbn_media_grant',0,M('tbn_media_grant')->getlastsql());
      $this->ajaxReturn(array('code'=>0,'msg'=>'交办成功'));
    }else{
      D('Function')->write_log('交办案件',0,'交办失败','tbn_media_grant',0,M('tbn_media_grant')->getlastsql());
      $this->ajaxReturn(array('code'=>1,'msg'=>'交办失败'));
    }
    
  }

  /**
  * 撤消交办授权
  * by zw
  */
  public function cxjiaoban(){
    $system_num = getconfig('system_num');
    $fid  = I('fid');//交办id
    $where_tia['fgrant_reg_id']  = session('regulatorpersonInfo.fregulatorpid');
    $where_tia['fid']  = $fid;
    $where_tia['fcustomer']  = $system_num;

    $do_tia = M('tbn_media_grant')->where($where_tia)->delete();
    if(!empty($do_tia)){
       D('Function')->write_log('交办案件',1,'交办撤消成功','tbn_media_grant',$do_tia,M('tbn_media_grant')->getlastsql());
      $this->ajaxReturn(array('code'=>0,'msg'=>'已撤消交办'));
    }else{
       D('Function')->write_log('交办案件',0,'交办撤消失败','tbn_media_grant',0,M('tbn_media_grant')->getlastsql());
      $this->ajaxReturn(array('code'=>1,'msg'=>'撤消失败'));
    }
  }

}