<?php
namespace Api\Controller;
use Think\Controller;
class FixedMediaDataController extends Controller {
	
	
	#处理逾期未归档的媒体
	public function handle_expire(){
		header("Content-type: text/html; charset=utf-8");
		session_write_close();
		set_time_limit(600);
		$start_date = date('Y-m-d',time()-86400*15);
		$end_date = date('Y-m-d');
		
		$where = array(
							'fstatus'=>['in','1,99'],
							'fissue_date'=>['between',[date('Y-m-d',time()-86400*32),date('Y-m-d')]],
							'next_try_fixed_time'=>['lt',time()]
							
						);
		
		$yqwgdList = M('tinspect_plan','',C('ZIZHI_DB'))
									->field('fmedia_id,fissue_date,fstatus,fixed_try_count')
									->where($where)->order('fid desc')->limit(200)->select();
		
		$yqwgdCount = M('tinspect_plan','',C('ZIZHI_DB')) ->where($where)->count();

		
		echo $yqwgdCount.' 需要计算'."\n";
		

		foreach($yqwgdList as $yqwgd){
			$next_try_fixed_time = time() + ((time()-strtotime($yqwgd['fissue_date'])) / 86400)*3600/2; #下次尝试归档时间，每过一天加0.5小时
			$next_try_fixed_time += $yqwgd['fixed_try_count']*3600; #下次尝试归档时间，每尝试一次加1小时	
			
			$available_time2 = A('Common/Media','Model')->media_issue_data_state($yqwgd['fmedia_id'],$yqwgd['fissue_date']);
				
			$gdzt = $yqwgd['fstatus'];
			$fvvv = '';	
			if($available_time2['state'] == 0){ #判断是否符合归档条件
				$gdzt = $yqwgd['fstatus'];
				$fvvv = '尝试归档失败,原因：'.$available_time2['remark'];	


				M('tinspect_plan','',C('ZIZHI_DB'))
								->where(array('fmedia_id'=>$yqwgd['fmedia_id'],'fissue_date'=>$yqwgd['fissue_date']))
								->save(array('next_try_fixed_time'=>$next_try_fixed_time,'fixed_try_count'=>$yqwgd['fixed_try_count']+1));
			}else{

				$gdzt = 2;
				$fvvv = '自动归档成功,'.$available_time2['remark'];	
			}
			A('Admin/FixedMediaData','Model')->fixed($yqwgd['fmedia_id'],$yqwgd['fissue_date'],$gdzt,$fvvv,'system'); #归档状态修改
			if(I('get.debug') == 1) echo $yqwgd['fmedia_id'] . '	' . $yqwgd['fissue_date'].'	'.$gdzt."\n";
			
			
			
			
		}
		
		echo 'end';
	}
	

	
	#查询归档状态*
	public function get_state(){
		$this->ajaxReturn(array('code'=>-1,'msg'=>'该接口停用'));
		$mediaid = I('mediaid');
		$day = I('day');
		
		$fixed_media_data = M('fixed_media_data')->field('fixed_state,fixed_value')->where(array('fmediaid'=>$mediaid,'fdate'=>$day))->find();
		
		#$this->ajaxReturn(array('code'=>0,'msg'=>'','data'=>$fixed_media_data));
		
		
	}
	
	#开始生产*
	public function start_produce(){
		header("Content-type: text/html; charset=utf-8");
		session_write_close();
		set_time_limit(600);
		$dataWhere = array();
		$dataWhere['fstatus'] = 0;
		$dataWhere['fissue_date'] = ['between',[date('Y-m-d',time()-86400*31),date('Y-m-d')]];
		
		$dataList = M('tinspect_plan','',C('ZIZHI_DB'))->field('fid,fmedia_id,fissue_date,finspect_type')->where($dataWhere)->limit(500)->select();
		$dataCount = M('tinspect_plan','',C('ZIZHI_DB'))->where($dataWhere)->count();
		
		$kjInsertCount = 0;
		$clCount = 0;
		foreach($dataList as $data){
			
			if($data['finspect_type'] == 3){ #快剪生产
				$kjInsert = A('Api/CutTask','Model')->make_task($data['fmedia_id'],$data['fissue_date']);
				$kjInsertCount += $kjInsert;
				A('Admin/FixedMediaData','Model')->fixed($data['fmedia_id'],$data['fissue_date'],1,'开始生产(快剪)','robot');
			}else{
				A('Admin/FixedMediaData','Model')->fixed($data['fmedia_id'],$data['fissue_date'],1,'开始生产','robot');
			}
			
			$clCount += 1;
		
		}
			
		
		echo date('H:i:s') .'总共 '.$dataCount.'个计划需处理， 本次处理 '.$clCount.' 个计划，其中新增快剪任务 '.$kjInsertCount.' 个';
		
		
		
		
		
	}
	
	
	
	
	
	
	public function need_update_sci(){
		session_write_close();
		set_time_limit(600);
		$need_update_data = M('tinspect_plan','',C('ZIZHI_DB'))->field('fmedia_id,fissue_date')->where(array('need_update_sci'=>1))->limit(200)->select();
		$need_update_count = M('tinspect_plan','',C('ZIZHI_DB'))->field('fmedia_id,fissue_date')->where(array('need_update_sci'=>1))->count();
		
		foreach($need_update_data as $data){
			
			$mediaInfo = M('tmedia')->cache(true,600)->field('fid,left(fmediaclassid,2) as media_class')->where(array('fid'=>$data['fmedia_id']))->find();
			
			if($mediaInfo['media_class'] == '01' || $mediaInfo['media_class'] == '02'){
				$url = 'http://172.16.198.126/index/getChannelGatherDuration?date='.$data['fissue_date'].'&channel='.$data['fmedia_id'];
				$ret = http($url);
				$retArr = json_decode($ret,true);
				if($retArr['code'] === 0){
					#echo $url."\n";
					$source_collect_intact = 1 - ($retArr['data']['du_sc'] * 0.01);
					$saveData = array('need_update_sci'=>0,'source_collect_intact'=>$source_collect_intact);
				}else{
					$saveData = array('need_update_sci'=>0);
				}
			}elseif($mediaInfo['media_class'] == '03'){
				$ttee = M('tpapersource')->where(array('fmediaid'=>$data['fmediaid'],'fissuedate'=>$data['fissue_date']))->count();
				if($ttee){
					$saveData = array('need_update_sci'=>0,'source_collect_intact'=>1);
				}else{
					$saveData = array('need_update_sci'=>0,'source_collect_intact'=>0);
				}
			}else{
				$saveData = array('need_update_sci'=>0,'source_collect_intact'=>0);
			}
			
			
			$eT = M('tinspect_plan','',C('ZIZHI_DB'))->where(array('fmedia_id'=>$data['fmedia_id'],'fissue_date'=>$data['fissue_date']))->save($saveData);
			#var_dump($eT);
			echo $eT.'	'.$data['fmedia_id'].'	'.$data['fissue_date'].'	'.$saveData['source_collect_intact']."\n";
			
		}
		
		echo date('H:i:s') .' success '.$need_update_count;
		
		
	}
	
	
	/*需要提升任务优先级*/
	public function need_task_priority(){
		
		session_write_close();
		set_time_limit(600);
		$sclrwwcList = M('msg_queue')->where(array('queue_name'=>'sclrwwc'))->limit(5)->select();
		
		foreach($sclrwwcList as $sclrwwc){
			#var_dump($sclrwwc);
			$dataRet = http($sclrwwc['queue_text'],array(),'GET',false,30);
			$retArr = json_decode($dataRet,true);
			$mediaInfo = M('tmedia')->cache(true,600)->field('fid,media_region_id,left(fmediaclassid,2) as media_class')->where(array('fid'=>$retArr['data']['channel']))->find();
			#var_dump($mediaInfo);
			var_dump($dataRet);
			$group_id = 0;
			$priority = 999;
			if(substr($mediaInfo['media_region_id'],0,2) == '33'){
				$group_id = 65;
				$priority = 1000;
			}
			foreach($retArr['data']['datalist'] as $uuid){
				
				$cacheMd5 = md5(intval($mediaInfo['media_class']).$uuid.$group_id.$priority);
				if(!S($cacheMd5)){
					$aa = A('TaskInput/TaskInput','Model')->changeTask(intval($mediaInfo['media_class']),$uuid,$group_id,$priority);
					S($cacheMd5,1,3600*6);
					
				}else{
					var_dump('有缓存，跳过');
				}	
				
				var_dump($aa);
			}
			M('msg_queue')->where(array('fid'=>$sclrwwc['fid']))->delete();
		}
	}

	/*获取节目*/
	public function get_prog(){

		session_write_close();
		set_time_limit(600);
		$need_update_data = M('tinspect_plan','',C('ZIZHI_DB'))->field('fmedia_id,fissue_date')->where(array('need_get_prog'=>1))->limit(100)->select();
		
		foreach($need_update_data as $data){
			
			$rr = A('Api/FixedMediaData','Model')->get_prog($data['fmedia_id'],$data['fissue_date']);
			$eT = M('tinspect_plan','',C('ZIZHI_DB'))->where(array('fmedia_id'=>$data['fmedia_id'],'fissue_date'=>$data['fissue_date']))->save(array('need_get_prog'=>0));
			
			echo $eT.'	'.$data['fmedia_id'].'	'.$data['fissue_date'].'	'.$rr."\n";
			
		}
		
		echo 'success';

	}
	
	/*获取发布记录*/
	public function get_issue(){
		
		session_write_close();
		set_time_limit(600);
		$need_update_data = M('tinspect_plan','',C('ZIZHI_DB'))->field('fmedia_id,fissue_date')->where(array('need_get_issue'=>1))->limit(10)->select();
		
		foreach($need_update_data as $data){
			
			$inCount = A('Common/AddIssue','Model')->add($data['fmedia_id'],$data['fissue_date']);
			echo $eT.'	'.$data['fmedia_id'].'	'.$data['fissue_date'].'	'.$inCount."\n";
			if($inCount == -1) continue;
			$eT = M('tinspect_plan','',C('ZIZHI_DB'))->where(array('fmedia_id'=>$data['fmedia_id'],'fissue_date'=>$data['fissue_date']))->save(array('need_get_issue'=>0));
			
			
			M('fixed_media_data_log')->add(array('fmediaid'=>$data['fmedia_id'],'fdate'=>$data['fissue_date'],'ftime'=>date('Y-m-d H:i:s'),'fvalue'=>'从高博那里获取发布记录，成功写入'.$inCount.'条记录','fperson'=>'robot'));
			
		}
		
		echo 'success';
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}