<?php

namespace Admin\Controller;
use Think\Controller;
class PapersourceController extends BaseController {

	public function index(){
		
		if(A('Admin/Authority','Model')->authority('201011') === 0){
			exit('无权限');//验证是否有新增权限
		}
		$p = I('p',1);//当前第几页
		$pp = I('pageSize', 10);//每页显示多少记录
		$keyword = I('keyword');//搜索关键词
		$fstarttime_s = I('fstarttime_s');// 发布日期
		$fstate = I('fstate');
		$noad = I('noad');
		if($fstarttime_s == ''){
			$fstarttime_s = date('Y-m-d');
			$_GET['fstarttime_s'] = $fstarttime_s;
		}	
		
		$fstarttime_e = I('fstarttime_e');// 发布日期
		if($fstarttime_e == ''){
			$fstarttime_e = date('Y-m-d');
			$_GET['fstarttime_e'] = $fstarttime_e;
		}
		
		if(I('region_id') != '') $region_id = I('region_id');
		$this_region_id = I('this_region_id');//是否只查询本级地区



		$where = array();
		$where['tpapersource.fissuedate'] = array('between',date('Y-m-d',strtotime($fstarttime_s)).','.date('Y-m-d',strtotime($fstarttime_e)));
		if($keyword != ''){
			$where['tmedia.fmedianame'] = array('like','%'.$keyword.'%');
		}
		if($fstate != ''){
			$where['tpapersource.fstate'] = $fstate;
		}
        if($noad == '1'){
            $where['tpapersource.noad'] = 1;
        }
		if($region_id > 0){
			if($this_region_id == 'true'){
				$where['tmediaowner.fregionid'] = $region_id;//地区搜索条件
			}elseif($region_id != 100000){
				$region_id_rtrim = A('Common/System','Model')->get_region_left($region_id);//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmediaowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}
		}
		//var_dump($where);
		$sourceList = M('tpapersource')
										->field('
												 tpapersource.*
												,tmedia.fmedianame as media_name
												,(select ad_input_user.nickname from ad_input_user where ad_input_user.wx_id = tpapersource.fuserid) as handler
													')
										->join('tmedia on tmedia.fid = tpapersource.fmediaid')	
										->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')			
										->where($where)
										->order('tpapersource.priority desc,tpapersource.fissuedate desc,tmedia.fid,tpapersource.fid')
										->page($p.','.$pp)->select();
		
		$sourceList = list_k($sourceList,$p,$pp);//为列表加上序号

		$sourceCount = M('tpapersource')->cache(true,60)
										->join('tmedia on tmedia.fid = tpapersource.fmediaid')
										->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
										->where($where)->count();// 查询满足要求的总记录数
		
		
		$mediaCount = M('tpapersource')->cache(true,60)
										->join('tmedia on tmedia.fid = tpapersource.fmediaid')
										->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
										->where($where)
										
										->count('distinct(tmedia.fid)');// 查询媒介记录数
		echo '<!--'.(M('tpapersource')->getLastSql()).'-->';								
		$Page       = new \Think\Page($sourceCount,$pp);// 实例化分页类 传入总记录数和每页显示的记录数

		$this->assign('page',$Page->show());// 赋值分页输出
		$this->assign('sourceList',$sourceList);
		$this->assign('mediaCount',$mediaCount);
		
		$this->display();
	}
	
	/*任务删除*/
	public function delete_task(){
		if(A('Admin/Authority','Model')->authority('201013') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
		}
		$sourceId = I('sourceId');//任务ID
		//M()->startTrans();//开启事务方法
		//M()->rollback();//事务回滚方法
		//M()->commit();//事务提交方法
		
		$samList = M('tpapersample')->where(array('sourceid'=>$sourceId))->getField('fpapersampleid',true);//查询样本列表,主要用于删除发布记录
		
		if($samList){
			$delIssueCount = M('tpaperissue')->where(array('fpapersampleid'=>array('in',$samList)))->delete();//删除发布记录
		}
		
		$delSamCount = M('tpapersample')->where(array('sourceid'=>$sourceId))->delete();//删除样本
		
		$delSourceCount = M('tpapersource')->where(array('fid'=>$sourceId))->delete();//删除报纸素材
		if($delSourceCount){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败'));
		}
		
	}
	
	/*任务编辑*/
	public function edit_task(){
		if(A('Admin/Authority','Model')->authority('201012') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
		}
		$fstate = I('fstate');//任务状态
		$sourceId = I('sourceId');//任务ID
		$e_data = array();
		$e_data['fstate'] = $fstate;
		if($fstate === '0'){
			$e_data['fuserid'] = 0;
		}
		if(M('tpapersource')->where(array('fid'=>$sourceId))->save($e_data)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败'));
		}
		
		
	}
	
	
	
	/*素材详情*/
	public function ajax_source_details(){
		if(A('Admin/Authority','Model')->authority('201011') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
		}
		$sourceId = I('sourceId');//素材id
		$papersourceDetails = M('tpapersource')
										->field('tpapersource.*,tmedia.fmedianame')
										->join('tmedia on tmedia.fid = tpapersource.fmediaid')
										->where(array('tpapersource.fid'=>$sourceId))
										->find();//查询素材详情
		$papersourceDetails['fissuedate'] = date('Y-m-d',strtotime($papersourceDetails['fissuedate']));							
								
		$issueList = M('tpaperissue')->field('
											tpaperissue.fpaperissueid,	
											tpapersample.tem_ad_name,	
											tpaperissue.fsize,
											tpaperissue.fissuetype,
											tpapersample.fjpgfilename,
											tpapersample.fversion,
											tad.fadname
											
											
												')
									->join('tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
									->join('tad on tad.fadid = tpapersample.fadid')
									->where(array('tpaperissue.sourceid'=>$sourceId))
									->select();//查询发布列表

		$this->ajaxReturn(array('code'=>0,'msg'=>'','papersourceDetails'=>$papersourceDetails,'issueList'=>$issueList));
		
		
		
	}
	
	
	
	/* 媒介、版面数量、广告数量*/
	public function media_page_ad_list(){
		session_write_close();
		/* select tmedia.fid as media_id,tmedia.fmedianame as media_name,tpapersource.fissuedate,
		COUNT(DISTINCT tpapersource.fid) as page_count,
		count(tpapersample.fpapersampleid) as ad_count

		from tmedia,tpapersource,tpapersample
		where 
					
					tpapersample.sourceid = tpapersource.fid 
					and tpapersource.fmediaid = tmedia.fid
					and tpapersource.fissuedate between '2018-02-01' and '2018-02-26'
		group by tmedia.fid,tpapersource.fissuedate */
		
		$keyword = I('keyword');//搜索关键词
		$fstarttime_s = I('fstarttime_s');// 发布日期
		$fstate = I('fstate');
		if($fstarttime_s == ''){
			$fstarttime_s = date('Y-m-d');
			$_GET['fstarttime_s'] = $fstarttime_s;
		}	
		
		$fstarttime_e = I('fstarttime_e');// 发布日期
		if($fstarttime_e == ''){
			$fstarttime_e = date('Y-m-d');
			$_GET['fstarttime_e'] = $fstarttime_e;
		}
		
		if(I('region_id') != '') $region_id = I('region_id');
		$this_region_id = I('this_region_id');//是否只查询本级地区
		
		
		
		$where = array();
		$where['tpapersource.fissuedate'] = array('between',date('Y-m-d',strtotime($fstarttime_s)).','.date('Y-m-d',strtotime($fstarttime_e)));
		if($keyword != ''){
			$where['tmedia.fmedianame'] = array('like','%'.$keyword.'%');
		}
		if($fstate != ''){
			$where['tpapersource.fstate'] = $fstate;
		}
		if($region_id > 0){
			if($this_region_id == 'true'){
				$where['tmediaowner.fregionid'] = $region_id;//地区搜索条件
			}elseif($region_id != 100000){
				$region_id_rtrim = A('Common/System','Model')->get_region_left($region_id);//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmediaowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}
		}
		
		
		$media_data_list = M('tpapersource')
											->field('
													tmedia.fid as media_id,
													tmedia.fmedianame as media_name,
													substr(tpapersource.fissuedate,1,10) as issuedate,
													COUNT(DISTINCT tpapersource.fid) as page_count
													')
											//->join('tpapersample on tpapersample.sourceid = tpapersource.fid')
											->join('tmedia on tmedia.fid = tpapersource.fmediaid')
											->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')	
											->where($where)
											->group('tmedia.fid,tpapersource.fissuedate')//
											->order('tpapersource.fissuedate asc , tmedia.fid')
											->select();
		$where2 = array();
		$where2['tpaperissue.fissuedate'] = array('between',date('Y-m-d',strtotime($fstarttime_s)).','.date('Y-m-d',strtotime($fstarttime_e)));
		if($keyword != ''){
			$where2['tmedia.fmedianame'] = array('like','%'.$keyword.'%');
		}
		
		if($region_id > 0){
			if($this_region_id == 'true'){
				$where2['tmediaowner.fregionid'] = $region_id;//地区搜索条件
			}elseif($region_id != 100000){
				$region_id_rtrim = A('Common/System','Model')->get_region_left($region_id);//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where2['left(tmediaowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}
		}									
		$media_issue_list = M('tpaperissue')
											/* ->field('
													tmedia.fid as media_id,
													
													substr(tpaperissue.fissuedate,1,10) as issuedate,
													COUNT(tpaperissue.fpaperissueid) as ad_count
													') */
											->join('tmedia on tmedia.fid = tpaperissue.fmediaid')
											->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')	
											->where($where2)
											->group('tmedia.fid,tpaperissue.fissuedate')
											->getField('
														concat(tmedia.fid,"_",substr(tpaperissue.fissuedate,1,10)) as media_id_date,
														
														COUNT(tpaperissue.fpaperissueid) as ad_count
													');

		
		$mediaDataList = array();
		$issuedateList = array();
		foreach($media_data_list as $media_data){
			$mediaDataList[$media_data['media_id']]['media_id'] = $media_data['media_id'];
			$mediaDataList[$media_data['media_id']]['media_name'] = $media_data['media_name'];
			$mediaDataList[$media_data['media_id']][$media_data['issuedate']]['page_count'] = $media_data['page_count'];//该日期的版面数量
			$mediaDataList[$media_data['media_id']][$media_data['issuedate']]['ad_count'] = //接下面
				$media_issue_list[$media_data['media_id'].'_'.$media_data['issuedate']]['ad_count'];//该日期的广告数量
			$issuedateList[] = $media_data['issuedate'];
			
		}
		$issuedateList = array_unique($issuedateList);
		$this->assign('issuedateList',$issuedateList);
		$this->assign('mediaDataList',$mediaDataList);
		
		$this->display();
	}







    /**
     * 批量修改素材剪辑优先级
     * @param $ids
     * @param $priority
     */
    public function batchChangePriority($ids, $priority)
    {
        $res = M('tpapersource')->where(['fid' => ['IN', $ids]])->save(['priority' => $priority]) === false ? false : true;
        if ($res){
            $this->ajaxReturn([
                'code' => '0',
                'msg' => '修改成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => '-1',
                'msg' => '修改失败'
            ]);
        }
    }


    /**
     * 退回报纸剪辑任务
     * @param $sourceId
     * @param $reason
     */
    public function paperSourceBack($sourceId, $reason)
    {
        // 检查状态, 只有状态为2的才允许退回
        if (M('tpapersource')->where(['fid' => ['EQ', $sourceId]])->getField('fstate') != 2){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '当前状态不允许退回'
            ]);
        }
        // 如果有样本, 就走剪辑错误流程, 没有的话直接改paper source表数据
        $sampleId = M('tpapersample')->where(['sourceid' => ['EQ', $sourceId]])->getField('fpapersampleid', true);
        // 如果只任务都做完了, 就不走剪辑错误了
        $useCutErr = M('task_input')->where([
            'task_state' => ['EQ', 2],
            'media_class' => ['EQ', 3],
            'sam_id' => ['IN', $sampleId]
        ])->count();
        if ($sampleId && $useCutErr == 0){
            $res = A('TaskInput/CutErr', 'Model')->cutErr('paper', $sampleId, $reason);
        }else{
            $res = M('tpapersource')
                ->where([
                    'fid' => ['EQ', $sourceId]
                ])
                ->save([
                    'fstate' => 6,
                    'cut_err_reason' => $reason,
                    'operation_time' => time(),
                ]) !== false;
        }
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
    }
	
	
	
	
	

}