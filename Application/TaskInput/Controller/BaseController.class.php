<?php

namespace TaskInput\Controller;
use Think\Controller;

class BaseController extends Controller {

    public $oParam;//公共变量

	public function _initialize() {
		$this->oParam = json_decode(file_get_contents('php://input'),true);//接收数据

		A('Common/AliyunLog','Model')->dmpLogs();//记录日志
		$direct = array(//设置无须验证的页面
            'TaskInput/Index/index',
            'TaskInput/Index/home',
            'TaskInput/Login/out_login',
            'TaskInput/TraditionAd/sampleTableSync',
            'TaskInput/NetAd/autoCreateTask',
            'TaskInput/NetAd/sqlCreateTask',
            'Admin/TaskInput/index',
            'TaskInput/NetAd/setNetEvidence',
            'TaskInput/NetAd/getNetEvidenceList',
            'TaskInput/NetAd/updateEvidenceInfo',
            'TaskInput/NetAd/getObject',
            'TaskInput/NetAd/pharData',
            'TaskInput/NetAd/testAddTask',
            'TaskInput/TraditionAd/get_file_up',
            'TaskInput/NetAd/freshNetCheckTask',
            'TaskInput/NetAd/getNetCheckTask',
            'TaskInput/NetAd/addEvidence',
            'TaskInput/NetAd/getEvidenceList',
            'TaskInput/NetAd/applyEvidenceList',
            'TaskInput/NetAd/applyEvidence',
            'TaskInput/NetAd/getIsAllowSubmit',
            'TaskInput/NetAd/showPDF',
            'TaskInput/QualityInspection/getTask',
            'TaskInput/QualityInspection/submitTask',
            'TaskInput/QualityInspection/testAddTaskData',
            'TaskInput/QualityInspection/myTask',
            'TaskInput/QualityInspection/listTask',
            'TaskInput/QualityInspection/getTaskInfo',
            'TaskInput/QualityInspection/setPriority',
            'TaskInput/Api/test_add_task',
            'TaskInput/TraditionAd/changeTask',
            'TaskInput/TraditionAd/changeTaskMulti',
            'TaskInput/TraditionAd/setDialect',
        );
        $personInfoCookie = cookie('wxInfo');
        // 如果这个人的session过期但是cookie未过期;
        if ($personInfoCookie && !session('wx_id')){
            $where = array('wx_id'=>sys_auth($personInfoCookie['wx_id'],'DECODE'),'alias'=>sys_auth($personInfoCookie['alias'],'DECODE'));
            $wx_id = M('ad_input_user')->where($where)->getField('wx_id');
            if ($wx_id){
                session('wx_id', $wx_id);
            }
        }

        if(!session('wx_id') && !in_array(MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME ,$direct) && MODULE_NAME != 'Admin'){
            if (IS_POST){
    			$this->ajaxReturn(array('code'=>-2,'msg'=>'登录状态过期, 请重新登录'));
            }else{
                $logInUrl = U('Adinput/VideoCutting/login');
                $this->show("<script>window.top.location.assign('{$logInUrl}');</script>");
            }
		}
	}
}