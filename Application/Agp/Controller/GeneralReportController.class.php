<?php
namespace Agp\Controller;
use Think\Controller;
use Think\Exception;
use Agp\Model\StatisticalReportModel;
use Agp\Model\StatisticalModel;
use OpenSearch\Client\OpenSearchClient;
use OpenSearch\Client\DocumentClient;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;
import('Vendor.PHPExcel');

class GeneralReportController extends BaseController{
    //淄博报表列表
    public function index() {
        $p 					= I('page', 1);//当前第几页
        $pp 				= 20;//每页显示多少记录
        $pnname 			= I('pnname');// 报告名称
        $pntype 			= I('pntype');// 报告类型
        $pnfiletype 		= I('pnfiletype');// 文件类型
        $pncreatetime 		= I('pnendtime');//生成时间
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $default_report = !empty($ALL_CONFIG['default_report'])?json_decode($ALL_CONFIG['default_report'],true):[-1];

        if (!empty($pnname)) {
            $where['pnname'] = array('like', '%' . $pnname . '%');//报告名称
        }
        if (!empty($pntype)) {
            $where['pntype'] = $pntype;//报告类型
        }else{
            $where['pntype'] = array('in',$default_report);
        }
        if (!empty($pnfiletype)) {
            $where['pnfiletype'] = $pnfiletype;//文件类型
        }
        if(!empty($pncreatetime)){
            $where['_string'] = ' pnendtime between "'.$pncreatetime[0].'" and "'.$pncreatetime[1].'"';
        }
        if($ischeck == 1){
            $where['pnstatus'] = 1;
        }elseif($ischeck == 2){
            $where['pnstatus'] = 0;
        }

        $where['pntrepid'] = session('regulatorpersonInfo.fregulatorpid');
        $where['fcustomer']  = $system_num;

        $count = M('tpresentation')
            ->where($where)
            ->count();
        
        $data = M('tpresentation')
            ->where($where)
            ->page($p,$pp)
            ->order('pncreatetime desc,pnid desc')
            ->select();
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
    }

    //报告删除接口
    public function tpresentation_delete() {
        $pnid = I('pnid');//报告ID
        $system_num = getconfig('system_num');

        $where['pnid'] = $pnid;
        $where['pntrepid'] = session('regulatorpersonInfo.fregulatorpid');
        $where['fcustomer']  = $system_num;

        $data = M('tpresentation')->where($where)->delete();

        if($data > 0){
            $this->ajaxReturn(array('code'=>0,'msg'=>'删除成功！'));
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'您无权删除此报告！'));
        }
    }

    //报告添加修改接口
    public function upload_report(){
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];

        $user = session('regulatorpersonInfo');//获取用户信息
        $pnid = I('pnid');
        $ffiles = I('ffiles');//文件云地址等信息
        $s_time = I('s_time');//开始时间
        $e_time = I('e_time');//结束时间
        $pnname = I('pnname');//报告名称
        $pntype = I('pntype');//报告类型
        $pnfiletype = 10;

        if(empty($pnname)) $this->ajaxReturn(array('code'=>-1,'msg'=>'报告名称有误'));
        if((empty($s_time) || empty($e_time)) && empty($pnid)) $this->ajaxReturn(array('code'=>-1,'msg'=>'时间参数有误'));
        if(empty($pntype) && empty($pnid)) $this->ajaxReturn(array('code'=>-1,'msg'=>'报告类型有误'));
        if(empty($ffiles)) $this->ajaxReturn(array('code'=>-1,'msg'=>'请上传报告文件'));

        $data['pnname']             = $pnname;
        $data['pnurl']              = $ffiles[0]['url'];
        if(empty($pnid)){
            $data['pntype']             = $pntype;
            $data['pnfiletype']         = $pnfiletype;
            $data['pnstarttime']        = $s_time;
            $data['pnendtime']          = $e_time;
            $data['pncreatetime']       = date('Y-m-d H:i:s');
            $data['pnhtml']             = '';
            $data['pnfocus']            = 0;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']          = $system_num;
            $do_tn = M('tpresentation')->add($data);
        }else{
            $data['pnediter']   = $user['fid'];
            $data['pnedittime'] = date('Y-m-d H:i:s');
            $do_tn = M('tpresentation')->where(['pnid'=>$pnid])->save($data);
        }
        
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'操作失败'));
        }
    }

    /*生成日报表XLS*/
    public  function day_report(){
        $illtype = I('illtype',-1);//违法类型
        $mclass = I('mclass',-1);//媒介类型
        $daytime = I('daytime',date('Y-m-d'));//日期
        $use_open_search = getconfig('use_open_search');//是否是用open_search的数据
        //为1则表示使用open_search的明细数据
        if($use_open_search == 1){
            $this->day_report_os($daytime,$illtype,$mclass);
        }else{
            $this->day_report_unos($daytime,$illtype,$mclass);
        }
    }
    
    //生成周月季年报word 20181218  by yjn  create_zibo_quarter_report
    public function general_report(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport = New StatisticalModel();//实例化数据统计模型
        $StatisticalReport1 = New StatisticalReportModel();//实例化数据统计模型
        $s_date = I('s_time',date('Y-m-d'));//开始时间
        $e_date = I('e_time',date('Y-m-d'));//结束时间
        $pntype = I('pntype',20);//默认月报 报告类型20周报、30月报、40互联网报告、70季报、75半年报、80年报
        $time_str = $this->time_str($pntype,$s_date,$e_date);
        $user = session('regulatorpersonInfo');//获取用户信息
        $media_types = $StatisticalReport->get_media_type();
        if(empty($media_types)){
            $this->ajaxReturn([
                'code'=>1,
                'msg'=>'暂无数据！可能还未授权过媒体！'
            ]);
        }
        $media_types_str = implode(array_values($media_types),'、');//获取媒体类型

        //获取地域称呼信息
        $area_call =  $this->area_call($user['regionid']);//regionid_str 区域id z_call 全市 x_call区县 s_call市属

        $day_str = $this->day_str($s_date,$e_date);//获取中文的开始结束时间

        //$days = round((strtotime($s_date) - strtotime($e_date) + 1)/86400);//计算天数
        $owner_media_ids = get_owner_media_ids();//获取权限内媒体id
        $ct_media_tv_count_hz = $StatisticalReport->ct_media_count('tv',$owner_media_ids,0);//本级电视媒体数量
        $ct_media_bc_count_hz = $StatisticalReport->ct_media_count('bc',$owner_media_ids,0);//本级广播媒体数量

        $ct_media_paper_count_hz = $StatisticalReport->ct_media_count('paper',$owner_media_ids,0);//本级报纸媒体数量

        $ct_media_tv_count_xj = $StatisticalReport->ct_media_count('tv',$owner_media_ids,2);//下级电视媒体数量

        $ct_media_bc_count_xj = $StatisticalReport->ct_media_count('bc',$owner_media_ids,2);//下级广播媒体数量

        $ct_media_paper_count_xj = $StatisticalReport->ct_media_count('paper',$owner_media_ids,2);//下级报纸媒体数量

        $net_media_ids = $StatisticalReport->net_media_type_ids('13');//互联网媒体


        $all_media_count = count($owner_media_ids);//全部有数据的媒体数量



        //开始设定json数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $data['content'][] = create_report_array('text','楷体初号hong','text',$time_str."广告");//大标题

        $data['content'][] = create_report_array('text','楷体初号hong','text',"监测报告");//大标题

        $data['content'][] = create_report_array('text','仿宋三号段落','text',$user['regulatorname']."广告监测中心对".$time_str.$area_call['z_call']."部分".$media_types_str."等".$all_media_count."家媒体发布广告的情况进行了监测，现将监测情况通报如下：");//开场描述

        $data['content'][] = create_report_array('text','仿宋三号标题','text',"一、监测范围及对象");//第一段标题


        if($ct_media_tv_count_hz != 0){
            $hz_media_count_str .= $ct_media_tv_count_hz."个电视媒体；";
        }
        if($ct_media_bc_count_hz != 0){
            $hz_media_count_str .= $ct_media_bc_count_hz."个广播媒体；";
        }
        if($ct_media_paper_count_hz != 0){
            $hz_media_count_str .= $ct_media_paper_count_hz."个报纸媒体；";
        }

        $data['content'][] = create_report_array('text','仿宋三号段落','text',"（一）".$area_call['s_call']."媒体：".$hz_media_count_str);//本级媒体数量

        if($ct_media_tv_count_xj != 0){
            $xj_media_count_str .= $ct_media_tv_count_xj."个电视媒体；";
        }
        if($ct_media_bc_count_xj != 0){
            $xj_media_count_str .= $ct_media_bc_count_xj."个广播媒体；";
        }
        if($ct_media_paper_count_xj != 0){
            $xj_media_count_str .= $ct_media_paper_count_xj."个报纸媒体；";
        }
        $tregion_count = M('tregion')->where(['fpid'=>$user['regionid']])->count();

        $data['content'][] = create_report_array('text','仿宋三号段落','text',"（二）其他".$tregion_count."个".$area_call['x_call']."：".$xj_media_count_str);//下级级媒体数量

        if(!empty($net_media_ids)){
            $net_media_type_str = '';
            $net_media_type = $StatisticalReport->get_media_type(13);
            foreach ($net_media_type as $net_media_type_key=>$net_media_type_val){
                $net_media_type_str .= count($StatisticalReport->net_media_type_ids($net_media_type_key))."家".$net_media_type_val.';';
            }
            $data['content'][] = create_report_array('text','仿宋三号段落','text',"（三）".$net_media_type_str);//互联网媒体数量
        }

        $data['content'][] = create_report_array('text','仿宋三号标题','text',"二、各类媒体广告发布情况");//第二段标题


        $every_region_tj =  $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],$s_date,$e_date,'compre_score');


        $every_media_class_tj =  $StatisticalReport->report_ad_monitor('fmedia_class_code',$owner_media_ids,$user['regionid'],$s_date,$e_date);

        $every_media_class_data = [];
        foreach ($every_media_class_tj as $every_media_class_tj_key => $ct_jczqk_val){

            if(!$ct_jczqk_val['titlename']){
                $tmediaclass_name = $ct_jczqk_val['tmediaclass_name'];
            }else{
                $tmediaclass_name = '合计';
            }

            $every_media_class_data[] = [
                $tmediaclass_name,
                $ct_jczqk_val['fad_times'],
                $ct_jczqk_val['fad_illegal_times'],
                $ct_jczqk_val['times_illegal_rate'].'%',
                $ct_jczqk_val['fad_count'],
                $ct_jczqk_val['fad_illegal_count'],
                $ct_jczqk_val['counts_illegal_rate']?$ct_jczqk_val['counts_illegal_rate'].'%':'0.00'.'%'
            ];

        }
        $every_media_class_data = to_string($every_media_class_data);
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "本期共监测广告(包括公益广告)".$every_media_class_data[(count($every_media_class_data)-1)][1]."条次，".$every_media_class_data[(count($every_media_class_data)-1)][4]."条数，其中监测发现各类涉嫌违法广告".$every_media_class_data[(count($every_media_class_data)-1)][2]."条次，".$every_media_class_data[(count($every_media_class_data)-1)][5]."条数。具体情况详见下表："
        ];


        $data['content'][] = [
            "type" => "table",
            "bookmark" => "淄博季报媒介类型汇总表格",
            "data" => $every_media_class_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "三、".$area_call['z_call']."（各".$area_call['x_call']."）媒体广告发布总体情况（排名按综合得分由高到底排列）"
        ];
        $every_region_tj =  $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],$s_date,$e_date,'compre_score');
        $every_region_tj_tcpm =  $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],$s_date,$e_date,'fad_illegal_times');
        $every_region_data = [];
        $every_region_illegal_times_data = [
            [''],
            ['']
        ];

        //各地区综合得分排名json
        foreach ($every_region_tj as $every_region_tj_key => $every_region_tj_val){

            if(!$every_region_tj_val['titlename']){
                $every_region_data[] = [
                    $every_region_tj_key+1,
                    $every_region_tj_val['tregion_name'],
                    $every_region_tj_val['fad_times'],
                    $every_region_tj_val['fad_illegal_times'],
                    $every_region_tj_val['times_illegal_rate'].'%',
                    $every_region_tj_val['fad_count'],
                    $every_region_tj_val['fad_illegal_count'],
                    $every_region_tj_val['counts_illegal_rate']?$every_region_tj_val['counts_illegal_rate'].'%':'0.00'.'%',
                    $every_region_tj_val['fad_play_len'],
                    $every_region_tj_val['fad_illegal_play_len'],
                    $every_region_tj_val['lens_illegal_rate']?$every_region_tj_val['lens_illegal_rate'].'%':'0.00'.'%',
                    $every_region_tj_val['ck_count']?$every_region_tj_val['ck_count']:'0',
                    $every_region_tj_val['ckl']?$every_region_tj_val['ckl']:'0.00'.'%',
                    $every_region_tj_val['cl_count']?$every_region_tj_val['cl_count']:'0',
                    $every_region_tj_val['cll']?$every_region_tj_val['cll']:'0.00'.'%',
                    $every_region_tj_val['compre_score']?$every_region_tj_val['compre_score']:'0',
                ];
            }
        }
        $every_region_data = to_string($every_region_data);
        foreach ($every_region_tj_tcpm as $every_region_tj_tcpm_key => $every_region_tj_tcpm_val){
            if(!$every_region_tj_tcpm_val['titlename']){
                if($every_region_tj_tcpm_val['fad_illegal_times'] > 0){
                    array_push($every_region_illegal_times_data[0],$every_region_tj_tcpm_val['tregion_name']);
                    array_push($every_region_illegal_times_data[1],strval($every_region_tj_tcpm_val['fad_illegal_times']));
                }
            }
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号居中正文",
            "text" => $area_call['z_call']."媒体发布涉嫌广告（违法综合得分）从高到低排名"
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "淄博季报各地区汇总表格",
            "data" => $every_region_data
        ];



        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各市区县违法广告发布量排名条形图hz",
            "data" => $every_region_illegal_times_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "四、".$area_call['z_call'].$media_types_str."广告发布情况（排名按发布违法广告条次由高到底排列）"
        ];

        $zb_ds_media_count = $ct_media_tv_count_hz + $ct_media_tv_count_xj;
        $zb_gb_media_count = $ct_media_bc_count_hz + $ct_media_bc_count_xj;
        $zb_bz_media_count = $ct_media_paper_count_hz + $ct_media_paper_count_xj;

        $regionid_str = trim(session('regulatorpersonInfo.regionid'),'0');

        $pc_media_count = $StatisticalReport->net_media_counts($regionid_str,'1301');//pc媒体数量
        $app_media_count = $StatisticalReport->net_media_counts($regionid_str,'1302');//APP数量
        $gzh_media_count = $StatisticalReport->net_media_counts($regionid_str,'1303');//公众号数量

        $zb_hlw_media_count = $pc_media_count + $app_media_count +$gzh_media_count;

        $media_class_array = ['01'=>$zb_dz_media_count,'02'=>$zb_gb_media_count,'03'=>$zb_bz_media_count,'13'=>$zb_hlw_media_count];
        foreach ($media_class_array as $media_class_key=>$media_class_val){
            $every_media_data = $this->get_media_data($owner_media_ids,$user['regionid'],$s_date,$e_date,$media_class_key);
            switch ($media_class_key){
                case '01':
                    $this_mt_count = $zb_ds_media_count;
                    $this_mt_type = '电视';
                    break;
                case '02':
                    $this_mt_count = $zb_gb_media_count;
                    $this_mt_type = '广播';
                    break;
                case '03':
                    $this_mt_count = $zb_bz_media_count;
                    $this_mt_type = '报纸';
                    break;
                case '13':
                    $this_mt_count = $zb_hlw_media_count;
                    $this_mt_type = '互联网';
                    break;
            }

            if(!empty($every_media_data)){
                if(!empty($every_media_data['list'])){

                    $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => $time_str."广告监测中心对".$area_call['z_call'].$this_mt_count."家".$this_mt_type."媒体发布的广告进行了监测，共监测各类广告".$every_media_data['text']['fad_count']."条，其中涉嫌违法广告".$every_media_data['text']['fad_illegal_count']."条，条数违法率为".$every_media_data['text']['counts_illegal_rate']."；监测各类广告".$every_media_data['text']['fad_times']."条次，其中涉嫌违法广告".$every_media_data['text']['fad_illegal_times']."条次，条次违法率为".$every_media_data['text']['times_illegal_rate'].$every_media_data['no_data_media_str']."。"
                    ];
                    $data['content'][] = [
                        "type" => "table",
                        "bookmark" => "各媒体违法广告发布量排名列表zb",
                        "data" => $every_media_data['list']
                    ];

                    $data['content'][] = [
                        "type" => "chart",
                        "bookmark" => "违法广告发布量排名通用hz",
                        "data" => $every_media_data['chart']
                    ];
                }else{
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "仿宋三号段落",
                        "text" => "本期".$this_mt_type."媒体未监测到广告数据。"
                    ];
                }
            }
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、".$area_call['z_call']."各类媒体发布的违法广告（全部类别）通报（排名按发布违法广告条次由高到底排列）"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）".$area_call['s_call']."媒体"
        ];


        $mt_type_array = ['01'=>"电视",'02'=>"广播",'03'=>"报纸",'13'=>"互联网"];
        foreach ($mt_type_array as $m_key=>$m_val){
            $every_ill_data = [];
            $sj_ill_data = $StatisticalReport->report_illegal_ad_list($owner_media_ids,$s_date,$e_date,$area_call['s_flevel'],$m_key);
            if(!empty($sj_ill_data)){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => $m_val
                ];
                foreach ($sj_ill_data as $sj_ill_data_key=>$sj_ill_data_val){
                    if(!$sj_ill_data_val['titlename']){
                        $every_ill_data[] = [
                            $sj_ill_data_key+1,
                            $sj_ill_data_val['fmedianame'],
                            $sj_ill_data_val['fad_name'],
                            $sj_ill_data_val['ffullname'],
                            $sj_ill_data_val['fad_ill_times']
                        ];
                    }
                }
                $every_ill_data = to_string($every_ill_data);

                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "具体类别广告汇总列表hz",
                    "data" => $every_ill_data
                ];

            }

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）".$area_call['x_call']."媒体"
        ];

        foreach ($mt_type_array as $m_key=>$m_val){
            $every_ill_data = [];
            $xj_ill_data = $StatisticalReport->report_illegal_ad_list($owner_media_ids,$s_date,$e_date,$area_call['x_flevel'],$m_key,0);
            if(!empty($xj_ill_data)){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => $m_val
                ];
                foreach ($xj_ill_data as $xj_ill_data_key=>$xj_ill_data_val){
                    if(!$xj_ill_data_val['titlename']){
                        $every_ill_data[] = [
                            $xj_ill_data_key+1,
                            $xj_ill_data_val['fmedianame'],
                            $xj_ill_data_val['fad_name'],
                            $xj_ill_data_val['ffullname'],
                            $xj_ill_data_val['fad_ill_times']
                        ];
                    }
                }
                $every_ill_data = to_string($every_ill_data);

                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "具体类别广告汇总列表hz",
                    "data" => $every_ill_data
                ];

            }

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "六、".$area_call['z_call']."各类违法广告比重结构图（排名按发布违法广告条次由高到底排列）"
        ];

        $every_adclass_tj1 =  $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],$s_date,$e_date,'fad_illegal_times');
        $every_adclass_tj2 =  $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],$s_date,$e_date,'times_illegal_rate');

        //按违法条次排序列表
        foreach ($every_adclass_tj1 as $every_adclass_tj1_key=>$every_adclass_tj1_val){
            if(!$every_adclass_tj1_val['titlename']){
                $every_ad_class_data[] = [
                    $every_adclass_tj1_key+1,
                    $every_adclass_tj1_val['tadclass_name'],
                    $every_adclass_tj1_val['fad_times'],
                    $every_adclass_tj1_val['fad_illegal_times'],
                    $every_adclass_tj1_val['times_illegal_rate'].'%',
                    $every_adclass_tj1_val['fad_count'],
                    $every_adclass_tj1_val['fad_illegal_count'],
                    $every_adclass_tj1_val['counts_illegal_rate']?$every_adclass_tj1_val['counts_illegal_rate'].'%':'0.00'.'%'
                ];
            }
        }
        $every_ad_class_data = to_string($every_ad_class_data);

        //按违法率排序描述
        $ill_tadclass_name = [];//前三违法广告类别
        $ill_tadclass_rate = [];//前三违法率
        foreach ($every_adclass_tj2 as $every_adclass_tj2_key=>$every_adclass_tj2_val){
            if(!$every_adclass_tj2_val['titlename']){
                if($every_adclass_tj2_key < 3 && $every_adclass_tj2_val['fad_illegal_times']){
                    $ill_tadclass_name[] = $every_adclass_tj2_val['tadclass_name'];
                    $ill_tadclass_rate[] = $every_adclass_tj2_val['times_illegal_rate'].'%';
                }else{
                    break;
                }
            }
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $time_str."监测情况看涉嫌违法广告类别主要发布在：".implode($ill_tadclass_name,'、')."的广告条次违法率较高，分别为".implode($ill_tadclass_rate,'、')."，其他类别条次违法率均低于".$ill_tadclass_rate[2]."。"
        ];

        if(!empty($every_ad_class_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表zb",
                "data" => $every_ad_class_data
            ];

        }
        //[01,02,06,13]

        $zd_adclass_ill_data = $StatisticalReport->report_illegal_ad_list($owner_media_ids,$s_date,$e_date,$area_call['x_flevel'],'',1,['01','02','06','13']);

        if(!empty($zd_adclass_ill_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "七、药品、医疗服务、医疗器械、保健食品违法广告发布情况（排名按发布违法广告条次由高到底排列）"
            ];
            foreach ($zd_adclass_ill_data as $zd_adclass_ill_data_key=>$zd_adclass_ill_data_val){
                if(!$zd_adclass_ill_data_val['titlename']){
                    $zd_ill_adclass_data[] = [
                        $zd_adclass_ill_data_key+1,
                        $zd_adclass_ill_data_val['fmedianame'],
                        $zd_adclass_ill_data_val['fad_name'],
                        $zd_adclass_ill_data_val['ffullname'],
                        $zd_adclass_ill_data_val['fad_ill_times']
                    ];
                }
            }
            $zd_ill_adclass_data = to_string($zd_ill_adclass_data);

            if(!empty($zd_ill_adclass_data)){
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "具体类别广告汇总列表hz",
                    "data" => $zd_ill_adclass_data
                ];
            }
        }



        $report_data = json_encode($data);
        $pnname = $user['regulatorname'].$time_str.'报告';
        $this->report_generation($pntype,$report_data,$pnname,$user,$s_date,$e_date);//生成报告方法
        //echo $report_data;exit;//20190103

    }

    //互联网通用报告接口
    public function general_net_report(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport = New StatisticalModel();//实例化数据统计模型
        $s_date = I('s_time',date('Y-m-d'));
        $e_date = I('e_time',date('Y-m-d'));

        $user = session('regulatorpersonInfo');//获取用户信息
        $date_sta_ymd = date('Y年m月d日',strtotime($s_date));//结束年月字符
        $date_end_ymd = date('Y年m月d日',strtotime($e_date));//结束年月字符

        //查找全部媒体，筒体全部互联网媒体数据
        $net_media_ids = $StatisticalReport->net_media_type_ids('13');
        $net_media_customize = $StatisticalReport->report_ad_monitor('fmedia_class_code',$net_media_ids,$user['regionid'],$s_date,$e_date,'times_illegal_rate','13');//当前客户全部互联网广告数据统计
        //如果没有互联网数据则返回
        if(empty($net_media_customize)){
            $this->ajaxReturn([
                'code'=>1,
                'msg'=>'暂无数据'
            ]);
        }
        /*
        * @互联网各媒体监测总情况ggslyzwfl
        *
        **/
        $every_net_media_data = [];
        $every_net_media_name = [];
        $every_net_media_jczqk = $StatisticalReport->report_ad_monitor('fmediaid',$net_media_ids,$user['regionid'],$s_date,$e_date,'times_illegal_rate','13');//pc媒体发布情况

        foreach ($every_net_media_jczqk as $every_net_media_jczqk_key=>$every_net_media_jczqk_val){
            if(!$every_net_media_jczqk_val['titlename']) {
                $every_net_media_data[] = [
                    strval($every_net_media_jczqk_key + 1),
                    $every_net_media_jczqk_val['tmedia_name'],
                    $every_net_media_jczqk_val['fad_times'],
                    $every_net_media_jczqk_val['fad_illegal_times'],
                    $every_net_media_jczqk_val['times_illegal_rate'] . '%'
                ];
                if ($every_net_media_jczqk_val['fad_illegal_times'] != 0) {
                    $every_net_media_name[] = $every_net_media_jczqk_val['tmedia_name'];
                }

            }
        }
        $every_net_media_data = to_string($every_net_media_data);

        /*
        * @互联网各广告类别各媒体监测总情况
        *
        **/
        $every_ad_type_data = [];
        $every_ad_type_name = [];
        $every_ad_type_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$net_media_ids,$user['regionid'],$s_date,$e_date,'times_illegal_rate','13');//pc媒体广告发布情况

        foreach ($every_ad_type_jczqk as $every_ad_type_jczqk_key=>$every_ad_type_jczqk_val){
            if(!$every_ad_type_jczqk_val['titlename']) {
                $every_ad_type_data[] = [
                    strval($every_ad_type_jczqk_key + 1),
                    $every_ad_type_jczqk_val['tadclass_name'],
                    $every_ad_type_jczqk_val['fad_times'],
                    $every_ad_type_jczqk_val['fad_illegal_times'],
                    $every_ad_type_jczqk_val['times_illegal_rate'] . '%'
                ];
                if ($every_ad_type_jczqk_val['fad_illegal_times'] != 0) {
                    $every_ad_type_name[] = $every_ad_type_jczqk_val['tadclass_name'];
                }

            }
        }
        $every_ad_type_data = to_string($every_ad_type_data);



        //定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];
        $data['content'][] = create_report_array('text','大标题','text',$user['regulatorpname']);
        $data['content'][] = create_report_array('text','黑体三号','text','时间范围为'.$date_sta_ymd.' 至 '.$date_end_ymd);


        if($net_media_customize[1]['fad_illegal_times'] == 0){
            $wf_des = "本次监测中，没有发现违法广告";
        }else{
            $wf_des = "本次共监测广告".$net_media_customize[1]['fad_times']."条，其中涉嫌违法广告".$net_media_customize[1]['fad_illegal_times']."条；其中,";
        }//本次共监测广告11491条，其中涉嫌违法广告205条；其中门户网站广告11105条，涉嫌违法广告201条；微信公众号广告172条，涉嫌违法广告4条；移动APP广告214条，未发现涉嫌违法广告。

        $net_media_type_array = $StatisticalReport->get_media_type(13);//互联网媒体各大类

        $every_net_type_data = [];//
        $every_net_ill_data[] = ['','涉嫌违法广告条'];
        $media_des = '';
        $media_issue_data = [];
        $adclass_issue_data = [];
        $ill_media_data = [];
        $ill_detail_data = [];
        $media_all_count = 0;
        $num = 0;
        foreach ($net_media_type_array as $net_media_type_key=>$net_media_type_val){
            $num++;
            $media_count = $StatisticalReport->net_media_counts(substr($user['regionid'],0,2),$net_media_type_key);//媒体数量
            $net_media_count_str.= $media_count."家".$net_media_type_val."；";
            if($media_count >= 1){
                $media_all_count += $media_count;
                $medias = $StatisticalReport->net_media_type_ids($net_media_type_key);
                $media_customize = $StatisticalReport->report_ad_monitor('fmedia_class_code',$medias,$user['regionid'],$s_date,$e_date,'times_illegal_rate','13');//当前广告数据
                if(count($media_customize) != 1){
                    if($media_customize[1]['fad_illegal_times'] != 0){
                        $wf_des .= $net_media_type_val."广告".$media_customize[1]['fad_times']."条，涉嫌违法广告条".$media_customize[1]['fad_illegal_times']."条；";
                    }else{
                        $wf_des .= $net_media_type_val."未发现涉嫌违法广告；";
                    }
                    $every_net_ill_data[] =  [$net_media_type_val,$media_customize[1]['fad_illegal_times']?strval($media_customize[1]['fad_illegal_times']):'0'];
                    $every_net_type_data[] =  [
                        $net_media_type_val,
                        $media_customize[1]['fad_times']?strval($media_customize[1]['fad_times']):'0',
                        $media_customize[1]['fad_illegal_times']?strval($media_customize[1]['fad_illegal_times']):'0',
                        ($media_customize[1]['times_illegal_rate']?$media_customize[1]['times_illegal_rate']:'0.00').'%'
                    ];


                    $every_media_data = [];
                    $every_media_name = [];
                    $every_media_id = [];
                    $every_media_jczqk = $StatisticalReport->report_ad_monitor('fmediaid',$medias,$user['regionid'],$s_date,$e_date,'times_illegal_rate','13');//pc媒体发布情况

                    foreach ($every_media_jczqk as $every_media_jczqk_key=>$every_media_jczqk_val){
                        if(!$every_media_jczqk_val['titlename']){
                            $tmedia_name = $every_media_jczqk_val['tmedia_name'];
                            if($every_media_jczqk_val['fad_illegal_times'] != 0){
                                $every_media_name[] = $every_media_jczqk_val['tmedia_name'];
                            }
                            $every_media_id[$every_media_jczqk_val['fmediaid']] = $every_media_jczqk_val['tmedia_name'];
                            $every_media_data[] = [
                                strval($every_media_jczqk_key+1),
                                $tmedia_name,
                                strval($every_media_jczqk_val['fad_times']),
                                strval($every_media_jczqk_val['fad_illegal_times']),
                                $every_media_jczqk_val['times_illegal_rate'].'%'
                            ];
                        }else{
                            $tmedia_name = '合计';
                        }
                    }

                    $every_adclass_data = [];
                    $every_adclass_name = [];
                    $every_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$medias,$user['regionid'],$s_date,$e_date,'times_illegal_rate','13');//pc媒体广告发布情况
                    foreach ($every_adclass_jczqk as $every_adclass_jczqk_key=>$every_adclass_jczqk_val){
                        if(!$every_adclass_jczqk_val['titlename']){
                            $tadclass_name = $every_adclass_jczqk_val['tadclass_name'];
                            if($every_adclass_jczqk_val['fad_illegal_times'] != 0){
                                $every_adclass_name[] = $every_adclass_jczqk_val['tadclass_name'];
                            }
                            $every_adclass_data[] = [
                                strval($every_adclass_jczqk_key+1),
                                $tadclass_name,
                                strval($every_adclass_jczqk_val['fad_times']),
                                strval($every_adclass_jczqk_val['fad_illegal_times']),
                                $every_adclass_jczqk_val['times_illegal_rate'].'%'
                            ];
                        }else{
                            $pc_tadclass_name = '合计';
                        }
                    }

                    //发布情况标题
                    $media_issue_data[] = create_report_array('text','宋体三号标题','text',$num.'.'.$net_media_type_val);

                    //判断是否有违法数据
                    $not_hava_data = '';
                    if($media_count-count($every_media_list[1]) == 0){
                        $not_hava_data = "其中".($media_count-count($every_media_list[1]))."家未监测到发布广告,其余".count($every_media_list[1])."家";
                    }
                    $media_des .= "本期共监测".$media_count."家".$net_media_type_val.",".$not_hava_data."共发布广告".$media_customize[1]['fad_times']."条，";//

                    if($media_customize[1]['fad_illegal_times'] != 0){
                        $media_des .= '其中涉嫌违法的广告有'.$media_customize[1]['fad_illegal_times'].'条，违法率为'.$media_customize[1]['times_illegal_rate'].'%。';
                    }else{
                        $media_des .= '未发现涉嫌违法广告。';
                    }
                    //发布情况描述
                    $media_issue_data[] = create_report_array('text','仿宋三号段落','text',$media_des);

                    $every_media_list = $this->create_net_chart_data($every_media_data);
                    if(!empty($every_media_list[1])){
                        $media_issue_data[] = create_report_array('text','宋体三号居中','text',$net_media_type_val.'发布情况');
                        $media_issue_data[] = create_report_array('chart','互联网广告量条形图','data',$every_media_list[1]);
                    }
                    if(!empty($every_media_list[0])){
                        $media_issue_data[] = create_report_array('table','网站监测情况列表','data',$every_media_list[0]);
                    }


                    $adclass_issue_data[] = [
                        "type" => "text",
                        "bookmark" => "宋体三号标题",
                        "text" => $net_media_type_val."类别广告发布情况"
                    ];
                    if(empty($every_adclass_name)){
                        $every_adclass_name = '本次监测中'.$net_media_type_val.'各广告类别暂未发现涉嫌违法广告';
                        $adclass_issue_data[] = [
                            "type" => "text",
                            "bookmark" => "仿宋三号段落",
                            "text" => $every_adclass_name
                        ];
                    }else{
                        $every_adclass_name = "本次监测中".$net_media_type_val."涉嫌违法广告类别主要集中在".implode('、',$every_adclass_name)."。";
                        $adclass_issue_data[] = [
                            "type" => "text",
                            "bookmark" => "仿宋三号段落",
                            "text" => $every_adclass_name
                        ];
                    }
                    $every_adclass_list = $this->create_net_chart_data($every_adclass_data);//
                    if(!empty($every_adclass_name[1])){

                        $adclass_issue_data[] = create_report_array('text','宋体三号居中','text',$net_media_type_val.'各广告类别发布情况');

                        $adclass_issue_data[] = [
                            "type" => "chart",
                            "bookmark" => "互联网广告量条形图",
                            "data" => $every_adclass_list[1]
                        ];
                    }
                    if(!empty($every_adclass_name[0])){
                        $adclass_issue_data[] = [
                            "type" => "table",
                            "bookmark" => "广告类别监测情况列表",
                            "data" => $every_adclass_list[0]
                        ];
                    }

                    if(!empty($every_media_id)) {

                        $ill_media_data[] = [
                            "type" => "text",
                            "bookmark" => "宋体三号标题",
                            "text" => $net_media_type_val
                        ];
                        foreach ($every_media_id as $every_media_id_key => $every_media_id_val) {

                            $every_media = $StatisticalReport->report_ad_monitor('fmediaid',[$every_media_id_key],$user['regionid'],$s_date,$e_date,'times_illegal_rate','13');//pc媒体广告发布情况
                            $every_media = to_string($every_media);

                            $every_adclass = $StatisticalReport->report_ad_monitor('fad_class_code',[$every_media_id_key],$user['regionid'],$s_date,$e_date,'times_illegal_rate','13');//pc媒体广告发布情况
                            $every_adclass = to_string($every_adclass);
                            $illegal_situation = $StatisticalReport->net_illegal_situation_list($s_date,$e_date,'13',$every_media_id_key);//pc媒体广告发布情况

                            $class_name = [];
                            $fbxs_type = [];
                            $every_adclass_wfdata = [];
                            foreach ($every_adclass as $every_adclass_key => $every_adclass_val) {

                                $class_name[] = $every_adclass_val['tadclass_name'];
                                $every_adclass_wfdata[] = [
                                    strval($every_adclass_key + 1),
                                    $every_adclass_val['tadclass_name']?$every_adclass_val['tadclass_name']:$every_adclass_val['titlename'],
                                    $every_adclass_val['fad_times'],
                                    $every_adclass_val['fad_illegal_times'],
                                    $every_adclass_val['times_illegal_rate'] . '%'
                                ];

                            }
                            $illegal_situation_data = [];
                            foreach ($illegal_situation as $illegal_situation_key => $illegal_situation_val) {

                                $illegal_situation_data[] = [
                                    strval($illegal_situation_key + 1),
                                    $illegal_situation_val['fadclass'],
                                    $illegal_situation_val['fillegalcontent']
                                ];

                            }

                            $class_name = implode('、', $class_name);

                            //TODO::0717
                            $ill_media_data[] = [
                                "type" => "text",
                                "bookmark" => "宋体三号数字序号标题",
                                "text" => $every_media_id_val
                            ];

                            $ill_media_data[] = [
                                "type" => "text",
                                "bookmark" => "仿宋三号段落",
                                "text" => "本次监测【" . $every_media_id_val . "】涉嫌违法发布广告" . $every_media[1]['fad_illegal_times'] . "条，监测发现" . $every_media[1]['fad_times'] . "条，主要发布类别" . $class_name . "。"
                            ];

                            $ill_media_data[] = [
                                "type" => "table",
                                "bookmark" => "单媒体详情列表无广告形式",
                                "data" => $every_adclass_wfdata
                            ];
                            if(!empty($illegal_situation_data)) {
                                $ill_media_data[] = [
                                    "type" => "text",
                                    "bookmark" => "仿宋三号居中",
                                    "text" => "该".$net_media_type_val."广告主要违法情形"
                                ];
                                $ill_media_data[] = [
                                    "type" => "table",
                                    "bookmark" => "单媒体违法情形列表",
                                    "data" => $illegal_situation_data
                                ];
                            }
                        }
                    }

                    $illegal_detail_data = $StatisticalReport->report_illegal_ad_list($medias,$s_date,$e_date,$user['regionid']);
                    $illegal_detail_data = to_string($illegal_detail_data);
                    if(!empty($illegal_detail_data)){
                        $ill_detail_data[] = [
                            "type" => "text",
                            "bookmark" => "宋体三号标题",
                            "text" => $net_media_type_val
                        ];
                        foreach ($illegal_detail_data as $illegal_detail_data_key => $illegal_detail_data_val){
                            $illegal_detail = $StatisticalReport->report_illegal_detail($illegal_detail_data_val['fsample_id']);

                            $img = htmlspecialchars_decode($illegal_detail['net_snapshot']);

                            $illegal_img = ['index'=>18,'type'=>'text','content'=>''];

                            if(is_img_link($img)){
                                $illegal_img = ['index'=>18,'type'=>'text','content'=>$img];
                            }

                            $fillegalcontent = ['index'=>12,'type'=>'text','content'=>str_replace("&nbsp;","",strip_tags(htmlspecialchars_decode($illegal_detail['fillegalcontent'])))];

                            $illegal_detail_list = [
                                ['index'=>2,'type'=>'text','content'=>$illegal_detail_data_val['fmedianame']],
                                ['index'=>4,'type'=>'text','content'=>$illegal_detail_data_val['ffullname']],
                                ['index'=>6,'type'=>'text','content'=>$illegal_detail_data_val['fad_ill_times'].'次'],
                                ['index'=>8,'type'=>'text','content'=>$illegal_detail['net_original_url']?$illegal_detail['net_original_url']:''],
                                ['index'=>10,'type'=>'text','content'=>$illegal_detail['net_target_url']?$illegal_detail['net_target_url']:''],
                                $fillegalcontent,
                                ['index'=>14,'type'=>'text','content'=>$illegal_detail_data_val['fexpressions']?$illegal_detail_data_val['fexpressions']:''],
                                ['index'=>16,'type'=>'text','content'=>get_confirmation_by_code_string($illegal_detail_data_val['fillegal_code'])],
                                $illegal_img,
                                ['index'=>20,'type'=>'text','content'=>$illegal_detail['thumb_url_true']?$illegal_detail['thumb_url_true']:'']
                            ];
                            $ill_detail_data[] = [
                                "type" => "text",
                                "bookmark" => "宋体三号数字序号标题",
                                "text" => ($illegal_detail_data_key+1).'、'.$illegal_detail_data_val['fad_name']
                            ];
                            $ill_detail_data[] = [
                                "type" => "filltable",
                                "bookmark" => "通用报告填表数据",//{"index":2,"type":"text","content":"xm"},
                                "data" => $illegal_detail_list
                            ];
                        }
                    }
                }
            }
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号",
            "text" => "本期共监测".$media_all_count.'家互联网媒体，共发布广告'.$net_media_customize[1]['fad_times'].'条，涉嫌违法广告'.$net_media_customize[1]['fad_illegal_times'].'条，违法率'.$net_media_customize[1]['times_illegal_rate']."%。；其中".$media_des
        ];

        if(empty($every_ad_type_name)){
            $every_ad_type_name = '本期各广告类别暂未发现违法广告';
        }else{
            $every_ad_type_name = "违法广告集中在".implode('、',$every_ad_type_name);
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号",
            "text" => $every_ad_type_name
        ];
        if(empty($every_net_media_name)){
            $every_net_media_name = '本期各互联网媒体暂未发现违法广告';
        }else{
            $every_net_media_name = "违法广告集中在".implode('、',$every_net_media_name)."中发布。";
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号",
            "text" => $every_net_media_name
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号加粗居中",
            "text" => $user['regulatorpname']."广告监测中心"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号加粗居中",
            "text" => $date_end_ymd
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号标题",
            "text" => "一、监测总体情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "此次共监测".$net_media_count_str
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "监测情况总条形图",//互联网
            "data" => $every_net_ill_data
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "监测情况总列表",
            "data" => $every_net_type_data
        ];

        //各媒体发布情况
        foreach ($media_issue_data as $media_issue_data_val){
            $data['content'][] = $media_issue_data_val;
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号标题",
            "text" => "二、主要类别广告发布情况"
        ];
        $net_type_str = '';
        $net_type_all_count = 0;
        $net_type_all_chart[] = ['','发布量'];
        foreach ($every_net_type_data as $net_type_key=>$net_type_val){
            $net_type_all_count += $net_type_val[1];
            $net_type_str .= $net_type_val[0].'广告'.$net_type_val[1].'条';
            if(($net_type_key+1) == count($every_net_type_data)){
                $net_type_str .= '。';
            }else{
                $net_type_str .= ',';
            }
            $net_type_all_chart[] = [
                $net_type_val[0],
                $net_type_val[1]
            ];
        }

        $data['content'][] = create_report_array('text','仿宋三号段落','text',"本次共监测广告".$net_type_all_count."条，其中".$net_type_str);
        $data['content'][] = create_report_array('chart','互联网广告发布数占比','data',$net_type_all_chart);

        //主要类别发布情况
        foreach ($adclass_issue_data as $adclass_issue_data_val){
            $data['content'][] = $adclass_issue_data_val;
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号标题",
            "text" => "三、涉嫌违法广告分析"
        ];



        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $wf_des
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "违法广告量占比图",
            "data" => $every_net_ill_data
        ];

        foreach ($ill_media_data as $ill_media_data_val){
            $data['content'][] = $ill_media_data_val;
        }

        //五、违法广告明细
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号标题",
            "text" => '五、违法广告明细'
        ];

        //组装违法明细所属数据
        foreach ($ill_detail_data as $ill_detail_data_val){
            $data['content'][] = $ill_detail_data_val;
        }


        $report_data = json_encode($data,true);//生成报告所需json
        //echo $report_data;exit;

        //报告名称
        $pnname = $user['regulatorname'].date('Y年m月d日',strtotime($s_date)).'至'.date('Y年m月d日',strtotime($e_date)).'互联网报告';
        $this->report_generation(40,$report_data,$pnname,$user,$s_date,$e_date);//生成报告

    }

    /*以下为分离方法，为上方主方法服务*/

    //生成日报表XLS open_search明细数据导出方法
    public  function day_report_os($daytime='2019-01-01',$illtype=-1,$mclass=-1){
        $ALL_CONFIG = getconfig('ALL');

        ini_set('memory_limit','1024M');
        if($daytime == ''){
            $daytime =  date('Y-m-d');
        }
        $bdmedias = get_owner_media_ids();//媒体权限组
        $user = session('regulatorpersonInfo');//获取用户信息
        //每次只取一个媒体的数据（为了防止满5000缺数据）

        foreach ($bdmedias as $key => $bdmedia){
            $mids[] = $bdmedia;
            if(count($mids) >= 10 || ($key+1) == count($bdmedias)){
                $data[] = $this->day_data($illtype,$mclass,$daytime,$mids);
                $mids = [];
            }
        }

        $issueList = [];
        foreach ($data as $data_val){
            $issueList = array_merge($issueList,$data_val);
        }

        //判断是否是数据导出
        if(!$issueList || empty($issueList)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
        }
        $excel_url = $this->out_excel($issueList);
        if($illtype == 0){
            $illtype_name = '不违法';
        }elseif($illtype == 20){
            $illtype_name = '违法';
        }elseif($illtype == 30){
            $illtype_name = '违法';
        }else{
            $illtype_name = '全部';
        }
        $media_type_name = '';
        switch (intval($mclass)){
            case 1:
                $media_type_name = '电视';
                break;
            case 2:
                $media_type_name = '广播';
                break;
            case 3:
                $media_type_name = '报纸';
                break;
            case 13:
                $media_type_name = '互联网';
                break;
        }

        $system_num = getconfig('system_num');

        $pnname = date('Y年m月d',strtotime($daytime)).'日'.$media_type_name.$illtype_name.'广告发布情况表';
        $data['pnname'] 			= $pnname;
        $data['pntype'] 			= 10;
        $data['pnfiletype'] 		= 30;
        $data['pnstarttime'] 		= $daytime;
        $data['pnendtime'] 		    = $daytime;
        $data['pncreatetime'] 		= date('Y-m-d H:i:s');
        $data['pnurl'] 				= $excel_url;
        $data['pnhtml'] 			= '';
        $data['pnfocus']            = 0;
        $data['pntrepid']           = $user['fregulatorpid'];
        $data['pncreatepersonid']   = $user['fid'];
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$excel_url));
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'生成失败'));
        }

    }

    //生成日报表XLS 非open_search明细数据导出方法（发布数据）
    public function day_report_unos($date='2019-01-01',$illtype=-1,$mclass=-1){
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        session_write_close();
        ini_set('memory_limit','1024M');
        header("Content-type: text/html; charset=utf-8");
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        if($date == ''){
            $date =  date('Y-m-d');
        }
        switch ($illtype){
            case -1:
                $ill_type = 'fillegaltypecode IN(0,30)';
                break;
            case 0:
                $ill_type = 'fillegaltypecode = 0';
                break;
            case 30:
                $ill_type = 'fillegaltypecode > 0';
                break;
        }

        $usermedia = M('tmedia_temp')->where('ftype=1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid'))->getField('fmediaid',true);

        $regulatorpersonInfo = M('tregulatorperson')
            ->field('fregulatorid as bumen_id,fid')
            ->where('fid='.session('regulatorpersonInfo.fid'))->find();
        if(!empty($regulatorpersonInfo)){
            $regulatorInfo = M('tregulator')->where(array('fid' => $regulatorpersonInfo['bumen_id']))->find();//监管机构信息
            $regulatorpersonInfo['fregulatorid']        = $regulatorInfo['fid'];//部门ID
            $regulatorpersonInfo['fregulatorpid']       = D('Function')->get_tregulatoraction($regulatorInfo['fid']);//获取当前单位所属机构ID
            $do_tr = M('tregulator')->field('tregulator.fname,tregion.fname as regionname,tregion.fname1 as regionname1,tregulator.fregionid')->join('tregion on tregulator.fregionid=tregion.fid')->where(array('tregulator.fid' => $regulatorpersonInfo['fregulatorpid']))->find();//获取机构信息
            $regulatorpersonInfo['regulatorpname']      = $do_tr['fname'];//所属机构名称
            $regulatorpersonInfo['regionid']            = $do_tr['fregionid'];//机构行政区划ID
            $regulatorpersonInfo['regionname']          = $do_tr['regionname'];//机构行政区划名称
            $regulatorpersonInfo['regionname1']         = $do_tr['regionname1'];//机构行政区划全名称
            $regulatorpersonInfo['regulator_regionid']  = $regulatorInfo['fregionid'];//部门行政区划ID
            $regulatorpersonInfo['regulatorname']       = $regulatorInfo['fname'];//部门名称
//用户媒介权限
            $mediajurisdiction = D('Function')->get_mediajurisdiction($regulatorInfo['fid'],[],[],$regulatorpersonInfo['fid']);//获取媒体权限组
            $regulatorpersonInfo['mediajurisdiction']   = $mediajurisdiction?$mediajurisdiction:array('0');

            $system_num = getconfig('system_num');
            $where_tt['freg_id'] = session('regulatorpersonInfo.fregulatorpid');
            $where_tt['fcustomer'] = $system_num;
            $do_tt = M('tbn_media_grant')->field('fmedia_id')->where($where_tt)->select();
            foreach ($do_tt as $key => $value) {
                array_push($regulatorpersonInfo['mediajurisdiction'], $value['fmedia_id']);
            }
        }
        $regionid = $regulatorpersonInfo['regionid'];//机构行政区划ID
        $regionid = substr($regionid , 0 , 2);
        $arr = explode("-" ,$date);
        $now_date = $arr[0].$arr[1].'_'.$regionid;//当月
        /*序号	媒体类别	媒体名称	发行日期	广告名称	广告类别	品牌	生产厂家	发布位置	违法类型	涉嫌违法内容	违法表现代码	违法表现	建样日期*/
        if($mclass == 1){
            $mt_name = '电视';
            $tv_table = 'ttvissue_'.date('Y',strtotime($date)).'_'.$regulatorpersonInfo['regionid'];
            $ttvsample = 'ttvsample_'.$regulatorpersonInfo['regionid'];
            $issues = 'select tv.fmediaid,tv.fsampleid,tc.fclass, (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,FROM_UNIXTIME(tv.fissuedate, "%Y-%m-%d") AS fissuedate,tad.fadname,ac.ffullname as fadclass,
tad.fbrand,tvs.fadent,tvs.fillegaltypecode,tvs.fillegalcontent,tvs.fexpressioncodes,tvs.fexpressions,left(tvs.fcreatetime,11) as fcreatetime,
concat_ws("-",FROM_UNIXTIME(tv.fstarttime, "%h:%i:%s"),FROM_UNIXTIME(tv.fendtime, "%h:%i:%s")) as fplace
from '.$tv_table.' as tv
join '.$ttvsample.' tvs on tvs.fid=tv.fsampleid
join tad on tad.fadid=tvs.fadid and tad.fadid<>0
join tmedia as tm on tm.fid=tvs.fmediaid
join tmediaclass as tc on tc.fid = left(tm.fmediaclassid,2)
join tadclass as ac on ac.fcode = left(tad.fadclasscode,2)
where tv.fmediaid in ('.join(',',$usermedia).')
and datediff("'.$date.'",FROM_UNIXTIME(tv.fissuedate, "%Y-%m-%d"))=0
and tvs.'.$ill_type;
        }elseif($mclass == 2){
            $mt_name = '广播';
            $tb_table = 'tbcissue_'.date('Y',strtotime($date)).'_'.$regulatorpersonInfo['regionid'];
            $tbcsample = 'tbcsample_'.$regulatorpersonInfo['regionid'];
            $issues = 'select tb.fmediaid,tb.fsampleid,tc.fclass, (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,FROM_UNIXTIME(tb.fissuedate, "%Y-%m-%d") AS fissuedate,tad.fadname,ac.ffullname as fadclass,tad.fbrand,tbs.fadent,tbs.fillegaltypecode,tbs.fillegalcontent,tbs.fexpressioncodes,tbs.fexpressions,left(tbs.fcreatetime,11) as fcreatetime,
concat_ws("-",FROM_UNIXTIME(tb.fstarttime, "%h:%i:%s"),FROM_UNIXTIME(tb.fendtime, "%h:%i:%s")) as fplace
from '.$tb_table.' as tb
join '.$tbcsample.' tbs on tbs.fid=tb.fsampleid
join tad on tad.fadid=tbs.fadid and tad.fadid<>0
join tmedia as tm on tm.fid=tbs.fmediaid
join tmediaclass as tc on tc.fid = left(tm.fmediaclassid,2)
join tadclass as ac on ac.fcode = left(tad.fadclasscode,2)
where tb.fmediaid in ('.join(',',$usermedia).')
and datediff("'.$date.'",FROM_UNIXTIME(tb.fissuedate, "%Y-%m-%d"))=0
and tbs.'.$ill_type;
        }elseif($mclass == 3){
            $mt_name = '报纸';
            $tp_table = 'tpaperissue_'.$regulatorpersonInfo['regionid'];
            $tpapersample = 'tbcsample_'.$regulatorpersonInfo['regionid'];
            $issues = 'select tp.fmediaid,tp.fpapersampleid as fsampleid,tc.fclass, (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,left(tp.fissuedate,11)as fissuedate,tad.fadname,ac.ffullname as fadclass,tad.fbrand,tps.fadent,tps.fillegaltypecode,tps.fillegalcontent,tps.fexpressioncodes,tps.fexpressions,left(tps.fcreatetime,11) as fcreatetime,tp.fpage as fplace
from '.$tp_table.' as tp
join '.$tpapersample.' tps on tps.fpapersampleid=tp.fpapersampleid
join tad on tad.fadid=tps.fadid and tad.fadid<>0
join tmedia as tm on tm.fid=tps.fmediaid
join tmediaclass as tc on tc.fid = left(tm.fmediaclassid,2)
join tadclass as ac on ac.fcode = left(tad.fadclasscode,2)
where tp.fmediaid in ('.join(',',$usermedia).')
and datediff("'.$date.'",tp.fissuedate)=0
and tps.'.$ill_type;
        }elseif($mclass == 13){
            $mt_name = '互联网';
            $issues = 'select tn.fmediaid,tn.tid as fsampleid,tc.fclass, (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,FROM_UNIXTIME(tn.net_created_date/1000, "%Y-%m-%d") AS fissuedate,tns.fadname,ac.ffullname as fadclass,tns.fbrand,tns.fadent,tns.fillegaltypecode,tns.fillegalcontent,tns.fexpressioncodes,null as fexpressions,FROM_UNIXTIME(tn.net_created_date/1000, "%Y-%m-%d") as fcreatetime,
concat_ws("-",tn.net_x,tn.net_y) as fplace
from tnetissueputlog as tn
join tnetissue tns on tn.tid=tns.major_key  and finputstate=2
join tmedia as tm on tm.fid=tns.fmediaid
join tmediaclass as tc on tc.fid = left(tm.fmediaclassid,2)
join tadclass as ac on ac.fcode = left(tns.fadclasscode,2)
where tn.fmediaid in ('.join(',',$usermedia).')
and datediff("'.$date.'",FROM_UNIXTIME(tn.net_created_date/1000, "%Y-%m-%d"))=0
and tbs.'.$ill_type;
        }elseif($mclass == -1){
            $issues = 'select tv.fmediaid,tv.fsampleid,tc.fclass, (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,FROM_UNIXTIME(tv.fissuedate, "%Y-%m-%d") AS fissuedate,tad.fadname,ac.ffullname as fadclass,
        tad.fbrand,tvs.fadent,tvs.fillegaltypecode,tvs.fillegalcontent,tvs.fexpressioncodes,tvs.fexpressions,left(tvs.fcreatetime,11) as fcreatetime,
        concat_ws("-",FROM_UNIXTIME(tv.fstarttime, "%h:%i:%s"),FROM_UNIXTIME(tv.fendtime, "%h:%i:%s")) as fplace
        from ttvissue_'.$now_date.' as tv
        join ttvsample tvs on tvs.fid=tv.fsampleid
        join tad on tad.fadid=tvs.fadid and tad.fadid<>0
        join tmedia as tm on tm.fid=tvs.fmediaid
        join tmediaclass as tc on tc.fid = left(tm.fmediaclassid,2)
        join tadclass as ac on ac.fcode = left(tad.fadclasscode,2)
        where tv.fmediaid in ('.join(',',$usermedia).')
        and datediff("'.$date.'",FROM_UNIXTIME(tv.fissuedate, "%Y-%m-%d"))=0

        union all (
        select tb.fmediaid,tb.fsampleid,tc.fclass, (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,FROM_UNIXTIME(tb.fissuedate, "%Y-%m-%d") AS fissuedate,tad.fadname,ac.ffullname as fadclass,tad.fbrand,tbs.fadent,tbs.fillegaltypecode,tbs.fillegalcontent,tbs.fexpressioncodes,tbs.fexpressions,left(tbs.fcreatetime,11) as fcreatetime,
        concat_ws("-",FROM_UNIXTIME(tb.fstarttime, "%h:%i:%s"),FROM_UNIXTIME(tb.fendtime, "%h:%i:%s")) as fplace
        from tbcissue_'.$now_date.' as tb
        join tbcsample tbs on tbs.fid=tb.fsampleid
        join tad on tad.fadid=tbs.fadid and tad.fadid<>0
        join tmedia as tm on tm.fid=tbs.fmediaid
        join tmediaclass as tc on tc.fid = left(tm.fmediaclassid,2)
        join tadclass as ac on ac.fcode = left(tad.fadclasscode,2)
        where tb.fmediaid in ('.join(',',$usermedia).')
        and datediff("'.$date.'",FROM_UNIXTIME(tb.fissuedate, "%Y-%m-%d"))=0)
        union all (
        select tp.fmediaid,tp.fpapersampleid as fsampleid,tc.fclass, (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,left(tp.fissuedate,11)as fissuedate,tad.fadname,ac.ffullname as fadclass,tad.fbrand,tps.fadent,tps.fillegaltypecode,tps.fillegalcontent,tps.fexpressioncodes,tps.fexpressions,left(tps.fcreatetime,11) as fcreatetime,tp.fpage as fplace
        from tpaperissue as tp
        join tpapersample tps on tps.fpapersampleid=tp.fpapersampleid
        join tad on tad.fadid=tps.fadid and tad.fadid<>0
        join tmedia as tm on tm.fid=tps.fmediaid
        join tmediaclass as tc on tc.fid = left(tm.fmediaclassid,2)
        join tadclass as ac on ac.fcode = left(tad.fadclasscode,2)
        where tp.fmediaid in ('.join(',',$usermedia).')
        and datediff("'.$date.'",tp.fissuedate)=0)
        union all (
        select tn.fmediaid,tn.tid as fsampleid,tc.fclass, (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,FROM_UNIXTIME(tn.net_created_date/1000, "%Y-%m-%d") AS fissuedate,tns.fadname,ac.ffullname as fadclass,tns.fbrand,tns.fadent,tns.fillegaltypecode,tns.fillegalcontent,tns.fexpressioncodes,null as fexpressions,FROM_UNIXTIME(tn.net_created_date/1000, "%Y-%m-%d") as fcreatetime,
        concat_ws("-",tn.net_x,tn.net_y) as fplace
        from tnetissueputlog as tn
        join tnetissue tns on tn.tid=tns.major_key  and finputstate=2
        join tmedia as tm on tm.fid=tns.fmediaid
        join tmediaclass as tc on tc.fid = left(tm.fmediaclassid,2)
        join tadclass as ac on ac.fcode = left(tns.fadclasscode,2)
        where tn.fmediaid in ('.join(',',$usermedia).')
        and datediff("'.$date.'",FROM_UNIXTIME(tn.net_created_date/1000, "%Y-%m-%d"))=0)
        ';
        }
        $data = M()->query($issues);//当月总发布量
        if(empty($data)){
            $this->ajaxReturn(array('code'=>-1,'msg'=>'当前选择条件下无数据'));
        }
        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        /*序号	媒体类别	媒体名称	发行日期	广告名称	广告类别	品牌	生产厂家	发布位置	违法类型	涉嫌违法内容	违法表现代码	违法表现	建样日期
        */
        $sheet ->setCellValue('A1','序号');
        $sheet ->setCellValue('B1','媒体类别');
        $sheet ->setCellValue('C1','媒体名称');
        $sheet ->setCellValue('D1','发行日期');
        $sheet ->setCellValue('E1','广告名称');
        $sheet ->setCellValue('F1','广告类别');
        $sheet ->setCellValue('G1','品牌');
        $sheet ->setCellValue('H1','生产厂家');
        $sheet ->setCellValue('I1','发布位置');
        $sheet ->setCellValue('J1','违法类型');
        $sheet ->setCellValue('K1','涉嫌违法内容');
        $sheet ->setCellValue('L1','违法表现代码');
        $sheet ->setCellValue('M1','违法表现');
        $sheet ->setCellValue('N1','建样日期');
        foreach ($data as $key => $value) {
            $sheet ->setCellValue('A'.($key+2) ,$key+1);//序号
            $sheet ->setCellValue('B'.($key+2) ,$value['fclass']);//媒体类别
            $sheet ->setCellValue('C'.($key+2) ,$value['fmedianame']);//媒体名称
            $sheet ->setCellValue('D'.($key+2) ,$value['fissuedate']);//发行日期
            $sheet ->setCellValue('E'.($key+2) ,$value['fadname']);//广告名称
            $sheet ->setCellValue('F'.($key+2) ,$value['fadclass']);//广告类别
            $sheet ->setCellValue('G'.($key+2) ,$value['fbrand']);//品牌
            $sheet ->setCellValue('H'.($key+2) ,$value['fadent']);//生产厂家
            $sheet ->setCellValue('I'.($key+2) ,$value['fplace']);//发布位置
            if($value['fillegaltypecode'] == 20){
                $fillegaltype = '违法';
            }elseif($value['fillegaltypecode'] == 30){
                $fillegaltype = '违法';
            }else{
                $fillegaltype = '不违法';
            }
            $sheet ->setCellValue('J'.($key+2) ,$fillegaltype);//违法类型
            $sheet ->setCellValue('K'.($key+2) ,$value['fillegalcontent']);//涉嫌违法内容
            $sheet ->setCellValue('L'.($key+2) ,$value['fexpressioncodes']);//违法表现代码
            $sheet ->setCellValue('M'.($key+2) ,$value['fexpressions']);//违法表现
            $sheet ->setCellValue('N'.($key+2) ,$value['fcreatetime']);//建样日期

        }
        $objActSheet =$objPHPExcel->getActiveSheet();

//给当前活动的表设置名称
        $objActSheet->setTitle('Sheet1');

        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $date_now = $date;
        $date = date('Ymdhis');
        $savefile = './Public/Xls/'.$date.'.xlsx';
        $objWriter->save($savefile);

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

        if($illtype == 0){
            $illtype_name = '不违法';
        }elseif($illtype == 20){
            $illtype_name = '违法';
        }elseif($illtype == 30){
            $illtype_name = '违法';
        }

        $system_num = getconfig('system_num');

//将生成记录保存
        $pnname = $regulatorpname.date('Y年m月d',strtotime($date_now)).'日'.$mt_name.$illtype_name.'广告发布情况表';
        $data['pnname'] 			= $pnname;
        $data['pntype'] 			= 10;
        $data['pnfiletype'] 		= 30;
        $data['pnstarttime'] 		= $date_now;
        $data['pnendtime'] 		    = $date_now;
        $data['pncreatetime'] 		= date('Y-m-d H:i:s');
        $data['pnurl'] 				= $ret['url'];
        $data['pnhtml'] 			= '';
        $data['pnfocus']            = 0;
        $data['pntrepid']           = $regulatorpersonInfo['fregulatorpid'];
        $data['pncreatepersonid']   = $regulatorpersonInfo['fid'];
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'生成失败'));
        }
    }

    //导出日报excel表格
    public function out_excel($data)
    {
        ini_set('memory_limit','1024M');
        header("Content-type: text/html; charset=utf-8");
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        $sheet ->setCellValue('A1','序号');
        $sheet ->setCellValue('B1','广告名称');
        $sheet ->setCellValue('C1','广告内容类别');
        $sheet ->setCellValue('D1','发布媒体');
        $sheet ->setCellValue('E1','媒体类型');
        $sheet ->setCellValue('F1','违法类型');
        $sheet ->setCellValue('G1','发布时间');
        $sheet ->setCellValue('H1','开始时间');
        $sheet ->setCellValue('I1','结束时间');
        $sheet ->setCellValue('J1','时长/版面');
        $sheet ->setCellValue('K1','违法代码');
        $sheet ->setCellValue('L1','涉嫌违法内容');

        $arr = [1=>'电视',2=>'广播',3=>'报刊',13=>'互联网'];

        foreach ($data as $key => $value) {
            $sheet ->setCellValue('A'.($key+2) ,$key+1);
            $sheet ->setCellValue('B'.($key+2) ,$value['fadname']);
            $sheet ->setCellValue('C'.($key+2) ,$value['fadclass']);
            $sheet ->setCellValue('D'.($key+2) ,$value['fmedianame']);
            if($value['mclass'] == 1){
                $sheet ->setCellValue('E'.($key+2) ,$arr[1]);
            }elseif($value['mclass'] == 2){
                $sheet ->setCellValue('E'.($key+2) ,$arr[2]);
            }elseif($value['mclass'] == 3){
                $sheet ->setCellValue('E'.($key+2) ,$arr[3]);
            }elseif($value['mclass'] == 13){
                $sheet ->setCellValue('E'.($key+2) ,$arr[13]);
            }
            $sheet ->setCellValue('F'.($key+2) ,$value['fillegaltype']);
            $sheet ->setCellValue('G'.($key+2) ,$value['fissuedate']);
            if($value['mclass'] == 1 || $value['mclass'] == 2){
                $sheet ->setCellValue('H'.($key+2) ,$value['fstarttime']);
                $sheet ->setCellValue('I'.($key+2) ,$value['fendtime']);
            }else{
                $sheet ->setCellValue('H'.($key+2) ,' ');
                $sheet ->setCellValue('I'.($key+2) ,' ');
            }
            if($value['mclass'] == 1 || $value['mclass'] == 2){
                $sheet ->setCellValue('J'.($key+2) ,$value['flength']);
            }elseif($value['mclass'] == 3){
                $sheet ->setCellValue('J'.($key+2) ,$value['fpage']);
            }else{
                $sheet ->setCellValue('J'.($key+2) ,' ');
            }
            $sheet ->setCellValue('K'.($key+2) ,$value['fexpressioncodes']);
            $sheet ->setCellValue('L'.($key+2) ,$value['fillegalcontent']);

        }

        $objActSheet =$objPHPExcel->getActiveSheet();
        //给当前活动的表设置名称
        $objActSheet->setTitle('Sheet1');

        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $date = date('Ymdhis');
        $savefile = './Public/Xls/'.$date.'.xlsx';
        $objWriter->save($savefile);
        D('Function')->write_log('监测数据',1,'excel导出成功');

        $ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件
        return $ret['url'];
    }

    //日报数据，循环查询OPENSEARCH
    public function day_data($illtype = -1,$mclass=-1,$daytime='2018-11-23',$bdmedias = [])
    {
        session_write_close();
        header('Access-Control-Allow-Origin:*');//允许跨域
        $system_num = getconfig('system_num');

        $p = 1;//当前第几页
        $pp = 500;//每页显示多少记录
        $forcount = -1;


        $client = A('Common/OpenSearch','Model')->client();//获得OpenSearchClient
        $searchClient = new SearchClient($client);
        $params = new SearchParamsBuilder();

        $params->setAppName(C('OPEN_SEARCH_CUSTOMER'));//设置应用名称

        $where = array();

        if($illtype != -1) $where['fillegaltypecode'] = $illtype;//搜索违法类别

        if($mclass != -1) $where['fmediaclassid'] = intval($mclass);//搜索媒介类别

        $where['fissuedate'] = strtotime($daytime);

        $forgid = strlen($system_num) == 6 ? '20'.$system_num:$system_num;
        $where['forgid'] = $forgid;

        $setQuery = A('Common/OpenSearch','Model')->setQuery($where);//组装搜索字句

        $whereFilter = array();
        $params->setQuery($setQuery);//设置搜索
        $params->addSort('fissuedate', SearchParamsBuilder::SORT_DECREASE);//降序排序
        $params->setFormat("json");//数据返回json格式

        //获取数据的所有媒体
        $params->addDistinct(array('key' => 'fmediaid', 'distTimes' => 1, 'distCount' => 1, 'reserved' => 'false'));//设置排重字段
        $params->setStart(0);//起始位置
        $params->setHits(500);//返回数量
        $params->setFetchFields(
            array(
                'fmediaid',//媒介id
            )
        );//设置需返回哪些字段
        $ret = $searchClient->execute($params->build());
        $result = json_decode($ret->result,true);
        $medialist = $result['result']['items'];
        $medias = [];
        foreach ($medialist as $key => $value) {
            $medias[] = $value['fmediaid'];
        }
        if(!empty($medias)){
            $fmedias1 = array_diff($bdmedias,array_diff($bdmedias,$medias));//本地有的媒体权限
            $fmedias2 = array_diff($medias,$bdmedias);//opensearch所有数据的媒体，本地没有的媒体
            if(count($fmedias1)<=250){
                $whereFilter['fmediaid'] = array('in',$bdmedias);
            }elseif(count($fmedias2)<=250){
                $whereFilter['fmediaid'] = array('notin',$fmedias2);
            }else{
                $whereFilter['fmediaid'] = array('=',0);
            }
            $setFilter = A('Common/OpenSearch','Model')->setFilter($whereFilter);//组装过滤条件
            $params->setFilter($setFilter);//设置文档过滤条件
            $params->addDistinct(array('key' => 'fmediaid', 'distTimes' => 1, 'distCount' => 1, 'reserved' => 'true'));//清除排重字段
        }else{
            return [];
        }

        $params->setFetchFields(
            array(
                'identify',//识别码（哈希）
                'fmediaclassid',//媒体类型
                'fmediaid',//媒介id
                'fmedianame',//媒介名称
                'fadclasscode', //广告类别id
                'fadname',//广告名称
                'fissuedate', //发布日期
                'flength',//发布时长
                'fpage',//版面
                'fstarttime', //发布开始时间戳
                'fendtime', //发布开始时间戳
                'fillegaltypecode', //违法类型
                'fexpressioncodes', //违法代码
                'fillegalcontent', //违法内容
            )
        );//设置需返回哪些字段
        $params->addDistinct();//清除排重字段
        $issueList = array();
        $a = 1;
        $adClassData = M('tadclass')->cache(3600,true)->field('fcode,ffullname')->where(['fstate'=>1])->select();
        $adClassArr = array_column($adClassData, 'ffullname','fcode');//广告类别
        while($forcount!=0){
            $params->setStart(($p-1)*$pp);//起始位置
            $params->setHits($pp);//返回数量
            $ret = $searchClient->execute($params->build());//执行查询并返回信息
            $result = json_decode($ret->result,true);
            $issueList0 = $result['result']['items'];
            foreach($issueList0 as $issu){

                $issu['fstarttime'] = date('H:i:s',$issu['fstarttime']);
                $issu['fendtime'] = date('H:i:s',$issu['fendtime']);
                $issu['fissuedate'] = date('Y-m-d',$issu['fissuedate']);
                $aa = $issu['fadclasscode'];
                $adclasscode_arr = explode(sprintf("%c", 9),$issu['fadclasscode'])[0]?$adClassArr[explode(sprintf("%c", 9),$issu['fadclasscode'])[0]]:'';
                $issu['fadclass'] = $adclasscode_arr;

                if($issu['fillegaltypecode'] == '0'){
                    $issu['fillegaltype'] = '不违法';
                }else{
                    $issu['fillegaltype'] = '违法';
                }
                $issu['mclass'] = $issu['fmediaclassid'];
                $issu['sid'] = $issu['identify'];
                $issueList[] = $issu;
                //销毁无用字段
                unset($issu['fmediaclassid']);
                unset($issu['identify']);
            }
            $p++;
            //循环导出数据专用，上限5000条
            if($forcount<0){
                $forcount = ceil($result['result']['total']/$pp);
            }
            if(!empty($forcount)){
                $forcount--;
            }
            if($p>10){
                break;
            }

        }
        return $issueList;
    }

    //生成报告相关形成区划称呼
    protected function area_call($regionid){
        $zxs = ['110000','120000','310000','500000'];//直辖市
        $re_level = M('tregion')->where(['fid'=>$regionid])->getField('flevel');//当前用户行政等级
        if(empty($re_level)){
           $this->ajaxReturn([
               'code'=>1,
               'msg'=>'暂无数据！',
           ]);
        }
        $regionid_str = substr($user['regionid'],0,4);
        $call = [
            'regionid_str'=>$regionid_str,//区域id
            'z_call'=>'全市',//总称呼
            'x_call'=>'区县',//下级称呼
            's_call'=>'市属',//媒体所属称呼
            's_flevel'=>$re_level,//自己行政区划
            'x_flevel'=>5,//下级行政区划
        ];
        //文字描述匹配 省  市  县
        switch ($re_level){
            case 1:
                $regionid_str = substr($user['regionid'],0,2);
                if(!in_array($user['regionid'],$zxs)){
                    $call = [
                        'regionid_str'=>$regionid_str,//区域id
                        'z_call'=>'全省',//总称呼
                        'x_call'=>'市级',//下级称呼
                        's_call'=>'省属',//媒体所属称呼
                        's_flevel'=>$re_level,//自己行政区划
                        'x_flevel'=>4,//下级行政区划
                    ];
                }
                break;
            case 5:
                $regionid_str = $user['regionid'];
                $call = [
                    'regionid_str'=>$regionid_str,//区域id
                    'z_call'=>'全区县',//总称呼
                    'x_call'=>'乡镇',//下级称呼
                    's_call'=>'县属',//媒体所属称呼
                    's_flevel'=>$re_level,//自己行政区划
                    'x_flevel'=>$re_level,//下级行政区划
                ];
                break;
        }
        return $call;
    }

    //获得开始结束时间中文
    protected function day_str($s_date,$e_date){
        if(strtotime($s_date) > strtotime($e_date)){
            $this->ajaxReturn([
                'code'=>1,
                'msg'=>'开始时间不能大于结束时间！'
            ]);
        }
        return [
            's' => date('Y年m月d日',strtotime($s_date)),
            'e' => date('Y年m月d日',strtotime($e_date))
        ];
    }

    //报告生成处理
    protected function report_generation($pntype,$report_data,$pnname,$user,$s_date,$e_date,$pnfiletype = 10){
        $ALL_CONFIG = getconfig('ALL');

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！ '));
            }

            $system_num = getconfig('system_num');

            //将生成记录保存
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= $pntype;
            $data['pnfiletype'] 		= $pnfiletype;
            $data['pnstarttime'] 		= date('Y-m-d',strtotime($s_date));
            $data['pnendtime'] 		    = date('Y-m-d',strtotime($e_date));
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = 0;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }

    //获取媒体数据
    protected function get_media_data($owner_media_ids,$user_regionid,$s_date,$e_date,$media_type){
        $StatisticalReport = New StatisticalModel();//实例化数据统计模型
        $every_media_tj =  $StatisticalReport->report_ad_monitor('fmediaid',$owner_media_ids,$user_regionid,$s_date,$e_date,'fad_illegal_times',$media_type);
        $every_media_data = [];
        $every_media_illegal_times_data = [
            [''],
            ['']
        ];
        //各地区综合得分排名json
        foreach ($every_media_tj as $every_media_tj_key => $every_media_tj_val){
            if(!$every_media_tj_val['titlename']){
                $medias[] = $every_media_tj_val['fmediaid'];

                if($every_media_tj_val['fad_illegal_times'] > 0 && count($every_media_illegal_times_data[0]) <= 15){
                    array_push($every_media_illegal_times_data[0],$every_media_tj_val['tmedia_name']);
                    array_push($every_media_illegal_times_data[1],strval($every_media_tj_val['fad_illegal_times']));
                }

                $every_media_data[] = [
                    $every_media_tj_key+1,
                    $every_media_tj_val['tregion_name'],
                    $every_media_tj_val['tmedia_name'],
                    $every_media_tj_val['fad_times'],
                    $every_media_tj_val['fad_illegal_times'],
                    $every_media_tj_val['times_illegal_rate'].'%',
                    $every_media_tj_val['fad_count'],
                    $every_media_tj_val['fad_illegal_count'],
                    $every_media_tj_val['counts_illegal_rate']?$every_media_tj_val['counts_illegal_rate'].'%':'0.00'.'%'
                ];
            }else{
                $hj_all = [
                    'fad_count' =>$every_media_tj_val['fad_count'],
                    'fad_illegal_count' =>$every_media_tj_val['fad_illegal_count'],
                    'counts_illegal_rate' =>$every_media_tj_val['counts_illegal_rate']?$every_media_tj_val['counts_illegal_rate'].'%':'0.00'.'%',
                    'fad_times' =>$every_media_tj_val['fad_times'],
                    'fad_illegal_times' =>$every_media_tj_val['fad_illegal_times'],
                    'times_illegal_rate' =>$every_media_tj_val['times_illegal_rate'].'%'
                ];
            }
        }

        $every_media_data = to_string($every_media_data);
        if(count($every_media_tj) <= 1){
            return [];
        }else{
            $no_data_medias = array_diff($owner_media_ids,$medias);
            $media_names = M('tmedia')->where(['fid'=>['IN',$no_data_medias],'fmediaclassid'=>['like',$media_type.'%']])->getField('fmedianame',true);
            if(!empty($media_names)){
                //$no_data_media_str = "(其中".implode($media_names,'、')."等媒体未监测到广告信息)";
            }else{
                $no_data_media_str = '';
            }
            return [
                'text'=>$hj_all,
                'list'=>$every_media_data,
                'chart'=>$every_media_illegal_times_data,
                'no_data_media_str' => $no_data_media_str
            ];
        }
    }

    //时间段中文字符串匹配 默认月报 报告类型 20周报、30月报、70季报、80年报
    protected function  time_str($pntype,$date,$e_data = ''){
        $time_str = '';
        $unix_time = strtotime($date);
        switch ($pntype){
            case '20':
                $time_str = date('Y',$unix_time).'年第'.date('W',$unix_time).'周';
                break;
            case '30':
                $time_str = date('Y年m月',$unix_time);
                break;
            case '25':
                if(date('d',$unix_time)  > 15){
                    $time_str = date('Y年m月下半月',$unix_time);
                }else{
                    $time_str = date('Y年m月上半月',$unix_time);
                }
                break;
            case '70':
                $jd = ceil((date('n', $unix_time))/3);
                $time_str = date('Y',$unix_time)."年第".number2chinese($jd)."季度";//当月是第几季度
                break;
            case '75':
                if(date('m', $unix_time) > 5){
                    $nd = "下半年";
                }else{
                    $nd = "上半年";
                }
                $time_str = date('Y',$unix_time)."年".$nd;//当月是第几季度
                break;
            case '80':
                $time_str = date('Y',$unix_time)."年";//当前年份
                break;
            default:
                $time_str = $date.'至'.$e_data."报告";
        }
        return $time_str;
    }

    /*生成互联网图表数据*/
    function create_net_chart_data($array){
        if(empty($array)){
            return [[],[]];
        }
        $array_data2[] = ["网站","广告量","违法量","违法率（%）"];

        foreach ($array as $array_key=>$array_val){
            $array_data1[] = [
                $array_val[0],
                $array_val[1],
                $array_val[2],
                $array_val[3],
                $array_val[4]
            ];
            $array_data2[] = [
                $array_val[1],
                $array_val[2],
                $array_val[3],
                $array_val[4]
            ];
        }
        return [$array_data1,$array_data2];
    }



}