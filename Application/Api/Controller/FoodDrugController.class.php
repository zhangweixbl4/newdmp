<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;

class FoodDrugController extends Controller {
	
	
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}



	public function food_drug_issue(){
		session_write_close();
		set_time_limit(600);
		for($i=0;$i<50;$i++){
			$this->food_drug_issue_do();
		}

	}
	
	/*媒介*/
	public function food_drug_issue_do(){
		session_write_close();
		set_time_limit(80);
		$dateInfo = A('Common/System','Model')->important_data('food_drug_issue');//获取处理过的日期
		$dateInfo = json_decode($dateInfo,true);//转为数组
		if(!$dateInfo){//如果是空的
			$month_date = date('Y-m-d',time() - 86400 * 8);
			$media_id = 0;
		}else{
			$month_date = $dateInfo['month_date'];//处理月份
			$media_id = $dateInfo['media_id'];//处理媒介id
		}
		//食药局需要的媒介ID
		
		$where = array();
		$where['tlabel.flabel'] = '食药局';
		$where['tmedialabel.fmediaid'] = array('gt',$media_id);
		
		$mediaInfo = M('tmedialabel')->cache(true,120)
			->field('tmedialabel.fmediaid as fid ')
			->join('tlabel ON tlabel.flabelid = tmedialabel.flabelid')
			->where($where)
			->order('tmedialabel.fmediaid asc')
			->find();
		
		if(!$mediaInfo){//判断能否查到媒介
			if(strtotime($month_date) > time()){//判断日期是否处理到了最近一天
				A('Common/System','Model')->important_data('food_drug_issue','{}');
			}else{
				A('Common/System','Model')->important_data('food_drug_issue',
					json_encode(array(
						'month_date'=>date("Y-m-d",strtotime($month_date) + 86400),//时间戳加一个月
						'media_id'=>0)
					));//不是最近一天
			}
			exit;
		}else{
			$ff = A('Common/System','Model')->important_data('food_drug_issue',
				json_encode(array('month_date'=>$month_date,'media_id'=>$mediaInfo['fid'])));
		}

		A('Api/FoodDrug','Model')->food_drug_issue($month_date,$mediaInfo['fid']);
	}
}