Vue.prototype.$ELEMENT = { size: "mini", zIndex: 3000 }
let ShiftDown = false
let shiftXY = []

let rightDown = false
let rightXY = []
let rightLastXY = []
let cfList = []



$('body').keydown(function (e) {

	if(e.originalEvent.key == 'Shift') ShiftDown = true
});
$('body').keyup(function (e) {
	if(e.originalEvent.key == 'Shift') ShiftDown = false
	shiftXY = []
});






const vm = new Vue({
  el: '#app',
  components: {timeSelect},
  
  
  
  
  data() {
    let month = dayjs().format('YYYY-MM')
    return {
      form: {
        keyword: '',
        region_id: '',
        this_region_id: 0,
        medialabel: '',
        mediaclass: '',
        month: month,
        fixed_state: '',
      },
      tableData: [],
      tableHead: [],
      tableHeight: '300px',
      selectedData: {},
      page: {
        p: 1,
        pp: 15,
      },
      total: 0,
      loading: false,
      regionTree: REGIONTREE,
      props: {
        checkStrictly: true,
        emitPath: false,
      },
      mediaType: {
        '01': {
          label: '电视',
        },
        '02': {
          label: '广播',
        },
        '03': {
          label: '报纸',
        },
        '05': {
          label: '户外',
        },
        '13': {
          label: '互联网',
        },
      },
      state: {
		  
		'-99': {
          label: '无计划',
          class: 'archive-99'
        },
        '-1': {
          label: '已删除',
          class: 'archive-1'
        },
        '0': {
          label: '待开始',
          class: 'archive0'
        },
        '1': {
          label: '生产中',
          class: 'archive1'
        },
        '2': {
          label: '已完成',
          class: 'archive2'
        },
		'99': {
          label: '特殊',
          class: 'archive99'
        },
		
      },
      sourceData: '',
      color: '#409EFF',
      fixedState: [],
    }
  },
  created() {
    this.sourceData = JSON.stringify(this.form)
    this.getList()
	
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight()
      document.querySelector('#app').style.opacity = 1
    })
    window.onresize = () => {
      this.getTableHeight()
    }
  },
  methods: {
    stateChange(val) {
      this.form.fixed_state = (val || []).join(',')
    },
    handleCommand(state) {
      this.$prompt('请输入修改说明', '修改说明', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        inputType: 'textarea'
      }).then(({ value }) => {
		  
		let selectedData0 = {}
		$('.list.selected').each(function(){
			let data = $(this).attr('data').split(',')
			console.log()
			if(!selectedData0[data[0]]){
				selectedData0[data[0]] = data[2]
			}else{
				selectedData0[data[0]] += ','+data[2]
			}
	
		});
		console.log(selectedData0)
        let data = {
          //fix_days: this.selectedData,
		  fix_days: selectedData0,
		  
          state: state,
          value: value
        }
        fly.post('/Admin/FixedMediaData/fixed', Qs.stringify(data)).then(({data}) => {
          if (data.code === 0) {
            this.$refs.timeSelect.clearData()
            $h.notify.success(data.msg, this)
            this.getList(this.page.p)
          } else {
            $h.notify.error(data.msg, this)
          }
        })
      }).catch(() => {})
    },
	
	change_inspect_type() {
      this.$prompt('请输入生产方式,0其他 1云剪 2全剪 3快剪', '修改生产方式', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',

        inputType: 'text'
      }).then(({ value }) => {
		  
		  
		let selectedData0 = {}
		$('.list.selected').each(function(){
			let data = $(this).attr('data').split(',')
			console.log()
			if(!selectedData0[data[0]]){
				selectedData0[data[0]] = data[2]
			}else{
				selectedData0[data[0]] += ','+data[2]
			}
	
		});
		
		let data = {
		  fix_days: selectedData0,
          finspect_type: value
        }
		console.log(data)
        fly.post('/Admin/FixedMediaData/change_inspect_type', Qs.stringify(data)).then(({data}) => {
          if (data.code === 0) {
            this.$refs.timeSelect.clearData()
            $h.notify.success(data.msg, this)
            this.getList(this.page.p)
          } else {
            $h.notify.error(data.msg, this)
          }
        })
		  
		  
      }).catch(() => {})
    },
	
	gd_fx() {
      this.$prompt('请输入归档理由', '归档到分析数据库', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',

        inputType: 'text'
      }).then(({ value }) => {
		  
		  
		let selectedData0 = {}
		$('.list.selected').each(function(){
			let data = $(this).attr('data').split(',')
			console.log()
			if(!selectedData0[data[0]]){
				selectedData0[data[0]] = data[2]
			}else{
				selectedData0[data[0]] += ','+data[2]
			}
	
		});
		
		let data = {
		  fix_days: selectedData0,
          value: value
        }

        fly.post('/Admin/FixedMediaData/gd_fx', Qs.stringify(data)).then(({data}) => {
          if (data.code === 0) {
            this.$refs.timeSelect.clearData()
            $h.notify.success(data.msg, this)
			
          } else {
            $h.notify.error(data.msg, this)
          }
        })
		  
		  
      }).catch(() => {})
    },
	
	get_issue() {
      this.$prompt('请输入拉取发布记录的理由', '重新拉取发布记录', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',

        inputType: 'text'
      }).then(({ value }) => {
		  
		  
		let selectedData0 = {}
		$('.list.selected').each(function(){
			let data = $(this).attr('data').split(',')
			console.log()
			if(!selectedData0[data[0]]){
				selectedData0[data[0]] = data[2]
			}else{
				selectedData0[data[0]] += ','+data[2]
			}
	
		});
		
		let data = {
		  fix_days: selectedData0,
          value: value
        }

        fly.post('/Admin/FixedMediaData/get_issue', Qs.stringify(data)).then(({data}) => {
          if (data.code === 0) {
            this.$refs.timeSelect.clearData()
            $h.notify.success(data.msg, this)
			
          } else {
            $h.notify.error(data.msg, this)
          }
        })
		  
		  
      }).catch(() => {})
    },
	
	
	
	
	
	
    getList(page = 1) {
      this.page.p = page
      let data = {
        fixed_state: this.fixedState,
		
        ...this.form,
        ...this.page
      }
      this.loading = true
      fly.post('/Admin/FixedMediaData/media_list', Qs.stringify(data)).then(({ data }) => {
		$('.list.selected').each(function(){
			$(this).removeClass('selected');
		});
        let days = this.getDays(this.form.month)
        this.tableHead = this.setDays(days)

        let tableData = (data.mediaList || []).map(i => {
          i.days = this.setDays(days)
		  
          return i
        })
		
        let idMap = this.getIdMap(data.fixed_media_data_list)
		
        tableData = tableData.map(i => {

          i.days = i.days.map(v => {
			 
            let data = (idMap[i.fid] || []).filter(f =>f.fissue_date === v.fday)
										
			
            let _data = data.length ? { ...v, ...data[0] } : v

            return _data
          })

          return i
        })
        this.tableData = tableData

        this.total = +data.count
        this.loading = false
		
      })
    },
    getIdMap(list) {
      let fmediaids = Array.from(new Set((list || []).map(i => i.fmedia_id)))
	  
      let obj = {}
      fmediaids.forEach(i => {
        obj[i] = list.filter(v => v.fmedia_id === i)
      })
      return obj
    },
    getTag(keyword, cb) {
      if (keyword) {
        fly.get(`/Api/Label/searchMediaLabel2?keyword=${keyword}`).then(res => {
          cb(res.data.value)
        })
      } else {
        cb([])
      }
    },
    pageSizeChange(pageSize) {
      this.page.pp = pageSize
      this.getList(this.page.p)
    },
    getTableHeight() {
      let formHeight = this.$refs.sform.$el.clientHeight
      this.tableHeight = document.body.clientHeight - formHeight - 84 + 'px'
    },
    reset(getList = false) {
      this.form = JSON.parse(this.sourceData)
      if(getList) this.getList(1)
    },
    setDays(length = 31) {
      let days = []
      for (let i = 0; i < length; i++){
        let date = dayjs(`${this.form.month}-01`).add(i, 'd')
        days.push({
          //fixed_state: '0',
		  fstatus: '-99',
		  
          fday: date.format('YYYY-MM-DD'),
          day: date.format('DD'),
          selected: false
        })
      }
      return days
    },
    getDays(time) {
      let year = dayjs(time).format('YYYY')
      let month = dayjs(time).format('MM')
      return new Date(year, month, 0).getDate()
    }
  },
})