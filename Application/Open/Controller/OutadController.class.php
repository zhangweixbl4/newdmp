<?php
namespace Open\Controller;
use Think\Controller;

/**
 * 户外广告数据接口
 */
class OutadController extends BaseController {
	
	/**
	* 接口接收的JSON参数并解码为对象
	*/
	protected $oParam;
	protected $apiurl = 'http://47.96.87.152';//推送接口地址
	
	/**
	 * 构造函数
	 * @Param	Mixed variable
	 * @Return	void 返回的数据
	 */
	public function _initialize(){
		$this->oParam = json_decode(file_get_contents('php://input'));
		// TokenController::AuthRequestSign($this->oParam);	//验证请求签名
	}
	
	/**
	* 构造函数_back
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function _initialize_back(){
		$this->oParam = json_decode(file_get_contents('php://input'));
		TokenController::AuthRequestSign($this->oParam);	//验证请求签名
		$ret = TokenController::AuthToken($this->oParam->token);//验证Token有效期
		if ($ret['code'] != 0){
			$this->ajaxReturn(array('code'=>1,'msg'=>$ret['msg']));
		}
	}

	/**
	 * 接收广告数据
	 * by zw
	 * @Param	Mixed variable
	 * @Return	void 返回的数据
	 */
	public function pushAdinfo(){
		$adJson = (array)$this->oParam->adJson;//广告基础数据
		$adDetailsJson = (array)$this->oParam->adDetailsJson;//广告详情表
		$latAndLonJson = (array)$this->oParam->latAndLonJson;//广告详情与广告位关联表
		$detailJson = (array)$this->oParam->detailJson;//预览表的基础数据
		$previewJson = (array)$this->oParam->previewJson;//预审记录
		$imgMaterial = (array)$this->oParam->imgMaterial;//图片小样
		$videoMaterial = (array)$this->oParam->videoMaterial;//视频小样
		$outerMaterial = (array)$this->oParam->outerMaterial;//其他材料
		$companyJson = (array)$this->oParam->companyJson;//企业信息
		$ownerJson = (array)$this->oParam->ownerJson;//广告主信息
		if(empty($adJson['id'])){
			$this->ajaxReturn(array('code' => 1, 'msg' => '推送失败，参数有误'));
		}

		$do_taio = M('tod_ad_info') -> where(['id' => $adJson['id']]) -> find();

		$addadJson['id'] = $adJson['id'];
		$addadJson['ad_typename'] = $adJson['type'] == 1?'普通广告':'公益广告';
		$addadJson['mode'] = $adJson['mode']== 1?'代人发布':'自己发布';
		$addadJson['name'] = $adJson['name'];
		$addadJson['class_name'] = $adJson['class_name'];
		$addadJson['area'] = $adJson['area'];
		$addadJson['pretrial_id'] = $detailJson['pretrial_id'];
		$addadJson['pretrial_count'] = $detailJson['checksn'];
		$addadJson['check'] = $detailJson['pretrial_type'];
		if($detailJson['pretrial_type'] != 1 && $detailJson['pretrial_type'] != 4){
			$addadJson['pretrial_time'] = date('Y-m-d H:i:s');
			$addadJson['pretrial_result'] = $previewJson['remarks'];
		}

		$addadJson['upunit_id'] = $adJson['upunit_id'];
		$addadJson['upunit_name'] = $companyJson['name'];
		$addadJson['upunit_person'] = $companyJson['contacts'];
		$addadJson['upunit_num'] = $companyJson['code'];
		$addadJson['upunit_phone'] = $companyJson['tel'];
		$addadJson['upunit_area'] = $companyJson['addressInfo'];
		$addadJson['upunit_address'] = $companyJson['addree_info'];

		$addadJson['manage_name'] = $ownerJson['owner_name'];
		$addadJson['manage_phone'] = $ownerJson['tel'];
		$addadJson['manage_person'] = $ownerJson['representative'];

		$addadJson['is_car'] = $adDetailsJson['is_car']?$adDetailsJson['is_car']:1;
		if($ownerJson['o_type'] == 1){
			$addadJson['manage_classname'] = '国有企业';
		}elseif($ownerJson['o_type'] == 2){
			$addadJson['manage_classname'] = '国有事业';
		}elseif($ownerJson['o_type'] == 3){
			$addadJson['manage_classname'] = '集体事业';
		}elseif($ownerJson['o_type'] == 4){
			$addadJson['manage_classname'] = '私营事业';
		}elseif($ownerJson['o_type'] == 5){
			$addadJson['manage_classname'] = '个体工商户';
		}elseif($ownerJson['o_type'] == 6){
			$addadJson['manage_classname'] = '外商投资企业';
		}
		$addadJson['fcreatetime'] = date('Y-m-d H:i:s');
		
		if(!empty($latAndLonJson) && !empty($adDetailsJson)){
			$adlon_datas = [];
			foreach ($latAndLonJson as $key => $value) {
				$value = (array)$value;
				$adlon_data['ad_id'] = $adDetailsJson['ad_id'];
				$adlon_data['address_info'] = $value['address_info'];
				$adlon_data['ads_name'] = $value['ads_name'];
				$adlon_data['is_traffic'] = $value['is_traffic'];
				$adlon_data['traffic_type'] = $this->gettraffic_type($value['traffic_type']);
				if(!empty($value['address_info']) && !empty($value['ads_name'])){
					$adlon_data['traffic_id'] = $value['ads_name'].'('.$value['address_info'].')';
				}
				$adlon_data['address_note'] = $value['address_note'];
				$adlon_data['fcreatetime'] = date('Y-m-d H:i:s');
				$adlon_data['source_id'] = $value['source_id'];
				if($adDetailsJson['is_car'] == 2){
					if($adDetailsJson['form'] == 0){
						$adlon_data['form'] = '平面';
					}elseif($adDetailsJson['form'] == 1){
						$adlon_data['form'] = '视频';
					}elseif($adDetailsJson['form'] == 2){
						$adlon_data['form'] = '平面+视频';
					}
					$adlon_data['number'] = $this->getnumber($adDetailsJson['number']);
					$adlon_data['s_date'] = $adDetailsJson['s_date']/1000;
					$adlon_data['e_date'] = $adDetailsJson['e_date']/1000;
					$adlon_data['cars'] = $adDetailsJson['cars'];
					$adlon_data['area'] = $adDetailsJson['area'];
					$adlon_data['video_length'] = $adDetailsJson['video_length'];
				}else{
					if($value['form'] == 0){
						$adlon_data['form'] = '平面';
					}elseif($value['form'] == 1){
						$adlon_data['form'] = '视频';
					}elseif($value['form'] == 2){
						$adlon_data['form'] = '平面+视频';
					}
					$adlon_data['number'] = $this->getnumber($value['number']);
					$adlon_data['s_date'] = $value['s_date']/1000;
					$adlon_data['e_date'] = $value['e_date']/1000;
					$adlon_data['cars'] = $value['cars'];
					$adlon_data['area'] = $value['area'];
					$adlon_data['video_length'] = $value['video_length'];
				}
				$adlon_datas[] = $adlon_data;
			}
		}

		//语句执行成功情况下，插入广告材料数据
		$admaterials = [];
		if(!empty($imgMaterial)){
			$admaterial['ad_id'] = $imgMaterial['ad_id'];
			$admaterial['material_url'] = $imgMaterial['material_url'];
			$admaterial['material_type'] = $imgMaterial['material_type'];
			$admaterial['material_detail'] = $imgMaterial['material_detail'];
			$admaterial['check_sn'] = $imgMaterial['check_sn'];
			$admaterials[] = $admaterial;
		}
		if(!empty($videoMaterial)){
			$admaterial['ad_id'] = $videoMaterial['ad_id'];
			$admaterial['material_url'] = $videoMaterial['material_url'];
			$admaterial['material_type'] = $videoMaterial['material_type'];
			$admaterial['material_detail'] = $videoMaterial['material_detail'];
			$admaterial['check_sn'] = $videoMaterial['check_sn'];
			$admaterials[] = $admaterial;
		}
		if(!empty($outerMaterial)){
			$admaterial['ad_id'] = $outerMaterial['ad_id'];
			$admaterial['material_url'] = $outerMaterial['material_url'];
			$admaterial['material_type'] = $outerMaterial['material_type'];
			$admaterial['material_detail'] = $outerMaterial['material_detail'];
			$admaterial['check_sn'] = $outerMaterial['check_sn'];
			$admaterials[] = $admaterial;
		}
		
		M()->startTrans();
		if(!empty($do_taio)){
			$do_taio2 = M('tod_ad_info') ->where(['fid' => $do_taio['fid']]) -> save($addadJson);//保存广告信息
			M('tod_lat_and_lon')->where(['ad_id' => $do_taio['id']])->delete();
			$nowadid = $do_taio['fid'];
		}else{
			$do_taio2 = M('tod_ad_info') -> add($addadJson);//添加广告信息
			$nowadid = $do_taio2;
		}
		$do_taio3 = M('tod_lat_and_lon') -> addAll($adlon_datas);//添加广告位
		if(!empty($admaterials)){
			M('tod_material') -> addAll($admaterials);
		}

		if(empty($do_taio)){
			//添加户外广告众包任务
			if($detailJson['pretrial_type'] == 1 || $detailJson['pretrial_type'] == 2 || $detailJson['pretrial_type'] == 4){
				$taskdata['fadinfo_id'] = $nowadid;
				if($detailJson['pretrial_type'] == 1 || $detailJson['pretrial_type'] == 4){
					$taskdata['syn_state'] = 1;
					$taskdata['task_state'] = 0;
				}else{
					$taskdata['syn_state'] = 2;
					$taskdata['task_state'] = 2;
				}
				$taskdata['net_created_date'] = time();
				$taskdata['createtime'] = time();

				$do_task = M('todtask') -> add($taskdata);
			}
		}else{
			//修改户外广告众包任务
			$taskdata['syn_state'] = 1;
			$taskdata['task_state'] = 13;

			$do_task = M('todtask')->where(['fadinfo_id'=>$nowadid])->save($taskdata);
		}
		
		if ($do_taio2 && $do_taio3){
			M()->commit();
			$code = 0;
            $msg = '推送成功';
		}else{
			M()->rollback();
            $code = 1;
            if (!$do_taio2){
                $msg = '广告信息推送失败';
            }elseif(!$do_taio3){
                $msg = '广告位推送失败';
            }else{
            	$msg = '未知错误';
            }
		}
		$this->ajaxReturn(array('code' => 0, 'msg' => '推送成功'));
	}

	/**
	 * 接收广告核查数据
	 * by zw
	 * @Param	Mixed variable
	 * @Return	void 返回的数据
	 */
	public function pushCheckinfo(){
		$data = (array)$this->oParam->checkInfoJson;
		$imgJson = (array)$this->oParam->imgJson;
		if(empty($data['ad_id']) || empty($data['ad_source_id'])){
			$this->ajaxReturn(array('code' => 1, 'msg' => '推送失败，参数有误'));
		}

		$do_tcio = M('tod_lat_and_lon') ->field('fid,ad_id') -> where(['source_id' => $data['ad_source_id'],'ad_id' => $data['ad_id']]) -> find();
		if(empty($do_tcio)){
			$this->ajaxReturn(array('code' => 1, 'msg' => '推送失败，台账信息不存在'));
		}

		$do_tao = M('tod_ad_info') ->where(['id'=>$data['ad_id']]) ->find();

		$addata['check_name'] = $data['check_name'];
		$addata['check_owner'] = $data['check_owner'];
		$addata['check_type'] = $this->getnumber($data['check_type']);
		$addata['address'] = $data['address'];
		$addata['check_result'] = $data['check_result'];
		$addata['check_createtime'] = date('Y-m-d H:i:s');

		$do_tcio2 = M('tod_lat_and_lon') ->where(['fid' => $do_tcio['fid']]) -> save($addata);//更新广告核查信息
		if(!empty($do_tcio2)){
			M('tod_check_pic') -> where(['source_id' => $data['ad_source_id']]) ->delete();
		}

		//语句执行成功情况下，插入广告材料数据
		if(!empty($do_tcio2)){
			if(!empty($imgJson)){
				$admaterial['source_id'] = $data['ad_source_id'];
				$admaterial['material_url'] = $imgJson['material_url'];
				$admaterial['material_type'] = $imgJson['material_type'];
				$admaterial['material_detail'] = $imgJson['material_detail'];
				
				M('tod_check_pic') -> add($admaterial);
			}
			
			$where_task['fadinfo_id'] = $do_tao['fid'];
			$where_task['fadsource_id'] = $data['ad_source_id'];
			$do_task = M('todtask') -> where($where_task) ->find();
			if(empty($do_task)){
				if($data['check_result'] == 1 || $data['check_result'] == 2){
					//添加户外广告众包违法判定任务
					$taskdata['fadinfo_id'] = $do_tao['fid'];
					$taskdata['fadsource_id'] = $data['ad_source_id'];
					$taskdata['syn_state'] = 1;
					$taskdata['task_state'] = 20;
					$taskdata['net_created_date'] = time();
					$taskdata['createtime'] = time();
					M('todtask') -> add($taskdata);
				}
			}else{
				if($data['check_result'] == 1 || $data['check_result'] == 2){
					//修改户外广告众包违法判定任务
					$taskdata['syn_state'] = 1;
					$taskdata['task_state'] = 23;

					M('todtask')->where(['fadinfo_id'=>$do_tao['fid'],'fadsource_id'=>$data['ad_source_id']])->save($taskdata);
				}else{
					$taskdata['task_state'] = -1;
					M('todtask')->where(['fadinfo_id'=>$do_tao['fid'],'fadsource_id'=>$data['ad_source_id']])->save($taskdata);
				}
			}
		}

		$this->ajaxReturn(array('code' => 0, 'msg' => '推送成功'));
	}

	/**
	* 获取线路类型
	*/
	private function gettraffic_type($t){
		switch($t){
			case 1:
				$ts = '公交线路';
				break;
			case 2:
				$ts = '地铁线路'; 
				break;
			case 3:
				$ts = '巴士线路';
				break;
			case 4:
				$ts = '公交站'; 
				break;
			case 5:
				$ts = '地铁站'; 
				break;
			case 6:
				$ts = '普通广告位'; 
				break;
			default:
				$ts = '';
				break;
		}
		return $ts;
	}

	/**
	* 获取发布规格
	*/
	private function getnumber($t){
		switch($t){
			case 1:
				$ts = '投影式';
				break;
			case 2:
				$ts = '电子显示屏'; 
				break;
			case 3:
				$ts = '展示牌';
				break;
			case 4:
				$ts = '电子显示装置'; 
				break;
			case 5:
				$ts = '灯箱'; 
				break;
			case 6:
				$ts = '霓虹灯'; 
				break;
			case 7:
				$ts = '交通工具'; 
				break;
			case 8:
				$ts = '水上漂浮物'; 
				break;
			case 9:
				$ts = '升空器具'; 
				break;
			case 10:
				$ts = '充气物'; 
				break;
			case 11:
				$ts = '模型'; 
				break;
			case 12:
				$ts = '户外路牌广告'; 
				break;
			case 13:
				$ts = '其他'; 
				break;
			default:
				$ts = '';
				break;
		}
		return $ts;
	}

	/**
	 * 推送预审信息
	 * by zw
	 * @Param	Mixed variable
	 * @Return	void 返回的数据
	 */
	public function get_pretrial_detail($infoid = 0){
		$do_ad = M('tod_ad_info')->field('id,pretrial_id,check,pretrial_count,pretrial_result')->find($infoid);

		$addata['id'] = $do_ad['id'];
		$addata['pretrial_id'] = $do_ad['pretrial_id'];
		$addata['check'] = $do_ad['check'];
		$addata['pretrial_count'] = $do_ad['pretrial_count'];
		if($do_ad['check'] == 3){
			$addata['pretrial_result'] = $do_ad['pretrial_result'];
		}

		$url = $this->apiurl.'/expertService/acad/pushPreviewDate';
		$data = json_encode($addata);
		list($return_code, $return_content) = http_post($url, $data);//return_code是http状态码

		if($return_code == 200){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 推送违法判定信息
	 * by zw
	 * @Param	Mixed variable
	 * @Return	void 返回的数据
	 */
	public function get_illegal_detail($data = []){
		$do_ad = M('tod_ad_info')->field('id')->find($data['ad_id']);
		$data['ad_id'] = $do_ad['id'];
		$data['id'] = $do_ad['id'];
		$url = $this->apiurl.'/expertService/adCheck/pushCheckDate';
		$data = json_encode($data); 
		list($return_code, $return_content) = http_post($url, $data);//return_code是http状态码
		if($return_code == 200){
			return true;
		}else{
			return false;
		}
	}
}