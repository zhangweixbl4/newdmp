const taskInfo = {
  template: `<div v-loading="loading" class="task-info">
    <div class="ad-text" v-if="showAdText && task.sample_to_word">{{task.sample_to_word}}</div>
    <div style="height: 60px;overflow: auto;">
      <el-row v-if="sameAdNameTaskList.length > 0">
        <div>
          下列为供参考的部分同名广告, 点击按钮查看详情, 红色为已判定的违法广告,
          <span style="color: red;">导入时请注意违法判定</span>
        </div>
        <div class="sameAdNameTaskList">
          <el-button :type="item.fillegaltypecode > 0 ? 'danger' : 'info'" v-for="(item, index) in sameAdNameTaskList"
            @click="checkTask(item.taskid)" :size="item.fillegaltypecode > 0 ? 'medium' : 'mini'" :key="index" plain>{{task.media_class
            === '3' ? '广告' + (index+1) : '广告时长' + item.fadlen}}</el-button>
        </div>
        <el-dialog title="任务详情" :modal="false" :visible="showTaskInfo" @close="closeTaskInfo" width="90%" class="subTaskInfo">
          <task-info :task="checkTaskInfo" ref="task" @next="closeTaskInfo" @back="closeTaskInfo" @fillTaskData="receiveFillTaskData"></task-info>
        </el-dialog>
      </el-row>
    </div>
    <hr>
    <el-row>
      <el-col :span="12">
        <el-row>
          <div class="sample-title">
            <el-button type="text" @click="openCutErrorForm" v-if="type === 'input'">提交剪辑错误</el-button>
            <div>任务状态: {{task.task_state | taskStateFilter}} {{task.quality_state == 2 ? ' , 已质检' : ''}}</div>
            <div v-if="task.task_state == 1 && type === 'input'">任务领取时间: {{receiveTime}}, 剩余时间 <b>{{remainTime}}</b> 秒</div>
          </div>
        </el-row>
        <div class="btnArea">
          <el-row v-if="task.task_state == 4" class="task-back">
            <div class="describe">该任务被退回</div>
            <div class="reason">退回理由: {{task.back_reason}}</div>
            <el-button type="primary" @click="reformTask" v-if="type == 'review'">重做任务</el-button>
          </el-row>
          <el-row v-if="task.cut_err_state == 1" class="task-back">
            <div class="describe">该任务为剪辑错误</div>
            <div class="reason">剪辑错误说明: {{task.cut_err_reason}}</div>
          </el-row>
          <el-row v-if="type === 'quality'">
            <el-button type="primary" @click="reformTask" v-if="task.task_state == 2">质检员重做任务</el-button>&nbsp;&nbsp;
          </el-row>
          <el-row v-if="type === 'review'">
            <el-button type="warning" @click="rejectTask" v-if="task.task_state == 2 && task.quality_state != 2">撤回任务</el-button>
          </el-row>
          <el-row v-if="type === 'quality'">
            <div v-if="task.quality_state == 0">
              <!--<el-button type="success" @click="passTask" v-if="task.task_state != 5">通过任务</el-button>-->
              <el-button type="warning" @click="rejectTask">退回任务</el-button>
            </div>
          </el-row>
        </div>
        <el-row>
          <b>任务id</b> : {{task.taskid}}
        </el-row>
        <el-row>
          <b>UUID</b> : {{ task.sam_uuid }}
        </el-row>
        <el-row>
          <b>播出媒体</b> : {{task.media_name}}
          <span v-if="adType == 'tv'" style="display: inline-block;width: 50px;">
            <img :src="task.tv_logo" alt="" style="width: 50px;">
          </span>
        </el-row>
        <el-row>
          <b>发布时间</b> : {{task.issue_date}}
        </el-row>
        <el-row v-if="task.media_class != 3">
          <b>视频/音频长度</b> : {{task.fadlen}}
        </el-row>
        <el-row class="sample-content">
          <div class="text-center" v-if="adType === 'paper'">
            <el-button @click="confirmPaper" v-if="!task.paperIsSure" type="info">确认版面无误后, 点击查看广告</el-button>
            <el-button @click="checkPaper" v-else type="info">查看版面</el-button>
          </div>
          <div>
            <div v-if="adType=='tv'">
              <video controls="controls" :src="task.source_path" autoplay></video>
            </div>
            <div v-if="adType=='bc'">
              <audio :src="task.source_path" controls autoplay></audio>
            </div>
            <div v-if="adType=='paper'">
              <img :src="task.paperUrl" alt="" v-if="!task.paperIsSure">
              <img :src="task.source_path" alt="" v-else>
            </div>
          </div>
          <div class="rate" v-if="adType!='paper'">
            <b>播放速度</b>
            <el-radio-group v-model="rate" @change="rateChange">
              <el-radio :label="0.75">0.75</el-radio>
              <el-radio :label="1">1</el-radio>
              <el-radio :label="1.25">1.25</el-radio>
              <el-radio :label="1.5">1.5</el-radio>
              <el-radio :label="2.0">2.0</el-radio>
            </el-radio-group>
          </div>
        </el-row>
      </el-col>
      <el-col :span="11" :offset="1">
        此任务为广告信息录入与违法判定<b>一起提交</b>
        <el-button v-if="showFillBtn" @click="fillTaskData">将此任务广告及判定信息导入到正在录入的任务</el-button>
        <el-form ref="adInfo" :model="task" label-width="120px" :rules="rule" :disabled="type !== 'input'">
          <el-tabs v-model="activeTabName" type="border-card">
            <el-tab-pane label="信息录入" name="adInfo">
              <el-form-item label="临时广告名称">
                {{task.tem_ad_name}}
              </el-form-item>
              <el-form-item label="广告名称" prop="fadname">
                <el-autocomplete v-model="task.fadname" :fetch-suggestions="searchAdName" @input="inputAdName" @focus="() => showAdText = true" @blur="adNameBlur"
                  :placeholder="task.fadname">
                  <el-button slot="append" @click="setAdName('社会公益')" v-if="type === 'input' && !task.adIsSure">社会公益</el-button>
                </el-autocomplete>
              </el-form-item>
              <el-form-item>
                <a :href="'https://www.baidu.com/s?wd=' + task.fadname" target="_blank" v-if="task.fadname">百度搜索此广告</a>
              </el-form-item>
              <el-form-item label="广告品牌" prop="fbrand">
                <el-input v-model="task.fbrand" :disabled="task.adIsSure">
                  <el-button slot="append" @click="appendAbBrand" v-if="type === 'input' && !task.adIsSure">无突出品牌</el-button>
                </el-input>
              </el-form-item>
              <el-form-item label="广告主" prop="fname" class="jcsb">
                <el-autocomplete v-model="task.fname" :fetch-suggestions="searchAdOwner" :disabled="task.adIsSure && (task.fname != '广告主不详' && task.fname!='')" @focus="() => showAdText = true" @blur="() => showAdText = false">
                  <el-button slot="append" @click="task.fname = '广告主不详'" v-if="type === 'input' && !task.adIsSure">广告主不详</el-button>
                </el-autocomplete>
              </el-form-item>
              <el-form-item label="商业类别" prop="adCateArr2">
                <el-cascader @change="changeFormAdClass2" :options="adClassTree" v-model="task.adCateArr2" ref="adCateArr2" @focus="() => showAdText = true" @blur="() => showAdText = false"
                  :filterable="true" v-if="type == 'input'" :disabled="task.adIsSure"></el-cascader>
                <span v-else>{{task.adCateText2}}</span>
              </el-form-item>
              <el-form-item label="广告类别" prop="adCateArr">
                <el-cascader :options="adClassArr" v-model="task.adCateArr" ref="adCateArr" :filterable="true" v-if="type == 'input'" @focus="() => showAdText = true" @blur="() => showAdText = false"
                  :disabled="task.adIsSure"></el-cascader>
                <span v-else>{{task.adCateText1}}</span>
              </el-form-item>
              <el-form-item v-if="showClassTip">
                <span style="color: red;font-size: 12px;line-height: 20px;">检测到“公益”，请确认是否为公益广告并选择正确广告类别</span>
              </el-form-item>
              <el-form-item v-if="task.adIsSure || type != 'input' ">
                <button class="btn btn-xs btn-warning" @click="adError" type="button">广告信息有误?(广告名称有误请退回任务或者直接修改)</button>
              </el-form-item>
              <el-form-item label="是否为栏目广告" v-if="adType!='paper'" prop="is_long_ad">
                <el-radio-group v-model="task.is_long_ad">
                  <el-radio label="0">否</el-radio>
                  <el-radio label="1">是</el-radio>
                </el-radio-group>
              </el-form-item>
              <el-form-item label="代言人">
                <el-input v-model="task.fspokesman" @focus="() => showAdText = true" @blur="() => showAdText = false"></el-input>
              </el-form-item>
              <el-form-item label="版本说明">
                <el-input v-model="task.fversion" @focus="() => showAdText = true" @blur="() => showAdText = false"></el-input>
              </el-form-item>
              <div class="text-center" v-if="!showMoreInfo && type === 'input'" style="cursor: pointer" @click="toggleShowMoreInfo">显示额外信息</div>
              <div v-else>
                <el-form-item>
                  <a :href="'http://app2.sfda.gov.cn/datasearchp/all.do?tableName=TABLE25&formRender=cx&searchcx=&name=' + task.fadname"
                    target="_blank">在食药监局数据库搜索该广告</a>
                </el-form-item>
                <el-form-item label="生产批准文号">
                  <el-input v-model="task.fmanuno" @focus="() => showAdText = true" @blur="() => showAdText = false"></el-input>
                </el-form-item>
                <el-form-item label="广告中生产批准文号">
                  <el-input v-model="task.fadmanuno" @focus="() => showAdText = true" @blur="() => showAdText = false"></el-input>
                </el-form-item>
                <el-form-item label="广告批准文号">
                  <el-input v-model="task.fapprno" @focus="() => showAdText = true" @blur="() => showAdText = false"></el-input>
                </el-form-item>
                <el-form-item label="广告中广告批准文号">
                  <el-input v-model="task.fadapprno" @focus="() => showAdText = true" @blur="() => showAdText = false"></el-input>
                </el-form-item>
                <el-form-item label="生产企业名称">
                  <el-input v-model="task.fent" @focus="() => showAdText = true" @blur="() => showAdText = false"></el-input>
                </el-form-item>
                <el-form-item label="广告中生产企业名称">
                  <el-input v-model="task.fadent" @focus="() => showAdText = true" @blur="() => showAdText = false"></el-input>
                </el-form-item>
                <el-form-item label="生产企业地区">
                  <el-input v-model="task.fentzone" @focus="() => showAdText = true" @blur="() => showAdText = false"></el-input>
                </el-form-item>
                <div class="text-center" v-if="showMoreInfo  && type === 'input'" style="cursor: pointer" @click="toggleShowMoreInfo">隐藏额外信息</div>
              </div>
            </el-tab-pane>
            <el-tab-pane :label="tempLock ? '此广告无需判定违法' : '违法信息'" name="illegalInfo">
              <el-form-item label="" v-if="tempLock">
                <span style="color: #E6A23C;">8月1日前的违法信息无需判定</span>
              </el-form-item>
              <el-form-item label="违法程度" prop="fillegaltypecode" v-if="!tempLock">
                <el-radio-group v-model="task.fillegaltypecode">
                  <el-radio :label="item.value" v-for="item in illegalTypeArr" :key="item.value" :disabled="tempLock">{{item.text}}</el-radio>
                </el-radio-group>
              </el-form-item>
              <div v-if="isIllegal || !task.fillegaltypecode">
                <el-form-item label="违法表现" prop="fexpressioncodes" class="illegalExpression" v-if="!tempLock">
                  <el-popover placement="left" width="800" trigger="click">
                    <div>
                      <el-input placeholder="输入关键字进行过滤" v-model="illegalKeyword">
                      </el-input>
                      <el-tree class="filter-tree" :data="illegalArr" node-key="fcode" accordion show-checkbox
                        :filter-node-method="filterNode" @check-change="selectIllegal" ref="illegalSelecter">
                      </el-tree>
                    </div>
                    <el-button slot="reference" :disabled="tempLock">点击选择</el-button>
                  </el-popover>
                  <el-card>
                    <!--<div class="text-center" v-if="task.checkedIllegalArr.length === 0">请选择</div>-->
                    <div v-for="(item,index) in task.checkedIllegalArr" :key="item.fcode" style="margin: 5px;" title="点击删除"
                      @click="deleteIllegal(index)">
                      <b>{{index+1}}.</b> {{item.fexpression}}
                    </div>
                  </el-card>
                </el-form-item>
              </div>
              <div v-if="isIllegal">
                <el-form-item label="违法内容" prop="fillegalcontent" v-if="!tempLock">
                  <el-input type="textarea" :rows="3" v-model="task.fillegalcontent" :disabled="tempLock">
                  </el-input>
                </el-form-item>
              </div>
            </el-tab-pane>
          </el-tabs>
          <br>
          <el-form-item v-if="type === 'input'">
            <el-button type="primary" @click="onSubmit">提 交</el-button>
          </el-form-item>
          <el-dialog title="点击行即填充" :visible.sync="showFillDataList" width="80%" v-if="type === 'input'">
            <el-table :data="task.fillData" highlight-current-row @current-change="selectMoreInfoRow">
              <el-table-column property="fent" label="生产企业（证件持有人）名称"></el-table-column>
              <el-table-column property="fadent" label="广告中标识的生产企业（证件持有人）名称"></el-table-column>
              <el-table-column property="fmanuno" label="生产批准文号"></el-table-column>
              <el-table-column property="fadmanuno" label="广告中生产批准文号"></el-table-column>
              <el-table-column property="fapprno" label="广告批准文号"></el-table-column>
              <el-table-column property="fadapprno" label="广告中广告批准文号"></el-table-column>
              <el-table-column property="fentzone" label="生产企业（证件持有人）所在地区"></el-table-column>
            </el-table>
          </el-dialog>
        </el-form>

      </el-col>
    </el-row>
    <el-dialog title="请选择剪辑错误" :visible.sync="showCutErrorForm" :modal="false">
      <submit-cut-error :taskid="task.taskid" :media-class="task.media_class" ref="submitCutError" @success="submitCutErrorSuccess"></submit-cut-error>
    </el-dialog>
  </div>`,
  props: {
    task: {
      type: Object,
      default: () => ({})
    },
    fromManage: {
      type: Boolean,
      default: false
    }
  },
  components: {
    taskInfo: this,
    submitCutError
  },
  name: 'taskInfo',
  data: function () {
    const validateAdBrand = (rule, value, callback) => {
      if (!value) {
        callback()
        return false
      }
      if (value.indexOf('&') === -1 && value !== '无突出品牌' && value.indexOf('无突出品牌') !== -1) {
        callback(new Error('如果无突出品牌, 此栏就只允许填写无突出品牌'))
      }
      // 获取广告名称, 取出广告名称中有多少个&
      let adNameAndCount = this.task.fadname.match(/&/g)
      adNameAndCount = adNameAndCount ? adNameAndCount.length : 0
      let adBrandAndCount = this.task.fbrand.match(/&/g)
      adBrandAndCount = adBrandAndCount ? adBrandAndCount.length : 0
      if (adNameAndCount < adBrandAndCount) {
        callback(new Error('广告名称中的&数量与广告品牌中的&数量不同'))
      }
      // 品牌可能是多个
      if (value.indexOf('&') !== -1) {
        if (value.split('&').some(v => v !== '无突出品牌' && !v.match(/^.牌|&.牌/) && v.indexOf('牌') !== -1)) {
          this.$message('请确认录入是否正确, 一般情况下品牌中不需要输入XX牌')
        }
      } else {
        if (!value.match(/^.牌|&.牌/) && value !== '无突出品牌' && value.indexOf('牌') !== -1) {
          this.$message('请确认录入是否正确, 一般情况下品牌中不需要输入XX牌')
        }
      }
      if (value.match(/第\D届/)) {
        this.$message('请确认录入是否正确, 届数需要用数字')
      }
      if (value.match(/\D年/)) {
        this.$message('请确认录入是否正确, 年份需要用数字')
      }
      if (value.match(/&$/)) {
        callback('广告品牌不允许以&结尾')
      }
      callback()
    }
    const validateAdOwner = (rule, value, callback) => {
      if (value && value.indexOf('广告主不详') !== -1 && value !== '广告主不详') {
        callback(new Error('如果广告主不详, 此栏就只允许填写广告主不详'))
      }
      callback()
    }
    const validateAdName = (rule, value, callback) => {
      if (!value) {
        callback()
        return
      }
      // 广告可能是多个
      if (value && value.indexOf('牌') !== -1) {
        this.$message('请确认录入是否正确, 一般情况下广告名称中不需要输入XX牌')
      }
      if (value.match(/第\D届/)) {
        this.$message('请确认录入是否正确, 届数需要用数字')
      }
      if (value.match(/\D年/)) {
        this.$message('请确认录入是否正确, 年份需要用数字')
      }
      if (value.match(/&$/)) {
        callback('广告名称不允许以&结尾')
      }
      callback()
    }

    return {
      loading: false,
      viewerInstance: {
        destroy: () => {}
      },
      adClassTree,
      illegalTypeArr,
      adClassArr,
      illegalArr,
      type,
      rate: 1,
      rule: {
        fadname: [{
            required: true,
            message: '请填写广告名称',
            trigger: 'submit'
          },
          {
            validator: validateAdName,
            trigger: 'change'
          },
        ],
        fname: [{
            required: true,
            message: '请填写广告主',
            trigger: 'submit'
          },
          {
            validator: validateAdOwner,
            trigger: 'change'
          },
        ],
        fillegaltypecode: [{
          required: true,
          message: '请选择违法程度',
          trigger: 'change'
        }],
        fillegalcontent: [{
          required: true,
          message: '请填写违法内容',
          trigger: 'change'
        }],
        fexpressioncodes: [{
          required: true,
          message: '请选择违法表现',
          trigger: 'change'
        }],
        fbrand: [{
            required: true,
            message: '请输入品牌',
            trigger: 'submit'
          },
          {
            validator: validateAdBrand,
            trigger: 'change'
          },
        ],
        adCateArr2: [{
          required: true,
          message: '请选择商业类别',
          trigger: 'submit'
        }],
        adCateArr: [{
          required: true,
          message: '请选择广告类别',
          trigger: 'submit'
        }],
        is_long_ad: [{
          required: true,
          message: '请选择',
          trigger: 'submit'
        }],
      },
      activeTabName: 'adInfo',
      showMoreInfo: false,
      showFillDataList: false,
      illegalKeyword: '',
      remainTime: 1200,
      intervalId: 0,
      sameAdNameTaskList: [],
      showTaskInfo: false,
      checkTaskInfo: {},
      showCutErrorForm: false,
      showFillBtn: false,
      showAdText: true,
      showClassTip: false,
    }
  },
  computed: {
    adType: function () {
      const obj = {
        '1': 'tv',
        '2': 'bc',
        '3': 'paper'
      }
      return obj[this.task.media_class] || null
    },
    isIllegal: function () {
      return this.task.fillegaltypecode !== '0'
    },
    receiveTime: function () {
      return this.task.lasttime ? moment.unix(this.task.lasttime).format('YYYY-MM-DD HH:mm:ss') : ''
    },
    tempLock: function () {
      return moment(this.task.issue_date).unix() < moment('2018-08-01').unix()
    },
  },
  watch: {
    illegalKeyword(val) {
      this.$refs.illegalSelecter.filter(val);
    }
  },
  methods: {
    adNameBlur() {
      this.showClassTip = $h.testAdName(this.task.fadname)
      this.showAdText = false
    },
    dataInit: function () {
      clearInterval(this.intervalId)
      this.remainTime = 1200
      this.showCutErrorForm = false
      this.showFillBtn = false
      this.loading = false
      this.type = type
      this.activeTabName = 'adInfo'
      this.showMoreInfo = this.type !== 'input'
      this.showFillDataList = false
      this.sameAdNameTaskList = []
      this.checkTaskInfo = {}
      this.illegalKeyword = ''
      if (this.adType === 'paper') {
        Vue.set(this.task, 'paperIsSure', false)
        this.viewerInit()
      } else {
        this.rateChange()
        try {
          this.viewerInstance.destroy()
        } catch (e) {

        }
      }
      this.inputAdName()
      this.task.is_long_ad = this.task.is_long_ad || '0'
      try {
        if (this.task.fadclasscode) {
          var adClass = [];
          if (this.task.fadclasscode.length === 4) {
            adClass.push(this.task.fadclasscode.substr(0, 2))
            adClass.push(this.task.fadclasscode)
          }
          if (this.task.fadclasscode.length === 2) {
            adClass.push(this.task.fadclasscode)
          }
          if (adClass.length === 0) {
            Vue.set(this.task, 'fadclasscode', '')
          } else {
            Vue.set(this.task, 'fadclasscode', adClass[adClass.length - 1])
          }
          Vue.set(this.task, 'adCateArr', adClass)
        } else {
          Vue.set(this.task, 'adCateArr', [])
          Vue.set(this.task, 'fadclasscode', '')
        }
        if (this.task.fadclasscode_v2) {
          var adClass2 = [];
          for (var i = 2; i <= this.task.fadclasscode_v2.length; i += 2) {
            adClass2.push(this.task.fadclasscode_v2.substr(0, i))
          }
          if (adClass2.length === 0) {
            Vue.set(this.task, 'fadclasscode_v2', '')
          } else {
            Vue.set(this.task, 'fadclasscode_v2', adClass2[adClass2.length - 1])
          }
          Vue.set(this.task, 'adCateArr2', adClass2)
        } else {
          Vue.set(this.task, 'adCateArr2', [])
          Vue.set(this.task, 'fadclasscode_v2', '')
        }
        if (!this.task.fadclasscode_v2) {
          Vue.set(this.task, 'fadclasscode_v2', 3203)
        }
      } catch (e) {}
      if (this.type == 'input' && this.task.task_state == 1) {
        this.intervalId = setInterval(this.timer, 1000)
      }
    },
    timer: function () {
      this.remainTime = moment.unix(+this.task.lasttime + 1200).diff(moment(), 'seconds')
      if (this.remainTime <= 0) {
        let tempId = setInterval(() => {})
        while (tempId > 0) {
          clearInterval(tempId)
          tempId--
        }
        $.post('/Api/TaskInput/back_overtime_task').then(() => {
          this.$alert('抱歉, 任务超时被收回, 点击确定开始下一个任务').then(() => {
            this.$emit('next')
          })
        })
      }
    },
    viewerInit: function () {
      this.$nextTick(() => {
        try {
          this.viewerInstance.destroy()
          this.viewerInstance = new Viewer(document.querySelector('.sample-content img'), {
            inline: true,
            toolbar: false,
            navbar: false
          })
        } catch (e) {}
      })
    },
    rateChange: function (value) {
      value = value || 1
      const selector = this.adType === 'tv' ? '.sample-content video' : '.sample-content audio'
      try {
        const dom = document.querySelector(selector)
        dom.playbackRate = value;
      } catch (e) {}
    },
    reformTask: function () {
      this.$confirm('确认重做任务?').then(() => {
        this.type = 'input'
        this.inputAdName();
      }).catch(() => {})
    },
    searchAdName: function (queryString, cb) {
      $.post("/Api/Ad/searchAdByName", {
        adName: queryString
      }).then((res) => {
        cb(res.data);
      })
    },
    searchAdOwner: function (queryString, cb) {
      $.post("/Api/Adowner/searchAdOwner", {
        name: queryString
      }).then((res) => {
        cb(res.data);
      })
    },
    changeFormAdClass2: function (value) {
      var code = value[value.length - 1]
      Vue.set(this.task, 'fadclasscode_v2', code)
      $.post('/Api/Adclass/getAdClassA', {
        code: code
      }).then((res) => {
        if (res.code == 0) {
          let fadclasscode = res.data
          let adCateArr = this._processAdClass(res.data)
          if (this.task.fadclasscode) {
            this.$confirm('该广告已有广告类别, 是否改为商业类别对应的广告类别 ?').then(() => {
              this._updateAdCateArr(adCateArr, fadclasscode)
            }).catch(() => {})
          } else {
            this._updateAdCateArr(adCateArr, fadclasscode)
          }
        } else {
          this.$message('该分类未关联广告类别, 请手动选择')
        }
      })
    },
    _processAdClass: function (data) {
      let adClass = [];
      if (data.length === 6) {
        adClass.push(data.substr(0, 2))
        adClass.push(data.substr(0, 4))
        adClass.push(data)
      }
      if (data.length === 4) {
        adClass.push(data.substr(0, 2))
        adClass.push(data)
      }
      if (data.length === 2) {
        adClass.push(data)
      }
      return adClass
    },
    _updateAdCateArr: function (adCateArr, fadclasscode) {
      Vue.set(this.task, 'adCateArr', adCateArr)
      Vue.set(this.task, 'fadclasscode', fadclasscode)
    },
    toggleShowMoreInfo: function () {
      if (!this.showMoreInfo) {
        this.loading = true
        var typeMap = {
          paper: 'get_paper_ad_more_field',
          tv: 'get_tv_ad_more_field',
          bc: 'get_bc_ad_more_field'
        }
        $.post('/api/ad/' + typeMap[this.adType], {
          fadname: this.task.fadname
        }).then((res) => {
          if (res.code == 0) {
            this.task.fillData = res.moreFieldList
            if (this.task.fillData.length !== 0) {
              this.showFillDataList = true
            }
          }
          this.loading = false
        })
      }
      this.showMoreInfo = !this.showMoreInfo
    },
    filterNode(value, data) {
      if (!value) return true;
      return data.label.indexOf(value) !== -1;
    },
    selectIllegal: function (data, isChecked) {
      if (data.fpcode === '') {
        return false
      }
      this.task.checkedIllegalArr = this.task.checkedIllegalArr || []
      var index = this.task.checkedIllegalArr.indexOf(data);
      if (isChecked && index === -1 && data.fpcode !== '') {
        this.task.checkedIllegalArr.push(data)
      }
      if (!isChecked && index !== -1) {
        this.task.checkedIllegalArr.splice(index, 1)
      }
      this.changeIllegal()
      if (this.task.checkedIllegalArr.length > 0) {
        var maxCode = 0
        this.task.checkedIllegalArr.forEach((v) => {
          if (v.fillegaltype > maxCode) {
            maxCode = v.fillegaltype
          }
        })
        Vue.set(this.task, 'fillegaltypecode', maxCode)
      }
      this.$forceUpdate()
    },
    deleteIllegal: function (index) {
      if (this.type != 'input' || this.tempLock) {
        return false
      }
      this.task.checkedIllegalArr.splice(index, 1);
      this.$refs.illegalSelecter.setCheckedNodes(this.task.checkedIllegalArr);
      this.changeIllegal()
    },
    changeIllegal: function () {
      if (this.task.checkedIllegalArr.length === 0) {
        Vue.set(this.task, 'fexpressioncodes', '')
      }
      var arr = this.task.checkedIllegalArr.map((v) => {
        return v.fcode;
      });
      var str = arr.join(';');
      Vue.set(this.task, 'fexpressioncodes', str)
    },
    onSubmit() {
      this.task.fadclasscode = this.task.adCateArr[this.task.adCateArr.length - 1]

      if (!$h.checkAdNameAndCode(this.task.fadname, this.task.fadclasscode)) {
        return this.$message.error('公益广告请按此格式填写：公益（xxxxxx）')
      }

      if (this.adType === 'paper' && !this.task.paperIsSure) {
        this.$message.warning('请确认版面无误后点击报纸上方查看广告按钮')
        return false
      }
      this.$refs['adInfo'].validate((res, fields) => {
        if (res) {
          let text = '确认提交?'
          if (this.sameAdNameTaskList.length > 0) {
            text = '此广告有不同判定, 仍然确认提交?'
          }
          this.$confirm(text).then(() => {
            if (this.loading) {
              this.$message.warning('正在提交中, 请不要重复提交')
              return false
            }
            this.loading = true;
            var tagObj = {}
            if (this.task.adIsSure) {
              tagObj.isSure = 1
            }
            if (this.isIllegal) {
              tagObj.isIllegal = 1
            }
            if (this.task.fadclasscode == 2301) {
              this.loading = false
              this.$alert('当前广告分类非法, 请选择另一个分类')
              return false
            }
            this.task.fadclasscode_v2 = this.task.adCateArr2[this.task.adCateArr2.length - 1]
            this.task.fadname = this.task.fadname.replace(/＆/g, '&')
            this.task.fbrand = this.task.fbrand.replace(/＆/g, '&')
            Vue.set(this.task, 'fbrand', this.task.fbrand)
            Vue.set(this.task, 'fadclasscode', this.task.fadclasscode)
            if (!this.task.fadclasscode_v2) {
              Vue.set(this.task, 'fadclasscode_v2', 3203)
            }
            let url = this.fromManage ? '/Admin/TaskInput/submitAdInputTask' : 'submitTask'
            $.post(url, {
              data: this.task,
              tags: tagObj
            }).then((res) => {
              if (res.code == 0) {
                this.$confirm('提交成功, 是否继续做新任务?', '提交成功', {
                  confirmButtonText: '继续',
                  cancelButtonText: '取消',
                  type: 'success',
                }).then(() => {
                  this.$emit('next')
                }).catch(() => {
                  this.$emit('back')
                })
              } else {
                this.$message({
                  message: res.msg,
                  type: 'error'
                })
                this.$emit('error')
              }
            })
          }).catch(() => {})
        } else {
          var illegalFieldArr = ['fillegaltypecode', 'fillegalcontent', 'fexpressioncodes'];
          var key = Object.keys(fields)[0];
          if (illegalFieldArr.indexOf(key) !== -1) {
            this.activeTabName = 'illegalInfo'
          } else {
            this.activeTabName = 'adInfo'
          }
          var msg = fields[Object.keys(fields)[0]][0].message;
          this.$alert(msg, '提示')
        }
      })
    },
    inputAdName: function () {
      this.searchSameAdNameTask()
      if (this.type != 'input' || !this.task.fadname) {
        return false
      }
      $.post('/Api/Ad/checkAdIsSureByName', {
        adName: this.task.fadname
      }).then((res) => {
        if (res.data.is_sure != 0) {
          this.task.adIsSure = true
        } else {
          this.task.adIsSure = false
        }
        if (res.data.fadid) {
          this.task = Object.assign(this.task, res.data)
          this.task.adCateArr = this._processAdClass(this.task.fadclasscode)
          this.task.adCateArr2 = this._processAdClass(this.task.fadclasscode_v2)
        }
        this.$forceUpdate()
      })
    },
    searchSameAdNameTask: function () {
      if (!this.task.fadname) {
        return false
      }
      let data = {
        adName: this.task.fadname,
        mediaClass: this.task.media_class,
        taskId: this.task.taskid
      }
      if (this.task.media_class != 3) {
        data.adLen = this.task.fadlen
      }
      $.post('/Api/TaskInput/searchSameAdNameTask', data).then((res) => {
        if (res.code == 0) {
          this.sameAdNameTaskList = res.data
        } else if (res.code == -1) {
          this.sameAdNameTaskList = []
        }
      })

    },
    selectMoreInfoRow: function (row) {
      Object.assign(this.task, row)
    },
    submitCutErrorSuccess: function () {
      this.$emit('next')
    },
    openCutErrorForm: function () {
      this.showCutErrorForm = true
      this.$nextTick(() => {
        this.$refs.submitCutError.dataInit()
      })
    },
    confirmPaper: function () {
      this.task.paperIsSure = true
      this.viewerInit()
    },
    checkPaper: function () {
      this.task.paperIsSure = false
      this.viewerInit()
    },
    submitRecallRequest: function () {
      this.$confirm('是否发起撤回请求?待质检员审核确认后任务将退回到被退回状态', '是否确认?', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'danger'
      }).then(() => {
        $.post('receiveRecallRequest', {
          taskid: this.task.taskid,
          taskType: 1
        }).then((res) => {
          if (res.code == -2 && res.msg == 'need log in again') {
            this.$message.error('登录状态过期,请再次登录')
          }
          this.$message({
            message: res.msg,
            type: res.type,
          })
          this.$emit('back')
        })
      }).catch(() => {})
    },
    passTask: function () {
      this.$confirm('通过通过任务?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
      }).then(() => {
        const data = {
          taskid: this.task.taskid,
          taskType: 1,
        }
        this.loading = true
        $.post('passTask', data).then((res) => {
          this.loading = false
          if (res.code == -2 && res.msg == 'need log in again') {
            this.$message.error('登录状态过期,请再次登录')
          }
          if (res.code == 0) {
            this.$message({
              type: 'success',
              message: '通过成功'
            })
          } else {
            this.$message({
              type: 'error',
              message: '通过失败'
            })
          }
          this.$emit('back')
        })
      }).catch(() => {})
    },
    rejectTask: function () {
      this.$prompt('确认退回任务?', '提示', {
        confirmButtonText: '提交',
        cancelButtonText: '取消',
        inputPattern: /\S/,
        inputErrorMessage: '请输入退回说明'
      }).then((value) => {
        const data = {
          taskid: this.task.taskid,
          taskType: 1,
          reason: value.value,
          mediaClass: this.task.media_class
        }
        this.loading = true
        $.post('submitRejectTaskRequest', data).then((res) => {
          this.loading = false
          if (res.code == -2 && res.msg == 'need log in again') {
            this.$message.error('登录状态过期,请再次登录')
          }
          if (res.code == 0) {
            this.$message({
              type: 'success',
              message: '成功退回'
            })
          } else {
            this.$message({
              type: 'error',
              message: '退回失败'
            })
          }
          this.$emit('back')
        })
      }).catch(() => {})
    },
    checkTask: function (taskid) {
      this.loading = true
      $('video').each(function () {
        this.pause()
      })
      $('audio').each(function () {
        this.pause()
      })
      $.post('index', {
        taskid: taskid,
        taskType: 1
      }).then((res) => {
        if (res.code == 0) {
          this.checkTaskInfo = res.data
        } else {
          this.$message.warning(res.msg)
        }
        this.showTaskInfo = true
        this.loading = false
        this.$nextTick(() => {
          this.$refs.task.dataInit()
          this.$refs.task.type = 'quality'
          if (this.type === 'input') {
            this.$refs.task.showFillBtn = true
          }
        })
      })
    },
    closeTaskInfo: function () {
      this.$emit('close')
      this.showTaskInfo = false
      this.checkTaskInfo = {}
    },
    appendAbBrand: function () {
      if (!this.task.fbrand) {
        this.task.fbrand = '无突出品牌'
        return true
      }
      if (this._validateAdBrand()) {
        this.task.fbrand += '&无突出品牌'
      } else {
        this.$message.warning('品牌中的&数量不得超过广告名称中&的数量')
      }
    },
    _validateAdBrand: function () {
      if (!this.task.fadname) {
        this.$message.warning('请输入广告名称')
        return false
      }
      if (!this.task.fbrand) {
        this.task.fbrand = ''
        return false
      }
      // 获取广告名称, 取出广告名称中有多少个&
      let adNameAndCount = this.task.fadname.match(/&\S/g)
      adNameAndCount = adNameAndCount ? adNameAndCount.length : 0
      let adBrandAndCount = this.task.fbrand.match(/&/g)
      adBrandAndCount = adBrandAndCount ? adBrandAndCount.length : 0
      // 只有品牌中的&数量小于等于名称中的&数量, 才允许添加
      return adNameAndCount > adBrandAndCount
    },
    fillTaskData: function () {
      this.$confirm('确认将此广告数据添加到正在录入数据?').then(() => {
        const fieldArr = ['fspokesman', 'fversion', 'fmanuno', 'fadmanuno', 'fapprno', 'fadapprno', 'fent', 'fadent', 'fentzone', 'fillegaltypecode', 'fexpressioncodes', 'fillegalcontent', 'is_long_ad', 'checkedIllegalArr', 'fadname', 'fbrand', 'fname', 'adCateArr2', 'adCateArr', ]
        const data = {}
        fieldArr.forEach((v) => {
          if (this.task.hasOwnProperty(v)) {
            data[v] = this.task[v]
          }
        })
        this.$emit('fillTaskData', data)
        this.$emit('back')
      })
    },
    receiveFillTaskData: function (data) {
      this.task = Object.assign(this.task, data)
      this.dataInit()
      this.$nextTick(() => {
        Vue.set(this.task, 'adCateArr', data.hasOwnProperty('adCateArr') ? data.adCateArr : [])
        Vue.set(this.task, 'adCateArr2', data.hasOwnProperty('adCateArr2') ? data.adCateArr2 : [])
      })
    },
    adError: function () {
      this.$prompt('请填写广告错误处与正确信息', '提示', {
        confirmButtonText: '提交',
        cancelButtonText: '取消',
        inputPattern: /\S/,
        inputErrorMessage: '请输入错误之处与正确信息'
      }).then((value) => {
        const data = {
          fadid: this.task.fadid,
          comment: value.value
        }
        this.loading = true
        $.post('/api/ad/changeAdIsSure', data).then((res) => {
          this.loading = false
          if (res.code == -2 && res.msg == 'need log in again') {
            this.$message.error('登录状态过期,请再次登录')
          }
          if (res.code == 0) {
            this.$message({
              type: 'success',
              message: res.msg
            })
          } else {
            this.$message({
              type: 'error',
              message: res.msg
            })
          }
        })
      }).catch(() => {})
    },
    setAdName: function (adName) {
      if (this.task.fadname !== adName) {
        this.task.fadname = adName
        this.inputAdName()
      }
    }
  },
  filters: {
    taskStateFilter: function (value) {
      const arr = [
        '未分配',
        '任务中',
        '已完成',
        '',
        '被退回',
        '等待撤回通过'
      ]
      return arr[+value]
    }
  }
}