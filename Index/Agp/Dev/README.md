# vue 开发模版

## 目录
+ [vue 开发模版](#vue-开发模版)
  - [前提](#前提)
  - [注意事项](#注意事项)
  - [模板简介](#模板简介)
  - [模板目录](#模板目录)
  - [插件配置](#插件配置)
  - [插件说明](#插件说明)
    + [sass](#sass)
    + [element-ui](#element-ui)
    + [echarts](#echarts)
      - [在项目中使用](#在项目中使用)
    + [axios](#axios)
    + [faker](#faker)
    + [定制 element-ui](#定制-element-ui)
  - [运行模版](#运行模版)


#### 前提

此模板的使用前提为系统中装有 `node.js` 环境: `node.js` 安装方法移步 [官网](http://nodejs.cn/download/) 或 [教程](`node.js`) （`node.js` 安装包中自带 `npm` 包，无需自行安装）。

#### 注意事项

模版中某些插件的使用需要安装全局依赖，请先确认是否使用了以下插件：

* element-theme-chalk
* faker

如要使用插件，请先执行以下命令：

**注：由于国内网络环境因素，不推荐使用 `npm` 命令。推荐使用 `cnpm` 命令。**

`cnpm` 安装命令：

```bash
npm install -g cnpm --registry=https://registry.npm.taobao.org
```
安装完成后所有的 `npm` 命令都可以使用 `cnpm` 命令运行。

```bash
#使用 element-theme-chalk 必须安装此依赖
cnpm i element-theme -g
#使用 faker 必须安装此依赖
cnpm install faker -G
```

#### 模板简介

此模版以 `vue-cli` 为基础建立，已配置好开发中基本会用到的插件，开发中情根据实际情况使用插件。

#### 模板目录

``

#### 插件配置
在克隆模版到本地后，打开`/package.json`，在 `dependencies` 和 `devDependencies` 中分别是生产环境和开发环境中用到的插件，如要删除不会用到的插件，请在此文件中删除对应的插件名。

**注：`package.json` 为 `json` 格式文件，请勿使用注释方式删除插件！**

#### 插件说明

##### sass

css 预处理器，可提高项目的开发效率和可维护性

* node-sass
* sass-loader
* sass-resources-loader

如果项目中不使用 `sass` ：

1. 在 `package.json` 文件中删除这三项。

2. 在 `/build/utils.js` 中删除以下代码：
```javascript
scss: generateLoaders('sass').concat(
  {
    loader: 'sass-resources-loader',
    options: {
      //你要引入的 .scss 文件的路径
      resources: path.resolve(__dirname, '../src/assets/global.scss')
    }
  }
)
```
3. 删除 `/src/assets/` 目录下的 `global.scss` 文件。

##### element-ui

* element-ui

饿了么开源的框架

如不在项目中使用 `element-ui` ：

1. 在 `package.json` 文件中删除此项。

2. 在 `/src/main.js` 文件中删除如下代码：
```javascript
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)
```

##### echarts

百度开源的图表，其中 `echarts-liquidfill` 依赖于 `echarts` 。如不使用 `echarts` ，请连同 `echarts-liquidfill` 一并删除。

* echarts
* echarts-liquidfill

###### 在项目中使用

在 `.vue` 文件中引入后就能使用：

```javascript
import echarts from "echarts"
import "echarts-liquidfill/src/liquidFill.js"
```

如不在项目中使用 `echarts` ：

1. 在 `package.json` 文件中删除这两项。


##### axios

`Vue` 官方推荐的 `AJAX` 插件，推荐在项目中使用，使用方式请移步 [官方文档](https://github.com/axios/axios) 或自行百度。

如不在项目中使用 `axios` ：

1. 在 `package.json` 文件中删除此项。

##### faker

用于后端没数据时模拟数据

配置方法请移步 [教程](https://www.jianshu.com/p/ccd53488a61b) 。

如不在项目中使用 `faker` ：

1. 在 `package.json` 文件中删除此项。
2. 删除 `/mock` 文件夹。
3. 清空 `/config/index.js` 文件中 `proxyTable` 对象。


##### 定制 element-ui

下列插件用于定制 element-ui ，若使用默认 ui ，则无需安装。

* element-theme
* element-theme-chalk

如项目不使用定制 ui ：

1. 在 `package.json` 文件中删除 `element-theme-chalk`。

#### 运行模版

完成全局插件以及 `package.json` 的配置后，使用命令行工具 cd 至项目目录，执行：

```bash
#安装 package.json 中的依赖
cnpm install
```

依赖安装完成之后，若使用了 `faker` 插件，则运行：

```bash
#启动本地服务器，运行此命令使用 npm
npm run mockdev
```

如果项目正常启动，那么在控制台中应该可以看到一个请求。

若未使用 `faker` 插件，则运行：

```bash
#启动本地服务器，运行此命令使用 npm
npm run dev
```

至此，模版运行成功，可以正常进行项目开发。