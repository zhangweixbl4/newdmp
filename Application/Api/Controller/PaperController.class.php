<?php
namespace Api\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use Think\Controller;
use Think\Exception;


class PaperController extends Controller {
	
	
	public function _initialize() {

        header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}
	
	
	
	/*改变报纸剪辑的优先级*/
	public function change_cut_priority(){
        ini_set('memory_limit','800M');
        set_time_limit(300);
		$fmonths = array();
		$fmonths[] = date('Y-m-01');
		$fmonths[] = date('Y-m-01',strtotime("-1 months", time()));


//		M()->startTrans(); //开启事务


		foreach($fmonths as $fmonth){
			$zongju_data_confirm = M('zongju_data_confirm')->field('condition,dmp_confirm_content,zongju_confirm_content')->where(array('fmonth'=>$fmonth))->find();

            $condition = json_decode($zongju_data_confirm['condition'],true);
			$dmp_confirm_content = json_decode($zongju_data_confirm['dmp_confirm_content'],true);
			$zongju_confirm_content = json_decode($zongju_data_confirm['zongju_confirm_content'],true);
			
			//var_dump($condition['paper']);//报纸需要做哪些天的数据
			$days = array();
            $dayWhere = [];

            foreach($condition['paper'] as $day){
				$days[] = date('Y-m-'.$day,strtotime($fmonth));
                $dayWhere['paper'][] = date('Y-m-'.$day,strtotime($fmonth));
			}
            foreach ($condition['tv'] as $item) {
                $dayWhere['tv'][] = date('Y-m-'.$item,strtotime($fmonth));
            }
            foreach ($condition['bc'] as $item) {
                $dayWhere['bc'][] = date('Y-m-'.$item,strtotime($fmonth));
            }

            // 地方局客户优先级是最高的, 优先级改为3000
            $dfjLabelIds = M('tlabel')->cache(true)->where([
                'fstate' => ['EQ', 1],
                'flaebl' => ['NOT IN', ['国家局', '食药局', '快剪']],
            ])->getField('flabelid', true);
            $dfjMediaIds = M('tmedialabel')->cache(true)->where([
                'flabelid' => ['IN', $dfjLabelIds],
                'fstate' => ['EQ', 1],
            ])->getField('fmediaid', true);
            // 修改地方局客户任务优先级的时候, 昨天的数据是最优先做的
            M('task_input')
                ->where(array(
                    'media_id'=>['in',$dfjMediaIds],
//                    'priority' => ['IN', [1000, 3001]],
                    'priority' => ['EQ', 1000],
                ))
                ->save(array('priority'=>3000));
            M('task_input')
                ->where(array(
                    'media_id'=>['in',$dfjMediaIds],
                    'issue_date' => ['EQ', date('Y-m-d', strtotime('-1 days'))],
                    'priority' => ['EQ', 3000]
                ))
                ->save(array('priority'=>3500));
            M('tpapersource')
                ->where(array(
                    'fmediaid'=>['in',$dfjMediaIds],
                    'priority' => ['EQ', 1000]
                ))
                ->save(array('priority'=>3000));

            $gjjlabelMedia = M('tmedialabel')->cache(true,600)->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')->where(array('tlabel.flabel'=>'国家局'))->getField('fmediaid',true);//查询国家局标签
            // 国家局媒体范围内的, 优先级调高, 但不是最高
            M('tpapersource')
                ->where(array(
                    'fmediaid'=>array('in',$gjjlabelMedia),
                    'priority' => ['EQ', 1000]
                ))
                ->save(array('priority'=>2000));
            M('cut_task')
                ->where(array(
                    'fmediaid'=>array('in',$gjjlabelMedia),
                    'priority' => ['EQ', 1000]
                ))
                ->save(array('priority'=>2000));
            M('task_input')
                ->where(array(
                    'media_id'=>array('in',$gjjlabelMedia),
                    'priority' => ['EQ', 1000]
                ))
                ->save(array('priority'=>2000));
			if($dayWhere){
			    // 调整版面剪辑优先级
				$change_num = M('tpapersource')
								
								->where(array(
												'fmediaid'=>array('in',$gjjlabelMedia),
												'fissuedate'=>array('in',$days),
												'priority'=>array('EQ',2000)
											))
								->save(array('priority'=>3111));//修改国家局范围内的监测日期的优先级

                // 批量修改快剪任务优先级
                M('cut_task')
                    ->where(array(
                        'fmediaid'=>array('in',$gjjlabelMedia),
                        'fissuedate'=>array('in',$days),
                        'priority'=>array('EQ',2000)
                    ))
                    ->save(array('priority'=>3111));//修改国家局范围内的监测日期的优先级

                // 批量修改抽查日期内众包任务的优先级
                try{
//                    $this->batchChangeTaskPriority($dayWhere);
                }catch (Exception $e){
                    A('Adinput/Weixin','Model')->task_back(450,'批量更新优先级失败','请尽快处理');
                }

			}

			foreach($dmp_confirm_content['paper'] as $paperConfirmContent){//报纸数据哪些天的已经确认了
				$change_num = 0;
				if($paperConfirmContent['selected']){
					$change_num = M('tpapersource')
								
								->where(array(
												'fmediaid'=>array('in',$paperConfirmContent['selected']),//已经确认的媒体
												'fissuedate'=>$paperConfirmContent['date'],//已经确认的日期
												'fstate'=>array('in','0,1'),						//只修改任务进行中的和初始状态的
											))
								->save(array('fstate'=>9));//修改已确认数据的任务状态为已终止
				}

			}
			
			
		}
		
//		M()->commit(); //成功提交事务

		//M()->rollback(); //失败回滚事务

		
	}


    /**
     * 图片重新上传
     * @param $paperid int 媒体表里面的fchannelid
     * @param $rq
     * @param $bm
     * @param $furl
     */
    public function fixPaper($paperid, $rq, $bm, $furl)
    {
        $rq = date('Y-m-d', strtotime($rq));
        echo M('tpapersource')
            ->where([
                'fmediaid' => ['EXP', " IN (select fid from tmedia where fchannelid = $paperid)"],
                'fissuedate' => ['EQ', $rq],
                'fpage' => ['EQ', $bm]
            ])
            ->save([
                'furl' => $furl,
                'fstate' => 0,
                'fuserid' => 0,
            ]);
	}


    /**
     * 国家局抽查优先级批量更新
     * @param $days
     * @return bool
     */
    private function batchChangeTaskPriority($days)
    {
        $cacheData = S('zongju_check_batch_change_priority');
        if (!$cacheData){
            $this->changeTvBcTaskPriority($days['tv'], 1);
            S('zongju_check_batch_change_priority', [
                'last_time' => time(),
                'media_class' => 'tv'
            ], 3600*24);
            return true;
        }
        if ($cacheData['last_time'] + 3600 < time()){
            return true;
        }
        if ($cacheData['media_class'] == 'tv'){
            if ($days['tv']){
                $this->changeTvBcTaskPriority($days['tv'], 1);
            }
            S('zongju_check_batch_change_priority', [
                'last_time' => time(),
                'media_class' => 'bc'
            ], 3600*24);
        }elseif($cacheData['media_class'] == 'bc'){
            if ($days['bc']){
                $this->changeTvBcTaskPriority($days['bc'], 2);
            }
            S('zongju_check_batch_change_priority', [
                'last_time' => time(),
                'media_class' => 'paper'
            ], 3600*24);
        }else{
            if ($days['paper']){
                M('task_input')
                    ->where(array(
                        'issue_date'=>array('in',$days['paper']),
                        'media_class' => ['EQ', 3],
                        'task_state' => ['EQ', 0],
                        'cut_err_state' => ['EQ', 0],
                        'priority' => ['EQ', 2000],
                    ))
                    ->save(array('priority'=>3111));
                }
            S('zongju_check_batch_change_priority', [
                'last_time' => time(),
                'media_class' => 'tv'
            ], 3600*24);
        }
        return true;

	}

    /**
     * @param $days array 发布日期限制  ['2018-08-01', '2018-08-02']
     * @param $type string 1:电视  2: 广播
     * @return bool
     */
    private function changeTvBcTaskPriority($days, $type)
    {
        if (!$days){
            return true;
        }
        $labelId = $type == 1 ? 243 : 245;
        $sam_list = M('tbn_ad_summary_day')
            ->where([
                'fdate' => ['IN', $days],
                'fmedia_class_code' => ['EQ', '0'.$type],
                'fmediaid' => ['IN', M('tmedialabel')->cache(true,600)->where(array('flabelid'=>$labelId))->getField('fmediaid',true)]
            ])
            ->getField('fad_sam_list', true);
        $sampleIds = [];
        foreach ($sam_list as $key => $rowItem) {
            $idAndMediaClassStrArr = explode(',', trim($rowItem,','));
            foreach ($idAndMediaClassStrArr as $samItem) {
                if (!$samItem){
                    continue;
                }
                $idAndMediaClassArr = explode('_', $samItem);
                if ($idAndMediaClassArr[0]){
                    $sampleIds[] = $idAndMediaClassArr[0];
                }
                if (count($sampleIds) > 1000){
                    $this->changeTvBcTaskPriority2($sampleIds, $type);
                    unset($sampleIds);
                    $sampleIds = [];
                }
            }
        }
        if (!$sampleIds){
            return true;
        }
        $this->changeTvBcTaskPriority2($sampleIds, $type);
    }

    private function changeTvBcTaskPriority2($ids, $type)
    {
        $tableName = $type == 1 ? 'ttvsample' : 'tbcsample';
        $uuids = M($tableName)
            ->field('uuid')
            ->where([
                'fid' => ['IN',$ids]
            ])
            ->group('uuid')
            ->buildSql();
        $taskIdSubQuery = M('task_input')
            ->field('taskid')
            ->where([
                'task_state' => ['EQ', 0],
                'cut_err_state' => ['EQ', 0],
                'media_class' => ['EQ', $type],
                'sam_uuid' => ['EXP', ' IN '.$uuids]
            ])
            ->buildSql();
        M('task_input')
            ->where([
                'taskid' => ['EXP', ' IN '.$taskIdSubQuery]
            ])
            ->save([
                'priority' => '3111'
            ]);
    }

    public function unlockChangeTaskPriority($code)
    {
        if ($code!=date('Ymd')){
            return false;
        }
        S('zongju_check_batch_change_priority', null);
    }


}