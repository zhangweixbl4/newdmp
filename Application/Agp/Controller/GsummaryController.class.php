<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 汇总数据
 */

class GsummaryController extends BaseController
{
	/**
	 * 广告联盟数据汇总
	 * by zw
	 */
	public function trackerData(){
		$issueDate = I('issueDate');//时间段，数组
		$whereNcr = '1=1';
		$ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $tregionlevel = $ALL_CONFIG['tregionlevel'];

		if($tregionlevel == 30){//省级
			$whereNcr .= ' and c.fregion_id like "'.substr($system_num,0,2).'%"';
		}elseif($tregionlevel == 20){//市级
			$whereNcr .= ' and c.fregion_id like "'.substr($system_num,0,4).'%"';
		}elseif($tregionlevel == 10){//县级
			$whereNcr .= ' and c.fregion_id like "'.substr($system_num,0,6).'%"';
		}
		if(!empty($issueDate)){
			$whereNcr .= ' and a.issue_date between "'.$issueDate[0].'" and "'.$issueDate[1].'"';
		}else{
			$whereNcr .= ' and a.issue_date between "'.date('Y-m-01').'" and "'.date('Y-m-d').'"';
		}
		$whereNcr .= ' and a.fstatus = 6';
		$whereNcr .= ' and c.trackerid <> 0';
		$whereNcr .= ' and a.ftaskid <> 0';
	
		$sqlstr = 'select websitename,sum(tiaocicount) tiaocicount 
			from (select c.websitename,c.trackerid,count(*) tiaocicount from tnetissue_customer a
				inner join trackers_record b on b.tid = a.id
				inner join tracker c on b.trackerid = c.trackerid
				where '.$whereNcr.'
				group by c.trackerid) a
			group by websitename
			order by tiaocicount desc
		';
		$doNcr = M()->query($sqlstr);

		$hejidata = 0;
		foreach ($doNcr as $key => $value) {
			$hejidata += $value['tiaocicount'];
		}
		$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$doNcr,'data2'=>$hejidata]);
	}

}