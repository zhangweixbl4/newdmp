<?php
//广告主控制器
namespace Admin\Controller;
use Think\Controller;
class AdownerController extends BaseController {
	// 接收JSON参数
    protected $P;
	/**
	 * @Des: 初始化
	 * @Edt: yuhou.wang
	 */	
    public function _initialize(){
        parent::_initialize();
        $this->P = json_decode(file_get_contents('php://input'),true);
	}
	/**
	 * @Des: 列表页
	 * @Edt: yuhou.wang
	 */	
    public function index(){
    	if(A('Admin/Authority','Model')->authority('200143') === 0){
			exit('没有权限');//验证是否有新增权限
		}
		$this->display();
	}
    /**
     * @Des: 获取广告主列表
     * @Edt: yuhou.wang
     * @param {String} keyword 关键字
     * @param {Int} region_id 行政区划
     * @param {Int} pageIndex 页码
     * @param {Int} pageSize 每页条数
     * @return: {JSON}
     */
	public function getList(){
		$pageIndex = $this->P['pageIndex'] ? $this->P['pageIndex'] : 1;
		$pageSize  = $this->P['pageSize'] ? $this->P['pageSize'] : 20;
		$keyword   = $this->P['keyword'];
		$region_id = $this->P['region_id'];
		$where = [
			'tadowner.fstate' => ['NEQ',-1],
			'tadowner.fid' => ['GT',0],
		];
		if(!empty($keyword)){
			$where['tadowner.fname|tadowner.faddress|tadowner.flinkman|tadowner.ftel|tadowner.fmail'] = ['LIKE','%'.$keyword.'%'];
		}
		if($region_id == 100000) $region_id = 0;
		if($region_id > 0){
			$region_id_rtrim = rtrim($region_id,'0');
			$region_id_strlen = strlen($region_id_rtrim);
			$where['LEFT(tadowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;
		}
		$count = M('tadowner')
			->join('tregion ON tregion.fid = tadowner.fregionid')
			->where($where)
			->count();
		$adownerList = [];
		if(!empty($count)){
			$adownerList = M('tadowner')
				->field('tadowner.*,tregion.ffullname as region_name')
				->join('tregion on tregion.fid = tadowner.fregionid')
				->where($where)
				->order('tadowner.fid desc')
				->page($pageIndex,$pageSize)
				->select();
		}
		$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$adownerList]);
	}
    /**
     * @Des: 获取广告主详情
     * @Edt: yuhou.wang
     * @param {String} fid 广告主ID
     * @return: {JSON}
     */
	public function getDetail(){
		$fid = (int)$this->P['fid'];
		$adownerDetails = M('tadowner')->where(['fid'=>$fid])->find();//查询广告主详情
		$adownerDetails['region_name'] = M('tregion')->where(['fid'=>$adownerDetails['fregionid']])->getField('ffullname');//查询行政区划的全称
		$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$adownerDetails]);
	}
    /**
     * @Des: 添加/编辑广告主
     * @Edt: yuhou.wang
     * @param {String} fid 广告主ID
     * @param {String} fname 广告主名称
     * @param {String} fregionid 行政区划
     * @param {String} faddress 地址
     * @param {String} flinkman 联系人
     * @param {String} ftel 联系电话
     * @param {String} fmail 邮件
     * @param {String} fstate 状态 -1删除，0-无效，1-有效
     * @return: {JSON}
     */
	public function saveAdowner(){
		$fid       = (int)$this->P['fid'];
		$fname     = $this->P['fname'];
		$fregionid = $this->P['fregionid'];
		$faddress  = $this->P['faddress'];
		$flinkman  = $this->P['flinkman'];
		$ftel      = $this->P['ftel'];
		$fmail     = $this->P['fmail'];
		$fcomment  = $this->P['fcomment'];
		$fstate    = $this->P['fstate'] ? $this->P['fstate'] : 1;//状态，-1删除，0-无效，1-有效
		$data = [
			'fname'     => $fname,
			'fregionid' => $fregionid,
			'faddress'  => $faddress,
			'flinkman'  => $flinkman,
			'ftel'      => $ftel,
			'fmail'     => $fmail,
			'fcomment'  => $fcomment,
			'fstate'    => $fstate,
		];
		if($fid == 0){
			// 新增
			if(A('Admin/Authority','Model')->authority('200141') === 0){
				$this->ajaxReturn(['code'=>-1,'msg'=>'您没有相对应的权限']);//验证是否有新增权限
			}
			$is_repeat = M('tadowner')->where(['fname'=>$fname])->count();//查询广告主名称是否重复
			if($is_repeat > 0) $this->ajaxReturn(['code'=>-1,'msg'=>'广告主名称重复']);//广告主名称重复
			$data['fsource']     = '创建';//来源
			$data['fcreator']    = session('personInfo.fname');//创建人
			$data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$data['fmodifier']   = session('personInfo.fname');//修改人
			$data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tadowner')->add($data);//添加数据
			if($rr > 0){//判断是否添加成功
				$this->ajaxReturn(['code'=>0,'msg'=>'添加成功']);//返回成功
			}else{
				$this->ajaxReturn(['code'=>-1,'msg'=>'添加失败,原因未知']);//返回失败
			}
		}else{
			//修改
			if(A('Admin/Authority','Model')->authority('200142') === 0){
				$this->ajaxReturn(['code'=>-1,'msg'=>'您没有相对应的权限']);//验证是否有新增权限
			}
			$is_repeat = M('tadowner')->where(['fid'=>['NEQ',$fid],'fstate'=>['GT',0],'fname'=>$fname])->count();//查询广告主名称是否重复
			if($is_repeat > 0) $this->ajaxReturn(['code'=>-1,'msg'=>'广告主名称重复']);//广告主名称重复
			$data['fmodifier'] = session('personInfo.fname');//修改人
			$data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tadowner')->where(['fid'=>$fid])->save($data);//修改数据
			if($rr > 0){//判断是否修改成功
				$this->ajaxReturn(['code'=>0,'msg'=>'修改成功']);//返回成功
			}else{
				$this->ajaxReturn(['code'=>-1,'msg'=>'修改失败,原因未知']);//返回失败
			}
		}
	}
    /**
     * @Des: 合并广告主
     * @Edt: yuhou.wang
     * @param {String} fids 广告主ID,多个以半角逗号分隔
     * @param {String} mfid 目标广告主ID,以此广告主为合并目标，其它将被删除
     * @return: {JSON}
     */
    public function mergeAdowner(){
		$fids	   = $this->P['fids'];
        $mfid      = $this->P['mfid'];
        $mergefids = explode(',',$fids);
		// 排除目标fids
		array_splice($mergefids,array_search($mfid,$mergefids),1);
        // 更新所有对fadowner引用
        $relatedTables = [
            'tad' => 'fadid'
        ];
        foreach($relatedTables as $table=>$key){
            // 记录合并日志
            $records = M($table)->field($key.',fadowner')->where(['fadowner'=>['IN',$mergefids]])->select();
            if(!empty($records)){
                $beforeInfo = [
                    'table' => $table,
                    'records' => json_encode($records),
                ];
                $logData = [
                    'fadowner_id' => $mfid,
                    'fbefore'     => json_encode($beforeInfo),
                    'fafter'      => '',
                    'fcreator_id' => session('personInfo.fid'),
                    'fcreator'    => session('personInfo.fname'),
                    'fcreatetime' => date('Y-m-d H:i:s'),
                    'fcomment'    => '广告主合并'
                ];
                M('tadowner_log')->add($logData);
                // 更新引用
                M($table)->where(['fadowner'=>['IN',$mergefids]])->save(['fadowner'=>$mfid]);
            }
        }
        // 删除目标外其它广告主记录
        $adOwnerRow = [
            'fstate'       => -1,
            'fcomment'     => '已被合并至:'.$mfid,
            'fmodifier'    => session('personInfo.fname'),
            'fmodifytime'  => date('Y-m-d H:i:s'),
        ];
        $res = M('tadowner')->where(['fid'=>['IN',$mergefids]])->save($adOwnerRow);
		$this->ajaxReturn(['code'=>0,'msg'=>'合并成功']);
	}
	
	// 获取广告主列表
    public function index_20191210_old(){
    	if(A('Admin/Authority','Model')->authority('200143') === 0){
			exit('没有权限');//验证是否有新增权限
		}
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$keyword = I('keyword');//搜索关键词
		$region_id = I('region_id');//地区ID
		if($region_id == 100000) $region_id = 0;//如果传值等于100000，相当于查全国
		$where = array();//查询条件
		$where['tadowner.fstate'] = array('neq',-1);
		$where['tadowner.fid'] = array('gt',0);
		if($keyword != ''){
			$where['tadowner.fname|tadowner.faddress|tadowner.flinkman|tadowner.ftel|tadowner.fmail'] = array('like','%'.$keyword.'%');
		} 
		if($region_id > 0){
			$region_id_rtrim = rtrim($region_id,'0');//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(tadowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		
		}
		$count = M('tadowner')->join('tregion on tregion.fid = tadowner.fregionid')->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$adownerList = M('tadowner')
			->field('tadowner.*,tregion.fname as region_name')
			->join('tregion on tregion.fid = tadowner.fregionid')
			->where($where)
			->order('tadowner.fid desc')
			->limit($Page->firstRow.','.$Page->listRows)->select();//查询广告主列表
		$adownerList = list_k($adownerList,$p,$pp);//为列表加上序号
		$this->assign('adownerList',$adownerList);//广告主列表
		$this->assign('page',$Page->show());//分页输出
		$this->display();
	}
	/*广告主添加、修改*/
	public function add_edit_adowner(){
		$fid= I('fid');//广告ID
		$fid = intval($fid);//转为数字
		$a_e_data = array();
		$a_e_data['fname'] = I('fname');//广告主名称
		$a_e_data['fregionid'] = I('fregionid');//行政区划
		$a_e_data['faddress'] = I('faddress');//地址
		$a_e_data['flinkman'] = I('flinkman');//联系人
		$a_e_data['ftel'] = I('ftel');//联系电话
		$a_e_data['fmail'] = I('fmail');//电子邮件
		$a_e_data['fstate'] = I('fstate',1);//状态，-1删除，0-无效，1-有效
		if($fid == 0){//判断是修改还是新增
			if(A('Admin/Authority','Model')->authority('200141') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
			}
			$is_repeat = M('tadowner')->where(array('fname'=>$a_e_data['fname']))->count();//查询广告主名称是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'广告主名称重复'));//广告主名称重复
			$a_e_data['fsource'] = '创建';//来源
			$a_e_data['fcreator'] = session('personInfo.fname');//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tadowner')->add($a_e_data);//添加数据
			
			if($rr > 0){//判断是否添加成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));//返回失败
			}
		}else{//修改
			if(A('Admin/Authority','Model')->authority('200142') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
			}
			$is_repeat = M('tadowner')->where(array('fid'=>array('neq',$fid),'fname'=>$a_e_data['fname']))->count();//查询广告主名称是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'广告主名称重复'));//广告主名称重复
			$a_e_data['fmodifier'] = session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tadowner')->where(array('fid'=>$fid))->save($a_e_data);//修改数据
			if($rr > 0){//判断是否修改成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));//返回失败
			}
		}
	}
			
	/*广告主详情*/
	public function ajax_adowner_details(){
		$fid = I('fid');//获取广告主ID
		$adownerDetails = M('tadowner')->where(array('fid'=>$fid))->find();//查询广告主详情
		$adownerDetails['region_name'] = M('tregion')->where(array('fid'=>$adownerDetails['fregionid']))->getField('ffullname');//查询行政区划的全称
		$this->ajaxReturn(array('code'=>0,'adownerDetails'=>$adownerDetails));
	}
}