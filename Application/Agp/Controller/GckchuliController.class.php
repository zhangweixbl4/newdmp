<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 国家局处理结果查看
 * by zw
 */

class GckchuliController extends BaseController{
  /**
  * 处理结果列表
  * by zw
  */
  public function cljieguo_list(){
    Check_QuanXian(['ckchuli']);
    session_write_close();
    ini_set('memory_limit','2048M');
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $illadTimeoutTips = $ALL_CONFIG['illadTimeoutTips'];//线索处理超时提示，0无，1有
    $againIssueTips = $ALL_CONFIG['againIssueTips'];//上月已做案件处罚后，本月还在继续播放的，是否需要进行提醒，0不需要，1需要
    $agpDataMerge = $ALL_CONFIG['agpDataMerge'];
    $cusHosts = json_decode($ALL_CONFIG['agpCustomerMarge'],true);

    $outtype = I('outtype');//导出类型
    $mediaownerid = I('mediaownerid');//媒体机构ID
    if(!empty($outtype) || !empty($mediaownerid)){
      $p  = I('page', 1);//当前第几页ks
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页ks
      $pp = 20;//每页显示多少记录
    }

    if($agpDataMerge == 1 || $agpDataMerge == 2){
      $cusHosts = array_intersect(S(session('regulatorpersonInfo.fcode').'checkCustomer'),$cusHosts);
      foreach ($cusHosts as $key => $value) {
        if($_SERVER['HTTP_HOST'] != $value && $system_num != $value){
          $joinArr[] = $this->returnListSql($value);
        }else{
          $joinArr[] = $this->returnListSql();
        }
      }
      $joinStr = '('.implode(') union all(', $joinArr).')';
    }else{
      $joinStr = $this->returnListSql($system_num);
    }

    $whereStr = $this->returnListWhere();
    
    //国家局系统只显示有打国家局标签媒体的数据
    if($system_num == '100000'){
      $where_tn = ' and tn.flevel in (1,2,3) and (a.fmedia_id in(select distinct(fmediaid) from tmedia_temp where fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid').') or a.fid in(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id))';
    }else{
      $joins = '(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id';//国家局不受授权查看限制
    }
  
    if(!empty($mediaownerid)){
      $whereStr .= ' and c.fmediaownerid = '.$mediaownerid;
    }

    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
      ->join('tregion tn on a.fregion_id=tn.fid'.$where_tn)
      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
      ->join($joins)
      ->where($whereStr)
      ->count();//查询满足条件的总记录数

    //安徽特定数据
    if($system_num == '340000'){
      $addfield = ',a.fprice,a.fnumber';
    }

    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name,(case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus,a.fstatus2,a.fstatus3,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fregion_id,a.fadowner as fadownername,tn.ffullname mediaregion,a.favifilename,a.fjpgfilename,a.fresult_datetime,a.fresult_unit,a.fresult_user,a.fsend_datetime,ifnull(a.fexaminetime,a.create_time) fsend_time,a.adowner_regionid,a.fsample_id,a.fcustomer'.$addfield)
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
      ->join('tregion tn on a.fregion_id=tn.fid'.$where_tn)
      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
      ->join($joins)
      ->where($whereStr)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();

    foreach ($do_tia as $key => $value) {
      if($agpDataMerge == 1 || $agpDataMerge == 2){
        $do_tia[$key]['customerName'] = A('Core')->returnSystemType($value['fcustomer'],$system_num);//所属系统类别
      }

      $do_tia[$key]['frece_reg'] = M('tbn_case_send')->cache(true,2)->where(['fillegal_ad_id'=>$value['fid']])->order('fstatus asc,fid desc')->getField('frece_reg');//处理机构

      $do_tia[$key]['adownerregion'] = M('tregion')->cache(true,86400)->where(['fid'=>$value['adowner_regionid']])->getField('ffullname');

      //判断案件未处理是否超过90天
      if(!empty($value['fsend_time']) && !empty($illadTimeoutTips) && empty($outtype) && empty($value['fstatus'])){
        if((time()-strtotime($value['fsend_time']))>90*86400){
          $do_tia[$key]['showTimeout'] = 1;
        }else{
          $do_tia[$key]['showTimeout'] = 0;
        }
      }else{
        $do_tia[$key]['showTimeout'] = 0;
      }

      //判断上个月有播放并处理了的，本月还在播放的违法广告
      if(!empty($againIssueTips) && empty($value['fstatus'])){
        $where_issue['a.fcustomer'] = $value['fcustomer'];
        $where_issue['a.fsample_id'] = $value['fsample_id'];
        $where_issue['a.fstatus'] = 10;
        $where_issue['b.fissue_date'] = ['between',[date('Y-m-01',strtotime(date('Y-m-01',strtotime($value['fstarttime'])) . ' -1 month')),date('Y-m-d',strtotime(date('Y-m-01',strtotime($value['fstarttime'])) . ' -1 day'))]];
        $againIssue = M('tbn_illegal_ad')
          ->alias('a')
          ->join('tbn_illegal_ad_issue b on a.fid = b.fillegal_ad_id')
          ->where($where_issue)
          ->count();
        if(!empty($againIssue)){
          $do_tia[$key]['againIssue'] = 1;
        }else{
          $do_tia[$key]['againIssue'] = 0;
        }
      }else{
        $do_tia[$key]['againIssue'] = 0;
      }
    }

    if(!empty($outtype)){
      if(empty($do_tia)){
        $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
      }

      $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-违法广告清单';//文档内部标题名称
      $outdata['datalie'] = [
        '序号'=>'key',
        '地域'=>'mediaregion',
        '发布媒体'=>'fmedianame',
        '媒体分类'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fmedia_class} == 1','电视'],
            ['{fmedia_class} == 2','广播'],
            ['{fmedia_class} == 3','报纸'],
            ['{fmedia_class} == 13','互联网'],
          ]
        ],
        '广告类别'=>'fadclass',
        '广告名称'=>'fad_name',
        '广告主'=>'fadownername',
        '播出总条次'=>'fcount',
        '播出周数'=>'diffweek',
        '播出时间段'=>[
          'type'=>'zwhebing',
          'data'=>'{fstarttime}~{fendtime}'
        ],
        '违法表现代码'=>'fillegal_code',
        '违法表现'=>'fexpressions',
        '涉嫌违法原因'=>'fillegal',
        '证据下载地址'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fmedia_class} == 1 || {fmedia_class} == 2','{favifilename}'],
            ['','{fjpgfilename}']
          ]
        ],
        '下发时间'=>'fsend_time',
        '派发时间'=>'fsend_datetime',
        '查看状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fview_status} == 0','未查看'],
            ['{fview_status} == 10','已查看'],
          ]
        ],
        '处理状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fstatus} == 0','未处理'],
            ['{fstatus} == 10','已上报'],
            ['{fstatus} == 30','数据复核'],
          ]
        ],
        '处理结果'=>'fresult',
        '处理时间'=>'fresult_datetime',
        '处理机构'=>'fresult_unit',
        '处理人'=>'fresult_user',
        '立案状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fstatus2} == 15','待处理'],
            ['{fstatus2} == 16','已处理'],
            ['{fstatus2} == 17','处理中'],
            ['{fstatus2} == 20','撤消'],
          ]
        ],
        '复核状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fstatus3} == 10','复核中'],
            ['{fstatus3} == 20','复核失败'],
            ['{fstatus3} == 30','复核通过待调整'],
            ['{fstatus3} == 40','复核通过已调整'],
            ['{fstatus3} == 50','复核有异议'],
          ]
        ],
      ];
      if($isexamine == 0){
        unset($outdata['datalie']['下发时间']);
      }
      if($system_num == '340000'){
        $outdata['datalie']['文书编号'] = 'fnumber';
        $outdata['datalie']['罚没金额'] = 'fprice';
      }
      $outdata['lists'] = $do_tia;
      $ret = A('Api/Function')->outdata_xls($outdata);

      D('Function')->write_log('违法广告清单',1,'导出成功');
      $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

    }else{
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
    }
  }

  /**
  * 案件继续处理
  * by zw
  */
  public function anjian_cxsb(){
    Check_QuanXian(['sbchuli','ckchuli']);
    $system_num = getconfig('system_num');
    $fid        = I('fid');//违法广告ID
    $attachinfo = I('attachinfo');//上传的文件信息
    $selfresult = I('selfresult');//处理结果
    $fnumber = I('fnumber');//文书编号
    $fprice = I('fprice');//罚没金额
    $attach     = [];//预添加的附件信息
    $attach2    = [];//预删除的附件信息

    if(!empty($selfresult)){
      $where_tia['fid']     = $fid;
      $where_tia['_string'] = 'fid in (select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').')';

      $data_tia['fstatus']          = 10;//已上报
      $data_tia['fview_status']     = 10;//已查看
      $data_tia['fresult']          = $selfresult;//处理结果
      $data_tia['fprice']           = $fprice;//罚没金额
      $data_tia['fnumber']          = $fnumber;//文书编号
      $data_tia['fresult_datetime'] = date('Y-m-d H:i:s');//上报时间

      //判断是否有立案调查，如有：添加立案调查记录
      if(strpos($selfresult,'立案')!==false){
        $data_tia['fstatus2']   = 17;//立案待处理
      }
      $do_tia = M('tbn_illegal_ad')->where($where_tia)->save($data_tia);
      if(!empty($do_tia)){
        //上传附件
        $attach_data['ftype']    = 0;//类型
        $attach_data['fillegal_ad_id']    = $fid;//违法广告
        $attach_data['fuploader']         = session('regulatorpersonInfo.fname');//上传人
        $attach_data['fupload_time']      = date('Y-m-d H:i:s');//上传时间
        foreach ($attachinfo as $key2 => $value2){
          $attach_data['fattach']     = $value2['fattach'];
          $attach_data['fattach_url'] = $value2['fattach_url'];
          if($value2['fattach_status'] == 2){
            array_push($attach,$attach_data);
          }elseif($value2['fattach_status'] == 0){
            array_push($attach2,$value2['fattach_url']);
          }
        }
      }
      if(!empty($attach)){
        M('tbn_illegal_ad_attach')->addAll($attach);
      }
      if(!empty($attach2)){
        $where_att['fattach_url'] = array('in',$attach2);
        M('tbn_illegal_ad_attach')->where($where_att)->delete();
      }
      D('Function')->write_log('违法广告清单',1,'继续处理成功','tbn_illegal_ad',$do_tia,M('tbn_illegal_ad')->getlastsql());
      $this->ajaxReturn(array('code'=>0,'msg'=>'处理成功'));
    }else{
      D('Function')->write_log('违法广告清单',0,'请选择处理结果','tbn_illegal_ad',0);
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择处理结果'));
    }
  }

  /**
  * 查看案件证据
  * by zw
  */
  public function fjjieguo(){
    $fid = I('fid');//违法广告ID
    $where_tia['a.fillegal_ad_id'] = $fid;
    $where_tia['a.fstate'] = 0;
    $where_tia['a.ftype'] = 0;

    //证据附件
    $do_tia = M('tbn_illegal_ad_attach')->field('a.fattach,a.fattach_url')->alias('a')->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id')->where($where_tia)->order('a.fid asc')->select();

    $where_tia['a.ftype'] = ['in',[10,20]];
    //立案附件
    $do_tia2 = M('tbn_illegal_ad_attach')->field('a.fattach,a.fattach_url,a.ftype')->alias('a')->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id')->where($where_tia)->order('a.fid asc')->select();

    $where_tia['a.ftype'] = 30;
    //督办附件
    $do_tia3 = M('tbn_illegal_ad_attach')->field('a.fattach,a.fattach_url,a.ftype')->alias('a')->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id')->where($where_tia)->order('a.fid asc')->select();

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','zjfjdata'=>$do_tia,'lafjdata'=>$do_tia2,'dbfjdata'=>$do_tia3));
  }

  /**
  * 撤消处理结果
  * by zw
  */
  public function cxjieguo(){
    Check_QuanXian(['sbchuli','ckchuli']);
    $selfid     = I('fid');//违法广告ID组
    
    $data_tia['fstatus']        = 0;
    $data_tia['fstatus2']       = 0;
    $data_tia['fstatus3']       = 0;
    $data_tia['fview_status']   = 0;
    $data_tia['fresult']        = '';
    $data_tia['flatime']        = null;
    $data_tia['fresult_datetime']  = null;
    $data_tia['fresult_unit']   = '';
    $data_tia['fresult_user']   = '';

    $where_tia['fstatus']   = ['in',[10,30]];
    foreach ($selfid as $key => $value) {
      $where_tia['fid'] = $value;
      $do_tia = M('tbn_illegal_ad')
        ->where($where_tia)
        ->save($data_tia);//更改状态
      if(!empty($do_tia)){
        M()->execute('update tbn_illegal_ad_attach set fstate=10 where fstate=0 and fillegal_ad_id='.$value);//上报文件失效
        M()->execute('update tbn_data_check set tcheck_status=20 where fillegal_ad_id='.$value);//复核记录退回
        M()->execute('update tbn_data_check_attach set fstate=10 where fstate=0 and fillegal_ad_id='.$value);//复核文件失效
        M()->execute('update tbn_case_send set fstatus=0 where fstatus<>0 and fillegal_ad_id='.$value.' and fid in(select fid from (select max(fid) fid from tbn_case_send where fstatus<>0 and fillegal_ad_id='.$value.' ) a)');//分派记录重置
      }
    }
    if(!empty($do_tia)){
      D('Function')->write_log('处理结果查看',1,'撤消完成','tbn_illegal_ad',$do_tia,M('tbn_illegal_ad')->getlastsql());
      $this->ajaxReturn(array('code'=>0,'msg'=>'撤消完成'));
    }else{
      D('Function')->write_log('处理结果查看',0,'撤消失败','tbn_illegal_ad',0,M('tbn_illegal_ad')->getlastsql());
      $this->ajaxReturn(array('code'=>1,'msg'=>'撤消失败'));
    }
  }

  /**
  * 提交督办材料
  * by zw
  */
  public function scjiandu(){
    $selfid     = I('selfid');//违法广告ID组
    $attachinfo = I('attachinfo');//上传的文件信息
    $islafile   = getconfig('islafile');
    $attach     = [];
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    if(empty($attachinfo) && !empty($islafile)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请上传文件'));
    }

    foreach ($selfid as $key => $value) {
      //上传附件
      $attach_data['ftype']  = 30;//类型
      $attach_data['fillegal_ad_id']  = $value;//违法广告
      $attach_data['fuploader']       = session('regulatorpersonInfo.fid');//上传人
      $attach_data['fupload_time']    = date('Y-m-d H:i:s');//上传时间
      foreach ($attachinfo as $key2 => $value2){
        $attach_data['fattach']     = $value2['fattachname'];
        $attach_data['fattach_url'] = $value2['fattachurl'];
        array_push($attach,$attach_data);
      }
    }
    if(!empty($attach)){
      M('tbn_illegal_ad_attach')->addAll($attach);
    }
    D('Function')->write_log('执法监督',1,'操作成功','tbn_illegal_ad',0);
    $this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
  }

  /**
  * 获取违法广告监控情况
   * by zw
   */
  public function illCondition(){
    session_write_close();
    $showType = I('showType')?I('showType'):0;//展现方式，0汇总，1列表
    $dataType = I('dataType')?I('dataType'):[];//需要获取的数据，数组，1重复播放的，2已停播的，3新增的，空为所有
    $outtype = I('outtype');//导出类型

    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $isrelease = $ALL_CONFIG['isrelease'];
    $ischeck = $ALL_CONFIG['ischeck'];
    $isexamine = $ALL_CONFIG['isexamine'];

    // $nowdata = '2019-06-01';
    $nowdata = date('Y-m-01');

    $where_tia['_string'] = '1=1';
    
    //国家局系统只显示有打国家局标签媒体的数据
    if($system_num == '100000'){
      $wherestr = ' and tn.flevel in (1,2,3)';
    }
    $where_tia['a.fstatus3'] = array('not in',array(30,40));
    $where_tia['a.fcustomer']  = $system_num;
    
    //是否抽查模式
    if(!empty($ischeck)){
        $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
        //如果抽查表有数据
        if(!empty($spot_check_data)){
            $dates = [];//定义日期数组
            foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                $year_month = '';
                $date_str = [];
                $year_month = substr($spot_check_data_val['fmonth'],0,7);
                if(!empty($spot_check_data_val['condition'])){
                  $date_str = explode(',',$spot_check_data_val['condition']);
                }
                foreach ($date_str as $date_str_val){
                    $dates[] = $year_month.'-'.$date_str_val;
                }
            }
        $where_time  .= ' and tbn_illegal_ad_issue.fissue_date in ("'.implode('","', $dates).'")';
        }else{
            $where_time  .= ' and 1=0';
        }
    }

    if(in_array($isexamine, [10,20,30,40,50])){
      $where_tia['a.fexamine'] = 10;
    }

    //本月的数据
    $where_time2 = gettimecondition(date('Y',strtotime($nowdata)),30,date('m',strtotime($nowdata)),'fissue_date',-1,$isrelease);
    $samples1 = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
      ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
      ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.$where_time2.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
      ->where($where_tia)
      ->getField('fsample_id',true);

    //上月的数据
    $where_time3 = gettimecondition(date('Y',strtotime($nowdata. ' -1 month')),30,date('m',strtotime($nowdata. ' -1 month')),'fissue_date',-1,$isrelease);
    $samples2 = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
      ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
      ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.$where_time3.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
      ->where($where_tia)
      ->group('a.fsample_id')
      ->getField('fsample_id',true);

    $noPlay = [];//已停播
    $newPlay = [];//新播出
    $cfIll = array_intersect($samples1,$samples2);//重复播放的样本
    foreach ($samples1 as $value) {
      if(!in_array($samples2,$value)){
        $newPlay[] = $value;
      }
    }

    foreach ($samples2 as $value) {
      if(!in_array($samples1,$value)){
        $noPlay[] = $value;
      }
    }

    if(empty($showType)){
      if(empty($dataType) || in_array(1,$dataType)){
        if(!empty($cfIll)){
          //重复播放的违法广告量
          $where_tia['fsample_id'] = ['in',$cfIll];
          $cfIllCount = M('tbn_illegal_ad')
            ->alias('a')
            ->join('tadclass b on a.fad_class_code=b.fcode')
            ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
            ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
            ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.$where_time2.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
            ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
            ->where($where_tia)
            ->count();
          $data['cfIllCount'] = $cfIllCount?$cfIllCount:0;
        }else{
          $data['cfIllCount'] = 0;
        }
      }

      if(empty($dataType) || in_array(2,$dataType)){
        if(!empty($noPlay)){
          //已停播的违法广告量
          $where_tia['fsample_id'] = ['in',$noPlay];
          $noPlayCount = M('tbn_illegal_ad')
            ->alias('a')
            ->join('tadclass b on a.fad_class_code=b.fcode')
            ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
            ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
            ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.$where_time3.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
            ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
            ->where($where_tia)
            ->count();
          $data['noPlayCount'] = $noPlayCount?$noPlayCount:0;
        }else{
          $data['noPlayCount'] = 0;
        }
      }
      
      if(empty($dataType) || in_array(3,$dataType)){
        if(!empty($newPlay)){
          //新播的违法广告量
          $where_tia['fsample_id'] = ['in',$newPlay];
          $newPlayCount = M('tbn_illegal_ad')
            ->alias('a')
            ->join('tadclass b on a.fad_class_code=b.fcode')
            ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
            ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
            ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.$where_time2.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
            ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
            ->where($where_tia)
            ->count();
          $data['newPlayCount'] = $newPlayCount?$newPlayCount:0;
        }else{
          $data['newPlayCount'] = 0;
        }
      }
    }else{
      $fields = 'a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name,(case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus2,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fstatus,a.fregion_id';
      if(empty($dataType) || in_array(1,$dataType)){
        if(!empty($cfIll)){
          //重复播放的违法广告量
          $where_tia['fsample_id'] = ['in',$cfIll];
          $cfIllData = M('tbn_illegal_ad')
            ->alias('a')
            ->field($fields)
            ->join('tadclass b on a.fad_class_code=b.fcode')
            ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
            ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
            ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.$where_time2.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
            ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
            ->where($where_tia)
            ->select();
          $data['cfIllData'] = $cfIllData?$cfIllData:[];
        }else{
          $data['cfIllData'] = [];
        }
        $waitData = $data['cfIllData'];
      }

      if(empty($dataType) || in_array(2,$dataType)){
        if(!empty($noPlay)){
          //已停播的违法广告量
          $where_tia['fsample_id'] = ['in',$noPlay];
          $noPlayData = M('tbn_illegal_ad')
            ->alias('a')
            ->field($fields)
            ->join('tadclass b on a.fad_class_code=b.fcode')
            ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
            ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
            ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.$where_time3.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
            ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
            ->where($where_tia)
            ->select();
          $data['noPlayData'] = $noPlayData?$noPlayData:[];
        }else{
          $data['noPlayData'] = [];
        }
        $waitData = $data['noPlayData'];
      }
      
      if(empty($dataType) || in_array(3,$dataType)){
        if(!empty($newPlay)){
          //新播的违法广告量
          $where_tia['fsample_id'] = ['in',$newPlay];
          $newPlayData = M('tbn_illegal_ad')
            ->alias('a')
            ->field($field)
            ->join('tadclass b on a.fad_class_code=b.fcode')
            ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
            ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
            ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.$where_time2.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
            ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
            ->where($where_tia)
            ->select();
          $data['newPlayData'] = $newPlayData?$newPlayData:[];
        }else{
          $data['newPlayData'] = [];
        }
        $waitData = $data['newPlayData'];
      }
      
    }
    
    if(!empty($outtype)){
      if(empty($waitData)){
        $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
      }

      $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-数据汇总违法条数列表';//文档内部标题名称
      $outdata['datalie'] = [
        '序号'=>'key',
        '媒体分类'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fmedia_class} == 1','电视'],
            ['{fmedia_class} == 2','广播'],
            ['{fmedia_class} == 3','报纸'],
            ['{fmedia_class} == 13','互联网'],
          ]
        ],
        '广告类别'=>'fadclass',
        '广告名称'=>'fad_name',
        '发布媒体'=>'fmedianame',
        '播出总条次'=>'fcount',
        '播出周数'=>'diffweek',
        '播出时间段'=>[
          'type'=>'zwhebing',
          'data'=>'{fstarttime}~{fendtime}'
        ],
        '违法表现代码'=>'fillegal_code',
        '违法表现'=>'fexpressions',
        '涉嫌违法原因'=>'fillegal',
      ];
      $outdata['lists'] = $waitData;
      $ret = A('Api/Function')->outdata_xls($outdata);

      D('Function')->write_log('数据汇总违法条数',1,'导出成功');
      $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
    }else{
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
    }
  }

  /**
   * 获取案件线索分析数据
   * by zw
   */
  public function allIllAnalysis(){
    session_write_close();
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $isrelease = $ALL_CONFIG['isrelease'];
    $ischeck = $ALL_CONFIG['ischeck'];
    $illDistinguishPlatfo = $ALL_CONFIG['illDistinguishPlatfo'];//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode($ALL_CONFIG['media_class'],true);//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

    $illadTypes[] = '平台监测';
    $illadTypes2 = $ALL_CONFIG['illadTypes']?json_decode($ALL_CONFIG['illadTypes'],true):[];//重点案件线索类型（例：投诉举报，上级交办，部门移交，其他）
    $illadTypes = array_merge($illadTypes, $illadTypes2);

    //时间条件筛选
    $issueDate    = I('issueDate');//选择时间
    if(!empty($issueDate)){
      $where_time = gettimecondition(date('Y-m-d',strtotime($issueDate[0])),0,implode(',', $issueDate),'fissue_date',-1,$isrelease);
    }else{
      $where_time = '1=1';
    }
    $where_tia['_string'] = '1=1';
    
    //国家局系统只显示有打国家局标签媒体的数据
    if($system_num == '100000'){
      $wherestr = ' and tn.flevel in (1,2,3)';
    }
    $where_tia['a.fstatus3'] = array('not in',array(30,40));
    $where_tia['a.fcustomer']  = $system_num;
    
    //是否抽查模式
    if(!empty($ischeck)){
        $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
        //如果抽查表有数据
        if(!empty($spot_check_data)){
            $dates = [];//定义日期数组
            foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                $year_month = '';
                $date_str = [];
                $year_month = substr($spot_check_data_val['fmonth'],0,7);
                if(!empty($spot_check_data_val['condition'])){
                  $date_str = explode(',',$spot_check_data_val['condition']);
                }
                foreach ($date_str as $date_str_val){
                    $dates[] = $year_month.'-'.$date_str_val;
                }
            }
        $where_time  .= ' and tbn_illegal_ad_issue.fissue_date in ("'.implode('","', $dates).'")';
        }else{
            $where_time  .= ' and 1=0';
        }
    }

    $isexamine = $ALL_CONFIG['isexamine'];
    if(in_array($isexamine, [10,20,30,40,50])){
      $where_tia['a.fexamine'] = 10;
    }
    if(!empty($illDistinguishPlatfo)){
      $where_tia['left(c.fmediaclassid,2)'] = ['in',$media_class];
    }

    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fregion_id,tn.fname regionname,fview_status,fstatus,fresult')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
      ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
      ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
      ->where($where_tia)
      ->order('a.fregion_id asc')
      ->select();

    $hejidata['diquzongCount'] = 0;
    foreach ($illadTypes as $illadType) {
      $hejidata['xiansuodata'][$illadType]['fenleizongCount'] = 0;//线索量
      $hejidata['xiansuodata'][$illadType]['chakanCount'] = 0;//查看量
      $hejidata['xiansuodata'][$illadType]['chuliCount'] = 0;//处理量
      $hejidata['xiansuodata'][$illadType]['zhetingCount'] = 0;//责停量
      $hejidata['xiansuodata'][$illadType]['lianCount'] = 0;//立案量
      $hejidata['xiansuodata'][$illadType]['shifaCount'] = 0;//移交司法量
      $hejidata['xiansuodata'][$illadType]['qitaCount'] = 0;//其他量
    }

    //平台监测
    foreach ($do_tia as $key => $value) {
      if(empty($data[$value['regionname']])){
        $data[$value['regionname']]['diquzongCount'] = 0;//地区总线索量
        foreach ($illadTypes as $illadType) {
          $data[$value['regionname']]['xiansuodata'][$illadType]['fenleizongCount'] = 0;//线索量
          $data[$value['regionname']]['xiansuodata'][$illadType]['chakanCount'] = 0;//查看量
          $data[$value['regionname']]['xiansuodata'][$illadType]['chuliCount'] = 0;//处理量
          $data[$value['regionname']]['xiansuodata'][$illadType]['zhetingCount'] = 0;//责停量
          $data[$value['regionname']]['xiansuodata'][$illadType]['lianCount'] = 0;//立案量
          $data[$value['regionname']]['xiansuodata'][$illadType]['shifaCount'] = 0;//移交司法量
          $data[$value['regionname']]['xiansuodata'][$illadType]['qitaCount'] = 0;//其他量
        }
      }

      //合计数据
      $hejidata['diquzongCount'] += 1;
      $hejidata['xiansuodata'][$illadTypes[0]]['fenleizongCount'] += 1;

      $data[$value['regionname']]['diquzongCount'] += 1;
      $data[$value['regionname']]['xiansuodata'][$illadTypes[0]]['fenleizongCount'] += 1;
      if(!empty($value['fview_status'])){
        $data[$value['regionname']]['xiansuodata'][$illadTypes[0]]['chakanCount'] += 1;
        $hejidata['xiansuodata'][$illadTypes[0]]['chakanCount'] += 1;
      }
      if(!empty($value['fstatus'])){
        $data[$value['regionname']]['xiansuodata'][$illadTypes[0]]['chuliCount'] += 1;
        $hejidata['xiansuodata'][$illadTypes[0]]['chuliCount'] += 1;
      }
      if(strpos($value['fresult'],'责令停止发布') !== false){ 
        $data[$value['regionname']]['xiansuodata'][$illadTypes[0]]['zhetingCount'] += 1;
        $hejidata['xiansuodata'][$illadTypes[0]]['zhetingCount'] += 1;
      }
      if(strpos($value['fresult'],'立案') !== false){ 
        $data[$value['regionname']]['xiansuodata'][$illadTypes[0]]['lianCount'] += 1;
        $hejidata['xiansuodata'][$illadTypes[0]]['lianCount'] += 1;
      }
      if(strpos($value['fresult'],'移送司法') !== false){ 
        $data[$value['regionname']]['xiansuodata'][$illadTypes[0]]['shifaCount'] += 1;
        $hejidata['xiansuodata'][$illadTypes[0]]['shifaCount'] += 1;
      }
      if(strpos($value['fresult'],'其他') !== false){ 
        $data[$value['regionname']]['xiansuodata'][$illadTypes[0]]['qitaCount'] += 1;
        $hejidata['xiansuodata'][$illadTypes[0]]['qitaCount'] += 1;
      }
    }

    if(!empty($issueDate)){

    }
    $where_tizd['fcustomer'] = $system_num;
    if(!empty($issueDate)){
      $where_tizd['a.fsend_time'] = ['between',$issueDate];
    }
    $do_tizd = M('tbn_illegal_zdad')
      ->alias('a')
      ->field('a.fregion_id,tn.fname regionname,fview_status,fstatus,fresult,fill_type')
      ->join('tregion tn on a.fregion_id=tn.fid')
      ->where($where_tizd)
      ->order('a.fregion_id asc')
      ->select();
    foreach ($do_tizd as $key => $value) {
      if(empty($data[$value['regionname']])){
        $data[$value['regionname']]['diquzongCount'] = 0;
        foreach ($illadTypes as $illadType) {
          $data[$value['regionname']]['xiansuodata'][$illadType]['fenleizongCount'] = 0;
          $data[$value['regionname']]['xiansuodata'][$illadType]['chakanCount'] = 0;
          $data[$value['regionname']]['xiansuodata'][$illadType]['chuliCount'] = 0;
          $data[$value['regionname']]['xiansuodata'][$illadType]['zhetingCount'] = 0;
          $data[$value['regionname']]['xiansuodata'][$illadType]['lianCount'] = 0;
          $data[$value['regionname']]['xiansuodata'][$illadType]['shifaCount'] = 0;
          $data[$value['regionname']]['xiansuodata'][$illadType]['qitaCount'] = 0;
        }
      }

      //合计数据
      $hejidata['diquzongCount'] += 1;
      $hejidata['xiansuodata'][$value['fill_type']]['fenleizongCount'] += 1;

      $data[$value['regionname']]['diquzongCount'] += 1;
      $data[$value['regionname']]['xiansuodata'][$value['fill_type']]['fenleizongCount'] += 1;
      if(!empty($value['fview_status'])){
        $data[$value['regionname']]['xiansuodata'][$value['fill_type']]['chakanCount'] += 1;
        $hejidata['xiansuodata'][$value['fill_type']]['chakanCount'] += 1;
      }
      if($value['fstatus'] == 30){
        $data[$value['regionname']]['xiansuodata'][$value['fill_type']]['chuliCount'] += 1;
        $hejidata['xiansuodata'][$value['fill_type']]['chuliCount'] += 1;
      }
      if(strpos($value['fresult'],'责令停止发布') !== false){ 
        $data[$value['regionname']]['xiansuodata'][$value['fill_type']]['zhetingCount'] += 1;
        $hejidata['xiansuodata'][$value['fill_type']]['zhetingCount'] += 1;
      }
      if(strpos($value['fresult'],'立案') !== false){ 
        $data[$value['regionname']]['xiansuodata'][$value['fill_type']]['lianCount'] += 1;
        $hejidata['xiansuodata'][$value['fill_type']]['lianCount'] += 1;
      }
      if(strpos($value['fresult'],'移送司法') !== false){ 
        $data[$value['regionname']]['xiansuodata'][$value['fill_type']]['shifaCount'] += 1;
        $hejidata['xiansuodata'][$value['fill_type']]['shifaCount'] += 1;
      }
      if(strpos($value['fresult'],'其他') !== false){ 
        $data[$value['regionname']]['xiansuodata'][$value['fill_type']]['qitaCount'] += 1;
        $hejidata['xiansuodata'][$value['fill_type']]['qitaCount'] += 1;
      }
    }

    foreach ($hejidata['xiansuodata'] as $key => $value) {
      $hejidata['xiansuodata'][$key]['chakanlv'] = ($value['chakanCount']==0 || $value['fenleizongCount'] == 0)?'0.00':round($value['chakanCount']/$value['fenleizongCount']*100,2).'%';
      $hejidata['xiansuodata'][$key]['chulilv'] = ($value['chuliCount']==0 || $value['fenleizongCount'] == 0)?'0.00':round($value['chuliCount']/$value['fenleizongCount']*100,2).'%';
    }

    foreach ($data as $key => $value) {
      foreach ($value['xiansuodata'] as $key2 => $value2) {
        $data[$key]['xiansuodata'][$key2]['chakanlv'] = ($value2['chakanCount']==0 || $value2['fenleizongCount'] == 0)?'0.00%':round($value2['chakanCount']/$value2['fenleizongCount']*100,2).'%';
        $data[$key]['xiansuodata'][$key2]['chulilv'] = ($value2['chuliCount']==0 || $value2['fenleizongCount'] == 0)?'0.00%':round($value2['chuliCount']/$value2['fenleizongCount']*100,2).'%';
      }
    }

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data,'hejidata'=>$hejidata));
  }

  /**
  * 生成unionsql
  * by zw
  */
  private function returnListSql($system_num = ''){
    $iscontain = I('iscontain');//是否包含下属地区
    $area = I('area');//所属地区
    $netadtype  = I('netadtype');//平台类别
    $is_show_fsend    = I('is_show_fsend');//是否包含确认广告，1包含，2不包含，0未定义
    $fstate = I('fstate');
    $fstate2 = I('fstate2');
    $fresultdatetime = I('fresultdatetime');//处理时间
    $fsenddatetime = I('fsenddatetime');//派发时间

    $ALL_CONFIG = getconfig('ALL',$system_num);
    $system_num = $ALL_CONFIG['system_num'];
    $illadTimeoutTips = $ALL_CONFIG['illadTimeoutTips'];//线索处理超时提示，0无，1有
    $againIssueTips = $ALL_CONFIG['againIssueTips'];//上月已做案件处罚后，本月还在继续播放的，是否需要进行提醒，0不需要，1需要
    $illSupervise = $ALL_CONFIG['illSupervise'];//是否需要案件处理的执法监督功能，0不需要，1需要
    $ischeck = $ALL_CONFIG['ischeck'];
    $illDistinguishPlatfo = $ALL_CONFIG['illDistinguishPlatfo'];//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode($ALL_CONFIG['media_class'],true);//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）
    $isexamine = $ALL_CONFIG['isexamine'];
    $isrelease = $ALL_CONFIG['isrelease'];

    //媒体类别格式化
    foreach ($media_class as $value) {
      $mClasses[] = (int)$value;
    }

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    if($is_show_fsend==1){
      $is_show_fsend = -2;
    }else{
      $is_show_fsend = 0;
    }
    $where_time = gettimecondition($years,$timetypes,$timeval,'b.fissue_date',$is_show_fsend,$isrelease);

    //是否抽查模式
    if(!empty($ischeck)){
        $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
        //如果抽查表有数据
        if(!empty($spot_check_data)){
            $dates = [];//定义日期数组
            foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                $year_month = '';
                $date_str = [];
                $year_month = substr($spot_check_data_val['fmonth'],0,7);
                if(!empty($spot_check_data_val['condition'])){
                  $date_str = explode(',',$spot_check_data_val['condition']);
                }
                foreach ($date_str as $date_str_val){
                    $dates[] = $year_month.'-'.$date_str_val;
                }
            }
          $where_time   .= ' and b.fissue_date in ("'.implode('","', $dates).'")';
        }else{
          $where_time   .= ' and 1=0';
        }
    }

    if(!empty($netadtype)){
      $where_time .= ' and a.fplatform = '.$netadtype;
    }

    if($fstate != -1){//违法广告处理状态
      if($fstate == 0){
        $where_time .= ' and a.fstatus = 0' ;
        if($fstate2 == 10){
          $where_time .= ' and a.fview_status = 0' ;
        }elseif($fstate2 == 20){
          $where_time .= ' and a.fview_status = 10' ;
        }
      }elseif($fstate == 10){
        $where_time .= ' and a.fstatus = 10' ;
        if($fstate2 == 10){
          $where_time .= ' and a.fresult like "%责令停止发布%"' ;
        }elseif($fstate2 == 20){
          $where_time .= ' and a.fresult like "%立案%"' ;
        }elseif($fstate2 == 30){
          $where_time .= ' and a.fresult like "%立案%" and a.fstatus2 = 17' ;
        }elseif($fstate2 == 40){
          $where_time .= ' and a.fresult like "%立案%" and a.fstatus2 = 16' ;
        }elseif($fstate2 == 50){
          $where_time .= ' and a.fresult like "%移送司法%"' ;
        }elseif($fstate2 == 60){
          $where_time .= ' and a.fresult like "%其他%"' ;
        }
      }elseif($fstate == 30){
        $where_time .= ' and a.fstatus = 30' ;
      }else{
        $where_time .= ' and a.fstatus not in(30,40)';
      }
    }else{
      $where_time .= ' and a.fstatus not in(30,40)';
    }

    if(!empty($area)){//所属地区
      if($area != '100000'){
        if(!empty($iscontain)){
          $tregion_len = get_tregionlevel($area);
          if($tregion_len == 1 || $tregion_len == 6){//国家级
            $where_time .= ' and a.fregion_id = '.$area;
          }elseif($tregion_len == 2){//省级
            $where_time .= ' and a.fregion_id like "'.substr($area,0,2).'%"';
          }elseif($tregion_len == 4){//市级
            $where_time .= ' and a.fregion_id like "'.substr($area,0,4).'%"';
          }
        }else{
          $where_time .= ' and a.fregion_id = '.$area;
        }
      }
    }

    if(in_array($isexamine, [10,20,30,40,50])){
      $where_time .= ' and a.fexamine = 10';
    }
    if(!empty($illDistinguishPlatfo)){
      $where_time .= ' and a.fmedia_class in('.implode(',', $mClasses).')';
    }

    if(!empty($fresultdatetime)){
      $where_time .= ' and a.fresult_datetime between "'.$fresultdatetime[0].'" and "'.date('Y-m-d',strtotime($fresultdatetime[1])).' 23:59:59'.'"';
    }
    if(!empty($fsenddatetime)){
      $where_time .= ' and a.fsend_datetime between "'.$fsenddatetime[0].'" and "'.date('Y-m-d',strtotime($fsenddatetime[1])).' 23:59:59'.'"';
    }

    return 'select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue b,tbn_illegal_ad a where '.$where_time.' and a.fid = b.fillegal_ad_id and a.fcustomer = "'.$system_num.'" group by fillegal_ad_id';
  }

  /**
  * 生成where条件
  * by zw
  */
  private function returnListWhere(){
    $whereStr = '1=1';
    $search_type  = I('search_type');//搜索类别
    $search_val   = I('search_val');//搜索值
    $adclass   = I('adclass');//广告类别名
    $mclass   = I('mclass');//媒体类别

    if(!empty($search_val)){
      if($search_type == 10){//媒体分类
        $whereStr .= ' and a.fmedia_class = '.$search_val;
      }elseif($search_type == 20){//广告类别
        $whereStr .= ' and b.ffullname like "%'.$search_val.'%"';
      }elseif($search_type == 30){//广告名称
        $whereStr .= ' and a.fad_name like "%'.$search_val.'%"';
      }elseif($search_type == 40){//发布媒体
        $whereStr .= ' and c.fmedianame like "%'.$search_val.'%"';
      }elseif($search_type == 50){//违法原因
        $whereStr .= ' and a.fillegal like "%'.$search_val.'%"';
      }elseif($search_type == 60){//广告主
        $whereStr .= ' and tor.fname like "%'.$search_val.'%"';
      }
    }

    if(!empty($adclass)){
      $whereStr .= ' and b.ffullname like "%'.$adclass.'%"';
    }

    if(!empty($mclass)){
      $whereStr .= ' and a.fmedia_class = "'.$mclass.'"';
    }

    return $whereStr;
  }
}