<?php
namespace Agp\Controller;
use Think\Controller;
use Think\Exception;
use OpenSearch\Client\OpenSearchClient;
use OpenSearch\Client\DocumentClient;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;

class IndexController extends BaseController
{
    public function ghome() {
    	session_write_close();
    	Check_QuanXian(['home']);
    	$ALL_CONFIG = getconfig('ALL');
    	$system_num = $ALL_CONFIG['system_num'];
    	$ischeck = $ALL_CONFIG['ischeck'];

		//各媒体数量
		$data['tvmediacount']	= 0;
		$data['bcmediacount']	= 0;
		$data['papermediacount']= 0;
		$data['netmediacount']	= 0;
		$data['outmediacount']	= 0;

		$do_ta = M('tmedia')
			->master(true)
			->alias('a')
			->field('fmediaclassid')
			->join('tmediaowner d on d.fid=a.fmediaownerid ')
			->join('tmedia_temp ttp on a.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fstate = 1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->join('tregion c on a.media_region_id=c.fid')
			->where('a.fid=a.main_media_id and a.fstate = 1')
			->select();
		foreach ($do_ta as $key => $value) {
			$mediatp = substr($value['fmediaclassid'],0,2);
			if($mediatp=='01'){
				$data['tvmediacount'] += 1;
			}elseif($mediatp=='02'){
				$data['bcmediacount'] += 1;
			}elseif($mediatp=='03'){
				$data['papermediacount'] += 1;
			}elseif($mediatp=='13'){
				$data['netmediacount'] += 1;
			}elseif($mediatp=='05'){
				$data['outmediacount'] += 1;
			}
		}

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
    }

	/**
	* 获取首页的统计数据
	 * @return array|string  $data 数据
	 * by zw
	 */
	public function getindexdata(){
		session_write_close();
		Check_QuanXian('home');
		$ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $use_open_search = $ALL_CONFIG['use_open_search'];
        $isillegal_summary = $ALL_CONFIG['isillegal_summary'];
        $ischeck = $ALL_CONFIG['ischeck'];
        $media_class = json_decode($ALL_CONFIG['media_class'],true);//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

    	$homeType = I('homeType');//首页类型，0标准（数据以广告明细为主），1（数据以线索为主）

		$where_ta['fmediaownerid'] 	= array('neq',0);
		$where_ta['tmedia.fstate'] 		= 1;
		$where_ta['_string'] 		= 'tmedia.fid=tmedia.main_media_id';
		$do_ta = M('tmedia')
			->master(true)
			->field('fmediaclassid')
			->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fstate = 1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->where($where_ta)
			->count();

		$data['mediacount'] 	= $do_ta;//媒体数量

		$daytime = date('Y-m-d');

		if(empty($homeType)){
			if(!empty($use_open_search)){
				$this->getindexdata2();//opensearch使用2方法统计
			}
			$tb_tvissue_nowmonth = gettable('tv',strtotime($daytime),session('regulatorpersonInfo.regionid'));//电视本月
	        $tb_tvissue_nevmonth = gettable('tv',strtotime("$daytime -1 month"),session('regulatorpersonInfo.regionid'));//电视上月
	        $tb_bcissue_nowmonth = gettable('bc',strtotime($daytime),session('regulatorpersonInfo.regionid'));//广播本月
	        $tb_bcissue_nevmonth = gettable('bc',strtotime("$daytime -1 month"),session('regulatorpersonInfo.regionid'));//广播上月

	        $till_tvcount = 0;//电视总违法
	        $till_bccount = 0;//广播总违法
	        $till_papercount = 0;//报纸总违法
	        $till_netcount = 0;//互联网总违法
	        $data_arr = [];//七天数据集

	        //是否抽查模式
			if(!empty($ischeck)){
			    $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
			    //如果抽查表有数据
			    if(!empty($spot_check_data)){
			        $dates = [];//定义日期数组
			        foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
			            $year_month = '';
			            $date_str = [];
			            $year_month = substr($spot_check_data_val['fmonth'],0,7);
			            if(!empty($spot_check_data_val['condition'])){
			            	$date_str = explode(',',$spot_check_data_val['condition']);
			            }
			            foreach ($date_str as $date_str_val){
			                $dates[] = $year_month.'-'.$date_str_val;
			            }
			        }
			        $wherestrtv 	.= ' and from_unixtime(issue.fissuedate,"%Y-%m-%d") in ("'.implode('","', $dates).'")';
					$wherestrpaper 	.= ' and issue.fissuedate in ("'.implode('","', $dates).'")';
					$wherestrbc 	.= ' and from_unixtime(issue.fissuedate,"%Y-%m-%d") in ("'.implode('","', $dates).'")';
					$wherestrnet 	.= ' and from_unixtime(issue.net_created_date/1000,"%Y-%m-%d") in ("'.implode('","', $dates).'")';
			    }else{
			        $wherestrtv 	.= ' and 1=0';
					$wherestrpaper 	.= ' and 1=0';
					$wherestrbc 	.= ' and 1=0';
					$wherestrnet 	.= ' and 1=0';
			    }
			}

	        //电视近7天违法量和总量
	        $tb_tvsql = "select from_unixtime(issue.fissuedate,'%Y-%m-%d') as fissuedate,count(*) as tecount,sum(case when fillegaltypecode>0 then 1 else 0 end) as tillcount
	        	from (
	        	(select ttp.fmediaid,fsampleid,fissuedate from ".$tb_tvissue_nowmonth." issue,tmedia_temp ttp where fissuedate between UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 7 DAY)) and  UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 1 DAY)) ".$wherestrtv." and issue.fmediaid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = '".$system_num."' and ttp.fuserid=".session('regulatorpersonInfo.fid')." )
	        	union all (select ttp.fmediaid,fsampleid,fissuedate from ".$tb_tvissue_nevmonth." issue,tmedia_temp ttp where fissuedate between UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 7 DAY)) and  UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 1 DAY)) ".$wherestrtv." and issue.fmediaid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer='".$system_num."' and ttp.fuserid=".session('regulatorpersonInfo.fid')." )
	        ) as issue
	        	inner join tmedia on issue.fmediaid=tmedia.fid and tmedia.main_media_id=tmedia.fid
	        	inner join ttvsample as sample on sample.fid = issue.fsampleid  and sample.fstate = 1
	        	inner join tad on sample.fadid=tad.fadid and tad.fadid<>0 and left(tad.fadclasscode,2) <> '23'
	        	group by issue.fissuedate
	         ";
         	try{
	        	$tvdata = M()->query($tb_tvsql);
            }catch (Exception $e){
         		$tvdata = [];
            }
	        foreach ($tvdata as $key => $value) {
	        	if(!empty($value)){
	        		$data['tetvcount'] += $value['tecount'];
	        	}
	        }

	        $data_arr[] = $tvdata;
	        //广播近7天违法量和总量
	        $tb_bcsql = "select from_unixtime(issue.fissuedate,'%Y-%m-%d') as fissuedate,count(*) as tecount,sum(case when fillegaltypecode>0 then 1 else 0 end) as tillcount
	        	from (
	        	(select ttp.fmediaid,fsampleid,fissuedate from ".$tb_bcissue_nowmonth." issue,tmedia_temp ttp where fissuedate between UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 7 DAY)) and  UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 1 DAY)) ".$wherestrbc." and issue.fmediaid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = '".$system_num."' and ttp.fuserid=".session('regulatorpersonInfo.fid')." )
	        	union all (select ttp.fmediaid,fsampleid,fissuedate from ".$tb_bcissue_nevmonth." issue,tmedia_temp ttp where fissuedate between UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 7 DAY)) and  UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 1 DAY)) ".$wherestrbc." and issue.fmediaid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = '".$system_num."' and ttp.fuserid=".session('regulatorpersonInfo.fid')." )
	        ) as issue
	        	inner join tmedia on issue.fmediaid=tmedia.fid and tmedia.main_media_id=tmedia.fid
	        	inner join tbcsample as sample on sample.fid = issue.fsampleid and sample.fstate = 1
	        	inner join tad on sample.fadid=tad.fadid and tad.fadid<>0 and left(tad.fadclasscode,2) <> '23'
	        	group by issue.fissuedate
	         ";
	        try{
	        	$bcdata = M()->query($tb_bcsql);
            }catch (Exception $e){
         		$bcdata = [];
            }
	        foreach ($bcdata as $key => $value) {
	        	if(!empty($value)){
	        		$data['tebccount'] += $value['tecount'];
	        	}
	        }
	        $data_arr[] = $bcdata;
	        //报纸近7天违法量和总量
	        $tb_papersql = "select DATE_FORMAT(issue.fissuedate,'%Y-%m-%d') as fissuedate,count(*) as tecount,sum(case when fillegaltypecode>0 then 1 else 0 end) as tillcount
	        	from tpaperissue as issue
	        	inner join tmedia on issue.fmediaid=tmedia.fid and tmedia.main_media_id=tmedia.fid
	        	inner join tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = '".$system_num."' and ttp.fuserid=".session('regulatorpersonInfo.fid')."
	        	inner join tpapersample as sample on sample.fpapersampleid = issue.fpapersampleid  and sample.fstate = 1
	        	inner join tad on sample.fadid=tad.fadid and tad.fadid<>0 and left(tad.fadclasscode,2) <> '23'
	        	where  issue.fissuedate between DATE_SUB(CURDATE(), INTERVAL 7 DAY) and DATE_SUB(CURDATE(), INTERVAL 1 DAY) ".$wherestrpaper."
	        	group by issue.fissuedate
	         ";
	        try{
	        	$paperdata = M()->query($tb_papersql);
            }catch (Exception $e){
         		$paperdata = [];
            }
	        foreach ($paperdata as $key => $value) {
	        	if(!empty($value)){
	        		$data['tepapercount'] += $value['tecount'];
	        	}
	        }
	        $data_arr[] = $paperdata;
	        if(in_array('13', $media_class)){
	        	//互联网近7天违法量和总量
		        $tb_netsql = "select from_unixtime((issue.fissuedate),'%Y-%m-%d') as fissuedate,count(*) as tecount,sum(case when fillegaltypecode>0 then 1 else 0 end) as tillcount
		        	from tnetissueputlog as issue
		        	inner join tmedia on issue.fmediaid=tmedia.fid and tmedia.main_media_id=tmedia.fid
		        	inner join tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = '".$system_num."' and ttp.fuserid=".session('regulatorpersonInfo.fid')."
		        	inner join tnetissue as sample on sample.major_key = issue.tid  and finputstate=2
		        	where issue.fissuedate BETWEEN UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 7 DAY)) and UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 1 DAY)) ".$wherestrnet."
		        	group by issue.fissuedate
		         ";
	         	try{
		        	$netdata = M()->query($tb_netsql);
	            }catch (Exception $e){
	         		$netdata = [];
	            }
		        foreach ($netdata as $key => $value) {
		        	if(!empty($value)){
		        		$data['tenetcount'] += $value['tecount'];
		        	}
		        }
		        $data_arr[] = $netdata;
		    }

			$data['issuecount'] = 0;
			$timedata = get_weeks();
			foreach ($timedata as $key => $value) {
				$data['sevendaydata'][$key]['nowdate'] = $value;//近7天时间
				$data['sevendaydata'][$key]['nowcount'] = 0;//近7天每天发布量
				$data['sevendaydata'][$key]['nowcount2'] = 0;//近7天每天违法量
				foreach ($data_arr as $key3 => $value3) {
					foreach ($value3 as $key2 => $value2) {
						if($value2['fissuedate'] == $value){
							if(!empty($value2['tecount'])){
								$data['sevendaydata'][$key]['nowcount'] += $value2['tecount'];
								$data['issuecount'] += $value2['tecount'];
								$data['sevendaydata'][$key]['nowcount2'] += $value2['tillcount'];
							}
						}
					}
				}
			}

			//统计违法线索量
			if(!empty($isillegal_summary)){
			    $illegalcount = $this->getIllCount();
			    $data['illegalcount'] = $illegalcount;
			}else{
				//电视当月违法总量
		        $tb_tvsql2 = "select count(*) as tecount
		        	from $tb_tvissue_nowmonth as issue
		        	inner join tmedia on issue.fmediaid=tmedia.fid and tmedia.main_media_id=tmedia.fid
		        	inner join tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = '".$system_num."' and ttp.fuserid=".session('regulatorpersonInfo.fid')."
		        	inner join ttvsample as sample on sample.fid = issue.fsampleid and sample.fstate = 1
		        	inner join tad on sample.fadid=tad.fadid and tad.fadid<>0 and left(tad.fadclasscode,2) <> '23'
		        	where fillegaltypecode>0 ".$wherestrtv."
		         ";
		        try{
		        	$tvdata2 = M()->query($tb_tvsql2);
		        	$till_tvcount = $tvdata2[0]['tecount'];
	            }catch (Exception $e){
	         		$till_tvcount = 0;
	            }
		        //广播当月违法总量
		        $tb_bcsql2 = "select count(*) as tecount
		        	from $tb_bcissue_nowmonth as issue
		        	inner join tmedia on issue.fmediaid=tmedia.fid and tmedia.main_media_id=tmedia.fid
		        	inner join tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = '".$system_num."' and ttp.fuserid=".session('regulatorpersonInfo.fid')."
		        	inner join tbcsample as sample on sample.fid = issue.fsampleid and sample.fstate = 1
		        	inner join tad on sample.fadid=tad.fadid and tad.fadid<>0 and left(tad.fadclasscode,2) <> '23'
		        	where fillegaltypecode>0 ".$wherestrbc."
		         ";
		        try{
		        	$bcdata2 = M()->query($tb_bcsql2);
		        	$till_bccount = $bcdata2[0]['tecount'];
	            }catch (Exception $e){
		        	$till_bccount = 0;
	            }
		        //报纸当月的违法总量
		        $tb_papersql2 = "select count(*) as tecount
		        	from tpaperissue as issue
		        	inner join tmedia on issue.fmediaid=tmedia.fid and tmedia.main_media_id=tmedia.fid
		        	inner join tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = '".$system_num."' and ttp.fuserid=".session('regulatorpersonInfo.fid')."
		        	inner join tpapersample as sample on sample.fpapersampleid = issue.fpapersampleid and sample.fstate = 1
		        	inner join tad on sample.fadid=tad.fadid and tad.fadid<>0 and left(tad.fadclasscode,2) <> '23'
		        	where  DATE_FORMAT(issue.fissuedate,'%Y-%m-%d') = '".date('Y-m')."' ".$wherestrpaper." and fillegaltypecode>0
		         ";
		        try{
		        	$paperdata2 = M()->query($tb_papersql2);
		        	$till_papercount = $paperdata2[0]['tecount'];
	            }catch (Exception $e){
	         		$till_papercount = 0;
	            }
		        if(in_array('13', $media_class)){
			        //互联网当月违法总量
			        $tb_netsql2 = "select count(*) as tecount
			        	from tnetissueputlog as issue
			        	inner join tmedia on issue.fmediaid=tmedia.fid and tmedia.main_media_id=tmedia.fid
			        	inner join tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = '".$system_num."' and ttp.fuserid=".session('regulatorpersonInfo.fid')."
			        	inner join tnetissue as sample on sample.major_key = issue.tid  and finputstate=2
			        	where from_unixtime((issue.fissuedate),'%Y-%m') = '".date('Y-m')."' ".$wherestrnet." and fillegaltypecode>0 and is_first_broadcast = 1
			         ";
			        try{
			        	$netdata2 = M()->query($tb_netsql2);
		            }catch (Exception $e){
		         		$netdata2 = [];
		            }
			        $till_netcount = $netdata2[0]['tecount'];
		        }
		        $data['tillegaladcount'] = $till_tvcount+$till_bccount+$till_papercount+$till_netcount;//违法量总数
			}
		}else{
			$illegalcount = $this->getIllCount();
			$data['illegalcount'] = $illegalcount?$illegalcount:0;
			$illegalprocessedcount = $this->getIllCount(10);
			$data['illegalprocessedcount'] = $illegalprocessedcount?$illegalprocessedcount:0;
			$illegalwaitcount = $this->getIllCount(0);
			$data['illegalwaitcount'] = $illegalwaitcount?$illegalwaitcount:0;
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}



	/**
	* 获取首页的统计数据
	 * @return array|string  $data 数据
	 * by zw
	 */
	public function getindexdata2(){
		session_write_close();
		$ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isillegal_summary = $ALL_CONFIG['isillegal_summary'];
        $ischeck = $ALL_CONFIG['ischeck'];
        $illDistinguishPlatfo = $ALL_CONFIG['illDistinguishPlatfo'];//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
        $media_class = json_decode($ALL_CONFIG['media_class'],true);//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

		$where_ta['fmediaownerid'] 	= array('neq',0);
		$where_ta['tmedia.fstate'] 		= 1;
		$where_ta['_string'] 		= 'tmedia.fid=tmedia.main_media_id';
		$do_ta = M('tmedia')
			->master(true)
			->field('fmediaclassid')
			->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fstate = 1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->where($where_ta)
			->count();

		$data['mediacount'] 	= $do_ta;//媒体数量

        //是否抽查模式
		if(!empty($ischeck)){
		    $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
		    //如果抽查表有数据
		    if(!empty($spot_check_data)){
		        $dates = [];//定义日期数组
		        foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
		            $year_month = '';
		            $date_str = [];
		            $year_month = substr($spot_check_data_val['fmonth'],0,7);
		            if(!empty($spot_check_data_val['condition'])){
		            	$date_str = explode(',',$spot_check_data_val['condition']);
		            }
		            foreach ($date_str as $date_str_val){
		                $dates[] = $year_month.'-'.$date_str_val;
		            }
		        }
				$wherestr .= ' and a.fdate in ("'.implode('","', $dates).'")';
		    }else{
		        $wherestr .= ' and 1=0';
		    }
		}

		$wherestr = ' and a.fcustomer = "'.$system_num.'" ';

        //近7天总量
        $tb_sql = "SELECT
			fdate fissuedate,fmedia_class_code,sum(fad_times_long_ad) tecount
			FROM
				tbn_ad_summary_day_v3 a,
				tmedia_temp b,
				tmedia c
			WHERE a.fmediaid = b.fmediaid ".$wherestr."
			AND a.fmediaid = c.fid
			AND fdate between
				DATE_SUB(CURDATE(), INTERVAL 7 DAY)
			AND DATE_SUB(CURDATE(), INTERVAL 1 DAY)
			AND b.ftype = 1
			AND b.fcustomer = '".$system_num."'
			AND b.fuserid = ".session('regulatorpersonInfo.fid')."
			AND left(c.media_region_id,2) = ".substr($system_num, (strlen($system_num)-6), 2)."
			group by fdate,fmedia_class_code";

        $tbdata = M()->query($tb_sql);
		$data['issuecount']= 0;
		$data['tetvcount']= 0;
		$data['tebccount']= 0;
		$data['tepapercount']= 0;
		$data['tenetcount']= 0;
		$timedata = get_weeks();
		foreach ($timedata as $key => $value) {
			$data['sevendaydata'][$key]['nowdate'] = $value;//近7天时间
			$data['sevendaydata'][$key]['nowcount'] = 0;//近7天每天发布量
			foreach ($tbdata as $key2 => $value2) {
				if($value2['fissuedate'] == $value){
					if(!empty($value2['tecount'])){
						//每个媒体类别7天发布量
						if(in_array($value2['fmedia_class_code'], $media_class)){
							if($value2['fmedia_class_code'] == '01'){
								$data['tetvcount'] += $value2['tecount'];
							}elseif($value2['fmedia_class_code'] == '02'){
								$data['tebccount'] += $value2['tecount'];
							}elseif($value2['fmedia_class_code'] == '03'){
								$data['tepapercount'] += $value2['tecount'];
							}elseif($value2['fmedia_class_code'] == '13'){
								$data['tenetcount'] += $value2['tecount'];
							}
							$data['sevendaydata'][$key]['nowcount'] += $value2['tecount'];
							$data['issuecount'] += $value2['tecount'];
						}
					}
				}
			}
		}

		//统计违法线索量
		if(!empty($isillegal_summary)){
		    $illegalcount = $this->getIllCount();
		    $data['illegalcount'] = $illegalcount;
		}else{
			//------获取本月违法量开始------
			$bdmedias = get_owner_media_ids();//媒体权限组

			$client = A('Common/OpenSearch','Model')->client();//获得OpenSearchClient
			$searchClient = new SearchClient($client);
			$params = new SearchParamsBuilder();
			$params->setAppName(C('OPEN_SEARCH_CUSTOMER'));//设置应用名称

			$where = array();
			$where['fillegaltypecode'] = 30;//搜索违法类别

			$where['regionid_array'] = substr(session('regulatorpersonInfo.regionid'),0,2).'0000';

			$forgid = strlen($system_num) == 6 ? '20'.$system_num:$system_num;
			$where['forgid'] = $forgid;

			$where['_string'][] = ' fissuedate2:['.strtotime(date('Y-m-01')).','.time().']';//时间搜索条件

			$setQuery = A('Common/OpenSearch','Model')->setQuery($where);//组装搜索字句


			// var_dump(date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s').' -1 day')));die;
			$whereFilter = array();
			$whereFilter['fmediaid'] = array('in',$bdmedias);
			$setFilter = A('Common/OpenSearch','Model')->setFilter($whereFilter);//组装过滤条件

			$params->setQuery($setQuery);//设置搜索
			$params->setFilter($setFilter);//设置文档过滤条件
			$params->setFormat("json");//数据返回json格式

			$params->setFetchFields(
				array(	
					'identify',//识别码（哈希）
					'fissuedate',
				)
			);//设置需返回哪些字段

			$params->setStart(0);//起始位置
			$params->setHits(10);//返回数量
			$ret = $searchClient->execute($params->build());//执行查询并返回信息
			$result = json_decode($ret->result,true);
			$data['tillegaladcount'] = $result['result']['total']?$result['result']['total']:0;
			//------获取本月违法量结束------

			//------近7天违法广告量开始-----
			foreach ($data['sevendaydata'] as $key => $value) {
				$whereFilter['fissuedate'] = array('between',array(strtotime($value['nowdate']),strtotime($value['nowdate'])));
				$setFilter = A('Common/OpenSearch','Model')->setFilter($whereFilter);//组装过滤条件
				$params->setFilter($setFilter);//设置文档过滤条件
				$ret = $searchClient->execute($params->build());//执行查询并返回信息
				$result = json_decode($ret->result,true);
				$data['sevendaydata'][$key]['nowcount2'] = $result['result']['total']?$result['result']['total']:0;//近7天每天违法发布量
			}
			//------近7天违法广告量结束-----
		}

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}

	/**
	* 获取首页的统计数据
	 * @return array|string  $data 数据
	 * by zw
	 */
	public function getindexdata3(){
		session_write_close();
		$ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isillegal_summary = $ALL_CONFIG['isillegal_summary'];
        $ischeck = $ALL_CONFIG['ischeck'];
        $illDistinguishPlatfo = $ALL_CONFIG['illDistinguishPlatfo'];//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
        $media_class = json_decode($ALL_CONFIG['media_class'],true);//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

        //各媒体数量
		$data['tvmediacount']	= 0;
		$data['bcmediacount']	= 0;
		$data['papermediacount']= 0;
		$data['netmediacount']	= 0;
		$data['outmediacount']	= 0;

		$do_ta = M('tmedia')
			->master(true)
			->alias('a')
			->field('fmediaclassid')
			->join('tmediaowner d on d.fid=a.fmediaownerid ')
			->join('tmedia_temp ttp on a.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fstate = 1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->join('tregion c on a.media_region_id=c.fid')
			->where('a.fid=a.main_media_id and a.fstate = 1')
			->select();
		foreach ($do_ta as $key => $value) {
			$mediatp = substr($value['fmediaclassid'],0,2);
			if($mediatp=='01'){
				$data['tvmediacount'] += 1;
			}elseif($mediatp=='02'){
				$data['bcmediacount'] += 1;
			}elseif($mediatp=='03'){
				$data['papermediacount'] += 1;
			}elseif($mediatp=='13'){
				$data['netmediacount'] += 1;
			}elseif($mediatp=='05'){
				$data['outmediacount'] += 1;
			}
		}


        //是否抽查模式
		if(!empty($ischeck)){
		    $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
		    //如果抽查表有数据
		    if(!empty($spot_check_data)){
		        $dates = [];//定义日期数组
		        foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
		            $year_month = '';
		            $date_str = [];
		            $year_month = substr($spot_check_data_val['fmonth'],0,7);
		            if(!empty($spot_check_data_val['condition'])){
		            	$date_str = explode(',',$spot_check_data_val['condition']);
		            }
		            foreach ($date_str as $date_str_val){
		                $dates[] = $year_month.'-'.$date_str_val;
		            }
		        }
				$wherestr .= ' and a.fdate in ("'.implode('","', $dates).'")';
		    }else{
		        $wherestr .= ' and 1=0';
		    }
		}

		$wherestr = ' and a.fcustomer = "'.$system_num.'" ';

        //近7天总量
        $tb_sql = "SELECT
			fdate fissuedate,fmedia_class_code,sum(fad_times_long_ad) tecount
			FROM
				tbn_ad_summary_day_v3 a,
				tmedia_temp b,
				tmedia c
			WHERE a.fmediaid = b.fmediaid ".$wherestr."
			AND a.fmediaid = c.fid
			AND fdate between
				DATE_SUB(CURDATE(), INTERVAL 7 DAY)
			AND DATE_SUB(CURDATE(), INTERVAL 1 DAY)
			AND b.ftype = 1
			AND b.fcustomer = '".$system_num."'
			AND b.fuserid = ".session('regulatorpersonInfo.fid')."
			AND left(c.media_region_id,2) = ".substr($system_num, 0, 2)."
			group by fdate,fmedia_class_code";

        $tbdata = M()->query($tb_sql);

		$data['issuecount']= 0;
		$data['tetvcount']= 0;
		$data['tebccount']= 0;
		$data['tepapercount']= 0;
		$data['tenetcount']= 0;
		$timedata = get_weeks();
		foreach ($timedata as $key => $value) {
			$data['sevendaydata'][$key]['nowdate'] = $value;//近7天时间
			$data['sevendaydata'][$key]['nowcount'] = 0;//近7天每天发布量
			foreach ($tbdata as $key2 => $value2) {
				if($value2['fissuedate'] == $value){
					if(!empty($value2['tecount'])){
						//每个媒体类别7天发布量
						if(in_array($value2['fmedia_class_code'], $media_class)){
							if($value2['fmedia_class_code'] == '01'){
								$data['tetvcount'] += $value2['tecount'];
							}elseif($value2['fmedia_class_code'] == '02'){
								$data['tebccount'] += $value2['tecount'];
							}elseif($value2['fmedia_class_code'] == '03'){
								$data['tepapercount'] += $value2['tecount'];
							}elseif($value2['fmedia_class_code'] == '13'){
								$data['tenetcount'] += $value2['tecount'];
							}
							$data['sevendaydata'][$key]['nowcount'] += $value2['tecount'];
							$data['issuecount'] += $value2['tecount'];
						}
					}
				}
			}
		}

		//------获取本月违法量开始------
		$bdmedias = get_owner_media_ids();//媒体权限组

		$client = A('Common/OpenSearch','Model')->client();//获得OpenSearchClient
		$searchClient = new SearchClient($client);
		$params = new SearchParamsBuilder();
		$params->setAppName(C('OPEN_SEARCH_CUSTOMER'));//设置应用名称

		$where = array();
		$where['fillegaltypecode'] = 30;//搜索违法类别

		$where['regionid_array'] = substr(session('regulatorpersonInfo.regionid'),0,2).'0000';

		$forgid = strlen($system_num) == 6 ? '20'.$system_num:$system_num;
		$where['forgid'] = $forgid;

		$where['_string'][] = ' fissuedate2:['.strtotime(date('Y-m-01')).','.time().']';//时间搜索条件

		$setQuery = A('Common/OpenSearch','Model')->setQuery($where);//组装搜索字句

		$whereFilter = array();
		$whereFilter['fmediaid'] = array('in',$bdmedias);
		$setFilter = A('Common/OpenSearch','Model')->setFilter($whereFilter);//组装过滤条件

		$params->setQuery($setQuery);//设置搜索
		$params->setFilter($setFilter);//设置文档过滤条件
		$params->setFormat("json");//数据返回json格式

		$params->setFetchFields(
			array(	
				'identify',//识别码（哈希）
				'fissuedate',
			)
		);//设置需返回哪些字段

		$params->setStart(0);//起始位置
		$params->setHits(10);//返回数量

		//------近7天违法广告量开始-----
		foreach ($data['sevendaydata'] as $key => $value) {
			$whereFilter['fissuedate'] = array('between',array(strtotime($value['nowdate']),strtotime($value['nowdate'])));
			$setFilter = A('Common/OpenSearch','Model')->setFilter($whereFilter);//组装过滤条件
			$params->setFilter($setFilter);//设置文档过滤条件
			$ret = $searchClient->execute($params->build());//执行查询并返回信息
			$result = json_decode($ret->result,true);
			$data['sevendaydata'][$key]['nowcount2'] = $result['result']['total']?$result['result']['total']:0;//近7天每天违法发布量
		}
		//------近7天违法广告量结束-----

		//统计违法线索统计量
	    $illData = $this->getIllCount(-1,1);
	    $data['illMonthData'] = $illData;

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}

	/**
	* 获取媒体列表
	 * by zw
	 */
	public function get_medialist(){
		Check_QuanXian(['home']);
		$system_num = getconfig('system_num');

		$p = I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录

		$where_ta['_string'] = '1=1';
		$mclass 	= I('mclass');//媒体类型
		$netadtype  = I('netadtype');//平台类别
		$medianame	= I('medianame');//媒体名称
		$ownername 	= I('ownername');//机构名称
		$regionid 	= I('regionid');//区划ID
		$status 	= I('status');//状态
		$iscontain = I('iscontain');//是否包含下属地区

		if(!empty($mclass)){
			$where_ta['_string'] .= ' and left(fmediaclassid,2)="'.$mclass.'"';
		}

		if(!empty($netadtype)){
			if($netadtype==1){
				$where_ta['_string'] .= ' and left(fmediaclassid,4)="1301"';
			}elseif($netadtype==2){
				$where_ta['_string'] .= ' and left(fmediaclassid,4)="1302"';
			}elseif($netadtype==9){
				$where_ta['_string'] .= ' and left(fmediaclassid,4)="1303"';
			}
		}

		if(!empty($medianame)){
			$where_ta['tmedia.fmedianame'] = array('like','%'.$medianame.'%');
		}

		if(!empty($ownername)){
			$where_ta['tmediaowner.fname'] = array('like','%'.$ownername.'%');
		}

		if(!empty($regionid)){//所属地区
	      if($regionid != '100000'){
	        if(!empty($iscontain)){
				$tregion_len = get_tregionlevel($regionid);
				if($tregion_len == 1 || $tregion_len == 6){//国家级
					$where_ta['tmedia.media_region_id'] = $regionid;
				}elseif($tregion_len == 2){//省级
					$where_ta['tmedia.media_region_id'] = ['like',substr($regionid,0,2).'%'];
				}elseif($tregion_len == 4){//市级
					$where_ta['tmedia.media_region_id'] = ['like',substr($regionid,0,4).'%'];
				}else{
					$where_ta['tmedia.media_region_id'] = $regionid;
				}
	        }else{
        		$where_ta['tmedia.media_region_id'] = $regionid;
	        }
	      }
	    }

		$where_ta['tmedia.fmediaownerid'] = array('neq',0);

		if($status == 20){
			$where_ta['ttp.fstate'] = 0;
		}else{
			$where_ta['ttp.fstate'] = 1;
		}

		$where_ta['_string'] .= ' and tmedia.fid=tmedia.main_media_id';

		$count = M('tmedia')
			->join('tmediaowner on tmediaowner.fid=tmedia.fmediaownerid ','left')
			->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->join('tregion on tmedia.media_region_id=tregion.fid')
			->where($where_ta)
			->count();// 查询满足要求的总记录数

		$data_ta = M('tmedia')
			->field('tmedia.fid,tmedia.fmediacode,tmedia.fdpid,(case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.fcollecttype,tmediaowner.fname,tregion.ffullname,(case when left(fmediaclassid,4)="1301" then "PC端" when left(fmediaclassid,4)="1302" then "移动端" when left(fmediaclassid,4)="1303" then "微信端" end) as mediaclassname,left(fmediaclassid,2) mclass,tmedia.live_m3u8')
			->join('tmediaowner on tmediaowner.fid=tmedia.fmediaownerid ','left')
			->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->join('tregion on tmedia.media_region_id=tregion.fid')
			->where($where_ta)
			->order('tmediaowner.fregionid asc,tmedia.fid asc')
			->page($p,$pp)
			->select();//查询复核数据
			
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data_ta)));

	}

	//获取待办任务
	public function getwaittask(){
		session_write_close();
		$gettypes = I('gettypes');//默认,home
		$ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $agpDataMerge = $ALL_CONFIG['agpDataMerge'];
        $isrelease = $ALL_CONFIG['isrelease'];
        $ischeck = $ALL_CONFIG['ischeck'];
        $systemtips = $ALL_CONFIG['systemtips'];
        $isexamine = $ALL_CONFIG['isexamine'];
        $illDistinguishPlatfo = $ALL_CONFIG['illDistinguishPlatfo'];//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
        $media_class = json_decode($ALL_CONFIG['media_class'],true);//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）
      	$cusHosts = json_decode($ALL_CONFIG['agpCustomerMarge'],true);
      	$cusHosts = array_intersect(S(session('regulatorpersonInfo.fcode').'checkCustomer'),$cusHosts);

    	$taskdata = [];
    	$menujurisdiction = S(session('regulatorpersonInfo.fcode').'_menujurisdiction');
    	if(!empty($gettypes)){
			$taskdata[] = $gettypes;
    	}else{
    		$taskdata = json_decode($systemtips,true);
    	}

    	$where_childtask = '1=1';
		if(!empty($isrelease) || $system_num == '100000'){
			$where_childtask   .= ' and fsend_status=2';
		}

		if(session('regulatorpersonInfo.fregulatorlevel')!=30){
			//是否抽查模式
			if(!empty($ischeck)){
			    $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
			    //如果抽查表有数据
			    if(!empty($spot_check_data)){
			        $dates = [];//定义日期数组
			        foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
			            $year_month = '';
			            $date_str = [];
			            $year_month = substr($spot_check_data_val['fmonth'],0,7);
			            if(!empty($spot_check_data_val['condition'])){
		            	$date_str = explode(',',$spot_check_data_val['condition']);
		            }
			            foreach ($date_str as $date_str_val){
			                $dates[] = $year_month.'-'.$date_str_val;
			            }
			        }
					$where_childtask 	.= ' and tbn_illegal_ad_issue.fissue_date in ("'.implode('","', $dates).'")';
			    }else{
			        $where_childtask 	.= ' and 1=0';
			    }
			}
		}

		//案件待办数量
		$tkname = 'sbchuli|1';
		if((in_array($tkname,$taskdata) || in_array(explode('|',$tkname)[0],$taskdata)) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			if($agpDataMerge == 1 || $agpDataMerge == 2){
		      foreach ($cusHosts as $key => $value) {
		        if($_SERVER['HTTP_HOST'] != $value && $system_num != $value){
		          $joinArr[] = A('Gsbchuli')->returnListSql($value);
		        }else{
		          $joinArr[] = A('Gsbchuli')->returnListSql();
		        }
		      }
		      $joinStr = '('.implode(') union all(', $joinArr).')';
		    }else{
		      $joinStr = A('Gsbchuli')->returnListSql($system_num);
		    }

		    $count_task = M('tbn_illegal_ad')
		      ->alias('a')
		      ->join('tadclass b on a.fad_class_code=b.fcode')
		      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
		      ->join('tregion tn on tn.fid=a.fregion_id')
		      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
		      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.fsendtype = 0 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
		      ->count();

			D('Function')->update_task($tkname,$count_task.'条案件任务待办',$count_task);
		}

		//案件接收待办数量
		$tkname = 'sbchuli|2';
		if((in_array($tkname,$taskdata) || in_array(explode('|',$tkname)[0],$taskdata)) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			if($agpDataMerge == 1 || $agpDataMerge == 2){
		      foreach ($cusHosts as $key => $value) {
		        if($_SERVER['HTTP_HOST'] != $value && $system_num != $value){
		          $joinArr[] = A('Gsbchuli')->returnListSql($value);
		        }else{
		          $joinArr[] = A('Gsbchuli')->returnListSql();
		        }
		      }
		      $joinStr = '('.implode(') union all(', $joinArr).')';
		    }else{
		      $joinStr = A('Gsbchuli')->returnListSql($system_num);
		    }

		    $count_task = M('tbn_illegal_ad')
		      ->alias('a')
		      ->join('tadclass b on a.fad_class_code=b.fcode')
		      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
		      ->join('tregion tn on tn.fid=a.fregion_id')
		      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
		      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.fsendtype = 1 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
		      ->count();

			D('Function')->update_task($tkname,$count_task.'条案件接收任务待办',$count_task);
		}

		//立案调查待办数量
		$tkname = 'lachuli|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			if($agpDataMerge == 1 || $agpDataMerge == 2){
		      foreach ($cusHosts as $key => $value) {
		        if($_SERVER['HTTP_HOST'] != $value && $system_num != $value){
		          $joinArr[] = A('Glachuli')->returnListSql($value);
		        }else{
		          $joinArr[] = A('Glachuli')->returnListSql();
		        }
		      }
		      $joinStr = '('.implode(') union all(', $joinArr).')';
		    }else{
		      $joinStr = A('Glachuli')->returnListSql($system_num);
		    }

		    $count_task = M('tbn_illegal_ad')
		      ->alias('a')
		      ->join('tadclass b on a.fad_class_code=b.fcode')
		      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id'.$wheres)
		      ->join('tregion tn on tn.fid=a.fregion_id')
		      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
		      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 10 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
		      ->where($whereStr)
		      ->count();

		    D('Function')->update_task($tkname,$count_task.'条立案调查处理任务待办',$count_task);
		}

		//线索登记待办数量
		$tkname = 'fjcluelist|1';
		if((in_array($tkname,$taskdata) || in_array(explode('|',$tkname)[0],$taskdata)) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			if(!empty($illDistinguishPlatfo)){
		      $where_task['left(c.fmediaclassid,2)'] = ['in',$media_class];
		    }
			$where_task['a.fstatus']   = 0;
			$where_task['a.fregisterid'] 	= 0;
		    $where_task['a.fcustomer']  = $system_num;

		    $count_task = M('tbn_illegal_ad')
				->alias('a')
				->join('tmedia d on  a.fmedia_id=d.fid and d.fid=d.main_media_id')//媒介信息
				->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on a.fid = snd.illad_id')
				->join('tadclass e on a.fad_class_code=e.fcode ')//广告内容列别
				->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime from tbn_illegal_ad_issue,tbn_illegal_ad where tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
				->where($where_task)
				->count();
			D('Function')->update_task($tkname,$count_task.'条线索登记任务待办',$count_task);
		}

		 //数据复核数量
		$tkname = 'ginspectresult2|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			$where_task['fcustomer'] = $system_num;
			$where_task['fcheckorg'] = session('regulatorpersonInfo.regionid');
			$where_task['fstate'] = array('in',array(0,2));
			$count_task = M('tbn_data_check_plan')
			  ->master(true)
			  ->where($where_task)
			  ->count();// 查询满足要求的总记录数
	        D('Function')->update_task($tkname,$count_task.'条数据复核任务待办',$count_task);
	    }

		//数据复核待办数量
		$tkname = 'sjfuhe|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			if(!empty($illDistinguishPlatfo)){
		      $where_task['left(d.fmediaclassid,2)'] = ['in',$media_class];
		    }
		    $fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');//机构级别
		    $regionid = substr(session('regulatorpersonInfo.regionid'),0,2);
		    if($fregulatorlevel == 30){
		      $where_task['a.tcheck_jingdu'] = $fregulatorlevel;//复核进度
		    }else{
		      $where_task['a.tregionid'] = $regionid;//区划
		      $where_task['a.tcheck_jingdu'] = $fregulatorlevel;//复核进度
		    }

		    $where_task['a.tcheck_status'] = 0;//处理状态
		    $where_task['a.tkuayu_status'] = 0;//是否跨域
		    $where_task['a.ftype'] = 0;
		    
		    $count_task = M('tbn_data_check')
		      ->master(true)
		      ->alias('a')
		      ->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')
		      ->join('tadclass c on b.fad_class_code=c.fcode')
		      ->join('tmedia d on b.fmedia_id=d.fid and d.fid=d.main_media_id')
		      ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_childtask.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on b.fid=x.fillegal_ad_id')
		      ->join('tbn_case_send snd on snd.fillegal_ad_id=b.fid and snd.fstatus = 30')
		      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on b.fid = csd.fillegal_ad_id')
		      ->where($where_task)
		      ->count();
		     D('Function')->update_task($tkname,$count_task.'条数据复核任务待办',$count_task);
		}

	     //跨地域数据复核待办数量
		$tkname = 'sjfuhe|2';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			if(!empty($illDistinguishPlatfo)){
		      $where_task['left(d.fmediaclassid,2)'] = ['in',$media_class];
		    }
		    $fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');//机构级别
		    $regionid = substr(session('regulatorpersonInfo.regionid'),0,2);
		    if($fregulatorlevel == 30){
		      $where_task['a.tcheck_jingdu'] = $fregulatorlevel;//复核进度
		    }else{
		      $where_task['a.tregionid'] = $regionid;//区划
		      $where_task['a.tcheck_jingdu'] = $fregulatorlevel;//复核进度
		    }

		    $where_task['a.tcheck_status'] = 0;//处理状态
		    $where_task['a.tkuayu_status'] = 10;//是否跨域
		    $count_task = M('tbn_data_check')
		      ->master(true)
		      ->alias('a')
		      ->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')
		      ->join('tadclass c on b.fad_class_code=c.fcode')
		      ->join('tmedia d on b.fmedia_id=d.fid and d.fid=d.main_media_id')
		      ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_childtask.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on b.fid=x.fillegal_ad_id')
		      ->join('tbn_case_send snd on snd.fillegal_ad_id=b.fid and snd.fstatus = 30')
		      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on b.fid = csd.fillegal_ad_id')
		      ->where($where_task)
		      ->count();
		     D('Function')->update_task($tkname,$count_task.'条跨地域数据复核任务待办',$count_task);
		 }

		 //重点案件线索上报待办数量
		$tkname = 'sbzdanjian|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);
		    $where_task['a.fmodel_type']   = 0;
		    $where_task['a.fstatus']       = 20;
		    $where_task['a.fdomain_isout'] = 0;
		    $where_task['a.fcustomer'] = $system_num;
		    $where_task['b.frece_reg_id'] = session('regulatorpersonInfo.fregulatorpid');
		    $where_task['b.fstatus']       = 0;

		    $count_task = M('tbn_illegal_zdad a')
		      ->master(true)
		      ->join('tbn_zdcase_send b on a.fid = b.fillegal_ad_id')
		      ->where($where_task)
		      ->count();
		     D('Function')->update_task($tkname,$count_task.'条重点案件线索任务待办',$count_task);
		 }

		  //跨地域重点案件线索上报待办数量
		$tkname = 'kdyjsanjian|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

		    $where_task['a.fmodel_type']   = 0;
		    $where_task['a.fstatus']       = 20;
		    $where_task['a.fdomain_isout'] = 10;
		    $where_task['a.fcustomer'] = $system_num;
		    $where_task['b.frece_reg_id'] = session('regulatorpersonInfo.fregulatorpid');
		    $where_task['b.fstatus']       = 0;

		    $count_task = M('tbn_illegal_zdad a')
		      ->master(true)
		      ->join('tbn_zdcase_send b on a.fid = b.fillegal_ad_id')
		      ->where($where_task)
		      ->count();
		     D('Function')->update_task($tkname,$count_task.'条跨地域重点案件线索任务待办',$count_task);
		 }

		 //重点案件线索上报待办数量
		$tkname = 'tsbzdanjian|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

		    $where_task['a.fmodel_type']   = 1;
		    $where_task['a.fstatus']       = 20;
		    $where_task['a.fdomain_isout'] = 0;
		    $where_task['a.fcustomer'] = $system_num;
		    $where_task['b.frece_reg_id'] = session('regulatorpersonInfo.fregulatorpid');
		    $where_task['b.fstatus']       = 0;

		    $count_task = M('tbn_illegal_zdad a')
		    	->master(true)
      			->join('tbn_zdcase_send b on a.fid = b.fillegal_ad_id')
		      	->where($where_task)
		      	->count();

		     D('Function')->update_task($tkname,$count_task.'条重点案件线索任务待办',$count_task);
		 }

		 //近三天通报消息数量
	 	$tkname = 'tbqingkuanglb|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			$where_task['mon_sendtreid'] 	= session('regulatorpersonInfo.fregulatorpid');
			$where_task['mon_senduserid'] 	= session('regulatorpersonInfo.fid');
			$where_task['monb_customer'] 	= $system_num;
			$where_task['_string'] 			= 'date_sub(curdate(), INTERVAL 3 DAY) <= date(`mon_send_time`)';
			$count_task = M('tbn_monitor')
			  ->cache(true,120)
			  ->alias('m')
			  ->join('tbn_monitor_bureau as mb on m.mon_id = mb.monb_monid','LEFT')//发送消息
			  ->where($where_task)
			  ->count();// 查询满足要求的总记录数
			 D('Function')->update_task($tkname,$count_task.'条最新通报消息',$count_task);
		}

		 //近三天互动消息数量
		$tkname = 'xxlbjiaoliu|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			$where_task['mrr_receivetreid'] = session('regulatorpersonInfo.fregulatorpid');
	      	$where_task['_string'] 			= 'date_sub(curdate(), INTERVAL 3 DAY) <= date(`me_sendtime`)';
			$count_task = M('tbn_message_receiveuser')
			  ->cache(true,120)
			  ->alias('mr')
			  ->join('tbn_message as m on mr.mrr_meid = m.me_id ', 'LEFT')//接收消息
			  ->where($where_task)
			  ->count();// 查询满足要求的总记录数
	        D('Function')->update_task($tkname,$count_task.'条最新互动信息',$count_task);
	    }

		//地方数据复核待办
		$tkname = 'clueTask|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			$fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');
		    if($fregulatorlevel == 20){
				$where_task['a.tregionid'] = ['like',substr(session('regulatorpersonInfo.regionid'),0,2).'%'];
			}elseif($fregulatorlevel == 10){
				$where_task['a.tregionid'] = ['like',substr(session('regulatorpersonInfo.regionid'),0,4).'%'];
			}elseif($fregulatorlevel == 0){
				$where_task['a.tregionid'] = ['like',substr(session('regulatorpersonInfo.regionid'),0,6).'%'];
			}
	      	$where_task['a.tcheck_jingdu'] 	= session('regulatorpersonInfo.fregulatorlevel');//复核进度
		    $where_task['a.tcheck_status'] 	= 0;//处理状态
			$where_task['a.fsample_type'] 	= 'tv';
	    	$count_task = M('fj_data_check')
	    		->master(true)
	    		->alias('a')
	    		->join('ttvsample g on g.fid=a.fsample_id')
	    		->join('tad b on g.fadid = b.fadid and b.fadid<>0')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id')
		    	->where($where_task)
				->count();
			D('Function')->update_task($tkname,$count_task.'条电视类型复核任务待办',$count_task,'{"fmediaclass":"tv"}');

			$where_task['a.fsample_type'] 	= 'bc';
	    	$count_task = M('fj_data_check')
	    		->master(true)
	    		->alias('a')
	    		->join('tbcsample g on g.fid=a.fsample_id')
	    		->join('tad b on g.fadid = b.fadid and b.fadid<>0')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id')
		    	->where($where_task)
				->count();
			D('Function')->update_task($tkname,$count_task.'条广播类型复核任务待办',$count_task,'{"fmediaclass":"bc"}');

			$where_task['a.fsample_type'] 	= 'paper';
	    	$count_task = M('fj_data_check')
	    		->master(true)
	    		->alias('a')
	    		->join('tbcsample g on g.fid=a.fsample_id')
	    		->join('tad b on g.fadid = b.fadid and b.fadid<>0 ')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id')
		    	->where($where_task)
				->count();
			D('Function')->update_task($tkname,$count_task.'条报纸类型复核任务待办',$count_task,'{"fmediaclass":"paper"}');
		}

		//线索处理待办
		$tkname = 'fjresrecheck|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			$fjxsgl = $ALL_CONFIG['fjxsgl'];
			if(!empty($fjxsgl)){
				if($system_num == 350000){
					$tregister_jurisdiction = M('fj_tregisterjur')->where('ftr_type=10 and ftr_objid='.session('regulatorpersonInfo.fid'))->getField('ftr_jurid',true);//线索管理人员权限
					if(!empty($tregister_jurisdiction)){
						$where_task['b.fstate'] = array('in',$tregister_jurisdiction);
						$where_task['fsendstatus'] = 0;
						$where_task['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
						$count_task = M('fj_tregister')
							// ->master(true)
							->alias('a')
							->join('fj_tregisterflow b on a.fid=b.fregisterid')
							->where($where_task)
							->count();
					}
					D('Function')->update_task($tkname,$count_task.'条线索处理任务待办',$count_task);
				}
			}
		}

		//线索处理待办
		$tkname = 'filladlist|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			$where_time = '1=1';
		    //是否抽查模式
		    if(!empty($ischeck)){
		        $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
		        //如果抽查表有数据
		        if(!empty($spot_check_data)){
		            $dates = [];//定义日期数组
		            foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
		                $year_month = '';
		                $date_str = [];
		                $year_month = substr($spot_check_data_val['fmonth'],0,7);
		                if(!empty($spot_check_data_val['condition'])){
		                  $date_str = explode(',',$spot_check_data_val['condition']);
		                }
		                foreach ($date_str as $date_str_val){
		                    $dates[] = $year_month.'-'.$date_str_val;
		                }
		            }
		        $where_time   .= ' and tbn_illegal_ad_issue.fissue_date in ("'.implode('","', $dates).'")';
		        }else{
		            $where_time   .= ' and 1=0';
		        }
		    }

			if(session('regulatorpersonInfo.fregulatorlevel')==0){
				$where_task['a.fregion_id']  = session('regulatorpersonInfo.regionid');
			}elseif(session('regulatorpersonInfo.fregulatorlevel')==10){
				$where_task['a.fregion_id']  = array('like',substr(session('regulatorpersonInfo.regionid'),0,4).'%');
			}elseif(session('regulatorpersonInfo.fregulatorlevel')==20){
				$where_task['a.fregion_id']  = array('like',substr(session('regulatorpersonInfo.regionid'),0,2).'%');
			}
		    $where_task['a.fexamine'] = 0;
		    $where_task['a.fcustomer'] = $system_num;
		    if(!empty($illDistinguishPlatfo)){
		      $where_task['left(c.fmediaclassid,2)'] = ['in',$media_class];
		    }

		    $count_task = M('tbn_illegal_ad')
		      ->alias('a')
		      ->join('tadclass b on a.fad_class_code=b.fcode')
		      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
		      ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
		      ->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on a.fid = snd.illad_id')
		      ->where($where_task)
		      ->count();//查询满足条件的总记录数
		    D('Function')->update_task($tkname,$count_task.'条线索审核任务待办',$count_task);
		}

		//线索派发待办
        $tkname = 'gdatasend|1';
        if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
            $count_task = 0;
            unset($where_task);

            $wheres1 = '1=1';
            //是否抽查模式
            if(!empty($ischeck)){
                $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
                //如果抽查表有数据
                if(!empty($spot_check_data)){
                    $dates = [];//定义日期数组
                    foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                        $year_month = '';
                        $date_str = [];
                        $year_month = substr($spot_check_data_val['fmonth'],0,7);
                        if(!empty($spot_check_data_val['condition'])){
                            $date_str = explode(',',$spot_check_data_val['condition']);
                        }
                        foreach ($date_str as $date_str_val){
                            $dates[] = $year_month.'-'.$date_str_val;
                        }
                    }
                    $wheres1 .= ' and tbn_illegal_ad_issue.fissue_date in ("'.implode('","', $dates).'")';
                }else{
                    $wheres1 .= ' and 1=0';
                }
            }

            if($isrelease == 2){
                $wheres1 .= ' and tbn_illegal_ad_issue.fsend_status in(0,1) ';
            }else{
                $wheres1 .= ' and tbn_illegal_ad_issue.fsend_status = 1 ';
            }

            if(in_array($isexamine, [10,20,30,40,50])){
              $wheres1 .= ' and tbn_illegal_ad.fexamine = 10';
            }

            if($system_num == '100000'){
                $where_level = ' AND tbn_illegal_ad.fregion_id in (select fid from tregion where flevel in (1,2,3)) ';
            }
            if(!empty($illDistinguishPlatfo)){
		      $where2 = ' and left(b.fmediaclassid,2) in('.implode(',', $media_class).')';
		    }

            //查询总记录数
            $count_task = M()->query('
               SELECT count(*) as fadcount
                FROM
                 (
                SELECT fad_name,fillegal_ad_id,tbn_illegal_ad_issue.fmedia_class
                FROM tbn_illegal_ad_issue,tbn_illegal_ad,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd 
                WHERE tbn_illegal_ad.fid = snd.illad_id and tbn_illegal_ad.fid=tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fmedia_id=tbn_illegal_ad_issue.fmedia_id and tbn_illegal_ad.fcustomer="'.$system_num.'" '.$where_level.'  and '.$wheres1.'
                GROUP BY tbn_illegal_ad.fad_name,tbn_illegal_ad_issue.fillegal_ad_id,tbn_illegal_ad_issue.fmedia_class) a,tmedia b,tmediaowner c,tregion d,tadclass f,tbn_illegal_ad e left join tbn_data_inspectlog dig on e.fid=dig.dig_lid
                WHERE e.fmedia_id=b.fid AND b.fid=b.main_media_id AND e.fad_class_code=f.fcode AND b.fmediaownerid=c.fid AND c.fregionid=d.fid AND a.fillegal_ad_id=e.fid '.$where2);

            D('Function')->update_task($tkname,$count_task[0]['fadcount'].'条线索派发任务待办',$count_task[0]['fadcount']);
        }

        //线索派发待办
        $tkname = 'njcluehandle|1';
        if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
            $count_task = 0;
            unset($where_task);

            //查询总记录数
            $count_task = M()->query('
               SELECT count(*) as illcount
                FROM nj_clue a 
                inner join nj_clueflow b on a.fid = b.fclueid and fcreatetrepid = '.session('regulatorpersonInfo.fregulatorpid').' and fsendstatus = 0
                where a.fstatus in(20,30,40)
                ');
            D('Function')->update_task($tkname,$count_task[0]['illcount'].'条线索督办任务待办',$count_task[0]['illcount']);
        }

		//待办任务
		$where_wk['fuserid'] = session('regulatorpersonInfo.fid');
		$where_wk['ftaskcount'] = array('gt',0);
		$where_wk['fcustomer'] = $system_num;
		$data_wk = M('tbn_waittask')->master(true)->where($where_wk)->select();

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data_wk));

	}

	/*
	* 获取违法广告数据量
	* returntype 0默认返回只获取违法数量，1包含条次
	* by zw
	*/
	public function getIllCount($fstate = -1,$returntype = 0){
		$ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isrelease = $ALL_CONFIG['isrelease'];
        $ischeck = $ALL_CONFIG['ischeck'];
        $isexamine = $ALL_CONFIG['isexamine'];
        $illDistinguishPlatfo = $ALL_CONFIG['illDistinguishPlatfo'];//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
        $media_class = json_decode($ALL_CONFIG['media_class'],true);//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

		$where_time = gettimecondition(date('Y'),30,date('m'),'fissue_date',0,$isrelease);
	    $where_tia['a.fstatus3'] = array('not in',array(30,40));
	    $where_tia['_string'] = '1=1';

	    if($fstate != -1){//违法广告处理状态
	      if($fstate == 0){
	        $where_tia['a.fstatus'] = 0 ;
	        if($fstate2 == 10){
	          $where_tia['a.fview_status'] = 0 ;
	        }elseif($fstate2 == 20){
	          $where_tia['a.fview_status'] = 10 ;
	        }
	      }elseif($fstate == 10){
	        $where_tia['a.fstatus'] = 10 ;
	        if($fstate2 == 10){
	          $where_tia['a.fresult'] = array('like','%责令停止发布%') ;
	        }elseif($fstate2 == 20){
	          $where_tia['a.fresult'] = array('like','%立案%') ;
	        }elseif($fstate2 == 30){
	          $where_tia['a.fresult'] = array('like','%立案%') ;
	          $where_tia['a.fstatus2'] = 17 ;
	        }elseif($fstate2 == 40){
	          $where_tia['a.fresult'] = array('like','%立案%') ;
	          $where_tia['a.fstatus2'] = 16 ;
	        }elseif($fstate2 == 50){
	          $where_tia['a.fresult'] = array('like','%移送司法%') ;
	        }elseif($fstate2 == 60){
	          $where_tia['a.fresult'] = array('like','%其他%') ;
	        }
	      }elseif($fstate == 30){
	        $where_tia['a.fstatus'] = 30 ;
	      }
	    }

	    //国家局系统只显示有打国家局标签媒体的数据
	    if($system_num == '100000'){
	      $wherestr = ' and tn.flevel in (1,2,3)';
	    }

	    $where_tia['a.fcustomer']  = $system_num;
	    
	    //是否抽查模式
	    if(!empty($ischeck)){
	        $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
	        //如果抽查表有数据
	        if(!empty($spot_check_data)){
	            $dates = [];//定义日期数组
	            foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
	                $year_month = '';
	                $date_str = [];
	                $year_month = substr($spot_check_data_val['fmonth'],0,7);
	                if(!empty($spot_check_data_val['condition'])){
			            	$date_str = explode(',',$spot_check_data_val['condition']);
			            }
	                foreach ($date_str as $date_str_val){
	                    $dates[] = $year_month.'-'.$date_str_val;
	                }
	            }
	        $where_time  .= ' and b.fissue_date in ("'.implode('","', $dates).'")';
	        }else{
	            $where_time  .= ' and 1=0';
	        }
	    }

	    if(in_array($isexamine, [10,20,30,40,50])){
	      $where_tia['a.fexamine'] = 10;
	    }
	    if(!empty($illDistinguishPlatfo)){
	      $where_tia['left(c.fmediaclassid,2)'] = ['in',$media_class];
	    }

		$illegalData = M('tbn_illegal_ad')
	      ->alias('a')
	      ->field('x.fcount,a.fstatus,a.fmedia_class')
	      ->join('tadclass b on a.fad_class_code=b.fcode')
	      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
	      ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
	      ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue b,tbn_illegal_ad a where '.$where_time.' and a.fid = b.fillegal_ad_id and a.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
	      ->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on a.fid = snd.illad_id')
	      ->where($where_tia)
	      ->select();
	    if(empty($returntype)){
		    return count($illegalData);
	    }else{
	    	$retdata['ztiaoshu'] = count($illegalData);//线索条数
	    	$retdata['wcltiaoshu'] = 0;//未处理条数
	    	$retdata['ycltiaoshu'] = 0;//已处理条数
	    	$retdata['ztiaoci'] = 0;//线索条条次
	    	$retdata['wcltiaoci'] = 0;//未处理条次
	    	$retdata['ycltiaoci'] = 0;//已处理条次
	    	$retdata['mclassdata'] = [];//各媒体数据

	    	foreach ($illegalData as $key => $value) {
	    		$retdata['ztiaoci'] += $value['fcount'];
	    		$retdata['mclassdata'][sprintf("%02d", $value['fmedia_class'])]['mclassztiaoci'] += $value['fcount'];
	    		if(empty($value['fstatus'])){
	    			$retdata['wcltiaoci'] += $value['fcount'];
	    			$retdata['wcltiaoshu'] += 1;
	    			$retdata['mclassdata'][sprintf("%02d", $value['fmedia_class'])]['mclasswcltiaoci'] += $value['fcount'];
	    			$retdata['mclassdata'][sprintf("%02d", $value['fmedia_class'])]['mclasswcltiaoshu'] += 1;
	    		}else{
	    			$retdata['ycltiaoci'] += $value['fcount'];
	    			$retdata['ycltiaoshu'] += 1;
	    			$retdata['mclassdata'][sprintf("%02d", $value['fmedia_class'])]['mclassycltiaoci'] += $value['fcount'];
	    			$retdata['mclassdata'][sprintf("%02d", $value['fmedia_class'])]['mclassycltiaoshu'] += 1;
	    		}
	    	}
	    	return $retdata;
	    }
	}

}
