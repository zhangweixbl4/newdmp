<?php
namespace Agp\Controller;
use Think\Controller;


/**
 * 日志管理
 */

class FlogManageController extends BaseController
{
	//获取数据
	public function index(){
		Check_QuanXian(['logManagement']);
		session_write_close();
		$system_num = getconfig('system_num');//客户编号
		$outtype = I('outtype');//导出类型
		if(!empty($outtype)){
	      $p  = I('page', 1);//当前第几页ks
	      $pp = 50000;//每页显示多少记录
	    }else{
	      $p  = I('page', 1);//当前第几页ks
	      $pp = 20;//每页显示多少记录
	    }

		$type = I('type');
		if($type){
			$param['type'] = array('like','%'.$type.'%');
		}
		$status = I('status');
		if($status){
			$param['status'] = $status;
		}

		$record_date = I('record_date');

		if($record_date){
			$param['record_date'] = array('BETWEEN',array($record_date[0],$record_date[1]));
		}

		$param['fregulatorpid'] = session('regulatorpersonInfo.fregulatorpid');
		$param['fcustomer'] = $system_num;

		$count 	= M('f_log_manage')->field('fid,type,status,remark,record_date,fusername,fregulatorname')->where($param)->count();//查询满足要求的总记录数

	    $res = M('f_log_manage')->where($param)->page($p,$pp)->order('fid desc')->select();

	    if(empty($outtype)){
		    $this->ajaxReturn(array('code'=>0,'msg'=>'数据获取成功','data'=>array('count'=>$count,'list'=>$res)));//返回ajax
	    }else{
	    	if(empty($res)){
	          $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
	        }

	        $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-日志列表';//文档内部标题名称
	        $outdata['datalie'] = [
	          '序号'=>'key',
	          '操作类型'=>'type',
	          '操作状态'=>[
	            'type'=>'zwif',
	            'data'=>[
	              ['{status} == 1','成功'],
	              ['','失败']
	            ]
	          ],
	          '备注'=>'remark',
	          '记录时间'=>'record_date',
	          '操作人'=>'fusername',
	          '所属部门'=>'fregulatorname'
	        ];
	        $outdata['lists'] = $res;
	        $ret = A('Api/Function')->outdata_xls($outdata);

	        D('Function')->write_log('日志列表',1,'导出成功');
	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
	    }
	}
}