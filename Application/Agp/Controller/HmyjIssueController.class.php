<?php
namespace Agp\Controller;
use Think\Controller;
use Think\Log;

/**
 * 鸿茅药酒广告发布记录查询
 */
class HmyjIssueController extends BaseController {

	/**
	* 构造函数
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
	}

	/**
	* 获取发布列表,默认条件或搜索条件
	* @Param	Mixed variable
	* @Return	Mixed 返回的数据 code-状态（0成功1失败），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	*/
    public function index(){
    	$tableName        = 'hmyj_issue';					//表名
    	$fadname          = I('fadname');					//广告名称
    	$adclass_code     = I('adclass_code');				//广告类别
    	$fmediaid         = I('fmediaid');					//媒介ID
    	$media_class      = I('media_class','01');			//媒介类型，01电视，02广播，03报纸
    	$fmedianame       = I('fmedianame');				//媒介名称
    	$region_id        = I('region_id');					//地区ID
    	$fillegaltypecode = I('fillegaltypecode');			//违法类型
    	$fstarttime       = I('fstarttime',date('Y-m-01'));	//发布日期起始
    	$fendtime         = I('fendtime',date('Y-m-d'));	//发布日期结束
    	$pageIndex        = I('pageIndex',1);				//页码
    	$pageSize         = I('pageSize',10);				//每页记录数
    	$this_region_id   = I('this_region_id');			//是否只查询本级地区
    	$preArr = array(
    		'01' => 'ttv',
    		'02' => 'tbc',
    		'03' => 'tpaper'
    	);
    	$pre = $preArr[$media_class] ? $preArr[$media_class] : 'ttv';
    	$sampleTable = $pre.'sample';				//样本表名
    	$sampleId = $media_class == '03' ? 'fpapersampleid' : 'fid';//样本表主键ID字段名
    	$where = [];//查询条件
    	if($region_id > 0){
    		if($this_region_id == 'true'){
    			$where['T.fregionid'] = $region_id;	//地区搜索条件
    		}elseif($region_id != 100000){
    			$region_id_rtrim = A('Common/System','Model')->get_region_left($region_id);	//地区ID去掉末尾的0
    			$region_id_strlen = strlen($region_id_rtrim);								//去掉0后还有几位
    			$where['left(T.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;		//地区搜索条件
    		}
    	}
    	if($fadname != ''){
    		$where['T.fadname'] = array('like','%'.$fadname.'%');							//广告名称
    	}
    	if ($adclass_code != ''){
    		$len = strlen($adclass_code);
    		$where['LEFT(T.fadclasscode,'.$len.')'] = $adclass_code;						//广告类别代码
    	}
    	if ($fmediaid){
    		//TODO:媒介搜索请求媒介ID，前端待处理
    		//$where['T.fmediaid'] = $fmediaid;//发布媒介
    		$where['T.fmedianame'] = array('like','%'.$fmediaid.'%');						//媒介名称
    	}
    	if ($fmedianame != ''){
    		$where['T.fmedianame'] = array('like','%'.$fmedianame.'%');						//媒介名称
    	}
    	if ($media_class != ''){
    		$where['LEFT(T.fmediaclassid,2)'] = $media_class;										//广告类别ID
    	}
    	if($fillegaltypecode != ''){
    		$where['T.fillegaltypecode'] = $fillegaltypecode;								//违法类型代码
    	}
    	if($fstarttime != '' || $fendtime != ''){
    		$where['T.fissuedate'] = array('between',$fstarttime.','.$fendtime);
    	}
    	$count = M($tableName)
	    	->alias('T')
	    	->where($where)
    		->count();
    	$field = 'fid,
			fmediaclassid,
			fsampleid,
			fmediaid,
			fmedianame,
			fregionid,
			fadname,
			fadowner,
			fbrand,
			fspokesman,
			fadclasscode,
			fstarttime,
			fendtime,
			fissuedate,
			flength,
			fillegaltypecode,
			fexpressioncodes,
			fillegalcontent,
			sam_source_path';
    	$s = ($pageIndex-1)*$pageSize;//第几行
    	$issueList = M($tableName)
    		->alias('T')
    		->cache(true,1200)
    		->field($field)
	    	->where($where)
	    	->order('fstarttime DESC')
	    	->limit($s,$pageSize)
	    	->select();
    	//序号前缀，T-电视 B-广播 P-报纸
    	$fNameArr = array(
    		'01'=>'T',
    		'02'=>'B',
    		'03'=>'P'
    	);
    	$fName = $fNameArr[$media_class] ? $fNameArr[$media_class] : 'T';
    	foreach ($issueList as $k=>$row){
    		$issueList[$k]['fadclass'] = M('tadclass')
    			->cache(true,3600)
    			->where(array('fcode'=>$row['fadclasscode']))
    			->getField('ffullname');
    		$issueList[$k]['fregion'] = M('tregion')
    			->cache(true,3600)
    			->where(array('fid'=>$row['fregionid']))
    			->getField('ffullname');
    		if ($media_class == '01' || $media_class == '02'){
	    		$issueList[$k]['favifilename'] = M($sampleTable)
	    			->cache(true,3600)
	    			->where(array($sampleId => $row['fsampleid']))
	    			->getField('favifilename');
    		}
    		$mName = date('YmdHis',$row['fstarttime']);
    		$lName = substr($row['fid'],-2);
    		$issueList[$k]['findex'] = $fName.$mName.$lName;
    		$issueList[$k]['fstarttime'] = date('Y-m-d H:i:s',$row['fstarttime']);
    		$issueList[$k]['fendtime'] = date('Y-m-d H:i:s',$row['fendtime']);
    		//发布时间显示精确到秒
    		$issueList[$k]['fissuedate'] = date('Y-m-d H:i:s',$row['fstarttime']);
    	}
    	$data = array(
    		'count' => $count,
    		'list'  => $issueList
    	);
    	$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}

	/**
	* 获取广告发布详情
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function detail(){
		$fid = I('fid');							// 发布ID
		$fstarttime = I('fstarttime',date('Y-m-d'));// 发布日期起始
		$fendtime = I('fendtime',date('Y-m-d'));	// 发布日期结束
		$tableName = 'hmyj_issue';
		if (!$fid){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}
		$field = '
			T.fid,
			T.fsampleid,
			T.fmediaid,
			T.fmedianame,
			T.fmediaclassid,
			T.fregionid,
			T.fadid,
			T.fadname,
			T.fadclasscode,
			T.fadowner,
			T.fadownername,
			T.fbrand,
			T.fspokesman,
			T.fstarttime,
			T.fendtime,
			T.fissuedate,
			T.flength,
			T.fillegaltypecode,
			T.fillegalcontent,
			T.fexpressioncodes,
			T.fexpressions,
			T.fconfirmations,
			T.sam_source_path,
			tadclass.ffullname AS fadclass,
			tregion.ffullname AS fregion';
		$Detail = M($tableName)
			->alias('T')
			->field($field)
			->join('tadclass on tadclass.fcode = T.fadclasscode')
			->join('tregion on tregion.fid = T.fregionid')
			->where(['T.fid'=>$fid])
			->find();//查询发布记录详情
		if (empty($Detail)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'记录不存在或已被删除'));
		}
		
		$stime = $Detail['fstarttime'];
		$etime = $Detail['fendtime'] + 30; //强制结束时间加20秒
		$Detail['fstarttime'] = date('Y-m-d H:i:s',$Detail['fstarttime']);
		$Detail['fendtime'] = date('Y-m-d H:i:s',$Detail['fendtime'] + 30);
		$Detail['m3u8_url'] = A('Common/Media','Model')->get_m3u8($Detail['fmediaid'],$stime,$etime);
		//违法判定依据,兼容旧接口
		$Detail['fexpression'] = $Detail['fexpressions'];
		$Detail['fconfirmation'] = $Detail['fconfirmations'];
		$data = $Detail;
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}
	
	/**
	 * 生成广告片段
	 * @Param	Int $issueid 发布记录id
	 * @Param	Varchar $table_name 表名
	 * @Return	void 返回的数据
	 */
	public function create_ad_part(){
		$issueid    = I('fid');//发布记录id
		$fstarttime = I('fstarttime',date('Y-m-01'));// 发布日期起始
		$fendtime   = I('fendtime',date('Y-m-d'));// 发布日期结束
		$tableName  = 'hmyj_issue';//按查询日期确定表名
		$back_url = U('Agp/HmyjIssue/edit_ad_part@'.$_SERVER['HTTP_HOST'],array('issueid'=>$issueid,'table_name'=>$tableName)) ;
		$cut_url = C('ISSUE_CUT_URL');//剪辑广告片段的url
		$issueInfo = M($tableName)
			->cache(true,6)
			->field('fmediaid,fstarttime,fendtime,fsampleid')
			->where(array('fid'=>$issueid))
			->find();
		$start_time = $issueInfo['fstarttime'];
		$end_time = $issueInfo['fendtime'] + 20; //强制结束时间加20秒
		$mediaInfo = M('tmedia')
			->cache(true,300)
			->field('left(fmediaclassid,2) as media_class')
			->where(array('fid'=>$issueInfo['fmediaid']))
			->find();
		if($mediaInfo['media_class'] == '01'){
			$tb = 'tv';
			$file_type = 'mp4';
		}elseif($mediaInfo['media_class'] == '02'){
			$tb = 'bc';
			$file_type = 'mp3';
		}else{
			$tb = 'tv';
		}
		$adInfo = M('t'.$tb.'sample')
			->cache(true,60)
			->field('tad.fadname')
			->join('tad on tad.fadid = t'.$tb.'sample.fadid and tad.fadid<>0')
			->where(array('t'.$tb.'sample.fid'=>$issueInfo['fsampleid']))
			->find();
		$m3u8_url = A('Common/Media','Model')->get_m3u8($issueInfo['fmediaid'],$start_time,$end_time);
		$post_data = array(
			'start_time'=>$start_time,
			'end_time'=>$end_time,
			'm3u8_url'=>urlencode($m3u8_url),
			'file_type'=>$file_type,
			'back_url'=>urlencode($back_url),
			'file_name'=>urlencode(date('YmdHis',$start_time).'_'.$adInfo['fadname'].'_'.$tableName.'_'.$issueid),
		);
		$ret = http($cut_url,$post_data,'GET',false,60);
		$this->ajaxReturn(array('code'=>0,'msg'=>'正在生成'));
	}
	
	/*修改发布记录视频片段地址*/
	public function edit_ad_part(){
		$post_data = file_get_contents('php://input');
		$post_data = json_decode($post_data,true);
		S('issue_m3u8_'.I('get.table_name').'_'.I('get.issueid'),$post_data,864000);
	}
	
	/*获取广告片段生成状态*/
	public function get_ad_part(){
		session_write_close();
		$issueid = I('fid');//发布记录id
		$fstarttime = I('fstarttime',date('Y-m-01'));// 发布日期起始
		//$fendtime = I('fendtime',date('Y-m-d'));// 发布日期结束
		$tableName = 'hmyj_issue';//按查询日期确定表名
		$data = S('issue_m3u8_'.$tableName.'_'.$issueid);
		if($data && intval($data['code']) >= 0){
			$data['source_url'] = $data['source_url'].'?attname=';//立即下载(七牛)
			$this->ajaxReturn($data);
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'请等待'));
		}
	}
	
	/**
	* 导出到文件
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function export(){
		$scheme 	= $_SERVER["REQUEST_SCHEME"] ? $_SERVER["REQUEST_SCHEME"].'://' : '';
		$servername = $_SERVER['SERVER_NAME'] ? $_SERVER['SERVER_NAME'] : '';
		$port 		= $_SERVER["SERVER_PORT"] ? ':'.$_SERVER["SERVER_PORT"] : '';
		$url 		= P_U('/Agp/HmyjIssue/exportExe',array_merge(I()));
		$file_url 	= $scheme.$servername.$port.$url;
		$this->ajaxReturn(array('code'=>0,'msg'=>'导出成功','file_url'=>$file_url));
	}
	
	/**
	* 导出-方法一
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function exportExe(){
		set_time_limit(600);
		$tableName        = 'hmyj_issue';						//表名
		$fstarttime       = I('fstarttime',date('Y-m-01'));		//发布日期起始
		$fendtime         = I('fendtime',date('Y-m-d'));		//发布日期结束
		$pageIndex        = I('pageIndex',1);					//页码
		$pageSize         = I('pageSize',20000);				//每页导出记录数
		$fadname          = I('fadname');						//广告名称
		$adclass_code     = I('adclass_code');					//广告类别
		$fmediaid         = I('fmediaid');						//媒介ID
		$media_class      = I('media_class','01');				//媒介类型，01电视，02广播，03报纸
		$fmedianame       = I('fmedianame');					//媒介名称
		$region_id        = I('region_id');						//地区ID
		$fillegaltypecode = I('fillegaltypecode');				//违法类型ID
		$this_region_id   = I('this_region_id');				//是否只查询本级地区
		$file_type        = strtoupper(I('file_type','EXCEL'));	//导出文件类型 XML、EXCEL、TXT
		$file_name        = I('file_name','导出文件'.time());	 //导出文件名 
		$preArr = array(
			'01' => 'ttv',
			'02' => 'tbc',
			'03' => 'tpaper'
		);
		$pre = $preArr[$media_class] ? $preArr[$media_class] : 'ttv';
		$sampleTable = $pre.'sample';				//样本表名
		$sampleId = $media_class == '03' ? 'fpapersampleid' : 'fid';//样本表主键ID字段名
		$where = [];//查询条件
		if($region_id > 0){
			if($this_region_id == 'true'){
				$where['T.fregionid'] = $region_id;	//地区搜索条件
			}elseif($region_id != 100000){
				$region_id_rtrim = A('Common/System','Model')->get_region_left($region_id);	//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);								//去掉0后还有几位
				$where['left(T.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;		//地区搜索条件
			}
		}
		if($fadname != ''){
			$where['T.fadname'] = array('like','%'.$fadname.'%');							//广告名称
		}
		if ($adclass_code != ''){
			$where['T.fadclasscode'] = $adclass_code;										//广告类别代码
		}
		if ($fmediaid){
			//TODO:媒介搜索请求媒介ID，前端待处理
			//$where['T.fmediaid'] = $fmediaid;//发布媒介
			$where['T.fmedianame'] = array('like','%'.$fmediaid.'%');						//媒介名称
		}
		if ($fmedianame != ''){
			$where['T.fmedianame'] = array('like','%'.$fmedianame.'%');						//媒介名称
		}
		if ($media_class != ''){
			$where['LEFT(T.fmediaclassid,2)'] = $media_class;										//广告类别ID
		}
		if($fillegaltypecode != ''){
			$where['T.fillegaltypecode'] = $fillegaltypecode;								//违法类型代码
		}
		if($fstarttime != '' || $fendtime != ''){
			$where['T.fissuedate'] = array('between',$fstarttime.','.$fendtime);
		}
		$count = M($tableName)
			->alias('T')
			->where($where)
			->count();
		if ($count > 20000){
			$this->ajaxReturn(array('code'=>1,'msg'=>'无法导出，请筛选条件确保数据在20000条以内！'));
		}
		$field = '
			T.fid,
			T.fsampleid,
			T.fmediaid,
			T.fmedianame,
			T.fmediaclassid,
			T.fregionid,
			T.fadid,
			T.fadname,
			T.fadclasscode,
			T.fadowner,
			T.fadownername,
			T.fbrand,
			T.fspokesman,
			T.fstarttime,
			T.fendtime,
			T.fissuedate,
			T.flength,
			T.fillegaltypecode,
			T.fillegalcontent,
			T.fexpressioncodes,
			T.fexpressions,
			T.fconfirmations,
			T.sam_source_path';
		$issueList = M($tableName)
			->alias('T')
			->cache(true,1200)
			->field($field)
			->where($where)
			->order('fissuedate DESC')
			->select();
		//导出文件序号前缀，T-电视 B-广播 P-报纸
		$fNameArr = array(
			'01'=>'T',
			'02'=>'B',
			'03'=>'P'
		);
		$fName = $fNameArr[$media_class] ? $fNameArr[$media_class] : 'T';
		//导出文件数据集
		$dataSet = array();
		foreach ($issueList as $k=>$row){
			$issueList[$k]['fadclass'] = M('tadclass')
				->cache(true,3600)
				->where(array('fcode'=>substr($row['fadclasscode'], 0, 2)))
				->getField('ffullname');
			$issueList[$k]['fregion'] = M('tregion')
				->cache(true,3600)
				->where(array('fid'=>$row['fregionid']))
				->getField('ffullname');
			$issueList[$k]['fmediaclass'] = M('tmediaclass')
				->cache(true,3600)
				->where(array('fid'=>$row['fmediaclassid']))
				->getField('ffullname');
			$issueList[$k]['fillegaltype'] = M('tillegaltype')
				->cache(true,3600)
				->where(array('fcode'=>$row['fillegaltypecode']))
				->getField('fillegaltype');
			$expArr = explode(';', $row['fexpressioncodes']);
			foreach ($expArr as $key=>$val){
				$illegalexp = M('tillegal')
					->cache(true,60)
					->field('fexpression,fconfirmation')
					->where(array('fcode'=>$val,'fstate'=>1))
					->find();
				$issueList[$k]['fexpression'] .= $illegalexp['fexpression'].';';
				$issueList[$k]['fconfirmation'] .= $illegalexp['fconfirmation'].';';
			}
			if ($media_class == '01' || $media_class == '02'){
				$issueList[$k]['favifilename'] = M($sampleTable)
					->cache(true,3600)
					->where(array($sampleId => $row['fsampleid']))
					->getField('favifilename');
			}
			
			$mName = date('YmdHis',$row['fstarttime']);
			$lName = substr($row['fid'],-2);
			$suffix = $media_class == '02' ? '.mp3' : '.mp4';//案例文件名后缀
			//显式地赋值,便于与表头对应及后续维护
			$dataSet[$k] = [
				'findex' 		=> $fName.$mName.$lName,
				'fregion' 		=> $issueList[$k]['fregion'],
				'fmediaclass' 	=> $issueList[$k]['fmediaclass'],
				'fmedianame' 	=> $row['fmedianame'],
				'fstarttime' 	=> date('Y-m-d H:i:s',$row['fstarttime']),
				'flength' 		=> $row['flength'],
				'fadclass' 		=> $issueList[$k]['fadclass'],
				'fadname' 		=> $row['fadname'],
				// 'fillegaltype' 	=> $issueList[$k]['fillegaltype'],
				// 'fconfirmations'=> $issueList[$k]['fconfirmation'],
				// 'fcasefilename'	=> date('YmdHis',$row['fstarttime']).'_'.$row['fadname'].'_'.$tableName.'_'.$row['fid'].$suffix,
				'favifilename' 	=> $issueList[$k]['favifilename'],
				// 'fissueurl'		=> 'http://'.$_SERVER['SERVER_NAME'].'/Api/Issue/create_ad_part_issue?issueid='.$row['fid'].'&table_name='.$tableName,
			];
		}
		$excelHead = ['序号','媒体地区','媒体分类','媒体名称','发布时间','时长或版面','广告类别','广告名称','资源链接'];
		
		//保留版本
		// $excelHead = array(
		// 	'序号','媒体地区','媒体分类','媒体名称','发布日期','时长或版面','广告类别',
		// 	'广告名称','违法类型','判定依据','案例文件名','视频链接','发布链接'
		// );
		
		if($file_type == 'XML'){
			header("Content-Disposition: attachment;filename=".$file_name.".xml");
			echo arrayToXml($dataSet);
			exit;
		}
		$fileContent = A('Common/Csv','Model')->convertFile($dataSet,$excelHead);
		if($file_type == 'EXCEL' || $file_type == 'XLS'){
			header("Content-Disposition: attachment;filename=".$file_name.".csv");
		}elseif($file_type == 'TXT'){
			header("Content-Disposition: attachment;filename=".$file_name.".txt");
		}
		echo $fileContent;
		exit;
	}
	
	/**
	* 导出-方法二
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function exportExc(){
		$tableName = 'hmyj_issue';
		$fstarttime = I('fstarttime',date('Y-m-01'));// 发布日期起始
		$fendtime = I('fendtime',date('Y-m-d'));// 发布日期结束
		$media_class = I('media_class','01');//媒介类型，01电视，02广播，03报纸
		$preArr = array(
			'01' => 'ttv',
			'02' => 'tbc',
			'03' => 'tpaper'
		);
		$pre = $preArr[$media_class] ? $preArr[$media_class] : 'ttv';
		$sampleTable = $pre.'sample';//样本表名
		$regulatorpersonInfo = M('tregulatorperson')
			->where(array('fid'=>session('regulatorpersonInfo.fid')))
			->find();//监管人员信息
		$regulatorInfo = M('tregulator')
			->where(array('fid'=>session('regulatorpersonInfo.fregulatorid')))
			->find();//监管机构信息
		$pageIndex = I('pageIndex',1);//页码
		$pageSize = I('pageSize',10);//每页记录数
		$keyword = I('keyword');//搜索关键词
		$fadname = I('fadname');// 广告名称
		$fversion = I('fversion');// 版本描述
		$fmediaid  = I('fmediaid');// 媒介ID
		$fmedianame = I('fmedianame');//媒介名称
		$fillegaltypecode = I('fillegaltypecode',0);// 违法类型ID
		$region_id = I('region_id');//地区ID
		$this_region_id = I('this_region_id');//是否只查询本级地区
		$adclass_code = I('adclass_code');//广告类别
		
		$file_type = strtoupper(I('file_type','EXCEL'));//导出文件类型 XML、EXCEL、TXT
		$file_name = I('file_name','导出文件'.time());//导出文件名 
		
		$where = array();//查询条件
		if ($fmediaid){
			$where['T.fmediaid'] = $fmediaid;//发布媒介
		}
		if($region_id > 0){
			if($this_region_id == 'true'){
				$where['fregionid'] = $region_id;//地区搜索条件
			}elseif($region_id != 100000){
				$region_id_rtrim = A('Common/System','Model')->get_region_left($region_id);//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}
		}
		if($keyword != ''){
			$where['fadname'] = array('like','%'.$keyword.'%');//广告名称
		}
		if($fadname != ''){
			$where['fadname'] = array('like','%'.$fadname.'%');//广告名称
		}
		if($fillegaltypecode != ''){
			$where['T.fillegaltypecode'] = $fillegaltypecode;//违法类型代码
		}
		if($fstarttime != '' || $fendtime != ''){
			$where['T.fissuedate'] = array('between',$fstarttime.','.$fendtime);
		}
		if ($fmedianame != ''){
			$where['T.fmedianame'] = array('like','%'.$fmedianame.'%');//媒介名称
		}
		
		Log::write('$where：'.json_encode($where),Log::DEBUG);
		
		$excelHead = array(
			'序号','媒体所在地区','媒体分类','媒体名称','广告发布时间'
			,'刊播时长或版面','总局数据库查询的产品类别','广告中标识的产品名称','总局数据库查询的产品名称','广告中标识的生产批准文号'
			,'总局数据库查询的生产批准文号','广告中标识的广告批准文号','总局数据库查询的广告批准文号','广告中标识的生产企业（证件持有者）名称',	'总据库查询的生产企业（证件持有者）名称'
			,'总局数据库查询的生产企业（证件持有者）所在地区','违法广告分类','严重违法广告研判依据','案例文件名','视频链接'
		);
		if($sampleTable == 'tpapersample'){
			$sampleId = 'fpapersampleid';
			$field = '
				T.fid,
				tregion.ffullname AS fregion,
				tmediaclass.ffullname,
				 (case when instr(t.fmedianame,"（") > 0 then left(t.fmedianame,instr(t.fmedianame,"（") -1) else t.fmedianame end) as fmedianame,
				T.fissuedate,
				T.fissuedate AS fissuedate2,
				tadclass.ffullname AS fadclass,
				T.fadname,
				T.fadname AS fadname2,
				IFNULL(T.fadmanuno,"无"),
				IFNULL(T.fmanuno,"无"),
				IFNULL(T.fadapprno,"无"),
				IFNULL(T.fapprno,"无"),
				IFNULL(T.fadent,"无"),
				IFNULL(T.fent,"无"),
				IFNULL(T.fentzone,"无"),
				tillegaltype.fillegaltype,
				IFNULL('.$sampleTable.'.fconfirmations,"无"),
				IFNULL('.$sampleTable.'.fexpressions,"无"),
				IFNULL('.$sampleTable.'.fexpressions AS fexpressions2,"无")';
		}else{
			$sampleId = 'fid';
			$field = '
				T.fid,
				tregion.ffullname AS fregion,
				tmediaclass.ffullname,
				 (case when instr(t.fmedianame,"（") > 0 then left(t.fmedianame,instr(t.fmedianame,"（") -1) else t.fmedianame end) as fmedianame,
				T.fissuedate,
				T.flength,
				tadclass.ffullname AS fadclass,
				T.fadname,
				T.fadname AS fadname2,
				IFNULL(T.fadmanuno,"无"),
				IFNULL(T.fmanuno,"无"),
				IFNULL(T.fadapprno,"无"),
				IFNULL(T.fapprno,"无"),
				IFNULL(T.fadent,"无"),
				IFNULL(T.fent,"无"),
				IFNULL(T.fentzone,"无"),
				tillegaltype.fillegaltype,
				IFNULL('.$sampleTable.'.fconfirmations,"无"),
				IFNULL('.$sampleTable.'.fexpressions,"无"),
				IFNULL('.$sampleTable.'.favifilename,"无")';
		}
		$issueList = M($tableName)
			->alias('T')
			->cache(true,1200)
			->field($field)
			->join($sampleTable.' on '.$sampleTable.'.'.$sampleId.' = T.fsampleid')
			->join('tadclass on tadclass.fcode = T.fadclasscode')
			->join('tregion on tregion.fid = T.fregionid')
			->join('tmediaclass on tmediaclass.fid = T.fmediaclassid')
			->join('tillegaltype on tillegaltype.fcode = T.fillegaltypecode')
			->where($where)
			->order('fissuedate DESC')
			->select();
		//导出文件序号前缀，T-电视 B-广播 P-报纸
		$filePreArr = array(
			'01'=>'T',
			'02'=>'B',
			'03'=>'P'
		);
		if (count($issueList) >= 20000){
			$this->ajaxReturn(array('code'=>1,'msg'=>'无法导出，请确保数据在20000条以内！'));
		}
		$filePre = $filePreArr[$media_class] ? $filePreArr[$media_class] : 'T';
		foreach ($issueList as $k=>$v){
			$mName = date('Ymd',strtotime($v['fissuedate']));
			$lName = substr($v['fid'],0,6);
			$issueList[$k]['fid'] = $filePre.$mName.$lName;
		}
		if($file_type == 'XML'){
			header("Content-Disposition: attachment;filename=".$file_name.".xml");
			echo arrayToXml($issueList);
			exit;
		}
		$csvContent = A('Common/Csv','Model')->convertFile($issueList,$excelHead);
		if($file_type == 'EXCEL' || $file_type == 'XLS'){
			header("Content-Disposition: attachment;filename=".$file_name.".csv");
		}elseif($file_type == 'TXT'){
			header("Content-Disposition: attachment;filename=".$file_name.".txt");
		}
		echo $csvContent;
		exit;
	}
}