<?php
namespace Admin\Controller;
use Think\Controller;
class DeviceController extends BaseController {
	
	
	

    public function index(){

    	if(A('Admin/Authority','Model')->authority('200094') === 0){
			exit('没有权限');//验证是否有新增权限
		}
		$p = I('p',1);//当前第几页
		$pp = 12;//每页显示多少记录
		$keyword = I('keyword');//搜索关键词
		$region_id = intval(I('region_id'));//搜索地区ID
		if($region_id == 100000) $region_id = 0;//如果传值等于100000，相当于查全国
		$collect_id = intval(I('collect_id'));//搜索采集点ID
		$devicetype = I('devicetype');//搜索设备类型
		
		$where = array();//查询条件
		$where['tdevice.fstate'] = array('neq',-1);
		$where['tdevice.fid'] = array('gt',0);
		if($devicetype){//判断是否搜索设备类型
			$devicetype1 = array();
			foreach($devicetype as $devicetype0){
				if($devicetype0 != 3 || C('YUNHE_SHOW')) $devicetype1[] = $devicetype0;
			}
			$devicetype1 = array_merge($devicetype1,array(0));

			$where['tdevice.fdevicetypeid'] = array('in',array_merge($devicetype1));

		}else{
			if(!C('YUNHE_SHOW')) $where['tdevice.fdevicetypeid'] = array('not in','3');
		}
		if($collect_id > 0){
			$where['tcollect.fid'] = $collect_id;//搜索采集点ID
			
		}
		
		
		if($region_id > 0 && $region_id != 100000){
			
			$region_id_rtrim = A('Common/System','Model')->get_region_left($region_id);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(tcollect.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		
		}
		
		
		$region_authority = A('Admin/Authority','Model')->region_authority();//获取地区权限

		if($region_authority){
			$where['left(tcollect.fregionid,2)'] = array('in',$region_authority);
		}
		
		if($keyword != ''){
			$where['tdevice.fname|tdevice.fcode'] = array('like','%'.$keyword.'%');
		} 
		$count = M('tdevice')
							->join('tcollect on tcollect.fid = tdevice.fcollectid','left')
							->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$deviceList = M('tdevice')
								->field('tdevice.*,
										tcollect.fname as collect_name,
										count(tmedia.fid) as media_count,
										tdevicetype.ftype as devicetype_name,
										tdevicetype.icon as devicetype_icon
										')
								->join('tcollect on tcollect.fid = tdevice.fcollectid','left')//连接采集点表，查询采集点名称
								->join('tmedia on tdevice.fid  = tmedia.fdeviceid and tmedia.fstate <> -1','left')//连接媒体表，查询单个设备采集的媒体数量
								->join('tdevicetype on tdevicetype.fid = tdevice.fdevicetypeid','left')//连接设备类型表，查询设备的类型和图标
								->group('tdevice.fid,tdevice.fname')
								->order('tdevice.fonline desc,tdevice.fid desc')
								->where($where)->limit($Page->firstRow.','.$Page->listRows)->select();//查询设备列表
						
		//var_dump(M('tdevice')->getLastSql());						
		$deviceList = list_k($deviceList,$p,$pp);//为列表加上序号
		
		//var_dump($deviceList);
		$devicetypeList = M('tdevicetype')->where(array())->select();//设备类型列表
		$this->assign('deviceList',$deviceList);//设备列表
		$this->assign('devicetypeList',$devicetypeList);//设备类型列表

		$this->assign('page',$Page->show());//分页输出
		$this->display();

	}
	
	
	/*添加、编辑设备*/
	public function add_edit_device(){
		$fid= I('fid');//设备ID
		$fid = intval($fid);//转为数字
		$a_e_data = array();
		$a_e_data['fcollectid'] = I('fcollectid');//采集点编码
		
		$a_e_data['fcode'] = I('fcode');//设备编码或序列号
		$a_e_data['fname'] = I('fname');//设备名称
		$a_e_data['fdevicetypeid'] = I('fdevicetypeid');//设备类型编码
		$a_e_data['ftechparam'] = I('ftechparam');//设备技术参数
		$a_e_data['infrared_code'] = I('infrared_code');//红外控制code
		$a_e_data['hard_version'] = I('hard_version');//硬件版本
		
		$a_e_data['fstate'] = I('fstate',1);//状态，-1删除，0-无效，1-有效
		
		
		$collectInfo = M('tcollect')->cache(true,60)->where(array('fid'=>$a_e_data['fcollectid']))->find();
		
		$region_authority = A('Admin/Authority','Model')->region_authority();
		
		if(!in_array(substr($collectInfo['fregionid'],0,2),$region_authority) && $region_authority){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有该地区的权限'));//返回失败
		}
		
		if($fid == 0){//判断是修改还是新增
			if(A('Admin/Authority','Model')->authority('200091') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
			}
			$is_repeat = M('tdevice')->where(array('fcode'=>$a_e_data['fcode']))->count();//查询fcode是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'设备序列号重复'));//返回设备序列号重复
			
			if($a_e_data['infrared_code'] != ''  && $a_e_data['infrared_code'] != 'new_cloud_box_2'){
				$is_ir_repeat = M('tdevice')->field('fname')->where(array('infrared_code'=>$a_e_data['infrared_code']))->find();//查询红外是否重复
				if($is_ir_repeat){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'该红外MAC在设备【'.$is_ir_repeat['fname'].'】使用'));//返回红外重复
				}
			}
			
			if($a_e_data['fdevicetypeid'] == 3){
				//A('Common/Shilian','Model')->add_new_yunhe($a_e_data['fcode']);
				$a_e_data['device_no'] = A('Common/Shilian','Model')->device_add($a_e_data['fcode']);//如果是广告云盒
				A('Common/Shilian','Model')->device_update($a_e_data['fcode'],0,$a_e_data['fname']);
			}
			 

			$a_e_data['fcreator'] = session('personInfo.fid').'_'.session('personInfo.fname');//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tdevice')->add($a_e_data);//添加数据
			
			if($rr > 0){//判断是否添加成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));//返回失败
			}
			
		}else{//修改
			if(A('Admin/Authority','Model')->authority('200092') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
			}
			$is_repeat = M('tdevice')->where(array('fid'=>array('neq',$fid),'fcode'=>$a_e_data['fcode']))->count();//查询fcode是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'设备序列号重复'));//返回设备序列号重复
			if($a_e_data['infrared_code'] != '' && $a_e_data['infrared_code'] != 'new_cloud_box_2'){
				$is_ir_repeat = M('tdevice')->field('fname')->where(array('fid'=>array('neq',$fid),'infrared_code'=>$a_e_data['infrared_code']))->find();//查询红外是否重复
				if($is_ir_repeat){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'该红外MAC在设备【'.$is_ir_repeat['fname'].'】使用'));//返回红外重复
				}
			}
			if($a_e_data['fdevicetypeid'] == 3){
				//A('Common/Shilian','Model')->move_yunhe($a_e_data['fcode']);
				$a_e_data['device_no'] = A('Common/Shilian','Model')->device_add($a_e_data['fcode']);//如果是广告云盒
				A('Common/Shilian','Model')->device_update($a_e_data['fcode'],0,$a_e_data['fname']);
				
				
			}
			$a_e_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			$deviceInfo = M('tdevice')->field('
											fcollectid,
											fcode,
											fname,
											fdevicetypeid,
											ftechparam,
											infrared_code,
											fstate,
											device_no
												')->where(array('fid'=>$fid))->find();
			
			$arr_contrast = arr_contrast($a_e_data,$deviceInfo,'fmodifier,fmodifytime');
			
			$rr = M('tdevice')->where(array('fid'=>$fid))->save($a_e_data);//修改数据
			
			
			
			
			
			if($rr > 0){//判断是否修改成功
				M('save_table_log')->add(array(
												'save_table' => 'tdevice',
												'primary_key' => $fid,
												'save_time' => date('Y-m-d H:i:s'),
												'save_ago' => json_encode($arr_contrast['ago_arr'],JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT),
												'save_later' => json_encode($arr_contrast['later_arr'],JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT),
												'save_user' => session('personInfo.fid').'_'.session('personInfo.fname'),
													));
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));//返回失败
			}
			
		}
		
		
		
	}
	
	
	/*设备详情*/
	public function ajax_device_details(){
		
		$fid = I('fid');//获取设备ID
		$deviceDetails = M('tdevice')->where(array('fid'=>$fid))->find();
		
		$deviceDetails['devicetype_name'] = M('tdevicetype')->where(array('fid'=>$deviceDetails['fdevicetypeid']))->getField('ftype');//设备类型名称
		$deviceDetails['collect_name'] = M('tcollect')->where(array('fid'=>$deviceDetails['fcollectid']))->getField('fname');//采集点名称
		$deviceDetails['collect_name'] = M('tcollect')->where(array('fid'=>$deviceDetails['fcollectid']))->getField('fname');//采集点名称
		$deviceDetails['device_run_state'] = '未知';
		if($deviceDetails['device_no'] > 0){
			$device_run_state = A('Common/Shilian','Model')->device_checkonline(array($deviceDetails['device_no']));
			if($device_run_state['ret'] === 0 ){
				$device_run_state = $device_run_state['state'][0];
				//var_dump($device_run_state);
			}
			
		}
		if($deviceDetails['infrared_code'] != ''){//红外在线状态和最后在线时间
			$ir_online = A('Common/Infrared','Model')->online_check($deviceDetails['infrared_code']);//检查红外设备是否在线
			
			
			if($ir_online['online'] == true) $deviceDetails['ir_run_state'] = '在线';
			if($ir_online['online'] == false) $deviceDetails['ir_run_state'] = '离线';
			$deviceDetails['keepaliveTime'] = date('Y-m-d H:i:s',$ir_online['keepaliveTime']);

		}
		

		
		if($device_run_state === true){
			M('tdevice')->where(array('fid'=>$fid))->save(array('fonline'=>1));
			$deviceDetails['device_run_state'] = '在线';
		}elseif($device_run_state === false){
			M('tdevice')->where(array('fid'=>$fid))->save(array('fonline'=>0));
			$deviceDetails['device_run_state'] = '离线';
		}else{
			M('tdevice')->where(array('fid'=>$fid))->save(array('fonline'=>null));
		}
		$mediaList = M('tmedia')->where(array('fdeviceid'=>$fid))->getField('fmedianame',true);

		$deviceDetails['mediaList'] = implode('、',$mediaList);

		$this->ajaxReturn(array('code'=>0,'deviceDetails'=>$deviceDetails));
	}
	
	
	/*删除设备*/
	public function ajax_device_del(){
		
		//M()->startTrans();
		if(A('Admin/Authority','Model')->authority('200093') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
		}
		$fid = I('fid');//获取设备ID
		$media_count = M('tmedia')->where(array('fdeviceid'=>$fid))->count();//查询这个设备的媒介数量
		$deviceInfo = M('tdevice')->where(array('fid'=>$fid))->find();//查询设备信息
		if($media_count > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败,该设备还有关联的媒介'));
		
		$rr = M('tdevice')->where(array('fid'=>$fid))->save(array('fstate'=>-1));
		
		
		if($rr > 0){
			A('Common/Shilian','Model')->device_del($deviceInfo['fcode']);//调用拾联接口删除设备
			M('tdevice')->where(array('fid'=>$fid))->delete();//删除数据库记录
			//M()->rollback();
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除失败,原因未知'));
		}
		
		
	}
	
	
	/*获取设备运行参数*/
	public function get_device_run_param(){
		$fid = I('fid');//获取设备ID
		$deviceDetails = M('tdevice')->field('fname,frunparam,frunparamupdatetime,foperatingsystem')->where(array('fid'=>$fid))->find();//设备详情
		
		if(is_null(json_decode($deviceDetails['frunparam']))){//判断运行参数是否为json
			$deviceDetails['frunparam'] = '{"disk":[{"name":"磁盘","free":800120,"total":1024000,"use":223880}],"memory_use_ratio":'.(rand(35,40)/100).',"cpu_use_ratio":'.(rand(10,15)/100).',"collect_time":"1900-01-01 00:00:00","sn":"000000"}';
			
		}
		
		$this->ajaxReturn($deviceDetails);
		
	}
	
	/*获取硬件版本号*/
	public function getdeviceversion(){
		$fid = I('fid');//获取设备ID
		if(!$fid) $this->ajaxReturn(array('code'=>-1,'msg'=>'参数错误 fid未获取到'));
		$deviceDetails = M('tdevice')->field('fcode')->where(array('fid'=>$fid))->find();//设备详情
		if(!$deviceDetails) $this->ajaxReturn(array('code'=>-1,'msg'=>'找不到此设备'));
		$rr = A('Common/Shilian','Model')->getdeviceversion($deviceDetails['fcode']);
		#var_dump($rr['devinfos'][0]['hw_version']);
		$this->ajaxReturn(array('code'=>0,'msg'=>'','hard_version'=>$rr['devinfos'][0]['hw_version']));
	}
	
	
	
	
	

}