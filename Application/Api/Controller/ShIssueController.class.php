<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;

class ShIssueController extends Controller {

    /**
    * 接口接收的JSON参数并解码为对象
    */
    protected $oParam;

    /**
     * 初始化
     */
    public function _initialize(){
        header("Content-type: text/html; charset=utf-8");
        if(I('auto') == '1'){
            echo '<meta http-equiv="refresh"content="0.1"/>';
        }
        $this->oParam = json_decode(file_get_contents('php://input'),true);
    }

    /**
     * 开始上海数据抽取任务
     */
    public function index(){
        session_write_close();
        set_time_limit(600);
        // 上海计划优先抽取，再抽取其它
        // $this->getShIssue();
        // 原其它地区数据
        for($i=0;$i<50;$i++){
            $this->shIssueExc();
        }
    }

    /**
     * 任务执行
     */
    public function shIssueExc(){
        session_write_close();
        set_time_limit(80);
        $dateInfo = A('Common/System','Model')->important_data('sh_issue'); //获取处理过的日期
        $dateInfo = json_decode($dateInfo,true);//转为数组
        if(!$dateInfo){//如果是空的
            $month_date = date('Y-m-d',time() - 86400 * 8);
            $media_id = 0;
        }else{
            $month_date = $dateInfo['month_date'];//处理月份
            $media_id = (int)$dateInfo['media_id'];//处理媒介id
        }
        // 上海局(310000)需要的数据,聊城局(371500)需要的数据(20181024),襄阳(420600)需要的数据(20181025),北京(110000)需要的数据20190307,安徽(340000)需要的数据20190310
        // 上海数据停止自动抽取20190321
        $where = [
            'media_region_id' => ['IN','371500,420600,110000,340000'],
            'fid' => ['GT',$media_id],
            'fstate' => 1
        ];
        $mediaInfo = M('tmedia')
            ->cache(true,120)
            ->field('fid')
            ->where($where)
            ->order('fid asc')
            ->find();
        if(!$mediaInfo){//判断能否查到媒介
            if(strtotime($month_date) > time()){//判断日期是否处理到了最近一天
                A('Common/System','Model')->important_data('sh_issue','{}');
            }else{
                A('Common/System','Model')->important_data('sh_issue',
                    json_encode([
                        'month_date'=>date("Y-m-d",strtotime($month_date) + 86400),//时间戳加一天
                        'media_id'=>0
                        ]
                    )
                );//不是最近一天
            }
            exit;
        }else{
            $ff = A('Common/System','Model')
                ->important_data('sh_issue',json_encode(['month_date'=>$month_date,'media_id'=>$mediaInfo['fid']]));
        }
        A('Api/ShIssue','Model')->ShIssue($month_date,$mediaInfo['fid']);
    }

    /*
    * 获取用户判断任务内的发布记录是否最新广告
    * taskid 任务表ID，update是否更新状态（更新状态只更新发布记录的是否新广告状态值，不进行剪辑任务生成，0否，1是）
    * by zw
    */
    public function gettask_isnew_tad($taskid = 0, $update = 0){
        $oldidarr = [];//存旧广告ID
        $newidarr = [];//存新广告ID
        $samplearr = [];//存样本信息
        $noCutMedia = [11200000004897];

        //获取任务信息
        $where_tt['a.fid'] = $taskid;
        $where_tt['a.fdstatus'] = ['neq',11];
        $do_tt = M('tapimedialist')
            ->alias('a')
            ->field('from_unixtime(fissuedate,"%Y-%m-%d") fissuedate,b.fmediaid,b.fmediatype')
            ->join('tapimedia_map b on a.fmediaid = b.fid')
            ->where($where_tt)
            ->find();
        $mtype = $do_tt['fmediatype'] == 1?'mp4':'mp3';//生成剪辑文件类型
        if(!empty($do_tt)){
            //获取任务的发布记录
            $where_sie['fmediaid'] = $do_tt['fmediaid'];
            $where_sie['fissuedate'] = $do_tt['fissuedate'];
            $do_sie = M('sh_issue')
                ->field('fsampleid,fid,fstarttime,fendtime,fmediaid,fisillegal')
                ->where($where_sie)
                ->order('fid asc')
                ->select();
            if(!empty($do_sie)){
                M('sh_issue')
                    ->master(true)
                    ->where($where_sie)
                    ->save(['fis_new'=>0]);
                foreach ($do_sie as $key => $value) {
                    if(empty($value['fsampleid'])){//用户手动新增数据，加入到新广告中
                        $newidarr[] = $value['fid'];
                        if(empty($update)){
                            $data_td = [];
                            $data_td['fuser_id'] = 'new_tad_cut_task';
                            $data_td['fmedia_id'] = $value['fmediaid'];
                            $data_td['fissue_date'] = date('Y-m-d',$value['fstarttime']);
                            $data_td['fuuid'] = $value['fid'];
                            $data_td['fsampleid'] = $value['fsampleid'];
                            $data_td['fstart_time'] = date('Y-m-d H:i:s',$value['fstarttime']);
                            $data_td['fend_time'] = date('Y-m-d H:i:s',$value['fendtime']);
                            $data_td['fmedia_class_id'] = $do_tt['fmediatype'];
                            $data_td['fcreate_time'] = date('Y-m-d H:i:s');
                            $data_td['fcreator'] = '新广告剪辑';
                            $data_td['fstatus'] = 0;
                            $waitsend[] = $data_td;
                        }
                    }else{
                        if($value['fisillegal'] == 1 && empty($samplearr[$value['fsampleid']])){//如果被标记为待初审，加入到新广告中
                            $newidarr[] = $value['fid'];
                            if(empty($update)){
                                $data_td = [];
                                $data_td['fuser_id'] = 'new_tad_cut_task';
                                $data_td['fmedia_id'] = $value['fmediaid'];
                                $data_td['fissue_date'] = date('Y-m-d',$value['fstarttime']);
                                $data_td['fuuid'] = $value['fid'];
                                $data_td['fsampleid'] = $value['fsampleid'];
                                $data_td['fstart_time'] = date('Y-m-d H:i:s',$value['fstarttime']);
                                $data_td['fend_time'] = date('Y-m-d H:i:s',$value['fendtime']);
                                $data_td['fmedia_class_id'] = $do_tt['fmediatype'];
                                $data_td['fcreate_time'] = date('Y-m-d H:i:s');
                                $data_td['fcreator'] = '新广告剪辑';
                                $data_td['fstatus'] = 0;
                                $waitsend[] = $data_td;
                            }
                        }else{
                            $where_count['a.fsampleid'] = $value['fsampleid'];
                            $where_count['a.fid'] = ['neq',$value['fid']];
                            $where_count['a.fis_new'] = 1;
                            $do_count = M('sh_issue')
                                ->alias('a')
                                ->join('tapimedia_map b on a.fmediaid = b.fmediaid')
                                ->join('tapimedialist c on c.fmediaid = b.fid and from_unixtime(c.fissuedate,"%Y-%m-%d") = a.fissuedate and c.fdstatus = 4')
                                ->where($where_count)
                                ->count();
                            if(!empty($do_count) || !empty($samplearr[$value['fsampleid']]) ){
                                $oldidarr[] = $value['fid'];
                            }else{
                                $newidarr[] = $value['fid'];
                                if(empty($update)){
                                    $data_td = [];
                                    $data_td['fuser_id'] = 'new_tad_cut_task';
                                    $data_td['fmedia_id'] = $value['fmediaid'];
                                    $data_td['fissue_date'] = date('Y-m-d',$value['fstarttime']);
                                    $data_td['fuuid'] = $value['fid'];
                                    $data_td['fsampleid'] = $value['fsampleid'];
                                    $data_td['fstart_time'] = date('Y-m-d H:i:s',$value['fstarttime']);
                                    $data_td['fend_time'] = date('Y-m-d H:i:s',$value['fendtime']);
                                    $data_td['fmedia_class_id'] = $do_tt['fmediatype'];
                                    $data_td['fcreate_time'] = date('Y-m-d H:i:s');
                                    $data_td['fcreator'] = '新广告剪辑';
                                    $data_td['fstatus'] = 0;
                                    $waitsend[] = $data_td;
                                }
                            }
                        }
                        
                        $samplearr[$value['fsampleid']] = $value['fid'];//将发布ID存入样本数组中
                    }
                    
                }

                //批量更新是否最新广告字段
                if(!empty($oldidarr)){
                    $where_old['fid'] = ['in',$oldidarr];
                    $data_old['fis_new'] = 0;
                    $old_update = M('sh_issue')
                        ->where($where_old)
                        ->save($data_old);
                }

                if(!empty($newidarr)){
                    $where_new['fid'] = ['in',$newidarr];
                    $data_new['fis_new'] = 1;
                    if(empty($update)){
                        if(in_array($do_tt['fmediaid'], $noCutMedia)){
                            $data_new['fcutstatus'] = 0;
                            $data_new['fmodifier'] = "new_tad_cut_task";
                            $data_new['fmodifytime'] = date('Y-m-d H:i:s');
                            $data_new['fcuttime'] = date('Y-m-d H:i:s');
                            $data_new['fcuturl'] = ['exp','sam_source_path'];
                        }else{
                            $data_new['fcutstatus'] = -1;
                        }
                    }
                    $new_update = M('sh_issue') ->where($where_new) ->save($data_new);
                }
            }

            //如果发布记录和旧广告的记录数相等，直接将任务设置完成状态
            if(empty($update)){
                //剪辑任务添加记录
                if(!empty($waitsend) && !in_array($do_tt['fmediaid'], $noCutMedia)){
                    M('tad_cut')->where(['fuuid'=>['in',$newidarr],'fuser_id'=>'new_tad_cut_task','unix_timestamp(fcreate_time)'=>['lt',time()-10]])->delete();
                    M('tad_cut')->addAll($waitsend);
                }
                if(count($do_sie) == count($oldidarr) || in_array($do_tt['fmediaid'], $noCutMedia)){
                    M()->execute('update tapimedialist set fdstatus = 2,fmodifier = "'.session('regulatorpersonInfo.fname').'",fmodifytime = "'.date('Y-m-d H:i:s').'" where fid = '.$taskid);
                }else{
                    M()->execute('update tapimedialist set fdstatus = 11,fmodifier = "'.session('regulatorpersonInfo.fname').'",fmodifytime = "'.date('Y-m-d H:i:s').'" where fid = '.$taskid);
                }
            }
            return true;
        }else{
            return false;
        }
    }

    /**
     * 按上海数据计划抽取发布数据至sh_issue
     * 日期：2019-03-25
     */
    public function getShIssue(){
        session_write_close();
        set_time_limit(80);

        // TODO:按计划抽取已完成的数据
        $planList = M('tapimedialist')
            ->where(['fdstatus' => ['NOT IN',[2,7]]])
            ->select();
        $planMediaList = [];
        $planDateList = [];
        $existPlanList = [];
        $avaPlanList = [];
        foreach($planList as $key => $plan){
            $fmediaidMap = M('tapimedia_map')
                ->cache(true,3600)
                ->where(['fid'=>$plan['fmediaid']])
                ->getField('fmediaid');
            array_push($planMediaList,$fmediaidMap); // 计划内媒介ID
            array_push($planDateList,$plan['fissuedate']); // 计划内日期
            array_push($existPlanList,['fmediaid'=>$fmediaidMap,'fissuedate'=>$plan['fissuedate']]);

            $avaTimeStamp = A('Common/Media','Model')->available_time($fmediaidMap);
            $isAvailable = ($plan['fissuedate'] < ($avaTimeStamp - 86400)) ? true : false;
            if($isAvailable){
                $planInfo = [
                    'month_date' => date('Y-m-d',$plan['fissuedate']),
                    'media_id' => $fmediaidMap
                ];
                // array_push($avaPlanList,$planInfo);
                A('Api/ShIssue','Model')->ShIssue($planInfo['month_date'],$planInfo['media_id']);
            }else{
                continue;
            }
        }

        // TODO:其它已完成的数据

        $dateInfo = A('Common/System','Model')->important_data('sh_issue'); //获取处理过的日期
        $dateInfo = json_decode($dateInfo,true);//转为数组
        if(!$dateInfo){//如果是空的
            $month_date = date('Y-m-d',time() - 86400 * 8);
            $media_id = 0;
        }else{
            $month_date = $dateInfo['month_date'];//处理月份
            $media_id = (int)$dateInfo['media_id'];//处理媒介id
        }
        // 上海局(310000)
        // 'fid' => ['GT',$media_id],
        // 'fid' => ['NOT IN',$planMediaList],
            // 'fid' => [
            //  ['GT',$media_id],
            //  ['NOT IN',$planMediaList],// 排除计划内媒介
            //  'AND'
            // ],
        $where = [
            'fid' => ['GT',$media_id],
            'media_region_id' => ['IN','310000'],
            'fstate' => 1
        ];
        $mediaInfo = M('tmedia')
            ->cache(true,120)
            ->field('fid')
            ->where($where)
            ->order('fid asc')
            ->find();
        // 判断数据是否完成加工
        $avaTimeStampOther = A('Common/Media','Model')->available_time($mediaInfo['fid']);
        $isAvailableOther = ($plan['fissuedate'] < ($avaTimeStampOther - 86400)) ? true : false;
        if($isAvailableOther){
            // 完成加工
            if(!$mediaInfo){
                if(strtotime($month_date) > time()){
                    // 判断日期是否处理到了最近一天
                    A('Common/System','Model')->important_data('sh_issue','{}');
                }else{
                    // 不是最近一天
                    A('Common/System','Model')->important_data('sh_issue',
                        json_encode([
                            'month_date'=>date("Y-m-d",strtotime($month_date) + 86400),//时间戳加一天
                            'media_id'=>0
                            ]
                        )
                    );
                }
                exit;
            }else{
                $ff = A('Common/System','Model')
                    ->important_data('sh_issue',json_encode(['month_date'=>$month_date,'media_id'=>$mediaInfo['fid']]));
            }
            A('Api/ShIssue','Model')->ShIssue($month_date,$mediaInfo['fid']);
        }
    }

    /**
     * 根据设置调整任务优先级（暂上海专用），可轮循 30s/次
     * by zw
     */
    public function update_nettask_priority($taskid = 0){
        if(!empty($taskid)){
            $where_tt['a.fid'] = $taskid;
        }

        $where_tt['a.fdstatus'] = ['in',[0,1]];
        $where_tt['b.fstatus'] = 1;
        $where_tt['b.fmaincode'] = 0;
        $where_tt['b.fmediatype'] = 4;
        $where_tt['b.fmediaid'] = ['neq',0];
        $where_tt['a.fissuedate'] = ['elt',time()];
        $where_tt['_string'] = "a.fmodifytime <= '".date('Y-m-d H:i:s',(time()-600))."' or a.fmodifytime is null";
        $do_tt = M('tapimedialist')
            ->alias('a')
            ->field('a.fid,a.fissuedate,c.fmediaids,b.fthreshold,b.ftasklevel,a.fdcount,a.fdfinishcount,a.fdillegalcount,b.fid bmediaid')
            ->join('tapimedia_map b on a.fmediaid = b.fid')
            ->join('(select group_concat(fmediaid) fmediaids,fmediacode,fmediatype from tapimedia_map where fstatus = 1 group by fmediacode,fmediatype) c on b.fmediacode = c.fmediacode and b.fmediatype = c.fmediatype')
            ->where($where_tt)
            ->order('a.fmodifytime asc')
            ->find();
        if(empty($do_tt)){
            if(!empty($taskid)){
                return true;
            }else{
                $this->ajaxReturn(['code'=>0,'msg'=>'更新成功']);
            }
        }

        $fissuedate = $do_tt['fissuedate'];
        $fmonth = strtotime(date('Y-m-01',$fissuedate));

        $fdcount = 0;
        $fdfinishcount = 0;
        while(!empty($fissuedate)){
            $ftask_starttime = $fissuedate;//任务开始时间

            $where_tk = [];
            $where_tk['media_id'] = ['exp','in ('.$do_tt['fmediaids'].')'];
            $where_tk['issue_date'] = date('Y-m-d',$fissuedate);
            $taskdata = M('tnettask')
                ->field('task_state')
                ->where($where_tk)
                ->select();
            $taskcount = count($taskdata);//当日任务量
            $fdcount += $taskcount;//总数据量

            //更新本日任务优先级
            if($do_tt['ftasklevel']>0 && $taskcount>0){
                M('tnettask') ->where($where_tk) ->save(['priority'=>$do_tt['ftasklevel']]);
            }

            //获取前一天日期
            if($do_tt['fthreshold']>0 && $fdcount<$do_tt['fthreshold']){
                $fissuedate = strtotime("-1 day",$fissuedate);
            }else{
                $fissuedate = 0;
            }

            //判断指定时间是否非本月时间,是否有其他任务已存在
            if(!empty($fissuedate)){
                if($fmonth > $fissuedate){
                    $fissuedate = 0;
                }else{
                    $where_tt2['a.fdstatus'] = ['in',[0,1,2,3,4]];
                    $where_tt2['a.fmediaid'] = $do_tt['bmediaid'];
                    $where_tt2['a.fissuedate'] = $fissuedate;
                    $do_tt2 = M('tapimedialist')
                        ->alias('a')
                        ->where($where_tt2)
                        ->count();
                    if(!empty($do_tt2)){
                        $fissuedate = 0;
                    }
                }
            }

        }

        $data_tmp['fmodifytime'] = date('Y-m-d H:i:s');
        $data_tmp['ftask_starttime'] = $ftask_starttime;
        $data_tmp['fdcount'] = $fdcount;

        $do_tmp = M('tapimedialist')
            ->where(['fid' => $do_tt['fid']])
            ->save($data_tmp);

        if(!empty($do_tmp)){
            return true;
        }else{
            return false;
        }

    }

    /**
     * 轮循推送互联网数据到客户表，上海（2秒/次）
     * by zw
     */
    public function lunxun_push_netsample_shnowtask(){
        set_time_limit(0);

        $major_key = M('tnetissue') 
            ->master(true) 
            ->alias('a')
            ->join('tapimedia_map c on a.fmediaid = c.fmediaid')
            ->join('(select * from tapimedialist where fdstatus = 0) d on c.fid = d.fmediaid')
            ->join('tnetsam_handle f on a.major_key = f.major_key')
            ->getField('a.major_key');

        $this->push_netsample($major_key,0,'31');
    }

    /**
     * 轮循推送互联网数据到客户表，上海（2秒/次）
     * by zw
     */
    public function lunxun_push_netsample_sh(){
        set_time_limit(0);

        $where_nk['b.fmediaclassid'] = ['like','13%'];
        $where_nk['b.media_region_id'] = ['like','31%'];
        $major_key = M('tnetissue') 
            ->master(true) 
            ->alias('a')
            ->join('tmedia b on a.fmediaid = b.fid')
            ->join('tnetsam_handle f on a.major_key = f.major_key')
            ->where($where_nk) 
            ->getField('a.major_key');

        $this->push_netsample($major_key,0,'31');
    }

    /**
     * 轮循推送互联网数据到客户表（5秒/次）
     * by zw
     */
    public function lunxun_push_netsample(){
        set_time_limit(0);
        
        $major_key = M('tnetsam_handle')->getField('major_key');
        $this->push_netsample($major_key,0,'31');

    }

    /**
     * 强制导致互联网数据
     * by zw
     */
    public function testNetSample(){
        $major_key = I('major_key');
        if(empty($major_key)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数有误']);
        }

        $this->push_netsample($major_key,0,'31');
    }

    /**
     * 任务完成后将样本数据推送到客户样本数据表
     * by zw
     */
    public function push_netsample($major_key = 0,$retType = 0,$cr = ''){
        set_time_limit(60);
        $addarea = [];
        $allLogData = [];
        if(empty($major_key)) return rtMsg(['code'=>1,'msg'=>'暂无可同步信息'],$retType);

        //判断是数组还是字符串
        if(is_array($major_key)){
            $where_ne['a.major_key'] = ['in',$major_key];
            $where_nhe['major_key'] = ['in',$major_key];
        }else{
            $where_ne['a.major_key'] = $major_key;
            $where_nhe['major_key'] = $major_key;
        }

        M('tnetsam_handle')->where($where_nhe)->delete();//删除任务

        $where_ne['c.fmediaclassid'] = ['like','13%'];
        if(!empty($cr)){
            $where_ne['c.media_region_id'] = ['like',$cr.'%'];
        }
        $do_ne_all = M('tnetissue')
            ->alias('a')
            ->master(true)
            ->field('a.*,b.fname fownername,c.media_region_id,c.fmediacode cfmediacode,b.fregionid bfregionid,c.fmedianame,g.fname gfname,f.*')
            ->join('tadowner b on a.fadowner = b.fid','left')
            ->join('tmedia c on a.fmediaid = c.fid')
            ->join('tmediaowner g on g.fid = c.fmediaownerid','left')
            ->join('trackers_record e on a.major_key = e.tid','left')
            ->join('tracker f on e.trackerid = f.trackerid','left')
            ->where($where_ne)
            ->group('a.major_key')
            ->select();

        foreach ($do_ne_all as $key => $do_ne) {
            $addarea = [];
            $data_ncr = [];
            $net_trackers = [];
            if(!empty($do_ne['media_region_id'])){
                $sa = [];
                $sa['fregion'] = (int)$do_ne['media_region_id'];
                $sa['ftype'] = 1;
                $sa['fissueid'] = $do_ne['major_key'];
                $addarea[] = $sa;
            }
            if(!empty($do_ne['fregion_id'])){
                $sa = [];
                $sa['fregion'] = (int)$do_ne['fregion_id'];
                $sa['ftype'] = 2;
                $sa['fissueid'] = $do_ne['major_key'];
                $addarea[] = $sa;
            }

            //获取是否有计划，如有，则将记录绑定到计划
            $whereapi['a.fissuedate'] = strtotime(date('Y-m-d',$do_ne['net_created_date']/1000));
            $whereapi['a.fdstatus'] = ['in',[0,1]];
            $whereapi['_string'] = 'b.fmediaid = "'.$do_ne['fmediaid'].'" or child_fmediaid like "%'.$do_ne['fmediaid'].'%"';
            $doapi = M('tapimedialist') 
                ->field('a.fid,a.fuserid,a.priority') 
                ->alias('a')
                ->join('tapimedia_map b on a.fmediaid = b.fid')
                ->join('(select group_concat(tapimedia_map.fmediaid) child_fmediaid,fmaincode,fmediatype from tapimedia_map,tmedia where tapimedia_map.fmediaid = tmedia.fid and fmaincode>0 group by fmaincode,fmediatype) d on b.fmediacode = d.fmaincode and b.fmediatype = d.fmediatype','left')
                ->where($whereapi) 
                ->find();
            if(!empty($do_ne['trackerid'])){
                $net_trackers[] = [
                    'area'=>$do_ne['area']?$do_ne['area']:'',
                    'entname'=>$do_ne['entname']?$do_ne['entname']:'',
                    'host'=>$do_ne['trackerhost']?$do_ne['trackerhost']:'',
                    'id'=>$do_ne['trackerid']?$do_ne['trackerid']:0
                ];
            }else{
                $net_trackers = [];
            }
            
            $data_ncr['id'] = $do_ne['major_key'];
            $data_ncr['title'] = $do_ne['fadname'];
            $data_ncr['brand'] = $do_ne['fbrand'];
            $data_ncr['type'] = $do_ne['ftype'];
            $data_ncr['shape'] = $do_ne['net_shape'];
            $data_ncr['advertiser_na'] = $do_ne['fownername'];
            $data_ncr['advertiser_id'] = $do_ne['fadowner'];
            $data_ncr['publisher_na'] = $do_ne['fmedianame'];
            $data_ncr['publisher_id'] = $do_ne['fmediaid'];
            $data_ncr['publisher_entname'] = $do_ne['gfname'];
            $data_ncr['publisher_host'] = $do_ne['cfmediacode'];
            $data_ncr['publisher_area'] = $do_ne['media_region_id'];
            $data_ncr['trackers_info'] = json_encode($net_trackers);
            $data_ncr['file_url'] = $do_ne['net_url'];
            $data_ncr['thumb_url_true'] = $do_ne['thumb_url_true'];
            $data_ncr['width'] = $do_ne['net_width'];
            $data_ncr['height'] = $do_ne['net_height'];
            $data_ncr['left'] = $do_ne['net_x'];
            $data_ncr['right'] = $do_ne['net_y'];
            $data_ncr['original_url'] = $do_ne['net_original_url'];
            $data_ncr['target_url'] = $do_ne['net_target_url'];
            $data_ncr['created_date'] = date('Y-m-d H:i:s',$do_ne['net_created_date']/1000);
            $data_ncr['issue_date'] = date('Y-m-d',$do_ne['net_created_date']/1000);
            $data_ncr['size'] = $do_ne['net_size'];
            $data_ncr['platform'] = $do_ne['net_platform'];
            $data_ncr['attribute04'] = $do_ne['net_attribute04'];
            if((int)substr($do_ne['fadclasscode'], 0,4) >=2302 && (int)substr($do_ne['fadclasscode'], 0,4) <=2399){
                $data_ncr['adtype'] = 2301;
            }elseif((int)substr($do_ne['fadclasscode'], 0,4) >=1413 && (int)substr($do_ne['fadclasscode'], 0,4) <=1415){
                $data_ncr['adtype'] = 1409;
            }else{
                $data_ncr['adtype'] = $do_ne['fadclasscode'];
            }
            $data_ncr['adilltype'] = $do_ne['fillegaltypecode']?$do_ne['fillegaltypecode']:0;
            $data_ncr['adillegal'] = $do_ne['fexpressions'];
            $data_ncr['adilllaw'] = $do_ne['fexpressioncodes'];
            $data_ncr['adillcontent'] = $do_ne['fillegalcontent'];
            $data_ncr['advertiser_area'] = $do_ne['fregionid'];
            $data_ncr['tracker_area'] = $do_ne['fregion_id'];
            $data_ncr['modifytime'] = time();
            $data_ncr['fstatus'] = 1;

            $data_ncr['ads_classify'] = '';
            $data_ncr['advertiser'] = $do_ne['net_advertiser'];
            $data_ncr['approval_num'] = $do_ne['fapprovalid'];
            $data_ncr['approval_unit'] = $do_ne['fapprovalunit'];
            $data_ncr['attribute01'] = '';
            $data_ncr['attribute02'] = '';
            $data_ncr['attribute03'] = '';
            $data_ncr['attribute05'] = '';
            $data_ncr['attribute06'] = '';
            $data_ncr['attribute07'] = '';
            $data_ncr['audit'] = 0;
            $data_ncr['body_height'] = $do_ne['net_body_height'];
            $data_ncr['body_width'] = $do_ne['net_body_width'];
            $data_ncr['cookie'] = '';
            $data_ncr['depth'] = 0;
            $data_ncr['domain'] = '';
            $data_ncr['fcode'] = '';
            $data_ncr['fright'] = '';
            $data_ncr['iframes'] = '';
            $data_ncr['illegal_clause'] = '';
            $data_ncr['illegal_content'] = '';
            $data_ncr['ip'] = $do_ne['net_ip'];
            $data_ncr['keywords'] = '';
            $data_ncr['last_seen'] = 0;
            $data_ncr['machine_ip'] = $do_ne['net_machine_ip'];
            $data_ncr['machine_location'] = $do_ne['net_machine_location'];
            $data_ncr['manufacturer'] = '';
            $data_ncr['material'] = $do_ne['material'];
            $data_ncr['md5'] = $do_ne['net_md5'];
            $data_ncr['metas'] = '';
            $data_ncr['modified_date'] = 0;
            $data_ncr['old_file_url'] = $do_ne['net_old_url'];
            $data_ncr['old_page_url'] = '';
            $data_ncr['parent'] = $do_ne['net_parent'];
            $data_ncr['production_license_num'] = '';
            $data_ncr['publisher'] = '';
            $data_ncr['region'] = $do_ne['net_region'];
            $data_ncr['screen'] = '';
            $data_ncr['screen_height'] = $do_ne['net_screen_height'];
            $data_ncr['screen_width'] = $do_ne['net_screen_width'];
            $data_ncr['session_id'] = '';
            $data_ncr['share_url'] = '';
            $data_ncr['shl'] = 'AI';
            $data_ncr['shsj'] = 0;
            $data_ncr['shyj'] = '通过';
            $data_ncr['snapshot'] = $do_ne['net_snapshot'];
            $data_ncr['spokesman'] = $do_ne['fspokesman'];
            $data_ncr['status'] = 0;
            $data_ncr['subject_md5'] = '';
            $data_ncr['tags'] = '';
            $data_ncr['thumb_height'] = $do_ne['net_thumb_height'];
            $data_ncr['thumb_url'] = $do_ne['thumb_url_true'];
            $data_ncr['thumb_width'] = $do_ne['net_thumb_width'];
            $data_ncr['thumbnail'] = '';
            $data_ncr['trackers'] = $do_ne['net_trackers']?$do_ne['net_trackers']:'';
            $data_ncr['url'] = $do_ne['net_url'];
            $data_ncr['url_md5'] = $do_ne['net_url_md5'];
            $data_ncr['views'] = 0;
            $data_ncr['volume'] = 0;

            if(!empty($doapi)){
                if($doapi['fuserid'] == '22638a3131d0f0a7346b178fd29f939c'){//上海用户数据进入条件
                    if(strlen($data_ncr['adtype']) >=4 && $data_ncr['title']!='百度推广'){
                        $data_ncr['ftaskid'] = $doapi['fid'];
                    }
                }else{
                    $data_ncr['ftaskid'] = $doapi['fid'];
                }
                $data_ncr['priority'] = $doapi['priority'];
            }
            $data_ncr['fnettaskid'] = $taskid;
            $data_ncr['monitor'] = $do_ne['dfmediacode']?1:0;
            if(!empty($do_ne['net_snapshot'])){//如果存在广告截图
                $data_ncr['shutpage'] = $do_ne['net_snapshot'];
            }

            $do_ncr = M('tnetissue_customer')->where(['id'=>$do_ne['major_key']])->count();
            if(empty($do_ncr)){
                $do_ncr = M('tnetissue_customer') -> add($data_ncr);
                if(!empty($do_ncr)){
                    $allLogData[] = [
                        'fbizname'=>'互联网客户数据_推送',
                        'ftype'=>1,
                        'furl'=>'http://'.$_SERVER['HTTP_HOST'] . U('Api/ShIssue/push_netsample'),
                        'fmain_id'=>$do_ne['major_key'],
                        'fpostcontent'=>$data_ncr,
                        'freturncontent'=>$res,
                    ];
                }

                //加入所属地域数据
                if(!empty($do_ncr) && !empty($addarea)){
                    M('tnetissue_customer_area') -> addAll($addarea);
                }
            }

            if(!empty($do_ne['net_target_url']) && !empty($do_ncr) && empty($do_ne['net_snapshot'])){
                $post_data = [];
                //推送截图任务
                $postdata['url'] = $do_ne['net_target_url'];
                if($do_ne['net_platform'] == 1){
                    $postdata['devicetype'] = 'pc';
                }elseif($do_ne['net_platform'] == 9){
                     $postdata['devicetype'] = 'android';
                }elseif($do_ne['net_platform'] == 2){
                      $postdata['devicetype'] = strtolower($do_ne['net_attribute04']);
                }else{
                    $postdata['devicetype'] = 'pc';
                }
                $postdata['callbackurl'] = 'http://'.$_SERVER['HTTP_HOST'].'/Api/ShIssue/get_neturlcut';
                $postdata['postid'] = $do_ne['major_key'];
                $this->push_cuttask($postdata,1);
            }else{
                $post_data = [];
                //发送压缩图片请求
                $gourl = 'http://'.$_SERVER['HTTP_HOST'] . U('Api/ShIssue/sampleimg') ;
                $post_data['issueid'] = $do_ne['major_key'];
                $post_data['imgurl'] = $do_ne['net_snapshot'];
                $post_data['fstatus'] = 2;
                $ret = http($gourl,json_encode($post_data),'POST',false,1);
            }
        }
        if(!empty($do_ne_all)){
            $res = ['code'=>0,'msg'=>'成功执行'.count($do_ne_all).'条','data'=>$do_ne_all];
        }else{
            $res = ['code'=>1,'msg'=>'执行失败，可能原因：数据不存在或不需要转入'];
        }

        //日志
        if(!empty($allLogData)){
            A('Api/Core','Model')->addLog($allLogData);
        }

        return rtMsg($res,$retType);
    }

    /**
     * 广告明细的样本数据推送到客户样本数据表
     * by zw
     */
    public function push_netsample2($sampleid = [],$cTaskId = 0){
        session_write_close();
        set_time_limit(0);
        $allLogData = [];
        $finishkey = [];//完成的客户数据
        $waitCutData = [];//等待截图数据
        if(empty($sampleid)){
            return ['code'=>5,'msg'=>'参数有误'];
        }

        $whereapi['fid'] = $cTaskId;
        $whereapi['fdstatus'] = ['in',[0,1]];
        $doapi = M('tapimedialist') 
            ->where($whereapi) 
            ->find();
        if(empty($doapi)){
            return ['code'=>5,'msg'=>'任务不存在或已完成'];
        }
        
        $where_ne['a.major_key'] = ['in',$sampleid];
        $do_ne_all = M('tnetissue')
            ->alias('a')
            ->master(true)
            ->field('a.*,b.fname fownername,c.media_region_id,c.fmediacode cfmediacode,b.fregionid bfregionid,c.fmedianame,g.fname gfname,f.*')
            ->join('tadowner b on a.fadowner = b.fid','left')
            ->join('tmedia c on a.fmediaid = c.fid')
            ->join('tmediaowner g on g.fid = c.fmediaownerid','left')
            ->join('trackers_record e on a.major_key = e.tid','left')
            ->join('tracker f on e.trackerid = f.trackerid','left')
            ->where($where_ne)
            ->group('a.major_key')
            ->select();
        foreach ($do_ne_all as $key => $do_ne) {
            $addarea = [];
            $data_ncr = [];
            if(!empty($do_ne['media_region_id'])){
                $sa = [];
                $sa['fregion'] = (int)$do_ne['media_region_id'];
                $sa['ftype'] = 1;
                $sa['fissueid'] = $do_ne['major_key'];
                $addarea[] = $sa;
            }
            if(!empty($do_ne['fregion_id'])){
                $sa = [];
                $sa['fregion'] = (int)$do_ne['fregion_id'];
                $sa['ftype'] = 2;
                $sa['fissueid'] = $do_ne['major_key'];
                $addarea[] = $sa;
            }

            $do_ne['net_created_date'] = strtotime(date('Y-m-d',$doapi['fissuedate']).' '.date('H:i:s',$do_ne['net_created_date']/1000))*1000;
            $countIssue = M('tnetissue')->where(['net_created_date'=>$do_ne['net_created_date'],'marin_major_key'=>$do_ne['major_key']])->count();
            if(!empty($countIssue)){
                continue;
            }

            unset($do_ne['major_key']);
            $do_ne['fmodifytime'] = date('Y-m-d H:i:s');
            $do_ne['fmodifier'] = 'zhangwei';
            $do_ne['major_key'] = M('tnetissue')->add($do_ne);
            if(!empty($do_ne['trackerid'])){
                $net_trackers[] = [
                    'area'=>$do_ne['area']?$do_ne['area']:'',
                    'entname'=>$do_ne['entname']?$do_ne['entname']:'',
                    'host'=>$do_ne['trackerhost']?$do_ne['trackerhost']:'',
                    'id'=>$do_ne['trackerid']?$do_ne['trackerid']:0
                ];
            }else{
                $net_trackers = [];
            }
            
            $data_ncr['id'] = $do_ne['major_key'];
            $data_ncr['title'] = $do_ne['fadname'];
            $data_ncr['brand'] = $do_ne['fbrand'];
            $data_ncr['type'] = $do_ne['ftype'];
            $data_ncr['shape'] = $do_ne['net_shape'];
            $data_ncr['advertiser_na'] = $do_ne['fownername'];
            $data_ncr['advertiser_id'] = $do_ne['fadowner'];
            $data_ncr['publisher_na'] = $do_ne['fmedianame'];
            $data_ncr['publisher_id'] = $do_ne['fmediaid'];
            $data_ncr['publisher_entname'] = $do_ne['gfname'];
            $data_ncr['publisher_host'] = $do_ne['cfmediacode'];
            $data_ncr['publisher_area'] = $do_ne['media_region_id'];
            $data_ncr['trackers_info'] = json_encode($net_trackers);
            $data_ncr['file_url'] = $do_ne['net_url'];
            $data_ncr['thumb_url_true'] = $do_ne['thumb_url_true'];
            $data_ncr['width'] = $do_ne['net_width'];
            $data_ncr['height'] = $do_ne['net_height'];
            $data_ncr['left'] = $do_ne['net_x'];
            $data_ncr['right'] = $do_ne['net_y'];
            $data_ncr['original_url'] = $do_ne['net_original_url'];
            $data_ncr['target_url'] = $do_ne['net_target_url'];
            $data_ncr['created_date'] = date('Y-m-d H:i:s',$do_ne['net_created_date']/1000);
            $data_ncr['issue_date'] = date('Y-m-d',$do_ne['net_created_date']/1000);
            $data_ncr['size'] = $do_ne['net_size'];
            $data_ncr['platform'] = $do_ne['net_platform'];
            $data_ncr['attribute04'] = $do_ne['net_attribute04'];
            if((int)substr($do_ne['fadclasscode'], 0,4) >=2302 && (int)substr($do_ne['fadclasscode'], 0,4) <=2399){
                $data_ncr['adtype'] = 2301;
            }elseif((int)substr($do_ne['fadclasscode'], 0,4) >=1413 && (int)substr($do_ne['fadclasscode'], 0,4) <=1415){
                $data_ncr['adtype'] = 1409;
            }else{
                $data_ncr['adtype'] = $do_ne['fadclasscode'];
            }
            $data_ncr['adilltype'] = $do_ne['fillegaltypecode']?$do_ne['fillegaltypecode']:0;
            $data_ncr['adillegal'] = $do_ne['fexpressions'];
            $data_ncr['adilllaw'] = $do_ne['fexpressioncodes'];
            $data_ncr['adillcontent'] = $do_ne['fillegalcontent'];
            $data_ncr['advertiser_area'] = $do_ne['fregionid'];
            $data_ncr['tracker_area'] = $do_ne['fregion_id'];
            $data_ncr['modifytime'] = time();

            $data_ncr['ads_classify'] = '';
            $data_ncr['advertiser'] = $do_ne['net_advertiser'];
            $data_ncr['approval_num'] = $do_ne['fapprovalid'];
            $data_ncr['approval_unit'] = $do_ne['fapprovalunit'];
            $data_ncr['attribute01'] = '';
            $data_ncr['attribute02'] = '';
            $data_ncr['attribute03'] = '';
            $data_ncr['attribute05'] = '';
            $data_ncr['attribute06'] = '';
            $data_ncr['attribute07'] = '';
            $data_ncr['audit'] = 0;
            $data_ncr['body_height'] = $do_ne['net_body_height'];
            $data_ncr['body_width'] = $do_ne['net_body_width'];
            $data_ncr['cookie'] = '';
            $data_ncr['depth'] = 0;
            $data_ncr['domain'] = '';
            $data_ncr['fcode'] = '';
            $data_ncr['fright'] = '';
            $data_ncr['iframes'] = '';
            $data_ncr['illegal_clause'] = '';
            $data_ncr['illegal_content'] = '';
            $data_ncr['ip'] = $do_ne['net_ip'];
            $data_ncr['keywords'] = '';
            $data_ncr['last_seen'] = 0;
            $data_ncr['machine_ip'] = $do_ne['net_machine_ip'];
            $data_ncr['machine_location'] = $do_ne['net_machine_location'];
            $data_ncr['manufacturer'] = '';
            $data_ncr['material'] = $do_ne['material'];
            $data_ncr['md5'] = $do_ne['net_md5'];
            $data_ncr['metas'] = '';
            $data_ncr['modified_date'] = 0;
            $data_ncr['old_file_url'] = $do_ne['net_old_url'];
            $data_ncr['old_page_url'] = '';
            $data_ncr['parent'] = $do_ne['net_parent'];
            $data_ncr['production_license_num'] = '';
            $data_ncr['publisher'] = '';
            $data_ncr['region'] = $do_ne['net_region'];
            $data_ncr['screen'] = '';
            $data_ncr['screen_height'] = $do_ne['net_screen_height'];
            $data_ncr['screen_width'] = $do_ne['net_screen_width'];
            $data_ncr['session_id'] = '';
            $data_ncr['share_url'] = '';
            $data_ncr['shl'] = 'AI';
            $data_ncr['shsj'] = 0;
            $data_ncr['shyj'] = '通过';
            $data_ncr['snapshot'] = $do_ne['net_snapshot'];
            $data_ncr['spokesman'] = $do_ne['fspokesman'];
            $data_ncr['status'] = 0;
            $data_ncr['subject_md5'] = '';
            $data_ncr['tags'] = '';
            $data_ncr['thumb_height'] = $do_ne['net_thumb_height'];
            $data_ncr['thumb_url'] = $do_ne['thumb_url_true'];
            $data_ncr['thumb_width'] = $do_ne['net_thumb_width'];
            $data_ncr['thumbnail'] = '';
            $data_ncr['trackers'] = $do_ne['net_trackers']?$do_ne['net_trackers']:'';
            $data_ncr['url'] = $do_ne['net_url'];
            $data_ncr['url_md5'] = $do_ne['net_url_md5'];
            $data_ncr['views'] = 0;
            $data_ncr['volume'] = 0;
            $data_ncr['ftaskid'] = $cTaskId;
            $data_ncr['priority'] = $doapi['priority'];
            $data_ncr['fnettaskid'] = $taskid;
            $data_ncr['monitor'] = $do_ne['dfmediacode']?1:0;
            if(!empty($do_ne['net_snapshot'])){//如果存在广告截图
                $data_ncr['fstatus'] = 2;
                $data_ncr['shutpage'] = $do_ne['net_snapshot'];
                $data_ncr['shutpagesize'] = 0;
                $data_ncr['shutpage_thumb'] = $do_ne['net_snapshot'];
                $data_ncr['shutpage_thumb_size'] = 0;
            }else{
               $data_ncr['fstatus'] = 1; 
            }

            $do_ncr = M('tnetissue_customer') -> add($data_ncr);
            if(!empty($do_ncr)){
                $allLogData[] = [
                    'fbizname'=>'互联网客户数据_推送',
                    'ftype'=>1,
                    'furl'=>'http://'.$_SERVER['HTTP_HOST'] . U('Api/ShIssue/push_netsample'),
                    'fmain_id'=>$do_ne['major_key'],
                    'fpostcontent'=>$data_ncr,
                    'freturncontent'=>$res,
                ];
            }
            //加入所属地域数据
            if(!empty($do_ncr) && !empty($addarea)){
                M('tnetissue_customer_area') -> addAll($addarea);
            }
           
            if(!empty($do_ncr)){
                $finishkey[] = $do_ne['major_key'];
            }

            if(empty($do_ne['net_snapshot'])){
                $post_data = [];
                //推送截图任务
                $postdata['url'] = $do_ne['net_target_url'];
                if($do_ne['net_platform'] == 1){
                    $postdata['devicetype'] = 'pc';
                }elseif($do_ne['net_platform'] == 9){
                     $postdata['devicetype'] = 'android';
                }elseif($do_ne['net_platform'] == 2){
                      $postdata['devicetype'] = strtolower($do_ne['net_attribute04']);
                }else{
                    $postdata['devicetype'] = 'pc';
                }
                $postdata['callbackurl'] = 'http://'.$_SERVER['HTTP_HOST'].'/Api/ShIssue/get_neturlcut';
                $postdata['postid'] = $do_ne['major_key'];
                $waitCutData[] = $postdata;
            }
        }
        if(!empty($do_ne_all)){
            $res = ['code'=>0,'msg'=>'推送成功','data'=>count($finishkey)];
        }else{
            $res = ['code'=>1,'msg'=>'推送失败'];
        }

        //截图
        if(!empty($waitCutData)){
            $this->push_cuttask($waitCutData,1);
        }
     
        //日志
        if(!empty($allLogData)){
            A('Api/Core','Model')->addLog($allLogData);
        }
        return $res;
    }

    /*
    * 图片压缩
    * by zw
    */
    public function sampleimg(){
        $data = (array)json_decode(file_get_contents('php://input'));
        $issueid = $data['issueid'];//样本ID
        $imgurl = $data['imgurl'];//图片地址

        $savepath = 'Public/Word/';//存储路径
        $imgdata = getFile($imgurl,$savepath);//下载文件
        if(!empty($imgdata)){
            $resizeimg = resize_image($imgdata['filePath'],$savepath,0,0);
            $data_ncr['shutpagesize'] = $imgdata['fileSize'];
            if(!empty($resizeimg)){
                $ret = A('Common/AliyunOss','Model')->file_up('threeyears/'.$resizeimg['fileName'],$resizeimg['filePath'],[]);//上传云
                unlink($resizeimg['filePath']);//删除文件
                $data_ncr['shutpage_thumb'] = $ret['url'];
                $data_ncr['shutpage_thumb_size'] = $resizeimg['fileSize'];
            }else{
                $data_ncr['shutpage_thumb'] = $imgurl;
                $data_ncr['shutpage_thumb_size'] = $imgdata['fileSize'];
            }
            unlink($imgdata['filePath']);//删除文件
        }else{
            $data_ncr['shutpagesize'] = 0;
            $data_ncr['shutpage_thumb'] = $imgurl;
            $data_ncr['shutpage_thumb_size'] = 0;
        }
        if(!empty($data['fstatus'])){
            $data_ncr['fstatus'] = $data['fstatus'];
        }
        $update_ncr = M('tnetissue_customer') -> where(['id'=>$issueid,'fstatus'=>['in',[0,1,4,5]]]) ->save($data_ncr);
        if(!empty($update_ncr)){
            $this->ajaxReturn(['code'=>0,'msg'=>'压缩成功','data'=>$data_ncr]);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'压缩失败']);
        }
    }

    /*
    * 重新推送未完成的截图任务
    * by zw
    */
    public function loop_cuttask(){
        session_write_close();
        set_time_limit(0);
        $error = [];
        $finish = [];
        $data = M('tnetissue_customer')->field('id,target_url,platform,attribute04')->where(['ftaskid'=>['gt',0],'fstatus'=>1])->select();
        foreach ($data as $key => $value) {
            $postdata['url'] = $value['target_url'];
            if($value['platform'] == 1){
                $postdata['devicetype'] = 'pc';
            }elseif($value['platform'] == 9){
                 $postdata['devicetype'] = 'android';
            }elseif($value['platform'] == 2){
                  $postdata['devicetype'] = strtolower($value['attribute04']);
            }else{
                $postdata['devicetype'] = 'pc';
            }
            $postdata['callbackurl'] = 'http://'.$_SERVER['HTTP_HOST'].'/Api/ShIssue/get_neturlcut';
            $postdata['postid'] = $value['id'];

            $ret = $this->push_cuttask($postdata,1,99);
            if(empty($ret)){
                $error[] = $value['id'];
            }else{
                $finish[] = $value['id'];
            }
        }
        var_dump('失败：');
        dump($error);
        var_dump('成功：');
        dump($finish);
    }

    /*
    * 推送网页截图任务
    * by zw
    */
    public function push_cuttask($postdata = [],$waittime = 1,$priority = 0){
        session_write_close();
        set_time_limit(0);
        if(empty($postdata)){
            return false;
        }

        if(empty($postdata[0])){
            $cldata[0] = $postdata;
        }else{
            $cldata = $postdata;
        }

        foreach ($cldata as $key => $value) {
            $push_task = http('http://47.102.102.183:8088/v1/snapshot/sendjob',$value,'POST',false,$waittime);//新地址
            if(!empty($priority)){
                $postdata = [];
                $postdata['postid'] = $value['postid'];
                $postdata['priority'] = 99;
                $this->push_cutpriority($postdata);
            }

            //日志
            $logData['fbizname'] = '互联网截图_任务推送';
            $logData['ftype'] = 1;
            $logData['furl'] = 'http://47.102.102.183:8088/v1/snapshot/sendjob';
            $logData['fmain_id'] = $value['postid'];
            $logData['fpostcontent'] = $value;
            $logData['freturncontent'] = $push_task;
            $allLogData[] = $logData;
        }
        A('Api/Core','Model')->addLog($allLogData);
        return $push_task;
    }

    /*
    * 推送网页截图优先级
    * by zw
    */
    public function push_cutpriority($postdata = []){
        $push_data = http('http://47.102.102.183:8088/v1/snapshot/updatejob',$postdata,'POST',false,10);

        //日志
        $logData['fbizname'] = '互联网截图_截图优先级';
        $logData['ftype'] = 1;
        $logData['furl'] = 'http://47.102.102.183:8088/v1/snapshot/updatejob';
        $logData['fmain_id'] = $postdata['postid'];
        $logData['fpostcontent'] = $postdata;
        $logData['freturncontent'] = $push_data;
        A('Api/Core','Model')->addLog($logData);
        return $push_data;
    }

    /*
    * 获取互联网数据的截图回调
    * by zw
    */
    public function get_neturlcut(){
        $getData = $this->oParam;
        $postid = $getData['postid'];
        $capurl = $getData['capurl'];
        $caplength = $getData['caplength'];
        $capjpgurl = $getData['capjpgurl'];
        $capjpglength = $getData['capjpglength'];
        $view_ncr = M('tnetissue_customer')->field('fstatus')->where(['id'=>$postid])->find();

        //日志
        $logData['fbizname'] = '互联网截图_截图回调';
        $logData['ftype'] = 2;
        $logData['furl'] = 'http://'.$_SERVER['HTTP_HOST'] . U('Api/ShIssue/get_neturlcut');
        $logData['fmain_id'] = $postid;
        $logData['fpostcontent'] = json_decode(file_get_contents('php://input'),true);
        $logData['freturncontent'] = $view_ncr;
        A('Api/Core','Model')->addLog($logData);

        if(empty($view_ncr)){
            $this->ajaxReturn(['code'=>1,'description'=>'反馈获取数据失败，数据不存在','result'=>false,'postid'=>$postid]);
        }
        $where_ncr['id'] = $postid;
        $where_ncr['shutpage_thumb'] = '';
        if(empty($capurl) && empty($capjpgurl)){
            $data_ncr['fstatus'] = 2;
            $data_ncr['modifytime'] = time();
        }else{
            $data_ncr['shutpage'] = $capurl?$capurl:$capjpgurl;
            $data_ncr['shutpagesize'] = $caplength;
            $data_ncr['shutpage_thumb'] = $capjpgurl?$capjpgurl:$capurl;
            $data_ncr['shutpage_thumb_size'] = $capjpglength;
            if($view_ncr['fstatus'] == 1){
                $data_ncr['fstatus'] = 2;
            }
            $data_ncr['modifytime'] = time();
        }
        $do_ncr = M('tnetissue_customer') -> where($where_ncr) ->save($data_ncr);
        if(!empty($do_ncr)){
            $this->ajaxReturn(['code'=>0,'description'=>'反馈获取数据成功','result'=>true]);
        }else{
            $this->ajaxReturn(['code'=>1,'description'=>'反馈获取数据失败','result'=>false]);
        }
    }

    /*
    * 新广告剪辑任务轮循
    * by zw
    */
    public function get_newtadcuttask(){
        $where_tt['a.fdstatus'] = 11;
        $where_tt['b.fmediatype'] = ['in',[1,2]];
        $do_tt = M('tapimedialist')
            ->alias('a')
            ->field('a.fissuedate,b.fmediaid,a.fid')
            ->join('tapimedia_map b on a.fmediaid = b.fid')
            ->where($where_tt)
            ->order('a.fmodifytime asc,a.fissuedate asc')
            ->find();
        if(!empty($do_tt)){
            $where_ise['a.fissuedate'] = date('Y-m-d',$do_tt['fissuedate']);
            $where_ise['a.fmediaid'] = $do_tt['fmediaid'];
            $where_ise['a.fcutstatus'] = ['lt',0];
            $where_ise['b.fstatus'] = ['in',[2,-1]];
            $where_ise['_string'] = 'a.fis_new = 1';
            $do_ise = M('sh_issue')
                ->alias('a')
                ->field('a.fid,b.fstatus,b.fcut_time,b.furl,a.sam_source_path,b.fid cid')
                ->join('tad_cut b on a.fid = b.fuuid and fuser_id = "new_tad_cut_task"')
                ->where($where_ise)
                ->select();
            $finishcut = [];
            foreach ($do_ise as $key => $value) {
                $finishcut[] = $value['cid'];
                $where_update['fid'] = $value['fid'];
                $updatedata['fmodifier'] = "new_tad_cut_task";
                $updatedata['fmodifytime'] = date('Y-m-d H:i:s');

                if($value['fstatus'] == 2){
                    $updatedata['fcutstatus'] = 0;
                    $updatedata['fcuttime'] = date('Y-m-d H:i:s');
                    $updatedata['fcuturl'] = $value['furl'];
                }else{
                    $updatedata['fcutstatus'] = 1;
                    $updatedata['fcuttime'] = date('Y-m-d H:i:s');
                    $updatedata['fcuturl'] = $value['sam_source_path'];
                }
                M('sh_issue')->where($where_update)->save($updatedata);
            }
            if(!empty($finishcut)){
                M()->execute('update tad_cut set fstatus = 3 where fid in('.implode(',', $finishcut).')');
            }

            //更新任务的更新时间
            if(!empty($do_ise)){
                M('tapimedialist')->where(['fid'=>$do_tt['fid']])->save(['fmodifier'=>'new_tad_cut_task','fmodifytime'=>date('Y-m-d H:i:s')]);//
            }

            //判断如果完成了所有剪辑任务，就将任务状态改成已完成待接收
            $where_ise2['fissuedate'] = date('Y-m-d',$do_tt['fissuedate']);
            $where_ise2['fmediaid'] = $do_tt['fmediaid'];
            $where_ise2['fcutstatus'] = ['in',[-2,-1]];
            $where_ise2['_string'] = 'fis_new = 1';
            $do_count2 = M('sh_issue')->where($where_ise2)->count();
            if(empty($do_count2)){
                $where_amp['fmediaid'] = $do_tt['fmediaid'];
                $tt_mediaid = M('tapimedia_map')->where($where_amp)->getField('fid');
                if(!empty($tt_mediaid)){
                    $where_tt2['fid'] = $do_tt['fid'];
                    $data_tt2['fdstatus'] = 2;
                    $do_tt2 = M('tapimedialist') -> where($where_tt2) ->save($data_tt2);
                }
            }

            $this->ajaxReturn(['code'=>0,'msg'=>'更新成功']);
        }else{
            $this->ajaxReturn(['code'=>0,'msg'=>'暂无任务']);
        }

    }

    /*
    * 客户互联网样本对一直未成功返回截图信息的进行轮循检查，并重新推送请求
    * by zw
    */
    public function loop_cutimgtask(){
        $gourl = 'http://'.$_SERVER['HTTP_HOST'] . U('Api/ShIssue/sampleimg') ;
        $do_ncr = M('tnetissue_customer')->master(true)->field('id,fpushcutcount')->where(['fstatus' => 1,'ftaskid' => ['gt',0],'modifytime'=>['lt',(time()-7200)]])->order('modifytime asc')->limit(10)->select();
        foreach ($do_ncr as $key => $value) {
            if((int)$value['fpushcutcount']<=10){
                $where_ne['major_key'] = $value['id'];
                $do_ne = M('tnetissue')
                    ->alias('a')
                    ->field('net_target_url,net_snapshot,net_platform,net_attribute04')
                    ->where($where_ne)
                    ->find();
                if(!empty($do_ne)){
                    if(!empty($do_ne['net_target_url']) && empty($do_ne['net_snapshot'])){
                        $postdata['url'] = $do_ne['net_target_url'];
                        if($do_ne['net_platform'] == 1){
                            $postdata['devicetype'] = 'pc';
                        }elseif($do_ne['net_platform'] == 9){
                             $postdata['devicetype'] = 'android';
                        }elseif($do_ne['net_platform'] == 2){
                              $postdata['devicetype'] = strtolower($do_ne['net_attribute04']);
                        }else{
                            $postdata['devicetype'] = 'pc';
                        }
                        $postdata['callbackurl'] = 'http://'.$_SERVER['HTTP_HOST'].'/Api/ShIssue/get_neturlcut';
                        $postdata['postid'] = $value['id'];
                        $cuttask_status = S('cuttask');
                        if(empty($cuttask_status)){
                            $returndata = $this->push_cuttask($postdata,0,99);
                            if(empty($returndata)){
                                S('cuttask',date('Y-m-d H:i:s'),14400);//两小时内不作提醒
                                $smsphone = [13600522308];//提醒人员，王凯
                                $token = '230ed1d06ef484b5acde1fe84a267d11cded956a95e3a69f91f9c43be0d4130e';
                                push_ddtask('友情提示',"> 友情提示：\n\n互联网广告截图任务推送失败，请检查服务是否正常",$smsphone,$token,'markdown');
                            }
                        }
                        $value['fpushcutcount'] += 1;
                    }else{
                        //发送压缩图片请求
                        $post_data['issueid'] = $value['id'];
                        $post_data['imgurl'] = $do_ne['net_snapshot'];
                        $ret = http($gourl,json_encode($post_data),'POST',false,1);
                        $value['fpushcutcount'] += 1;
                    }
                }
            }

            $save_ncr = [];
            if((int)$do_ncr[$key]['fpushcutcount']<(int)$value['fpushcutcount']){
                $save_ncr['modifytime'] = time();
                $save_ncr['fpushcutcount'] = $value['fpushcutcount'];
            }else{
                $save_ncr['fstatus'] = 2;
                $save_ncr['modifytime'] = time();
            }
            M('tnetissue_customer')->where(['id'=>$value['id']])->save($save_ncr);
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'已执行','count'=>count($do_ncr)]);
    }

    /**
    * by zw
    * 获取互联网媒体爬取数据原文
    * @Param    Mixed variable
    * @Return   void 返回的数据
    */
    public function GetNetADSpiderInfo(){
        set_time_limit(0);
        $data = json_decode(file_get_contents('php://input'),true);
        $do_ne = M('tnetissue')
            ->field('major_key,fadname,net_target_url,net_id')
            ->where(['major_key'=>['in',$data['fid']]])
            ->select();
        $adddata = [];
        foreach ($do_ne as $key => $value) {
            $data = [];
            $url = 'http://iad.adlife.com.cn/index/getAttribute?id='.$value['net_id'];
            $data = $this->http_get($url);
            $data = json_decode($data,true);
            if(!empty($data['data'])){
                file_put_contents('LOG/major_key.txt',$value['major_key']."\r\n",FILE_APPEND);
                file_put_contents('LOG/fadname.txt',$value['fadname']."\r\n",FILE_APPEND);
                file_put_contents('LOG/net_target_url.txt',$value['net_target_url']."\r\n",FILE_APPEND);
                file_put_contents('LOG/'.$value['major_key'].'.txt',json_encode($data));
            }else{
                $adddata[] = $value['major_key'];
            }
        }
        echo '生成失败：';
        dump($adddata);

    }

    /**
     * 模拟get请求
     * by zw
     */
    public function http_get($url){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

        curl_setopt($ch, CURLOPT_URL, $url);
        //参数为1表示传输数据，为0表示直接输出显示。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //若给定url自动跳转到新的url,有了下面参数可自动获取新url内容：302跳转
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        //设置cURL允许执行的最长秒数。
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        //https请求必须
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //参数为0表示不带头文件，为1表示带头文件
        curl_setopt($ch, CURLOPT_HEADER,0);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    /*
    * 媒体数据检查完成后，通过该接口接收完成状态
    * zw
    */
    public function push_finish_backtask(){
        $mediaid = I('mediaid');
        $issuedate = I('issuedate');

        $do_amt = M('tapimediatask')
            ->alias('a')
            ->field('a.fid,a.fmediaid,b.fmedianame,a.fcustomer')
            ->join('tmedia b on a.fmediaid = b.fid')
            ->where(['fmediaid'=>$mediaid,'fissuedate'=>$issuedate,'fdstatus'=>0])
            ->select();
        foreach ($do_amt as $key => $value) {
            if($value['fcustomer'] == 310000){
                $where_aml['a.fdstatus'] = 12;
                $where_aml['b.fmediaid'] = $mediaid;
                $where_aml['a.fissuedate'] = strtotime(date('Y-m-d',strtotime($issuedate)));
                $do_aml = M('tapimedialist') 
                    ->field('a.fid,c.fmedianame,a.fissuedate,a.fmodifytime,b.fmediacodename')
                    ->alias('a')
                    ->join('tapimedia_map b on a.fmediaid = b.fid')
                    ->join('tmedia c on b.fmediaid = c.fid')
                    ->where($where_aml) 
                    ->find();
                if(!empty($do_aml)){
                    M('tapimedialist')->where(['fid'=>$do_aml['fid']])->save(['fdstatus'=>13,'fmodifytime'=>date('Y-m-d H:i:s')]);
                    // 钉钉通知
                    $title = '退回计划处理完成提醒';
                    $content = "> 友情提示：\n\n上海退回计划已完成剪辑处理，请尽快进行后续操作（录入、审核），完成数据提供，注意将任务优先级调至最高。\n\n媒体名称：".$do_aml['fmedianame']."（".$do_aml['fmediacodename']."） \n\n计划日期：".date('Y-m-d',$do_aml['fissuedate'])."\n\n退回时间：".$do_aml['fmodifytime']."\n\n";
                    $smsphone = [13858090079,15867123863];//提醒人员，沈露
                    $token = '3cc83737c35805d21c51028067c707418c64a8a257b7ed30bccef3ef5b295efc';
                    push_ddtask($title,$content,$smsphone,$token,'markdown');
                }
            }elseif($value['fcustomer'] == 100000){
                // 钉钉通知
                $title = '国家局第三方数据复核任务相关媒体剪辑完成提醒';
                $content = "> 友情提示：\n\n国家局第三方数据复核相关媒体已完成剪辑处理，请尽快进行后续操作（录入、审核），完成数据提供，注意将任务优先级调至最高。\n\n媒体名称：".$value['fmedianame']."（".$value['fmediaid']."）";
                $smsphone = [13799348980];//提醒人员，王裕厚
                $token = '4916d1ab6a29a2ac4a755e8e7113d477c1e687736d08d0977f324f7c5a187e96';
                push_ddtask($title,$content,$smsphone,$token,'markdown');
            }

            //更改内部任务状态
            M('tapimediatask')->where(['fid'=>$value['fid']])->save(['fdstatus'=>1,'fmodifier'=>0,'fmodifytime'=>date('Y-m-d H:i:s')]);
        }
        
        $this->ajaxReturn(['code'=>0,'msg'=>'推送完成']);
    }

    /*
    * 媒体数据需要重新检查，推送重新检查的媒体信息和理由给数据剪辑方
    * mediaid 媒体ID，reason错误原因，issuedate错误日期(如2019-05-01)
    * zw
    */
    public function push_error_backtask($mediaid,$reason,$issuedate,$fcustomer){
        if($fcustomer == 310000){
            $user = 'shanghai';
        }elseif($fcustomer == 100000){
            $user = 'guojia';
        }else{
            $user = '';
        }
        $data['count'] = -1;
        $data['submit_user'] = $user;
        $data['reason'] = $reason;
        $sttime = strtotime(date('Y-m-d',strtotime($issuedate)));
        $endtime = $sttime+86399;//高博孙涛沟通，由于对方机制，若这里传参的(结束时间-开始时间)>83000秒时，对方接收后将强制修改结束时间为72000秒(20小时) 2019-12-16
        $res = http('http://47.96.182.117:8002/edit_error_submit/'.$mediaid.'/'.$sttime.'/'.$endtime.'/',$data,'post',false,10);
        if(!empty($res)){
            //添加内部任务
            $data_amt['fmediaid'] = $mediaid;
            $data_amt['fissuedate'] = date('Y-m-d',strtotime($issuedate));
            $data_amt['fdstatus'] = 0;
            $data_amt['fcontent'] = $reason;
            $data_amt['fcreatetime'] = date('Y-m-d H:i:s');
            $data_amt['fmodifytime'] = date('Y-m-d H:i:s');
            $data_amt['fmodifier'] = 0;
            $data_amt['fcustomer'] = $fcustomer;
            M('tapimediatask')->add($data_amt);
            return true;
        }else{
            return false;
        }
    }

    /*
    * 轮循检索媒体剪辑任务是否完成（10分钟/次）
    * by zw
    */
    public function retrieval_backtask(){
        $where_amt['a.fdstatus'] = ['in',[0,1]];
        $where_amt['_string'] = 'unix_timestamp(a.fmodifytime)<'.(time()-300); 

        $do_amt = M('tapimediatask') 
            ->field('a.*')
            ->alias('a')
            ->where($where_amt) 
            ->select();
        foreach ($do_amt as $key => $value) {
            if($value['fcustomer'] == 310000){//上海用户任务检测
                if($value['fdstatus'] == 0 && (time()-strtotime($value['fmodifytime']))>7200){//超时2小时无限提醒
                    $where_aml['a.fdstatus'] = 12;
                    $where_aml['b.fmediaid'] = $value['fmediaid'];
                    $where_aml['a.fissuedate'] = strtotime($value['fissuedate']);
                    $do_aml = M('tapimedialist') 
                        ->field('b.fmediaid,c.media_region_id,b.fmediatype,a.fissuedate,a.fdstatus,a.fmodifytime,c.fmedianame,b.fmediacodename')
                        ->alias('a')
                        ->join('tapimedia_map b on a.fmediaid = b.fid')
                        ->join('tmedia c on b.fmediaid = c.fid')
                        ->where($where_aml) 
                        ->find();
                    if(!empty($do_aml)){
                        // 钉钉通知
                        $title = '退回计划处理超时';
                        $content = "> 友情提示：\n\n上海退回计划处理已超过2小时，请尽快处理：\n\n".$do_aml['fmedianame']."（".$do_aml['fmediacodename']."） ".$value['fissuedate']."\n\n退回时间：".$do_aml['fmodifytime']."\n\n";

                        $smsphone = [13858090079];//提醒人员，沈露
                        $token = '3cc83737c35805d21c51028067c707418c64a8a257b7ed30bccef3ef5b295efc';
                        push_ddtask($title,$content,$smsphone,$token,'markdown');
                    }
                }elseif($value['fdstatus'] == 1 && (time()-strtotime($value['fmodifytime']))>300){//剪辑完成判断众包任务是否还有，没有的话将状态改为任务完成
                    $where_aml['a.fdstatus'] = 13;
                    $where_aml['b.fmediaid'] = $value['fmediaid'];
                    $where_aml['a.fissuedate'] = strtotime($value['fissuedate']);
                    $do_aml = M('tapimedialist') 
                        ->field('a.fid,b.fmediaid,c.media_region_id,b.fmediatype,a.fissuedate,a.fmodifytime')
                        ->alias('a')
                        ->join('tapimedia_map b on a.fmediaid = b.fid')
                        ->join('tmedia c on b.fmediaid = c.fid')
                        ->where($where_aml) 
                        ->find();
                    if(!empty($do_aml)){
                        $provinceId = substr($do_aml['media_region_id'],0,2);
                        if($do_aml['fmediatype'] == 1){
                            $issuetable = 'ttvissue_' . date('Ym',$do_aml['fissuedate']) . '_' . $provinceId;//获取表名
                            $sampletable = 'ttvsample';
                        }elseif($do_aml['fmediatype'] == 2){
                            $issuetable = 'tbcissue_' . date('Ym',$do_aml['fissuedate']) . '_' . $provinceId;
                            $sampletable = 'tbcsample';
                        }else{
                            continue;
                        }
                        $where_ie['a.fissuedate'] = $do_aml['fissuedate'];
                        $where_ie['a.fmediaid'] = $do_aml['fmediaid'];
                        $where_ie['b.fadid'] = 0;
                        $do_ie = M($issuetable)
                            ->alias('a')
                            ->join($sampletable.' b on a.fsampleid = b.fid')
                            ->where($where_ie)
                            ->count();
                        $do_task = M('task_input')
                            ->where(['media_id'=>$do_aml['fmediaid'],'issue_date'=>date('Y-m-d',$do_aml['fissuedate']),'task_state'=>['neq',2]])
                            ->count();
                        if(empty($do_ie) && empty($do_task)){
                            M('tapimedialist')->where(['fid'=>$do_aml['fid']])->save(['fdstatus'=>0,'fmodifytime'=>date('Y-m-d H:i:s'),'fstatus'=>0]);
                            M('tapimedialist_backlog')->where(['fapimedialistid'=>$do_aml['fid'],'fstate'=>['in',[0,1]]])->save(['fstate'=>2]);
                            M('tapimediatask')->where(['fid'=>$value['fid']])->save(['fdstatus'=>2,'fmodifytime'=>date('Y-m-d H:i:s'),'fstatus'=>0]);
                        }
                    }
                }
            }elseif($value['fcustomer'] == 100000){//国家局任务状态检测
                $do_ma = M('tmedia')->where(['fid'=>$value['fmediaid']])->find();
                if($value['fdstatus'] == 0 && (time()-strtotime($value['fmodifytime']))>7200){//超时2小时无限提醒
                    // 钉钉通知
                    $title = '国家局第三方数据复核任务相关媒体剪辑处理超时';
                    $content = "> 友情提示：\n\n国家局第三方数据复核剪辑处理已超过2小时，请尽快处理：\n\n".$do_ma['fmedianame']."（".$do_ma['fid']."）\n\n复核日期：".$value['fissuedate']."\n\n提交时间：".$value['fmodifytime']."\n\n";
                    $smsphone = [17611266159];//提醒人员，马国琪
                    $token = '3cc83737c35805d21c51028067c707418c64a8a257b7ed30bccef3ef5b295efc';
                    push_ddtask($title,$content,$smsphone,$token,'markdown');
                }elseif($value['fdstatus'] == 1 && (time()-strtotime($value['fmodifytime']))>300){//剪辑完成判断众包任务是否还有，没有的话将状态改为任务完成
                    $provinceId = substr($do_ma['media_region_id'],0,2);
                    if(substr($do_ma['fmediaclassid'], 0,2) == '01'){
                        $issuetable = 'ttvissue_' . date('Ym',strtotime($value['fissuedate'])) . '_' . $provinceId;//获取表名
                        $sampletable = 'ttvsample';
                    }elseif(substr($do_ma['fmediaclassid'], 0,2) == '02'){
                        $issuetable = 'tbcissue_' . date('Ym',strtotime($value['fissuedate'])) . '_' . $provinceId;
                        $sampletable = 'tbcsample';
                    }else{
                        continue;
                    }

                    $where_ie['a.fissuedate'] = $value['fissuedate'];
                    $where_ie['a.fmediaid'] = $value['fmediaid'];
                    $where_ie['b.fadid'] = 0;
                    $do_ie = M($issuetable)
                        ->alias('a')
                        ->join($sampletable.' b on a.fsampleid = b.fid')
                        ->where($where_ie)
                        ->count();
                    if(empty($do_ie)){
                        M('tapimediatask')->where(['fid'=>$value['fid']])->save(['fdstatus'=>2,'fmodifytime'=>date('Y-m-d H:i:s'),'fstatus'=>0]);
                    }

                }
            }
            
        }
            
        $this->ajaxReturn(['code'=>0,'msg'=>'更新完成']);

    }

    /*
    * 轮循推送截图文本识别任务（60秒/次）
    * by zw
    */
    public function push_cutpic_task(){
        $doNcr = M('tnetissue_customer')->field('id,file_url,target_url,shutpage')->where(['fstatus'=>14,'ftaskid'=>['gt',0]])->select();
        $addData = [];
        $db_cut = M('material','','DB_CONFIG_CUT');
        foreach ($doNcr as $key => $value) {
            $countNcr = $db_cut->where(['id'=>$value['id']])->count();
            if(empty($countNcr)){
                $addNcr = [
                    'id'=>$value['id'],
                    'ad_pic'=>$value['file_url'],
                    'landing_page_url'=>$value['target_url'],
                    'landing_page_pic'=>$value['shutpage'],
                    'priority'=>99,
                    'status'=>0,
                    'create_time'=>date('Y-m-d H:i:s'),
                ];
                $addData[] = $addNcr;
            }
        }

        if(!empty($addData)){
            $finishCount = $db_cut->addAll($addData);
        }

        $this->ajaxReturn(['finish'=>$finishCount?$finishCount:0,'data'=>$addData]);
    }

    /*
    * 轮循获取截图文本识别内容，更新计划的完成识别完成状态（5秒/次）
    * by zw
    */
    public function lunxun_cutpic_text(){
        $taskid = I('taskid');//任务ID
        
        $whereNcr['fstatus'] = 14;
        if(!empty($taskid)){
            $whereNcr['ftaskid'] = $taskid;
        }
        $ids = M('tnetissue_customer')->where($whereNcr) ->order('shutpage_modifytime asc')->limit(10)->getField('id',true);
        if(empty($ids)){
            $this->ajaxReturn(['code'=>1,'msg'=>'no task']);
        }
        $db_cut = M('material','','DB_CONFIG_CUT');
        $finishData = [];
        foreach ($ids as $value) {
            $do_cut = $db_cut->field('id,landing_page_result,status')->where(['status'=>['in',[-1,2,3,9]],'id'=>$value])->order('update_time asc')->find();
            if(empty($do_cut)){
                continue;
            }
            if($do_cut['status'] == 2 || $do_cut['status'] == 9){
                $save_ncr['shutpage_content'] = $do_cut['landing_page_result'];
            }elseif($do_cut['status'] == 3){
                $save_ncr['shutpage_content'] = '';
            }else{
                $save_ncr['shutpage_content'] = '无法识别';
            }
            $save_ncr['fstatus'] = 6;
            $save_ncr['shutpage_modifytime'] = time();
            $do_ncr = M('tnetissue_customer')->where(['id'=>$do_cut['id']])->save($save_ncr);
            if(!empty($do_ncr)){
                $finishData[] = $do_cut;
                $db_cut->where(['status'=>['in',[2,3]],'id'=>$do_cut['id']])->save(['status'=>9,'update_time'=>date('Y-m-d H:i:s')]);
            }
        }
        M('tnetissue_customer')->where(['id'=>['in',$ids]])->save(['shutpage_modifytime'=>time()]);
        
        $this->ajaxReturn(['code'=>0,'msg'=>'finish','data'=>$finishData]);
    }

    /*
    * 获取断流时段
    * by zw
    */
    public function get_interrupt_flow(){
        $fstarttime = I('fstarttime');
        $fendtime = I('fendtime');
        $fmediaid = I('fmediaid');
        if(empty($fstarttime) || empty($fendtime) || empty($fmediaid)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数有误']);
        }
        $res = http('http://az.hz-data.xyz:8087/channel/'.$fmediaid.'/m3u8.gap?start_time='.$fstarttime.'&end_time='.$fendtime,[],'get',false,10);
        $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$res]);
    }

    /*
    * 修改国家局第三方数据复核计划内部遗漏检查结果的众包处理优先级
    * by zw
    */
    public function lunxun_neibuyilou_priority(){
        $save_tit = M()->execute('update task_input,tapimediatask  set task_input.priority = 50
where task_input.media_id = tapimediatask.fmediaid and task_input.issue_date = tapimediatask.fissuedate and task_input.task_state = 0 and tapimediatask.fdstatus in (2,3)');
        $this->ajaxReturn(['code'=>0,'msg'=>'更新完成','data'=>$save_tit]);
    }

    /*
    * 检测上海当月任务是否已推送，包含传统和互联网
    * by zw
    */
    public function lunxun_create_task(){
        if(time()>=strtotime(date('Y-m-02'))){
            $do_pt = M('tapimedialist')
                ->alias('a')
                ->join('tapimedia_map b on a.fmediaid = b.fid')
                ->where(['a.fuserid'=>'22638a3131d0f0a7346b178fd29f939c','a.fissuedate'=>['egt',strtotime(date('Y-m-01'))],'b.fmediatype'=>['in',[1,2]]])
                ->count();
            if(empty($do_pt)){
                // 钉钉通知
                $title = '上海计划提醒';
                $content = "> 友情提示：\n\n上海工商局".(int)date('m')."月份传统媒体计划还未推送，请确认原因！\n\n";
                $smsphone = [13758156171];//提醒人员，赵国琪
                $token = '6299ea2a7184853c83972cf1df3ae1a838d23d18e1ea207cd168c6b3b92fd8cc';
                push_ddtask($title,$content,$smsphone,$token,'markdown');
            }

            $do_net = M('tapimedialist')
                ->alias('a')
                ->join('tapimedia_map b on a.fmediaid = b.fid')
                ->where(['a.fuserid'=>'22638a3131d0f0a7346b178fd29f939c','a.fissuedate'=>['egt',strtotime(date('Y-m-01'))],'b.fmediatype'=>4])
                ->count();
            if(empty($do_net)){
                // 钉钉通知
                $title = '上海计划提醒';
                $content = "> 友情提示：\n\n上海工商局".(int)date('m')."月份互联网媒体计划还未推送，请确认原因！\n\n";
                $smsphone = [13758156171];//提醒人员，赵国琪
                $token = '6299ea2a7184853c83972cf1df3ae1a838d23d18e1ea207cd168c6b3b92fd8cc';
                push_ddtask($title,$content,$smsphone,$token,'markdown');
            }
        }else{
            $this->ajaxReturn(['code'=>0,'msg'=>'无需检测']);
        }
    }

    /*
    * 检测上海当月传统媒体任务是否正常，每天8点跑一次
    * by zw
    */
    public function lunxun_inspectMedia(){
        set_time_limit(0);
        $where_pt['a.fuserid'] = '22638a3131d0f0a7346b178fd29f939c';
        $where_pt['a.fissuedate'] = ['gt',strtotime(date('Y-m-01'))];
        $where_pt['b.fmediatype'] = ['in',[1,2]];
        $where_pt['a.fdstatus'] = ['in',[1,2]];
        $do_pt = M('tapimedialist')
            ->field('b.fmediaid,a.fissuedate,c.fmedianame')
            ->alias('a')
            ->join('tapimedia_map b on a.fmediaid = b.fid')
            ->join('tmedia c on b.fmediaid = c.fid')
            ->where($where_pt)
            ->select();
        // 钉钉通知
        $title = '上海计划提醒';
        $smsphone = [13858090079];//提醒人员，沈露
        $token = '3cc83737c35805d21c51028067c707418c64a8a257b7ed30bccef3ef5b295efc';
        foreach ($do_pt as $key => $value) {
            $isOpen = isMediaDateAvailable($value['fmediaid'],$value['fissuedate']);
            if(!empty($isOpen)){
                $do_check = http('http://47.96.182.117/gather/getChannelStreamInfo?channel='.$value['fmediaid'].'&date='.date('Y-m-d',$value['fissuedate']),[],'get',false,30);
                $check = json_decode($do_check,true);
                if(empty($check['data']['code'])){
                    if((float)$check['data']['du_sc']>60 || (int)$check['data']['error_duration']>200){
                        $content = "> 友情提示：\n\n上海工商局传统媒体计划，".$value['fmedianame']."（".date('Y-m-d',$value['fissuedate'])."）存在数据质量问题，断流比：".$check['data']['du_sc']."%，断流时长：".$check['data']['error_time']."秒，断流次数：".$check['data']['error_duration']."次。请确认，有必要时，建议让用户更换检查计划！\n\n";
                        push_ddtask($title,$content,$smsphone,$token,'markdown');
                    }
                }else{
                    $content = "> 友情提示：\n\n上海工商局传统媒体计划，".$value['fmedianame']."（".date('Y-m-d',$value['fissuedate'])."）数据未就绪。请确认原因，有必要时建议让用户更换检查计划！\n\n";
                    push_ddtask($title,$content,$smsphone,$token,'markdown');
                }
            }
        }
    }

    /*
    * 获取上海互联网重点行业汇总数据
    * by zw
    */
    public function zdHangYeHuizong(){
        $stdate = I('stdate');//开始日期
        $eddate = I('eddate');//结束日期
        if(empty($stdate) || empty($eddate)){
            exit('任务日期参数有误');
        }

        $sqlstr = "select from_unixtime(b.fissuedate,'%Y-%m-%d') '任务日期',
            c.fmediacodename '媒体名称',
            count(0) as '交付总量',
            sum(CASE WHEN a.adtype like '01%' then 1 else 0 end) as '药品',
            sum(CASE WHEN a.adtype like '02%' then 1 else 0 end) as '医疗器械',
            sum(CASE WHEN a.adtype like '03%' then 1 else 0 end) as '化妆品',
            sum(CASE WHEN a.adtype like '04%' then 1 else 0 end) as '房地产',
            sum(CASE WHEN a.adtype like '05%' then 1 else 0 end) as '普通食品',
            sum(CASE WHEN a.adtype like '06%' then 1 else 0 end) as '保健食品',
            sum(CASE WHEN a.adtype like '13%' then 1 else 0 end) as '医疗服务',
            sum(CASE WHEN a.adtype like '14%' then 1 else 0 end) as '金融服务',
            sum(CASE WHEN a.adtype like '17%' then 1 else 0 end) as '商业投资',
            sum(CASE WHEN a.adtype like '18%' then 1 else 0 end) as '培训教育' 
             from tnetissue_customer a 
            JOIN tapimedialist b on a.ftaskid = b.fid and b.fissuedate BETWEEN UNIX_TIMESTAMP('".$stdate."') AND UNIX_TIMESTAMP('".$eddate."') 
            JOIN tapimedia_map c ON b.fmediaid = c.fid and c.fmediatype = 4 
            where a.fstatus = 6 
            GROUP BY b.fissuedate,c.fmediacodename";
        $data = M()->query($sqlstr);

        foreach ($data as $key => $value) {
            $data[$key]['重点行业交付量'] = (int)$value['药品']+(int)$value['医疗器械']+(int)$value['化妆品']+(int)$value['房地产']+(int)$value['普通食品']+(int)$value['保健食品']+(int)$value['医疗服务']+(int)$value['金融服务']+(int)$value['商业投资']+(int)$value['培训教育'];
            $data[$key]['重点行业比率'] = round($data[$key]['重点行业交付量']/$value['交付总量']*100,2).'%';
        }

        $outdata['title'] = '上海互联网广告类别汇总（'.$stdate.'至'.$eddate.'）';
        $outdata['datalie'] = [
          '序号'=>'key',
          '任务日期'=>'任务日期',
          '媒体名称'=>'媒体名称',
          '交付总量'=>'交付总量',
          '重点行业交付量'=>'重点行业交付量',
          '重点行业比率'=>'重点行业比率',
          '药品'=>'药品',
          '医疗器械'=>'医疗器械',
          '化妆品'=>'化妆品',
          '房地产'=>'房地产',
          '普通食品'=>'普通食品',
          '保健食品'=>'保健食品',
          '医疗服务'=>'医疗服务',
          '金融服务'=>'金融服务',
          '商业投资'=>'商业投资',
          '培训教育'=>'培训教育',
        ];

        $outdata['lists'] = $data;
        $ret = A('Api/Function')->outdata_xls($outdata);
        if(!empty($ret['url'])){
            echo $ret['url'];
        }else{
            echo '暂无数据';
        }

    }

    /*
    * 获取分众素材任务列表
    * by zw
    */
    public function getSourceTask(){
        $page_size = I('page_size')?I('page_size'):0;//每页条数
        $page_num = I('page_num')?I('page_num'):1;//页码
        $retData = [];

        //查询任务列表
        $params = [
            'app_id' => 1004,
            'biz_name' => '分众剪辑',
            'executor_id' => 0,
            'executor_org_id' => 0,
            'link_code' => '200101',
            'link_name' => '分众剪辑',
            'page_num' => $page_num,
            'page_size' => $page_size,
            'person_group_ids' => 0,
            'rent_id' => 0,
            'state' => 0
        ];

        $ret = accessService('/api-process/task20/taskList',$params,'POST');
        //查询生产计划明细
        if(!empty($ret['status']) && !empty($ret['data']['total'])){
            date_default_timezone_set('Asia/Shanghai');
            foreach ($ret['data']['list'] as $key => $value) {
                $ret2 = accessService('/api-commer/inspectplan/detail/'.$value['biz_key'],[],'GET');
                if(!empty($ret2['status'])){
                    $whereSource['a.fmediaid'] = $ret2['data']['mediaId'];
                    $whereSource['a.fissuedate'] = $ret2['data']['issueDate'];
                    $whereSource['a.fstatus'] = 1;
                    $doSource = M('tmediasource a')->field('fnumber,ffilename,ffileurl')->where($whereSource)->select();
                    $sourceList = $doSource?$doSource:[];
                    $retData[] = [
                        'task_link_id' => $value['task_link_id'],
                        'mediaid' => $ret2['data']['mediaId'],
                        'issuedate' => date('Y-m-d',strtotime($ret2['data']['issueDate'])),
                        'sourceList' => $sourceList
                    ];
                }
            }
        }
        if(!empty($retData)){
            $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$retData]);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'暂无任务']);
        }
    }

    /*
    * 分众素材任务完成
    * by zw
    */
    public function finishSourceTask(){
        $task_link_id = I('task_link_id');//任务环节主键

        if(empty($task_link_id)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
        }

        //查询任务列表
        $params3 = [
            'app_id' => 1004,
            'biz_name' => '分众剪辑',
            'executor_id' => 0,
            'executor_org_id' => 0,
            'link_code' => '200101',
            'link_name' => '分众剪辑',
            'page_num' => 0,
            'page_size' => 0,
            'person_group_ids' => 0,
            'rent_id' => 0,
            'state' => 0
        ];

        $ret3 = accessService('/api-process/task20/taskList',$params3,'POST');
        foreach ($ret3['data']['list'] as $key => $value) {
            if($value['task_link_id'] == $task_link_id){
                $biz_key = $value['biz_key'];//获取业务主键
                break;
            }
        }

        if(empty($biz_key)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'任务已完成或不存在'));
        }

        //完成任务
        $params = [
            'comment' => '',
            'executor_id' => 0,
            'next_executor' => '',
            'next_executor_id' => 0,
            'next_executor_org' => '',
            'next_executor_org_id' => 0,
            'person_group_id' => 0,
            'creator_org' => 0,
            'priority' => 0,
            'task_link_id' => $task_link_id
        ];
        $ret = accessService('/api-process/task20/commitTask',$params,'POST');

        if(!empty($ret['status'])){
            $ret4 = accessService('/api-commer/inspectplan/detail/'.$biz_key,[],'GET');//获取计划信息

            //获取媒体信息
            if(!empty($ret4['status'])){
                $viewMA = M('tmedia')->where(['fid'=>$ret4['data']['mediaId']])->find();
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'计划不存在'));
            }

            $ret2 = accessService('/api-commer/inspectplan/updatestatus/'.$biz_key.'?old_status='.$ret4['data']['status'].'&status=5',[],'PUT');//修改计划状态

            //钉钉提醒
            $title = '上海分众任务提醒';
            $content = "> 分众任务完成，请关注。\n\n媒体名称：".$viewMA['fmedianame']."（".$ret4['data']['mediaId']."） \n\n计划日期：".date('Y-m-d',strtotime($ret4['data']['issueDate']))."\n\n计划ID：".$biz_key."\n\n任务ID：".$ret['data']['task_id']."\n\n任务环节ID：".$task_link_id."\n\n";
            $smsphone = [13758156171];//提醒人员，赵国琪
            $token = '6299ea2a7184853c83972cf1df3ae1a838d23d18e1ea207cd168c6b3b92fd8cc';
            push_ddtask($title,$content,$smsphone,$token,'markdown');

            $this->ajaxReturn(array('code'=>0,'msg'=>'任务完成操作成功'));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'任务操作失败'));
        }
    }
}
