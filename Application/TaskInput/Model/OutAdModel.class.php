<?php
namespace TaskInput\Model;
use Think\Model;

class OutAdModel{

    /**
     * 户外广告任务状态码。两位数组成:第一位表示环节 9-表示不针对环节，第二位表示状态
     */
    protected $TaskState = [
        0,  //初始
        11, //环节1进行中
        12, //环节1完成
        13, //环节1退回
        17, //环节1退回回收
        18, //环节1回收
        21, //环节2进行中
        22, //环节2完成
        23, //环节2退回
        27, //环节2退回回收
        28, //环节2回收
        2,  //完成
        9,  //(暂未定义)
    ];

    /**
     * 户外广告任务流程日志类型码。两位数组成:第一位表示环节 9-表示不针对环节，第二位表示状态 1-领取任务 2-提交任务 3-退回 4-超时释放 5-定时同步 6-提交非广告
     */
    public $LogType = [
        "0" => "待领取任务环节(广告预审)",
        "20" => "待领取任务环节2(违法审核)",
        "11" => "领取任务环节1(广告预审)",
        "21" => "领取任务环节2(违法审核)",
        "12" => "提交环节1(广告预审)",
        "22" => "提交环节2(违法审核)",
        "13" => "退回环节1(广告预审)",
        "23" => "退回环节2(违法审核)",
        "17" => "退回超时释放环节1(广告预审)",
        "18" => "超时释放环节1(广告预审)",
        "27" => "退回超时释放环节2(违法审核)",
        "28" => "超时释放环节2(违法审核)",
    ];

    protected $task_count_field_map = [
        '1' => [
            '1' => 'tv_input',
            '2' => 'tv_inspect',
        ],
        '2' => [
            '1' => 'bc_input',
            '2' => 'bc_inspect',
        ],
        '3' => [
            '1' => 'paper_input',
            '2' => 'paper_inspect',
        ],
        '13' => [
            '1' => 'net_input',
            '2' => 'net_inspect',
        ],
    ]; // 第一层是媒体类型, 第二层是环节类型

    protected $v3_task_count_map = [
        '1' => 'v3_tv_task',
        '2' => 'v3_bc_task',
        '3' => 'v3_paper_task',
        '13' => 'v3_net_task',
    ];

    /**
     * 错误信息
     */
    protected $error = '';
    
    /**
     * 获取户外广告任务 2019-02-20
     */
    public function getTask()
    {
        $wx_id = intval(session('wx_id'));
        // 获取用户所在组的权限信息
        $userGroupInfo = M('ad_input_user_group')
            ->field('
                ad_input_user_group.other_authority, 
                ad_input_user_group.classauthority, 
                ad_input_user_group.mediaauthority, 
                ad_input_user_group.regionauthority, 
                ad_input_user_group.dateauthority, 
                ad_input_user.prefer_list, 
                ad_input_user.priority_list'
                )
            ->join('ad_input_user on ad_input_user.group_id = ad_input_user_group.group_id')
            ->where(['ad_input_user.wx_id' => $wx_id])
            ->find();
        // 获取用户环节权限，如:['1','2']
        $linkTypes = (new AdInputUserModel())->getTaskLinkAuth2($wx_id);
        // 任务日期排序,默认倒序,做环节2时正序,以处理积压的环节2任务
        $orderType = 'DESC';
        // 根据用户环节权限得到任务状态搜索条件
        // 任务过滤条件
        if($linkTypes === ['3']){
            $searchWhere['task.task_state'] = 0;
        }elseif($linkTypes === ['2']){
            $orderType = 'ASC';
            $searchWhere['task.task_state'] = 20;
        }elseif($linkTypes === ['2','3']){
            $orderType = 'ASC';
            $searchWhere['task.task_state'] = ['IN', [0,20]];
        }else{
            $searchWhere['task.task_state'] = 0;
        }
        // 如果有正在执行的任务，直接返回正在执行的任务id
        $taskid = M('todtask')
            ->alias('task')
            ->where([
                'task.wx_id' => ['EQ', $wx_id],
                'task.task_state' => ['IN',[11,21]],
                'task.modifytime' =>['gt',(time()-1200)]
            ])
            ->getField('task.taskid');
        if($taskid) return $taskid;

        // 被退回的任务，优先分配给负责人
        $taskid = M('todtask')
            ->alias('task')
            ->where([
                'task.wx_id' => ['EQ', $wx_id],
                'task.task_state' => ['IN',[13,23]]
            ])
            ->getField('task.taskid');
        if($taskid){
            M('todtask')->where(['taskid'=>$taskid])->save(['modifytime' => time()]);
            return $taskid;  
        } 

        $while_num = 0;
        $e_task_state = false;
        while(!$e_task_state){
            $while_num++;
            $orderArr = [];
            $orderArr[] = 'task.priority desc';
            $orderArr[] = 'task.net_created_date '.$orderType;
            $order = implode(',', $orderArr);

            // 超时任务领取
            $taskList = M('todtask')
                ->alias('task')
                ->field('task.taskid,task.task_state')
                ->where([
                    'task.wx_id' => ['NEQ', $wx_id],
                    'task.task_state' => ['IN',[11,21]],
                    'task.modifytime' =>['lt',(time()-1200)]
                ])
                ->limit(100)
                ->select();

            if(!$taskList){
                $searchWhere['task.priority'] = ['GT', 0];
                $taskList = M('todtask')->alias('task')
                    ->field('task.taskid,task.task_state')
                    ->where($searchWhere)
                    ->order($order)
                    ->limit(100)
                    ->select();
            }
            
            if(!$taskList){
                $searchWhere['task.priority'] = 0;
                $searchWhere['task.net_created_date'] = ['BETWEEN',[strtotime('-1 day'),time()]];
                $taskList = M('todtask')->alias('task')
                    ->field('task.taskid,task.task_state')
                    ->where($searchWhere)
                    ->order($order)
                    ->limit(100)
                    ->select();
            }
            if(!empty($taskList)){
                shuffle($taskList);//数组打乱排序
                foreach ($taskList as $taskInfo) {
                    $taskid = $taskInfo['taskid'];
                    $currentState = $taskInfo['task_state'];
                    if($currentState == 0 || $currentState == 11 || $currentState == 13){
                        $nextTaskState = 11;
                    }elseif($currentState == 20 || $currentState == 21 || $currentState == 23){
                        $nextTaskState = 21;
                    }
                    M()->startTrans();
                    $e_task_state = M('todtask')
                        ->where([
                            'taskid' => ['EQ', $taskid],
                            'task_state' => $currentState
                        ])
                        ->save([
                            'wx_id' => $wx_id,
                            'task_state' => $nextTaskState,
                            'modifytime' => time()
                        ]);
                    if($e_task_state){
                        $e_task_state = M('todtask_flowlog')
                            ->add([
                                'taskid' => $taskid,
                                'tasklink' => substr($currentState,0,1),
                                'wx_id' => $wx_id,
                                'logtype' => $nextTaskState,
                                'log' => $this->LogType[$nextTaskState],
                                'creator' => $wx_id,
                                'createtime' => time()
                            ]);
                        if($e_task_state){
                            M()->commit();
                            return $taskid;
                        }
                    }
                    M()->rollback();
                }
            }else{
                return false;
            }
            
            if($while_num > 10) return false;//如果抢了10次还没有任务，那就返回false
        }
    }

    /**
     * 获取日志类型属性
     * @Param String $code 类型代码，不传则返回列表数组
     */
    public function getLogType($code){
        if($code){
            return $this->LogType[$code];
        }else{
            return $this->LogType;
        }
    }

    /**
     * 错误信息
     */
    public function getError(){
        return $this->error;
    }

	
}
