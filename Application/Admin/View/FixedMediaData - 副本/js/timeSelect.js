const timeSelect = {
	
  template: `
  
  <div class="timecontainer" v-loading="loading" id="timecontainer">
  <ul id="rightMenu" oncontextmenu="return false">
		<li class="rightMenuList me1" @mouseout="rightMenuListHover(0)" @mouseover="rightMenuListHover(1)" @mousedown="getInfo()"> <span >查看状态<span></li>
		<li class="rightMenuList me2" @mouseout="rightMenuListHover(0)" @mouseover="rightMenuListHover(1)" @mousedown="getLog()"> <span >查看归档记录<span></li>
		<li class="rightMenuList me3" @mouseout="rightMenuListHover(0)" @mouseover="rightMenuListHover(1)" @mousedown="vm.handleCommand(0)"> <span >改为未归档<span></li>
		<li class="rightMenuList me4" @mouseout="rightMenuListHover(0)" @mouseover="rightMenuListHover(1)" @mousedown="vm.handleCommand(-1)"> <span >改为逾期未归档<span></li>
		<li class="rightMenuList me5" @mouseout="rightMenuListHover(0)" @mouseover="rightMenuListHover(1)" @mousedown="vm.handleCommand(99)"> <span >改为特殊状态<span></li>
		<li class="rightMenuList me6" @mouseout="rightMenuListHover(0)" @mouseover="rightMenuListHover(1)" @mousedown="vm.handleCommand(1)"> <span >改为已归档<span></li>
		<li class="rightMenuList me7" @mouseout="rightMenuListHover(0)" @mouseover="rightMenuListHover(1)" @mousedown="vm.change_inspect_type()"> <span >修改生产方式<span></li>
		<li class="rightMenuList me8" @mouseout="rightMenuListHover(0)" @mouseover="rightMenuListHover(1)" @mousedown="vm.gd_fx()"> <span >归档到分析数据库<span></li>
		
	</ul>
    <div class="swrap" @mouseleave="activeDay = ''">
      <div class="mainbox">
        <div class="_boxlist" @click.stop>
          <div class="_list" v-for="i in tableHead" :key="i.day">{{i.day}}</div>
        </div> 
        <div id="listWrap" @contextmenu.prevent="" @mousemove="right_select_move()" @mousedown="right_select_down()" @mouseup="right_select_up()">
          <div class="boxlist"  v-for="(item,index0) in list" :key="item.fid"><!-- @mousedown="mousedownfn" -->
            <div class="list" :title=i.msg
              v-for="(i,index) in item.days"
			  
              :class="{
				
                selected: i.selected === 1,
                archive0: i.fixed_state == '0',
                archive1: i.fixed_state == '1',
                'archive-1': i.fixed_state == '-1',
                archive99: i.fixed_state == '99',
                hover: i.day === activeDay,
                overtime: isOverTime(i)
				
				
              }"
				:source_collect_intact = "i.source_collect_intact"
				:data="item.fid + ',' + index + ',' + i.fday"
				
			  @mousedown="showInfo()"
			  
			  :xx="index"
			  :yy="index0"
			  
              
              :key="index">
              <div :class="{dayNum:true,u_1:i.finspect_type==3}" :xx="index" :yy="index0" >
                {{i.day}}
                <div class="source_collect_intact" v-if="i.source_collect_intact" :style="{width: 'calc(34px * ' + i.source_collect_intact + ')'}"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- @click.stop="showInfo(item,i)" -->
    <el-dialog
      class="info-dialog"
      title=""
      :visible.sync="showDialog"
      :show-close="false"
      width="400px">
      <div v-html="activeDateData"></div>
    </el-dialog>
    <el-drawer
      :title="title"
      :visible.sync="drawer"
      direction="rtl">
      <div style="height: calc(100vh - 100px); overflow: auto;">
        <el-table
          :data="logData"
          style="width: 100%">
          <el-table-column
            type="index"
            width="50">
          </el-table-column>
          <el-table-column
            prop="fvalue"
            label="操作内容">
            <template slot-scope="scope">
              <el-tooltip effect="dark" placement="left">
                <div slot="content" v-html="scope.row._fvalue"></div>
                <div v-html="scope.row.fvalue" style="width:10em;overflow: hidden;white-space:nowrap;text-overflow:ellipsis;"></div>
              </el-tooltip>
            </template>
          </el-table-column>
          <el-table-column
            prop="fperson"
            label="操作人"
            width="100">
          </el-table-column>
          <el-table-column
            prop="ftime"
            label="操作时间"
            width="140">
          </el-table-column>
        </el-table>
      </div>
    </el-drawer>
  </div>`,




  props: {
    tableData: {
      type: Array,
      required: true,
    },
    tableHead: {
      type: Array,
      required: true,
    }
  },
  data() {
    return {
      drawer: false,
      title: '',
      list: [],
      selectAll: false,
      isSelect: true,
      selectBoxDashed: null,
      startX: null,
      startY: null,
      initx: null,
      inity: null,
      scrollX: null,
      scrollY: null,
      logData: [],
      loading: false,
      timer: null,
      isMove: false,
      showDialog: false,
      activeDay: '',
      activeDateData: '',
      state: {
        '0': {
          label: '未归档',
          class: 'archive0'
        },
        '1': {
          label: '已归档',
          class: 'archive1'
        },
        '-1': {
          label: '逾期未归档',
          class: 'archive-1'
        },
        '99': {
          label: '特殊',
          class: 'archive99'
        },
      }
    }
  },
  watch: {
    tableData: {
      handler() {
        this.list = [...this.tableData]
      },
      deep: true
    }
  },
  created() {
    this.list = [...this.tableData]
  },
  methods: {
    isOverTime({last_time = 0, last_change_time = 0}) {
      return Number(last_change_time) > Number(last_time)
    },
    clearData() {
      this.checkData = []
    },
    showInfo() {
		if(event.button != 0) return false
		if(event.ctrlKey){
			xx = event.target.getAttribute('xx')
			yy = event.target.getAttribute('yy')
			$('.list').each(function(){
				myxx = $(this).attr('xx')
				myyy = $(this).attr('yy')
				if(xx == myxx && yy == myyy){
					$(this).toggleClass('selected')
				}
			});
		}else if(event.shiftKey){
			xx = parseInt(event.target.getAttribute('xx'))
			yy = parseInt(event.target.getAttribute('yy'))
			
			if(shiftXY.length == 2){
				$('.list').each(function(){
				
					myxx = parseInt($(this).attr('xx'))
					myyy = parseInt($(this).attr('yy'))
					
					if((myxx == shiftXY[0] && myyy == shiftXY[1]) || (myxx < xx && myxx < shiftXY[0]) || (myxx > xx && myxx > shiftXY[0]) || (myyy < yy && myyy < shiftXY[1]) || (myyy > yy && myyy > shiftXY[1])){
						
					
					}else{
						$(this).toggleClass('selected')
					}
				
					
								
				});
				
			}else{
				$('.list').each(function(){
					myxx = $(this).attr('xx')
					myyy = $(this).attr('yy')
					if(xx == myxx && yy == myyy){
						$(this).toggleClass('selected')
					}
				});
			}
			
			shiftXY = [parseInt(xx),parseInt(yy)]

		}else{
			
			xx = event.target.getAttribute('xx')
			yy = event.target.getAttribute('yy')
			$('.list').each(function(){
				myxx = $(this).attr('xx')
				myyy = $(this).attr('yy')
				if(xx == myxx && yy == myyy){
					$(this).addClass('selected')
				}else{
					$(this).removeClass('selected')
				}
			});
		}

    },
	
	getInfo(){
		
		let mediaid_date = $('.list.selected').attr('data').split(',')
		

		this.loading = true
		let data = {
			fmediaid: mediaid_date[0],
			fdate: mediaid_date[2]
		}

		fly.post('/Admin/FixedMediaData/get_msg', Qs.stringify(data)).then(({ data }) => {
			//console.log(data.msg)
			this.showDialog = true
			this.loading = false
			this.activeDateData = data.msg	
		}).catch(() => {
			this.loading = false
		})

	},
    getLog() {
      clearTimeout(this.timer)
      this.loading = true
      //this.title = `${item.fmedianame} ${i.fday} 的归档日志`
	  

	  
	  

		let mediaid_date = $('.list.selected').attr('data').split(',')	

      fly.get('/Admin/FixedMediaData/s_log?fmediaid='+mediaid_date[0]+'&fdate='+mediaid_date[2]).then(({data}) => {
		  this.title = data.mediaInfo.fmedianame+' '+ mediaid_date[2] +' 的归档日志'
        if (data.fixed_media_data_log_list.length) {
          this.logData = data.fixed_media_data_log_list.map(i => {
            i._fvalue = i.fvalue
            i.fvalue = i.fvalue.replace(/<\/br>/g, '')
            return i
          })
          this.drawer = true
        } else {
          $h.notify.info('该日无归档日志！',this)
        }
        this.loading = false
      })
    },

	//监听鼠标按键按下
    right_select_down() {

		if(event.button == 2){
			$('.me1').hide()
			$('.me2').hide()
			if($('.list.selected').length == 1 ){
				$('.me1').show()
				$('.me2').show()
				
				xx = event.target.getAttribute('xx')
				yy = event.target.getAttribute('yy')
				$('.list').each(function(){
					myxx = $(this).attr('xx')
					myyy = $(this).attr('yy')
					if(xx == myxx && yy == myyy){
						$(this).addClass('selected')
					}else{
						$(this).removeClass('selected')
					}
				});
				
			}
			oEvent = event;
			var oUl=document.getElementById('rightMenu');
			oUl.style.display='block';
			oUl.style.left=oEvent.clientX+'px';//如果超出可视区就加scroll
			oUl.style.top=oEvent.clientY+'px';

		} 			 
		

		if(event.button != 0) return
		//document.getElementById('listWrap').oncontextmenu = function() { return false } //禁用右键菜单
		console.log('按下了鼠标')
		console.log('按下鼠标时的坐标',$(event.toElement).attr('xx'),$(event.toElement).attr('yy'))
		rightXY = [parseInt($(event.toElement).attr('xx')),parseInt($(event.toElement).attr('yy'))]
		rightLastXY = [parseInt($(event.toElement).attr('xx')),parseInt($(event.toElement).attr('yy'))]
		rightDown = true

    },
	
	//监听鼠标按键弹起
	right_select_up() {

		if(event.button != 0) return
		console.log('松开了鼠标')
		rightDown = false
		rightXY = []
		rightLastXY = []
		cfList = []
		
		
		
    },
	
	//监听鼠标移动
	right_select_move() {

		if(event.buttons != 1) return false
		myxx = parseInt($(event.toElement).attr('xx'))
		myyy = parseInt($(event.toElement).attr('yy'))
		
		if((myxx != rightLastXY[0] || myyy != rightLastXY[1]) && !isNaN(myxx)){
			
			console.log('坐标发生变化',myxx,myyy)
			
			rightLastXY = [myxx,myyy]
			
			
			$('.list').each(function(){
				
				myxxt = parseInt($(this).attr('xx'))
				myyyt = parseInt($(this).attr('yy'))
				
				
				if(	
					   (myxxt < rightXY[0] && myxxt < rightLastXY[0]) 
					|| (myxxt > rightXY[0] && myxxt > rightLastXY[0]) 
					|| (myyyt < rightXY[1] && myyyt < rightLastXY[1]) 
					|| (myyyt > rightXY[1] && myyyt > rightLastXY[1])
					|| (myxxt == rightXY[0] && myyyt == rightXY[1])
					
																		){
					
					cfListIndex = cfList.indexOf($(this).attr('xx')+'_'+$(this).attr('yy')) 
					if(cfListIndex >= 0){
						cfList.splice(cfListIndex,1)
						$(this).toggleClass('selected')
					}
					
				}else if(cfList.indexOf($(this).attr('xx')+'_'+$(this).attr('yy')) >= 0){
					
				}else{
					cfList.push($(this).attr('xx')+'_'+$(this).attr('yy'))
					$(this).toggleClass('selected')
					
				}
			
				
							
			});
			
			
			
		}
		
	
    },
	
	
	rightMenuListHover(f){
		
		if(f == 1){
			//console.log($(event.toElement).prop('tagName'))
			if($(event.toElement).prop('tagName') == 'SPAN'){
				
				$(event.toElement).parent().addClass('rml_se')
			}else{
				$(event.toElement).addClass('rml_se')
			}
			
		}else if(f == 0){
			$('.rightMenuList').removeClass('rml_se')
		}
	}
	
	
	
	





  }
}