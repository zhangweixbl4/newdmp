<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 线索反馈
 */

class FxsfankuiController extends BaseController
{
	/**
	 * 获取待反馈线索列表
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function index() {
		session_write_close();

		$p 	= I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录

		$where['_string'] 	= '((fwaitregulatorid='.session('regulatorpersonInfo.fregulatorpid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid='.session('regulatorpersonInfo.fid').'))';//用户处理权限范围

		$fname 			= I('fname');//登记表名
		$fmodifytime 	= I('fmodifytime');//登记时间
		$status = I('status');//登录来源
		if(!empty($fname)){
			$where['fname'] = array('like','%'.$fname.'%');
		}
		if(!empty($fmodifytime)){
			$where['_string'] .= ' and datediff("'.$fmodifytime.'",fregistertime)=0';
		}
		if(!empty($status)){
			if($status==1){
				$where['fstate'] = 30;
			}elseif($status==2){
				$where['fstate'] = 32;
			}else{
				$where['fstate'] = array('in',array(30,32));
			}
		}else{
			$where['fstate'] 	= array('in',array(30,32));
		}

		$count = M('tregister')
			->where($where)
			->count();//查询满足条件的总记录数

		
		$data = M('tregister')
			->where($where)
			->order('fid desc')
			->page($p,$pp)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	/**
	 *线索反馈操作
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function update_register(){

		$fid 			= I('fid');//登记信息ID
		$fpunish 		= I('fpunish');//处罚方式
		$fdealcontents 	= I('fdealcontents');//反馈意见
		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}

		$where['fid'] 		= $fid;//登记表ID
		$where['fstate'] 	= array('in',array(30,32));
		$where['_string'] 	= '((fwaitregulatorid='.session('regulatorpersonInfo.fregulatorpid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid='.session('regulatorpersonInfo.fid').'))';//用户处理权限范围

		$data = M('tregister')->where($where)->find();//获取当前登记信息
		$tregisterdb 		= M('tregister');
		if(!empty($data)){
			$twdo = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'ffinishtime is null'])->find();//获取前一流程id
			if(!empty($twdo)){
				//更新上一流程的处理人
				$twdata['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
				$twdata['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
				$twdata['ffinishtime']					= date('Y-m-d H:i:s');//处理时间
				$twdo1 = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'ffinishtime is null'])->save($twdata);
				if(!empty($twdo1)){
					/*新增流程记录*/
					$sh_list['fregisterid'] 				= $fid;
					$sh_list['fupflowid'] 					= $twdo['flowid'];
					$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
					$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
					$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
					$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
					$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
					$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
					$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
					$sh_list['fflowname'] 					= '线索反馈';
					$sh_list['fcreateinfo'] 				= $fdealcontents;
					$sh_list['fregulatorid'] 				= I('fwaitregulatorid');
					$sh_list['fstate'] 		= 31;
					$sh_list['fchildstate'] = 0;
					$sh_list['freason'] 					= '线索反馈';
					$flowid = M("tregisterflow")->add($sh_list);
				}
				if(!empty($flowid)){
					M()->execute('update document set dt_flowid='.$flowid.' where dt_tregisterid='.$fid.' and dt_flowid=0');
					M()->execute('update tregisterfile set fillegaladflowid='.$flowid.' where fregisterid='.$fid.' and fillegaladflowid=0');
				}
			}

			$res['fwaitregulatorid']		= I('fwaitregulatorid');//下一处理部门默认无
			$res['fwaitpersonid']			= I('fwaitpersonid');//下一处理人默认无
			//待写入的登记信息--公共
			$res['fstate'] 				= 31;//将状态改成初核待签批状态
			$res['fmodifier']			= session('regulatorpersonInfo.fname');//修改人
			$res['fmodifytime']			= date('Y-m-d H:i:s');//修改时间
			$res['fdealregulatorpid']	= session('regulatorpersonInfo.fregulatorpid');//反馈机构id
			$res['fdealregulatorpname']	= session('regulatorpersonInfo.regulatorpname');//反馈机构名称
			$res['fdealpersonid']		= session('regulatorpersonInfo.fid');//反馈人id
			$res['fdealpersonname']		= session('regulatorpersonInfo.fname');//反馈人姓名
			$res['fdealcontents']		= $fdealcontents;//反馈情况
			if(!empty($fpunish)){
				$res['fpunish']			= implode(';', $fpunish) ;//处罚方式
			}
			$res['fdealtime']			= date('Y-m-d H:i:s');//反馈时间
			$res['fusersid'] 			= $data['fusersid'].session('regulatorpersonInfo.fid').',';//加入到处理用户组
			$trdo = $tregisterdb->where('fid='.$fid)->save($res);
			if(!empty($trdo) && !empty($flowid)){
				D('Function')->write_log('线索反馈',1,'操作成功','tregister',$trdo,M('tregister')->getlastsql());
				$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
			}else{
				D('Function')->write_log('线索反馈',0,'操作失败','tregister',0,M('tregister')->getlastsql());
				$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
			}
		}else{
			D('Function')->write_log('线索反馈',0,'操作失败','tregister',0,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
		}
	}

	/**
	 *获取待签批列表
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function get_dqpregisterlist(){
		session_write_close();
		
		$p 	= I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录

		$where['tregister.fstate'] 				= 31;//待签批状态
		$where['_string'] 	= '((fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid=0) or (fwaitregulatorid='.session('regulatorpersonInfo.fregulatorid').' and fwaitpersonid='.session('regulatorpersonInfo.fid').'))';//用户处理权限范围

		$fname 			= I('fname');//登记表名
		$fmodifytime 	= I('fmodifytime');//登记时间
		if(!empty($fname)){
			$where['fname'] = array('like','%'.$fname.'%');
		}
		if(!empty($fmodifytime)){
			$where['_string'] .= ' and datediff("'.$fmodifytime.'",fregistertime)=0';
		}

		$count = M('tregister')
			->where($where)
			->count();//查询满足条件的总记录数

		
		$data = M('tregister')
			->where($where)
			->order('fregistertime desc,fid desc')
			->page($p,$pp)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	/**
	 * 签批操作
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function action_qpregister(){
		
		if(I('fid')){
			$fid = I('fid');
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}
		$submit = I('submit')?I('submit'):1;//1签批2退回

		$where['fid'] 							= $fid;//登记表ID
		$where['tregister.fstate'] 				= 31;//初核待签批状态
		$where['tregisterflow.fregulatorid'] 	= session('regulatorpersonInfo.fregulatorid');//登记机关id
		$data = M('tregister')->join('tregisterflow on tregister.fid=tregisterflow.fregisterid and tregisterflow.ffinishtime is null')->where($where)->find();
		if(!empty($data)){
			
			if($submit==1){//签批
				$twdo = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'ffinishtime is null'])->find();//获取前一流程id
				if(!empty($twdo)){
					//更新上一流程的处理人
					$twdata['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
					$twdata['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
					$twdata['ffinishtime']					= date('Y-m-d H:i:s');//处理时间
					$twdata['fistree']						= 10;//设为主流程
					$twdo1 = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'ffinishtime is null'])->save($twdata);

					if(!empty($twdo1)){
						if($data['fchecktype']==10 || $data['fchecktype']==30){
							//新增流程记录
							$sh_list['fregisterid'] 				= $fid;
							$sh_list['fupflowid'] 					= $twdo['flowid'];
							$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
							$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
							$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
							$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
							$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
							$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
							$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
							$sh_list['fflowname'] 					= '线索反馈';
							$sh_list['freason'] 					= '反馈完成';
							$sh_list['fregulatorid'] 				= session('regulatorpersonInfo.fregulatorid');
							$sh_list['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
							$sh_list['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
							$sh_list['ffinishtime'] 				= date('Y-m-d H:i:s');
							$sh_list['fcreateinfo'] 				= I('result');
							$sh_list['fstate'] 						= 40;
							$flowid = M("tregisterflow")->add($sh_list);

							$trdata['fstate'] 						= 40;//线索处理完成
							$trdata['fwaitregulatorid']				= 0;//下一处理部门默认无
							$trdata['fwaitpersonid']				= 0;//下一处理人默认无
						}else{
							$fpid = D('Function')->get_tregulatoraction($data['fregisterregulatorid']);
							if(session('regulatorpersonInfo.fregulatorpid')==$fpid){
								//新增流程记录
								$sh_list['fregisterid'] 				= $fid;
								$sh_list['fupflowid'] 					= $twdo['flowid'];
								$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
								$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
								$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
								$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
								$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
								$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
								$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
								$sh_list['fflowname'] 					= '线索反馈';
								$sh_list['freason'] 					= '反馈完成';
								$sh_list['fregulatorid'] 				= session('regulatorpersonInfo.fregulatorid');
								$sh_list['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
								$sh_list['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
								$sh_list['ffinishtime'] 				= date('Y-m-d H:i:s');
								$sh_list['fcreateinfo'] 				= I('result');
								$sh_list['fstate'] 						= 40;
								$flowid = M("tregisterflow")->add($sh_list);

								$trdata['fstate'] 						= 40;//线索处理完成
								$trdata['fwaitregulatorid']				= 0;//下一处理部门默认无
								$trdata['fwaitpersonid']				= 0;//下一处理人默认无
							}else{
								//新增流程记录
								$sh_list['fregisterid'] 				= $fid;
								$sh_list['fupflowid'] 					= $twdo['flowid'];
								$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
								$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
								$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
								$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
								$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
								$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
								$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
								$sh_list['fflowname'] 					= '线索反馈';
								$sh_list['freason'] 					= '反馈上级';
								$sh_list['fregulatorid'] 				= $data['fregisterregulatorid'];
								$sh_list['fcreateinfo'] 				= I('result');
								$sh_list['fstate'] 						= 30;
								$flowid = M("tregisterflow")->add($sh_list);

								$trdata['fstate'] 						= 30;//反馈待处理
								$trdata['fwaitregulatorid']				= $data['fregisterregulatorid'];//下一处理部门默认无
								$trdata['fwaitpersonid']				= 0;//下一处理人默认无
							}
							
						}
						
						//保存签批信息
						$trdata['result'] 		= I('result');//签批建议
						$trdata['fdealapprovalpersonid'] 		= session('regulatorpersonInfo.fid');//签批人ID
						$trdata['fdealapprovalpersonname'] 		= session('regulatorpersonInfo.fname');//签批人
						$trdata['fdealapprovaltime'] 			= date('Y-m-d H:i:s');//签批时间
						$trdata['fusersid'] 					= $data['fusersid'].session('regulatorpersonInfo.fid').',';//加入到处理用户组
						$trdo = M('tregister')->where('fid='.$fid)->save($trdata);
						
					}

				}

				
			}else{
				$twdo = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'ffinishtime is null'])->find();//获取前一流程id
				if(!empty($twdo)){
					//保存签批信息
					$trdata['result'] 	= I('result');//反馈签批建议
					$trdata['fdealapprovalpersonid'] 	= session('regulatorpersonInfo.fid');//签批人ID
					$trdata['fdealapprovalpersonname'] 	= session('regulatorpersonInfo.fname');//签批人
					$trdata['fdealapprovaltime'] 		= date('Y-m-d H:i:s');//签批时间
					$trdata['fstate'] 					= 32;//签批退回
					$trdata['fwaitregulatorid'] 		= $twdo['fcreateregualtorid'];//下一处理部门
					$trdata['fwaitpersonid'] 			= $twdo['fcreateregualtorpersonid'];//下一处理人
					$trdata['fusersid'] 				= $data['fusersid'].session('regulatorpersonInfo.fid').',';//加入到处理用户组
					$trdo = M('tregister')->where('fid='.$fid)->save($trdata);

					//更新上一流程的处理人
					$twdata['fregulatorpersonid'] 			= session('regulatorpersonInfo.fid');
					$twdata['fregulatorpersonname'] 		= session('regulatorpersonInfo.fname');
					$twdata['ffinishtime']					= date('Y-m-d H:i:s');//处理时间
					$twdo1 = M('tregisterflow')->where(['fregisterid'=>$fid,'_string'=>'ffinishtime is null'])->save($twdata);

					if(!empty($twdo1)){
						//新增流程记录
						$sh_list['fregisterid'] 				= $fid;
						$sh_list['fupflowid'] 					= $twdo['flowid'];
						$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
						$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
						$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
						$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
						$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
						$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
						$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
						$sh_list['fflowname'] 					= '线索反馈';
						$sh_list['freason'] 					= '反馈签批（退回）';
						$sh_list['fregulatorid'] 				= $twdo['fcreateregualtorid'];
						$sh_list['fcreateinfo'] 				= I('result');
						$sh_list['fstate'] 						= 32;
						$flowid = M("tregisterflow")->add($sh_list);
					}
				}
			}
			D('Function')->write_log('反馈签批',1,'签批成功','tregister',$trdo,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'签批成功'));

		}else{
			D('Function')->write_log('反馈签批',0,'签批失败','tregister',0,M('tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'签批失败'));
		}
	}
}