<?php
namespace Api\Model;
use Think\Exception;
use Think\Log;


class FixedMediaDataModel{
	
	/*获取节目*/
	public function get_prog($mediaId,$date){
		
		$mediaInfo = M('tmedia')->field('fid,left(fmediaclassid,2) as media_class')->where(array('fid'=>$mediaId))->find();
		
		if($mediaInfo['media_class'] == '01'){
			$tableName = 'ttvprog';
		}elseif($mediaInfo['media_class'] == '02'){
			$tableName = 'tbcprog';
		}else{
			return 0;
		}
		
		$startTime = strtotime($date);
		$endTime = $startTime + 86399;
	
		$getUrl = 'http://47.96.182.117/index/getEpgSummaryList?id='.$mediaId.'&start='.$startTime.'&end='.$endTime;
		
		$retStr = http($getUrl);
		$retArr = json_decode($retStr,true);
		$all_data = [];
		foreach($retArr['dataList'] as $data){
			
			foreach($data['child'] as $de){
				if($de['is_ad'] != 2) continue;
				
				$a_data = array();
				$a_data['fmediaid'] = $mediaId;//媒介ID
				$a_data['fissuedate'] = date('Y-m-d',strtotime($date));//发布日期
				$a_data['fcontent'] = $de['title'];//节目内容
				$a_data['fclasscode'] = $data['category'];//节目类别代码
				$a_data['fstart'] = $de['start_time'];//开始播放时间
				$a_data['fend'] = $de['end_time'];//结束播放时间
				$a_data['faudiosimliar'] = 100;//音频相似度
				$a_data['fvideosimliar'] = 100;//视频相似度
				$a_data['fu0'] = 0;//位置，单位毫秒
				$a_data['fduration'] = $de['end'] - $de['start'];//位置，单位秒
				
				$a_data['fsamplefilename'] = '';//样本文件名，样本文件名用于判断是否需要写入新样本
				$a_data['flastissuedate'] = date('Y-m-d',strtotime($date));//最后发布日期
				$a_data['fcreator'] = '自动从接口获取';//创建人
				$a_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
				$a_data['fmodifier'] = '自动从接口获取';//修改人
				$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
				$a_data['push_state'] = 1;
				
				
				$all_data[] = $a_data;
				#var_dump($a_data);
			}
		}
		
		
		$insertCount = 0;
		if(count($all_data) > 0){
			M($tableName)->where(array('fmediaid'=>$mediaId,'fissuedate'=>$date))->delete();
			$ff = M($tableName)->addAll($all_data);
			if($ff) $insertCount = count($all_data) ;
		}
		return $insertCount;
	}
	
	
	
	
	
	
	
	
	
}