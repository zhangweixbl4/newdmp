<?php
//样本控制器
namespace Admin\Controller;
use Think\Controller;
class SampleController extends TaskInputController {

    private $sampleTableMap = [
        '1' => 'ttvsample',
        '2' => 'tbcsample',
        '3' => 'tpapersample'
    ];

	public function index(){
		if(I('fcreatetime_s') == '') $_GET['fcreatetime_s'] = date('Y-m-d',strtotime(date('Y-m-d')) - 86400*3);
		if(I('fcreatetime_e') == '') $_GET['fcreatetime_e'] = date('Y-m-d');
		if(I('media_class') == '' ) $_GET['media_class'] = '01';
		$media_class = I('media_class');//媒体类型，01电视，02广播，03报纸
		
		if($media_class == '01'){
			A('Admin/Tvsample')->index();
		}elseif($media_class == '02'){
			A('Admin/Bcsample')->index();
		}elseif($media_class == '03'){
			A('Admin/Papersample')->index();
		}else{
			A('Admin/Tvsample')->index();
		}
		
		
		$this->display();
	}

    /**
     * 批量修改任务列表
     */
    public function taskList()
    {
        if(A('Admin/Authority','Model')->authority('200156') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限'
            ]);
        }
        if (IS_POST){
            $page = I('page', 1);
            $pageSize = I('pageSize', 10);
            $formData = I('where');
            $where = [
                'finputstate' => ['EQ', 2],
                'finspectstate' => ['EQ', 2],
            ];
            if (!$formData['media_class']){
                $tableName = 'ttvsample';
                $formData['media_class'] = '1';
            }else{
                $tableName = $this->sampleTableMap[$formData['media_class']];
            }
            if ($formData['timeRangeArr']){
                $where['fissuedate'] = ['BETWEEN', $formData['timeRangeArr']];
            }
            if ($formData['timeRangeArr2']){
                $where['fmodifytime'] = ['BETWEEN', $formData['timeRangeArr2']];
            }
            // tad表
            $adWhere = [];
            if ($formData['adClass'] != ''){
                $len = strlen($formData['adClass']);
                $adWhere["LEFT(fadclasscode, $len)"] = ['EQ', $formData['adClass']];
            }
            if ($formData['adClass2'] != ''){
                $len = strlen($formData['adClass2']);
                $adWhere["LEFT(fadclasscode_v2, $len)"] = ['EQ', $formData['adClass2']];
            }
            if ($formData['fadid'] != ''){
                $adWhere["fadid"] = ['EQ', $formData['fadid']];
            }
            if ($formData['adName'] != ''){
                $adWhere['fadname'] = ['LIKE', '%'.$formData['adName'].'%'];
            }
            if ($formData['adOwner'] != ''){
                $adOwnerSubQuery = M('tadowner')
                    ->field('fid')
                    ->where([
                        'fname' => ['LIKE', '%'.$formData['adOwner'].'%']
                    ])
                    ->buildSql();
                $adWhere['fadowner'] = ['EXP', " IN ".$adOwnerSubQuery];
            }
            // tmedia表
            $mediaWhere = [];
            if ($formData['mediaName'] != ''){
                $mediaWhere['fmedianame'] = ['LIKE', '%'.$formData['mediaName'].'%'];
            }
            // tmediaowner表
            $mediaOwnerWhere = [];
            if ($formData['regionId'] != ''){
                if ($formData['onlyThisLevel'] != 1){
                    $region_id_rtrim = A('Common/System','Model')->get_region_left($formData['regionId']);//地区ID去掉末尾的0
                    $region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
                    $mediaOwnerWhere["LEFT(fregionid, $region_id_strlen)"] = ['EQ', $region_id_rtrim];
                }else{
                    $mediaOwnerWhere['fregionid'] = ['LIKE', '%'.$formData['regionId'].'%'];
                }
                $mediaOwnerSubQuery = M('tmediaowner')
                    ->field('fid')
                    ->where($mediaOwnerWhere)
                    ->buildSql();
                $mediaWhere['fmediaownerid'] = ['EXP', 'IN '. $mediaOwnerSubQuery];//地区搜索条件
            }
            // 媒介标签搜索
            if ($formData['mediaLabel'] != ''){
                $labelSubQuery = M('tlabel')
                    ->field('flabelid')
                    ->where([
                        'fstate' => ['EQ', 1],
                        'flabel' => ['EQ', $formData['mediaLabel']],
                    ])
                    ->buildSql();
                $mediaLabelSubQuery = M('tmedialabel')
                    ->field('fmediaid')
                    ->where([
                        'fstate' => ['EQ', 1],
                        'flabelid' => ['EXP', ' IN ' . $labelSubQuery]
                    ])
                    ->buildSql();

                $mediaWhere['fid'] = ['EXP', ' IN ' . $mediaLabelSubQuery];
            }
            if ($mediaWhere != []){
                $mediaSubQuery = M('tmedia')
                    ->field('fid')
                    ->where($mediaWhere)
                    ->buildSql();
                $where['fmediaid'] = ['EXP', 'IN '. $mediaSubQuery]; // 媒体搜索条件
            }
            if ($adWhere != []){
                $adSubQuery = M('tad')
                    ->field('fadid')
                    ->where($adWhere)
                    ->buildSql();
                if ($formData['adName'] != ''){
                    $where['_string'] = "(fadid IN $adSubQuery OR tem_ad_name LIKE ('%{$formData['adName']}%') )";
                }else{
                    $where['fadid'] = ['EXP', ' IN '.$adSubQuery];
                }
            }
            $res = M($tableName)
                ->where($where)
                ->order('fmodifytime desc')
                ->page($page, $pageSize)
                ->select();
            $total = M($tableName)
                ->where($where)
                ->count();
            foreach ($res as $key => $value) {
                $res[$key]['fmedianame'] = M('tmedia')->cache(true)->where(['fid' => ['EQ', $value['fmediaid']]])->getField('fmedianame');
                $adInfo = M('tad')->field('fadname, fadclasscode,fadclasscode_v2, fadowner, fbrand')->find($value['fadid']);
                $res[$key]['tem_ad_name'] = r_highlight($res[$key]['tem_ad_name'], $formData['adName'], 'red');
                $res[$key]['fillegaltypecode'] = $res[$key]['fillegaltypecode'] ? $res[$key]['fillegaltypecode'] : '0';
                $res[$key]['fadname'] = $adInfo['fadname'];
                $res[$key]['fadclasscode'] = $adInfo['fadclasscode'];
                $res[$key]['fbrand'] = $adInfo['fbrand'];
                $res[$key]['ffullname'] = M('tadclass')->cache(true)->where(['fcode' => ['EQ', $adInfo['fadclasscode']]])->getField('ffullname');
                $res[$key]['adClassV2'] = M('hz_ad_class')->cache(true)->where(['fcode' => ['EQ', $adInfo['fadclasscode_v2']]])->getField('ffullname');
                $res[$key]['adOwnerName'] = M('tadowner')->cache(true)->where(['fid' => ['EQ', $adInfo['fadowner']]])->getField('fname');
                $res[$key]['fadname'] = r_highlight($res[$key]['fadname'], $formData['adName'], 'red');
                $res[$key]['fmedianame'] = r_highlight($res[$key]['fmedianame'], $formData['mediaName'], 'red');
                $res[$key]['media_class'] = $formData['media_class'];
                if ($formData['media_class'] == 3){
                    $res[$key]['fid'] = $value['fpapersampleid'];
                }
            }
            $this->ajaxReturn([
                'data' => $res,
                'total' => $total,
            ]);

        }else{
            $illegalTypeArr = json_encode($this->getIllegalTypeArr());
            $adClassArr = json_encode($this->getAdClassArr());
            $adClassTree = json_encode($this->getAdClassTree());
            $regionTree = json_encode($this->getRegionTree());
            $illegalArr = json_encode($this->getIllegalArr());
            $this->assign(compact('illegalTypeArr', 'adClassArr', 'adClassTree', 'regionTree', 'illegalArr'));
            $this->display();
        }
	}

    public function getSampleInfo($mediaClass, $sampleId)
    {
        if(A('Admin/Authority','Model')->authority('200156') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限'
            ]);
        }
        $sample = M($this->sampleTableMap[$mediaClass])
            ->find($sampleId);
        $data = [];
        $taskData = $sample;
        // 如果adid不是0, 则找到广告相关数据
        if ($sample['fadid'] != 0){
            $data['ad'] = M('tad')->field('fadname, fbrand, fadclasscode, fadclasscode_v2, fadowner')->find($taskData['fadid']);
            $data['adOwner'] = M('tadowner')->field('fname')->find($data['ad']['fadowner']);
            $taskData['adCateText1'] = M('tadclass')->where(['fcode' => ['EQ', $data['ad']['fadclasscode']]])->cache(true)->getField('ffullname');
            $taskData['adCateText2'] = M('hz_ad_class')->where(['fcode' => ['EQ', $data['ad']['fadclasscode_v2']]])->cache(true)->getField('ffullname');
            $taskData['adCateText2'] = $taskData['adCateText2'] ? $taskData['adCateText2'] : '';
        }
        foreach ($data as $key => $value) {
            if ($value){
                $taskData = array_merge($taskData, $value);
            }
        }
        if ($taskData['fexpressioncodes']){
            $taskData['checkedIllegalArr'] = M('tillegal')->field('fcode, fconfirmation, fexpression, fillegaltype, fpcode, fcode id, fexpression label')->cache(true)->where(['fcode' => ['IN', explode(';',$taskData['fexpressioncodes'])]])->select();
        }
        $taskData['source_path'] = $taskData['favifilename'] ? $taskData['favifilename'] : $taskData['fjpgfilename'];
        $taskData['media_class'] = $mediaClass;
        if ($mediaClass == 3){
            $taskData['paperUrl'] = M('tpapersource')->where(['fid' => ['EQ', $taskData['sourceid']]])->getField('furl');
        }
        $taskData['media_name'] = M('tmedia')->cache(60)->where(['fid' => ['EQ', $taskData['fmediaid']]])->getField('fmedianame');
        $this->ajaxReturn($taskData);
	}


    public function submitSampleAdInfoBatchModify($adInfo, $samIds, $mediaClass)
    {
        if(A('Admin/Authority','Model')->authority('200156') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限'
            ]);
        }
        $userName = session('personInfo.fname');
        $fadname = $adInfo['fadname'];
        $fbrand = $adInfo['fbrand'];
        $adclass_code = $adInfo['fadclasscode'];
        $adclass_code_v2 = $adInfo['fadclasscode_v2'];
        $adowner_name = $adInfo['fname'];
        // 任务信息
        $samInfo = [];
        $samInfo['fadid'] = A('Common/Ad','Model')->getAdId($fadname,$fbrand,$adclass_code,$adowner_name,$userName, $adclass_code_v2);//获取广告ID
        $samInfo['need_sync'] = 0;
        $samInfo['fmodifier'] = $userName;
        $samInfo['fmodifytime'] = date('Y-m-d H:i:s');
        $tableName = $this->sampleTableMap[strval($mediaClass)];
        if ($mediaClass == 3){
            $where = [
                'fpapersampleid' => ['IN', $samIds],
            ];
        }else{
            $where = [
                'fid' => ['IN', $samIds],
            ];
        }
        $res = M($tableName)
            ->where($where)
            ->save($samInfo);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
    }

    public function submitSampleIllegalInfoBatchModify($illegalInfo, $samIds, $mediaClass)
    {
        if(A('Admin/Authority','Model')->authority('200156') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限'
            ]);
        }
        $userName = session('personInfo.fname');
        // 任务信息
        $samInfo = [];
        if ($illegalInfo['fillegaltypecode'] != 0){
            $samInfo['fexpressioncodes'] = $illegalInfo['fexpressioncodes'];
            $samInfo['fillegalcontent'] = $illegalInfo['fillegalcontent'];
            $expressionCodes = explode(';', $samInfo['fexpressioncodes']);
            $expressionsAndConfirmation = M('tillegal')->field('fexpression, fconfirmation')->cache(true)->where(['fcode' => ['IN', $expressionCodes]])->select();
            $samInfo['fexpressions'] = '';
            $samInfo['fconfirmations'] = '';
            foreach ($expressionsAndConfirmation as $item) {
                $samInfo['fexpressions'] .= $item['fexpression'] . ' ;; ' ;
                $samInfo['fconfirmations'] .= $item['fconfirmation'] . ' ;; ' ;
            }
        }else{
            $samInfo['fexpressioncodes'] = '';
            $samInfo['fillegalcontent'] = '';
        }

        $samInfo['fillegaltypecode'] = $illegalInfo['fillegaltypecode'];
        $samInfo['need_sync'] = 0;
        $samInfo['fmodifier'] = $userName;
        $samInfo['fmodifytime'] = date('Y-m-d H:i:s');
        $tableName = $this->sampleTableMap[strval($mediaClass)];
        if ($mediaClass == 3){
            $where = [
                'fpapersampleid' => ['IN', $samIds],
            ];
        }else{
            $where = [
                'fid' => ['IN', $samIds],
            ];
        }
        $res = M($tableName)
            ->where($where)
            ->save($samInfo);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}