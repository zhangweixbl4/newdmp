<?php
namespace Gongshang\Controller;
use Think\Controller;

class QualityCheckController extends BaseController {
	//质检任务列表
    public function qualityList(){

    	$keywords = I('keywords');
    	$random_time = I('random_time');
    	$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
    	$fregulatorid = session('regulatorpersonInfo.fregulatorid');
    	$where['tdcplan.fregulatorid'] = $fregulatorid;
    	if($keywords) $where['tdcplan.fdsplanname'] = array('like','%'.trim($keywords,' ').'%');	
    	if($random_time){
    		$starttime = date('Y-m-d 00:00:00',strtotime($random_time));
			$endtime = date('Y-m-d 00:00:00',strtotime('+1 day',strtotime($starttime)));
			$where['tdcplan.fcreatetime'] = array(array('EGT',$starttime),array('LT',$endtime),'AND');
    	}
    	$count = M('tdcplan')->where($where)->count();
    	$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
    	$qualityList = M('tdcplan')
    						->field('tdcplan.*,'.
    								'tcctask.fmediatype,'.
    								'tdctask.fmediatype as mediatype,'.
    								'tsctask.fmediatype as type,'.
    								'count(tcctask.fcctaskid) as tcc_ext_num,'.//素材抽查数量
    								'count(case when tcctask.fstate = 2 then 1 else null end) as tcc_check_num,'.//素材检查数量
    								'count(case when tcctask.fcheckresult = "正常" then 1 else null end) as tcc_qua_num,'.//素材合格数量
    								'count(distinct(tdctask.fdctaskid)) as tdc_ext_num,'.//数据抽查数量
    								'count(distinct(case when tdctask.fstate = 2 then tdctask.fdctaskid else null end)) as tdc_check_num,'.//数据检查数量
    								'count(distinct(case when tdctask.fdctaskid = tdctaskresult.fdctaskid then tdctask.fdctaskid else null end)) as tdc_unqua_num,'.//数据不合格数量
    								'count(tsctask.fsctaskid) as tsc_ext_num,'.//样本抽查数量
    								'count(case when tsctask.fstate = 2 then 1 else null end) as tsc_check_num,'.
    								'count(case when tsctask.fcheckresult = "正常" then 1 else null end) as tsc_qua_num')
    						->join('left join tcctask on tcctask.fdcplanid = tdcplan.fdcplanid')
    						->join('left join tdctask on tdctask.fdcplanid = tdcplan.fdcplanid')
    						->join('left join tsctask on tsctask.fdcplanid = tdcplan.fdcplanid')
    						->join('left join tdctaskresult on tdctaskresult.fdctaskid = tdctask.fdctaskid')
    						->where($where)
    						->group('tdcplan.fdcplanid')
    						->order('tdcplan.fcreatetime desc')
    						->limit($Page->firstRow.','.$Page->listRows)
    						->select();
		foreach ($qualityList as $key => $value) {
    		$qualityList[$key]['k'] = ($p - 1) * $pp + $key + 1;
    		if($value['tcc_ext_num'] > 0){
    			$qualityList[$key]['ext_num'] = $ext_num = $value['tcc_ext_num'];
    			$qualityList[$key]['check_num'] = $check_num = $value['tcc_check_num'];
    			$qualityList[$key]['qua_num'] = $qua_num = $value['tcc_qua_num'];
    		}
    		if($value['tdc_ext_num'] > 0){
    			$qualityList[$key]['ext_num'] = $ext_num = $value['tdc_ext_num'];
    			$qualityList[$key]['check_num'] = $check_num = $value['tdc_check_num'];
    			$qualityList[$key]['qua_num'] = $qua_num = $value['tdc_check_num'] - $value['tdc_unqua_num'];
    			$qualityList[$key]['fmediatype'] = $value['mediatype'];
    		}
    		if($value['tsc_ext_num'] > 0){
    			$qualityList[$key]['ext_num'] = $ext_num = $value['tsc_ext_num'];
    			$qualityList[$key]['check_num'] = $check_num = $value['tsc_check_num'];
    			$qualityList[$key]['qua_num'] = $qua_num = $value['tsc_qua_num'];
    			$qualityList[$key]['fmediatype'] = $value['type'];
    		}
    		
    		$qualityList[$key]['qua_rate'] = number_format($qua_num/$check_num*100,2).'%';//合格率
    		$qualityList[$key]['check_rate'] = number_format($check_num/$ext_num*100,2).'%';//检查率
    	}
    	
    	$this->assign('info_type',C('info_type'));//信息类型
    	$this->assign('qualityList',$qualityList);
    	$this->assign('page',$Page->show());//分页输出
		$this->display();
		
	}

	//质检任务列表-任务明细
	public function qualityTaskTwo(){

		$fdcplanid = I('fdcplanid');//计划ID
		$fplantype = I('fplantype',1);//信息类型
		$keywords = I('keywords');
		$media_type = I('media_type');
		$is_pass = I('is_pass');
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		if($keywords) $where['tmedia.fmedianame'] = array('like','%'.trim($keywords,' ').'%');

		switch ($fplantype) {
			case 1:
				$where['tcctask.fdcplanid'] = $fdcplanid;
				if($is_pass == 1) $where['tcctask.fcheckresult'] = '正常';
				if($is_pass == 2) $where['tcctask.fcheckresult'] = array('neq','正常');
				$count = M('tcctask')
								->join('tmedia on tmedia.fid = tcctask.fmediaid')
								->join('tdcplan on tdcplan.fdcplanid = tcctask.fdcplanid')
								->where($where)->count();
				$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
				$taskList = M('tcctask')
								->field('tcctask.*,tmedia.fmedianame,tdcplan.fcreatetime')
								->join('tmedia on tmedia.fid = tcctask.fmediaid')
								->join('tdcplan on tdcplan.fdcplanid = tcctask.fdcplanid')
								->where($where)->limit($Page->firstRow.','.$Page->listRows)->select();
				break;
			case 2:
				$where['tdctask.fdcplanid'] = $fdcplanid;
				if($is_pass == 1){
					$where['tdctaskresult.fdctaskid'] = array('exp','is null');
					$where['tdctask.fstate'] = 2;
				}
				if($is_pass == 2) $where['tdctaskresult.fdctaskid'] = array('exp','is not null');
				$count = M('tdctask')
								->join('tmedia on tmedia.fid = tdctask.fmediaid')
								->join('tdcplan on tdcplan.fdcplanid = tdctask.fdcplanid')
								->join('left join tdctaskresult on tdctaskresult.fdctaskid = tdctask.fdctaskid')
								->where($where)->count('distinct(tdctask.fdctaskid)');
				$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
				$taskList = M('tdctask')
								->field('tdctask.*,tmedia.fmedianame,tdcplan.fcreatetime,group_concat(tdctaskresult.fresulttype) as fcheckresult')
								->join('tmedia on tmedia.fid = tdctask.fmediaid')
								->join('tdcplan on tdcplan.fdcplanid = tdctask.fdcplanid')
								->join('left join tdctaskresult on tdctaskresult.fdctaskid = tdctask.fdctaskid')
								->where($where)->limit($Page->firstRow.','.$Page->listRows)->group('tdctask.fdctaskid')->select();

				break;
			case 3:
				$where['tsctask.fdcplanid'] = $fdcplanid;
				if($is_pass == 1) $where['tsctask.fcheckresult'] = '正常';
				if($is_pass == 2) $where['tsctask.fcheckresult'] = array('neq','正常');
				$count = M('tsctask')
								->join('tmedia on tmedia.fid = tsctask.fmediaid')
								->join('tad on tad.fadid = tsctask.fadid')
								->join('tdcplan on tdcplan.fdcplanid = tsctask.fdcplanid')
								->join('tillegaltype on tillegaltype.fcode = tsctask.fillegaltypecode')
								->where($where)->count();
				$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
				$taskList = M('tsctask')
								->field('tsctask.*,
										tmedia.fmedianame,
										tdcplan.fcreatetime,tdcplan.fregulator,
										tillegaltype.fillegaltype,
										tad.fadname')
								->join('tmedia on tmedia.fid = tsctask.fmediaid')
								->join('tad on tad.fadid = tsctask.fadid')
								->join('tdcplan on tdcplan.fdcplanid = tsctask.fdcplanid')
								->join('tillegaltype on tillegaltype.fcode = tsctask.fillegaltypecode')
								->where($where)->limit($Page->firstRow.','.$Page->listRows)->select();
				break;
			default:
				$taskList = array();
				$Page = new \Think\Page(0,$pp);
				break;
		}

		$taskList = list_k($taskList,$p,$pp);
		$this->assign('taskList',$taskList);
		$this->assign('page',$Page->show());//分页输出
		$this->display();

	}

	//质检任务列表-任务详情（素材）
	public function materialRandomTwo(){

		$fcctaskid = I('fcctaskid');//任务ID
		$where['tcctask.fcctaskid'] = $fcctaskid;
		$taskInfo = M('tcctask')
						->field('tcctask.*,tmedia.fmedianame,tdcplan.fcreatetime')
						->join('left join tmedia on tmedia.fid = tcctask.fmediaid')
						->join('left join tdcplan on tdcplan.fdcplanid = tcctask.fdcplanid')
						->where($where)->limit(1)->find();

		$this->assign('taskInfo',$taskInfo);
		$this->display();

	}

	//质检任务列表-任务详情（数据）
	public function dataRandomTwo(){

		$fdctaskid = I('fdctaskid');//任务ID
		$where['tdctask.fdctaskid'] = $fdctaskid;
		$taskInfo = M('tdctask')
							->field('tdctask.*,tmedia.fmedianame,tdcplan.fcreatetime')
							->join('join tmedia on tmedia.fid = tdctask.fmediaid')
							->join('join tdcplan on tdcplan.fdcplanid = tdctask.fdcplanid')
							->where($where)->limit(1)->find();
		//根据媒介类型获取素材
		switch ($taskInfo['fmediatype']) {
			case 'tv':
				$adList = M('ttvissue')
								->field('ttvissue.*,tad.fadname')
								->join('left join ttvsample on ttvsample.fid = ttvissue.ftvsampleid')
								->join('left join tad on tad.fadid = ttvsample.fadid')
								->where(array('ttvissue.fmediaid' => $taskInfo['fmediaid'],'ttvissue.fstarttime' => array('EGT',$taskInfo['fstarttime']),'ttvissue.fendtime' => array('LT',$taskInfo['fendtime'])))
								->limit(7)->select();
				break;
			case 'bc':
				$adList = M('tbcissue')
								->field('tbcissue.*,tad.fadname')
								->join('left join tbcsample on tbcsample.fid = tbcissue.fbcsampleid')
								->join('left join tad on tad.fadid = tbcsample.fadid')
								->where(array('tbcissue.fmediaid' => $taskInfo['fmediaid'],'tbcissue.fstarttime' => array('EGT',$taskInfo['fstarttime']),'tbcissue.fendtime' => array('LT',$taskInfo['fendtime'])))
								->limit(7)->select();
				break;
			case 'paper':
				$adList = M('tpaperissue')
								->field('tpaperissue.*,tad.fadname')
								->join('left join tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
								->join('left join tad on tad.fadid = tpapersample.fadid')
								->where(array('tpaperissue.fmediaid' => $taskInfo['fmediaid'],'tpaperissue.fissuedate' => array(array('EGT',$taskInfo['fstarttime'],array('LT',$taskInfo['fendtime']),'and'))))
								->limit(7)->select();
				break;
			default:
				$adList = array();
				break;
		}
		$taskResult = M('tdctaskresult')->where(array('fdctaskid' => $taskInfo['fdctaskid']))->select();

		$this->assign('taskInfo',$taskInfo);
		$this->assign('adList',$adList);
		$this->assign('taskResult',$taskResult);
		$this->display();

	}

	//质检任务列表-任务详情（样本）
	public function sampleRandomTwo(){

		$fsctaskid = I('fsctaskid');//任务ID
		$where['tsctask.fsctaskid'] = $fsctaskid;
		$taskInfo = M('tsctask')
						->field('tsctask.*,tmedia.fmedianame,tad.fadname,tillegaltype.fillegaltype')
						->join('left join tmedia on tmedia.fid = tsctask.fmediaid')
						->join('left join tad on tad.fadid = tsctask.fadid')
						->join('left join tillegaltype on tillegaltype.fcode = tsctask.fillegaltypecode')
						->where($where)->limit(1)->find();

		$this->assign('taskInfo',$taskInfo);
		$this->display();

	}

	//检查任务列表
	public function qualityTask(){

		$fstate = I('fstate');//任务状态值
		$info_type = I('info_type');//信息类型
		$keywords = I('keywords');
		$media_type = I('media_type');
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$fregulatorid = session('regulatorpersonInfo.fregulatorid');//检查机构ID
		$where['tdcplan.fregulatorid'] = $fregulatorid;
		if($keywords) $where['tmedia.fmedianame'] = array('like','%'.trim($keywords,' ').'%');

		switch ($info_type) {
			case 1:
				$where['tcctask.fstate'] = array('in',array(0,1));
				if($fstate == 2) $where['tcctask.fstate'] = 2;//状态值为2时改变
				if($media_type) $where['tcctask.fmediatype'] = $media_type;
				$count = M('tcctask')
								->join('tmedia on tmedia.fid = tcctask.fmediaid')
								->join('tdcplan on tdcplan.fdcplanid = tcctask.fdcplanid')
								->where($where)->count();
				$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
				$taskList = M('tcctask')
								->field('tcctask.*,tmedia.fmedianame,tdcplan.fcreatetime')
								->join('tmedia on tmedia.fid = tcctask.fmediaid')
								->join('tdcplan on tdcplan.fdcplanid = tcctask.fdcplanid')
								->where($where)->limit($Page->firstRow.','.$Page->listRows)->select();
				break;
			case 2:
				$where['tdctask.fstate'] = array('in',array(0,1));
				if($fstate == 2) $where['tdctask.fstate'] = 2;//状态值为2时改变
				if($media_type) $where['tdctask.fmediatype'] = $media_type;
				$count = M('tdctask')
								->join('tmedia on tmedia.fid = tdctask.fmediaid')
								->join('tdcplan on tdcplan.fdcplanid = tdctask.fdcplanid')
								->join('left join tdctaskresult on tdctaskresult.fdctaskid = tdctask.fdctaskid')
								->where($where)->count('distinct(tdctask.fdctaskid)');
				$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
				$taskList = M('tdctask')
								->field('tdctask.*,tmedia.fmedianame,tdcplan.fcreatetime,group_concat(tdctaskresult.fresulttype) as fcheckresult')
								->join('tmedia on tmedia.fid = tdctask.fmediaid')
								->join('tdcplan on tdcplan.fdcplanid = tdctask.fdcplanid')
								->join('left join tdctaskresult on tdctaskresult.fdctaskid = tdctask.fdctaskid')
								->where($where)->limit($Page->firstRow.','.$Page->listRows)->group('tdctask.fdctaskid')->select();
				break;
			case 3:
				$where['tsctask.fstate'] = array('in',array(0,1));
				if($fstate == 2) $where['tsctask.fstate'] = 2;//状态值为2时改变
				if($media_type) $where['tsctask.fmediatype'] = $media_type;
				$count = M('tsctask')
								->join('tad on tad.fadid = tsctask.fadid')
								->join('tmedia on tmedia.fid = tsctask.fmediaid')
								->join('tdcplan on tdcplan.fdcplanid = tsctask.fdcplanid')
								->join('tillegaltype on tillegaltype.fcode = tsctask.fillegaltypecode')
								->where($where)->count();
				$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
				$taskList = M('tsctask')
								->field('tsctask.*,
									tmedia.fmedianame,
									tad.fadname,
									tdcplan.fregulator,tdcplan.fcreatetime,
									tillegaltype.fillegaltype')
								->join('tad on tad.fadid = tsctask.fadid')
								->join('tmedia on tmedia.fid = tsctask.fmediaid')
								->join('tdcplan on tdcplan.fdcplanid = tsctask.fdcplanid')
								->join('tillegaltype on tillegaltype.fcode = tsctask.fillegaltypecode')
								->where($where)->limit($Page->firstRow.','.$Page->listRows)->select();
				break;
			default:
				$taskList = array();
				$Page = new \Think\Page(0,$pp);
				break;
		}
		
		$taskList = list_k($taskList,$p,$pp); 
		$this->assign('taskList',$taskList);
		$this->assign('page',$Page->show());//分页输出
		$this->display();

	}

	//素材抽检详情
	public function materialRandom(){

		$fcctaskid = I('fcctaskid');//任务ID
				$where['tcctask.fcctaskid'] = $fcctaskid;
				$taskInfo = M('tcctask')
								->field('tcctask.*,tmedia.fmedianame,tdcplan.fcreatetime')
								->join('left join tmedia on tmedia.fid = tcctask.fmediaid')
								->join('left join tdcplan on tdcplan.fdcplanid = tcctask.fdcplanid')
								->where($where)->limit(1)->find();
		
		$this->assign('taskInfo',$taskInfo);
		$this->display();

	}

	//数据抽检详情
	public function dataRandom(){
		session_write_close();
		$fdctaskid = I('fdctaskid');//任务ID
		$where['tdctask.fdctaskid'] = $fdctaskid;
		$taskInfo = M('tdctask')
							->field('tdctask.*,tmedia.fmedianame,tdcplan.fcreatetime')
							->join('left join tmedia on tmedia.fid = tdctask.fmediaid')
							->join('left join tdcplan on tdcplan.fdcplanid = tdctask.fdcplanid')
							->where($where)->limit(1)->find();
		//根据媒介类型获取素材
		switch ($taskInfo['fmediatype']) {
			case 'tv':
				$adList = M('ttvissue')
								->field('ttvissue.*,tad.fadname')
								->join('join ttvsample on ttvsample.fid = ttvissue.ftvsampleid')
								->join('join tad on tad.fadid = ttvsample.fadid')
								->where(array('ttvissue.fmediaid' => $taskInfo['fmediaid'],'ttvissue.fstarttime' => array('EGT',$taskInfo['fstarttime']),'ttvissue.fendtime' => array('LT',$taskInfo['fendtime'])))
								->limit(7)->order('ttvissue.fstarttime')->select();
				break;
			case 'bc':
				$adList = M('tbcissue')
								->field('tbcissue.*,tad.fadname')
								->join('join tbcsample on tbcsample.fid = tbcissue.fbcsampleid')
								->join('join tad on tad.fadid = tbcsample.fadid')
								->where(array('tbcissue.fmediaid' => $taskInfo['fmediaid'],'tbcissue.fstarttime' => array('EGT',$taskInfo['fstarttime']),'tbcissue.fendtime' => array('LT',$taskInfo['fendtime'])))
								->limit(7)->select();
				break;
			case 'paper':
				$adList = M('tpaperissue')
								->field('tpaperissue.*,tad.fadname')
								->join('join tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
								->join('join tad on tad.fadid = tpapersample.fadid')
								->where(array('tpaperissue.fmediaid' => $taskInfo['fmediaid'],'tpaperissue.fissuedate' => array(array('EGT',$taskInfo['fstarttime'],array('LT',$taskInfo['fendtime']),'and'))))
								->limit(7)->select();
				break;
			default:
				$adList = array();
				break;
		}

		foreach($adList as $key => $value){
			$adList[$key]['starttime'] = strtotime($value['fstarttime']);
			$adList[$key]['endtime'] = strtotime($value['fendtime']);
		}

		$this->assign('taskInfo',$taskInfo);
		$this->assign('adList',$adList);
		$this->display();

	}

	//样本抽检详情
	public function sampleRandom(){

		$fsctaskid = I('fsctaskid');//任务ID
		$where['tsctask.fsctaskid'] = $fsctaskid;
		$taskInfo = M('tsctask')
						->field('tsctask.*,tmedia.fmedianame,tad.fadname,tillegaltype.fillegaltype')
						->join('left join tmedia on tmedia.fid = tsctask.fmediaid')
						->join('left join tad on tad.fadid = tsctask.fadid')
						->join('left join tillegaltype on tillegaltype.fcode = tsctask.fillegaltypecode')
						->where($where)->limit(1)->find();
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
		$illegalType = M('tillegaltype')->select();

    	$this->assign('IllegalList',$IllegalList);//违法表现数据列表
		$this->assign('taskInfo',$taskInfo);
		$this->assign('illegalType',$illegalType);
		$this->display();

	}

	//素材任务提交
	public function ajax_submit(){

		$fcctaskid = I('fcctaskid');//任务ID
		$fdcplanid = I('fdcplanid');//计划ID
		$fcheckresult = I('fcheckresult');//检查结果
		$fcheckerror = I('fcheckerror');//异常结果
		$fcheckdescribe = I('fcheckdescribe');//检查描述

		if($fcheckresult == 1){
			$fcheckresult = '正常';
		}elseif($fcheckresult == 2){
			if(!$fcheckerror) $this->ajaxReturn(array('code'=>-1,'msg'=>'未选择异常结果'));
			$fcheckresult = implode(',',$fcheckerror);
		}else{
			$fcheckresult = '';
		}

		$task_data['fcheckresult'] = $fcheckresult;
		$task_data['fcheckdescribe'] = $fcheckdescribe;
		$task_data['fcheck'] = session('regulatorpersonInfo.fname');
		$task_data['fchecktime'] = date('Y-m-d H:i:s',time());
		$task_data['fstate'] = 2;
		$result = M('tcctask')->where(array('fcctaskid' => $fcctaskid))->save($task_data);
		if($result){			
			$this->ajaxReturn(array('code'=>0,'msg'=>'判定成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'判定失败'));
		}
		//检查数量和抽检数量
		// $count = M('tcctask')->field('count(fcctaskid) as all_num,count(case when fstate = 2 then 1 else null end) as check_num')->where(array('fdcplanid' => $fdcplanid))->limit(1)->find();
		// if($count['all_num'] == $count['check_num']) M('tdcplan')->where(array('fdcplanid' => $fdcplanid))->save(array('fstate' => 2));

	}

	//数据任务提交
	public function ajax_data_submit(){

		$fdctaskid = I('fdctaskid');//任务ID
		$fdcplanid = I('fdcplanid');//计划ID
		$fcheckresult = I('list');//检查结果
		$fcheckdescribe = I('fcheckdescribe');//检查描述

		$fdctaskInfo = M('tdctask')->where(array('fdctaskid' => $fdctaskid))->limit(1)->find();
		if($fcheckresult){
			foreach($fcheckresult as $key => $value){
				$data['fdctaskid'] = $fdctaskid;
				$data['fmediaid'] = $fdctaskInfo['fmediaid'];
				$data['fresulttype'] = $value['aderror'];
				$data['fadname'] = $value['adname'];
				$data['fstarttime'] = $value['starttime']?$value['starttime']:'';
				$data['fendtime'] = $value['endtime']?$value['endtime']:'';
				$data['fcreator'] = session('regulatorpersonInfo.fname');
				$data['fcreatetime'] = date('Y-m-d H:i:s');
				$result_data[] = $data;
			}
			M('tdctaskresult')->addAll($result_data);
		}

		$task_data['fdecribe'] = $fcheckdescribe;
		$task_data['fcheck'] = session('regulatorpersonInfo.fname');
		$task_data['fchecktime'] = date('Y-m-d H:i:s',time());
		$task_data['fstate'] = 2;
		$result = M('tdctask')->where(array('fdctaskid' => $fdctaskid))->save($task_data);
		
		if($result){
			$this->ajaxReturn(array('code'=>0,'msg'=>'提交成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'提交失败'));
		}
		

	} 

	//样本任务提交
	public function ajax_sample_submit(){

		$fsctaskid = I('fsctaskid');//任务ID
		$is_pass = I('is_pass');
		$fcheckdescribe = I('fcheckdescribe');

		if($is_pass == 1){
			$fcheckresult = '正常';
		}elseif($is_pass == 2){
			$clue_error = I('clue_error');
			$fcheckresult = implode(',',$clue_error);
			$illegal = I('illegal');
			if($illegal){
				$illegalList = M('tillegal')->where(array('fcode' => array('in',$illegal)))->select();
			}
			foreach($illegalList as $key => $value){
				$fcheckillegaltypecode = $value['fillegaltype'];
				$fcheckexpressioncodes .= $value['fcode'].';';
				$fcheckexpressions .= $value['fexpression'].';';
				$fcheckconfirmations .= $value['fconfirmation'].';';
				$fcheckpunishments .= $value['fpunishment'].';';
				$fcheckpunishmenttypes .= $value['fpunishmenttype'].';';
			}
			$task_data['fcheckillegaltypecode'] = $fcheckillegaltypecode;
			$task_data['fcheckexpressioncodes'] = rtrim($fcheckexpressioncodes,';');
			$task_data['fcheckexpressions'] = rtrim($fcheckexpressions,';');
			$task_data['fcheckconfirmations'] = rtrim($fcheckconfirmations,';');
			$task_data['fcheckpunishments'] = rtrim($fcheckconpunishments,';');
			$task_data['fcheckpunishmenttypes'] = rtrim($fcheckpunishmenttypes,';');
 		}else{
			$fcheckresult = '';
		}

		$task_data['fcheckresult'] = $fcheckresult;
		$task_data['fcheckdescribe'] = $fcheckdescribe;
		$task_data['fcheck'] = session('regulatorpersonInfo.fname');
		$task_data['fchecktime'] = date('Y-m-d H:i:s');
		$task_data['fstate'] = 2;
		$result = M('tsctask')->where(array('fsctaskid' => $fsctaskid))->save($task_data);

		if($result){
			$this->ajaxReturn(array('code'=>0,'msg'=>'提交成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'提交失败'));
		}

	}

	//任务结果
	public function taskResult(){

		$fplantype = I('info_type');//信息类型
		$keywords = I('keywords');
		$media_type = I('media_type');
		$is_pass = I('is_pass');
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		if($keywords) $where['tmedia.fmedianame'] = array('like','%'.trim($keywords,' ').'%');
		
		switch ($fplantype) {
			case 1:
				$where['tcctask.fstate'] = 2;
				if($media_type) $where['tcctask.fmediatype'] = $media_type;
				if($is_pass == 1) $where['tcctask.fcheckresult'] = '正常';
				if($is_pass == 2) $where['tcctask.fcheckresult'] = array('neq','正常');
				$count = M('tcctask')
								->join('tmedia on tmedia.fid = tcctask.fmediaid')
								->join('tdcplan on tdcplan.fdcplanid = tcctask.fdcplanid')
								->where($where)->count();
				$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
				$taskList = M('tcctask')
								->field('tcctask.*,tmedia.fmedianame,tdcplan.fcreatetime')
								->join('tmedia on tmedia.fid = tcctask.fmediaid')
								->join('tdcplan on tdcplan.fdcplanid = tcctask.fdcplanid')
								->where($where)->limit($Page->firstRow.','.$Page->listRows)->select();
				break;
			case 2:
				$where['tdctask.fstate'] = 2;
				if($media_type) $where['tdctask.fmediatype'] = $media_type;
				$where_two = array();
				if($is_pass == 1) $where['tdctaskresult.fdctaskid'] = array('exp','is null');
				if($is_pass == 2) $where['tdctaskresult.fdctaskid'] = array('exp','is not null');
				$count = M('tdctask')
								->join('tmedia on tmedia.fid = tdctask.fmediaid')
								->join('tdcplan on tdcplan.fdcplanid = tdctask.fdcplanid')
								->join('left join tdctaskresult on tdctaskresult.fdctaskid = tdctask.fdctaskid')
								->where($where)->count('distinct(tdctask.fdctaskid)');
				$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
				$taskList = M('tdctask')
								->field('tdctask.*,tmedia.fmedianame,tdcplan.fcreatetime,group_concat(tdctaskresult.fresulttype) as fcheckresult')
								->join('tmedia on tmedia.fid = tdctask.fmediaid')
								->join('tdcplan on tdcplan.fdcplanid = tdctask.fdcplanid')
								->join('left join tdctaskresult on tdctaskresult.fdctaskid = tdctask.fdctaskid')
								->where($where)
								->limit($Page->firstRow.','.$Page->listRows)->group('tdctask.fdctaskid')->select();
				break;
			case 3:
				$where['tsctask.fstate'] = 2;
				if($media_type) $where['tsctask.fmediatype'] = $media_type;
				if($is_pass == 1) $where['tsctask.fcheckresult'] = '正常';
				if($is_pass == 2) $where['tsctask.fcheckresult'] = array('neq','正常');
				$count = M('tsctask')
								->join('tad on tad.fadid = tsctask.fadid')
								->join('tmedia on tmedia.fid = tsctask.fmediaid')
								->join('tdcplan on tdcplan.fdcplanid = tsctask.fdcplanid')
								->join('tillegaltype on tillegaltype.fcode = tsctask.fillegaltypecode')
								->where($where)->count();
				$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
				$taskList = M('tsctask')
								->field('tsctask.*,
									tmedia.fmedianame,
									tad.fadname,
									tdcplan.fregulator,tdcplan.fcreatetime,
									tillegaltype.fillegaltype')
								->join('tad on tad.fadid = tsctask.fadid')
								->join('tmedia on tmedia.fid = tsctask.fmediaid')
								->join('tdcplan on tdcplan.fdcplanid = tsctask.fdcplanid')
								->join('tillegaltype on tillegaltype.fcode = tsctask.fillegaltypecode')
								->where($where)->limit($Page->firstRow.','.$Page->listRows)->select();
				break;
			default:
				$taskList = array();
				$Page = new \Think\Page(0,$pp);
				break;
		}

		$taskList = list_k($taskList,$p,$pp);
		$this->assign('taskList',$taskList);
		$this->assign('page',$Page->show());//分页输出
		$this->display();

	}

	//生成计划任务
	public function ajax_build_task(){

		$fdsplanname = I('fdsplanname');//计划名称
		$info_type = I('info_type')?I('info_type'):rand(1,3);//信息类型
		$media_type = I('media_type')?I('media_type'):array_rand(C('media_type'),1);//媒体类型
		$random_time = I('random_time')?I('random_time'):C('random_time')[rand(0,2)];//时间段
		$time_len = I('time_len')?I('time_len'):C('duration')[rand(0,2)];//时长
		$random_count = I('random_count')?I('random_count'):C('random_count')[rand(0,2)];//任务数量
		$fcondition = array(
				'fdsplanname' => $fdsplanname,
				'fplantype' => C('info_type')[$info_type],
				'media_type' => $media_type,
				'number' => $number
			);//计划条件
		$fregulatorid = session('regulatorpersonInfo.fregulatorid');//机构ID
		//$fregulatorid = 20340000;//$time_len = 0.5;$random_time = 10;$random_count = 30;
		M()->startTrans();//开启事务
		//添加计划
		$plan_data['fplantype'] = $info_type;
		$plan_data['fdsplanname'] = $fdsplanname;
		$plan_data['fcondition'] = json_encode($fcondition);
		$plan_data['fregulatorid'] = $fregulatorid;
		$plan_data['fregulator'] = session('regulatorpersonInfo.regulatorname');
		$plan_data['fcreator'] = session('regulatorpersonInfo.fname');
		$plan_data['fcreatetime'] = date('Y-m-d H:i:s',time());
		$plan_data['fstate'] = 1;
		$fdcplanid = M('tdcplan')->add($plan_data);//返回自增ID

		//1.素材质检任务 2.数据质检任务 3.样本质检任务
		switch ($info_type) {
			case 1:
				$result = $this->tcctdc_task($info_type,$fregulatorid,$fdcplanid,$media_type,$random_time,$time_len,$random_count);
				break;
			case 2:
				$result = $this->tcctdc_task($info_type,$fregulatorid,$fdcplanid,$media_type,$random_time,$time_len,$random_count);
				break;
			case 3:
				$result = $this->tsctask($fregulatorid,$fdcplanid,$media_type,$random_time,$random_count);
				break;
			default:
				$result = array();
				break;
		}

		if($fdcplanid && $result){
			M()->commit();//提交事务			
			$this->ajaxReturn(array('code'=>0,'msg'=>'生成任务成功'));
		}else{
			M()->rollback();//回滚事务
			$this->ajaxReturn(array('code'=>-1,'msg'=>'生成任务失败'));
		}

	}

	//添加素材质检任务
	public function tcctdc_task($info_type,$fregulatorid,$fdcplanid,$media_type,$random_time,$time_len,$random_count){

		$starttime = date('Y-m-d 00:00:00',time()-86400*$random_time);//开始时间
		$times = 24*$random_time/$time_len-1;		
		
		$tmedia_where['left(tmedia.fmediaclassid,2)'] = C('media_type')[$media_type];
		$tmedia_where['tregulatormedia.fregulatorcode'] = $fregulatorid;
		$tmediaidList = M('tmedia')
							->join('tregulatormedia ON tregulatormedia.fmediaid = tmedia.fid')
							->where($tmedia_where)->group('tmedia.fid')->field('tmedia.fid')->select();//获取主管机构监管的媒介ID
		if(!$tmediaidList){
			$tmediaidList = M('tmedia')
								->field('tmedia.fid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tregulator on tregulator.fregionid = tmediaowner.fregionid')
								->where(['tregulator.fid' => $fregulatorid,'left(tmedia.fmediaclassid,2)' => C('media_type')[$media_type]])->select();
		}
		if(!$tmediaidList) $this->ajaxReturn(array('code'=>-1,'msg'=>'工商局下无对应的媒介'));

		$arr = array('fdcplanid' => $fdcplanid,'fmediatype'=>$media_type);
		for ($i = 0; $i < $random_count; $i++) { 
			$random_key = array_rand($tmediaidList,1);
			$data['fmediaid'] = $tmediaidList[$random_key]['fid'];//随机获取媒体ID
			$time = rand(0,$times);
			$fstarttime = strtotime($starttime) + 3600*$time*$time_len;//随机开始时间
			$fendtime = $fstarttime + 3600*$time_len;//结束时间
			$data['fissuedate'] = date('Y-m-d 00:00:00',$fstarttime);
			$data['fstarttime'] = date('Y-m-d H:i:s',$fstarttime);
			$data['fendtime'] = date('Y-m-d H:i:s',$fendtime);
			$data['fplaybackurl'] = A('Common/Shilian','Model')->get_playback_url($data['fmediaid'],$fstarttime,$fendtime);
			if($data['fplaybackurl'] == false) $data['fplaybackurl'] = ' ';
			$task_data[] = array_merge($data,$arr);
		}
		if($info_type == 1){
			$result = M('tcctask')->addAll($task_data);
		}elseif($info_type == 2){
			$result = M('tdctask')->addAll($task_data);
		}else{
			$result = '';
		}
		
		return $result;

	}


	//添加样本质检任务
	public function tsctask($fregulatorid,$fdcplanid,$media_type,$random_time,$random_count){

		$starttime = date('Y-m-d 00:00:00',time()-86400*$random_time);//开始时间
		$endtime = date('Y-m-d 00:00:00');
		$where['tregulatormedia.fregulatorcode'] = $fregulatorid;
		$filename = 'favifilename';
		switch ($media_type) {
			case 'tv':
				$tv_where['ttvsample.fissuedate'] = array(array('EGT',$starttime),array('LT',$endtime),'and');
				$tv_where['ttvsample.finputstate'] = 2;
				$tv_where['ttvsample.finspectstate'] = 2;
				
				$sample = M('ttvsample')
								->join('tregulatormedia on tregulatormedia.fmediaid = ttvsample.fmediaid')
								->where($where)->where($tv_where)->order('rand()')->limit($random_count)->select();
				if(!$sample){
					$sample = M('ttvsample')
									->field('ttvsample.*')
									->join('tmedia on tmedia.fid = ttvsample.fmediaid')
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->join('tregulator on tregulator.fregionid = tmediaowner.fregionid')
									->where(['tregulator.fid' => $fregulatorid])->where($tv_where)->order('rand()')->limit($random_count)->select();
				}
				break;
			case 'bc':
				$bc_where['tbcsample.fissuedate'] = array(array('EGT',$starttime),array('LT',$endtime),'and');
				$bc_where['tbcsample.finputstate'] = 2;
				$bc_where['tbcsample.finspectstate'] = 2;
				$sample = M('tbcsample')
								->join('tregulatormedia on tregulatormedia.fmediaid = tbcsample.fmediaid')
								->where($where)->where($bc_where)->order('rand()')->limit($random_count)->select();
				if(!$sample){
					$sample = M('tbcsample')
									->field('tbcsample.*')
									->join('tmedia on tmedia.fid = tbcsample.fmediaid')
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->join('tregulator on tregulator.fregionid = tmediaowner.fregionid')
									->where(['tregulator.fid' => $fregulatorid])->where($bc_where)->order('rand()')->limit($random_count)->select();
				}
				break;
			case 'paper':
				$sample = M('tpapersample')
								->join('tregulatormedia on tregulatormedia.fmediaid = tpapersample.fmediaid')
								->where($where)->order('rand()')->limit($random_count)->select();
				if(!$sample){
					$sample = M('tpapersample')
									->field('tpapersample.*')
									->join('tmedia on tmedia.fid = tpapersample.fmediaid')
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->join('tregulator on tregulator.fregionid = tmediaowner.fregionid')
									->where(['tregulator.fid' => $fregulatorid])->order('rand()')->limit($random_count)->select();
				}
				$filename = 'fjpgfilename';
				break;
			default:
				$sample = array();
				break;
		}
		if(!$sample) $this->ajaxReturn(array('code'=>-1,'msg'=>'无样本记录'));
		foreach($sample as $key => $value){
			$data['fdcplanid'] = $fdcplanid;
			$data['fmediatype'] = $media_type;
			$data['fmediaid'] = $value['fmediaid'];
			$data['fissuedate'] = $value['fissuedate'];
			$data['fadid'] = $value['fadid'];
			$data['fversion'] = $value['fversion'];
			$data['fspokesman'] = $value['fspokesman'];
			$data['fadlen'] = isset($value['fadlen'])?$value['fadlen']:'';
			$data['fapprovalunit'] = $value['fapprovalunit'];
			$data['fapprovalid'] = $value['fapprovalid'];
			$data['fillegalcontent'] = $value['fillegalcontent'];
			$data['fillegaltypecode'] = $value['fillegaltypecode'];
			$data['fexpressioncodes'] = $value['fexpressioncodes'];
			$data['fexpressions'] = $value['fexpressions'];
			$data['fconfirmations'] = $value['fconfirmations'];
			$data['fpunishments'] = $value['fpunishments'];
			$data['fpunishmenttypes'] = $value['fpunishmenttypes'];
			$data[$filename] = $value[$filename];
			$data['fstate'] = 0;
			$task_data[] = $data;
		}
		
		return M('tsctask')->addAll($task_data);

	}

}