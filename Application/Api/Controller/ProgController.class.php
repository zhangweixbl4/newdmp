<?php
namespace Api\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use Think\Controller;
use Think\Exception;


class ProgController extends Controller {
	
	
	public function _initialize() {

        header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}
	
	#推送广播节目数据给王文权
	public function push_bc(){
		session_write_close();
		set_time_limit(600);
		$dataList = M('tbcprog')->where(array('push_state'=>0))->limit(100)->select();
		$wwqUrl = 'http://172.16.198.126/index/addEpgInfo';
		foreach($dataList as $data){
			#var_dump($data['fmediaid']);
			$fclasscode = $data['fclasscode'];
			if($fclasscode > 400 && $fclasscode < 500) $fclasscode -= 100;
			
			$pData = array();
			$pData['channel'] = $data['fmediaid'];
			$pData['start'] = strtotime($data['fstart']);
			$pData['title'] = $data['fcontent'];
			$pData['user'] = $data['fmodifier'];
			$pData['category'] = $fclasscode;
			
			$rr = http($wwqUrl,$pData,'POST');
			var_dump($pData,$rr);
			$req = json_decode($rr,true);
			if($req['id'] != 0){
				#var_dump($rr);
				$bb = M('tbcprog')->where(array('fprogid'=>$data['fprogid']))->save(array('push_state'=>1,'push_id'=>$req['id'],'push_req'=>$rr));
				#var_dump($bb);
				#var_dump(M('tbcprog')->getLastSql());
			}
			
			
		}
		
		echo 'end';
		
	}
	
	#推送电视节目数据给王文权
	public function push_tv(){
		session_write_close();
		set_time_limit(600);
		$dataList = M('ttvprog')->where(array('push_state'=>0))->limit(100)->select();
		$wwqUrl = 'http://172.16.198.126/index/addEpgInfo';
		foreach($dataList as $data){
			#var_dump($data['fmediaid']);
			$fclasscode = $data['fclasscode'];
			if($fclasscode > 400 && $fclasscode < 500) $fclasscode -= 100;
			
			$pData = array();
			$pData['channel'] = $data['fmediaid'];
			$pData['start'] = strtotime($data['fstart']);
			$pData['title'] = $data['fcontent'];
			$pData['user'] = $data['fmodifier'];
			$pData['category'] = $fclasscode;
			
			$rr = http($wwqUrl,$pData,'POST');
			var_dump($pData,$rr);
			$req = json_decode($rr,true);
			if($req['id'] != 0){
				#var_dump($rr);
				$bb = M('ttvprog')->where(array('fprogid'=>$data['fprogid']))->save(array('push_state'=>1,'push_id'=>$req['id'],'push_req'=>$rr));
				#var_dump($bb);
				#var_dump(M('ttvprog')->getLastSql());
			}
			
			
		}
		
		echo 'end';
		
	}
	
	
	
}