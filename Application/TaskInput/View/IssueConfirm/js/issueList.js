const issueList = {
    template: `<div v-loading="loading">
    <el-table
            :data="table"
            border
            @selection-change="handleSelectionChange"
    >
        <el-table-column
                type="selection"
                width="40">
        </el-table-column>
        <el-table-column
                prop="region"
                label="地区"
        ></el-table-column>
        <el-table-column
                label="媒介类型"
                width="80"
        >
            <template slot-scope="scope">
                {{scope.row.fmedia_class | mediaClassFilter}}
            </template>
        </el-table-column>
        <el-table-column
                prop="media_name"
                label="媒体名称"
        >
        </el-table-column>
        <el-table-column
                v-if="type == 'ad'"
                prop="fad_name"
                label="广告名称"
        >
        </el-table-column>
        <el-table-column
                v-if="type == 'ad'"
                prop="ad_class"
                label="广告类别"
        >
        </el-table-column>
        <el-table-column
                v-if="type == 'ad'"
                label="违法表现代码"
        >
            <template slot-scope="scope">
                <el-tooltip placement="top">
                    <div slot="content">
                        <ul>
                            <li v-for="(item, index) in scope.row.fexpressions" :key="index">{{item}}</li>
                        </ul>
                    </div>
                    <el-button type="text">{{scope.row.fillegal_code}}</el-button>
                </el-tooltip>
            </template>
        </el-table-column>
        <el-table-column
                label="条次"
                width="50"
        >
            <template slot-scope="scope">
                <el-button type="text" @click="showIssueList(scope.row)">{{scope.row.illegal_count}}</el-button>
            </template>
        </el-table-column>
    </el-table>
    <el-button type="info" v-if="selectedList.length > 0" @click="batchConfirm">批量确认</el-button>
    <el-pagination
            layout="total, prev, pager, next"
            @current-change="pageChange"
            :current-page="page"
            :total="total">
    </el-pagination>
    <el-dialog
            :title="currentAd.fad_name"
            :visible="showAdInfo"
            width="95%"
            :modal="false"
            :before-close="closeAdInfo"
    >
        <ad-info 
                :ad="currentAd" 
                ref="adInfo"
                @success="closeAdInfo"
                v-if="type=='ad'"
        ></ad-info>
        <issue-list
                v-else
                :table="tableData"
                :total="totalData"
                type="ad"
                :page="page"
        ></issue-list>
    </el-dialog>
</div>`,
    name: 'issueList',
    props: ['table', 'total', 'type', 'page'],
    components: {
        adInfo,
        issueList: this,
    },
    data: function () {
        return {
            currentAd: {},
            showAdInfo: false,
            loading: false,
            tableData: this.table,
            totalData: this.total,
            selectedList: []
        }
    },
    methods:{
        pageChange: function (val) {
            this.$emit('page-change', val)
        },
        showIssueList: function (row) {
            this.loading = true
            if (this.type === 'ad'){
                $.post('illegalIssueList', {
                    where: this.$root.where,
                    row: row,
                }).then((res) => {
                    if (res.code == 0){
                        this.currentAd = Object.assign(res.data, row)
                        this.showAdInfo = true
                        this.$nextTick(() => {
                            this.$refs.adInfo.processData()
                        })
                    } else {
                        this.$message.error('获取信息失败')
                    }
                    this.loading = false
                })
            }else{
                const where = JSON.parse(JSON.stringify(this.$root.where))
                where.mediaName = row.media_name
                $.post('', {
                    where: where,
                    page: 1,
                    searchType: 'ad',
                }).then((res) => {
                    this.showAdInfo = true
                    this.loading = false
                    this.tableData = res.table
                    this.totalData = +res.total
                })
            }

        },
        closeAdInfo: function () {
            this.showAdInfo = false
            this.currentAd = {}
            if (this.type=='ad'){
                this.$refs.adInfo.changeTab()
            }
            this.$emit('search-req')
        },
        handleSelectionChange: function (val) {
            this.selectedList = val
        },
        batchConfirm: function () {
            if (this.selectedList.length === 0){
                this.$message.warning('请至少选择一项')
                return false
            }
            this.$confirm('是否确认 ? ').then(() => {
                const pk = this.type === 'ad' ? 'fid' : 'fmedia_id'
                const data = {
                    ids: this.selectedList.map((v) => v[pk]).join(','),
                    type: this.type,
                    where: this.$root.where
                }
                this.loading = true
                $.post('batchConfirm', data).then((res) => {
                    this.loading = false
                    if (res.code == 0){
                        this.$message.success(res.msg)
                    } else{
                        this.$message.error(res.msg)
                    }
                    this.$emit('search-req')
                })
            }).catch(() => {})
        }
    },
    filters: {
        mediaClassFilter: function (value) {
            let res = ''
            mediaClassArr.forEach((v) => {
                if (v.value == value){
                    res = v.text
                }
            })
            return res
        }
    }
}