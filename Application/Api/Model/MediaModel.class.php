<?php
namespace Api\Model;
use Think\Exception;


class MediaModel{


    /*修改媒介发布数量*/
    public function media_issue_count($media_id,$date,$countArr){
		
		$media_id = intval($media_id);//转为int类型
		
		if(M('tmedia')->cache(true,60)->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')->where(array('tmedia.fid'=>$media_id))->count() == 0){//判断媒介是否存在
			M('media_issue_count')->where(array('media_id'=>$media_id,'issue_date'=>$date))->delete();//删除数据
			return false;
		} 
		if($date == ''){//判断是否传递日期
			$date = date('Y-m-d');
		}else{
			$date = date('Y-m-d',strtotime($date));
		}
		
		$fid = M('media_issue_count')->where(array('media_id'=>$media_id,'issue_date'=>$date))->getField('fid');
		if(intval($fid) === 0 ){
			try{//尝试新增数据
				$fid = M('media_issue_count')->add(array('media_id'=>$media_id,'issue_date'=>$date));
			}catch(Exception $e) { //如果新增失败则获取id
				$fid = M('media_issue_count')->where(array('media_id'=>$media_id,'issue_date'=>$date))->getField('fid');
			} 
		}
		
		$save_data = $countArr;

		$save_ret = M('media_issue_count')->where(array('fid'=>$fid))->save($save_data);//修改数量
		//var_dump($save_data);
		if($save_ret >= 0){
			return true;
		}else{
			return false;
		}
		
    }
	
	
	/*计算媒介发布数量*/
	public function dmp_media_issue_count($date,$media_id = 0){
		
		set_time_limit(60);
		if($date == ''){//判断是否传递日期
			$date = date('Y-m-d');
		}else{
			$date = date('Y-m-d',strtotime($date));
		}
		


		$month = date("Ym", strtotime($date));//月份
		$regionList = M('tregion')->cache(true,86400)->field('left(fid,2) as region_prefix')->where(array('right(fid,4)'=>'0000'))->select();
		
		
		
		
		foreach($regionList as $region){//循环地区
			$table_postfix = '_'.$month.'_'.$region['region_prefix'];//表后缀
			
			$dmp_tv_where = array();
			$dmp_tv_where['ttvissue.fissuedate'] = strtotime($date);
			
			
			
			
			$dmp_bc_where = array();
			$dmp_bc_where['tbcissue.fissuedate'] = strtotime($date);
			
			
			$dmp_paper_where = array();
			$dmp_paper_where['tpaperissue.fissuedate'] = $date;
			
			
			
			
			if($media_id > 0){
				$dmp_tv_where['ttvissue.fmediaid'] = $media_id;
				$dmp_bc_where['tbcissue.fmediaid'] = $media_id;
				$dmp_paper_where['tpaperissue.fmediaid'] = $media_id;
				
			}
			$adclass_data = array();//初始化广告分类数据
			try{
				$dmp_tv = M('ttvissue'.$table_postfix)//按媒体查询
								->alias('ttvissue')
								->field('
										ttvissue.fmediaid,
										count(*) as issue_count,
										
										sum(ttvissue.flength) as issue_duration,
										count(case when ttvsample.fillegaltypecode = 20 then 1 else null end) illegal_20_count,
										
										count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) illegal_30_count,
										
										sum(case when ttvsample.fillegaltypecode = 20 then ttvissue.flength else 0 end) as illegal_20_duration,
										sum(case when ttvsample.fillegaltypecode = 30 then ttvissue.flength else 0 end) as illegal_30_duration,
										ttvissue.fregionid
										
										')
								->join('ttvsample on ttvsample.fid = ttvissue.fsampleid')		
								->where($dmp_tv_where)
								->group('fmediaid')
								->select();
				//var_dump(M('ttvissue'.$table_postfix)->getLastSql());				
				$dmp_tv_ad_class = M('ttvissue'.$table_postfix)//按广告分类查询
								->alias('ttvissue')
								->field('
										ttvissue.fmediaid,
										left(tad.fadclasscode,2) as ad_class_code,
										count(*) as issue_count,
										
										sum(ttvissue.flength) as issue_duration,
										count(case when ttvsample.fillegaltypecode = 20 then 1 else null end) illegal_20_count,
										
										count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) illegal_30_count,
										
										sum(case when ttvsample.fillegaltypecode = 20 then ttvissue.flength else 0 end) as illegal_20_duration,
										sum(case when ttvsample.fillegaltypecode = 30 then ttvissue.flength else 0 end) as illegal_30_duration
										
										')
								->join('ttvsample on ttvsample.fid = ttvissue.fsampleid')
								->join('tad on tad.fadid = ttvsample.fadid')
								->where($dmp_tv_where)
								->group('fmediaid,left(tad.fadclasscode,2)')
								->select();
				
				foreach($dmp_tv_ad_class as $ad_class_data){
					$adclass_data[$ad_class_data['fmediaid']][] = $ad_class_data;//分类数据重新组装
				}
				
				
			}catch(Exception $error) {
				$dmp_tv = array();
			} 
			
			try{ 
				$dmp_bc = M('tbcissue'.$table_postfix)
								->alias('tbcissue')
								->field('
										tbcissue.fmediaid,
										count(*) as issue_count,
										
										sum(tbcissue.flength) as issue_duration,
										count(case when tbcsample.fillegaltypecode = 20 then 1 else null end) illegal_20_count,
										
										count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) illegal_30_count,
										
										sum(case when tbcsample.fillegaltypecode = 20 then tbcissue.flength else 0 end) as illegal_20_duration,
										sum(case when tbcsample.fillegaltypecode = 30 then tbcissue.flength else 0 end) as illegal_30_duration,
										tbcissue.fregionid
										')
								->join('tbcsample on tbcsample.fid = tbcissue.fsampleid')		
								->where($dmp_bc_where)
								->group('fmediaid')
								->select();
				$dmp_bc_ad_class = M('tbcissue'.$table_postfix)//按广告分类查询
								->alias('tbcissue')
								->field('
										tbcissue.fmediaid,
										left(tad.fadclasscode,2) as ad_class_code,
										count(*) as issue_count,
										
										sum(tbcissue.flength) as issue_duration,
										count(case when tbcsample.fillegaltypecode = 20 then 1 else null end) illegal_20_count,
										
										count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) illegal_30_count,
										
										sum(case when tbcsample.fillegaltypecode = 20 then tbcissue.flength else 0 end) as illegal_20_duration,
										sum(case when tbcsample.fillegaltypecode = 30 then tbcissue.flength else 0 end) as illegal_30_duration
										
										')
								->join('tbcsample on tbcsample.fid = tbcissue.fsampleid')
								->join('tad on tad.fadid = tbcsample.fadid')
								->where($dmp_bc_where)
								->group('fmediaid,left(tad.fadclasscode,2)')
								->select();
				
				foreach($dmp_bc_ad_class as $ad_class_data){
					$adclass_data[$ad_class_data['fmediaid']][] = $ad_class_data;//分类数据重新组装
				}
								
			}catch(Exception $error) { 
				//var_dump($error);
				$dmp_bc = array();
			} 	 

			try{ 
				$dmp_paper_1 = M('tpaperissue')
								
								->field('
										tpaperissue.fmediaid,
										count(*) as issue_count,
										
										0 as issue_duration,
										count(case when tpapersample.fillegaltypecode = 20 then 1 else null end) illegal_20_count,
										
										count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) illegal_30_count,
										
										0 as illegal_20_duration,
										0 as illegal_30_duration
										')
								->join('tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')		
								->where($dmp_paper_where)
								->group('fmediaid')
								->select();
				$dmp_paper = array();				
				foreach($dmp_paper_1 as $dmp_paper_0){
					$dmp_paper_0['fregionid'] = M('tmedia')->cache(true,86400)->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')->where(array('tmedia.fid'=>$dmp_paper_0['fmediaid']))->getField('tmediaowner.fregionid');
					$dmp_paper[] = $dmp_paper_0;
				}				
				$dmp_paper_ad_class = M('tpaperissue')//按广告分类查询
								
								->field('
										tpaperissue.fmediaid,
										left(tad.fadclasscode,2) as ad_class_code,
										count(*) as issue_count,
										
										0 as issue_duration,
										count(case when tpapersample.fillegaltypecode = 20 then 1 else null end) illegal_20_count,
										
										count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) illegal_30_count,
										
										0 as illegal_20_duration,
										0 as illegal_30_duration
										
										')
								->join('tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
								->join('tad on tad.fadid = tpapersample.fadid')
								->where($dmp_paper_where)
								->group('fmediaid,left(tad.fadclasscode,2)')
								->select();
				
				foreach($dmp_paper_ad_class as $ad_class_data){
					$adclass_data[$ad_class_data['fmediaid']][] = $ad_class_data;//分类数据重新组装
				}
				//var_dump($dmp_paper);			
			}catch(Exception $error) { 

				$dmp_paper = array();
			} 	 
								
			$issueList = array_merge($dmp_tv,$dmp_bc,$dmp_paper);
			

			//var_dump($issueList);
			foreach($issueList as $issue){
				if(intval($issue['fregionid']) > 0){
					$this->media_issue_count($issue['fmediaid'],$date,array(
																			'issue_count'=>intval($issue['issue_count']),
																			
																			'illegal_20_count'=>intval($issue['illegal_20_count']),
																			
																			'illegal_30_count'=>intval($issue['illegal_30_count']),
																			
																			'issue_duration'=>intval($issue['issue_duration']),
																			'illegal_20_duration'=>intval($issue['illegal_20_duration']),
																			'illegal_30_duration'=>intval($issue['illegal_30_duration']),
																			'adclass_data'=>json_encode($adclass_data[$issue['fmediaid']]),
																			'fregionid'=>$issue['fregionid'],
																			
																			));
				
				}
			}

			
		}//循环地区结束
		
		
		return true;

	}
	
	
	/*样本与发布记录映射关系*/
	public function sam_relation_issue($type,$samId,$tableName){
		
		if(!in_array($type,array('tv','bc'))){//判断是否是电视、广播
			return false;
		}
		
		if(intval($samId <= 0)){//判断是否有传值样本id
			return false;
		}
		
		
		if(S('sam_relation_issue_2_'.$type.'_'.$samId.'_'.$tableName) == 'true'){//判断是否存在缓存
			return true;
		}else{
			
			$issue_relation = M('t'.$type.'sample')->where(array('fid'=>$samId))->getField('issue_relation');//查询关系
			$issue_relation_arr = explode(',',$issue_relation);//转为数组
			if(!in_array($tableName,$issue_relation_arr)){//判断数组是否包含
				if($issue_relation){//判断原来是否包含判断
					M('t'.$type.'sample')->where(array('fid'=>$samId))->save(array('issue_relation'=>array('exp','concat(issue_relation, "'.$tableName.',")')));
					//file_put_contents('LOG/SYNSAM',M('t'.$type.'sample')->getLastSql(),FILE_APPEND);
				}else{
					M('t'.$type.'sample')->where(array('fid'=>$samId))->save(array('issue_relation'=>$tableName.','));
					//file_put_contents('LOG/SYNSAM2',M('t'.$type.'sample')->getLastSql(),FILE_APPEND);
				}
			}
			
			
			S('sam_relation_issue_2_'.$type.'_'.$samId.'_'.$tableName,'true',86400*10);
			
			return true;
		}
		
		
		
		
	}
	
	
	/*查询执行标签的媒介*/
	public function s_label_media($medialabel,$s_media_class){


		$where = array();
		

			
		$mediaIdList = M('tmedialabel')
									->cache(true,60)
									->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
									->where(array('tlabel.flabel'=>$medialabel))
									->getField('fmediaid',true);
						
		if($mediaIdList){
			$where['tmedia.fid'] = array('in',$mediaIdList);	
		}else{
			$where['tmedia.fid'] = 0;
		}							
		
		
		
		
		$where['tmedia.priority'] = array('egt',0);


		if($s_media_class){
			$where['left(tmedia.fmediaclassid,2)'] = $s_media_class;
			
		}
		
		
		$mediaList = M('tmedia')
							->cache(true,60)
							->field('tmedia.fid')
							->where($where)->limit(10000)->select();
		$mediaIdList = array();					
		foreach($mediaList as $mediaInfo){
			$mediaIdList[] = $mediaInfo['fid'];
		}		
		
		return $mediaIdList;
		
	}
	

	
	
	/*修改媒介发布数量*/
    public function tbn_ad_summary_day_do($media_id,$date,$fad_class_code,$countArr){
		
		
		$confirm_state = A('Common/IllegalAdZongju','Model')->zongjuDataConfirm($media_id,$date);//该数据的确认状态
		
		
		$mediaInfo = M('tmedia')
								->cache(true,3600)
								->field('left(tmedia.fmediaclassid,2) as fmedia_class_code,tmedia.fmediaownerid,tmediaowner.fregionid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>$media_id))
								->find();
		
		if(!$mediaInfo){//判断媒介是否存在
			M('tbn_ad_summary_day')->where(array('fmediaid'=>$media_id,'fdate'=>$date,'fad_class_code'=>$fad_class_code))->delete();//删除数据
			//var_dump( M('tbn_ad_summary_day')->getLastSql());
			return false;
		} 

		
		
		$fid = S('tbn_ad_summary_day_'.$media_id.'_'.$date.'_'.$fad_class_code);
		if(intval($fid) === 0 ){
			
			$summaryInfo = M('tbn_ad_summary_day')->field('fid,confirm_state')->where(array('fmediaid'=>$media_id,'fdate'=>$date,'fad_class_code'=>$fad_class_code))->find();
			$fid = $summaryInfo['fid'];
			
		}

		if(intval($fid) === 0 ){
			try{//尝试新增数据
				$fid = M('tbn_ad_summary_day')->add(array('fmediaid'=>$media_id,'fdate'=>$date,'fad_class_code'=>$fad_class_code));
				if($fid) S('tbn_ad_summary_day_'.$media_id.'_'.$date.'_'.$fad_class_code,$fid,5);
			}catch(Exception $e) { //如果新增失败则获取id
				$summaryInfo = M('tbn_ad_summary_day')->field('fid,confirm_state')->where(array('fmediaid'=>$media_id,'fdate'=>$date,'fad_class_code'=>$fad_class_code))->find();
				$fid = $summaryInfo['fid'];
			} 
		}
		
		if(intval($summaryInfo['confirm_state']) > 0 && $confirm_state > 0 ){//判断如果是该数据已经确认过，且该天是已确认的，则直接返回false
			file_put_contents('LOG/tbn_ad_summary_day_do_'.date('Y-m-d').'.log',date('H:i:s').'	'.$media_id.'	'.$date.'	'.$fad_class_code.'	未汇总'."\n",FILE_APPEND);
			return false;
		}else{
			file_put_contents('LOG/tbn_ad_summary_day_do_'.date('Y-m-d').'.log',date('H:i:s').'	'.$media_id.'	'.$date.'	'.$fad_class_code.'	已汇总'."\n",FILE_APPEND);
		}
		
		
		
		$countArr['fmedia_class_code'] = $mediaInfo['fmedia_class_code'];
		$countArr['fmediaownerid'] = $mediaInfo['fmediaownerid'];
		$countArr['fregionid'] = $mediaInfo['fregionid'];
		$countArr['confirm_state'] = $confirm_state;
		
		$save_ret = M('tbn_ad_summary_day')->where(array('fid'=>$fid))->save($countArr);//修改数量
		//var_dump(M('tbn_ad_summary_day')->getLastSql());
		if($save_ret >= 0){
			return true;
		}else{
			return false;
		}
		
    }


    /**
     * 获取下一个媒体id
     */
    public function getNextMediaId($id = '0')
    {
        $res = M('tmedia')
            ->where([
                'fid' => ['GT', $id],
                'fstate' => ['EQ', 1],
                'LEFT(fmediaclassid, 2)' => ['IN', '01,02,03'],
				#'_string' => 'fid in (select fmediaid from tregulatormedia where fregulatorcode = 20320100)'
            ])
            ->order('fid ASC')
            ->getField('fid');
		#file_put_contents('LOG\GETN',M('tmedia')->getLastSql() . "\n",FILE_APPEND);
		#var_dump(M('tmedia')->getLastSql());
        if (!$res){
            return 0;
        }
        return $res;
    }
	

}



