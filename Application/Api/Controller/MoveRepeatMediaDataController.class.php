<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;

class MoveRepeatMediaDataController extends Controller {
	
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}	
	

	
	public function move_repeat_media_data(){
		session_write_close();
		set_time_limit(80);
		$month = I('month');//月份，例如2018-04-01表示4月
		
		if($month){
			$this->move_repeat_media_data_do($month);	
		}
	
		
		
		
		
	}
	
	
	/*处理移动数据*/
	private function move_repeat_media_data_do($datemonth){
		echo $datemonth . "\n";
		
		$mediaList = M('tmedia')
								->cache(true,3600*12)
								->field('	
											main_media_id,
											fmedianame,
											count(*) as count
											')
								->where(array('fmedianame'=>array('neq',''),'left(fmediaclassid,2)'=>array('in',array('01','02'))))
								->group('main_media_id')
								->having('count(*) > 1')
								->select();
								

		
		//exit;
		$startDate = $datemonth;//查询月的开始日期
		$endDate = date('Y-m-d',strtotime("+1 months", strtotime($datemonth)) - 1);//查询月的结束日期

		
		foreach($mediaList as $media){
			//var_dump($media);
			$mediaInfo = M('tmedia')
									->cache(true,3600*6)
									->field('
											tmediaowner.fregionid,
											left(tmedia.fmediaclassid,2) as media_class
												
												')
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->where(array('tmedia.fid'=>$media['main_media_id']))
									->find();//查询媒介信息
			//var_dump($mediaInfo);						
			$other_media_id_list = M('tmedia')
											->cache(true,3600*6)
											->where(array('tmedia.main_media_id'=>$media['main_media_id'],'tmedia.fid'=>array('neq',$media['main_media_id'])))
											->getField('fid',true);//查询除最高优先级外的其他媒介id列表
			//var_dump($media['fid']);//优先级最高的媒介id
			//var_dump($other_media_id_list);//除优先级最高的媒介以为的媒介id
			
			
			
			if($mediaInfo['media_class'] == '01'){
				
				$table_name = 'ttvissue_'.date('Ym',strtotime($datemonth)).'_'.substr($mediaInfo['fregionid'],0,2);
				$e_num_1 = M('ttvissue')
									->where(array('fissuedate'=>array('between',array($startDate,$endDate)),'fmediaid'=>array('in',$other_media_id_list)))
									->save(array('fmediaid'=>$media['main_media_id']));
									
				try {
					$e_num_2 = M($table_name)
									->where(array('fissuedate'=>array('between',array(strtotime($startDate),strtotime($endDate))),'fmediaid'=>array('in',$other_media_id_list)))
									->save(array('fmediaid'=>$media['main_media_id']));
									
								
				} catch (Exception $e) {
					$e_num_2 = 0;
				}

				if(	$e_num_1 > 0)	echo $media['fmedianame'].'	 大表迁移了	'.$e_num_1.	' 条记录'."\n";			
				if(	$e_num_1 > 0)	echo $media['fmedianame'].'	 分表迁移了	'.$e_num_2.	' 条记录'."\n";	
				//if($datemonth == date('Y-m-01')){
					$e_num_3 = M('ttvsample')->where(array('fmediaid'=>array('in',$other_media_id_list)))->save(array('fmediaid'=>$media['main_media_id']));
					if($e_num_3 > 0) echo $media['fmedianame'].'	 样本表迁移了	'.$e_num_3.	' 条记录'."\n";	
				//}
				
				
			}elseif($mediaInfo['media_class'] == '02'){
				
				$table_name = 'tbcissue_'.date('Ym',strtotime($datemonth)).'_'.substr($mediaInfo['fregionid'],0,2);
				
				$e_num_1 = M('tbcissue')
									->where(array('fissuedate'=>array('between',array($startDate,$endDate)),'fmediaid'=>array('in',$other_media_id_list)))
									->save(array('fmediaid'=>$media['main_media_id']));
				try {
					$e_num_2 = M($table_name)
									->where(array('fissuedate'=>array('between',array(strtotime($startDate),strtotime($endDate))),'fmediaid'=>array('in',$other_media_id_list)))
									->save(array('fmediaid'=>$media['main_media_id']));
				} catch (Exception $e) {
					$e_num_2 = 0;
				}

				if(	$e_num_1 > 0)	echo $media['fmedianame'].'	 大表迁移了	'.$e_num_1.	' 条记录'."\n";			
				if(	$e_num_1 > 0)	echo $media['fmedianame'].'	 分表迁移了	'.$e_num_2.	' 条记录'."\n";	
				//if($datemonth == date('Y-m-01')){
					$e_num_3 = M('tbcsample')->where(array('fmediaid'=>array('in',$other_media_id_list)))->save(array('fmediaid'=>$media['main_media_id']));
					if($e_num_3 > 0) echo $media['fmedianame'].'	 样本表迁移了	'.$e_num_3.	' 条记录'."\n";	
				//}
				
				
			}
			
			
		}
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}