<?php
//样本控制器
namespace Admin\Controller;
use Think\Controller;
use Think\Page;
use OpenSearch\Client\OpenSearchClient;
use OpenSearch\Client\DocumentClient;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;

class IssueController extends BaseController {

	public function index(){
		if(I('media_class') == '' ) $_GET['media_class'] = '01';
		
		if(I('fstarttime_s') == '') $_GET['fstarttime_s'] = date('Y-m-d');

		if(I('fstarttime_e') == '') $_GET['fstarttime_e'] = date('Y-m-d');

        $data = $_POST;
		$media_class = I('media_class');//媒体类型，01电视，02广播，03报纸

		if($media_class == '01'){
			A('Admin/Tvissue')->index($data);
		}elseif($media_class == '02'){
			A('Admin/Bcissue')->index($data);
		}elseif($media_class == '03'){
			A('Admin/Paperissue')->index($data);
		}
		
		$this->display();
	}

    /*
    * 数据导出
    * by zw
    */
    public function outIssueXls($data = [],$mclass = '01'){
        $outdata['datalie'] = [
          '序号'=>'key',
          '广告名称'=>'fadname',
          '广告主'=>'fadownername',
          '广告内容类别'=>'fadclassfullname',
          '发布媒体'=>'fmedianame',
          '媒体类型'=>[
            'type'=>'zwif',
            'data'=>[
              ['{mclass} == "01"','电视'],
              ['{mclass} == "02"','广播'],
              ['{mclass} == "03"','报纸'],
            ]
          ],
          '发布日期'=>'issuedate',
          '开始时间'=>[
            'type'=>'zwif',
            'data'=>[
              ['{mclass} == "01" || {mclass} == "02"','{starttime}'],
              ['',''],
            ]
          ],
          '结束时间'=>[
            'type'=>'zwif',
            'data'=>[
              ['{mclass} == "01" || {mclass} == "02"','{endtime}'],
              ['',''],
            ]
          ],
          '时长/版面'=>[
            'type'=>'zwif',
            'data'=>[
              ['{mclass} == "01" || {mclass} == "02"','{flength}'],
              ['{mclass} == "03"','{fpage}'],
              ['',''],
            ]
          ],
          '违法类型'=>'fillegaltype',
          '违法表现代码'=>'fexpressioncodes',
          '违法表现'=>'fexpressions',
          '涉嫌违法内容'=>'fillegalcontent',
          '素材地址'=>'ysscurl',
        ];
        if($mclass == '03'){
            unset($outdata['datalie']['开始时间']);
            unset($outdata['datalie']['结束时间']);
        }

        $outdata['lists'] = $data;
        $ret = A('Api/Function')->outdata_xls($outdata);

        return $ret;
    }

    /**
     * 总局的确认页面
     */
    public function zongjuList()
    {
        $this->zongjuListAuth();
        if (IS_POST){
            $page = I('page', 1);
            $total = M('zongju_data_confirm')
                ->count();
            $data = M('zongju_data_confirm')
                ->order('fmonth desc')
                ->page($page, 12)
                ->select();
            $dmp_confirm_stateArr = ['初始', 'DMP已确认'];
            foreach ($data as &$row) {
                $row['dmp_confirm_state_text'] = $dmp_confirm_stateArr[$row['dmp_confirm_state']];
                $row['dmp_confirm_time'] = date('Y-m-d H:i:s', $row['dmp_confirm_time']);
                if ($row['gather_group']){
                    $tempWhere = [
                        'fgroup' => ['EQ', $row['gather_group']]
                    ];
                    $gatherTotal = M('z_gather_task')->where($tempWhere)->count();
                    $tempWhere['fstate'] = ['EQ', 1];
                    $complateNum = M('z_gather_task')->where($tempWhere)->count();
                    $row['gatherRate'] = round($complateNum / $gatherTotal, 2) * 100 . '%';
                }
            }
            $this->ajaxReturn([
                'code' => 0,
                'data' => $data,
                'total' => $total,
            ]);

        }else{
            $this->display();
        }
	}

    /**
     * 查看确认日志
     * @param $id
     * @param $type int  1:DMP确认日志  2:总局确认日志
     */
    public function zongjuConfirmLog($id, $type)
    {
        $this->zongjuListAuth();
        $logField = $type == 1 ? 'affirm_flow' : 'zj_affirm_flow';
        $flow = M('zongju_data_confirm')->find($id)[$logField];
        $flow = explode(' ;; ', $flow);
        $flow = array_filter($flow);
        $this->assign(compact('flow'));
        $this->display();
	}

    /**
     * 总局确认的权限限制
     * @param string $type 默认是get, 可填ajax
     * @return bool
     */
    private function zongjuListAuth($type='get')
    {
        if(A('Admin/Authority','Model')->authority('300941') === 0){
            if ($type == 'get'){
                echo '<h2>无权限查看此页面, 请联系管理员</h2>';
                exit();
            }elseif ($type == 'ajax'){
                $this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
            }
        }
	}

    /**
     * 新增抽查任务
     * @param $month    string  YYYY-mm-dd
     * @param $condition    string      json格式的抽查条件
     */
    public function addTask($month, $condition)
    {
        $this->zongjuListAuth('ajax');
        $data = [
            'fmonth' => $month,
            'condition' => $condition,
        ];
        $hasMonth = M('zongju_data_confirm')->where(['fmonth' => ['EQ', $month]])->count() != 0;
        if ($hasMonth){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '这个月已经有抽查任务,请修改这个月抽查条件或者选择其他月份',
            ]);
        }
        $res = M('zongju_data_confirm')->add($data) === false ? false : true;
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '新增成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '新增失败'
            ]);
        }
    }

    /**
     * 修改抽查任务条件
     * @param $id
     * @param $condition
     */
    public function editTask($id, $condition)
    {
        $this->zongjuListAuth('ajax');
        $taskInfo = M('zongju_data_confirm')->find($id);
        $dmp_confirm_content = json_decode($taskInfo['dmp_confirm_content'], true);
        $zj_confirm_content = json_decode($taskInfo['zongju_confirm_content'], true);
        // 分两种情况考虑, 一种是没有确认内容的, 直接修改; 一种是有确认内容的,需要同时修改确认内容
        if (!$dmp_confirm_content && !$zj_confirm_content){
            $res = M('zongju_data_confirm')
                ->where(['fid' => ['EQ', $id]])
                ->save(['condition' => $condition]) === false ? false : true;
            if ($res){
                $this->ajaxReturn([
                    'code' => 0,
                    'msg' => '修改成功'
                ]);
            }else{
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '修改失败'
                ]);
            }
        }else{
            $saveData = [
                'condition' => $condition
            ];
            $monthPrefix = substr($taskInfo['fmonth'], 0, 8);
            $oldCondition = json_decode($taskInfo['condition'], true);
            $oldConditionArr = [];
            foreach ($oldCondition as $key1 => $value1) {
                $oldConditionArr[$key1] = [];
                foreach ($value1 as $key2 => $value2) {
                    $oldConditionArr[$key1][$key2] = $monthPrefix . $value2;
                }
            }
            $newCondition = json_decode($condition, true);
            $newConditionArr = [];
            foreach ($newCondition as $key1 => $value1) {
                $newConditionArr[$key1] = [];
                foreach ($value1 as $key2 => $value2) {
                    $newConditionArr[$key1][$key2] = $monthPrefix . $value2;
                }
            }

            if ($dmp_confirm_content){
                $temp = [];
                $saveData['dmp_confirm_content'] = [];
                // 此操作只涉及到日期, 所以确认数据照旧, 只是针对日期进行相对应的修改
                $classArr = ['tv', 'bc', 'paper'];
                foreach ($classArr as $key1 => $class) {
                    // 先处理旧的确认内容, 结构改成 date => selected的形式
                    foreach ($dmp_confirm_content[$class] as $key2 => $oldContentItem) {
                        $temp[$class][$oldContentItem['date']] = $oldContentItem['selected'];
                    }
                    // 处理新条件
                    foreach ($newConditionArr[$class] as $key2 => $newDate) {
                        // 如果新条件在旧条件里面, 就挪过来, 如果没有, 就新建一项并且selected内容为空数组
                        if (in_array($newDate, $oldConditionArr[$class])){
                            $saveData['dmp_confirm_content'][$class][] = [
                                'date' => $newDate,
                                'selected' => $temp[$class][$newDate]
                            ];
                        }else{
                            $saveData['dmp_confirm_content'][$class][] = [
                                'date' => $newDate,
                                'selected' => []
                            ];
                        }
                    }
                }
                $saveData['dmp_confirm_content'] = json_encode($saveData['dmp_confirm_content']);
            }
            if ($zj_confirm_content){
                $temp = [];
                $saveData['zongju_confirm_content'] = [];
                // 此操作只涉及到日期, 所以确认数据照旧, 只是针对日期进行相对应的修改
                $classArr = ['tv', 'bc', 'paper'];
                foreach ($classArr as $key1 => $class) {
                    // 先处理旧的确认内容, 结构改成 date => selected的形式
                    foreach ($zj_confirm_content[$class] as $key2 => $oldContentItem) {
                        $temp[$class][$oldContentItem['date']] = $oldContentItem['selected'];
                    }
                    // 处理新条件
                    foreach ($newConditionArr[$class] as $key2 => $newDate) {
                        // 如果新条件在旧条件里面, 就挪过来, 如果没有, 就新建一项并且selected内容为空数组
                        if (in_array($newDate, $oldConditionArr[$class])){
                            $saveData['zongju_confirm_content'][$class][] = [
                                'date' => $newDate,
                                'selected' => $temp[$class][$newDate]
                            ];
                        }else{
                            $saveData['zongju_confirm_content'][$class][] = [
                                'date' => $newDate,
                                'selected' => []
                            ];
                        }
                    }
                }
                $saveData['zongju_confirm_content'] = json_encode($saveData['zongju_confirm_content']);
            }
            $res = M('zongju_data_confirm')
                ->where(['fid' => ['EQ', $id]])
                ->save($saveData) === false ? false : true;
            if ($res){
                $this->ajaxReturn([
                    'code' => 0,
                    'msg' => '修改成功'
                ]);
            }else{
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '修改失败'
                ]);
            }
        }

    }


    /**
     * 获取单条任务信息
     * @param $id
     */
    public function getAllDays($id)
    {
        $this->zongjuListAuth('ajax');
        $data = M('zongju_data_confirm')->find($id);
        $this->ajaxReturn([
            'code' => 0,
            'data' => $data
        ]);
    }

    /**
     * dmp确认修改
     * @param $id
     * @param $content
     */
    public function DMPConfirm($id, $content)
    {
        M()->startTrans();
        $this->zongjuListAuth('ajax');
        $info = M('zongju_data_confirm')->find($id);
        $userName = session('personInfo.fname');
        $date = date('Y-m-d H:i:s');
        $flow = $info['affirm_flow'] . "$date, $userName 修改了DMP确认 ;; ";
        // 处理DMP取消了在总局确认中已经确认的媒体
//        $classArr = ['tv', 'bc', 'paper'];
//        $zj_confirm = json_decode($info['zongju_confirm_content'], true);
//        $dmp_confirm = json_decode($content, true);
//        $dmp_temp = [];
//        foreach ($classArr as $class) {
//            foreach ($dmp_confirm[$class] as $key2 => $contentItem) {
//                $dmp_temp[$class][$contentItem['date']] = $contentItem['selected'];
//            }
//            // 如果总局确认已确认的某一项不在dmp确认中, 就去掉它
//            foreach ($zj_confirm[$class] as $zj_content_index => $zj_item) {
//                foreach ($zj_item['selected'] as $selected_index => $zj_selected_id) {
//                    if (!in_array($zj_selected_id, $dmp_temp[$class][$zj_item['date']])){
//                        array_splice($zj_confirm[$class][$zj_content_index]['selected'], $selected_index, 1);
//                    }
//                }
//            }
//        }
//        $zj_confirm = json_encode($zj_confirm);
//        if ($zj_confirm === 'null'){
//            $zj_confirm = null;
//        }
        $res = M('zongju_data_confirm')
            ->where(['fid' => ['EQ', $id]])
            ->save([
                'dmp_confirm_content' => $content,
//                'zongju_confirm_content' => $zj_confirm,
                'affirm_flow' => $flow
            ]) === false ? false : true;
        if ($res && $this->editIllegalIssueState($id)){
            M()->commit();
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '确认成功'
            ]);
        }else{
            M()->rollback();
            $this->ajaxReturn([
                'code' => -1,
                'data' => '确认失败'
            ]);
        }
    }

    /**
     * 修改违法记录的发布状态
     * @param $id
     * @return bool
     */
    private function editIllegalIssueState($id)
    {
        $row = M('zongju_data_confirm')->find($id);
        $row['dmp_confirm_content'] = json_decode($row['dmp_confirm_content'], true);
        $row['zj_confirm_content'] = json_decode($row['zj_confirm_content'], true);
        foreach ($row['dmp_confirm_content'] as $class) {
            foreach ($class as $date) {
                foreach ($date['selected'] as $mediaId) {
                    $adList = M('tbn_illegal_ad_issue')
                        ->field('fid, fsend_status, fsend_log')
                        ->where([
                            'fissue_date' => ['EQ', $date['date']],
                            'fmedia_id' => ['EQ', $mediaId]
                        ])
                        ->select();
                    foreach ($adList as $item) {
                        if ($item['fsend_status'] == 0){
                            $res = M('tbn_illegal_ad_issue')
                                ->where(['fid' => ['EQ', $item['fid']]])
                                ->save([
                                    'fsend_status' => 1,
                                    'fsend_log' => $item['fsend_log'].'【DMP后台】'.session('personInfo.fname').'(ID:'.session('personInfo.fid').') 于'.date('Y-m-d H:i:s').'DMP确认媒体；'
                                ]) === false ? false : true;
                            if (!$res){
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * 总局确认修改
     * @param $id
     * @param $content
     */
    public function ZjConfirm($id, $content)
    {
        return false;
        M()->startTrans();
        $this->zongjuListAuth('ajax');
        $info = M('zongju_data_confirm')->find($id);
        $userName = session('personInfo.fname');
        $date = date('Y-m-d H:i:s');
        $flow = $info['zj_affirm_flow'] . "$date, $userName 修改了总局确认 ;; ";
        $res = M('zongju_data_confirm')
            ->where(['fid' => ['EQ', $id]])
            ->save([
                'zongju_confirm_content' => $content,
                'zj_affirm_flow' => $flow
            ]) === false ? false : true;
        if ($res && $this->editIllegalIssueState($id)){
            M()->commit();
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '确认成功'
            ]);
        }else{
            M()->rollback();
            $this->ajaxReturn([
                'code' => -1,
                'data' => '确认失败'
            ]);
        }
    }

    public function isConfirm($date, $mediaId)
    {
        $issueSubQuery = M('tbn_illegal_ad_issue')
            ->field('fillegal_ad_id')
            ->where([
                'fmedia_id' => $mediaId,
                'fissue_date' => $date,
            ])
            ->group('fillegal_ad_id')
            ->buildSql();
        $data = M('tbn_illegal_ad')
            ->field('fexamine')
            ->where([
                'fid' => ['EXP', 'IN '.$issueSubQuery]
            ])
            ->group('fexamine')
            ->select();
        $res = true;
        foreach ($data as $datum) {
            if ($datum['fexamine'] == 0){
                $res = false;
            }
        }
        if (!$res){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '该日违法广告未全部确认'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '该日违法广告已全部确认'
            ]);
        }
    }

    /**
     * 获取违法广告列表
     * @param $date
     * @param $mediaId
     */
    public function getIllegalAdList($date, $mediaId)
    {
        $page = I('page', 1);
        $issueSubQuery = M('tbn_illegal_ad_issue')
            ->field('fillegal_ad_id')
            ->where([
                'fmedia_id' => $mediaId,
                'fissue_date' => $date,
            ])
            ->group('fillegal_ad_id')
            ->buildSql();
        $data = M('tbn_illegal_ad')
            ->where([
                'fid' => ['EXP', 'IN '.$issueSubQuery]
            ])
            ->order('fexamine asc')
            ->page($page, 10)
            ->select();
        $total = M('tbn_illegal_ad')
            ->where([
                'fid' => ['EXP', 'IN '.$issueSubQuery]
            ])
            ->count();
        foreach ($data as $key => $value) {
            $data[$key]['adClass'] = M('tadclass')->where(['fcode' => ['EQ', $value['fad_class_code']]])->getField('ffullname');
            $data[$key]['stateText'] = $value['fexamine'] == 0 ? '未审核' : '已审核';
        }
        $this->ajaxReturn([
            'data' => $data,
            'total' => $total,
        ]);
    }

    /**
     * 获取违法广告详情
     * @param $adId
     */
    public function getIllegalAdInfo($adId)
    {
        $data = M('tbn_illegal_ad')
            ->find($adId);
        if (!$data){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '未找到该违法广告'
            ]);
        }
        $data['adClass'] = M('tadclass')->where(['fcode' => ['EQ', $data['fad_class_code']]])->getField('ffullname');
        $data['mediaName'] = M('tmedia')->where(['fid' => ['EQ', $data['fmedia_id']]])->getField('fmedianame');
        $this->ajaxReturn([
            'code' => 0,
            'data' => $data
        ]);
    }

    /**
     * 确认违法广告信息
     * @param $adId
     */
    public function confirmIllegalAdInfo($adId)
    {
        $res = M('tbn_illegal_ad')
            ->where(['fid' => ['EQ', $adId]])
            ->save(['fexamine' => 10]) === false ? false : true;
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '确认成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '确认失败'
            ]);
        }
    }

    /**
     * 取消确认违法广告信息
     * @param $adId
     */
    public function cancelConfirmIllegalAdInfo($adId)
    {
        $res = M('tbn_illegal_ad')
            ->where(['fid' => ['EQ', $adId]])
            ->save(['fexamine' => 0]) === false ? false : true;
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '取消确认成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '取消确认失败'
            ]);
        }
    }

    /**
     * 获取违法广告报纸版面
     * @param $adId
     */
    public function getIllegalAdPaper($adId)
    {
        $samId = M('tbn_illegal_ad')->where(['fid' => ['EQ', $adId]])->getField('fsample_id');
        $sourceUrl = A('Common/Paper','Model')->get_slim_source($samId);
        if ($sourceUrl){
            $this->ajaxReturn([
                'code' => 0,
                'data' => $sourceUrl
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '未找到版面'
            ]);
        }
    }

    /**
     * 提交任务   任务状态从0 改为1
     * @param $fid int  zongju_data_confirm表主键
     */
    public function confirmTask($fid)
    {
        $dmp_confirm_content = $this->updateDmpConfirmContent($fid);
        $log = M('zongju_data_confirm')->where(['fid' => ['EQ', $fid]])->getField('affirm_flow');
        $userName = session('personInfo.fname');
        $res = M('zongju_data_confirm')
            ->where([
                'fid' => ['EQ', $fid],
                'dmp_confirm_state' => ['EQ', 0]
            ])
            ->save([
                'dmp_confirm_state' => 1,
                'dmp_confirm_content' => $dmp_confirm_content,
                'affirm_flow' => $log. date('Y-m-d H:i:s') . $userName .'确认了任务 ;; '
            ]);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '确认任务成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '确认任务失败'
            ]);
        }
    }

    /**
     * 获取媒体全部确认的json
     * @param $fid  int     总局抽查任务表主键
     * @return string   媒体全部确认的json
     */
    private function updateDmpConfirmContent($fid)
    {
        $row = M('zongju_data_confirm')->field('condition, fmonth')->where(['fid' => ['EQ', $fid]])->find();
        $condition = json_decode($row['condition'], true);
        $month = substr($row['fmonth'], 0, 7);
        $mediaList = [];
        $subQuery = M('tmedialabel')->field('fmediaid')->where(['fstate'=>1,'flabelid' => ['EQ', 243]])->buildSql();
        $mediaList['tv'] = M('tmedia')->where(['fid' => ['EXP', ' IN '.$subQuery], 'fstate' => ['EQ', 1]])->getField('fid', true);
        $subQuery = M('tmedialabel')->field('fmediaid')->where(['fstate'=>1,'flabelid' => ['EQ', 244]])->buildSql();
        $mediaList['paper'] = M('tmedia')->where(['fid' => ['EXP', ' IN '.$subQuery], 'fstate' => ['EQ', 1]])->getField('fid', true);
        $subQuery = M('tmedialabel')->field('fmediaid')->where(['fstate'=>1,'flabelid' => ['EQ', 245]])->buildSql();
        $mediaList['bc'] = M('tmedia')->where(['fid' => ['EXP', ' IN '.$subQuery], 'fstate' => ['EQ', 1]])->getField('fid', true);
        $res = [];
        foreach ($condition as $key => $value) {
            $res[$key] = [];
            foreach ($value as $day) {
                $res[$key][] = [
                    'date' => $month . '-' . $day,
                    'selected' => $mediaList[$key]
                ];
            }
        }
        return json_encode($res);
    }

    /**
     * 开始这个月的数据汇总
     * @param $fid
     */
    public function startDataGather($fid)
    {
        $model = M('zongju_data_confirm');
        $dmp_confirm_state = $model->where(['fid' => ['EQ', $fid]])->getField('dmp_confirm_state');
        if ($dmp_confirm_state !== '0'){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '改计划已经上交国家局, 不允许再汇总'
            ]);
        }
        $mediaJson = json_decode($this->updateDmpConfirmContent($fid), true);
        $addData = [];
        $time = time();
        foreach ($mediaJson as $mediaList) {
            foreach ($mediaList as $day) {
                foreach ($day['selected'] as $mediaId) {
                    $addData[] = [
                        'fdate' => $day['date'],
                        'fmediaid' => $mediaId,
                        'fgroup' => $time,
                        'fstate' => '0',
                        'fcreatetime' => date('Y-m-d H:i:s'),
                    ];
                }
            }
        }
        $model->startTrans();
        $addRes = M('z_gather_task')->addAll($addData) !== false;
        $rowUpdate = $model->where(['fid' => ['EQ', $fid]])->save(['gather_group' => $time]) !== false;
        if ($addRes && $rowUpdate){
            $model->commit();
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '提交成功, 正在汇总中'
            ]);
        }else{
            $model->rollback();
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '提交失败'
            ]);
        }

    }
	
	public function get_chouqu_msg(){

		$dcqCount = M('make_customer_ad_issue')->where(array('fstate'=>['in','0,9'],'forgid'=>20100000))->count();
		
		
		$msg = '';
		if($dcqCount) $msg = '正在抽取到客户数据 '.$dcqCount.' 个媒体天';
		$this->ajaxReturn(array('code' => 0, 'msg' => $msg));
	}

    /**
     * 地区抽查页面
     */
    public function spotCheck()
    {
        $this->zongjuListAuth();
        if (IS_POST){
            $page = I('page', 1);
            $total = M('spot_check')
                ->count();
            $data = M('spot_check')
                ->field('spot_check.*, (select ffullname from tregion where fid = spot_check.fcustomer) as regionName')
                ->order('fmonth desc')
                ->page($page, 12)
                ->select();
            $this->ajaxReturn([
                'code' => 0,
                'data' => $data,
                'total' => $total,
            ]);

        }else{
            $regionTree = json_encode($this->getRegionTree());
            $this->assign(compact('regionTree'));
            $this->display();
        }
    }


    protected function getRegionTree()
    {
        $data = M('tregion')->field('fid value, fpid, fname label')->cache(true)->where(['fstate' => ['EQ', 1], 'fid' => ['NEQ', 100000]])->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpid']])){
                $items[$value['fpid']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }

    /**
     * 新增地区抽检任务
     * @param $fmonth
     * @param $condition
     * @param $region
     */
    public function addSpotCheckTask($fmonth, $condition, $region)
    {
        $data['fmonth'] = $fmonth;
        $data['condition'] = $condition;
        $data['fcustomer'] = $region;
        $data['created_user'] = session('personInfo.fname');
        $data['created_time'] = time();
        $res = M('spot_check')
            ->add($data);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '创建成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '创建失败'
            ]);
        }
    }

    /**
     * 修改任务条件
     * @param $condition
     * @param $fid
     */
    public function editSpotCheckTask( $condition, $fid)
    {
        $where = [
            'fid' => ['EQ', $fid]
        ];
        $data['condition'] = trim($condition, ',');
        $data['update_user'] = session('personInfo.fname');
        $data['update_time'] = time();
        $res = M('spot_check')
            ->where($where)
            ->save($data);
        if ($res !== false){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
    }

    /**
     * 获取地区抽检任务信息
     * @param $id
     */
    public function getSpotCheckTask($id)
    {
        $data = [
            'data' => M('spot_check')->find($id),
            'code' => 0
        ];

        $this->ajaxReturn($data);
    }

    /**
     * 删除地区抽检任务
     * @param $fid
     */
    public function delSpotTask($fid)
    {
        $res = M('spot_check')->delete($fid);
        if ($res !== false){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '删除成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '删除失败'
            ]);
        }
    }
	
	
	/*应用数据管理*/
	public function customer_issue(){
		
		$getRegionTree = A('Common/Region','Model')->getRegionTree();
		//var_dump($getRegionTree);
		
		$getAdClassArr = A('Common/AdClass','Model')->getAdClassArr();
		//var_dump($getAdClassArr);
		
		
		$this->assign('getRegionTree',$getRegionTree);
		$this->assign('getAdClassArr',$getAdClassArr);
		
		$this->display();
	}
	
	
	
	/*应用数据管理(搜索)*/
	public function search_customer_issue(){
		header('Access-Control-Allow-Origin:*');//允许跨域

		$p = I('p',1);//当前第几页
		$pp = I('pp',10);//每页显示多少记录
		
		if(strtotime(I('issuedate_min')) < strtotime('2017-07-01')) $_GET['issuedate_min'] = date('Y-m-d',time() - 86400*360);
		if(strtotime(I('issuedate_max')) < strtotime('2017-07-01')) $_GET['issuedate_max'] = date('Y-m-d');
		
		
		
		
		$client = A('Common/OpenSearch','Model')->client();//获得OpenSearchClient
		$searchClient = new SearchClient($client);

		$params = new SearchParamsBuilder();
		$params->setStart(($p*$pp) - $pp);//起始位置

		$params->setHits($pp);//返回数量

		$params->setAppName(C('OPEN_SEARCH_CUSTOMER'));//设置应用名称
		
		
		$where = array();
		$where['_string'][] = ' fissuedate2:['.strtotime(I('issuedate_min')).','.strtotime(I('issuedate_max')).'] ';

		
		
		if(I('fmedianame') != '') $where['fmedianame'] = I('fmedianame');//搜索媒介名称
		if(I('fadname') != '') $where['fadname'] = I('fadname');//搜索广告名称
		if(I('fadclasscode') != '') $where['fadclasscode'] = I('fadclasscode');//搜索广告类别
		if(I('fillegaltypecode') != '') $where['fillegaltypecode'] = I('fillegaltypecode');//搜索违法类别
		if(I('forgid')) $where['forgid'] = I('forgid');//搜索客户id
		if(I('mediatype') != '') $where['fmediaclassid'] = intval(I('mediatype'));//搜索媒介类别
		if(I('fregionid') != ''){//如果有传入地区搜索条件
			if(I('this_region_id') == 'true'){
				$where['fregionid'] = I('fregionid');
			}else{
				$where['regionid_array'] = I('fregionid');
			}
			
		}

		$setQuery = A('Common/OpenSearch','Model')->setQuery($where);//组装搜索字句
		

		
		$params->setQuery($setQuery);//设置搜索
		
		
		$whereFilter = array();
		#$whereFilter['fissuedate'] = array('between',array(strtotime(I('issuedate_min')),strtotime(I('issuedate_max'))));
		$whereFilter['flength'] = array('between',array(I('adlength_min'),I('adlength_max')));

		
		
		
		
		

		$setFilter = A('Common/OpenSearch','Model')->setFilter($whereFilter);//组装过滤条件

		//var_dump($setFilter);
		
		$params->addSort('fissuedate', SearchParamsBuilder::SORT_DECREASE);//排序
		$params->setFilter($setFilter);//设置文档过滤条件
		$params->setFormat("json");//数据返回json格式
		
		$params->setFetchFields(array(
										'identify',//识别码（哈希）

										'fmediaclassid',
										
										'fmediaid', //媒介id
										'fmedianame',//媒介名称
										'fmediaownername',//媒介机构名称
										
										'fregionid', //地区id
										'fadname',//广告名称
										'fadowner',//广告主
										
										'fbrand', //品牌
										'fspokesman', //代言人
										'fadclasscode', //广告类别id
										'fstarttime', //发布开始时间戳
										'fendtime',//发布结束时间戳
										'fissuedate', //发布日期
										'flength', //发布时长(秒)
										'fillegaltypecode', //违法类型

										
										
										
									));//设置需返回哪些字段
		
		
		$ret = $searchClient->execute($params->build());//执行查询并返回信息
		

		$result = json_decode($ret->result,true);//
		
		$issueList0 = $result['result']['items'];
		
		$issueList = array();
		
		
		if(!S('adClass')){
			$cacheAdClass = M('tadclass')->cache(true,600)->field('fcode,fadclass,ffullname')->select();
			$adClass = array();
			foreach($cacheAdClass as $cacheAdClassV){
				$adClass[$cacheAdClassV['fcode']] = $cacheAdClassV;
			}
			S('adClass',$adClass,86400);
			
		}else{
			$adClass = S('adClass');
			
		}
		
		
		//var_dump($adClass);

		foreach($issueList0 as $issu){
			$issu['starttime'] = date('H:i:s',$issu['fstarttime']);
			$issu['endtime'] = date('H:i:s',$issu['fendtime']);
			$issu['issuedate'] = date('Y-m-d',$issu['fissuedate']);
			
			$adclasscode_arr = explode('	',$issu['fadclasscode']);
			
			$issu['adclasscode'] = $adClass[$adclasscode_arr[0]]['ffullname'];
			
			$issueList[] = $issu;
		}
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','total'=>$result['result']['total'],'issueList'=>$issueList));//返回数据
		
		
		
		
		
		
		
	}
	
	
	/*发布数据详情*/
	public function issue_info(){
		
		$identify = I('identify');
		
		header('Access-Control-Allow-Origin:*');//允许跨域


	
		
		
		
		$client = A('Common/OpenSearch','Model')->client();//获得OpenSearchClient
		$searchClient = new SearchClient($client);

		$params = new SearchParamsBuilder();


		$params->setHits(1);//返回数量

		$params->setAppName(C('OPEN_SEARCH_CUSTOMER'));//设置应用名称
		
		
		$setQuery = A('Common/OpenSearch','Model')->setQuery(array('id'=>$identify));//组装搜索字句
		

		
		$params->setQuery($setQuery);//设置搜索

		$params->setFormat("json");//数据返回json格式
		
		$params->setFetchFields(array(
										'identify',		
										'fmediaclassid',	
										'fsampleid',	
										'fmediaid',	
										'fmedianame',			
										'fregionid',	
										'fadname',			
										'fadowner',		
										'adowner_regionid',		
										'fbrand',
										'fspokesman',
										'fadclasscode',
										'fstarttime',	
										'fendtime',
										'fissuedate',
										'flength',
										'fillegaltypecode',	
										'fexpressioncodes',
										'fillegalcontent',
										'sam_source_path',
										'fprice',
										'issue_platform_type',
										'landing_img',
										'issue_page_url',
										'target_url',		
										'operator_name',
										'operator_id',
										'issue_ad_type',
										'search_other',	
										'fmediaownername',
										'fmediaownerid',
										'regionid_array',
										'last_update_time',
										'forgid',
	
									));//设置需返回哪些字段
		
		
		$ret = $searchClient->execute($params->build());//执行查询并返回信息
		

		$result = json_decode($ret->result,true);//
		
		$issueInfo0 = $result['result']['items'][0];
		
		$issueInfo = array();
		
		foreach($issueInfo0 as $field => $value){
			if($value || $field == 'forgid') $issueInfo[$field] = $value;
			
		}
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','issueInfo'=>$issueInfo));//返回数据
		
		
		
		
		
		
		
	}
	
	
	/*新增发布记录*/
	public function add_issue(){
		
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->display();
	}
	
	
	/*新增发布记录*/
	public function add_issue_do(){
		
		
		
			$mediaId = I('fmediaid');
			$sstarttime = strtotime(I('fstarttime'));
			
			$fadlen = I('fadlen');
			
			$sendtime = $sstarttime + $fadlen;
			$adname = I('adname');
			$uuid = I('uuid');
			$furl = I('furl'); 
		
			if($fadlen < 3 || $fadlen > 86400){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'时长错误'));
			}
		
		
			$mediaInfo = M('tmedia')
									->cache(true,600)
									->field('tmedia.fmediaclassid,tmedia.fmedianame,tmediaowner.fregionid')
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->where(array('tmedia.fid'=>$mediaId))
									->find();

			if(substr($mediaInfo['fmediaclassid'],0,2) == '01'){
				$tb = 'tv';
			}elseif(substr($mediaInfo['fmediaclassid'],0,2) == '02'){
				$tb = 'bc';
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介ID不对'));
			}
		
		
			//M()->startTrans();//开始事务
		
			$a_data = array();
			$a_data['fmediaid'] = $mediaId;//媒介ID
			$a_data['fissuedate'] = date('Y-m-d',$sstarttime);//发布日期
			
			$a_data['sstarttime'] = date('Y-m-d H:i:s',$sstarttime);//开始时间
			$a_data['sendtime'] = date('Y-m-d H:i:s',$sendtime);//结束时间


            $adId = M('tad')->cache(true,60)->where(['fadname' => $adname,'is_sure' => 1,])->order('fmodifytime desc')->getField('fadid');
			if($adId){
				$a_data['fadid'] = $adId;
			}else{
				$a_data['fadid'] = 0;
			}
			if($fadlen >= 600){
				$a_data['is_long_ad'] = 1;
			}else{
				$a_data['is_long_ad'] = 0;
			}

			$a_data['favifilename'] = $furl;//视频路径

			
			if(strstr($furl,'http://datamanagerplatform.finesoft.com.cn/')){//判断传过来的地址是否七牛地址
				$a_data['fcuted'] = 2;//上传状态
				$add_InputTask = true;
				$a_data['favifilepng'] = $sam['video_path'].'?vframe/jpg/offset/2/h/100';//视频缩略图
			}else{
				$a_data['fcuted'] = 3;//上传状态
			}
			$a_data['fadlen'] = $fadlen;//样本时长
			$a_data['finputstate'] = 0;//录入状态
			$a_data['finspectstate'] = 0;//判断状态
			
			if( $furl == ''){
				$a_data['fsourcefilename'] = $sstarttime.','.$sendtime;//用于存储开始时间、结束时间
			}
			
			
			$a_data['fcreator'] = session('personInfo.fid').'_'.session('personInfo.fname');//创建人
			$a_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
			$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$a_data['tem_ad_name'] = $adname;//临时广告名称
			
			$a_data['uuid'] = $uuid;//唯一ID

			
			if(M('t'.$tb.'sample')->where(array('uuid'=>$uuid,'fmediaid'=>$mediaId))->count() > 0){
				//M()->rollback();//回滚事务
				$this->ajaxReturn(array('code'=>-1,'msg'=>'uuid重复'));
				
			}
			$add_sam_id = M('t'.$tb.'sample')->add($a_data);//添加数据
			//var_dump(M('t'.$tb.'sample')->getLastSql());
			if(!$add_sam_id){
				//M()->rollback();//回滚事务
				$this->ajaxReturn(array('code'=>-1,'msg'=>'样本添加出错'));
			}
		
			
			$a_data = array();
		
		
			$a_data['fmediaid'] = $mediaId;//媒介ID
			$a_data['f'.$tb.'sampleid'] = $add_sam_id;//样本ID

			$a_data['fissuedate'] = date('Y-m-d',$sstarttime);//发布日期
			
			$a_data['sstarttime'] = date('Y-m-d H:i:s',$sstarttime);//开始时间
			$a_data['sendtime'] = date('Y-m-d H:i:s',$sendtime);//结束时间
			
			
			
			$a_data['fstarttime'] = date('Y-m-d H:i:s',$sstarttime);//发布开始时间
			$a_data['fendtime'] = date('Y-m-d H:i:s',$sendtime);//发布结束时间
			$a_data['flength'] = $fadlen;//发布时长
			$a_data['faudiosimilar'] = 100;//相似度得分
			$a_data['fregionid'] = $mediaInfo['regionid'];//地区
			
			$a_data['fu0'] = ($sstarttime * 1000) - (strtotime(date('Y-m-d',$sstarttime)) * 1000);
			$a_data['fduration'] = $fadlen * 1000;
			
			
			
			$a_data['fcreator'] = session('personInfo.fid').'_'.session('personInfo.fname');//创建人
			$a_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
			$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			if($add_InputTask){
				$taskid = A('Common/InputTask','Model')->add_task(intval(substr($mediaInfo['fmediaclassid'],0,2)),$add_sam_id);//添加到任务表
				//
			}
			
			if(M('t'.$tb.'issue')->where(array('fmediaid'=>$mediaId,'fstarttime'=>date('Y-m-d H:i:s',$sstarttime)))->count() > 0){
				//M()->rollback();//回滚事务
				$this->ajaxReturn(array('code'=>-1,'msg'=>'发布记录重复'));
			}
			$issueId = M('t'.$tb.'issue')->add($a_data);
			if(!$issueId){
				//M()->rollback();//回滚事务
				$this->ajaxReturn(array('code'=>-1,'msg'=>'发布记录添加出错'));
			}
			//M()->commit();//提交事务
			$this->ajaxReturn(array('code'=>0,'msg'=>$mediaInfo['fmedianame'].'_'.date('Y-m-d H:i:s',$sstarttime).'_成功'));
		
		
	}
	
	
	/**
     * 抽查计划列表
     * by zw
     */
    public function ccday_list(){
        session_write_close();
        $p = I('page', 1);//当前第几页
        $pp = 5;//每页显示多少记录

        $where_zdc['dmp_confirm_state'] = ['neq', 1];
        $where_zdc['fstarttime'] = ['egt','2018-07-01'];

        $count = M('zongju_data_confirm')
            ->alias('a')
            ->where($where_zdc)
            ->count();//查询满足条件的总记录数

        $do_zdc = M('zongju_data_childconfirm')
            ->alias('a')
            ->field('a.fid,date_format(fstarttime,"%Y-%m-01") as fmonth,dmp_confirm_state,condition,fstarttime,fendtime,gather_group')
            ->where($where_zdc)
            ->order('a.fendtime desc')
            ->page($p,$pp)
            ->select();

        foreach ($do_zdc as $key => $value) {
            $condition = json_decode($value['condition'],true);
            $do_zdc[$key]['tv'] = $condition['tv']?$condition['tv']:[];
            $do_zdc[$key]['bc'] = $condition['bc']?$condition['bc']:[];
            $do_zdc[$key]['paper'] = $condition['paper']?$condition['paper']:[];
        }
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_zdc)));
    }
	
	

}