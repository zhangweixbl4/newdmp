const zjConfirm = {
    props: ['fid'],
    template: `<div v-loading="loading">
    <el-tabs v-model="classActive" @tab-click="chooseClass">
        <el-tab-pane :label="type.label" :name="type.label" v-for="(type,index) in classArr" :key="index">
            <el-tabs v-model="dateActive" @tab-click="chooseDate">
                <el-tab-pane :label="item.date" :name="dateIndex + ''" v-for="(item, dateIndex) in classArr[index]['prop']" :key="item.date">
                    <el-form :inline="true" :model="mediaWhereData">
                        <el-form-item label="媒体名称">
                            <el-input v-model="mediaWhereData.name"></el-input>
                        </el-form-item>
                        <el-form-item label="地区" class="regionSearch">
                            <el-popover
                                    @show="showRegionSelecter"
                                    @hide="resetRegionArr"
                                    placement="bottom"
                                    width="400"
                                    trigger="click">
                                <div class="mediaList">
                                    <el-button class="item " v-for="item in regionArr" @click="chooseRegion(item)" :key="item.value">{{item.label}}</el-button>
                                </div>
                                <el-input v-model="mediaWhereData.searchRegion" slot="reference"></el-input>
                            </el-popover>
                            <el-checkbox v-model="onlyThisLevel">只搜索本级</el-checkbox>
                        </el-form-item>
                        <el-form-item>
                            <el-button type="primary" @click="getMediaList">搜索</el-button>
                        </el-form-item>
                        <el-form-item>
                            <el-button type="primary" @click="getAlreadyConfirmMediaList">查看已确认</el-button>
                        </el-form-item>
                        <el-form-item>
                            <el-button type="primary" @click="allPick">全部确认</el-button>
                        </el-form-item>
                        <el-form-item>
                            <el-button type="warning" @click="clearSelected">全部不确认</el-button>
                        </el-form-item>
                    </el-form>
                    <el-row v-loading="searchLoading">
                        <el-col :span="24" class="mediaList">
                            <el-button :type="item.selected ? 'success' : 'info'" v-for="item in displayMediaList" :key="item.id" v-on:click="confirmMedia(item.id)" size="mini" title="点击确认,再次点击取消确认">{{item.name}}</el-button>
                        </el-col>
                    </el-row>
                    <el-row align="middle" style="display: flex;align-items: center">
                        <el-col :span="3">
                            <div>已确认{{activeDate.selected ? activeDate.selected.length : 0}}条</div>
                        </el-col>
                        <el-col :span="18">
                            <el-pagination
                                    layout="total, prev, pager, next"
                                    @current-change="pageChange"
                                    :page-size="20"
                                    :current-page="page"
                                    :total="total">
                            </el-pagination>
                        </el-col>
                    </el-row>
                </el-tab-pane>
            </el-tabs>
        </el-tab-pane>
    </el-tabs>
    <el-row>
        <el-col :span="8" :offset="7">
            <el-button type="primary" @click="submit">提交</el-button>
        </el-col>
    </el-row>
</div>
    `,
    data: function () {
        return {
            taskInfo: {},
            tvDates: [],
            bcDates: [],
            paperDates: [],
            month:'',
            loading: false,
            classActive: null,
            dateActive: null,
            mediaList: [],
            page: 1,
            total: 0,
            mediaWhereData: {},
            searchLoading: false,
            searchRegion: '',
            regionArrAll: [],
            regionArr: [],
            onlyThisLevel: false,
            dmpConfirmContent: {}
        }
    },
    computed: {
        mediaWhere: function(){
            const where = this.mediaWhereData
            where.class = this.classActive
            where.page = this.page
            where.isZj = 1
            where.label = '国家局'
            if (this.onlyThisLevel){
                where.onlyThisLevel = 1
            }
            return where
        },
        classArr: function () {
            return [
                {
                    label: '电视',
                    prop: this.tvDates
                },
                {
                    label: '广播',
                    prop: this.bcDates
                },
                {
                    label: '报纸',
                    prop: this.paperDates
                },
            ]
        },
        activeDate: function () {
            if (this.classActive === '0' || this.dateActive === null){
                return []
            }
            let dates
            if (this.classActive === '电视'){
                dates = this.tvDates
            }
            if (this.classActive === '广播'){
                dates = this.bcDates
            }
            if (this.classActive === '报纸'){
                dates = this.paperDates
            }
            return dates[+this.dateActive]
        },
        activeDmpDate: function () {
            if (this.classActive === '0' || this.dateActive === null){
                return []
            }
            let dates
            if (this.classActive === '电视'){
                dates = this.dmpConfirmContent.tv
            }
            if (this.classActive === '广播'){
                dates = this.dmpConfirmContent.bc
            }
            if (this.classActive === '报纸'){
                dates = this.dmpConfirmContent.paper
            }
            return dates[+this.dateActive]
        },
        displayMediaList: function () {
            if (this.activeDate == undefined){
                return []
            }
            const selectedArr = this.activeDate.selected
            if (!selectedArr){
                return []
            }
            const arr = this.mediaList.map((item)=>{
                item.selected = selectedArr.indexOf(item.id) !== -1
                return item
            })
            return arr
        }
    },
    methods: {
        myCreated: function () {
            this.mediaWhereData = {}
            this.dateActive = null
            this.classActive = null
            this.getData()
            $.post('/Api/Region/getRegionArr').then((res) => {
                this.regionArrAll = res.data
                this.regionArr = res.data
            })
        },
        getData: function () {
            this.loading = true
            $.post('getAllDays', {id: this.fid}).then((res) => {
                if (res.code == 0){
                    this.taskInfo = res.data
                    this.processData()
                    this.loading = false
                } else{
                    this.$message.error('网络错误');
                }
            })
        },
        processData: function () {

            this.dmpConfirmContent = {}
            const content = JSON.parse(this.taskInfo.dmp_confirm_content)
            this.dmpConfirmContent.tv = content.tv
            this.dmpConfirmContent.bc = content.bc
            this.dmpConfirmContent.paper = content.paper
            if (this.taskInfo.zongju_confirm_content){
                const content = JSON.parse(this.taskInfo.zongju_confirm_content)
                this.tvDates = content.tv
                this.bcDates = content.bc
                this.paperDates = content.paper
            }else{
                const month = this.taskInfo.fmonth.split('-')
                month.pop()
                this.month = month.join('-')
                const condition = JSON.parse(this.taskInfo.condition)
                this.tvDates = condition.tv.map(item => {
                    return {
                        date: this.month+'-'+item,
                        selected: [],
                    }
                })
                this.bcDates = condition.bc.map(item => {
                    return {
                        date: this.month+'-'+item,
                        selected: [],
                    }
                })
                this.paperDates = condition.paper.map(item => {
                    return {
                        date: this.month+'-'+item,
                        selected: [],
                    }
                })
            }
        },
        chooseClass: function () {
            this.dateActive = null
            this.mediaWhereData = {}
        },
        chooseDate: function () {
            this.mediaWhereData = {}
            this.pageChange(1)
        },
        getMediaList: function () {
            this.searchLoading = true
            this.mediaWhereData.fids2 = this.activeDmpDate.selected.join(',')

            $.post('/Api/Media/getMediaListByLabel', this.mediaWhere).then((res) => {
                this.mediaList = res.data
                this.total = +res.total
                this.searchLoading = false
            })
        },
        pageChange: function (page) {
            this.page = page
            this.getMediaList()
        },
        confirmMedia: function (id) {
            const selected = this.activeDate.selected
            const index = selected.indexOf(id);
            if (index === -1){
                selected.push(id)
            }else {
                selected.splice(index, 1)
            }
        },
        getAlreadyConfirmMediaList: function () {
            this.mediaWhereData.fids = this.activeDate.selected.join(',')
            this.pageChange(1)
        },
        chooseRegion: function (item) {
            this.mediaWhereData.searchRegion = item.ffullname
            this.mediaWhereData.regionId = item.value
            $.post('/Api/Region/searchChildren', {id: item.value}).then((res) => {
                this.regionArr = res.data
            })
        },
        resetRegionArr: function () {
            this.regionArr = this.regionArrAll
        },
        showRegionSelecter: function () {
            this.mediaWhereData.searchRegion = ''
            this.mediaWhereData.regionId = ''
        },
        submit: function () {
            this.$confirm('确认提交?').then(() => {
                let zjConfirm = {
                    tv: this.tvDates,
                    bc: this.bcDates,
                    paper: this.paperDates,
                }
                zjConfirm = JSON.stringify(zjConfirm)
                $.post('ZjConfirm', {id: this.fid, content: zjConfirm}).then((res) => {
                    if (res.code == 0){
                        this.$message.success(res.msg)
                        this.$emit('close-zj')
                    } else {
                        this.$message.error(res.msg)
                    }
                })
            }).catch(() => {})
        },
        allPick: function () {
            this.loding = true
            $.post('/Api/Media/getMediaIdsByLabel', this.mediaWhere).then((res) => {
                this.activeDate.selected = res.data
                this.loading = false
            })
        },
        clearSelected: function () {
            this.activeDate.selected = []
        }
    }
}