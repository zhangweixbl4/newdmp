<?php
namespace Agp\Controller;
use Think\Controller;
import('Vendor.PHPExcel');

/**
 * 安徽广告抽查
 */
class AdDataCheckController extends BaseController{

    /**
     * 接收参数
     */
    protected $P;

    /**
     * 广告抽查状态
     */
    protected $showStatus = [
        0 => '不可检查',
        1 => '待检查',
        2 => '正检查',
        3 => '已检查',
    ];

    /**
     * 初始化
     */
    public function _initialize(){
        $this->P = json_decode(file_get_contents('php://input'),true);
    }

    /**
     * 数据检查计划-获取列表
     */
    public function Index(){
        $system_num = getconfig('system_num');//客户编号
        $regulatorpersonInfo = session('regulatorpersonInfo');
        $fmonth     = strtotime($this->P['fmonth']);
        $fregionid  = $this->P['fregionid'];
        $fmediaid   = $this->P['fmediaid'];
        $fstate     = $this->P['fstate'];
        $fissuedate = $this->P['fissuedate'];
        $fdstatus   = $this->P['fdstatus'];
        $fchecker   = $this->P['fchecker'];
        $fstate     = $this->P['fdstatus'] ? $this->P['fdstatus'] : $this->P['fstate'];
        $pageIndex  = $this->P['pageIndex'] ? $this->P['pageIndex'] : 1;
        $pageSize   = $this->P['pageSize'] ? $this->P['pageSize']  : 1000;
        $limitIndex = ($pageIndex-1)*$pageSize;
        $where = [
            'fcustomer' => $system_num
        ];
        if(!empty($fmonth)){
            $where['fmonth'] = $fmonth;
        }
        if(!empty($fmediaid)){
            $where['fmediaid'] = $fmediaid;
        }
        if(!empty($fissuedate)){
            $where['fissuedate'] = $fissuedate;
        }
        if(!empty($fchecker)){
            $fcheckerid = M('tregulatorperson')->cache(true,60)->where(['fname'=>['LIKE','%'.$fchecker.'%']])->getField('fid',true);
            $where['fchecker'] = ['IN',$fcheckerid];
        }
        if(!empty($fstate)){
            // 状态搜索：(0-不可检查)、1-待检查、2-正检查、3-已检查
            switch($fstate){
                case 1:
                    $where['fstate'] = ['IN','0,5'];break;
                case 2:
                    $where['fstate'] = ['IN','1,3,6,8'];break;
                case 3:
                    $where['fstate'] = ['IN','2,7'];break;
                default:
                    $where['fstate'] = ['IN','0,1,2,3,4,5,6,7,8'];break;
            }
        }
        $count = M('tbn_data_check_plan')->where($where)->count();
        $dataSet = [];
        if($count > 0){
            $dataSet = M('tbn_data_check_plan')
                ->where($where)
                ->limit($limitIndex,$pageSize)
                ->order('fplantime DESC')
                ->select();
            foreach($dataSet as $key=>$row){
                $fshowstatus = 1;
                if(in_array($row['fstate'],[0,5])){
                    $fshowstatus = 1;
                }elseif(in_array($row['fstate'],[1,6,3,8])){
                    $fshowstatus = 2;
                }elseif(in_array($row['fstate'],[2,7])){
                    $fshowstatus = 3;
                }
                $dataSet[$key] = [
                    'fid' => $row['fid'],
                    'fshowstatus' => $fshowstatus,
                    'fmediaid' => $row['fmediaid'],
                    'fmedianame' => M('tmedia')->cache(true,60)->where(['fid'=>$row['fmediaid']])->getField('fmedianame'),
                    'fissuedate' => date('Y-m-d',strtotime($row['fissuedate'])),
                    'fplantime' => $row['fplantime'],
                    'ffinishdate' => $row['ffinishdate'] ? date('Y-m-d H:i:s',strtotime($row['ffinishdate'])) : '',
                    'fchecker' => M('tregulatorperson')->cache(true,60)->where(['fid'=>$row['fchecker']])->getField('fname'),
                    'fsummary' => $row['fsummary'],
                ];
            }
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','count'=>$count, 'data'=>$dataSet]);
    }

    /**
     * 数据检查计划-新增
     */
    public function Add(){
        $system_num = getconfig('system_num');//客户编号
        $fmonth = strtotime($this->P['fmonth']);
        $fmediaid = $this->P['fmediaid'];
        $fregionid = $this->P['fregionid'];
        $fissuedate = $this->P['fissuedate'];
        $fcheckorg = $this->P['fcheckorg'];
        $fplandate = $this->P['fplandate'];
        $dataSet = [];
        $succRow = [];
        $errRow = [];
        if($fissuedate){
            $arrFissuedate = explode(',',$fissuedate);
            foreach($arrFissuedate as $key=>$val){
                $dataRow = [
                    'fcustomer' => $system_num,
                    'fmonth' => $fmonth,
                    'fmediaid' => $fmediaid,
                    'fregionid' => $fregionid,
                    'fissuedate' => $val,
                    'fcheckorg' => $fcheckorg,
                    'fplandate' => $fplandate,
                    'fplaner' => session('regulatorpersonInfo.fid'),
                    'fplantime' => date('Y-m-d H:i:s')
                ];
                $condition = [
                    'fcustomer' => $system_num,
                    'fmonth' => $fmonth,
                    'fmediaid' => $fmediaid,
                    'fissuedate' => $val,
                    'fcheckorg' => $fcheckorg,
                    'fplandate' => $fplandate,
                ];
                $isExist = M('tbn_data_check_plan')->where($condition)->count() > 0 ? true : false;
                if(!$isExist){
                    $fid = M('tbn_data_check_plan')->add($dataRow);
                    if($fid){
                        array_push($succRow,['fid'=>$fid]);
                    }
                }else{
                    array_push($errRow,$dataRow);
                }
            }
            $msg = '总'.count($arrFissuedate).'条记录。';
            if(count($succRow) > 0){
                $msg .= ' 成功:'.count($succRow).' 条';
                $dataSet = $succRow;
                if(count($errRow) > 0){
                    $msg .= ' 重复:'.count($errRow).'条';
                    $dataSet = $succRow + $errRow;
                }
                $this->ajaxReturn(['code'=>0, 'msg'=>$msg, 'data'=>$dataSet]);
            }else{
                $msg .= ' 失败:'.count($errRow).' 条';
                $this->ajaxReturn(['code'=>1, 'msg'=>$msg, 'data'=>$errRow]);
            }
        }
        $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
    }

    /**
     * 数据检查计划-详情
     */
    public function Detail(){
        $fid = $this->P['fid'];
        $plantype = [
            '01' => 'tv',
            '02' => 'bc',
            '03' => 'paper'
        ];
        if($fid){
            $info = M('tbn_data_check_plan')->where(['fid'=>$fid])->find();
            $fmediaclassid = M('tmedia')->where(['fid'=>$info['fmediaid']])->getField('fmediaclassid');
            $mtype = substr($fmediaclassid,0,2);
            $data = [
                'plantype'    => $plantype[$mtype],// TODO:复核计划类型，待增加到计划表字段 'tv' 、'bc'、'paper'
                'fid'         => $info['fid'],
                'fmonth'      => $info['fmonth'],
                'fregionid'   => $info['fregionid'],
                'fmediaid'    => $info['fmediaid'],
                'fissuedate'  => $info['fissuedate'],
                'fcheckorg'   => $info['fcheckorg'],
                'fchecker'    => $info['fchecker'],
                'fplandate'   => $info['fplandate'],
                'ffinishdate' => $info['ffinishdate'],
                'fplaner'     => $info['fplaner'],
                'fplantime'   => $info['fplantime'],
                'fstate'      => $info['fstate'],
                'freason'     => $info['freason'],
                'fsummary'    => $info['fsummary']
            ];
            $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$data]);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }

    }

    /**
     * 数据检查计划-删除
     */
    public function Delete(){
        $fid = $this->P['fid'];
        $c = 0;
        if($fid){
            $fids = explode(',', $fid);
            foreach($fids as $k=>$v){
                $del = M('tbn_data_check_plan')->where(['fid'=>$v])->delete();
                if($del){
                    $c++;
                }
            }
            if($c){
                $this->ajaxReturn(['code'=>0,'msg'=>'成功删除'.$c.'条记录']);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'未删除任何数据']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 数据检查计划-退回重新检查
     */
    public function BackReCheck(){
        $fid = $this->P['fid'];
        $freason = $this->P['freason'];
        if($fid){
            $where['fid'] = $fid;
            $dataRow = [
                'fstate' => 2,
                'freason' => $freason
            ];
            $ret = M('tbn_data_check_plan')->where($where)->save($dataRow);
            $data['fid'] = $ret;
            $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$data]);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 开始检查前校验
     */
    public function startCheckPlan(){
        $system_num = getconfig('system_num');//客户编号
        $fid        = $this->P['fid'];
        $fmediaid   = $this->P['fmediaid'];
        $fissuedate = $this->P['fissuedate'];
        if(!empty($fmediaid) && !empty($fissuedate)){
            // 获取媒体数据准备就绪的时间戳，判断是否可以检查
            try {
                $avaTimeStamp = A('Common/Media','Model')->available_time($fmediaid);
                $isAvailable = (strtotime($fissuedate) < ($avaTimeStamp - 86400)) ? true : false;
                if($isAvailable){
                    // 校验是否已存在该计划及计划检查状态,计划外则添加计划
                    $whereIsPlanExist = [
                        'fcustomer' => $system_num,
                        'fmediaid' => $fmediaid,
                        'fissuedate' => $fissuedate,
                    ];
                    $infoPlanExist = M('tbn_data_check_plan')->where($whereIsPlanExist)->find();
                    if(count($infoPlanExist) > 0){
                        // 更新计划表状态为进行中
                        $setPlanExist = [
                            'fcustomer' => $system_num,
                            'fmediaid' => $fmediaid,
                            'fissuedate' => $fissuedate,
                        ];
                        // 数据处理状态（0待处理，1正在处理，2处理完成，3完成后再检查处理中，4数据已传送完成，5计划外待处理，6计划外正在处理，7计划外处理完成 ，8计划外完成再检查进行中）
                        switch($infoPlanExist['fstate']){
                            case 0:
                            case 1:
                                $fstate = 1;break;
                            case 2:
                            case 3:
                            case 4:
                                $fstate = 3;break;
                            case 5:
                            case 6:
                                $fstate = 6;break;
                            case 7:
                            case 8:
                                $fstate = 8;break;
                            default:
                                $this->ajaxReturn(['code'=>1, 'msg'=>'该计划状态异常，请联系管理员']);break;
                        }
                        // 若是正在检查的任务进行提示但仍允许检查
                        if(in_array($infoPlanExist['fstate'],[1,3,6,8])){
                            $this->ajaxReturn(['code'=>2, 'msg'=>'该计划正在被检查中，是否仍然检查']);
                        }
                        // 更新状态
                        $setPlanExist = [
                            'fstate' => $fstate,
                        ];
                        $res = M('tbn_data_check_plan')->where($whereIsPlanExist)->save($setPlanExist);
                        $fid = M('tbn_data_check_plan')->where($whereIsPlanExist)->getField('fid');
                        $this->ajaxReturn(['code'=>0, 'msg'=>'成功','fid'=>$fid]);
                    }else{
                        // 计划外则添加计划
                        $addPlan = [
                            'fcustomer' => $system_num,
                            'fmediaid' => $fmediaid,
                            'fissuedate' => $fissuedate,
                            'fplaner' => session('regulatorpersonInfo.fid') ? session('regulatorpersonInfo.fid') : 'API',
                            'fplantime' => date('Y-m-d H:i:s'),
                            'fstate' => 5,
                        ];
                        $res = M('tbn_data_check_plan')->add($addPlan);
                        if($res){
                            $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'fid'=>$res]);
                        }else{
                            $this->ajaxReturn(['code'=>1, 'msg'=>'出错']);
                        }
                    }
                    $this->ajaxReturn(['code'=>0, 'msg'=>'数据已就绪']);
                }else{
                    $this->ajaxReturn(['code'=>1, 'msg'=>'数据生产中，请稍后检查']);
                }
            } catch (\Throwable $th) {
                $this->ajaxReturn(['code'=>1, 'msg'=>$th]);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 数据检查-开始检查
     */
    public function Check(){
        $fid        = $this->P['fid'];
        $fstarttime = $this->P['fstarttime'];
        $fendtime   = $this->P['fendtime'];
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex']: 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 5000);
        $limitIndex = ($pageIndex-1)*$pageSize;
        if($fid && $fstarttime && $fendtime){
            $planInfo = M('tbn_data_check_plan')
                ->where(['fid'=>$fid])
                ->find();
            if($planInfo){
                $fmediaid   = $planInfo['fmediaid'];
                $m3u8_url   = A('Common/Media','Model')->get_m3u8($fmediaid,strtotime($fstarttime),strtotime($fendtime));
                $retList    = A('Common/AdIssue','Model')->getAdIssueByMedia($fmediaid,$fstarttime,$fendtime);
                $count      = (int)$retList['count'];//包含非广告时段总条数
                $countAds   = (int)$retList['count'];//仅广告总条数
                $List       = $retList['data'];
                $adList     = [];
                $resultList = [];
                if(!empty($List)){
                    foreach($List as $key=>$row){
                        if($key == 0){//第一条
                            $interval = $row['fstarttime'] - strtotime($fstarttime); //与起始时间间隔大于3秒,则创建一个空时间段
                            if($interval > 3){
                                $adNull = [
                                    'fcreateperson'    => '',
                                    'fadname'          => '--',
                                    'fadclass'         => '',
                                    'fstarttime'       => date('Y-m-d H:i:s',strtotime($fstarttime)),
                                    'fendtime'         => date('Y-m-d H:i:s',$row['fstarttime']),
                                    'flength'          => $interval,
                                    'fillegaltypecode' => '',
                                    'fexpressioncodes' => '',
                                    'fexpressions'     =>  ''
                                ];
                                array_push($adList,$adNull);
                                $count++;//创建的空闲时段计入记录总条数
                            }
                        }elseif($key > 0 && $key != (count($List)-1)){
                            $interval = $row['fstarttime'] - $List[$key-1]['fendtime']; //两个广告时间间隔大于3秒,则创建一个空时间段
                            if($interval > 3){
                                $adNull = [
                                    'fcreateperson'    => '',
                                    'fadname'          => '--',
                                    'fadclass'         => '',
                                    'fstarttime'       => date('Y-m-d H:i:s',$List[$key-1]['fendtime']),
                                    'fendtime'         => date('Y-m-d H:i:s',$row['fstarttime']),
                                    'flength'          => $interval,
                                    'fillegaltypecode' => '',
                                    'fexpressioncodes' => '',
                                    'fexpressions'     =>  ''
                                ];
                                array_push($adList,$adNull);
                                $count++;//创建的空闲时段计入记录总条数
                            }
                        }
                        //广告记录
                        $ad = [
                            'fcreateperson'    => $row['fmodifier'],
                            'fissueid'         => $row['fissueid'],
                            'fadname'          => $row['fadname'],
                            'fadclass'         => $row['fadclass'],
                            'fadclasscode'     => $row['fadclasscode'],
                            'fstarttime'       => date('Y-m-d H:i:s',$row['fstarttime']),
                            'fendtime'         => date('Y-m-d H:i:s',$row['fendtime']),
                            'flength'          => $row['flength'],
                            'fillegaltypecode' => $row['fillegaltypecode'] > 0 ? '违法' : '不违法',
                            'fexpressioncodes' => $row['fexpressioncodes'],
                            'fexpressions'     => $row['fexpressions']
                        ];
                        array_push($adList,$ad);
                        //最后一条后的空时间段
                        if($key == (count($List)-1)){
                            $interval = strtotime($fendtime) - $row['fendtime']; //与结束时间间隔大于3秒,则创建一个空时间段
                            if($interval > 3){
                                $adNull = [
                                    'fcreateperson'    => '',
                                    'fadname'          => '--',
                                    'fadclass'         => '',
                                    'fstarttime'       => date('Y-m-d H:i:s',$row['fendtime']),
                                    'fendtime'         => date('Y-m-d H:i:s',strtotime($fendtime)),
                                    'flength'          => $interval,
                                    'fillegaltypecode' => '',
                                    'fexpressioncodes' => '',
                                    'fexpressions'     =>  ''
                                ];
                                array_push($adList,$adNull);
                                $count++;//创建的空闲时段计入记录总条数
                            }
                        }
                    }
                }else{
                    $adNull = [
                        'fcreateperson'    => '',
                        'fadname'          => '--',
                        'fadclass'         => '',
                        'fstarttime'       => $fstarttime,
                        'fendtime'         => $fendtime,
                        'flength'          => strtotime($fendtime) - strtotime($fstarttime),
                        'fillegaltypecode' => '',
                        'fexpressioncodes' => '',
                        'fexpressions'     => ''
                    ];
                    array_push($adList,$adNull);
                    $count++;//创建的空闲时段计入记录总条数
                }
                $resultList = array_slice($adList,$limitIndex,$pageSize);
                $dataSet = [
                    'fid'=> $fid,
                    'live_m3u8_url' => $m3u8_url,
                    'fad' => $resultList
                ];
                $countNotAds = $count - $countAds;
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$count, 'countAds'=>$countAds,'countNotAds' => $countNotAds, 'data'=>$dataSet]);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'未找到该检查计划']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 数据检查-新增结果
     */
    public function AddResult(){
        $fdata_check_plan_id = $this->P['fid'];
        $fissueid            = $this->P['fissueid'] ? $this->P['fissueid']: 0;
        $fstart              = $this->P['fstart'];
        $fend                = $this->P['fend'];
        $fpage               = $this->P['fpage'] ? $this->P['fpage'] : '';
        $fadname             = $this->P['fadname'] ? $this->P['fadname'] : '';
        $fresult_type        = $this->P['fresult_type'];
        $fresult             = $this->P['fresult'];
        $fcreateperson       = $this->P['fcreateperson'];
        if($fdata_check_plan_id){
            $dataRow = [
                'fdata_check_plan_id' => $fdata_check_plan_id,
                'fissueid' => $fissueid,
                'fstart' => $fstart,
                'fend' => $fend,
                'fpage' => $fpage,
                'fadname' => $fadname,
                'fresult_type' => $fresult_type,
                'fresult' => $fresult,
                'fcreator' => session('regulatorpersonInfo.fid'),
                'fcreatetime' => date('Y-m-d H:i:s'),
                'fcreateperson' => $fcreateperson
            ];
            $condition = [
                'fdata_check_plan_id' => $fdata_check_plan_id,
                'fstart' => $fstart,
                'fend' => $fend,
                'fpage' => $fpage,
                'fadname' => $fadname,
                'fresult_type' => $fresult_type,
                'fresult' => $fresult
            ];
            $isExist = M('tbn_data_check_result')->where($condition)->count() > 0 ? true : false;
            if(!$isExist){
                $fid = M('tbn_data_check_result')->add($dataRow);
                if($fid){
                    $this->ajaxReturn(['code'=>0,'msg'=>'成功']);
                }
            }
            $this->ajaxReturn(['code'=>1,'msg'=>'请勿重复添加']);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 数据检查-修改发布记录
     * by zw
     */
    public function EditResult(){
        $fissueid           = $this->P['fissueid'];//发布记录ID
        $fmediaid           = $this->P['fmediaid'];//媒体ID
        $fstart             = $this->P['fstart'];//开始时间
        $fend               = $this->P['fend'];//结束时间
        $fadname            = $this->P['fadname'];//广告名称
        $fadclass           = $this->P['fadclass'];//广告类别
        $fexpressioncodes   = $this->P['fexpressioncodes'];//违法表现代码
        $mediaclass = M('tmedia') ->where(['fid'=>$fmediaid])->getField('fmediaclassid');
        $mediaclass = substr($mediaclass,0,2);
        $region = substr($system_num,0,2);
        if($mediaclass == '01'){
            $tbname_issue = gettable('tv',strtotime($fstart));
            $tbname_sample = 'ttvsample';
        }elseif($mediaclass == '02'){
            $tbname_issue = gettable('bc',strtotime($fstart));
            $tbname_sample = 'tbcsample';
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'参数有误']);
        }
        $do_view = M($tbname_issue)
            ->field('c.fbrand,c.fadowner,c.fadname,c.fadclasscode,c.fadid,b.fid,a.fstarttime,a.fendtime')
            ->alias('a')
            ->join($tbname_sample.' b on a.fsampleid = b.fid')
            ->join('tad c on b.fadid = c.fadid')
            ->where(['a.fid'=>$fissueid])
            ->find();

        if(!empty($do_view)){
            //如果广告名称有更改
            if(($do_view['fadname']!=$fadname || $fadclass != $do_view['fadclasscode']) && !empty($fadclass) && !empty($fadname)){
                $data_ad['fadname'] = $fadname;
                $data_ad['fbrand'] = $do_view['fbrand'];
                $data_ad['fadclasscode'] = $fadclass;
                $data_ad['fadowner'] = $do_view['fadowner'];
                $data_ad['fcreator'] = session('regulatorpersonInfo.fname');
                $data_ad['fcreatetime'] = date('Y-m-d H:i:s');
                $data_ad['fstate'] = 1;
                $data_ad['is_sure'] = 0;
                $data_ad['confirmer'] = 0;
                $data_ad['comment'] = '数据检查新增';
                $sample_data['fadid'] = M('tad') -> add($data_ad);
            }

            //如果违法广告有变更
            if(!empty($fexpressioncodes)){
                if($fexpressioncodes === true){
                    $sample_data['fillegaltypecode'] = 0;
                    $sample_data['fexpressioncodes'] = '';
                    $sample_data['fexpressions'] = '';
                    $sample_data['fconfirmations'] = '';
                }else{
                    $sample_data['fillegaltypecode'] = 30;
                    $sample_data['fexpressioncodes'] = $fexpressioncodes;
                    // 取出违法表现内容及判定依据
                    $arrExpressioncode = explode(',',$fexpressioncodes);
                    foreach($arrExpressioncode as $key=>$fcode){
                        $fexpressionInfo = M('tillegal')->where(['fcode'=>$fcode,'fstate'=>1])->find();
                        $sample_data['fexpressions'] .= $fexpressionInfo['fexpression'].';';
                        $sample_data['fconfirmations'] .= $fexpressionInfo['fconfirmation'].';';
                    }
                }
            }

            //更改样本信息
            if(!empty($sample_data)){
                M($tbname_sample) -> where(['fid'=>$do_view['fid']]) ->save($sample_data);
            }
            if(($do_view['fstarttime'] != strtotime($fstart) || $do_view['fendtime'] != strtotime($fend)) && !empty($fstart) && !empty($fend)){
                M($tbname_issue)->where(['fid'=>$fissueid])->save(['fstarttime'=>strtotime($fstart),'fendtime'=>strtotime($fend)]);
            }

            $this->ajaxReturn(['code'=>0,'msg'=>'修改成功']);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'参数有误']);
        }

    }

    /**
     * 数据检查-删除结果
     */
    public function DeleteResult(){
        $fid = $this->P['fid'];
        $c = 0;
        if($fid){
            $fids = explode(',', $fid);
            foreach($fids as $k=>$v){
                $del = M('tbn_data_check_result')->where(['fid'=>$v])->delete();
                if($del){
                    $c++;
                }
            }
            if($c){
                $this->ajaxReturn(['code'=>0,'msg'=>'成功删除'.$c.'条记录']);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'未删除任何数据']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 数据检查计划-查看结果
     */
    public function ViewResult(){
        $system_num = getconfig('system_num');
        $fid = $this->P['fid'];
        $nottype = $this->P['nottype'];//排除结果类型，多个以逗号分隔
        $pageIndex = $this->P['pageIndex'] ? $this->P['pageIndex'] : 1;
        $pageSize = $this->P['pageSize'] ? $this->P['pageSize'] : 1000;
        $limitIndex = ($pageIndex-1)*$pageSize;
        if($fid){
            $where['fdata_check_plan_id'] = $fid;
            if($nottype != '') $where['fresult_type'] = ['NOT IN',$nottype];//排除结果类型 0-遗漏 1-错误 2-误判
            $count = M('tbn_data_check_result')->where($where)->count();
            $dataSet = M('tbn_data_check_result')
                ->where($where)
                ->limit($limitIndex,$pageSize)
                ->order('fstart ASC,fpage ASC')
                ->select();
            $resultMap = [
                'flength' => '时长',
                'fadname' => '广告名称',
                'fadclass' => '广告类别',
                'fexpressioncodes' => '违法表现',
                'fothers' => '其它'
            ];
            if(!empty($dataSet)){
                foreach($dataSet as $key => $row){
                    $resultStr = '';
                    $fexpressioncodes = '';
                    $fexpressions = '';
                    $fresult = json_decode($row['fresult'],true);
                    foreach($fresult as $k => $v){
                        if($v === true){
                            $resultStr .= ' '.$resultMap[$k].' ; ';
                        }elseif($v != ''){
                            if($k == 'flength'){
                                $flength = explode(',',$v);
                                $v = $flength[2].'\'';
                                // $v = date('Y-m-d H:i:s',$flength[0]).'~'.date('Y-m-d H:i:s',$flength[1]).'('.$flength[2].')';
                            }
                            if($k == 'fadclass'){
                                $v = M('tadclass')->cache(true,120)->where(['fcode'=>$v])->getField('ffullname');
                            }
                            $resultStr .= ' '.$resultMap[$k].':'.$v.' ; ';
                            if($k == 'fexpressioncodes'){
                                $fexpressioncodes = $v;
                                $codeArr = explode(',',$v);
                                foreach($codeArr as $i => $value){
                                    $fexpressions .= M('tillegal')->cache(true,60)->where(['fcode'=>$value])->getField('fexpression').' | ';
                                }
                            }
                        }
                    }
                    $dataSet[$key]['fresultStr'] = $resultStr;
                    $dataSet[$key]['fexpressioncodes'] = $fexpressioncodes;
                    $dataSet[$key]['fexpressions'] = $fexpressions;
                }
            }
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$count, 'data'=>$dataSet]);
        }else{
            $fmediaid = $this->P['fmediaid'];//媒体ID
            $fstatus = (int)$this->P['fstatus'];//处理状态
            $fstart = $this->P['fstart'];//发布开始日期
            $fend = $this->P['fend'];//发布结束日期
            $fresult_type = (int)$this->P['fresult_type'];//类型

            $where['tbn_data_check_plan.fcustomer'] = $system_num;
            if($fresult_type != -1){
                $where['tbn_data_check_result.fresult_type'] = $fresult_type;
            }
            if(!empty($fmediaid)){
                $where['tbn_data_check_plan.fmediaid'] = $fmediaid;
            }
            if(!empty($fstart) && !empty($fend)){
                $where['tbn_data_check_plan.fissuedate'] = ['between',[$fstart,$end]];
            }
            if($fstatus != -1){
                $where['tbn_data_check_result.fstatus'] = $fstatus;
            }

            $count = M('tbn_data_check_result')
                ->join('tbn_data_check_plan on tbn_data_check_plan.fid = tbn_data_check_result.fdata_check_plan_id')
                ->join('tmedia on tmedia.fid = tbn_data_check_plan.fmediaid')
                ->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
                ->where($where)
                ->count();
            $dataSet = M('tbn_data_check_result')
                ->field('tbn_data_check_result.*,tmedia.fmedianame,tbn_data_check_plan.fmediaid')
                ->join('tbn_data_check_plan on tbn_data_check_plan.fid = tbn_data_check_result.fdata_check_plan_id')
                ->join('tmedia on tmedia.fid = tbn_data_check_plan.fmediaid')
                ->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
                ->where($where)
                ->limit($limitIndex,$pageSize)
                ->order('fstart ASC,fpage ASC')
                ->select();
            $resultMap = [
                'flength' => '时长',
                'fadname' => '广告名称',
                'fadclass' => '广告类别',
                'fexpressioncodes' => '违法表现',
                'fothers' => '其它'
            ];
            $resultStr = '';
            $fexpressioncodes = '';
            $fexpressions = '';
            foreach($dataSet as $key => $row){
                $dataSet[$key]['issueurl'] = A('Common/Media','Model')->get_m3u8($row['fmediaid'],strtotime($row['fstart']),strtotime($row['fend']));
                $resultStr = '';
                $fresult = json_decode($row['fresult'],true);
                foreach($fresult as $k => $v){
                    if($v === true){
                        $resultStr .= ' '.$resultMap[$k].' ; ';
                    }elseif($v != ''){
                        if($k == 'flength'){
                            $flength = explode(',',$v);
                            $v = $flength[2].'\'';
                            // $v = date('Y-m-d H:i:s',$flength[0]).'~'.date('Y-m-d H:i:s',$flength[1]).'('.$flength[2].')';
                        }
                        if($k == 'fadclass'){
                            $v = M('tadclass')->cache(true,120)->where(['fcode'=>$v])->getField('ffullname');
                        }
                        $resultStr .= ' '.$resultMap[$k].':'.$v.' ; ';
                        if($k == 'fexpressioncodes'){
                            $fexpressioncodes = $v;
                            $codeArr = explode(',',$v);
                            foreach($codeArr as $i => $value){
                                $fexpressions .= M('tillegal')->cache(true,60)->where(['fcode'=>$value,'fstate'=>1])->getField('fexpression').' | ';
                            }
                        }
                    }
                }
                $dataSet[$key]['fresultStr'] = $resultStr;
                $dataSet[$key]['fexpressioncodes'] = $fexpressioncodes;
                $dataSet[$key]['fexpressions'] = $fexpressions;
            }

            $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$count, 'data'=>$dataSet]);
        }
    }

    // //获取视频播放地址
    // public function get_issueurl(){
    //     $fmediaid = $this->P['fmediaid'];
    //     $fstart = $this->P['fstart'];
    //     $fend = $this->P['fend'];
    //     $url = A('Common/Media','Model')->get_m3u8($fmediaid,strtotime($fstart),strtotime($fend));
    //     $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'url'=>$url]);
    // }

    /**
     * 数据检查-汇总检查结果
     */
    protected function resultSummary($fid){
        if($fid){
            $where = [
                'fdata_check_plan_id' => $fid,
            ];
            $ret = M('tbn_data_check_result')
                ->field('fresult_type,count(fresult_type) AS count')
                ->where($where)
                ->group('fresult_type')
                ->select();
            return $ret;
        }else{
            return false;
        }
    }

    /**
     * 完成检查-提交
     */
    public function Submit(){
        $fid = $this->P['fid'];
        $regulatorpersonInfo = session('regulatorpersonInfo');
        if($fid){
            $fchecker = $regulatorpersonInfo['fid'];
            $ffinishdate = date('Y-m-d H:i:s');
            $fstate = 7;
            $fsummaryArr = $this->resultSummary($fid);
            $fsummary = '';
            if(!empty($fsummaryArr)){
                $total = 0;
                foreach($fsummaryArr as $key => $val){
                    $count = (int)$val['count'];
                    $total += $count;
                    if($count > 0){
                        switch($val['fresult_type']){
                            case "0":
                                $fsummary .= '遗漏：'.$count.'条 ';
                                break;
                            case "1":
                                $fsummary .= '错误：'.$count.'条 ';
                                break;
                            case "2":
                                $fsummary .= '误判：'.$count.'条 ';
                                break;
                        }
                    }
                }
                if($total == 0) $fsummary = '';//正常
            }else{
                $fsummary = '';//正常
            }
            $data = [
                'fchecker' => $fchecker,
                'ffinishdate' => $ffinishdate,
                'fstate' => $fstate,
                'fsummary' => $fsummary
            ];
            $ret = M('tbn_data_check_plan')->where(['fid'=>$fid])->save($data);
            if($ret){
                $this->ajaxReturn(['code'=>0,'msg'=>'提交成功']);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'提交时出错']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 完成检查-提交2
     */
    public function submitPlan(){
        $fid = $this->P['fid'];
        $regulatorpersonInfo = session('regulatorpersonInfo');
        if($fid){
            $fchecker = $regulatorpersonInfo['fid'];
            $ffinishdate = date('Y-m-d H:i:s');
            $fstate = 7;
            $fsummaryArr = $this->resultSummary($fid);
            $fsummary = '';
            if(!empty($fsummaryArr)){
                $total = 0;
                foreach($fsummaryArr as $key => $val){
                    $count = (int)$val['count'];
                    $total += $count;
                    if($count > 0){
                        switch($val['fresult_type']){
                            case "0":
                                $fsummary .= '遗漏：'.$count.'条 ';
                                break;
                            case "1":
                                $fsummary .= '错误：'.$count.'条 ';
                                break;
                            case "2":
                                $fsummary .= '误判：'.$count.'条 ';
                                break;
                        }
                    }
                }
                if($total == 0) $fsummary = '';//正常
            }else{
                $fsummary = '';//正常
            }
            $data = [
                'fchecker' => $fchecker,
                'ffinishdate' => $ffinishdate,
                'fstate' => $fstate,
                'fsummary' => $fsummary
            ];
            $ret = M('tbn_data_check_plan')->where(['fid'=>$fid])->save($data);
            if($ret){
                $this->ajaxReturn(['code'=>0,'msg'=>'提交成功']);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'提交时出错']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 根据媒介获取可检查日期
     */
    public function getAvaDateByMedia(){
        $fmediaid   = $this->P['fmediaid'];
        if($fmediaid){
            $avaTimeStamp = A('Common/Media','Model')->available_time($fmediaid);
            $data = [
                'avatimestamp' => date('Y-m-d',$avaTimeStamp)
            ];
            $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$data]);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 返回违法表现树
     * @return array
     */
    public function getIllegalArr()
    {
        $data = M('tillegal')->field('fcode, fpcode, fexpression, fconfirmation, fillegaltype')->where(['fstate' => ['EQ', 1]])->cache(true)->select();
        $res = [];
        foreach ($data as &$datum) {
            $datum['id'] = $datum['fcode'];
            $datum['label'] = $datum['fexpression'];
            if (!$datum['fpcode']){
                $res[] = $datum;
            }
        }
        foreach ($data as $item) {
            foreach ($res as &$re) {
                if ($item['fpcode'] === $re['fcode']){
                    $re['children'][] = $item;
                    break;
                }
            }
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$res]);
        // return $res;
    }

	/**
	* 导出广告抽查记录
	* @Param	Mixed variable
	* @Return	void 返回的数据
	*/
	public function ExportCheckList(){
		set_time_limit(600);
        $system_num = getconfig('system_num');//客户编号
    	$file_type  = $this->P['file_type'] ? strtoupper($this->P['file_type']) : 'EXCEL';//导出文件类型 EXCEL、TXT、XML
    	$file_name  = $this->P['file_name'] ? $this->P['file_name'] : '导出文件'.time();	 //导出文件名
        $regulatorpersonInfo = session('regulatorpersonInfo');
        $fmonth     = strtotime($this->P['fmonth']);
        $fregionid  = $this->P['fregionid'];
        $fmediaid   = $this->P['fmediaid'];
        $fstate     = $this->P['fstate'];
        $fissuedate = $this->P['fissuedate'];
        $fdstatus   = $this->P['fdstatus'];
        $fstate     = $this->P['fdstatus'] ? $this->P['fdstatus'] : $this->P['fstate'];
        $pageIndex  = $this->P['pageIndex'] ? $this->P['pageIndex'] : 1;
        $pageSize   = $this->P['pageSize'] ? $this->P['pageSize']  : 1000;
        $limitIndex = ($pageIndex-1)*$pageSize;
        $where = [
            'fcustomer' => $system_num
        ];
        if(!empty($fmonth)){
            $where['fmonth'] = $fmonth;
        }
        if(!empty($fmediaid)){
            $where['fmediaid'] = $fmediaid;
        }
        if(!empty($fissuedate)){
            $where['fissuedate'] = $fissuedate;
        }
        if(!empty($fstate)){
            // 状态搜索：(0-不可检查)、1-待检查、2-正检查、3-已检查
            switch($fstate){
                case 1:
                    $where['fstate'] = ['IN','0,5'];break;
                case 2:
                    $where['fstate'] = ['IN','1,3,6,8'];break;
                case 3:
                    $where['fstate'] = ['IN','2,7'];break;
                default:
                    $where['fstate'] = ['IN','0,1,2,3,4,5,6,7,8'];break;
            }
        }
        $count = M('tbn_data_check_plan')->where($where)->count();
		if ($count > 20000){
			$this->ajaxReturn(array('code'=>1,'msg'=>'无法导出，请筛选条件确保数据在20000条以内！'));
		}
        $dataSetOrg = [];
        if($count > 0){
            $dataSetOrg = M('tbn_data_check_plan')
                ->where($where)
                ->order('fplantime DESC')
                ->select();
            foreach($dataSetOrg as $key=>$row){
                $fshowstatus = 1;
                if(in_array($row['fstate'],[0,5])){
                    $fshowstatus = 1;
                }elseif(in_array($row['fstate'],[1,6,3,8])){
                    $fshowstatus = 2;
                }elseif(in_array($row['fstate'],[2,7])){
                    $fshowstatus = 3;
                }
                $dataSetOrg[$key] = [
                    'fid' => $row['fid'],
                    'fshowstatus' => $fshowstatus,
                    'fmediaid' => $row['fmediaid'],
                    'fmedianame' => M('tmedia')->cache(true,60)->where(['fid'=>$row['fmediaid']])->getField('fmedianame'),
                    'fissuedate' => date('Y-m-d',strtotime($row['fissuedate'])),
                    'fplantime' => $row['fplantime'],
                    'ffinishdate' => $row['ffinishdate'] ? date('Y-m-d H:i:s',strtotime($row['ffinishdate'])) : '',
                    'fchecker' => M('tregulatorperson')->cache(true,60)->where(['fid'=>$row['fchecker']])->getField('fname'),
                    'fsummary' => $row['fsummary'],
                ];
            }
        }
		//导出文件数据集含表头
		$dataSet[0] = ['序号','媒体','日期','状态','结果','创建时间','抽查人','抽查时间'];
		foreach ($dataSetOrg as $k => $row){
			//显式地赋值,便于与表头对应及后续维护
			$data[$k] = [
				'findex'      => $k+1,
				'fmedianame'  => $row['fmedianame'],
				'fissuedate'  => $row['fissuedate'],
				'fshowstatus' => $this->showStatus[$row['fshowstatus']],
				'fsummary'    => $row['fsummary'],
				'fplantime'   => $row['fplantime'],
				'fchecker'    => $row['fchecker'],
				'ffinishdate' => $row['ffinishdate'],
            ];
            array_push($dataSet,$data[$k]);
		}
        if(!$dataSet || empty($dataSet)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
        }
        if($file_type=='EXCEL'){
            $this->downloadExcel($dataSet);
        }elseif($file_type=='XML'){
            $this->downloadXml($dataSet);
        }elseif($file_type=='TXT'){
            $this->downloadTxt($dataSet);
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'暂不支持导出该文件类型，可选项：EXCEL、XML、TXT'));
        }
	}

	/**
     * 导出到Excel文件
     * @Param   Array $data 数据集，包含表头且表头与内容列顺序对应
     * @Return  String $url 生成上传OSS文件下载地址
     */
	public function downloadExcel($data)
	{
		header("Content-type: text/html; charset=utf-8");
	    $objPHPExcel = new \PHPExcel();
	    $objPHPExcel
            ->getProperties()                   //获得文件属性对象，给下文提供设置资源
            ->setCreator("DATAGATHER")          //设置文件的创建者
            ->setLastModifiedBy("DATAGATHER")   //设置最后修改者
            ->setTitle("导出文件")               //设置标题
            ->setSubject("Office2007 XLSX" )    //设置主题
            ->setDescription("DATAGATHER")      //设置备注
            ->setKeywords("DATAGATHER")         //设置标记
            ->setCategory("XLSX");              //设置类别
        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        $AZ = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
		foreach ($data as $key => $value) {
            $i = 0;
            foreach($value as $col => $content){
                $cell = $AZ[$i].($key+1);
                $sheet -> setCellValue($cell,$content);
                $i++;
            }
		}
		$objActSheet =$objPHPExcel->getActiveSheet();
		$objActSheet->setTitle('广告抽查记录');//设置当前活动工作表名称
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $filename = '广告抽查记录'.date('YmdHis').'.xlsx';
        $dir = './LOG/';
        if(!file_exists($dir)){
            mkdir($dir);
        }
		$savefile = $dir.$filename;
		$objWriter->save($savefile);
		D('Function')->write_log('广告抽查记录',1,'Excel导出成功');
        $ret = A('Common/AliyunOss','Model')
            ->file_up('tem/'.$filename,$savefile,['Content-Disposition' => 'attachment;filename=']);//上传云
		unlink($savefile);//删除文件
		$this->ajaxReturn(['code'=>0,'msg'=>'导出成功','data'=>$ret['url']]);
	}

	/**
     * 导出到XML文件
     * @Param   Array $data 数据集，包含表头且表头与内容列顺序对应
     * @Return  String $url 生成上传OSS文件下载地址
     */
	public function downloadXml($data)
	{
		$xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        $xml .= "<ads>\n";
		foreach ($data as $key =>$value) {
            if($key > 0){
                //第一行表头不导出
                $xml .= "<item>\n";
                foreach($value as $col => $content){
                    $xml .= "<".$col.">" . $content . "</".$col.">\n";
                }
                $xml .= "</item>\n";
            }
		}
		$xml .= "</ads>\n";

        $filename = '广告抽查记录'.date('YmdHis').'.xml';
        $dir = './LOG/';
        if(!file_exists($dir)){
            mkdir($dir);
        }
		$savefile = $dir.$filename;
		file_put_contents($savefile,$xml);//生成本地文件
		D('Function')->write_log('广告抽查记录',1,'XML导出成功');
        $ret = A('Common/AliyunOss','Model')
            ->file_up('tem/'.$filename,$savefile,['Content-Disposition' => 'attachment;filename=']);//上传云
		unlink($savefile);//删除文件
		$this->ajaxReturn(['code'=>0,'msg'=>'导出成功','data'=>$ret['url']]);
	}

	/**
     * 导出到TXT文件
     * @Param   Array $data 数据集，包含表头且表头与内容列顺序对应
     * @Return  String $url 生成上传OSS文件下载地址
     */
	public function downloadTxt($data)
	{
		$txtcontent = '';
		foreach ($data as $key => $value) {
            foreach($value as $col => $content){
                $txtcontent .= $content.'  ';
            }
            $txtcontent .= "\n";
		}
        $filename = '广告抽查记录'.date('YmdHis').'.txt';
        $dir = './LOG/';
        if(!file_exists($dir)){
            mkdir($dir);
        }
		$savefile = $dir.$filename;
		file_put_contents($savefile,$txtcontent);
		D('Function')->write_log('广告抽查记录',1,'TXT导出成功');
        $ret = A('Common/AliyunOss','Model')
            ->file_up('tem/'.$filename,$savefile,['Content-Disposition' => 'attachment;filename=']);//上传云
		unlink($savefile);//删除文件
		$this->ajaxReturn(['code'=>0,'msg'=>'导出成功','data'=>$ret['url']]);
	}
}
