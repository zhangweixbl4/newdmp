<?php
namespace Api\Controller;
use Think\Controller;

class M3u8RecController extends Controller {
	

	
	/*获取回放地址*/
	public function get_playback_url(){
		
		$fid = I('fid');//媒介id
		$start_time = strtotime(I('start_time'));//开始时间戳
		$end_time = $start_time + 3600;
		
		$mediaInfo = M('tmedia')->where(array('fid'=>$fid))->find();
		
		//$get_playback_url = 
		if($get_playback_url == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'M3U8失败'));
		if($get_playback_url != '') $this->ajaxReturn(array('code'=>0,'playback_url'=>$get_playback_url));

	}
	
	/**
	 * 创建生成素材任务
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * media_id 媒体ID，Sstart开始时间，Send结束时间，source_name素材名称，source_type素材类别，业务表主键，source_id当前素材表主键
	 * by zw
	 */
	public function add_source_make(){
		$source_id 		= I('source_id');//任务ID
		$tid 			= I('tid')?I('tid'):0;//相关表ID
		$media_id 		= I('media_id');//媒介id
		$source_name 	= I('source_name');//素材名称
		$source_type 	= I('source_type') === ''?99:I('source_type');//生成类型
		$Sstart 		= I('Sstart');//开始时间
		$Send 			= I('Send');//结束时间
		$source_state = 1;//状态

		$Sstart_time 	= strtotime($Sstart);
		$Send_time 		= strtotime($Send);

		$doMedia = M('tmedia')->where(['fid'=>$media_id])->find();
		if(empty($doMedia)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'媒体不存在'));
		}else{
			$mclass = substr($doMedia['fmediaclassid'], 0, 2);
		}

		if(empty($source_id)){
			if($mclass == '01' || $mclass == '02'){
				if(($Send_time - $Sstart_time) > 7200 ){
					$this->ajaxReturn(array('code'=>1,'msg'=>'不允许超过2小时'));
				}elseif(($Send_time - $Sstart_time) < 2){
					$this->ajaxReturn(array('code'=>1,'msg'=>'不允许小于2秒'));
				}
			}else{
				if(($Send_time - $Sstart_time) > 86400*7 ){
					$this->ajaxReturn(array('code'=>1,'msg'=>'不允许超过7天'));
				}
			}
		}
		if(!empty($source_id)){
			$a_data['source_state'] = $source_state;
			$a_data['create_time'] = date('Y-m-d H:i:s');
			$a_data['creater_id'] = session('regulatorpersonInfo.fid')?session('regulatorpersonInfo.fid'):0;
			$a_data['fail_cause'] = '';
			$a_data['source_url'] = '';
			M('source_make')->where('source_id='.$source_id)->save($a_data);
			$do_se = M('source_make')->where('source_id='.$source_id)->find();
			$media_id 		= $do_se['media_id'];
			$source_name 	= $do_se['source_name'];
			$start_time 	= strtotime($do_se['start_time']);
			$end_time 		= strtotime($do_se['end_time']);
		}else{
			if(empty($source_name)){
				$this->ajaxReturn(array('code'=>1,'msg'=>'素材名称不能为空'));
			}

			$a_data['media_id'] = $media_id;
			$a_data['source_name'] = $source_name;
			$a_data['start_time'] = $Sstart;
			$a_data['end_time'] = $Send;
			$a_data['source_state'] = $source_state;
			$a_data['create_time'] = date('Y-m-d H:i:s');
			$a_data['creater_id'] = session('regulatorpersonInfo.fid')?session('regulatorpersonInfo.fid'):0;
			$a_data['source_type'] = $source_type;
			$a_data['source_tid'] = $tid;
			$a_data['source_treid'] = session('regulatorpersonInfo.fregulatorpid');
			$a_data['source_mediaclass'] = $mclass;
			$source_id = M('source_make')->add($a_data);
			$start_time = strtotime($Sstart);
			$end_time = strtotime($Send);
		}
		/**************************************************************************************************/
		
		if($mclass == '01'){//判断是电视
			$cut_url = C('ISSUE_CUT_URL');//剪辑电视广告片段的url
			$m3u8_url = A('Common/Media','Model')->get_m3u8($media_id,$start_time,$end_time);
			$back_url = 'http://'.$_SERVER['HTTP_HOST'] . U('Api/M3u8Rec/edit_tv_ad_part/source_id/'.$source_id) ;
			$post_data = array(
				'tvissue_id'=>$source_id,
				'start_time'=>$start_time,
				'end_time'=>$end_time,
				'm3u8_url'=>urlencode($m3u8_url),
				'file_type'=>'mp4',
				'back_url'=>urlencode($back_url),
			);
			$ret = http($cut_url,$post_data,'GET',false,1);
		}elseif($mclass == '02'){//判断是广播
			$cut_url = C('ISSUE_CUT_URL');//剪辑广播广告片段的url
			$m3u8_url = A('Common/Media','Model')->get_m3u8($media_id,$start_time,$end_time);
			$back_url = 'http://'.$_SERVER['HTTP_HOST'] . U('Api/M3u8Rec/edit_bc_ad_part/source_id/'.$source_id) ;
			$post_data = array(
				'bcissue_id'=>$source_id,
				'start_time'=>$start_time,
				'end_time'=>$end_time,
				'm3u8_url'=>urlencode($m3u8_url),
				'file_type'=>'mp3',
				'back_url'=>urlencode($back_url),
			);
			$ret = http($cut_url,$post_data,'GET',false,1);
		}elseif($mclass == '03'){//判断是报纸
			$cut_url = C('RAR_FILE_URL');
			$back_url = 'http://'.$_SERVER['HTTP_HOST'] . U('Api/M3u8Rec/edit_paper_ad_part/source_id/'.$source_id) ;

			$where_paper['tpaperissue.fissuedate'] = array('between',array($Sstart,$Send));
			$where_paper['tpaperissue.fmediaid'] = $media_id;
			$paperissueDetails = M('tpaperissue')
				->field('concat(fadname,DATE_FORMAT(tpaperissue.fissuedate,"%Y年%m月%d日"),".",substring_index(furl,".",-1)) as name,furl as url')
				->join('tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
				->join('tad on tad.fadid = tpapersample.fadid and tad.fadid<>0')
				->join('tpapersource on tpapersource.fid = tpapersample.sourceid','LEFT')
				->where($where_paper)
				->select();//查询样本详情
			if(!empty($paperissueDetails)){
				$post_data['debug'] = 0;
				$post_data['file_name'] = $source_name.'.rar';
				$post_data['back_url'] = $back_url;
				$post_data['file_list'] = $paperissueDetails;
				$ret = http($cut_url,json_encode($post_data),'post',false,1);
			}else{
				M()->execute('delete from source_make where source_id='.$source_id);
				$this->ajaxReturn(array('code'=>1,'msg'=>'暂无信息可下载'));
			}
			
		}

		if(!empty($source_id) || !empty($do_se)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'已进入创建流程','data'=>array('source_id'=>$source_id,'source_state'=>$source_state)));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'创建失败,未知原因'));
		}
		
	}
	
	/*修改发布记录视频片段地址*/
	public function edit_tv_ad_part(){
		$source_id = I('get.source_id');

		$post_data = file_get_contents('php://input');

		$post_data = json_decode($post_data,true);
		
		//0为生成成功，非0为生成失败
		if($post_data['code']==0 && !empty($post_data['source_url'])){
			$source_state = 2;
		}else{
			$source_state = -1;
		}

		M('source_make')
			->where(array('source_id'=>$source_id))
			->save(array(
							'source_url'=>$post_data['source_url'],
							'validity_time'=>$post_data['validity_date']?strtotime($post_data['validity_date']):0,
							'fail_cause'=>$post_data['msg'],
							'source_state'=>$source_state
							));
		
	}
	
	/*修改发布记录视频片段地址*/
	public function edit_bc_ad_part(){
		$source_id = I('get.source_id');
		$post_data = file_get_contents('php://input');
		
		$post_data = json_decode($post_data,true);

		//0为生成成功，非0为生成失败
		if($post_data['code']==0 && !empty($post_data['source_url'])){
			$source_state = 2;
		}else{
			$source_state = -1;
		}

		M('source_make')
			->where(array('source_id'=>$source_id))
			->save(array(
				'source_url'=>$post_data['source_url'],
				'validity_time'=>$post_data['validity_date']?strtotime($post_data['validity_date']):0,
				'fail_cause'=>$post_data['msg'],
				'source_state'=>$source_state
				));
	}

	/*修改发布记录视频片段地址*/
	public function edit_paper_ad_part(){
		$source_id = I('get.source_id');
		$post_data = file_get_contents('php://input');
		
		$post_data = json_decode($post_data,true);

		//0为生成成功，非0为生成失败
		if($post_data['code']==0 && !empty($post_data['source_url'])){
			$source_state = 2;
		}else{
			$source_state = -1;
		}

		M('source_make')
			->where(array('source_id'=>$source_id))
			->save(array(
				'source_url'=>$post_data['source_url'],
				'validity_time'=>$post_data['validity_date']?strtotime($post_data['validity_date']):0,
				'fail_cause'=>$post_data['msg'],
				'source_state'=>$source_state
				));
	}

	/**
	 * 返回拼接结果
	 * by zw
	 */
	public function returnCutData(){
		$fidentify = I('fidentify');//主键
		if(empty($fidentify)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
		}
		if(is_array($fidentify)){
			$whereSM['source_id'] = ['in',$fidentify];
		}else{
			$whereSM['source_id'] = $fidentify;
		}
		$whereSM['source_state'] = ['neq',1];

		$doSM = M('source_make')->field('source_id,source_state,source_url,fail_cause')->where($whereSM)->select();
		if(!empty($doSM)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$doSM));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂无结果'));
		}
	}

	/**
	 * 监测素材生成状态
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * by zw
	 */
	public function get_source_make_state(){
		$selsource_id = I('selsource_id');//获取选中项

		if(!empty($selsource_id)){
			$where['source_id'] = array('in',$selsource_id);
			$where['source_state'] = 2;

			$do_sme = M('source_make')->field('source_id,source_url')->where($where)->select();

			if(!empty($do_sme)){
				$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_sme));
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'暂无更新'));
			}
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂无更新'));
		}
	}

	/**
	 * 监测素材生成状态超时
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * by zw
	 */
	public function get_source_make_state2(){
		$sqlstr = 'select count(*) AS cc from source_make where (unix_timestamp(now())-unix_timestamp(create_time))>(unix_timestamp(end_time)-unix_timestamp(start_time)) and source_state=1';
		$do_sme = M()->query($sqlstr);
		if($do_sme[0]['cc']>0){
			M()->execute('update source_make set source_state=-1 where (unix_timestamp(now())-unix_timestamp(create_time))>(unix_timestamp(end_time)-unix_timestamp(start_time)) and source_state=1');
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂无更新'));
		}
		
	}
}