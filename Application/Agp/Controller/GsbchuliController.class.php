<?php
namespace Agp\Controller;
use Think\Controller;

import('Vendor.PHPExcel');

/**
 * 国家局处理结果上报
 * by zw
 */

class GsbchuliController extends BaseController{

  /**
  * 案件列表
  * by zw
  */
  public function anjian_list(){
    session_write_close();
    Check_QuanXian(['sbchuli']);
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $agpDataMerge = $ALL_CONFIG['agpDataMerge'];
    $cusHosts = json_decode($ALL_CONFIG['agpCustomerMarge'],true);

    $outtype = I('outtype');//导出类型
    if(!empty($outtype)){
      $p  = I('page', 1);//当前第几页ks
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页ks
      $pp = 20;//每页显示多少记录
    }
    
    if($agpDataMerge == 1 || $agpDataMerge == 2){
      $cusHosts = array_intersect(S(session('regulatorpersonInfo.fcode').'checkCustomer'),$cusHosts);
      foreach ($cusHosts as $key => $value) {
        if($_SERVER['HTTP_HOST'] != $value && $system_num != $value){
          $joinArr[] = $this->returnListSql($value);
        }else{
          $joinArr[] = $this->returnListSql();
        }
      }
      $joinStr = '('.implode(') union all(', $joinArr).')';
    }else{
      $joinStr = $this->returnListSql($system_num);
    }

    $whereStr = $this->returnListWhere();

    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.fsendtype = 0 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->where($whereStr)
      ->count();//查询满足条件的总记录数

    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name,a.fmedia_id, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus,a.fstatus2,a.fstatus3,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fadowner as fadownername,tn.ffullname mediaregion,a.favifilename,a.fjpgfilename,a.fresult_datetime,a.fresult_unit,a.fresult_user,ifnull(a.fexaminetime,a.create_time) fsend_time,a.adowner_regionid,a.fsample_id,ifnull(db.dbcount,0) dbcount,a.fcustomer')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.fsendtype = 0 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->join('(select count(*) dbcount,fillegal_ad_id from tbn_illegal_ad_attach where fstate = 0 and ftype = 30 group by fillegal_ad_id) db on a.fid = db.fillegal_ad_id','left')
      ->where($whereStr)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();

      foreach ($do_tia as $key => $value) {
        if($agpDataMerge == 1 || $agpDataMerge == 2){
          $do_tia[$key]['customerName'] = A('Core')->returnSystemType($value['fcustomer'],$system_num);//所属系统类别
        }

        $do_tia[$key]['adownerregion'] = M('tregion')->cache(true,86400)->where(['fid'=>$value['adowner_regionid']])->getField('ffullname');

        //判断案件未处理是否超过90天
        if(!empty($value['fsend_time']) && !empty($illadTimeoutTips) && empty($outtype) && empty($value['fstatus'])){
          if((time()-strtotime($value['fsend_time']))>90*86400){
            $do_tia[$key]['showTimeout'] = 1;
          }else{
            $do_tia[$key]['showTimeout'] = 0;
          }
        }else{
          $do_tia[$key]['showTimeout'] = 0;
        }

        //判断上个月有播放并处理了的，本月还在播放的违法广告
        if(!empty($againIssueTips) && empty($value['fstatus'])){
          $where_issue['a.fcustomer'] = $system_num;
          $where_issue['a.fsample_id'] = $value['fsample_id'];
          $where_issue['a.fstatus'] = 10;
          $where_issue['b.fissue_date'] = ['between',[date('Y-m-01',strtotime(date('Y-m-01',strtotime($value['fstarttime'])) . ' -1 month')),date('Y-m-d',strtotime(date('Y-m-01',strtotime($value['fstarttime'])) . ' -1 day'))]];
          $againIssue = M('tbn_illegal_ad')
            ->alias('a')
            ->join('tbn_illegal_ad_issue b on a.fid = b.fillegal_ad_id')
            ->where($where_issue)
            ->count();
          if(!empty($againIssue)){
            $do_tia[$key]['againIssue'] = 1;
          }else{
            $do_tia[$key]['againIssue'] = 0;
          }
        }else{
          $do_tia[$key]['againIssue'] = 0;
        }
      }
    
      if(!empty($outtype)){
        if(empty($do_tia)){
          $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
        }

        $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-案件线索列表';//文档内部标题名称
        $outdata['datalie'] = [
          '序号'=>'key',
          '地域'=>'mediaregion',
          '发布媒体'=>'fmedianame',
          '媒体分类'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1','电视'],
              ['{fmedia_class} == 2','广播'],
              ['{fmedia_class} == 3','报纸'],
              ['{fmedia_class} == 13','互联网'],
            ]
          ],
          '广告类别'=>'fadclass',
          '广告名称'=>'fad_name',
          '播出总条次'=>'fcount',
          '播出周数'=>'diffweek',
          '播出时间段'=>[
            'type'=>'zwhebing',
            'data'=>'{fstarttime}~{fendtime}'
          ],
          '违法表现代码'=>'fillegal_code',
          '违法表现'=>'fexpressions',
          '涉嫌违法原因'=>'fillegal',
          '证据下载地址'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1 || {fmedia_class} == 2','{favifilename}'],
              ['','{fjpgfilename}']
            ]
          ],
          '下发时间'=>'fsend_time',
          '查看状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fview_status} == 0','未查看'],
              ['{fview_status} == 10','已查看'],
            ]
          ],
          '处理状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus} == 0','未处理'],
              ['{fstatus} == 10','已上报'],
              ['{fstatus} == 30','数据复核'],
            ]
          ],
          '处理结果'=>'fresult',
          '处理时间'=>'fresult_datetime',
          '处理机构'=>'fresult_unit',
          '处理人'=>'fresult_user',
          '立案状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus2} == 15','待处理'],
              ['{fstatus2} == 16','已处理'],
              ['{fstatus2} == 17','处理中'],
              ['{fstatus2} == 20','撤消'],
            ]
          ],
          '复核状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus3} == 10','复核中'],
              ['{fstatus3} == 20','复核失败'],
              ['{fstatus3} == 30','复核通过待调整'],
              ['{fstatus3} == 40','复核通过已调整'],
              ['{fstatus3} == 50','复核有异议'],
            ]
          ],
        ];
        $outdata['lists'] = $do_tia;
        $ret = A('Api/Function')->outdata_xls($outdata);

        D('Function')->write_log('案件线索列表',1,'导出成功');
        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

      }else{
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
      }
  }

  /**
  * 获取同名案件
  * by zw
  */
  public function otherAnJian(){
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $agpDataMerge = $ALL_CONFIG['agpDataMerge'];
    $cusHosts = json_decode($ALL_CONFIG['agpCustomerMarge'],true);

    $selfid = I('selfid');//违法广告ID组
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }

    $samples = [];
    $selIllData = M('tbn_illegal_ad')->field('fsample_id,fmedia_class')->where(['fid'=>['in',$selfid]])->select();
    foreach ($selIllData as $key => $value) {
      $samples[] = $value['fsample_id'].'-'.$value['fmedia_class'];
    }
    
    if($agpDataMerge == 1 || $agpDataMerge == 2){
      $cusHosts = array_intersect(S(session('regulatorpersonInfo.fcode').'checkCustomer'),$cusHosts);
      foreach ($cusHosts as $key => $value) {
        if($_SERVER['HTTP_HOST'] != $value && $system_num != $value){
          $joinArr[] = $this->returnListSql($value);
        }else{
          $joinArr[] = $this->returnListSql();
        }
      }
      $joinStr = '('.implode(') union all(', $joinArr).')';
    }else{
      $joinStr = $this->returnListSql($system_num);
    }

    $whereStr = '1=1';
    $whereStr .= ' and a.fid not in('.implode(',', $selfid).')';
    $whereStr .= ' and concat(a.fsample_id,"-",a.fmedia_class) in("'.implode('","', $samples).'")';
    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fcustomer ptNum,count(*) ptCount,group_concat(a.fid) fids')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.fsendtype = 0 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->where($whereStr)
      ->group('a.fcustomer')
      ->select();
    foreach ($do_tia as $key => $value) {
      $do_tia[$key]['customerName'] = A('Core')->returnSystemType($value['ptNum'],$system_num);//所属系统类别
      
      $do_tia[$key]['ptIllId'] = explode(',', $value['fids']);  
    }
    $this->ajaxReturn(array('code'=>0,'msg'=>'处理完成','data'=>$do_tia));
  }

  /**
  * 查看状态更改
  * by zw
  */
  public function anjian_look(){
    $fid   = I('fid');//违法广告ID

    $where_tia['fid'] = $fid;
    $where_tia['fview_status'] = 0;

    $data_tia['fview_status'] = 10;
    $do_tia = M('tbn_illegal_ad')->where($where_tia)->save($data_tia);
    $this->ajaxReturn(array('code'=>0,'msg'=>'处理完成'));
  }

  /**
  * 查看案件详情
  * by zw
  */
  public function anjian_view(){
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $isrelease = $ALL_CONFIG['isrelease'];
    $ischeck = $ALL_CONFIG['ischeck'];
    $scsavetime = $ALL_CONFIG['scsavetime'];
    $illNotarization = $ALL_CONFIG['illNotarization'];//违法线索是否允许公证，0不允许，1允许

    $fid = I('fid');//违法广告ID
    $mclass = I('mclass');//媒介类别
    $sendstatus = I('sendstatus',0);//发布状态
    $fissuedatest   = I('fissuedatest');//发布时间起
    $fissuedateed   = I('fissuedateed');//发布时间止
    $dit_id = I('dit_id');//检查计划ID

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $times_table = I('times_table');
    $table_condition = I('table_condition');//选择时间
    if(!empty($times_table) && !empty($table_condition)){
      if($times_table == 'tbn_ad_summary_day'){
          $timetypes = 0;
          $timeval = $table_condition;
      }elseif($times_table == 'tbn_ad_summary_week'){
          $timetypes = 10;
          $timeval = $table_condition;
      }elseif($times_table == 'tbn_ad_summary_half_month'){
          $timetypes = 20;
          $table_condition2 = explode('-', $table_condition);
          $table_condition3 = (int)$table_condition2[0]*2;
          $table_condition4 = ((int)$table_condition2[1])==1?0:1;
          $timeval = $table_condition3+$table_condition4;
      }elseif($times_table == 'tbn_ad_summary_month'){
          $timetypes = 30;
          $table_condition2 = explode('-', $table_condition);
          $timeval = (int)$table_condition2[0];
      }elseif($times_table == 'tbn_ad_summary_quarter'){
          $timetypes = 40;
          if($table_condition == '01-01'){
              $timeval = 1;
          }elseif($table_condition == '04-01'){
              $timeval = 2;
          }elseif($table_condition == '07-01'){
              $timeval = 3;
          }else{
              $timeval = 4;
          }
      }elseif($times_table == 'tbn_ad_summary_half_year'){
          $timetypes = 50;
          if($table_condition == '01-01'){
              $timeval = 1;
          }else{
              $timeval = 2;
          }
      }elseif($times_table == 'tbn_ad_summary_year'){
          $timetypes = 60;
          $timeval = 1;
      }
    }

    if(!empty($dit_id)){
      if(!empty($sendstatus)){
        $sendstatus = -1;
      }else{
        if(session('regulatorpersonInfo.fregulatorlevel')==30){
          $sendstatus = -2;
        }else{
          $sendstatus = 1;
        }
      }
    }
    $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date',$sendstatus,$isrelease);
    
    $where_time .= ' and a.fid = '.$fid;
    $where_time .= ' and a.fmedia_class = '.$mclass;

    if(!empty($dit_id)){
      $where_dit['dit_id'] = $dit_id;
      if(session('regulatorpersonInfo.fregulatorlevel')!=30){
        $where_dit['dit_tnid'] = session('regulatorpersonInfo.regionid');
      }
      $do_dit = M('tbn_data_inspect')->field('dit_tnid')->where($where_dit)->find();
      if(empty($do_dit)){
        $this->ajaxReturn(array('code'=>1,'msg'=>'获取失败'));
      }
    }

    if(!empty($fissuedatest) && !empty($fissuedateed)){
      $where_time .= ' and fissue_date between "'.$fissuedatest.'" and "'.$fissuedateed.'"';
    }

    //是否抽查模式
    if(!empty($ischeck)){
        $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
        //如果抽查表有数据
        if(!empty($spot_check_data)){
            $dates = [];//定义日期数组
            foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                $year_month = '';
                $date_str = [];
                $year_month = substr($spot_check_data_val['fmonth'],0,7);
                if(!empty($spot_check_data_val['condition'])){
                  $date_str = explode(',',$spot_check_data_val['condition']);
                }
                foreach ($date_str as $date_str_val){
                    $dates[] = $year_month.'-'.$date_str_val;
                }
            }
        $where_time   .= ' and b.fissue_date in ("'.implode('","', $dates).'")';
        }else{
            $where_time   .= ' and 1=0';
        }
    }

    if($mclass==3){//报纸详情读取
      $do_tia = M('tbn_illegal_ad')
        ->alias('a')
        ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fjpgfilename,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fsample_id,n.fpage,a.fadowner as fadownername,a.fad_class_code')
        ->join('tpaperissue n on n.fpapersampleid=a.fsample_id')
        ->join('tadclass b on a.fad_class_code=b.fcode')
        ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
        ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue b,tbn_illegal_ad a where '.$where_time.' and a.fid = b.fillegal_ad_id group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
        ->order('x.fstarttime desc')
        ->find();
      if(!empty($do_tia)){
        $where_te['fpapersampleid'] = $do_tia['fsample_id'];
        $do_te = M('tpapersample')
          ->alias('a')
          ->field('b.furl')
          ->join('tpapersource b on a.sourceid=b.fid')
          ->where($where_te)
          ->find();
        $do_tia['fjpgfilename2'] = $do_te['furl'];
      }
    }elseif($mclass==1 || $mclass == 2){//电视或广播
      $do_tia = M('tbn_illegal_ad')
        ->alias('a')
        ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fsample_id,a.favifilename,a.fadowner as fadownername,a.fad_class_code')
        ->join('tadclass b on a.fad_class_code=b.fcode')
        ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
        ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue b,tbn_illegal_ad a where '.$where_time.' and a.fid = b.fillegal_ad_id group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
        ->find();

      $where_te['_string'] = $where_time;
      $do_te = M('tbn_illegal_ad_issue')
        ->alias('b')
        ->field('b.fid as tid,b.fmedia_id,fstarttime,fendtime,(case when fissue_date>DATE_SUB(CURDATE(), INTERVAL '.$scsavetime.' MONTH) then 1 else 0 end) isplay')
        ->join('tbn_illegal_ad a on a.fid = b.fillegal_ad_id')
        ->where($where_te)
        ->order('fstarttime desc')
        ->select();
      $do_tia['issue_list'] = $do_te;
      
      $where2['source_type'] = 20;
      $where2['source_tid'] = $fid;
      $where2['source_mediaclass'] = sprintf("%02d", $mclass);
      $where2['validity_time'] = ['gt',time()];
      $data2 = M('source_make')->field('source_url,source_id,source_state')->where($where2)->order('source_id desc')->find();
      if(!empty($data2)){
        $isurl = getHttpStatus($data2['source_url']);
        if($isurl == 200){
          $do_tia['source_url'] = $data2['source_url'];
          $do_tia['source_id']  = $data2['source_id'];
          $do_tia['source_state'] = $data2['source_state'];
        }else{
          $do_tia['source_state'] = $data2['source_state'];
        }
      }
    }elseif($mclass == 13){
      $do_tia = M('tbn_illegal_ad')
        ->alias('a')
        ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.favifilename,a.fjpgfilename,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fsample_id,a.fadowner as fadownername,c.fmediacode,c.fmediaclassid,a.fad_class_code')
        ->join('tadclass b on a.fad_class_code=b.fcode')
        ->join('tmedia c on a.fmedia_id=c.fid')
        ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue b,tbn_illegal_ad a where '.$where_time.' and a.fid = b.fillegal_ad_id group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
        ->order('x.fstarttime desc')
        ->find();

        $do_tia['favifilename'] = $do_tia['favifilename'] == 'empty'?'':$do_tia['favifilename'];
        $do_tia['fexpressions'] = htmlspecialchars_decode($do_tia['fexpressions']);
        $do_tia['fillegal'] = $do_tia['fillegal']?htmlspecialchars_decode($do_tia['fillegal']):$do_tia['fexpressions'];

        //样本数据
        $sampleView = M('tnetissue')->alias('n')->field('n.article_content,n.net_platform,n.net_original_snapshot,n.thumb_url_true,n.ftype,n.net_target_url,n.net_original_url')->where(['major_key'=>$do_tia['fsample_id']])->find();
        if(!empty($sampleView)){
          $do_tia = array_merge($do_tia,$sampleView);
        }else{
          $do_tia['net_original_snapshot'] = $do_tia['fjpgfilename'];
          $do_tia['thumb_url_true'] = $do_tia['fjpgfilename'];
          $do_tia['net_target_url'] = $do_tia['favifilename'];
          $do_tia['net_original_url'] = $do_tia['favifilename'];
          if($do_tia['fmediaclassid'] == '1302'){
            $do_tia['net_platform'] = 2;
          }elseif($do_tia['fmediaclassid'] == '1303'){
            $do_tia['net_platform'] = 9;
          }else{
            $do_tia['net_platform'] = 1;
          }
          $do_tia['article_content'] = '';
          $do_tia['ftype'] = 'image';
        }

        //是否可转入存证
        $do_tia['notarizationdata'] = '';//存证信息
        $do_tia['notarization'] = 0;//存证状态
        if(!empty($illNotarization)){
          $do_esm = M('tb_evidence_sam')
            ->alias('b')
            ->field('a.id,a.title,a.url,a.rid,a.md5,a.screenshot,b.fcreatetime cdate,a.status')
            ->join('tb_evidence a on b.frid = a.rid')
            ->where(['b.fmediaclass'=>$mclass,'b.fsample_id'=>$do_tia['fsample_id'],'a.status'=>1])
            ->find();
          if(!empty($do_esm)){
            $isadd = M('tb_applicant_evis')
              ->where(['fuserid'=>session('regulatorpersonInfo.fid'),'fapplicantid'=>0,'fevidenceid'=>$do_esm['id']])
              ->count();
            $do_tia['notarization'] = $isadd?2:1;
            $do_tia['notarizationdata'] = $do_esm;
          }
        }
    }

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_tia));
  }

  /**
  * 获取视频地址
  * by zw
  */
  public function get_favifilename(){
    $fmedia_id  = I('fmedia_id');//主媒体ID
    $fstarttime = I('fstarttime');//播放开始时间
    $fendtime   = I('fendtime');//播放结束时间
    $favifilelist = [];
    if(empty($fstarttime) || empty($fendtime)){
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>0,'data'=>[]));
    }
    $favifiledata['favifilename'] = A('Common/Media','Model')->get_m3u8($fmedia_id,strtotime($fstarttime),strtotime($fendtime));
    if(!empty($favifiledata['favifilename'])){
      $favifiledata['fmedia_id'] = $fmedia_id;
      $favifilelist[] = $favifiledata;
    }

    $where_ta['main_media_id'] = $fmedia_id;
    $where_ta['fid'] = array('neq',$fmedia_id);
    $where_ta['fstate'] = 1;
    $do_ta = M('tmedia')->field('fid')->where($where_ta)->select();
    foreach ($do_ta as $key => $value) {
      $favifiledata['favifilename'] = A('Common/Media','Model')->get_m3u8($value['fid'],strtotime($fstarttime),strtotime($fendtime));

      if(!empty($favifiledata['favifilename'])){
        $favifiledata['fmedia_id'] = $value['fid'];
        $favifilelist[] = $favifiledata;
      }
    }

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>count($favifilelist),'data'=>$favifilelist));

  }

  /**
  * 案件上报
  * by zw
  */
  public function anjian_sb(){
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $isTipSpanRegionIll = $ALL_CONFIG['isTipSpanRegionIll'];//跨地域案件立案后是否提醒，0否，1是
    $issbfile = $ALL_CONFIG['issbfile'];//违法广告处理时，上报文件是否必传，0非必须，10必须
    $system_num = $ALL_CONFIG['system_num'];

    $selfid     = I('selfid');//违法广告ID组
    $attachinfo = I('attachinfo');//上传的文件信息
    $selfresult = I('selfresult');//处理结果
    $ptIllId = I('ptIllId');//合并处理的违法广告
    $attach     = [];//预添加的附件信息
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    //除了配置必传之外，有合并数据一起上报时也必传文件材料
    if(empty($attachinfo) && (!empty($issbfile) || !empty($ptIllId))){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请上传文件材料'));
    }
    if(empty($selfresult)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择处理结果'));
    }

    $prvupids = [];//用于更新上一派发记录的处理状态更新
    $smsRegulator = [];//需要通知的机构
    $smsData = [];//通知内容
    if(!empty($ptIllId)){
      $selfid = array_merge($selfid,$ptIllId);
    }
    $selTia = M('tbn_illegal_ad')->field('fid,fcustomer,fmedia_class,fsample_id,fregion_id')->where(['fid'=>['in',$selfid]])->select();
    foreach ($selTia as $key => $value) {
      $where_tia['fid']     = $value['fid'];
      $where_tia['fstatus'] = 0;
      $where_tia['_string'] = 'fid in (select fillegal_ad_id from tbn_case_send where fstatus = 0 and frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').')';

      $data_tia['fstatus']          = 10;//已上报
      $data_tia['fview_status']     = 10;//已查看
      $data_tia['fresult']          = $selfresult;//处理结果
      $data_tia['fresult_datetime'] = date('Y-m-d H:i:s');//上报时间
      $data_tia['fresult_unit']     = session('regulatorpersonInfo.regulatorpname');//上报机构
      $data_tia['fresult_unitid']     = session('regulatorpersonInfo.fregulatorpid');//上报机构
      $data_tia['fresult_user']     = session('regulatorpersonInfo.fname');//上报人
      $data_tia['fresult_userid']     = session('regulatorpersonInfo.fid');//上报人
      //判断是否有立案调查，如有：添加立案调查记录
      if(strpos($selfresult,'立案')!==false){
        $data_tia['fstatus2']   = 17;//立案待处理

        if(!empty($isTipSpanRegionIll)){//跨地域案件立案后是否提醒，0否，1是
          if($value['fmedia_class'] == 1 || $value['fmedia_class'] == 2){
            if($value['fmedia_class'] == 1){
              $sampletb = 'ttvsample';
            }elseif($value['fmedia_class'] == 2){
              $sampletb = 'tbcsample';
            }
            if(!empty($sampletb)){
              $uuid = M($sampletb)->where(['fid'=>$value['fsample_id']])->getField('uuid');
              $do_illad = M('tbn_illegal_ad')
                ->alias('a')
                ->field('a.fad_name,a.fregion_id')
                ->join($sampletb.' b on a.fsample_id = b.fid')
                ->where(['fmedia_class'=>$value['fmedia_class'],'b.uuid'=>$uuid,'a.fstatus'=>0,'a.fcustomer'=>$value['fcustomer'],'fregion_id'=>['neq',substr($value['fregion_id'],0,4)]])
                ->select();
              
              foreach ($do_illad as $illkey => $illvalue) {
                if(!in_array('20'.$illvalue['fregion_id'], $smsRegulator)){
                  $smsRegulator[] = '20'.$illvalue['fregion_id'];
                  $smsData['20'.$illvalue['fregion_id']][] = ['ad'=>$illvalue['fad_name']];
                }else{
                  $smsData['20'.$illvalue['fregion_id']][] = ['ad'=>$illvalue['fad_name']];
                }
              }
            }
          }
        }
      }
      $save_tia = M('tbn_illegal_ad')->where($where_tia)->save($data_tia);

      if(!empty($save_tia)){
        $prvupids[] = $value['fid'];

        //上传附件
        $attach_data['ftype']    = 0;//类型
        $attach_data['fillegal_ad_id']    = $value['fid'];//违法广告
        $attach_data['fuploader']         = session('regulatorpersonInfo.fname');//上传人
        $attach_data['fupload_time']      = date('Y-m-d H:i:s');//上传时间
        foreach ($attachinfo as $key2 => $value2){
          $attach_data['fattach']     = $value2['fattachname'];
          $attach_data['fattach_url'] = $value2['fattachurl'];
          array_push($attach,$attach_data);
        }
      }
    }

    if(!empty($smsRegulator)){
      $where_tregulator['_string'] = '(plbcount = 0 or plbcount is null or plbcount2 = 1)';
      $where_tregulator['a.fid'] = ['in',$smsRegulator];
      $do_tregulator = M('tregulator')
        ->alias('a')
        ->field('a.fid,b.fmobile')
        ->join('tregulatorperson b on a.fid = b.fregulatorid and length(b.fmobile) = 11')
        ->join('(select count(*) as plbcount,sum(case when fcustomer = "'.$system_num.'" then 1 else 0 end) as plbcount2,fpersonid from tpersonlabel  where fstate = 1 group by fpersonid) lb on b.fid = lb.fpersonid','left')
        ->where($where_tregulator)
        ->select();

      foreach ($do_tregulator as $key => $value) {
        $smsstr = '';
        if(!empty($smsData[$value['fid']])){
          foreach ($smsData[$value['fid']] as $key2 => $value2) {
            if(!empty($smsstr)){
              $smsstr .= '、'.$value2['ad'];
            }else{
              $smsstr = $value2['ad'];
            }
          }
          $smsstr = '发现广告（'.$smsstr.'）已在其他地区立案';
            $ret = A('Common/Alitongxin','Model')->usertask_sms($value['fmobile'],'省广告监测平台监测提醒。'.$smsstr);
        }
      }
    }

    //更新上一派发记录的处理状态
    if(!empty($prvupids)){
      $where_prv['fstatus'] = 0;
      $where_prv['frece_reg_id'] = session('regulatorpersonInfo.fregulatorpid');
      $where_prv['fillegal_ad_id'] = ['in',$prvupids];
      $data_prv['fstatus'] = 10;
      $data_prv['freceiver'] = session('regulatorpersonInfo.fname');
      $data_prv['frece_time'] = date('Y-m-d H:i:s');
      M('tbn_case_send')->where($where_prv)->save($data_prv);
    }

    //有上传附件时需要添加附件记录
    if(!empty($attach)){
      M('tbn_illegal_ad_attach')->addAll($attach);
    }
    D('Function')->write_log('处理结果上报',1,'上报成功','tbn_illegal_ad',$save_tia,M('tbn_illegal_ad')->getlastsql());
    $this->ajaxReturn(array('code'=>0,'msg'=>'提交成功'));
  }

  /**
  * 案件派发
  * by zw
  */
  public function anjian_pf(){
    $selfid     = I('selfid');//违法广告ID组
    $tregionid  = I('tregionid');//行政区划ID

    $adddata    = [];
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    if(empty($tregionid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择需要派发的机构'));
    }
    if($tregionid == session('regulatorpersonInfo.regionid')){
      $this->ajaxReturn(array('code'=>1,'msg'=>'无法向本机构派发'));
    }
    $where_tr['fregionid'] = $tregionid;
    $where_tr['_string'] = 'fstate=1 and ftype=20 and fkind=1';
    $do_tr = M('tregulator')->field('fid,fname')->where($where_tr)->find();
    if(empty($do_tr)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'该区域还未建立相应机构'));
    }

    $prvupids = [];//用于更新上一派发记录的处理状态更新
    foreach ($selfid as $key => $value) {
      $prvupids[] = $value;

      //将需要添加的派发记录放到数组中
      $data_tcs['fillegal_ad_id'] = $value;
      $data_tcs['frece_reg_id']   = $do_tr['fid'];
      $data_tcs['frece_reg']      = $do_tr['fname'];
      $data_tcs['fsendtype']      = 1;
      $data_tcs['fsend_reg_id']   = session('regulatorpersonInfo.fregulatorpid');
      $data_tcs['fsend_reg']      = session('regulatorpersonInfo.regulatorpname');
      $data_tcs['fsender']        = session('regulatorpersonInfo.fname');
      $data_tcs['fsend_time']     = date('Y-m-d H:i:s');
      array_push($adddata, $data_tcs);
    }
    if(!empty($adddata)){
      //更新上一派发记录的处理状态
      if(!empty($prvupids)){
        $where_prv['fstatus'] = 0;
        $where_prv['frece_reg_id'] = session('regulatorpersonInfo.fregulatorpid');
        $where_prv['fillegal_ad_id'] = ['in',$prvupids];
        $data_prv['fstatus'] = 20;
        $data_prv['freceiver'] = session('regulatorpersonInfo.fname');
        $data_prv['frece_time'] = date('Y-m-d H:i:s');
        M('tbn_case_send')->where($where_prv)->save($data_prv);
      }

      //添加新派发记录
      M('tbn_case_send')->addAll($adddata);

      D('Function')->write_log('处理结果上报',1,'派发成功','tbn_illegal_ad',0,implode(",",$selfid));
      $this->ajaxReturn(array('code'=>0,'msg'=>'派发成功'));
    }else{
      D('Function')->write_log('处理结果上报',0,'派发失败','tbn_illegal_ad',0,M('tbn_illegal_ad')->getlastsql());
      $this->ajaxReturn(array('code'=>1,'msg'=>'派发失败'));
    }
  }

  /**
  * 取消案件派发
  * by zw
  */
  public function anjian_unpf(){
    $selfid     = I('selfid');//违法广告ID组

    $adddata    = [];
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    foreach ($selfid as $key => $value) {
      $where_csd['fillegal_ad_id'] = $value;
      $where_csd['fstatus'] = 0;
      $where_csd['fsendtype'] = 1;
      $where_csd['fsend_reg_id'] = session('regulatorpersonInfo.fregulatorpid');
      $do_csd = M('tbn_case_send')->where($where_csd)->delete();//删除当前机构的派发记录
      if(!empty($do_csd)){
        $where_csd2['fillegal_ad_id'] = $value;
        $where_csd2['fstatus'] = 20;
        $where_csd2['frece_reg_id'] = session('regulatorpersonInfo.fregulatorpid');
        $do_csd2 = M('tbn_case_send')->where($where_csd2)->save(['fstatus'=>0,'frece_time'=>date('Y-m-d H:i:s')]);//更新上一派发记录
      }
      D('Function')->write_log('取消案件派发',1,'取消成功','tbn_illegal_ad',$value,M('tbn_illegal_ad')->getlastsql());
    }
    
    $this->ajaxReturn(array('code'=>0,'msg'=>'取消成功'));
  }

  /**
  * 接收案件列表
  * by zw
  */
  public function jsanjian_list(){
    Check_QuanXian(['sbchuli']);
    session_write_close();
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $agpDataMerge = $ALL_CONFIG['agpDataMerge'];
    $cusHosts = json_decode($ALL_CONFIG['agpCustomerMarge'],true);

    $outtype = I('outtype');//导出类型
    if(!empty($outtype)){
      $p  = I('page', 1);//当前第几页ks
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页ks
      $pp = 20;//每页显示多少记录
    }
    
    if($agpDataMerge == 1 || $agpDataMerge == 2){
      $cusHosts = array_intersect(S(session('regulatorpersonInfo.fcode').'checkCustomer'),$cusHosts);
      foreach ($cusHosts as $key => $value) {
        if($_SERVER['HTTP_HOST'] != $value && $system_num != $value){
          $joinArr[] = $this->returnListSql($value);
        }else{
          $joinArr[] = $this->returnListSql();
        }
      }
      $joinStr = '('.implode(') union all(', $joinArr).')';
    }else{
      $joinStr = $this->returnListSql($system_num);
    }

    $whereStr = $this->returnListWhere();

    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.fsendtype = 1 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->where($whereStr)
      ->count();//查询满足条件的总记录数

    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name,a.fmedia_id, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus,a.fstatus2,a.fstatus3,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fadowner as fadownername,tn.ffullname mediaregion,a.favifilename,a.fjpgfilename,a.fresult_datetime,a.fresult_unit,a.fresult_user,a.create_time,d.fsend_time,a.adowner_regionid,a.fsample_id,ifnull(db.dbcount,0) dbcount')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.fsendtype = 1 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->join('(select count(*) dbcount,fillegal_ad_id from tbn_illegal_ad_attach where fstate = 0 and ftype = 30 group by fillegal_ad_id) db on a.fid = db.fillegal_ad_id','left')
      ->where($whereStr)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();

    foreach ($do_tia as $key => $value) {
      if($agpDataMerge == 1 || $agpDataMerge == 2){
        $do_tia[$key]['customerName'] = A('Core')->returnSystemType($value['fcustomer'],$system_num);//所属系统类别
      }

      $do_tia[$key]['adownerregion'] = M('tregion')->cache(true,86400)->where(['fid'=>$value['adowner_regionid']])->getField('ffullname');

      //判断案件未处理是否超过90天
      if(!empty($value['fsend_time']) && !empty($illadTimeoutTips) && !empty($outtype) && empty($value['fstatus'])){
        if((time()-strtotime($value['fsend_time']))>90*86400){
          $do_tia[$key]['showTimeout'] = 1;
        }else{
          $do_tia[$key]['showTimeout'] = 0;
        }
      }else{
        $do_tia[$key]['showTimeout'] = 0;
      }

      //判断上个月有播放并处理了的，本月还在播放的违法广告
      if(!empty($againIssueTips) && empty($value['fstatus'])){
        $where_issue['a.fcustomer'] = $system_num;
        $where_issue['a.fsample_id'] = $value['fsample_id'];
        $where_issue['a.fstatus'] = 10;
        $where_issue['b.fissue_date'] = ['between',[date('Y-m-01',strtotime(date('Y-m-01',strtotime($value['fstarttime'])) . ' -1 month')),date('Y-m-d',strtotime(date('Y-m-01',strtotime($value['fstarttime'])) . ' -1 day'))]];
        $againIssue = M('tbn_illegal_ad')
          ->alias('a')
          ->join('tbn_illegal_ad_issue b on a.fid = b.fillegal_ad_id')
          ->where($where_issue)
          ->count();
        if(!empty($againIssue)){
          $do_tia[$key]['againIssue'] = 1;
        }else{
          $do_tia[$key]['againIssue'] = 0;
        }
      }else{
        $do_tia[$key]['againIssue'] = 0;
      }
    }
    
    if(!empty($outtype)){
      if(empty($do_tia)){
        $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
      }
      $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-案件线索接收列表';//文档内部标题名称
      $outdata['datalie'] = [
        '序号'=>'key',
        '地域'=>'mediaregion',
        '发布媒体'=>'fmedianame',
        '媒体分类'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fmedia_class} == 1','电视'],
            ['{fmedia_class} == 2','广播'],
            ['{fmedia_class} == 3','报纸'],
            ['{fmedia_class} == 13','互联网'],
          ]
        ],
        '广告类别'=>'fadclass',
        '广告名称'=>'fad_name',
        '播出总条次'=>'fcount',
        '播出周数'=>'diffweek',
        '播出时间段'=>[
          'type'=>'zwhebing',
          'data'=>'{fstarttime}~{fendtime}'
        ],
        '违法表现代码'=>'fillegal_code',
        '违法表现'=>'fexpressions',
        '涉嫌违法原因'=>'fillegal',
        '证据下载地址'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fmedia_class} == 1 || {fmedia_class} == 2','{favifilename}'],
            ['','{fjpgfilename}']
          ]
        ],
        '派发时间'=>'fsend_time',
        '查看状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fview_status} == 0','未查看'],
            ['{fview_status} == 10','已查看'],
          ]
        ],
        '处理状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fstatus} == 0','未处理'],
            ['{fstatus} == 10','已上报'],
            ['{fstatus} == 30','数据复核'],
          ]
        ],
        '处理结果'=>'fresult',
        '处理时间'=>'fresult_datetime',
        '处理机构'=>'fresult_unit',
        '处理人'=>'fresult_user',
        '立案状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fstatus2} == 15','待处理'],
            ['{fstatus2} == 16','已处理'],
            ['{fstatus2} == 17','处理中'],
            ['{fstatus2} == 20','撤消'],
          ]
        ],
        '复核状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fstatus3} == 10','复核中'],
            ['{fstatus3} == 20','复核失败'],
            ['{fstatus3} == 30','复核通过待调整'],
            ['{fstatus3} == 40','复核通过已调整'],
            ['{fstatus3} == 50','复核有异议'],
          ]
        ],
      ];
      $outdata['lists'] = $do_tia;
      $ret = A('Api/Function')->outdata_xls($outdata);

      D('Function')->write_log('案件线索接收列表',1,'生成成功');
      $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

    }else{
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
    }

  }

  /**
  * 派发案件列表
  * by zw
  */
  public function pfanjian_list(){
    Check_QuanXian(['sbchuli']);
    session_write_close();
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $agpDataMerge = $ALL_CONFIG['agpDataMerge'];
    $cusHosts = json_decode($ALL_CONFIG['agpCustomerMarge'],true);

    $outtype = I('outtype');//导出类型
    if(!empty($outtype)){
      $p  = I('page', 1);//当前第几页ks
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页ks
      $pp = 20;//每页显示多少记录
    }

    if($agpDataMerge == 1 || $agpDataMerge == 2){
      $cusHosts = array_intersect(S(session('regulatorpersonInfo.fcode').'checkCustomer'),$cusHosts);
      foreach ($cusHosts as $key => $value) {
        if($_SERVER['HTTP_HOST'] != $value && $system_num != $value){
          $joinArr[] = $this->returnListSql($value);
        }else{
          $joinArr[] = $this->returnListSql();
        }
      }
      $joinStr = '('.implode(') union all(', $joinArr).')';
    }else{
      $joinStr = $this->returnListSql($system_num);
    }

    $whereStr = $this->returnListWhere();

    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fsendtype = 1 and d.fstatus=0 and d.fsend_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->where($whereStr)
      ->count();//查询满足条件的总记录数

    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus,a.fstatus2,a.fstatus3,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,d.frece_reg,a.fadowner as fadownername,tn.ffullname,a.favifilename,a.fjpgfilename,a.fresult_datetime,a.fresult_unit,a.fresult_user,d.fsend_time,a.fcustomer')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fsendtype = 1 and d.fstatus=0 and d.fsend_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->where($whereStr)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();

    if($agpDataMerge == 1 || $agpDataMerge == 2){
      foreach ($do_tia as $key => $value) {
        $do_tia[$key]['customerName'] = A('Core')->returnSystemType($value['fcustomer'],$system_num);//所属系统类别
      }
    }

    if(!empty($outtype)){
      if(empty($do_tia)){
        $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
      }

      $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-案件线索派发列表';//文档内部标题名称
      $outdata['datalie'] = [
        '序号'=>'key',
        '地域'=>'ffullname',
        '发布媒体'=>'fmedianame',
        '媒体分类'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fmedia_class} == 1','电视'],
            ['{fmedia_class} == 2','广播'],
            ['{fmedia_class} == 3','报纸'],
            ['{fmedia_class} == 13','互联网'],
          ]
        ],
        '广告类别'=>'fadclass',
        '广告名称'=>'fad_name',
        '播出总条次'=>'fcount',
        '播出周数'=>'diffweek',
        '播出时间段'=>[
          'type'=>'zwhebing',
          'data'=>'{fstarttime}~{fendtime}'
        ],
        '违法表现代码'=>'fillegal_code',
        '违法表现'=>'fexpressions',
        '涉嫌违法原因'=>'fillegal',
        '证据下载地址'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fmedia_class} == 1 || {fmedia_class} == 2','{favifilename}'],
            ['','{fjpgfilename}']
          ]
        ],
        '派发时间'=>'fsend_time',
        '查看状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fview_status} == 0','未查看'],
            ['{fview_status} == 10','已查看'],
          ]
        ],
        '处理状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fstatus} == 0','未处理'],
            ['{fstatus} == 10','已上报'],
            ['{fstatus} == 30','数据复核'],
          ]
        ],
        '处理结果'=>'fresult',
        '处理时间'=>'fresult_datetime',
        '处理机构'=>'fresult_unit',
        '处理人'=>'fresult_user',
        '立案状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fstatus2} == 15','待处理'],
            ['{fstatus2} == 16','已处理'],
            ['{fstatus2} == 17','处理中'],
            ['{fstatus2} == 20','撤消'],
          ]
        ],
        '复核状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fstatus3} == 10','复核中'],
            ['{fstatus3} == 20','复核失败'],
            ['{fstatus3} == 30','复核通过待调整'],
            ['{fstatus3} == 40','复核通过已调整'],
            ['{fstatus3} == 50','复核有异议'],
          ]
        ],
      ];
      $outdata['lists'] = $do_tia;
      $ret = A('Api/Function')->outdata_xls($outdata);

      D('Function')->write_log('案件线索派发列表',1,'导出成功');
      $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

    }else{
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
    }
  }

  /**
  * 本省数据复核列表
  * by zw
  */
  public function bsfuhe_list(){
    Check_QuanXian(['sbchuli']);
    session_write_close();
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $agpDataMerge = $ALL_CONFIG['agpDataMerge'];
    $isrelease = $ALL_CONFIG['isrelease'];
    $cusHosts = json_decode($ALL_CONFIG['agpCustomerMarge'],true);

    $outtype = I('outtype');//导出类型
    if(!empty($outtype)){
      $p  = I('page', 1);//当前第几页ks
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页ks
      $pp = 20;//每页显示多少记录
    }
    
    if($agpDataMerge == 1 || $agpDataMerge == 2){
      $cusHosts = array_intersect(S(session('regulatorpersonInfo.fcode').'checkCustomer'),$cusHosts);
      foreach ($cusHosts as $key => $value) {
        if($_SERVER['HTTP_HOST'] != $value && $system_num != $value){
          $joinArr[] = $this->returnListSql($value);
        }else{
          $joinArr[] = $this->returnListSql();
        }
      }
      $joinStr = '('.implode(') union all(', $joinArr).')';
    }else{
      $joinStr = $this->returnListSql($system_num);
    }

    $whereStr = $this->returnListWhere();

    if($system_num == '100000'){
      //时间条件筛选
      $years      = I('years');//选择年份
      $timetypes  = I('timetypes');//选择时间段
      $timeval    = I('timeval');//选择时间
      $where_time2 = gettimecondition($years,$timetypes,$timeval,'fissue_date',0,$isrelease);
      if(session('regulatorpersonInfo.fregulatorlevel') == 10 || session('regulatorpersonInfo.fregulatorlevel') == 0){
        $whereStr .= ' and a.fad_name not in (SELECT fad_name FROM tbn_illegal_ad a inner join tregion c on a.fregion_id=c.fid and c.flevel in (1,2,3) inner join tbn_illegal_ad_issue b on a.fid = b.fillegal_ad_id where '.$where_time2.' and a.fcustomer='.$system_num.' and left(a.fregion_id,4)<>"'.substr(session('regulatorpersonInfo.regionid'),0,2) .'00" and left(a.fregion_id,4)<>"'.substr(session('regulatorpersonInfo.regionid'),0,4).'" GROUP BY a.fad_name) ';
      }elseif(session('regulatorpersonInfo.fregulatorlevel') == 20){
        $whereStr .= ' and a.fad_name not in (SELECT fad_name FROM tbn_illegal_ad a inner join tregion c on a.fregion_id=c.fid and c.flevel in (1,2,3) inner join tbn_illegal_ad_issue b on a.fid = b.fillegal_ad_id where '.$where_time2.' and a.fcustomer = '.$system_num.' and left(fregion_id,2)<>"'.substr(session('regulatorpersonInfo.regionid'),0,2).'" GROUP BY fad_name)';
      }elseif(session('regulatorpersonInfo.fregulatorlevel') == 30){
        $whereStr .= ' and a.fad_name not in (select fad_name from (SELECT fad_name FROM tbn_illegal_ad a inner join tregion c on a.fregion_id=c.fid and c.flevel in (1,2,3) inner join tbn_illegal_ad_issue b on a.fid = b.fillegal_ad_id where '.$where_time2.' and a.fcustomer = '.$system_num.' group by left(a.fregion_id,2),a.fad_name) as a group by a.fad_name having count(1)>1) ';
      }
    }

    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->where($whereStr)
      ->count();//查询满足条件的总记录数

    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus2,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fadowner as fadownername,a.fcustomer')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->where($whereStr)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();

    if($agpDataMerge == 1 || $agpDataMerge == 2){
      foreach ($do_tia as $key => $value) {
        $do_tia[$key]['customerName'] = A('Core')->returnSystemType($value['fcustomer'],$system_num);//所属系统类别
      }
    }
    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
  }

  /**
  * 跨地域数据复核列表
  * by zw
  */
  public function ksfuhe_list(){
    Check_QuanXian(['sbchuli']);
    session_write_close();
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $agpDataMerge = $ALL_CONFIG['agpDataMerge'];
    $isrelease = $ALL_CONFIG['isrelease'];
    $cusHosts = json_decode($ALL_CONFIG['agpCustomerMarge'],true);

    $outtype = I('outtype');//导出类型
    if(!empty($outtype)){
      $p  = I('page', 1);//当前第几页ks
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页ks
      $pp = 20;//每页显示多少记录
    }
    
    if($agpDataMerge == 1 || $agpDataMerge == 2){
      $cusHosts = array_intersect(S(session('regulatorpersonInfo.fcode').'checkCustomer'),$cusHosts);
      foreach ($cusHosts as $key => $value) {
        if($_SERVER['HTTP_HOST'] != $value && $system_num != $value){
          $joinArr[] = $this->returnListSql($value);
        }else{
          $joinArr[] = $this->returnListSql();
        }
      }
      $joinStr = '('.implode(') union all(', $joinArr).')';
    }else{
      $joinStr = $this->returnListSql($system_num);
    }

    $whereStr = $this->returnListWhere();

    if($system_num == '100000'){
      //时间条件筛选
      $years      = I('years');//选择年份
      $timetypes  = I('timetypes');//选择时间段
      $timeval    = I('timeval');//选择时间
      $where_time2 = gettimecondition($years,$timetypes,$timeval,'fissue_date',0,$isrelease);
      if(session('regulatorpersonInfo.fregulatorlevel') == 10 || session('regulatorpersonInfo.fregulatorlevel') == 0){
        $joins = 'inner join (select fad_name,(count(1)+1) as fregion_count,concat("'.session('regulatorpersonInfo.regionname1').',",group_concat(fname1)) as fregionname,sum(yds) as ydscount from (select a.fad_name,b.fname1,(case when left(a.fregion_id,2) <> "'.substr(session('regulatorpersonInfo.regionid'),0,2).'" then 1 else 0 end) as yds from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid and b.flevel in (1,2,3) inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where '.$where_time2.' and fcustomer = '.$system_num.' and left(fregion_id,4)<>"'.substr(session('regulatorpersonInfo.regionid'),0,2) .'00" and left(fregion_id,4)<>"'.substr(session('regulatorpersonInfo.regionid'),0,4).'" group by a.fregion_id,a.fad_name ) as a group by a.fad_name) k on a.fad_name = k.fad_name';
      }elseif(session('regulatorpersonInfo.fregulatorlevel') == 20){
        $joins = 'inner join (select fad_name,(count(1)+1) as fregion_count,concat("'.session('regulatorpersonInfo.regionname1').',",group_concat(fname1)) as fregionname,count(1) as ydscount from (select a.fad_name,b.fname1 from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid and b.flevel in (1,2,3) inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where '.$where_time2.' and fcustomer = '.$system_num.' and left(fregion_id,2)<>"'.substr(session('regulatorpersonInfo.regionid'),0,2).'" group by a.fregion_id,a.fad_name ) as a group by a.fad_name) k on a.fad_name = k.fad_name';
      }elseif(session('regulatorpersonInfo.fregulatorlevel') == 30){
        $joins = 'inner join (select fad_name,count(1) as fregion_count,group_concat(fname1) as fregionname,count(1) as ydscount from (select a.fad_name,b.fname1 from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid and b.flevel in (1,2,3) inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where '.$where_time2.' and fcustomer = '.$system_num.' group by left(a.fregion_id,2),a.fad_name ) as a group by a.fad_name having fregion_count>1) k on a.fad_name = k.fad_name';
      }
      $fieldstr = ',fregion_count,fregionname,ydscount';
    }else{
      $whereStr = '1=0';
    }
   
    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->join($joins)
      ->where($whereStr)
      ->count();//查询满足条件的总记录数

    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus2,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fcustomer'.$fieldstr)
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->join($joins)
      ->where($whereStr)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();

    if($agpDataMerge == 1 || $agpDataMerge == 2){
      foreach ($do_tia as $key => $value) {
        if($agpDataMerge == 1 || $agpDataMerge == 2){
          $do_tia[$key]['customerName'] = A('Core')->returnSystemType($value['fcustomer'],$system_num);//所属系统类别
        }
      }
    }

    if($system_num == '100000'){
      foreach ($do_tia as $key => $value) {
        $do_tia[$key]['isedit'] = 0;
        $asqlstr = 'select DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,fad_name from tbn_illegal_ad_issue,tbn_illegal_ad where fsend_status = 2 and tbn_illegal_ad.fid = '.$value['fid'].' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id';
        $doAA = M()->query($asqlstr);
        if(empty($doAA)){
          continue;
        }
        $stday = date('d',strtotime($doAA[0]['fstarttime']));
        $edday = date('d',strtotime($doAA[0]['fendtime']));
        if($stday >= 1 && $stday <= 15){
          $stdate = date('Y-m-01',strtotime($doAA[0]['fstarttime']));
        }else{
          $stdate = date('Y-m-16',strtotime($doAA[0]['fstarttime']));
        }
        if($edday >= 1 && $edday <= 15){
          $eddate = date('Y-m-15',strtotime($doAA[0]['fendtime']));
        }else{
          $eddate = date('Y-m-d',strtotime(date('Y-m-01',strtotime($doAA[0]['fendtime'])). ' +1 month -1 day'));
        }

        if(session('regulatorpersonInfo.fregulatorlevel') == 10 || session('regulatorpersonInfo.fregulatorlevel') == 0){
          $sqlstr = 'select count(*) acount from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid and b.flevel in (1,2,3) inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where fsend_status = 2 and fcustomer = '.$system_num.' and left(fregion_id,4)<>"'.substr(session('regulatorpersonInfo.regionid'),0,2) .'00" and left(fregion_id,4)<>"'.substr(session('regulatorpersonInfo.regionid'),0,4).'" and fissue_date between "'.$stdate.'" and "'.$eddate.'" and fad_name = "'.$doAA[0]['fad_name'].'"';
        }elseif(session('regulatorpersonInfo.fregulatorlevel') == 20){
          $sqlstr = 'select count(*) acount from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid and b.flevel in (1,2,3) inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where fsend_status = 2 and fcustomer = '.$system_num.' and left(fregion_id,2)<>"'.substr(session('regulatorpersonInfo.regionid'),0,2).'" and fissue_date between "'.$stdate.'" and "'.$eddate.'" and fad_name = "'.$doAA[0]['fad_name'].'"';
        }elseif(session('regulatorpersonInfo.fregulatorlevel') == 30){
          $sqlstr = 'select count(*) acount from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid and b.flevel in (1,2,3) inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where fsend_status = 2 and fcustomer = '.$system_num.' and fissue_date between "'.$stdate.'" and "'.$eddate.'" and fad_name = "'.$doAA[0]['fad_name'].'"';
        }

        $doSS = M()->query($sqlstr);
        if(!empty($doSS[0]['acount'])){
          $do_tia[$key]['isedit'] = 0;
        }else{
          $do_tia[$key]['isedit'] = 1;
        }
      }
    }
    

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
  }

  /**
  * 提交复核申请
  * by zw
  */
  public function fhshuju(){
    
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $datacheck_isfile = $ALL_CONFIG['datacheck_isfile'];//数据复核时是否强制上传文件，0否，1强制所有级别用户，2强制非顶级用户
    $datacheck_istext = $ALL_CONFIG['datacheck_istext'];//数据复核是否有意见输入，0没有，1所有都有，2顶级有，3所有都有并强制要求输入，4顶级有并强制要求输入
    $tregionlevel = $ALL_CONFIG['tregionlevel'];//到客户机构等级终止提交复核
    $agpDataMerge = $ALL_CONFIG['agpDataMerge'];//AGP的数据合并方式，0不合并，1合并所有能合并的，2仅合并违法线索

    $selfid     = I('selfid');//违法广告ID组
    $attachinfo = I('attachinfo');//上传的文件信息
    $kystatus   = I('kystatus');//是否跨域（0否，10是）
    $fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');//机构级别
    $fregulatorppid = session('regulatorpersonInfo.fregulatorppid');
    $selnote = I('selnote');//审核意见
    $send_upids = [];//需要更新的派发记录ID

    if(strlen($fregulatorppid) == 8){
      $fregulatorppregion = substr($fregulatorppid,2,6);
      $tregion_len = get_tregionlevel($fregulatorppregion);
      if($tregion_len == 1){//国家级
        $fregulatorppregion = 30;
      }elseif($tregion_len == 2){//省级
        $fregulatorppregion = 20;
      }elseif($tregion_len == 4){//市级
        $fregulatorppregion = 10;
      }elseif($tregion_len == 6){//县级
        $fregulatorppregion = 0;
      }
    }else{
      $fregulatorppregion = $fregulatorlevel + 10;
    }

    $attach     = [];
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    if(empty($attachinfo) && empty($agpDataMerge)){
      if($datacheck_isfile == 1 || ($datacheck_isfile == 2 && $tregionlevel != ($fregulatorlevel + 10))){
        $this->ajaxReturn(array('code'=>1,'msg'=>'请上传文件'));
      }
    }
    if(empty($selnote) && empty($agpDataMerge)){
      if($datacheck_istext == 3 || ($datacheck_istext == 4 && $tregionlevel == ($fregulatorlevel + 10))){
        $this->ajaxReturn(array('code'=>1,'msg'=>'请输入审核意见'));
      }
    }
    $where_tia['fstatus']     = 0;
    $selTia = M('tbn_illegal_ad')->field('fid,fcustomer,fmedia_class,fsample_id,fregion_id')->where(['fid'=>['in',$selfid]])->select();
    foreach ($selTia as $key => $value) {
      if(!empty($agpDataMerge)){
        $tregionlevel = getconfig('tregionlevel',$value['fcustomer']);//到客户机构等级终止提交复核
      }
      if(!empty($kystatus)){
        $asqlstr = 'select DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,fad_name from tbn_illegal_ad_issue,tbn_illegal_ad where fsend_status = 2 and tbn_illegal_ad.fid = '.$value['fid'].' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$value['fcustomer'].'" group by fillegal_ad_id';
        $doAA = M()->query($asqlstr);
        if(empty($doAA)){
          continue;
        }
        $stday = date('d',strtotime($doAA[0]['fstarttime']));
        $edday = date('d',strtotime($doAA[0]['fendtime']));
        if($stday >= 1 && $stday <= 15){
          $stdate = date('Y-m-01',strtotime($doAA[0]['fstarttime']));
        }else{
          $stdate = date('Y-m-16',strtotime($doAA[0]['fstarttime']));
        }
        if($edday >= 1 && $edday <= 15){
          $eddate = date('Y-m-15',strtotime($doAA[0]['fendtime']));
        }else{
          $eddate = date('Y-m-d',strtotime(date('Y-m-01',strtotime($doAA[0]['fendtime'])). ' +1 month -1 day'));
        }

        if(session('regulatorpersonInfo.fregulatorlevel') == 10 || session('regulatorpersonInfo.fregulatorlevel') == 0){
          $sqlstr = 'select count(*) acount from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid and b.flevel in (1,2,3) inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where fsend_status = 2 and fcustomer = '.$value['fcustomer'].' and left(fregion_id,4)<>"'.substr(session('regulatorpersonInfo.regionid'),0,2) .'00" and left(fregion_id,4)<>"'.substr(session('regulatorpersonInfo.regionid'),0,4).'" and fissue_date between "'.$stdate.'" and "'.$eddate.'" and fad_name = "'.$doAA[0]['fad_name'].'"';
        }elseif(session('regulatorpersonInfo.fregulatorlevel') == 20){
          $sqlstr = 'select count(*) acount from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid and b.flevel in (1,2,3) inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where fsend_status = 2 and fcustomer = '.$value['fcustomer'].' and left(fregion_id,2)<>"'.substr(session('regulatorpersonInfo.regionid'),0,2).'" and fissue_date between "'.$stdate.'" and "'.$eddate.'" and fad_name = "'.$doAA[0]['fad_name'].'"';
        }elseif(session('regulatorpersonInfo.fregulatorlevel') == 30){
          $sqlstr = 'select count(*) acount from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid and b.flevel in (1,2,3) inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where fsend_status = 2 and fcustomer = '.$value['fcustomer'].' and fissue_date between "'.$stdate.'" and "'.$eddate.'" and fad_name = "'.$doAA[0]['fad_name'].'"';
        }

        $doSS = M()->query($sqlstr);
        if(!empty($doSS[0]['acount'])){
          continue;
        }
      }

      $where_tia['fid']         = $value['fid'];
      $where_tia['fcustomer']  = $value['fcustomer'];
   
      $data_tia['fstatus']      = 30;//复核状态
      $data_tia['fstatus3']     = 10;//复核申请
      $data_tia['fview_status'] = 10;//已查看
      $do_tia = M('tbn_illegal_ad')->where($where_tia)->save($data_tia);//判断用户是否有该信息处理的权限，并保存相应状态
      if(!empty($do_tia)){
        $send_upids[] = $value['fid'];
        $data_tdc['fillegal_ad_id'] = $value['fid'];
        $data_tdc['tcheck_createtime']  = date('Y-m-d H:i:s');
        $data_tdc['tregionid'] = substr(session('regulatorpersonInfo.regionid'),0,2);
        if($fregulatorlevel == 30){//国家级
          $data_tdc['tgj_treid']     = session('regulatorpersonInfo.fregulatorpid');
          $data_tdc['tgj_trename']   = session('regulatorpersonInfo.regulatorpname');
          $data_tdc['tgj_username']  = session('regulatorpersonInfo.fname');
          $data_tdc['tgj_time']      = date('Y-m-d H:i:s');
          $data_tdc['tgj_result']    = $selnote?$selnote:'同意';
          $data_tdc['tlook_status']   = $fregulatorlevel;
          $data_tdc['tkuayu_status']  = 10;
          $data_tdc['tcheck_jingdu']  = $fregulatorppregion;
          $data_tdc['tcheck_status']  = 10;

          M()->execute('update tbn_illegal_ad set fstatus3=30 where fid='.$value['fid']);//复核成功
          $weihuphone = $ALL_CONFIG['fuhephone'];
          $fhphone = S('fh'.$weihuphone);
          if(empty($fhphone)){
            S('fh'.$weihuphone,date('Y-m-d H:i:s'),43200);//半天一提醒
            $sms_return = A('Common/Alitongxin','Model')->usertask_sms($weihuphone,'复核任务');
          }
        }elseif($fregulatorlevel == 20){//省级
          if(empty($kystatus) || (!empty($kystatus) && empty($doSS[0]['acount'])) || $tregionlevel==30){
            $data_tdc['tshengj_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_tdc['tshengj_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_tdc['tshengj_username']  = session('regulatorpersonInfo.fname');
            $data_tdc['tshengj_time']      = date('Y-m-d H:i:s');
            $data_tdc['tshengj_result']    = $selnote?$selnote:'同意';
            $data_tdc['tlook_status']   = $fregulatorlevel;
            $data_tdc['tkuayu_status']  = $kystatus;
            $data_tdc['tcheck_jingdu']  = $fregulatorppregion;
            $data_tdc['tcheck_status']  = 10;

            M()->execute('update tbn_illegal_ad set fstatus3=30 where fid='.$value['fid']);//复核成功
            $weihuphone = $ALL_CONFIG['fuhephone'];
            $fhphone = S('fh'.$weihuphone);
            if(empty($fhphone)){
              S('fh'.$weihuphone,date('Y-m-d H:i:s'),43200);//半天一提醒
              $sms_return = A('Common/Alitongxin','Model')->usertask_sms($weihuphone,'复核任务');
            }
          }else{
            $data_tdc['tshengj_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_tdc['tshengj_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_tdc['tshengj_username']  = session('regulatorpersonInfo.fname');
            $data_tdc['tshengj_time']      = date('Y-m-d H:i:s');
            $data_tdc['tshengj_result']    = $selnote?$selnote:'申请复核';
            $data_tdc['tlook_status']   = $fregulatorlevel;
            $data_tdc['tkuayu_status']  = $kystatus;
            $data_tdc['tcheck_jingdu']  = $fregulatorppregion;
            $data_tdc['tcheck_status']  = 0;
          }
        }elseif($fregulatorlevel == 10){//市级
          if($tregionlevel==20){
            $data_tdc['tshij_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_tdc['tshij_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_tdc['tshij_username']  = session('regulatorpersonInfo.fname');
            $data_tdc['tshij_time']      = date('Y-m-d H:i:s');
            $data_tdc['tshij_result']    = $selnote?$selnote:'同意';
            $data_tdc['tlook_status']   = $fregulatorlevel;
            $data_tdc['tkuayu_status']  = $kystatus;
            $data_tdc['tcheck_jingdu']  = $fregulatorppregion;
            $data_tdc['tcheck_status']  = 10;

            M()->execute('update tbn_illegal_ad set fstatus3=30 where fid='.$value['fid']);//复核成功
            $weihuphone = $ALL_CONFIG['fuhephone'];
            $fhphone = S('fh'.$weihuphone);
            if(empty($fhphone)){
              S('fh'.$weihuphone,date('Y-m-d H:i:s'),43200);//半天一提醒
              $sms_return = A('Common/Alitongxin','Model')->usertask_sms($weihuphone,'复核任务');
            }
          }else{
            $data_tdc['tshij_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_tdc['tshij_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_tdc['tshij_username']  = session('regulatorpersonInfo.fname');
            $data_tdc['tshij_time']      = date('Y-m-d H:i:s');
            $data_tdc['tshij_result']    = $selnote?$selnote:'申请复核';
            $data_tdc['tlook_status']   = $fregulatorlevel;
            $data_tdc['tkuayu_status']  = $kystatus;
            $data_tdc['tcheck_jingdu']  = $fregulatorppregion;
            $data_tdc['tcheck_status']  = 0;
          }
        }else{//区级
          if($tregionlevel==10){
            $data_tdc['tquj_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_tdc['tquj_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_tdc['tquj_username']  = session('regulatorpersonInfo.fname');
            $data_tdc['tquj_time']      = date('Y-m-d H:i:s');
            $data_tdc['tquj_result']    = $selnote?$selnote:'同意';
            $data_tdc['tlook_status']   = $fregulatorlevel;
            $data_tdc['tkuayu_status']  = $kystatus;
            $data_tdc['tcheck_jingdu']  = $fregulatorppregion;
            $data_tdc['tcheck_status']  = 10;
          }else{
            $data_tdc['tquj_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_tdc['tquj_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_tdc['tquj_username']  = session('regulatorpersonInfo.fname');
            $data_tdc['tquj_time']      = date('Y-m-d H:i:s');
            $data_tdc['tquj_result']    = $selnote?$selnote:'申请复核';
            $data_tdc['tlook_status']   = $fregulatorlevel;
            $data_tdc['tkuayu_status']  = $kystatus;
            $data_tdc['tcheck_jingdu']  = $fregulatorppregion;
            $data_tdc['tcheck_status']  = 0;
          }
        }
        $do_tdc = M('tbn_data_check')->add($data_tdc);

        //上传附件
        $attach_data['fillegal_ad_id']    = $value['fid'];//违法广告
        $attach_data['fcheck_id']         = $do_tdc;//复核表Id
        $attach_data['fupload_trelevel']  = $fregulatorlevel;//上传机构级别
        $attach_data['fupload_treid']     = session('regulatorpersonInfo.fregulatorpid');//机构ID
        $attach_data['fupload_trename']   = session('regulatorpersonInfo.regulatorpname');//机构名
        $attach_data['fuploader']         = session('regulatorpersonInfo.fname');//上传人
        $attach_data['fupload_time']      = date('Y-m-d H:i:s');//上传时间
        foreach ($attachinfo as $key2 => $value2){
          $attach_data['fattach']     = $value2['fattachname'];
          $attach_data['fattach_url'] = $value2['fattachurl'];
          array_push($attach,$attach_data);
        }
      }
    }
    if(!empty($do_tdc)){
      if(!empty($send_upids)){
        M('tbn_case_send')->where(['fstatus'=>0,'fillegal_ad_id'=>['in',$send_upids]])->save(['freceiver'=>session('regulatorpersonInfo.fname'),'frece_time'=>date('Y-m-d H:i:s'),'fstatus'=>30]);
      }
      M('tbn_data_check_attach')->addAll($attach);
      D('Function')->write_log('数据复核',1,'操作成功','tbn_illegal_ad',0,M('tbn_illegal_ad')->getlastsql());
      $this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
    }else{
      D('Function')->write_log('数据复核',0,'操作失败','tbn_illegal_ad',0,M('tbn_illegal_ad')->getlastsql());
      $this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
    }
  }

  /**
  * 违法广告线索审核列表
  * by zw
  */
  public function filladlist(){
    session_write_close();
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $ischeck = $ALL_CONFIG['ischeck'];
    $isrelease = $ALL_CONFIG['isrelease'];
    $illDistinguishPlatfo = $ALL_CONFIG['illDistinguishPlatfo'];//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode($ALL_CONFIG['media_class'],true);//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

    $iscontain = I('iscontain');//是否包含下属地区

    $outtype = I('outtype');//导出类型
    if(!empty($outtype)){
      $p  = I('page', 1);//当前第几页
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页
      $pp = 20;//每页显示多少记录
    }
    
    $fregionid = I('fregionid');//行政区划
    $fexamine = I('fexamine')?I('fexamine'):0;//线索审核状态

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    if($isrelease == 2){
      $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date',-1,$isrelease);
    }else{
      $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date',0,$isrelease);
    }

    //是否抽查模式
    if(!empty($ischeck)){
        $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
        //如果抽查表有数据
        if(!empty($spot_check_data)){
            $dates = [];//定义日期数组
            foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                $year_month = '';
                $date_str = [];
                $year_month = substr($spot_check_data_val['fmonth'],0,7);
                if(!empty($spot_check_data_val['condition'])){
                  $date_str = explode(',',$spot_check_data_val['condition']);
                }
                foreach ($date_str as $date_str_val){
                    $dates[] = $year_month.'-'.$date_str_val;
                }
            }
        $where_time   .= ' and tbn_illegal_ad_issue.fissue_date in ("'.implode('","', $dates).'")';
        }else{
            $where_time   .= ' and 1=0';
        }
    }

    $netadtype  = I('netadtype');//平台类别
    $search_type  = I('search_type');//搜索类别
    $search_val   = I('search_val');//搜索值
    if(!empty($search_val)){
      if($search_type == 10){//媒体分类
        $where_tia['a.fmedia_class'] = $search_val;
      }elseif($search_type == 20){//广告类别
        $where_tia['b.ffullname'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 30){//广告名称
        $where_tia['a.fad_name'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 40){//发布媒体
        $where_tia['c.fmedianame'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 50){//违法原因
        $where_tia['a.fillegal'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 60){//广告主
        $where_tia['tor.fname'] = array('like','%'.$search_val.'%');
      }
    }

    // $where_tia['a.fstatus']     = 0;//处理状态，未处理

    if(!empty($fregionid)){
      $tregion_len = get_tregionlevel($fregionid);
      if(!empty($iscontain)){
        if($tregion_len == 1){//国家级
          $where_tia['a.fregion_id'] = $fregionid;
        }elseif($tregion_len == 2){//省级
          $where_tia['a.fregion_id'] = ['like',substr($fregionid,0,2).'%'];
        }elseif($tregion_len == 4){//市级
          $where_tia['a.fregion_id'] = ['like',substr($fregionid,0,4).'%'];
        }elseif($tregion_len == 6){//县级
          $where_tia['a.fregion_id'] = ['like',substr($fregionid,0,6).'%'];
        }
      }else{
        $where_tia['a.fregion_id']  = $fregionid;
      }
    }

    $where_tia['a.fexamine'] = $fexamine;

    if(!empty($netadtype)){
      $where_tia['a.fplatform']  = $netadtype;
    }
    if(!empty($illDistinguishPlatfo)){
      $where_tia['left(c.fmediaclassid,2)'] = ['in',$media_class];
    }
  
    $where_tia['a.fcustomer'] = $system_num;

    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on a.fid = snd.illad_id')
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus2,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fexamine,(case when a.fexamine=0 then "未审核" else "已审核" end) as fexaminename,(case when a.fmedia_class=1 then "电视" when a.fmedia_class=2 then "广播" when a.fmedia_class=3 then "报纸" when a.fmedia_class=13 then "互联网" end) as fmedia_classname,a.fadowner as fadownername,a.fexaminetime,dck.tcheck_status,dck.tcheck_fhreson,dck.tshengj_result,dck.tshij_result,dck.tquj_result,tn.ffullname')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on a.fid = snd.illad_id')
      ->join('(select * from tbn_data_check where id in (select max(id) from tbn_data_check where ftype = 1 group by fillegal_ad_id)) dck on a.fid = dck.fillegal_ad_id','left')
      ->where($where_tia)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();
      
    if(!empty($outtype)){
      if(empty($do_tia)){
        $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
      }

      $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-违法广告线索审核列表';//文档内部标题名称
      $outdata['datalie'] = [
          '序号'=>'key',
          '地域'=>'ffullname',
          '广告名称'=>'fad_name',
          '广告类别'=>'fadclass',
          '发布媒体'=>'fmedianame',
          '媒体类别'=>'fmedia_classname',
          '广告主'=>'fadownername',
          '播出总条次'=>'fcount',
          '播出周数'=>'diffweek',
          '播出时间段'=>[
            'type'=>'zwhebing',
            'data'=>'{fstarttime}~{fendtime}'
          ],
          '违法表现代码'=>'fillegal_code',
          '违法表现'=>'fexpressions',
          '涉嫌违法原因'=>'fillegal',
          '证据下载地址'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1 || {fmedia_class} == 2','{favifilename}'],
              ['','{fjpgfilename}']
            ]
          ],
          '状态'=>'fexaminename',
        ];
      $outdata['lists'] = $do_tia;
      $ret = A('Api/Function')->outdata_xls($outdata);
      D('Function')->write_log('违法广告线索审核列表',1,'导出成功');
      $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

    }else{
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
    }
  }

  /**
  * 违法广告线索审核
  * by zw
  */
  public function filladexamine(){
    $ALL_CONFIG = getconfig('ALL');
    $isexamine = $ALL_CONFIG['isexamine'];
    $fuhephone = $ALL_CONFIG['fuhephone'];

    $selfid     = I('selfid');//违法广告ID组
    $selfresult = I('selfresult');//审核状态
    $selnote = I('selnote')?I('selnote'):"同意";//审核意见
    $fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');//机构级别
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    if(empty($selfresult)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择审核状态'));
    }

    $where_tia['fid']      = array('in',$selfid);
    
    $data_tia['fexamine'] = $selfresult;
    $data_tia['fexaminetime'] = date('Y-m-d H:i:s');

    if(in_array($isexamine, [20,40,50])){
      //审核不通过的数据直接申请复核
      if($selfresult == 20){
        $data_tia['fstatus'] = 30;
        $data_tia['fstatus3'] = 30;
        M('tbn_case_send')->where(['fillegal_ad_id'=>['in',$selfid],'fstatus'=>0])->save(['fstatus'=>30]);
        M('tbn_illegal_ad')->where($where_tia)->save($data_tia);
      }else{
        foreach ($selfid as $key => $value) {
          $nowcount = M('tbn_illegal_ad')->where(['fid'=>$value,'fexamine'=>20])->count();
          if(!empty($nowcount)){
            M()->execute('update tbn_illegal_ad_attach set fstate=10 where fstate=0 and fillegal_ad_id='.$value);//上报文件失效
            M()->execute('update tbn_data_check set tcheck_status=20 where fillegal_ad_id='.$value);//复核记录退回
            M()->execute('update tbn_data_check_attach set fstate=10 where fstate=0 and fillegal_ad_id='.$value);//复核文件失效
            M()->execute('update tbn_case_send set fstatus=0 where fstatus<>0 and fillegal_ad_id='.$value.' and fid in(select fid from (select max(fid) fid from tbn_case_send where fstatus<>0 and fillegal_ad_id='.$value.' ) a)');//分派记录重置
            M('tbn_illegal_ad')->where(['fid'=>$value])->save(['fstatus'=>0,'fstatus3'=>0,'fexamine'=>$selfresult,'fexaminetime'=>date('Y-m-d H:i:s')]);
          }else{
            M('tbn_illegal_ad')->where(['fid'=>$value])->save($data_tia);
          }
        }
      }
    }else{
      M('tbn_illegal_ad')->where($where_tia)->save($data_tia);
    }
    
    if($selfresult == 20 && in_array($isexamine, [20,40,50])){
      $data_tdc = [];
      $data_tdc['tcheck_createtime']  = date('Y-m-d H:i:s');
      $data_tdc['tregionid'] = substr(session('regulatorpersonInfo.regionid'),0,2);
      foreach ($selfid as $key => $value) {
        $data_tdc['fillegal_ad_id'] = $value;
        $data_tdc['ftype']  = 1;
        if($fregulatorlevel == 30){//国家级
          $data_tdc['tgj_treid']     = session('regulatorpersonInfo.fregulatorpid');
          $data_tdc['tgj_trename']   = session('regulatorpersonInfo.regulatorpname');
          $data_tdc['tgj_username']  = session('regulatorpersonInfo.fname');
          $data_tdc['tgj_time']      = date('Y-m-d H:i:s');
          $data_tdc['tgj_result']    = $selnote;
          $data_tdc['tlook_status']   = $fregulatorlevel;
          $data_tdc['tkuayu_status']  = 10;
          $data_tdc['tcheck_jingdu']  = 40;
          $data_tdc['tcheck_status']  = 10;
        }elseif($fregulatorlevel == 20){//省级
          $data_tdc['tshengj_treid']     = session('regulatorpersonInfo.fregulatorpid');
          $data_tdc['tshengj_trename']   = session('regulatorpersonInfo.regulatorpname');
          $data_tdc['tshengj_username']  = session('regulatorpersonInfo.fname');
          $data_tdc['tshengj_time']      = date('Y-m-d H:i:s');
          $data_tdc['tshengj_result']    = $selnote;
          $data_tdc['tlook_status']   = $fregulatorlevel;
          $data_tdc['tkuayu_status']  = 0;
          $data_tdc['tcheck_jingdu']  = 30;
          $data_tdc['tcheck_status']  = 10;
        }elseif($fregulatorlevel == 10){//市级
          $data_tdc['tshij_treid']     = session('regulatorpersonInfo.fregulatorpid');
          $data_tdc['tshij_trename']   = session('regulatorpersonInfo.regulatorpname');
          $data_tdc['tshij_username']  = session('regulatorpersonInfo.fname');
          $data_tdc['tshij_time']      = date('Y-m-d H:i:s');
          $data_tdc['tshij_result']    = $selnote;
          $data_tdc['tlook_status']   = $fregulatorlevel;
          $data_tdc['tkuayu_status']  = 0;
          $data_tdc['tcheck_jingdu']  = 20;
          $data_tdc['tcheck_status']  = 10;
        }else{//区级
          $data_tdc['tquj_treid']     = session('regulatorpersonInfo.fregulatorpid');
          $data_tdc['tquj_trename']   = session('regulatorpersonInfo.regulatorpname');
          $data_tdc['tquj_username']  = session('regulatorpersonInfo.fname');
          $data_tdc['tquj_time']      = date('Y-m-d H:i:s');
          $data_tdc['tquj_result']    = $selnote;
          $data_tdc['tlook_status']   = $fregulatorlevel;
          $data_tdc['tkuayu_status']  = 0;
          $data_tdc['tcheck_jingdu']  = 10;
          $data_tdc['tcheck_status']  = 10;
        }
        $data_tdcs[] = $data_tdc;
      }
      M('tbn_data_check')->addAll($data_tdcs);

      $fhphone = S('fh'.$weihuphone);
      if(empty($fhphone)){
          S('fh'.$weihuphone,date('Y-m-d H:i:s'),43200);//半天一提醒
          $sms_return = A('Common/Alitongxin','Model')->usertask_sms($weihuphone,'复核任务');
      }
    }

    D('Function')->write_log('违法广告线索审核',1,'操作完成>'.$selfid,'tbn_illegal_ad',0,M('tbn_illegal_ad')->getlastsql());
    $this->ajaxReturn(array('code'=>0,'msg'=>'操作完成'));
  
  }

  /**
  * 违法广告线索修改
  * by zw
  */
  public function filladedit(){
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $isexamine = $ALL_CONFIG['isexamine'];

    if(!in_array($isexamine, [30,40,50])){
      $this->ajaxReturn(array('code'=>1,'msg'=>'无权限'));
    }

    $fid = I('fid');//fid
    $fad_name = I('fad_name');//广告名称
    $fad_class_code = I('fad_class_code');//广告类别
    $fillegal_code = I('fillegal_code');//违法表现代码
    $fexpressions = I('fexpressions');//违法表现
    $fillegal = I('fillegal');//违法内容

    $viewTia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fmedia_class,b.cr_name,a.fad_name,a.fad_class_code,a.fillegal_code,a.fexpressions,a.fillegal,c.fmedianame,a.fmedia_id,a.fsample_id')
      ->join('customer b on a.fcustomer = b.cr_num')
      ->join('tmedia c on a.fmedia_id = c.fid')
      ->where(['a.fid'=>$fid,'fcustomer'=>$system_num])
      ->find();
    if(empty($viewTia)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'无权限'));
    }


    $saveData['fad_name'] = $fad_name;
    $saveData['fad_class_code'] = $fad_class_code;
    $saveData['fillegal_code'] = $fillegal_code;
    $saveData['fexpressions'] = $fexpressions;
    $saveData['fillegal'] = $fillegal;

    $checkEditData = checkEditData($viewTia,$saveData,['fad_name','fad_class_code','fillegal_code','fexpressions','fillegal'],[],['广告名称','广告类别','违法表现代码','违法表现','违法内容']);//获取修改前后差异数据

    if(empty($checkEditData)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'修改失败，未作调整'));
    }

    $checkEditData['记录时间'] = date('Y-m-d H:i:s');
    $checkEditData['操作人'] = session('regulatorpersonInfo.fname').'（'.session('regulatorpersonInfo.fid').'）';
    $flog = json_encode($checkEditData,JSON_UNESCAPED_UNICODE);
    $saveData['flog'] = ['exp',"concat('".$flog."','；',flog)"]; 
    $saveTia = M('tbn_illegal_ad')->where(['fid'=>$fid,'fcustomer'=>$system_num])->save($saveData);
    if(!empty($saveTia)){
      if($viewTia['fmedia_class'] == 1){
        $sampleIssueDate = M('ttvsample')->where(['fid'=>$viewTia['fsample_id']])->getField('fissuedate');
      }elseif($viewTia['fmedia_class'] == 2){
        $sampleIssueDate = M('tbcsample')->where(['fid'=>$viewTia['fsample_id']])->getField('fissuedate');
      }elseif($viewTia['fmedia_class'] == 3){
        $sampleIssueDate = M('tpapersample')->where(['fpapersampleid'=>$viewTia['fsample_id']])->getField('fissuedate');
      }

      if(in_array($isexamine, [30,40])){
        $title = '违法广告审核提醒';
        $content = "> ".$viewTia['cr_name']."平台用户在对违法广告审核时有如下调整：\n\n";
        $content .= "违法广告：".$viewTia['fad_name']."\n\n";
        $content .= "媒体名称：".$viewTia['fmedianame']."（".$viewTia['fmedia_id']."）\n\n";
        $content .= "样本ID：".$viewTia['fsample_id']."\n\n";
        if(!empty($sampleIssueDate)){
          $content .= "发布日期：".date('Y-m-d',strtotime($sampleIssueDate))."\n\n";
        }
        $content .= "调整内容：".$flog."\n\n";
        $content .= "针对以上用户的调整，请确认后对样本进行适当修改，以保障之后提供的数据能尽量满足用户需求 \n\n";
        $smsphone = [18109017112,13658062036];//提醒人员，曹席亮、袁俊
        $token = 'f39909c52fe6a7d42c8ea886ad12fdd2d656c38b10dcb2969e140361ae23b545';
        push_ddtask($title,$content,$smsphone,$token,'markdown');
      }
      
      D('Function')->write_log('违法广告修改',1,'修改成功','tbn_illegal_ad',$saveData,M('tbn_illegal_ad')->getlastsql());
      $this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
    }else{
      $this->ajaxReturn(array('code'=>1,'msg'=>'修改失败'));
    }

  }

  /**
  * 违法广告线索修改
  * by zw
  */
  public function filladdel(){
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $isexamine = $ALL_CONFIG['isexamine'];

    if(!in_array($isexamine, [50])){
      $this->ajaxReturn(array('code'=>1,'msg'=>'无权限'));
    }

    $fid = I('fid');//主键

    if(empty($fid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
    }

    $whereIai['_string'] = 'fillegal_ad_id in (select fid from tbn_illegal_ad where fid = '.$fid.' and fcustomer = "'.$system_num.'")';
    $whereIai['fexamine'] = 0;
    $delIai = M('tbn_illegal_ad_issue')->where($whereIai)->delete();

    if(!empty($delIai)){
      $this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
    }else{
      $this->ajaxReturn(array('code'=>1,'msg'=>'删除失败'));
    }
  }

  /**
  * 生成unionsql
  * by zw
  */
  public function returnListSql($system_num = ''){
    $iscontain = I('iscontain');//是否包含下属地区
    $area = I('area');//所属地区
    $netadtype  = I('netadtype');//平台类别

    $ALL_CONFIG = getconfig('ALL',$system_num);
    $system_num = $ALL_CONFIG['system_num'];
    $illadTimeoutTips = $ALL_CONFIG['illadTimeoutTips'];//线索处理超时提示，0无，1有
    $againIssueTips = $ALL_CONFIG['againIssueTips'];//上月已做案件处罚后，本月还在继续播放的，是否需要进行提醒，0不需要，1需要
    $illSupervise = $ALL_CONFIG['illSupervise'];//是否需要案件处理的执法监督功能，0不需要，1需要
    $ischeck = $ALL_CONFIG['ischeck'];
    $illDistinguishPlatfo = $ALL_CONFIG['illDistinguishPlatfo'];//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode($ALL_CONFIG['media_class'],true);//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）
    $isexamine = $ALL_CONFIG['isexamine'];
    $isrelease = $ALL_CONFIG['isrelease'];

    //媒体类别格式化
    foreach ($media_class as $value) {
      $mClasses[] = (int)$value;
    }

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $where_time = gettimecondition($years,$timetypes,$timeval,'b.fissue_date',0,$isrelease);

    $where_time .= ' and a.fstatus = 0';

    //是否抽查模式
    if(!empty($ischeck)){
        $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
        //如果抽查表有数据
        if(!empty($spot_check_data)){
            $dates = [];//定义日期数组
            foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                $year_month = '';
                $date_str = [];
                $year_month = substr($spot_check_data_val['fmonth'],0,7);
                if(!empty($spot_check_data_val['condition'])){
                  $date_str = explode(',',$spot_check_data_val['condition']);
                }
                foreach ($date_str as $date_str_val){
                    $dates[] = $year_month.'-'.$date_str_val;
                }
            }
          $where_time   .= ' and b.fissue_date in ("'.implode('","', $dates).'")';
        }else{
          $where_time   .= ' and 1=0';
        }
    }
    
    if(!empty($netadtype)){
      $where_time .= ' and a.fplatform = '.$netadtype;
    }
    
    if(!empty($area)){//所属地区
      if($area != '100000'){
        if(!empty($iscontain)){
          $tregion_len = get_tregionlevel($area);
          if($tregion_len == 1 || $tregion_len == 6){//国家级
            $where_time .= ' and a.fregion_id = '.$area;
          }elseif($tregion_len == 2){//省级
            $where_time .= ' and a.fregion_id like "'.substr($area,0,2).'%"';
          }elseif($tregion_len == 4){//市级
            $where_time .= ' and a.fregion_id like "'.substr($area,0,4).'%"';
          }
        }else{
          $where_time .= ' and a.fregion_id = '.$area;
        }
      }
    }

    if(in_array($isexamine, [10,20,30,40,50])){
      $where_time .= ' and a.fexamine = 10';
    }
    if(!empty($illDistinguishPlatfo)){
      $where_time .= ' and a.fmedia_class in('.implode(',', $mClasses).')';
    }

    return 'select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue b,tbn_illegal_ad a where '.$where_time.' and a.fid = b.fillegal_ad_id and a.fcustomer = "'.$system_num.'" group by fillegal_ad_id';
  }

  /**
  * 生成where条件
  * by zw
  */
  private function returnListWhere(){
    $whereStr = '1=1';
    $search_type  = I('search_type');//搜索类别
    $search_val   = I('search_val');//搜索值
    $adclass   = I('adclass');//广告类别名
    $mclass   = I('mclass');//媒体类别

    if(!empty($search_val)){
      if($search_type == 10){//媒体分类
        $whereStr .= ' and a.fmedia_class = '.$search_val;
      }elseif($search_type == 20){//广告类别
        $whereStr .= ' and b.ffullname like "%'.$search_val.'%"';
      }elseif($search_type == 30){//广告名称
        $whereStr .= ' and a.fad_name like "%'.$search_val.'%"';
      }elseif($search_type == 40){//发布媒体
        $whereStr .= ' and c.fmedianame like "%'.$search_val.'%"';
      }elseif($search_type == 50){//违法原因
        $whereStr .= ' and a.fillegal like "%'.$search_val.'%"';
      }elseif($search_type == 60){//广告主
        $whereStr .= ' and tor.fname like "%'.$search_val.'%"';
      }
    }

    if(!empty($adclass)){
      $whereStr .= ' and b.ffullname like "%'.$adclass.'%"';
    }

    if(!empty($mclass)){
      $whereStr .= ' and a.fmedia_class = "'.$mclass.'"';
    }

    return $whereStr;
  }

}