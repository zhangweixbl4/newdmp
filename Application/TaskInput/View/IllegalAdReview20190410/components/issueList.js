const issueList = {
    template: `<div v-loading="loading">
    <el-table
            :data="table"
            border
            @selection-change="handleSelectionChange"
    >
        <el-table-column
                type="selection"
                v-if="userType === 'dmp'"
                width="40">
        </el-table-column>
        <el-table-column
                prop="region"
                label="地区"
        ></el-table-column>
        <el-table-column
                label="媒体名称"
        >
            <template slot-scope="scope">
                <span v-html="scope.row.mediaName"></span>
            </template>
        </el-table-column>
        <el-table-column
                label="广告名称"
        >
            <template slot-scope="scope">
                <span v-html="scope.row.fadname"></span>
            </template>
        </el-table-column>
        <el-table-column
                prop="adClass"
                label="广告类别"
        >
        </el-table-column>
        <el-table-column
                prop="adClass2"
                label="商业类别"
                v-if="userType === 'dmp'"
        >
        </el-table-column>
        <el-table-column
                label="违法表现代码"
        >
            <template slot-scope="scope">
                <el-tooltip placement="top">
                    <div slot="content">
                        <ul class="list-unstyled">
                            <li v-for="(item, index) in scope.row.fexpressions" :key="index">{{item}}</li>
                        </ul>
                    </div>
                    <el-button type="text">{{scope.row.fexpressioncodes}}</el-button>
                </el-tooltip>
            </template>
        </el-table-column>
        <el-table-column
                label="条次"
                width="50"
        >
            <template slot-scope="scope">
                <el-button type="text" @click="showIssueList(scope.row.fid)">{{scope.row.count}}</el-button>
            </template>
        </el-table-column>
    </el-table>
    <el-button type="info" v-if="selectedList.length > 0 && userType === 'dmp'" @click="batchConfirm(1)">批量确认</el-button>
    <el-button type="info" v-if="selectedList.length > 0 && userType === 'dmp'" @click="batchConfirm(0)">批量退回</el-button>
    <el-pagination
            layout="total, prev, pager, next"
            @current-change="pageChange"
            :current-page="page"
            :total="total">
    </el-pagination>
    <el-dialog
            :title="currentAd.fadname"
            :visible="showAdInfo"
            width="100%"
            :modal="false"
            :before-close="closeAdInfo"
    >
        <ad-info 
                :ad="currentAd" 
                ref="adInfo"
                @success="closeAdInfo"
        ></ad-info>
    </el-dialog>
    <el-dialog
            title="请输入退回的备注, 可选择按钮进行快捷输入"
            :visible.sync="showRejectForm"
            :before-close="closeRejectForm">
        <div>
            <el-button @click="rejectReason += '串台'">
                串台
            </el-button>
            <el-button @click="rejectReason += '剪辑广告不完整'">
                剪辑广告不完整
            </el-button>
            <el-button @click="rejectReason += '违法判定错误'">
                违法判定错误
            </el-button>
            <el-button @click="rejectReason += '广告类别错误'">
                广告类别错误
            </el-button>
            <el-button @click="rejectReason += '广告类别错误'">
                其他
            </el-button>
            
        </div>
        <br>
        <div>
            <el-input v-model="rejectReason"></el-input>
        </div>
        <hr>
        <div>
            <el-button type="primary" @click="rejectAd">提交</el-button>
        </div>
    </el-dialog>
</div>`,
    name: 'issueList',
    props: ['table', 'total', 'page'],
    components: {
        adInfo
    },
    data: function () {
        return {
            currentAd: {},
            showAdInfo: false,
            loading: false,
            showRejectForm: false,
            rejectReason: '',
            tableData: this.table,
            totalData: this.total,
            selectedList: [],
            userType,
        }
    },
    methods:{
        pageChange: function (val) {
            this.$emit('page-change', val)
        },
        showIssueList: function (sampleId) {
            this.loading = true
                $.post('getIllegalAdInfo', {
                    condition: this.$root.baseCondition,
                    sampleId,
                }).then((res) => {
                    if (res.code == 0){
                        this.currentAd = res.data
                        this.showAdInfo = true
                        this.$nextTick(() => {
                            this.$refs.adInfo.processData()
                        })
                    } else {
                        this.$message.error('获取信息失败')
                    }
                    this.loading = false
                })
        },
        closeAdInfo: function () {
            this.showAdInfo = false
            this.currentAd = {}
            this.$refs.adInfo.changeTab()
            this.$emit('search-req')
        },
        handleSelectionChange: function (val) {
            this.selectedList = val
        },
        batchConfirm: function (type) {
            if (this.selectedList.length === 0){
                this.$message.warning('请至少选择一项')
                return false
            }
            if (type === 1) {
                this.$confirm('是否确认 ? ').then(() => {
                    const data = {
                        ids: this.selectedList.map((v) => v['fid']).join(','),
                        condition: this.$root.baseCondition,
                        type,
                    }
                    this.loading = true
                    $.post('batchConfirm', data).then((res) => {
                        this.loading = false
                        if (res.code == 0){
                            this.$message.success(res.msg)
                        } else{
                            this.$message.error(res.msg)
                        }
                        this.$emit('search-req')
                    })
                }).catch(() => {})
            }else{
                this.showRejectForm = true
                this.rejectReason = ''
            }
        },
        closeRejectForm: function () {
            this.showRejectForm = false
            this.rejectReason = ''
        },
        rejectAd: function () {
            if (!this.rejectReason){
                this.$alert('请输入理由')
                return
            }
            this.$confirm('确认退回?').then(() => {
                this.loading = true
                const data = {
                    ids: this.selectedList.map((v) => v['fid']).join(','),
                    condition: this.$root.baseCondition,
                    type: 0,
                    reason: this.rejectReason
                }
                $.post('batchConfirm', data).then((res) => {
                    this.loading = false
                    if (res.code == 0){
                        this.$message.success(res.msg)
                        this.closeRejectForm()
                    } else{
                        this.$message.error(res.msg)
                    }
                    this.$emit('search-req')
                })
            })
        }
    },
    filters: {
        mediaClassFilter: function (value) {
            let res = ''
            mediaClassArr.forEach((v) => {
                if (v.value == value){
                    res = v.text
                }
            })
            return res
        }
    }
}