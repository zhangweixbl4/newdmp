<?php
namespace Api\Controller;
use Think\Controller;


class UserController extends Controller
{
    public function searchUserByNickname($keyword)
    {
        $res = M('ad_input_user')->field('nickname value')->where(['nickname' => ['LIKE', '%' . $keyword . '%']])->select();
        $this->ajaxReturn([
            'data' => $res
        ]);
    }

    public function searchUserByAlias()
    {
        $keyword = I('keyword');
        if ($keyword){
            $res = M('ad_input_user')->field('alias value')->where(['alias' => ['LIKE', '%' . $keyword . '%']])->select();
        }else{
            $res = M('ad_input_user')->field('alias value')->page(1,10)->select();
        }
        $this->ajaxReturn([
            'data' => $res
        ]);
    }

    public function searchUserValueLabelByAlias()
    {
        $keyword = I('keyword');
        if ($keyword){
            $res = M('ad_input_user')->field('alias label, wx_id value')->where(['alias' => ['LIKE', '%' . $keyword . '%']])->select();
        }else{
            $res = M('ad_input_user')->field('alias label, wx_id value')->page(1,10)->cache(60)->select();
        }
        $this->ajaxReturn([
            'data' => $res
        ]);
    }

    public function searchPersonName()
    {
        $keyword = I('keyword');
        if ($keyword){
            $res = M('tperson')->field('fname value')->where(['fname' => ['LIKE', '%' . $keyword . '%']])->select();
        }else{
            $res = M('tperson')->field('fname value')->page(1,10)->select();
        }
        $this->ajaxReturn([
            'data' => $res
        ]);
    }

    public function searchUserGroupByGroupName($keyword)
    {
        $res = M('ad_input_user_group')->field('group_name value')->where(['group_name' => ['LIKE', '%' . $keyword . '%']])->select();
        $this->ajaxReturn([
            'data' => $res
        ]);
    }
}