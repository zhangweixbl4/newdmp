Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}
var vm = new Vue({
  el: '#app',
  data: {
    table: [],
    where: {},
    count: 0,
    loading: false,
    page: 1,
    showExportExcel: false,
    exportWhere: {},
    total: 0,
    tableHeight: '400px'
  },
  created() {
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight()
    })
    window.addEventListener('resize', () => {
      this.getTableHeight()
    })
  },
  methods: {
    searchReq(){
      this.loading = true
      const data = {
        where: this.where,
        page: this.page
      }
      $fly.post('user_task_count', Qs.stringify(data)).then(res => {
        this.table = res.data
        res.total.nickname = '总计'
        this.table.push(res.total)
        this.total = +res.count
        this.loading = false
      })
    },
    search() {
      this.page = 1
      this.searchReq()
    },
    pageChange(page) {
      this.page = page
      this.searchReq()
    },
    querySearchUser(queryString, cb) {
      const data = {
        keyword: queryString
      }
      $fly.post("/Api/User/searchUserByAlias", Qs.stringify(data)).then(res => {
        cb(res.data)
      })
    },
    querySearchUserGroup(queryString, cb) {
      const data = {
        keyword: queryString
      }
      $fly.post("/Api/User/searchUserGroupByGroupName", Qs.stringify(data)).then(res => {
        cb(res.data)
      })
    },
    closeExportExcel() {
      this.showExportExcel = false
      this.exportWhere = {}
    },
    exportExcel() {
      if (this.exportWhere == {}) {
        this.$message.error('请选择时间')
      } else {
        open('exportTaskCountByExcel' + '?startTime=' + this.exportWhere.timeRange[0] + '&endTime=' +
          this.exportWhere.timeRange[1])
      }
    },
    getTableHeight() {
      let formHeight = this.$refs.sform.$el.clientHeight
      this.tableHeight = document.body.clientHeight - formHeight - 106 + 'px'
    },
  }
})