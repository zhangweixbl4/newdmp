<?php
namespace Agp\Controller;
use Think\Controller;
import('Vendor.PHPExcel');
/**
 * 国家局数据统计
 * by yjn
 */
class GjzStatisticalDataController extends BaseController{

    /**
     * 违法广告统计
     * by yjm
     *http://www.ctcmc.com.cn/web/ggsjzx.asp
     * 1.通过选择时间，确认实例化哪张表
     * 2.通过点击选项卡，连接对应表
     * 3.条件
     *  ① '区域'=>['省','副省级市','市','县区']
     *  ② 媒体（媒体的种类）
     *  ③ 广告类别（广告的种类）
     *  ④ 综合得分、时长、时长违法率、违法条次和违法率
     */
    public function illegal_ad_monitor(){
        Check_QuanXian(['jchuizong','wfchachu']);
        ini_set('memory_limit','3072M');
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type: text/html; charset=utf-8");
        $user = session('regulatorpersonInfo');//获取用户信息

        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isrelease = $ALL_CONFIG['isrelease'];
        $use_open_search = $ALL_CONFIG['use_open_search'];
        $ischeck = $ALL_CONFIG['ischeck'];
        $is_manage_media = $ALL_CONFIG['set_manage_region'];//是否通过指定区域管理媒体统计

        $region_order = I('region_order',1);//是否地域排名

        $is_include_sub = I('is_include_sub',0);//是否包含下级

        $ad_pm_type = I('ad_pm_type',1);//确定是点击监测情况或者线索菜单

        $is_out_report = I('is_out_report',0);//确定是否点击导出EXCEL

        $is_have_count = I('is_have_count',0);//确定是否点击导出EXCEL包含条数

        $year = I('year',date('Y'));//默认当年

        $times_table = I('times_table','');//选表，默认月表

        $is_show_fsend = I('is_show_fsend',2);//是否发布前的数据

        $is_show_longad = I('is_show_longad',0);//是否包含长广告

        $permission_media = I('permission_media',1);//控制权限媒体

        $title = I('title','XX市');//描述标题
        $regio_area = I('regio_area','XX市各县');//选择区划文字描述

        $isfixeddata = I('isfixeddata');//是否实时数据

        $date_cycle = I('date_cycle');//日期周期文字描述


        $map = [];//定义查询条件
        if($system_num == '100000' || empty($isfixeddata)){
            $summary_table = "tbn_ad_summary_day_z";
            $map[$summary_table.'.fcustomer'] = $system_num;
            if($is_show_longad == 1){
                //包含长广告
                $play_len_field = 'in_long_ad_play_len';
                $fad_times_field = 'fad_times';
                $fad_count_field = 'fad_sam_list';
            }else{
                //不包含长广告
                $play_len_field = 'fad_play_len';
                $fad_times_field = 'basic_ad_times';
                $fad_count_field = 'basic_ad_sam_list';
            }
        }else{
            if($use_open_search == 1) {
                $summary_table = "tbn_ad_summary_day_v3";
                $map[$summary_table.'.fcustomer'] = $system_num;
                if($is_show_longad == 1){
                    //包含长广告
                    $play_len_field = 'fad_play_len_long_ad';
                    $fad_times_field = 'fad_times_long_ad';
                    $fad_count_field = 'fsam_list_long_ad';
                }else{
                    //不包含长广告
                    $play_len_field = 'fad_play_len';
                    $fad_times_field = 'fad_times';
                    $fad_count_field = 'fsam_list';
                }
            }else{
                $summary_table = "tbn_ad_summary_day";
                if($is_show_longad == 1){
                    $play_len_field = 'in_long_ad_play_len';
                    $fad_times_field = 'fad_times';
                    $fad_count_field = 'fad_sam_list';
                }else{
                    $play_len_field = 'fad_play_len';
                    $fad_times_field = 'basic_ad_times';
                    $fad_count_field = 'basic_ad_sam_list';
                }
            }

        }

        //默认表
        if($times_table == ''){
            $times_table = 'tbn_ad_summary_month';
        }
        $table_condition = I('table_condition',date('m').'-01');//根据选取的不用表展示不同的条件  //默认当月
        //根据选择的不同的时间组合不同的条件
        switch ($times_table){
            case 'tbn_ad_summary_day':
                $sedays = explode(',',$table_condition);
                $cycle_stamp = [
                    'start' =>strtotime($sedays[0]),
                    'end' =>(strtotime($sedays[1])+86400)
                ];//周期首末时间戳
                break;
            case 'tbn_ad_summary_week':
                if($table_condition < 10){
                    $table_condition = '0'.$table_condition;
                }
                $cycle_stamp = $this->weekday($year,$table_condition);//周期首末时间戳
                $cycle_stamp['end'] = $cycle_stamp['end']+86400;
                break;
            case 'tbn_ad_summary_half_month':
            case 'tbn_ad_summary_month':
            case 'tbn_ad_summary_quarter':
                $table_condition = $year.'-'.$table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                break;
            case 'tbn_ad_summary_half_year':
            case 'tbn_ad_summary_year':
                $table_condition = $table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                break;
        }

        $s_time = date('Y-m-d',$cycle_stamp['start']);
        $e_time = date('Y-m-d',($cycle_stamp['end']-86400));

        $map[$summary_table.'.fdate'] = ['BETWEEN',[$s_time,$e_time]];
        $map['_string'] = 'tmedia.fid = tmedia.main_media_id';
        $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_time,$e_time]];
        $illegal_map['tbn_illegal_ad.fstatus3'] = ['NOT IN',[30,40]];
        $s_timestamp = strtotime($s_time);//开始时间戳
        $e_timestamp = strtotime($e_time);//结束时间戳

        //获取上个周期的日期范围
        if(($e_timestamp - $s_timestamp) <= 0){
            $last_s_time = date('Y-m-d',($s_timestamp-86400));//上周期开始时间戳
            $last_e_time = $last_s_time;//上周期结束时间戳
        }else{
            $last_s_time = date('Y-m-d',($s_timestamp-($e_timestamp-$s_timestamp)-86400));
            $last_e_time = date('Y-m-d',($s_timestamp-86400));
        }
        //连表
        //分组统计条件1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
        $fztj = I('fztj','');//默认连接地域表
        if($fztj == ''){
            $fztj = 'fregionid';
        }

        //浏览权限设置start
        $re_level = $this->judgment_region($user['regionid']);//当前用户行政等级

        //是否需要抽查
        if(!empty($ischeck)){
            $spot_check_data = M('spot_check')->where(['fcustomer'=>$user['system_num']])->select();//查询抽查表数据
            //如果抽查表有数据
            if(!empty($spot_check_data)){
                $dates = [];//定义日期数组
                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                    $condition = trim($spot_check_data_val['condition']);
                    if(!empty($condition)){
                        $year_month = '';
                        $date_str = [];
                        $year_month = substr($spot_check_data_val['fmonth'],0,7);
                        if(!empty($spot_check_data_val['condition'])){
                            $date_str = explode(',',$spot_check_data_val['condition']);
                        }
                        foreach ($date_str as $date_str_val){

                            $dates[] = $year_month.'-'.$date_str_val;
                        }
                    }
                }
                $map[$summary_table.'.fdate'] = [['IN',$dates],['BETWEEN',[$s_time,$e_time]],'AND'];
                $illegal_map['tbn_illegal_ad_issue.fissue_date'] = [['IN',$dates],['BETWEEN',[$s_time,$e_time]],'AND'];
            }else{
                $map['_string'] = '1 = 0';
                $illegal_map['_string'] = '1 = 0';
            }
        }

        //如果是要看同级的用户，行政区划登记上升一级1210
        $is_show_tj = $ALL_CONFIG['is_show_tj'];//是否显示同级20181210
        $is_show_sj = $ALL_CONFIG['is_show_sj'];//是否显示省级20181210
        if(!empty($is_show_tj) && $re_level > 1){
            if($re_level == 2 || $re_level == 3 || $re_level == 4){
                $re_level = 1;
                $regionid = substr($user['regionid'],0,2).'0000';
            }else{
                $regionid = trim($user['regionid'],'0').'00';
                $re_level = $this->judgment_region($regionid);
            }
        }else{
            $regionid = $user['regionid'];
        }

        //判断$re_level用户点击哪个菜单
        //线索汇总
        if($ad_pm_type == 2){
            //审核通过的违法数据
            $isexamine = $ALL_CONFIG['isexamine'];
            if(in_array($isexamine, [10,20,30,40,50])){
                $illegal_map['tbn_illegal_ad.fexamine'] = 10;
            }
            if(!empty($isrelease) || $system_num == '100000'){
                $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
            }
            $region = I('region2','');//区域
            if($region == ''){
                if($system_num == '100000' && session('regulatorpersonInfo.fregulatorlevel') != 30){
                    //$map['tregion.fpid|tregion.fid'] = $regionid;
                    $map["tregion.flevel"] = ['IN','1,2,3'];
                }else{
                    if($re_level == 2 || $re_level == 3 || $re_level == 4){
                        $region = 5;
                    }else{
                        $region = $re_level;
                    }
                }
            }
            $illegal_map['tbn_illegal_ad.fcustomer'] = $system_num;
            //如果是要查看同级的就不添加媒体权限限制
            if(empty($is_show_tj) && ($permission_media == 1 || $permission_media == 2)){
                switch ($permission_media){
                    case 1:
                        $exp = 'IN';
                        break;
                    case 2:
                        $exp = 'NOT IN';
                        break;
                }
                $media_ids = M('tmedia_temp')

                    ->where(['ftype'=>['IN',[1,2]],'fcustomer'=>$system_num,'fuserid'=>session('regulatorpersonInfo.fid')])
                    ->getField('fmediaid',true);

                if(empty($media_ids)){
                    $this->ajaxReturn([
                        'code' => -1,
                        'msg'=>'无数据'
                    ]);
                }

                $map['tmedia.fid'] = [$exp,$media_ids];
                $illegal_map['tbn_illegal_ad_issue.fmedia_id'] = array($exp,$media_ids);//权限媒体
            }

            /*
             *
             * 省 2  市 4
             *
             * 1、地域排名  group by    与  left
             *
             * 2、广告类别排名 like  与 level
             *
             * 3、媒体排名 like  与 level
             *
             * 4、媒介机构 like  与 level
             *
             *
             *
             *
             * */

            //$zxs = ['110000','120000','310000','500000'];//直辖市
            /*
                        if($is_include_sub == 1){
                            switch ($region){

                                case 1:
                                    $illegal_map['tregion.fid'] = ['like',"%".substr($user_regionid,0,2)."0000"];
                                    break;

                                case 2:
                                case 3:
                                case 4:
                                    $illegal_map['tregion.fid'] = ['like',"%".substr($user_regionid,0,4)."00"];
                                    break;
                            }
                        }*/

            //线索
            switch ($region){
                case 0:
                    $map['tregion.fid'] = ['like',substr($user['regionid'],0,2).'%'];
                    $illegal_map['tregion.fid'] = ['like',substr($user['regionid'],0,2).'%'];
                    break;
                case 1://全国各省违法线索情况
                    if($re_level == 0){
                        $map["tregion.flevel"] = 1;
                        $illegal_map["tregion.flevel"] = 1;
                    }elseif($re_level == 1){
                        //是否有权限内的媒体是省级的
                        if(!empty($is_show_sj)){
                            $map['tregion.fid'] = substr($user['regionid'],0,2).'0000';
                            $illegal_map['tregion.fid'] = substr($user['regionid'],0,2).'0000';
                        }else{
                            if($is_include_sub == 1){
                                $map['tregion.fid'] = ['like',substr($user['regionid'],0,2)."%"];
                                $illegal_map['tregion.fid'] = ['like',substr($user['regionid'],0,2)."%"];
                            }else{
                                $map['tregion.fid'] = $regionid;
                                $illegal_map['tregion.fid'] = $regionid;
                            }

                        }
                    }elseif($re_level < 5 && $re_level > 1 && !empty($is_show_sj)){
                        //是否有权限内的媒体是省级的
                        $map['tregion.fid'] = substr($user['regionid'],0,2).'0000';
                        $illegal_map['tregion.fid'] = substr($user['regionid'],0,2).'0000';
                    }
                    break;
                case 2:
                    if($re_level == 0){
                        //全国各副省级市违法线索情况
                        $map["tregion.flevel"] = 2;
                        $illegal_map["tregion.flevel"] = 2;
                    }elseif($re_level == 1){
                        //本省各副省级市违法线索情况
                        if($is_include_sub == 1){
                            $map["tregion.flevel"] = ['IN','2,5'];
                            $illegal_map["tregion.flevel"] = ['IN','2,5'];
                            $map['tregion.fid'] = ['like',substr($user['regionid'],0,2)."%"];
                            $illegal_map['tregion.fid'] = ['like',substr($user['regionid'],0,2)."%"];
                        }else{
                            $map["tregion.flevel"] = 2;
                            $illegal_map["tregion.flevel"] = 2;
                            $map["tregion.fpid"] = $regionid;
                            $illegal_map["tregion.fpid"] = $regionid;
                        }
                    }elseif($re_level == 2){

                        if($is_include_sub == 1){
                            $map["tregion.flevel"] = ['IN','2,5'];
                            $illegal_map["tregion.flevel"] = ['IN','2,5'];
                            $map['tregion.fid'] = ['like',substr($user['regionid'],0,4)."%"];
                            $illegal_map['tregion.fid'] = ['like',substr($user['regionid'],0,4)."%"];
                        }else{
                            $map["tregion.flevel"] = 2;
                            $illegal_map["tregion.flevel"] = 2;
                            $map["tregion.fid"] = $regionid;
                            $illegal_map["tregion.fid"] = $regionid;
                        }
                    }
                    break;
                case 3://各计划单列市
                    if($re_level == 0){
                        //全国各计划单列市违法线索情况
                        $map["tregion.flevel"] = 3;
                        $illegal_map["tregion.flevel"] = 3;
                    }elseif($re_level == 1){
                        //本省各计划单列市违法线索情况

                        if($is_include_sub == 1){

                            $map["tregion.flevel"] = ['IN','3,5'];
                            $illegal_map["tregion.flevel"] = ['IN','3,5'];
                            $map['tregion.fid'] = ['like',substr($user['regionid'],0,2)."%"];
                            $illegal_map['tregion.fid'] = ['like',substr($user['regionid'],0,2)."%"];

                        }else{

                            $map["tregion.flevel"] = 3;
                            $illegal_map["tregion.flevel"] = 3;
                            $map["tregion.fpid"] = $regionid;
                            $illegal_map["tregion.fpid"] = $regionid;
                        }

                    }elseif($re_level == 3){
                        if($is_include_sub == 1){

                            $map["tregion.flevel"] = ['IN','3,5'];
                            $illegal_map["tregion.flevel"] = ['IN','3,5'];
                            $map['tregion.fid'] = ['like',substr($user['regionid'],0,4)."%"];
                            $illegal_map['tregion.fid'] = ['like',substr($user['regionid'],0,4)."%"];

                        }else{

                            $map["tregion.flevel"] = 3;
                            $illegal_map["tregion.flevel"] = 3;
                            $map["tregion.fid"] = $regionid;
                            $illegal_map["tregion.fid"] = $regionid;

                        }

                    }
                    break;
                case 4://各市
                    if($re_level == 0){
                        //如果是全国各市违法线索情况
                        $map["tregion.flevel"] = 4;
                        $illegal_map["tregion.flevel"] = 4;

                    }elseif($re_level == 1){
                        //本省各市违法线索情况
                        if($is_include_sub == 1){

                            $map["tregion.flevel"] = ['IN','2,3,4,5'];
                            $illegal_map["tregion.flevel"] = ['IN','2,3,4,5'];
                            $map['tregion.fid'] = ['like',substr($user['regionid'],0,2)."%"];
                            $illegal_map['tregion.fid'] = ['like',substr($user['regionid'],0,2)."%"];

                        }else{
                            $map["tregion.flevel"] = ['IN','2,3,4'];
                            $illegal_map["tregion.flevel"] = ['IN','2,3,4'];
                            $map["tregion.fpid"] = $regionid;
                            $illegal_map["tregion.fpid"] = $regionid;
                        }
                    }elseif($re_level == 4){

                        if($is_include_sub == 1){

                            $map["tregion.flevel"] = ['IN','2,3,4,5'];
                            $illegal_map["tregion.flevel"] = ['IN','2,3,4,5'];
                            $map['tregion.fid'] = ['like',substr($user['regionid'],0,4)."%"];
                            $illegal_map['tregion.fid'] = ['like',substr($user['regionid'],0,4)."%"];

                        }else{

                            $map["tregion.flevel"] = ['IN','2,3,4'];
                            $illegal_map["tregion.flevel"] = ['IN','2,3,4'];
                            $map["tregion.fid"] = $regionid;
                            $illegal_map["tregion.fid"] = $regionid;

                        }



                    }elseif(!empty($is_show_sj) && $re_level == 5){
                        //是否有权限内的媒体是市级的
                        $map['tregion.fid'] = substr($user['regionid'],0,4).'00';
                        $illegal_map['tregion.fid'] = substr($user['regionid'],0,4).'00';
                    }
                    break;
                case 5://各区县
                    if($re_level == 0){
                        //全国各区县违法线索情况
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }elseif($re_level == 1){
                        //本省各区县违法线索情况
                        $fpid = substr($regionid,0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                        //本市各区县违法线索情况
                        $fpid = substr($regionid,0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;

                    }elseif($re_level == 5){
                        $fpid = substr($regionid,0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }
                    break;
            }
        }
        else{

            $region = I('region','');//区域
            //如果没有选择非副省级省会城市
            if($region != 6){

                $map["tregion.flevel"] = ['IN','1,2,3'];//限制只看省 副省级市 计划市

                //判断是否国家局系统
                $gjj_label_media = $this->get_gjj_label_media();

                $map[$summary_table.'.fmediaid'] = array('in',$gjj_label_media);//国家局标签媒体
                $map[$summary_table.'.confirm_state'] = ['GT',0];//确认状态大于0
                $map[$summary_table.'.fcustomer'] = '100000';//客户编号
                /*违法相关*/
                $illegal_map['tbn_illegal_ad.fcustomer'] = '100000';//客户编号
                $illegal_map['tbn_illegal_ad_issue.fmedia_id'] = array('in',$gjj_label_media);//国家局标签媒体
                //是否显示发布前数据
                if($is_show_fsend == 1){
                    $illegal_map['tbn_illegal_ad_issue.fsend_status'] = ['IN',[1,2]];//
                }else{
                    $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
                }
            }else{
                $illegal_map['tbn_illegal_ad.fcustomer'] = '90100100';//客户编号
                $map[$summary_table.'.fcustomer'] = '90100100';//客户编号
                if($is_show_fsend != 1){
                    $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
                }
            }


            switch ($region){
                case 0:
                    $map["tregion.flevel"] = ['IN',[1,2,3]];
                    $illegal_map["tregion.flevel"] = ['IN',[1,2,3]];
                    break;
                case 1://省级和国家级的各省监测情况
                    /* if($re_level == 0 || $re_level == 1 || $re_level == 3){
                       $map["tregion.flevel"] = 1;
                    }*/
                    $map["tregion.flevel"] = 1;
                    $illegal_map["tregion.flevel"] = 1;
                    break;
                case 2://副省级市
                    $map["tregion.flevel"] = 2;
                    $illegal_map["tregion.flevel"] = 2;
                    break;
                case 6://非副省级省会城市
                    $map["tregion.flevel"] = 4;
                    $map["tregion.fid"] = ['like',"%0100"];
                    $illegal_map["tregion.flevel"] = 4;
                    $illegal_map["tregion.fid"] = ['like',"%0100"];
                    break;
                case 3://各计划单列市
                    $map["tregion.flevel"] = 3;
                    $illegal_map["tregion.flevel"] = 3;
                    break;
                case 4://各市
                    if($re_level == 0){
                        //国家级用户的各市监测情况
                        $map["tregion.flevel"] = 4;
                        $illegal_map["tregion.flevel"] = 4;
                    }else{
                        //市级或者省级用户的各市监测情况
                        if($re_level == 4 || $re_level == 2 || $re_level == 3){
                            $fpid = substr($user['regionid'],0,2).'0000';
                            $map["tregion.fpid"] = $fpid;
                            $illegal_map["tregion.fpid"] = $fpid;
                        }elseif($re_level == 1){
                            $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                            $illegal_map['tregion.fpid|tregion.fid'] = $user['regionid'];
                        }
                    }
                    break;
                case 5://各区县
                    if($re_level == 0){
                        //如果是全国用户
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }elseif($re_level == 1){
                        //如果是省级用户
                        $fpid = substr($user['regionid'],0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){

                        $map['tregion.fpid'] = $user['regionid'];
                        $illegal_map['tregion.fpid'] = $user['regionid'];
                        $map["tregion.flevel"] = 5;
                        $illegal_map['tregion.flevel'] = 5;
                    }elseif($re_level == 5){
                        $fpid = substr($user['regionid'],0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }
                    break;
            }
        }
        //浏览权限设置end


        //媒介类型
        $media_class = I('media_class','');

        if(!empty($media_class)){

            if($system_num == "120000" && $media_class == "13"){
                $map[$summary_table.'.fmedia_class_code'] = "999999999";//媒体
                $illegal_map['tbn_illegal_ad.fmedia_class'] = 99999999999;
            }else{
                if(!in_array(-1,$media_class)){
                    $map[$summary_table.'.fmedia_class_code'] = ['IN',$media_class];//$media_class 媒体
                    $media_class_int = [];
                    foreach ($media_class as $media_class_v){
                        $media_class_int[] = intval($media_class_v);
                    }
                    $illegal_map['tbn_illegal_ad.fmedia_class'] = ['IN',$media_class_int];
                }
            }

        }else{
            if($system_num == "120000"){
                $map[$summary_table.'.fmedia_class_code'] = ['IN',["01","02","03"]];//媒体
                $illegal_map['tbn_illegal_ad.fmedia_class'] = ['IN',[1,2,3]];
            }
        }

        //媒体细类
        /*        $media_son_class = I('media_son_class','');//媒体细类
                if($media_son_class != ''){
                    $map[$summary_table.'.fmedia_class_code'] = ['like',$media_son_class.'%'];//媒体细类
                }*/

        $ad_class = I('ad_class');//广告大类I('ad_class',"")
        if(!empty($ad_class)){
            foreach ($ad_class as $ad_class_key=>$ad_class_val){
                if($ad_class_val < 10){
                    $ad_class[$ad_class_key] = '0'.$ad_class_val;
                }
            }
            $map[$summary_table.'.fad_class_code'] = ['IN',$ad_class];//广告类别
            $illegal_map['left(tbn_illegal_ad.fad_class_code,2)'] = ['IN',$ad_class];//广告类别
        }

        $onther_class = I('onther_class','');//排名条件
        $pmzd = $onther_class;
        if($onther_class == ''){
            if($fztj == 'fad_class_code'){
                //如果是广告类别排名的话,默认条次占比排名
                $onther_class =  'fad_illegal_times';
            }else{
                //如果是其他的话,默认条次评分排名
                $onther_class =  'fad_times';
                $pmzd = 'fad_times';
            }
        }else{
            if($onther_class == 'comprehensive_score'){
                $onther_class = 'fad_count';
                $pmzd = 'fad_count';
            }
        }

        //如果是地域排名 按行政区划排名
        $onther_class_val = $onther_class;
        if($fztj == 'fregionid' && $region_order == 1){
            $onther_class =  'forder';
        }

        $data = [];//定义统计数据空数组

        //按照选定时间实例化表
        $table_model = M($summary_table);

        $id = 'fid';//定义默认表ID
        $name = 'fname';//定义默认单元名称



        //匹配分组条件，连接不同表
        $tregion_name = "tregion.fname1 as tregion_name";
        $group_field = $summary_table.'.'.$fztj;
        $group_by = $summary_table.'.'.$fztj;

        //控制字段
        if($is_manage_media != 1){
            $ill_region_join = "tregion.fid = tbn_illegal_ad.fregion_id";
            $xs_group_manage = "tbn_illegal_ad.fregion_id";
            $manage_region_join = "tregion.fid = $summary_table.fregionid";
            $manage_id = "$summary_table.fregionid";
        }else{
            $ill_region_join = "tregion.fid = tmedia.manage_region_id";
            $xs_group_manage = "tmedia.manage_region_id";
            $manage_region_join = "tregion.fid = tmedia.manage_region_id";
            $manage_id = "tmedia.manage_region_id";
        }


        switch ($fztj){
            case 'fmedia_class_code':
                $join_table = 'tmediaclass';//fmedia_class_code媒介类别表fid,
                $name = 'fclass';
                $xs_group = 'tbn_illegal_ad.fmedia_class';
                $xs_group_field = 'tbn_illegal_ad.fmedia_class';
                $hb_field = 'fmedia_class';
                break;
            case 'fmediaownerid':
                $join_table = 'tmediaowner';//fmediaownerid媒介机构表fid
                $xs_group = 'tbn_illegal_ad.fmediaownerid';
                $xs_group_field = 'tbn_illegal_ad.fmediaownerid';
                $hb_field = 'fmediaownerid';
                $menu = '媒介机构';
                break;
            case 'fregionid':
                $join_table = 'tregion';//fregionid行政区划表fid
                $hb_field = 'fregion_id';
                $field_name = 'tregion_name';
                $menu = '地域';
                if($region == 0 && $ad_pm_type == 1){
                    $xs_group_field = "CONCAT(left($xs_group_manage,2),'0000') as fregion_id";//1102
                    $xs_group = "left($xs_group_manage,2)";
                    $tregion_name = "(SELECT tregion.fname1 FROM tregion WHERE tregion.fid = CONCAT(left($manage_id,2),'0000') LIMIT 1) AS tregion_name";
                    $group_field = "CONCAT(left($manage_id,2),'0000') as fregionid,".$join_table.".forder";
                    $group_by = "left($manage_id,2)";
                }else{
                    $group_field = $summary_table.'.'.$fztj.','.$join_table.'.forder';
                    $xs_group = $xs_group_manage;
                    $xs_group_field = $xs_group_manage." as fregion_id";
                    if($is_manage_media == 1){
                        $group_field = $manage_id.' as fregionid,'.$join_table.'.forder';
                        $group_by = $manage_id;
                    }
                    if($is_include_sub == 1){
                        switch ($region){

                            case 1:
                                $xs_group_field = "CONCAT(left($xs_group_manage,2),'0000') as fregion_id";//1102
                                $xs_group = "left($xs_group_manage,2)";
                                $tregion_name = "(SELECT tregion.fname1 FROM tregion WHERE tregion.fid = CONCAT(left($manage_id,2),'0000') LIMIT 1) AS tregion_name";
                                $group_field = "CONCAT(left($manage_id,2),'0000') as fregionid,".$join_table.".forder";
                                $group_by = "left($manage_id,2)";
                                break;

                            case 2:
                            case 3:
                            case 4:
                                $xs_group_field = "CONCAT(left($xs_group_manage,4),'00') as fregion_id";//1102
                                $xs_group = "left($xs_group_manage,4)";
                                $tregion_name = "(SELECT tregion.fname1 FROM tregion WHERE tregion.fid = CONCAT(left($manage_id,4),'00') LIMIT 1) AS tregion_name";
                                $group_field = "CONCAT(left($manage_id,4),'00') as fregionid,".$join_table.".forder";
                                $group_by = "left($manage_id,4)";
                                break;
                        }
                    }
                }
                break;
            case 'fmediaid':
                $join_table = 'tmedia';//fmediaid媒介表fid
                $name = 'fmedianame';
                $xs_group = 'tbn_illegal_ad.fmedia_id';
                $xs_group_field = 'tbn_illegal_ad.fmedia_id';
                $hb_field = 'fmedia_id';
                $menu = '媒体';
                break;
            case 'fad_class_code':
                $join_table = 'tadclass';//fad_class_code广告内容类别表fcode
                $id = 'fcode';
                $name = 'fadclass';
                $xs_group = 'left(tbn_illegal_ad.fad_class_code,2)';
                $xs_group_field = 'left(tbn_illegal_ad.fad_class_code,2) as fad_class_code';
                $hb_field = 'fad_class_code';
                $menu = '广告类别';
                $field_name = 'tadclass_name';
                break;
        }
        //违法广告条次与时长
        //是否包含违法长广告时长
        if($is_show_longad != 1){
            //$illegal_map['tbn_illegal_ad.is_long_ad'] = 0;
            $illegal_map['_string'] = '(unix_timestamp(tbn_illegal_ad_issue.fendtime) - unix_timestamp(tbn_illegal_ad_issue.fstarttime) < 600 OR tbn_illegal_ad.fmedia_class = 3)';

        }


        $illegal_group = 'tbn_illegal_ad.'.$xs_group;
        $tbn_illegal_data = M('tbn_illegal_ad_issue')
            ->field("
              $xs_group_field,
              COUNT(DISTINCT tbn_illegal_ad.fsample_id) AS fad_illegal_count,
              SUM(
                UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fendtime
                ) - UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fstarttime
                )
                ) AS fad_illegal_play_len,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fstatus2 in(15,17) THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS yla,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fstatus2=16 THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS yja,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fresult like '%责令停止发布%' THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS tzfb,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fresult like '%立案%' THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS lian,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fresult like '%移送司法%' THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS yjsf,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fresult like '%其他%' THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS others,
              COUNT(1) AS fad_illegal_times
            ")
            ->where($illegal_map)
            ->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                tregion.fid = $xs_group_manage"
            )
            ->join("
                tadclass on 
                tadclass.fcode = tbn_illegal_ad.fad_class_code "
            )
            ->group($xs_group)
            ->select();
        //$date_chunk = $this->prDates($s_time,$e_time,31);//三十一天一批次
        $data = [];
        /*        foreach ($date_chunk as $date_val){
                    $map[$summary_table.'.fdate'] = ['IN',$date_val];*/

        $data = $table_model
            ->cache(true,600)
            ->field("
                $group_field,
                tmediaclass.fclass as tmediaclass_name,
                tmediaowner.fname as tmediaowner_name,
                $tregion_name,
                tregion.flevel as flevel,
                 (case when instr(tmedia.fmedianame,'（') > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,'（') -1) else tmedia.fmedianame end) as tmedia_name,
                tadclass.fadclass as tadclass_name,
                $summary_table.fmediaid as fmediaid,
                $summary_table.fmediaownerid as fmediaownerid,
                $summary_table.fmedia_class_code as fmedia_class_code,
                GROUP_CONCAT($fad_count_field SEPARATOR ',') as fad_sam_list,
                $summary_table.fad_class_code as fad_class_code,
                sum($summary_table.$fad_times_field) as fad_times,
                sum($summary_table.$play_len_field) as fad_play_len
           ")
            ->join("
                tmediaclass on 
                tmediaclass.fid = $summary_table.fmedia_class_code"
            )
            ->join("
                tmediaowner on 
                tmediaowner.fid = $summary_table.fmediaownerid"
            )
            ->join("
                tmedia on 
                tmedia.fid = $summary_table.fmediaid"
            )
            ->join("
                tregion on 
                tregion.fid = $manage_id"
            )
            ->join("
                tadclass on 
                tadclass.fcode = $summary_table.fad_class_code"
            )
            ->where($map)
            ->group($group_by)
            ->select();
        //循环计算条数
        foreach ($data as $data_key => $data_val){
            $sam_array = explode(',',$data_val['fad_sam_list']);
            $fad_count = count(array_unique($sam_array));
            unset($data[$data_key]['fad_sam_list']);
            $data[$data_key]['fad_count'] = $fad_count;//插入条数
        }


        //组合违法线索相关数据
        foreach ($tbn_illegal_data as $tbn_illegal_data_key=>$tbn_illegal_data_value){
            foreach ($data as $data_key=>$data_value){
                if($tbn_illegal_data_value[$hb_field] == $data_value[$fztj]){
                    unset($tbn_illegal_data_value[$hb_field]);
                    $data[$data_key]['counts_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_count']/$data_value['fad_count'];//条数违法率
                    $data[$data_key]['fad_illegal_count'] = $tbn_illegal_data_value['fad_illegal_count'];//违法条数
                    $data[$data_key]['times_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_times']/$data_value['fad_times'];//条次违法率
                    $data[$data_key]['lens_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_play_len']/$data_value['fad_play_len'];//时长违法率
                    $data[$data_key] = array_merge($tbn_illegal_data_value, $data[$data_key]);//合并数组
                }
            }
        }

        //循环计算
        foreach ($data as $data_key => $data_val){
            if(!isset($data_val['fad_illegal_count'])){
                $data[$data_key]['fad_illegal_count'] = 0;
            }
            if(!isset($data_val['counts_illegal_rate'])){
                $data[$data_key]['counts_illegal_rate'] = 0;
            }

            if(!isset($data_val['fad_illegal_times'])){
                $data[$data_key]['fad_illegal_times'] = 0;
            }
            if(!isset($data_val['times_illegal_rate'])){
                $data[$data_key]['times_illegal_rate'] = 0;
            }
            if(!isset($data_val['fad_illegal_play_len'])){
                $data[$data_key]['fad_illegal_play_len'] = 0;
            }
            if(!isset($data_val['lens_illegal_rate'])){
                $data[$data_key]['lens_illegal_rate'] = 0;
            }

            if(!isset($data_val['yla'])){
                $data[$data_key]['yla'] = 0;
            }
            if(!isset($data_val['yja'])){
                $data[$data_key]['yja'] = 0;
            }
            if(!isset($data_val['tzfb'])){
                $data[$data_key]['tzfb'] = 0;
            }
            if(!isset($data_val['lian'])){
                $data[$data_key]['lian'] = 0;
            }
            if(!isset($data_val['yjsf'])){
                $data[$data_key]['yjsf'] = 0;
            }
            if(!isset($data_val['others'])){
                $data[$data_key]['others'] = 0;
            }
            $all_count['fad_count'] += $data[$data_key]['fad_count'];
            $all_count['fad_illegal_count'] += $data[$data_key]['fad_illegal_count'];
            $all_count['fad_illegal_times'] += $data[$data_key]['fad_illegal_times'];
            $all_count['fad_times'] += $data[$data_key]['fad_times'];
            $all_count['fad_illegal_play_len'] += $data[$data_key]['fad_illegal_play_len'];
            $all_count['fad_play_len'] += $data[$data_key]['fad_play_len'];
            $all_count['yla'] += $data[$data_key]['yla'];
            $all_count['yja'] += $data[$data_key]['yja'];
            $all_count['tzfb'] += $data[$data_key]['tzfb'];
            $all_count['lian'] += $data[$data_key]['lian'];
            $all_count['yjsf'] += $data[$data_key]['yjsf'];
            $all_count['others'] += $data[$data_key]['others'];
        }



        //上个周期的违法广告条次与时长
        if(!empty($dates)){
            $illegal_map['tbn_illegal_ad_issue.fissue_date'] = [['IN',$dates],['BETWEEN',[$last_s_time,$last_e_time]],'AND'];
        }else{
            $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$last_s_time,$last_e_time]];
        }
        $tbn_prv_illegal_data = M('tbn_illegal_ad_issue')
            ->field("
              $xs_group_field,
              COUNT(DISTINCT tbn_illegal_ad.fsample_id) AS fad_illegal_count,
              SUM(
                UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fendtime
                ) - UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fstarttime
                )
                ) AS fad_illegal_play_len,
              COUNT(1) AS fad_illegal_times
            ")//,COUNT(DISTINCT tbn_illegal_ad.fid) AS fad_illegal_count
            ->where($illegal_map)
            ->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad.fmedia_id"
            )
            ->join("
                tregion on 
                tregion.fid = $xs_group_manage"
            )
            ->join("
                tadclass on 
                tadclass.fcode = tbn_illegal_ad.fad_class_code "
            )
            ->group($xs_group)
            ->select();

        //查询上个周期的数据
        if(!empty($dates)){
            $map[$summary_table.'.fdate'] = [['IN',$dates],['BETWEEN',[$last_s_time,$last_e_time]],'AND'];
        }else{
            $map[$summary_table.'.fdate'] = ['BETWEEN',[$last_s_time,$last_e_time]];
        }
        $illegal_prv_ad_mod = $table_model
            ->cache(true,600)
            ->field("
                $group_field,
                GROUP_CONCAT($fad_count_field SEPARATOR ',') as fad_sam_list,
                $summary_table.fad_class_code as fad_class_code,
                sum($summary_table.fad_times) as fad_times,
                sum($summary_table.$play_len_field) as fad_play_len
            ")
            ->join("
            tmediaclass on 
            tmediaclass.fid = $summary_table.fmedia_class_code"
            )
            ->join("
            tmediaowner on 
            tmediaowner.fid = $summary_table.fmediaownerid"
            )
            ->join("
            tmedia on 
            tmedia.fid = $summary_table.fmediaid"
            )
            ->join("
            tregion on 
            tregion.fid = $manage_id"
            )
            ->join("
            tadclass on 
            tadclass.fcode = $summary_table.fad_class_code"
            )
            ->where($map)
            ->group($group_by)
            ->select();

        //循环计算条数
        foreach ($illegal_prv_ad_mod as $illegal_prv_ad_mod_key => $illegal_prv_ad_mod_val){
            $sam_array = explode(',',$tbn_prv_illegal_data_val['fad_sam_list']);
            $fad_count = count(array_unique($sam_array));
            unset($illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_sam_list']);
            $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_count'] = $fad_count;//插入条数
        }

        //组合上个周期违法线索相关数据
        foreach ($tbn_prv_illegal_data as $tbn_illegal_data_key=>$tbn_illegal_data_value){
            foreach ($illegal_prv_ad_mod as $data_key=>$data_value){
                if($tbn_illegal_data_value[$hb_field] == $data_value[$fztj]){
                    unset($tbn_illegal_data_value[$hb_field]);
                    $illegal_prv_ad_mod[$data_key]['counts_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_count']/$data_value['fad_count'];//条数违法率
                    $illegal_prv_ad_mod[$data_key]['fad_illegal_count'] = $tbn_illegal_data_value['fad_illegal_count'];//违法条数
                    $illegal_prv_ad_mod[$data_key]['times_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_times']/$data_value['fad_times'];//合并数组
                    $illegal_prv_ad_mod[$data_key]['lens_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_play_len']/$data_value['fad_play_len'];//合并数组
                    $illegal_prv_ad_mod[$data_key] = array_merge($tbn_illegal_data_value, $illegal_prv_ad_mod[$data_key]);//合并数组
                }
            }
        }
        foreach ($illegal_prv_ad_mod as $illegal_prv_ad_mod_key => $illegal_prv_ad_mod_val){
            if(!isset($illegal_prv_ad_mod_val['fad_illegal_count'])){

                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_count'] = 0;
            }
            if(!isset($illegal_prv_ad_mod_val['counts_illegal_rate'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['counts_illegal_rate'] = 0;
            }

            if(!isset($illegal_prv_ad_mod_val['fad_illegal_times'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_times'] = 0;
            }

            if(!isset($illegal_prv_ad_mod_val['times_illegal_rate'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['times_illegal_rate'] = 0;
            }
            if(!isset($illegal_prv_ad_mod_val['fad_illegal_play_len'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_play_len'] = 0;
            }
            if(!isset($illegal_prv_ad_mod_val['fad_play_len'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_play_len'] = 0;
            }
            $prv_all_count['fad_count'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_count'];
            $prv_all_count['fad_illegal_count'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_count'];

            $prv_all_count['fad_illegal_times'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_times'];
            $prv_all_count['fad_times'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_times'];
            $prv_all_count['fad_illegal_play_len'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_play_len'];
            $prv_all_count['fad_play_len'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_play_len'];
        }
        //对比上个周期的数据
        foreach ($illegal_prv_ad_mod as $illegal_prv_ad_val) {
            foreach ($data as $data_key=>$data_value) {

                if ($illegal_prv_ad_val[$fztj] == $data_value[$fztj]) {

                    if (!$illegal_prv_ad_val['fad_illegal_count'] || ($data_value['fad_illegal_count'] - $illegal_prv_ad_val['fad_illegal_count']) == 0) {
                        $data[$data_key]['prv_fad_illegal_count'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_count = round(($data_value['fad_illegal_count'] - $illegal_prv_ad_val['fad_illegal_count']) / $illegal_prv_ad_val['fad_illegal_count'], 4) * 100;
                        if ($prv_fad_illegal_count < 0) {
                            $data[$data_key]['prv_fad_illegal_count'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_count . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_count'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_count . '%)';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_times'] || ($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) == 0) {
                        $data[$data_key]['prv_fad_illegal_times'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_times = round(($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) / $illegal_prv_ad_val['fad_illegal_times'], 4) * 100;
                        if ($prv_fad_illegal_times < 0) {
                            $data[$data_key]['prv_fad_illegal_times'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_times . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_times'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_times . '%)';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_play_len'] || ($data_value['fad_illegal_play_len'] - $illegal_prv_ad_val['fad_illegal_play_len']) == 0) {
                        $data[$data_key]['prv_fad_illegal_play_len'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_play_len = round(($data_value['fad_illegal_play_len'] - $illegal_prv_ad_val['fad_illegal_play_len']) / $illegal_prv_ad_val['fad_illegal_play_len'], 4) * 100;

                        if ($prv_fad_illegal_play_len < 0) {
                            $data[$data_key]['prv_fad_illegal_play_len'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_play_len . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_play_len'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_play_len . '%)';
                        }
                    }
                }
            }
        }

        //如果是地域排名 按行政区划排名
        if($fztj == 'fregionid' && $region_order == 1){

            $data = $this->pxsf($data,$onther_class,false);//顺序排序

        }else{

            $data = $this->pxsf($data,$onther_class);//倒叙排序

        }

        $array_num = 0;//定义限定数量
        $data_pm = [];//定义图表数据空数组
        $data_count = count($data);//获取数据条数

        $cl_count_all = 0;//线索全部数量
        $ck_count_all = 0;//线索全部数量
        $count_all = 0;//总数
        $times=0;

        //处理排序后的数据
        foreach ($data as $data_key=>$data_value){
            $data[$data_key]['counts_illegal_rate'] = round($data_value['counts_illegal_rate'],4)*100;
            $data[$data_key]['times_illegal_rate'] = round($data_value['times_illegal_rate'],4)*100;
            $data[$data_key]['lens_illegal_rate'] = round($data_value['lens_illegal_rate'],4)*100;
            //循环获取前35条数据
            if($array_num < 35 && $array_num < count($data) && $data_value[$onther_class] > 0){
                $data_pm['name'][] = $data_value[$join_table.'_name'];
                //不同排名条件显示各自的数值
                switch ($onther_class){
                    case 'forder':
                        $data_pm['num'][] = $data[$data_key][$onther_class_val];
                        break;
                    case 'fad_illegal_times':
                        $data_pm['num'][] = $data_value['fad_illegal_times'];//违法条次
                        break;
                    case 'times_illegal_rate':
                        $data_pm['num'][] = round($data_value['times_illegal_rate'],4)*100;//条次违法率
                        break;
                    case 'fad_times':
                        $data_pm['num'][] = $data[$data_key]['fad_times'];//条次
                        break;
                    case 'fad_illegal_count':
                        $data_pm['num'][] = $data[$data_key]['fad_illegal_count'];//条数
                        break;
                    case 'counts_illegal_rate':
                        $data_pm['num'][] = $data[$data_key]['counts_illegal_rate'];//条数违法率
                        break;
                    case 'fad_count':
                        $data_pm['num'][] = $data[$data_key]['fad_count'];//条数
                        break;
                    case 'fad_play_len':
                        $data_pm['num'][] = $data[$data_key]['fad_play_len'];//时长
                        break;
                    case 'fad_illegal_play_len':
                        $data_pm['num'][] = $data_value['fad_illegal_play_len'];//违法时长
                        break;
                    case 'lens_illegal_rate':
                        $data_pm['num'][] = round($data_value['lens_illegal_rate'],4)*100;//时长违法率
                        break;
                }
                $array_num++;//fad_count  广告条次：fad_times  广告时长：fad_play_len
            }

        }

        /*线索统计模块*/
        //线索查看处理情况tbn_illegal_ad
        //已处理量
        $tbn_illegal_ad_mod = M('tbn_illegal_ad');
        if(!empty($isrelease) || $system_num == '100000'){
            $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
        }
        if(!empty($dates)){
            $illegal_map['tbn_illegal_ad_issue.fissue_date'] = [['IN',$dates],['BETWEEN',[$s_time,$e_time]],'AND'];
        }else{
            $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_time,$e_time]];
        }
        $ycl_count = $tbn_illegal_ad_mod
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as cll,
                $xs_group_field
           ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                tregion.fid = $xs_group_manage"
            )
            ->where($illegal_map)
            ->where(['tbn_illegal_ad.fstatus'=>['gt',0]])
            ->group($xs_group)
            ->select();
        //全部数量
        $cl_count = $tbn_illegal_ad_mod
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as clall,
                $xs_group_field
            ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                tregion.fid = $xs_group_manage"
            )
            ->where($illegal_map)
            ->group($xs_group)
            ->select();
        $xs_data = [];//定义线索统计情况相关数据

        //如果没有被处理的数据
        if(empty($ycl_count)){
            foreach ($cl_count as $cl_val){
                $xs_data[$cl_val[$hb_field]]['cll'] = '0%';
                $xs_data[$cl_val[$hb_field]][$hb_field] = $cl_val[$hb_field];
                $xs_data[$cl_val[$hb_field]]['cl_count'] = 0;
                $xs_data[$cl_val[$hb_field]]['all_count'] = $cl_val['clall'];
            }
        }else{
            //循环获取
            foreach ($ycl_count as $ycl_val){
                foreach ($cl_count as $cl_val){
                    if($ycl_val[$hb_field] == $cl_val[$hb_field]){
                        if($ycl_val['cll'] != 0){
                            $xs_data[$cl_val[$hb_field]][$hb_field] = $cl_val[$hb_field];
                            $xs_data[$cl_val[$hb_field]]['cll'] = (round($ycl_val['cll']/$cl_val['clall'],4)*100).'%';
                            $xs_data[$cl_val[$hb_field]]['cl_count'] = $ycl_val['cll'];
                        }else{
                            $xs_data[$cl_val[$hb_field]]['cll'] = '0%';
                            $xs_data[$cl_val[$hb_field]][$hb_field] = $cl_val[$hb_field];
                            $xs_data[$cl_val[$hb_field]]['cl_count'] = 0;
                        }
                        $xs_data[$cl_val[$hb_field]]['all_count'] = $cl_val['clall'];
                    }
                }
            }
        }
        //已查看量
        $yck_count = $tbn_illegal_ad_mod
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as ckl,
                $xs_group_field
            ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                tregion.fid = $xs_group_manage"
            )
            ->where($illegal_map)
            ->where(['tbn_illegal_ad.fview_status'=>['gt',0]])
            ->group($xs_group)
            ->select();

        //全部数量
        $ck_count = $tbn_illegal_ad_mod
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as ckall,
                $xs_group_field
            ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                tregion.fid = $xs_group_manage"
            )
            ->where($illegal_map)
            ->group($xs_group)
            ->select();

        //如果没有一查看的数量
        if(empty($yck_count)){
            foreach ($ck_count as $ck_val){
                $xs_data[$ck_val[$hb_field]]['ckl'] = '0%';
                $xs_data[$ck_val[$hb_field]]['ck_count'] = 0;
                $xs_data[$ck_val[$hb_field]]['all_count'] = $ck_val['ckall'];
            }
        }else{
            //循环获取
            foreach ($yck_count as $yck_val){
                foreach ($ck_count as $ck_val){
                    if($yck_val[$hb_field] == $ck_val[$hb_field]){
                        if($yck_val['ckl'] != 0){
                            $xs_data[$ck_val[$hb_field]]['ck_count'] = $yck_val['ckl'];
                        }else{
                            $xs_data[$ck_val[$hb_field]]['ck_count'] = 0;
                        }
                        $xs_data[$ck_val[$hb_field]]['ckl'] = (round($yck_val['ckl']/$ck_val['ckall'],4)*100).'%';
                    }
                    $xs_data[$ck_val[$hb_field]]['all_count'] = $ck_val['ckall'];
                }
            }//组建案件查看与处理情况
        }


        //组合线索相关数据
        foreach ($xs_data as $xs_key=>$xs_value){
            foreach ($data as $data_key=>$data_value){
                if($xs_key == $data_value[$fztj]){
                    unset($xs_value[$hb_field]);
                    $cl_count_all += $xs_value['cl_count'];
                    $ck_count_all += $xs_value['ck_count'];
                    $count_all += $xs_value['all_count'];
                    $data[$data_key] = array_merge($xs_value, $data[$data_key]);//合并数组
                }
            }
        }

        //组合广告类别百分比图表数据
        $ad_class_num = 0;
        if($fztj == 'fad_class_code') {
            $data_pm = [];
            $data = $this->pxsf($data, $onther_class);//排序
            foreach ($data as $ad_class_key=>$ad_class_value){
                if($ad_class_value[$onther_class] != 0){
                    $data_pm['data'][] = [
                        'name' => $ad_class_value[$join_table.'_name'],
                        'value' => $ad_class_value[$onther_class]
                    ];
                    $data_pm['name'][] = $ad_class_value[$join_table.'_name'];
                }
            }
        }

        $all_prv_fad_illegal_count = round(($all_count['fad_illegal_count'] - $prv_all_count['fad_illegal_count']) / $prv_all_count['fad_illegal_count'], 4) * 100;
        $all_prv_fad_illegal_play_len = round(($all_count['fad_illegal_play_len'] - $prv_all_count['fad_illegal_play_len']) / $prv_all_count['fad_illegal_play_len'], 4) * 100;
        $all_prv_fad_illegal_times = round(($all_count['fad_illegal_times'] - $prv_all_count['fad_illegal_times']) / $prv_all_count['fad_illegal_times'], 4) * 100;

        $counts_illegal_rate = round($all_count['fad_illegal_count']/$all_count['fad_count'],4)*100;
        $times_illegal_rate = round($all_count['fad_illegal_times']/$all_count['fad_times'],4)*100;
        $lens_illegal_rate = round($all_count['fad_illegal_play_len']/$all_count['fad_play_len'],4)*100;

        $prv_counts_illegal_rate = round($prv_all_count['fad_illegal_count']/$prv_all_count['fad_count'],4)*100;
        $prv_times_illegal_rate = round($prv_all_count['fad_illegal_times']/$prv_all_count['fad_times'],4)*100;
        $prv_lens_illegal_rate = round($prv_all_count['fad_illegal_play_len']/$prv_all_count['fad_play_len'],4)*100;

        if ($all_prv_fad_illegal_count < 0) {
            $all_prv_fad_illegal_count = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_count . '%)';
        } else {
            if($all_prv_fad_illegal_count == 0){
                $all_prv_fad_illegal_count = '(0.00%)';

            }else{
                $all_prv_fad_illegal_count = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_count . '%)';
            }
        }

        if ($all_prv_fad_illegal_play_len < 0) {
            $all_prv_fad_illegal_play_len = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_play_len . '%)';
        } else {
            if($all_prv_fad_illegal_play_len == 0){
                $all_prv_fad_illegal_play_len = '(0.00%)';

            }else{
                $all_prv_fad_illegal_play_len = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_play_len . '%)';
            }
        }

        if ($all_prv_fad_illegal_times < 0) {
            $all_prv_fad_illegal_times = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_times . '%)';
        } else {
            if($all_prv_fad_illegal_times == 0){
                $all_prv_fad_illegal_times = '(0.00%)';

            }else{
                $all_prv_fad_illegal_times = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_times . '%)';
            }
        }

        $compar_fad_count = $all_count['fad_count'] - $prv_all_count['fad_count'];
        if ($compar_fad_count < 0) {
            $compar_fad_count = '减少'.($compar_fad_count*-1);
        } else {
            if($compar_fad_count == 0){
                $compar_fad_count = '无变化';

            }else{
                $compar_fad_count = '增加'.$compar_fad_count;
            }
        }
        $compar_fad_illegal_count = $all_count['fad_illegal_count'] - $prv_all_count['fad_illegal_count'];
        if ($compar_fad_illegal_count < 0) {
            $compar_fad_illegal_count = '减少'.($compar_fad_illegal_count*-1);
        } else {
            if($compar_fad_illegal_count == 0){
                $compar_fad_illegal_count = '无变化';

            }else{
                $compar_fad_illegal_count = '增加'.$compar_fad_illegal_count;
            }
        }
        $compar_illegal_count_rate = $counts_illegal_rate - $prv_counts_illegal_rate;
        if ($compar_illegal_count_rate < 0) {
            $compar_illegal_count_rate = '下降'.($compar_illegal_count_rate*-1).'%';
        } else {
            if($compar_illegal_count_rate == 0){
                $compar_illegal_count_rate = '无变化';

            }else{
                $compar_illegal_count_rate = '上升'.$compar_illegal_count_rate.'%';
            }
        }
        $compar_fad_times = $all_count['fad_times'] - $prv_all_count['fad_times'];
        if ($compar_fad_times < 0) {
            $compar_fad_times = '减少'.($compar_fad_times*-1);
        } else {
            if($compar_fad_times == 0){
                $compar_fad_times = '无变化';

            }else{
                $compar_fad_times = '增加'.$compar_fad_times;
            }
        }
        $compar_fad_illegal_times = $all_count['fad_illegal_times'] - $prv_all_count['fad_illegal_times'];
        if ($compar_fad_illegal_times < 0) {
            $compar_fad_illegal_times = '减少'.($compar_fad_illegal_times*-1);
        } else {
            if($compar_fad_illegal_times == 0){
                $compar_fad_illegal_times = '无变化';

            }else{
                $compar_fad_illegal_times = '增加'.$compar_fad_illegal_times;
            }
        }
        $compar_illegal_times_rate = $times_illegal_rate - $prv_times_illegal_rate;
        if ($compar_illegal_times_rate < 0) {
            $compar_illegal_times_rate = '下降'.($compar_illegal_times_rate*-1).'%';
        } else {
            if($compar_illegal_times_rate == 0){
                $compar_illegal_times_rate = '无变化';
            }else{
                $compar_illegal_times_rate = '上升'.$compar_illegal_times_rate.'%';
            }
        }
        $compar_fad_play_len = $all_count['fad_play_len'] - $prv_all_count['fad_play_len'];
        if ($compar_fad_play_len < 0) {
            $compar_fad_play_len = '减少'.($compar_fad_play_len*-1);
        } else {
            if($compar_fad_play_len == 0){
                $compar_fad_play_len = '无变化';

            }else{
                $compar_fad_play_len = '增加'.$compar_fad_play_len;
            }
        }
        $compar_fad_illegal_play_len = $all_count['fad_illegal_play_len'] - $prv_all_count['fad_illegal_play_len'];
        if ($compar_fad_illegal_play_len < 0) {
            $compar_fad_illegal_play_len = '减少'.($compar_fad_illegal_play_len*-1);
        } else {
            if($compar_fad_illegal_play_len == 0){
                $compar_fad_illegal_play_len = '无变化';

            }else{
                $compar_fad_illegal_play_len = '增加'.$compar_fad_illegal_play_len;
            }
        }
        $compar_illegal_play_len_rate = $lens_illegal_rate - $prv_lens_illegal_rate;
        if ($compar_illegal_play_len_rate < 0) {
            $compar_illegal_play_len_rate = '下降'.($compar_illegal_play_len_rate*-1).'%';
        } else {
            if($compar_illegal_play_len_rate == 0){
                $compar_illegal_play_len_rate = '无变化';
            }else{
                $compar_illegal_play_len_rate = '上升'.$compar_illegal_play_len_rate.'%';
            }
        }
        $compar_prv_com_score = round($all_count['com_score'] - $prv_all_count['prv_com_score'],2);
        if ($compar_prv_com_score < 0) {
            $compar_prv_com_score = '减少'.($compar_prv_com_score*-1);
        } else {
            if($compar_prv_com_score == 0){
                $compar_prv_com_score = '无变化';

            }else{
                $compar_prv_com_score = '增加'.$compar_prv_com_score;
            }
        }

        /*
        已立案 yla  fstatus = 10 and fstatus2 in(15,17)
        已结案 yja fstatus = 10 and fstatus2=16
        责令停止发布 tzfb fstatus = 10 and fresult like '%责令停止发布%'
        立案 lian fstatus = 10 and fresult like '%立案%'
        移交司法机关 yjsf fstatus = 10 and fresult like '%移送司法%'
        其他  others fstatus = 10 and fresult like '%其他%'
         * */

        //统计放在最后一条$all_count     $prv_all_count
        $data[] = [
            'all_xs_count' =>$count_all,
            'titlename'=>'全部',
            "cll"=>(round($cl_count_all/$count_all,4)*100).'%',
            "cl_count" =>$cl_count_all,
            "ck_count"=>$ck_count_all,
            "yla"=>$all_count['yla'],
            "yja"=>$all_count['yja'],
            "tzfb"=>$all_count['tzfb'],
            "lian"=>$all_count['lian'],
            "yjsf"=>$all_count['yjsf'],
            "others"=>$all_count['others'],
            "ckl"=>(round($ck_count_all/$count_all,4)*100).'%',
            "prv_fad_illegal_count"=>$all_prv_fad_illegal_count,
            "prv_fad_illegal_play_len"=>$all_prv_fad_illegal_play_len,
            "prv_fad_illegal_times"=>$all_prv_fad_illegal_times,

            'fad_count'=>$all_count['fad_count'],//广告条数
            'fad_illegal_count'=>$all_count['fad_illegal_count'],//违法广告数量
            'counts_illegal_rate'=>round($all_count['fad_illegal_count']/$all_count['fad_count'],4)*100,
            'fad_times'=>$all_count['fad_times'],//广告条次
            'fad_illegal_times'=>$all_count['fad_illegal_times'],//违法广告条次
            'times_illegal_rate'=>round($all_count['fad_illegal_times']/$all_count['fad_times'],4)*100,
            'fad_play_len'=>$all_count['fad_play_len'],//总时长
            'fad_illegal_play_len'=>$all_count['fad_illegal_play_len'],//违法时长
            'lens_illegal_rate'=>round($all_count['fad_illegal_play_len']/$all_count['fad_play_len'],4)*100,
            "com_score"=>round($all_count['com_score'],2),//综合分

            "compar_fad_count"=>$compar_fad_count,//对比上个周期广告条数
            "compar_fad_illegal_count"=>$compar_fad_illegal_count,//对比上个周期违法广告数量
            "compar_illegal_count_rate"=>$compar_illegal_count_rate,//对比上个周期条数违法率
            "compar_fad_times"=>$compar_fad_times,//对比上个周期广告条次
            "compar_fad_illegal_times"=>$compar_fad_illegal_times,//对比上个周期违法广告条次
            "compar_illegal_times_rate"=>$compar_illegal_times_rate,//对比上个周期条次违法率
            "compar_fad_play_len"=>$compar_fad_play_len,//对比上个周期总时长
            "compar_fad_illegal_play_len"=>$compar_fad_illegal_play_len,//对比上个周期违法时长
            "compar_illegal_play_len_rate"=>$compar_illegal_play_len_rate,//对比上个周期时长违法率
            "compar_prv_com_score"=>$compar_prv_com_score,//对比上个周期综合分
        ];

        //是否展示条数相关
        /*        if(!$isshow_count){
                    foreach ($data as $data_key=>$data_val){
                        unset($data[$data_key]['fad_count']);
                        unset($data[$data_key]['fad_illegal_count']);
                        unset($data[$data_key]['counts_illegal_rate']);
                    }
                }*/

        $finally_data = [
            'count'=> count($data)-1,
            'data_pm'=>$data_pm,//图表数据
            'data_list'=>$data//列表数据
        ];

        if($is_out_report == 1){

            if($is_have_count == 1){
                //如果是线索菜单并且是点击了导出按钮
                $objPHPExcel = new \PHPExcel();
                $objPHPExcel
                    ->getProperties()  //获得文件属性对象，给下文提供设置资源
                    ->setCreator( "MaartenBalliauw")             //设置文件的创建者
                    ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
                    ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
                    ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
                    ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
                    ->setKeywords( "office 2007 openxmlphp")        //设置标记
                    ->setCategory( "Test resultfile");                //设置类别

                $sheet = $objPHPExcel->setActiveSheetIndex(0);
                //分组导出数据1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
                switch ($fztj){
                    case 'fmediaid':
                        $sheet ->mergeCells("D1:F1");
                        $sheet ->mergeCells("G1:I1");
                        $sheet ->mergeCells("J1:L1");
                        $sheet ->mergeCells("M1:P1");
                        $sheet ->setCellValue('A1','');
                        $sheet ->setCellValue('B1','');
                        $sheet ->setCellValue('C1','');
                        $sheet ->setCellValue('D1','');
                        $sheet ->setCellValue('E1','广告条数');
                        $sheet ->setCellValue('H1','广告条次');
                        $sheet ->setCellValue('K1','广告时长');
                        $sheet ->setCellValue('N1','线索查看及处理情况');

                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','媒体所在地');
                        $sheet ->setCellValue('C2','媒体名称');
                        $sheet ->setCellValue('D2','总条数');
                        $sheet ->setCellValue('E2','违法条数');
                        $sheet ->setCellValue('F2','条数违法率');
                        $sheet ->setCellValue('G2','总条次');
                        $sheet ->setCellValue('H2','违法条次');
                        $sheet ->setCellValue('I2','条次违法率');
                        $sheet ->setCellValue('J2','总时长');
                        $sheet ->setCellValue('K2','违法时长');
                        $sheet ->setCellValue('L2','时长违法率');
                        $sheet ->setCellValue('M2','查看量');
                        $sheet ->setCellValue('N2','查看率');
                        $sheet ->setCellValue('O2','处理量');
                        $sheet ->setCellValue('P2','处理率');

                        foreach ($data as $key => $value) {

                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                                $sheet ->setCellValue('C'.($key+3),'全部媒体');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                                $sheet ->setCellValue('C'.($key+3),$value['tmedia_name']);
                            }

                            $sheet ->setCellValue('D'.($key+3),$value['fad_count']);
                            $sheet ->setCellValue('E'.($key+3),$value['fad_illegal_count']);
                            $sheet ->setCellValue('F'.($key+3),$value['counts_illegal_rate'].'%');
                            $sheet ->setCellValue('G'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('H'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('I'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('j'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('K'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('L'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('M'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('N'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('O'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('P'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fregionid':
                        $sheet ->mergeCells("C1:E1");
                        $sheet ->mergeCells("F1:H1");
                        $sheet ->mergeCells("I1:K1");
                        $sheet ->mergeCells("L1:O1");
                        $sheet ->setCellValue('A1','');
                        $sheet ->setCellValue('B1','');
                        $sheet ->setCellValue('C1','广告条数');
                        $sheet ->setCellValue('F1','广告条次');
                        $sheet ->setCellValue('I1','广告时长');
                        $sheet ->setCellValue('L1','线索查看及处理情况');
                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','地域名称');
                        $sheet ->setCellValue('C2','总条数');
                        $sheet ->setCellValue('D2','违法条数');
                        $sheet ->setCellValue('E2','条数违法率');
                        $sheet ->setCellValue('F2','总条次');
                        $sheet ->setCellValue('G2','违法条次');
                        $sheet ->setCellValue('H2','条次违法率');
                        $sheet ->setCellValue('I2','总时长');
                        $sheet ->setCellValue('J2','违法时长');
                        $sheet ->setCellValue('K2','时长违法率');
                        $sheet ->setCellValue('L2','查看量');
                        $sheet ->setCellValue('M2','查看率');
                        $sheet ->setCellValue('N2','处理量');
                        $sheet ->setCellValue('O2','处理率');

                        foreach ($data as $key => $value) {


                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                            }

                            $sheet ->setCellValue('C'.($key+3),$value['fad_count']);
                            $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_count']);
                            $sheet ->setCellValue('E'.($key+3),$value['counts_illegal_rate'].'%');
                            $sheet ->setCellValue('F'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('H'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('I'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('J'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('K'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('L'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('M'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('N'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('O'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fmediaownerid':
                        $sheet ->mergeCells("E1:G1");
                        $sheet ->mergeCells("H1:J1");
                        $sheet ->mergeCells("K1:M1");
                        $sheet ->mergeCells("N1:Q1");
                        $sheet ->setCellValue('A1','');
                        $sheet ->setCellValue('B1','');
                        $sheet ->setCellValue('C1','');
                        $sheet ->setCellValue('D1','');
                        $sheet ->setCellValue('E1','广告条数');
                        $sheet ->setCellValue('H1','广告条次');
                        $sheet ->setCellValue('K1','广告时长');
                        $sheet ->setCellValue('N1','线索查看及处理情况');

                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','媒介机构所在地');
                        $sheet ->setCellValue('C2','媒介机构名称');
                        $sheet ->setCellValue('D2','媒介类型');
                        $sheet ->setCellValue('E2','总条数');
                        $sheet ->setCellValue('F2','违法条数');
                        $sheet ->setCellValue('G2','条数违法率');
                        $sheet ->setCellValue('H2','总条次');
                        $sheet ->setCellValue('I2','违法条次');
                        $sheet ->setCellValue('J2','条次违法率');
                        $sheet ->setCellValue('K2','总时长');
                        $sheet ->setCellValue('L2','违法时长');
                        $sheet ->setCellValue('M2','时长违法率');
                        $sheet ->setCellValue('N2','查看量');
                        $sheet ->setCellValue('O2','查看率');
                        $sheet ->setCellValue('P2','处理量');
                        $sheet ->setCellValue('Q2','处理率');

                        foreach ($data as $key => $value) {

                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                                $sheet ->setCellValue('C'.($key+3),'全部媒介机构');
                                $sheet ->setCellValue('D'.($key+3),'全部类型');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                                $sheet ->setCellValue('C'.($key+3),$value['tmediaowner_name']);
                                $sheet ->setCellValue('D'.($key+3),$value['tmediaclass_name']);
                            }

                            $sheet ->setCellValue('E'.($key+3),$value['fad_count']);
                            $sheet ->setCellValue('F'.($key+3),$value['fad_illegal_count']);
                            $sheet ->setCellValue('G'.($key+3),$value['counts_illegal_rate'].'%');
                            $sheet ->setCellValue('H'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('I'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('J'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('K'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('L'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('M'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('N'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('O'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('P'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('Q'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fad_class_code':
                        $sheet ->mergeCells("C1:E1");
                        $sheet ->mergeCells("F1:H1");
                        $sheet ->mergeCells("I1:K1");
                        $sheet ->mergeCells("L1:O1");
                        $sheet ->setCellValue('A1','');
                        $sheet ->setCellValue('B1','');
                        $sheet ->setCellValue('C1','广告条数');
                        $sheet ->setCellValue('F1','广告条次');
                        $sheet ->setCellValue('I1','广告时长');
                        $sheet ->setCellValue('L1','线索查看及处理情况');

                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','广告类别');
                        $sheet ->setCellValue('C2','总条数');
                        $sheet ->setCellValue('D2','违法条数');
                        $sheet ->setCellValue('E2','条数违法率');
                        $sheet ->setCellValue('F2','总条次');
                        $sheet ->setCellValue('G2','违法条次');
                        $sheet ->setCellValue('H2','条次违法率');
                        $sheet ->setCellValue('I2','总时长');
                        $sheet ->setCellValue('J2','违法时长');
                        $sheet ->setCellValue('K2','时长违法率');
                        $sheet ->setCellValue('L2','查看量');
                        $sheet ->setCellValue('M2','查看率');
                        $sheet ->setCellValue('N2','处理量');
                        $sheet ->setCellValue('O2','处理率');

                        foreach ($data as $key => $value) {


                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部类别');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tadclass_name']);
                            }

                            $sheet ->setCellValue('C'.($key+3),$value['fad_count']);
                            $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_count']);
                            $sheet ->setCellValue('E'.($key+3),$value['counts_illegal_rate'].'%');
                            $sheet ->setCellValue('F'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('H'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('I'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('J'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('K'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('L'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('M'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('N'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('O'.($key+3),$value['cll']);

                        }
                        break;
                }
                $objActSheet =$objPHPExcel->getActiveSheet();
                $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
                $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

                $date = date('Ymdhis');
                $savefile = './Public/Xls/'.$date.'.xlsx';
                $savefile2 = '/Public/Xls/'.$date.'.xlsx';
                $objWriter->save($savefile);
                //即时导出下载
                /*          header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                            header('Content-Disposition:attachment;filename="'.$date.'.xlsx"');
                            header('Cache-Control:max-age=0');
                            $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
                            $objWriter->save( 'php://output');*/
                $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
                unlink($savefile);//删除文件
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
            }else{
                //如果是线索菜单并且是点击了导出按钮
                $objPHPExcel = new \PHPExcel();
                $objPHPExcel
                    ->getProperties()  //获得文件属性对象，给下文提供设置资源
                    ->setCreator( "MaartenBalliauw")             //设置文件的创建者
                    ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
                    ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
                    ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
                    ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
                    ->setKeywords( "office 2007 openxmlphp")        //设置标记
                    ->setCategory( "Test resultfile");                //设置类别

                $sheet = $objPHPExcel->setActiveSheetIndex(0);
                //分组导出数据1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
                switch ($fztj){
                    case 'fmediaid':
                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','媒体所在地');
                        $sheet ->setCellValue('C2','媒体名称');
                        $sheet ->setCellValue('D2','总条次');
                        $sheet ->setCellValue('E2','违法条次');
                        $sheet ->setCellValue('F2','条次违法率');
                        $sheet ->setCellValue('G2','总时长');
                        $sheet ->setCellValue('H2','违法时长');
                        $sheet ->setCellValue('I2','时长违法率');
                        $sheet ->setCellValue('J2','查看量');
                        $sheet ->setCellValue('K2','查看率');
                        $sheet ->setCellValue('L2','处理量');
                        $sheet ->setCellValue('M2','处理率');
                        foreach ($data as $key => $value) {

                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                                $sheet ->setCellValue('C'.($key+3),'全部媒体');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                                $sheet ->setCellValue('C'.($key+3),$value['tmedia_name']);
                            }
                            $sheet ->setCellValue('D'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('E'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('F'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('G'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('H'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('I'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('J'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('K'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('L'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('M'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fregionid':
                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','地域名称');
                        $sheet ->setCellValue('C2','总条次');
                        $sheet ->setCellValue('D2','违法条次');
                        $sheet ->setCellValue('E2','条次违法率');
                        $sheet ->setCellValue('F2','总时长');
                        $sheet ->setCellValue('G2','违法时长');
                        $sheet ->setCellValue('H2','时长违法率');
                        $sheet ->setCellValue('I2','查看量');
                        $sheet ->setCellValue('J2','查看率');
                        $sheet ->setCellValue('K2','处理量');
                        $sheet ->setCellValue('L2','处理率');

                        foreach ($data as $key => $value) {


                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                            }
                            $sheet ->setCellValue('C'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('E'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('F'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('H'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('I'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('J'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('K'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('L'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fmediaownerid':
                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','媒介机构所在地');
                        $sheet ->setCellValue('C2','媒介机构名称');
                        $sheet ->setCellValue('D2','媒介类型');
                        $sheet ->setCellValue('E2','总条次');
                        $sheet ->setCellValue('F2','违法条次');
                        $sheet ->setCellValue('G2','条次违法率');
                        $sheet ->setCellValue('H2','总时长');
                        $sheet ->setCellValue('I2','违法时长');
                        $sheet ->setCellValue('J2','时长违法率');
                        $sheet ->setCellValue('K2','查看量');
                        $sheet ->setCellValue('L2','查看率');
                        $sheet ->setCellValue('M2','处理量');
                        $sheet ->setCellValue('N2','处理率');

                        foreach ($data as $key => $value) {

                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                                $sheet ->setCellValue('C'.($key+3),'全部媒介机构');
                                $sheet ->setCellValue('D'.($key+3),'全部类型');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                                $sheet ->setCellValue('C'.($key+3),$value['tmediaowner_name']);
                                $sheet ->setCellValue('D'.($key+3),$value['tmediaclass_name']);
                            }
                            $sheet ->setCellValue('E'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('F'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('G'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('H'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('I'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('J'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('K'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('L'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('M'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('N'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fad_class_code':
                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','广告类别');
                        $sheet ->setCellValue('C2','总条次');
                        $sheet ->setCellValue('D2','违法条次');
                        $sheet ->setCellValue('E2','条次违法率');
                        $sheet ->setCellValue('F2','总时长');
                        $sheet ->setCellValue('G2','违法时长');
                        $sheet ->setCellValue('H2','时长违法率');
                        $sheet ->setCellValue('I2','查看量');
                        $sheet ->setCellValue('J2','查看率');
                        $sheet ->setCellValue('K2','处理量');
                        $sheet ->setCellValue('L2','处理率');

                        foreach ($data as $key => $value) {
                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部类别');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tadclass_name']);
                            }
                            $sheet ->setCellValue('C'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('E'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('F'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('H'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('I'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('J'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('K'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('L'.($key+3),$value['cll']);
                        }
                        break;
                }
                $objActSheet =$objPHPExcel->getActiveSheet();
                $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
                $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

                $date = date('Ymdhis');
                $savefile = './Public/Xls/'.$date.'.xlsx';
                $savefile2 = '/Public/Xls/'.$date.'.xlsx';
                $objWriter->save($savefile);
                //即时导出下载
                /*          header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                            header('Content-Disposition:attachment;filename="'.$date.'.xlsx"');
                            header('Cache-Control:max-age=0');
                            $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
                            $objWriter->save( 'php://output');*/
                $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
                unlink($savefile);//删除文件
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
            }

        }elseif($is_out_report == 2){
            $this->jchz_out_word($menu,$field_name,$title,$data,$date_cycle,$s_time,$e_time,$regio_area);
        }else{
            $this->ajaxReturn($finally_data);
        }

    }

    public function illegal_ad_monitor20190710(){
        Check_QuanXian(['jchuizong','wfchachu']);
        ini_set('memory_limit','3072M');
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type: text/html; charset=utf-8");
        $user = session('regulatorpersonInfo');//获取用户信息

        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isrelease = $ALL_CONFIG['isrelease'];
        $use_open_search = $ALL_CONFIG['use_open_search'];
        $ischeck = $ALL_CONFIG['ischeck'];

        $region_order = I('region_order',1);//是否地域排名

        $is_have_self = I('is_have_self',0);//是否显示本级

        $ad_pm_type = I('ad_pm_type','');//确定是点击监测情况或者线索菜单

        $is_out_report = I('is_out_report',0);//确定是否点击导出EXCEL

        $is_have_count = I('is_have_count',0);//确定是否点击导出EXCEL包含条数

        $year = I('year',date('Y'));//默认当年

        $times_table = I('times_table','');//选表，默认月表

        $is_show_fsend = I('is_show_fsend',2);//是否发布前的数据

        $is_show_longad = I('is_show_longad',0);//是否包含长广告

        $permission_media = I('permission_media',1);//控制权限媒体

        $title = I('title','XX市');//描述标题
        $regio_area = I('regio_area','XX市各县');//选择区划文字描述

        $isfixeddata = I('isfixeddata');//是否实时数据

        $date_cycle = I('date_cycle');//日期周期文字描述


        $map = [];//定义查询条件
        if($system_num == '100000' || empty($isfixeddata)){
            $summary_table = "tbn_ad_summary_day_z";

            if($is_show_longad == 1){
                //包含长广告
                $play_len_field = 'in_long_ad_play_len';
                $fad_times_field = 'fad_times';
                $fad_count_field = 'fad_sam_list';
            }else{
                //不包含长广告
                $play_len_field = 'fad_play_len';
                $fad_times_field = 'basic_ad_times';
                $fad_count_field = 'basic_ad_sam_list';
            }
        }else{
            if($use_open_search == 1) {
                $summary_table = "tbn_ad_summary_day_v3";
                if($is_show_longad == 1){
                    //包含长广告
                    $play_len_field = 'fad_play_len_long_ad';
                    $fad_times_field = 'fad_times_long_ad';
                    $fad_count_field = 'fsam_list_long_ad';
                }else{
                    //不包含长广告
                    $play_len_field = 'fad_play_len';
                    $fad_times_field = 'fad_times';
                    $fad_count_field = 'fsam_list';
                }
            }else{
                $summary_table = "tbn_ad_summary_day";
                if($is_show_longad == 1){
                    $play_len_field = 'in_long_ad_play_len';
                    $fad_times_field = 'fad_times';
                    $fad_count_field = 'fad_sam_list';
                }else{
                    $play_len_field = 'fad_play_len';
                    $fad_times_field = 'basic_ad_times';
                    $fad_count_field = 'basic_ad_sam_list';
                }
            }

        }

        //默认表
        if($times_table == ''){
            $times_table = 'tbn_ad_summary_month';
        }
        $table_condition = I('table_condition',date('m').'-01');//根据选取的不用表展示不同的条件  //默认当月
        //根据选择的不同的时间组合不同的条件
        switch ($times_table){
            case 'tbn_ad_summary_day':
                $sedays = explode(',',$table_condition);
                $cycle_stamp = [
                    'start' =>strtotime($sedays[0]),
                    'end' =>(strtotime($sedays[1])+86400)
                ];//周期首末时间戳
                break;
            case 'tbn_ad_summary_week':
                if($table_condition < 10){
                    $table_condition = '0'.$table_condition;
                }
                $cycle_stamp = $this->weekday($year,$table_condition);//周期首末时间戳
                $cycle_stamp['end'] = $cycle_stamp['end']+86400;
                break;
            case 'tbn_ad_summary_half_month':
            case 'tbn_ad_summary_month':
            case 'tbn_ad_summary_quarter':
                $table_condition = $year.'-'.$table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                break;
            case 'tbn_ad_summary_half_year':
            case 'tbn_ad_summary_year':
                $table_condition = $table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                break;
        }

        $s_time = date('Y-m-d',$cycle_stamp['start']);
        $e_time = date('Y-m-d',($cycle_stamp['end']-86400));

        $map[$summary_table.'.fdate'] = ['BETWEEN',[$s_time,$e_time]];
        $map['_string'] = 'tmedia.fid = tmedia.main_media_id';
        $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_time,$e_time]];
        $illegal_map['tbn_illegal_ad.fstatus3'] = ['NOT IN',[30,40]];
        $s_timestamp = strtotime($s_time);//开始时间戳
        $e_timestamp = strtotime($e_time);//结束时间戳

        //获取上个周期的日期范围
        if(($e_timestamp - $s_timestamp) <= 0){
            $last_s_time = date('Y-m-d',($s_timestamp-86400));//上周期开始时间戳
            $last_e_time = $last_s_time;//上周期结束时间戳
        }else{
            $last_s_time = date('Y-m-d',($s_timestamp-($e_timestamp-$s_timestamp)-86400));
            $last_e_time = date('Y-m-d',($s_timestamp-86400));
        }
        //连表
        //分组统计条件1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
        $fztj = I('fztj','');//默认连接地域表
        if($fztj == ''){
            $fztj = 'fregionid';
        }

        //浏览权限设置start
        $re_level = $this->judgment_region($user['regionid']);//当前用户行政等级

        //是否需要抽查

        if(!empty($ischeck)){
            $spot_check_data = M('spot_check')->where(['fcustomer'=>$user['system_num']])->select();//查询抽查表数据
            //如果抽查表有数据
            if(!empty($spot_check_data)){
                $dates = [];//定义日期数组
                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                    $condition = trim($spot_check_data_val['condition']);
                    if(!empty($condition)){
                        $year_month = '';
                        $date_str = [];
                        $year_month = substr($spot_check_data_val['fmonth'],0,7);
                        if(!empty($spot_check_data_val['condition'])){
                            $date_str = explode(',',$spot_check_data_val['condition']);
                        }
                        foreach ($date_str as $date_str_val){

                            $dates[] = $year_month.'-'.$date_str_val;
                        }
                    }
                }
                $map[$summary_table.'.fdate'] = [['IN',$dates],['BETWEEN',[$s_time,$e_time]],'AND'];
                $illegal_map['tbn_illegal_ad_issue.fissue_date'] = [['IN',$dates],['BETWEEN',[$s_time,$e_time]],'AND'];
            }else{
                $map['_string'] = '1 = 0';
                $illegal_map['_string'] = '1 = 0';
            }
        }

        //如果是要看同级的用户，行政区划登记上升一级1210
        $is_show_tj = $ALL_CONFIG['is_show_tj'];//是否显示同级20181210
        $is_show_sj = $ALL_CONFIG['is_show_sj'];//是否显示省级20181210
        if(!empty($is_show_tj) && $re_level > 1){
            if($re_level == 2 || $re_level == 3 || $re_level == 4){
                $re_level = 1;
                $regionid = substr($user['regionid'],0,2).'0000';
            }else{
                $regionid = trim($user['regionid'],'0').'00';
                $re_level = $this->judgment_region($regionid);
            }
        }else{
            $regionid = $user['regionid'];
        }

        //判断$re_level用户点击哪个菜单
        //线索汇总
        if($ad_pm_type == 2){

            /*      $fcustomers = M('tbn_illegal_ad')->group('fcustomer')->getField('fcustomer',true);
                    foreach ($fcustomers as $fcustomers_key=>$fcustomers_val){
                        if($fcustomers_val == '100000'){
                            unset($fcustomers[$fcustomers_key]);
                        }
                    }
                    $illegal_map['tbn_illegal_ad.fcustomer'] = ['IN',$fcustomers];*/
            if(!empty($isrelease) || $system_num == '100000'){
                $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
            }
            $region = I('region2','');//区域
            if($region == ''){
                if($system_num == '100000' && session('regulatorpersonInfo.fregulatorlevel') != 30){
                    $map['tregion.fpid|tregion.fid'] = $regionid;
                    $map["tregion.flevel"] = ['IN','1,2,3'];
                }else{
                    if($re_level == 2 || $re_level == 3 || $re_level == 4){
                        $region = 5;
                    }else{
                        $region = $re_level;
                    }
                }
            }
            $illegal_map['tbn_illegal_ad.fcustomer'] = $system_num;
            //如果是要查看同级的就不添加媒体权限限制
            if(empty($is_show_tj) && ($permission_media == 1 || $permission_media == 2)){
                switch ($permission_media){
                    case 1:
                        $exp = 'IN';
                        break;
                    case 2:
                        $exp = 'NOT IN';
                        break;
                }
                $media_ids = M('tmedia_temp')

                    ->where(['ftype'=>['IN',[1,2]],'fcustomer'=>$system_num,'fuserid'=>session('regulatorpersonInfo.fid')])
                    ->getField('fmediaid',true);

                $map['tmedia.fid'] = [$exp,$media_ids];
                $illegal_map['tbn_illegal_ad_issue.fmedia_id'] = array($exp,$media_ids);//权限媒体
            }
            //线索
            switch ($region){
                case 0:
                case 1://全国各省违法线索情况
                    if($re_level == 0){
                        $map["tregion.flevel"] = 1;
                        $illegal_map["tregion.flevel"] = 1;
                    }elseif($re_level == 1){
                        //是否有权限内的媒体是省级的
                        if(!empty($is_show_sj)){
                            $map['tregion.fid'] = substr($user['regionid'],0,2).'0000';
                            $illegal_map['tregion.fid'] = substr($user['regionid'],0,2).'0000';
                        }else{
                            $map['tregion.fid'] = $regionid;
                            $illegal_map['tregion.fid'] = $regionid;
                        }
                    }elseif($re_level < 5 && $re_level > 1 && !empty($is_show_sj)){
                        //是否有权限内的媒体是省级的
                        $map['tregion.fid'] = substr($user['regionid'],0,2).'0000';
                        $illegal_map['tregion.fid'] = substr($user['regionid'],0,2).'0000';
                    }
                    break;
                case 2:
                    if($re_level == 0){
                        //全国各副省级市违法线索情况
                        $map["tregion.flevel"] = 2;
                        $illegal_map["tregion.flevel"] = 2;
                    }elseif($re_level == 1){
                        //本省各副省级市违法线索情况
                        if($is_have_self == 1){
                            $map["tregion.flevel"] = ['IN','1,2'];
                            $illegal_map["tregion.flevel"] = ['IN','1,2'];
                            $map['tregion.fpid|tregion.fid'] = $regionid;
                            $illegal_map['tregion.fpid|tregion.fid'] = $regionid;
                        }else{
                            $map["tregion.flevel"] = 2;
                            $illegal_map["tregion.flevel"] = 2;
                            $map["tregion.fpid"] = $regionid;
                            $illegal_map["tregion.fpid"] = $regionid;
                        }
                    }elseif($re_level == 2){
                        $map["tregion.fpid"] = $regionid;
                        $illegal_map["tregion.fpid"] = $regionid;
                    }
                    break;
                case 3://各计划单列市
                    if($re_level == 0){
                        //全国各计划单列市违法线索情况
                        $map["tregion.flevel"] = 3;
                        $illegal_map["tregion.flevel"] = 3;
                    }elseif($re_level == 1){
                        //本省各计划单列市违法线索情况
                        if($is_have_self == 1){
                            $map["tregion.flevel"] = ['IN','1,3'];
                            $illegal_map["tregion.flevel"] = ['IN','1,3'];
                            $map['tregion.fpid|tregion.fid'] = $regionid;
                            $illegal_map['tregion.fpid|tregion.fid'] = $regionid;
                        }else{
                            $map["tregion.flevel"] = 3;
                            $illegal_map["tregion.flevel"] = 3;
                            $map["tregion.fpid"] = $regionid;
                            $illegal_map["tregion.fpid"] = $regionid;
                        }

                    }elseif($re_level == 3){
                        //$fpid = substr($regionid,0,2).'0000';
                        $map["tregion.fpid"] = $regionid;
                        $illegal_map["tregion.fpid"] = $regionid;
                    }
                    break;
                case 4://各市
                    if($re_level == 0){
                        //如果是全国各市违法线索情况
                        $map["tregion.flevel"] = 4;
                        $illegal_map["tregion.flevel"] = 4;
                    }elseif($re_level == 1){
                        //本省各市违法线索情况
                        if($is_have_self == 1){
                            $map['tregion.fpid|tregion.fid'] = $regionid;
                            $illegal_map['tregion.fpid|tregion.fid'] = $regionid;
                        }else{
                            $map["tregion.fpid"] = $regionid;
                            $illegal_map["tregion.fpid"] = $regionid;
                        }
                    }elseif($re_level == 4){
                        $map["tregion.fpid"] = $regionid;
                        $illegal_map["tregion.fpid"] = $regionid;
                    }elseif(!empty($is_show_sj) && $re_level == 5){
                        //是否有权限内的媒体是市级的
                        $map['tregion.fid'] = substr($user['regionid'],0,4).'00';
                        $illegal_map['tregion.fid'] = substr($user['regionid'],0,4).'00';
                    }
                    break;
                case 5://各区县
                    if($re_level == 0){
                        //全国各区县违法线索情况
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }elseif($re_level == 1){
                        //本省各区县违法线索情况
                        $fpid = substr($regionid,0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                        //本市各区县违法线索情况
                        if($is_have_self == 1){
                            $map['tregion.fpid|tregion.fid'] = $regionid;
                            $illegal_map['tregion.fpid|tregion.fid'] = $regionid;
                        }else{
                            $fpid = substr($regionid,0,2);
                            $map["tregion.fpid"] = ['like',$fpid.'%'];
                            $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                            $map["tregion.flevel"] = 5;
                            $illegal_map["tregion.flevel"] = 5;
                        }
                    }elseif($re_level == 5){
                        $fpid = substr($regionid,0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }
                    break;
            }
        }
        else{

            $region = I('region','');//区域
            //如果没有选择非副省级省会城市
            if($region != 6){

                $map["tregion.flevel"] = ['IN','1,2,3'];//限制只看省 副省级市 计划市

                //判断是否国家局系统
                $gjj_label_media = $this->get_gjj_label_media();

                $map[$summary_table.'.fmediaid'] = array('in',$gjj_label_media);//国家局标签媒体
                $map[$summary_table.'.confirm_state'] = ['GT',0];//确认状态大于0
                $map[$summary_table.'.fcustomer'] = '100000';//客户编号
                /*违法相关*/
                $illegal_map['tbn_illegal_ad.fcustomer'] = '100000';//客户编号
                $illegal_map['tbn_illegal_ad_issue.fmedia_id'] = array('in',$gjj_label_media);//国家局标签媒体
                //是否显示发布前数据
                if($is_show_fsend == 1){
                    $illegal_map['tbn_illegal_ad_issue.fsend_status'] = ['IN',[1,2]];//
                }else{
                    $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
                }
            }else{
                $illegal_map['tbn_illegal_ad.fcustomer'] = '90100100';//客户编号
                $map[$summary_table.'.fcustomer'] = '90100100';//客户编号
                if($is_show_fsend != 1){
                    $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
                }
            }


            switch ($region){
                case 0:
                    $map["tregion.flevel"] = ['IN',[1,2,3]];
                    $illegal_map["tregion.flevel"] = ['IN',[1,2,3]];
                    break;
                case 1://省级和国家级的各省监测情况
                    /* if($re_level == 0 || $re_level == 1 || $re_level == 3){
                       $map["tregion.flevel"] = 1;
                    }*/
                    $map["tregion.flevel"] = 1;
                    $illegal_map["tregion.flevel"] = 1;
                    break;
                case 2://副省级市
                    $map["tregion.flevel"] = 2;
                    $illegal_map["tregion.flevel"] = 2;
                    break;
                case 6://非副省级省会城市
                    $map["tregion.flevel"] = 4;
                    $map["tregion.fid"] = ['like',"%0100"];
                    $illegal_map["tregion.flevel"] = 4;
                    $illegal_map["tregion.fid"] = ['like',"%0100"];
                    break;
                case 3://各计划单列市
                    $map["tregion.flevel"] = 3;
                    $illegal_map["tregion.flevel"] = 3;
                    break;
                case 4://各市
                    if($re_level == 0){
                        //国家级用户的各市监测情况
                        $map["tregion.flevel"] = 4;
                        $illegal_map["tregion.flevel"] = 4;
                    }else{
                        //市级或者省级用户的各市监测情况
                        if($re_level == 4 || $re_level == 2 || $re_level == 3){
                            $fpid = substr($user['regionid'],0,2).'0000';
                            $map["tregion.fpid"] = $fpid;
                            $illegal_map["tregion.fpid"] = $fpid;
                        }elseif($re_level == 1){
                            $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                            $illegal_map['tregion.fpid|tregion.fid'] = $user['regionid'];
                        }
                    }
                    break;
                case 5://各区县
                    if($re_level == 0){
                        //如果是全国用户
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }elseif($re_level == 1){
                        //如果是省级用户
                        $fpid = substr($user['regionid'],0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                        //$map['tregion.fpid|tregion.fid'] = $user['regionid'];
                        //$illegal_map['tregion.fpid|tregion.fid'] = $user['regionid'];
                        $map['tregion.fpid'] = $user['regionid'];
                        $illegal_map['tregion.fpid'] = $user['regionid'];
                        $map["tregion.flevel"] = 5;
                        $illegal_map['tregion.flevel'] = 5;
                    }elseif($re_level == 5){
                        $fpid = substr($user['regionid'],0,4);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }
                    break;
            }
        }
        //浏览权限设置end


        //媒介类型
        $media_class = I('media_class','');
        if($media_class != ''){

            if($system_num == "120000" && $media_class == "13"){
                $map[$summary_table.'.fmedia_class_code'] = "999999999";//媒体
                $illegal_map['tbn_illegal_ad.fmedia_class'] = 99999999999;
            }else{
                $map[$summary_table.'.fmedia_class_code'] = $media_class;//媒体
                $illegal_map['tbn_illegal_ad.fmedia_class'] = intval($media_class);
            }

        }else{
            if($system_num == "120000"){
                $map[$summary_table.'.fmedia_class_code'] = ['IN',["01","02","03"]];//媒体
                $illegal_map['tbn_illegal_ad.fmedia_class'] = ['IN',[1,2,3]];
            }
        }

        //媒体细类
        /*        $media_son_class = I('media_son_class','');//媒体细类
                if($media_son_class != ''){
                    $map[$summary_table.'.fmedia_class_code'] = ['like',$media_son_class.'%'];//媒体细类
                }*/

        $ad_class = I('ad_class');//广告大类I('ad_class',"")
        if(!empty($ad_class)){
            foreach ($ad_class as $ad_class_key=>$ad_class_val){
                if($ad_class_val < 10){
                    $ad_class[$ad_class_key] = '0'.$ad_class_val;
                }
            }
            $map[$summary_table.'.fad_class_code'] = ['IN',$ad_class];//广告类别
            $illegal_map['left(tbn_illegal_ad.fad_class_code,2)'] = ['IN',$ad_class];//广告类别
        }

        $onther_class = I('onther_class','');//排名条件
        $pmzd = $onther_class;
        if($onther_class == ''){
            if($fztj == 'fad_class_code'){
                //如果是广告类别排名的话,默认条次占比排名
                $onther_class =  'fad_illegal_times';
            }else{
                //如果是其他的话,默认条次评分排名
                $onther_class =  'fad_times';
                $pmzd = 'fad_times';
            }
        }
        //如果是地域排名 按行政区划排名
        $onther_class_val = $onther_class;
        if($fztj == 'fregionid' && $region_order == 1){
            $onther_class =  'forder';
        }

        $data = [];//定义统计数据空数组

        //按照选定时间实例化表
        $table_model = M($summary_table);

        $id = 'fid';//定义默认表ID
        $name = 'fname';//定义默认单元名称

        //匹配分组条件，连接不同表
        $tregion_name = "tregion.fname1 as tregion_name";
        $group_field = $summary_table.'.'.$fztj;
        $group_by = $summary_table.'.'.$fztj;
        switch ($fztj){
            case 'fmedia_class_code':
                $join_table = 'tmediaclass';//fmedia_class_code媒介类别表fid,
                $name = 'fclass';
                $xs_group = 'tbn_illegal_ad.fmedia_class';
                $xs_group_field = 'tbn_illegal_ad.fmedia_class';
                $hb_field = 'fmedia_class';
                break;
            case 'fmediaownerid':
                $join_table = 'tmediaowner';//fmediaownerid媒介机构表fid
                $xs_group = 'tbn_illegal_ad.fmediaownerid';
                $xs_group_field = 'tbn_illegal_ad.fmediaownerid';
                $hb_field = 'fmediaownerid';
                $menu = '媒介机构';
                break;
            case 'fregionid':
                $join_table = 'tregion';//fregionid行政区划表fid
                $hb_field = 'fregion_id';
                $field_name = 'tregion_name';
                $menu = '地域';
                if($region == 0){
                    $xs_group_field = "CONCAT(left(tbn_illegal_ad.fregion_id,2),'0000') as fregion_id";//1102
                    $xs_group = "left(tbn_illegal_ad.fregion_id,2)";
                    $tregion_name = "(SELECT tregion.fname1 FROM tregion WHERE tregion.fid = CONCAT(left(tbn_ad_summary_day_z.fregionid,2),'0000') LIMIT 1) AS tregion_name";
                    $group_field = 'CONCAT(left(tbn_ad_summary_day_z.fregionid,2),"0000") as fregionid,'.$join_table.'.forder';
                    $group_by = 'left(tbn_ad_summary_day_z.fregionid,2)';
                }else{
                    $group_field = $summary_table.'.'.$fztj.','.$join_table.'.forder';
                    $xs_group = 'tbn_illegal_ad.fregion_id';
                    $xs_group_field = 'tbn_illegal_ad.fregion_id';
                }
                break;
            case 'fmediaid':
                $join_table = 'tmedia';//fmediaid媒介表fid
                $name = 'fmedianame';
                $xs_group = 'tbn_illegal_ad.fmedia_id';
                $xs_group_field = 'tbn_illegal_ad.fmedia_id';
                $hb_field = 'fmedia_id';
                $menu = '媒体';
                break;
            case 'fad_class_code':
                $join_table = 'tadclass';//fad_class_code广告内容类别表fcode
                $id = 'fcode';
                $name = 'fadclass';
                $xs_group = 'left(tbn_illegal_ad.fad_class_code,2)';
                $xs_group_field = 'left(tbn_illegal_ad.fad_class_code,2) as fad_class_code';
                $hb_field = 'fad_class_code';
                $menu = '广告类别';
                $field_name = 'tadclass_name';
                break;
        }

        //违法广告条次与时长
        //是否包含违法长广告时长
        if($is_show_longad != 1){
            //$illegal_map['tbn_illegal_ad.is_long_ad'] = 0;
            $illegal_map['_string'] = '(unix_timestamp(tbn_illegal_ad_issue.fendtime) - unix_timestamp(tbn_illegal_ad_issue.fstarttime) < 600 OR tbn_illegal_ad.fmedia_class = 3)';

        }

        //审核通过的违法数据
        $isexamine = $ALL_CONFIG['isexamine'];
        if(in_array($isexamine, [10,20,30,40,50])){
            $illegal_map['tbn_illegal_ad.fexamine'] = 10;
        }

        $illegal_group = 'tbn_illegal_ad.'.$xs_group;
        $tbn_illegal_data = M('tbn_illegal_ad_issue')
            ->field("
              $xs_group_field,
              COUNT(DISTINCT tbn_illegal_ad.fsample_id) AS fad_illegal_count,
              SUM(
                UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fendtime
                ) - UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fstarttime
                )
                ) AS fad_illegal_play_len,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fstatus2 in(15,17) THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS yla,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fstatus2=16 THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS yja,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fresult like '%责令停止发布%' THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS tzfb,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fresult like '%立案%' THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS lian,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fresult like '%移送司法%' THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS yjsf,
                 COUNT(
                    DISTINCT (
                        CASE
                        WHEN fstatus = 10 and fresult like '%其他%' THEN
                            tbn_illegal_ad.fid
                        END
                    )
                ) AS others,
              COUNT(1) AS fad_illegal_times
            ")
            ->where($illegal_map)
            ->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tadclass on 
                tadclass.fcode = tbn_illegal_ad.fad_class_code "
            )
            ->group($xs_group)
            ->select();
        //$date_chunk = $this->prDates($s_time,$e_time,31);//三十一天一批次
        $data = [];
        /*        foreach ($date_chunk as $date_val){
                    $map[$summary_table.'.fdate'] = ['IN',$date_val];*/

        $tj_ad_mod = $table_model
            ->cache(true,600)
            ->field("
                $group_field,
                tmediaclass.fclass as tmediaclass_name,
                tmediaowner.fname as tmediaowner_name,
                $tregion_name,
                tregion.flevel as flevel,
                 (case when instr(tmedia.fmedianame,'（') > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,'（') -1) else tmedia.fmedianame end) as tmedia_name,
                tadclass.fadclass as tadclass_name,
                $summary_table.fmediaid as fmediaid,
                $summary_table.fmediaownerid as fmediaownerid,
                $summary_table.fmedia_class_code as fmedia_class_code,
                GROUP_CONCAT($fad_count_field SEPARATOR ',') as fad_sam_list,
                $summary_table.fad_class_code as fad_class_code,
                sum($summary_table.$fad_times_field) as fad_times,
                sum($summary_table.$play_len_field) as fad_play_len
           ")
            ->join("
                tmediaclass on 
                tmediaclass.fid = $summary_table.fmedia_class_code"
            )
            ->join("
                tmediaowner on 
                tmediaowner.fid = $summary_table.fmediaownerid"
            )
            ->join("
                tregion on 
                tregion.fid = $summary_table.fregionid"
            )
            ->join("
                tmedia on 
                tmedia.fid = $summary_table.fmediaid"
            )
            ->join("
                tadclass on 
                tadclass.fcode = $summary_table.fad_class_code"
            )
            ->where($map)
            ->group($group_by)->fetchSql(true)
            ->select();

        $data = $tj_ad_mod;

        //循环计算条数
        foreach ($data as $data_key => $data_val){
            $sam_array = explode(',',$data_val['fad_sam_list']);
            $fad_count = count(array_unique($sam_array));
            unset($data[$data_key]['fad_sam_list']);
            $data[$data_key]['fad_count'] = $fad_count;//插入条数
        }

        //循环计算违法条数
        /*        foreach ($tbn_illegal_data as $tbn_illegal_data_key => $tbn_illegal_data_val){
                    $sam_array = explode(',',$tbn_illegal_data_val['fsample_ids']);
                    $fad_illegal_count = count(array_unique($sam_array));
                    unset($tbn_illegal_data[$tbn_illegal_data_key]['fsample_ids']);
                    $tbn_illegal_data[$tbn_illegal_data_key]['fad_illegal_count'] = $fad_illegal_count;//插入违法条数
                }*/
        //组合违法线索相关数据
        foreach ($tbn_illegal_data as $tbn_illegal_data_key=>$tbn_illegal_data_value){
            foreach ($data as $data_key=>$data_value){
                if($tbn_illegal_data_value[$hb_field] == $data_value[$fztj]){
                    unset($tbn_illegal_data_value[$hb_field]);
                    $data[$data_key]['counts_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_count']/$data_value['fad_count'];//条数违法率
                    $data[$data_key]['fad_illegal_count'] = $tbn_illegal_data_value['fad_illegal_count'];//违法条数
                    $data[$data_key]['times_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_times']/$data_value['fad_times'];//条次违法率
                    $data[$data_key]['lens_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_play_len']/$data_value['fad_play_len'];//时长违法率
                    $data[$data_key] = array_merge($tbn_illegal_data_value, $data[$data_key]);//合并数组
                }
            }
        }

        //循环计算
        foreach ($data as $data_key => $data_val){
            if(!isset($data_val['fad_illegal_count'])){
                $data[$data_key]['fad_illegal_count'] = 0;
            }
            if(!isset($data_val['counts_illegal_rate'])){
                $data[$data_key]['counts_illegal_rate'] = 0;
            }

            if(!isset($data_val['fad_illegal_times'])){
                $data[$data_key]['fad_illegal_times'] = 0;
            }
            if(!isset($data_val['times_illegal_rate'])){
                $data[$data_key]['times_illegal_rate'] = 0;
            }
            if(!isset($data_val['fad_illegal_play_len'])){
                $data[$data_key]['fad_illegal_play_len'] = 0;
            }
            if(!isset($data_val['lens_illegal_rate'])){
                $data[$data_key]['lens_illegal_rate'] = 0;
            }

            if(!isset($data_val['yla'])){
                $data[$data_key]['yla'] = 0;
            }
            if(!isset($data_val['yja'])){
                $data[$data_key]['yja'] = 0;
            }
            if(!isset($data_val['tzfb'])){
                $data[$data_key]['tzfb'] = 0;
            }
            if(!isset($data_val['lian'])){
                $data[$data_key]['lian'] = 0;
            }
            if(!isset($data_val['yjsf'])){
                $data[$data_key]['yjsf'] = 0;
            }
            if(!isset($data_val['others'])){
                $data[$data_key]['others'] = 0;
            }
            $all_count['fad_count'] += $data[$data_key]['fad_count'];
            $all_count['fad_illegal_count'] += $data[$data_key]['fad_illegal_count'];
            $all_count['fad_illegal_times'] += $data[$data_key]['fad_illegal_times'];
            $all_count['fad_times'] += $data[$data_key]['fad_times'];
            $all_count['fad_illegal_play_len'] += $data[$data_key]['fad_illegal_play_len'];
            $all_count['fad_play_len'] += $data[$data_key]['fad_play_len'];
            $all_count['yla'] += $data[$data_key]['yla'];
            $all_count['yja'] += $data[$data_key]['yja'];
            $all_count['tzfb'] += $data[$data_key]['tzfb'];
            $all_count['lian'] += $data[$data_key]['lian'];
            $all_count['yjsf'] += $data[$data_key]['yjsf'];
            $all_count['others'] += $data[$data_key]['others'];
        }



        //上个周期的违法广告条次与时长
        if(!empty($dates)){
            $illegal_map['tbn_illegal_ad_issue.fissue_date'] = [['IN',$dates],['BETWEEN',[$last_s_time,$last_e_time]],'AND'];
        }else{
            $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$last_s_time,$last_e_time]];
        }
        $tbn_prv_illegal_data = M('tbn_illegal_ad_issue')
            ->field("
              $xs_group_field,
              COUNT(DISTINCT tbn_illegal_ad.fsample_id) AS fad_illegal_count,
              SUM(
                UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fendtime
                ) - UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fstarttime
                )
                ) AS fad_illegal_play_len,
              COUNT(1) AS fad_illegal_times
            ")//,COUNT(DISTINCT tbn_illegal_ad.fid) AS fad_illegal_count
            ->where($illegal_map)
            ->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad.fmedia_id"
            )
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->join("
                tadclass on 
                tadclass.fcode = tbn_illegal_ad.fad_class_code "
            )
            ->group($xs_group)
            ->select();

        //查询上个周期的数据
        if(!empty($dates)){
            $map[$summary_table.'.fdate'] = [['IN',$dates],['BETWEEN',[$last_s_time,$last_e_time]],'AND'];
        }else{
            $map[$summary_table.'.fdate'] = ['BETWEEN',[$last_s_time,$last_e_time]];
        }
        $illegal_prv_ad_mod = $table_model
            ->cache(true,600)
            ->field("
                $group_field,
                GROUP_CONCAT($fad_count_field SEPARATOR ',') as fad_sam_list,
                $summary_table.fad_class_code as fad_class_code,
                sum($summary_table.fad_times) as fad_times,
                sum($summary_table.$play_len_field) as fad_play_len
            ")
            ->join("
            tmediaclass on 
            tmediaclass.fid = $summary_table.fmedia_class_code"
            )
            ->join("
            tmediaowner on 
            tmediaowner.fid = $summary_table.fmediaownerid"
            )
            ->join("
            tregion on 
            tregion.fid = $summary_table.fregionid"
            )
            ->join("
            tmedia on 
            tmedia.fid = $summary_table.fmediaid"
            )
            ->join("
            tadclass on 
            tadclass.fcode = $summary_table.fad_class_code"
            )
            ->where($map)
            ->group($group_by)
            ->select();

        //循环计算条数
        foreach ($illegal_prv_ad_mod as $illegal_prv_ad_mod_key => $illegal_prv_ad_mod_val){
            $sam_array = explode(',',$tbn_prv_illegal_data_val['fad_sam_list']);
            $fad_count = count(array_unique($sam_array));
            unset($illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_sam_list']);
            $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_count'] = $fad_count;//插入条数
        }

        //组合上个周期违法线索相关数据
        foreach ($tbn_prv_illegal_data as $tbn_illegal_data_key=>$tbn_illegal_data_value){
            foreach ($illegal_prv_ad_mod as $data_key=>$data_value){
                if($tbn_illegal_data_value[$hb_field] == $data_value[$fztj]){
                    unset($tbn_illegal_data_value[$hb_field]);
                    $illegal_prv_ad_mod[$data_key]['counts_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_count']/$data_value['fad_count'];//条数违法率
                    $illegal_prv_ad_mod[$data_key]['fad_illegal_count'] = $tbn_illegal_data_value['fad_illegal_count'];//违法条数
                    $illegal_prv_ad_mod[$data_key]['times_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_times']/$data_value['fad_times'];//合并数组
                    $illegal_prv_ad_mod[$data_key]['lens_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_play_len']/$data_value['fad_play_len'];//合并数组
                    $illegal_prv_ad_mod[$data_key] = array_merge($tbn_illegal_data_value, $illegal_prv_ad_mod[$data_key]);//合并数组
                }
            }
        }
        foreach ($illegal_prv_ad_mod as $illegal_prv_ad_mod_key => $illegal_prv_ad_mod_val){
            if(!isset($illegal_prv_ad_mod_val['fad_illegal_count'])){

                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_count'] = 0;
            }
            if(!isset($illegal_prv_ad_mod_val['counts_illegal_rate'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['counts_illegal_rate'] = 0;
            }

            if(!isset($illegal_prv_ad_mod_val['fad_illegal_times'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_times'] = 0;
            }

            if(!isset($illegal_prv_ad_mod_val['times_illegal_rate'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['times_illegal_rate'] = 0;
            }
            if(!isset($illegal_prv_ad_mod_val['fad_illegal_play_len'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_play_len'] = 0;
            }
            if(!isset($illegal_prv_ad_mod_val['fad_play_len'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_play_len'] = 0;
            }
            $prv_all_count['fad_count'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_count'];
            $prv_all_count['fad_illegal_count'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_count'];

            $prv_all_count['fad_illegal_times'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_times'];
            $prv_all_count['fad_times'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_times'];
            $prv_all_count['fad_illegal_play_len'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_play_len'];
            $prv_all_count['fad_play_len'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_play_len'];
        }
        //对比上个周期的数据
        foreach ($illegal_prv_ad_mod as $illegal_prv_ad_val) {
            foreach ($data as $data_key=>$data_value) {

                if ($illegal_prv_ad_val[$fztj] == $data_value[$fztj]) {

                    if (!$illegal_prv_ad_val['fad_illegal_count'] || ($data_value['fad_illegal_count'] - $illegal_prv_ad_val['fad_illegal_count']) == 0) {
                        $data[$data_key]['prv_fad_illegal_count'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_count = round(($data_value['fad_illegal_count'] - $illegal_prv_ad_val['fad_illegal_count']) / $illegal_prv_ad_val['fad_illegal_count'], 4) * 100;
                        if ($prv_fad_illegal_count < 0) {
                            $data[$data_key]['prv_fad_illegal_count'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_count . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_count'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_count . '%)';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_times'] || ($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) == 0) {
                        $data[$data_key]['prv_fad_illegal_times'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_times = round(($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) / $illegal_prv_ad_val['fad_illegal_times'], 4) * 100;
                        if ($prv_fad_illegal_times < 0) {
                            $data[$data_key]['prv_fad_illegal_times'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_times . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_times'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_times . '%)';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_play_len'] || ($data_value['fad_illegal_play_len'] - $illegal_prv_ad_val['fad_illegal_play_len']) == 0) {
                        $data[$data_key]['prv_fad_illegal_play_len'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_play_len = round(($data_value['fad_illegal_play_len'] - $illegal_prv_ad_val['fad_illegal_play_len']) / $illegal_prv_ad_val['fad_illegal_play_len'], 4) * 100;

                        if ($prv_fad_illegal_play_len < 0) {
                            $data[$data_key]['prv_fad_illegal_play_len'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_play_len . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_play_len'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_play_len . '%)';
                        }
                    }
                }
            }
        }

        //如果是地域排名 按行政区划排名
        if($fztj == 'fregionid' && $region_order == 1){

            $data = $this->pxsf($data,$onther_class,false);//顺序排序

        }else{

            $data = $this->pxsf($data,$onther_class);//倒叙排序

        }

        $array_num = 0;//定义限定数量
        $data_pm = [];//定义图表数据空数组
        $data_count = count($data);//获取数据条数

        $cl_count_all = 0;//线索全部数量
        $ck_count_all = 0;//线索全部数量
        $count_all = 0;//总数
        $times=0;

        //处理排序后的数据
        foreach ($data as $data_key=>$data_value){
            $data[$data_key]['counts_illegal_rate'] = round($data_value['counts_illegal_rate'],4)*100;
            $data[$data_key]['times_illegal_rate'] = round($data_value['times_illegal_rate'],4)*100;
            $data[$data_key]['lens_illegal_rate'] = round($data_value['lens_illegal_rate'],4)*100;
            //循环获取前35条数据
            if($array_num < 35 && $array_num < count($data) && $data_value[$onther_class] > 0){
                $data_pm['name'][] = $data_value[$join_table.'_name'];
                //不同排名条件显示各自的数值
                switch ($onther_class){
                    case 'forder':
                        $data_pm['num'][] = $data[$data_key][$onther_class_val];
                        break;
                    case 'fad_illegal_times':
                        $data_pm['num'][] = $data_value['fad_illegal_times'];//违法条次
                        break;
                    case 'times_illegal_rate':
                        $data_pm['num'][] = round($data_value['times_illegal_rate'],4)*100;//条次违法率
                        break;
                    case 'fad_times':
                        $data_pm['num'][] = $data[$data_key]['fad_times'];//条次
                        break;
                    case 'fad_illegal_count':
                        $data_pm['num'][] = $data[$data_key]['fad_illegal_count'];//条数
                        break;
                    case 'counts_illegal_rate':
                        $data_pm['num'][] = $data[$data_key]['counts_illegal_rate'];//条数违法率
                        break;
                    case 'fad_count':
                        $data_pm['num'][] = $data[$data_key]['fad_count'];//条数
                        break;
                    case 'fad_play_len':
                        $data_pm['num'][] = $data[$data_key]['fad_play_len'];//时长
                        break;
                    case 'fad_illegal_play_len':
                        $data_pm['num'][] = $data_value['fad_illegal_play_len'];//违法时长
                        break;
                    case 'lens_illegal_rate':
                        $data_pm['num'][] = round($data_value['lens_illegal_rate'],4)*100;//时长违法率
                        break;
                }
                $array_num++;//fad_count  广告条次：fad_times  广告时长：fad_play_len
            }

        }

        /*线索统计模块*/
        //线索查看处理情况tbn_illegal_ad
        //已处理量
        $tbn_illegal_ad_mod = M('tbn_illegal_ad');
        if(!empty($isrelease) || $system_num == '100000'){
            $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
        }
        if(!empty($dates)){
            $illegal_map['tbn_illegal_ad_issue.fissue_date'] = [['IN',$dates],['BETWEEN',[$s_time,$e_time]],'AND'];
        }else{
            $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_time,$e_time]];
        }
        $ycl_count = $tbn_illegal_ad_mod
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as cll,
                $xs_group_field
           ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->where($illegal_map)
            ->where(['tbn_illegal_ad.fstatus'=>['gt',0]])
            ->group($xs_group)
            ->select();
        //全部数量
        $cl_count = $tbn_illegal_ad_mod
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as clall,
                $xs_group_field
            ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->where($illegal_map)
            ->group($xs_group)
            ->select();
        $xs_data = [];//定义线索统计情况相关数据

        //如果没有被处理的数据
        if(empty($ycl_count)){
            foreach ($cl_count as $cl_val){
                $xs_data[$cl_val[$hb_field]]['cll'] = '0%';
                $xs_data[$cl_val[$hb_field]][$hb_field] = $cl_val[$hb_field];
                $xs_data[$cl_val[$hb_field]]['cl_count'] = 0;
                $xs_data[$cl_val[$hb_field]]['all_count'] = $cl_val['clall'];
            }
        }else{
            //循环获取
            foreach ($ycl_count as $ycl_val){
                foreach ($cl_count as $cl_val){
                    if($ycl_val[$hb_field] == $cl_val[$hb_field]){
                        if($ycl_val['cll'] != 0){
                            $xs_data[$cl_val[$hb_field]][$hb_field] = $cl_val[$hb_field];
                            $xs_data[$cl_val[$hb_field]]['cll'] = (round($ycl_val['cll']/$cl_val['clall'],4)*100).'%';
                            $xs_data[$cl_val[$hb_field]]['cl_count'] = $ycl_val['cll'];
                        }else{
                            $xs_data[$cl_val[$hb_field]]['cll'] = '0%';
                            $xs_data[$cl_val[$hb_field]][$hb_field] = $cl_val[$hb_field];
                            $xs_data[$cl_val[$hb_field]]['cl_count'] = 0;
                        }
                        $xs_data[$cl_val[$hb_field]]['all_count'] = $cl_val['clall'];
                    }
                }
            }
        }
        //已查看量
        $yck_count = $tbn_illegal_ad_mod
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as ckl,
                $xs_group_field
            ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->where($illegal_map)
            ->where(['tbn_illegal_ad.fview_status'=>['gt',0]])
            ->group($xs_group)
            ->select();

        //全部数量
        $ck_count = $tbn_illegal_ad_mod
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as ckall,
                $xs_group_field
            ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->where($illegal_map)
            ->group($xs_group)
            ->select();

        //如果没有一查看的数量
        if(empty($yck_count)){
            foreach ($ck_count as $ck_val){
                $xs_data[$ck_val[$hb_field]]['ckl'] = '0%';
                $xs_data[$ck_val[$hb_field]]['ck_count'] = 0;
                $xs_data[$ck_val[$hb_field]]['all_count'] = $ck_val['ckall'];
            }
        }else{
            //循环获取
            foreach ($yck_count as $yck_val){
                foreach ($ck_count as $ck_val){
                    if($yck_val[$hb_field] == $ck_val[$hb_field]){
                        if($yck_val['ckl'] != 0){
                            $xs_data[$ck_val[$hb_field]]['ck_count'] = $yck_val['ckl'];
                        }else{
                            $xs_data[$ck_val[$hb_field]]['ck_count'] = 0;
                        }
                        $xs_data[$ck_val[$hb_field]]['ckl'] = (round($yck_val['ckl']/$ck_val['ckall'],4)*100).'%';
                    }
                    $xs_data[$ck_val[$hb_field]]['all_count'] = $ck_val['ckall'];
                }
            }//组建案件查看与处理情况
        }


        //组合线索相关数据
        foreach ($xs_data as $xs_key=>$xs_value){
            foreach ($data as $data_key=>$data_value){
                if($xs_key == $data_value[$fztj]){
                    unset($xs_value[$hb_field]);
                    $cl_count_all += $xs_value['cl_count'];
                    $ck_count_all += $xs_value['ck_count'];
                    $count_all += $xs_value['all_count'];
                    $data[$data_key] = array_merge($xs_value, $data[$data_key]);//合并数组
                }
            }
        }

        //组合广告类别百分比图表数据
        $ad_class_num = 0;
        if($fztj == 'fad_class_code') {
            $data_pm = [];
            $data = $this->pxsf($data, $onther_class);//排序
            foreach ($data as $ad_class_key=>$ad_class_value){
                if($ad_class_value[$onther_class] != 0){
                    $data_pm['data'][] = [
                        'name' => $ad_class_value[$join_table.'_name'],
                        'value' => $ad_class_value[$onther_class]
                    ];
                    $data_pm['name'][] = $ad_class_value[$join_table.'_name'];
                }
            }
        }

        $all_prv_fad_illegal_count = round(($all_count['fad_illegal_count'] - $prv_all_count['fad_illegal_count']) / $prv_all_count['fad_illegal_count'], 4) * 100;
        $all_prv_fad_illegal_play_len = round(($all_count['fad_illegal_play_len'] - $prv_all_count['fad_illegal_play_len']) / $prv_all_count['fad_illegal_play_len'], 4) * 100;
        $all_prv_fad_illegal_times = round(($all_count['fad_illegal_times'] - $prv_all_count['fad_illegal_times']) / $prv_all_count['fad_illegal_times'], 4) * 100;

        $counts_illegal_rate = round($all_count['fad_illegal_count']/$all_count['fad_count'],4)*100;
        $times_illegal_rate = round($all_count['fad_illegal_times']/$all_count['fad_times'],4)*100;
        $lens_illegal_rate = round($all_count['fad_illegal_play_len']/$all_count['fad_play_len'],4)*100;

        $prv_counts_illegal_rate = round($prv_all_count['fad_illegal_count']/$prv_all_count['fad_count'],4)*100;
        $prv_times_illegal_rate = round($prv_all_count['fad_illegal_times']/$prv_all_count['fad_times'],4)*100;
        $prv_lens_illegal_rate = round($prv_all_count['fad_illegal_play_len']/$prv_all_count['fad_play_len'],4)*100;

        if ($all_prv_fad_illegal_count < 0) {
            $all_prv_fad_illegal_count = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_count . '%)';
        } else {
            if($all_prv_fad_illegal_count == 0){
                $all_prv_fad_illegal_count = '(0.00%)';

            }else{
                $all_prv_fad_illegal_count = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_count . '%)';
            }
        }

        if ($all_prv_fad_illegal_play_len < 0) {
            $all_prv_fad_illegal_play_len = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_play_len . '%)';
        } else {
            if($all_prv_fad_illegal_play_len == 0){
                $all_prv_fad_illegal_play_len = '(0.00%)';

            }else{
                $all_prv_fad_illegal_play_len = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_play_len . '%)';
            }
        }

        if ($all_prv_fad_illegal_times < 0) {
            $all_prv_fad_illegal_times = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_times . '%)';
        } else {
            if($all_prv_fad_illegal_times == 0){
                $all_prv_fad_illegal_times = '(0.00%)';

            }else{
                $all_prv_fad_illegal_times = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_times . '%)';
            }
        }

        $compar_fad_count = $all_count['fad_count'] - $prv_all_count['fad_count'];
        if ($compar_fad_count < 0) {
            $compar_fad_count = '减少'.($compar_fad_count*-1);
        } else {
            if($compar_fad_count == 0){
                $compar_fad_count = '无变化';

            }else{
                $compar_fad_count = '增加'.$compar_fad_count;
            }
        }
        $compar_fad_illegal_count = $all_count['fad_illegal_count'] - $prv_all_count['fad_illegal_count'];
        if ($compar_fad_illegal_count < 0) {
            $compar_fad_illegal_count = '减少'.($compar_fad_illegal_count*-1);
        } else {
            if($compar_fad_illegal_count == 0){
                $compar_fad_illegal_count = '无变化';

            }else{
                $compar_fad_illegal_count = '增加'.$compar_fad_illegal_count;
            }
        }
        $compar_illegal_count_rate = $counts_illegal_rate - $prv_counts_illegal_rate;
        if ($compar_illegal_count_rate < 0) {
            $compar_illegal_count_rate = '下降'.($compar_illegal_count_rate*-1).'%';
        } else {
            if($compar_illegal_count_rate == 0){
                $compar_illegal_count_rate = '无变化';

            }else{
                $compar_illegal_count_rate = '上升'.$compar_illegal_count_rate.'%';
            }
        }
        $compar_fad_times = $all_count['fad_times'] - $prv_all_count['fad_times'];
        if ($compar_fad_times < 0) {
            $compar_fad_times = '减少'.($compar_fad_times*-1);
        } else {
            if($compar_fad_times == 0){
                $compar_fad_times = '无变化';

            }else{
                $compar_fad_times = '增加'.$compar_fad_times;
            }
        }
        $compar_fad_illegal_times = $all_count['fad_illegal_times'] - $prv_all_count['fad_illegal_times'];
        if ($compar_fad_illegal_times < 0) {
            $compar_fad_illegal_times = '减少'.($compar_fad_illegal_times*-1);
        } else {
            if($compar_fad_illegal_times == 0){
                $compar_fad_illegal_times = '无变化';

            }else{
                $compar_fad_illegal_times = '增加'.$compar_fad_illegal_times;
            }
        }
        $compar_illegal_times_rate = $times_illegal_rate - $prv_times_illegal_rate;
        if ($compar_illegal_times_rate < 0) {
            $compar_illegal_times_rate = '下降'.($compar_illegal_times_rate*-1).'%';
        } else {
            if($compar_illegal_times_rate == 0){
                $compar_illegal_times_rate = '无变化';
            }else{
                $compar_illegal_times_rate = '上升'.$compar_illegal_times_rate.'%';
            }
        }
        $compar_fad_play_len = $all_count['fad_play_len'] - $prv_all_count['fad_play_len'];
        if ($compar_fad_play_len < 0) {
            $compar_fad_play_len = '减少'.($compar_fad_play_len*-1);
        } else {
            if($compar_fad_play_len == 0){
                $compar_fad_play_len = '无变化';

            }else{
                $compar_fad_play_len = '增加'.$compar_fad_play_len;
            }
        }
        $compar_fad_illegal_play_len = $all_count['fad_illegal_play_len'] - $prv_all_count['fad_illegal_play_len'];
        if ($compar_fad_illegal_play_len < 0) {
            $compar_fad_illegal_play_len = '减少'.($compar_fad_illegal_play_len*-1);
        } else {
            if($compar_fad_illegal_play_len == 0){
                $compar_fad_illegal_play_len = '无变化';

            }else{
                $compar_fad_illegal_play_len = '增加'.$compar_fad_illegal_play_len;
            }
        }
        $compar_illegal_play_len_rate = $lens_illegal_rate - $prv_lens_illegal_rate;
        if ($compar_illegal_play_len_rate < 0) {
            $compar_illegal_play_len_rate = '下降'.($compar_illegal_play_len_rate*-1).'%';
        } else {
            if($compar_illegal_play_len_rate == 0){
                $compar_illegal_play_len_rate = '无变化';
            }else{
                $compar_illegal_play_len_rate = '上升'.$compar_illegal_play_len_rate.'%';
            }
        }
        $compar_prv_com_score = round($all_count['com_score'] - $prv_all_count['prv_com_score'],2);
        if ($compar_prv_com_score < 0) {
            $compar_prv_com_score = '减少'.($compar_prv_com_score*-1);
        } else {
            if($compar_prv_com_score == 0){
                $compar_prv_com_score = '无变化';

            }else{
                $compar_prv_com_score = '增加'.$compar_prv_com_score;
            }
        }

        /*
        已立案 yla  fstatus = 10 and fstatus2 in(15,17)
        已结案 yja fstatus = 10 and fstatus2=16
        责令停止发布 tzfb fstatus = 10 and fresult like '%责令停止发布%'
        立案 lian fstatus = 10 and fresult like '%立案%'
        移交司法机关 yjsf fstatus = 10 and fresult like '%移送司法%'
        其他  others fstatus = 10 and fresult like '%其他%'
         * */

        //统计放在最后一条$all_count     $prv_all_count
        $data[] = [
            'all_xs_count' =>$count_all,
            'titlename'=>'全部',
            "cll"=>(round($cl_count_all/$count_all,4)*100).'%',
            "cl_count" =>$cl_count_all,
            "ck_count"=>$ck_count_all,
            "yla"=>$all_count['yla'],
            "yja"=>$all_count['yja'],
            "tzfb"=>$all_count['tzfb'],
            "lian"=>$all_count['lian'],
            "yjsf"=>$all_count['yjsf'],
            "others"=>$all_count['others'],
            "ckl"=>(round($ck_count_all/$count_all,4)*100).'%',
            "prv_fad_illegal_count"=>$all_prv_fad_illegal_count,
            "prv_fad_illegal_play_len"=>$all_prv_fad_illegal_play_len,
            "prv_fad_illegal_times"=>$all_prv_fad_illegal_times,

            'fad_count'=>$all_count['fad_count'],//广告条数
            'fad_illegal_count'=>$all_count['fad_illegal_count'],//违法广告数量
            'counts_illegal_rate'=>round($all_count['fad_illegal_count']/$all_count['fad_count'],4)*100,
            'fad_times'=>$all_count['fad_times'],//广告条次
            'fad_illegal_times'=>$all_count['fad_illegal_times'],//违法广告条次
            'times_illegal_rate'=>round($all_count['fad_illegal_times']/$all_count['fad_times'],4)*100,
            'fad_play_len'=>$all_count['fad_play_len'],//总时长
            'fad_illegal_play_len'=>$all_count['fad_illegal_play_len'],//违法时长
            'lens_illegal_rate'=>round($all_count['fad_illegal_play_len']/$all_count['fad_play_len'],4)*100,
            "com_score"=>round($all_count['com_score'],2),//综合分

            "compar_fad_count"=>$compar_fad_count,//对比上个周期广告条数
            "compar_fad_illegal_count"=>$compar_fad_illegal_count,//对比上个周期违法广告数量
            "compar_illegal_count_rate"=>$compar_illegal_count_rate,//对比上个周期条数违法率
            "compar_fad_times"=>$compar_fad_times,//对比上个周期广告条次
            "compar_fad_illegal_times"=>$compar_fad_illegal_times,//对比上个周期违法广告条次
            "compar_illegal_times_rate"=>$compar_illegal_times_rate,//对比上个周期条次违法率
            "compar_fad_play_len"=>$compar_fad_play_len,//对比上个周期总时长
            "compar_fad_illegal_play_len"=>$compar_fad_illegal_play_len,//对比上个周期违法时长
            "compar_illegal_play_len_rate"=>$compar_illegal_play_len_rate,//对比上个周期时长违法率
            "compar_prv_com_score"=>$compar_prv_com_score,//对比上个周期综合分
        ];

        //是否展示条数相关
        /*        if(!$isshow_count){
                    foreach ($data as $data_key=>$data_val){
                        unset($data[$data_key]['fad_count']);
                        unset($data[$data_key]['fad_illegal_count']);
                        unset($data[$data_key]['counts_illegal_rate']);
                    }
                }*/

        $finally_data = [
            'count'=> count($data)-1,
            'data_pm'=>$data_pm,//图表数据
            'data_list'=>$data//列表数据
        ];

        if($is_out_report == 1){

            if($is_have_count == 1){
                //如果是线索菜单并且是点击了导出按钮
                $objPHPExcel = new \PHPExcel();
                $objPHPExcel
                    ->getProperties()  //获得文件属性对象，给下文提供设置资源
                    ->setCreator( "MaartenBalliauw")             //设置文件的创建者
                    ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
                    ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
                    ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
                    ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
                    ->setKeywords( "office 2007 openxmlphp")        //设置标记
                    ->setCategory( "Test resultfile");                //设置类别

                $sheet = $objPHPExcel->setActiveSheetIndex(0);
                //分组导出数据1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
                switch ($fztj){
                    case 'fmediaid':
                        $sheet ->mergeCells("D1:F1");
                        $sheet ->mergeCells("G1:I1");
                        $sheet ->mergeCells("J1:L1");
                        $sheet ->mergeCells("M1:P1");
                        $sheet ->setCellValue('A1','');
                        $sheet ->setCellValue('B1','');
                        $sheet ->setCellValue('C1','');
                        $sheet ->setCellValue('D1','');
                        $sheet ->setCellValue('E1','广告条数');
                        $sheet ->setCellValue('H1','广告条次');
                        $sheet ->setCellValue('K1','广告时长');
                        $sheet ->setCellValue('N1','线索查看及处理情况');

                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','媒体所在地');
                        $sheet ->setCellValue('C2','媒体名称');
                        $sheet ->setCellValue('D2','总条数');
                        $sheet ->setCellValue('E2','违法条数');
                        $sheet ->setCellValue('F2','条数违法率');
                        $sheet ->setCellValue('G2','总条次');
                        $sheet ->setCellValue('H2','违法条次');
                        $sheet ->setCellValue('I2','条次违法率');
                        $sheet ->setCellValue('J2','总时长');
                        $sheet ->setCellValue('K2','违法时长');
                        $sheet ->setCellValue('L2','时长违法率');
                        $sheet ->setCellValue('M2','查看量');
                        $sheet ->setCellValue('N2','查看率');
                        $sheet ->setCellValue('O2','处理量');
                        $sheet ->setCellValue('P2','处理率');

                        foreach ($data as $key => $value) {

                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                                $sheet ->setCellValue('C'.($key+3),'全部媒体');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                                $sheet ->setCellValue('C'.($key+3),$value['tmedia_name']);
                            }

                            $sheet ->setCellValue('D'.($key+3),$value['fad_count']);
                            $sheet ->setCellValue('E'.($key+3),$value['fad_illegal_count']);
                            $sheet ->setCellValue('F'.($key+3),$value['counts_illegal_rate'].'%');
                            $sheet ->setCellValue('G'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('H'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('I'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('j'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('K'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('L'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('M'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('N'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('O'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('P'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fregionid':
                        $sheet ->mergeCells("C1:E1");
                        $sheet ->mergeCells("F1:H1");
                        $sheet ->mergeCells("I1:K1");
                        $sheet ->mergeCells("L1:O1");
                        $sheet ->setCellValue('A1','');
                        $sheet ->setCellValue('B1','');
                        $sheet ->setCellValue('C1','广告条数');
                        $sheet ->setCellValue('F1','广告条次');
                        $sheet ->setCellValue('I1','广告时长');
                        $sheet ->setCellValue('L1','线索查看及处理情况');
                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','地域名称');
                        $sheet ->setCellValue('C2','总条数');
                        $sheet ->setCellValue('D2','违法条数');
                        $sheet ->setCellValue('E2','条数违法率');
                        $sheet ->setCellValue('F2','总条次');
                        $sheet ->setCellValue('G2','违法条次');
                        $sheet ->setCellValue('H2','条次违法率');
                        $sheet ->setCellValue('I2','总时长');
                        $sheet ->setCellValue('J2','违法时长');
                        $sheet ->setCellValue('K2','时长违法率');
                        $sheet ->setCellValue('L2','查看量');
                        $sheet ->setCellValue('M2','查看率');
                        $sheet ->setCellValue('N2','处理量');
                        $sheet ->setCellValue('O2','处理率');

                        foreach ($data as $key => $value) {


                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                            }

                            $sheet ->setCellValue('C'.($key+3),$value['fad_count']);
                            $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_count']);
                            $sheet ->setCellValue('E'.($key+3),$value['counts_illegal_rate'].'%');
                            $sheet ->setCellValue('F'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('H'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('I'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('J'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('K'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('L'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('M'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('N'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('O'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fmediaownerid':
                        $sheet ->mergeCells("E1:G1");
                        $sheet ->mergeCells("H1:J1");
                        $sheet ->mergeCells("K1:M1");
                        $sheet ->mergeCells("N1:Q1");
                        $sheet ->setCellValue('A1','');
                        $sheet ->setCellValue('B1','');
                        $sheet ->setCellValue('C1','');
                        $sheet ->setCellValue('D1','');
                        $sheet ->setCellValue('E1','广告条数');
                        $sheet ->setCellValue('H1','广告条次');
                        $sheet ->setCellValue('K1','广告时长');
                        $sheet ->setCellValue('N1','线索查看及处理情况');

                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','媒介机构所在地');
                        $sheet ->setCellValue('C2','媒介机构名称');
                        $sheet ->setCellValue('D2','媒介类型');
                        $sheet ->setCellValue('E2','总条数');
                        $sheet ->setCellValue('F2','违法条数');
                        $sheet ->setCellValue('G2','条数违法率');
                        $sheet ->setCellValue('H2','总条次');
                        $sheet ->setCellValue('I2','违法条次');
                        $sheet ->setCellValue('J2','条次违法率');
                        $sheet ->setCellValue('K2','总时长');
                        $sheet ->setCellValue('L2','违法时长');
                        $sheet ->setCellValue('M2','时长违法率');
                        $sheet ->setCellValue('N2','查看量');
                        $sheet ->setCellValue('O2','查看率');
                        $sheet ->setCellValue('P2','处理量');
                        $sheet ->setCellValue('Q2','处理率');

                        foreach ($data as $key => $value) {

                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                                $sheet ->setCellValue('C'.($key+3),'全部媒介机构');
                                $sheet ->setCellValue('D'.($key+3),'全部类型');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                                $sheet ->setCellValue('C'.($key+3),$value['tmediaowner_name']);
                                $sheet ->setCellValue('D'.($key+3),$value['tmediaclass_name']);
                            }

                            $sheet ->setCellValue('E'.($key+3),$value['fad_count']);
                            $sheet ->setCellValue('F'.($key+3),$value['fad_illegal_count']);
                            $sheet ->setCellValue('G'.($key+3),$value['counts_illegal_rate'].'%');
                            $sheet ->setCellValue('H'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('I'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('J'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('K'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('L'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('M'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('N'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('O'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('P'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('Q'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fad_class_code':
                        $sheet ->mergeCells("C1:E1");
                        $sheet ->mergeCells("F1:H1");
                        $sheet ->mergeCells("I1:K1");
                        $sheet ->mergeCells("L1:O1");
                        $sheet ->setCellValue('A1','');
                        $sheet ->setCellValue('B1','');
                        $sheet ->setCellValue('C1','广告条数');
                        $sheet ->setCellValue('F1','广告条次');
                        $sheet ->setCellValue('I1','广告时长');
                        $sheet ->setCellValue('L1','线索查看及处理情况');

                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','广告类别');
                        $sheet ->setCellValue('C2','总条数');
                        $sheet ->setCellValue('D2','违法条数');
                        $sheet ->setCellValue('E2','条数违法率');
                        $sheet ->setCellValue('F2','总条次');
                        $sheet ->setCellValue('G2','违法条次');
                        $sheet ->setCellValue('H2','条次违法率');
                        $sheet ->setCellValue('I2','总时长');
                        $sheet ->setCellValue('J2','违法时长');
                        $sheet ->setCellValue('K2','时长违法率');
                        $sheet ->setCellValue('L2','查看量');
                        $sheet ->setCellValue('M2','查看率');
                        $sheet ->setCellValue('N2','处理量');
                        $sheet ->setCellValue('O2','处理率');

                        foreach ($data as $key => $value) {


                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部类别');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tadclass_name']);
                            }

                            $sheet ->setCellValue('C'.($key+3),$value['fad_count']);
                            $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_count']);
                            $sheet ->setCellValue('E'.($key+3),$value['counts_illegal_rate'].'%');
                            $sheet ->setCellValue('F'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('H'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('I'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('J'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('K'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('L'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('M'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('N'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('O'.($key+3),$value['cll']);

                        }
                        break;
                }
                $objActSheet =$objPHPExcel->getActiveSheet();
                $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
                $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

                $date = date('Ymdhis');
                $savefile = './Public/Xls/'.$date.'.xlsx';
                $savefile2 = '/Public/Xls/'.$date.'.xlsx';
                $objWriter->save($savefile);
                //即时导出下载
                /*          header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                            header('Content-Disposition:attachment;filename="'.$date.'.xlsx"');
                            header('Cache-Control:max-age=0');
                            $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
                            $objWriter->save( 'php://output');*/
                $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
                unlink($savefile);//删除文件
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
            }else{
                //如果是线索菜单并且是点击了导出按钮
                $objPHPExcel = new \PHPExcel();
                $objPHPExcel
                    ->getProperties()  //获得文件属性对象，给下文提供设置资源
                    ->setCreator( "MaartenBalliauw")             //设置文件的创建者
                    ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
                    ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
                    ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
                    ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
                    ->setKeywords( "office 2007 openxmlphp")        //设置标记
                    ->setCategory( "Test resultfile");                //设置类别

                $sheet = $objPHPExcel->setActiveSheetIndex(0);
                //分组导出数据1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
                switch ($fztj){
                    case 'fmediaid':
                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','媒体所在地');
                        $sheet ->setCellValue('C2','媒体名称');
                        $sheet ->setCellValue('D2','总条次');
                        $sheet ->setCellValue('E2','违法条次');
                        $sheet ->setCellValue('F2','条次违法率');
                        $sheet ->setCellValue('G2','总时长');
                        $sheet ->setCellValue('H2','违法时长');
                        $sheet ->setCellValue('I2','时长违法率');
                        $sheet ->setCellValue('J2','查看量');
                        $sheet ->setCellValue('K2','查看率');
                        $sheet ->setCellValue('L2','处理量');
                        $sheet ->setCellValue('M2','处理率');
                        foreach ($data as $key => $value) {

                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                                $sheet ->setCellValue('C'.($key+3),'全部媒体');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                                $sheet ->setCellValue('C'.($key+3),$value['tmedia_name']);
                            }
                            $sheet ->setCellValue('D'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('E'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('F'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('G'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('H'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('I'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('J'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('K'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('L'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('M'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fregionid':
                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','地域名称');
                        $sheet ->setCellValue('C2','总条次');
                        $sheet ->setCellValue('D2','违法条次');
                        $sheet ->setCellValue('E2','条次违法率');
                        $sheet ->setCellValue('F2','总时长');
                        $sheet ->setCellValue('G2','违法时长');
                        $sheet ->setCellValue('H2','时长违法率');
                        $sheet ->setCellValue('I2','查看量');
                        $sheet ->setCellValue('J2','查看率');
                        $sheet ->setCellValue('K2','处理量');
                        $sheet ->setCellValue('L2','处理率');

                        foreach ($data as $key => $value) {


                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                            }
                            $sheet ->setCellValue('C'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('E'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('F'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('H'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('I'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('J'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('K'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('L'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fmediaownerid':
                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','媒介机构所在地');
                        $sheet ->setCellValue('C2','媒介机构名称');
                        $sheet ->setCellValue('D2','媒介类型');
                        $sheet ->setCellValue('E2','总条次');
                        $sheet ->setCellValue('F2','违法条次');
                        $sheet ->setCellValue('G2','条次违法率');
                        $sheet ->setCellValue('H2','总时长');
                        $sheet ->setCellValue('I2','违法时长');
                        $sheet ->setCellValue('J2','时长违法率');
                        $sheet ->setCellValue('K2','查看量');
                        $sheet ->setCellValue('L2','查看率');
                        $sheet ->setCellValue('M2','处理量');
                        $sheet ->setCellValue('N2','处理率');

                        foreach ($data as $key => $value) {

                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部地域');
                                $sheet ->setCellValue('C'.($key+3),'全部媒介机构');
                                $sheet ->setCellValue('D'.($key+3),'全部类型');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                                $sheet ->setCellValue('C'.($key+3),$value['tmediaowner_name']);
                                $sheet ->setCellValue('D'.($key+3),$value['tmediaclass_name']);
                            }
                            $sheet ->setCellValue('E'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('F'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('G'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('H'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('I'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('J'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('K'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('L'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('M'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('N'.($key+3),$value['cll']);

                        }
                        break;
                    case 'fad_class_code':
                        $sheet ->setCellValue('A2','排名');
                        $sheet ->setCellValue('B2','广告类别');
                        $sheet ->setCellValue('C2','总条次');
                        $sheet ->setCellValue('D2','违法条次');
                        $sheet ->setCellValue('E2','条次违法率');
                        $sheet ->setCellValue('F2','总时长');
                        $sheet ->setCellValue('G2','违法时长');
                        $sheet ->setCellValue('H2','时长违法率');
                        $sheet ->setCellValue('I2','查看量');
                        $sheet ->setCellValue('J2','查看率');
                        $sheet ->setCellValue('K2','处理量');
                        $sheet ->setCellValue('L2','处理率');

                        foreach ($data as $key => $value) {
                            $sheet ->setCellValue('A'.($key+3),($key+1));
                            if($data_count == $key){
                                $sheet ->setCellValue('B'.($key+3),'全部类别');
                            }else{
                                $sheet ->setCellValue('B'.($key+3),$value['tadclass_name']);
                            }
                            $sheet ->setCellValue('C'.($key+3),$value['fad_times']);
                            $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_times']);
                            $sheet ->setCellValue('E'.($key+3),$value['times_illegal_rate'].'%');
                            $sheet ->setCellValue('F'.($key+3),$value['fad_play_len']);
                            $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_play_len']);
                            $sheet ->setCellValue('H'.($key+3),$value['lens_illegal_rate'].'%');
                            $sheet ->setCellValue('I'.($key+3),$value['ck_count']);
                            $sheet ->setCellValue('J'.($key+3),$value['ckl']);
                            $sheet ->setCellValue('K'.($key+3),$value['cl_count']);
                            $sheet ->setCellValue('L'.($key+3),$value['cll']);
                        }
                        break;
                }
                $objActSheet =$objPHPExcel->getActiveSheet();
                $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
                $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

                $date = date('Ymdhis');
                $savefile = './Public/Xls/'.$date.'.xlsx';
                $savefile2 = '/Public/Xls/'.$date.'.xlsx';
                $objWriter->save($savefile);
                //即时导出下载
                /*          header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                            header('Content-Disposition:attachment;filename="'.$date.'.xlsx"');
                            header('Cache-Control:max-age=0');
                            $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
                            $objWriter->save( 'php://output');*/
                $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
                unlink($savefile);//删除文件
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
            }

        }elseif($is_out_report == 2){
            $this->jchz_out_word($menu,$field_name,$title,$data,$date_cycle,$s_time,$e_time,$regio_area);
        }else{
            $this->ajaxReturn($finally_data);
        }

    }

    public function app_ad_monitor(){
        ini_set('memory_limit','3072M');
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type: text/html; charset=utf-8");
        $user = session('regulatorpersonInfo');//获取用户信息

        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isrelease = $ALL_CONFIG['isrelease'];
        $use_open_search = $ALL_CONFIG['use_open_search'];
        $ischeck = $ALL_CONFIG['ischeck'];

        $region_order = I('region_order',0);//是否地域排名

        $is_have_self = I('is_have_self',0);//是否显示本级

        $ad_pm_type = I('ad_pm_type','');//确定是点击监测情况或者线索菜单

        $is_out_report = I('is_out_report',0);//确定是否点击导出EXCEL

        $is_have_count = I('is_have_count',0);//确定是否点击导出EXCEL包含条数

        $year = I('year',date('Y'));//默认当年

        $times_table = I('times_table','');//选表，默认月表

        $is_show_fsend = I('is_show_fsend',2);//是否发布前的数据

        $is_show_longad = I('is_show_longad',1);//是否包含长广告

        $permission_media = I('permission_media',1);//控制权限媒体

        $title = I('title','XX市');//描述标题
        $regio_area = I('regio_area','XX市各县');//选择区划文字描述

        $isfixeddata = I('isfixeddata');//是否实时数据

        $date_cycle = I('date_cycle');//日期周期文字描述

        $onther_class = I('onther_class','');//排名条件

        $arr_p = I('page',1);//数组分页
        $arr_count = I('page_count',10);//获取数量
        //连表
        //分组统计条件1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
        $fztj = I('fztj','');//默认连接地域表 fmedia_class_code  fmediaownerid fregionid fmediaid fad_class_code
        if($fztj == ''){
            $fztj = 'fregionid';
        }
        //默认表
        if($times_table == ''){
            $times_table = 'tbn_ad_summary_month';
        }
        $table_condition = I('table_condition',date('m').'-01');//根据选取的不用表展示不同的条件  //默认当月

        $table_condition_str = str_replace("-","_",$table_condition);//

        $ranking = I('ranking',0);//是否只是获取排名
        if($ranking == 1){
            if(S('rank_number_'.$onther_class.$table_condition_str) != null && $fztj == 'fregionid'){
                $rank_res = [
                    'code'=> 0,
                    'msg'=>"获取成功_S！",
                    'rank_number'=>intval(S('rank_number_'.$onther_class.$table_condition_str))
                ];
                $this->ajaxReturn($rank_res);
            }
        }
        $map = [];//定义查询条件
        if($system_num == '100000'||empty($isfixeddata)){
            $summary_table = "tbn_ad_summary_day_z";

            if($is_show_longad == 1){
                //包含长广告
                $play_len_field = 'in_long_ad_play_len';
                $fad_times_field = 'fad_times';
                $fad_count_field = 'fad_sam_list';
            }else{
                //不包含长广告
                $play_len_field = 'fad_play_len';
                $fad_times_field = 'basic_ad_times';
                $fad_count_field = 'basic_ad_sam_list';
            }
        }else{
            if($use_open_search == 1) {
                $summary_table = "tbn_ad_summary_day_v3";
                if($is_show_longad == 1){
                    //包含长广告
                    $play_len_field = 'fad_play_len_long_ad';
                    $fad_times_field = 'fad_times_long_ad';
                    $fad_count_field = 'fsam_list_long_ad';
                }else{
                    //不包含长广告
                    $play_len_field = 'fad_play_len';
                    $fad_times_field = 'fad_times';
                    $fad_count_field = 'fsam_list';
                }
            }else{
                $summary_table = "tbn_ad_summary_day";
                if($is_show_longad == 1){
                    $play_len_field = 'in_long_ad_play_len';
                    $fad_times_field = 'fad_times';
                    $fad_count_field = 'fad_sam_list';
                }else{
                    $play_len_field = 'fad_play_len';
                    $fad_times_field = 'basic_ad_times';
                    $fad_count_field = 'basic_ad_sam_list';
                }
            }

        }


        //根据选择的不同的时间组合不同的条件
        switch ($times_table){
            case 'tbn_ad_summary_day':
                $sedays = explode(',',$table_condition);
                $cycle_stamp = [
                    'start' =>strtotime($sedays[0]),
                    'end' =>(strtotime($sedays[1])+86400)
                ];//周期首末时间戳
                break;
            case 'tbn_ad_summary_week':
                if($table_condition < 10){
                    $table_condition = '0'.$table_condition;
                }
                $cycle_stamp = $this->weekday($year,$table_condition);//周期首末时间戳
                $cycle_stamp['end'] = $cycle_stamp['end']+86400;
                break;
            case 'tbn_ad_summary_half_month':
            case 'tbn_ad_summary_month':
            case 'tbn_ad_summary_quarter':
                $table_condition = $year.'-'.$table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                break;
            case 'tbn_ad_summary_half_year':
            case 'tbn_ad_summary_year':
                $table_condition = $table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                break;
        }

        $s_time = date('Y-m-d',$cycle_stamp['start']);
        $e_time = date('Y-m-d',($cycle_stamp['end']-86400));

        $map[$summary_table.'.fdate'] = ['BETWEEN',[$s_time,$e_time]];
        $map['_string'] = 'tmedia.fid = tmedia.main_media_id';
        $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_time,$e_time]];
        $illegal_map['tbn_illegal_ad.fstatus3'] = ['NOT IN',[30,40]];
        $s_timestamp = strtotime($s_time);//开始时间戳
        $e_timestamp = strtotime($e_time);//结束时间戳

        //获取上个周期的日期范围
        if(($e_timestamp - $s_timestamp) <= 0){
            $last_s_time = date('Y-m-d',($s_timestamp-86400));//上周期开始时间戳
            $last_e_time = $last_s_time;//上周期结束时间戳
        }else{
            $last_s_time = date('Y-m-d',($s_timestamp-($e_timestamp-$s_timestamp)-86400));
            $last_e_time = date('Y-m-d',($s_timestamp-86400));
        }


        //浏览权限设置start
        $re_level = $this->judgment_region($user['regionid']);//当前用户行政等级

        //是否需要抽查

        if(!empty($ischeck)){
            $spot_check_data = M('spot_check')->where(['fcustomer'=>$user['system_num']])->select();//查询抽查表数据
            //如果抽查表有数据
            if(!empty($spot_check_data)){
                $dates = [];//定义日期数组
                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                    $condition = trim($spot_check_data_val['condition']);
                    if(!empty($condition)){
                        $year_month = '';
                        $date_str = [];
                        $year_month = substr($spot_check_data_val['fmonth'],0,7);
                        if(!empty($spot_check_data_val['condition'])){
                            $date_str = explode(',',$spot_check_data_val['condition']);
                        }
                        foreach ($date_str as $date_str_val){

                            $dates[] = $year_month.'-'.$date_str_val;
                        }
                    }
                }
                $map[$summary_table.'.fdate'] = [['IN',$dates],['BETWEEN',[$s_time,$e_time]],'AND'];
                $illegal_map['tbn_illegal_ad_issue.fissue_date'] = [['IN',$dates],['BETWEEN',[$s_time,$e_time]],'AND'];
            }else{
                $map['_string'] = '1 = 0';
                $illegal_map['_string'] = '1 = 0';
            }
        }

        //如果是要看同级的用户，行政区划登记上升一级1210
        $is_show_tj = $ALL_CONFIG['is_show_tj'];//是否显示同级20181210
        $is_show_sj = $ALL_CONFIG['is_show_sj'];//是否显示省级20181210
        if(!empty($is_show_tj) && $re_level > 1){
            if($re_level == 2 || $re_level == 3 || $re_level == 4){
                $re_level = 1;
                $regionid = substr($user['regionid'],0,2).'0000';
            }else{
                $regionid = trim($user['regionid'],'0').'00';
                $re_level = $this->judgment_region($regionid);
            }
        }else{
            $regionid = $user['regionid'];
        }

        //判断$re_level用户点击哪个菜单
        //线索汇总
        if($ad_pm_type == 2){

            /*      $fcustomers = M('tbn_illegal_ad')->group('fcustomer')->getField('fcustomer',true);
                    foreach ($fcustomers as $fcustomers_key=>$fcustomers_val){
                        if($fcustomers_val == '100000'){
                            unset($fcustomers[$fcustomers_key]);
                        }
                    }
                    $illegal_map['tbn_illegal_ad.fcustomer'] = ['IN',$fcustomers];*/
            if(!empty($isrelease) || $system_num == '100000'){
                $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
            }
            $region = I('region2','');//区域
            if($region == ''){
                if($system_num == '100000' && session('regulatorpersonInfo.fregulatorlevel') != 30){
                    $map['tregion.fpid|tregion.fid'] = $regionid;
                    $map["tregion.flevel"] = ['IN','1,2,3'];
                }else{
                    if($re_level == 2 || $re_level == 3 || $re_level == 4){
                        $region = 5;
                    }else{
                        $region = $re_level;
                    }
                }
            }
            $illegal_map['tbn_illegal_ad.fcustomer'] = $system_num;
            //如果是要查看同级的就不添加媒体权限限制
            if(empty($is_show_tj) && ($permission_media == 1 || $permission_media == 2)){
                switch ($permission_media){
                    case 1:
                        $exp = 'IN';
                        break;
                    case 2:
                        $exp = 'NOT IN';
                        break;
                }
                $media_ids = M('tmedia_temp')
                    ->where(['ftype'=>['IN',[1,2]],'fcustomer'=>$system_num,'fuserid'=>session('regulatorpersonInfo.fid')])
                    ->getField('fmediaid',true);
                $map['tmedia.fid'] = [$exp,$media_ids];
                $illegal_map['tbn_illegal_ad_issue.fmedia_id'] = array($exp,$media_ids);//权限媒体
            }
            //线索
            switch ($region){
                case 0:
                case 1://全国各省违法线索情况
                    if($re_level == 0){
                        $map["tregion.flevel"] = 1;
                        $illegal_map["tregion.flevel"] = 1;
                    }elseif($re_level == 1){
                        //是否有权限内的媒体是省级的
                        if(!empty($is_show_sj)){
                            $map['tregion.fid'] = substr($user['regionid'],0,2).'0000';
                            $illegal_map['tregion.fid'] = substr($user['regionid'],0,2).'0000';
                        }else{
                            $map['tregion.fid'] = $regionid;
                            $illegal_map['tregion.fid'] = $regionid;
                        }
                    }elseif($re_level < 5 && $re_level > 1 && !empty($is_show_sj)){
                        //是否有权限内的媒体是省级的
                        $map['tregion.fid'] = substr($user['regionid'],0,2).'0000';
                        $illegal_map['tregion.fid'] = substr($user['regionid'],0,2).'0000';
                    }
                    break;
                case 2:
                    if($re_level == 0){
                        //全国各副省级市违法线索情况
                        $map["tregion.flevel"] = 2;
                        $illegal_map["tregion.flevel"] = 2;
                    }elseif($re_level == 1){
                        //本省各副省级市违法线索情况
                        if($is_have_self == 1){
                            $map["tregion.flevel"] = ['IN','1,2'];
                            $illegal_map["tregion.flevel"] = ['IN','1,2'];
                            $map['tregion.fpid|tregion.fid'] = $regionid;
                            $illegal_map['tregion.fpid|tregion.fid'] = $regionid;
                        }else{
                            $map["tregion.flevel"] = 2;
                            $illegal_map["tregion.flevel"] = 2;
                            $map["tregion.fpid"] = $regionid;
                            $illegal_map["tregion.fpid"] = $regionid;
                        }
                    }elseif($re_level == 2){
                        $map["tregion.fpid"] = $regionid;
                        $illegal_map["tregion.fpid"] = $regionid;
                    }
                    break;
                case 3://各计划单列市
                    if($re_level == 0){
                        //全国各计划单列市违法线索情况
                        $map["tregion.flevel"] = 3;
                        $illegal_map["tregion.flevel"] = 3;
                    }elseif($re_level == 1){
                        //本省各计划单列市违法线索情况
                        if($is_have_self == 1){
                            $map["tregion.flevel"] = ['IN','1,3'];
                            $illegal_map["tregion.flevel"] = ['IN','1,3'];
                            $map['tregion.fpid|tregion.fid'] = $regionid;
                            $illegal_map['tregion.fpid|tregion.fid'] = $regionid;
                        }else{
                            $map["tregion.flevel"] = 3;
                            $illegal_map["tregion.flevel"] = 3;
                            $map["tregion.fpid"] = $regionid;
                            $illegal_map["tregion.fpid"] = $regionid;
                        }

                    }elseif($re_level == 3){
                        //$fpid = substr($regionid,0,2).'0000';
                        $map["tregion.fpid"] = $regionid;
                        $illegal_map["tregion.fpid"] = $regionid;
                    }
                    break;
                case 4://各市
                    if($re_level == 0){
                        //如果是全国各市违法线索情况
                        $map["tregion.flevel"] = 4;
                        $illegal_map["tregion.flevel"] = 4;
                    }elseif($re_level == 1){
                        //本省各市违法线索情况
                        if($is_have_self == 1){
                            $map['tregion.fpid|tregion.fid'] = $regionid;
                            $illegal_map['tregion.fpid|tregion.fid'] = $regionid;
                        }else{
                            $map["tregion.fpid"] = $regionid;
                            $illegal_map["tregion.fpid"] = $regionid;
                        }
                    }elseif($re_level == 4){
                        $map["tregion.fpid"] = $regionid;
                        $illegal_map["tregion.fpid"] = $regionid;
                    }elseif(!empty($is_show_sj) && $re_level == 5){
                        //是否有权限内的媒体是市级的
                        $map['tregion.fid'] = substr($user['regionid'],0,4).'00';
                        $illegal_map['tregion.fid'] = substr($user['regionid'],0,4).'00';
                    }
                    break;
                case 5://各区县
                    if($re_level == 0){
                        //全国各区县违法线索情况
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }elseif($re_level == 1){
                        //本省各区县违法线索情况
                        $fpid = substr($regionid,0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                        //本市各区县违法线索情况
                        if($is_have_self == 1){
                            $map['tregion.fpid|tregion.fid'] = $regionid;
                            $illegal_map['tregion.fpid|tregion.fid'] = $regionid;
                        }else{
                            $fpid = substr($regionid,0,4);
                            $map["tregion.fpid"] = ['like',$fpid.'%'];
                            $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                            $map["tregion.flevel"] = 5;
                            $illegal_map["tregion.flevel"] = 5;
                        }
                    }elseif($re_level == 5){
                        $fpid = substr($regionid,0,4);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }
                    break;
            }
        }
        else{

            $region = I('region','');//区域
            //如果没有选择非副省级省会城市
            if($region != 6){

                $map["tregion.flevel"] = ['IN','1,2,3'];//限制只看省 副省级市 计划市

                //判断是否国家局系统
                $gjj_label_media = $this->get_gjj_label_media();

                $map[$summary_table.'.fmediaid'] = array('in',$gjj_label_media);//国家局标签媒体
                $map[$summary_table.'.confirm_state'] = ['GT',0];//确认状态大于0
                $map[$summary_table.'.fcustomer'] = '100000';//客户编号
                /*违法相关*/
                $illegal_map['tbn_illegal_ad.fcustomer'] = '100000';//客户编号
                $illegal_map['tbn_illegal_ad_issue.fmedia_id'] = array('in',$gjj_label_media);//国家局标签媒体
                //是否显示发布前数据
                if($is_show_fsend == 1){
                    $illegal_map['tbn_illegal_ad_issue.fsend_status'] = ['IN',[1,2]];//
                }else{
                    $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
                }
            }else{
                $illegal_map['tbn_illegal_ad.fcustomer'] = '90100100';//客户编号
                if($is_show_fsend != 1){
                    $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
                }
            }


            switch ($region){
                case 0:
                    $map["tregion.flevel"] = ['IN',[1,2,3]];
                    $illegal_map["tregion.flevel"] = ['IN',[1,2,3]];
                    break;
                case 1://省级和国家级的各省监测情况
                    /* if($re_level == 0 || $re_level == 1 || $re_level == 3){
                       $map["tregion.flevel"] = 1;
                    }*/
                    $map["tregion.flevel"] = 1;
                    $illegal_map["tregion.flevel"] = 1;
                    break;
                case 2://副省级市
                    $map["tregion.flevel"] = 2;
                    $illegal_map["tregion.flevel"] = 2;
                    break;
                case 6://非副省级省会城市
                    $map["tregion.flevel"] = 4;
                    $map["tregion.fid"] = ['like',"%0100"];
                    $illegal_map["tregion.flevel"] = 4;
                    $illegal_map["tregion.fid"] = ['like',"%0100"];
                    break;
                case 3://各计划单列市
                    $map["tregion.flevel"] = 3;
                    $illegal_map["tregion.flevel"] = 3;
                    break;
                case 4://各市
                    if($re_level == 0){
                        //国家级用户的各市监测情况
                        $map["tregion.flevel"] = 4;
                        $illegal_map["tregion.flevel"] = 4;
                    }else{
                        //市级或者省级用户的各市监测情况
                        if($re_level == 4 || $re_level == 2 || $re_level == 3){
                            $fpid = substr($user['regionid'],0,2).'0000';
                            $map["tregion.fpid"] = $fpid;
                            $illegal_map["tregion.fpid"] = $fpid;
                        }elseif($re_level == 1){
                            $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                            $illegal_map['tregion.fpid|tregion.fid'] = $user['regionid'];
                        }
                    }
                    break;
                case 5://各区县
                    if($re_level == 0){
                        //如果是全国用户
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }elseif($re_level == 1){
                        //如果是省级用户
                        $fpid = substr($user['regionid'],0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                        //$map['tregion.fpid|tregion.fid'] = $user['regionid'];
                        //$illegal_map['tregion.fpid|tregion.fid'] = $user['regionid'];
                        $map['tregion.fpid'] = $user['regionid'];
                        $illegal_map['tregion.fpid'] = $user['regionid'];
                        $map["tregion.flevel"] = 5;
                        $illegal_map['tregion.flevel'] = 5;
                    }elseif($re_level == 5){
                        $fpid = substr($user['regionid'],0,4);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                        $illegal_map["tregion.flevel"] = 5;
                    }
                    break;
            }
        }
        //浏览权限设置end


        //媒介类型
        $media_class = I('media_class','');
        if($media_class != ''){
            $map[$summary_table.'.fmedia_class_code'] = $media_class;//媒体
            $illegal_map['tbn_illegal_ad.fmedia_class'] = intval($media_class);
        }

        //媒体细类
        /*        $media_son_class = I('media_son_class','');//媒体细类
                if($media_son_class != ''){
                    $map[$summary_table.'.fmedia_class_code'] = ['like',$media_son_class.'%'];//媒体细类
                }*/

        $ad_class = I('ad_class');//广告大类I('ad_class',"")
        if(!empty($ad_class)){
            foreach ($ad_class as $ad_class_key=>$ad_class_val){
                if($ad_class_val < 10){
                    $ad_class[$ad_class_key] = '0'.$ad_class_val;
                }
            }
            $map[$summary_table.'.fad_class_code'] = ['IN',$ad_class];//广告类别
            $illegal_map['left(tbn_illegal_ad.fad_class_code,2)'] = ['IN',$ad_class];//广告类别
        }


        $pmzd = $onther_class;
        if($onther_class == ''){
            if($fztj == 'fad_class_code'){
                //如果是广告类别排名的话,默认条次占比排名
                $onther_class =  'fad_illegal_times';
            }else{
                //如果是其他的话,默认条次评分排名
                $onther_class =  'fad_times';
                $pmzd = 'fad_times';
            }
        }
        //如果是地域排名 按行政区划排名
        $onther_class_val = $onther_class;
        if($fztj == 'fregionid' && $region_order == 1){
            $onther_class =  'forder';
        }

        $data = [];//定义统计数据空数组

        //按照选定时间实例化表
        $table_model = M($summary_table);

        $id = 'fid';//定义默认表ID
        $name = 'fname';//定义默认单元名称

        //匹配分组条件，连接不同表
        $tregion_name = "tregion.fname1 as tregion_name";
        $group_field = $summary_table.'.'.$fztj;
        $group_by = $summary_table.'.'.$fztj;
        switch ($fztj){
            case 'fmedia_class_code':
                $join_table = 'tmediaclass';//fmedia_class_code媒介类别表fid,
                $name = 'fclass';
                $xs_group = 'tbn_illegal_ad.fmedia_class';
                $xs_group_field = 'tbn_illegal_ad.fmedia_class';
                $group_field .= ",$summary_table.fmedia_class_code as fmedia_class_code,tmediaclass.fclass as tmediaclass_name";
                $hb_field = 'fmedia_class';
                break;
            case 'fmediaownerid':
                $join_table = 'tmediaowner';//fmediaownerid媒介机构表fid
                $xs_group = 'tbn_illegal_ad.fmediaownerid';
                $xs_group_field = 'tbn_illegal_ad.fmediaownerid';
                $hb_field = 'fmediaownerid';
                $group_field .= ',tmediaowner.fname as tmediaowner_name,'.$summary_table.'.fmediaownerid as fmediaownerid';
                $menu = '媒介机构';
                break;
            case 'fregionid':
                $join_table = 'tregion';//fregionid行政区划表fid
                $hb_field = 'fregion_id';
                $field_name = 'tregion_name';
                $menu = '地域';
                if($region == 0){
                    $xs_group_field = "CONCAT(left(tbn_illegal_ad.fregion_id,2),'0000') as fregion_id";//1102
                    $xs_group = "left(tbn_illegal_ad.fregion_id,2)";
                    $tregion_name = "(SELECT tregion.fname1 FROM tregion WHERE tregion.fid = CONCAT(left(tbn_ad_summary_day_z.fregionid,2),'0000') LIMIT 1) AS tregion_name";
                    $group_field = 'CONCAT(left(tbn_ad_summary_day_z.fregionid,2),"0000") as fregionid,'.$join_table.'.forder,'.$tregion_name;
                    $group_by = 'left(tbn_ad_summary_day_z.fregionid,2)';
                }else{
                    $group_field = $summary_table.'.'.$fztj.','.$join_table.'.forder';
                    $xs_group = 'tbn_illegal_ad.fregion_id';
                    $xs_group_field = 'tbn_illegal_ad.fregion_id';
                    $group_field .= ",".$tregion_name;
                }
                break;
            case 'fmediaid':
                $join_table = 'tmedia';//fmediaid媒介表fid
                $name = 'fmedianame';
                $xs_group = 'tbn_illegal_ad.fmedia_id';
                $xs_group_field = 'tbn_illegal_ad.fmedia_id';
                $hb_field = 'fmedia_id';
                $group_field .= ',(case when instr(tmedia.fmedianame,\'（\') > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,\'（\') -1) else tmedia.fmedianame end) as tmedia_name,'.$summary_table.'.fmediaid as fmediaid';
                $menu = '媒体';
                break;
            case 'fad_class_code':
                $join_table = 'tadclass';//fad_class_code广告内容类别表fcode
                $id = 'fcode';
                $name = 'fadclass';
                $xs_group = 'left(tbn_illegal_ad.fad_class_code,2)';
                $xs_group_field = 'left(tbn_illegal_ad.fad_class_code,2) as fad_class_code';
                $hb_field = 'fad_class_code';
                $group_field .= ','.$summary_table.'.fad_class_code as fad_class_code,tadclass.fadclass as tadclass_name';
                $menu = '广告类别';
                $field_name = 'tadclass_name';
                break;
        }

        //违法广告条次与时长
        //是否包含违法长广告时长
        if($is_show_longad != 1){
            //$illegal_map['tbn_illegal_ad.is_long_ad'] = 0;
            $illegal_map['_string'] = '(unix_timestamp(tbn_illegal_ad_issue.fendtime) - unix_timestamp(tbn_illegal_ad_issue.fstarttime) < 600 OR tbn_illegal_ad.fmedia_class = 3)';

        }

        //审核通过的违法数据
        $isexamine = $ALL_CONFIG['isexamine'];
        if(in_array($isexamine, [10,20,30,40,50])){
            $illegal_map['tbn_illegal_ad.fexamine'] = 10;
        }

        $illegal_group = 'tbn_illegal_ad.'.$xs_group;
        $tbn_illegal_data = M('tbn_illegal_ad_issue')
            ->field("
              $xs_group_field,
              COUNT(DISTINCT tbn_illegal_ad.fsample_id) AS fad_illegal_count,
              SUM(
                UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fendtime
                ) - UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fstarttime
                )
                ) AS fad_illegal_play_len,
              COUNT(1) AS fad_illegal_times,
            COUNT(
                DISTINCT (
                    CASE
                    WHEN tbn_illegal_ad.fstatus > 0 THEN
                        tbn_illegal_ad.fid
                    END
                )
            ) AS cl_count,
         COUNT(
            DISTINCT (
                CASE
                WHEN tbn_illegal_ad.fview_status > 0 THEN
                    tbn_illegal_ad.fid
                END
            )
        ) AS ck_count,
         COUNT(DISTINCT tbn_illegal_ad.fid) AS all_count
            ")
            ->where($illegal_map)
            ->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tadclass on 
                tadclass.fcode = tbn_illegal_ad.fad_class_code "
            )
            ->group($xs_group)
            ->select();
        $data = $table_model
            ->cache(true,600)
            ->field("
                $group_field,
                tregion.flevel as flevel,
                GROUP_CONCAT($fad_count_field SEPARATOR ',') as fad_sam_list,
                sum($summary_table.$fad_times_field) as fad_times,
                sum($summary_table.$play_len_field) as fad_play_len
           ")
            ->join("
                tmediaclass on 
                tmediaclass.fid = $summary_table.fmedia_class_code"
            )
            ->join("
                tmediaowner on 
                tmediaowner.fid = $summary_table.fmediaownerid"
            )
            ->join("
                tregion on 
                tregion.fid = $summary_table.fregionid"
            )
            ->join("
                tmedia on 
                tmedia.fid = $summary_table.fmediaid"
            )
            ->join("
                tadclass on 
                tadclass.fcode = $summary_table.fad_class_code"
            )
            ->where($map)
            ->group($group_by)
            ->select();

        //循环计算条数
        foreach ($data as $data_key => $data_val){
            $sam_array = explode(',',$data_val['fad_sam_list']);
            $fad_count = count(array_unique($sam_array));
            unset($data[$data_key]['fad_sam_list']);
            $data[$data_key]['fad_count'] = $fad_count;//插入条数
        }

        //组合违法线索相关数据
        foreach ($tbn_illegal_data as $tbn_illegal_data_key=>$tbn_illegal_data_value){
            foreach ($data as $data_key=>$data_value){
                if($tbn_illegal_data_value[$hb_field] == $data_value[$fztj]){
                    unset($tbn_illegal_data_value[$hb_field]);
                    $data[$data_key]['counts_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_count']/$data_value['fad_count'];//条数违法率
                    $data[$data_key]['fad_illegal_count'] = $tbn_illegal_data_value['fad_illegal_count'];//违法条数
                    $data[$data_key]['times_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_times']/$data_value['fad_times'];//条次违法率
                    $data[$data_key]['lens_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_play_len']/$data_value['fad_play_len'];//时长违法率
                    $data[$data_key]['all_count'] = $tbn_illegal_data_value['all_count'];//违法广告条数
                    $data[$data_key]['ck_count'] = $tbn_illegal_data_value['ck_count'];//查看量
                    $data[$data_key]['ckl'] = $tbn_illegal_data_value['ck_count']/$tbn_illegal_data_value['all_count'];//查看率
                    $data[$data_key]['cl_count'] = $tbn_illegal_data_value['cl_count'];//处理量
                    $data[$data_key]['cll'] = $tbn_illegal_data_value['cll']/$tbn_illegal_data_value['all_count'];//处理率
                    $data[$data_key] = array_merge($tbn_illegal_data_value, $data[$data_key]);//合并数组
                }
            }
        }
        $cl_count_all = 0;//线索全部数量
        $ck_count_all = 0;//线索全部数量
        $count_all = 0;//总数
        //循环计算
        foreach ($data as $data_key => $data_val){
            if(!isset($data_val['fad_illegal_count'])){
                $data[$data_key]['fad_illegal_count'] = 0;
            }
            if(!isset($data_val['counts_illegal_rate'])){
                $data[$data_key]['counts_illegal_rate'] = 0;
            }

            if(!isset($data_val['fad_illegal_times'])){
                $data[$data_key]['fad_illegal_times'] = 0;
            }
            if(!isset($data_val['times_illegal_rate'])){
                $data[$data_key]['times_illegal_rate'] = 0;
            }
            if(!isset($data_val['fad_illegal_play_len'])){
                $data[$data_key]['fad_illegal_play_len'] = 0;
            }
            if(!isset($data_val['lens_illegal_rate'])){
                $data[$data_key]['lens_illegal_rate'] = 0;
            }
            /*            $all_count['fad_count'] += $data[$data_key]['fad_count'];
                        $all_count['fad_illegal_count'] += $data[$data_key]['fad_illegal_count'];
                        $all_count['fad_illegal_times'] += $data[$data_key]['fad_illegal_times'];
                        $all_count['fad_times'] += $data[$data_key]['fad_times'];
                        $all_count['fad_illegal_play_len'] += $data[$data_key]['fad_illegal_play_len'];
                        $all_count['fad_play_len'] += $data[$data_key]['fad_play_len'];
                        $cl_count_all += $tbn_illegal_data_value['cl_count'];//线索全部数量
                        $ck_count_all += $tbn_illegal_data_value['ck_count'];//线索全部数量
                        $count_all += $tbn_illegal_data_value['all_count'];//总数*/
        }

        //如果是地域排名 按行政区划排名
        if($fztj == 'fregionid' && $region_order == 1){
            $data = $this->pxsf($data,$onther_class,false);//顺序排序
        }else{
            $data = $this->pxsf($data,$onther_class);//倒叙排序
        }
        $array_num = 0;//
        $limit = I('limit',6);//定义限定数量
        $data_pm = [];//定义图表数据空数组
        $data_count = count($data);//获取数据条数

        $times_num = 0;

        //处理排序后的数据
        foreach ($data as $data_key=>$data_value){
            $times_num++;
            $data[$data_key]['counts_illegal_rate'] = round($data_value['counts_illegal_rate'],4)*100;
            $data[$data_key]['times_illegal_rate'] = round($data_value['times_illegal_rate'],4)*100;
            $data[$data_key]['lens_illegal_rate'] = round($data_value['lens_illegal_rate'],4)*100;
            $data[$data_key]['cll'] = round($data_value['cll'],4)*100;
            $data[$data_key]['ckl'] = round($data_value['ckl'],4)*100;

            //循环获取前几条数据
            if($array_num < $limit && $array_num < count($data) && $data_value[$onther_class] > 0){
                $data_pm['name'][] = $data_value[$join_table.'_name'];
                //不同排名条件显示各自的数值
                switch ($onther_class){
                    case 'forder':
                        $data_pm['num'][] = $data[$data_key][$onther_class_val];
                        break;
                    case 'fad_illegal_times':
                        $data_pm['num'][] = $data_value['fad_illegal_times'];//违法条次
                        break;
                    case 'times_illegal_rate':
                        $data_pm['num'][] = round($data_value['times_illegal_rate'],4)*100;//条次违法率
                        break;
                    case 'fad_times':
                        $data_pm['num'][] = $data[$data_key]['fad_times'];//条次
                        break;
                    case 'fad_illegal_count':
                        $data_pm['num'][] = $data[$data_key]['fad_illegal_count'];//条数
                        break;
                    case 'counts_illegal_rate':
                        $data_pm['num'][] = $data[$data_key]['counts_illegal_rate'];//条数违法率
                        break;
                    case 'fad_count':
                        $data_pm['num'][] = $data[$data_key]['fad_count'];//条数
                        break;
                    case 'fad_play_len':
                        $data_pm['num'][] = $data[$data_key]['fad_play_len'];//时长
                        break;
                    case 'fad_illegal_play_len':
                        $data_pm['num'][] = $data_value['fad_illegal_play_len'];//违法时长
                        break;
                    case 'lens_illegal_rate':
                        $data_pm['num'][] = round($data_value['lens_illegal_rate'],4)*100;//时长违法率
                        break;
                }
                $array_num++;//fad_count  广告条次：fad_times  广告时长：fad_play_len
            }

            //获取排名
            if($ranking == 1 && $user['regionid'] == $data_value['fregionid'] && $fztj == 'fregionid'){
                S('rank_number_'.$onther_class_val.$table_condition_str,$times_num,600);
                $rank_res = [
                    'code'=> 0,
                    'msg'=>"获取成功！",
                    'rank_number'=>$times_num
                ];
                $this->ajaxReturn($rank_res);
            }
        }

        //组合广告类别百分比图表数据
        $ad_class_num = 0;
        if($fztj == 'fad_class_code') {
            $data_pm = [];
            $data = $this->pxsf($data, $onther_class);//排序
            foreach ($data as $ad_class_key=>$ad_class_value){
                if($ad_class_value[$onther_class] != 0 && $ad_class_num < $limit){
                    $data_pm['data'][] = [
                        'name' => $ad_class_value[$join_table.'_name'],
                        'value' => $ad_class_value[$onther_class]
                    ];
                    $data_pm['name'][] = $ad_class_value[$join_table.'_name'];
                }
                $ad_class_num++;
            }
        }

        //统计放在最后一条$all_count     $prv_all_count
        /*        $data[] = [
                    'all_xs_count' =>$count_all,
                    'titlename'=>'全部',
                    "cll"=>(round($cl_count_all/$count_all,4)*100).'%',
                    "cl_count" =>   $cl_count_all,
                    "ck_count"=>    $ck_count_all,
                    "ckl"=>(round($ck_count_all/$count_all,4)*100).'%',
                    'fad_count'=>$all_count['fad_count'],//广告条数
                    'fad_illegal_count'=>$all_count['fad_illegal_count'],//违法广告数量
                    'counts_illegal_rate'=>round($all_count['fad_illegal_count']/$all_count['fad_count'],4)*100,
                    'fad_times'=>$all_count['fad_times'],//广告条次
                    'fad_illegal_times'=>$all_count['fad_illegal_times'],//违法广告条次
                    'times_illegal_rate'=>round($all_count['fad_illegal_times']/$all_count['fad_times'],4)*100,
                    'fad_play_len'=>$all_count['fad_play_len'],//总时长
                    'fad_illegal_play_len'=>$all_count['fad_illegal_play_len'],//违法时长
                    'lens_illegal_rate'=>round($all_count['fad_illegal_play_len']/$all_count['fad_play_len'],4)*100,
                ];*/

        $data_page = arr_page($data,$arr_p,$arr_count);
        $data_page = array_values($data_page);
        if(empty($data_pm)){
            $data_pm = [
                'num'=>[],
                'name'=>[]
            ];
        }
        if(empty($data_page)){
            $data_page = [];
        }
        $finally_data = [
            'count'=> count($data),//总数
            'data_pm'=>$data_pm,//图表数据
            'data_list'=>$data_page//列表数据
        ];

        $this->ajaxReturn($finally_data);//app

    }

    //汇总报告生成
    public function jchz_out_word($menu,$field_name,$title,$data,$date_cycle,$s_time,$e_time,$regio_area){
        $system_num = getconfig('system_num');

        $array_dimension = [
            'fad_times' => '广告发布条次',
            'fad_illegal_times' => '广告违法发布条次',
            'times_illegal_rate' => '广告条次违法率',
            'fad_play_len' => '广告时长',
            'fad_illegal_play_len' => '广告违法时长',
            'lens_illegal_rate' => '广告时长违法率'
        ];

        $count = count($data);
        $data_list_chart[] = ["地域名称",$title_txt];
        $data_table_list = [];
        foreach ($data as $key=>$data_val){

            if($key != ($count - 1) && $count != 1){

                foreach ($array_dimension as $array_dimension_key=>$array_dimension_val){
                    $list_chart_data[$array_dimension_key][] = [$data_val[$field_name],strval($data_val[$array_dimension_key])];//条状图和占比图
                }

                $data_table_list[] = [
                    $data_val[$field_name],
                    strval($data_val['fad_times']),//1
                    strval($data_val['fad_illegal_times']),//2
                    $data_val['times_illegal_rate'].'%',//3
                    strval($data_val['fad_play_len']),//4
                    strval($data_val['fad_illegal_play_len']),//5
                    $data_val['lens_illegal_rate'].'%',//6
                    $data_val['ck_count']?$data_val['ck_count']:'0',
                    $data_val['ckl']?$data_val['ckl']:'0%',
                    $data_val['cl_count']?$data_val['cl_count']:'0',
                    $data_val['cll']?$data_val['cll']:'0%'
                ];

            }
        }

        //根据所查字段排序
        foreach ($list_chart_data as $list_chart_data_key=>$list_chart_data_val){

            $list_chart_data_f[$list_chart_data_key] = $this->pxsf($list_chart_data_val,1,false);
        }
        $list_chart_data_f = [
            'fad_times' => $list_chart_data_f['fad_times'],
            'fad_illegal_times' => $list_chart_data_f['fad_illegal_times'],
            'times_illegal_rate' => $list_chart_data_f['times_illegal_rate'],
            'fad_play_len' => $list_chart_data_f['fad_play_len'],
            'fad_illegal_play_len' => $list_chart_data_f['fad_illegal_play_len'],
            'lens_illegal_rate' => $list_chart_data_f['lens_illegal_rate']];
        //组合图表数据
        foreach ($list_chart_data as $list_chart_data_val){
            $data_list_chart[] = $list_chart_data_val;
        }

        //列表根据所查字段排序
        //$data_table_list = $this->pxsf($data_table_list,$score_rule);
        //组合数据
        $data_list_table = [];
        foreach ($data_table_list as $data_table_list_key=>$data_table_list_val){
            $data_list_table[] = [
                strval(++$data_table_list_key),
                $data_table_list_val[0],
                $data_table_list_val[1],
                $data_table_list_val[2],
                $data_table_list_val[3],
                $data_table_list_val[4],
                $data_table_list_val[5],
                $data_table_list_val[6],
                $data_table_list_val[7],
                $data_table_list_val[8],
                $data_table_list_val[9],
                $data_table_list_val[10]
            ];
        }
        $user = session('regulatorpersonInfo');//获取用户信息

        $report_data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $report_data['content'][] = [
            "type" => "text",
            "bookmark" => "大标题",
            "text" => $user['regionname1']."工商行政管理局"
        ];

        $report_data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体二号",
            "text" => date('Y年m月d日',time())
        ];

        $report_data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $title
        ];

        $report_data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号数字序号标题",
            "text" => "一、".$regio_area."广告发布总情况及线索处理情况列表"
        ];

        $report_data['content'][] = [
            "type" => "table",
            "bookmark" => "涉嫌违法广告发布情况带时长和线索",
            "data" => $data_list_table
        ];

        $t_num = 0;
        foreach ($list_chart_data_f as $list_chart_data_f_key=>$list_chart_data_f_val){
            switch ($t_num){
                case 0:
                    $title_num = '二';
                    break;
                case 1:
                    $title_num = '三';
                    break;
                case 2:
                    $title_num = '四';
                    break;
                case 3:
                    $title_num = '五';
                    break;
                case 4:
                    $title_num = '六';
                    break;
                case 5:
                    $title_num = '七';
                    break;
            }
            $list_chart_data_array = [];
            $list_chart_data_array[] = ["",$array_dimension[$list_chart_data_f_key]];
            foreach ($list_chart_data_f_val as $list_chart_data_f_val_val){
                $list_chart_data_array[] = [
                    $list_chart_data_f_val_val[0],
                    $list_chart_data_f_val_val[1],
                ];
            }
            $report_data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号标题",
                "text" => $title_num."、".$regio_area.$array_dimension[$list_chart_data_f_key].'展示'
            ];
            $report_data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号数字序号标题",
                "text" => "1、".$regio_area.$array_dimension[$list_chart_data_f_key]."条形图"
            ];

            $report_data['content'][] = [
                "type" => "chart",
                "bookmark" => "广告量条形图",
                "data" => $list_chart_data_array
            ];//
            $report_data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号数字序号标题",
                "text" => "2、".$regio_area.$array_dimension[$list_chart_data_f_key]."占比图"
            ];

            $report_data['content'][] = [
                "type" => "chart",
                "bookmark" => "通用占比图",
                "data" => $list_chart_data_array
            ];//通用占比图
            $t_num++;
        }

        $report_data = json_encode($report_data);
        //echo $report_data;exit;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！错误信息：'.$response));
            }


            //将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regionname1'].$date_cycle.'汇总报告'.date('Y年m月d日 H时i分',time()).'生成';
            $data['pnname']             = $pnname;
            $data['pntype']             = 90;
            $data['pnfiletype']         = 10;
            $data['pnstarttime']        = $s_time;
            $data['pnendtime']          = $e_time;
            $data['pncreatetime']       = date('Y-m-d H:i:s');
            $data['pnurl']              = $response['ReportFileName'];
            $data['pnhtml']             = '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }
        }
    }

    //跨地域统计排名
    public function zones_ad(){
        session_write_close();
        $is_out_report = I('is_out_report',0);//确定是否点击导出EXCEL
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isrelease = $ALL_CONFIG['isrelease'];

        $system_num = '100000';
        $search_type = I('search_type');//查询类型
        $region_level = I('region_level');//数据级别
        $media_class = I('media_class');//媒体类型
        $ad_class = I('ad_class');//广告类别
        $is_show_fsend = I('is_show_fsend');//是否发布
        $is_show_longad = I('is_show_longad');//是否长广告

        if($is_show_fsend==1){
            $is_show_fsend = -2;
        }else{
            $is_show_fsend = 0;
        }

        //时间条件筛选
        $years      = I('year');//选择年份
        $times_table = I('times_table');
        $table_condition = I('table_condition');//选择时间
        if($times_table == 'tbn_ad_summary_day'){
            $timetypes = 0;
            $timeval = $table_condition;
        }elseif($times_table == 'tbn_ad_summary_week'){
            $timetypes = 10;
            $timeval = $table_condition;
        }elseif($times_table == 'tbn_ad_summary_half_month'){
            $timetypes = 20;
            $table_condition2 = explode('-', $table_condition);
            $table_condition3 = ((int)$table_condition2[0]-1)*2;
            $table_condition4 = ((int)$table_condition2[1])==1?1:2;
            $timeval = $table_condition3+$table_condition4;
        }elseif($times_table == 'tbn_ad_summary_month'){
            $timetypes = 30;
            $table_condition2 = explode('-', $table_condition);
            $timeval = (int)$table_condition2[0];
        }elseif($times_table == 'tbn_ad_summary_quarter'){
            $timetypes = 40;
            if($table_condition == '01-01'){
                $timeval = 1;
            }elseif($table_condition == '04-01'){
                $timeval = 2;
            }elseif($table_condition == '07-01'){
                $timeval = 3;
            }else{
                $timeval = 4;
            }
        }elseif($times_table == 'tbn_ad_summary_half_year'){
            $timetypes = 50;
            $table_condition2 = explode('-', $table_condition);
            if($table_condition2[2] == '01'){
                $timeval = 1;
            }else{
                $timeval = 2;
            }
        }elseif($times_table == 'tbn_ad_summary_year'){
            $timetypes = 60;
            $timeval = 1;
        }
        $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date',$is_show_fsend,$isrelease);

        if(!empty($region_level)){
            $tregionstr = ' and flevel = '.$region_level;
        }else{
            $tregionstr = ' and flevel in (1,2,3) ';
        }

        if(!empty($media_class)){

            if($system_num == "120000" && $media_class == "13"){
                $where_str .= ' and a.fmedia_class = -1 ';

            }else{
                if(!in_array(-1,$media_class)){

                    $media_class_int = [];
                    foreach ($media_class as $media_class_v){
                        $media_class_int[] = intval($media_class_v);
                    }
                    $where_str .= ' and a.fmedia_class IN('.join(',',$media_class_int).') ';
                }
            }

        }else{
            if($system_num == "120000"){
                $where_str .= ' and a.fmedia_class IN(1,2,3) ';

            }
        }


        if(!empty($ad_class)){
            $ad_class2 = [];
            foreach ($ad_class as $key => $value) {
                if($value<10){
                    $ad_class2[] = '0'.$value;
                }else{
                    $ad_class2[] = $value;
                }
            }
            $where_str .= ' and left(a.fad_class_code,2) in ('.implode(',', $ad_class2).')';
        }
        if(empty($is_show_longad)){
            $where_str .= ' and (unix_timestamp(fendtime) - unix_timestamp(fstarttime) < 600 OR a.fmedia_class > 2)';
        }

        if($search_type == 1){
            $sqlstr = '
                select n.fname1,n.fid as rid,count(distinct(a.fad_name)) as fadcount,group_concat(distinct(a.fad_name)) as fadnames,fmedianames
                from tbn_illegal_ad a
                inner join tbn_illegal_ad_issue m on a.fid=m.fillegal_ad_id
                inner join tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id
                inner join tregion n on a.fregion_id = n.fid '.$tregionstr.'
                inner join (select fad_name,group_concat(distinct(fmedianames)) as fmedianames from (select a.fad_name,b.fname1,group_concat(distinct(d.fmedianame)) as fmedianames from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id inner join tmedia d on a.fmedia_id = d.fid where '.$where_time.' and fcustomer = '.$system_num.' group by left(a.fregion_id,2),a.fad_name ) as a group by a.fad_name having count(1)>1) k on a.fad_name = k.fad_name 
                where '.$where_time.$where_str.' and fcustomer = '.$system_num.'
                group by a.fregion_id
                order by fadcount desc
            ';
        }elseif($search_type == 2){
            $sqlstr = '
                select c.fmedianame,c.fid as mid,count(distinct(a.fad_name)) as fadcount,group_concat(distinct(a.fad_name)) as fadnames
                from tbn_illegal_ad a
                inner join tbn_illegal_ad_issue m on a.fid=m.fillegal_ad_id
                inner join tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id
                inner join tregion n on a.fregion_id = n.fid '.$tregionstr.'
                inner join (select fad_name from (select a.fad_name,b.fname1 from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where '.$where_time.' and fcustomer = '.$system_num.' group by left(a.fregion_id,2),a.fad_name ) as a group by a.fad_name having count(1)>1) k on a.fad_name = k.fad_name 
                where '.$where_time.$where_str.' and fcustomer = '.$system_num.'
                group by a.fmedia_id
                order by fadcount desc
            ';
        }else{
            $sqlstr = '
                select a.fad_name,fregion_count,fregionname,fregionids,b.fadclass,fmedianames
                from tbn_illegal_ad a
                inner join tbn_illegal_ad_issue m on a.fid=m.fillegal_ad_id
                inner join tadclass b on left(a.fad_class_code,2)=b.fcode
                inner join tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id
                inner join tregion n on a.fregion_id = n.fid '.$tregionstr.'
                inner join (select fad_name,count(1) as fregion_count,group_concat(fname1) as fregionname,group_concat(fid) as fregionids,group_concat(distinct(fmedianames)) as fmedianames from (select a.fad_name,b.fname1,b.fid,group_concat(distinct(d.fmedianame)) as fmedianames from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id inner join tmedia d on a.fmedia_id = d.fid where '.$where_time.' and fcustomer = '.$system_num.' group by left(a.fregion_id,2),a.fad_name ) as a group by a.fad_name having count(1)>1) k on a.fad_name = k.fad_name 
                where '.$where_time.$where_str.' and fcustomer = '.$system_num.'
                group by a.fad_name
                order by fregion_count desc
            ';
        }

        $data_list = M()->query($sqlstr);

        if($is_out_report == 1){
            $objPHPExcel = new \PHPExcel();
            $objPHPExcel
                ->getProperties()  //获得文件属性对象，给下文提供设置资源
                ->setCreator( "MaartenBalliauw")             //设置文件的创建者
                ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
                ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
                ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
                ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
                ->setKeywords( "office 2007 openxmlphp")        //设置标记
                ->setCategory( "Test resultfile");                //设置类别

            $sheet = $objPHPExcel->setActiveSheetIndex(0);

            if($times_table == 'tbn_ad_summary_day'){
                $title = explode(',', $timeval);
                $titlestr = $title[0].'至'.$title[1];
            }elseif($times_table == 'tbn_ad_summary_week'){
                $titlestr = $years.'年第'.$timeval.'周';
            }elseif($times_table == 'tbn_ad_summary_half_month'){
                $titlestr = $years.'年'.$table_condition3.'月';
                if($table_condition4 == 0){
                    $titlestr .= '上半月';
                }else{
                    $titlestr .= '下半月';
                }
            }elseif($times_table == 'tbn_ad_summary_month'){
                $titlestr = $years.'年'.$table_condition2[0].'月';
            }elseif($times_table == 'tbn_ad_summary_quarter'){
                $titlestr = $years.'年第'.$timeval.'季度';
            }elseif($times_table == 'tbn_ad_summary_half_year'){
                if($table_condition == '01-01'){
                    $titlestr = $years.'年上半年';
                }else{
                    $titlestr = $years.'年下半年';
                }
            }elseif($times_table == 'tbn_ad_summary_year'){
                $titlestr = $years.'年全年';
            }

            $web_logowz = $ALL_CONFIG['web_logowz'];//平台名称
            $web_logowz = str_replace('<br>', '', $web_logowz);
            if($search_type == 1){
                $titlestr = '跨地域违法广告-地域排名（'.$titlestr.'）';
                $titlestr2 = '跨地域违法广告地域排名';
                $sheet -> mergeCells('A1:E1');
                $sheet ->getStyle('A1:E1')->applyFromArray(array('font' => array ('bold' => true),'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
                $sheet ->setCellValue('A1',$titlestr);

                $sheet ->setCellValue('A1','排名');
                $sheet ->setCellValue('B1','地域名称');
                $sheet ->setCellValue('C1','违法广告数量');
                $sheet ->setCellValue('D1','违法广告名称');
                $sheet ->setCellValue('E1','播出媒体');
                foreach ($data_list as $key => $value) {
                    $sheet ->setCellValue('A'.($key+3),($key+1));
                    $sheet ->setCellValue('B'.($key+3),$value['fname1']);
                    $sheet ->setCellValue('C'.($key+3),$value['fadcount']);
                    $sheet ->setCellValue('D'.($key+3),$value['fad_name']);
                    $sheet ->setCellValue('E'.($key+3),$value['fmedianames']);
                }
            }elseif($search_type == 2){
                $titlestr = '跨地域违法广告-媒体排名（'.$titlestr.'）';
                $titlestr2 = '跨地域违法广告媒体排名';
                $sheet -> mergeCells('A1:D1');
                $sheet ->getStyle('A1:D1')->applyFromArray(array('font' => array ('bold' => true),'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
                $sheet ->setCellValue('A1',$titlestr);

                $sheet ->setCellValue('A1','排名');
                $sheet ->setCellValue('B1','媒体名称');
                $sheet ->setCellValue('C1','违法广告数量');
                $sheet ->setCellValue('D1','违法广告名称');
                foreach ($data_list as $key => $value) {
                    $sheet ->setCellValue('A'.($key+3),($key+1));
                    $sheet ->setCellValue('B'.($key+3),$value['fmedianame']);
                    $sheet ->setCellValue('C'.($key+3),$value['fadcount']);
                    $sheet ->setCellValue('D'.($key+3),$value['fadnames']);
                }
            }else{
                $titlestr = '跨地域违法广告-广告排名（'.$titlestr.'）';
                $titlestr2 = '跨地域违法广告广告排名';
                $sheet -> mergeCells('A1:F1');
                $sheet ->getStyle('A1:F1')->applyFromArray(array('font' => array ('bold' => true),'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
                $sheet ->setCellValue('A1',$titlestr);

                $sheet ->setCellValue('A2','排名');
                $sheet ->setCellValue('B2','广告名称');
                $sheet ->setCellValue('C2','广告类别');
                $sheet ->setCellValue('D2','跨地域数量');
                $sheet ->setCellValue('E2','播出地域');
                $sheet ->setCellValue('F2','播出媒体');
                foreach ($data_list as $key => $value) {
                    $sheet ->setCellValue('A'.($key+3),($key+1));
                    $sheet ->setCellValue('B'.($key+3),$value['fad_name']);
                    $sheet ->setCellValue('C'.($key+3),$value['fadclass']);
                    $sheet ->setCellValue('D'.($key+3),$value['fregion_count']);
                    $sheet ->setCellValue('E'.($key+3),$value['fregionname']);
                    $sheet ->setCellValue('F'.($key+3),$value['fmedianames']);
                }
            }

            $objActSheet =$objPHPExcel->getActiveSheet();
            $objActSheet->setTitle($titlestr2);//表格设置名称
            $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $date = date('Ymdhis');
            $savefile = './Public/Xls/'.$date.'.xlsx';
            $objWriter->save($savefile);


            $ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='.$web_logowz.$titlestr.'.xlsx'));//上传云
            unlink($savefile);//删除文件

            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$data_list));
        }
    }

    //跨地域违法广告列表
    public function kdyad_illegal_list(){
        session_write_close();
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isrelease = $ALL_CONFIG['isrelease'];

        $system_num = 100000;
        $isrelease = 1;
        $search_type = I('search_type');//查询类型
        $region_level = I('region_level');//数据级别
        $media_class = I('media_class');//媒体类型
        $ad_class = I('ad_class');//广告类别
        $is_show_fsend = I('is_show_fsend');//是否发布
        $is_show_longad = I('is_show_longad');//是否长广告
        $fadname = I('fadname');//广告名称
        $fmediaid = I('fmediaid');//媒体ID
        $tregionid = I('tregionid');//地域ID

        if($is_show_fsend==1){
            $is_show_fsend = -2;
        }else{
            $is_show_fsend = 0;
        }

        //时间条件筛选
        $years      = I('year');//选择年份
        $times_table = I('times_table');
        $table_condition = I('table_condition');//选择时间
        if($times_table == 'tbn_ad_summary_day'){
            $timetypes = 0;
            $timeval = $table_condition;
        }elseif($times_table == 'tbn_ad_summary_week'){
            $timetypes = 10;
            $timeval = $table_condition;
        }elseif($times_table == 'tbn_ad_summary_half_month'){
            $timetypes = 20;
            $table_condition2 = explode('-', $table_condition);
            $table_condition3 = ((int)$table_condition2[0]-1)*2;
            $table_condition4 = ((int)$table_condition2[1])==1?1:2;
            $timeval = $table_condition3+$table_condition4;
        }elseif($times_table == 'tbn_ad_summary_month'){
            $timetypes = 30;
            $table_condition2 = explode('-', $table_condition);
            $timeval = (int)$table_condition2[0];
        }elseif($times_table == 'tbn_ad_summary_quarter'){
            $timetypes = 40;
            if($table_condition == '01-01'){
                $timeval = 1;
            }elseif($table_condition == '04-01'){
                $timeval = 2;
            }elseif($table_condition == '07-01'){
                $timeval = 3;
            }else{
                $timeval = 4;
            }
        }elseif($times_table == 'tbn_ad_summary_half_year'){
            $timetypes = 50;
            $table_condition2 = explode('-', $table_condition);
            if($table_condition2[2] == '01'){
                $timeval = 1;
            }else{
                $timeval = 2;
            }
        }elseif($times_table == 'tbn_ad_summary_year'){
            $timetypes = 60;
            $timeval = 1;
        }
        $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date',$is_show_fsend,$isrelease);

        if(!empty($region_level)){
            $tregionstr = ' and flevel = '.$region_level;
        }else{
            $tregionstr = ' and flevel in (1,2,3) ';
        }
        $where_str = '1=1';

        if(!empty($media_class)){

            if($system_num == "120000" && $media_class == "13"){
                $where_str .= ' and a.fmedia_class = -1 ';

            }else{
                if(!in_array(-1,$media_class)){

                    $media_class_int = [];
                    foreach ($media_class as $media_class_v){
                        $media_class_int[] = intval($media_class_v);
                    }
                    $where_str .= ' and a.fmedia_class IN('.join(',',$media_class_int).') ';
                }
            }

        }else{
            if($system_num == "120000"){
                $where_str .= ' and a.fmedia_class IN(1,2,3) ';

            }
        }


        if(!empty($ad_class)){
            $ad_class2 = [];
            foreach ($ad_class as $key => $value) {
                if($value<10){
                    $ad_class2[] = '0'.$value;
                }else{
                    $ad_class2[] = $value;
                }
            }
            $where_str .= ' and left(a.fad_class_code,2) in ('.implode(',', $ad_class2).')';
        }
        if(empty($is_show_longad)){
            $where_str .= ' and (unix_timestamp(fendtime) - unix_timestamp(fstarttime) < 600 OR a.fmedia_class > 2)';
        }
        if(!empty($fadname)){
            $where_str .= ' and a.fad_name ="'.$fadname.'"';
        }
        if(!empty($fmediaid)){
            $where_str .= ' and a.fmedia_id ='.$fmediaid;
        }
        if(!empty($tregionid)){
            $tregion_len = get_tregionlevel($tregionid);

            if($tregion_len == 1){//国家级
                $where_str .= ' and a.fregion_id ='.$tregionid;
            }elseif($tregion_len == 2){//省级
                $where_str .= ' and a.fregion_id like "'.substr($tregionid,0,2).'%"';
            }elseif($tregion_len == 4){//市级
                $where_str .= ' and a.fregion_id like "'.substr($tregionid,0,4).'%"';
            }elseif($tregion_len == 6){//县级
                $where_str .= ' and a.fregion_id like "'.substr($tregionid,0,6).'%"';
            }
        }

        if($search_type == 2){
            $sqlstr = '
                select a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus2,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek
                from tbn_illegal_ad a
                inner join tadclass b on left(a.fad_class_code,2)=b.fcode
                inner join tbn_illegal_ad_issue m on a.fid=m.fillegal_ad_id
                inner join tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id
                inner join tregion n on a.fregion_id = n.fid '.$tregionstr.'
                inner join (select fad_name from (select a.fad_name,b.fname1 from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid and flevel in (1,2,3) inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where '.$where_time.' and fcustomer = '.$system_num.' group by left(a.fregion_id,2),a.fad_name ) as a group by a.fad_name having count(1)>1) k on a.fad_name = k.fad_name 
                inner join (select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue where '.$where_time.' group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id
                where '.$where_str.' and fcustomer = '.$system_num.'
            ';
        }elseif($search_type == 1){
            $sqlstr = '
                select a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus2,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek
                from tbn_illegal_ad a
                inner join tadclass b on left(a.fad_class_code,2)=b.fcode
                inner join tbn_illegal_ad_issue m on a.fid=m.fillegal_ad_id
                inner join tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id
                inner join tregion n on a.fregion_id = n.fid '.$tregionstr.'
                inner join (select fad_name from (select a.fad_name,b.fname1 from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where '.$where_time.' and fcustomer = '.$system_num.' group by left(a.fregion_id,2),a.fad_name ) as a group by a.fad_name having count(1)>1) k on a.fad_name = k.fad_name 
                inner join (select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue where '.$where_time.' group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id
                where '.$where_str.' and fcustomer = '.$system_num.'
            ';
        }else{
            $sqlstr = '
                select a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus2,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,fregion_count,fregionname
                from tbn_illegal_ad a
                inner join tadclass b on left(a.fad_class_code,2)=b.fcode
                inner join tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id
                inner join tregion n on a.fregion_id = n.fid '.$tregionstr.'
                inner join (select fad_name,count(1) as fregion_count,group_concat(fname1) as fregionname from (select a.fad_name,b.fname1 from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid '.$tregionstr.' inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where '.$where_time.' and fcustomer = '.$system_num.' group by left(a.fregion_id,2),a.fad_name ) as a group by a.fad_name having count(1)>1) k on a.fad_name = k.fad_name 
                inner join (select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue where '.$where_time.' group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id
                where '.$where_str.' and fcustomer = '.$system_num.'
            ';
        }
        $data_list = M()->query($sqlstr);
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data_list));
    }

    //媒体类型及媒体排名
    public function media_type_class_pm(){
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isrelease = $ALL_CONFIG['isrelease'];

        $user = session('regulatorpersonInfo');//获取用户信息
        $year = I('year',date('Y'));//默认当年
        $times_table = I('times_table','');//选表，默认月表

        $table_condition = I('table_condition',date('m').'-01');//根据选取的不用表展示不同的条件  //默认当月

        $is_show_longad = I('is_show_longad',0);//是否包含长广告

        //根据选择的不同的时间组合不同的条件
        switch ($times_table){
            case 'tbn_ad_summary_week':
                if($table_condition < 10){
                    $table_condition = '0'.$table_condition;
                }
                $cycle_stamp = $this->weekday($year,$table_condition);//周期首末时间戳
                break;
            case 'tbn_ad_summary_half_month':
            case 'tbn_ad_summary_month':
            case 'tbn_ad_summary_quarter':
                $table_condition = $year.'-'.$table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                break;
            case 'tbn_ad_summary_half_year':
            case 'tbn_ad_summary_year':
                $table_condition = $table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                break;
        }

        $s_time = date('Y-m-d',$cycle_stamp['start']);
        $e_time = date('Y-m-d',($cycle_stamp['end']-86400));
        $map = [];//定义查询条件

        $re_level = $this->judgment_region($user['regionid']);
        if($re_level == 0){
            $map["tregion.flevel"] = 1;
        }else{
            $map["tbn_illegal_ad.fregion_id"] = $user['regionid'];
        }

        $map['_string'] = '1=1';

        /*如果是国家局*/
        if($system_num == '100000'){
            //判断是否国家局系统
            /*$gjj_label_media = M('tmedia_temp')->where('ftype=1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid'))->getField('fmediaid',true);*/

            $gjj_label_media = $this->get_gjj_label_media();

            $map['tbn_illegal_ad_issue.fmedia_id'] = array('in',$gjj_label_media);//国家局标签媒体
            $map['tregion.flevel'] = ['IN','0,1,2,3'];
            //是否包含违法长广告时长
            if($is_show_longad != 1){
                $map['_string'] .= ' and (TIMESTAMPDIFF(SECOND,tbn_illegal_ad_issue.fstarttime,tbn_illegal_ad_issue.fendtime)< 600 OR tbn_illegal_ad_issue.fmedia_class>2)';
            }
        }

        if(!empty($isrelease) || $system_num == '100000'){
            $map['tbn_illegal_ad_issue.fsend_status'] = 2;
        }

        $map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_time,$e_time]];
        $map['_string'] .= ' and tmedia.fid = tmedia.main_media_id';
        $map['_string'] .= " and tbn_illegal_ad.fcustomer='".$system_num."'";
        $map['tbn_illegal_ad.fstatus3'] = ['NOT IN',[30,40]];

        $maps = [1,2,3];
        foreach ($maps as $key=>$map_val){
            $map["tbn_illegal_ad.fmedia_class"] = $map_val;

            $media_pm_mod = M('tbn_illegal_ad_issue')
                ->field("
                  COUNT(1) AS fad_illegal_times,
                  tbn_illegal_ad.fmedia_id as fmedia_id,
                  tmedia.fmedianame as tmedia_name
                ")
                ->where($map)
                ->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
                ->join("
                    tmediaclass on 
                    tmediaclass.fid = tbn_illegal_ad.fmedia_class"
                )
                ->join("
                    tmedia on 
                    tmedia.fid = tbn_illegal_ad.fmedia_id"
                )
                ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
                )
                ->group('fmedia_id')
                ->order('fad_illegal_times desc')
                ->limit(5)
                ->select();
            $media_pm_arr = [];
            foreach ($media_pm_mod as $media_pm_mod_val){

                $media_pm_arr[$media_pm_mod_val['fmedia_id']] = $media_pm_mod_val['tmedia_name'];
            }
            $media_pm['0'.($key+1)] = $media_pm_arr;
        }

        $data_list = [];
        $num = 0;
        $id = 0;

        foreach ($media_pm as $key => $media_pm_val){
            if($key == '01'){
                $data_list['01']['media_name'] = '电视';
            }elseif($key == '02'){
                $data_list['02']['media_name'] = '广播';
            }elseif($key == '03'){
                $data_list['03']['media_name'] = '报纸、期刊';
            }

            foreach ($media_pm_val as $val_key=>$media_pm_val_v){
                if($key == '01'){
                    $data_list['01']['num'.++$num] = $media_pm_val_v;
                    $data_list['01']['id'.++$id] = $val_key;
                }elseif($key == '02'){
                    $data_list['02']['num'.++$num] = $media_pm_val_v;
                    $data_list['02']['id'.++$id] = $val_key;
                }elseif($key == '03'){
                    $data_list['03']['num'.++$num] = $media_pm_val_v;
                    $data_list['03']['id'.++$id] = $val_key;
                }
            }
            if($num == count($media_pm_val) || $id == count($media_pm_val)){
                $num = 0;
                $id = 0;
            }
        }
        $this->ajaxReturn(array_values($data_list));
    }

    //全国热力图页面接口
    public function thermal_map(){
        Check_QuanXian(['diagram']);
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $use_open_search = $ALL_CONFIG['use_open_search'];

        $isfixeddata = I('isfixeddata');//是否实时数据
        $year = I('year',date('Y'));//默认当年
        $times_table = I('times_table','');//选表，默认月表
        $table_condition = I('table_condition',date('m').'-01');//根据选取的不用表展示不同的条件  //默认当月
        $is_show_fsend = I('is_show_fsend',2);//选表，默认月表
        //默认表
        if($times_table == ''){
            $times_table = 'tbn_ad_summary_month';
        }
        $is_show_fsend = I('is_show_fsend',2);//是否发布前的数据
        $is_show_longad = I('is_show_longad',0);//是否包含长广告

        if($system_num == '100000'||empty($isfixeddata)){
            $summary_table = "tbn_ad_summary_day_z";
            $map[$summary_table.'.fcustomer'] = $system_num;

            if($is_show_longad == 1){
                //包含长广告
                $play_len_field = 'in_long_ad_play_len';
                $fad_times_field = 'fad_times';
                $fad_count_field = 'fad_sam_list';
            }else{
                //不包含长广告
                $play_len_field = 'fad_play_len';
                $fad_times_field = 'basic_ad_times';
                $fad_count_field = 'basic_ad_sam_list';
            }
        }else{
            if($use_open_search == 1) {
                $summary_table = "tbn_ad_summary_day_v3";
                $map[$summary_table.'.fcustomer'] = $system_num;
                if($is_show_longad == 1){
                    //包含长广告
                    $play_len_field = 'fad_play_len_long_ad';
                    $fad_times_field = 'fad_times_long_ad';
                    $fad_count_field = 'fsam_list_long_ad';
                }else{
                    //不包含长广告
                    $play_len_field = 'fad_play_len';
                    $fad_times_field = 'fad_times';
                    $fad_count_field = 'fsam_list';
                }
            }else{
                $summary_table = "tbn_ad_summary_day";
                if($is_show_longad == 1){
                    $play_len_field = 'in_long_ad_play_len';
                    $fad_times_field = 'fad_times';
                    $fad_count_field = 'fad_sam_list';
                }else{
                    $play_len_field = 'fad_play_len';
                    $fad_times_field = 'basic_ad_times';
                    $fad_count_field = 'basic_ad_sam_list';
                }
            }

        }


        //根据选择的不同的时间组合不同的条件
        switch ($times_table){
            case 'tbn_ad_summary_week':
                if($table_condition < 10){
                    $table_condition = '0'.$table_condition;
                }
                $cycle_stamp = $this->weekday($year,$table_condition);//周期首末时间戳
                break;
            case 'tbn_ad_summary_half_month':
            case 'tbn_ad_summary_month':
            case 'tbn_ad_summary_quarter':
                $table_condition = $year.'-'.$table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                break;
            case 'tbn_ad_summary_half_year':
            case 'tbn_ad_summary_year':
                $table_condition = $table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                break;
        }

        $s_time = date('Y-m-d',$cycle_stamp['start']);
        $e_time = date('Y-m-d',($cycle_stamp['end']-86400));
        $map = [];//定义查询条件
        $map[$summary_table.'.fdate'] = ['BETWEEN',[$s_time,$e_time]];
        $map[$summary_table.'.confirm_state'] = ['GT',0];
        $map['_string'] = 'tmedia.fid = tmedia.main_media_id';
        $illegal_map['_string'] = "tbn_illegal_ad.fcustomer='".$system_num."'";
        $illegal_map['_string'] = 'tmedia.fid = tmedia.main_media_id';
        $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_time,$e_time]];
        $illegal_map['tbn_illegal_ad.fstatus3'] = ['NOT IN',[30,40]];
        $s_timestamp = strtotime($s_time);//开始时间戳
        $e_timestamp = strtotime($e_time);//结束时间戳

        //获取上个周期的日期范围
        if(($e_timestamp - $s_timestamp) <= 0){
            $last_s_time = date('Y-m-d',($s_timestamp-86400));//上周期开始时间戳
            $last_e_time = $last_s_time;//上周期结束时间戳
        }else{
            $last_s_time = date('Y-m-d',($s_timestamp-($e_timestamp-$s_timestamp)-86400));
            $last_e_time = date('Y-m-d',($s_timestamp-86400));
        }

        //浏览权限设置start
        $user = session('regulatorpersonInfo');//获取用户信息

        $re_level = $this->judgment_region($user['regionid']);//当前用户行政等级


        $table_model = $summary_table;
        $times_table = $summary_table;

        $region_id = I('region_id','');//点击的区域ID
        $region_level = I('region_level','2');//区域等级
        $zxs = ['110000','120000','310000','500000'];//直辖市
        $tregion_map = [];
        $group_filed = 'substr('.$times_table.'.fregionid,1,'.$region_level.')';
        $r_name = 'fname1';
        switch ($region_level){
            case '2':
                $tregion_map['flevel'] = 1;
                $r_name = 'fname';
                break;
            case '4':
                $map['tregion.fpid'] = ['LIKE',substr($region_id,0,2).'%'];
                $tregion_map['tregion.fpid'] = ['LIKE',substr($region_id,0,2).'%'];
                if(!in_array($region_id,$zxs)){
                    $tregion_map['flevel'] = ['in',[2,3,4]];
                }else{
                    $tregion_map['flevel'] = 5;
                }
                break;
            case '6':
                $map['tregion.fpid'] = ['LIKE',substr($region_id,0,4).'%'];
                $tregion_map['tregion.fpid'] = ['LIKE',substr($region_id,0,4).'%'];
                $tregion_map['flevel'] = 5;
                $zero = '';
                break;
        }

        //媒介类型
        $media_class = I('media_class','');
        if($media_class != ''){
            $map[$times_table.'.fmedia_class_code'] = ['like',$media_class.'%'];//媒体
        }

        //媒体细类
        $media_son_class = I('media_son_class','');//媒体细类
        if($media_son_class != ''){
            $map[$times_table.'.fmedia_class_code'] = ['like',$media_son_class.'%'];//媒体细类
        }

        //广告类型
        $ad_class = I('ad_class','');//广告大类
        if($ad_class != ''){
            if($ad_class < 10){
                $ad_class = '0'.$ad_class;
            }
            $map[$times_table.'.fad_class_code'] = $ad_class;//广告类别
        }

        $onther_class = I('onther_class','');//排名条件

        if($onther_class == ''){
            $onther_class =  $fad_times_field;
        }

        $map['tregion.flevel'] = ['IN','0,1,2,3'];
        $illegal_map['_string'] = '1=1';
        $illegal_map['tregion.flevel'] = ['IN','0,1,2,3'];
        if($system_num == '100000'){

            /*                $gjj_label_media = M('tmedia_temp')->where('ftype=1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid'))->getField('fmediaid',true);*/
            $gjj_label_media = $this->get_gjj_label_media();
            $map[$summary_table.'.fmediaid'] = array('in',$gjj_label_media);//国家局标签媒体
            $illegal_map['tbn_illegal_ad_issue.fmedia_id'] = array('in',$gjj_label_media);//国家局标签媒体


            //是否包含违法长广告
            if($is_show_longad != 1){
                $illegal_map['_string'] .= ' and (TIMESTAMPDIFF(SECOND,tbn_illegal_ad_issue.fstarttime,tbn_illegal_ad_issue.fendtime)<600 OR tbn_illegal_ad_issue.fmedia_class>2)';
            }
            if(session('regulatorpersonInfo.fregulatorlevel') != 30){//判断是否国家级用户

                if($re_level == 4 || $re_level == 5){
                    $map[$summary_table.'.fid'] = 0;
                }
            }
        }else{
            $onther_class_tj = "sum($times_table.$onther_class)";
        }

        $isrelease = $ALL_CONFIG['isrelease'];
        if(!empty($isrelease) || $system_num == '100000'){
            //是否显示发布前数据
            if($is_show_fsend == 1){
                $illegal_map['tbn_illegal_ad_issue.fsend_status'] = ['IN',[1,2]];
            }else{
                $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
            }
        }

        $tregion_mod = M('tregion')
            ->field('substr(fid,1,'.$region_level.') as fid_sub,fid,'.$r_name)->where($tregion_map)->select();
        //查询符合条件的数据

        if($onther_class == 'fad_illegal_times' || $onther_class == 'fad_illegal_play_len'){
            if($onther_class == 'fad_illegal_play_len'){
                $onther_class_tj = "SUM(
                    UNIX_TIMESTAMP(
                        tbn_illegal_ad_issue.fendtime
                    ) - UNIX_TIMESTAMP(
                        tbn_illegal_ad_issue.fstarttime
                    )
                )";
            }elseif($onther_class == 'fad_illegal_times'){
                $onther_class_tj = "COUNT(1)";
            }
            //违法广告条次与时长
            $province_illegal_count = M('tbn_illegal_ad_issue')
                ->field("
                tregion.$r_name as name,
                substr(tregion.fid,1,$region_level) as fregionid,
                $onther_class_tj as count
            ")
                ->where($illegal_map)
                ->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
                ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
                )
                ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id"
                )
                ->group('substr(tbn_illegal_ad.fregion_id,1,'.$region_level.')')
                ->select();
        }
        elseif($onther_class == 'fad_illegal_count'){
            //违法广告条数
            $province_illegal_count = M('tbn_illegal_ad_issue')
                ->field("
                tregion.$r_name as name,
                substr(tregion.fid,1,$region_level) as fregionid,
                GROUP_CONCAT(
                CONCAT_WS(
                    '_',
                    tbn_illegal_ad.fsample_id,
                    tbn_illegal_ad.fmedia_class
                ) SEPARATOR ','
                ) AS fsample_ids
            ")
                ->where($illegal_map)
                ->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
                ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
                )
                ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id"
                )
                ->group('substr(tbn_illegal_ad.fregion_id,1,'.$region_level.')')
                ->select();

            //循环计算违法条数
            foreach ($province_illegal_count as $tbn_illegal_data_key => $tbn_illegal_data_val){
                $sam_array = explode(',',$tbn_illegal_data_val['fsample_ids']);
                $fad_illegal_count = count(array_unique($sam_array));
                unset($province_illegal_count[$tbn_illegal_data_key]['fsample_ids']);
                $province_illegal_count[$tbn_illegal_data_key]['count'] = $fad_illegal_count;//插入违法条数
            }
        }
        elseif($onther_class == 'fad_count'){

            $province_illegal_count = M($summary_table)
                ->cache(true,600)
                ->field("
                tregion.$r_name as name,
                substr(tregion.fid,1,$region_level) as fregionid,
                GROUP_CONCAT($fad_count_field SEPARATOR ',') as fad_sam_list
           ")
                ->join("
                tregion on 
                tregion.fid = $times_table.fregionid"
                )
                ->join("
                tmedia on 
                tmedia.fid = $table_model.fmediaid"
                )
                ->group($group_filed)
                ->where($map)
                ->select();
            //循环计算条数
            foreach ($province_illegal_count as $data_key => $data_val){
                $sam_array = explode(',',$data_val['fad_sam_list']);
                $fad_count = count(array_unique($sam_array));
                unset($province_illegal_count[$data_key]['fad_sam_list']);
                $province_illegal_count[$data_key]['count'] = $fad_count;//插入条数
            }
        }
        else{
            if($onther_class == 'fad_play_len'){
                $onther_class = $play_len_field;
            }else{
                $onther_class = $fad_times_field;
            }
            $onther_class_tj = "sum($times_table.$onther_class)";
            $province_illegal_count = M($summary_table)
                ->cache(true,600)
                ->field("
                tregion.$r_name as name,
                substr(tregion.fid,1,$region_level) as fregionid,
                $onther_class_tj as count
           ")
                ->join("
                tregion on 
                tregion.fid = $times_table.fregionid"
                )
                ->join("
                tmedia on 
                tmedia.fid = $table_model.fmediaid"
                )
                ->group($group_filed)
                ->where($map)
                ->select();
        }

        $data = [];
        foreach ($tregion_mod as $tregion_mod_key=>$tregion_mod_val){
            foreach ($province_illegal_count as $province_illegal_count_key=>$province_illegal_count_val){
                if($tregion_mod_val['fid_sub'] == $province_illegal_count_val['fregionid']){
                    $data[$tregion_mod_val['fid_sub']] = [
                        'id'=>$tregion_mod_val['fid'],
                        'name'=>$tregion_mod_val[$r_name],
                        'value'=>$province_illegal_count_val['count']
                    ];
                }
            }
        }
        $data = array_values($data);
        $this->ajaxReturn($data);
    }

    //近三十天违法广告情况热力图
    public function thermal_map_30(){
        Check_QuanXian(['home']);
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isexamine = $ALL_CONFIG['isexamine'];
        $isrelease = $ALL_CONFIG['isrelease'];

        //浏览权限设置start
        $region_id = I('region_id','');//点击的区域ID
        $is_show_fsend = I('is_show_fsend',2);//是否发布前的数据
        $is_show_longad = I('is_show_longad',0);//是否包含长广告
        $region_level = I('region_level',2);//区域等级

        $user = session('regulatorpersonInfo');//获取用户信息
        $re_level = $this->judgment_region($user['regionid']);//当前用户行政等级
        $zxs = ['110000','120000','310000','500000'];//直辖市
        $tregion_map = [];
        $map = [];
        $group_filed = 'substr(tbn_illegal_ad.fregion_id,1,'.$region_level.')';

        $r_name = 'fname1';
        switch ($region_level){
            case '2':
                $r_name = 'fname';
                if($re_level == 1){
                    $map['tregion.fid'] = ['LIKE',substr($region_id,0,2).'%'];
                    $tregion_map['fid'] = ['LIKE',substr($region_id,0,2).'%'];
                }
                $tregion_map['flevel'] = 1;
                break;
            case '4':
                $map['tregion.fpid'] = ['LIKE',substr($region_id,0,2).'%'];
                $tregion_map['fpid'] = ['LIKE',substr($region_id,0,2).'%'];
                if(!in_array($region_id,$zxs)){
                    $tregion_map['flevel'] = ['in',[2,3,4]];
                }else{
                    $map['tregion.flevel'] = 5;
                    $tregion_map['flevel'] = 5;
                }
                break;
            case '6':
                $map['tregion.fpid'] = ['LIKE',substr($region_id,0,4).'%'];
                $map['flevel'] = 5;
                $tregion_map['fpid'] = ['LIKE',substr($region_id,0,4).'%'];
                $tregion_map['flevel'] = 5;
                $zero = '';
                break;
        }
        $user = session('regulatorpersonInfo');//获取用户信息

        $re_level = $this->judgment_region($user['regionid']);//当前用户行政等级
        $map['_string'] = '1=1';
        //审核通过的违法数据
        if(in_array($isexamine, [10,20,30,40,50])){
            $map['tbn_illegal_ad.fexamine'] = 10;
        }
        /*如果是国家局*/
        if($system_num == '100000'){
            //判断是否国家局系统
            /*            $gjj_label_media = M('tmedia_temp')->where('ftype=1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid'))->getField('fmediaid',true);*/

            $map['tbn_illegal_ad.fmedia_id'] = array('in',$this->get_gjj_label_media());//国家局标签媒体

            //是否包含违法长广告
            if($is_show_longad != 1){
                $map['_string'] .= ' and (TIMESTAMPDIFF(SECOND,tbn_illegal_ad_issue.fstarttime,tbn_illegal_ad_issue.fendtime)<600 OR tbn_illegal_ad_issue.fmedia_class>2)';
            }
        }
        //是否显示发布前数据
        if(!empty($isrelease) || $system_num == '100000'){
            if($is_show_fsend == 1){
                $map['tbn_illegal_ad_issue.fsend_status'] = ['IN',[1,2]];
            }else{
                $map['tbn_illegal_ad_issue.fsend_status'] = 2;
            }
        }

        $map['_string'] .= ' and tmedia.fid = tmedia.main_media_id';
        $map['_string'] .= " and tbn_illegal_ad.fcustomer='".$system_num."'";
        $map['tbn_illegal_ad.fstatus3'] = ['NOT IN',[30,40]];
        $tregion_mod = M('tregion')->field('substr(fid,1,'.$region_level.') as fid_sub,fid,'.$r_name)->where($tregion_map)->select();
        //查询符合条件的数据
        $s_date = date('Y-m-d',time()-(86400*30));
        $e_date = date('Y-m-d',time());
        $map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_date,$e_date]];

        $province_illegal_count = M('tbn_illegal_ad_issue')
            ->field("
                tregion.$r_name as name,
                substr(tregion.fid,1,$region_level) as fregion_id,
                count(1) as count
           ")
            ->join("
                    tbn_illegal_ad on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id"
            )
            ->group($group_filed)
            ->where($map)
            ->select();//条数


        $data = [];
        foreach ($tregion_mod as $tregion_mod_key=>$tregion_mod_val){
            foreach ($province_illegal_count as $province_illegal_count_key=>$province_illegal_count_val){
                if($tregion_mod_val['fid_sub'] == $province_illegal_count_val['fregion_id']){
                    $data[$tregion_mod_val['fid_sub']] = [
                        'id'=>$tregion_mod_val['fid'],
                        'name'=>$tregion_mod_val[$r_name],
                        'value'=>$province_illegal_count_val['count']
                    ];
                }
            }
        }
        $data = array_values($data);
        $this->ajaxReturn($data);
    }


    //全国热力图页面接口-中视广联
    public function thermal_map_zsgl(){
        ini_set('memory_limit','1024M');
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type: text/html; charset=utf-8");
        $user = session('regulatorpersonInfo');//获取用户信息

        $ad_pm_type = I('ad_pm_type','');//确定是点击监测情况或者线索菜单
        $is_out_excel = I('is_out_excel',0);//确定是否点击导出EXCEL

        $year = I('year',date('Y'));//默认当年
        $times_table = I('times_table','');//选表，默认月表

        //默认表
        if($times_table == ''){
            $times_table = 'tbn_ad_summary_month';
        }

        //$table_condition = I('table_condition',date('m').'-01');//根据选取的不用表展示不同的条件  //默认当月
        $table_condition = I('table_condition','11-01');//根据选取的不用表展示不同的条件  //默认当月
        $previous_cycles = $this->get_previous_cycles($times_table,$year,$table_condition);//获取上个周期时间
        //根据选择的不同的时间组合不同的条件
        switch ($times_table){
            case 'tbn_ad_summary_week':
                if($table_condition < 10){
                    $table_condition = '0'.$table_condition;
                }
                $cycle_stamp = $this->weekday($year,$table_condition);//周期首末时间戳
                $prv_cycle_stamp = $this->weekday($year,$previous_cycles);//周期首末时间戳
                break;
            case 'tbn_ad_summary_half_month':
            case 'tbn_ad_summary_month':
            case 'tbn_ad_summary_quarter':
                $table_condition = $year.'-'.$table_condition;//半月，月，季度
                $previous_cycles = $year.'-'.$previous_cycles;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                $prv_cycle_stamp = $this->get_stemp($previous_cycles,$times_table);
                break;
            case 'tbn_ad_summary_half_year':
            case 'tbn_ad_summary_year':
                $table_condition = $table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                $prv_cycle_stamp = $this->get_stemp($previous_cycles,$times_table);
                break;
        }

        $s_time = date('Y-m-d',$cycle_stamp['start']);
        $e_time = date('Y-m-d',($cycle_stamp['end']-86400));

        $tregion_map = [];//定义查询条件

        $tregion_map['BOCHURIQI'] = ['BETWEEN',[$s_time,$e_time]];

        //媒介类型
        $media_class = I('media_class','');
        if($media_class != ''){
            switch ($media_class){
                case '01':
                    $tregion_map['MEITILEIXING'] = 1;//电视
                    break;
                case '02':
                    $tregion_map['MEITILEIXING'] = 0;//广播
                    break;
                case '03':
                    $tregion_map['MEITILEIXING'] = 2;//报纸
                    break;
            }
        }

        $ad_class = I('ad_class');//广告大类I('ad_class',"")
        if(!empty($ad_class)){
            foreach ($ad_class as $ad_class_key=>$ad_class_val){
                if($ad_class_val < 10){
                    $ad_class[$ad_class_key] = '0'.$ad_class_val;
                }
            }
            $tregion_map['LEIBIEId'] = ['IN',$ad_class];//广告类别
        }

        $onther_class = I('onther_class','fad_times');//排名条件

        if($onther_class == ''){
            $onther_class = 'fad_times';
        }

        switch ($onther_class){
            case 'fad_times' :
                $check_table = 't_guanggaohuizong';
                $onther_class = 'SUM(ZONGTIAOCI) as value';
                break;
            case 'fad_play_len' :
                $check_table = 't_guanggaohuizong';
                $onther_class = 'SUM(ZONGHSICHANG) as value';
                break;
            case 'fad_illegal_times' :
                $check_table = 't_weifaguanggao';
                $onther_class = 'COUNT(LEIBIEId) AS value';
                break;
            case 'fad_illegal_play_len' :
                $check_table = 't_weifaguanggao';
                $onther_class = "SUM(
                                    CASE
                                    WHEN SHICHANG REGEXP '^[0-9]+$' THEN
                                        SHICHANG
                                    ELSE
                                        0
                                    END
                                ) AS value";
                break;
        }

        $re_level = $this->judgment_region($user['regionid']);//当前用户行政等级


        $region_id = I('region_id','');//点击的区域ID
        $region_level = I('region_level','2');//区域等级
        $zxs = ['110000','120000','310000','500000'];//直辖市

        switch ($region_level){
            case '2':
                $tregion_name = 'substr(SHENG,1,2)';
                $tregionid = "CONCAT(substr(DIYUBIANHAO,1,2),'0000') as id";
                break;
            case '4':
                $tregion_name = 'SHI';
                $tregionid = "CONCAT(substr(DIYUBIANHAO,1,4),'00') as id";
                $tregion_map['DIYUBIANHAO'] = ['LIKE',substr($region_id,0,2).'%'];
                if(!in_array($region_id,$zxs)){
                    $tregion_map['FLEVEL'] = ['in',[2,3,4]];
                }else{
                    $tregion_map['FLEVEL'] = 5;
                }
                break;
            case '6':
                $tregion_name = 'XINA';
                $tregionid = "DIYUBIANHAO as id";
                $tregion_map['DIYUBIANHAO'] = ['LIKE',substr($region_id,0,2).'%'];
                $tregion_map['FLEVEL'] = 5;
                break;
        }

        //1021
        $data = M($check_table)->cache(true,600)
            ->field("
            $tregion_name as name,
            $tregionid,
            $onther_class
            ")
            ->where($tregion_map)
            ->group($tregion_name)
            ->select();
        foreach ($data as $data_key=>$data_val){
            if($data_val['name'] == '内蒙'){
                $data[$data_key]['name'] = '内蒙古';
            }elseif ($data_val['name'] == '黑龙'){
                $data[$data_key]['name'] = '黑龙江';
            }
        }
        $this->ajaxReturn($data);
    }

    //中视广联汇总数据
    public function zsgl_data(){
        ini_set('memory_limit',-1);
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type: text/html; charset=utf-8");
        $user = session('regulatorpersonInfo');//获取用户信息

        $ad_pm_type = I('ad_pm_type','');//确定是点击监测情况或者线索菜单
        $is_out_excel = I('is_out_excel',0);//确定是否点击导出EXCEL

        $year = I('year','2017');//默认当年
        //$year = I('year',date('Y'));//默认当年
        $times_table = I('times_table','');//选表，默认月表
        $region_order = I('region_order',1);//是否地域排名
        $is_have_self = I('is_have_self',0);//是否显示本级

        //默认表
        if($times_table == ''){
            $times_table = 'tbn_ad_summary_month';
        }

        //$table_condition = I('table_condition',date('m').'-01');//根据选取的不用表展示不同的条件  //默认当月
        $table_condition = I('table_condition','12-01');//根据选取的不用表展示不同的条件  //默认当月
        $previous_cycles = $this->get_previous_cycles($times_table,$year,$table_condition);//获取上个周期时间

        //根据选择的不同的时间组合不同的条件
        switch ($times_table){
            case 'tbn_ad_summary_week':
                if($table_condition < 10){
                    $table_condition = '0'.$table_condition;
                }
                $cycle_stamp = $this->weekday($year,$table_condition);//周期首末时间戳
                $prv_cycle_stamp = $this->weekday($year,$previous_cycles);//周期首末时间戳
                break;
            case 'tbn_ad_summary_half_month':
            case 'tbn_ad_summary_month':
            case 'tbn_ad_summary_quarter':
                $table_condition = $year.'-'.$table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                $prv_cycle_stamp = $this->get_stemp($previous_cycles,$times_table);

                break;
            case 'tbn_ad_summary_half_year':
            case 'tbn_ad_summary_year':
                $table_condition = $table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                $prv_cycle_stamp = $this->get_stemp($previous_cycles,$times_table);
                break;
        }

        $s_time = date('Y-m-d',$cycle_stamp['start']);
        $e_time = date('Y-m-d',($cycle_stamp['end']-86400));

        $last_s_time = date('Y-m-d',$prv_cycle_stamp['start']);
        $last_e_time = date('Y-m-d',($prv_cycle_stamp['end']-86400));

        $where = [];//定义查询条件

        $where['BOCHURIQI'] = ['BETWEEN',[$s_time,$e_time]];

        //分组统计条件
        $group = I('fztj','fmediaid');//分组

        //媒介类型
        $media_class = I('media_class','');
        if($media_class != ''){
            switch ($media_class){
                case '01':
                    $where['MEITILEIXING'] = 1;//电视
                    break;
                case '02':
                    $where['MEITILEIXING'] = 0;//广播
                    break;
                case '03':
                    $where['MEITILEIXING'] = 2;//报纸
                    break;
            }
        }

        $ad_class = I('ad_class');//广告大类I('ad_class',"")
        if(!empty($ad_class)){
            $where['LEIBIEId'] = ['IN',$ad_class];//广告类别
        }


        $onther_class = I('onther_class','fad_times');//排名条件

        $order = $onther_class.' desc';
        $re_level = $this->judgment_region($user['regionid']);//当前用户行政等级

        //判断用户点击哪个菜单
        if($ad_pm_type == 2){
            $region = I('region2','');//区域
            if($region == ''){
                if($re_level == 2 || $re_level == 3 || $re_level == 4){
                    $region = 5;
                }elseif($re_level == 1){
                    $region = 4;
                }
            }

            //线索$where_region['DIYUBIANHAO'] = ['like',substr($user['regionid'],0,2)."%"];
            //$where_region["flevel"] = 5;
            switch ($region){
                case 0:
                case 1:
                    if($re_level == 0){
                        $where_region["FLEVEL"] = 1;
                    }elseif($re_level == 1){
                        $where_region['DIYUBIANHAO'] = $user['regionid'];
                    }
                    break;
                case 2:
                    if($re_level == 0){
                        //全国各副省级市违法线索情况
                        $where_region["FLEVEL"] = 2;
                    }elseif($re_level == 1){
                        //本省各副省级市违法线索情况
                        $where_region["FLEVEL"] = 2;
                        $where_region["DIYUBIANHAO"] = ['like',substr($user['regionid'],0,2)."%"];
                    }elseif($re_level == 2){
                        $where_region["DIYUBIANHAO"] = $user['regionid'];
                    }
                    break;
                case 3://各计划单列市
                    if($re_level == 0){
                        //全国各计划单列市违法线索情况
                        $where_region["FLEVEL"] = 3;
                    }elseif($re_level == 1){
                        //本省各计划单列市违法线索情况
                        $where_region["FLEVEL"] = 3;
                        $where_region["DIYUBIANHAO"] = ['like',substr($user['regionid'],0,2)."%"];
                    }elseif($re_level == 3){
                        $where_region["DIYUBIANHAO"] = $user['regionid'];
                    }
                    break;
                case 4://各市
                    if($re_level == 0){
                        //如果是全国各市违法线索情况
                        $where_region["FLEVEL"] = 4;
                    }elseif($re_level == 1){
                        //本省各市违法线索情况
                        if($is_have_self == 1){
                            $where_region["FLEVEL"] = ['IN','1,2,3,4'];
                        }else{
                            $where_region["FLEVEL"] = ['IN','2,3,4'];
                        }
                        $where_region["DIYUBIANHAO"] = ['like',substr($user['regionid'],0,2)."%"];

                    }elseif($re_level == 4){
                        $where_region["DIYUBIANHAO"] = $user['regionid'];
                    }
                    break;
                case 5://各区县
                    if($re_level == 0){

                        //全国各区县违法线索情况
                        $where_region["FLEVEL"] = 5;

                    }elseif($re_level == 1){
                        //本省各区县违法线索情况
                        $where_region["DIYUBIANHAO"] = ['like',substr($user['regionid'],0,2)."%"];
                        $where_region["FLEVEL"] = 5;

                    }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){

                        //本市各区县违法线索情况
                        if($is_have_self == 1){
                            $where_region["FLEVEL"] = ['IN','2,3,4,5'];
                        }else{
                            $where_region["FLEVEL"] = 5;
                        }
                        $where_region["DIYUBIANHAO"] = ['like',substr($user['regionid'],0,4)."%"];

                    }elseif($re_level == 5){
                        $where_region["DIYUBIANHAO"] = $user['regionid'];
                    }
                    break;
            }
        }else{
            $region = I('region','');//区域
            if($region == ''){
                if($re_level == 4){
                    $region = 4;
                }else{
                    if($re_level == 5){
                        $region = $re_level;
                    }else{
                        $region = 0;//$re_level;
                    }
                }
            }
            //违法情况
            switch ($region){
                case 1://省级和国家级的各省监测情况
                    $where_region["_string"] = 'FLEVEL = 1 or DIYUMINGCHENG = "中央"';
                    break;
                case 2://副省级市
                    $where_region["FLEVEL"] = 2;
                    break;
                case 3://各计划单列市
                    $where_region["FLEVEL"] = 3;
                    break;
                case 4://各市
                    if($re_level == 0){
                        //国家级用户的各市监测情况
                        $where_region["FLEVEL"] = 4;
                    }else{
                        //市级或者省级用户的各市监测情况
                        $where_region['DIYUBIANHAO'] = ['like',substr($user['regionid'],0,2)."%"];
                        $where_region["FLEVEL"] = 4;
                    }
                    break;
                case 5://各区县
                    if($re_level == 0){
                        //如果是全国用户
                        $where_region["FLEVEL"] = 5;
                    }elseif($re_level == 1){
                        //如果是省级用户
                        $where_region['DIYUBIANHAO'] = ['like',substr($user['regionid'],0,2)."%"];
                        $where_region["FLEVEL"] = 5;
                    }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                        $where_region['DIYUBIANHAO'] = ['like',substr($user['regionid'],0,4)."%"];
                        $where_region["FLEVEL"] = 5;
                    }elseif($re_level == 5){
                        $where_region['DIYUBIANHAO'] = $user['regionid'];
                    }
                    break;
            }
        }

        switch ($region){
            case 0:
            case 1:
                $tregion_field_name = 'SHENG';
                break;
            case 2:
            case 3:
            case 4:
                $tregion_field_name = "
                                        CASE
                                    WHEN SHI = '' THEN
                                        SHENG
                                    ELSE
                                        SHI
                                    END";
                break;
            case 5:
                $tregion_field_name = "
                                        CASE
                                    WHEN XINA = '' THEN
                                        SHI
                                    ELSE
                                        XINA
                                    END";
                break;
        }

        //匹配分组字段名称
        if($group == ''){
            $field = "$tregion_field_name as tregion_name,DIYUBIANHAO as fregionid";
            $group = 'fregionid';
            $group_name = 'tregion_name';
            $field_data = "a.tregion_name,a.fregionid";
            $order = 'fregionid';
            $onther_class_val = $onther_class;
            $onther_class = 'fregionid';
        }else{
            switch ($group){
                case 'fregionid':
                    $group = 'fregionid';
                    $group_name = 'tregion_name';
                    if($region_order == 0){
                        $onther_class_val = $onther_class;
                    }else{
                        $order = 'fregionid';
                        $onther_class_val = $onther_class;
                        $onther_class = 'fregionid';
                    }
                    $field = "$tregion_field_name as tregion_name,DIYUBIANHAO as fregionid";
                    $field_data = "a.tregion_name,a.fregionid";
                    break;
                case 'fad_class_code':
                    $group = 'fad_class_code';
                    $group_name = 'tadclass_name';
                    $field = 'LEIBIEMINGCHENG as tadclass_name,LEIBIEId as fad_class_code';
                    $field_data = "a.tadclass_name,a.fad_class_code";
                    break;
                case 'fmediaid':
                    $group = 'fmediaid';
                    $group_name = 'tmedia_name';
                    $field = "PINDAOMINGCHENG as tmedia_name,$tregion_field_name as tregion_name,PINDAOBIANHAO as fmediaid";
                    $field_data = "a.tmedia_name,a.tregion_name,a.fmediaid";
                    break;
                case 'fmediaownerid':
                    $group = 'PINDAOBIANHAO';
                    $group_name = 'tmedia_name';
                    $field = "PINDAOMINGCHENG as tmedia_name,$tregion_field_name as tregion_name,PINDAOBIANHAO";
                    $field_data = "a.tmedia_name,a.tregion_name,a.PINDAOBIANHAO";
                    $where['_string'] = '1 = 0';
                    break;
            }
        }
        $hz_data = M('t_guanggaohuizong')
            ->field("
            $field,
            sum(ZONGTIAOCI) AS fad_times,
            sum(ZONGHSICHANG) AS fad_play_len
            ")
            ->where($where_region)
            ->where($where)
            ->group($group)
            ->fetchSql(true)
            ->select();
        $wf_data = M('t_weifaguanggao')
            ->field("
                $field,
                SUM(
                    CASE
                    WHEN SHICHANG REGEXP '^[0-9]+$' THEN
                        SHICHANG
                    ELSE
                        0
                    END
                ) AS fad_illegal_play_len,
                COUNT(1) AS fad_illegal_times
            ")
            ->where($where)
            ->group($group)
            ->fetchSql(true)
            ->select();
        $Model = new \Think\Model();
        $data = $Model->table("($hz_data)")
            ->alias('a')
            ->field("
                $field_data,
                a.fad_times as fad_times,
                IFNULL(b.fad_illegal_times,0) as fad_illegal_times,
                round(
                    (
                        sum(IFNULL(b.fad_illegal_times,0)) / sum(a.fad_times)
                    ),
                    4
                ) AS times_illegal_rate,
                a.fad_play_len,
                IFNULL(b.fad_illegal_play_len,0) as fad_illegal_play_len,
                round(
                    (
                        sum(IFNULL(b.fad_illegal_play_len,0)) / sum(a.fad_play_len)
                    ),
                    4
                ) AS lens_illegal_rate
            ")
            ->join("($wf_data) as b on b.$group = a.$group",'left')
            ->group($group)//->fetchSql(true)
            ->order($order)
            ->select();
        //dump($data);exit;

        $where['BOCHURIQI'] = ['BETWEEN',[$last_s_time,$last_e_time]];
        $prv_hz_data = M('t_guanggaohuizong')
            ->field("
            $field,
            sum(ZONGTIAOCI) AS fad_times,
            sum(ZONGHSICHANG) AS fad_play_len
            ")
            ->where($where_region)
            ->where($where)
            ->group($group)
            ->fetchSql(true)
            ->select();
        $prv_wf_data = M('t_weifaguanggao')
            ->field("
                $field,
                SUM(
                    CASE
                    WHEN SHICHANG REGEXP '^[0-9]+$' THEN
                        SHICHANG
                    ELSE
                        0
                    END
                ) AS fad_illegal_play_len,
                COUNT(LEIBIEId) AS fad_illegal_times
            ")
            ->where($where)
            ->group($group)
            ->fetchSql(true)
            ->select();

        $prv_Model = new \Think\Model();
        $prv_data = $prv_Model->table("($prv_hz_data)")->cache(true,600)
            ->alias('a')
            ->field("
                $field_data,
                a.fad_times as fad_times,
                IFNULL(b.fad_illegal_times,0) as fad_illegal_times,
                round(
                    (
                        sum(IFNULL(b.fad_illegal_times,0)) / sum(a.fad_times)
                    ),
                    4
                ) AS times_illegal_rate,
                a.fad_play_len,
                IFNULL(b.fad_illegal_play_len,0) as fad_illegal_play_len,
                round(
                    (
                        sum(IFNULL(b.fad_illegal_play_len,0)) / sum(a.fad_play_len)
                    ),
                    4
                ) AS lens_illegal_rate
            ")
            ->join("($prv_wf_data) as b on b.$group = a.$group",'left')
            ->group($group)
            ->order($order)
            ->select();
        foreach ($prv_data as $prv_data_key => $prv_data_val){
            $prv_all_count['fad_times'] += $prv_data[$prv_data_key]['fad_times'];
            $prv_all_count['fad_illegal_times'] += $prv_data[$prv_data_key]['fad_illegal_times'];
            $prv_all_count['fad_illegal_play_len'] += $prv_data[$prv_data_key]['fad_illegal_play_len'];
            $prv_all_count['fad_play_len'] += $prv_data[$prv_data_key]['fad_play_len'];
        }

        foreach ($data as $data_key => $data_val){
            $all_count['fad_times'] += $data[$data_key]['fad_times'];
            $all_count['fad_illegal_times'] += $data[$data_key]['fad_illegal_times'];
            $all_count['fad_illegal_play_len'] += $data[$data_key]['fad_illegal_play_len'];
            $all_count['fad_play_len'] += $data[$data_key]['fad_play_len'];
        }
        $data_array = [];
        foreach ($data as $data_key=>$data_val) {
            $data_array[$data_val[$group]] = $data_val;
        }

        $prv_data_array = [];
        foreach ($prv_data as $prv_data_key=>$illegal_prv_ad_val) {
            $prv_data_array[$illegal_prv_ad_val[$group]] = $illegal_prv_ad_val;
        }

        //对比上个周期的数据
        foreach ($data as $data_key=>$data_value){
            if(isset($prv_data_array[$data_value[$group]])){
                if (!$prv_data_array[$data_value[$group]]['fad_illegal_count'] || ($data_value['fad_illegal_count'] - $prv_data_array[$data_value[$group]]['fad_illegal_count']) == 0) {
                    $data[$data_key]['prv_fad_illegal_count'] = '(0.00%)';
                } else {

                    $prv_fad_illegal_count = round(($data_value['fad_illegal_count'] - $prv_data_array[$data_value[$group]]['fad_illegal_count']) / $prv_data_array[$data_value[$group]]['fad_illegal_count'], 4) * 100;
                    if ($prv_fad_illegal_count < 0) {
                        $data[$data_key]['prv_fad_illegal_count'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_count . '%)';
                    } else {
                        $data[$data_key]['prv_fad_illegal_count'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_count . '%)';
                    }
                }

                if (!$prv_data_array[$data_value[$group]]['fad_illegal_times'] || ($data_value['fad_illegal_times'] - $prv_data_array[$data_value[$group]]['fad_illegal_times']) == 0) {
                    $data[$data_key]['prv_fad_illegal_times'] = '(0.00%)';
                } else {

                    $prv_fad_illegal_times = round(($data_value['fad_illegal_times'] - $prv_data_array[$data_value[$group]]['fad_illegal_times']) / $prv_data_array[$data_value[$group]]['fad_illegal_times'], 4) * 100;
                    if ($prv_fad_illegal_times < 0) {
                        $data[$data_key]['prv_fad_illegal_times'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_times . '%)';
                    } else {
                        $data[$data_key]['prv_fad_illegal_times'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_times . '%)';
                    }
                }

                if (!$prv_data_array[$data_value[$group]]['fad_illegal_play_len'] || ($data_value['fad_illegal_play_len'] - $prv_data_array[$data_value[$group]]['fad_illegal_play_len']) == 0) {
                    $data[$data_key]['prv_fad_illegal_play_len'] = '(0.00%)';
                } else {

                    $prv_fad_illegal_play_len = round(($data_value['fad_illegal_play_len'] - $prv_data_array[$data_value[$group]]['fad_illegal_play_len']) / $prv_data_array[$data_value[$group]]['fad_illegal_play_len'], 4) * 100;

                    if ($prv_fad_illegal_play_len < 0) {
                        $data[$data_key]['prv_fad_illegal_play_len'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_play_len . '%)';
                    } else {
                        $data[$data_key]['prv_fad_illegal_play_len'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_play_len . '%)';
                    }
                }
            }

        }


        //处理排序后的数据

        $array_num = 0;
        $data_pm = [];
        foreach ($data as $data_key=>$data_value){
            $data[$data_key]['times_illegal_rate'] = round($data_value['times_illegal_rate'],4)*100;
            $data[$data_key]['lens_illegal_rate'] = round($data_value['lens_illegal_rate'],4)*100;
            //循环获取前35条数据
            if($array_num < 35 && $array_num < count($data) && $data_value[$onther_class] > 0){
                $data_pm['name'][] = $data_value[$group_name];
                //不同排名条件显示各自的数值
                switch ($onther_class){
                    case 'fregionid':
                        $data_pm['num'][] = $data[$data_key][$onther_class_val];
                        break;
                    case 'fad_illegal_times':
                        $data_pm['num'][] = $data_value['fad_illegal_times'];//违法条次
                        break;
                    case 'times_illegal_rate':
                        $data_pm['num'][] = round($data_value['times_illegal_rate'],4)*100;//条次违法率
                        break;
                    case 'fad_times':
                        $data_pm['num'][] = $data[$data_key]['fad_times'];//条次
                        break;
                    case 'fad_illegal_count':
                        $data_pm['num'][] = $data[$data_key]['fad_illegal_count'];//条数
                        break;
                    case 'counts_illegal_rate':
                        $data_pm['num'][] = $data[$data_key]['counts_illegal_rate'];//条数违法率
                        break;
                    case 'fad_count':
                        $data_pm['num'][] = $data[$data_key]['fad_count'];//条数
                        break;
                    case 'fad_play_len':
                        $data_pm['num'][] = $data[$data_key]['fad_play_len'];//时长
                        break;
                    case 'fad_illegal_play_len':
                        $data_pm['num'][] = $data_value['fad_illegal_play_len'];//违法时长
                        break;
                    case 'lens_illegal_rate':
                        $data_pm['num'][] = round($data_value['lens_illegal_rate'],4)*100;//时长违法率
                        break;
                }
                $array_num++;//fad_count  广告条次：fad_times  广告时长：fad_play_len
            }

        }
        if($group == 'fad_class_code') {
            $data_pm = [];
            foreach ($data as $ad_class_key=>$ad_class_value){
                if($ad_class_value[$onther_class] != 0){
                    $data_pm['data'][] = [
                        'name' => $ad_class_value[$group_name],
                        'value' => $ad_class_value[$onther_class]
                    ];
                    $data_pm['name'][] = $ad_class_value[$group_name];
                }
            }
        }

        $all_prv_fad_illegal_play_len = round(($all_count['fad_illegal_play_len'] - $prv_all_count['fad_illegal_play_len']) / $prv_all_count['fad_illegal_play_len'], 4) * 100;
        $all_prv_fad_illegal_times = round(($all_count['fad_illegal_times'] - $prv_all_count['fad_illegal_times']) / $prv_all_count['fad_illegal_times'], 4) * 100;

        $times_illegal_rate = round($all_count['fad_illegal_times']/$all_count['fad_times'],4)*100;
        $lens_illegal_rate = round($all_count['fad_illegal_play_len']/$all_count['fad_play_len'],4)*100;

        $prv_times_illegal_rate = round($prv_all_count['fad_illegal_times']/$prv_all_count['fad_times'],4)*100;
        $prv_lens_illegal_rate = round($prv_all_count['fad_illegal_play_len']/$prv_all_count['fad_play_len'],4)*100;

        if ($all_prv_fad_illegal_count < 0) {
            $all_prv_fad_illegal_count = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_count . '%)';
        } else {
            if($all_prv_fad_illegal_count == 0){
                $all_prv_fad_illegal_count = '(0.00%)';

            }else{
                $all_prv_fad_illegal_count = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_count . '%)';
            }
        }

        if ($all_prv_fad_illegal_play_len < 0) {
            $all_prv_fad_illegal_play_len = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_play_len . '%)';
        } else {
            if($all_prv_fad_illegal_play_len == 0){
                $all_prv_fad_illegal_play_len = '(0.00%)';

            }else{
                $all_prv_fad_illegal_play_len = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_play_len . '%)';
            }
        }

        if ($all_prv_fad_illegal_times < 0) {
            $all_prv_fad_illegal_times = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_times . '%)';
        } else {
            if($all_prv_fad_illegal_times == 0){
                $all_prv_fad_illegal_times = '(0.00%)';

            }else{
                $all_prv_fad_illegal_times = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_times . '%)';
            }
        }

        $compar_fad_count = $all_count['fad_count'] - $prv_all_count['fad_count'];
        if ($compar_fad_count < 0) {
            $compar_fad_count = '减少'.($compar_fad_count*-1);
        } else {
            if($compar_fad_count == 0){
                $compar_fad_count = '无变化';

            }else{
                $compar_fad_count = '增加'.$compar_fad_count;
            }
        }
        $compar_fad_illegal_count = $all_count['fad_illegal_count'] - $prv_all_count['fad_illegal_count'];
        if ($compar_fad_illegal_count < 0) {
            $compar_fad_illegal_count = '减少'.($compar_fad_illegal_count*-1);
        } else {
            if($compar_fad_illegal_count == 0){
                $compar_fad_illegal_count = '无变化';

            }else{
                $compar_fad_illegal_count = '增加'.$compar_fad_illegal_count;
            }
        }
        $compar_illegal_count_rate = $counts_illegal_rate - $prv_counts_illegal_rate;
        if ($compar_illegal_count_rate < 0) {
            $compar_illegal_count_rate = '下降'.($compar_illegal_count_rate*-1).'%';
        } else {
            if($compar_illegal_count_rate == 0){
                $compar_illegal_count_rate = '无变化';

            }else{
                $compar_illegal_count_rate = '上升'.$compar_illegal_count_rate.'%';
            }
        }
        $compar_fad_times = $all_count['fad_times'] - $prv_all_count['fad_times'];
        if ($compar_fad_times < 0) {
            $compar_fad_times = '减少'.($compar_fad_times*-1);
        } else {
            if($compar_fad_times == 0){
                $compar_fad_times = '无变化';

            }else{
                $compar_fad_times = '增加'.$compar_fad_times;
            }
        }
        $compar_fad_illegal_times = $all_count['fad_illegal_times'] - $prv_all_count['fad_illegal_times'];
        if ($compar_fad_illegal_times < 0) {
            $compar_fad_illegal_times = '减少'.($compar_fad_illegal_times*-1);
        } else {
            if($compar_fad_illegal_times == 0){
                $compar_fad_illegal_times = '无变化';

            }else{
                $compar_fad_illegal_times = '增加'.$compar_fad_illegal_times;
            }
        }
        $compar_illegal_times_rate = $times_illegal_rate - $prv_times_illegal_rate;
        if ($compar_illegal_times_rate < 0) {
            $compar_illegal_times_rate = '下降'.($compar_illegal_times_rate*-1).'%';
        } else {
            if($compar_illegal_times_rate == 0){
                $compar_illegal_times_rate = '无变化';
            }else{
                $compar_illegal_times_rate = '上升'.$compar_illegal_times_rate.'%';
            }
        }
        $compar_fad_play_len = $all_count['fad_play_len'] - $prv_all_count['fad_play_len'];
        if ($compar_fad_play_len < 0) {
            $compar_fad_play_len = '减少'.($compar_fad_play_len*-1);
        } else {
            if($compar_fad_play_len == 0){
                $compar_fad_play_len = '无变化';

            }else{
                $compar_fad_play_len = '增加'.$compar_fad_play_len;
            }
        }
        $compar_fad_illegal_play_len = $all_count['fad_illegal_play_len'] - $prv_all_count['fad_illegal_play_len'];
        if ($compar_fad_illegal_play_len < 0) {
            $compar_fad_illegal_play_len = '减少'.($compar_fad_illegal_play_len*-1);
        } else {
            if($compar_fad_illegal_play_len == 0){
                $compar_fad_illegal_play_len = '无变化';

            }else{
                $compar_fad_illegal_play_len = '增加'.$compar_fad_illegal_play_len;
            }
        }
        $compar_illegal_play_len_rate = $lens_illegal_rate - $prv_lens_illegal_rate;
        if ($compar_illegal_play_len_rate < 0) {
            $compar_illegal_play_len_rate = '下降'.($compar_illegal_play_len_rate*-1).'%';
        } else {
            if($compar_illegal_play_len_rate == 0){
                $compar_illegal_play_len_rate = '无变化';
            }else{
                $compar_illegal_play_len_rate = '上升'.$compar_illegal_play_len_rate.'%';
            }
        }
        $compar_prv_com_score = round($all_count['com_score'] - $prv_all_count['prv_com_score'],2);
        if ($compar_prv_com_score < 0) {
            $compar_prv_com_score = '减少'.($compar_prv_com_score*-1);
        } else {
            if($compar_prv_com_score == 0){
                $compar_prv_com_score = '无变化';

            }else{
                $compar_prv_com_score = '增加'.$compar_prv_com_score;
            }
        }

        //统计放在最后一条$all_count     $prv_all_count
        $data[] = [
            'titlename'=>'全部',
            "prv_fad_illegal_count"=>$all_prv_fad_illegal_count,
            "prv_fad_illegal_play_len"=>$all_prv_fad_illegal_play_len,
            "prv_fad_illegal_times"=>$all_prv_fad_illegal_times,

            'fad_times'=>$all_count['fad_times'],//广告条次
            'fad_illegal_times'=>$all_count['fad_illegal_times'],//违法广告条次
            'times_illegal_rate'=>round($all_count['fad_illegal_times']/$all_count['fad_times'],4)*100,
            'fad_play_len'=>$all_count['fad_play_len'],//总时长
            'fad_illegal_play_len'=>$all_count['fad_illegal_play_len'],//违法时长
            'lens_illegal_rate'=>round($all_count['fad_illegal_play_len']/$all_count['fad_play_len'],4)*100,

            "compar_fad_times"=>$compar_fad_times,//对比上个周期广告条次
            "compar_fad_illegal_times"=>$compar_fad_illegal_times,//对比上个周期违法广告条次
            "compar_illegal_times_rate"=>$compar_illegal_times_rate,//对比上个周期条次违法率
            "compar_fad_play_len"=>$compar_fad_play_len,//对比上个周期总时长
            "compar_fad_illegal_play_len"=>$compar_fad_illegal_play_len,//对比上个周期违法时长
            "compar_illegal_play_len_rate"=>$compar_illegal_play_len_rate,//对比上个周期时长违法率
        ];
        $finally_data = [
            'count'=> count($data)-1,
            'data_pm'=>$data_pm,//图表数据
            'data_list'=>$data//列表数据
        ];
        if($is_out_excel == 1){
            //如果是线索菜单并且是点击了导出按钮
            $objPHPExcel = new \PHPExcel();
            $objPHPExcel
                ->getProperties()  //获得文件属性对象，给下文提供设置资源
                ->setCreator( "MaartenBalliauw")             //设置文件的创建者
                ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
                ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
                ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
                ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
                ->setKeywords( "office 2007 openxmlphp")        //设置标记
                ->setCategory( "Test resultfile");                //设置类别

            $sheet = $objPHPExcel->setActiveSheetIndex(0);
            //分组导出数据1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
            switch ($group){
                case 'PINDAOBIANHAO':
                    $sheet ->setCellValue('A2','排名');
                    $sheet ->setCellValue('B2','媒体所在地');
                    $sheet ->setCellValue('C2','媒体名称');
                    $sheet ->setCellValue('D2','总条次');
                    $sheet ->setCellValue('E2','违法条次');
                    $sheet ->setCellValue('F2','条次违法率');
                    $sheet ->setCellValue('G2','总时长');
                    $sheet ->setCellValue('H2','违法时长');
                    $sheet ->setCellValue('I2','时长违法率');
                    foreach ($data as $key => $value) {

                        $sheet ->setCellValue('A'.($key+3),($key+1));
                        if($data_count == $key){
                            $sheet ->setCellValue('B'.($key+3),'全部地域');
                            $sheet ->setCellValue('C'.($key+3),'全部媒体');
                        }else{
                            $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                            $sheet ->setCellValue('C'.($key+3),$value['tmedia_name']);
                        }
                        $sheet ->setCellValue('D'.($key+3),$value['fad_times']);
                        $sheet ->setCellValue('E'.($key+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('F'.($key+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('G'.($key+3),$value['fad_play_len']);
                        $sheet ->setCellValue('H'.($key+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('I'.($key+3),$value['lens_illegal_rate'].'%');

                    }
                    break;
                case 'DIYUBIANHAO':
                    $sheet ->setCellValue('A2','排名');
                    $sheet ->setCellValue('B2','地域名称');
                    $sheet ->setCellValue('C2','总条次');
                    $sheet ->setCellValue('D2','违法条次');
                    $sheet ->setCellValue('E2','条次违法率');
                    $sheet ->setCellValue('F2','总时长');
                    $sheet ->setCellValue('G2','违法时长');
                    $sheet ->setCellValue('H2','时长违法率');
                    foreach ($data as $key => $value) {


                        $sheet ->setCellValue('A'.($key+3),($key+1));
                        if($data_count == $key){
                            $sheet ->setCellValue('B'.($key+3),'全部地域');
                        }else{
                            $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                        }
                        $sheet ->setCellValue('C'.($key+3),$value['fad_times']);
                        $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('E'.($key+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('F'.($key+3),$value['fad_play_len']);
                        $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('H'.($key+3),$value['lens_illegal_rate'].'%');

                    }
                    break;
                case 'fmediaownerid':
                    $sheet ->setCellValue('A2','排名');
                    $sheet ->setCellValue('B2','媒介机构所在地');
                    $sheet ->setCellValue('C2','媒介机构名称');
                    $sheet ->setCellValue('D2','媒介类型');
                    $sheet ->setCellValue('E2','总条次');
                    $sheet ->setCellValue('F2','违法条次');
                    $sheet ->setCellValue('G2','条次违法率');
                    $sheet ->setCellValue('H2','总时长');
                    $sheet ->setCellValue('I2','违法时长');
                    $sheet ->setCellValue('J2','时长违法率');
                    break;
                case 'LEIBIEId':
                    $sheet ->setCellValue('A2','排名');
                    $sheet ->setCellValue('B2','广告类别');
                    $sheet ->setCellValue('C2','总条次');
                    $sheet ->setCellValue('D2','违法条次');
                    $sheet ->setCellValue('E2','条次违法率');
                    $sheet ->setCellValue('F2','总时长');
                    $sheet ->setCellValue('G2','违法时长');
                    $sheet ->setCellValue('H2','时长违法率');

                    foreach ($data as $key => $value) {
                        $sheet ->setCellValue('A'.($key+3),($key+1));
                        if($data_count == $key){
                            $sheet ->setCellValue('B'.($key+3),'全部类别');
                        }else{
                            $sheet ->setCellValue('B'.($key+3),$value['tadclass_name']);
                        }
                        $sheet ->setCellValue('C'.($key+3),$value['fad_times']);
                        $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('E'.($key+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('F'.($key+3),$value['fad_play_len']);
                        $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('H'.($key+3),$value['lens_illegal_rate'].'%');
                    }
                    break;
            }
            $objActSheet =$objPHPExcel->getActiveSheet();
            $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
            $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            $date = date('Ymdhis');
            $savefile = './Public/Xls/'.$date.'.xlsx';
            $savefile2 = '/Public/Xls/'.$date.'.xlsx';
            $objWriter->save($savefile);
            //即时导出下载
            /*          header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        header('Content-Disposition:attachment;filename="'.$date.'.xlsx"');
                        header('Cache-Control:max-age=0');
                        $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
                        $objWriter->save( 'php://output');*/
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$savefile2));

        }else{
            $this->ajaxReturn($finally_data);
        }

    }

    //中视广联汇违法广告列表
    public function zsgl_ill_list()
    {
        Check_QuanXian(['monitoringDataZS']);
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $ischeck = $ALL_CONFIG['ischeck'];

        $fad_class_code   = I('fad_class_code');//广告类别，用于汇总中违法广告数据的查看
        $fmediaid         = I('fmediaid');//媒体ID，用于汇总中违法广告数据的查看
        $fmedianame       = I('fmedianame');//媒体名称，用于历史汇总中违法广告明细数据的查看
        $mclass           = I('mclass');//媒体类别，用于汇总中违法广告数据的查看
        $fad_class_code2  = I('fad_class_code2');//广告类别组，用于汇总中违法广告数据的查看
        $area             = I('area');//地域ID，用于汇总中违法广告数据的查看
        $iscontain        = I('iscontain');//地域ID，用于汇总中违法广告数据的查看
        $flevel           = I('flevel');//地域级别，用于汇总中违法广告数据的查看
        //$is_show_longad   = I('is_show_longad');//是否有长广告，用于汇总中违法广告数据的查看
        $zd = 'BOCHURIQI';

        if(!empty($area)){
            $where['DIYUBIANHAO'] = $area;
            if($iscontain == 1){
                $re_level = $this->judgment_region($area);//当前用户行政等级
                switch ($re_level){
                    case 1:
                        $where['DIYUBIANHAO'] = ['like',substr($area,0,2).'%'];
                        break;
                    case 2:
                    case 3:
                    case 4:
                        $where['DIYUBIANHAO'] = ['like',substr($area,0,4).'%'];
                        break;
                }
            }
        }

        if(!empty($fmediaid)){
            $where['PINDAOBIANHAO'] = $fmediaid;
        }

        if(!empty($fmedianame)){
            $where['PINDAOMINGCHENG'] = ['like',"%$fmedianame%"];
        }

        if(!empty($mclass)){

            switch ($mclass){
                case '01':
                    $where['MEITILEIXING'] = 1;//电视
                    break;
                case '02':
                    $where['MEITILEIXING'] = 0;//广播
                    break;
                case '03':
                    $where['MEITILEIXING'] = 2;//报纸
                    break;
            }
        }
        $where['_string'] = '1 = 1';

        if(!empty($flevel)){
            if($area != '100000'){
                $where['_string'] .= ' and FLEVEL ='.$flevel;
            }
        }


        //时间条件筛选
        $years      = I('years');//选择年份
        $times_table = I('times_table');
        $table_condition = I('table_condition');//选择时间

        if($times_table == 'tbn_ad_summary_day'){
            $timetypes = 0;
            $timeval = $table_condition;
        }elseif($times_table == 'tbn_ad_summary_week'){
            $timetypes = 10;
            $timeval = $table_condition;
        }elseif($times_table == 'tbn_ad_summary_half_month'){
            $timetypes = 20;
            $table_condition2 = explode('-', $table_condition);
            $table_condition3 = ((int)$table_condition2[0]-1)*2;
            $table_condition4 = ((int)$table_condition2[1])==1?1:2;
            $timeval = $table_condition3+$table_condition4;
        }elseif($times_table == 'tbn_ad_summary_month'){
            $timetypes = 30;
            $table_condition2 = explode('-', $table_condition);
            $timeval = (int)$table_condition2[0];
        }elseif($times_table == 'tbn_ad_summary_quarter'){
            $timetypes = 40;
            if($table_condition == '01-01'){
                $timeval = 1;
            }elseif($table_condition == '04-01'){
                $timeval = 2;
            }elseif($table_condition == '07-01'){
                $timeval = 3;
            }else{
                $timeval = 4;
            }
        }elseif($times_table == 'tbn_ad_summary_half_year'){
            $timetypes = 50;
            $table_condition2 = explode('-', $table_condition);
            if($table_condition2[2] == '01'){
                $timeval = 1;
            }else{
                $timeval = 2;
            }
        }elseif($times_table == 'tbn_ad_summary_year'){
            $timetypes = 60;
            $timeval = 1;
        }


        if(!empty($years)){
            switch ($timetypes) {
                case 10:
                    $where_time .=' and year('.$zd.')='.$years;
                    $where_time .= ' and weekofyear('.$zd.')='.$timeval;
                    break;
                case 20:
                    $month = floor($timeval/2);
                    $ts = $timeval%2;
                    if($ts == 1){
                        $starttime  = $years.'-'.($month+1).'-01';
                        if(($month+1) == 2){
                            $endtime    = $years.'-'.($month+1).'-14';
                        }else{
                            $endtime    = $years.'-'.($month+1).'-15';
                        }
                        $where_time .= ' and  DATE_FORMAT('.$zd.',"%Y-%m-%d") between "'.$starttime.'" and "'.$endtime.'"';
                    }else{
                        if(($month+1) == 2){
                            $starttime  = $years.'-'.$month.'-15';
                        }else{
                            $starttime  = $years.'-'.$month.'-16';
                        }
                        $where_time .= ' and  DATE_FORMAT('.$zd.',"%Y-%m-%d") between "'.$starttime.'" and LAST_DAY("'.$starttime.'")';
                    }
                    break;
                case 30:
                    $where_time .=' and  year('.$zd.')='.$years;
                    $where_time .= ' and month('.$zd.')='.$timeval;
                    break;
                case 40:
                    $where_time .=' and  year('.$zd.')='.$years;
                    $where_time .= ' and quarter('.$zd.')='.$timeval;
                    break;
                case 50:
                    if($timeval == 1){
                        $starttime  = $years.'-01-01';
                        $endtime    = $years.'-07-01';
                    }else{
                        $starttime  = $years.'-07-01';
                        $endtime    = ($years+1).'-01-01';
                    }
                    $where_time .= ' and  DATE_FORMAT('.$zd.',"%Y-%m-%d") between "'.$starttime.'" and concat(YEAR("'.$endtime.'"),"-12-31")';
                    break;
                case 60:
                    $where_time .=' and  year('.$zd.')='.$years;
                    break;
            }
            $where['_string'] .= $where_time;
        }

        if(!empty($fad_class_code)){//广告类别排名查询
            $where['left(LEIBIEId,2)'] = $fad_class_code;
            if($flevel == 4){
                $where['_string'] .= 'FLEVEL in (1,2,3,4)';
            }
            if($flevel == 5){
                $where['_string'] .= 'FLEVEL in (2,3,4,5)';
            }
        }

        if(!empty($fad_class_code2)){
            $fadclass = [];
            foreach ($fad_class_code2 as $key => $value) {
                if($value<10){
                    $fadclass[] = '0'.$value;
                }else{
                    $fadclass[] = $value;
                }
            }
            $where['left(LEIBIEId,2)'] = array('in',$fadclass);
        }

        $ad_name    = I('name');//广告名
        $start_date = I('start_date');//开始日期
        $end_date   = I('end_date');//结束日期

        $p  = I('page', 1);//当前第几页
        $pp = I('pagecount', 10);//每页显示多少记录

        if($ad_name){
            $where['GGMINGCHENG'] = array('like','%'.$ad_name.'%');
        }
        if($start_date && $end_date){
            $where['BOCHURIQI'] = array('between',array($start_date,$end_date));
        }

        $count = M('t_weifaguanggao')
            ->where($where)
            ->count();


        $res = M('t_weifaguanggao')
            ->field('
            CASE
            WHEN MEITILEIXING = 0 THEN
            "2"
            WHEN MEITILEIXING = 1 THEN
            "1"
            WHEN MEITILEIXING = 2 THEN
            "3"
            END as fmedia_class,
            LEIBIEMINGCHENG as fadclass,
            GGMINGCHENG as fad_name,
            PINDAOMINGCHENG as fmedianame,
            BOCHURIQI as fissue_date,
            KAISHISHIJIAN as fstarttime,
            JIESHUSHIJIAN as fendtime,
            WEIFAXIANG as fillegal_code,
            WEIFABIAOXIAN as fexpressions,
            SHICHANG as difftime
        ')
            ->where($where)
            ->page($p,$pp)
            ->select();
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$res)));
    }

    //中视广联媒体列表
    public function zsgl_media_list(){

        $keyword = I('keyword','中国');
        if(!empty($keyword)){
            $map['PINDAOMINGCHENG'] = ['like',"%$keyword%"];
            $media_list = M('t_weifaguanggao')
                ->field("
                    PINDAOBIANHAO as id,
                    PINDAOMINGCHENG as name
                ")->where($map)
                ->group('PINDAOMINGCHENG')
                ->order('PINDAOBIANHAO')
                ->limit(10)
                ->select();
        }else{
            $media_list = [];
        }
        $this->ajaxReturn([
            'code' => 0,
            'msg'  => '数据接收成功！',
            'data' => $media_list
        ]);

    }

    /*
     * 排序
     * */
    public function pxsf($data = [],$score_rule = 'com_score',$is_desc = true){
        $data_count = count($data);
        for($k=0;$k<=$data_count;$k++)
        {
            for($j=$data_count-1;$j>$k;$j--){
                if($is_desc){
                    if($data[$j][$score_rule]>$data[$j-1][$score_rule]){
                        $temp = $data[$j];
                        $data[$j] = $data[$j-1];
                        $data[$j-1] = $temp;
                    }
                }else{
                    if($data[$j][$score_rule]<$data[$j-1][$score_rule]){
                        $temp = $data[$j];
                        $data[$j] = $data[$j-1];
                        $data[$j-1] = $temp;
                    }
                }

            }
        }
        return $data;
    }
    /*
    * 获取条件
    * */
    public function get_condition(){
        ini_set('memory_limit','1024M');
        set_time_limit(0);
        session_write_close();//停止使用session
        $system_num = getconfig('system_num');
        $isfixeddata = I('isfixeddata');//是否实时数据
        if($system_num == '100000'||empty($isfixeddata)){
            $summary_table = "tbn_ad_summary_day_z";
            $where_se[$summary_table.'.fcustomer'] = $system_num;
        }else{
            $summary_table = "tbn_ad_summary_day";
        }

        $user = session('regulatorpersonInfo');//fregulatorid机构ID

        $region_level = $this->judgment_region($user['regionid']);

        $data['jgname'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');

        $year_mod = M($summary_table)->field('left(fdate,4) as yeas')->where($where_se)->group('fdate')->select();

        foreach ($year_mod as $year_mod_key=>$year_mod_val){
            if(!in_array($year_mod_val['yeas'],$years)){
                $years[] = $year_mod_val['yeas'];
            }
        }
        $years = [];
        $year_num = date('Y') - 2015;
        $i = 0;
        while ($i <= $year_num){
            $years[] = date('Y') - $i;
            $i++;
        }

        $data['year'] = $years;//选年

        $data['select_table']['times_table'] = [
            'tbn_ad_summary_week'=>'周',
            'tbn_ad_summary_half_month'=>'半月',
            'tbn_ad_summary_month'=>'月',
            'tbn_ad_summary_quarter'=>'季度',
            'tbn_ad_summary_half_year'=>'半年',
            'tbn_ad_summary_year'=>'年'
        ];//选表

        $select_half_year = [];
        foreach ($data['year'] as $v_half_year){
            $select_half_year[$v_half_year.'-01-01'] = $v_half_year.'上半年';//$v_year.'上半年';
            $select_half_year[$v_half_year.'-07-01'] = $v_half_year.'下半年';//$v_year.'下半年';
        }

        $select_year = [];
        foreach ($data['year'] as $v_year){
            $select_year[$v_year.'-01-01'] = $v_year.'年';//$v_year.'上半年';
        }


        $data['select_table']['period'] = [
            'tbn_ad_summary_week'=>$this->create_number(52),
            'tbn_ad_summary_half_month'=>$this->create_month(true),
            'tbn_ad_summary_month'=>$this->create_month(false),
            'tbn_ad_summary_quarter'=>[
                '01-01'=>'第一季度',
                '04-01'=>'第二季度',
                '07-01'=>'第三季度',
                '10-01'=>'第四季度'
            ],
            'tbn_ad_summary_half_year'=>$select_half_year,
            'tbn_ad_summary_year'=>$select_year
        ];

        //版本0613
        switch ($region_level){
            case 0:
                if($system_num == '100000'){
                    $data['region_level'] = [
                        '1' =>'省级',
                        '2' =>'副省级市',
                        '3' =>'计划单列市'
                    ];//行政级别
                    $data['region_level2'] = [
                        '1' =>'省级',
                        '2' =>'副省级市',
                        '3' =>'计划单列市'
                    ];//行政级别
                    $data['region'] = [
                        '1' =>'省级',
                        '2' =>'副省级市',
                        '3' =>'计划单列市'
                    ];//选区域
                    $data['region2'] = [
                        '1' =>'各省',
                        '2' =>'各副省级市',
                        '3' =>'各计划单列市'
                    ];//选区域
                }else{
                    $data['region_level'] = [
                        '1' =>'省级',
                        '2' =>'副省级市',
                        '3' =>'计划单列市'/*,
                        '4' =>'市级',
                        '5' =>'县级'*/
                    ];//行政级别
                    $data['region_level2'] = [
                        '1' =>'省级',
                        '2' =>'副省级市',
                        '3' =>'计划单列市',
                        '4' =>'市级',
                        '5' =>'县级'
                    ];//行政级别
                    $data['region'] = [
                        '1' =>'省级',
                        '2' =>'副省级市',
                        '3' =>'计划单列市'/*,
                        '4' =>'市级',
                        '5' =>'县级'*/
                    ];//选区域
                    $data['region2'] = [
                        '1' =>'各省',
                        '2' =>'各副省级市',
                        '3' =>'各计划单列市',
                        '4' =>'各市',
                        '5' =>'各县'
                    ];//选区域
                }

                $data['region_name'] = '全国';
                $data['region_name1'] = '全国';
                $data['region_name2'] = '全国';
                break;
            case 1:

                if($system_num == '100000' && session('regulatorpersonInfo.fregulatorlevel') != 30){
                    $data['region_level'] = [
                        '1' =>'省级',
                        '2' =>'副省级市',
                        '3' =>'计划单列市'
                    ];//行政级别
                    $data['region_level2'] = [
                        '2' =>'副省级市',
                        '3' =>'计划单列市'
                    ];//行政级别
                    $data['region'] = [
                        '1' =>'省级',
                        '2' =>'副省级市',
                        '3' =>'计划单列市'
                    ];//选区域
                    $data['region2'] = [
                        '1' =>'省级',
                        '2' =>'副省级市',
                        '3' =>'计划单列市'
                    ];//选区域
                }else{
                    $data['region_level'] = [
                        '1' =>'省级',
                        '2' =>'副省级市',
                        '3' =>'计划单列市'/*,
                        '4' =>'市级',
                        '5' =>'县级'*/
                    ];//行政级别
                    $data['region_level2'] = [
                        '2' =>'副省级市',
                        '3' =>'计划单列市',
                        '4' =>'市级',
                        '5' =>'县级'
                    ];//行政级别
                    $data['region'] = [
                        '1' =>'省级',
                        '2' =>'副省级市',
                        '3' =>'计划单列市'/*,
                        '4' =>'市级',
                        '5' =>'县级'*/
                    ];//选区域
                    $data['region2'] = [
                        '4' =>'各市',
                        '5' =>'各县'
                    ];//选区域
                }

                $data['region_name'] = '全国';//1
                $data['region_name1'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//1
                $data['region_name2'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//2
                break;
            case 2:
            case 3:
                $data['region_level'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市'/*,
                    '4' =>'市级',
                    '5' =>'县级'*/
                ];//行政级别
                $data['region_level2'] = [
                    '5' =>'县级'
                ];//行政级别
                $data['region'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市'/*,
                    '4' =>'市级',
                    '5' =>'县级'*/
                ];//选区域
                $data['region2'] = [
                    '5' =>'各县'
                ];//选区域
                $data['region_name'] = '全国';//1
                $fpid = substr($user['regionid'],0,2).'0000';
                $data['region_name1'] = M('tregion')->where(['fid'=>$fpid])->getField('fname1');//1
                $data['region_name2'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//2
                break;
            case 4:
                $data['region_level'] = [
                    '4' =>'市级',
                    '5' =>'县级'
                ];//行政级别
                $data['region_level2'] = [
                    '5' =>'县级'
                ];//行政级别
                $data['region'] = [
                    '4' =>'市级',
                    '5' =>'县级'
                ];//选区域
                $data['region2'] = [
                    '5' =>'各县'
                ];//选区域
                $fpid = substr($user['regionid'],0,2).'0000';
                $data['region_name'] = M('tregion')->where(['fid'=>$fpid])->getField('fname1');
                $data['region_name1'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//1
                $data['region_name2'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');
                break;
            case 5:
                $data['region_level'] = [
                    '5' =>'县级'
                ];//行政级别
                $data['region_level2'] = [
                    '5' =>'县级'
                ];//行政级别
                $data['region'] = [
                    '5' =>'县级'
                ];//选区域
                $data['region2'] = [
                    '5' =>'各县'
                ];//选区域
                $fpid = substr($user['regionid'],0,4).'00';
                $data['region_name'] = M('tregion')->where(['fid'=>$fpid])->getField('fname1');
                $data['region_name1'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//1
                $data['region_name2'] =M('tregion')->where(['fid'=>$fpid])->getField('fname1') ;
                break;
        }

        //版本0624
        /*switch ($region_level){
            case 0:
                $data['region_level'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市'
                ];//行政级别
                $data['region_level2'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市'
                ];//行政级别
                $data['region'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市'
                ];//选区域
                $data['region2'] = [
                    '1' =>'各省',
                    '2' =>'各副省级市',
                    '3' =>'各计划单列市'
                ];//选区域
                $data['region_name'] = '全国';
                $data['region_name1'] = '全国';
                $data['region_name2'] = '全国';
                break;
            case 1:
                $data['region_level'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市'
                ];//行政级别
                $data['region_level2'] = [
                    '2' =>'副省级市',
                    '3' =>'计划单列市'
                ];//行政级别
                $data['region'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市'
                ];//选区域
                $data['region2'] = [
                    '4' =>'各市',
                    '5' =>'各县'
                ];//选区域
                $data['region_name'] = '全国';//1
                $data['region_name1'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//1
                $data['region_name2'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//2
                break;
            case 2:
            case 3:
                $data['region_level'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市'
                ];//行政级别
                $data['region_level2'] = [
                    '5' =>'县级'
                ];//行政级别
                $data['region'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市'
                ];//选区域
                $data['region2'] = [
                    '5' =>'各县'
                ];//选区域
                $data['region_name'] = '全国';//1
                $fpid = substr($user['regionid'],0,2).'0000';
                $data['region_name1'] = M('tregion')->where(['fid'=>$fpid])->getField('fname1');//1
                $data['region_name2'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//2
                break;
            case 4:
                $data['region_level'] = [
                    '4' =>'市级',
                    '5' =>'县级'
                ];//行政级别
                $data['region_level2'] = [
                    '5' =>'县级'
                ];//行政级别
                $data['region'] = [
                    '4' =>'市级',
                    '5' =>'县级'
                ];//选区域
                $data['region2'] = [
                    '5' =>'各县'
                ];//选区域
                $fpid = substr($user['regionid'],0,2).'0000';
                $data['region_name'] = M('tregion')->where(['fid'=>$fpid])->getField('fname1');
                $data['region_name1'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//1
                $data['region_name2'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');
                break;
            case 5:
                $data['region_level'] = [
                    '5' =>'县级'
                ];//行政级别
                break;
        }*/

        //1.fmedia_class_code媒体类别 2.fmediaownerid媒体 3.fregionid行政区划代码 4.fmediaid媒介5.fad_class_code广告内容类别代码
        //fmedia_id    fad_name   fregion_id
        $data['cross_region_rank'] = [
            'fad_name' =>'广告',
            'fregion_id' =>'地域',
            'fmedia_id' =>'媒体'
        ];//跨地域排名条件

        $data['media_class']['media_pclass'] = [
            '' => '全部媒体',
            '01' => '电视',
            '02' => '广播',
            '03' => '报纸'
        ];//选媒体

        $media_class_01['fpid'] = [['like','01%'],['neq',''],['neq',0]];
        $media_class_02['fpid'] = [['like','02%'],['neq',''],['neq',0]];
        $media_class_03['fpid'] = [['like','03%'],['neq',''],['neq',0]];


        $data['media_class']['01'] = M('tmediaclass')->where($media_class_01)->getField('fid,fclass',true);//选电视媒体细类
        $data['media_class']['02'] = M('tmediaclass')->where($media_class_02)->getField('fid,fclass',true);//选广播媒体细类
        $data['media_class']['03'] = M('tmediaclass')->where($media_class_03)->getField('fid,fclass',true);//选报纸媒体细类
        //选广告大类id
        $ad_class_id_map['fpcode'] = [['neq',''],['neq','0']];
        $ad_class_id = M('tadclass')->where($ad_class_id_map)->distinct(true)->getField('fpcode',true);

        //选广告大类名称
        $ad_class_map['fcode'] = ['IN',$ad_class_id];
        //$ad_class = M('tadclass')->field('fcode as id,fadclass as name')->where($ad_class_map)->distinct(true)->order('fadclassid')->select();//选广告大类名称
        $ad_class = M('tadclass')->where($ad_class_map)->distinct(true)->order('fadclassid')->getField('fcode,fadclass',true);//选广告大类名称

        $ad_class_name1 = [];
        foreach ($ad_class as $key=>$val){
            $ad_class_name1[intval($key)] = $val;
        }
        /*        $ad_class_name2 = M('')->query("select DISTINCT LEIBIEId as value,LEIBIEMINGCHENG as name from t_guanggaohuizong WHERE LEIBIEId BETWEEN '01' AND '27' limit 24");//选广告大类名称
                $ad_class_name2 = $this->pxsf($ad_class_name2,'value',false);
                array_pop($ad_class_name2);*/
        $data['ad_class2'] = [
            ['value' => '01','name'=>'药品类'],
            ['value' => '02','name'=>'保健食品类'],
            ['value' => '03','name'=>'医疗器械类'],
            ['value' => '06','name'=>'普通食品类'],
            ['value' => '07','name'=>'酒类'],
            ['value' => '08','name'=>'烟草类'],
            ['value' => '09','name'=>'化妆品类'],
            ['value' => '10','name'=>'房地产类'],
            ['value' => '13','name'=>'电器类'],
            ['value' => '14','name'=>'交通产品类'],
            ['value' => '15','name'=>'知识产品类'],
            ['value' => '16','name'=>'普通商品类'],
            ['value' => '17','name'=>'医疗服务类'],
            ['value' => '18','name'=>'金融服务类'],
            ['value' => '19','name'=>'信息服务类'],
            ['value' => '20','name'=>'互联网服务类'],
            ['value' => '21','name'=>'商业招商投资类'],
            ['value' => '22','name'=>'教育培训服务类'],
            ['value' => '23','name'=>'会展文体活动服务类'],
            ['value' => '24','name'=>'普通服务类'],
            ['value' => '25','name'=>'形象宣传类'],
            ['value' => '26','name'=>'非商业类'],
            ['value' => '27','name'=>'其它']
        ];
        $data['ad_class'] = $ad_class_name1;

        //广告条数：
        $data['onther_class'] = [
            'fad_count'=>'条数',
            'fad_illegal_count'=>'违法条数',
            'counts_illegal_rate'=>'条数违法率',
            'fad_times'=>'条次',
            'fad_illegal_times'=>'违法条次',
            'times_illegal_rate'=>'条次违法率',
            'fad_play_len'=>'时长',
            'fad_illegal_play_len'=>'违法时长',
            'lens_illegal_rate'=>'时长违法率'
        ];//排名条件
        $data['onther_class2'] = [
            'fad_times'=>'条次',
            'fad_illegal_times'=>'违法条次',
            'times_illegal_rate'=>'条次违法率',
            'fad_play_len'=>'时长',
            'fad_illegal_play_len'=>'违法时长',
            'lens_illegal_rate'=>'时长违法率'
        ];//排名条件
        $data['data_class'] = [
            'fad_count'=>'条数',
            'fad_illegal_count'=>'违法条数',
            'fad_times'=>'条次',
            'fad_illegal_times'=>'违法条次',
            'fad_play_len'=>'时长',
            'fad_illegal_play_len'=>'违法时长'
        ];//排名条件

        $data['data_class1'] = [
            'fad_times'=>'条次',
            'fad_illegal_times'=>'违法条次',
            'fad_play_len'=>'时长',
            'fad_illegal_play_len'=>'违法时长'
        ];//排名条件

        $data['data_class2'] = [
            'fad_times'=>'条次',
            'fad_illegal_times'=>'违法条次',
            'fad_play_len'=>'时长',
            'fad_illegal_play_len'=>'违法时长'
        ];//排名条件


        $this->ajaxReturn($data);
    }

    //生成数字
    public function create_number($num){
        $n = 0;
        $m = [];
        while ($n < $num){
            $n++;
            $m[] = $n;
        }
        return $m;
    }

    //生成半月
    public function create_month($half = true,$kv = true){
        $n = 0;
        $m = [];
        while ($n < 12){
            $n++;
            $str = $n;
            if($n<10){
                $str = '0'.$n;
            }
            if($kv){
                if($half){
                    $m[$str.'-01'] = $n.'月上半月';
                    if($n == 2){
                        $m[$str.'-15'] = $n.'月下半月';
                    }else{
                        $m[$str.'-16'] = $n.'月下半月';
                    }
                }else{
                    $m[$str.'-01'] = $n.'月';
                }
            }else{
                if($half){
                    $m[] = $str.'-01';
                    if($n == 2){
                        $m[] = $str.'-15';
                    }else{
                        $m[] = $str.'-16';
                    }
                }else{
                    $m[] = $str.'-01';
                }
            }

        }
        return $m;
    }

    //判断当前用户行政等级,并返回
    public function judgment_region($regionid){
        $level = M('tregion')->where(['fid'=>$regionid])->getField('flevel');
        /*        if($level == 2 || $level == 3){
                    $data['region'] = [
                        '1' =>'省',
                        '2' =>'副省级市',
                        '3' =>'计划单列市',
                        '4' =>'市',
                        '5' =>'区县'
                    ];//选区域

                    $data['region_level'] = [
                        '1' =>'全国',
                        '2' =>'省级',
                        '3' =>'市级',
                        '4' =>'县级'
                    ];//行政级别
                }*/
        return $level;
    }


    //获取行政等级下的所有id
    public function get_region_ids($region_level){
        $region_ids = M('tregion')->where(['flevel'=>$region_level])->getField('fid',true);
        return $region_ids;
    }

    //获取上个周期
    public function get_previous_cycles($times_table ='tbn_ad_summary_week',$year='2018',$table_condition='17'){
        if($times_table == 'tbn_ad_summary_week'){
            $year_mod = M('tbn_ad_summary_week')->distinct(true)->getField('fweek',true);
        }else{
            $year_mod = M($times_table)->distinct(true)->getField('fdate',true);
        }
        $years = [];
        foreach ($year_mod as $year_date){
            $years[] = substr($year_date,0,4);
        }
        $years = array_unique($years);//选年
        $select_half_year = [];
        foreach ($years as $v_half_year){
            $select_half_year[] = $v_half_year.'-01-01';//$v_year.'上半年'
            $select_half_year[] = $v_half_year.'-07-01';//$v_year.'下半年';
        }

        $select_year = [];
        foreach ($years as $v_year){
            $select_year[] = $v_year.'-01-01';
        }
        switch ($times_table){
            case 'tbn_ad_summary_week':
                $cycles = $this->create_number(52);
                break;
            case 'tbn_ad_summary_half_month':
                $cycles = $this->create_month(true,false);
                break;
            case 'tbn_ad_summary_month':
                $cycles = $this->create_month(false,false);
                break;
            case 'tbn_ad_summary_quarter':
                $cycles = ['01-01','04-01','07-01','10-01'];
                break;
            case 'tbn_ad_summary_half_year':
                $cycles = $select_half_year;
                break;
            case 'tbn_ad_summary_year':
                $cycles = $select_year;
                break;
        }

        $year_array = explode('-',$table_condition);
        $cycles_count = count($cycles);
        foreach ($cycles as $cy_key => $cycle){
            if($cycle == $table_condition){
                if($times_table == 'tbn_ad_summary_half_year' || $times_table == 'tbn_ad_summary_year'){
                    if($cy_key == 0){
                        $year_array[0] -= 1;
                        $prv = explode('-',$cycles[$cycles_count-1]);
                        return $year_array[0].'-'.$prv[1].'-'.$prv[2];
                    }else{
                        return $cycles[$cy_key-1];
                    }
                }
                if($cy_key == 0){
                    $year -= 1;
                    $condition = $cycles[$cycles_count-1];
                }else{
                    $condition = $cycles[$cy_key-1];
                }
                if($times_table == 'tbn_ad_summary_week'){
                    if($condition < 10){
                        $condition = '0'.$condition;
                    }
                    return $year.$condition;
                }else{
                    return $year.'-'.$condition;
                }

            }
        }
    }

    /**
     * 获取某年第几周的开始日期和结束日期
     * @param int $year
     * @param int $week 第几周;
     */
    public function weekday($year,$week=0){
        $year_start = mktime(0,0,0,1,1,$year);
        $year_end = mktime(0,0,0,12,31,$year);

        // 判断第一天是否为第一周的开始
        if (intval(date('W',$year_start))===1){
            $start = $year_start;//把第一天做为第一周的开始
        }else{
            $week++;
            $start = strtotime('+1 monday',$year_start);//把第一个周一作为开始
        }

        // 第几周的开始时间
        if ($week===1){
            $weekday['start'] = $start;
        }else{
            $weekday['start'] = strtotime('+'.($week-0).' monday',$start);
        }

        // 第几周的结束时间
        $weekday['end'] = strtotime('+1 sunday',$weekday['start']);
        if (date('Y',$weekday['end'])!=$year){
            $weekday['end'] = $year_end;
        }
        return $weekday;
    }

    //获取首末周期时间戳
    public function get_stemp($cycles='2018-01-01',$time_type='tbn_ad_summary_half_month'){
        switch($time_type){
            case 'tbn_ad_summary_half_month':
                $day = substr($cycles,-2);
                $month = substr($cycles,5,2);
                $year = substr($cycles,0,4);
                if($day == '01'){
                    if($month == '02'){
                        $time_string = $year.'-'.$month.'-15';
                    }else{
                        $time_string = $year.'-'.$month.'-16';
                    }
                }else{
                    if($month == '12'){
                        $year = $year+1;
                        $time_string = $year.'-01-01';
                    }else{
                        if($month == '02'){
                            $cycles = $year.'-'.$month.'-15';
                        }
                        $month = ($month+1) < 10 ? '0'.($month+1):($month+1);
                        $time_string = $year.'-'.$month.'-01';
                    }
                }
                break;
            case 'tbn_ad_summary_month':
                $month = substr($cycles,5,2);
                $year = substr($cycles,0,4);

                if($month == '12'){
                    $year = $year+1;
                    $time_string = $year.'-01-01';
                }else{
                    $month = ($month+1) < 10 ? '0'.($month+1):($month+1);
                    $time_string = $year.'-'.$month.'-01';
                }
                break;
            case 'tbn_ad_summary_quarter':
                $quarter = substr($cycles,5,2);
                $year = substr($cycles,0,4);
                if($quarter == '10'){
                    $year = $year+1;
                    $time_string = $year.'-01-01';
                }else{
                    $quarter = ($quarter+3) < 10 ? '0'.($quarter+3):($quarter+3);
                    $time_string = $year.'-'.$quarter.'-01';
                }
                break;
            case 'tbn_ad_summary_half_year':
                $half_year = substr($cycles,5,2);
                $year = substr($cycles,0,4);
                if($half_year == '07'){
                    $year = $year+1;
                    $time_string = $year.'-01-01';
                }else{
                    $time_string = $year.'-07-01';
                }
                break;
            case 'tbn_ad_summary_year':
                $year = substr($cycles,0,4);
                $year = $year+1;
                $cycles = $cycles.'-01-01';
                $time_string = $year.'-01-01';
                break;
        }

        $cycle['start'] = strtotime($cycles);
        $cycle['end'] = strtotime($time_string);
        return $cycle;
    }


    /**
     * 备份 20180728
     * 违法广告统计
     * by yjm
     *http://www.ctcmc.com.cn/web/ggsjzx.asp
     * 1.通过选择时间，确认实例化哪张表
     * 2.通过点击选项卡，连接对应表
     * 3.条件
     *  ① '区域'=>['省','副省级市','市','县区']
     *  ② 媒体（媒体的种类）
     *  ③ 广告类别（广告的种类）
     *  ④ 综合得分、时长、时长违法率、违法条次和违法率
     */
    public function illegal_ad_monitor_20180728(){
        set_time_limit(0);
        session_write_close();//停止使用session
        $user = session('regulatorpersonInfo');//获取用户信息
        $system_num = getconfig('system_num');

        $ad_pm_type = I('ad_pm_type','');//确定是点击监测情况或者线索菜单
        $is_out_excel = I('is_out_excel',0);//确定是否点击导出EXCEL

        $year = I('year',date('Y'));//默认当年
        $page = I('page',1);
        $times_table = I('times_table','');//选表，默认月表

        //默认表
        if($times_table == ''){
            $times_table = 'tbn_ad_summary_month';
        }

        $table_condition = I('table_condition',date('m').'-01');//根据选取的不用表展示不同的条件  //默认当月

        $previous_cycles = $this->get_previous_cycles($times_table,$year,$table_condition);//获取上个周期时间
        $cycle_stamp = 1;
        //根据选择的不同的时间组合不同的条件
        if($table_condition != ''){
            switch ($times_table){
                case 'tbn_ad_summary_week':
                    if($table_condition < 10){
                        $table_condition = '0'.$table_condition;
                    }
                    $cycle_stamp = $this->weekday($year,$table_condition);//周期首末时间戳
                    $table_condition = $year.$table_condition;//周

                    break;
                case 'tbn_ad_summary_half_month':
                case 'tbn_ad_summary_month':
                case 'tbn_ad_summary_quarter':
                    $table_condition = $year.'-'.$table_condition;//半月，月，季度
                    break;
            }
        }else{
            $month = date('m');
            $table_condition = $year.'18';//半月，月，季度   默认当月
        }

        //连表
        //分组统计条件1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
        $fztj = I('fztj','');//默认连接地域表
        if($fztj == ''){
            $fztj = 'fregionid';
        }


        $map = [];//定义查询条件

        //浏览权限设置start
        $re_level = $this->judgment_region($user['regionid']);//当前用户行政等级
        /*如果是国家局*/

        if($system_num == '100000'){
            //判断是否国家局系统
            $old_table = $times_table;
            $times_table = $old_table.'_confirmed';
            $gjj_label_media = M('tmedia_temp')->where('ftype=1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid'))->getField('fmediaid',true);
            $map[$times_table.'.fmediaid'] = array('in',$gjj_label_media);//国家局标签媒体
            $map_xs['fmedia_id'] = array('in',$gjj_label_media);//限定线索查询媒体范围
            $confirm_map = [];

            if(session('regulatorpersonInfo.fregulatorlevel') == 30){//判断是否国家级用户
                $confirm_map['zj_confirm_state'] = 2;
            }else{
                if($re_level == 4 || $re_level == 5){
                    $map[$times_table.'.fid'] = 0;
                }
            }

            $confirm_map['dmp_confirm_state'] = 2;
            $confirm_date = M('zongju_data_confirm')->where($confirm_map)->getField('fmonth',true);
            if(empty($confirm_date)){
                $map[$times_table.'.fid'] = 0;
            }else{
                $is_confirm = false;
                foreach ($confirm_date as $confirm_date_val){
                    $month_start_date = strtotime($confirm_date_val);//指定月份月初时间戳
                    $month_end_date = mktime(23, 59, 59, date('m', strtotime($confirm_date_val))+1, 00);//指定月份月末时间戳
                    switch ($old_table){
                        case 'tbn_ad_summary_year':
                        case 'tbn_ad_summary_half_year':
                        case 'tbn_ad_summary_quarter':
                            $cycle_stamp = $this->get_stemp($table_condition,$old_table);
                            if(($month_start_date >= $cycle_stamp['start']) && ($month_end_date < $cycle_stamp['end'])){
                                $is_confirm = true;
                            }
                            break;
                        case 'tbn_ad_summary_month':
                        case 'tbn_ad_summary_half_month':
                            $cycle_stamp = $this->get_stemp($table_condition,$old_table);
                            $start_ym = date('Ym',$month_start_date);
                            $cycle_sym_start = date('Ym',$cycle_stamp['start']);
                            $cycle_sym_end = date('Ym',$cycle_stamp['end']);
                            if($start_ym == $cycle_sym_start || $start_ym == $cycle_sym_end){
                                $is_confirm = true;
                            }
                            break;
                        case 'tbn_ad_summary_week':
                            $start_ym = date('Ym',$month_start_date);
                            $cycle_sym_start = date('Ym',$cycle_stamp['start']);
                            $cycle_sym_end = date('Ym',$cycle_stamp['end']);
                            if($start_ym == $cycle_sym_start || $start_ym == $cycle_sym_end){
                                $is_confirm = true;
                            }
                            break;
                    }
                }
                if(!$is_confirm) {
                    $map[$times_table.'.fid'] = 0;
                }
            }

        }

        //判断用户点击哪个菜单
        if($ad_pm_type == 2){
            $region = I('region2','');//区域
            if($region == ''){
                if($system_num == '100000' && session('regulatorpersonInfo.fregulatorlevel') != 30){
                    $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                    $map["tregion.flevel"] = ['IN','1,2,3'];
                }else{
                    if($re_level == 2 || $re_level == 3 || $re_level == 4){
                        $region = 5;
                    }else{
                        $region = $re_level;
                    }
                }

            }
            //线索
            switch ($region){
                case 0:
                case 1://全国各省违法线索情况
                    if($re_level == 0){
                        $map["tregion.flevel"] = 1;
                    }elseif($re_level == 1){
                        $map['tregion.fid'] = $user['regionid'];
                    }
                    break;
                case 2:
                    if($re_level == 0){
                        //全国各副省级市违法线索情况
                        $map["tregion.flevel"] = 2;
                    }elseif($re_level == 1){
                        //本省各副省级市违法线索情况
                        $map["tregion.flevel"] = 2;
                        $map["tregion.fpid"] = $user['regionid'];
                    }elseif($re_level == 2){
                        $map["tregion.fpid"] = $user['regionid'];
                    }
                    break;
                case 3://各计划单列市
                    if($re_level == 0){
                        //全国各计划单列市违法线索情况
                        $map["tregion.flevel"] = 3;
                    }elseif($re_level == 1){
                        //本省各计划单列市违法线索情况
                        $map["tregion.flevel"] = 3;
                        $map["tregion.fpid"] = $user['regionid'];
                    }elseif($re_level == 3){
                        //$fpid = substr($user['regionid'],0,2).'0000';
                        $map["tregion.fpid"] = $user['regionid'];
                    }
                    break;
                case 4://各市
                    if($re_level == 0){
                        //如果是全国各市违法线索情况
                        $map["tregion.flevel"] = 4;
                    }elseif($re_level == 1){
                        //本省各市违法线索情况
                        $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                    }elseif($re_level == 4){
                        $map["tregion.fpid"] = $user['regionid'];
                    }
                    break;
                case 5://各区县
                    if($re_level == 0){
                        //全国各区县违法线索情况
                        $map["tregion.flevel"] = 5;
                    }elseif($re_level == 1){
                        //本省各区县违法线索情况
                        $fpid = substr($user['regionid'],0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                    }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                        //本市各区县违法线索情况
                        $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                        //$map["tregion.flevel"] = 5;
                    }elseif($re_level == 5){
                        $fpid = substr($user['regionid'],0,4);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                    }
                    break;
            }
        }else{
            $region = I('region','');//区域
            if($region == ''){
                if($re_level == 4){
                    $region = 4;
                }else{
                    if($re_level == 5){
                        $region = $re_level;
                    }else{
                        $region = 0;//$re_level;
                    }
                }
            }
            //违法情况
            switch ($region){
                case 0:
                case 1://省级和国家级的各省监测情况
                    /*                    if($re_level == 0 || $re_level == 1 || $re_level == 3){
                                            $map["tregion.flevel"] = 1;
                                        }*/
                    $map["tregion.flevel"] = 1;
                    break;
                case 2://副省级市
                    $map["tregion.flevel"] = 2;
                    break;
                case 3://各计划单列市
                    $map["tregion.flevel"] = 3;
                    break;
                case 4://各市
                    if($re_level == 0){
                        //国家级用户的各市监测情况
                        $map["tregion.flevel"] = 4;
                    }else{
                        //市级或者省级用户的各市监测情况
                        if($re_level == 4 || $re_level == 2 || $re_level == 3){
                            $fpid = substr($user['regionid'],0,2).'0000';
                            $map["tregion.fpid"] = $fpid;
                        }elseif($re_level == 1){
                            $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                        }
                    }
                    break;
                case 5://各区县
                    if($re_level == 0){
                        //如果是全国用户
                        $map["tregion.flevel"] = 5;
                    }elseif($re_level == 1){
                        //如果是省级用户
                        $fpid = substr($user['regionid'],0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                    }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                        $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                        //$map['tregion.fpid|'.$times_table.'.fregionid'] =array($user['regionid'],substr($user['regionid'],0,4),'_multi'=>true);
                    }elseif($re_level == 5){
                        $fpid = substr($user['regionid'],0,4);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                    }
                    break;
            }
        }
        //浏览权限设置end


        //媒介类型
        $media_class = I('media_class','');
        if($media_class != ''){
            $map[$times_table.'.fmedia_class_code'] = ['like',$media_class.'%'];//媒体
        }

        //媒体细类
        $media_son_class = I('media_son_class','');//媒体细类
        if($media_son_class != ''){
            $map[$times_table.'.fmedia_class_code'] = ['like',$media_son_class.'%'];//媒体细类
        }

        //广告类型
        /*        $ad_class = I('ad_class','');//广告大类
                if($ad_class != ''){
                    if($ad_class < 10){
                        $ad_class = '0'.$ad_class;
                    }
                    $map[$times_table.'.fad_class_code'] = $ad_class;//广告类别
                }*/


        //$ad_class = json_encode(['01','02','03']);
        $ad_class = I('ad_class');//广告大类I('ad_class',"")
        if(!empty($ad_class)){
            foreach ($ad_class as $ad_class_key=>$ad_class_val){
                if($ad_class_val < 10){
                    $ad_class[$ad_class_key] = '0'.$ad_class_val;
                }
            }
            $map[$times_table.'.fad_class_code'] = ['IN',$ad_class];//广告类别
        }

        $onther_class = I('onther_class','');//排名条件

        if($onther_class == ''){
            if($fztj == 'fad_class_code'){
                //如果是广告类别排名的话,默认条次占比排名
                $onther_class =  'fad_illegal_times';
            }else{
                //如果是其他的话,默认综合评分排名
                $onther_class =  'com_score';
            }
        }

        $data = [];//定义统计数据空数组

        //按照选定时间实例化表
        $table_model = M($times_table);

        $id = 'fid';//定义默认表ID
        $name = 'fname';//定义默认单元名称

        //匹配分组条件，连接不同表
        switch ($fztj){
            case 'fmedia_class_code':
                $join_table = 'tmediaclass';//fmedia_class_code媒介类别表fid,
                $name = 'fclass';
                $xs_group = 'fmedia_class';
                break;
            case 'fmediaownerid':
                $join_table = 'tmediaowner';//fmediaownerid媒介机构表fid
                $xs_group = 'fmediaownerid';
                break;
            case 'fregionid':
                $join_table = 'tregion';//fregionid行政区划表fid
                $xs_group = 'fregion_id';
                break;
            case 'fmediaid':
                $join_table = 'tmedia';//fmediaid媒介表fid
                $name = 'fmedianame';
                $xs_group = 'fmedia_id';
                break;
            case 'fad_class_code':
                $join_table = 'tadclass';//fad_class_code广告内容类别表fcode
                $id = 'fcode';
                $name = 'fadclass';
                $xs_group = 'fad_class_code';
                break;
        }

        //按条件分组数据,因为周表与其他表时间格式不统一 weekday get_stemp
        if($table_condition != ''){
            if($old_table == 'tbn_ad_summary_week'){
                $map[$times_table.'.fweek'] = $table_condition;
                $cycles_field = 'fweek';
                $week_year = substr($table_condition,0,4);
                $week_num = substr($table_condition,-2);
                $map_xs_array = $this->weekday($week_year,$week_num);
            }else{
                $map[$times_table.'.fdate'] = $table_condition;
                $cycles_field = 'fdate';
                $map_xs_array = $this->get_stemp($table_condition,$times_table);
            }
        }

        //查询符合条件的数据
        $group_field = $times_table.'.'.$fztj;//
        $illegal_ad_mod = $table_model
            ->cache(true,600)
            ->field("
                $group_field,
                tmediaclass.fclass as tmediaclass_name,
                tmediaowner.fname as tmediaowner_name,
                tregion.fname1 as tregion_name,
                 (case when instr(tmedia.fmedianame,'（') > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,'（') -1) else tmedia.fmedianame end) as tmedia_name,
                tadclass.fadclass as tadclass_name,
                $times_table.fmediaid as fmediaid,
                $times_table.fmediaownerid as fmediaownerid,
                $times_table.fmedia_class_code as fmedia_class_code,
                $times_table.fregionid as fregionid,
                $times_table.fad_class_code as fad_class_code,
                $times_table.$cycles_field as cycles_time,
                sum($times_table.fad_illegal_times) as fad_illegal_times,
                sum($times_table.fad_times) as fad_times,
                sum($times_table.fad_illegal_count) as fad_illegal_count,
                sum($times_table.fad_count) as fad_count,
                sum($times_table.fad_illegal_play_len) as fad_illegal_play_len,
                sum($times_table.fad_play_len) as fad_play_len 
           ")
            ->join("
                tmediaclass on 
                tmediaclass.fid = substring($times_table.fmedia_class_code,1,2)"
            )
            ->join("
                tmediaowner on 
                tmediaowner.fid = $times_table.fmediaownerid"
            )
            ->join("
                tregion on 
                tregion.fid = $times_table.fregionid"
            )
            ->join("
                tmedia on 
                tmedia.fid = $times_table.fmediaid"
            )
            ->join("
                tadclass on 
                tadclass.fcode = substring($times_table.fad_class_code,1,2)"
            )
            ->where($map)
            ->group($group_field)
            //->page($page.',15')
            ->select();
        $illegal_ad_mod_count = $table_model
            ->cache(true,600)
            ->join("
                tmediaclass on 
                tmediaclass.fid = substring($times_table.fmedia_class_code,1,2)"
            )
            ->join("
                tmediaowner on 
                tmediaowner.fid = $times_table.fmediaownerid"
            )
            ->join("
                tregion on 
                tregion.fid = $times_table.fregionid"
            )
            ->join("
                tmedia on 
                tmedia.fid = $times_table.fmediaid"
            )
            ->join("
                tadclass on 
                tadclass.fcode = substring($times_table.fad_class_code,1,2)"
            )
            ->where($map)
            ->group($group_field)
            ->select();
        $illegal_ad_mod_count = count($illegal_ad_mod_count);
        //统计数量，用来计算
        $all_count = $table_model
            ->cache(true,600)
            ->where($map)
            ->field("
                sum($times_table.fad_illegal_times) as fad_illegal_times,
                sum($times_table.fad_times) as fad_times,
                sum($times_table.fad_illegal_count) as fad_illegal_count,
                sum($times_table.fad_count) as fad_count,
                sum($times_table.fad_illegal_play_len) as fad_illegal_play_len,
                sum($times_table.fad_play_len) as fad_play_len
           ")
            ->join("
                tregion on 
                tregion.fid = $times_table.fregionid"
            )
            ->find();

        //线索查看处理情况tbn_illegal_ad
        $tbn_illegal_ad_mod = M('tbn_illegal_ad');
        $map_xs['unix_timestamp(tbn_illegal_ad.create_time)'] = array('between',array($map_xs_array['start'],$map_xs_array['end']));
        //已处理量
        $ycl_count = $tbn_illegal_ad_mod
            ->cache(true,600)
            ->field("
                count(fstatus) as cll,
                $xs_group
           ")
            ->where(['fstatus'=>['gt',0]])
            ->where($map_xs)
            ->group($xs_group)
            ->select();

        //全部数量
        $cl_count = $tbn_illegal_ad_mod
            ->cache(true,600)
            ->field("
                count(fstatus) as clall,
                $xs_group
            ")
            ->where($map_xs)
            ->group($xs_group)
            ->select();
        $xs_data = [];//定义线索统计情况相关数据

        //如果没有被处理的数据
        if(empty($ycl_count)){
            foreach ($cl_count as $cl_val){
                $xs_data[$cl_val[$xs_group]]['cll'] = '0%';
                $xs_data[$cl_val[$xs_group]][$xs_group] = $cl_val[$xs_group];
                $xs_data[$cl_val[$xs_group]]['cl_count'] = 0;
                $xs_data[$cl_val[$xs_group]]['all_count'] = $cl_val['clall'];
            }
        }else{
            //循环获取
            foreach ($ycl_count as $ycl_val){
                foreach ($cl_count as $cl_val){
                    if($ycl_val[$xs_group] == $cl_val[$xs_group]){
                        if($ycl_val['cll'] != 0){
                            $xs_data[$cl_val[$xs_group]][$xs_group] = $cl_val[$xs_group];
                            $xs_data[$cl_val[$xs_group]]['cll'] = (round($ycl_val['cll']/$cl_val['clall'],4)*100).'%';
                            $xs_data[$cl_val[$xs_group]]['cl_count'] = $ycl_val['cll'];
                        }else{
                            $xs_data[$cl_val[$xs_group]]['cll'] = '0%';
                            $xs_data[$cl_val[$xs_group]][$xs_group] = $cl_val[$xs_group];
                            $xs_data[$cl_val[$xs_group]]['cl_count'] = 0;
                        }
                        $xs_data[$cl_val[$xs_group]]['all_count'] = $cl_val['clall'];
                    }
                }
            }
        }
        //已查看量
        $yck_count = $tbn_illegal_ad_mod
            ->cache(true,600)
            ->field("
                count(fview_status) as ckl,
                $xs_group
            ")
            ->where(['fview_status'=>['gt',0]])
            ->where($map_xs)
            ->group($xs_group)
            ->select();

        //全部数量
        $ck_count = $tbn_illegal_ad_mod
            ->cache(true,600)
            ->field("
                count(fview_status) as ckall,
                $xs_group
            ")
            ->where($map_xs)
            ->group($xs_group)
            ->select();

        //如果没有一查看的数量
        if(empty($yck_count)){
            foreach ($ck_count as $ck_val){
                $xs_data[$ck_val[$xs_group]]['ckl'] = '0%';
                $xs_data[$ck_val[$xs_group]]['ck_count'] = 0;
                $xs_data[$ck_val[$xs_group]]['all_count'] = $ck_val['ckall'];
            }
        }else{
            //循环获取
            foreach ($yck_count as $yck_val){
                foreach ($ck_count as $ck_val){
                    if($yck_val[$xs_group] == $ck_val[$xs_group]){
                        if($yck_val['ckl'] != 0){
                            $xs_data[$ck_val[$xs_group]]['ck_count'] = $yck_val['ckl'];
                        }else{
                            $xs_data[$ck_val[$xs_group]]['ck_count'] = 0;
                        }
                        $xs_data[$ck_val[$xs_group]]['ckl'] = (round($yck_val['ckl']/$ck_val['ckall'],4)*100).'%';
                    }
                    $xs_data[$ck_val[$xs_group]]['all_count'] = $ck_val['ckall'];
                }
            }//组建案件查看与处理情况
        }
        //上个周期时间条件
        if($previous_cycles){
            if($old_table == 'tbn_ad_summary_week'){
                $map[$times_table.'.fweek'] = $previous_cycles;
                $prv_cycles_field = 'fweek';
            }else{
                $map[$times_table.'.fdate'] = $previous_cycles;
                $prv_cycles_field = 'fdate';
            }

            //查询上个周期的数据
            $illegal_prv_ad_mod = $table_model
                ->cache(true,600)
                ->field("
                $times_table.fmediaid as fmediaid,
                $times_table.fmediaownerid as fmediaownerid,
                $times_table.fmedia_class_code as fmedia_class_code,
                $times_table.fregionid as fregionid,
                $times_table.fad_class_code as fad_class_code,
                $times_table.$prv_cycles_field as cycles_time,
                sum($times_table.fad_illegal_times) as fad_illegal_times,
                sum($times_table.fad_times) as fad_times,
                sum($times_table.fad_illegal_count) as fad_illegal_count,
                sum($times_table.fad_count) as fad_count,
                sum($times_table.fad_illegal_play_len) as fad_illegal_play_len,
                sum($times_table.fad_play_len) as fad_play_len
           ")
                ->join("
                tmediaclass on 
                tmediaclass.fid = $times_table.fmedia_class_code"
                )
                ->join("
                tmediaowner on 
                tmediaowner.fid = $times_table.fmediaownerid"
                )
                ->join("
                tregion on 
                tregion.fid = $times_table.fregionid"
                )
                ->join("
                tmedia on 
                tmedia.fid = $times_table.fmediaid"
                )
                ->join("
                tadclass on 
                tadclass.fcode = $times_table.fad_class_code"
                )
                ->where($map)
                ->group($fztj)
                //->page($page.',15')
                ->select();



            //统计数量，用来计算
            $prv_all_count = $table_model
                ->cache(true,600)
                ->where($map)
                ->field("
                sum($times_table.fad_illegal_times) as fad_illegal_times,
                sum($times_table.fad_times) as fad_times,
                sum($times_table.fad_illegal_count) as fad_illegal_count,
                sum($times_table.fad_count) as fad_count,
                sum($times_table.fad_illegal_play_len) as fad_illegal_play_len,
                sum($times_table.fad_play_len) as fad_play_len
           ")
                ->join("
                tregion on 
                tregion.fid = $times_table.fregionid"
                )
                ->find();

            //$this->ajaxReturn($illegal_prv_ad_mod);
        }


        /*
        地域排名综合得分计算公式为：综合得分=(条次违法率+条数违法率)/2*50+时长违法率*50
        电视、广播媒体排名综合得分计算公式为：综合得分=(条次违法率+条数违法率)/2*50+时长违法率*50
        报纸媒体综合得分计算公式为：综合得分=条次违法率*60+（违法总条次/全部地域违法总条次）*40
        */
        //按公式计算上个周期的综合评分总量
        $prv_com_score = 0;
        foreach ($illegal_prv_ad_mod as $prv_ad_key => $illegal_prv_ad) {
            //如果是报纸
            $fmedia_class_code = substr($illegal_prv_ad['fmedia_class_code'], 0, 2);
            $prv_data[$prv_ad_key]['times_illegal_rate'] = $illegal_prv_ad['fad_illegal_times'] / $illegal_prv_ad['fad_times'];//条次违法率
            $prv_data[$prv_ad_key]['counts_illegal_rate'] = $illegal_prv_ad['fad_illegal_count'] / $illegal_prv_ad['fad_count'];//条数违法率
            if ($fmedia_class_code == '03') {
                $com_score = ($prv_data[$prv_ad_key]['times_illegal_rate'] * 60) + ($illegal_prv_ad['fad_illegal_times'] / $all_count['fad_illegal_times']) * 40;
            } else {
                $prv_data[$prv_ad_key]['lens_illegal_rate'] = $illegal_prv_ad['fad_illegal_play_len'] / $illegal_prv_ad['fad_play_len'];//时长违法率
                $com_score = ($prv_data[$prv_ad_key]['times_illegal_rate'] + $prv_data[$prv_ad_key]['counts_illegal_rate']) / 2 * 50 + ($prv_data[$prv_ad_key]['lens_illegal_rate'] * 50);
            }
            $prv_data[$prv_ad_key]['com_score'] = $com_score;
            $prv_com_score += $com_score;
            $prv_data[$prv_ad_key] = array_merge($prv_data[$prv_ad_key], $illegal_prv_ad);
        }
        $prv_all_count['prv_com_score'] = $prv_com_score/count($illegal_prv_ad_mod);
        //按公式计算综合评分
        $now_com_score = 0;
        foreach ($illegal_ad_mod as $ad_key => $illegal_ad) {
            //如果是报纸
            $fmedia_class_code = substr($illegal_ad['fmedia_class_code'], 0, 2);
            $data[$ad_key]['times_illegal_rate'] = $illegal_ad['fad_illegal_times'] / $illegal_ad['fad_times'];//条次违法率
            $data[$ad_key]['counts_illegal_rate'] = $illegal_ad['fad_illegal_count'] / $illegal_ad['fad_count'];//条数违法率
            if ($fmedia_class_code == '03') {
                $com_score = ($data[$ad_key]['times_illegal_rate'] * 60) + ($illegal_ad['fad_illegal_times'] / $all_count['fad_illegal_times']) * 40;
            } else {
                $data[$ad_key]['lens_illegal_rate'] = $illegal_ad['fad_illegal_play_len'] / $illegal_ad['fad_play_len'];//时长违法率
                $com_score = ($data[$ad_key]['times_illegal_rate'] + $data[$ad_key]['counts_illegal_rate']) / 2 * 50 + ($data[$ad_key]['lens_illegal_rate'] * 50);
            }
            $data[$ad_key]['com_score'] = $com_score;
            $now_com_score += $com_score;
            $data[$ad_key] = array_merge($data[$ad_key], $illegal_ad);
        }
        $all_count['now_com_score'] = $now_com_score/count($illegal_ad_mod);

        //对比上个周期的数据
        foreach ($illegal_prv_ad_mod as $illegal_prv_ad_val) {
            foreach ($data as $data_key=>$data_value) {

                if ($illegal_prv_ad_val[$fztj] == $data_value[$fztj]) {

                    if (!$illegal_prv_ad_val['fad_illegal_count'] || ($data_value['fad_illegal_count'] - $illegal_prv_ad_val['fad_illegal_count']) == 0) {
                        $data[$data_key]['prv_fad_illegal_count'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_count = round(($data_value['fad_illegal_count'] - $illegal_prv_ad_val['fad_illegal_count']) / $illegal_prv_ad_val['fad_illegal_count'], 4) * 100;
                        if ($prv_fad_illegal_count < 0) {
                            $data[$data_key]['prv_fad_illegal_count'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_count . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_count'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_count . '%)';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_times'] || ($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) == 0) {
                        $data[$data_key]['prv_fad_illegal_times'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_times = round(($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) / $illegal_prv_ad_val['fad_illegal_times'], 4) * 100;
                        if ($prv_fad_illegal_times < 0) {
                            $data[$data_key]['prv_fad_illegal_times'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_times . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_times'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_times . '%)';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_play_len'] || ($data_value['fad_illegal_play_len'] - $illegal_prv_ad_val['fad_illegal_play_len']) == 0) {
                        $data[$data_key]['prv_fad_illegal_play_len'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_play_len = round(($data_value['fad_illegal_play_len'] - $illegal_prv_ad_val['fad_illegal_play_len']) / $illegal_prv_ad_val['fad_illegal_play_len'], 4) * 100;

                        if ($prv_fad_illegal_play_len < 0) {
                            $data[$data_key]['prv_fad_illegal_play_len'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_play_len . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_play_len'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_play_len . '%)';
                        }
                    }
                }
            }
        }

        //对比

        $data = $this->pxsf($data,$onther_class);//排序

        $array_num = 0;//定义限定数量
        $data_pm = [];//定义图表数据空数组
        $data_count = count($data);//获取数据条数

        $cl_count_all = 0;//线索全部数量
        $ck_count_all = 0;//线索全部数量
        $count_all = 0;//总数
        $times=0;

        //处理排序后的数据
        foreach ($data as $data_key=>$data_value){

            $data[$data_key]['times_illegal_rate'] = round($data_value['times_illegal_rate'],4)*100;
            $data[$data_key]['counts_illegal_rate'] = round($data_value['counts_illegal_rate'],4)*100;
            $data[$data_key]['lens_illegal_rate'] = round($data_value['lens_illegal_rate'],4)*100;
            $data[$data_key]['com_score'] = round($data_value['com_score'],2);
            $data[$data_key]['wf_zb'] = round($data_value['fad_illegal_count']/$all_count['fad_illegal_count'],5)*100;
            //循环获取前30条数据
            if($array_num < 30 && $array_num < count($data) && $data_value[$onther_class] > 0){
                $data_pm['name'][] = $data_value[$join_table.'_name'];
                //不同排名条件显示各自的数值
                switch ($onther_class){
                    case 'com_score':
                        $data_pm['num'][] = $data[$data_key]['com_score'];//综合
                        break;
                    case 'fad_illegal_play_len':
                        $data_pm['num'][] = $data_value['fad_illegal_play_len'];//违法时长
                        break;
                    case 'lens_illegal_rate':
                        $data_pm['num'][] = $data[$data_key]['lens_illegal_rate'];//时长违法率
                        break;
                    case 'fad_illegal_times':
                        $data_pm['num'][] = $data_value['fad_illegal_times'];//违法条次
                        break;
                    case 'times_illegal_rate':
                        $data_pm['num'][] = $data[$data_key]['times_illegal_rate'];//条次违法率
                        break;
                    case 'fad_illegal_count':
                        $data_pm['num'][] = $data[$data_key]['fad_illegal_count'];//条数
                        break;
                    case 'counts_illegal_rate':
                        $data_pm['num'][] = $data[$data_key]['counts_illegal_rate'];//条数违法率
                        break;
                    case 'fad_count':
                        $data_pm['num'][] = $data[$data_key]['fad_count'];//条数违法率
                        break;
                    case 'fad_times':
                        $data_pm['num'][] = $data[$data_key]['fad_times'];//条数违法率
                        break;
                    case 'fad_play_len':
                        $data_pm['num'][] = $data[$data_key]['fad_play_len'];//条数违法率
                        break;
                }
                $array_num++;//fad_count  广告条次：fad_times  广告时长：fad_play_len
            }

        }

        //组合线索相关数据

        foreach ($xs_data as $xs_key=>$xs_value){
            foreach ($data as $data_key=>$data_value){
                if($xs_key == $data_value[$fztj]){
                    unset($xs_value[$xs_group]);
                    $cl_count_all += $xs_value['cl_count'];
                    $ck_count_all += $xs_value['ck_count'];
                    $count_all += $xs_value['all_count'];
                    $data[$data_key] = array_merge($xs_value, $data[$data_key]);//合并数组
                }
            }
        }


        //组合广告类别百分比图表数据
        $ad_class_num = 0;
        if($fztj == 'fad_class_code') {
            $data_pm = [];
            $data = $this->pxsf($data, $onther_class);//排序
            foreach ($data as $ad_class_key=>$ad_class_value){
                if($ad_class_value[$onther_class] != 0){
                    $data_pm['data'][] = [
                        'name' => $ad_class_value[$join_table.'_name'],
                        'value' => $ad_class_value[$onther_class]
                    ];
                    $data_pm['name'][] = $ad_class_value[$join_table.'_name'];
                }
            }
        }

        $all_prv_fad_illegal_count = round(($all_count['fad_illegal_count'] - $prv_all_count['fad_illegal_count']) / $prv_all_count['fad_illegal_count'], 4) * 100;
        $all_prv_fad_illegal_play_len = round(($all_count['fad_illegal_play_len'] - $prv_all_count['fad_illegal_play_len']) / $prv_all_count['fad_illegal_play_len'], 4) * 100;
        $all_prv_fad_illegal_times = round(($all_count['fad_illegal_times'] - $prv_all_count['fad_illegal_times']) / $prv_all_count['fad_illegal_times'], 4) * 100;

        $counts_illegal_rate = round($all_count['fad_illegal_count']/$all_count['fad_count'],4)*100;
        $times_illegal_rate = round($all_count['fad_illegal_times']/$all_count['fad_times'],4)*100;
        $lens_illegal_rate = round($all_count['fad_illegal_play_len']/$all_count['fad_play_len'],4)*100;

        $prv_counts_illegal_rate = round($prv_all_count['fad_illegal_count']/$prv_all_count['fad_count'],4)*100;
        $prv_times_illegal_rate = round($prv_all_count['fad_illegal_times']/$prv_all_count['fad_times'],4)*100;
        $prv_lens_illegal_rate = round($prv_all_count['fad_illegal_play_len']/$prv_all_count['fad_play_len'],4)*100;

        if ($all_prv_fad_illegal_count < 0) {
            $all_prv_fad_illegal_count = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_count . '%)';
        } else {
            if($all_prv_fad_illegal_count == 0){
                $all_prv_fad_illegal_count = '(0.00%)';

            }else{
                $all_prv_fad_illegal_count = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_count . '%)';
            }
        }

        if ($all_prv_fad_illegal_play_len < 0) {
            $all_prv_fad_illegal_play_len = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_play_len . '%)';
        } else {
            if($all_prv_fad_illegal_play_len == 0){
                $all_prv_fad_illegal_play_len = '(0.00%)';

            }else{
                $all_prv_fad_illegal_play_len = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_play_len . '%)';
            }
        }

        if ($all_prv_fad_illegal_times < 0) {
            $all_prv_fad_illegal_times = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_times . '%)';
        } else {
            if($all_prv_fad_illegal_times == 0){
                $all_prv_fad_illegal_times = '(0.00%)';

            }else{
                $all_prv_fad_illegal_times = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_times . '%)';
            }
        }

        $compar_fad_count = $all_count['fad_count'] - $prv_all_count['fad_count'];
        if ($compar_fad_count < 0) {
            $compar_fad_count = '减少'.($compar_fad_count*-1);
        } else {
            if($compar_fad_count == 0){
                $compar_fad_count = '无变化';

            }else{
                $compar_fad_count = '增加'.$compar_fad_count;
            }
        }
        $compar_fad_illegal_count = $all_count['fad_illegal_count'] - $prv_all_count['fad_illegal_count'];
        if ($compar_fad_illegal_count < 0) {
            $compar_fad_illegal_count = '减少'.($compar_fad_illegal_count*-1);
        } else {
            if($compar_fad_illegal_count == 0){
                $compar_fad_illegal_count = '无变化';

            }else{
                $compar_fad_illegal_count = '增加'.$compar_fad_illegal_count;
            }
        }
        $compar_illegal_count_rate = $counts_illegal_rate - $prv_counts_illegal_rate;
        if ($compar_illegal_count_rate < 0) {
            $compar_illegal_count_rate = '下降'.($compar_illegal_count_rate*-1).'%';
        } else {
            if($compar_illegal_count_rate == 0){
                $compar_illegal_count_rate = '无变化';

            }else{
                $compar_illegal_count_rate = '上升'.$compar_illegal_count_rate.'%';
            }
        }
        $compar_fad_times = $all_count['fad_times'] - $prv_all_count['fad_times'];
        if ($compar_fad_times < 0) {
            $compar_fad_times = '减少'.($compar_fad_times*-1);
        } else {
            if($compar_fad_times == 0){
                $compar_fad_times = '无变化';

            }else{
                $compar_fad_times = '增加'.$compar_fad_times;
            }
        }
        $compar_fad_illegal_times = $all_count['fad_illegal_times'] - $prv_all_count['fad_illegal_times'];
        if ($compar_fad_illegal_times < 0) {
            $compar_fad_illegal_times = '减少'.($compar_fad_illegal_times*-1);
        } else {
            if($compar_fad_illegal_times == 0){
                $compar_fad_illegal_times = '无变化';

            }else{
                $compar_fad_illegal_times = '增加'.$compar_fad_illegal_times;
            }
        }
        $compar_illegal_times_rate = $times_illegal_rate - $prv_times_illegal_rate;
        if ($compar_illegal_times_rate < 0) {
            $compar_illegal_times_rate = '下降'.($compar_illegal_times_rate*-1).'%';
        } else {
            if($compar_illegal_times_rate == 0){
                $compar_illegal_times_rate = '无变化';
            }else{
                $compar_illegal_times_rate = '上升'.$compar_illegal_times_rate.'%';
            }
        }
        $compar_fad_play_len = $all_count['fad_play_len'] - $prv_all_count['fad_play_len'];
        if ($compar_fad_play_len < 0) {
            $compar_fad_play_len = '减少'.($compar_fad_play_len*-1);
        } else {
            if($compar_fad_play_len == 0){
                $compar_fad_play_len = '无变化';

            }else{
                $compar_fad_play_len = '增加'.$compar_fad_play_len;
            }
        }
        $compar_fad_illegal_play_len = $all_count['fad_illegal_play_len'] - $prv_all_count['fad_illegal_play_len'];
        if ($compar_fad_illegal_play_len < 0) {
            $compar_fad_illegal_play_len = '减少'.($compar_fad_illegal_play_len*-1);
        } else {
            if($compar_fad_illegal_play_len == 0){
                $compar_fad_illegal_play_len = '无变化';

            }else{
                $compar_fad_illegal_play_len = '增加'.$compar_fad_illegal_play_len;
            }
        }
        $compar_illegal_play_len_rate = $lens_illegal_rate - $prv_lens_illegal_rate;
        if ($compar_illegal_play_len_rate < 0) {
            $compar_illegal_play_len_rate = '下降'.($compar_illegal_play_len_rate*-1).'%';
        } else {
            if($compar_illegal_play_len_rate == 0){
                $compar_illegal_play_len_rate = '无变化';
            }else{
                $compar_illegal_play_len_rate = '上升'.$compar_illegal_play_len_rate.'%';
            }
        }
        $compar_prv_com_score = round($all_count['com_score'] - $prv_all_count['prv_com_score'],2);
        if ($compar_prv_com_score < 0) {
            $compar_prv_com_score = '减少'.($compar_prv_com_score*-1);
        } else {
            if($compar_prv_com_score == 0){
                $compar_prv_com_score = '无变化';

            }else{
                $compar_prv_com_score = '增加'.$compar_prv_com_score;
            }
        }

        //统计放在最后一条$all_count     $prv_all_count
        $data[] = [
            'all_xs_count' =>$count_all,
            'titlename'=>'全部',
            "cll"=>(round($cl_count_all/$count_all,4)*100).'%',
            "cl_count" =>$cl_count_all,
            "ck_count"=>$ck_count_all,
            "ckl"=>(round($ck_count_all/$count_all,4)*100).'%',
            "prv_fad_illegal_count"=>$all_prv_fad_illegal_count,
            "prv_fad_illegal_play_len"=>$all_prv_fad_illegal_play_len,
            "prv_fad_illegal_times"=>$all_prv_fad_illegal_times,

            'fad_count'=>$all_count['fad_count'],//广告条数
            'fad_illegal_count'=>$all_count['fad_illegal_count'],//违法广告数量
            'counts_illegal_rate'=>round($all_count['fad_illegal_count']/$all_count['fad_count'],4)*100,
            'fad_times'=>$all_count['fad_times'],//广告条次
            'fad_illegal_times'=>$all_count['fad_illegal_times'],//违法广告条次
            'times_illegal_rate'=>round($all_count['fad_illegal_times']/$all_count['fad_times'],4)*100,
            'fad_play_len'=>$all_count['fad_play_len'],//总时长
            'fad_illegal_play_len'=>$all_count['fad_illegal_play_len'],//违法时长
            'lens_illegal_rate'=>round($all_count['fad_illegal_play_len']/$all_count['fad_play_len'],4)*100,
            "com_score"=>round($all_count['com_score'],2),//综合分

            "compar_fad_count"=>$compar_fad_count,//对比上个周期广告条数
            "compar_fad_illegal_count"=>$compar_fad_illegal_count,//对比上个周期违法广告数量
            "compar_illegal_count_rate"=>$compar_illegal_count_rate,//对比上个周期条数违法率
            "compar_fad_times"=>$compar_fad_times,//对比上个周期广告条次
            "compar_fad_illegal_times"=>$compar_fad_illegal_times,//对比上个周期违法广告条次
            "compar_illegal_times_rate"=>$compar_illegal_times_rate,//对比上个周期条次违法率
            "compar_fad_play_len"=>$compar_fad_play_len,//对比上个周期总时长
            "compar_fad_illegal_play_len"=>$compar_fad_illegal_play_len,//对比上个周期违法时长
            "compar_illegal_play_len_rate"=>$compar_illegal_play_len_rate,//对比上个周期时长违法率
            "compar_prv_com_score"=>$compar_prv_com_score,//对比上个周期综合分
        ];




        $finally_data = [
            'count'=> $illegal_ad_mod_count,
            'data_pm'=>$data_pm,//图表数据
            'data_list'=>$data//列表数据
        ];

        if($is_out_excel == 1){
            //如果是线索菜单并且是点击了导出按钮
            $objPHPExcel = new \PHPExcel();
            $objPHPExcel
                ->getProperties()  //获得文件属性对象，给下文提供设置资源
                ->setCreator( "MaartenBalliauw")             //设置文件的创建者
                ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
                ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
                ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
                ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
                ->setKeywords( "office 2007 openxmlphp")        //设置标记
                ->setCategory( "Test resultfile");                //设置类别

            $sheet = $objPHPExcel->setActiveSheetIndex(0);
            //分组导出数据1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
            switch ($fztj){
                case 'fmediaid':
                    $sheet ->mergeCells("D1:F1");
                    $sheet ->mergeCells("G1:I1");
                    $sheet ->mergeCells("J1:L1");
                    $sheet ->mergeCells("M1:P1");
                    $sheet ->setCellValue('A1','');
                    $sheet ->setCellValue('B1','');
                    $sheet ->setCellValue('C1','');
                    $sheet ->setCellValue('D1','');
                    $sheet ->setCellValue('E1','广告条数');
                    $sheet ->setCellValue('H1','广告条次');
                    $sheet ->setCellValue('K1','广告时长');
                    $sheet ->setCellValue('N1','线索查看及处理情况');

                    $sheet ->setCellValue('A2','排名');
                    $sheet ->setCellValue('B2','媒体所在地');
                    $sheet ->setCellValue('C2','媒体名称');
                    $sheet ->setCellValue('D2','总条数');
                    $sheet ->setCellValue('E2','违法条数');
                    $sheet ->setCellValue('F2','条数违法率');
                    $sheet ->setCellValue('G2','总条次');
                    $sheet ->setCellValue('H2','违法条次');
                    $sheet ->setCellValue('I2','条次违法率');
                    $sheet ->setCellValue('J2','总时长');
                    $sheet ->setCellValue('K2','违法时长');
                    $sheet ->setCellValue('L2','时长违法率');
                    $sheet ->setCellValue('M2','查看量');
                    $sheet ->setCellValue('N2','查看率');
                    $sheet ->setCellValue('O2','处理量');
                    $sheet ->setCellValue('P2','处理率');

                    foreach ($data as $key => $value) {

                        $sheet ->setCellValue('A'.($key+3),($key+1));
                        if($data_count == $key){
                            $sheet ->setCellValue('B'.($key+3),'全部地域');
                            $sheet ->setCellValue('C'.($key+3),'全部媒体');
                        }else{
                            $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                            $sheet ->setCellValue('C'.($key+3),$value['tmedia_name']);
                        }

                        $sheet ->setCellValue('D'.($key+3),$value['fad_count']);
                        $sheet ->setCellValue('E'.($key+3),$value['fad_illegal_count']);
                        $sheet ->setCellValue('F'.($key+3),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('G'.($key+3),$value['fad_times']);
                        $sheet ->setCellValue('H'.($key+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('I'.($key+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('G'.($key+3),$value['fad_play_len']);
                        $sheet ->setCellValue('K'.($key+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('L'.($key+3),$value['lens_illegal_rate'].'%');
                        $sheet ->setCellValue('M'.($key+3),$value['ck_count']);
                        $sheet ->setCellValue('N'.($key+3),$value['ckl']);
                        $sheet ->setCellValue('O'.($key+3),$value['cl_count']);
                        $sheet ->setCellValue('P'.($key+3),$value['cll']);

                    }
                    break;
                case 'fregionid':
                    $sheet ->mergeCells("C1:E1");
                    $sheet ->mergeCells("F1:H1");
                    $sheet ->mergeCells("I1:K1");
                    $sheet ->mergeCells("L1:O1");
                    $sheet ->setCellValue('A1','');
                    $sheet ->setCellValue('B1','');
                    $sheet ->setCellValue('C1','广告条数');
                    $sheet ->setCellValue('F1','广告条次');
                    $sheet ->setCellValue('I1','广告时长');
                    $sheet ->setCellValue('L1','线索查看及处理情况');

                    $sheet ->setCellValue('A2','排名');
                    $sheet ->setCellValue('B2','地域名称');
                    $sheet ->setCellValue('C2','总条数');
                    $sheet ->setCellValue('D2','违法条数');
                    $sheet ->setCellValue('E2','条数违法率');
                    $sheet ->setCellValue('F2','总条次');
                    $sheet ->setCellValue('G2','违法条次');
                    $sheet ->setCellValue('H2','条次违法率');
                    $sheet ->setCellValue('I2','总时长');
                    $sheet ->setCellValue('J2','违法时长');
                    $sheet ->setCellValue('K2','时长违法率');
                    $sheet ->setCellValue('L2','查看量');
                    $sheet ->setCellValue('M2','查看率');
                    $sheet ->setCellValue('N2','处理量');
                    $sheet ->setCellValue('O2','处理率');

                    foreach ($data as $key => $value) {


                        $sheet ->setCellValue('A'.($key+3),($key+1));
                        if($data_count == $key){
                            $sheet ->setCellValue('B'.($key+3),'全部地域');
                        }else{
                            $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                        }

                        $sheet ->setCellValue('C'.($key+3),$value['fad_count']);
                        $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_count']);
                        $sheet ->setCellValue('E'.($key+3),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('F'.($key+3),$value['fad_times']);
                        $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('H'.($key+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('I'.($key+3),$value['fad_play_len']);
                        $sheet ->setCellValue('J'.($key+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('K'.($key+3),$value['lens_illegal_rate'].'%');
                        $sheet ->setCellValue('L'.($key+3),$value['ck_count']);
                        $sheet ->setCellValue('M'.($key+3),$value['ckl']);
                        $sheet ->setCellValue('N'.($key+3),$value['cl_count']);
                        $sheet ->setCellValue('O'.($key+3),$value['cll']);

                    }
                    break;
                case 'fmediaownerid':
                    $sheet ->mergeCells("E1:G1");
                    $sheet ->mergeCells("H1:J1");
                    $sheet ->mergeCells("K1:M1");
                    $sheet ->mergeCells("N1:Q1");
                    $sheet ->setCellValue('A1','');
                    $sheet ->setCellValue('B1','');
                    $sheet ->setCellValue('C1','');
                    $sheet ->setCellValue('D1','');
                    $sheet ->setCellValue('E1','广告条数');
                    $sheet ->setCellValue('H1','广告条次');
                    $sheet ->setCellValue('K1','广告时长');
                    $sheet ->setCellValue('N1','线索查看及处理情况');

                    $sheet ->setCellValue('A2','排名');
                    $sheet ->setCellValue('B2','媒介机构所在地');
                    $sheet ->setCellValue('C2','媒介机构名称');
                    $sheet ->setCellValue('D2','媒介类型');
                    $sheet ->setCellValue('E2','总条数');
                    $sheet ->setCellValue('F2','违法条数');
                    $sheet ->setCellValue('G2','条数违法率');
                    $sheet ->setCellValue('H2','总条次');
                    $sheet ->setCellValue('I2','违法条次');
                    $sheet ->setCellValue('J2','条次违法率');
                    $sheet ->setCellValue('K2','总时长');
                    $sheet ->setCellValue('L2','违法时长');
                    $sheet ->setCellValue('M2','时长违法率');
                    $sheet ->setCellValue('N2','查看量');
                    $sheet ->setCellValue('O2','查看率');
                    $sheet ->setCellValue('P2','处理量');
                    $sheet ->setCellValue('Q2','处理率');

                    foreach ($data as $key => $value) {

                        $sheet ->setCellValue('A'.($key+3),($key+1));
                        if($data_count == $key){
                            $sheet ->setCellValue('B'.($key+3),'全部地域');
                            $sheet ->setCellValue('C'.($key+3),'全部媒介机构');
                            $sheet ->setCellValue('D'.($key+3),'全部类型');
                        }else{
                            $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                            $sheet ->setCellValue('C'.($key+3),$value['tmediaowner_name']);
                            $sheet ->setCellValue('D'.($key+3),$value['tmediaclass_name']);
                        }

                        $sheet ->setCellValue('E'.($key+3),$value['fad_count']);
                        $sheet ->setCellValue('F'.($key+3),$value['fad_illegal_count']);
                        $sheet ->setCellValue('G'.($key+3),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('H'.($key+3),$value['fad_times']);
                        $sheet ->setCellValue('I'.($key+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('J'.($key+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('K'.($key+3),$value['fad_play_len']);
                        $sheet ->setCellValue('L'.($key+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('M'.($key+3),$value['lens_illegal_rate'].'%');
                        $sheet ->setCellValue('N'.($key+3),$value['ck_count']);
                        $sheet ->setCellValue('O'.($key+3),$value['ckl']);
                        $sheet ->setCellValue('P'.($key+3),$value['cl_count']);
                        $sheet ->setCellValue('Q'.($key+3),$value['cll']);

                    }
                    break;
                case 'fad_class_code':
                    $sheet ->mergeCells("C1:E1");
                    $sheet ->mergeCells("F1:H1");
                    $sheet ->mergeCells("I1:K1");
                    $sheet ->mergeCells("L1:O1");
                    $sheet ->setCellValue('A1','');
                    $sheet ->setCellValue('B1','');
                    $sheet ->setCellValue('C1','广告条数');
                    $sheet ->setCellValue('F1','广告条次');
                    $sheet ->setCellValue('I1','广告时长');
                    $sheet ->setCellValue('L1','线索查看及处理情况');

                    $sheet ->setCellValue('A2','排名');
                    $sheet ->setCellValue('B2','广告类别');
                    $sheet ->setCellValue('C2','总条数');
                    $sheet ->setCellValue('D2','违法条数');
                    $sheet ->setCellValue('E2','条数违法率');
                    $sheet ->setCellValue('F2','总条次');
                    $sheet ->setCellValue('G2','违法条次');
                    $sheet ->setCellValue('H2','条次违法率');
                    $sheet ->setCellValue('I2','总时长');
                    $sheet ->setCellValue('J2','违法时长');
                    $sheet ->setCellValue('K2','时长违法率');
                    $sheet ->setCellValue('L2','查看量');
                    $sheet ->setCellValue('M2','查看率');
                    $sheet ->setCellValue('N2','处理量');
                    $sheet ->setCellValue('O2','处理率');

                    foreach ($data as $key => $value) {


                        $sheet ->setCellValue('A'.($key+3),($key+1));
                        if($data_count == $key){
                            $sheet ->setCellValue('B'.($key+3),'全部类别');
                        }else{
                            $sheet ->setCellValue('B'.($key+3),$value['tadclass_name']);
                        }

                        $sheet ->setCellValue('C'.($key+3),$value['fad_count']);
                        $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_count']);
                        $sheet ->setCellValue('E'.($key+3),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('F'.($key+3),$value['fad_times']);
                        $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('H'.($key+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('I'.($key+3),$value['fad_play_len']);
                        $sheet ->setCellValue('J'.($key+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('K'.($key+3),$value['lens_illegal_rate'].'%');
                        $sheet ->setCellValue('L'.($key+3),$value['ck_count']);
                        $sheet ->setCellValue('M'.($key+3),$value['ckl']);
                        $sheet ->setCellValue('N'.($key+3),$value['cl_count']);
                        $sheet ->setCellValue('O'.($key+3),$value['cll']);

                    }
                    break;
            }
            $objActSheet =$objPHPExcel->getActiveSheet();
            $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
            $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            $date = date('Ymdhis');
            $savefile = './Public/Xls/'.$date.'.xlsx';
            $savefile2 = '/Public/Xls/'.$date.'.xlsx';
            $objWriter->save($savefile);
            //即时导出下载
            /*          header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        header('Content-Disposition:attachment;filename="'.$date.'.xlsx"');
                        header('Cache-Control:max-age=0');
                        $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
                        $objWriter->save( 'php://output');*/
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$savefile2));

        }else{
            $this->ajaxReturn($finally_data);
        }

    }
    // 备份20180728 跨地域统计排名
    public function zones_ad_20180728(){
        $user = session('regulatorpersonInfo');//fregulatorid机构ID
        //选表
        $year = I('year',date('Y'));//默认当年
        $times_table = I('times_table','');//选表，默认月表
        $ad_pm_type = I('ad_pm_type','');//确定是点击哪个
        $is_out_excel = I('is_out_excel',0);//确定是否点击导出EXCEL
        if($times_table == ''){
            $times_table = 'tbn_zones_ad_month';
        }
        switch ($times_table){
            case 'tbn_ad_summary_week':
                $times_table = 'tbn_zones_ad_week';//周
                break;
            case 'tbn_ad_summary_half_month':
                $times_table = 'tbn_zones_ad_half_month';//半月
                break;
            case 'tbn_ad_summary_month':
                $times_table = 'tbn_zones_ad_month';//月
                break;
            case 'tbn_ad_summary_quarter':
                $times_table = 'tbn_zones_ad_quarter';//季度
                break;
            case 'tbn_ad_summary_half_year':
                $times_table = 'tbn_zones_ad_half_year';//半年
                break;
            case 'tbn_ad_summary_year':
                $times_table = 'tbn_zones_ad_year';//年
                break;
        }
        $table_condition = I('table_condition','05-01');//根据选取的不用表展示不同的条件  //默认当月
        $previous_cycles = $this->get_previous_cycles($times_table,$year,$table_condition);
        //根据选择的不同的时间组合不同的条件
        if($table_condition != ''){
            switch ($times_table){
                case 'tbn_zones_ad_week':
                    $table_condition = $year.$table_condition;//周
                    break;
                case 'tbn_zones_ad_half_month':
                case 'tbn_zones_ad_month':
                case 'tbn_zones_ad_quarter':
                    $table_condition = $year.'-'.$table_condition;//半月，月，季度
                    break;
            }
        }else{
            $month = date('m');
            $table_condition = $year.'18';//半月，月，季度   默认当月
        }

        //ad_class[ fad_class ] 广告类别 media_pclass[ fmedia_id ] 媒体 region_level[ fregion_id ]级别   cross_region_rank [  ] 广告/地域/媒体

        //区域级别
        $re_level = $this->judgment_region($user['regionid']);

        if($ad_pm_type == 2){
            $region = I('region_level2','');//区域
            if($region == ''){
                if($re_level == 2 || $re_level == 3 || $re_level == 4){
                    $region = 5;
                }else{
                    $region = $re_level;
                }
            }
            //线索
            switch ($region){
                case 0:
                case 1://全国各省违法线索情况
                    if($re_level == 0){
                        $map["tregion.flevel"] = 1;
                    }elseif($re_level == 1){
                        $map["tregion.fpid"] = $user['regionid'];
                    }
                    break;
                case 2:
                    if($re_level == 0){
                        //全国各副省级市违法线索情况
                        $map["tregion.flevel"] = 2;
                    }elseif($re_level == 1){
                        //本省各副省级市违法线索情况
                        $map["tregion.flevel"] = 2;
                        $map["tregion.fpid"] = $user['regionid'];
                    }elseif($re_level == 2){
                        $map["tregion.fpid"] = $user['regionid'];
                    }
                    break;
                case 3://各计划单列市
                    if($re_level == 0){
                        //全国各计划单列市违法线索情况
                        $map["tregion.flevel"] = 3;
                    }elseif($re_level == 1){
                        //本省各计划单列市违法线索情况
                        $map["tregion.flevel"] = 3;
                        $map["tregion.fpid"] = $user['regionid'];
                    }elseif($re_level == 3){
                        $map["tregion.fpid"] = $user['regionid'];
                    }
                    break;
                case 4://各市
                    if($re_level == 0){
                        //如果是全国各市违法线索情况
                        $map["tregion.flevel"] = 4;
                    }else{
                        //本省各市违法线索情况
                        if($re_level == 4 || $re_level == 2 || $re_level == 3){
                            $fpid = substr($user['regionid'],0,2).'0000';
                            $map["tregion.fpid"] = $fpid;
                        }elseif($re_level == 1){
                            $fpid = $user['regionid'];
                            $map["tregion.fpid"] = $fpid;
                        }
                    }
                    break;
                case 5://各区县
                    if($re_level == 0){
                        //全国各区县违法线索情况
                        $map["tregion.flevel"] = 5;
                    }elseif($re_level == 1){
                        //本省各区县违法线索情况
                        $fpid = substr($user['regionid'],0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                    }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                        //本市各区县违法线索情况
                        $map["tregion.fpid"] = $user['regionid'];
                        $map["tregion.flevel"] = 5;
                    }
                    break;
            }
        }else{
            $region = I('region_level','');//区域
            if($region == ''){
                /*                if($re_level == 2 || $re_level == 3 || $re_level == 4){
                                    $region = 4;
                                }else{*/
                $region = 0;//$re_level;
                //}
            }
            //违法情况
            switch ($region){
                case 0:
                case 1://省级和国家级的各省监测情况
                    //if($re_level == 0 || $re_level == 1){
                    $map["tregion.flevel"] = 1;
                    //}
                    break;
                case 2://副省级市
                    $map["tregion.flevel"] = 2;
                    break;
                case 3://各计划单列市
                    $map["tregion.flevel"] = 3;
                    break;
                case 4://各市
                    if($re_level == 0){
                        //国家级用户的各市监测情况
                        $map["tregion.flevel"] = 4;
                    }else{
                        //市级或者省级用户的各市监测情况
                        if($re_level == 4 || $re_level == 2 || $re_level == 3){
                            $fpid = substr($user['regionid'],0,2).'0000';
                            $map["tregion.fpid"] = $fpid;

                        }elseif($re_level == 1){
                            $fpid = $user['regionid'];
                            $map["tregion.fpid"] = $fpid;
                        }
                    }
                    break;
                case 5://各区县
                    if($re_level == 0){
                        //如果是全国用户
                        $map["tregion.flevel"] = 5;
                    }elseif($re_level == 1){
                        //如果是省级用户
                        $fpid = substr($user['regionid'],0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                    }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                        $map["tregion.fpid"] = $user['regionid'];
                        $map["tregion.flevel"] = 5;
                    }
                    break;
            }
        }
        //浏览权限设置end

        //媒介类型
        $media_class = I('media_class','');
        if($media_class != ''){
            $map[$times_table.'.fmediaclass_id'] = $media_class;//媒体
        }

        //广告类型
        $ad_class = I('ad_class','');//广告大类
        if($ad_class != ''){
            if($ad_class < 10){
                $ad_class = '0'.$ad_class;
            }
            $map[$times_table.'.fad_class'] = ['like',$ad_class.'%'];//广告类别
        }

        $pm_class = I('cross_region_rank','');//其他查询条件
        if($pm_class == ''){
            $pm_class =  'fad_name';//fmedia_id    fad_name   fregion_id
        }

        //按照选定时间实例化表
        $table_model = M($times_table);
        //按条件分组数据
        if($table_condition != ''){
            $map[$times_table.'.fdate'] = $table_condition;
            $cycles_field = 'fdate';
        }
        //查询数据
        $group_field = $times_table.'.'.$fztj;
        header("Content-type: text/html; charset=utf-8");

        $zones_ad_mod = $table_model
            ->cache(true,600)
            ->field("
                tmediaclass.fclass as tmediaclass_name,
                tregion.fname1 as tregion_name,
                 (case when instr(tmedia.fmedianame,'（') > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,'（') -1) else tmedia.fmedianame end) as tmedia_name,
                tadclass.fadclass as tadclass_name,
                $times_table.fregion_id as fregion_id,
                $times_table.fmediaclass_id as fmediaclass_id,
                $times_table.fmedia_id as fmedia_id,
                $times_table.fad_class as fad_class,
                $times_table.issue_count as issue_count,
                $times_table.fad_name as fad_name
            ")
            ->join("
                tregion on 
                tregion.fid = $times_table.fregion_id"
            )
            ->join("
                tmediaclass on 
                tmediaclass.fid = $times_table.fmediaclass_id"
            )
            ->join("
                tadclass on 
                tadclass.fcode = substring($times_table.fad_class,1,2)"
            )
            ->join("
                tmedia on 
                tmedia.fid = $times_table.fmedia_id"
            )
            ->where($map)
            ->group('fad_name,fregion_id')
            ->having('count(*) < 2')
            ->select();
        $zones_data = $table_model
            ->cache(true,600)
            ->field("
                $pm_class,
                $times_table.fad_name as fad_name,
                sum(issue_count) as zones_count
                ")
            ->join("
                tregion on 
                tregion.fid = $times_table.fregion_id"
            )
            ->where($map)
            ->group($pm_class)
            ->select();

        $res = array();
        $is_one = [];

        if($pm_class == 'fmedia_id' || $pm_class == 'fregion_id'){

            foreach ($zones_ad_mod as $res_key=>$value) {
                if(isset($res[$value["fad_name"]])){
                    $cf[] = $value; //重复数据
                    $res[$value["fad_name"]]['res_num']++;
                }else{
                    $is_one[$value[$pm_class]] = $res_key;
                    $res[$value["fad_name"]] = $value;
                    $res[$value["fad_name"]]['res_num']++;
                }
            }

            foreach ($zones_ad_mod as $zones_data_key=>$zones_val){
                foreach ($res as $res_val){
                    if($res_val['res_num'] ==1 && $zones_val[$pm_class] == $res_val[$pm_class]){
                        unset($zones_ad_mod[$zones_data_key]);
                    }
                }
            }
        }
        $data_list = [];
        $count = 0;
        foreach ($zones_ad_mod as $zones_ad_mod_val){
            foreach ($zones_data as $zones_data_val){
                if($zones_ad_mod_val[$pm_class] == $zones_data_val[$pm_class]){
                    if($pm_class == 'fmedia_id' || $pm_class == 'fregion_id'){
                        if(!strstr($data_list[$zones_data_val[$pm_class]]['fad_name'],$zones_ad_mod_val['fad_name'].', ')){
                            $data_list[$zones_data_val[$pm_class]]['fad_name'] .= $zones_ad_mod_val['fad_name'].', ';
                        }else{
                            continue;
                        }
                        $data_list[$zones_data_val[$pm_class]]['tregion_name'] = $zones_ad_mod_val['tregion_name'];
                    }elseif($pm_class == 'fad_name'){
                        if(!strstr($data_list[$zones_data_val[$pm_class]]['tregion_name'],$zones_ad_mod_val['tregion_name'].', ')){
                            $data_list[$zones_data_val[$pm_class]]['tregion_name'] .= $zones_ad_mod_val['tregion_name'].', ';
                        }else{
                            continue;
                        }
                        $data_list[$zones_data_val[$pm_class]]['fad_name'] = $zones_ad_mod_val['fad_name'];
                    }
                    $data_list[$zones_data_val[$pm_class]]['tmedia_name'] = $zones_ad_mod_val['tmedia_name'];

                    $data_list[$zones_data_val[$pm_class]]['tadclass_name'] = $zones_ad_mod_val['tadclass_name'];
                    $data_list[$zones_data_val[$pm_class]][$pm_class] = $zones_ad_mod_val[$pm_class];
                    if($pm_class != 'fad_name'){
                        $data_list[$zones_data_val[$pm_class]]['zones_count'] = $zones_data_val['zones_count'];
                    }else{
                        $data_list[$zones_data_val[$pm_class]]['zones_count']++;
                    }
                }
            }
        }

        $data_list = array_values($data_list);

        $data_list = $this->pxsf($data_list,'zones_count');
        $array_num = 0;
        $data_pm = [];

        foreach ($data_list as $list_key=>$data_list_val){
            if($data_list_val['zones_count'] < 2 && $pm_class == 'fad_name'){
                unset($data_list[$list_key]);
                continue;
            }
            if($pm_class == 'fregion_id' || $pm_class == 'fmedia_id'){
                $data_list[$list_key]['fad_name'] = substr($data_list_val['fad_name'],0,-2);
            }else{
                $data_list[$list_key]['tregion_name'] = substr($data_list_val['tregion_name'],0,-2);;
            }
            //循环获取前30条数据
            if($array_num < 30 && $array_num < count($data_list)){
                if($pm_class == 'fregion_id'){
                    $data_pm['name'][] = $data_list_val['tregion_name'];
                }elseif($pm_class == 'fmedia_id'){
                    $data_pm['name'][] = $data_list_val['tmedia_name'];
                }else{
                    $data_pm['name'][] = $data_list_val[$pm_class];
                }
                $data_pm['num'][] = $data_list_val['zones_count'];
                $array_num++;
            }
        }
        $finally_data = [
            'data_pm'=>$data_pm,
            'data_list'=>$data_list
        ];
        if($is_out_excel == 1){
            $objPHPExcel = new \PHPExcel();
            $objPHPExcel
                ->getProperties()  //获得文件属性对象，给下文提供设置资源
                ->setCreator( "MaartenBalliauw")             //设置文件的创建者
                ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
                ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
                ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
                ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
                ->setKeywords( "office 2007 openxmlphp")        //设置标记
                ->setCategory( "Test resultfile");                //设置类别

            $sheet = $objPHPExcel->setActiveSheetIndex(0);
            //分组导出数据1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
            $pm_class =  'fmedia_id';//fmedia_id    fad_name   fregion_id
            if($pm_class == 'fregion_id'){
                $sheet ->setCellValue('A1','排名');
                $sheet ->setCellValue('B1','地域名称');
                $sheet ->setCellValue('C1','违法广告数量');
                $sheet ->setCellValue('D1','违法广告名称');
                foreach ($data_list as $key => $value) {
                    $sheet ->setCellValue('A'.($key+2),($key+1));
                    $sheet ->setCellValue('B'.($key+2),$value['tregion_name']);
                    $sheet ->setCellValue('C'.($key+2),$value['zones_count']);
                    $sheet ->setCellValue('D'.($key+2),$value['fad_name']);
                }
            }elseif($pm_class == 'fmedia_id'){
                $sheet ->setCellValue('A1','排名');
                $sheet ->setCellValue('B1','媒体名称');
                $sheet ->setCellValue('C1','违法广告数量');
                $sheet ->setCellValue('D1','违法广告名称');
                foreach ($data_list as $key => $value) {
                    $sheet ->setCellValue('A'.($key+2),($key+1));
                    $sheet ->setCellValue('B'.($key+2),$value['media_name']);
                    $sheet ->setCellValue('C'.($key+2),$value['zones_count']);
                    $sheet ->setCellValue('D'.($key+2),$value['fad_name']);
                }
            }else{
                $sheet ->setCellValue('A1','排名');
                $sheet ->setCellValue('B1','广告名称');
                $sheet ->setCellValue('C1','广告类别');
                $sheet ->setCellValue('D1','跨地域数量');
                $sheet ->setCellValue('E1','播出地域');
                foreach ($data_list as $key => $value) {
                    $sheet ->setCellValue('A'.($key+2),($key+1));
                    $sheet ->setCellValue('B'.($key+2),$value['fad_name']);
                    $sheet ->setCellValue('C'.($key+2),$value['tadclass_name']);
                    $sheet ->setCellValue('D'.($key+2),$value['zones_count']);
                    $sheet ->setCellValue('E'.($key+2),$value['tregion_name']);
                }
            }


            $objActSheet =$objPHPExcel->getActiveSheet();
            $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
            $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            $date = date('Ymdhis');
            $savefile = './Public/Xls/'.$date.'.xlsx';
            $savefile2 = '/Public/Xls/'.$date.'.xlsx';
            $objWriter->save($savefile);
            //即时导出下载
            /*          header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        header('Content-Disposition:attachment;filename="'.$date.'.xlsx"');
                        header('Cache-Control:max-age=0');
                        $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
                        $objWriter->save( 'php://output');*/
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$savefile2));

        }else{
            $this->ajaxReturn($finally_data);
        }
    }

    /**
     * @return array
     */
    public function get_gjj_label_media(){
        $gjj_label_media_map = [];
        $gjj_label_media_map['tmedialabel.flabelid'] = ['IN',[243,244,245,248]];
        $gjj_label_media_map['tregion.flevel'] = ['IN',[1,2,3]];
        $gjj_label_media_map['tmedialabel.fstate'] = ['IN',[1,0]];
        $gjj_label_media = M('tmedialabel')
            ->join("tmedia on tmedia.fid = tmedialabel.fmediaid and tmedia.fid = tmedia.main_media_id and tmedia.fstate =1")
            ->join("tregion on tregion.fid = tmedia.media_region_id")
            ->where($gjj_label_media_map)
            ->group('fmediaid')
            ->getField('fmediaid',true);
        $gjj_label_media = array_unique($gjj_label_media);


        return $gjj_label_media;
    }

    /**
     * 违法条数列表
     * by zw
     */
    public function tiaoshu_list(){
        session_write_close();
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isrelease = $ALL_CONFIG['isrelease'];
        $ischeck = $ALL_CONFIG['ischeck'];
        $is_manage_media = $ALL_CONFIG['set_manage_region'];//是否通过指定区域管理媒体统计
        $isexamine = $ALL_CONFIG['isexamine'];

        $user = session('regulatorpersonInfo');//获取用户信息
        $fad_class_code   = I('fad_class_code');//广告类别，用于汇总中违法广告数据的查看
        $fmediaid         = I('fmediaid');//媒体ID，用于汇总中违法广告数据的查看
        $fmediaownerid    = I('fmediaownerid');//媒体机构ID，用于汇总中违法广告数据的查看
        $mclass           = I('mclass');//媒体类别，用于汇总中违法广告数据的查看
        $fad_class_code2  = I('fad_class_code2');//广告类别组，用于汇总中违法广告数据的查看
        $flevel           = I('flevel');//地域级别，用于汇总中违法广告数据的查看
        $is_show_longad   = I('is_show_longad');//是否有长广告，用于汇总中违法广告数据的查看
        $is_show_fsend    = I('is_show_fsend');//是否包含确认广告，1包含未发布，其他值已发布
        $is_have_self    = I('is_have_self',0);//是否包含本级
        $is_include_sub    = I('is_include_sub',0);//是否包含下级
        $ad_pm_type = I('ad_pm_type',1);
        $outtype = I('outtype');//导出类型
        if(!empty($outtype)){
            $p  = I('page', 1);//当前第几页ks
            $pp = 50000;//每页显示多少记录
        }else{
            $p  = I('page', 1);//当前第几页ks
            $pp = 10;//每页显示多少记录
        }
        $area = I('area');

        //时间条件筛选
        $years      = I('year');//选择年份
        $times_table = I('times_table');
        $table_condition = I('table_condition');//选择时间
        if($times_table == 'tbn_ad_summary_day'){
            $timetypes = 0;
            $timeval = $table_condition;
        }elseif($times_table == 'tbn_ad_summary_week'){
            $timetypes = 10;
            $timeval = $table_condition;
        }elseif($times_table == 'tbn_ad_summary_half_month'){
            $timetypes = 20;
            $table_condition2 = explode('-', $table_condition);
            $table_condition3 = ((int)$table_condition2[0]-1)*2;
            $table_condition4 = ((int)$table_condition2[1])==1?1:2;
            $timeval = $table_condition3+$table_condition4;
        }elseif($times_table == 'tbn_ad_summary_month'){
            $timetypes = 30;
            $table_condition2 = explode('-', $table_condition);
            $timeval = (int)$table_condition2[0];
        }elseif($times_table == 'tbn_ad_summary_quarter'){
            $timetypes = 40;
            if($table_condition == '01-01'){
                $timeval = 1;
            }elseif($table_condition == '04-01'){
                $timeval = 2;
            }elseif($table_condition == '07-01'){
                $timeval = 3;
            }else{
                $timeval = 4;
            }
        }elseif($times_table == 'tbn_ad_summary_half_year'){
            $timetypes = 50;
            $table_condition2 = explode('-', $table_condition);
            if($table_condition2[2] == '01'){
                $timeval = 1;
            }else{
                $timeval = 2;
            }
        }elseif($times_table == 'tbn_ad_summary_year'){
            $timetypes = 60;
            $timeval = 1;
        }
        if($is_show_fsend==1){
            $is_show_fsend = -2;
        }else{
            $is_show_fsend = 0;
        }
        $iszjdata = I('iszjdata')?I('iszjdata'):0;//是否总局数据
        if(!empty($iszjdata)){
            $system_num = 100000;
            $is_show_fsend = 2;
        }else{
            $system_num = $system_num;
        }
        $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date',$is_show_fsend,$isrelease);

        $search_type  = I('search_type');//搜索类别
        $search_val   = I('search_val');//搜索值
        if(!empty($search_val)){
            if($search_type == 10){//媒体分类
                $where_tia['a.fmedia_class'] = $search_val;
            }elseif($search_type == 20){//广告类别
                $where_tia['b.ffullname'] = array('like','%'.$search_val.'%');
            }elseif($search_type == 30){//广告名称
                $where_tia['a.fad_name'] = array('like','%'.$search_val.'%');
            }elseif($search_type == 40){//发布媒体
                $where_tia['c.fmedianame'] = array('like','%'.$search_val.'%');
            }elseif($search_type == 50){//违法原因
                $where_tia['a.fillegal'] = array('like','%'.$search_val.'%');
            }
        }


        if(!empty($mclass)){

            if($system_num == "120000" && $mclass == "13"){
                $where_tia['a.fmedia_class'] = 99999999999;
            }else{
                if(!in_array(-1,$mclass)){
                    $media_class_int = [];
                    foreach ($mclass as $media_class_v){
                        $media_class_int[] = intval($media_class_v);
                    }
                    $where_tia['a.fmedia_class'] = ['IN',$media_class_int];
                }
            }

        }else{
            if($system_num == "120000"){
                $where_tia['a.fmedia_class'] = ['IN',[1,2,3]];
            }
        }

/*        if(!empty($mclass)){
            $where_tia['a.fmedia_class'] = $mclass;
        }*/

        //国家局系统只显示有打国家局标签媒体的数据
        if($system_num == '100000'){
            $wherestr = ' and tn.flevel in (1,2,3)';
        }

        if(!empty($flevel)){
            $wherestr = ' and tn.flevel in('.$flevel.')';
        }

        if(!empty($fad_class_code2)){
            $fadclass = [];
            foreach ($fad_class_code2 as $key => $value) {
                if($value<10){
                    $fadclass[] = '0'.$value;
                }else{
                    $fadclass[] = $value;
                }
            }
            $where_tia['left(a.fad_class_code,2)'] = array('in',$fadclass);
        }

        if((!empty($fmediaownerid) || !empty($fmediaid) || !empty($fad_class_code)) && empty($area)){
            if($system_num == '100000' && session('regulatorpersonInfo.fregulatorlevel')==30){
                $area = '100000';
            }else{
                $area = session('regulatorpersonInfo.regionid');
            }
        }


        if($is_manage_media == 1){
            $region_field = 'c.manage_region_id';
            $map_region_field = 'tn.fid';//a.fregion_id
        }else{
            $region_field = 'fregion_id';
            $map_region_field = 'a.fregion_id';//
        }


        if(!empty($area)){//所属地区
            if($area != '100000'){
                if(($flevel==1 || $flevel == 2 || $flevel==3 || $flevel==4 || $flevel==5) && is_numeric($flevel)){

                    $where_tia[$map_region_field]  = $area;
                }else{
                    $where_tia["left($map_region_field,2)"] = substr($area, 0,2);
                }
            }
        }

        if(!empty($fmediaid)){//媒体排名查询
            $where_tia['a.fmedia_id'] = $fmediaid;
            $wherestr = ' and tn.flevel in (1,2,3,4,5)';
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==0 && $flevel == 5){
                $where_tia[$map_region_field]  = $area;
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==10&&$flevel == 5){
                $where_tia[$map_region_field]  = array('like',substr($area, 0,4).'%');
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==20&&$flevel == 4){
                $where_tia[$map_region_field]  = array('like',substr($area, 0,2).'%');
            }
        }

        if(!empty($fmediaownerid)){//机构排名查询
            $where_tia['a.fmediaownerid'] = $fmediaownerid;
            $wherestr = ' and tn.flevel in (1,2,3,4,5)';
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==0 && $flevel == 5){
                $where_tia[$map_region_field]  = $area;
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==10&&$flevel == 5){
                $where_tia[$map_region_field]  = array('like',substr($area, 0,4).'%');
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==20&&$flevel == 4){
                $where_tia[$map_region_field]  = array('like',substr($area, 0,2).'%');
            }
        }

        if(!empty($fad_class_code)){//广告类别排名查询
            $where_tia['left(a.fad_class_code,2)'] = $fad_class_code;
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==0 && $flevel == 5){
                $where_tia[$map_region_field]  = $area;
            }

            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==10&&$flevel == 5){
                if(!empty($is_have_self)){
                    $wherestr = ' and tn.flevel in (2,3,4,5)';
                }else{
                    $wherestr = ' and tn.flevel = 5';
                }
                $where_tia[$map_region_field]  = array('like',substr($area, 0,4).'%');
            }

            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==20&&$flevel == 4){
                if(!empty($is_have_self)){
                    $wherestr = ' and tn.flevel in (1,2,3,4)';
                }else{
                    $wherestr = ' and tn.flevel in (2,3,4)';
                }
                $where_tia[$map_region_field]  = array('like',substr($area, 0,2).'%');
            }

        }

        $where_tia['a.fcustomer']  = $system_num;
        //是否抽查模式
        if(!empty($ischeck)){
            $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
            //如果抽查表有数据
            if(!empty($spot_check_data)){
                $dates = [];//定义日期数组
                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                    $cc_condition = trim($spot_check_data_val['condition']);
                    if(!empty($cc_condition)){
                        $year_month = '';
                        $date_str = [];
                        $year_month = substr($spot_check_data_val['fmonth'],0,7);
                        if(!empty($spot_check_data_val['condition'])){
                            $date_str = explode(',',$spot_check_data_val['condition']);
                        }
                        foreach ($date_str as $date_str_val){
                            $dates[] = $year_month.'-'.$date_str_val;
                        }
                    }

                }

                $where_time  .= ' and a.fissue_date in ("'.implode('","', $dates).'")';
            }else{
                $where_time  .= ' and 1=0';
            }
        }

        if($is_show_longad === '0'){
            $where_time .= ' and (TIMESTAMPDIFF(SECOND,a.fstarttime,a.fendtime)<600 or a.fmedia_class>2)';
        }

        //审核通过的违法数据
        if(in_array($isexamine, [10,20,30,40,50])){
            $where_tia['a.fexamine'] = 10;
        }


        if($ad_pm_type == 2){
            $region = I('region2','');//区域
            if($is_include_sub == 1 && $region != 5 && $region != 0){
                switch ($region){
                    case 1:
                        $where_tia[$map_region_field] = ['like',substr($area,0,2).'%'];
                        $wherestr = ' and tn.flevel in (1,2,3,4,5)';
                        break;
                    case 2:
                    case 3:
                    case 4:
                        $where_tia[$map_region_field] = ['like',substr($area,0,4).'%'];
                        $wherestr = ' and tn.flevel in (2,3,4,5)';
                        break;
                }
            }
        }

        $where_tia['a.fstatus3'] = ['NOT IN',[30,40]];
        $count = M('tbn_illegal_ad')
            ->alias('a')
            ->join('tadclass b on a.fad_class_code=b.fcode')
            ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
            ->join('tregion tn on '.$region_field.' = tn.fid'.$wherestr)
            ->join('(select count(*) as fcount,max(fillegal_ad_id) as fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue a,tbn_illegal_ad b where '.$where_time.' and a.fillegal_ad_id=b.fid and b.fcustomer = '.$system_num.' group by fsample_id,b.fmedia_class) as x on a.fid=x.fillegal_ad_id')
            ->join('tmedia_temp ttp on c.fid=ttp.fmediaid and ttp.ftype in(1,2) and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
            ->where($where_tia)
            ->count();//查询满足条件的总记录数

        $do_tia = M('tbn_illegal_ad')
            ->alias('a')
            ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name,(case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus2,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fstatus,a.fregion_id')
            ->join('tadclass b on a.fad_class_code=b.fcode')
            ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
            ->join('tregion tn on '.$region_field.' =tn.fid'.$wherestr)
            ->join('(select count(*) as fcount,max(fillegal_ad_id) as fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue a,tbn_illegal_ad b where '.$where_time.' and a.fillegal_ad_id=b.fid and b.fcustomer = '.$system_num.' group by fsample_id,b.fmedia_class) as x on a.fid=x.fillegal_ad_id')
            ->join('tmedia_temp ttp on c.fid=ttp.fmediaid and ttp.ftype in(1,2) and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
            ->where($where_tia)
            ->order('x.fstarttime desc')
            ->page($p,$pp)
            ->select();
        if(!empty($outtype)){
            if(empty($do_tia)){
                $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
            }

            $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-数据汇总违法条数列表';//文档内部标题名称
            $outdata['datalie'] = [
                '序号'=>'key',
                '媒体分类'=>[
                    'type'=>'zwif',
                    'data'=>[
                        ['{fmedia_class} == 1','电视'],
                        ['{fmedia_class} == 2','广播'],
                        ['{fmedia_class} == 3','报纸'],
                        ['{fmedia_class} == 13','互联网'],
                    ]
                ],
                '广告类别'=>'fadclass',
                '广告名称'=>'fad_name',
                '发布媒体'=>'fmedianame',
                '播出总条次'=>'fcount',
                '播出周数'=>'diffweek',
                '播出时间段'=>[
                    'type'=>'zwhebing',
                    'data'=>'{fstarttime}~{fendtime}'
                ],
                '违法表现代码'=>'fillegal_code',
                '违法表现'=>'fexpressions',
                '涉嫌违法原因'=>'fillegal',
            ];
            $outdata['lists'] = $do_tia;
            $ret = A('Api/Function')->outdata_xls($outdata);

            D('Function')->write_log('数据汇总违法条数',1,'导出成功');
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

        }else{
            $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
        }
    }

    /**
     *违法条次列表
     */

    public function tiaoci_list()
    {
        $user = session('regulatorpersonInfo');//fregulatorid机构ID
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isrelease = $ALL_CONFIG['isrelease'];
        $ischeck = $ALL_CONFIG['ischeck'];
        $is_manage_media = $ALL_CONFIG['set_manage_region'];//是否通过指定区域管理媒体统计
        $isexamine = $ALL_CONFIG['isexamine'];

        $fad_class_code   = I('fad_class_code');//广告类别，用于汇总中违法广告数据的查看
        $fmediaid         = I('fmediaid');//媒体ID，用于汇总中违法广告数据的查看
        $fmediaownerid    = I('fmediaownerid');//媒体机构ID，用于汇总中违法广告数据的查看
        $mclass           = I('mclass');//媒体类别，用于汇总中违法广告数据的查看
        $fad_class_code2  = I('fad_class_code2');//广告类别组，用于汇总中违法广告数据的查看
        $area           = I('area');//地域ID，用于汇总中违法广告数据的查看
        $flevel         = I('flevel');//地域级别，用于汇总中违法广告数据的查看
        $is_show_longad   = I('is_show_longad');//是否有长广告，用于汇总中违法广告数据的查看
        $is_show_fsend    = I('is_show_fsend');//是否包含确认广告，1包含未发布，其他值已发布
        $is_have_self    = I('is_have_self',0);//是否包含本级
        $ad_pm_type    = I('ad_pm_type',1);//
        $is_include_sub    = I('is_include_sub',0);//是否包含下级
        $outtype = I('outtype');//导出类型

        if($is_manage_media == 1){
            $region_field = 'tm.manage_region_id';
            $map_region_field = 'tn.fid';//a.fregion_id
        }else{
            $region_field = 'tia.fregion_id';
            $map_region_field = 'tia.fregion_id';//
        }
        if(!empty($outtype)){
            $p  = I('page', 1);//当前第几页ks
            $pp = 50000;//每页显示多少记录
        }else{
            $p  = I('page', 1);//当前第几页ks
            $pp = 10;//每页显示多少记录
        }

        //时间条件筛选
        $years      = I('year');//选择年份
        $times_table = I('times_table');
        $table_condition = I('table_condition');//选择时间
        if($times_table == 'tbn_ad_summary_day'){
            $timetypes = 0;
            $timeval = $table_condition;
        }elseif($times_table == 'tbn_ad_summary_week'){
            $timetypes = 10;
            $timeval = $table_condition;
        }elseif($times_table == 'tbn_ad_summary_half_month'){
            $timetypes = 20;
            $table_condition2 = explode('-', $table_condition);
            $table_condition3 = ((int)$table_condition2[0]-1)*2;
            $table_condition4 = ((int)$table_condition2[1])==1?1:2;
            $timeval = $table_condition3+$table_condition4;
        }elseif($times_table == 'tbn_ad_summary_month'){
            $timetypes = 30;
            $table_condition2 = explode('-', $table_condition);
            $timeval = (int)$table_condition2[0];
        }elseif($times_table == 'tbn_ad_summary_quarter'){
            $timetypes = 40;
            if($table_condition == '01-01'){
                $timeval = 1;
            }elseif($table_condition == '04-01'){
                $timeval = 2;
            }elseif($table_condition == '07-01'){
                $timeval = 3;
            }else{
                $timeval = 4;
            }
        }elseif($times_table == 'tbn_ad_summary_half_year'){
            $timetypes = 50;
            $table_condition2 = explode('-', $table_condition);
            if($table_condition2[2] == '01'){
                $timeval = 1;
            }else{
                $timeval = 2;
            }
        }elseif($times_table == 'tbn_ad_summary_year'){
            $timetypes = 60;
            $timeval = 1;
        }
        if($is_show_fsend==1){
            $is_show_fsend = -2;
        }else{
            $is_show_fsend = 0;
        }
        //如果要求用总局数据，而平台不属于总局平台时，发布状态要等于2，即已确认发布
        $iszjdata = I('iszjdata')?I('iszjdata'):0;//是否总局数据
        if(!empty($iszjdata)){
            $system_num = 100000;
            $is_show_fsend = 2;
        }
        $where_time = gettimecondition($years,$timetypes,$timeval,'tiai.fissue_date',$is_show_fsend,$isrelease);


        if($system_num == '100000'){
            if(!empty($area)){
                if(($flevel==1 || $flevel == 2 || $flevel==3 || $flevel==4 || $flevel==5) && is_numeric($flevel)){
                    $where[$map_region_field]  = $area;
                }else{
                    $where["left($map_region_field,2)"] = substr($area, 0,2);
                }
            }else{
                if(session('regulatorpersonInfo.fregulatorlevel')!=30){
                    $regionid = session('regulatorpersonInfo.regionid');//机构行政区划ID
                    $regionid = substr($regionid , 0 , 2);
                    $where[$map_region_field] = array('like',$regionid.'%');
                }
            }
        }else{
            if(!empty($area)){
                $where[$map_region_field] = $area;
            }
        }


        if(!empty($mclass)){

            if($system_num == "120000" && $mclass == "13"){
                $where['tia.fmedia_class'] = 99999999999;
            }else{
                if(!in_array(-1,$mclass)){
                    $media_class_int = [];
                    foreach ($mclass as $media_class_v){
                        $media_class_int[] = intval($media_class_v);
                    }
                    $where['tia.fmedia_class'] = ['IN',$media_class_int];
                }
            }

        }else{
            if($system_num == "120000"){
                $where['tia.fmedia_class'] = ['IN',[1,2,3]];
            }
        }

/*        if(!empty($mclass)){
            $where['tia.fmedia_class'] = $mclass;
        }*/

        //国家局系统只显示有打国家局标签媒体的数据
        if($system_num == '100000'){
            $wherestr = ' and tn.flevel in (1,2,3)';
        }

        if(!empty($flevel)){
            $wherestr = ' and tn.flevel in('.$flevel.')';
        }

        if(!empty($fad_class_code)){//广告类别排名查询
            $where['left(tia.fad_class_code,2)'] = $fad_class_code;

            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==0 && $flevel == 5){
                $where_tia[$map_region_field]  = $area;
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==10&&$flevel == 5){
                if(!empty($is_have_self)){
                    $wherestr = ' and tn.flevel in (2,3,4,5)';
                }else{
                    $wherestr = ' and tn.flevel = 5';
                }
                $where_tia[$map_region_field]  = array('like',substr($area, 0,4).'%');
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==20&&$flevel == 4){
                if(!empty($is_have_self)){
                    $wherestr = ' and tn.flevel in (1,2,3,4)';
                }else{
                    $wherestr = ' and tn.flevel in (2,3,4)';
                }
                $where_tia[$map_region_field]  = array('like',substr($area, 0,2).'%');
            }

        }

        if(!empty($fmediaid)){
            $where['tia.fmedia_id'] = $fmediaid;
            $wherestr = ' and tn.flevel in (1,2,3,4,5)';
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==0 && $flevel == 5){
                $where_tia[$map_region_field]  = $area;
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==10&&$flevel == 5){
                $where_tia[$map_region_field]  = array('like',substr($area, 0,4).'%');
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==20&&$flevel == 4){
                $where_tia[$map_region_field]  = array('like',substr($area, 0,2).'%');
            }
        }

        if(!empty($fmediaownerid)){
            $where['tia.fmediaownerid'] = $fmediaownerid;
            $wherestr = ' and tn.flevel in (1,2,3,4,5)';
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==0 && $flevel == 5){
                $where_tia[$map_region_field]  = $area;
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==10&&$flevel == 5){
                $where_tia[$map_region_field]  = array('like',substr($area, 0,4).'%');
            }
            if($system_num != '100000' &&session('regulatorpersonInfo.fregulatorlevel')==20&&$flevel == 4){
                $where_tia[$map_region_field]  = array('like',substr($area, 0,2).'%');
            }
        }

        if(!empty($fad_class_code2)){
            $fadclass = [];
            foreach ($fad_class_code2 as $key => $value) {
                if($value<10){
                    $fadclass[] = '0'.$value;
                }else{
                    $fadclass[] = $value;
                }
            }
            $where['left(tia.fad_class_code,2)'] = array('in',$fadclass);
        }

        $ad_name    = I('name');//广告名
        $start_date = I('start_date');//开始日期
        $end_date   = I('end_date');//结束日期

        if($ad_name){
            $where['tia.fad_name'] = array('like','%'.$ad_name.'%');
        }
        if($start_date && $end_date){
            $where['tiai.fissue_date'] = array('between',array($start_date,$end_date));
        }

        //是否抽查模式
        if(!empty($ischeck)){
            $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
            //如果抽查表有数据
            if(!empty($spot_check_data)){
                $dates = [];//定义日期数组
                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                    $year_month = '';
                    $date_str = [];
                    $year_month = substr($spot_check_data_val['fmonth'],0,7);
                    if(!empty($spot_check_data_val['condition'])){
                        $date_str = explode(',',$spot_check_data_val['condition']);
                    }
                    foreach ($date_str as $date_str_val){
                        $dates[] = $year_month.'-'.$date_str_val;
                    }
                }
                $where_time     .= ' and tiai.fissue_date in ("'.implode('","', $dates).'")';
            }else{
                $where_time     .= ' and 1=0';
            }
        }

        if($is_show_longad === '0'){
            $where_time .= ' and (TIMESTAMPDIFF(SECOND,tiai.fstarttime,tiai.fendtime)<600 or tiai.fmedia_class>2)';
        }

        $where['_string'] .= $where_time;
        //审核通过的违法数据
        if(in_array($isexamine, [10,20,30,40,50])){
            $where['tia.fexamine'] = 10;
        }

        if($ad_pm_type == 2){
            $region = I('region2','');//区域
            if($is_include_sub == 1 && $region != 5 && $region != 0){
                switch ($region){
                    case 1:
                        $where[$map_region_field] = ['like',substr($area,0,2).'%'];
                        $wherestr = ' and tn.flevel in (1,2,3,4,5)';
                        break;
                    case 2:
                    case 3:
                    case 4:
                        $where[$map_region_field] = ['like',substr($area,0,4).'%'];
                        $wherestr = ' and tn.flevel in (2,3,4,5)';
                        break;
                }
            }
        }


        $where['tia.fstatus3'] = ['NOT IN',[30,40]];
        $count = M('tbn_illegal_ad_issue')
            ->alias('tiai')
            ->join('tbn_illegal_ad as tia on tiai.fillegal_ad_id = tia.fid and tia.fcustomer="'.$system_num.'"')
            ->join('tmedia as tm on tiai.fmedia_id = tm.fid and tm.fid=tm.main_media_id')
            ->join('tadclass as tc on tia.fad_class_code = tc.fcode')
            ->join('tregion tn on '.$region_field.' = tn.fid'.$wherestr)
            ->join('tmedia_temp ttp on tm.fid=ttp.fmediaid and ttp.ftype in(1,2) and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
            ->where($where)
            ->count();
        $res = M('tbn_illegal_ad_issue')
            ->alias('tiai')
            ->field('
                tia.fid as sid,
                tia.fmedia_class,
                tc.ffullname as fadclass,
                tia.fad_name,
                 (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,
                tiai.fissue_date,
                tiai.fstarttime,
                tiai.fendtime,
                tiai.fpage,
                tia.fillegal,
                tia.fexpressions,
                tia.fillegal_code,
                tia.favifilename,
                tia.fjpgfilename,
                (UNIX_TIMESTAMP(tiai.fendtime)-UNIX_TIMESTAMP(tiai.fstarttime)) as difftime
            ')
            ->join('tbn_illegal_ad as tia on tiai.fillegal_ad_id = tia.fid and tia.fcustomer="'.$system_num.'"')
            ->join('tmedia as tm on tiai.fmedia_id = tm.fid  and tm.fid=tm.main_media_id')
            ->join('tadclass as tc on tia.fad_class_code = tc.fcode')
            ->join('tregion tn on '.$region_field.' = tn.fid'.$wherestr)
            ->join('tmedia_temp ttp on tm.fid=ttp.fmediaid and ttp.ftype in(1,2) and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
            ->where($where)
            ->page($p,$pp)
            ->select();

        if(empty($outtype)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$res)));
        }else{
            if(empty($res)){
                $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
            }

            $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-数据汇总违法条次列表';//文档内部标题名称
            $outdata['datalie'] = [
                '序号'=>'key',
                '媒体分类'=>[
                    'type'=>'zwif',
                    'data'=>[
                        ['{fmedia_class} == 1','电视'],
                        ['{fmedia_class} == 2','广播'],
                        ['{fmedia_class} == 3','报纸'],
                        ['{fmedia_class} == 13','互联网'],
                    ]
                ],
                '广告分类'=>'fadclass',
                '广告名称'=>'fad_name',
                '发布媒体'=>'fmedianame',
                '播出日期'=>'fissue_date',
                '开始时间'=>'fstarttime',
                '结束时间'=>'fendtime',
                '时长/版面'=>[
                    'type'=>'zwif',
                    'data'=>[
                        ['{fmedia_class} == 1','{difftime}'],
                        ['{fmedia_class} == 2','{difftime}'],
                        ['{fmedia_class} == 3','{fpage}']
                    ]
                ],
                '违法表现代码'=>'fillegal_code',
                '违法表现'=>'fexpressions',
                '涉嫌违法原因'=>'fillegal'
            ];
            $outdata['lists'] = $res;
            $ret = A('Api/Function')->outdata_xls($outdata);

            D('Function')->write_log('数据汇总违法条次',1,'导出成功');
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }
    }



    function prDates($start,$end,$date_num){
        $date_array = [];
        $dt_start = strtotime($start);
        $dt_end = strtotime($end);
        while ($dt_start<=$dt_end){
            $date_array[] = date('Y-m-d',$dt_start);
            $dt_start = strtotime('+1 day',$dt_start);
        }
        return array_chunk($date_array,$date_num);
    }

}