<?php
namespace Api\Controller;
use Think\Controller;
use OpenSearch\Client\OpenSearchClient;
use OpenSearch\Client\DocumentClient;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;

class AdController extends Controller {
	
	/*获取广告列表*/
    public function get_ad_list(){
		
		$ad_name = I('ad_name');//获取广告名称
		$where = array();
		$where['fstate'] = 1;
		$where['fadid'] = array('gt',0);
		
		$where['fadname'] = array('like','%'.$ad_name.'%');
		
		$adList = M('tad')->field('fadid,fadname')->where($where)->limit(10)->select();//查询广告列表

		$this->ajaxReturn(array('code'=>0,'value'=>$adList));
		
	}
	/*获取品牌*/
	public function get_brand(){
		
		$ad_name = I('ad_name');//获取广告名称
		$where = array();
		$where['fstate'] = 1;
		$where['fadid'] = array('gt',0);
		
		$where['_string'] = '"'.$ad_name.'" like concat("%",fbrand,"%") and fbrand <> ""';
		if($ad_name){
			$brandList = M('tad')->field('fbrand')->where($where)->group('fbrand')->limit(10)->select();//
		}else{
			$brandList = array();
		}
		

		$this->ajaxReturn(array('code'=>0,'value'=>$brandList));
		
	}
	
	
	/*广告详情*/
	public function ajax_ad_details(){
		
		$fadid = I('fadid');//获取媒介ID
		$adDetails = M('tad')->where(array('fadid'=>$fadid))->find();//查询广告详情
		
		$adDetails['adclass'] = M('tadclass')->where(array('fcode'=>$adDetails['fadclasscode']))->getField('ffullname');//查询广告类别的名称
		$adDetails['adowner_name'] = M('tadowner')->where(array('fid'=>$adDetails['fadowner']))->getField('fname');//查询广告主名称

		
		$this->ajaxReturn(array('code'=>0,'adDetails'=>$adDetails));
	}
	
	/*获取指定电视广告名称的药监局字段*/
	public function get_tv_ad_more_field(){
		$fadname = I('fadname');//广告名称
		
		$moreFieldList = M('ttvsample')->cache(true,10)
									->field('
												ttvsample.fadmanuno,
												ttvsample.fmanuno,
												ttvsample.fadapprno,
												ttvsample.fapprno,
												ttvsample.fadent,
												ttvsample.fent,
												ttvsample.fentzone,
												ttvsample.fadlen
												')
									->join('tad on tad.fadid = ttvsample.fadid')
									->where(array('tad.fadname'=>$fadname))
									->group('ttvsample.fmanuno')
									->select();
									
		$this->ajaxReturn(array('code'=>0,'msg'=>'','moreFieldList'=>$moreFieldList));							
		
		
		
	}
	
	/*获取指定广播广告名称的药监局字段*/
	public function get_bc_ad_more_field(){
		$fadname = I('fadname');//广告名称
		
		$moreFieldList = M('tbcsample')->cache(true,10)
									->field('
												tbcsample.fadmanuno,
												tbcsample.fmanuno,
												tbcsample.fadapprno,
												tbcsample.fapprno,
												tbcsample.fadent,
												tbcsample.fent,
												tbcsample.fentzone,
												tbcsample.fadlen
												')
									->join('tad on tad.fadid = tbcsample.fadid')
									->where(array('tad.fadname'=>$fadname))
									->group('tbcsample.fmanuno')
									->select();
									
		$this->ajaxReturn(array('code'=>0,'msg'=>'','moreFieldList'=>$moreFieldList));							
		
		
		
	}
	
	/*获取指定报纸广告名称的药监局字段*/
	public function get_paper_ad_more_field(){
		$fadname = I('fadname');//广告名称
		
		$moreFieldList = M('tpapersample')->cache(true,10)
									->field('
												tpapersample.fadmanuno,
												tpapersample.fmanuno,
												tpapersample.fadapprno,
												tpapersample.fapprno,
												tpapersample.fadent,
												tpapersample.fent,
												tpapersample.fentzone
												')
									->join('tad on tad.fadid = tpapersample.fadid')
									->where(array('tad.fadname'=>$fadname))
									->group('tpapersample.fmanuno')
									->select();
									
		$this->ajaxReturn(array('code'=>0,'msg'=>'','moreFieldList'=>$moreFieldList));							
		
		
		
	}
	
	/*获取指定网络广告名称的药监局字段*/
	public function get_net_ad_more_field(){
		$fadname = I('fadname');//广告名称
		
		$moreFieldList = M('tnetissue')->cache(true,10)
									->field('
												tnetissue.fadmanuno,
												tnetissue.fmanuno,
												tnetissue.fadapprno,
												tnetissue.fapprno,
												tnetissue.fadent,
												tnetissue.fent,
												tnetissue.fentzone
												')
									->where(array('tnetissue.fadname'=>$fadname,'tnetissue.fmanuno'=>array('neq','')))
									->group('tnetissue.fmanuno')
									->select();
									
		$this->ajaxReturn(array('code'=>0,'msg'=>'','moreFieldList'=>$moreFieldList));							
		

		
	}
	
	
	/**
     * 根据广告名搜索广告信息
     * @param $adName string 广告部分或全部名称
     */
    public function searchAdByName(){
		$jsonParameter = json_decode(file_get_contents('php://input'),true);
		if($jsonParameter){
			$adName = $jsonParameter['adName'];
			$fcreator_id = $jsonParameter['fcreator_id'];
			$fpriority = $jsonParameter['fpriority'];
			
		}else{
			$adName = I('get.adName','');
			$fcreator_id = I('get.fcreator_id','');
			$fpriority = I('get.fpriority','');
			
			if(!$adName) $adName = I('post.adName','');
			if(!$fcreator_id) $fcreator_id = I('post.fcreator_id','');
			if(!$fpriority) $fpriority = I('post.fpriority','');
			
			
		}
        if(!$fpriority) $fpriority = '1';
		$adName = str_replace('%','%',$adName);
		$adName = explode('%',$adName);
        header('Access-Control-Allow-Origin:*');//允许跨域
		$adNameList = A('Api/Ad','Model')->searchAdByName($adName,'',$fpriority);
		if($fcreator_id){ #判断是否传入创建人id
		
			$adNameListSelf = A('Api/Ad','Model')->searchAdByName($adName,$fcreator_id,$fpriority);
			if($adNameListSelf) $adNameList = array_merge($adNameList,$adNameListSelf);
		}
		$this->ajaxReturn(array('code'=>0,'data'=>$adNameList,'value'=>$adNameList));
	}
	
	

    /**
     * 根据广告名搜索广告信息
     * @param $adName string 广告部分或全部名称
     */
    public function searchAdByName_20190812($adName = '')
    {
        $adName = str_replace([' ', '　', '％'], '%', $adName);
        $where = [
            'is_sure' => ['GT', 0],
            //'fadid' => ['NEQ', 0],
            'fadname' => ['LIKE', '%' . $adName . '%'],
        ];
		$res0 = M('tad')->cache(true,600)
            ->field('fadname value')
            ->where(array('is_sure'=>array('gt',0),'fadname'=>$adName))
           
            ->limit(1)->select();
		
		
        $resList = M('tad')->cache(true,600)
            ->field('fadname value')
            ->where($where)
            //->order("field(fadname, '$adName') desc".', fmodifytime desc')
            ->limit(10)->select();
		$res1 = array();	
		foreach($resList as $resL){
			if($resL['value'] != '未命名广告' && $resL['value'] != $adName){
				$res1[] = array('value'=>$resL['value']);
			}
			
		}	
        $this->ajaxReturn([
            'code' => 0,
            'data' => array_merge($res0,$res1),
        ]);
	}

    /**
     * 根据广告名搜索是否确认
     * @param $adName string 广告名
     */
    public function checkAdIsSureByName($adName='')
    {
		header('Access-Control-Allow-Origin:*');//允许跨域
		$jsonParameter = json_decode(file_get_contents('php://input'),true);
		if($jsonParameter){
			$adName = $jsonParameter['adName'];
		}else{
			$adName = I('get.adName','');
			if(!$adName) $adName = I('post.adName','');
		}
		
        $adName = trim($adName);
        $where = [
            'fadid'   => ['NEQ', 0],
            'fadname' => ['EQ', $adName],
            'fstate'  => ['GT', 0]
        ];
        $res = M('tad')->field('fadclasscode, fadclasscode_v2, fadid, fadname, fadowner, fbrand, is_sure')->where($where)->order('fmodifytime desc')->find();
        if ($res){
            $res['fname'] = $res['fadownername'] = M('tadowner')->where(['fid' => ['EQ', $res['fadowner']]])->getField('fname');
            $fadclassInfo = M('tadclass')->where(['fcode'=>$res['fadclasscode']])->find();
            $res['fadclassname'] = $fadclassInfo['fadclass'];
            $res['fadclassfullname'] = $fadclassInfo['ffullname'];
            $fadclassInfo_v2 = M('hz_ad_class')->where(['fcode'=>$res['fadclasscode_v2'],'fstatus'=>1])->find();
            $res['fadclassname_v2'] = $fadclassInfo_v2['fname'];
            $res['fadclassfullname_v2'] = $fadclassInfo_v2['ffullname'];
        }else{
            $res = [
                'is_sure' => 0
            ];
        }
        $this->ajaxReturn([
            'code' => 0,
            'data' => $res,
        ]);
    }


    /**
     * 根据广告id搜索是否确认
     * @param $id
     */
    public function checkAdIsSureById($id)
    {
        $where = [
            'fstate' => ['EQ', 1],
            'fadid' => ['EQ', $id]
        ];
        $res = M('tad')->where($where)->order('fadid asc')->find()['is_sure'];
        $this->ajaxReturn([
            'code' => 0,
            'data' => $res,
        ]);
    }

    /**
     * 将已确认的广告标记为需要修改, fstate改为9
     * @param $fadid
     * @param $comment
     */
    public function changeAdIsSure($fadid, $comment)
    {
        $alias = M('ad_input_user')->where(['wx_id' => ['EQ', session('wx_id')]])->getField('alias');
        $comment = "提交人: $alias . " . $comment;
        $res = M('tad')->where([
            'fadid' => ['EQ', $fadid],
            'is_sure' => ['NEQ', 0],
        ])->save([
            'fstate' => 9,
            'comment' => $comment
        ]) !== false;
        $logData = [
            'id' => $fadid,
            'comment' => $comment
        ];
        if ($res){
            $logData['msg'] = '提交已确认广告错误成功';
            A('Common/AliyunLog','Model')->ad_log($logData);
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功',
            ]);
        }else{
            $logData['msg'] = '提交已确认广告错误失败';
            A('Common/AliyunLog','Model')->ad_log($logData);
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
    }

    /**
     * 确认广告
     * @param $id
     */
    public function confirmAd($id)
    {
        $adInfo = M('tad')->find($id);
        $arr = ['fadname', 'fbrand', 'fadclasscode', 'fadowner', 'fadclasscode_v2'];
        $isComplete = true;
        foreach ($arr as $item) {
            if ($adInfo[$item] == ''){
                $isComplete = false;
            }
        }
        if ($isComplete){
            $wx_id = session('wx_id');
            $where = ['fadid' => ['EQ', $id]];
            $hasConfirm = $adInfo['is_sure'] != 0;
            if ($hasConfirm){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '该广告已确认'
                ]);
            }
            $data = [
                'is_sure' => 1,
                'fmodifytime' => date('Y-m-d H:i:s'),
                'confirmer' => $wx_id
            ];
            $res = M('tad')->where($where)->save($data) == false ? false : true;
            $logData = [
                'id' => $id,
            ];
            if ($res){
                $logData['msg'] = '确认成功';
                A('Common/AliyunLog','Model')->ad_log($logData);
                A('InputPc/Task','Model')->change_task_count($wx_id,'v3_ad_sure',1);
                $this->ajaxReturn([
                    'code' => 0,
                    'msg' => '确认成功',
                ]);
            } else {
                $logData['msg'] = '确认失败';
                A('Common/AliyunLog','Model')->ad_log($logData);
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '确认失败',
                ]);
            }
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '广告信息不完整, 请补充完整后再确认',
            ]);
        }


	}

    /**
     * 更新与广告有关的部分信息: 广告名, 品牌, 广告主, 类别
     * @param $adInfo array 更新后的广告完整信息
     */
    public function updateAdPartialInfo($adInfo)
    {
        $wx_id = session('wx_id');
        $alias = M('ad_input_user')->cache(60)->where(['wx_id' => ['EQ', $wx_id]])->getField('alias');
        $where = [
            'fadid' => ['EQ', $adInfo['fadid']],
            'fcreator' => ['EQ', $alias],
        ];
        $recordData = [
            'fadid' => $adInfo['fadid'],
            'wx_id' => $wx_id,
            'before' => json_encode(M('tad')->find($adInfo['fadid'])),
            'update_time' => time(),
            'user_name' => $alias
        ];
        $data = [
            'fadname' => $adInfo['fadname'],
            'fbrand' => $adInfo['fbrand'],
            'fadclasscode' => $adInfo['adClassCode'],
            'fadclasscode_v2' => $adInfo['adClassCodeV2'],
            'fadowner' => A('Common/Ad','Model')->getAdOwnerId($adInfo['adowner_name'], $alias),
        ];
        $logData = [
            'old' => M('tad')->find($adInfo['fadid']),
            'new' => $adInfo
        ];
        $res = M('tad')->where($where)->save($data);
        if ($res){
            // 修改成功了，再修改相关的修改人信息
            $data = [
                'fmodifier' => $alias,
                'fmodifytime' => date('Y-m-d H:i:s'),
                'is_sure' => 1
            ];
            M('tad')->where($where)->save($data);
            // 将当前广告名称的其他已确认广告取消确认
            M('tad')
                ->where([
                    'fadname' => ['EQ', $adInfo['fadname']],
                    'fadid' => ['NEQ', $adInfo['fadid']],
                    'is_sure' => ['EQ', 1]
                ])
                ->save([
                    'is_sure' => 0
                ]);
            $recordData['after'] = json_encode(M('tad')->find($adInfo['fadid']));
            M('ad_record')->add($recordData);
            $logData['msg'] = '修改成功';
            A('Common/AliyunLog','Model')->ad_log($logData);
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '更新成功',
            ]);
        }else{
            $logData['msg'] = '修改失败';
            A('Common/AliyunLog','Model')->ad_log($logData);
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '更新失败',
            ]);
        }
    }

    /**
     * 搜索广告品牌
     * @param $keyword
     */
    public function searchAdBrand($keyword)
    {
        $where = [
            'fstate' => ['EQ', 1],
            'fadid' => ['NEQ', 0],
            'fbrand' => ['LIKE', '%' . $keyword . '%']
        ];
        $res = M('tad')->field('fbrand value')->where($where)->group('fbrand')->limit(10)->select();
        $this->ajaxReturn([
            'code' => 0,
            'data' => $res,
        ]);
    }


    /**
     * 添加已确认的广告
     * @param $adInfo
     */
    public function addConfirmedAd($adInfo)
    {
        foreach ($adInfo as $item) {
            if ($item === ''){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '广告信息不全'
                ]);
            }
        }
        $fadname = $adInfo['fadname'];
        $fbrand = $adInfo['fbrand'];
        $adclass_code = $adInfo['fadclasscode'];
        $adclass_code_v2 = $adInfo['fadclasscode_v2'];
        $adowner_name = $adInfo['fname'];
        $alias = M('ad_input_user')
            ->where([
                'wx_id' => ['EQ', session('wx_id')],
            ])
            ->getField('alias');
        // 任务信息
        $adId = A('Common/Ad','Model')->getAdId($fadname,$fbrand,$adclass_code,$adowner_name,$alias, $adclass_code_v2);//获取广告ID
        $this->confirmAd($adId);
    }
	
	/*同步广告主题数据到OpenSearch*/
	public function sync_ad_opensearch(){
		
		
		$adList = array();
		for($i=1;$i<500;$i++){
			$adName = redis()->lPop('ad_name_open_search');
			if(!$adName) break;
			
			$adList[$adName] ++;
			
		}
		$openSearchPush = array();
		foreach($adList as $adName => $ad){
			$openSearchPush[] = array('cmd'=>'add','fields'=>array('identify'=>md5($adName),'fadname'=>$adName,'last_time'=>time()));
		}
		
		if($openSearchPush){
			$inputState = A('Common/OpenSearch','Model')->push('tad',$openSearchPush,'ad_name_search');//写入OpenSearch
			
		}
		var_dump($inputState);
		
		
	}
	
	
	/*同步广告主题数据到OpenSearch*/
	public function sync_ad_opensearch2(){
		session_write_close();
		set_time_limit(600);
		$msectime = msectime();
		header("Content-type: text/html; charset=utf-8");
		
		$mysqlAdList = M('tad')->field('fadid,fadname,fpriority,fstate,fcreator_id')->where(array('to_opensearch_time'=>array('lt',time()-3600*24)))->limit(200)->select();
		$mysqlAdCount = M('tad')->where(array('to_opensearch_time'=>array('lt',time()-3600*24)))->count();
		
		
		$openSearchPush = [];
		$adIds = [];
		foreach($mysqlAdList as $mysqlAd){
			
			$data = array();
			$data['identify'] = md5($mysqlAd['fadid']);
			$data['fadname'] = $mysqlAd['fadname'];
			$data['fpriority'] = $mysqlAd['fpriority'];
			$data['fstate'] = $mysqlAd['fstate'];
			$data['fcreator_id'] = $mysqlAd['fcreator_id'];
			
			if($mysqlAd['fstate'] == -1){
				$openSearchPush[] = array('cmd'=>'delete','fields'=>array('identify'=>md5($mysqlAd['fadid'])));
			}else{
				$openSearchPush[] = array('cmd'=>'add','fields'=>$data);
			}
			$adIds[] = $mysqlAd['fadid'];
		}
		
		
		
		if($openSearchPush){
			$inputState = A('Common/OpenSearch','Model')->push('tad',$openSearchPush,'ad_name_search');//写入OpenSearch
			$inputState = json_decode($inputState->result,true);
			
			if($inputState['status'] == 'OK'){
				M('tad')->where(array('fadid'=>array('in',$adIds)))->save(array('to_opensearch_time'=>time()));
			}
		
			
		}
		
		
		echo $inputState['status']."\n";
		echo '共同步 '.count($openSearchPush)." 条广告\n";
		echo '用时 '. (msectime() - $msectime) ." 毫秒\n";
		
		
		echo '还剩余 '.$mysqlAdCount." 条广告未同步\n";

		
		
	}
	
	
	public function test(){
		$fadid = '110000000022773';
		$rr = A('Api/Ad','Model')->delOpenAd($fadid);
		
		var_dump($rr);
	}
	
	
	
	
	
	
	
	
	
	
	
	

}