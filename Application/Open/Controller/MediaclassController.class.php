<?php
namespace Open\Controller;
use Think\Controller;

class MediaclassController extends BaseController {
	
	/*获取媒介类别列表*/
    public function get_mediaclass_list(){
		$mediaclass_data = file_get_contents('php://input');//获取post数据
		$mediaclass_data = json_decode($mediaclass_data,true);//把json转化成数组
		
		if($mediaclass_data['model'] == 'get_all'){//判断是否获取全部
			$mediaclassList = M('tmediaclass')->field('')->where()->select();//
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取的是全部列表','mediaclassList'=>$mediaclassList));
		}
		$fid = $mediaclass_data['fid'];//媒介分类code
		if($fid == '') $fid = '';
		//var_dump($fid);
		$mediaclassList = M('tmediaclass')->field('fid,fclass,ffullname')->where(array('fpid'=>$fid))->select();//查询媒介分类列表
		
		$mediaclassDetails = M('tmediaclass')->field('fid,fclass,ffullname')->where(array('fid'=>$fid))->find();//查询媒介分类详情
		
		if(!$mediaclassDetails) $mediaclassDetails = array('fid'=>'','fclass'=>'全部','ffullname'=>'全部');
		$this->ajaxReturn(array('code'=>0,'msg'=>'','mediaclassList'=>$mediaclassList,'mediaclassDetails'=>$mediaclassDetails));
		
	}
	
}