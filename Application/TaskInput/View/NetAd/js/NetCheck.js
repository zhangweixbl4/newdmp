Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}
Vue.use(VueViewer.default)
const initClassCode = num => {
  if (num === '') {
    return []
  } else if (num.length === 2) {
    return [num]
  } else {
    return [num.substr(0, 2), num]
  }
}
const vm = new Vue({
  el: "#app",
  directives: {
    scroll: {
      inserted: (el,binding) => {
        el.addEventListener('scroll', () => {
          let scrollHeight = el.scrollHeight
          let scrollTop = el.scrollTop
          let height = el.offsetHeight
          if ((scrollTop + height) === scrollHeight) {
            let _this = binding.value
            _this.page.pageIndex += 1
            if(_this.haveMore) _this.searchAd()
          }
        })
      }
    }
  },
  data() {
    return {
      timeValue: [],
      adValue: '',
      loading: false,
      visible: false,
      haveMore: true,
      hadSearch: false,
      lLoading: false,
      fid: '',
      page: {
        pageSize: 200,
        pageIndex: 1
      },
      overtime: 1200,
      boxHeight: '',
      bodyStyle: {
        'height': '100%',
        'box-sizing': 'border-box',
        'padding': '10px',
        'overflow': 'auto'
      },
      activeRow: {
        id: ''
      },
      adList: [],
      form: {
        id: '',
        adilllaw: '',
        brand: '',
        title: '',
        adtype: [],
        advertiser_id: '',
        advertiser_na: '',
        adilltype: '0',
        adillegal: '',
        adillcontent: '',
        urgentcount:0
      },
      isEdit: false,
      rules: {
        adilllaw: [
          { required: true, message: '请选择违法表现', trigger: 'blur' }
        ],
        adillegal: [
          { required: true, message: '违法表现不能为空', trigger: 'blur' }
        ],
        adillcontent: [
          { required: true, message: '违法内容不能为空', trigger: 'blur' }
        ]
      },
      listForm: {
        pageIndex: 0
      },
      haveMore: true,
      showIllegal: true,
      historyEdit: false,
      filterText: '',
      illegalTypeArr: illegalTypeArr,
      illegalArr: illegalArr,
      adClassArr: adClassArr,
      selectedList: [],
      intervalTime: 1200,
      getTaskTime: '',
      timer: null,
      owner: [],
      treeProps: {
        label: 'label',
        children: 'children'
      },
      bucket_url: '',
      uploading: false,
      uploadOptions: {},
      upOptions: {
        up_server: ""
      },
      suggestInfo: '',
      btnForbidden: false,
      taskCount: '',
      showClassTip: false
    }
  },
  watch: {
    filterText(val) {
      this.$refs.tree.filter(val);
    },
    'form.title': {
      handler(n, o) {
        this.compareData(this.activeRow, this.form)
      },
      deep: true
    },
    'form.advertiser_na': {
      handler(n, o) {
        this.compareData(this.activeRow, this.form)
      },
      deep: true
    },
    'form.adillcontent': {
      handler(n, o) {
        this.compareData(this.activeRow, this.form)
      },
      deep: true
    }
  },
  computed: {
    taskTitle() {
      return `领取：${this.getTaskTime}，剩余 ${this.intervalTime} 秒` || ''
    }
  },
  mounted() {
    this.$nextTick(() => {
      this.load()
      this.showIllegal = false
      this.getBoxHeight()
      document.querySelector('#app').style.opacity = 1
    })
    window.onresize = () => {
      this.getBoxHeight()
    }
  },
  methods: {
    setTimer(time) {
      let endtime = Number(time) + this.overtime
      this.timer = setInterval(() => {
        let nowtime = +new Date() / 1000
        let intervalTime = parseInt(endtime - nowtime)
        if (intervalTime <= 0) {
          clearInterval(this.timer)
          this.$alert('抱歉, 任务超时被收回, 点击确定开始下一个任务').then(res => {
            this.load()
          })
        } else {
          this.intervalTime = intervalTime
        }
      }, 1000)
    },
    load() {
      this.loading = true
      clearInterval(this.timer)
      $.post('/TaskInput/NetAd/getNetCheckTask')
        .then(res => {
          if(res.code === 0){
            if(res.data.check_reason == null) {
              this.suggestInfo = `补录原因：无`
            } else {
              this.suggestInfo = `补录原因：${res.data.check_reason}`
            }
            let data = res.data
            if (data.adtype) {
              data.adtype = initClassCode(data.adtype)
            } else {
              data.adtype = []
            }
            if (data.adillcontent === null) data.adillcontent = ''
            if (data.adillegal === null) data.adillegal = ''
            if (data.adilllaw === null) data.adilllaw = ''
            if (data.adtypename === null) data.adtypename = ''
            if(data.count === null) data.count = ''
            this.taskCount = data.count
            this.setAd({ ...data })
            this.form = { ...data }
            this.overtime = Number(res.overtime)
            this.getTaskTime = moment.unix(data.modifytime).format('HH:mm:ss')
            this.setTimer(data.modifytime)
          }else if(res.code === 1){
            this.form = {
              id: '',
              adilllaw: '',
              brand: '',
              title: '',
              adtype: [],
              advertiser_id: '',
              advertiser_na: '',
              adilltype: '0',
              adillegal: '',
              adillcontent: '',
              urgentcount:0
            }
            this.activeRow = {
              id: ''
            },
            this.$message({
              message: res.msg,
              duration: 1500
            })
          }else{
            this.$message.error(res.msg)
          }
          this.loading = false
        })
    },
    compareData(sourceData, data) {
      let isedit = false
      const compareArr = ["title", "brand", "adtype", "adtypename", "advertiser_id", "advertiser_na", "file_url", "target_url", "original_url", "adilltype", "adilllaw", "adillegal", "adillcontent", "shutpage", "shutpage_thumb", "type"]
      compareArr.forEach(i => {
        switch (i) {
          case 'adtype':
            let a = sourceData[i] ? [...sourceData[i]].pop() : ''
            let b = [...data[i]].pop()
            if (a !== b) isedit = true
            break
          case 'adilltype':
            if (sourceData[i] !== '0') {
              let adilllaw = this.selectedList.map(i => i.fcode).join(',')
              if (sourceData.adilllaw !== adilllaw) {
                isedit = true
              }
              if (sourceData.adillcontent !== data.adillcontent) {
                isedit = true
              }
            } else if(sourceData[i] === '0') {
              if (sourceData[i] !== data[i]) {
                isedit = true
              }
            }
            break;
          case 'adillcontent':
            break;
          default:
            if (sourceData[i] !== data[i]) {
              isedit = true
            }
            break
        }
      })
      this.isEdit = isedit
    },
    initData() {
      let data = { ...this.form }
      let adilllaw = this.selectedList.map(i => i.fcode).join(',')
      this.form.adilllaw = adilllaw
      data.adilllaw = adilllaw
      data.adtype = data.adtype[data.adtype.length - 1]
      if (data.adilltype === '0') {
        delete data.adillegal
        delete data.adilllaw
        delete data.adillcontent
      }
      if (!data.shutpage) {
        this.$message.error('请上传截图后提交')
        return false
      }
      let r = this.owner.find(i => i.value === data.advertiser_na)
      r ? data.advertiser_id = r.fid : data.advertiser_id = ''
      return data
    },
    adNameBlur() {
      this.showClassTip = $h.testAdName(this.form.title)
    },
    submit() {
      let data = this.initData()
      if (!$h.checkAdNameAndCode(data.title, data.adtype)) {
        return this.$message.error('公益广告请按此格式填写：公益（xxxxxx）')
      }
      if(!data) return false
      this.$refs.form.validate((valid) => {
        if (valid) {
          if (this.isEdit) {
            this.$confirm('该数据已被修改过, 是否继续?', '提示', {
              confirmButtonText: '确定',
              cancelButtonText: '取消',
              type: 'warning'
            }).then(() => {
              this._submit(data)
            }).catch(() => {
            })
          } else {
            this._submit(data)
          }
        }
      })
    },
    _submit(data) {
      this.loading = true
      $.ajax({
        type: 'POST',
        url: '/TaskInput/NetAd/checkPassSave',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(data),
        success: res => {
          if (res.code === 0) {
            this.reset()
            this.load()
            // 有搜索结果重新请求，更新数据
            if(this.adList.length) this.searchAd(1)
          } else {
            this.$message.error(res.msg)
          }
        }
      })
    },
    // nopass() {
    //   this.loading = true
    //   this.visible = false
    //   $.post('/TaskInput/NetAd/checkReject', JSON.stringify({ id: this.form.id }))
    //   .then((res) => {
    //     if(res.code === 0){
    //       this.load()
    //     }else{
    //       this.$message.error(res.msg)
    //     }
    //   })
    //   .catch(() => {
    //   })
    // },
    finiash() {
      let fid = this.fid
      $.post('/TaskInput/NetAd/NetPlanFinish', JSON.stringify({ fid }))
        .then(res => {
          if (res.code === 0) {
            this.$message({
              message: res.msg,
              type: 'success',
              duration: 1500
            })
          }
        })
    },
    filterUnActive() {
      let activeIndex = 0
      let result = []
      this.adList.forEach((i, index) => {
        if (i.active) {
          activeIndex = index
        } else {
          result.push(i)
        }
      })
      return { result, activeIndex }
    },
    searchAd(page) {
      if (page) {
        this.page.pageIndex = 1
        this.haveMore = true
      }
      this.hadSearch = true
      this.lLoading = true
      let data = {
        title: this.adValue,
        date: (this.timeValue || []).join(','),
        ...this.page
      }
      $.post('/TaskInput/NetAd/getHistoryList',JSON.stringify(data))
        .then((res) => {
          if (res.code === 0) {
            // 取消 loading
            this.lLoading = false
            let list = res.data.map(i => {
              if (i.adtype) {
                i.adtype = initClassCode(i.adtype)
              } else {
                i.adtype = []
              }
              if (i.adillcontent === null) i.adillcontent = ''
              if (i.adillegal === null) i.adillegal = ''
              if (i.adilllaw === null) i.adilllaw = ''
              if (i.adtypename === null) i.adtypename = ''
              return i
            })
            if (data.pageIndex === 1) {
              // 如果是页码为 1，把 adList 直接替换
              this.adList = list
            } else {
              // 如果是页码不为 1，在 adList 后拼接返回的结果
              this.adList = [...this.adList, ...list]
            }
            // 如果返回结果条数小于定义的条数，说明无更多数据
            if (list.length < this.page.pageSize) {
              this.haveMore = false
            } else {
              this.haveMore = true
            }
          }
      })
    },
    /**
     * 设置表格高度
     */
    getBoxHeight() {
      this.boxHeight = `${document.body.clientHeight - 63}px`
    },
    clearActive() {
      this.adList = this.adList.map(i => {
        i.active = false
        return i
      })
    },
    historyClick(row) {
      this.historyEdit = true
      if(!row.check_reason) {
        this.suggestInfo = `补录原因：无`
      } else {
        this.suggestInfo = `补录原因：${row.check_reason}`
      }
      $.post('/TaskInput/NetAd/getIsAllowSubmit',JSON.stringify({taskid: row.id}))
      .then((res)=> {
        if(res.code == 0){
          if(res.data == 0) {
            this.btnForbidden =  true
          }
          if(res.data == 1) {
            this.btnForbidden = false
            this.setAd(row)
          }
        }
      }),
      this.setAd(row)
    },
    backToTask() {
      this.btnForbidden =  false
      this.historyEdit = false
      this.load()
    },
    /**
     * 设置当前选中的广告
     * @param {object} row 点击广告的广告对象
     */
    setAd(row) {
      let _row = row
      if (_row.adtype && typeof(_row.adtype) === 'string') {
        let num = _row.adtype
        if (num === '') {
          _row.adtype = []
        } else if (num.length === 2) {
          _row.adtype = [_row.adtype]
        } else {
          _row.adtype = [num.substr(0, 2), num]
        }
      }
      this.activeRow = row
      this.searchAdOwner(row.advertiser_na)
      Object.keys(this.form).forEach(i => {
        this.form[i] = row[i] || ''
        if (i === 'adilllaw' && row[i]) {
          this.$refs.tree.setCheckedKeys(row[i].split(','))
          this.selectedList = this.$refs.tree.getCheckedNodes()
        }
      })
    },
    alertClose(obj) {
      let keys = []
      let selectedList = this.selectedList.filter(item => {
        if (item.id !== obj.id) {
          keys.push(item.id)
        }
        return item.id !== obj.id
      })
      this.selectedList = selectedList
      this.form.adillegal = this.selectedList.map(i => i.fexpression).join(';')
      this.$refs.tree.setCheckedKeys(keys)
      this.compareData(this.activeRow, this.form)
    },
    // 设置已选中的节点
    nodeCheck(node, data) {
      this.selectedList = data.checkedNodes
      this.form.adillegal = this.selectedList.map(i => i.fexpression).join(';')
      this.compareData(this.activeRow, this.form)
    },
    // 搜索节点处理函数
    filterNode(value, data) {
      if (!value) return true
      return data.label.indexOf(value) !== -1
    },
    // 清空已选节点
    reset() {
      this.selectedList = []
      this.$refs.tree.setCheckedKeys([])
    },
    searchAdName(queryString, cb) {
      $.post("/Api/Ad/searchAdByName", {
        adName: queryString
      }).then((res) => {
        cb(res.data);
      })
    },
    searchAdOwner(queryString, cb) {
      $.post("/Api/Adowner/searchAdOwner", {
        name: queryString
      }).then((res) => {
        this.owner = res.data
        cb(res.data)
      })
    },
    inputAdName() {
      $.post('/Api/Ad/checkAdIsSureByName', { adName: this.form.title })
        .then((res) => {
          const r = res.data
          if (r.is_sure != 0) {
            this.form.title = r.fadname
            this.form.brand = r.fbrand
            this.form.advertiser_na = r.fname
            this.form.advertiser_id = r.fadowner
            this.form.adtype = initClassCode(r.fadclasscode)
          }
        this.$forceUpdate()
      })
    },
    getQueryString(name) {
      let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
      let r = window.location.search.substr(1).match(reg);
      if (r != null) return unescape(r[2]); return null;
    },
    /**
     * 获取上传七牛服务器和token
     */
    _getToken() {
      $.post('/TaskInput/TraditionAd/get_file_up')
        .then(res => {
          if (res.code === 0) {
            this.upOptions = res.data
            this.uploadOptions = res.data.val
            this.bucket_url = res.data.bucket_url
          } else {
            this.$message.error(res.msg)
          }
        })
        .catch(() => {
          this.$message.error('网络错误')
        })
    },
    _setKey(file) {
      this.uploading = true
      this.uploadOptions.key = `${file.uid}.${file.type.split('/').pop()}`
    },
    _uploadSuccess(response, file, fileList) {
      const shutpage = `${this.bucket_url}${response.key}`
      // 给原图赋值
      this.activeRow.shutpage = shutpage
      let data = {
        id: this.activeRow.id,
        shutpage,
      }
      $.post('/TaskInput/NetAd/reUploadShutpage',JSON.stringify(data))
        .then(res => {
          if (res.code === 0) {
            // 给缩略图赋值
            this.activeRow.shutpage_thumb = res.shutpage_thumb
            this.activeRow.shutpage = shutpage
            this.form.shutpage_thumb = res.shutpage_thumb
            this.form.shutpage = shutpage
          } else {
            this.$message.error(`${res.msg}，重新获取任务`)
            this.load()
          }
          this.uploading = false
    })
      this.$message.success('文件上传完成！')
    },
    _uploadError() {
      this.uploading = false
      this.$message.error('上传出错！')
    },
    _uploadProgress() {
    },
  }
})