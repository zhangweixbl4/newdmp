<?php
namespace TaskInput\Model;
use Think\Exception;


class CutErrModel {

    public function cutErr($mediaClass,$samId,$cut_err_reason){
		
		$errLog = date('Y-m-d H:i:s').'	'.$mediaClass.'	'.$samId.'	'.$cut_err_reason.'	';//日志
        $wx_id = session('wx_id');
        $userName = M('ad_input_user')->cache(true, 60)->where(['wx_id' => ['EQ', $wx_id]])->getField('alias');
        $wx_username = $wx_id.'_'.$userName;
		
		
		if($mediaClass == 'tv' || $mediaClass == 'bc'){//电视或广播退回操作
		
			$ai_aip_url = 'http://47.96.182.117/index/saveUuid';
			M()->startTrans();
			$samInfo = M('t'.$mediaClass.'sample')->field('issue_relation,uuid,fmediaid,favifilename')->where(array('fid'=>$samId))->find();//查询样本信息
			$errLog .= $samInfo['uuid'].'	';
			$issue_relation_list =$samInfo['issue_relation'];//查询该样本的关联发布记录表
			$issue_relation_list = array_filter(explode(',',$issue_relation_list));//转为数组并去掉空数组
			$del_s_num = 0;
			
			/* foreach($issue_relation_list as $table_name){//循环关联的发布记录表
				try{//
					$del_s_num += M($table_name)->where(array('fsampleid'=>$samId))->delete();//删除发布记录分表数据
				}catch(Exception $e) { //
					
				} 
			} */
			#$del_l_num = M('t'.$mediaClass.'issue')->where(array('f'.$mediaClass.'sampleid'=>$samId))->delete();//删除发布记录总表数据
            $cut_err_reason .= '  录入人: '. $wx_id . '_' . $userName;
			$postData = array('uuid'=>$samInfo['uuid'],'fid'=>$samInfo['fmediaid'],'error_cause'=>$cut_err_reason, 'user' => $wx_id . '_' . $userName);//组装请求到数据生成的接口数据
			$ai_api_ret_json = http($ai_aip_url,$postData,'POST');//发起请求
			$errLog .= $samInfo['fmediaid'].'	'.$samInfo['favifilename'].'	'.$del_l_num.'	'.$del_s_num.'	'.$ai_api_ret_json;
			$ai_api_ret = json_decode($ai_api_ret_json,true);//返回结果转为json
			
			#file_put_contents('LOG/cutErr'.date('Ymd').'.log',$errLog."\n",FILE_APPEND);//记录日志	
			$dmpLogs2 = A('Common/AliyunLog','Model')->dmpLogs2('cut_err',array(
																	'uuid'=>$samInfo['uuid'],
																	'fmediaid'=>$samInfo['fmediaid'],
																	'favifilename'=>$samInfo['favifilename'],
																	
																	'ai_api_ret'=>$ai_api_ret_json,
																	
																	));														
			//var_dump($dmpLogs2);
			if($ai_api_ret['code'] === 0 || true){//判断请求是否返回了正常结果
				#M('t'.$mediaClass.'sample')->where(array('fid'=>$samId))->delete();//删除样本
				M('t'.$mediaClass.'sample')
							->where(array('fid'=>$samId))
							->save(array(
											'fstate'=>-1,
											'fmodifytime'=>date('Y-m-d H:i:s'),
											'fmodifier'=>$wx_username,
											'abnormal_reason'=>$cut_err_reason
										));
				S($samInfo['uuid'].'_'.$samInfo['fmediaid'],null);//清除缓存，新增发布记录会用到这个缓存
				M()->commit();
				return true;
			}else{
				M()->rollback();
				return false;
			}
		
		}elseif($mediaClass == 'paper'){//报纸退回操作
		
			$samInfo = M('tpapersample')->field('fpapersampleid,sourceid,finputstate,finspectstate')->where(array('fpapersampleid'=>$samId))->find();//样本信息
            $sourceInfo = M('tpapersource')->field('fuserid')->where(array('fid'=>$samInfo['sourceid']))->find();//素材信息
			
			M()->startTrans();//开启事务

            $wx_id = session('wx_id');
            $time = time();
            // 找到该版面还没做的样本id
            $sampleWhere = [
                'finputstate' => ['EQ', 1],
                'finspectstate' => ['EQ', 1],
                'sourceid' => ['EQ', $samInfo['sourceid']]
            ];
            $samIds = M('tpapersample')
                ->where($sampleWhere)
                ->getField('fpapersampleid', true);
            if (!$samIds){
                $samIds = ['0'];
            }
            // 将关联所有样本修改状态
            $sampleUpdate = M('tpapersample')
                ->where([
                    'fpapersampleid' => ['IN', $samIds]
                ])
                ->save([
                    'finputstate' => -1,
                    'finspectstate' => -1,
                    'fstate' => -1,
                ]) !== false;
            // 修改当前剪辑错误样本的信息
            $sampleUpdate = $sampleUpdate && M('tpapersample')
                ->where([
                    'fpapersampleid' => ['EQ', $samId]
                ])
                ->save([
                    'cut_err_reason' => $cut_err_reason . '退回人:' . $userName,
                    'sub_cut_err_user' => $wx_id,
                ]) !== false;
            // 找到这些样本还没有做的任务id
            $taskWhere = [
                'media_class' => ['EQ', 3],
                'sam_id' => ['IN', $samIds],
                'task_state' => ['EQ', 0],
                'cut_err_state' => ['EQ', 0]
            ];
            $taskIds = M('task_input')
                ->where($taskWhere)
                ->getField('taskid', true);
            // 修改所有的的任务状态为剪辑错误
            if ($taskIds){
                $taskUpdate = M('task_input')
                    ->where([
                        'taskid' => ['IN', $taskIds],
                        'task_state' => ['EQ', 0],
                    ])
                    ->save([
                        'cut_err_state' => 1,
                    ]) !== false;
                M('task_input_link')
                    ->where([
                        'taskid' => ['IN', $taskIds],
                        'state' => 1
                    ])
                    ->save([
                        'state' => 0,
                        'update' => time(),
                    ]);
            }else{
                $taskUpdate = true;
            }
            // 将修改写入任务流程表
            if ($taskIds){
                $flowData = [];
                foreach ($taskIds as $taskId) {
                    $flowData[] = [
                        'taskid' => $taskId,
                        'flow_content' => '该报纸版面有剪辑错误, 任务被批量标记为剪辑错误',
                        'flow_time' => $time,
                        'wx_id' => $wx_id,
                    ];
                }
                $taskFlowRes = M('task_input_flow')->addAll($flowData);
            }else{
                $taskFlowRes = true;
            }

            // 修改版面素材状态
            $userName = M('ad_input_user')->cache(true, 60)->where(['wx_id' => ['EQ', $wx_id]])->getField('alias');
            $sourceUpdate = M('tpapersource')
                ->where(['fid' => ['EQ', $samInfo['sourceid']]])
                ->save([
                    'fstate' => 6,
                    'cut_err_reason' => '操作人:'.$wx_username.' ||| '.$cut_err_reason,
                    'operation_time' => $time,
                ]);
			file_put_contents('LOG/cutErr'.date('Ymd').'.log',$errLog."\n",FILE_APPEND);//记录日志
            if($sampleUpdate && $taskUpdate && $taskFlowRes && $sourceUpdate){//修改广告样本和版面素材均成功才提交事务
                A('InputPc/Task','Model')->change_task_count($sourceInfo['fuserid'],'paper_cut_err',1);
                $logData = [
                    'sourceId' => $samInfo['sourceid'],
                    'samIds' => $samIds,
                    'currentSamId' => $samId,
                    'taskIds' => $taskIds,
                    'type' => 'save',
                    'msg' => '提交报纸剪辑错误成功',
                ];
                A('Common/AliyunLog','Model')->paper_source_log($logData);
                M()->commit();//提交事务
				return true;
			}else{
                $logData = [
                    'sourceId' => $samInfo['sourceid'],
                    'samIds' => $samIds,
                    'currentSamId' => $samId,
                    'taskIds' => $taskIds,
                    'type' => 'save',
                    'msg' => '提交报纸剪辑错误失败',
                ];
                A('Common/AliyunLog','Model')->paper_source_log($logData);
				M()->rollback();//回滚事务
				return false;
			}
		}
			
	}
	

}