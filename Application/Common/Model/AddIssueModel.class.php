<?php

namespace Common\Model;
class AddIssueModel{
   
   /*新增单媒体天的发布记录*/
   public function add($mediaId,$date){
	   

		$mediaInfo = M('tmedia')->cache(true,120)->field('fid,fmedianame,media_region_id,left(fmediaclassid,2) as media_class')->where(array('fid'=>$mediaId))->find();

		if($mediaInfo['media_class'] == '01'){
			$tb = 'tv';
		}elseif($mediaInfo['media_class'] == '02'){
			$tb = 'bc';
		}else{
			return 0;
		}
		
		$getDataUrl = 'http://118.31.55.177:8002/summary_day/'.$mediaId.'/'.$date.'/';

		$retStr = http($getDataUrl,array(),'GET',false,30);
		$retArr = json_decode($retStr,true);
		
		if($retArr['ret'] !== 0) return -1;
		
		
		$oldIssue = M('t'.$tb.'issue')->field('fstarttime')->where(array('fmediaid'=>$mediaId,'fissuedate'=>$date))->select();
		$oldIssueSTime = array_column($oldIssue,'fstarttime');

		
		
		
		$addAllData = [];
		foreach($retArr['data'] as $issue){
			#var_dump($data['uuid']);
			
			if(in_array(date('Y-m-d H:i:s',intval($issue['start'])),$oldIssueSTime)){
				continue;
			}
			
			$samId = $this->getSamId($mediaInfo,$issue['uuid']);

			if(!$samId) continue;
			
			
			$a_data = array();
			$a_data['fmediaid'] = $mediaId;//媒介ID
			$a_data['f'.$tb.'sampleid'] = $samId;//样本ID
			
			$a_data['fissuedate'] = date('Y-m-d',$issue['start']);//发布日期
			$a_data['fstarttime'] = date('Y-m-d H:i:s',intval($issue['start']));//发布开始时间
			$a_data['fendtime'] = date('Y-m-d H:i:s',intval($issue['end']));//发布结束时间
			$a_data['flength'] = intval($issue['end']) - intval($issue['start']);//发布时长
			$a_data['faudiosimilar'] = intval($issue['score']);//相似度得分
			$a_data['fregionid'] = $mediaInfo['media_region_id'];//地区
			
			$a_data['fu0'] = ($issue['start'] * 1000) - (strtotime(date('Y-m-d',$issue['start'])) * 1000);
			$a_data['fduration'] = ($issue['end'] - $issue['start']) * 1000;

			$a_data['fcreator'] = 'Ai';//创建人
			$a_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_data['fmodifier'] = 'Ai';//修改人
			$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			

			$addAllData[] = $a_data;
		}
		$inCount = 0;
		$rr = M('t'.$tb.'issue')->addAll($addAllData);
		if($rr) $inCount = count($addAllData);
		
		
		return $inCount;
	    
   }
   
   /*根据UUID和媒介id获取样本id*/
   public function getSamId($mediaInfo,$uuid){
		
		$cache_key = 'getSamId_'.$mediaInfo['fid'].'_'.$uuid;
		$cache_fid = S($cache_key);
		if($cache_fid) return $cache_fid; #返回缓存id

		if($mediaInfo['media_class'] == '01'){
			$tb = 'tv';
		}elseif($mediaInfo['media_class'] == '02'){
			$tb = 'bc';
		}else{
			return false;
		}
		
		$samInfo = M('t'.$tb.'sample')->field('fid,fissuedate')->where(array('fmediaid'=>$mediaInfo['fid'],'uuid'=>$uuid))->find();
		
		if($samInfo){
			S($cache_key,$samInfo['fid'],60); #如果有查到样本信息，则存入缓存
			return $samInfo['fid']; #如果有样本，直接返回样本id
		}
		
		#以下是没有样本的情况
		$getUuidUrl = C('AI_DOMAIN').'uuid/get_uuid';
		$getUuidData = array('channel' => $mediaInfo['fid'],'sample_uuid' => $uuid);
		$uuidInfo = json_decode(http($getUuidUrl,$getUuidData,'GET'),true);

		$dmpLogs2 = A('Common/AliyunLog','Model')->dmpLogs2('getuuid',array(
																			'mediaId'=>$mediaInfo['fid'],
																			'uuid'=>$uuid,
																			'uuidInfo'=>json_encode($uuidInfo),
																		));

		if(count($uuidInfo['samList']) == 0) return false; #如果没有返回样本列表 返回false
		$sam = $uuidInfo['samList'][0];
		$a_data = array();
		$a_data['fmediaid'] = $mediaInfo['fid'];//媒介ID
		$a_data['fissuedate'] = date('Y-m-d',$sam['start']);//发布日期
		$a_data['last_issue_date'] = date('Y-m-d',$sam['start']);//最后发布日期
		$a_data['sstarttime'] = date('Y-m-d H:i:s',$sam['start']);//开始时间
		$a_data['sendtime'] = date('Y-m-d H:i:s',$sam['end']);//结束时间
        
        $adId = M('tad') // 根据临时广告名称查找已确认广告id, 如果找得到就直接赋给样本, 没有的话样本广告id为0
                ->where(array( 'fadname' => ['EQ', $sam['title']], 'is_sure' => ['NEQ', 0], 'fstate'  => ['GT', 0], ))
                ->cache(true,3)
                ->order('fmodifytime desc')
                ->getField('fadid');
		if($adId){
			$a_data['fadid'] = $adId;#广告id
		}else{
			$a_data['fadid'] = 0;
		}
			
		$a_data['favifilename'] = $sam['video_path'];//视频路径
		$a_data['favifilepng'] = $sam['img_path'];//缩略图路径
		$a_data['fadlen'] = floatval($sam['end']) - floatval($sam['start']);//样本时长


		$a_data['fcreator'] = 'Ai_'.$sam['creator'];//创建人
		$a_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$a_data['fmodifier'] = 'Ai_'.$sam['creator'];//修改人
		$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$a_data['tem_ad_name'] = $sam['title'];//临时广告名称
		$a_data['fversion'] = $sam['ver_info']; #版本说明
		$a_data['fspokesman'] = $sam['ad_speaker']; #代言人
		
		$a_data['uuid'] = $sam['uuid'];//唯一ID
		if($a_data['fadlen'] < 3 || $a_data['fadlen'] > 86400){//判断时长是否合法
			$dmpLogs2 = A('Common/AliyunLog','Model')->dmpLogs2('sample_adlen_error',$a_data);		
			return false;
		}
			
		$add_sam_id = M('t'.$tb.'sample')->add($a_data);//添加数据
		if($add_sam_id) S($cache_key,$add_sam_id,60); #如果有新增成功，则存入缓存
		
	
		$taskid = A('Common/InputTask','Model')->add_task(intval($mediaInfo['media_class']),$add_sam_id);//添加到任务表
				

		

		return $add_sam_id;
	   
	   
   }
   
}