<?php
//广告管理控制器
namespace Admin\Controller;
use Think\Controller;
class IllegalController extends BaseController {
	
	
	

    public function index(){
		
		if(A('Admin/Authority','Model')->authority('200191') === 0){
			exit('没有权限');//验证是否有新增权限
		}
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
		$this->assign('IllegalList',$IllegalList);//违法表现数据列表
		$this->display();
	}
	
	public function ajax_illegal_details(){
		$fcode = I('fcode');//获取违法表现code
		$IllegalDetails = M('tillegal')->field('tillegal.*,tillegaltype.fillegaltype')->join('tillegaltype on tillegaltype.fcode = tillegal.fillegaltype')->where(array('tillegal.fcode'=>$fcode))->find();//根据违法表现code查询详细信息
		
		$this->ajaxReturn(array('code'=>0,'IllegalDetails'=>$IllegalDetails));//ajax返回
	}
	

}