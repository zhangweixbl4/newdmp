<?php
namespace InputPc\Controller;
use Think\Controller;
class PaperSourceController extends BaseController {
	
    public function index(){
		//var_dump(file_up());
		if(	!A('InputPc/Task','Model')->user_other_authority('1004')){
			exit('无权限');
		}
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->display();
	}

	//获取文件上传的参数
	public function get_uploadtoken(){
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>file_up()));
	}

	/*查询报纸最近上传版面、版面类型、版面等信息*/
	public function paper_prompt(){
		$wx_id = intval(session('wx_id'));
		$mediaId = I('mediaId');//媒介id
		if(empty($mediaId)){
			$this->ajaxReturn(array(
				'last_issuedate'=>'',
				'last_page'=>'',
				'pageList'=>[],
				'allpageList'=>[],
				'pagetypeList'=>[],
				'allpagetypeList'=>[]
			));
		}
		$last_page = M('tpapersource')
									->field('fissuedate,fpage')
									->where(array('fmediaid'=>$mediaId))
									->order('fid desc')
									->find();//查询最后上传的一个版面
		$pageList = M('tpapersource')->cache(true,60)
								->where(array('fmediaid'=>$mediaId,'fcreatorid'=>$wx_id))
								->group('fpage')
								->order('fpage asc')
								->getField('fpage',true);//本人上传的版面编号
		if(!$pageList) $pageList = array();	
		$allpageList = M('tpapersource')->cache(true,60)
								->where(array('fmediaid'=>$mediaId,'fissuedate'=>array('gt',date('Y-m-d',time()-86400*60))))
								->group('fpage')
								->order('fpage asc')
								->getField('fpage',true);//最近二个月上传的版面编号
		if(!$allpageList) $allpageList = array();	

		$pagetypeList = M('tpapersource')->cache(true,60)
								->where(array('fpagetype'=>array('neq',''),'fmediaid'=>$mediaId,'fcreatorid'=>$wx_id))
								->group('fpagetype')
								->order('fpagetype asc')
								->getField('fpagetype',true);//本人上传的版本类型
		if(!$pagetypeList) $pagetypeList = array();
		$allpagetypeList = M('tpapersource')->cache(true,60)
								->where(array('fpagetype'=>array('neq',''),'fmediaid'=>$mediaId,'fissuedate'=>array('gt',date('Y-m-d',time()-86400*60))))
								->group('fpagetype')
								->order('fpagetype asc')
								->getField('fpagetype',true);//查询最近二个月上传的版本类型
		if(!$allpagetypeList) $allpagetypeList = array();	

		$fissuedate = $last_page['fissuedate']?date('Y-m-d',strtotime($last_page['fissuedate'])):'暂无上传记录';
		$fpage = $last_page['fpage']?$last_page['fpage']:'';
		$this->ajaxReturn(array(
								'last_issuedate'=>$fissuedate,
								'last_page'=>$fpage,
								'pageList'=>$pageList,
								'allpageList'=>$allpageList,
								'pagetypeList'=>$pagetypeList,
								'allpagetypeList'=>$allpagetypeList
								
								));
		
	}

	//获取自己常用的媒体列表
	public function get_media_list(){
		$wx_id = intval(session('wx_id'));
		
		$mediaList = M('tpapersource')
	        ->alias('a')
	        ->field('b.fid,b.fmedianame')
	        ->join('tmedia b on a.fmediaid = b.fid')
			->where(array('a.fcreatorid'=>$wx_id))
			->group('a.fmediaid')
			->order('count(1) desc,b.fmedianame asc')
			->select();
		$this->ajaxReturn(array('code'=>0,'value'=>$mediaList));
	}
	
	public function add_papersource_bak(){
		if(	!A('InputPc/Task','Model')->user_other_authority('1004')){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
		}
		$wx_id = intval(session('wx_id'));
		$userInfo = M('ad_input_user')->where(array('wx_id'=>$wx_id))->find();
		$fmediaid = I('fmediaid');//媒介id
		$fissuedate = date('Y-m-d',strtotime(I('fissuedate')));//发布日期
		$fpage = I('fpage');//版面
		$fpagetype = I('fpagetype');//版面类型
		$furl = I('furl');//存储位置
        $noad = I('noad'); // 是否有广告
		
		$mediaInfo = M('tmedia')->where(array('fid'=>$fmediaid))->find();//查询是否存在该媒体
		if(!$mediaInfo) $this->ajaxReturn(array('code'=>-1,'msg'=>'添加或修改数据失败,媒体ID有错误'));
		if($furl == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'添加或修改数据失败,储存位置不能为空'));
		if($noad == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'请选择是否有广告'));

		
		$a_e_data['fmediaid'] = $fmediaid;//媒体ID
		$a_e_data['fissuedate'] = $fissuedate;//发布日期
		$a_e_data['fpage'] = $fpage;//版面
		
		
		if(M('tpapersource')->where($a_e_data)->count() > 0){//判断是否存在数据
			$this->ajaxReturn(array('code'=>-1,'msg'=>'请勿重复添加'));
		}
		
		$a_e_data['fpagetype'] = $fpagetype;//版面类型
		$a_e_data['furl'] = $furl;//存储位置
		$a_e_data['noad'] = $noad;
		$a_e_data['fcreator'] = $userInfo['nickname'];//创建人
		$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$a_e_data['fmodifier'] = $userInfo['nickname'];//修改人
		$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		
		$sourceId = M('tpapersource')->add($a_e_data);//创建
		if($sourceId > 0){
            $logData = [
                'data' => $a_e_data,
                'sourceId' => $sourceId,
                'type' => 'add',
                'msg' => '添加成功'
            ];
            A('Common/AliyunLog','Model')->paper_source_log($logData);
			M('tmedia')->where(array('fid'=>$fmediaid))->save(array('fsort'=>array('exp','fsort+1')));//修改媒介排序
			A('InputPc/Task','Model')->change_task_count($wx_id,'paper_page_up',1);//增加上传版面数量
			$this->ajaxReturn(array('code'=>0,'msg'=>$mediaInfo['fmedianame'].'_'.$fissuedate.'_'.$fpage.'添加成功'));
		}else{
            $logData = [
                'data' => $a_e_data,
                'sourceId' => $sourceId,
                'type' => 'add',
                'msg' => '添加失败'
            ];
            A('Common/AliyunLog','Model')->paper_source_log($logData);
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));
		}
		

	}

	public function get_papersourcelist(){
		if(	!A('InputPc/Task','Model')->user_other_authority('1004')){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
		}
		$fmediaid = I('fmediaid');//媒介id
		$fissuedate = date('Y-m-d',strtotime(I('fissuedate')));//发布日期

		$mediaInfo = M('tmedia')->where(array('fid'=>$fmediaid))->find();//查询是否存在该媒体
		if(!$mediaInfo) $this->ajaxReturn(array('code'=>-1,'msg'=>'媒体ID有误'));
		if(empty($fissuedate)) $this->ajaxReturn(array('code'=>-1,'msg'=>'时间选择有误'));

		$a_e_data['fmediaid'] = $fmediaid;//媒体ID
		$a_e_data['fissuedate'] = $fissuedate;//发布日期
		$pse_list = M('tpapersource') -> field('fpage,fpagetype,furl,noad,fcreatetime,fid') -> where($a_e_data) -> order('fpage asc') -> select();
		if(empty($pse_list)){
			//如果未找到上传记录，则读取上周同一星期的上传模板
			$predate = date("Y-m-d",strtotime("-7 day",strtotime($fissuedate)));
			$a_e_data['fissuedate'] = $predate;//发布日期
			$pse_list = M('tpapersource') -> field('fpage,fpagetype,"" furl,noad') -> where($a_e_data) -> order('fpage asc') -> select();
			if(empty($pse_list)){
				//如果未找到上调上传记录，则读取前一天的上传模板
				$predate = date("Y-m-d",strtotime("-1 day",strtotime($fissuedate)));
				$a_e_data['fissuedate'] = $predate;//发布日期
				$pse_list = M('tpapersource') -> field('fpage,fpagetype,"" furl,noad') -> where($a_e_data) -> order('fpage asc') -> select();
			}
		}
		$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$pse_list?$pse_list:[]]);
	}

	public function del_papersource(){
		if(	!A('InputPc/Task','Model')->user_other_authority('1004')){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
		}
		$wx_id = intval(session('wx_id'));
		$userInfo = M('ad_input_user')->where(array('wx_id'=>$wx_id))->find();
		$fid = I('fid');//素材ID
		$del_pse = M('tpapersource') -> where(['fid' => $fid,'fstate' => 0,'_string' => '(unix_timestamp(now())-unix_timestamp(fcreatetime))<=1200']) ->delete();
		if(!empty($del_pse)){
			A('InputPc/Task','Model')->change_task_count($wx_id,'paper_page_up',-1);//增加上传版面数量
			// 扣减积分
			$taskInfo = [
				'wx_id'       => $wx_id,
				'taskid'      => $fid,
				'update_time' => time()
			];
			A('TaskInput/TaskInputScore','Model')->minusScoreByUploadPaper($taskInfo);
			$this->ajaxReturn( array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn( array('code'=>-1,'msg'=>'删除失败，记录不存在'));
		}
	}

	public function add_papersource(){
		if(	!A('InputPc/Task','Model')->user_other_authority('1004')){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
		}
		$wx_id = intval(session('wx_id'));
		$userInfo = M('ad_input_user')->where(array('wx_id'=>$wx_id))->find();
		$fmediaid = I('fmediaid');//媒介id
		$fissuedate = date('Y-m-d',strtotime(I('fissuedate')));//发布日期
		$data = I('data');//版面数据
		
		$mediaInfo = M('tmedia')->where(array('fid'=>$fmediaid))->find();//查询是否存在该媒体
		if(!$mediaInfo) $this->ajaxReturn(array('code'=>-1,'msg'=>'媒体ID有误'));

		$adddata = [];
		foreach ($data as $key => $value) {
			if(!empty($value['fpage']) && !empty($value['furl'])){
				// if(empty($value['fpagetype'])) $this->ajaxReturn(array('code'=>-1,'msg'=>'版面'.$value['fpage'].'未选择版面类别'));
				if($value['noad'] === '') $this->ajaxReturn(array('code'=>-1,'msg'=>'版面'.$value['fpage'].'未选择有无广告'));
				$adddata[] = ['fpage' => $value['fpage'],'fpagetype' => $value['fpagetype'],'furl' => $value['furl'],'noad' => $value['noad']];
			}
		}

		foreach ($adddata as $key => $value) {
			$a_e_data = [];
			$where_pse['fmediaid'] = $fmediaid;
			$where_pse['fissuedate'] = $fissuedate;
			$where_pse['fpage'] = $value['fpage'];

			$a_e_data['fmediaid'] = $fmediaid;//媒体ID
			$a_e_data['fissuedate'] = $fissuedate;//发布日期
			$a_e_data['fpage'] = $value['fpage'];//版面
			$a_e_data['fpagetype'] = $value['fpagetype'];//版面类型
			$a_e_data['furl'] = $value['furl'];//存储位置
			$a_e_data['noad'] = $value['noad'];
			$a_e_data['fmodifier'] = $userInfo['nickname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$psedata = M('tpapersource')->field('fcreatetime,fid')->where($where_pse)->find();
			if(!empty($psedata)){//判断是否存在数据
				if((time()-strtotime($psedata['fcreatetime']))<=1200){
					$sourceId = M('tpapersource') -> where(['fid' => $psedata['fid']]) -> save($a_e_data);//保存
					if(!empty($sourceId)){
						$logData = [
			                'data' => $a_e_data,
			                'sourceId' => $psedata['fid'],
			                'type' => 'edit',
			                'msg' => '修改成功'
			            ];
			            A('Common/AliyunLog','Model')->paper_source_log($logData);
					}
				}
			}else{
				$a_e_data['fcreator'] = $userInfo['nickname'];//创建人
				$a_e_data['fcreatorid'] = $wx_id;//创建人ID
				$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
				$sourceId = M('tpapersource')->add($a_e_data);//创建

				if($sourceId > 0){
					// 计入积分
					$taskInfo = [
						'wx_id'       => $wx_id,
						'taskid'      => $sourceId,
						'update_time' => time(),
						'info'        => $a_e_data
					];
					A('TaskInput/TaskInputScore','Model')->addScoreByUploadPaper($taskInfo);
					// 日志
		            $logData = [
		                'data' => $a_e_data,
		                'sourceId' => $sourceId,
		                'type' => 'add',
		                'msg' => '添加成功'
		            ];
		            A('Common/AliyunLog','Model')->paper_source_log($logData);
					M('tmedia')->where(array('fid'=>$fmediaid))->save(array('fsort'=>array('exp','fsort+1')));//修改媒介排序
					A('InputPc/Task','Model')->change_task_count($wx_id,'paper_page_up',1);//增加上传版面数量
				}else{
		            $logData = [
		                'data' => $a_e_data,
		                'sourceId' => $sourceId,
		                'type' => 'add',
		                'msg' => '添加失败'
		            ];
		            A('Common/AliyunLog','Model')->paper_source_log($logData);
				}
			}
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>$mediaInfo['fmedianame'].'_'.$fissuedate.'保存成功'));
		
	}
	
	
}