Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}
var vm = new Vue({
  el: '#app',
  data: function () {
    return {
      stateArr,
      adClassArr: adClassArr,
      adClassTree: adClassTree,
      illegalTypeArr: illegalTypeArr,
      regionTree: regionTree,
      loading: true,
      where: {
        source_class: ''
      },
      row: {},
      playDialog: false,
      page: 1,
      tableData: [],
      total: 0,
      taskInfo: {},
      searchTempWhere: {},
      selectedRow: [],
      pageSize: 10,
      mediaList: [],
      sourceMediaList: [],
      showChangeDialog: false,
      showTaskDialog: false,
      query: '',
      wxid: '',
      addMedia: {
        fmediaid: '',
        wx_id: '',
      },
      mediaList: [],
      Date: [],
      mediaId: '',
      minDate: null,
      pickerOptions: {
        disabledDate: time => {
          if (this.minDate) {
            let val = this.minDate.getTime()
            return time.getTime() < val || time.getTime() > val + 3600 * 24 * 15 * 1000 || time.getTime() > Date.now()
          } else {
            return time.getTime() > Date.now() || time.getTime() < Date.now() - 3600 * 24 * 30 * 1000
          }
        },
        onPick: (val) => {
          this.minDate = val.minDate
        },
      }
    }
  },
  watch: {
    query(query) {
      let sourceMediaList = this.sourceMediaList
      let _query = query.toLowerCase()
      if (query) {
        this.mediaList = sourceMediaList.filter(v => {
          return (v.fmediaid.indexOf(_query) > -1) || (v.fmedianame.toLowerCase().indexOf(_query) > -1)
        })
      } else {
        this.mediaList = sourceMediaList
      }
    },
    wxid(wxid) {
      let sourceMediaList = this.sourceMediaList
      if (wxid) {
        this.mediaList = sourceMediaList.filter(v => {
          return v.wx_id === wxid
        })
      } else {
        this.mediaList = sourceMediaList
      }
    }
  },
  computed: {
    mediaListLength() {
      return this.sourceMediaList.length
    }
  },
  created: function () {
    this.getLastDate()
    this.sendSearchReq()
  },
  methods: {
    handleConfirm(row) {
      let data = {
        wx_id: row.wx_id,
        fid: row.fid,
      }
      $.post('/Admin/CutTask/change_tqc_media_permission', data).then(res => {
        if (res.code === 0) {
          this.$message.success(res.msg)
          row.edit = false
          row.originalWxid = row.wx_id
        } else {
          this.$message.error(res.msg)
        }
      })
    },
    cancelEdit(row) {
      row.edit = false
      row.wx_id = row.originalWxid
    },
    handleMediaLink() {
      $.post('/Admin/CutTask/tqc_media_permission').then(res => {
        if (res.code === 0) {
          const s = res.mediaList.map(v => {
            this.$set(v, 'edit', false)
            v.originalWxid = v.wx_id
            return v
          })
          this.mediaList = s
          this.sourceMediaList = s
          this.showChangeDialog = true
        }
      })
    },
    getMediaList(query) {
      $.get(`/index.php?m=Admin&c=CutTask&a=get_media_list&media_name=${query}`).then(res => {
        this.mediaList = res.value
      })
    },
    handleAddMedia() {
      $.post('/Admin/CutTask/add_tqc_media_permission', this.addMedia).then(res => {
        if (res.code === 0) {
          this.$message.success('添加成功')
          this.handleMediaLink()
        } else {
          this.$message.error(res.msg)
        }
      })
    },
    handleClose() {
      this.mediaList = []
      this.mediaId = ''
      this.Date = []
    },
    addTask() {
      let [startDate, endDate] = this.Date
      let data = {
        startDate,
        endDate,
        mediaId: this.mediaId
      }
      $.post('/Admin/CutTask/add_task', data).then(res => {
        if (res.code === 0) {
          this.$message.success(res.msg)
          this.handleClose()
          this.showTaskDialog = false
        } else {
          this.$message.error(res.msg)
        }
      })
    },
    getLastDate: function () {
      const arr = []
      let startTime = moment().format('YYYY-MM-') + '01'
      let endTime = moment().format('YYYY-MM-') + moment().daysInMonth()
      arr.push(startTime)
      arr.push(endTime)
      Vue.set(this.where, 'issueDate', arr)
    },
    search: function () {
      this.page = 1
      this.where.onlyThisLevel = this.searchTempWhere.onlyThisLevel ? 1 : 0
      if (this.searchTempWhere.region && this.searchTempWhere.region.length !== 0) {
        this.where.regionId = this.searchTempWhere.region[this.searchTempWhere.region.length - 1]
      } else {
        this.where.regionId = ''
      }

      this.sendSearchReq()
    },
    sendSearchReq: function () {
      this.loading = true
      $.post('', {
        where: this.where,
        page: this.page,
        pageSize: this.pageSize,
      }).then((res) => {
        this.tableData = res.table
        this.total = +res.total
        this.loading = false
      })
    },
    resetForm: function () {
      this.page = 1
      this.where = {}
      this.searchTempWhere = {}
      this.getLastDate()
      this.sendSearchReq()
    },
    pageChange: function (page) {
      this.page = page
      this.sendSearchReq()
    },
    tableRowClassName: function (row) {

    },
    searchAlias: function (queryString, cb) {
      $.post("/Api/User/searchUserByAlias", {
        keyword: queryString
      }, function (res) {
        cb(res.data);
      });
    },
    searchMediaLabel: function (queryString, cb) {
      $.post("/Api/Label/searchMediaLabel", {
        keyword: queryString
      }, function (res) {
        cb(res);
      });
    },
    selectRow: function (val) {
      this.selectedRow = val;
    },
    pageSizeChange: function (val) {
      this.pageSize = val
      this.search()
    },
    unlockTask: function () {
      if (this.selectedRow.length === 0) {
        return this.$alert('请至少选择一行')
      } else {
        this.$confirm('确认解锁这' + this.selectedRow.length + '项任务?').then(() => {
          this.loading = true
          const ids = this.selectedRow.map((v) => v.cut_task_id).join(',')
          $.post('unlockCutTask', {
            ids,
          }).then((res) => {
            this.loading = false
            if (res.code == 0) {
              this.$message.success(res.msg)
            } else {
              this.$message.error(res.msg)
            }
            this.sendSearchReq()
          })
        })
      }
    },
    deleteTask: function () {
      if (this.selectedRow.length === 0) {
        return this.$alert('请至少选择一行')
      } else {
        this.$confirm('确认删除这' + this.selectedRow.length + '项任务?').then(() => {
          this.loading = true
          const ids = this.selectedRow.map((v) => v.cut_task_id).join(',')
          $.post('deleteCutTask', {
            ids,
          }).then((res) => {
            this.loading = false
            if (res.code == 0) {
              this.$message.success(res.msg)
            } else {
              this.$message.error(res.msg)
            }
            this.sendSearchReq()
          })
        })
      }
    },
    changePriority: function () {
      if (this.selectedRow.length === 0) {
        return this.$alert('请至少选择一行')
      } else {
        this.$prompt('请输入设定的优先级', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          inputPattern: /\d/,
          inputErrorMessage: '请输入设定的优先级(数字)'
        }).then((val) => {
          this.loading = true
          const ids = this.selectedRow.map((v) => v.cut_task_id).join(',')
          $.post('changeCutTaskPriority', {
            ids,
            priority: val.value
          }).then((res) => {
            this.loading = false
            if (res.code == 0) {
              this.$message.success(res.msg)
            } else {
              this.$message.error(res.msg)
            }
            this.sendSearchReq()
          })
        })
      }
    },
    checkTaskFlow: function (row) {
      layer.open({
        title: '任务流程',
        type: 2,
        area: ['60%', '60%'],
        content: 'checkCutTaskFlow?cut_task_id=' + row.cut_task_id
      })
    },
    changeState: function () {
      if (this.selectedRow.length === 0) {
        return this.$alert('请至少选择一行')
      }
      this.$prompt('请输入状态?', '提示', {
        confirmButtonText: '提交',
        cancelButtonText: '取消',
        inputPattern: /^-?\d+$/,
        inputErrorMessage: '必须是数字'
      }).then((value) => {
        this.loading = true
        $.post('changeState', {
          ids: this.selectedRow.map((v) => v.cut_task_id).join(','),
          state: value.value
        }).then((res) => {
          this.loading = false
          if (res.code == 0) {
            this.$message.success(res.msg)
          } else {
            this.$message.error(res.msg)
          }
          this.sendSearchReq()
        })
      }).catch(() => {})
    }
  },
})