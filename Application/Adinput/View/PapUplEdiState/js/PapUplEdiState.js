Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}
const vm = new Vue({
  el: "#app",
  data() {
    return {
      form: {
        page_num: 1,
        page_size: 20,
        export_excel: 0,
        month: moment(new Date()).format('YYYY-MM'),
      },
      loading: false,
      allcount: 0,
      downBtn: false,
      tableData: [],
      tableHeight: '400px',
      printedNum: true,
      adNum: true,
      is_include_sub: false,
      regionTree: regionTree,
      show: false,
      durl: '',
    }
  },
  created() {
    this.getList(1,1)
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight()
      document.querySelector('#app').style.opacity = 1
    })
    window.onresize = () => {
      this.getTableHeight()
    }
  },
  methods: {
    getList(page,val) {
      if(page) this.form.page_num = page
      let data = {
        ...this.form
      }
      if(val == 2) {
        data.export_excel = 1
        this.show = true
      }
      if (this.form.region && this.form.region.length !== 0) {
        data.region = this.form.region[this.form.region.length - 1]
       }
      if(this.is_include_sub) {
        data.is_include_sub = 0
      } else {
        data.is_include_sub = 1
      }

      this.loading = true
      this.downBtn = true
      $.post('/Adinput/PapUplEdiState/apUplEdiState', data)
      .then(res => {
        if(res.code == 0){
          if(val == 1) {
            this.tableData = res.data.list
            this.allcount = res.data.total
          } else {
            this.durl = res.data
          }
          this.loading = false
          this.downBtn = false
        } else {
          this.$message.error(res.msg)
          this.show = false
        }
      })
      .catch(err=> {
        this.$message.error(err)
        this.show = false
      })
    },
    getTableHeight() {
      let formHeight = this.$refs.sform.$el.clientHeight
      this.tableHeight = document.body.clientHeight - formHeight - 120 + 'px'
    },
  },
})