<?php
namespace Api\Controller;
use Think\Controller;

class SeperatetaskController extends Controller {
	
	/*添加任务，来自上传文件的回调*/
	public function add_task(){
		$post_data = file_get_contents('php://input');
		//file_put_contents('LOG/add_task',$post_data."\n",FILE_APPEND);//写入记录文件，调试时使用
		$data = json_decode($post_data,true);
		$up_code = intval($data['up_code']);//上传成功状态码
		$media_id = $data['media_id'];//媒介ID
		$object_url = $data['object_url'];//云上的文件URL
		$issuedate = date('Y-m-d',strtotime($data['issuedate']));//发布日期

		if($up_code < 200 || $up_code > 299){
			if($post_data != '') file_put_contents('LOG/海康文件上传错误日志',date('Y-m-d H:i:s')."	".$post_data."	\n",FILE_APPEND);//写入记录文件
		}else{
			$a_e_data = array();
			$a_e_data['fmediaid'] = $media_id;//媒体ID
			$a_e_data['fissuedate'] = $issuedate;//发布日期
			
			if(strstr($object_url,'.reco')) $a_e_data['frecfilename'] = $object_url;//如果地址里面包含.reco
			if(strstr($object_url,'.avi')) $a_e_data['favifilename'] = $object_url;//如果地址里面包含.avi
			
			$a_e_data['fstarttime'] = date('Y-m-d H:i:s',strtotime($issuedate)+0);//发布开始时间
			$a_e_data['fendtime'] = date('Y-m-d H:i:s',strtotime($issuedate)+86399);//发布结束时间
			
			$a_e_data['fstate'] = -1;
			
			$taskInfo = M('tseperatetask')->where(array('fissuedate'=>$issuedate,'fmediaid'=>$media_id))->find();//查询有没有相同记录
			if($taskInfo['frecfilename'] != '' && $taskInfo['favifilename'] != '') exit('SUCCESS');//如果已经有reco和avi就退出
			if($taskInfo){
				$a_e_data['fmodifier'] = get_client_ip();//修改人
				$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
				M('tseperatetask')->where(array('fseperatetaskid'=>$taskInfo['fseperatetaskid']))->save($a_e_data);//修改记录
			}else{
				$a_e_data['fcreator'] = get_client_ip();//创建人
				$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
				$a_e_data['fmodifier'] = get_client_ip();//修改人
				$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
				M('tseperatetask')->add($a_e_data);//添加记录
			}
		}
		$taskInfo = M('tseperatetask')->where(array('fissuedate'=>$issuedate,'fmediaid'=>$media_id))->find();//查询有没有相同记录
		if($taskInfo['frecfilename'] != '' && $taskInfo['favifilename'] != ''){
			M('tseperatetask')->where(array('fseperatetaskid'=>$taskInfo['fseperatetaskid']))->save(array('fstate'=>0));//修改记录
		}

		exit('SUCCESS');

	}
	
}