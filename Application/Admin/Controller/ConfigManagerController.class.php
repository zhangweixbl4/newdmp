<?php
namespace Admin\Controller;
use Think\Exception;

class ConfigManagerController extends BaseController
{
    private $menuList = [
        [
            'name' => '剪辑错误管理',
            'url' => '/Admin/CutErrorReason/index'
        ],
        [
            'name' => '系统配置管理',
            'url' => '/Admin/ConfigManager/sysConfig'
        ],
    ];
    public function _initialize()
    {
        parent::_initialize();
        if(A('Admin/Authority','Model')->authority('300991') === 0){
            exit('没有权限');
        }
    }

    public function index()
    {
        $this->assign([
            'menuList' => $this->menuList
        ]);
        $this->display();
    }

    public function sysConfig()
    {
        if (IS_POST){
            $table = M('sys_config')->select();
            foreach ($table as $key => $value) {
                $table[$key]['type'] = 'show';
            }
            $this->ajaxReturn([
                'table' => $table
            ]);
        }else{
            $this->display();
        }
    }

    public function postSysConfig()
    {
        $data = I('post.');
        if (!$data['fkey']){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '请添加配置键名'
            ]);
        }
        if (!$data['fid']){
            try{
                $res = M('sys_config')->add($data);
            }catch (Exception $error){
                $res = false;
            }
        }else{
            unset($data['fkey']);
            $res = M('sys_config')->where(['fid' => ['EQ', $data['fid']]])->save($data);
        }
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '更新成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '更新失败'
            ]);
        }
    }


}