Vue.prototype.$ELEMENT = { size: 'mini', zIndex: 3000 }

const rules = {
  fname: [
    { required: true, message: '请输入名称', trigger: 'blur' },
  ],
  fcode: [
    { required: true, message: '请输入编码', trigger: 'blur' },
  ],
  fisadmin: [
    { required: true, message: '请选择角色', trigger: 'blur' },
  ],
  fkind: [
    { required: true, message: '请选择组织机构类型', trigger: 'blur' },
  ],
}

const handlePeopleMethods = {
  handleDeleteConfirm(fid) {
    $.post('/Admin/Regulatorperson/ajax_regulatorperson_del', { 'fid': fid }, res => {
      if (res.code === 0) {
        this.$message.success(res.msg)
        this.getRegulatorpersonList(this.currentRegionid)
      } else {
        this.$message.error(res.msg)
      }
    })
  },
  resetPeoplePermission() {
    this.mediaList = []
    this.selectedMedia = []
  },
  handleDialogClose() {
    this.mediaPermission = false
    this.orgMediaPermission = false
    this.orgMediaList = []
    this.selectedOrgMedia = []
    this.resetMediaSearch()
    this.resetPeoplePermission()
  },
  resetMediaSearch() {
    this.mediaQuery = {
      this_region_id: false,
      s_media_class: ''
    }
    this.s_regionid = []
  },
  handleConfirmPermission() {
    let data = {
      medialist: this.selectedMedia,
      fid: this.currentPeople.fid,
      fcustomer: this.fregionid,
    }
    $.post('/Admin/Regulatorperson/addpersonmedia', data, res => {
      if (res.code === 0) {
        this.$message.success(res.msg)
        this.setCustomer()
        this.handleDialogClose()
      } else {
        this.$message.error(res.msg)
      }
    })
  },
  getRegulatorpersonList(fid) {
    $.post("/Admin/Regulatorperson/ajax_regulatorperson_list", { "fregulatorid": fid }, res => {
      this.regulatorpersonList = res.regulatorpersonList.map(item => {
        item.visible = false
        return item
      })
    })
  },
  handlePermission(row) {
    this.currentPeople = row
    if (!this.customerList.length) {
      this.getCustomerList()
    }
    this.getMediaList()
    this.mediaPermission = true
  },
  handleEditDialogClose() {
    try{
      this.$refs.peopleInfoForm.resetFields()
      this.peopleInfo = {
        fcustomers: []
      }
      this.peopleEdit = false
    }
    catch (e) { }
    try {
      this.$refs.orgInfoForm.resetFields()
      this.orgInfo = {}
      this.orgEdit = false
      this.isOrgEdit = false
      this.regionid = []
    }
    catch (e) { }
  },
  handleConfirmPeople() {
    this.$refs.peopleInfoForm.validate((valid) => {
      if (valid) {
        let data = {
          fregulatorid: this.regulatorDetails.fid,
          ...this.peopleInfo
        }
        $.post('/Admin/Regulatorperson/add_edit_regulatorperson', data, res => {
          if (res.code === 0) {
            this.$message.success(res.msg)
            this.handleEditDialogClose()
            this.getRegulatorpersonList(this.currentRegionid)
          } else {
            this.$message.error(res.msg)
          }
        })
      }
    })
  },
  handleEditPeople(row) {
    this.peopleEdit = true
    $.post('/Admin/Regulatorperson/ajax_regulatorperson_details', { fid: row.fid }, res => {
      if (res.code === 0) {
        this.peopleInfoField.forEach(item => {
          if (item === 'personlabel') {
            this.$set(this.peopleInfo,'fcustomers',res.regulatorpersonDetails[item])
          } else {
            this.$set(this.peopleInfo,item,res.regulatorpersonDetails[item])
          }
        })
      }
    })
  },
  showMenuDialog(row,isOrgMenu) {
    this.menuDialog = true
    this.isOrgMenu = isOrgMenu
    if (this.menuTypeList.length === 0) {
      this.getMenuType()
    }
    let menuType = localStorage.getItem('menuType')
    this.menutype = localStorage.getItem('menuType') || '20'
    if (menuType) {
      this.menutype = menuType
    } else {
      this.menutype = '20'
      this.setMenuType('20')
    }
    if (row) {
      this.currentPeople = row
      this.getPersonMenuList(row.fid)
    } else {
      this.getOrgMenuList()
    }
  },
  checkChange(checked, data) {
    let selid = []
    let selstate = data.checkedKeys.includes(checked.menu_id) ? 1 : 0
    if (checked.parent_id === '0') {
      selid.push({
        fmenuid: checked.menu_id,
        fmenupid: checked.parent_id,
        selstate: selstate
      })
      if (checked.children) {
        let { children } = checked
        children.forEach(i => {
          selid.push({
            fmenuid: i.menu_id,
            fmenupid: i.parent_id,
            selstate: selstate
          })
        })
      }
    } else {
      selid.push({
        fmenuid: checked.menu_id,
        fmenupid: checked.parent_id,
        selstate: selstate
      })
    }
    let _data = {
      menutype: this.menutype,
      selid: selid,
      fcustomer: this.fregionid
    }
    if (this.isOrgMenu) {
      _data.fregulatorid = this.regulatorDetails.fid
    } else {
      _data.fpersonid = this.currentPeople.fid
    }
    let url = this.isOrgMenu ? '/Admin/Regulator/updatetremenu' : '/Admin/Regulatorperson/updatepersonmenu'
    $.post(url, _data).then(res => {
      if (res.code === 0) {
        this.$message.success(res.msg)
      } else {
        this.$message.error(res.msg)
      }
    })
  },
  getMenuType() {
    $.post('/Admin/Regulator/get_menu_type').then(res => {
      if (res.code === 0) {
        this.menuTypeList = res.data
      } else {
        this.$message.error(res.msg)
      }
    })
  },
  getOrgMenuList() {
    let data = {
      menutype: this.menutype,
      fid: this.regulatorDetails.fid,
      fcustomer: this.fregionid,
    }
    this.setCustomer()
    $.post('/Admin/Regulator/tremenulist', data).then(res => {
      if (res.code === 0) {
        this.menuList = res.menulist || []
        this.personMenuList = res.tremenulist || []
      }
    })
  },
  handleMenuCustomerChange() {
    if (this.isOrgMenu) {
      this.getOrgMenuList()
    } else {
      this.getPersonMenuList()
    }
  },
  getPersonMenuList(fid) {
    let data = {
      personid: fid || this.currentPeople.fid,
      menutype: this.menutype,
      fid: this.regulatorDetails.fid,
      fcustomer: this.fregionid,
    }
    this.setCustomer()
    $.post('/Admin/Regulatorperson/get_personmenulist', data).then(res => {
      if (res.code === 0) {
        this.menuList = res.menulist || []
        this.personMenuList = res.personmenulist || []
      }
    })
  },
  setMenuType(val) {
    localStorage.setItem('menuType',val)
  },
  menuChange(val) {
    this.setMenuType(val)
    if (this.isOrgMenu) {
      this.getOrgMenuList()
    } else {
      this.getPersonMenuList()
    }
  }
}
const handleOrgMethods = {
  // 媒介关联相关
  handleTransferChange(all,to,selected) {
    if (to === 'left') {
      this.$confirm('此操作删除关联媒体下的所有数据, 是否继续?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        this.deleteMediaPremission(selected)
      }).catch(() => {
        this.selectedOrgMedia = [...this.selectedOrgMedia,...selected]
      })
    } else {
      this.addMediaPremission(selected)
    }
  },
  addMediaPremission(id) {
    let data = {
      fregulatorcode: this.currentRegionid,
      fmediaid: id,
      fcustomer: this.fregionid,
    }
    this.loading = true
    $.post('/Admin/Regulator/add_correlation_media',data,res => {
      if (res.code === 0) {
        this.$message.success(res.msg)
        this.getHaveOrgPermission()
      }else{
        this.$message.error(res.msg)
      }
      this.loading = false
    })
  },
  deleteMediaPremission(id) {
    let data = {
      fregulatorcode: this.currentRegionid,
      fmediaid: id,
      fcustomer: this.fregionid,
    }
    this.loading = true
    $.post('/Admin/Regulator/del_correlation_media',data,res => {
      if (res.code === 0) {
        if ( this.mediaQuery.s_media || this.s_regionid.length || this.mediaQuery.medialabel || this.mediaQuery.s_media_class) {
          this.handleSearchMedia()
        } else {
          this.orgMediaList = this.orgMediaList.filter(item => {
            return !id.includes(item.fid)
          })
        }
        this.$message.success(res.msg)
      }else{
        this.$message.error(res.msg)
        this.selectedOrgMedia = [...this.selectedOrgMedia,...id]
      }
      this.loading = false
    })
  },
  handleCustomerChange() {
    this.setCustomer()
    this.getHaveOrgPermission(true)
    this.resetMediaSearch()
  },
  getHaveOrgPermission(clear) {
    let data = {
      fcustomer: this.fregionid,
      fregulatorcode: this.currentRegionid
    }
    this.loading = true
    $.post('/Admin/Regulator/get_correlation_media_list', data, res => {
      if (res.code === 0) {
        let arr = []
        let s = res.regulatormediaList.map(item => {
          let o = {}
          o.regionname = item.regionname
          o.fclass = item.fclass
          o.fid = item.fmediaid
          o.fmedianame = item.fmedianame
          o.fstate = item.fstate
          o.fisoutmedia = item.fisoutmedia
          arr.push(item.fmediaid)
          return o
        })
        arr = Array.from(new Set(arr))
        this.selectedOrgMedia = arr
        let r = []
        if (!clear) {
          r = this.orgMediaList.filter(item => {
            return !arr.includes(item.fid)
          })
        }
        this.orgMediaList = [...r, ...s]
      } else {
        this.$message.error(res.msg)
      }
      this.loading = false
    })
  },
  handleOrgPermission() {
    if (!this.permission.addOrg) return false
    this.getHaveOrgPermission()
    this.orgMediaPermission = true
  },
  handleSearchMedia() {
    let preSelectedOrgMedia = this.selectedOrgMedia
    let preSourceOrgMedia = this.orgMediaList.filter(item => {
      return preSelectedOrgMedia.includes(item.fid)
    })
    let data = {
      ...this.mediaQuery,
      s_regionid: [...this.s_regionid].pop() || ''
    }
    this.loading = true
    $.post('/Api/Media/s_media', data, res => {
      if (res.code === 0) {
        // 过滤搜索结果中已关联的项
        const filterArr = res.mediaList.filter(item => {
          return !preSelectedOrgMedia.includes(item.fid)
        })
        this.orgMediaList = [...filterArr, ...preSourceOrgMedia]
      } else {
        this.$message.error(res.msg)
      }
      this.loading = false
    })
  },
  //添加/编辑机构相关
  handleOrgAddEdit(isedit) {
    if (isedit) {
      this.isOrgEdit = true
      const source = this.regulatorDetails
      this.orgInfoField.forEach(item => {
        if (item === 'regionid') {
          let regionid = source.fregionid
          let sheng = regionid.substring(0,2)
          let shi = regionid.substring(2,4)
          let qu = regionid.substring(4, 6)
          let region = [`${sheng}0000`,`${sheng}${shi}00`,`${sheng}${shi}${qu}`]
          this.$set(this,item,Array.from(new Set(region)))
        } else {
          this.$set(this.orgInfo,item,source[item])
        }
      })
    } else {
      this.isOrgEdit = false
    }
    this.orgEdit = true
  },
  handleOrgAddEditConfirm() {
    this.$refs.orgInfoForm.validate((valid) => {
      if (valid) {
        let data = {
          ...this.orgInfo,
          fregionid: [...this.regionid].pop() || '100000',
          fpid: this.regulatorDetails.fid || '',
          fid: this.isOrgEdit ? this.currentRegionid : '',
        }
        $.post('/Admin/Regulator/add_edit_regulator', data, res => {
          if (res.code === 0) {
            if (this.isOrgEdit) {
              this.getRegulatorDetails(data.fid)
            }
            this.handleEditDialogClose()
            this.$message.success(res.msg)
          } else {
            this.$message.error(res.msg)
          }
        })
      }
    })
  },
  handleStatusUpdate(fstate) {
    let data = {
      fregulatorcode: this.currentRegionid,
      fmediaid: this.rightChecked,
      fcustomer: this.fregionid,
      fstate: fstate,
    }
    $.post('/Admin/Regulator/update_correlation_media_state', data, res => {
      if (res.code === 0) {
        this.orgMediaList = this.orgMediaList.filter(item => {
          return !this.rightChecked.includes(item.fid)
        })
        this.getHaveOrgPermission()
        this.$message.success(res.msg)
      } else {
        this.$message.error(res.msg)
      }
    })
  },
  handleIsoutmediaUpdate(fisoutmedia) {
    let data = {
      fregulatorcode: this.currentRegionid,
      fmediaid: this.rightChecked,
      fcustomer: this.fregionid,
      fisoutmedia: fisoutmedia,
    }
    $.post('/Admin/Regulator/update_correlation_isoutmedia', data, res => {
      if (res.code === 0) {
        this.orgMediaList = this.orgMediaList.filter(item => {
          return !this.rightChecked.includes(item.fid)
        })
        this.getHaveOrgPermission()
        this.$message.success(res.msg)
      } else {
        this.$message.error(res.msg)
      }
    })
  },
  rightCheckChange(selectArr,val) {
    this.rightChecked = selectArr
  }
}


const vm = new Vue({
  el: '#app',
  data() {
    const permission = {
      deletePeople: DELETE_PEOPLE,
      editPeople: EDIT_PEOPLE,
      addPeople: ADD_PEOPLE,
      editOrg: EDIT_ORG,
      addOrg: ADD_ORG,
      deleteOrgMedia: DELETE_ORG_MEDIA,
      addOrgMedia: ADD_ORG_MEDIA,
    }
    const peopleInfoField = ['fid','fcode','fduties','fname','fmobile','fdescribe','fisadmin', 'personlabel']
    const orgInfoField = ['fid','fcode','special_logo','fname','special_home_page','fdescribe','special_brand','special_web_title','fkind','regionid']
    return {
      menuTypeList: [],
      menuList: [],
      personMenuList: [],
      rightChecked: [],
      props: {
        value: 'fid',
        label: 'fname',
        children: 'regionList'
      },
      menuProps: {
        value: 'menu_id',
        label: 'menu_name',
      },
      loading: false,
      mediaPermission: false,
      isOrgMenu: false,
      peopleEdit: false,
      orgEdit: false,
      isOrgEdit: false,
      orgMediaPermission: false,
      fregionid: '',
      regionid: [],
      customerList: [],
      regulatorDetails: {},
      regulatorpersonList: [],
      currentRegionid: '',
      currentPeople: {},
      selectedMedia: [],
      selectedOrgMedia: [],
      preSelectedOrgMedia: [],
      preSourceOrgMedia: [],
      mediaList: [],
      orgMediaList: [],
      peopleInfo: {
        fcustomers: []
      },
      orgInfo: {},
      mediaQuery: {
        this_region_id: false,
        s_media_class: ''
      },
      menutype: '20',
      menuDialog: false,
      s_regionid: [],
      areaTree: AREA,
      permission,
      peopleInfoField,
      orgInfoField,
      rules,
      filterMethod(query, item) {
        return item.fmedianame.toLowerCase().indexOf(query) > -1;
      },
    }
  },
  computed: {
    showCustomer() {
      const o = this.menuTypeList.find(x => {
        return x.me_id === this.menutype
      })
      if (o) {
        return o.me_type === '0' ? false : true
      }
      return false
    }
  },
  methods: {
    getMediaList() {
      this.resetPeoplePermission()
      let data = {
        fid: this.currentPeople.fid,
        fcustomer: this.fregionid,
        fregulatorid: this.regulatorDetails.fid,
      }
      this.loading = true
      $.post('/Admin/Regulatorperson/getpersonmedia', data, res => {
        if (res.code === 0) {
          this.mediaList = res.data.medialist || []
          this.selectedMedia = res.data.personmedialist || []
        } else {
          this.$message.error(res.msg)
        }
        this.loading = false
      })
    },
    getCustomerList() {
      $.get(`/Admin/Regulator/get_customer_list?fregionid=${this.regulatorDetails.fregionid}&fcode=${this.regulatorDetails.fcode}`)
        .then(res => {
          if (res.code === 0) {
            const req = res.region_list
            let last_customer = localStorage.getItem('lastCustomer')
            req.forEach(item => {
              if (item.fid === last_customer) {
                this.fregionid = last_customer
              }
            })
            this.customerList = req
          }
      })
    },
    getRegulatorDetails(fid) {
      $.post("/Admin/Regulator/ajax_regulator_details", { "fid": fid }, res => {
        this.regulatorDetails = res.regulatorDetails
        this.fregionid = res.regulatorDetails.fregionid
        $("#addUser").attr('href','/Admin/Regulatorperson/regulatorperson_details/fregulatorid/'+res.regulatorDetails.fid);//说明
        $("#addUser").addClass('J_menuItem2')
        this.getCustomerList()
      })
    },
    setCustomer() {
      localStorage.setItem('lastCustomer', this.fregionid)
    },
    ...handlePeopleMethods,
    ...handleOrgMethods,
  },
})