Vue.prototype.$ELEMENT = { size: 'mini', zIndex: 3000 }
const vm = new Vue({
    el: '#app',
    components: {
        illegalAdSearch,
    },
    data: function () {
        return {
            loading: false,
            baseCondition: {
                month: '',
                region: '',
                mediaClass: '1',
            },
            regionArr,
            illegalTypeArr,
            adClassArr,
            adClassTree,
            secondLevelRegion: [],
            page: 1,
            pageSize: 10,
            total: 0,
        }
    },
    computed:{
        hasChooseMonthAndRegion: function () {
            return this.baseCondition.month && this.baseCondition.mediaClass
        }
    },
    methods: {
        selectBaseRegion: function (val) {
            this.baseCondition.region = val
            $.post('getSecondLevelRegion', {pid: this.baseCondition.region}).then((res) => {
                if (res.code == 0){
                    this.secondLevelRegion = res.data
                }
            })
            this.$nextTick(() => {
                this.$refs.illegalAdSearch.search()
            })
        },
        searchIllegalAdCount: function () {
            if (this.baseCondition.month && this.baseCondition.mediaClass) {
                this.loading = true
                $.post('checkIllegalAdCountByMonth', {month: this.baseCondition.month, mediaClass: this.baseCondition.mediaClass}).then((res) => {
                    this.loading = false
                    this.regionArr = res.data
					console.log(123)
                })
            }else{
                this.$message('请选择月份和媒体类型')
            }
        },
    }
})