const taskList = {
  template: `
    <div class="task-list"
      v-loading="loading">
      <el-table :data="tableData"
        border>
        <el-table-column type="index"
          width="50">
        </el-table-column>
        <el-table-column prop="duringTime"
          width="200"
          label="抽查时段">
        </el-table-column>
        <el-table-column label="抽查日期">
          <template slot-scope="scope">
            <div>电视：{{scope.row.tv.join(',')}}
            </div>
            <div>广播：{{scope.row.bc.join(',')}}</div>
            <div>报纸：{{scope.row.paper.join(',')}}</div>
          </template>
        </el-table-column>
        <el-table-column label="汇总" width="500px">
          <template slot-scope="scope">
          <div class="flex-box">
            <div class="flex-button"><el-button :disabled="scope.row.progress !== 0 && scope.row.progress !== 100" @click="start(scope.row)">{{scope.row.progress === 100 ? '重新汇总' : '开始汇总数据' }}</el-button></div>
            <div class="flex-progress"><el-progress :text-inside="true" :stroke-width="18" :percentage="scope.row.progress"></el-progress></div>
          </div>
          </template>
        </el-table-column>
		
		 <el-table-column label="汇总标识" >
          <template slot-scope="scope">
          
		  {{scope.row.gather_group}}
          
          </template>
        </el-table-column>
		
		<el-table-column label="抽取数据" width="120px">
          <template slot-scope="scope">
          <div class="flex-box">
            <div class="flex-button"><el-button  @click="chouqu(scope.row)">抽取到客户表</el-button></div>
			
            
          </div>
          </template>
        </el-table-column>
		<el-table-column label="检查违法" width="500px">
          <template slot-scope="scope">
          <div class="flex-box">
            <div class="flex-button"><el-button  @click="to_illegal_ad_issue_review(scope.row)">同步至违法检查</el-button></div>
			
            
          </div>
          </template>
        </el-table-column>
		
		
		
		
		
      </el-table>
      <el-pagination background
        layout="prev, pager, next, total"
        :total="Number(countAll)"
        :page-size="5"
        :current-page="Number(form.page)"
        @current-change="getList">
      </el-pagination>
    </div>
  `,
  data() {
    return {
      form: {
        page: '1',
      },
      tableData: [],
      countAll: '0',
      loading: false,
    
      state: {
        '0': '未确认',
        '2': '已确认',
      },
			
	  
    }
  },
  created() {
    this.getList()
  },
  methods: {
	   to_illegal_ad_issue_review({ fid }) {
        
		let e_time = 0;
		let allow_cancel = true;//是否允许取消
        this.$msgbox({
          title: '同步数据至违法检查',
          message: '',
          showCancelButton: true,
          confirmButtonText: '开始同步',
          cancelButtonText: '取消',
          beforeClose: (action, instance, done) => {
            if (action === 'confirm') {
              allow_cancel = false;//是否允许取消
              instance.confirmButtonLoading = true;
              instance.confirmButtonText = '同步中...';//执行时的按钮文字
			  
			   $.post('/Admin/ZongjuChoucha/to_illegal_ad_issue_review', { fid }).then(res => {
					clearInterval(ti);//清除定时器
					e_time = 0;//重置执行时间
					instance.confirmButtonLoading = false;
					instance.confirmButtonText = '开始同步';
					allow_cancel = true;//是否允许取消
				if (res.code === 0) {
				  
				  instance.message = '已同步完成,'+res.msg;//消息
				  this.$message.success(res.msg)
				} else {
				 
				  
				  instance.message = '同步失败,'+res.msg;//消息
				  this.$message.error(res.msg)
				}
			  }).catch(e => {
				  	clearInterval(ti);//清除定时器
					e_time = 0;//重置执行时间
					instance.confirmButtonLoading = false;
					instance.confirmButtonText = '开始同步';
					allow_cancel = true;//是否允许取消
					instance.message = '同步失败,网络错误';//消息
					this.$message.error('同步失败,网络错误');

			  })
			  
			  let ti = setInterval(() =>{
				  e_time ++;
				  instance.message = '正在同步中...,已耗时'+e_time+'秒';//消息
				  
				  
				  	
			  },1000);
			  
			  
			  
            } else {
				if(allow_cancel == true){
					done();//取消
				}else{
					this.$message.error('不允许取消');//不允许取消
				}	
			  
            }
          }
        });
      },
	  
	  chouqu({ fid }){
		console.log(fid)
		let e_time = 0;
		let allow_cancel = true;//是否允许取消
		this.$msgbox({
          title: '请确认是否开始抽取数据',
          message: '',
          showCancelButton: true,
          confirmButtonText: '开始抽取',
          cancelButtonText: '取消',
          beforeClose: (action, instance, done) => {
			
            if (action === 'confirm') {
				allow_cancel = false;//是否允许取消
				instance.confirmButtonLoading = true;
				instance.confirmButtonText = '正在提交...';//执行时的按钮文字
				$.post('/Admin/ZongjuChoucha/chouqu', { fid }).then(res => {
					
					e_time = 0;//重置执行时间
					instance.confirmButtonLoading = false;
					instance.confirmButtonText = '开始抽取';
					allow_cancel = true;//是否允许取消
					if (res.code === 0) {
						instance.message = '已成功提交,'+res.msg;//消息
						
					} else {
						instance.message = '提交失败,'+res.msg;//消息
						this.$message.error(res.msg)
					}
				}).catch(e => {
				  	
					e_time = 0;//重置执行时间
					instance.confirmButtonLoading = false;
					instance.confirmButtonText = '开始抽取';
					allow_cancel = true;//是否允许取消
					instance.message = '提交失败,网络错误';//消息
					this.$message.error('提交失败,网络错误');

				})
				
				
				
				
				
            } else {
				
				if(allow_cancel == true){
					done();//取消
				}else{
					this.$message.error('不允许取消');//不允许取消
				}	
				
			  
            }
          }
        });
	  },
	 
	  start({ fid,index }) {
		
		let e_time = 0;
		let allow_cancel = true;//是否允许取消
		this.$msgbox({
          title: '请确认是否开始汇总数据',
          message: '',
          showCancelButton: true,
          confirmButtonText: '开始汇总',
          cancelButtonText: '取消',
          beforeClose: (action, instance, done) => {
			
            if (action === 'confirm') {
				allow_cancel = false;//是否允许取消
				instance.confirmButtonLoading = true;
				instance.confirmButtonText = '正在提交汇总...';//执行时的按钮文字
				$.post('/Admin/ZongjuChoucha/start_gather', { fid }).then(res => {
					clearInterval(ti);//清除定时器
					e_time = 0;//重置执行时间
					instance.confirmButtonLoading = false;
					instance.confirmButtonText = '开始汇总';
					allow_cancel = true;//是否允许取消
					if (res.code === 0) {
						instance.message = '已成功提交汇总,'+res.msg;//消息
						
						setTimeout(() => {
							this.checkProgress({ fid, index });//延时5秒查询汇总进度
						},5000)
					} else {
						instance.message = '提交汇总失败,'+res.msg;//消息
						this.$message.error(res.msg)
					}
				}).catch(e => {
				  	clearInterval(ti);//清除定时器
					e_time = 0;//重置执行时间
					instance.confirmButtonLoading = false;
					instance.confirmButtonText = '开始汇总';
					allow_cancel = true;//是否允许取消
					instance.message = '提交汇总失败,网络错误';//消息
					this.$message.error('提交汇总失败,网络错误');

				})
				
				
				
				
				let ti = setInterval(() =>{
				  e_time ++;
				  instance.message = '正在提交汇总...,已耗时'+e_time+'秒';//消息

				},1000);
				
				
            } else {
				
				if(allow_cancel == true){
					done();//取消
				}else{
					this.$message.error('不允许取消');//不允许取消
				}	
				
			  
            }
          }
        });
		
		
		
      
    },
	 

    getList(page) {
      this.loading = true
      this.form.page = page || '1'
      let data = { ...this.form }
      $.post('/Admin/Issue/ccday_list', data).then(res => {
         if (res.code === 0) {
          this.tableData = res.data.list.map((item, index) => {
              this.$set(this,`runTime${index}`,0)
              item.duringTime = `${moment(item.fstarttime).format('YYYY 年 M 月 D 日')} 至 ${moment(item.fendtime).format('D 日')}`
              item.index = index
              item.progress = 0
              this.checkProgress(item)
              return item
            })
            this.countAll = res.data.count
          } else {
            this.$message.error(res.msg)
          }
          this.loading = false
        })
    },
   

    
    checkProgress({ fid,index }) {
      $.post('/Admin/ZongjuChoucha/gather_progress', { fid }).then(res => {
        let { progress } = res
        this.$set(this.tableData[index],'progress',progress)
        if (progress > 0 && progress < 100) {
          this.$set(this, `timer${index}`, setTimeout(() => {
            this.checkProgress({ fid,index })
          },5000))
        }
      })
    }
  }
}