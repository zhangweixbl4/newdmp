<?php
namespace Agp\Controller;
use Think\Controller;

class GadQueryController extends BaseController
{
	/**
	*根据广告名称模糊查询
	*/
	
	public function queryAdList()
	{
		Check_QuanXian(['ggmcchaxun']);
		$ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isrelease = $ALL_CONFIG['isrelease'];
        $ischeck = $ALL_CONFIG['ischeck'];
        $illDistinguishPlatfo = $ALL_CONFIG['illDistinguishPlatfo'];//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
        $media_class = json_decode($ALL_CONFIG['media_class'],true);//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）
	
		$iszjdata = I('iszjdata')?I('iszjdata'):0;//是否总局数据
		if(!empty($iszjdata)){
			$system_num = 100000;
		}

		$fad_class_code   = I('fad_class_code');//广告类别，用于汇总中违法广告数据的查看
	    $fmediaid         = I('fmediaid');//媒体ID，用于汇总中违法广告数据的查看
	    $fmediaownerid    = I('fmediaownerid');//媒体机构ID，用于汇总中违法广告数据的查看
	    $mclass           = I('mclass');//媒体类别，用于汇总中违法广告数据的查看
	    $fad_class_code2  = I('fad_class_code2');//广告类别组，用于汇总中违法广告数据的查看
	    $area 			= I('area');//地域ID，用于汇总中违法广告数据的查看
	    $flevel 		= I('flevel');//地域级别，用于汇总中违法广告数据的查看
	    $is_show_longad   = I('is_show_longad');//是否有长广告，用于汇总中违法广告数据的查看
	    $is_show_fsend    = I('is_show_fsend');//是否包含确认广告，1包含，2不包含，0未定义

	    $years      = I('years');//选择年份
	    $timetypes  = I('timetypes');//选择时间段
	    $timeval    = I('timeval');//选择时间
	    if($is_show_fsend==1){
	      $is_show_fsend = -2;
	    }else{
	      $is_show_fsend = 0;
	    }
	    $where_time = gettimecondition($years,$timetypes,$timeval,'tiai.fissue_date',$is_show_fsend,$isrelease);

		if($system_num == '100000'){
			if(!empty($area)){
				$where['tia.fregion_id'] = $area;
			}else{
				if(session('regulatorpersonInfo.fregulatorlevel')!=30){
					$regionid = session('regulatorpersonInfo.regionid');//机构行政区划ID
					$regionid = substr($regionid , 0 , 2);
					$where['tia.fregion_id'] = array('like',$regionid.'%');
				}
			}
		}else{
			if(!empty($area)){
	        	$where['tia.fregion_id'] = $area;
	        }	
		}

		if(!empty($fmediaid)){
			$where['tia.fmedia_id'] = $fmediaid;
		}

		if(!empty($fmediaownerid)){
			$where['tm.fmediaownerid'] = $fmediaownerid;
		}

		if(!empty($mclass)){
			$where['tia.fmedia_class'] = $mclass;
		}

		if(!empty($fad_class_code)){//广告类别
	     	$where['left(tia.fad_class_code,2)'] = $fad_class_code;
	    }

	    if($is_show_longad === "0"){
	      $where['tia.is_long_ad'] = floatval($is_show_longad);
	    }

	    if(!empty($fad_class_code2)){
	    	$fadclass = [];
	    	foreach ($fad_class_code2 as $key => $value) {
	    		if($value<10){
	    			$fadclass[] = '0'.$value;
	    		}else{
	    			$fadclass[] = $value;
	    		}
	    	}
	    	$where['left(tia.fad_class_code,2)'] = array('in',$fadclass);
	    }

		$ad_name 	= I('name');//广告名
		$start_date = I('start_date');//开始日期
		$end_date 	= I('end_date');//结束日期
		
		$p  = I('page', 1);//当前第几页
        $pp = 20;//每页显示多少记录

		if($ad_name){
			$where['tia.fad_name'] = array('like','%'.$ad_name.'%');
		}
		if($start_date && $end_date){
			$where['tiai.fissue_date'] = array('between',array($start_date,$end_date));
		}

		//是否抽查模式
		if(!empty($ischeck)){
		    $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
		    //如果抽查表有数据
		    if(!empty($spot_check_data)){
		        $dates = [];//定义日期数组
		        foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
		            $year_month = '';
		            $date_str = [];
		            $year_month = substr($spot_check_data_val['fmonth'],0,7);
		            if(!empty($spot_check_data_val['condition'])){
		            	$date_str = explode(',',$spot_check_data_val['condition']);
		            }
		            foreach ($date_str as $date_str_val){
		                $dates[] = $year_month.'-'.$date_str_val;
		            }
		        }
				$where_time 	.= ' and tiai.fissue_date in ("'.implode('","', $dates).'")';
		    }else{
		        $where_time 	.= ' and 1=0';
		    }
		}

		if(!empty($illDistinguishPlatfo)){
		  $where_tia['left(tm.fmediaclassid,2)'] = ['in',$media_class];
		}
		$where['_string'] .= $where_time;

		//国家局系统只显示有打国家局标签媒体的数据
	    if($system_num == '100000'){
	      $wherestr = ' and tn.flevel in (1,2,3)';
	    }

	    if(!empty($flevel)){
	      	$wherestr = ' and tn.flevel ='.$flevel;
	    }

		$count = M('tbn_illegal_ad_issue')
			->alias('tiai')
			->join('tbn_illegal_ad as tia on tiai.fillegal_ad_id = tia.fid and fcustomer="'.$system_num.'"')
			->join('tmedia as tm on tiai.fmedia_id = tm.fid and tm.fid=tm.main_media_id')
			->join('tadclass as tc on tia.fad_class_code = tc.fcode')
			->join('tregion tn on tia.fregion_id=tn.fid'.$wherestr)
			->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on snd.illad_id=tia.fid')
			->where($where)
			->count();

		

		$res = M('tbn_illegal_ad_issue')
			->alias('tiai')
			->field('
				tia.fid as sid,
				tia.fmedia_class,
				tc.ffullname as fadclass,
				tia.fad_name,
				 (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,
				tiai.fissue_date,
				tiai.fstarttime,
				tiai.fendtime,
				tiai.fpage,
				tia.fillegal,
				tia.fexpressions,
				tia.fillegal_code,
				tia.favifilename,
				tia.fjpgfilename,
				(UNIX_TIMESTAMP(tiai.fendtime)-UNIX_TIMESTAMP(tiai.fstarttime)) as difftime
			')
			->join('tbn_illegal_ad as tia on tiai.fillegal_ad_id = tia.fid and fcustomer="'.$system_num.'"')
			->join('tmedia as tm on tiai.fmedia_id = tm.fid  and tm.fid=tm.main_media_id')
			->join('tadclass as tc on tia.fad_class_code = tc.fcode')
			->join('tregion tn on tia.fregion_id=tn.fid'.$wherestr)
			->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on snd.illad_id=tia.fid')
			->where($where)
			->page($p,$pp)
			->select();

			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$res)));
	}
}