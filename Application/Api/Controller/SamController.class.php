<?php
namespace Api\Controller;
use Think\Controller;

class SamController extends Controller {
	
	
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}
	
	
	/*同步样本与发布记录的映射关系*/
	public function sam_relation_issue_tv(){
		set_time_limit(120);
		$marker =intval(A('Common/System','Model')->important_data('sam_relation_issue_tv'));
		
		$samList = M('ttvsample')->where(array('fid'=>array('gt',$marker)))->limit(100)->getField('fid',true);
		
		echo count($samList);
		A('Common/System','Model')->important_data('sam_relation_issue_tv',$samList[count($samList)-1]);
		
		foreach($samList as $sam){
			$issueList = M('ttvissue')->field('fissuedate,fregionid,left(fissuedate,7)')->where(array('ftvsampleid'=>$sam))->group('left(fissuedate,7),left(fregionid,2)')->select();
			//var_dump($issueList);
			foreach($issueList as $issue){
				$table_name = 'ttvissue_'.date('Ym',strtotime($issue['fissuedate'])) . '_' . substr($issue['fregionid'],0,2);
				A('Api/Media','Model')->sam_relation_issue('tv',$sam,$table_name);	
			}
			
		}
		
		
	}
	
	
	
	
	
	/*同步样本与发布记录的映射关系*/
	public function sam_relation_issue_bc(){
		set_time_limit(120);
		$marker =intval(A('Common/System','Model')->important_data('sam_relation_issue_bc'));
		
		$samList = M('tbcsample')->where(array('fid'=>array('gt',$marker)))->limit(100)->getField('fid',true);
		
		echo count($samList);
		A('Common/System','Model')->important_data('sam_relation_issue_bc',$samList[count($samList)-1]);
		
		foreach($samList as $sam){
			$issueList = M('tbcissue')->field('fissuedate,fregionid,left(fissuedate,7)')->where(array('fbcsampleid'=>$sam))->group('left(fissuedate,7),left(fregionid,2)')->select();
			//var_dump($issueList);
			foreach($issueList as $issue){
				$table_name = 'tbcissue_'.date('Ym',strtotime($issue['fissuedate'])) . '_' . substr($issue['fregionid'],0,2);
				A('Api/Media','Model')->sam_relation_issue('bc',$sam,$table_name);	
			}
			
		}
		
		
		
		
	}
	
	
	
	/*同步样本表最后发布日期*/
	public function last_issue_date(){
		session_write_close();
		set_time_limit(1200);


		
		
		$dateInfo = A('Common/System','Model')->important_data('sam_last_issue_date');//获取处理过的日期
		$dateInfo = json_decode($dateInfo,true);//转为数组

		
		
		if(!$dateInfo){//如果是空的
			$date = date('Y-m-d',time() - 86400 * 7);
		}else{

			$date = $dateInfo['date'];//处理月份

		}


		
			if(strtotime($date) >= strtotime(date('Y-m-d'))){//判断日期是否处理到了最近一天
				A('Common/System','Model')->important_data('sam_last_issue_date','{}');
			}else{
				A('Common/System','Model')->important_data('sam_last_issue_date',
																				json_encode(array(
																									'date'=>date("Y-m-d", strtotime($date) + 86400)
																								
																									)
																							)
															);//不是最近一天
			}

		

		A('Api/Sam','Model')->last_issue_date($date);
		
		
	}
	
	
	
	
	
	
	
	
	
	
	

}