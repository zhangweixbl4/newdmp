<?php

namespace Common\Model;
use Think\Exception;



class NetAdModel{
//$netAd['weixin_name'],$netAd['weixin_nickname'],$netAd['weixin_qr_code'],
	public function get_net_media_id($mediaName = '',$source_type = '',$media_class,$field_data=[]){

		if($mediaName == ''){
			return 0;
		}
		$where = array();
		$where['left(fmediaclassid,2)'] = '13';//13为互联网媒介类型
		$where['fmediacode'] = $mediaName;//媒介code
        //微信
        if($source_type == 4){
            $where['fmediacode'] = $field_data['weixin_name'];//媒介code
        }

		$mediaInfo = S('net_media_'.md5($mediaName));//从缓存获取
		if(!$mediaInfo){//如果缓存里面没有
			$mediaInfo = M('tmedia')->master(true)->field('fid')->where($where)->find();//查询媒介
			if($mediaInfo){//如果查询有结果，写入缓存
				S('net_media_'.md5($mediaName),$mediaInfo,86400);//从缓存获取
			}
		}
		if($mediaInfo){//判断有值就返回
			return $mediaInfo['fid'];//返回媒介id
		}else{
		    //新增并返回
			$a_data = array();
            if($source_type == 4){
                $a_data['fmedianame'] = $field_data['weixin_nickname'];//媒介名称
                $a_data['fjpgfilename'] = $field_data['weixin_avatar'];//媒介图标
                $a_data['remark'] = $field_data['weixin_introduce'];//媒介图标
                $a_data['fmediacode'] = $field_data['weixin_name'];
            }else{
                $a_data['fmedianame'] = $mediaName;//媒介名称
                $a_data['fmediacode'] = $mediaName;
            }

            $a_data['fmediaclassid'] = $media_class;
			$a_data['fdeviceid'] = 0;
			$a_data['fchannelid'] = 0;
			$a_data['fmediaownerid'] = 0;
			$a_data['fcollecttype'] = 30;
			$a_data['fcreator'] = '接口创建';//创建人
			$a_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_data['fmodifier'] = '接口创建';//修改人
			$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			$mediaId = M('tmedia')->add($a_data);
			M('tmedia')->where(['fid'=>$mediaId])->save(['main_media_id'=>$mediaId]);

			return $mediaId;
			
		}
		
		
		
		
	}

	public function add_edit_tadowner($tadowner,$media_id){
        $tadowner_res = M('tadowner')->field('fid')->where(['fname'=>$tadowner,'is_netowner' => 1])->find();//查询媒介

        if(!$tadowner_res){
/*
            $fmediaownerid = M('tmedia')->where(['fid'=>$media_id])->getField('fmediaownerid');//查询媒介机构

            if($fmediaownerid != 0){
                $fregionid = M('tmediaowner')->where(['fid'=>$fmediaownerid])->getField('fregionid');//查询媒介机构
                if($tadowner_res['fregionid'] != $fregionid){
                    $data['fregionid'] = 0;//行政区划
                    $data['fmodifier'] = '接口修改';//修改人
                    $data['fmodifytime'] = date('Y-m-d H:i:s',time());//修改时间
                    M('tadowner')->where(['fid'=>$tadowner_res['fid']])->save($data);//更新广告主
                    $res = $tadowner_res['fid'];
                }
            }

        }else{*/
            $data['fname'] = $tadowner;//广告主名称
            $data['fdomain'] = $tadowner;//广告主名称
            $data['fregionid'] = 0;//行政区划
            $data['fcreator'] = '接口创建';//创建人
            $data['fcreatetime'] = date('Y-m-d H:i:s',time());//创建时间
            $data['fmodifier'] = '接口修改';//修改人
            $data['fmodifytime'] = date('Y-m-d H:i:s',time());//修改时间
            $data['fstate'] = 1;//状态
            $data['is_netowner'] = 1;//是否互联网广告主
            $res = M('tadowner')->add($data);//查询媒介
            return $res;
        }else{
            return $tadowner_res['fid'];
        }
    }
	

}