<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 数据检查、检查结果
 * by zw
 */

class GjcshujuController extends BaseController{

    /**
     * 数据检查、检查结果列表
     * by zw
     */
    public function sjjc_list(){
        session_write_close();
        header("Content-type:text/html;charset=utf-8");
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $ischeck = $ALL_CONFIG['ischeck'];
        $isrelease = $ALL_CONFIG['isrelease'];
        $illDistinguishPlatfo = $ALL_CONFIG['illDistinguishPlatfo'];//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
        $media_class = json_decode($ALL_CONFIG['media_class'],true);//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

        $p  = I('page', 1);//当前第几页
        $pp = 20;//每页显示多少记录

        $years  = I('years')?I('years'):date('Y');//检查年份
        $months = I('months')?I('months'):date('m');//检查月份
        $area   = I('area');
        $fstate = I('fstate');

        //时间条件筛选
        $timetypes  = 30;//选择时间段
        $timeval    = I('timeval');//选择时间
        if(session('regulatorpersonInfo.fregulatorlevel')==30){
          $where_time = gettimecondition($years,$timetypes,$months,'fissue_date',-2,$isrelease);
        }else{
          $where_time = gettimecondition($years,$timetypes,$months,'fissue_date',1,$isrelease);
        }

        //是否抽查模式
        if(!empty($ischeck)){
            $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
            //如果抽查表有数据
            if(!empty($spot_check_data)){
                $dates = [];//定义日期数组
                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                    $year_month = '';
                    $date_str = [];
                    $year_month = substr($spot_check_data_val['fmonth'],0,7);
                    if(!empty($spot_check_data_val['condition'])){
		            	$date_str = explode(',',$spot_check_data_val['condition']);
		            }
                    foreach ($date_str as $date_str_val){
                        $dates[] = $year_month.'-'.$date_str_val;
                    }
                }
            $where_time   .= ' and tbn_illegal_ad_issue.fissue_date in ("'.implode('","', $dates).'")';
            }else{
                $where_time   .= ' and 1=0';
            }
        }

        $search_type  = I('search_type');//搜索类别
        $search_val   = I('search_val');//搜索值
        if(!empty($search_val)){
          if($search_type == 10){//媒体分类
            $where_tia['a.fmedia_class'] = $search_val;
          }elseif($search_type == 20){//广告类别
            $where_tia['b.ffullname'] = array('like','%'.$search_val.'%');
          }elseif($search_type == 30){//广告名称
            $where_tia['a.fad_name'] = array('like','%'.$search_val.'%');
          }elseif($search_type == 40){//发布媒体
            $where_tia['c.fmedianame'] = array('like','%'.$search_val.'%');
          }elseif($search_type == 50){//违法原因
            $where_tia['a.fillegal'] = array('like','%'.$search_val.'%');
          }
        }
        $where_tia['_string'] = '1=1';

        $wherestr = ' and tn.flevel in (1,2,3)';

        if(session('regulatorpersonInfo.fregulatorlevel')!=30){
            $wheredit = ' and dit_tnid='.session('regulatorpersonInfo.regionid');
        }else{
            if(!empty($area) && $area!=100000){//所属地区
                $wheredit = ' and dit_tnid='.$area;
            }
        }

        if($fstate != -1){
            if(!empty($fstate)){
                $where_tia['_string'] .= ' and dig.dig_status ='.$fstate;
            }else{
                $where_tia['_string'] .= ' and dig.dig_status is null';
            }
        }
        if(!empty($illDistinguishPlatfo)){
          $where_tia['left(c.fmediaclassid,2)'] = ['in',$media_class];
        }
        $where_tia['a.fcustomer']  = $system_num;

        $count = M('tbn_illegal_ad')
          ->alias('a')
          ->join('tadclass b on a.fad_class_code=b.fcode')
          ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
          ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
          ->join('tbn_data_inspect dit on tn.fid=dit.dit_covertnid and dit_year='.$years.' and dit_month='.$months.$wheredit)
          ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
          ->join('tbn_data_inspectlog dig on a.fid=dig.dig_lid','left')
          ->where($where_tia)
          ->count();//查询满足条件的总记录数

        
        $do_tia = M('tbn_illegal_ad')
          ->alias('a')
          ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name,(case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus2,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,dit.dit_id,ifnull(dig.dig_status,0) as dig_status,dig.dig_content')
          ->join('tadclass b on a.fad_class_code=b.fcode')
          ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
          ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
          ->join('tbn_data_inspect dit on tn.fid=dit.dit_covertnid and dit_year='.$years.' and dit_month='.$months.$wheredit)
          ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
          ->join('tbn_data_inspectlog dig on a.fid=dig.dig_lid','left')
          ->where($where_tia)
          ->order('x.fstarttime desc')
          ->page($p,$pp)
          ->select();

        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));

    }

    /**
     * 数据检查动作
     * by zw
     */
    public function sjjc_action(){
        $adid = I('adid');//违法广告ID
        $status = I('status');//结果状态
        $content = I('content');//处理意见

        $where_ia['fid'] = $adid;
        $where_ia['tbn_illegal_ad.fcustomer']  = $system_num;
        $do_ia = M('tbn_illegal_ad')->where($where_ia)->find();
        if(!empty($do_ia)){
            $where_dig['dig_lid'] = $adid;
            $where_dig['dig_type'] = 0;
            $do_dig = M('tbn_data_inspectlog')->where($where_dig)->find();
            if(!empty($do_dig)){
                $this->ajaxReturn(array('code'=>1,'msg'=>'数据已被检查过'));
            }else{
                $data_dia['dig_lid']        = $adid;
                $data_dia['dig_type']       = 0;
                $data_dia['dig_treid']      = session('regulatorpersonInfo.fregulatorpid');
                $data_dia['dig_personid']   = session('regulatorpersonInfo.fid');
                $data_dia['dig_addtime']    = date('Y-m-d H:i:s');
                $data_dia['dig_status']     = $status;
                $data_dia['dig_content']    = $content;
                M('tbn_data_inspectlog')->add($data_dia);
                $this->ajaxReturn(array('code'=>0,'msg'=>'提交成功'));
            }
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'处理失败'));
        }
    }

}