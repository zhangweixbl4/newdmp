<?php
namespace TaskInput\Model;
use Think\Model;
class AdInputUserModel extends Model
{
    /**
     * 根据用户id获取该用户在录入的权限
     * @param $wx_id
     * @return array
     */
    public function getTaskLinkAuth($wx_id)
    {
        $linkTypes = []; // 录入权限
        $other_authority = M('ad_input_user_group')
            ->where([
                'group_id' => ['EXP', " = (select group_id from ad_input_user where wx_id = $wx_id)"]
            ])
            ->cache(true, 60)
            ->getField('other_authority');
        $authArr = explode(',', $other_authority);
        if (in_array('1006', $authArr)){
            $linkTypes[] = '1';
        }
        // 判断违法判定权限
        if (in_array('1007', $authArr)){
            $linkTypes[] = '2';
        }
        return $linkTypes;
    }

    /**
     * 根据用户id获取该用户在录入的权限
     * @param $wx_id
     * @return array
     */
    public function getTaskLinkAuth2($wx_id)
    {
        $linkTypes = []; // 录入权限
        $other_authority = M('ad_input_user_group')
            ->where([
                'group_id' => ['EXP', " = (select group_id from ad_input_user where wx_id = $wx_id)"]
            ])
            ->cache(true, 60)
            ->getField('other_authority');
        $authArr = explode(',', $other_authority);
        // 判断违法判定权限
        if (in_array('1007', $authArr)){
            $linkTypes[] = '2';
        }
        // 判断预览权限
        if (in_array('1008', $authArr)){
            $linkTypes[] = '3';
        }
        return $linkTypes;
    }
}