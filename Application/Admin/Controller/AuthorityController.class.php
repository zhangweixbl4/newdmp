<?php
//权限控制器
namespace Admin\Controller;	
use Think\Controller;
class AuthorityController extends BaseController {

	public function index(){

	}

	public function authority(){
		
		$p_id = I('authority_id');
		if(!$p_id) $this->ajaxReturn(array('code' => '-1','authority' => '','msg' => '无authority_id值'));//ajax返回详情
		$where['p_id'] = $p_id;
		$authority = M('admin_authority')->where($where)->order('authority_code')->select();
		$this->ajaxReturn(array('code' => 0,'authority' => $authority));//ajax返回详情

	}

}

?>