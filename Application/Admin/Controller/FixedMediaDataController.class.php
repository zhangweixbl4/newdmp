<?php
namespace Admin\Controller;
use Think\Controller;
class FixedMediaDataController extends BaseController {
	
	/*
	查询逾期未归档媒体的SQL
	
		select fmediaid
					,(select fmedianame from tmedia where fid = fixed_media_data.fmediaid)
					,count(*)
					,GROUP_CONCAT(fdate)
		from fixed_media_data
		where fixed_state = -1

		GROUP BY fmediaid
	*/
	

    public function index(){
		$this->assign('getRegionTree',A('Common/Region','Model')->getRegionTree());
    	$this -> display();
	}
	
	
	public function media_list(){
		
		session_write_close();
		$p = intval(I('p')); #页码
		if ($p <= 0) $p = 1;
		
		$pp = intval(I('pp')); #每页显示条数
		if ($pp <= 0) $pp = 10;
		if ($pp > 100) $pp = 100;
		
		$month = I('month');
		$mediaclass = I('mediaclass');
		$keyword = I('keyword');
		$region_id = I('region_id');
		$this_region_id = I('this_region_id');
		$medialabel = I('medialabel');
		$fixed_state = I('fixed_state');
		
		$where = array();
		$where['tmedia.priority'] = array('egt',0);
		if($keyword){
			$where['tmedia.fmedianame|tmedia.fid'] = array('like','%'.$keyword.'%');
		}
		$where['tmedia.fstate'] = array('egt',0);
		if($mediaclass){
			$where['left(tmedia.fmediaclassid,2)'] = array('in',$mediaclass);
		}
		
		if($region_id > 0){
			if($this_region_id == '1'){
				$where['tmedia.media_region_id'] = $region_id;//地区搜索条件
			}elseif($region_id != 100000){
				$region_id_rtrim = A('Common/System','Model')->get_region_left($region_id);//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmedia.media_region_id,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}
		}
		
		if($medialabel != ''){//是否搜索媒介标签
			
			$medialabel_where = array();//媒介标签包含的媒介id查询条件初始化
			
			$medialabel_where['tlabel.flabel'] = str_replace('#','',$medialabel);//查询条件，去掉井号
			$mediaIdList = M('tmedialabel')
										->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
										->where($medialabel_where)
										->getField('fmediaid',true);//查询媒介标签包含的媒介id
			if(strstr($medialabel,'#')){//是否包含井号，包含逗号即表示搜索非该标签媒介
				$mediaId_where = 'notin';
			}else{
				$mediaId_where = 'in';
			}
			
			if($mediaIdList){
				$where['tmedia.fid'][] = array($mediaId_where,$mediaIdList);	
			}else{
				$where['tmedia.fid'][] = array($mediaId_where,array(0));
			}	
		}//查询媒介标签包含的媒介id结束***************************************************************
		
		$start_date = $month.'-01';
		$end_date = date('Y-m-d', strtotime($start_date.' +1 month -1 day'));
		
		
		
		$fixWhere = array();
		$fixWhere['fissue_date'] = ['between',[$start_date,$end_date]];
		
		if($fixed_state != ''){ #是否需要搜索状态
			$fixWhere['fstatus'] = ['in',$fixed_state];
		}
		
		$fixed_state_mediaid_list = M('tinspect_plan','',C('ZIZHI_DB'))->where($fixWhere)->getField('DISTINCT fmedia_id',true);
		
		if($fixed_state_mediaid_list) {
			$where['tmedia.fid'][] = array('in',$fixed_state_mediaid_list);
		}else{
			$where['tmedia.fid'][] = 0;
		}	

		$count = M('tmedia')->cache(true,60)->where($where)->count();// 查询满足要求的总记录数
		$mediaList = M('tmedia')->cache(true,60)->field('fid,fmedianame,left(fmediaclassid,2) as media_class')->where($where)->page($p,$pp)->select();//查询媒介列表

		
		$mediaDataIdList = array_column($mediaList, 'fid');
		
		if($mediaDataIdList){
			$fixed_media_data_list0 = M('tinspect_plan','',C('ZIZHI_DB'))
														->field('
																	fmedia_id
																	,fissue_date
																	,ffinish_time
																	,fstatus
																	,finspect_type
																	,finspect_level
																	,fixed_value
																	,ffinish_ads
																	,fillegal_ads
																	,last_change_time
																	,source_collect_intact
																')
														->where(array(
																		'fmedia_id'=>array('in',$mediaDataIdList),
																		'fissue_date'=>array('between',array($start_date,$end_date))
																	))
														->select();
		}
		
		//var_dump($fixed_media_data_list0);
		
		$fixed_media_data_list = [];
		foreach($fixed_media_data_list0 as $ittt){
			$msg = '采集完整率:'.$ittt['source_collect_intact']."\n";

			if($ittt['finspect_type'] == 3){
				$msg .= '生产方式:快剪'."\n";
			}elseif($ittt['finspect_type'] == 2){
				$msg .= '生产方式:全剪'."\n";
			}elseif($ittt['finspect_type'] == 1){
				$msg .= '生产方式:云剪'."\n";
			}else{
				$msg .= '生产方式:其它'."\n";
			}
			if($ittt['fstatus'] == 2) $msg .= '归档条次:'.$ittt['ffinish_ads'].'(其中违法'.$ittt['fillegal_ads'].'条次)';
			$ittt['last_time'] = strtotime($ittt['ffinish_time']);
			$ittt['msg'] = $msg;
			$fixed_media_data_list[] = $ittt;
			
			
		}
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','count'=>$count,'mediaList'=>$mediaList,'fixed_media_data_list'=>$fixed_media_data_list));
		
	}
	
	
	#查询单个媒体天的数据归档记录
	public function s_log(){
		$fmediaid = I('fmediaid'); #媒介id
		$fdate = I('fdate'); #日期
		
		$mediaInfo = M('tmedia')->field('fid,fmedianame,left(fmediaclassid,2) as media_class,media_region_id')->where(array('fid'=>$fmediaid))->find();
		
		$fixed_media_data_log_list = M('fixed_media_data_log')->field('ftime,fvalue,fperson')->where(array('fmediaid'=>$fmediaid,'fdate'=>$fdate))->select();
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','mediaInfo'=>$mediaInfo,'fixed_media_data_log_list'=>$fixed_media_data_log_list));
		
		
	}
	
	#查询正在归档的数量
	public function zzgd(){
		#select count(*) from make_customer_ad_issue where fstate in(0,9)
		$zzgd = M('make_customer_ad_issue')->where(array('fstate'=>['in','0,9']))->count();
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','zzgd'=>$zzgd));
		
	}
	
	
	#批量修改媒体数据归档状态
	public function fixed(){
		$fix_days = I('fix_days');
		$state = I('state');
		$value = I('value');
		$count = 0;
		foreach($fix_days as $mediaid => $days){
			$day_arr = explode(',',$days);
			foreach($day_arr as $day){
				$rr = A('Admin/FixedMediaData','Model')->fixed($mediaid,$day,$state,$value,session('personInfo.fid').'_'.session('personInfo.fname')); #归档状态修改
				if($rr) $count++;
			}
			
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'操作已完成 '.$count.' 条数据'));
		
		
		
		
		
	}
	
	
	#获取消息
	public function get_msg(){
		
		$fmediaid = I('fmediaid'); #媒介id
		$fdate = I('fdate'); #日期
		
		$mediaInfo = M('tmedia')->field('fid,fmedianame,left(fmediaclassid,2) as media_class,media_region_id')->where(array('fid'=>$fmediaid))->find();
		$fixed_media_data = M('tinspect_plan','',C('ZIZHI_DB'))->where(array('fmedia_id'=>$fmediaid,'fissue_date'=>$fdate))->find();
		$issue_table_postfix = date('Ym',strtotime($fdate)).'_'.substr($mediaInfo['media_region_id'],0,2);
		
		
		$rr = A('Common/Media','Model')->media_issue_data_state($fmediaid,$fdate); #查询媒体数据就绪状态
		
		$data = A('Admin/FixedMediaData','Model')->get_issue_count($fmediaid,$fdate);
		$msg = '';
		$msg .= '媒介:'.$mediaInfo['fmedianame'].',日期：'.$fdate.'<br />';
		if($fixed_media_data['fstatus'] == 2){
			$msg .= '归档时间:<span title="'.date('H:i:s',strtotime($fixed_media_data['ffinish_time'])).'">'.date('Y-m-d',strtotime($fixed_media_data['ffinish_time'])).'</span>';
			if($fixed_media_data['last_change_time']) $msg .= ',数据最后变化时间：<span title="'.date('H:i:s',$fixed_media_data['last_change_time'] ).'">'.date('Y-m-d',$fixed_media_data['last_change_time'] ).'</span>';
			
			$msg .= '<br />';
		}
		$msg .= '基础数据: 条次:'.$data['tc'].',条数:'.$data['ts'].',违法条次：'.$data['wftc'].',违法条数：'.$data['wfts'].'<br />';
		
		$msg .= '未录入条次:'.$data['wlrtc'].',未录入条数：'.$data['wlrts'].'<br />';

		if($rr['remark']) $msg .= '提示：'.$rr['remark'].'<br />';
		

		$this->ajaxReturn(array('code'=>0,'msg'=>$msg));
		
	}
	
	
	/*修改生产方式*/
	public function change_inspect_type(){
		if(C('IS_TEST')) $this->ajaxReturn(array('code'=>-1,'msg'=>'非生产环境不允许修改'));
		$fix_days = I('fix_days');
		$finspect_type = I('finspect_type');
		if(!in_array($finspect_type,['0','1','2','3'])) $this->ajaxReturn(array('code'=>-1,'msg'=>'生产类型代码错误'));
		$count = 0;
		foreach($fix_days as $mediaid => $days){
			$day_arr = explode(',',$days);
			foreach($day_arr as $day){
				
				$count += M('tinspect_plan','',C('ZIZHI_DB'))
											->where(array('fissue_date'=>$day,'fmedia_id'=>$mediaid/* ,'fstatus'=>['neq',2] */))
											->save(array('finspect_type'=>$finspect_type/* ,'fstatus'=>0 */));
			}
			
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'操作已完成 '.$count.' 条数据'));
		
	}
	
	
	/*归档到分析数据库*/
	public function gd_fx(){
		
		$fix_days = I('fix_days');
		$value = I('value');
		$count = 0;
		foreach($fix_days as $mediaid => $days){
			$day_arr = explode(',',$days);
			foreach($day_arr as $day){
				
				$count += M('tinspect_plan','',C('ZIZHI_DB'))->where(array('fmedia_id'=>$mediaid,'fissue_date'=>$day))->save(array('need_gd'=>1));
				if($count) M('fixed_media_data_log')->add(array('fmediaid'=>$mediaid,'fdate'=>$day,'ftime'=>date('Y-m-d H:i:s'),'fvalue'=>$value.',手动抽取到分析数据库' . I('value'),'fperson'=>session('personInfo.fname')));
			}
			
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'操作已完成 '.$count.' 条数据'));
		
	}
	
	/*需要获取发布记录数据*/
	public function get_issue(){
		
		$fix_days = I('fix_days');
		$value = I('value');
		$count = 0;
		foreach($fix_days as $mediaid => $days){
			$day_arr = explode(',',$days);
			foreach($day_arr as $day){
				
				$count += M('tinspect_plan','',C('ZIZHI_DB'))->where(array('fmedia_id'=>$mediaid,'fissue_date'=>$day,'fstatus'=>1))->save(array('need_get_issue'=>1));
				if($count) M('fixed_media_data_log')->add(array('fmediaid'=>$mediaid,'fdate'=>$day,'ftime'=>date('Y-m-d H:i:s'),'fvalue'=>$value.',手动重新拉取发布记录数据' . I('value'),'fperson'=>session('personInfo.fname')));
			}
			
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'操作已完成 '.$count.' 条数据'));
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}