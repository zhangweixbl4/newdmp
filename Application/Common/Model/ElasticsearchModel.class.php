<?php

namespace Common\Model;
use Think\Exception;

class ElasticsearchModel{


	/*添加文档*/
	public function add_doc($index,$myType,$docData){
		
		$url = C('ES_URL').$index.'/'.$myType;
		
		$data = json_encode($docData);
		$header = array('Authorization:Basic '.base64_encode(C('ES_USER').':'.C('ES_PASS')));
		$rr = http($url,$data,'POST',$header);
		
		$rr = json_decode($rr,true);
		if($rr['created'] === true){
			return true;
		}else{
			return $rr['status'];
		}	
		
		
	}
	
	/*搜索文档*/
	public function search_doc($index,$myType,$search){
		
		$url = C('ES_URL').$index.'/'.$myType.'/_search';
		$data = array(
						'size'=>100,//返回数量
						'query'=>array(
										'query_string'=>array('query'=>$search),
										),
										
						);
		$data = json_encode($data);				


					
		$header = array('Authorization:Basic '.base64_encode(C('ES_USER').':'.C('ES_PASS')));

		$rr = http($url,$data,'POST',$header);
		$rr = json_decode( $rr,true);
		
		return $rr['hits']['hits'];
		
		
	}
	
	/*更新文档*/
	public function up_doc($index,$myType,$docId,$docData){
		
		$url = C('ES_URL').$index.'/'.$myType.'/'.$docId;
		$data = json_encode($docData);
			


					
		$header = array('Authorization:Basic '.base64_encode(C('ES_USER').':'.C('ES_PASS')));

		$rr = http($url,$data,'POST',$header);
		$rr = json_decode( $rr,true);
		
		return $rr['result'];
		
		
	}
	
	
	/*删除文档*/
	public function del_doc($index,$myType,$docId){
		
		$url = C('ES_URL').$index.'/'.$myType.'/'.$docId;
		
			

		$header = array('Authorization:Basic '.base64_encode(C('ES_USER').':'.C('ES_PASS')));

		$rr = http($url,array(),'DELETE',$header);
		$rr = json_decode( $rr,true);
		
		return $rr['result'];
		
		
	}
	
	
	
	
	
	
	
	

}