<?php
namespace Agp\Controller;
use Think\Controller;
use Agp\Model\StatisticalReportModel;
import('Vendor.PHPExcel');
/**
 * 数据统计
 * by yjm
 */
class FjchzdataController extends BaseController{


    /**by jim
     *自定义统计查询
     *
     */
    public function illegal_ad_monitor_customize(){
        session_write_close();
        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);
        header("Content-type:text/html;charset=utf-8");
        //$s_time = strtotime(I('s_time',date('Y-m-d')));//开始时间  如2018-07-08
        $s_time = strtotime(I('s_time','2018-06-01'));//开始时间  如2018-07-08
        //$e_time = strtotime(I('e_time',date('Y-m-d H:i:s')));//结束时间
        $e_time = strtotime(I('e_time','2018-07-01'));//结束时间
        $level = I('level');//行政级别
        $media_type = I('media_type','');//媒体类别 电视、广播、报纸
        $ad_type = I('ad_type');//广告类别  23大类
        $ranking_condition = I('ranking_condition','com_score');//排名条件
        $tab_name = I('tab_name','fadname');//选项卡名称
        $StatisticalReport = new StatisticalReportModel();
        $user = session('regulatorpersonInfo');//获取用户信息
        $finally_data = $StatisticalReport->fj_ad_monitor_customize($s_time,$e_time,$user['regionid'],$level,$media_type,$ad_type,$ranking_condition,$tab_name);
        $is_out_excel = 0;
        if($is_out_excel == 1){
            //如果是线索菜单并且是点击了导出按钮
            $objPHPExcel = new \PHPExcel();
            $objPHPExcel
                ->getProperties()  //获得文件属性对象，给下文提供设置资源
                ->setCreator( "MaartenBalliauw")             //设置文件的创建者
                ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
                ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
                ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
                ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
                ->setKeywords( "office 2007 openxmlphp")        //设置标记
                ->setCategory( "Test resultfile");                //设置类别

            $sheet = $objPHPExcel->setActiveSheetIndex(0);
            //分组导出数据1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
            switch ($tab_name){
                case 'fmediaid':
                    $sheet ->mergeCells("D1:F1");
                    $sheet ->mergeCells("G1:I1");
                    $sheet ->mergeCells("J1:L1");
                    $sheet ->mergeCells("M1:P1");
                    $sheet ->setCellValue('A1','');
                    $sheet ->setCellValue('B1','');
                    $sheet ->setCellValue('C1','');
                    $sheet ->setCellValue('D1','');
                    $sheet ->setCellValue('E1','广告条数');
                    $sheet ->setCellValue('H1','广告条次');
                    $sheet ->setCellValue('K1','广告时长');
                    $sheet ->setCellValue('N1','线索查看及处理情况');

                    $sheet ->setCellValue('A2','排名');
                    $sheet ->setCellValue('B2','媒体所在地');
                    $sheet ->setCellValue('C2','媒体名称');
                    $sheet ->setCellValue('D2','总条数');
                    $sheet ->setCellValue('E2','违法条数');
                    $sheet ->setCellValue('F2','条数违法率');
                    $sheet ->setCellValue('G2','总条次');
                    $sheet ->setCellValue('H2','违法条次');
                    $sheet ->setCellValue('I2','条次违法率');
                    $sheet ->setCellValue('J2','总时长');
                    $sheet ->setCellValue('K2','违法时长');
                    $sheet ->setCellValue('L2','时长违法率');
                    $sheet ->setCellValue('M2','查看量');
                    $sheet ->setCellValue('N2','查看率');
                    $sheet ->setCellValue('O2','处理量');
                    $sheet ->setCellValue('P2','处理率');

                    foreach ($data as $key => $value) {

                        $sheet ->setCellValue('A'.($key+3),($key+1));
                        if($data_count == $key){
                            $sheet ->setCellValue('B'.($key+3),'全部地域');
                            $sheet ->setCellValue('C'.($key+3),'全部媒体');
                        }else{
                            $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                            $sheet ->setCellValue('C'.($key+3),$value['tmedia_name']);
                        }

                        $sheet ->setCellValue('D'.($key+3),$value['fad_count']);
                        $sheet ->setCellValue('E'.($key+3),$value['fad_illegal_count']);
                        $sheet ->setCellValue('F'.($key+3),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('G'.($key+3),$value['fad_times']);
                        $sheet ->setCellValue('H'.($key+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('I'.($key+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('G'.($key+3),$value['fad_play_len']);
                        $sheet ->setCellValue('K'.($key+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('L'.($key+3),$value['lens_illegal_rate'].'%');
                        $sheet ->setCellValue('M'.($key+3),$value['ck_count']);
                        $sheet ->setCellValue('N'.($key+3),$value['ckl']);
                        $sheet ->setCellValue('O'.($key+3),$value['cl_count']);
                        $sheet ->setCellValue('P'.($key+3),$value['cll']);

                    }
                    break;
                case 'fregionid':
                    $sheet ->mergeCells("C1:E1");
                    $sheet ->mergeCells("F1:H1");
                    $sheet ->mergeCells("I1:K1");
                    $sheet ->mergeCells("L1:O1");
                    $sheet ->setCellValue('A1','');
                    $sheet ->setCellValue('B1','');
                    $sheet ->setCellValue('C1','广告条数');
                    $sheet ->setCellValue('F1','广告条次');
                    $sheet ->setCellValue('I1','广告时长');
                    $sheet ->setCellValue('L1','线索查看及处理情况');

                    $sheet ->setCellValue('A2','排名');
                    $sheet ->setCellValue('B2','地域名称');
                    $sheet ->setCellValue('C2','总条数');
                    $sheet ->setCellValue('D2','违法条数');
                    $sheet ->setCellValue('E2','条数违法率');
                    $sheet ->setCellValue('F2','总条次');
                    $sheet ->setCellValue('G2','违法条次');
                    $sheet ->setCellValue('H2','条次违法率');
                    $sheet ->setCellValue('I2','总时长');
                    $sheet ->setCellValue('J2','违法时长');
                    $sheet ->setCellValue('K2','时长违法率');
                    $sheet ->setCellValue('L2','查看量');
                    $sheet ->setCellValue('M2','查看率');
                    $sheet ->setCellValue('N2','处理量');
                    $sheet ->setCellValue('O2','处理率');

                    foreach ($data as $key => $value) {


                        $sheet ->setCellValue('A'.($key+3),($key+1));
                        if($data_count == $key){
                            $sheet ->setCellValue('B'.($key+3),'全部地域');
                        }else{
                            $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                        }

                        $sheet ->setCellValue('C'.($key+3),$value['fad_count']);
                        $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_count']);
                        $sheet ->setCellValue('E'.($key+3),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('F'.($key+3),$value['fad_times']);
                        $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('H'.($key+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('I'.($key+3),$value['fad_play_len']);
                        $sheet ->setCellValue('J'.($key+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('K'.($key+3),$value['lens_illegal_rate'].'%');
                        $sheet ->setCellValue('L'.($key+3),$value['ck_count']);
                        $sheet ->setCellValue('M'.($key+3),$value['ckl']);
                        $sheet ->setCellValue('N'.($key+3),$value['cl_count']);
                        $sheet ->setCellValue('O'.($key+3),$value['cll']);

                    }
                    break;
                case 'fmediaownerid':
                    $sheet ->mergeCells("E1:G1");
                    $sheet ->mergeCells("H1:J1");
                    $sheet ->mergeCells("K1:M1");
                    $sheet ->mergeCells("N1:Q1");
                    $sheet ->setCellValue('A1','');
                    $sheet ->setCellValue('B1','');
                    $sheet ->setCellValue('C1','');
                    $sheet ->setCellValue('D1','');
                    $sheet ->setCellValue('E1','广告条数');
                    $sheet ->setCellValue('H1','广告条次');
                    $sheet ->setCellValue('K1','广告时长');
                    $sheet ->setCellValue('N1','线索查看及处理情况');

                    $sheet ->setCellValue('A2','排名');
                    $sheet ->setCellValue('B2','媒介机构所在地');
                    $sheet ->setCellValue('C2','媒介机构名称');
                    $sheet ->setCellValue('D2','媒介类型');
                    $sheet ->setCellValue('E2','总条数');
                    $sheet ->setCellValue('F2','违法条数');
                    $sheet ->setCellValue('G2','条数违法率');
                    $sheet ->setCellValue('H2','总条次');
                    $sheet ->setCellValue('I2','违法条次');
                    $sheet ->setCellValue('J2','条次违法率');
                    $sheet ->setCellValue('K2','总时长');
                    $sheet ->setCellValue('L2','违法时长');
                    $sheet ->setCellValue('M2','时长违法率');
                    $sheet ->setCellValue('N2','查看量');
                    $sheet ->setCellValue('O2','查看率');
                    $sheet ->setCellValue('P2','处理量');
                    $sheet ->setCellValue('Q2','处理率');

                    foreach ($data as $key => $value) {

                        $sheet ->setCellValue('A'.($key+3),($key+1));
                        if($data_count == $key){
                            $sheet ->setCellValue('B'.($key+3),'全部地域');
                            $sheet ->setCellValue('C'.($key+3),'全部媒介机构');
                            $sheet ->setCellValue('D'.($key+3),'全部类型');
                        }else{
                            $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                            $sheet ->setCellValue('C'.($key+3),$value['tmediaowner_name']);
                            $sheet ->setCellValue('D'.($key+3),$value['tmediaclass_name']);
                        }

                        $sheet ->setCellValue('E'.($key+3),$value['fad_count']);
                        $sheet ->setCellValue('F'.($key+3),$value['fad_illegal_count']);
                        $sheet ->setCellValue('G'.($key+3),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('H'.($key+3),$value['fad_times']);
                        $sheet ->setCellValue('I'.($key+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('J'.($key+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('K'.($key+3),$value['fad_play_len']);
                        $sheet ->setCellValue('L'.($key+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('M'.($key+3),$value['lens_illegal_rate'].'%');
                        $sheet ->setCellValue('N'.($key+3),$value['ck_count']);
                        $sheet ->setCellValue('O'.($key+3),$value['ckl']);
                        $sheet ->setCellValue('P'.($key+3),$value['cl_count']);
                        $sheet ->setCellValue('Q'.($key+3),$value['cll']);

                    }
                    break;
                case 'fad_class_code':
                    $sheet ->mergeCells("C1:E1");
                    $sheet ->mergeCells("F1:H1");
                    $sheet ->mergeCells("I1:K1");
                    $sheet ->mergeCells("L1:O1");
                    $sheet ->setCellValue('A1','');
                    $sheet ->setCellValue('B1','');
                    $sheet ->setCellValue('C1','广告条数');
                    $sheet ->setCellValue('F1','广告条次');
                    $sheet ->setCellValue('I1','广告时长');
                    $sheet ->setCellValue('L1','线索查看及处理情况');

                    $sheet ->setCellValue('A2','排名');
                    $sheet ->setCellValue('B2','广告类别');
                    $sheet ->setCellValue('C2','总条数');
                    $sheet ->setCellValue('D2','违法条数');
                    $sheet ->setCellValue('E2','条数违法率');
                    $sheet ->setCellValue('F2','总条次');
                    $sheet ->setCellValue('G2','违法条次');
                    $sheet ->setCellValue('H2','条次违法率');
                    $sheet ->setCellValue('I2','总时长');
                    $sheet ->setCellValue('J2','违法时长');
                    $sheet ->setCellValue('K2','时长违法率');
                    $sheet ->setCellValue('L2','查看量');
                    $sheet ->setCellValue('M2','查看率');
                    $sheet ->setCellValue('N2','处理量');
                    $sheet ->setCellValue('O2','处理率');

                    foreach ($data as $key => $value) {


                        $sheet ->setCellValue('A'.($key+3),($key+1));
                        if($data_count == $key){
                            $sheet ->setCellValue('B'.($key+3),'全部类别');
                        }else{
                            $sheet ->setCellValue('B'.($key+3),$value['tadclass_name']);
                        }

                        $sheet ->setCellValue('C'.($key+3),$value['fad_count']);
                        $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_count']);
                        $sheet ->setCellValue('E'.($key+3),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('F'.($key+3),$value['fad_times']);
                        $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('H'.($key+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('I'.($key+3),$value['fad_play_len']);
                        $sheet ->setCellValue('J'.($key+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('K'.($key+3),$value['lens_illegal_rate'].'%');
                        $sheet ->setCellValue('L'.($key+3),$value['ck_count']);
                        $sheet ->setCellValue('M'.($key+3),$value['ckl']);
                        $sheet ->setCellValue('N'.($key+3),$value['cl_count']);
                        $sheet ->setCellValue('O'.($key+3),$value['cll']);

                    }
                    break;
            }
            $objActSheet =$objPHPExcel->getActiveSheet();
            $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
            $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            $date = date('Ymdhis');
            $savefile = './Public/Xls/'.$date.'.xlsx';
            $savefile2 = '/Public/Xls/'.$date.'.xlsx';
            $objWriter->save($savefile);
            //即时导出下载
            /*          header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        header('Content-Disposition:attachment;filename="'.$date.'.xlsx"');
                        header('Cache-Control:max-age=0');
                        $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
                        $objWriter->save( 'php://output');*/
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$savefile2));

        }else{
            $this->ajaxReturn($finally_data);
        }
    }

    /**
     * 违法广告统计
     * by yjm
     *http://www.ctcmc.com.cn/web/ggsjzx.asp
     * 1.通过选择时间，确认实例化哪张表
     * 2.通过点击选项卡，连接对应表
     * 3.条件
     *  ① '区域'=>['省','副省级市','市','县区']
     *  ② 媒体（媒体的种类）
     *  ③ 广告类别（广告的种类）
     *  ④ 综合得分、时长、时长违法率、违法条次和违法率
     */

    public function illegal_ad_monitor(){

        $system_num = getconfig('system_num');

        $user = session('regulatorpersonInfo');//获取用户信息

        $ad_pm_type = I('ad_pm_type','');//确定是点击监测情况或者线索菜单
        $is_out_excel = I('is_out_excel',0);//确定是否点击导出EXCEL

        $year = I('year',date('Y'));//默认当年

        $times_table = I('times_table','');//选表，默认月表

        //默认表
        if($times_table == ''){
            $times_table = 'tbn_ad_summary_month';
        }

        $table_condition = I('table_condition','05-01');//根据选取的不用表展示不同的条件  //默认当月

        $previous_cycles = $this->get_previous_cycles($times_table,$year,$table_condition);//获取上个周期时间

        //根据选择的不同的时间组合不同的条件
        if($table_condition != ''){
            switch ($times_table){
                case 'tbn_ad_summary_week':
                    $table_condition = $year.$table_condition;//周
                    break;
                case 'tbn_ad_summary_half_month':
                case 'tbn_ad_summary_month':
                case 'tbn_ad_summary_quarter':
                    $table_condition = $year.'-'.$table_condition;//半月，月，季度
                    break;
            }
        }else{
            $month = date('m');
            $table_condition = $year.'18';//半月，月，季度   默认当月
        }

        //连表
        //分组统计条件1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
        $fztj = I('fztj','');//默认连接地域表
        if($fztj == ''){
            $fztj = 'fregionid';
        }


        $map = [];//定义查询条件

        //浏览权限设置start
        $re_level = $this->judgment_region($user['regionid']);//当前用户行政等级
        /*如果是国家局*/
        if($system_num == '100000'){
            if(!empty($gjj_label_media)){
                $usermedia = M('tmedia_temp')->where('ftype=1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid'))->getField('fmediaid',true);
                $map[$times_table.'.fmediaid'] = array('in',$usermedia);//国家局标签媒体
            }
        }
        //判断用户点击哪个菜单
        if($ad_pm_type == 2){
            $region = I('region2','');//区域
            if($region == ''){
                if($re_level == 2 || $re_level == 3 || $re_level == 4){
                    $region = 5;
                }else{
                    $region = $re_level;
                }
            }
            //线索
            switch ($region){
                case 0:
                case 1://全国各省违法线索情况
                    if($re_level == 0){
                        $map["tregion.flevel"] = 1;
                    }elseif($re_level == 1){
                        $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                    }
                    break;
                case 2:
                    if($re_level == 0){
                        //全国各副省级市违法线索情况
                        $map["tregion.flevel"] = 2;
                    }elseif($re_level == 1){
                        //本省各副省级市违法线索情况
                        $map["tregion.flevel"] = 2;
                        $map["tregion.fpid"] = $user['regionid'];
                    }elseif($re_level == 2){
                        $map["tregion.fpid"] = $user['regionid'];
                    }
                    break;
                case 3://各计划单列市
                    if($re_level == 0){
                        //全国各计划单列市违法线索情况
                        $map["tregion.flevel"] = 3;
                    }elseif($re_level == 1){
                        //本省各计划单列市违法线索情况
                        $map["tregion.flevel"] = 3;
                        $map["tregion.fpid"] = $user['regionid'];
                    }elseif($re_level == 3){
                        //$fpid = substr($user['regionid'],0,2).'0000';
                        $map["tregion.fpid"] = $user['regionid'];
                    }
                    break;
                case 4://各市
                    if($re_level == 0){
                        //如果是全国各市违法线索情况
                        $map["tregion.flevel"] = 4;
                    }elseif($re_level == 1){
                        //本省各市违法线索情况
                        $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                    }elseif($re_level == 4){
                        $map["tregion.fpid"] = $user['regionid'];
                    }
                    break;
                case 5://各区县
                    if($re_level == 0){
                        //全国各区县违法线索情况
                        $map["tregion.flevel"] = 5;
                    }elseif($re_level == 1){
                        //本省各区县违法线索情况
                        $fpid = substr($user['regionid'],0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                    }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                        //本市各区县违法线索情况
                        $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                        //$map["tregion.flevel"] = 5;
                    }elseif($re_level == 5){
                        $fpid = substr($user['regionid'],0,4);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                    }
                    break;
            }
        }else{
            $region = I('region','');//区域
            if($region == ''){
                if($re_level == 4){
                    $region = 4;
                }else{
                    if($re_level == 5){
                        $region = $re_level;
                    }else{
                        $region = 0;//$re_level;
                    }
                }
            }
            //违法情况
            switch ($region){
                case 0:
                case 1://省级和国家级的各省监测情况
                /*if($re_level == 0 || $re_level == 1 || $re_level == 3){
                $map["tregion.flevel"] = 1;
                }*/
                    $map["tregion.flevel"] = 1;
                    break;
                case 2://副省级市
                    $map["tregion.flevel"] = 2;
                    break;
                case 3://各计划单列市
                    $map["tregion.flevel"] = 3;
                    break;
                case 4://各市
                    if($re_level == 0){
                        //国家级用户的各市监测情况
                        $map["tregion.flevel"] = 4;
                    }else{
                        //市级或者省级用户的各市监测情况
                        if($re_level == 4 || $re_level == 2 || $re_level == 3){
                            $fpid = substr($user['regionid'],0,2).'0000';
                            $map["tregion.fpid"] = $fpid;
                        }elseif($re_level == 1){
                            $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                        }
                    }
                    break;
                case 5://各区县
                    if($re_level == 0){
                        //如果是全国用户
                        $map["tregion.flevel"] = 5;
                    }elseif($re_level == 1){
                        //如果是省级用户
                        $fpid = substr($user['regionid'],0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                    }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                        $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                        //$map['tregion.fpid|'.$times_table.'.fregionid'] =array($user['regionid'],substr($user['regionid'],0,4),'_multi'=>true);
                    }elseif($re_level == 5){
                        $fpid = substr($user['regionid'],0,4);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                    }
                    break;
            }
        }
        //浏览权限设置end


        //媒介类型
        $media_class = I('media_class','');
        if($media_class != ''){
            $map[$times_table.'.fmedia_class_code'] = ['like',$media_class.'%'];//媒体
        }

        //媒体细类
        $media_son_class = I('media_son_class','');//媒体细类
        if($media_son_class != ''){
            $map[$times_table.'.fmedia_class_code'] = ['like',$media_son_class.'%'];//媒体细类
        }

        //广告类型
        $ad_class = I('ad_class','');//广告大类
        if($ad_class != ''){
            if($ad_class < 10){
                $ad_class = '0'.$ad_class;
            }
            $map[$times_table.'.fad_class_code'] = $ad_class;//广告类别
        }

        $onther_class = I('onther_class','');//排名条件

        if($onther_class == ''){
            if($fztj == 'fad_class_code'){
                //如果是广告类别排名的话,默认条次占比排名
                $onther_class =  'fad_illegal_times';
            }else{
                //如果是其他的话,默认综合评分排名
                $onther_class =  'com_score';
            }
        }

        $data = [];//定义统计数据空数组

        //按照选定时间实例化表
        $table_model = M($times_table);

        $id = 'fid';//定义默认表ID
        $name = 'fname';//定义默认单元名称

        //匹配分组条件，连接不同表
        switch ($fztj){
            case 'fmedia_class_code':
                $join_table = 'tmediaclass';//fmedia_class_code媒介类别表fid,
                $name = 'fclass';
                $xs_group = 'fmedia_class';
                break;
            case 'fmediaownerid':
                $join_table = 'tmediaowner';//fmediaownerid媒介机构表fid
                $xs_group = 'fmediaownerid';
                break;
            case 'fregionid':
                $join_table = 'tregion';//fregionid行政区划表fid
                $xs_group = 'fregion_id';
                break;
            case 'fmediaid':
                $join_table = 'tmedia';//fmediaid媒介表fid
                $name = 'fmedianame';
                $xs_group = 'fmedia_id';
                break;
            case 'fad_class_code':
                $join_table = 'tadclass';//fad_class_code广告内容类别表fcode
                $id = 'fcode';
                $name = 'fadclass';
                $xs_group = 'fad_class_code';
                break;
        }

        //按条件分组数据,因为周表与其他表时间格式不统一 weekday get_stemp
        if($table_condition != ''){
            if($times_table == 'tbn_ad_summary_week'){
                $map[$times_table.'.fweek'] = $table_condition;
                $cycles_field = 'fweek';
                $week_year = substr($table_condition,0,4);
                $week_num = substr($table_condition,-2);
                $map_xs_array = $this->weekday($week_year,$week_num);
            }else{
                $map[$times_table.'.fdate'] = $table_condition;
                $cycles_field = 'fdate';
                $map_xs_array = $this->get_stemp($table_condition,$times_table);
            }
        }

        //查询符合条件的数据
        $group_field = $times_table.'.'.$fztj;
        $illegal_ad_mod = $table_model
            ->cache(true,600)
            ->field("
                $group_field,
                tmediaclass.fclass as tmediaclass_name,
                tmediaowner.fname as tmediaowner_name,
                tregion.fname1 as tregion_name,
                 (case when instr(tmedia.fmedianame,'（') > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,'（') -1) else tmedia.fmedianame end) as tmedia_name,
                tadclass.fadclass as tadclass_name,
                $times_table.fmediaid as fmediaid,
                $times_table.fmediaownerid as fmediaownerid,
                $times_table.fmedia_class_code as fmedia_class_code,
                $times_table.fregionid as fregionid,
                $times_table.fad_class_code as fad_class_code,
                $times_table.$cycles_field as cycles_time,
                sum($times_table.fad_illegal_times) as fad_illegal_times,
                sum($times_table.fad_times) as fad_times,
                sum($times_table.fad_illegal_count) as fad_illegal_count,
                sum($times_table.fad_count) as fad_count,
                sum($times_table.fad_illegal_play_len) as fad_illegal_play_len,
                sum($times_table.fad_play_len) as fad_play_len
           ")
            ->join("
                tmediaclass on 
                tmediaclass.fid = substring($times_table.fmedia_class_code,1,2)"
            )
            ->join("
                tmediaowner on 
                tmediaowner.fid = $times_table.fmediaownerid"
            )
            ->join("
                tregion on 
                tregion.fid = $times_table.fregionid"
            )
            ->join("
                tmedia on 
                tmedia.fid = $times_table.fmediaid"
            )
            ->join("
                tadclass on 
                tadclass.fcode = substring($times_table.fad_class_code,1,2)"
            )
            ->where($map)
            ->group($group_field)
            ->select();
        //统计数量，用来计算
        $all_count = $table_model
            ->cache(true,600)
            ->where($map)
            ->field("
                sum($times_table.fad_illegal_times) as fad_illegal_times,
                sum($times_table.fad_times) as fad_times,
                sum($times_table.fad_illegal_count) as fad_illegal_count,
                sum($times_table.fad_count) as fad_count,
                sum($times_table.fad_illegal_play_len) as fad_illegal_play_len,
                sum($times_table.fad_play_len) as fad_play_len
           ")
            ->join("
                tregion on 
                tregion.fid = $times_table.fregionid"
            )
            ->find();

        //线索查看处理情况tbn_illegal_ad
        $tbn_illegal_ad_mod = M('tbn_illegal_ad');
        $map_xs['unix_timestamp(tbn_illegal_ad.create_time)'] = array('between',array($map_xs_array['start'],$map_xs_array['end']));
        $map_xs['fcustomer'] = $system_num;
        //已处理量
        $ycl_count = $tbn_illegal_ad_mod
            ->cache(true,600)
            ->field("
                count(fstatus) as cll,
                $xs_group
           ")
            ->where(['fstatus'=>['gt',0]])
            ->where($map_xs)
            ->group($xs_group)
            ->select();

        //全部数量
        $cl_count = $tbn_illegal_ad_mod
            ->cache(true,600)
            ->field("
                count(fstatus) as clall,
                $xs_group
            ")
            ->where($map_xs)
            ->group($xs_group)
            ->select();
        $xs_data = [];//定义线索统计情况相关数据

        //如果没有被处理的数据
        if(empty($ycl_count)){
            foreach ($cl_count as $cl_val){
                $xs_data[$cl_val[$xs_group]]['cll'] = '0%';
                $xs_data[$cl_val[$xs_group]][$xs_group] = $cl_val[$xs_group];
                $xs_data[$cl_val[$xs_group]]['cl_count'] = 0;
                $xs_data[$cl_val[$xs_group]]['all_count'] = $cl_val['clall'];
            }
        }else{
            //循环获取
            foreach ($ycl_count as $ycl_val){
                foreach ($cl_count as $cl_val){
                    if($ycl_val[$xs_group] == $cl_val[$xs_group]){
                        if($ycl_val['cll'] != 0){
                            $xs_data[$cl_val[$xs_group]][$xs_group] = $cl_val[$xs_group];
                            $xs_data[$cl_val[$xs_group]]['cll'] = (round($ycl_val['cll']/$cl_val['clall'],4)*100).'%';
                            $xs_data[$cl_val[$xs_group]]['cl_count'] = $ycl_val['cll'];
                        }else{
                            $xs_data[$cl_val[$xs_group]]['cll'] = '0%';
                            $xs_data[$cl_val[$xs_group]][$xs_group] = $cl_val[$xs_group];
                            $xs_data[$cl_val[$xs_group]]['cl_count'] = 0;
                        }
                        $xs_data[$cl_val[$xs_group]]['all_count'] = $cl_val['clall'];
                    }
                }
            }
        }
        //已查看量
        $yck_count = $tbn_illegal_ad_mod
            ->cache(true,600)
            ->field("
                count(fview_status) as ckl,
                $xs_group
            ")
            ->where(['fview_status'=>['gt',0]])
            ->where($map_xs)
            ->group($xs_group)
            ->select();

        //全部数量
        $ck_count = $tbn_illegal_ad_mod
            ->cache(true,600)
            ->field("
                count(fview_status) as ckall,
                $xs_group
            ")
            ->where($map_xs)
            ->group($xs_group)
            ->select();

        //如果没有一查看的数量
        if(empty($yck_count)){
            foreach ($ck_count as $ck_val){
                $xs_data[$ck_val[$xs_group]]['ckl'] = '0%';
                $xs_data[$ck_val[$xs_group]]['ck_count'] = 0;
            }
        }else{
            //循环获取
            foreach ($yck_count as $yck_val){
                foreach ($ck_count as $ck_val){
                    if($yck_val[$xs_group] == $ck_val[$xs_group]){
                        if($yck_val['ckl'] != 0){
                            $xs_data[$ck_val[$xs_group]]['ck_count'] = $yck_val['ckl'];
                        }else{
                            $xs_data[$ck_val[$xs_group]]['ck_count'] = 0;
                        }
                        $xs_data[$ck_val[$xs_group]]['ckl'] = (round($yck_val['ckl']/$ck_val['ckall'],4)*100).'%';
                    }
                }
            }//组建案件查看与处理情况
        }

        //上个周期时间条件
        if($previous_cycles){
            if($times_table == 'tbn_ad_summary_week'){
                $map[$times_table.'.fweek'] = $previous_cycles;
                $prv_cycles_field = 'fweek';
            }else{
                $map[$times_table.'.fdate'] = $previous_cycles;
                $prv_cycles_field = 'fdate';
            }

            //查询上个周期的数据
            $illegal_prv_ad_mod = $table_model
                ->cache(true,600)
                ->field("
                $times_table.fmediaid as fmediaid,
                $times_table.fmediaownerid as fmediaownerid,
                $times_table.fmedia_class_code as fmedia_class_code,
                $times_table.fregionid as fregionid,
                $times_table.fad_class_code as fad_class_code,
                $times_table.$prv_cycles_field as cycles_time,
                sum($times_table.fad_illegal_times) as fad_illegal_times,
                sum($times_table.fad_times) as fad_times,
                sum($times_table.fad_illegal_count) as fad_illegal_count,
                sum($times_table.fad_count) as fad_count,
                sum($times_table.fad_illegal_play_len) as fad_illegal_play_len,
                sum($times_table.fad_play_len) as fad_play_len
           ")
                ->join("
                tmediaclass on 
                tmediaclass.fid = $times_table.fmedia_class_code"
                )
                ->join("
                tmediaowner on 
                tmediaowner.fid = $times_table.fmediaownerid"
                )
                ->join("
                tregion on 
                tregion.fid = $times_table.fregionid"
                )
                ->join("
                tmedia on 
                tmedia.fid = $times_table.fmediaid"
                )
                ->join("
                tadclass on 
                tadclass.fcode = $times_table.fad_class_code"
                )
                ->where($map)
                ->group($fztj)
                ->select();



            //统计数量，用来计算
            $prv_all_count = $table_model
                ->cache(true,600)
                ->where($map)
                ->field("
                sum($times_table.fad_illegal_times) as fad_illegal_times,
                sum($times_table.fad_times) as fad_times,
                sum($times_table.fad_illegal_count) as fad_illegal_count,
                sum($times_table.fad_count) as fad_count,
                sum($times_table.fad_illegal_play_len) as fad_illegal_play_len,
                sum($times_table.fad_play_len) as fad_play_len
           ")
                ->join("
                tregion on 
                tregion.fid = $times_table.fregionid"
                )
                ->find();

            //$this->ajaxReturn($illegal_prv_ad_mod);
        }


        /*
        地域排名综合得分计算公式为：综合得分=(条次违法率+条数违法率)/2*50+时长违法率*50
        电视、广播媒体排名综合得分计算公式为：综合得分=(条次违法率+条数违法率)/2*50+时长违法率*50
        报纸媒体综合得分计算公式为：综合得分=条次违法率*60+（违法总条次/全部地域违法总条次）*40
        */
        //按公式计算综合评分
        foreach ($illegal_ad_mod as $ad_key => $illegal_ad) {
            //如果是报纸
            $fmedia_class_code = substr($illegal_ad['fmedia_class_code'], 0, 2);
            $data[$ad_key]['times_illegal_rate'] = $illegal_ad['fad_illegal_times'] / $illegal_ad['fad_times'];//条次违法率
            $data[$ad_key]['counts_illegal_rate'] = $illegal_ad['fad_illegal_count'] / $illegal_ad['fad_count'];//条数违法率
            if ($fmedia_class_code == '03') {
                $com_score = ($data[$ad_key]['times_illegal_rate'] * 60) + ($illegal_ad['fad_illegal_times'] / $all_count['fad_illegal_times']) * 40;
            } else {
                $data[$ad_key]['lens_illegal_rate'] = $illegal_ad['fad_illegal_play_len'] / $illegal_ad['fad_play_len'];//时长违法率
                $com_score = ($data[$ad_key]['times_illegal_rate'] + $data[$ad_key]['counts_illegal_rate']) / 2 * 50 + ($data[$ad_key]['lens_illegal_rate'] * 50);
            }
            $data[$ad_key]['com_score'] = $com_score;
            $data[$ad_key] = array_merge($data[$ad_key], $illegal_ad);
        }

        //对比上个周期的数据
        foreach ($illegal_prv_ad_mod as $illegal_prv_ad_val) {
            foreach ($data as $data_key=>$data_value) {

                if ($illegal_prv_ad_val[$fztj] == $data_value[$fztj]) {

                    if (!$illegal_prv_ad_val['fad_illegal_count'] || ($data_value['fad_illegal_count'] - $illegal_prv_ad_val['fad_illegal_count']) == 0) {
                        $data[$data_key]['prv_fad_illegal_count'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_count = round(($data_value['fad_illegal_count'] - $illegal_prv_ad_val['fad_illegal_count']) / $illegal_prv_ad_val['fad_illegal_count'], 4) * 100;
                        if ($prv_fad_illegal_count < 0) {
                            $data[$data_key]['prv_fad_illegal_count'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_count . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_count'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_count . '%)';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_times'] || ($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) == 0) {
                        $data[$data_key]['prv_fad_illegal_times'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_times = round(($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) / $illegal_prv_ad_val['fad_illegal_times'], 4) * 100;
                        if ($prv_fad_illegal_times < 0) {
                            $data[$data_key]['prv_fad_illegal_times'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_times . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_times'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_times . '%)';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_play_len'] || ($data_value['fad_illegal_play_len'] - $illegal_prv_ad_val['fad_illegal_play_len']) == 0) {
                        $data[$data_key]['prv_fad_illegal_play_len'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_play_len = round(($data_value['fad_illegal_play_len'] - $illegal_prv_ad_val['fad_illegal_play_len']) / $illegal_prv_ad_val['fad_illegal_play_len'], 4) * 100;

                        if ($prv_fad_illegal_play_len < 0) {
                            $data[$data_key]['prv_fad_illegal_play_len'] = '<span style="color: green;">&darr;</span>' . '(' . $prv_fad_illegal_play_len . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_play_len'] = '<span style="color: red;">&uarr;</span>' . '(' . $prv_fad_illegal_play_len . '%)';
                        }
                    }
                }
            }
        }

        //对比

        $data = $this->pxsf($data,$onther_class);//排序

        $array_num = 0;//定义限定数量
        $data_pm = [];//定义图表数据空数组
        $data_count = count($data);//获取数据条数

        $cl_count_all = 0;//线索全部数量
        $ck_count_all = 0;//线索全部数量
        $count_all = 0;//总数
        $times=0;

        //处理排序后的数据
        foreach ($data as $data_key=>$data_value){

            $data[$data_key]['times_illegal_rate'] = round($data_value['times_illegal_rate'],4)*100;
            $data[$data_key]['counts_illegal_rate'] = round($data_value['counts_illegal_rate'],4)*100;
            $data[$data_key]['lens_illegal_rate'] = round($data_value['lens_illegal_rate'],4)*100;
            $data[$data_key]['com_score'] = round($data_value['com_score'],2);
            $data[$data_key]['wf_zb'] = round($data_value['fad_illegal_count']/$all_count['fad_illegal_count'],5)*100;
            //循环获取前30条数据
            if($array_num < 30 && $array_num < count($data) && $data_value[$onther_class] > 0){
                $data_pm['name'][] = $data_value[$join_table.'_name'];
                //不同排名条件显示各自的数值
                switch ($onther_class){
                    case 'com_score':
                        $data_pm['num'][] = $data[$data_key]['com_score'];//综合
                        break;
                    case 'fad_illegal_play_len':
                        $data_pm['num'][] = $data_value['fad_illegal_play_len'];//违法时长
                        break;
                    case 'lens_illegal_rate':
                        $data_pm['num'][] = $data[$data_key]['lens_illegal_rate'];//时长违法率
                        break;
                    case 'fad_illegal_times':
                        $data_pm['num'][] = $data_value['fad_illegal_times'];//违法条次
                        break;
                    case 'times_illegal_rate':
                        $data_pm['num'][] = $data[$data_key]['times_illegal_rate'];//条次违法率
                        break;
                    case 'fad_illegal_count':
                        $data_pm['num'][] = $data[$data_key]['fad_illegal_count'];//条数
                        break;
                    case 'counts_illegal_rate':
                        $data_pm['num'][] = $data[$data_key]['counts_illegal_rate'];//条数违法率
                        break;
                }
                $array_num++;
            }

        }

        //组合线索相关数据
        foreach ($xs_data as $xs_key=>$xs_value){
            foreach ($data as $data_key=>$data_value){
                if($xs_key == $data_value[$fztj]) {
                    unset($xs_value[$xs_group]);
                    $cl_count_all += $xs_value['cl_count'];
                    $ck_count_all += $xs_value['ck_count'];
                    $count_all += $xs_value['all_count'];
                    $data[$data_key] = array_merge($xs_value, $data[$data_key]);//合并数组
                }
            }
        }

        //组合广告类别百分比图表数据
        $ad_class_num = 0;
        if($fztj == 'fad_class_code') {
            $data_pm = [];
            $data = $this->pxsf($data, $onther_class);//排序
            foreach ($data as $ad_class_key=>$ad_class_value){
                if($ad_class_value[$onther_class] != 0){
                    $data_pm['data'][] = [
                        'name' => $ad_class_value[$join_table.'_name'],
                        'value' => $ad_class_value[$onther_class]
                    ];
                    $data_pm['name'][] = $ad_class_value[$join_table.'_name'];
                }
            }
        }



        $all_prv_fad_illegal_count = round(($all_count['fad_illegal_count'] - $prv_all_count['fad_illegal_count']) / $prv_all_count['fad_illegal_count'], 4) * 100;
        $all_prv_fad_illegal_play_len = round(($all_count['fad_illegal_play_len'] - $prv_all_count['fad_illegal_play_len']) / $prv_all_count['fad_illegal_play_len'], 4) * 100;
        $all_prv_fad_illegal_times = round(($all_count['fad_illegal_times'] - $prv_all_count['fad_illegal_times']) / $prv_all_count['fad_illegal_times'], 4) * 100;
        if ($all_prv_fad_illegal_count < 0) {
            $all_prv_fad_illegal_count = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_count . '%)';
        } else {
            if($all_prv_fad_illegal_count == 0){
                $all_prv_fad_illegal_count = '';

            }else{
                $all_prv_fad_illegal_count = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_count . '%)';
            }
        }

        if ($all_prv_fad_illegal_play_len < 0) {
            $all_prv_fad_illegal_play_len = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_play_len . '%)';
        } else {
            if($all_prv_fad_illegal_play_len == 0){
                $all_prv_fad_illegal_play_len = '';

            }else{
                $all_prv_fad_illegal_play_len = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_play_len . '%)';
            }
        }

        if ($all_prv_fad_illegal_times < 0) {
            $all_prv_fad_illegal_times = '<span style="color: green;">&darr;</span>' . '(' . $all_prv_fad_illegal_times . '%)';
        } else {
            if($all_prv_fad_illegal_times == 0){
                $all_prv_fad_illegal_times = '';

            }else{
                $all_prv_fad_illegal_times = '<span style="color: red;">&uarr;</span>' . '(' . $all_prv_fad_illegal_times . '%)';
            }
        }

        //统计放在最后一条
        $data[] = [
            'titlename'=>'全部',
            'fad_count'=>$all_count['fad_count'],
            'fad_illegal_count'=>$all_count['fad_illegal_count'],
            'counts_illegal_rate'=>round($all_count['fad_illegal_count']/$all_count['fad_count'],4)*100,
            'fad_times'=>$all_count['fad_times'],
            'fad_illegal_times'=>$all_count['fad_illegal_times'],
            'times_illegal_rate'=>round($all_count['fad_illegal_times']/$all_count['fad_times'],4)*100,
            'fad_play_len'=>$all_count['fad_play_len'],
            'fad_illegal_play_len'=>$all_count['fad_illegal_play_len'],
            'lens_illegal_rate'=>round($all_count['fad_illegal_play_len']/$all_count['fad_play_len'],4)*100,
            "cll"=>(round($cl_count_all/$count_all,4)*100).'%',
            "cl_count" =>$cl_count_all,
            "ck_count"=>$ck_count_all,
            "ckl"=>(round($ck_count_all/$count_all,4)*100).'%',
            "prv_fad_illegal_count"=>$all_prv_fad_illegal_count,
            "prv_fad_illegal_play_len"=>$all_prv_fad_illegal_play_len,
            "prv_fad_illegal_times"=>$all_prv_fad_illegal_times
        ];
        $finally_data = [
            'data_pm'=>$data_pm,//图表数据
            'data_list'=>$data//列表数据
        ];

        if($is_out_excel == 1){
            //如果是线索菜单并且是点击了导出按钮
            $objPHPExcel = new \PHPExcel();
            $objPHPExcel
                ->getProperties()  //获得文件属性对象，给下文提供设置资源
                ->setCreator( "MaartenBalliauw")             //设置文件的创建者
                ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
                ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
                ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
                ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
                ->setKeywords( "office 2007 openxmlphp")        //设置标记
                ->setCategory( "Test resultfile");                //设置类别

            $sheet = $objPHPExcel->setActiveSheetIndex(0);
            //分组导出数据1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
            switch ($fztj){
                case 'fmediaid':
                    $sheet ->mergeCells("D1:F1");
                    $sheet ->mergeCells("G1:I1");
                    $sheet ->mergeCells("J1:L1");
                    $sheet ->mergeCells("M1:P1");
                    $sheet ->setCellValue('A1','');
                    $sheet ->setCellValue('B1','');
                    $sheet ->setCellValue('C1','');
                    $sheet ->setCellValue('D1','');
                    $sheet ->setCellValue('E1','广告条数');
                    $sheet ->setCellValue('H1','广告条次');
                    $sheet ->setCellValue('K1','广告时长');
                    $sheet ->setCellValue('N1','线索查看及处理情况');

                    $sheet ->setCellValue('A2','排名');
                    $sheet ->setCellValue('B2','媒体所在地');
                    $sheet ->setCellValue('C2','媒体名称');
                    $sheet ->setCellValue('D2','总条数');
                    $sheet ->setCellValue('E2','违法条数');
                    $sheet ->setCellValue('F2','条数违法率');
                    $sheet ->setCellValue('G2','总条次');
                    $sheet ->setCellValue('H2','违法条次');
                    $sheet ->setCellValue('I2','条次违法率');
                    $sheet ->setCellValue('J2','总时长');
                    $sheet ->setCellValue('K2','违法时长');
                    $sheet ->setCellValue('L2','时长违法率');
                    $sheet ->setCellValue('M2','查看量');
                    $sheet ->setCellValue('N2','查看率');
                    $sheet ->setCellValue('O2','处理量');
                    $sheet ->setCellValue('P2','处理率');

                    foreach ($data as $key => $value) {

                        $sheet ->setCellValue('A'.($key+3),($key+1));
                        if($data_count == $key){
                            $sheet ->setCellValue('B'.($key+3),'全部地域');
                            $sheet ->setCellValue('C'.($key+3),'全部媒体');
                        }else{
                            $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                            $sheet ->setCellValue('C'.($key+3),$value['tmedia_name']);
                        }

                        $sheet ->setCellValue('D'.($key+3),$value['fad_count']);
                        $sheet ->setCellValue('E'.($key+3),$value['fad_illegal_count']);
                        $sheet ->setCellValue('F'.($key+3),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('G'.($key+3),$value['fad_times']);
                        $sheet ->setCellValue('H'.($key+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('I'.($key+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('G'.($key+3),$value['fad_play_len']);
                        $sheet ->setCellValue('K'.($key+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('L'.($key+3),$value['lens_illegal_rate'].'%');
                        $sheet ->setCellValue('M'.($key+3),$value['ck_count']);
                        $sheet ->setCellValue('N'.($key+3),$value['ckl']);
                        $sheet ->setCellValue('O'.($key+3),$value['cl_count']);
                        $sheet ->setCellValue('P'.($key+3),$value['cll']);

                    }
                    break;
                case 'fregionid':
                    $sheet ->mergeCells("C1:E1");
                    $sheet ->mergeCells("F1:H1");
                    $sheet ->mergeCells("I1:K1");
                    $sheet ->mergeCells("L1:O1");
                    $sheet ->setCellValue('A1','');
                    $sheet ->setCellValue('B1','');
                    $sheet ->setCellValue('C1','广告条数');
                    $sheet ->setCellValue('F1','广告条次');
                    $sheet ->setCellValue('I1','广告时长');
                    $sheet ->setCellValue('L1','线索查看及处理情况');

                    $sheet ->setCellValue('A2','排名');
                    $sheet ->setCellValue('B2','地域名称');
                    $sheet ->setCellValue('C2','总条数');
                    $sheet ->setCellValue('D2','违法条数');
                    $sheet ->setCellValue('E2','条数违法率');
                    $sheet ->setCellValue('F2','总条次');
                    $sheet ->setCellValue('G2','违法条次');
                    $sheet ->setCellValue('H2','条次违法率');
                    $sheet ->setCellValue('I2','总时长');
                    $sheet ->setCellValue('J2','违法时长');
                    $sheet ->setCellValue('K2','时长违法率');
                    $sheet ->setCellValue('L2','查看量');
                    $sheet ->setCellValue('M2','查看率');
                    $sheet ->setCellValue('N2','处理量');
                    $sheet ->setCellValue('O2','处理率');

                    foreach ($data as $key => $value) {


                        $sheet ->setCellValue('A'.($key+3),($key+1));
                        if($data_count == $key){
                            $sheet ->setCellValue('B'.($key+3),'全部地域');
                        }else{
                            $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                        }

                        $sheet ->setCellValue('C'.($key+3),$value['fad_count']);
                        $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_count']);
                        $sheet ->setCellValue('E'.($key+3),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('F'.($key+3),$value['fad_times']);
                        $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('H'.($key+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('I'.($key+3),$value['fad_play_len']);
                        $sheet ->setCellValue('J'.($key+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('K'.($key+3),$value['lens_illegal_rate'].'%');
                        $sheet ->setCellValue('L'.($key+3),$value['ck_count']);
                        $sheet ->setCellValue('M'.($key+3),$value['ckl']);
                        $sheet ->setCellValue('N'.($key+3),$value['cl_count']);
                        $sheet ->setCellValue('O'.($key+3),$value['cll']);

                    }
                    break;
                case 'fmediaownerid':
                    $sheet ->mergeCells("E1:G1");
                    $sheet ->mergeCells("H1:J1");
                    $sheet ->mergeCells("K1:M1");
                    $sheet ->mergeCells("N1:Q1");
                    $sheet ->setCellValue('A1','');
                    $sheet ->setCellValue('B1','');
                    $sheet ->setCellValue('C1','');
                    $sheet ->setCellValue('D1','');
                    $sheet ->setCellValue('E1','广告条数');
                    $sheet ->setCellValue('H1','广告条次');
                    $sheet ->setCellValue('K1','广告时长');
                    $sheet ->setCellValue('N1','线索查看及处理情况');

                    $sheet ->setCellValue('A2','排名');
                    $sheet ->setCellValue('B2','媒介机构所在地');
                    $sheet ->setCellValue('C2','媒介机构名称');
                    $sheet ->setCellValue('D2','媒介类型');
                    $sheet ->setCellValue('E2','总条数');
                    $sheet ->setCellValue('F2','违法条数');
                    $sheet ->setCellValue('G2','条数违法率');
                    $sheet ->setCellValue('H2','总条次');
                    $sheet ->setCellValue('I2','违法条次');
                    $sheet ->setCellValue('J2','条次违法率');
                    $sheet ->setCellValue('K2','总时长');
                    $sheet ->setCellValue('L2','违法时长');
                    $sheet ->setCellValue('M2','时长违法率');
                    $sheet ->setCellValue('N2','查看量');
                    $sheet ->setCellValue('O2','查看率');
                    $sheet ->setCellValue('P2','处理量');
                    $sheet ->setCellValue('Q2','处理率');

                    foreach ($data as $key => $value) {

                        $sheet ->setCellValue('A'.($key+3),($key+1));
                        if($data_count == $key){
                            $sheet ->setCellValue('B'.($key+3),'全部地域');
                            $sheet ->setCellValue('C'.($key+3),'全部媒介机构');
                            $sheet ->setCellValue('D'.($key+3),'全部类型');
                        }else{
                            $sheet ->setCellValue('B'.($key+3),$value['tregion_name']);
                            $sheet ->setCellValue('C'.($key+3),$value['tmediaowner_name']);
                            $sheet ->setCellValue('D'.($key+3),$value['tmediaclass_name']);
                        }

                        $sheet ->setCellValue('E'.($key+3),$value['fad_count']);
                        $sheet ->setCellValue('F'.($key+3),$value['fad_illegal_count']);
                        $sheet ->setCellValue('G'.($key+3),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('H'.($key+3),$value['fad_times']);
                        $sheet ->setCellValue('I'.($key+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('J'.($key+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('K'.($key+3),$value['fad_play_len']);
                        $sheet ->setCellValue('L'.($key+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('M'.($key+3),$value['lens_illegal_rate'].'%');
                        $sheet ->setCellValue('N'.($key+3),$value['ck_count']);
                        $sheet ->setCellValue('O'.($key+3),$value['ckl']);
                        $sheet ->setCellValue('P'.($key+3),$value['cl_count']);
                        $sheet ->setCellValue('Q'.($key+3),$value['cll']);

                    }
                    break;
                case 'fad_class_code':
                    $sheet ->mergeCells("C1:E1");
                    $sheet ->mergeCells("F1:H1");
                    $sheet ->mergeCells("I1:K1");
                    $sheet ->mergeCells("L1:O1");
                    $sheet ->setCellValue('A1','');
                    $sheet ->setCellValue('B1','');
                    $sheet ->setCellValue('C1','广告条数');
                    $sheet ->setCellValue('F1','广告条次');
                    $sheet ->setCellValue('I1','广告时长');
                    $sheet ->setCellValue('L1','线索查看及处理情况');

                    $sheet ->setCellValue('A2','排名');
                    $sheet ->setCellValue('B2','广告类别');
                    $sheet ->setCellValue('C2','总条数');
                    $sheet ->setCellValue('D2','违法条数');
                    $sheet ->setCellValue('E2','条数违法率');
                    $sheet ->setCellValue('F2','总条次');
                    $sheet ->setCellValue('G2','违法条次');
                    $sheet ->setCellValue('H2','条次违法率');
                    $sheet ->setCellValue('I2','总时长');
                    $sheet ->setCellValue('J2','违法时长');
                    $sheet ->setCellValue('K2','时长违法率');
                    $sheet ->setCellValue('L2','查看量');
                    $sheet ->setCellValue('M2','查看率');
                    $sheet ->setCellValue('N2','处理量');
                    $sheet ->setCellValue('O2','处理率');

                    foreach ($data as $key => $value) {


                        $sheet ->setCellValue('A'.($key+3),($key+1));
                        if($data_count == $key){
                            $sheet ->setCellValue('B'.($key+3),'全部类别');
                        }else{
                            $sheet ->setCellValue('B'.($key+3),$value['tadclass_name']);
                        }

                        $sheet ->setCellValue('C'.($key+3),$value['fad_count']);
                        $sheet ->setCellValue('D'.($key+3),$value['fad_illegal_count']);
                        $sheet ->setCellValue('E'.($key+3),$value['counts_illegal_rate'].'%');
                        $sheet ->setCellValue('F'.($key+3),$value['fad_times']);
                        $sheet ->setCellValue('G'.($key+3),$value['fad_illegal_times']);
                        $sheet ->setCellValue('H'.($key+3),$value['times_illegal_rate'].'%');
                        $sheet ->setCellValue('I'.($key+3),$value['fad_play_len']);
                        $sheet ->setCellValue('J'.($key+3),$value['fad_illegal_play_len']);
                        $sheet ->setCellValue('K'.($key+3),$value['lens_illegal_rate'].'%');
                        $sheet ->setCellValue('L'.($key+3),$value['ck_count']);
                        $sheet ->setCellValue('M'.($key+3),$value['ckl']);
                        $sheet ->setCellValue('N'.($key+3),$value['cl_count']);
                        $sheet ->setCellValue('O'.($key+3),$value['cll']);

                    }
                    break;
            }
            $objActSheet =$objPHPExcel->getActiveSheet();
            $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
            $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            $date = date('Ymdhis');
            $savefile = './Public/Xls/'.$date.'.xlsx';
            $savefile2 = '/Public/Xls/'.$date.'.xlsx';
            $objWriter->save($savefile);
            //即时导出下载
            /*          header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        header('Content-Disposition:attachment;filename="'.$date.'.xlsx"');
                        header('Cache-Control:max-age=0');
                        $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
                        $objWriter->save( 'php://output');*/
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$savefile2));

        }else{
            $this->ajaxReturn($finally_data);
        }

    }
    //跨地域统计排名
    public function zones_ad(){
        $user = session('regulatorpersonInfo');//fregulatorid机构ID
        //选表
        $year = I('year',date('Y'));//默认当年
        $times_table = I('times_table','');//选表，默认月表
        $ad_pm_type = I('ad_pm_type','');//确定是点击哪个
        $is_out_excel = I('is_out_excel',0);//确定是否点击导出EXCEL
        if($times_table == ''){
            $times_table = 'tbn_zones_ad_month';
        }
        switch ($times_table){
            case 'tbn_ad_summary_week':
                $times_table = 'tbn_zones_ad_week';//周
                break;
            case 'tbn_ad_summary_half_month':
                $times_table = 'tbn_zones_ad_half_month';//半月
                break;
            case 'tbn_ad_summary_month':
                $times_table = 'tbn_zones_ad_month';//月
                break;
            case 'tbn_ad_summary_quarter':
                $times_table = 'tbn_zones_ad_quarter';//季度
                break;
            case 'tbn_ad_summary_half_year':
                $times_table = 'tbn_zones_ad_half_year';//半年
                break;
            case 'tbn_ad_summary_year':
                $times_table = 'tbn_zones_ad_year';//年
                break;
        }
        $table_condition = I('table_condition','05-01');//根据选取的不用表展示不同的条件  //默认当月
        $previous_cycles = $this->get_previous_cycles($times_table,$year,$table_condition);
        //根据选择的不同的时间组合不同的条件
        if($table_condition != ''){
            switch ($times_table){
                case 'tbn_zones_ad_week':
                    $table_condition = $year.$table_condition;//周
                    break;
                case 'tbn_zones_ad_half_month':
                case 'tbn_zones_ad_month':
                case 'tbn_zones_ad_quarter':
                    $table_condition = $year.'-'.$table_condition;//半月，月，季度
                    break;
            }
        }else{
            $month = date('m');
            $table_condition = $year.'18';//半月，月，季度   默认当月
        }

        //ad_class[ fad_class ] 广告类别 media_pclass[ fmedia_id ] 媒体 region_level[ fregion_id ]级别   cross_region_rank [  ] 广告/地域/媒体

        //区域级别
        $re_level = $this->judgment_region($user['regionid']);

        if($ad_pm_type == 2){
            $region = I('region_level2','');//区域
            if($region == ''){
                if($re_level == 2 || $re_level == 3 || $re_level == 4){
                    $region = 5;
                }else{
                    $region = $re_level;
                }
            }
            //线索
            switch ($region){
                case 0:
                case 1://全国各省违法线索情况
                    if($re_level == 0){
                        $map["tregion.flevel"] = 1;
                    }elseif($re_level == 1){
                        $map["tregion.fpid"] = $user['regionid'];
                    }
                    break;
                case 2:
                    if($re_level == 0){
                        //全国各副省级市违法线索情况
                        $map["tregion.flevel"] = 2;
                    }elseif($re_level == 1){
                        //本省各副省级市违法线索情况
                        $map["tregion.flevel"] = 2;
                        $map["tregion.fpid"] = $user['regionid'];
                    }elseif($re_level == 2){
                        $map["tregion.fpid"] = $user['regionid'];
                    }
                    break;
                case 3://各计划单列市
                    if($re_level == 0){
                        //全国各计划单列市违法线索情况
                        $map["tregion.flevel"] = 3;
                    }elseif($re_level == 1){
                        //本省各计划单列市违法线索情况
                        $map["tregion.flevel"] = 3;
                        $map["tregion.fpid"] = $user['regionid'];
                    }elseif($re_level == 3){
                        $map["tregion.fpid"] = $user['regionid'];
                    }
                    break;
                case 4://各市
                    if($re_level == 0){
                        //如果是全国各市违法线索情况
                        $map["tregion.flevel"] = 4;
                    }else{
                        //本省各市违法线索情况
                        if($re_level == 4 || $re_level == 2 || $re_level == 3){
                            $fpid = substr($user['regionid'],0,2).'0000';
                            $map["tregion.fpid"] = $fpid;
                        }elseif($re_level == 1){
                            $fpid = $user['regionid'];
                            $map["tregion.fpid"] = $fpid;
                        }
                    }
                    break;
                case 5://各区县
                    if($re_level == 0){
                        //全国各区县违法线索情况
                        $map["tregion.flevel"] = 5;
                    }elseif($re_level == 1){
                        //本省各区县违法线索情况
                        $fpid = substr($user['regionid'],0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                    }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                        //本市各区县违法线索情况
                        $map["tregion.fpid"] = $user['regionid'];
                        $map["tregion.flevel"] = 5;
                    }
                    break;
            }
        }else{
            $region = I('region_level','');//区域
            if($region == ''){
                /*                if($re_level == 2 || $re_level == 3 || $re_level == 4){
                                    $region = 4;
                                }else{*/
                $region = 0;//$re_level;
                //}
            }
            //违法情况
            switch ($region){
                case 0:
                case 1://省级和国家级的各省监测情况
                    //if($re_level == 0 || $re_level == 1){
                    $map["tregion.flevel"] = 1;
                    //}
                    break;
                case 2://副省级市
                    $map["tregion.flevel"] = 2;
                    break;
                case 3://各计划单列市
                    $map["tregion.flevel"] = 3;
                    break;
                case 4://各市
                    if($re_level == 0){
                        //国家级用户的各市监测情况
                        $map["tregion.flevel"] = 4;
                    }else{
                        //市级或者省级用户的各市监测情况
                        if($re_level == 4 || $re_level == 2 || $re_level == 3){
                            $fpid = substr($user['regionid'],0,2).'0000';
                            $map["tregion.fpid"] = $fpid;

                        }elseif($re_level == 1){
                            $fpid = $user['regionid'];
                            $map["tregion.fpid"] = $fpid;
                        }
                    }
                    break;
                case 5://各区县
                    if($re_level == 0){
                        //如果是全国用户
                        $map["tregion.flevel"] = 5;
                    }elseif($re_level == 1){
                        //如果是省级用户
                        $fpid = substr($user['regionid'],0,2);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                    }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                        $map["tregion.fpid"] = $user['regionid'];
                        $map["tregion.flevel"] = 5;
                    }
                    break;
            }
        }
        //浏览权限设置end

        //媒介类型
        $media_class = I('media_class','');
        if($media_class != ''){
            $map[$times_table.'.fmediaclass_id'] = $media_class;//媒体
        }

        //广告类型
        $ad_class = I('ad_class','');//广告大类
        if($ad_class != ''){
            if($ad_class < 10){
                $ad_class = '0'.$ad_class;
            }
            $map[$times_table.'.fad_class'] = ['like',$ad_class.'%'];//广告类别
        }

        $pm_class = I('cross_region_rank','');//其他查询条件
        if($pm_class == ''){
            $pm_class =  'fad_name';//fmedia_id    fad_name   fregion_id
        }

        //按照选定时间实例化表
        $table_model = M($times_table);
        //按条件分组数据
        if($table_condition != ''){
            $map[$times_table.'.fdate'] = $table_condition;
            $cycles_field = 'fdate';
        }
        //查询数据
        $group_field = $times_table.'.'.$fztj;
        //dump($map);
        header("Content-type: text/html; charset=utf-8");

        $zones_ad_mod = $table_model
            ->cache(true,600)
            ->field("
                tmediaclass.fclass as tmediaclass_name,
                tregion.fname1 as tregion_name,
                 (case when instr(tmedia.fmedianame,'（') > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,'（') -1) else tmedia.fmedianame end) as tmedia_name,
                tadclass.fadclass as tadclass_name,
                $times_table.fregion_id as fregion_id,
                $times_table.fmediaclass_id as fmediaclass_id,
                $times_table.fmedia_id as fmedia_id,
                $times_table.fad_class as fad_class,
                $times_table.issue_count as issue_count,
                $times_table.fad_name as fad_name
            ")
            ->join("
                tregion on 
                tregion.fid = $times_table.fregion_id"
            )
            ->join("
                tmediaclass on 
                tmediaclass.fid = $times_table.fmediaclass_id"
            )
            ->join("
                tadclass on 
                tadclass.fcode = substring($times_table.fad_class,1,2)"
            )
            ->join("
                tmedia on 
                tmedia.fid = $times_table.fmedia_id"
            )
            ->where($map)
            ->group('fad_name,fregion_id')
            ->having('count(*) < 2')
            ->select();
        $zones_data = $table_model
            ->cache(true,600)
            ->field("
                $pm_class,
                $times_table.fad_name as fad_name,
                sum(issue_count) as zones_count
                ")
            ->join("
                tregion on 
                tregion.fid = $times_table.fregion_id"
            )
            ->where($map)
            ->group($pm_class)
            ->select();

        $res = array();
        $is_one = [];

        if($pm_class == 'fmedia_id' || $pm_class == 'fregion_id'){

            foreach ($zones_ad_mod as $res_key=>$value) {
                if(isset($res[$value["fad_name"]])){
                    $cf[] = $value; //重复数据
                    $res[$value["fad_name"]]['res_num']++;
                }else{
                    $is_one[$value[$pm_class]] = $res_key;
                    $res[$value["fad_name"]] = $value;
                    $res[$value["fad_name"]]['res_num']++;
                }
            }

            foreach ($zones_ad_mod as $zones_data_key=>$zones_val){
                foreach ($res as $res_val){
                    if($res_val['res_num'] ==1 && $zones_val[$pm_class] == $res_val[$pm_class]){
                        unset($zones_ad_mod[$zones_data_key]);
                    }
                }
            }
        }
        $date_list = [];
        $count = 0;
        foreach ($zones_ad_mod as $zones_ad_mod_val){
            foreach ($zones_data as $zones_data_val){
                if($zones_ad_mod_val[$pm_class] == $zones_data_val[$pm_class]){
                    if($pm_class == 'fmedia_id' || $pm_class == 'fregion_id'){
                        if(!strstr($date_list[$zones_data_val[$pm_class]]['fad_name'],$zones_ad_mod_val['fad_name'].', ')){
                            $date_list[$zones_data_val[$pm_class]]['fad_name'] .= $zones_ad_mod_val['fad_name'].', ';
                        }else{
                            continue;
                        }
                        $date_list[$zones_data_val[$pm_class]]['tregion_name'] = $zones_ad_mod_val['tregion_name'];
                    }elseif($pm_class == 'fad_name'){
                        if(!strstr($date_list[$zones_data_val[$pm_class]]['tregion_name'],$zones_ad_mod_val['tregion_name'].', ')){
                            $date_list[$zones_data_val[$pm_class]]['tregion_name'] .= $zones_ad_mod_val['tregion_name'].', ';
                        }else{
                            continue;
                        }
                        $date_list[$zones_data_val[$pm_class]]['fad_name'] = $zones_ad_mod_val['fad_name'];
                    }
                    $date_list[$zones_data_val[$pm_class]]['tmedia_name'] = $zones_ad_mod_val['tmedia_name'];

                    $date_list[$zones_data_val[$pm_class]]['tadclass_name'] = $zones_ad_mod_val['tadclass_name'];
                    $date_list[$zones_data_val[$pm_class]][$pm_class] = $zones_ad_mod_val[$pm_class];
                    if($pm_class != 'fad_name'){
                        $date_list[$zones_data_val[$pm_class]]['zones_count'] = $zones_data_val['zones_count'];
                    }else{
                        $date_list[$zones_data_val[$pm_class]]['zones_count']++;
                    }
                }
            }
        }

        $date_list = array_values($date_list);

        $date_list = $this->pxsf($date_list,'zones_count');
        $array_num = 0;
        $data_pm = [];

        foreach ($date_list as $list_key=>$date_list_val){
            if($date_list_val['zones_count'] < 2 && $pm_class == 'fad_name'){
                unset($date_list[$list_key]);
                continue;
            }
            if($pm_class == 'fregion_id' || $pm_class == 'fmedia_id'){
                $date_list[$list_key]['fad_name'] = substr($date_list_val['fad_name'],0,-2);
            }else{
                $date_list[$list_key]['tregion_name'] = substr($date_list_val['tregion_name'],0,-2);;
            }
            //循环获取前30条数据
            if($array_num < 30 && $array_num < count($date_list)){
                if($pm_class == 'fregion_id'){
                    $data_pm['name'][] = $date_list_val['tregion_name'];
                }elseif($pm_class == 'fmedia_id'){
                    $data_pm['name'][] = $date_list_val['tmedia_name'];
                }else{
                    $data_pm['name'][] = $date_list_val[$pm_class];
                }
                $data_pm['num'][] = $date_list_val['zones_count'];
                $array_num++;
            }
        }
        $finally_data = [
            'data_pm'=>$data_pm,
            'data_list'=>$date_list
        ];
        if($is_out_excel == 1){
            $objPHPExcel = new \PHPExcel();
            $objPHPExcel
                ->getProperties()  //获得文件属性对象，给下文提供设置资源
                ->setCreator( "MaartenBalliauw")             //设置文件的创建者
                ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
                ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
                ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
                ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
                ->setKeywords( "office 2007 openxmlphp")        //设置标记
                ->setCategory( "Test resultfile");                //设置类别

            $sheet = $objPHPExcel->setActiveSheetIndex(0);
            //分组导出数据1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
            $pm_class =  'fmedia_id';//fmedia_id    fad_name   fregion_id
            if($pm_class == 'fregion_id'){
                $sheet ->setCellValue('A1','排名');
                $sheet ->setCellValue('B1','地域名称');
                $sheet ->setCellValue('C1','违法广告数量');
                $sheet ->setCellValue('D1','违法广告名称');
                foreach ($date_list as $key => $value) {
                    $sheet ->setCellValue('A'.($key+2),($key+1));
                    $sheet ->setCellValue('B'.($key+2),$value['tregion_name']);
                    $sheet ->setCellValue('C'.($key+2),$value['zones_count']);
                    $sheet ->setCellValue('D'.($key+2),$value['fad_name']);
                }
            }elseif($pm_class == 'fmedia_id'){
                $sheet ->setCellValue('A1','排名');
                $sheet ->setCellValue('B1','媒体名称');
                $sheet ->setCellValue('C1','违法广告数量');
                $sheet ->setCellValue('D1','违法广告名称');
                foreach ($date_list as $key => $value) {
                    $sheet ->setCellValue('A'.($key+2),($key+1));
                    $sheet ->setCellValue('B'.($key+2),$value['media_name']);
                    $sheet ->setCellValue('C'.($key+2),$value['zones_count']);
                    $sheet ->setCellValue('D'.($key+2),$value['fad_name']);
                }
            }else{
                $sheet ->setCellValue('A1','排名');
                $sheet ->setCellValue('B1','广告名称');
                $sheet ->setCellValue('C1','广告类别');
                $sheet ->setCellValue('D1','跨地域数量');
                $sheet ->setCellValue('E1','播出地域');
                foreach ($date_list as $key => $value) {
                    $sheet ->setCellValue('A'.($key+2),($key+1));
                    $sheet ->setCellValue('B'.($key+2),$value['fad_name']);
                    $sheet ->setCellValue('C'.($key+2),$value['tadclass_name']);
                    $sheet ->setCellValue('D'.($key+2),$value['zones_count']);
                    $sheet ->setCellValue('E'.($key+2),$value['tregion_name']);
                }
            }


            $objActSheet =$objPHPExcel->getActiveSheet();
            $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
            $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            $date = date('Ymdhis');
            $savefile = './Public/Xls/'.$date.'.xlsx';
            $savefile2 = '/Public/Xls/'.$date.'.xlsx';
            $objWriter->save($savefile);
            //即时导出下载
            /*          header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        header('Content-Disposition:attachment;filename="'.$date.'.xlsx"');
                        header('Cache-Control:max-age=0');
                        $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
                        $objWriter->save( 'php://output');*/
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$savefile2));

        }else{
            $this->ajaxReturn($finally_data);
        }
    }

    //媒体类型及媒体排名
    public function media_type_class_pm(){
        $user = session('regulatorpersonInfo');//fregulatorid机构ID

        $year = I('year',date('Y'));//默认当年

        $times_table = I('times_table','');//选表，默认月表

        if($times_table == ''){
            $times_table = 'tbn_ad_summary_half_month';
        }

        $table_condition = I('table_condition','04-01');//根据选取的不用表展示不同的条件  //默认当月

        //根据选择的不同的时间组合不同的条件
        if($table_condition != ''){
            switch ($times_table){
                case 'tbn_ad_summary_week':
                    $table_condition = $year.$table_condition;//周
                    break;
                case 'tbn_ad_summary_half_month':
                case 'tbn_ad_summary_month':
                case 'tbn_ad_summary_quarter':
                    $table_condition = $year.'-'.$table_condition;//半月，月，季度
                    break;
            }
        }else{
            $month = date('m');
            $table_condition = $year.'18';//半月，月，季度   默认当月
        }

        if($table_condition != ''){
            if($times_table == 'tbn_ad_summary_week'){
                $map[$times_table.'.fweek'] = $table_condition;
                $cycles_field = 'fweek';

            }else{
                $map[$times_table.'.fdate'] = $table_condition;
                $cycles_field = 'fdate';
            }
        }
        $re_level = $this->judgment_region($user['regionid']);
        if($re_level == 0){
            $map["tregion.flevel"] = 1;
        }else{
            $map["$times_table.fregionid"] = $user['regionid'];
        }
        $table_model = M($times_table);

        $maps = [['like','01%'],['like','02%'],['like',['03%','04%','OR']]];
        foreach ($maps as $key=>$map_val){
            $media_map[$times_table.".fmedia_class_code"] = $map_val;
            $media_pm['0'.($key+1)] = $table_model
                ->cache(true,600)
                ->where($map)
                ->where($media_map)
                ->join("
                    tmediaclass on 
                    tmediaclass.fid = $times_table.fmedia_class_code"
                )
                ->join("
                    tmedia on 
                    tmedia.fid = $times_table.fmediaid"
                )
                ->join("
                tregion on 
                tregion.fid = $times_table.fregionid"
                )
                ->order('fad_illegal_count desc')
                ->group('fmediaid')
                ->limit(5)
                ->getField("
                    $times_table.fmediaid as fmediaid,
                     (case when instr(tmedia.fmedianame,'（') > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,'（') -1) else tmedia.fmedianame end) as tmedia_name
                ",true);
        }
        $data_list = [];
        $num = 0;
        $id = 0;
        foreach ($media_pm as $key => $media_pm_val){
            if($key == '01'){
                $data_list['01']['media_name'] = '电视';
            }elseif($key == '02'){
                $data_list['02']['media_name'] = '广播';
            }elseif($key == '03'){
                $data_list['03']['media_name'] = '报纸、期刊';
            }
            if($num == 5 || $id == 5){
                $num = 0;
                $id = 0;
            }
            foreach ($media_pm_val as $val_key=>$media_pm_val_v){
                if($key == '01'){
                    $data_list['01']['num'.++$num] = $media_pm_val_v;
                    $data_list['01']['id'.++$id] = $val_key;
                }elseif($key == '02'){
                    $data_list['02']['num'.++$num] = $media_pm_val_v;
                    $data_list['02']['id'.++$id] = $val_key;
                }elseif($key == '03'){
                    $data_list['03']['num'.++$num] = $media_pm_val_v;
                    $data_list['03']['id'.++$id] = $val_key;
                }
            }
        }
        $this->ajaxReturn(array_values($data_list));
    }

    /*
     * 排序
     *
     * */
    public function pxsf($data = [],$score_rule = 'com_score'){
        $data_count = count($data);
        for($k=0;$k<=$data_count;$k++)
        {
            for($j=$data_count-1;$j>$k;$j--){
                if($data[$j][$score_rule]>$data[$j-1][$score_rule]){
                    $temp = $data[$j];
                    $data[$j] = $data[$j-1];
                    $data[$j-1] = $temp;
                }
            }
        }
        return $data;
    }
    /*
    * 获取条件
    *
    * */
    public function get_condition(){
        $user = session('regulatorpersonInfo');//fregulatorid机构ID
        $region_level = $this->judgment_region($user['regionid']);
        $data['jgname'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');

        $times_table = I('times_table');
        if($times_table == '' || $times_table == 'tbn_ad_summary_week'){
            $year_mod = M('tbn_ad_summary_week')->distinct(true)->getField('fweek',true);
        }else{
            $year_mod = M($times_table)->distinct(true)->getField('fdate',true);
        }
        $years = [];
        foreach ($year_mod as $year_date){
            if($year_date){
                $years[] = substr($year_date,0,4);
            }
        }
        $data['year'] = array_values(array_unique($years));//选年

        $data['select_table']['times_table'] = [
            'tbn_ad_summary_week'=>'周',
            'tbn_ad_summary_half_month'=>'半月',
            'tbn_ad_summary_month'=>'月',
            'tbn_ad_summary_quarter'=>'季度',
            'tbn_ad_summary_half_year'=>'半年',
            'tbn_ad_summary_year'=>'年'
        ];//选表

        $select_half_year = [];
        foreach ($data['year'] as $v_half_year){
            $select_half_year[$v_half_year.'-01-01'] = $v_half_year.'上半年';//$v_year.'上半年';
            $select_half_year[$v_half_year.'-07-01'] = $v_half_year.'下半年';//$v_year.'下半年';
        }

        $select_year = [];
        foreach ($data['year'] as $v_year){
            $select_year[$v_year.'-01-01'] = $v_year.'年';//$v_year.'上半年';
        }


        $data['select_table']['period'] = [
            'tbn_ad_summary_week'=>$this->create_number(52),
            'tbn_ad_summary_half_month'=>$this->create_month(true),
            'tbn_ad_summary_month'=>$this->create_month(false),
            'tbn_ad_summary_quarter'=>[
                '01-01'=>'第一季度',
                '04-01'=>'第二季度',
                '07-01'=>'第三季度',
                '10-01'=>'第四季度'
            ],
            'tbn_ad_summary_half_year'=>$select_half_year,
            'tbn_ad_summary_year'=>$select_year
        ];




        //版本0613
        switch ($region_level){
            case 0:
                $data['region_level'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市',
                    '4' =>'市级',
                    '5' =>'县级'
                ];//行政级别
                $data['region_level2'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市',
                    '4' =>'市级',
                    '5' =>'县级'
                ];//行政级别
                $data['region'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市',
                    '4' =>'市级',
                    '5' =>'县级'
                ];//选区域
                $data['region2'] = [
                    '1' =>'各省',
                    '2' =>'各副省级市',
                    '3' =>'各计划单列市',
                    '4' =>'各市',
                    '5' =>'各县'
                ];//选区域
                $data['region_name'] = '全国';
                $data['region_name1'] = '全国';
                $data['region_name2'] = '全国';
                break;
            case 1:
                $data['region_level'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市',
                    '4' =>'市级',
                    '5' =>'县级'
                ];//行政级别
                $data['region_level2'] = [
                    '2' =>'副省级市',
                    '3' =>'计划单列市',
                    '4' =>'市级',
                    '5' =>'县级'
                ];//行政级别
                $data['region'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市',
                    '4' =>'市级',
                    '5' =>'县级'
                ];//选区域
                $data['region2'] = [
                    '4' =>'各市',
                    '5' =>'各县'
                ];//选区域
                $data['region_name'] = '全国';//1
                $data['region_name1'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//1
                $data['region_name2'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//2
                break;
            case 2:
            case 3:
                $data['region_level'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市',
                    '4' =>'市级',
                    '5' =>'县级'
                ];//行政级别
                $data['region_level2'] = [
                    '5' =>'县级'
                ];//行政级别
                $data['region'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市',
                    '4' =>'市级',
                    '5' =>'县级'
                ];//选区域
                $data['region2'] = [
                    '5' =>'各县'
                ];//选区域
                $data['region_name'] = '全国';//1
                $fpid = substr($user['regionid'],0,2).'0000';
                $data['region_name1'] = M('tregion')->where(['fid'=>$fpid])->getField('fname1');//1
                $data['region_name2'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//2
                break;
            case 4:
                $data['region_level'] = [
                    '4' =>'市级',
                    '5' =>'县级'
                ];//行政级别
                $data['region_level2'] = [
                    '5' =>'县级'
                ];//行政级别
                $data['region'] = [
                    '4' =>'市级',
                    '5' =>'县级'
                ];//选区域
                $data['region2'] = [
                    '5' =>'各县'
                ];//选区域
                $fpid = substr($user['regionid'],0,2).'0000';
                $data['region_name'] = M('tregion')->where(['fid'=>$fpid])->getField('fname1');
                $data['region_name1'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//1
                $data['region_name2'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');
                break;
            case 5:
                $data['region_level'] = [
                    '5' =>'县级'
                ];//行政级别
                $data['region_level2'] = [
                    '5' =>'县级'
                ];//行政级别
                $data['region'] = [
                    '5' =>'县级'
                ];//选区域
                $data['region2'] = [
                    '5' =>'各县'
                ];//选区域
                $fpid = substr($user['regionid'],0,4).'00';
                $data['region_name'] = M('tregion')->where(['fid'=>$fpid])->getField('fname1');
                $data['region_name1'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//1
                $data['region_name2'] =M('tregion')->where(['fid'=>$fpid])->getField('fname1') ;
                break;
        }

        //版本0624
        /*switch ($region_level){
            case 0:
                $data['region_level'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市'
                ];//行政级别
                $data['region_level2'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市'
                ];//行政级别
                $data['region'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市'
                ];//选区域
                $data['region2'] = [
                    '1' =>'各省',
                    '2' =>'各副省级市',
                    '3' =>'各计划单列市'
                ];//选区域
                $data['region_name'] = '全国';
                $data['region_name1'] = '全国';
                $data['region_name2'] = '全国';
                break;
            case 1:
                $data['region_level'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市'
                ];//行政级别
                $data['region_level2'] = [
                    '2' =>'副省级市',
                    '3' =>'计划单列市'
                ];//行政级别
                $data['region'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市'
                ];//选区域
                $data['region2'] = [
                    '4' =>'各市',
                    '5' =>'各县'
                ];//选区域
                $data['region_name'] = '全国';//1
                $data['region_name1'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//1
                $data['region_name2'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//2
                break;
            case 2:
            case 3:
                $data['region_level'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市'
                ];//行政级别
                $data['region_level2'] = [
                    '5' =>'县级'
                ];//行政级别
                $data['region'] = [
                    '1' =>'省级',
                    '2' =>'副省级市',
                    '3' =>'计划单列市'
                ];//选区域
                $data['region2'] = [
                    '5' =>'各县'
                ];//选区域
                $data['region_name'] = '全国';//1
                $fpid = substr($user['regionid'],0,2).'0000';
                $data['region_name1'] = M('tregion')->where(['fid'=>$fpid])->getField('fname1');//1
                $data['region_name2'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//2
                break;
            case 4:
                $data['region_level'] = [
                    '4' =>'市级',
                    '5' =>'县级'
                ];//行政级别
                $data['region_level2'] = [
                    '5' =>'县级'
                ];//行政级别
                $data['region'] = [
                    '4' =>'市级',
                    '5' =>'县级'
                ];//选区域
                $data['region2'] = [
                    '5' =>'各县'
                ];//选区域
                $fpid = substr($user['regionid'],0,2).'0000';
                $data['region_name'] = M('tregion')->where(['fid'=>$fpid])->getField('fname1');
                $data['region_name1'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');//1
                $data['region_name2'] = M('tregion')->where(['fid'=>$user['regionid']])->getField('fname1');
                break;
            case 5:
                $data['region_level'] = [
                    '5' =>'县级'
                ];//行政级别
                break;
        }*/



        //1.fmedia_class_code媒体类别 2.fmediaownerid媒体 3.fregionid行政区划代码 4.fmediaid媒介5.fad_class_code广告内容类别代码
        //fmedia_id    fad_name   fregion_id
        $data['cross_region_rank'] = [
            'fad_name' =>'广告',
            'fregion_id' =>'地域',
            'fmedia_id' =>'媒体'
        ];//跨地域排名条件

        $data['media_class']['media_pclass'] = [
            '' => '全部媒体',
            '01' => '电视',
            '02' => '广播',
            '03' => '报纸'
        ];//选媒体

        $media_class_01['fpid'] = [['like','01%'],['neq',''],['neq',0]];
        $media_class_02['fpid'] = [['like','02%'],['neq',''],['neq',0]];
        $media_class_03['fpid'] = [['like','03%'],['neq',''],['neq',0]];


        $data['media_class']['01'] = M('tmediaclass')->where($media_class_01)->getField('fid,fclass',true);//选电视媒体细类
        $data['media_class']['02'] = M('tmediaclass')->where($media_class_02)->getField('fid,fclass',true);//选广播媒体细类
        $data['media_class']['03'] = M('tmediaclass')->where($media_class_03)->getField('fid,fclass',true);//选报纸媒体细类
        //选广告大类id
        $ad_class_id_map['fpcode'] = [['neq',''],['neq','0']];
        $ad_class_id = M('tadclass')->where($ad_class_id_map)->distinct(true)->getField('fpcode',true);

        //选广告大类名称
        $ad_class_map['fcode'] = ['IN',$ad_class_id];

        $ad_class = M('tadclass')->where($ad_class_map)->distinct(true)->order('fadclassid')->getField('fcode,fadclass',true);//选广告大类名称

        $ad_class_name = [];
        foreach ($ad_class as $key=>$val){
            $ad_class_name[intval($key)] = $val;
        }
        $data['ad_class'] = $ad_class_name;

        $data['onther_class'] = [
            'fad_illegal_times'=>'违法条次',
            'times_illegal_rate'=>'条次违法率',
            'fad_illegal_count'=>'违法条数',
            'counts_illegal_rate'=>'条数违法率',
            'fad_illegal_play_len'=>'违法时长',
            'lens_illegal_rate'=>'时长违法率',
            'com_score'=>'综合'
        ];//排名条件


        $this->ajaxReturn($data);
    }

    //生成数字
    public function create_number($num){
        $n = 0;
        $m = [];
        while ($n < $num){
            $n++;
            $m[] = $n;
        }
        return $m;
    }

    //生成半月
    public function create_month($half = true,$kv = true){
        $n = 0;
        $m = [];
        while ($n < 12){
            $n++;
            $str = $n;
            if($n<10){
                $str = '0'.$n;
            }
            if($kv){
                if($half){
                    $m[$str.'-01'] = $n.'月上半月';
                    if($n == 2){
                        $m[$str.'-15'] = $n.'月下半月';
                    }else{
                        $m[$str.'-16'] = $n.'月下半月';
                    }
                }else{
                    $m[$str.'-01'] = $n.'月';
                }
            }else{
                if($half){
                    $m[] = $str.'-01';
                    if($n == 2){
                        $m[] = $str.'-15';
                    }else{
                        $m[] = $str.'-16';
                    }
                }else{
                    $m[] = $str.'-01';
                }
            }

        }
        return $m;
    }

    //判断当前用户行政等级,并返回
    public function judgment_region($regionid){
        $level = M('tregion')->where(['fid'=>$regionid])->getField('flevel');
        /*        if($level == 2 || $level == 3){
                    $data['region'] = [
                        '1' =>'省',
                        '2' =>'副省级市',
                        '3' =>'计划单列市',
                        '4' =>'市',
                        '5' =>'区县'
                    ];//选区域

                    $data['region_level'] = [
                        '1' =>'全国',
                        '2' =>'省级',
                        '3' =>'市级',
                        '4' =>'县级'
                    ];//行政级别
                }*/
        return $level;
    }


    //获取行政等级下的所有id
    public function get_region_ids($region_level){
        $region_ids = M('tregion')->where(['flevel'=>$region_level])->getField('fid',true);
        return $region_ids;
    }

    //获取上个周期
    public function get_previous_cycles($times_table ='tbn_ad_summary_week',$year='2018',$table_condition='17'){
        if($times_table == 'tbn_ad_summary_week'){
            $year_mod = M('tbn_ad_summary_week')->distinct(true)->getField('fweek',true);
        }else{
            $year_mod = M($times_table)->distinct(true)->getField('fdate',true);
        }
        $years = [];
        foreach ($year_mod as $year_date){
            $years[] = substr($year_date,0,4);
        }
        $years = array_unique($years);//选年
        $select_half_year = [];
        foreach ($years as $v_half_year){
            $select_half_year[] = $v_half_year.'-01-01';//$v_year.'上半年'
            $select_half_year[] = $v_half_year.'-07-01';//$v_year.'下半年';
        }

        $select_year = [];
        foreach ($years as $v_year){
            $select_year[] = $v_year.'-01-01';
        }
        switch ($times_table){
            case 'tbn_ad_summary_week':
                $cycles = $this->create_number(52);
                break;
            case 'tbn_ad_summary_half_month':
                $cycles = $this->create_month(true,false);
                break;
            case 'tbn_ad_summary_month':
                $cycles = $this->create_month(false,false);
                break;
            case 'tbn_ad_summary_quarter':
                $cycles = ['01-01','04-01','07-01','10-01'];
                break;
            case 'tbn_ad_summary_half_year':
                $cycles = $select_half_year;
                break;
            case 'tbn_ad_summary_year':
                $cycles = $select_year;
                break;
        }

        $year_array = explode('-',$table_condition);
        $cycles_count = count($cycles);
        foreach ($cycles as $cy_key => $cycle){
            if($cycle == $table_condition){
                if($times_table == 'tbn_ad_summary_half_year' || $times_table == 'tbn_ad_summary_year'){
                    if($cy_key == 0){
                        $year_array[0] -= 1;
                        $prv = explode('-',$cycles[$cycles_count-1]);
                        return $year_array[0].'-'.$prv[1].'-'.$prv[2];
                    }else{
                        return $cycles[$cy_key-1];
                    }
                }
                if($cy_key == 0){
                    $year -= 1;
                    $condition = $cycles[$cycles_count-1];
                }else{
                    $condition = $cycles[$cy_key-1];
                }
                if($times_table == 'tbn_ad_summary_week'){
                    if($condition < 10){
                        $condition = '0'.$condition;
                    }
                    return $year.$condition;
                }else{
                    return $year.'-'.$condition;
                }

            }
        }
    }

    /**
     * 获取某年第几周的开始日期和结束日期
     * @param int $year
     * @param int $week 第几周;
     */
    public function weekday($year,$week=0){
        $year_start = mktime(0,0,0,1,1,$year);
        $year_end = mktime(0,0,0,12,31,$year);

        // 判断第一天是否为第一周的开始
        if (intval(date('W',$year_start))===1){
            $start = $year_start;//把第一天做为第一周的开始
        }else{
            $week++;
            $start = strtotime('+1 monday',$year_start);//把第一个周一作为开始
        }

        // 第几周的开始时间
        if ($week===1){
            $weekday['start'] = $start;
        }else{
            $weekday['start'] = strtotime('+'.($week-0).' monday',$start);
        }

        // 第几周的结束时间
        $weekday['end'] = strtotime('+1 sunday',$weekday['start']);
        if (date('Y',$weekday['end'])!=$year){
            $weekday['end'] = $year_end;
        }
        /*        dump(date('Y-m-d H:i:s',$weekday['start']));
                dump(date('Y-m-d H:i:s',$weekday['end']));
                exit;*/
        return $weekday;
    }

    //获取首末周期时间戳
    public function get_stemp($cycles='2018-01-01',$time_type='tbn_ad_summary_half_month'){
        switch($time_type){
            case 'tbn_ad_summary_half_month':
                $day = substr($cycles,-2);
                $month = substr($cycles,5,2);
                $year = substr($cycles,0,4);
                if($day == '01'){
                    $time_string = $year.'-'.$month.'-16';
                }else{
                    if($month == '12'){
                        $year = $year+1;
                        $time_string = $year.'-01-01';
                    }else{
                        $month = ($month+1) < 10 ? '0'.($month+1):($month+1);
                        $time_string = $year.'-'.$month.'-01';
                    }
                }
                break;
            case 'tbn_ad_summary_month':
                $month = substr($cycles,5,2);
                $year = substr($cycles,0,4);

                if($month == '12'){
                    $year = $year+1;
                    $time_string = $year.'-01-01';
                }else{
                    $month = ($month+1) < 10 ? '0'.($month+1):($month+1);
                    $time_string = $year.'-'.$month.'-01';
                }
                break;
            case 'tbn_ad_summary_quarter':
                $quarter = substr($cycles,5,2);
                $year = substr($cycles,0,4);
                if($quarter == '10'){
                    $year = $year+1;
                    $time_string = $year.'-01-01';
                }else{
                    $quarter = ($quarter+3) < 10 ? '0'.($quarter+3):($quarter+3);
                    $time_string = $year.'-'.$quarter.'-01';
                }
                break;
            case 'tbn_ad_summary_half_year':
                $half_year = substr($cycles,5,2);
                $year = substr($cycles,0,4);
                if($half_year == '06'){
                    $year = $year+1;
                    $time_string = $year.'-01-01';
                }else{
                    $time_string = $year.'-07-01';
                }
                break;
            case 'tbn_ad_summary_year':
                $year = substr($cycles,0,4);
                $year = $year+1;
                $time_string = $year.'-01-01';
                break;
        }
        $cycle['start'] = strtotime($cycles);
        $cycle['end'] = strtotime($time_string);
        return $cycle;
    }

}