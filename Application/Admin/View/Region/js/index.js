Vue.prototype.$ELEMENT = { size: 'small', zIndex: 3000 }
const vm = new Vue({
  el: '#app',
  data() {
    return {
      filterText: '',
      activeRow: {},
      drawer: false,
      form: {
        fid: '',
        fpid: '',
        fname: '',
        ffullname: '',
        flevel: ''
      },
      sourceForm: '',
      isEdit: false,

      regionTree: [],
      props: {
        label: 'label',
        children: 'children'
      },
      flevel: {
        '1': { label: "省级" },
        '2': { label: "副省级" },
        '3': { label: "计划单列市" },
        '4': { label: "地级市" },
        '5': { label: "区县级" },
      },
      rules: {
        fid: [{ required: true, message: '此项必填', trigger: 'blur' }],
        fname: [{ required: true, message: '此项必填', trigger: 'blur' }],
        ffullname: [{ required: true, message: '此项必填', trigger: 'blur' }],
        flevel: [{ required: true, message: '此项必选', trigger: 'change' }],
      }
    }
  },
  watch: {
    filterText(val) {
      this.$refs.tree.filter(val);
    }
  },
  created() {
    this.getRegionTree()
    this.sourceForm = JSON.stringify(this.form)
  },
  methods: {
    getRegionTree() {
      fly.post('/Admin/Region/get_region_tree').then(({ data }) => {
        if (data.code === 0) {
          this.regionTree = data.data
        }
      })
    },
    add(data) {
      this.isEdit = false
      this.nodeClick(data).then(res => {
        this.form.fpid = res.fid
        this.drawer = true
      })
    },
    edit(data) {
      this.isEdit = true
      this.nodeClick(data).then(res => {
        Object.keys(this.form).forEach(i => {
          this.form[i] = res[i]
        })
        this.drawer = true
      })
    },
    deleteR(data) {
      console.log(data)
      this.$confirm(`确认删除【${data.label}】？`, '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        fly.post('/Admin/Region/del', { fid: data.value }).then(({ data }) => {
          if (data.code === 0) {
            $h.notify.success(data.msg, this)
          } else {
            $h.notify.error(data.msg, this)
          }
        })
      }).catch(() => {})
    },
    onSubmit() {
      this.$refs.form.validate((valid) => {
        if (valid) {
          let data = {
            ...this.form
          }
          let url = this.isEdit ? '/Admin/Region/edit' : '/Admin/Region/add'
          fly.post(url, data).then(({ data }) => {
            if (data.code === 0) {
              this.getRegionTree()
              $h.notify.success(data.msg, this)
              this.handleClose()
            } else {
              $h.notify.error(data.msg, this)
            }
          })
        }
      })
    },
    nodeClick(data) {
      return new Promise((resolve, reject) => {
        let fid = data.value
        fly.post('/Admin/Region/ajax_region_details', { fid }).then(({ data }) => {
          if (data.code === 0) {
            this.activeRow = data.regionDetails
            resolve(data.regionDetails)
          } else {
            $h.notify.error(data.msg. this)
          }
        })
      })
    },
    handleClose() {
      this.drawer = false
      this.isEdit = false
      this.form = JSON.parse(this.sourceForm)
    },
    filterNode(value, data) {
      if (!value) return true
      return data.label.indexOf(value) !== -1 || data.value.indexOf(value) !== -1
    },
  },
})