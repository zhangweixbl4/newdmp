<?php
namespace Open\Controller;
use Think\Controller;
use Think\Exception;

class AiController extends BaseController {
	
	
	
	/* public function _initialize() {
		
		
		
		//$this->ajaxReturn(array('code'=>-100,'msg'=>'ERROR'));
	} */
	
	
	/*添加样本接口*/
	public function add_sample(){
		$post_data = file_get_contents('php://input');//获取post数据
		//file_put_contents('LOG/'.date('Ymd').'AiS',$post_data . "\n",FILE_APPEND);
		$post_data = json_decode($post_data,true);//把json转化成数组
		
		$samList = $post_data['samList'];
		$mediaId = A('Common/Media','Model')->getMainMediaId($post_data['mediaId']);//获取主媒体ID
		$ret = $this->add_sample_m($mediaId,$samList);
		
		$this->ajaxReturn(array('code'=>$ret['code'],'msg'=>$ret['msg']));

 
	}
	
	
	/*发布记录添加接口*/
	public function add_issue(){
		A('Common/System','Model')->api_call_log();
		set_time_limit(600);
		$msectime = msectime();
		session_write_close();
		$post_data = file_get_contents('php://input');//获取post数据
		//file_put_contents('LOG/'.date('Ymd').'AiI',$post_data . "\n",FILE_APPEND);
		$post_data = json_decode($post_data,true);//把json转化成数组
		
		$issueList = $post_data['issueList'];
		$mediaId = A('Common/Media','Model')->getMainMediaId($post_data['mediaId']);//获取主媒体ID
		
		$regionid = M('tmedia')
									->cache(true,600)
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->where(array('tmedia.fid'=>$mediaId))
									->getField('tmediaowner.fregionid');
		
		$mediaInfo = M('tmedia')
									->field('fmediaclassid')
		
									->cache(true,600)
									->where(array('fid'=>$mediaId))->find();

		if(substr($mediaInfo['fmediaclassid'],0,2) == '01'){
			$tb = 'tv';
		}elseif(substr($mediaInfo['fmediaclassid'],0,2) == '02'){
			$tb = 'bc';
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介ID不对'));
		}

		foreach($issueList as $i_key =>$issue){
			$w_issue_date_list[] = date('Y-m-d',intval($issue['start']));
		}	
		$w_issue_date_list = array_unique($w_issue_date_list);
		
		$row_weight_time = msectime();
		if($w_issue_date_list){
			$validateIssueList = M('t'.$tb.'issue')
												->where(array('fmediaid'=>$mediaId,'fissuedate'=>array('in',$w_issue_date_list)))
												->getField('fstarttime',true);//查询排重数据
		}
		$row_weight_time = msectime() - $row_weight_time;
		//var_dump($validateIssueList);
								
		foreach($validateIssueList as $validateIssue){
			
			$validateIssueL[strtotime($validateIssue)] = 1;//组装排重数组
		}

		
		$all_issue_data = [];
		$get_uuid_time = 0;
		foreach($issueList as $issue){

			#S($issue['uuid'].'_'.$mediaId,null);//清空
			
			if(S($issue['uuid'].'_'.$mediaId)){//判断是否有缓存
				$samInfo = S($issue['uuid'].'_'.$mediaId);
			}else{//如果没有缓存就去数据库查询
				$samInfo = M('t'.$tb.'sample')->field('fid')->where(array('uuid'=>$issue['uuid'],'fmediaid'=>$mediaId))->find();
				if($samInfo){
					S($issue['uuid'].'_'.$mediaId,$samInfo,5);//写入缓存
				}
			}
			
			$samInfo = M('t'.$tb.'sample')->field('fid')->where(array('uuid'=>$issue['uuid'],'fmediaid'=>$mediaId))->find();
			if(!$samInfo){//如果样本不存在
				$uuid_msectime = msectime();
				$getUuidUrl = C('AI_DOMAIN').'uuid/get_uuid';
				$getUuidData = array(
									'channel' => $mediaId,
									'sample_uuid' => $issue['uuid'],
									);
				$uuidInfo = json_decode(http($getUuidUrl,$getUuidData,'GET'),true);
				$get_uuid_time += msectime() - $uuid_msectime;
				$dmpLogs2 = A('Common/AliyunLog','Model')->dmpLogs2('getuuid',array(
																					'mediaId'=>$mediaId,
																					'uuid'=>$issue['uuid'],
																					'uuidInfo'=>json_encode($uuidInfo),
																				));
				$samList = $uuidInfo['samList'];
				$ret = $this->add_sample_m($mediaId,$samList);
				$samInfo['fid'] = $ret['sam_id'];
				if($samInfo){
					S($issue['uuid'].'_'.$mediaId,$samInfo,5);//写入缓存
				}
			}
			
			$FixedMediaState = A('Admin/FixedMediaData','Model')->get_state($mediaId,date('Y-m-d',$issue['start']));
		
			if($FixedMediaState == 2){
				file_put_contents('LOG/'.date('Ymd').'delIssueFixedMedia',date('Y-m-d H:i:s').'	'.$mediaId.'	'. date('Y-m-d H:i:s',$startTime) ."\n",FILE_APPEND);
				$dmpLogs2 = A('Common/AliyunLog','Model')->dmpLogs2('issue_error',array(
																							'mediaId'=>$mediaId,
																							'starttime'=>date('Y-m-d H:i:s',$issue['start']),
																							'endTime' =>date('Y-m-d H:i:s',$issue['end']),
																							'remark'=>'这一天该媒体已归档，不允许新增',
																							 ));		
				continue;
			}
			
			$a_data = array();
			$a_data['fmediaid'] = $mediaId;//媒介ID
			$a_data['f'.$tb.'sampleid'] = $samInfo['fid'];//样本ID
			
			$a_data['fissuedate'] = date('Y-m-d',$issue['start']);//发布日期
			$a_data['fstarttime'] = date('Y-m-d H:i:s',intval($issue['start']));//发布开始时间
			$a_data['fendtime'] = date('Y-m-d H:i:s',intval($issue['end']));//发布结束时间
			$a_data['flength'] = intval($issue['end']) - intval($issue['start']);//发布时长
			$a_data['faudiosimilar'] = intval($issue['score']);//相似度得分
			$a_data['fregionid'] = $regionid;//地区
			
			$a_data['fu0'] = ($issue['start'] * 1000) - (strtotime(date('Y-m-d',$issue['start'])) * 1000);
			$a_data['fduration'] = ($issue['end'] - $issue['start']) * 1000;
			
			
			
			$a_data['fcreator'] = 'Ai';//创建人
			$a_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_data['fmodifier'] = 'Ai';//修改人
			$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			if($a_data['flength'] < 3 || $a_data['flength'] > 86400){//判断时长是否合法
				$dmpLogs2 = A('Common/AliyunLog','Model')->dmpLogs2('issue_error',array(
																							'mediaId'=>$mediaId,
																							'starttime'=>date('Y-m-d H:i:s',$issue['start']),
																							'endTime' =>date('Y-m-d H:i:s',$issue['end']),
																							'remark'=>'时长不合法',
																							 ));		
				continue;
			}elseif($validateIssueL[intval($issue['start'])]){//数据重复，触发排重
				$dmpLogs2 = A('Common/AliyunLog','Model')->dmpLogs2('issue_error',array(
																							'mediaId'=>$mediaId,
																							'starttime'=>date('Y-m-d H:i:s',$issue['start']),
																							'endTime' =>date('Y-m-d H:i:s',$issue['end']),
																							'remark'=>'记录重复',
																							 ));
				continue;
				
			}elseif(intval($samInfo['fid']) == 0){//样本不存在
				$dmpLogs2 = A('Common/AliyunLog','Model')->dmpLogs2('issue_error',array(
																							'mediaId'=>$mediaId,
																							'starttime'=>date('Y-m-d H:i:s',$issue['start']),
																							'endTime' =>date('Y-m-d H:i:s',$issue['end']),
																							'uuid'=>$issue['uuid'],
																							'remark'=>'样本不存在',
																							 ));
				continue;
			}else{
				$all_issue_data[] = $a_data;
			}
			
			
			
			
		}
		$w_state = M('t'.$tb.'issue')->addAll($all_issue_data);//添加数据
		
		$ajaxReturn = array();
		$ajaxReturn['code'] = 0;
		$ajaxReturn['msg'] = '';
		$ajaxReturn['get_uuid_time'] = $get_uuid_time;
		$ajaxReturn['e_time'] = intval(msectime()) - intval($msectime);
		$ajaxReturn['w_state'] = $w_state;
		$ajaxReturn['data_count'] = count($all_issue_data);
		$ajaxReturn['w_issue_date_list'] = count($w_issue_date_list);
		$ajaxReturn['row_weight_time'] = $row_weight_time;


		
		
		
		$this->ajaxReturn($ajaxReturn);
		
	}



	/*添加样本*/
	private function add_sample_m($mediaId,$samList){
		$mediaInfo = M('tmedia')->field('fmediaclassid')->where(array('fid'=>$mediaId))->find();
		if(substr($mediaInfo['fmediaclassid'],0,2) == '01'){
			$tb = 'tv';
		}elseif(substr($mediaInfo['fmediaclassid'],0,2) == '02'){
			$tb = 'bc';
		}else{
			return (array('code'=>-1,'msg'=>'媒介ID不对'));
		}
		foreach($samList as $sam){
			$a_data = array();
			$a_data['fmediaid'] = $mediaId;//媒介ID
			$a_data['fissuedate'] = date('Y-m-d',$sam['start']);//发布日期
			$a_data['last_issue_date'] = date('Y-m-d',$sam['start']);//最后发布日期
			$a_data['sstarttime'] = date('Y-m-d H:i:s',$sam['start']);//开始时间
			$a_data['sendtime'] = date('Y-m-d H:i:s',$sam['end']);//结束时间
            // 根据临时广告名称查找已确认广告id, 如果找得到就直接赋给样本, 没有的话样本广告id为0
            $adId = M('tad')
                ->where([
                    'fadname' => ['EQ', $sam['title']],
                    'is_sure' => ['NEQ', 0],
                    'fstate'  => ['GT', 0],
                ])
                ->cache(3)
                ->order('fmodifytime desc')
                ->getField('fadid');
			if($adId){
				$a_data['fadid'] = $adId;#广告id
			}else{
				$a_data['fadid'] = 0;
			}
			if($sam['ad_in_show']){ #是否长广告
				$a_data['is_long_ad'] = 1; #是长广告
			}else{
				$a_data['is_long_ad'] = 0; #非长广告
			}
			$a_data['favifilename'] = $sam['video_path'];//视频路径
			$a_data['favifilepng'] = $sam['img_path'];//缩略图路径
			if(strstr($sam['video_path'],'http://datamanagerplatform.finesoft.com.cn/')){//判断传过来的地址是否七牛地址
				$a_data['fcuted'] = 2;//上传状态
				if(!$a_data['favifilepng']){
					$a_data['favifilepng'] = $sam['video_path'].'?vframe/jpg/offset/2/h/100';//视频缩略图
				}
			}else{
				$a_data['fcuted'] = 3;//上传状态
			}
			$a_data['fadlen'] = intval($sam['end']) - intval($sam['start']);//样本时长
			$a_data['finputstate'] = 0;//录入状态
			$a_data['finspectstate'] = 0;//判断状态
			if( $sam['video_path'] == ''){
				$a_data['fsourcefilename'] = $sam['start'].','.$sam['end'];//用于存储开始时间、结束时间
			}
			$a_data['fcreator'] = 'Ai_'.$sam['creator'];//创建人
			$a_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_data['fmodifier'] = 'Ai_'.$sam['creator'];//修改人
			$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$a_data['tem_ad_name'] = $sam['title'];//临时广告名称
			$a_data['fversion'] = $sam['ver_info']; #版本说明
			$a_data['fspokesman'] = $sam['ad_speaker']; #代言人
			$a_data['uuid'] = $sam['uuid'];//唯一ID
			if($a_data['fadlen'] < 3 || $a_data['fadlen'] > 86400){//判断时长是否合法
				$dmpLogs2 = A('Common/AliyunLog','Model')->dmpLogs2('sample_adlen_error',$a_data);		
				continue;
			}
			if(M('t'.$tb.'sample')->where(array('uuid'=>$sam['uuid'],'fmediaid'=>$mediaId))->count() == 0){
				$add_sam_id = M('t'.$tb.'sample')->add($a_data);//添加数据
				$needCreateTask = false;
				if($a_data['fcuted'] == 2 && $add_sam_id > 0) $needCreateTask = true;
				if(strstr($sam['title'],'报时') && $a_data['fadid']) $needCreateTask = false;
				// if(strstr($sam['title'],'公益') && $a_data['fadid']) $needCreateTask = false;
				if(strstr($sam['title'],'节目预告') && $a_data['fadid']) $needCreateTask = false;
				if(strstr($sam['title'],'本媒体宣传') && $a_data['fadid']) $needCreateTask = false;
				if($needCreateTask){
					$taskid = A('Common/InputTask','Model')->add_task(intval(substr($mediaInfo['fmediaclassid'],0,2)),$add_sam_id);//添加到任务表
				}
				#$add_issue_data = array();
				#$add_issue_data['mediaId'] = $mediaId;
				#$add_issue_data['issueList'][] = array('start'=>$sam['start'],'end'=>$sam['end'],'uuid'=>$sam['uuid'],'score'=>100);
				#$add_issue_ret = http(U('Open/Ai/add_issue@'.$_SERVER['HTTP_HOST']),json_encode($add_issue_data),'POST',false,30);//向添加发布数据接口推送数据
				//file_put_contents('LOG/add_issue_ret',$add_issue_ret."\n",FILE_APPEND);
			}
		}
		if(count($samList) === 1){
			$sam_id = $add_sam_id;
		}
		return (array('code'=>0,'msg'=>'','sam_id'=>$sam_id));
	}
	
	/*获取媒介负责人电话*/
	public function get_media_linkman(){
		$post_data = file_get_contents('php://input');//获取post数据
		//file_put_contents('LOG/'.date('Ymd').'AiI',$post_data . "\n",FILE_APPEND);
		$post_data = json_decode($post_data,true);//把json转化成数组
		
		$media_id = $post_data['media_id'];
		$where = array();
		$where['tmedia.fid'] = $media_id; 
		$linkman = M('tmedia')
								->field('tmedia.fid as media_id , tcollect.fmanager as linkman , tcollect.ftel as tel')
								->join('tdevice on tdevice.fid = tmedia.fdeviceid')
								->join('tcollect on tcollect.fid = tdevice.fcollectid')
								->where($where)
								->find();
								
		if(!$linkman){
			$linkman = array();
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'','linkman'=>$linkman));
		
		
	}
	
	
	
	
	
	
	/*各媒介发布数量*/
	public function media_count_propell(){
		set_time_limit(0);
		$post_data = file_get_contents('php://input');//接收请求过来的数据
		
		//file_put_contents('LOG/media_count_propell',$post_data);//写入文件日志
		$post_data = json_decode($post_data,true);//转为数组
		foreach($post_data as $data){//循环数组
			
			 A('Api/Media','Model')->media_issue_count($data['channel'],$data['date'],array('ai_issue_count'=>$data['num']));//写入ai发布数量
			
		}

		$this->ajaxReturn(array('code'=>0,'msg'=>''));
		
		
	}
	
	
	/*删除发布记录*/
	public function del_issue(){
		A('Common/System','Model')->api_call_log();
		set_time_limit(600);
		session_write_close();
		$white_list = array(
							'118.31.57.30',
							'47.98.56.40',
							'116.62.79.116',
							'120.27.220.174',
							'172.16.238.171',
							'47.96.182.117',
							'172.16.198.126',
							'47.96.182.117',
							'47.96.163.56',
							'172.16.0.0/16',
							'192.168.0.0/16',
							'127.0.0.1',
							);
							
		$get_client_ip = get_client_ip();//请求端的IP
		
		
		$au = false; #IP是否经过授权
		foreach($white_list as $white_ip){
			$au = ip_in_network($get_client_ip,$white_ip);
			if($au) break;
		}
		
		if(!$au){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'没有权限')); 
		}
		
		
		$post_data = file_get_contents('php://input');//接收请求过来的数据
		
	
		$post_data = json_decode($post_data,true);//转为数组
		$mediaId = A('Common/Media','Model')->getMainMediaId($post_data['mediaId']);//媒介id
		$startTime = intval($post_data['startTime']);//开始时间
		
		$FixedMediaState = A('Admin/FixedMediaData','Model')->get_state($mediaId,date('Y-m-d',$startTime));
		
		if($FixedMediaState == 2){
			file_put_contents('LOG/'.date('Ymd').'delIssueFixedMedia',date('Y-m-d H:i:s').'	'.$mediaId.'	'. date('Y-m-d H:i:s',$startTime) ."\n",FILE_APPEND);
		
			$this->ajaxReturn(array('code'=>3,'msg'=>'这一天的数据已经归档,不允许删除'));
		}
		
		$is_h_ill = M('tbn_illegal_ad_issue')->where(array('fmedia_id'=>$mediaId,'fstarttime'=>date('Y-m-d H:i:s',$startTime)))->count();
		if($is_h_ill > 0){
			file_put_contents('LOG/'.date('Ymd').'delIssueIllegal',date('Y-m-d H:i:s').'	'.$mediaId.'	'. date('Y-m-d H:i:s',$startTime) ."\n",FILE_APPEND);
		
			$this->ajaxReturn(array('code'=>3,'msg'=>'这条数据涉及违法,不允许删除'));
		}
		$mediaInfo = M('tmedia')
								->cache(true,600)
								->field('tmedia.fid,left(tmedia.fmediaclassid,2) as media_class,tmediaowner.fregionid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>$mediaId))->find();
		if(!$mediaInfo){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'没有找到该媒介')); 
		}
		$provinceId = substr($mediaInfo['fregionid'],0,2);

		$monthCode = date('Ym',$startTime);
		if($mediaInfo['media_class'] == '01'){
			$media_class_tab = 'tv';
		}elseif($mediaInfo['media_class'] == '02'){
			$media_class_tab = 'bc';
		}
		if(!$media_class_tab){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介ID错误')); 
		}
		M()->startTrans();//开启事务方法
		try{
			$del_old = M('t'.$media_class_tab.'issue')->where(array('fmediaid'=>$mediaId,'fstarttime'=>date('Y-m-d H:i:s',$startTime)))->limit(1)->delete();
			$del_new = M('t'.$media_class_tab.'issue_'.$monthCode.'_'.$provinceId)->where(array('fmediaid'=>$mediaId,'fstarttime'=>$startTime))->limit(1)->delete();			
			M()->commit();//事务提交方法
		}catch(Exception $error) {
			$del_old = 0;
			$del_new = 0;
			M()->rollback();
		} 
		
		
		
		file_put_contents('LOG/'.date('Ymd').'delIssue',date('Y-m-d H:i:s').'	'.$mediaId.'	'.date('Y-m-d H:i:s',$startTime).'	'.$del_old.'	'.$del_new."\n",FILE_APPEND);
		
		if($del_old > 0 || $del_new > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'')); 
		}else{
			$this->ajaxReturn(array('code'=>2,'msg'=>'删除了0条记录')); 
		}
		
	}

	#修改样本状态
	public function edit_sam_state(){
		$mediaId = I('mediaId');
		$uuid = I('uuid');
		$state = I('state');
		if(!in_array($state,array('0','1'))) $this->ajaxReturn(array('code'=>-1,'msg'=>'state Error'));
		$mediaId = A('Common/Media','Model')->getMainMediaId($mediaId);//媒介id
		if(!$mediaId) $this->ajaxReturn(array('code'=>-1,'msg'=>'mediaId Error'));
		
		$mediaClass = M('tmedia')->where(array('fid' => $mediaId))->getField('fmediaclassid');
		$mediaClass = substr($mediaClass, 0, 2);
		
		if($mediaClass == '01'){
			$tb = 'tv';
		}elseif($mediaClass == '02'){
			$tb = 'bc';
		}else{
			$tb = '';
		}	
		if(!$tb){
			$this->ajaxReturn(array('code' => -1,'msg' => '媒介id错误'));
		}
		
		$editRow = M('t'.$tb.'sample')->where(array('fmediaid'=>$mediaId,'uuid'=>$uuid))->limit(1)->save(array('fstate'=>$state));
		#var_dump(M('t'.$tb.'sample')->getLastSql());
		$this->ajaxReturn(array('code'=>0,'editRow'=>$editRow,'msg'=>''));
		
		
		
	}


    /**
     * 删除样本及发布记录
     * @param $mediaId
     * @param $uuid
     */
    public function deleteSampleAndIssue($mediaId, $uuid)
    {
        A('Common/System','Model')->api_call_log();
		$this->ajaxReturn(['code' => -1,'msg' => '不再允许删除样本']);
		set_time_limit(600);
		session_write_close();
		$mediaId = A('Common/Media','Model')->getMainMediaId($mediaId);//媒介id
		$mediaClass = M('tmedia')->where(array('fid' => $mediaId))->getField('fmediaclassid');
		$mediaClass = substr($mediaClass, 0, 2);
		
		if($mediaClass == '01'){
			$mediaClass = 'tv';
		}elseif($mediaClass == '02'){
			$mediaClass = 'bc';
		}else{
			$mediaClass = '';
		}	
		if(!$mediaClass){
			$this->ajaxReturn(['code' => -1,'msg' => '删除失败,媒介id不正确,'.M('tmedia')->getLastSql()]);
		}
		$model = M('t' . $mediaClass . 'sample');
		$where = [
			'uuid' => ['EQ', $uuid],
			'fmediaid' => ['EQ', $mediaId],
		];
		// 获取发布记录
		$sampleInfo = $model->field('fid, issue_relation,fissuedate')->where($where)->find();

		
		$sampleId = $sampleInfo['fid'];
		$issue_relation = $sampleInfo['issue_relation'];
		$issue_relation = explode(',', $issue_relation);
		$issue_relation = array_filter($issue_relation);

		$sampleIdList = $model->where($where)->getField('fid',true);//查询符合条件的样本id列表

		M()->startTrans();//开启事务方法
		
		$sampleDelete = $model->where($where)->delete();// 删除样本
		$del_s_num = 0;
		$del_l_num = 0;
		if($sampleIdList){
			foreach ($issue_relation as $item) {
				$item = trim($item, ',');
				try{//
					$del_s_num += M($item)->where(array('fsampleid'=>array('in',$sampleIdList)))->delete();//删除发布表中的记录（小表）
				}catch(Exception $error) { //
					
				} 
				
				
			}
			
			$del_l_num = M('t'.$mediaClass.'issue')->where(['f'.$mediaClass.'sampleid' => ['in', $sampleIdList]])->delete();// 删除发布表中的记录（大表）
			S($uuid.'_'.$mediaId,null);//清除缓存，新增发布记录会用到这个缓存
		}
		M()->commit();//事务提交方法
		
		file_put_contents('LOG/'.date('Ymd').'delSamIssue',date('Y-m-d H:i:s').'	'.$mediaId.'	'.$uuid.'	'.$sampleDelete.'	'.$del_s_num.'	'.$del_s_num."\n",FILE_APPEND);

		$this->ajaxReturn(['code' => 0,'msg' => '删除成功']);
        

	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	* 媒介日期数据是否处理完成
	* @Param	int $media_id 媒介ID
	* @Param	string $issue_date 发布日期 如：2018-07-23
	* @Return	string $data 返回的数据
	*/
	public function setIsReady(){
		$input = file_get_contents('php://input');	//获取请求数据
		$post_data = json_decode($input,true);		//把json转化成数组
		$regulatorList = M('open_token')			//需获取接口数据的客户机构列表
			->field('fid,fuserid,fstate')
			->where(array('fstate'=>0))
			->select();
		if (!empty($post_data)){
			if (count($post_data) == count($post_data,COUNT_RECURSIVE)){
				//一维即单条数据
				$where['media_id'] = $post_data['media_id'];
				$where['issue_date'] = $post_data['issue_date'];
				$is_ready = $post_data['is_ready'];
				$ret = M('media_issue_count')
					->where($where)
					->save(array('is_ready'=>$is_ready));
				if ($ret){
					$fmediaid = $post_data['media_id'];
					$fissuedate = strtotime($post_data['issue_date']);
					//同时记录插入tapimedialist表
					if ($is_ready == '1'){
						$row['fmediaid'] = $fmediaid;
						$row['fissuedate'] = $fissuedate;
						$row['fstatus'] = 0;//默认为0-未获取状态
						$row['fcreator'] = 'Ai';
						$row['fcreatetime'] = date('Y-m-d H:i:s');
						foreach ($regulatorList as $k=>$v){
							$row['fuserid'] = $v['fuserid'];	//每个机构用户
							$result = M('tapimedialist')->add($row);
						}
					}elseif ($is_ready == '0'){
						//Ai撤销就绪状态
						$con['fmediaid'] = $fmediaid;
						$con['fissuedate'] = $fissuedate;
						$con['fstatus'] = 0;
						$row['fstatus'] = -1;//为0-未获取状态的记录改为删除状态
						$row['fmodifier'] = 'Ai';
						$row['fmodifytime'] = date('Y-m-d H:i:s');
						foreach ($regulatorList as $k=>$v){
							$con['fuserid'] = $v['fuserid'];//每个机构用户
							$result = M('tapimedialist')
								->where($con)
								->save($row);
						}
					}
					$this->ajaxReturn(array('code'=>0,'msg'=>'设置就绪状态成功'));
				}else{
					$this->ajaxReturn(array('code'=>1,'msg'=>'设置就绪状态失败'));
				}
			}elseif(count($post_data) < 100){
				set_time_limit(300);
				//多维即批量数据
				$totals = count($post_data);
				$succ = 0;	//成功条数
				$rep = 0;	//重复忽略条数
				$err = 0;	//失败条数
				foreach ($post_data as $datarow){
					$where['media_id'] = $datarow['media_id'];
					$where['issue_date'] = $datarow['issue_date'];
					$is_ready = $datarow['is_ready'];
					$ret = M('media_issue_count')
						->where($where)
						->save(array('is_ready'=>$is_ready));
					if ($ret){
						//同时记录插入tapimedialist表
						$fmediaid = $datarow['media_id'];
						$fissuedate = strtotime($datarow['issue_date']);
						if ($is_ready == '1'){
							//Ai推送就绪媒介列表
							$row['fmediaid'] = $fmediaid;
							$row['fissuedate'] = $fissuedate;
							$row['fstatus'] = 0;//默认为0-未获取状态
							$row['fcreator'] = 'Ai';
							$row['fcreatetime'] = date('Y-m-d H:i:s');
							foreach ($regulatorList as $k=>$v){
								$row['fuserid'] = $v['fuserid'];	//每个机构用户
								$result = M('tapimedialist')->add($row);
							}
							if ($result) $succ++;
						}elseif($is_ready == '0'){
							//Ai撤销就绪状态
							$con['fmediaid'] = $fmediaid;
							$con['fissuedate'] = $fissuedate;
							$con['fstatus'] = 0;
							$row['fstatus'] = -1;//为0-未获取状态的记录改为删除状态
							$row['fmodifier'] = 'Ai';
							$row['fmodifytime'] = date('Y-m-d H:i:s');
							$count = M('tapimedialist')
								->where($con)
								->count();
							if ($count > 0){
								foreach ($regulatorList as $k=>$v){
									$con['fuserid'] = $v['fuserid'];	//每个机构用户
									$result = M('tapimedialist')
										->where($con)
										->save($row);
								}
								if ($result) $succ++;
							}else{
								$succ++;
							}
						}else{
							$rep++;
						}
					}else{
						$err++;
					}
				}
				if ($succ > 0){
					$this->ajaxReturn(array('code'=>0,'msg'=>'设置就绪状态成功,共:'.$succ.'条'));
				}elseif($rep > 0){
					$this->ajaxReturn(array('code'=>0,'msg'=>'重复已忽略的记录,共:'.$rep.'条'));
				}else{
					$this->ajaxReturn(array('code'=>1,'msg'=>'设置就绪状态失败,共:'.$err.'条'));
				}
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'批量设置请不要超过100条记录'));
			}
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}
	}
	
	
	
	
	/*查询单个媒体一天的的发布记录*/
	public function get_media_issue(){
		A('Common/System','Model')->api_call_log();
		
		set_time_limit(600);
		session_write_close();
		
		$mediaId = I('mediaId');//媒介id
		$date = I('date');//发布日期
		A('Api/SynIssue','Model')->check_issue($mediaId,$date);
		$get_client_ip = get_client_ip();
		
		/* 
		$getCacheKey = md5('get_media_issue'.$mediaId.$date.$get_client_ip);
		if(I('f') == '1') S($getCacheKey,null);
		
		if(S($getCacheKey)){
			$this->ajaxReturn(array('code'=>199,'msg'=>'请求太频繁，请稍后再试','issueList'=>[]));//返回数据
		}
		S($getCacheKey,1,300);
		*/
		$mediaInfo = M('tmedia')
								->cache(true, 60)
								->field('tmedia.fid , tmedia.fmediaclassid , tmediaowner.fregionid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								
								->where(array('tmedia.fid' => $mediaId))
								->find();
		$mediaClass = substr($mediaInfo['fmediaclassid'],0,2);//媒介类型
		


		
		
		if($mediaClass == '01'){
			$mediaClass = 'tv';
		}elseif($mediaClass == '02'){
			$mediaClass = 'bc';
		}else{
			$mediaClass = '';
		}	
		if(!$mediaClass){
			$this->ajaxReturn(['code' => -1,'msg' => '查询失败,媒介id不正确']);
		}
		
		$tableName = 't'.$mediaClass.'issue_'.date('Ym',strtotime($date)).'_'.substr($mediaInfo['fregionid'],0,2);//发布记录的表名称
		$priorTableName = 't'.$mediaClass.'issue_'.date('Ym',strtotime($date)-86400).'_'.substr($mediaInfo['fregionid'],0,2);//发布记录的表名称
		
		
		try{//
			$issueListTem = M($tableName)
									->alias('issue_tab')
									->field('fid,fsampleid,fstarttime,fendtime,fcreatetime,"'.$tableName.'" as tableName,faudiosimilar')
									->where(array('issue_tab.fmediaid'=>$mediaId,'issue_tab.fissuedate'=>strtotime($date)))
									->select();//发布记录列表 	
		}catch( \Exception $error) { //
			$issueListTem = [];
		}
		
		try{//
			$priorIssueListTem = M($priorTableName)
									->alias('issue_tab')
									->field('fid,fsampleid,fstarttime,fendtime,fcreatetime,"'.$priorTableName.'" as tableName,faudiosimilar')
									->where(array('issue_tab.fmediaid'=>$mediaId,'issue_tab.fissuedate'=>strtotime($date)-86400,'issue_tab.fendtime'=>array('gt',strtotime($date))))
									->select();//前一天的跨天发布记录列表 	
		}catch( \Exception $error) { //
			$priorIssueListTem = [];
		}
		
		
		
		
									
							
		
									
		$issueListTem = array_merge($priorIssueListTem,$issueListTem);							
								

		$issueList = array();
		foreach($issueListTem as $issueTem){
			$samInfo = M('t'.$mediaClass.'sample')->alias('tsam')
											->cache(true,20)
											->field('uuid,(select fadname from tad where fadid = tsam.fadid) as adName')
											->where(array('fid'=>$issueTem['fsampleid'],'fstate'=>array('in','0,1')))
											->find();//样本uuid
			
			if(!$samInfo['uuid']){ #如果没有查到样本
				M()->startTrans();//开启事务方法	
				M($issueTem['tableName'])->where(array('fid'=>$issueTem['fid']))->delete();//删除无法关联样本的发布记录（小表）
				M('t'.$mediaClass.'issue')
										->where(array(
														'fmediaid'=>$mediaId,
														'fissuedate'=>date('Y-m-d',$issueTem['fstarttime']),
														'fstarttime'=>date('Y-m-d H:i:s',$issueTem['fstarttime']),
													))
										->delete();# 删除无法关联样本的发布记录（大表）
				M()->commit();//事务提交方法
				continue;//如果查不到样本id，则退出循环
			}	

			$issueList[] = array(
									'start'=>$issueTem['fstarttime'],
									'end'=>$issueTem['fendtime'],
									'similarity'=>$issueTem['faudiosimilar'],
									'samUuid'=>$samInfo['uuid'],
									'adName'=>$samInfo['adName'],
									);
			
		}

		$this->ajaxReturn(array('code'=>0,'msg'=>'','issueList'=>$issueList));//返回数据

		
			
		
	}
	
	/*发送警报短信*/
	public function send_alarm_sms(){
		
		$postData = file_get_contents('php://input');
		
		$postData = json_decode($postData,true);
		
		foreach($postData['mobile_list'] as $mobile){
			//var_dump($mobile);
			$ret = A('Common/Alitongxin','Model')->alarm(strval($mobile),$postData['title'],$postData['time'],$postData['content']);
			//var_dump($ret);
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>''));
	}
	
	
	/*通过UUID查询广告信息*/
	public function uuid_sam_info(){
		A('Common/System','Model')->api_call_log();
		$postData = file_get_contents('php://input');
		$postData = json_decode($postData,true);
		$uuidList = $postData['uuidList'];
		
		if(count($uuidList) == 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'uuid error'));
		}
		
		$samListTv = M('ttvsample')
								->field('
											ttvsample.fmediaid as mediaId,
											ttvsample.fadid,
											tad.fadname as adName,
											tad.fadclasscode,
											tad.fadclasscode_v2,
											tad.fbrand,
											tad.fadowner,
											(select task_state from task_input where media_class = 1 and sam_uuid = ttvsample.uuid limit 1) as task_state,
											ttvsample.fillegaltypecode as illegalType,
											ttvsample.fillegalcontent as illegal,
											ttvsample.fexpressioncodes as illegalCodes,
											ttvsample.uuid,
											ttvsample.fspokesman,
											ttvsample.fversion,
											ttvsample.is_long_ad,
											ttvsample.favifilename as source,
											ttvsample.sstarttime as startTime,
											ttvsample.sendtime as endTime
											
										')
								->join('tad on tad.fadid = ttvsample.fadid')
								->where(array('ttvsample.fstate'=>array('in','0,1'),'ttvsample.uuid'=>array('in',$uuidList)))
								->select();
		$samListBc = M('tbcsample')
								->field('
											tbcsample.fmediaid as mediaId,
											tbcsample.fadid,
											tad.fadname as adName,
											tad.fadclasscode,
											tad.fadclasscode_v2,
											tad.fbrand,
											tad.fadowner,
											(select task_state from task_input where media_class = 2 and sam_uuid = tbcsample.uuid limit 1) as task_state,
											tbcsample.fillegaltypecode as illegalType,
											tbcsample.fillegalcontent as illegal,
											tbcsample.fexpressioncodes as illegalCodes,
											tbcsample.uuid,
											tbcsample.fspokesman,
											tbcsample.fversion,
											tbcsample.is_long_ad,
											tbcsample.favifilename as source,
											tbcsample.sstarttime as startTime,
											tbcsample.sendtime as endTime
											
										')
								->join('tad on tad.fadid = tbcsample.fadid')
								->where(array('tbcsample.fstate'=>array('in','0,1'),'tbcsample.uuid'=>array('in',$uuidList)))
								->select();
		$samList = array_merge($samListTv,$samListBc);
		
		
		$taskList = M('task_input')
								->field('sam_uuid,cut_err_state,task_state')
								->where(array('cut_err_state'=>0,'sam_uuid'=>array('in',$uuidList)))
								->select();
		
		#var_dump($taskList);
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','samList'=>$samList,'taskList'=>$taskList));
		
	}
	
	
	/*媒体天状态发生变化*/
	public function mdc(){
		$mediaId = I('mediaId');
		$date = I('date');
		$last_change_time = intval(I('last_change_time')); #数据最后变动时间
		
		if($last_change_time){
			M('tinspect_plan','',C('ZIZHI_DB'))->where(array('fmedia_id'=>$mediaId,'fissue_date'=>$date))->save(array('last_change_time'=>$last_change_time));
		}
		
		$available_time2 = A('Common/Media','Model')->media_issue_data_state($mediaId,$date);
				
		if($available_time2['state'] == 1){
			A('Admin/FixedMediaData','Model')->fixed($mediaId,$date,2,'收到数据状态变动通知，并且符合归档条件,'.$available_time2['remark'],'system'); #归档状态修改
		}else{
			#A('Admin/FixedMediaData','Model')->fixed($mediaId,$date,99,'收到数据状态变动通知，但不符合归档条件,'.$available_time2['remark'],'system'); #归档状态修改
		
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>''));
	}
	/**
     * @Des: 测试-创建任务
     * @Edt: yuhou.wang
     * @Date: 2020-02-13 22:25:16
     * @param {type} 
     * @return: 
     */
    public function add_task($media_class,$sam_id){
		$ret = A('Common/InputTask','Model')->add_task($media_class,$sam_id);
		$this->ajaxReturn($ret);
	}
}