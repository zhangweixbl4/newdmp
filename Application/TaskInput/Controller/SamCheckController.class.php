<?php
namespace TaskInput\Controller;
use Think\Controller;
class SamCheckController extends Controller {
	
	
	
	/*列表页*/
    public function index(){
		session_write_close();
		$this->display();
	}

	/*数据列表接口*/
	public function dataList(){
		$p = intval(I('p')); #页码
		if ($p <= 0) $p = 1;
		
		$pp = intval(I('pp')); #每页显示条数
		if ($pp <= 0) $pp = 1;
		if ($pp > 100) $pp = 100;
		
		
		$where = array();
		if(I('fmedianame')) $where['tmedia.fmedianame'] = array('like','%'.I('fmedianame').'%');
		if(I('fstate') != '') $where['sam_check.fstate'] = I('fstate');
		if(I('onlyillegal') == '1') $where['sam_check.illegal_sam_count'] = array('gt',0);
		$where['sam_check.fdate'] = array('between',I('fdate'));
		$where['sam_count'] = array('egt',0);
		$dataCount = M('sam_check')
								
								->cache(true,120)

								->join('tmedia on tmedia.fid = sam_check.fmediaid')
								->where($where)
								->count();
		#var_dump(M('sam_check')->getLastSql());
		$dataList = M('sam_check')
								
								->cache(true,30)
								->field('
										sam_check.*
										,tmedia.fmedianame
										,tmedia.media_region_id
										
											')
								->join('tmedia on tmedia.fid = sam_check.fmediaid')
								->where($where)
								->page($p,$pp)
								->select();
								
								
		$this->ajaxReturn(array('code'=>0,'msg'=>'','dataCount'=>$dataCount,'dataList'=>$dataList));
		
	}
	
	/*媒体一天的数据*/
	public function mediaDay(){
		
		$fid = I('fid');
		$mediaDay = M('sam_check')
								
								->cache(true,30)
								->field('
										sam_check.*
										,tmedia.fmedianame
										,left(tmedia.fmediaclassid,2) as media_class
										,tmedia.media_region_id
										
											')
								->join('tmedia on tmedia.fid = sam_check.fmediaid')
								->where(array('sam_check.fid'=>$fid))
								
								->find();
		$timestampDay = strtotime($mediaDay['fdate']);
		$m3u8_24h = A('Common/Media','Model')->get_m3u8($mediaDay['fmediaid'],$timestampDay,$timestampDay+86400); 

		
		if ($mediaDay['media_class'] == '01'){
			$tab = 'tv';
		}elseif($mediaDay['media_class'] == '02'){
			$tab = 'bc';
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒体数据错误'));
		}
		
		$issueTabName = 't'.$tab.'issue_'.date('Ym',$timestampDay).'_'.substr($mediaDay['media_region_id'],0,2);
		
		

		try{
			$samList = M($issueTabName)
									->cache(true,60)
									->alias('tissue')
									->field('
												tsample.fid
												,tsample.fillegaltypecode
												,tsample.fversion
												,tsample.fadlen
												,(select fadname from tad where fadid = tsample.fadid) as fadname
												,(select ffullname from tad join tadclass on tadclass.fcode = tad.fadclasscode where tad.fadid = tsample.fadid) as fadclass
												,date(tsample.fissuedate) as samissuedate
												,count(*) as issuecount
												
												
												')
									->join('t'.$tab.'sample as tsample on tsample.fid = tissue.fsampleid')
									->where(array('tissue.fissuedate'=>$timestampDay,'tissue.fmediaid'=>$mediaDay['fmediaid']))
									->group('tsample.fid')
									->select();
		}catch( \Exception $error){
			$samList = [];
		}
								
		$sam_count = 0;
		$today_sam_count = 0;
		$issue_count = 0;
		$illegal_sam_count = 0;
		
		foreach($samList as $sam){
			$sam_count += 1;
			$issue_count += $sam['issuecount'];
			if ($sam['samissuedate'] == $mediaDay['fdate']) $today_sam_count += 1;
			if ($sam['fillegaltypecode'] > 0) $illegal_sam_count += 1;
			
		}
		
		$mediaDay['sam_count'] = $sam_count;
		$mediaDay['today_sam_count'] = $today_sam_count;
		$mediaDay['issue_count'] = $issue_count;
		$mediaDay['illegal_sam_count'] = $illegal_sam_count;
		
		
		
		
		M('sam_check')->where(array('fid'=>$fid))->save(array(
																'sam_count'=>$sam_count,
																'today_sam_count'=>$today_sam_count,
																'issue_count'=>$issue_count,
																'illegal_sam_count'=>$illegal_sam_count
																
																));
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','mediaDay'=>$mediaDay,'m3u8_24h'=>$m3u8_24h,'samList'=>$samList));
		
		
		
		
	}
	
	/*媒体某一天的某一个样本的发布记录数据*/
	public function samDay(){
		$samId = I('samId');
		$mediaId = I('mediaId');
		$fdate = I('fdate');
		
		$mediaInfo = M('tmedia')
								->cache(true,30)
								->field('
										tmedia.fid
										,tmedia.fmedianame
										,left(tmedia.fmediaclassid,2) as media_class
										,tmedia.media_region_id
										')
								->where(array('tmedia.fid'=>$mediaId))
								->find();
		
		$timestampDay = strtotime($fdate);
		

		
		if ($mediaInfo['media_class'] == '01'){
			$tab = 'tv';
		}elseif($mediaInfo['media_class'] == '02'){
			$tab = 'bc';
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒体数据错误'));
		}
		
		$issueTabName = 't'.$tab.'issue_'.date('Ym',$timestampDay).'_'.substr($mediaInfo['media_region_id'],0,2);
		

		try{
			$te1 = M($issueTabName)
									->cache(true,60)
									->alias('tissue')
									->field('fstarttime,fendtime')
									->where(array('tissue.fissuedate'=>$timestampDay,'tissue.fmediaid'=>$mediaId,'tissue.fsampleid'=>$samId))
									->select();
		}catch( \Exception $error){
			$te1 = [];
		}
		$issueList = [];			
		foreach($te1 as $te){
			
			$te['m3u8'] = A('Common/Media','Model')->get_m3u8($mediaId,$te['fstarttime'],$te['fendtime']); 
			$te['start'] = date('Y-m-d H:i:s',$te['fstarttime']);
			$te['end'] = date('Y-m-d H:i:s',$te['fendtime']);
			
			$issueList[] = $te;
		}

		$samInfo = M('t'.$tab.'sample')
									->cache(true,600)
									->alias('tsample')
									->field('
											
											tad.fadname
											,tsample.fspokesman
											,tsample.fversion
											,tsample.fillegaltypecode
											,tsample.fillegalcontent
											,tsample.fexpressioncodes
											,tad.fbrand
											,(select fname from tadowner where fid = tad.fadowner) as fadowner
											,tad.fadclasscode
											,(select ffullname from tadclass where fcode = tad.fadclasscode) as fadclass
											,tad.fadclasscode_v2
											,(select ffullname from hz_ad_class where fcode = tad.fadclasscode_v2) as fadclass_v2
											,tsample.favifilename
											,tsample.fadlen 
												
											')
									->join('tad on tad.fadid = tsample.fadid')
									->where(array('fid'=>$samId))
									->find();
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','issueList'=>$issueList,'samInfo'=>$samInfo));
		
		
	}
	
	
	/*样本列表页*/
	public function sampleList(){
		session_write_close();
		$this->display();
	}


	/*提交检查*/
	public function commitCheck(){
		$fid = I('fid');
		$fstate = I('fstate');
		
		$rer = M('sam_check')->where(array('fid'=>$fid))->save(array('fstate'=>$fstate));
		
		if($rer){
			$this->ajaxReturn(array('code'=>0,'msg'=>'提交成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'提交失败'));
		}
	}
	
	/*计算数据*/
	public function auto_com(){
		session_write_close();
		set_time_limit(600);
		$dataList = M('sam_check')
								->field('
										sam_check.*
										,tmedia.fmedianame
									
										,left(tmedia.fmediaclassid,2) as media_class
										,tmedia.media_region_id
										
											')
								->join('tmedia on tmedia.fid = sam_check.fmediaid')
								->where(array('sam_check.sam_count'=>array('exp','is null')))
								->limit(20)
								->select();
								
								
		
		foreach($dataList as $data){
			
			$timestampDay = strtotime($data['fdate']);
		

		
			if ($data['media_class'] == '01'){
				$tab = 'tv';
			}elseif($data['media_class'] == '02'){
				$tab = 'bc';
			}else{
				M('sam_check')->where(array('fid'=>$data['fid']))->delete();
				continue;
			}
			
			$issueTabName = 't'.$tab.'issue_'.date('Ym',$timestampDay).'_'.substr($data['media_region_id'],0,2);
			

			
			try{
				$samList = M($issueTabName)
										
										->alias('tissue')
										->field('
													tsample.fid
													,tsample.fillegaltypecode
													,date(tsample.fissuedate) as samissuedate
													,count(*) as issuecount
													
													
													')
										->join('t'.$tab.'sample as tsample on tsample.fid = tissue.fsampleid')
										->where(array('tissue.fissuedate'=>$timestampDay,'tissue.fmediaid'=>$data['fmediaid']))
										->group('tsample.fid')
										->select();
			}catch( \Exception $error){
				$samList = [];
			}
			
			$sam_count = 0;
			$today_sam_count = 0;
			$issue_count = 0;
			$illegal_sam_count = 0;
			
			foreach($samList as $sam){
				$sam_count += 1;
				$issue_count += $sam['issuecount'];
				if ($sam['samissuedate'] == $data['fdate']) $today_sam_count += 1;

				if ($sam['fillegaltypecode'] > 0) $illegal_sam_count += 1;
				
			}


			
			
			
			M('sam_check')->where(array('fid'=>$data['fid']))->save(array(
																	'sam_count'=>$sam_count,
																	'today_sam_count'=>$today_sam_count,
																	'issue_count'=>$issue_count,
																	'illegal_sam_count'=>$illegal_sam_count
																	
																	));
			
		}
		
		echo 'SUCCESS';
		
	}

























	

}