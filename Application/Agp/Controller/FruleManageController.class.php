<?php
namespace Agp\Controller;
use Think\Controller;


/**
 * 规则管理
 */

class FruleManageController extends BaseController
{


	//获取
	public function index_rule(){

		$userid = session('regulatorpersonInfo.fid');//用户id
		$regionid = session('regulatorpersonInfo.regionid');//行政区划id

		$region_id_rtrim = rtrim($regionid,'00');//去掉地区后面两位0，判断区
		$region_id_rtrim = rtrim($region_id_rtrim,'000');//去掉后面三位0，判断全国
		$region_id_rtrim = rtrim($region_id_rtrim,'00');//去掉后面两位0，判断市
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位
		if($tregion_len == 3){
			$region_id_rtrim = $region_id_rtrim.'0';
		}
	
		$res = M('f_rule_management')->where('fregionid="'.$region_id_rtrim.'"')->find();

		if(!$res){
			$res = M('f_rule_management')->where('fregionid=001')->find();
			$data['fregionid'] = $region_id_rtrim;
			$data['frequency'] = $res['frequency'];
			$data['frequency_score'] = $res['frequency_score'];
			$data['longtime'] = $res['longtime'];
			$data['longtime_score'] = $res['longtime_score'];
			$data['number'] = $res['number'];
			$data['number_score'] = $res['number_score'];
			$data['welfare_number'] = $res['welfare_number'];
			$data['ctime'] = date('Y-m-d H:i:s');//创建时间
			$data['cpersonid'] = $userid;

			$result = M('f_rule_management')->add($data);

		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'数据获取成功','data'=>$res));//返回ajax
	}

	//更新规则
	public function update_rule(){

		$regionid = session('regulatorpersonInfo.regionid');//行政区划id

		$tregion_len = get_tregionlevel($regionid);
		if($tregion_len != 2){
			$this->ajaxReturn(array('code'=>1,'msg'=>'没有更改权限'));//返回ajax
		}

		$frequency = I('frequency');//次数违法率每多少百分点
		$frequency_score = I('frequency_score');//次数违法率扣分数
		$longtime = I('longtime');//时长违法率每多少百分点
		$longtime_score = I('longtime_score');//时长违法率扣分
		$number = I('number');//广告违法次数每所少条次
		$number_score = I('number_score');//广告违法次数扣分
		$welfare_number = I('welfare_number');//公益广告评分标准

		$where['fregionid'] = substr(session('regulatorpersonInfo.regionid'),0,2);//行政区划id
		$data['frequency'] = $frequency;
		$data['frequency_score'] = $frequency_score;
		$data['longtime'] = $longtime;
		$data['longtime_score'] = $longtime_score;
		$data['number'] = $number;
		$data['number_score'] = $number_score;
		$data['welfare_number'] = $welfare_number;

		$res = M('f_rule_management')->where($where)->save($data);

		if($res){
			D('Function')->write_log('规则管理',1,'数据更新成功','f_rule_management',$res,M('f_rule_management')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'数据更新成功'));//返回ajax
		}else{
			D('Function')->write_log('规则管理',0,'数据更新失败','f_rule_management',0,M('f_rule_management')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'数据更新失败'));//返回ajax	
		}

	}

}