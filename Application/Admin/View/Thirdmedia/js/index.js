Vue.prototype.$ELEMENT = { size: 'mini', zIndex: 3000 }
const vm = new Vue({
  el: '#app',
  name: 'app',
  components: {
    addEditDialog,
    childrenMediaDialog,
  },
  data() {
    return {
      searchForm: {
        page: 1,
        fmediatype: '0',
        fstatus: '1',
        fmediacodename: '',
        fmediacode: ''
      },
      tableData: [],
      total: 0,
      mediaType: {
        '1': '电视',
        '2': '广播',
        '3': '报纸',
        '4': '互联网',
      },
      sourceSearchForm: '',
      showDialog: false,
      showChildrenDialog: false,
      loading: true,
      row: {},
      customer: [],
    }
  },
  created() {
    this.sourceSearchForm = JSON.stringify(this.searchForm)
    this.getList()
    this.getCustomer()
  },
  methods: {
    getList() {
      this.loading = true
      $.post('/Admin/Thirdmedia/media_list', this.searchForm).then(res => {
        if (res.code === 0) {
          this.tableData = res.data.list.map(i => {
            i.visible = false
            if (i.cids.length > 0) {
              i.children_media = []
              i.cids.forEach((v,index) => {
                i.children_media.push({
                  cid: i.cids[index],
                  mediaid: i.child_fmediaid[index],
                  medianame: i.child_fmedianame[index],
                  visible: false
                })
              })
            }
            if (this.row.fid && i.fid === this.row.fid) {
              console.log(i)
              this.$set(this,'row',i)
            }
            return i
          })
          this.total = Number(res.data.count)
        } else {
          this.$message.error(res.msg)
        }
        this.loading = false
      })
    },
    getCustomer() {
      $.post('/Admin/Thirdmedia/getuserlist').then(res => {
        this.customer = res.data
      })
    },
    handleCurrentChange(page) {
      this.searchForm.page = page
      this.getList()
    },
    handleSearch() {
      this.searchForm.page = 1
      this.getList()
    },
    editRow(row, column, e) {
      this.row = row
      this.showDialog = true
    },
    resetForm() {
      this.searchForm = JSON.parse(this.sourceSearchForm)
    },
    dialogClose(refresh) {
      this.row = {}
      this.showDialog = false
      this.showChildrenDialog = false
      if (refresh) {
        this.getList()
      }
    },

    deleteMedia({ fid }, e) {
      $.post('/Admin/Thirdmedia/delete_media', { fid }).then(res => {
        if (res.code === 0) {
          this.$message.success(res.msg)
          this.getList()
        } else {
          this.$message.error(res.msg)
        }
      })
      e.stopPropagation()
    },
    showChildrenMediaDialog(row, e) {
      this.showChildrenDialog = true
      this.row = row
      e.stopPropagation()
    }
  },
  filters: {
    mediatype(val) {
      try {
        return vm.mediaType[val]
      }
      catch (e){

      }
    }
  }
})