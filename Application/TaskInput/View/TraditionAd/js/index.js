Vue.use(VueViewer.default)
Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}
const vm = new Vue({
  el: '#app',
  components: {
    taskInfo,
    taskInfo2,
    netAdTask,
    netAdTask2,
    outdoorAdCheck,
  },
  data() {
    return {
      task: {},
      loading: false,
      type,
      taskType: null,
    }
  },
  methods: {
    chooseAdType(val) {
      this.taskType = val
      this.getTask()
    },
    getTask() {
      this.loading = true
      $.post('', {taskType: this.taskType}).then((res) => {
        this.loading = false
        if (res.code == -2 && res.msg == 'need log in again') {
          vm.$message.error('登录状态过期,请再次登录')
          parent.location.assign('/Adinput/VideoCutting/login')
        }else if (res.code === -1) {
          this.$message({
            type: 'error',
            duration: 1500,
            message: res.msg,
            onClose: () => {
              this.back()
            }
          })
        } else {
          this.task = res.data
          this.$nextTick(() => {
            this.$refs.task.dataInit()
          })
        }
      })
    },
    back() {
      window.location.reload()
    }
  }
})