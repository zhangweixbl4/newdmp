Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}
const vm = new Vue({
  el: "#app",
  data() {
    return {
      form: {
        pageIndex: 1,
        pageSize: 20,
        fdstatus: '-1',
        mtype: '',
        fissuedate: '',
      },
      loading: false,
      cloading: false,
      allcount: 0,
      tableData: [],
      disabedTime: '',
      markArr: [],
      addForm: {
        fissuedate: '',
        fmediaid: ''
      },
      allMediaList: [],
      allMediaType: [],
      tableHeight: '400px',
      multipleSelection: [],
      showPriority: false,
      changePriority: '',
      defaultDateValue:'2019-12-01',
      showDataIssue: false,
      changeDataIssueMonth: '',
      priority: {
        '0': '一级',
        '1': '二级',
        '2': '三级',
        '4': '四级',
        '5': '五级',
      },
      searchStatus: [
        {
          label: '全部',
          value: '-1'
        },{
          label: '待处理',
          value: '0'
        },{
          label: '识别中',
          value: '14'
        },{
          label: '待接收',
          value: '2'
        },{
          label: '已传送',
          value: '4'
        },
      ],
      fdstatus: {
        '0': {
          label: '待处理',
          type: 'danger'
        },
        '1': {
          label: '待处理',
          type: 'danger'
        },
        '2': {
          label: '待接收',
          type: 'waring'
        },
        '14': {
          label: '识别中',
          type: 'waring'
        },
        '4': {
          label: '已传送',
          type: 'success'
        },
      },
      url: '',
      show: false,
      centerDialogVisible: false
    }
  },
  created() {
    let stime = dayjs().startOf('month').format('YYYY-MM-DD')
    let etime = dayjs().format('YYYY-MM-DD')
    this.form.fissuedate = [stime, etime]
    this.getList()
    this.getMediaList()
    this.getMediaTypeList()
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight()
      document.querySelector('#app').style.opacity = 1
    })
    window.onresize = () => {
      this.getTableHeight()
    }
  },
  methods: {
    showDialog(){
      this.showDataIssue = true
      this.changeDataIssueMonth = dayjs().format('YYYY-MM-01')
    },
    getList(page) {
      if(page) this.form.pageIndex = page
      let data = {
        ...this.form
      }
      this.loading = true
      $.post('/TaskInput/NetAd/NetPlanList', JSON.stringify(data))
        .then(res => {
          if(res.code == 0){
            this.tableData = res.data
            // console.log(this.allMediaList)
            this.allcount = res.count
            this.loading = false
          }
      })
    },
    getMediaList(){
      $.post('/TaskInput/NetAd/getUserMediaList')
      .then(res=>{
        this.allMediaList = res.data
      })
    },
    // 获取媒体分类列表
    getMediaTypeList(){
      $.post('/TaskInput/NetAd/getmediatype')
      .then(res=>{
        this.allMediaType = res.data
      })
    },
    // 点击完成时修改状态
    finish(row) {
      let msg = `您确定需要将以下计划状态改为完成？
      <br>
      <strong>媒体名称</strong>: ${row.fmedianame}
      <br>
      <strong>抽查日期</strong>: ${row.fissuedate}
      <br>
      <strong>通过量</strong>: ${row.tgcount}条`
      this.$alert(msg, '提示', {
        dangerouslyUseHTMLString: true,
        showCancelButton: true,
      })
      .then(() => {
        $.post('/TaskInput/NetAd/PlanFinishAction',JSON.stringify({taskid:row.fid}))
        .then(res => {
          if(res.code == 0) {
            this.getList()
          }
          if(res.code == 1){
          }
        })
      })
    },
    getTableHeight() {
      this.tableHeight = `${document.body.clientHeight - 100}px`
    },
    handleCommand(command) {
      let data = {
        ...this.form
      }
      if(command == 'a') {
        $.post('/TaskInput/NetAd/Task_Baobiao',JSON.stringify(data))
        .then(res=>{
          if(res.code == 0) {
            this.centerDialogVisible = true
            this.url = res.data
          }
          if(res.code == 1) {
            this.centerDialogVisible = false
            this.$message.error(res.msg)
          }
        })
      } else {
        $.post('/TaskInput/NetAd/Task_Mingxi',JSON.stringify(data))
        .then(res=>{
          if(res.code == 0) {
            this.centerDialogVisible = true
            this.url = res.data
          }
          if(res.code == 1) {
            this.centerDialogVisible = false
            this.$message.error(res.msg)
          }
        })
      }
    },
    handleSelectionChange(val) {
      this.multipleSelection = val
    },
    submitChangePriority() {
      let data = {
        taskid: this.multipleSelection.map(i => i.fid).join(','),
        editpriority: this.changePriority
      }
      $.post('/TaskInput/NetAd/PlanDistinguishOrderAction', JSON.stringify(data)).then(res => {
        if (res.code === 0) {
          this.$notify({
            message: res.msg,
            type: 'success',
            duration: 1500
          })
          this.showPriority = false
          this.getList()
          this.multipleSelection = []
          this.$refs.multipleTable.clearSelection()
        } else {
          this.$notify.error({
            message: res.msg,
            duration: 1000
          })
        }
      })
    },
    submitChangeDataIssueMonth() {
      let data = {
        editmonth: this.changeDataIssueMonth
      }
      $.post('/TaskInput/NetAd/PlanDataIssueDateAction', JSON.stringify(data)).then(res => {
        this.changeDataIssueMonth = ''
        if (res.code === 0) {
          this.$notify({
            message: res.msg,
            type: 'success',
            duration: 1500
          })
          this.showDataIssue = false
          this.getList()
          this.multipleSelection = []
          this.$refs.multipleTable.clearSelection()
        } else {
          this.$notify.error({
            message: res.msg,
            duration: 1000
          })
        }
      })
    }
  },
})