<?php
namespace Agp\Controller;
use Think\Controller;

class ZbDataDrController extends BaseController{
    //在类初始化方法中，引入相关类库
    public function _initialize() {
        header("Content-type:text/html;charset=utf-8");
        vendor('PHPWord.PHPWord');
        vendor('PHPWord.PHPWord.IOFactory');
    }

    //写入淄博某日数据
    /*public function create_zb_data(){
        set_time_limit(0);
        ini_set('memory_limit','1024M');
        $week = date('w',time());
        if($week == 1){
            $s_date = date('Y-m-d',time()-(86400*3));
            $e_date = date('Y-m-d',time()-86400);
        }else{
            $s_date = date('Y-m-d',time()-86400);
            $e_date = date('Y-m-d',time()-86400);
        }
        $s_datetime = I('s_datetime',$s_date);
        $e_datetime = I('e_datetime',$e_date);
        if(date("H",time()) < '14' || date("H",time()) >= '16')
        {
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '请在今日下午2点至4点触发！'
            ]);
        }
        //淄博指定日期电视发布数据
        $tvf_sql = "SELECT
                ftvissueid AS source_fid,
                fmediaid,
                ftvsampleid AS fsampleid,
                UNIX_TIMESTAMP(fstarttime) AS fstarttime,
                UNIX_TIMESTAMP(fendtime) AS fendtime,
                flength,
                UNIX_TIMESTAMP(fissuedate) AS fissuedate,
                faudiosimilar,
                fregionid,
                UNIX_TIMESTAMP(fcreatetime) AS fcreatetime,
                fcreator
            FROM
                ttvissue
            WHERE
                fregionid LIKE '3703%'
            AND fissuedate BETWEEN '$s_datetime' AND '$e_datetime';";
        $tvf_res = M('')->query($tvf_sql);
        $tvf_mod = M('ttvissue_2018_370300');
        foreach ($tvf_res as $tvf_res_val){
            $exits_res = M('ttvissue_2018_370300')
                ->where([
                    'source_fid'=>$tvf_res_val['source_fid'],
                    'fissuedate' => ['BETWEEN',[strtotime($s_datetime),strtotime($e_datetime)]],
                    'fmediaid' => $tvf_res_val['fmediaid'],
                    'fstarttime' => $tvf_res_val['fstarttime']
                ])
                ->find();
            if($exits_res){
                continue;
            }else{
                $res_for_tvf = $tvf_mod->add($tvf_res_val);
            }
        }

        //淄博指定日期广播发布数据
        $tbf_sql = "SELECT
                    fbcissueid AS source_fid,
                    fmediaid,
                    fbcsampleid AS fsampleid,
                    UNIX_TIMESTAMP(fstarttime) AS fstarttime,
                    UNIX_TIMESTAMP(fendtime) AS fendtime,
                    flength,
                    UNIX_TIMESTAMP(fissuedate) AS fissuedate,
                    faudiosimilar,
                    fregionid,
                    UNIX_TIMESTAMP(fcreatetime) AS fcreatetime,
                    fcreator
                FROM
                    tbcissue
                WHERE
                    fregionid LIKE '3703%'
                AND fissuedate BETWEEN '$s_datetime' AND '$e_datetime';";
        $tbf_res = M('')->query($tbf_sql);//查询广播发布记录
        $tbf_mod = M('tbcissue_2018_370300');//淄博广播发布记录模型
        foreach ($tbf_res as $tbf_res_val){
            $exits_tbf = M('tbcissue_2018_370300')
                ->where([
                    'source_fid'=>$tbf_res_val['source_fid'],
                    'fissuedate' => ['BETWEEN',[strtotime($s_datetime),strtotime($e_datetime)]],
                    'fmediaid' => $tbf_res_val['fmediaid'],
                    'fstarttime' => $tbf_res_val['fstarttime']
                ])
                ->find();
            if($exits_tbf){
                continue;
            }else{
                $res_for_tbf = $tbf_mod->add($tbf_res_val);
            }
        }

        //淄博指定日期报纸发布数据
        $tpf_sql = "SELECT * FROM tpaperissue WHERE fregionid LIKE '3703%' AND tpaperissue.fissuedate BETWEEN '$s_datetime' AND '$e_datetime';";
        $tpf_res = M('')->query($tpf_sql);
        $tpf_mod = M('tpaperissue_370300');
        foreach ($tpf_res as $tpf_res_val){
            $tpf_res_val['source_fpaperissueid'] = $tpf_res_val['fpaperissueid'];
            unset($tpf_res_val['fpaperissueid']);
            $exits_tpf = M('tpaperissue_370300')
                ->where([
                    'source_fpaperissueid'=>$tpf_res_val['source_fpaperissueid'],
                    'fissuedate' => ['BETWEEN',[$s_datetime,$e_datetime]],
                ])
                ->find();
            if($exits_tpf){
                continue;
            }else{
                $res_for_tpf = $tpf_mod->add($tpf_res_val);
            }
        }

        //淄博电视样本不在淄博临时表中
        $tvs_sql = "SELECT
                    ttvsample.fid,
                    ttvsample.fmediaid,
                    ttvsample.fissuedate,
                    ttvsample.fadid,
                    ttvsample.fversion,
                    ttvsample.fspokesman,
                    ttvsample.fadlen,
                    ttvsample.fillegaltypecode,
                    ttvsample.fapprovalid,
                    ttvsample.fapprovalunit,
                    ttvsample.fillegalcontent,
                    ttvsample.fexpressioncodes,
                    ttvsample.fexpressions,
                    ttvsample.fconfirmations,
                    ttvsample.fpunishments,
                    ttvsample.fpunishmenttypes,
                    ttvsample.favifilename,
                    ttvsample.fcreator,
                    ttvsample.fcreatetime,
                    ttvsample.fmodifier,
                    ttvsample.fmodifytime,
                    ttvsample.fsource,
                    ttvsample.fstate,
                    ttvsample.finputstate,
                    ttvsample.finspectstate,
                    ttvsample.foffsetstart,
                    ttvsample.foffsetend,
                    ttvsample.fsourcefilename,
                    ttvsample.fcuted,
                    ttvsample.favifilepng,
                    ttvsample.uuid,
                    ttvsample.finputuser,
                    ttvsample.finspectuser,
                    ttvsample.fadmanuno,
                    ttvsample.fmanuno,
                    ttvsample.fadapprno,
                    ttvsample.fapprno,
                    ttvsample.fadent,
                    ttvsample.fent,
                    ttvsample.fentzone,
                    ttvsample.freason,
                    ttvsample.frechecker,
                    ttvsample.frechecktime,
                    ttvsample.frecheckstate,
                    ttvsample.abnormal_reason,
                    ttvsample.issue_relation,
                    ttvsample.freviewstate,
                    ttvsample.review_state,
                    ttvsample.freview_tregulator,
                    ttvsample.freview_userid,
                    ttvsample.is_long_ad,
                    ttvsample.last_issue_date,
                    ttvsample.tem_ad_name,
                    ttvsample.sstarttime,
                    ttvsample.sendtime
                    FROM
                        ttvsample
                    JOIN tmedia ON tmedia.fid = ttvsample.fmediaid
                    WHERE
                        tmedia.media_region_id LIKE '3703%' AND ttvsample.fid NOT IN(SELECT fid FROM ttvsample_370300);";
        $tvs_res = M('')->query($tvs_sql);
        $tvs_mod = M('ttvsample_370300');
        foreach ($tvs_res as $tvs_res_val){
            $res_for_tvs = $tvs_mod->add($tvs_res_val);
        }

        //淄博广播样本不在淄博临时表中
        $tbs_sql = "SELECT
                    tbcsample.fid,
                    tbcsample.fmediaid,
                    tbcsample.fissuedate,
                    tbcsample.fadid,
                    tbcsample.fversion,
                    tbcsample.fspokesman,
                    tbcsample.fadlen,
                    tbcsample.fapprovalid,
                    tbcsample.fapprovalunit,
                    tbcsample.fillegaltypecode,
                    tbcsample.fillegalcontent,
                    tbcsample.fexpressioncodes,
                    tbcsample.fexpressions,
                    tbcsample.fconfirmations,
                    tbcsample.fpunishments,
                    tbcsample.fpunishmenttypes,
                    tbcsample.favifilename,
                    tbcsample.fcreator,
                    tbcsample.fcreatetime,
                    tbcsample.fmodifier,
                    tbcsample.fmodifytime,
                    tbcsample.fsource,
                    tbcsample.fstate,
                    tbcsample.finputstate,
                    tbcsample.finspectstate,
                    tbcsample.foffsetstart,
                    tbcsample.foffsetend,
                    tbcsample.fsourcefilename,
                    tbcsample.fcuted,
                    tbcsample.favifilepng,
                    tbcsample.uuid,
                    tbcsample.finputuser,
                    tbcsample.finspectuser,
                    tbcsample.fadmanuno,
                    tbcsample.fmanuno,
                    tbcsample.fadapprno,
                    tbcsample.fapprno,
                    tbcsample.fadent,
                    tbcsample.fent,
                    tbcsample.fentzone,
                    tbcsample.freason,
                    tbcsample.frechecker,
                    tbcsample.frechecktime,
                    tbcsample.abnormal_reason,
                    tbcsample.issue_relation,
                    tbcsample.freviewstate,
                    tbcsample.review_state,
                    tbcsample.freview_tregulator,
                    tbcsample.freview_userid,
                    tbcsample.is_long_ad,
                    tbcsample.last_issue_date,
                    tbcsample.tem_ad_name,
                    tbcsample.sstarttime,
                    tbcsample.sendtime
                    FROM
                        tbcsample
                    JOIN tmedia ON tmedia.fid = tbcsample.fmediaid
                    WHERE
                        tmedia.media_region_id LIKE '3703%' AND tbcsample.fid not in(SELECT fid FROM tbcsample_370300);";
        $tbs_res = M('')->query($tbs_sql);
        $tbs_mod = M('tbcsample_370300');
        foreach ($tbs_res as $tbs_res_val){
            $res_for_tbs = $tbs_mod->add($tbs_res_val);
        }


        //淄博报纸样本不在淄博临时表中
        $tps_sql = "SELECT
                    tpapersample.fpapersampleid,
                    tpapersample.fmediaid,
                    tpapersample.fissuedate,
                    tpapersample.fadid,
                    tpapersample.fversion,
                    tpapersample.fspokesman,
                    tpapersample.fapprovalid,
                    tpapersample.fapprovalunit,
                    tpapersample.fillegaltypecode,
                    tpapersample.fillegalcontent,
                    tpapersample.fexpressioncodes,
                    tpapersample.fexpressions,
                    tpapersample.fconfirmations,
                    tpapersample.fpunishments,
                    tpapersample.fpunishmenttypes,
                    tpapersample.fjpgfilename,
                    tpapersample.fcreator,
                    tpapersample.fcreatetime,
                    tpapersample.fmodifier,
                    tpapersample.fmodifytime,
                    tpapersample.fsource,
                    tpapersample.fstate,
                    tpapersample.finputstate,
                    tpapersample.finspectstate,
                    tpapersample.finputuser,
                    tpapersample.finspectuser,
                    tpapersample.fadmanuno,
                    tpapersample.fmanuno,
                    tpapersample.fadapprno,
                    tpapersample.fapprno,
                    tpapersample.fadent,
                    tpapersample.fent,
                    tpapersample.fentzone,
                    tpapersample.sourceid,
                    tpapersample.freason,
                    tpapersample.frechecker,
                    tpapersample.frechecktime,
                    tpapersample.cut_err_reason,
                    tpapersample.sub_cut_err_user,
                    tpapersample.freviewstate,
                    tpapersample.review_state,
                    tpapersample.freview_tregulator,
                    tpapersample.freview_userid,
                    tpapersample.last_issue_date,
                    tpapersample.tem_ad_name
                    FROM
                        tpapersample
                    JOIN tmedia ON tmedia.fid = tpapersample.fmediaid
                    WHERE
                        tmedia.media_region_id LIKE '3703%' AND tpapersample.fpapersampleid NOT IN(SELECT fpapersampleid FROM tpapersample_370300);";
        $tps_res = M('')->query($tps_sql);
        $tps_mod = M('tpapersample_370300');
        foreach ($tps_res as $tps_res_val){
            $res_for_tps = $tps_mod->add($tps_res_val);
        }

        $this->ajaxReturn([
            'code'=>0,
            'msg'=>'数据导入成功！'
        ]);

    }*/

}