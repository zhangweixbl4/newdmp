﻿
function getAddress(obj, url, name) {
	
	//选择地区事件
	$(document).on('click', '#region-select .area-list .click_region_name', function () {
		var fid = $(this).attr("fid");//获取点击的地区ID
		$(obj).attr(name,fid);//修改input的fid
		
		get_region_list(obj,fid,url);
	});
	//点击tab标签事件
	$(document).on('click', '.ttab', function () {
		var fid = $(this).attr("fid");//获取点击的地区ID
		$(this).nextAll().remove();//移除右侧的ttab
		$(this).remove();//移除自己
		get_region_list(obj,fid,url);
	});	
	//表单点击事件
	$(obj).click(function(){
		$(obj).attr('readonly', true);
		var val = $(this).val();//获取表单内容，用于判断表单是否有值
		var fid = $(obj).attr(name);//获取存储的地区code
		var areaList = $.trim($("#region-select .area-list").html());//获取地区列表,用于判断地区列表是否为空
		$('#region-select').parent().css('position', 'relative');//input父元素加上相对定位
		if(val == '' || fid == '' || areaList == ''){
			
			$('#region-select').show();//显示地区选择控件
			$('#region-select .tab').empty();//移除所有已选择的
			get_region_list(obj, 0, url);//创建全国列表
		} else {
			$('#region-select').toggle();//显示地区选择控件
		}	
	});
}
//根据地区code创建标签和选择列表
function get_region_list(obj,fid,url){
	$.post(url,{"fid":fid},function(req){
		var listHTML = '';
		//循环地区列表
		for (let i = 0; i < req.regionList.length; i++) {
			listHTML += '<li><a class="click_region_name" fid="'+req.regionList[i].fid+'">'+req.regionList[i].fname+'</a></li>';
		}
		$(obj).val(req.regionDetails.ffullname);
		//如果地区列表不为空则把列表加入到 #region-select .tab
		if(listHTML != ''){
			$('#region-select .area-list').html(listHTML);//地区列表渲染
			$('#region-select .tab li').removeClass('curr');//移除所有tab的选择
			$('#region-select .tab').append('<li class="ttab curr" fid="'+req.regionDetails.fid+'"><a class=""><em>'+req.regionDetails.fname+'</em></a></li>');//创建新的tab
		}else{
			$('#region-select').hide();//隐藏地区选择控件
		} 
			
	});
}
