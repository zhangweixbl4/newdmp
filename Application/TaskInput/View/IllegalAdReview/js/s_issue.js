Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}
Vue.use(VueViewer.default)
var vm = new Vue({
  el: '#app',
  components: {aliplayer},
  data() {
    return {
      loading: true, //加载状态
      form: {
        fmedianame: '',//媒介名称
        fadname: '',//广告名称
        this_region_id: 'false',//地区仅本级
		    sx: '',
        pp: 15, //每页数量
        p: 1, //分页
      },
      copy: false,
      area: [],//地区
      adclasscode: [],//广告类别
      issDate: [], //发布日期
      tabelData: [], //列表数据
      allCount: '0', //列表数据总数
      dialogVisible: false, //详情弹窗显示隐藏字段
      details: {},//详情数据
      preData: {},//上次搜索的请求参数
      issueList: [],
      issuedate_min: '',
      issuedate_max: '',
      visible: false, // 修改按钮弹窗显示状态
      multipleSelection: [],// 列表选中项
      tableMultipleSelection: [],// 列表选中项
      preRow: {},
      aliplayer: true,
      regiontree: REGIONTREE,
      adclass: ADCLASS,
      playUrl: '',
      sx: {
        '0': '未确认',
        '1': '已确认违法',
		    '-1': '已确认不违法',
      },
      pickerOptions: {
        disabledDate: function (time) {
          return time.getTime() > new Date().getTime()
        }
      },
      visible: {
        'visible1': false,
        'visible2': false,
        'visible-1': false,
        'visible1_': false,
        'visible2_': false,
        'visible-1_': false,
      },
      index: 0,// 递归 index
      error: 0, // 批量请求中的出错数量
      activeName: 'issue', // 详情tab标签页默认选项
      remarkList: []
    }
  },
  computed: {
    isPaper() {
      return this.details.media_class === '03'
    },
    length() {
      return this.tableMultipleSelection.length
    }
  },
  created() {
    this.loading = !this.loading
    this.lastMonth()
    this.getList()
  },
  mounted() {
    this.$nextTick(() => {
      this.$refs.app.style.display = 'block'
      new ClipboardJS('.copied')
    })
  },
  methods: {
    /**
     * 获取列表
     * @param {number} p 页码
     */
    getList(p) {
      this.preData = Object.assign({}, this.form)
      this.preData.issuedate_min = this.issDate[0] || ''
      this.preData.issuedate_max = this.issDate[1] || ''
      this.preData.fregionid = this.area[this.area.length - 1] || ''
      this.preData.fadclasscode = this.adclasscode[this.adclasscode.length - 1] || ''
      this.preData.p = p || 1
      this.loading = true
      axios.post('/TaskInput/IllegalAdReview/search_issue', Qs.stringify(this.preData))
        .then(res => {
          if (res.data.code === 0) {
			      this.mediaCount = res.data.mediaCount
            this.tabelData = res.data.dataList
            this.issuedate_min = res.data.issuedate_min
            this.issuedate_max = res.data.issuedate_max
            this.allCount = res.data.total
          } else {
            this.$message.error(res.data.msg)
          }
          this.loading = false
        })
        .catch(e => {
          this.$message.error('网络错误')
          this.loading = false
        })
    },
    /**
     * 用于获取某条广告的详情
     * @param {Object} row 当前行的数据对象
     */
    show({ sam_id, media_id }) {
      let { issuedate_min, issuedate_max } = this
      let data = {
        sam_id,
        media_id,
        issuedate_min,
        issuedate_max
      }
      this.preRow = data
      axios.post('/TaskInput/IllegalAdReview/info', Qs.stringify(data)).then(res => {
        if (res.data.code === 0) {
          this.remarkList = res.data.remarkList
          let samInfo = res.data.samInfo
          samInfo.samid = res.data.samInfo.fid || res.data.samInfo.fpapersampleid
          delete samInfo.fid
          this.details = { ...samInfo, ...res.data.mediaInfo }
          this.issueList = res.data.issueList.map(i => {
            if (this.isPaper) {
              i.date = i.issue_date
            } else {
              i.mediaId = res.data.mediaInfo.fid
              i.date = `${i.starttime} 至 ${i.endtime.substring(11)}（${i.end_time - i.start_time}s）`
            }
            return i
          })
          this.playUrl = res.data.samInfo.sam_filename
          this.dialogVisible = true
        } else {
          this.$message.error(res.data.msg)
        }
      })
      .catch(e => {
        this.$message.error('网络错误')
      })
    },
    setType(type) {
      this.visible[`visible${type}`] = false
      let idList = [...this.multipleSelection].map(i => i.id).join(',')
      let data = {
        idList,
        type
      }
      axios.post('/TaskInput/IllegalAdReview/edit', Qs.stringify(data)).then(res => {
        if (res.data.code === 0) {
          this.multipleSelection = []
          this.show(this.preRow)
          this.$message.success(res.data.msg)
        } else {
          this.$message.error(res.data.msg)
        }
      })
    },
    startSet(type) {
      this.visible[`visible${type}_`] = false
      let arr = [...this.tableMultipleSelection]
      this.setTableType(type,arr)
    },
    setTableType(type, arr) {
      let { sam_id, media_id} = arr[this.index]
      let { issuedate_min,issuedate_max } = this
      let data = {
        sam_id,
        media_id,
        issuedate_min,
        issuedate_max,
        type
      }
      this.index += 1
      axios.post('/TaskInput/IllegalAdReview/batch_edit', Qs.stringify(data)).then(res => {
        if (res.data.code === 0) {
          if (this.index < this.length) {
            return this.setTableType(type,arr)
          } else {
            if (this.error) {
              this.$message.error(`有 ${this.error} 项修改出错，详情见控制台`)
            } else {
              this.$message.success(`${this.length} 项修改完成`)
            }
            this.tableMultipleSelection = []
            this.index = 0
            this.error = 0
            this.getList(this.preData.p)
          }
        } else {
          this.error += 1
          console.error(`第 ${this.index - 1} 项：${res.data.msg}`)
        }
      })
    },
    filterType(value, row) {
      return row.type === value
    },
    changeUrl(url) {
      this.aliplayer = false
      this.playUrl = `${url}?${+new Date()}`
      setTimeout(() => {
        this.aliplayer = true
      })
    },
    getM3u8(row, column, event) {
      if (row.m3u8Url) {
        return this.changeUrl(row.m3u8Url)
      }
      let mediaId = row.mediaId
      let startTime = row.start_time
      let endTime = row.end_time
      axios.get(`/Api/Media/get_media_m3u8_url?mediaId=${mediaId}&startTime=${startTime}&endTime=${endTime}`)
        .then(res => {
          this.changeUrl(res.data.m3u8Url)
          row.m3u8Url = res.data.m3u8Url
      })
    },
    /**
     * 详情弹窗关闭时触发
     */
    dialogClose() {
      this.details = {}
      this.preRow = {}
      this.multipleSelection = []
      this.getList(this.preData.p)
    },
    /**
     * 设置每页显示条数
     * @param {Number} size 每页显示条数
     */
    sizeChange(size) {
      this.form.pp = size
      this.getList()
    },
    // 设置筛选条件时间为近30天
    lastMonth() {
      let time = new Date().getTime()
      let lastMonth = time - (60 * 60 * 24 * 90 * 1000)
      this.issDate = Array.of(formatDate(new Date(lastMonth), 'yyyy-MM-dd'), formatDate(new Date(time), 'yyyy-MM-dd'))
    },
    hide() {
      this.copy = true
      setTimeout(() => {
        this.copy = false
      },500)
    },
    // 添加备注
    add_remark_list() {
      this.$prompt('请输入备注', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
      })
      .then(({ value })=> {     
        let data = {
          sam_id : this.details.samid,
          media_id: this.details.fid,
          content : value,
        }
        axios.post('/TaskInput/illegalAdReview/add_remark_list',Qs.stringify(data)).then(res => {         
          if (res.data.code === 0) {
            this.$message.success(res.data.msg)
            this.show(this.preRow)
          } else {
            this.$message.error(res.data.msg)
          }
        })
      })
    },
    // 详情页 tab 切换事件
    handleClick(tab, event) {
      
    }
    
  }
})