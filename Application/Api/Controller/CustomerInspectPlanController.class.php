<?php
namespace Api\Controller;
use Think\Controller;

class CustomerInspectPlanController extends Controller {
	
	
	
	
	/*同步监测计划给生产平台*/
	public function to_produce(){
		
		#var_dump(C('ZIZHI_DB'));
		
		$synDataList = M('tcustomer_inspect_plan','',C('ZIZHI_DB'))
						->field('fid,fmedia_id,fissue_date,fplan_delivery_date,fstatus')
						->where(array('fto_produce_state'=>0,'fissue_date'=>['egt','2020-04-01']))->limit(30)->select();
		
		$synUrl = 'http://47.96.182.117/manage/task/updateChannelDayTask';

		foreach($synDataList as $syn){
			#var_dump($syn);
			$postData = array();
			$postData['task_status'] = '1';
			if($syn['fstatus'] == -1) $postData['task_status'] = '0';
			$postData['channel'] = $syn['fmedia_id'];
			$postData['date'] = $syn['fissue_date'];
			$postData['task_aging'] = strtotime($syn['fplan_delivery_date']);
			
			
			$ret = http($synUrl,json_encode($postData),'POST');
			$retArr = json_decode($ret,true);
			if($retArr['code'] === 0){
				M('tcustomer_inspect_plan','',C('ZIZHI_DB'))->where(array('fid'=>$syn['fid']))->save(array('fto_produce_state'=>1));
			}elseif($retArr['code'] === -6){
				M('tcustomer_inspect_plan','',C('ZIZHI_DB'))->where(array('fid'=>$syn['fid']))->save(array('fto_produce_state'=>-6));
			}
			
			$dmpLogs2 = A('Common/AliyunLog','Model')->dmpLogs2('tcustomer_inspect_plan',array(
																							'request'=>json_encode($postData),
																							'response'=>$ret,
																							 ));	
			echo $syn['fmedia_id'].'	'. $syn['fissue_date'].'	'.$postData['task_status'].'	'.$ret."\n";																 
			
		}
		
		echo 'success '.date('Y-m-d H:i:s')."\n";
		
		
	}
	
	/*根据客户监测计划创建生产计划*/
	public function j2s(){
		
		$sql = "
					INSERT INTO tinspect_plan (
														`fmedia_id`,
														`fissue_date`, 
														`finspect_type`, 
														`fplan_time`, 
														`ffinish_time`, 
														`fads`,
														`ffinish_ads`,
														`fillegal_ads`, 
														`finspect_org_id`, 
														`finspect_org`, 
														`finspect_person_id`, 
														`finspect_person`,
														`finspect_level`,
														`fstatus`
													)  
					select 
									a.fmedia_id,
									a.fissue_date,
									IFNULL(c.finspect_type,0) as finspect_type,
									min(a.fplan_delivery_date) as fplan_time,
									max(a.fdelivery_date) as ffinish_time,
									max(a.fplan_ads) as fads,max(a.ffinish_ads) as ffinish_ads,
									max(a.fillegal_ads) as fillegal_ads,
									c.finspect_org_id,
									c.finspect_org,
									c.finspect_person_id,
									c.finspect_person,
									c.finspect_level,
									0 as fstatus
					from tcustomer_inspect_plan a 
					INNER JOIN tcustomer_media b on a.fcustomer_id = b.fcustomer_id AND a.fmedia_id = b.fmedia_id  
					LEFT JOIN tmedia_ext c on b.fmedia_id = c.fmedia_id 
					LEFT JOIN tinspect_plan d on d.fmedia_id = a.fmedia_id AND d.fissue_date = a.fissue_date
					where a.fstatus = 0 and a.fissue_date BETWEEN DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d'),INTERVAL 1 MONTH) and DATE_ADD(DATE_FORMAT(NOW(),'%Y-%m-%d'),INTERVAL 1 MONTH) AND d.fid is null  
					group by a.fmedia_id,a.fissue_date;
				";
		
		$aa = M('','',C('ZIZHI_DB'))->execute($sql);
		echo date('Y-m-d H:i:s').'	'.$aa;
	}
	
	
}