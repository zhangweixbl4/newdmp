<?php
namespace Api\Controller;
use Think\Controller;

class SystemController extends Controller {
	
	/*清理redis缓存*/
	public function clean_redis(){
		exit('不允许');
		$keys  = redis()->keys('*'); 
		foreach($keys as $key){
			redis()->del($key);
		}
	}
	
	/*上传crossdomain.xml文件到七牛云*/
	public function up_crossdomain(){
		$rr = A('Common/Qiniu','Model')->up_crossdomain('hzsj-video-nb');
	}
	
	/*监听使用*/
	public function monitoring_server(){
		
		$monitoringList = array('a','b','c');
		
		$monitoring_tab = I('monitoring_tab');//监听标记
		
		A('Common/System','Model')->important_data('monitoring_server_'.$monitoring_tab,time());
		
		foreach($monitoringList as $monitoring){
			$ttime = A('Common/System','Model')->important_data('monitoring_server_'.$monitoring);
			$ttime = intval($ttime);
			$dtime = time() - $ttime;
			if($dtime > 120){
				
				if(!S('monitoring_server_sms_'.$monitoring_tab)){
					A('Common/Alitongxin','Model')->monitoring_fault(13588258695,$monitoring,$dtime);
					S('monitoring_server_sms_'.$monitoring_tab,1,3600);
					echo '监测点'.$monitoring.'已有'.$dtime.'秒未响应,已发送短信'."\n"; 
				}else{
					echo '监测点'.$monitoring.'已有'.$dtime.'秒未响应'."\n";
				}
			} 
	
		}
		echo "end\n";
		
			
	}
	
	/*监控redis*/
	public function monitoring_redis(){
		
		$key = createNoncestr(8);
		$value = createNoncestr(8);
		
		$redis = redis();
		$mbValue = '';
		if($redis){
			S($key,$value,30);
			$mbValue = S($key);
		}	
		
		
		echo 'key:' . $key . "\n";

		
		
		if($mbValue != $value){
			echo 'redis已经死了'."\n";
			$ttime = A('Common/System','Model')->important_data('monitoring_redis');
			$ttime = intval($ttime);
			if(($ttime + 3600) < time()){
				A('Common/System','Model')->important_data('monitoring_redis',time());
				A('Common/Alitongxin','Model')->alarm('13588258695','DMP高速缓存服务',date('H:i:s'),'无法连接');
				echo '已发送通知'."\n";
			}
			
		}else{
			echo 'redis正常工作中'."\n";
		}
		
		
	}
	
	
	
	public function test_up(){
		
		
		$this->display();
	}
	
	/*获取我的IP*/
	public function my_ip(){
		echo get_client_ip();
	}
	
	/*检查数据库表的自增id是否合法*/
	public function acc(){
		
		
		$table_list = M()->query('select table_name,auto_increment from information_schema.tables where TABLE_SCHEMA = "hzsj_dmp" and table_type="base table" limit 1000;');
		

		foreach($table_list as $table){
			
			$table_name = $table['table_name'];
			
			
			$table_field = M()->query('select * from information_schema.columns where table_name="'.$table['table_name'].'"; ');
			
			foreach($table_field as $tablefield){
				if($tablefield['COLUMN_KEY'] == 'PRI'){
					$primary_key = $tablefield['COLUMN_NAME'];
					continue;
				}
			}
			
			

			$auto_increment = intval($table['auto_increment']);

			$max_primary_key = intval(M($table['table_name'])->order($table_field[0]['COLUMN_NAME'] .' desc')->getField($table_field[0]['COLUMN_NAME']));
			
			echo $table_name.'	'.$primary_key.'	'.$auto_increment.'	'.	$max_primary_key.'	'.($auto_increment - $max_primary_key)."\n";
			
		}
	}
	
	

}