Vue.prototype.$ELEMENT = {
    size: 'mini',
    zIndex: 3000
  }
  const vm = new Vue({
    el: "#app",
    data() {
      return {
        timeValue: [], //时 间
        bool:true,
        toggle: true,
        planData:[],
        itm:[],
        formInline: {
          fadname: '',
          fadowner: '',
          adilltype: '-1',
          fstatus: '8',
          pageIndex: 0,
          priority: '-1',
          timeorder: '1',
          check_status: '-1',
        },
        statusArr: [
          {
            value: '1',
            label: '截图中',
            class: 'textRed'
          },
          {
            value: '2',
            label: '待检查',
            class: 'textRed'
          },
          {
            value: '3',
            label: '截图失败',
            class: 'textRed'
          },
          {
            value: '4',
            label: '待补录',
            class: 'textRed'
          },
          {
            value: '5',
            label: '补录中',
            class: 'textRed'
          },
          {
            value: '6',
            label: '已通过',
            class: 'textGreen'
          },
          {
            value: '7',
            label: '作废',
            class: 'textRed'
          },
          {
            value: '8',
            label: '待检查',
            class: 'textRed'
          },
          {
            value: '14',
            label: '识别中',
          },
        ],
        statusArr2: [
          {
            value: '1',
            label: '截图中',
          },
          {
            value: '3',
            label: '截图失败',
          },
          {
            value: '8',
            label: '待检查',
          },
          {
            value: '4',
            label: '待补录',
          },
          {
            value: '5',
            label: '补录中',
          },
          {
            value: '14',
            label: '识别中',
          },
          {
            value: '6',
            label: '已通过',
          },
          {
            value: '7',
            label: '作废',
          },
        ],
        haveData: true,
        priority: {
          '0': '一级',
          '1': '二级',
          '2': '三级',
          '4': '四级',
          '5': '五级',
        },
        show: false,
        changePriority: '0',
        haveMore: true,
        title:'',
        url: '',
        textarea: '',
        dialogVisible: false,
        blArr: [
          '截图信息有误',
          '广告信息有误',
          '违法判定有误',
        ],
        radios: [
          { name: '广告落地页已失效', label: '1' },
          { name: '推广类广告',  label: '2'},
          { name: '其他', label: '3'},
        ],
        checkId: '',
        revokeTitle: '',
        detailData: [
          {
            type: '违法',
            time: '2019-07-29',
            reason: '信息不良'
          },
          {}
        ],
        zfReason: '广告落地页已失效',
        ruleForm: {
          blReason: [],
        },
        rules: {
          blReason: [
            { type: 'array', required: true, message: '请至少勾选一项原因', trigger: 'change' }
          ],
        },
        count: '',
        dblcount: '',
        djccount: '',
        jtzcount: '',
        btnLoading: false,
      }
    },
    computed: {
      taskid(){
        return this.getQueryString('id')
      },
      fdstatus(){
        return this.getQueryString('fdstatus')
      }
    },
    watch: {
      toggle(val){
        this.formInline.timeorder = val ? '1' : '0'
        this.PlanDataList(true)
      }
    },
    created() {
      this.PlanDataList()
    },
    mounted() {
      this.myscroll()
      this.$nextTick(() => {
        this.getTableHeight()
        document.querySelector('#app').style.opacity = 1
      })
      window.onresize = () => {
        this.getTableHeight()
      }
    },
    methods: {
      handleCommand(type){
        this.$refs[`open${type}`].$el.click()
      },
      // 状态显示调用的方法
      findStatus(item){
        return this.statusArr.find((i)=>{ return i.value == item })
      },
      myscroll(){
        _this = this
        document.getElementsByClassName('wrapper')[0].onscroll = function (e){
          if(this.scrollTop > (this.scrollHeight-1000) && _this.haveData){  // 判断滚动条滚动的距离 ， 滚动的距离等于元素的高度，代表滚动到底部，1000 可以修改 
            _this.PlanDataList() // 传个true代表是滚动触发的请求
          }
        }
      },
      getList(){
        this.haveData = true
        this.planData = []
        this.formInline.pageIndex = 0
        this.PlanDataList()
      },
      // 页面初始化加载数据
      PlanDataList(clear){
        if(clear){
          this.formInline.pageIndex = 0
          this.planData = []
        }
        if(!this.bool) {
          // bool 每次请求成功之前不允许调用接口，直接返回
          return
        }
        // 开关默认开启，  一进来关闭开关
        this.bool = false
        this.formInline.pageIndex += 1     // 请求之后让page ++
        const id = this.getQueryString('id')
        const data = {
          taskid: id,
          ...this.formInline
        }
        // 设置 起止时间
        let timeValue = this.timeValue || ['', '']
        data.sttime = timeValue[0]
        data.endtime = timeValue[1]
        $.post('/TaskInput/NetAd/PlanDataList',JSON.stringify(data)).then((res)=>{
          if(res.data.length < 20){
            this.haveData = false
            this.haveMore = false
          }
          this.planData =  clear ? res.data : this.planData.concat(res.data)
          this.count = res.count
          this.jtzcount = res.jtzcount
          this.dblcount= res.dblcount
          this.djccount= res.djccount
          this.bool = true // 请求成功之后打开开关
        })

      },
      // 查询
      getTableHeight() {
        this.tableHeight = `${document.body.clientHeight - 100}px`
      },
      // 修改检查优先级
      showDialog(flag) {
        if(flag) {
          this.title = '检查优先级'
          this.show = true
          this.url = '/TaskInput/NetAd/PlanOrderAction'
        } else {
          this.title = '截图优先级'
          this.url = '/TaskInput/NetAd/PlanCutOrderAction'
          this.show = true
        }
      },
      // 提交修改优先级对话框
      submitChangePriority() {
        this.btnLoading = true
        let [sttime, endtime ] = this.timeValue || ['', '']
        let data = {
          taskid: this.taskid,
          selad: [],
          sttime,
          endtime,
          ...this.formInline,
          editpriority: this.changePriority,
        }
        $.post(this.url, JSON.stringify(data))
          .then(res => {
            if (res.code === 0) {
              this.btnLoading = false
              this.$notify({
                message: res.msg,
                type: 'success',
                duration: 1500
              });
              this.getList()
              this.show = false
            } else {
              this.$notify.error({
                message: res.msg,
                duration: 1000
              });
          }
        })
      },
      getQueryString(name){
        let reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)")
        let r = window.location.search.substr(1).match(reg)
        if (r != null) return unescape(r[2])
        return null
      },
      orderEven(val) {
        this.getList()
      },
      // 通过 不通过 作废 事件
      submit(item, index, val) {
        this.itm = item
        this.checkId = item.id
        this.clickNum =  val
        const data = {
          "types": val,
          "taskid": item.id,
          "reason": this.textarea,
        }
        switch (val) {
          case 1: // 通过
              this._submit(data, '')
          break
          case 2: // 不通过
              this.dialogVisible = true
              this.revokeTitle = '转补录原因'
          break
          case 3: // 作废
              this.dialogVisible = true
              this.revokeTitle = '作废原因'
          break
        }
      },

      // 不通过 作废 提交按钮事件
      submitReturn() {
        const data = {
          "types":this.clickNum,
          "taskid": this.checkId,
        }
        this.$refs.ruleForm.validate((valid) => {
          if (valid) {
            if(this.revokeTitle == '转补录原因') {
            data.reasonsel = this.ruleForm.blReason
            this._submit(data, this.textarea)
            } else {
              this._submit(data, this.zfReason)
            }
            this.dialogVisible = false
          } else {
            return false;
          }

        })
      },

      // 通过 不通过 作废 事件 请求方法
      _submit(data, value) {
        this.planData = this.planData.filter(i => data.taskid !== i.id)
        data.reason = value
        $.post('/TaskInput/NetAd/PlanDataCheckAction',JSON.stringify(data))
        .then(res => {
          if(res.code === 0) {
            this.textarea = ''
          }
        })
      },

      // 关掉dialog 对话框
      closeDialog() {
        this.textarea = ''
        this.ruleForm.blReason = []
        this.zfReason = '广告落地页已失效'
        this.$refs.ruleForm.resetFields();
      },

      //数据撤消
      doBack(id,taskid){
        const data = {
          "id": id,
          "taskid": taskid
        }
        this.$confirm('撤消该处理结果, 是否继续?', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          $.post('/TaskInput/NetAd/PlanDataBackAction', JSON.stringify(data))
          .then(res => {
            if (res.code === 0) {
              this.$notify({
                message: res.msg,
                type: 'success',
                duration: 1500
              });
              this.getList()
            } else {
              this.$notify.error({
                message: res.msg,
                duration: 1000
              });
            }
          })
        }).catch(() => {
          this.$message({
            type: 'info',
            message: '已取消'
          });          
        });
        
      }
    }
  })