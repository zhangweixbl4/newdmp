
select task_input.fadid,(select min(fadid) from tad as tad2 where fadname = tad.fadname)
from task_input
join tad on tad.fadid = task_input.fadid
where tad.fadname in (select fadname from tad group by fadname HAVING count(*) > 1)
		and task_input.fadid <> (select min(fadid) from tad as tad2 where fadname = tad.fadname)

;

select ttvsample.fadid,(select min(fadid) from tad as tad2 where fadname = tad.fadname)
from ttvsample
join tad on tad.fadid = ttvsample.fadid
where tad.fadname in (select fadname from tad group by fadname HAVING count(*) > 1)
		and ttvsample.fadid <> (select min(fadid) from tad as tad2 where fadname = tad.fadname)

;

select tbcsample.fadid,(select min(fadid) from tad as tad2 where fadname = tad.fadname)
from tbcsample
join tad on tad.fadid = tbcsample.fadid
where tad.fadname in (select fadname from tad group by fadname HAVING count(*) > 1)
		and tbcsample.fadid <> (select min(fadid) from tad as tad2 where fadname = tad.fadname)

;

select tpapersample.fadid,(select min(fadid) from tad as tad2 where fadname = tad.fadname)
from tpapersample
join tad on tad.fadid = tpapersample.fadid
where tad.fadname in (select fadname from tad group by fadname HAVING count(*) > 1)
		and tpapersample.fadid <> (select min(fadid) from tad as tad2 where fadname = tad.fadname)

;

update task_input
join tad on tad.fadid = task_input.fadid
set task_input.fadid = (select min(fadid) from tad as tad2 where fadname = tad.fadname)
where tad.fadname in (select fadname from tad group by fadname HAVING count(*) > 1)
		and task_input.fadid <> (select min(fadid) from tad as tad2 where fadname = tad.fadname)

;

update ttvsample
join tad on tad.fadid = ttvsample.fadid
set ttvsample.fadid = (select min(fadid) from tad as tad2 where fadname = tad.fadname)
where tad.fadname in (select fadname from tad group by fadname HAVING count(*) > 1)
		and ttvsample.fadid <> (select min(fadid) from tad as tad2 where fadname = tad.fadname)

;

update tbcsample
join tad on tad.fadid = tbcsample.fadid
set tbcsample.fadid = (select min(fadid) from tad as tad2 where fadname = tad.fadname)
where tad.fadname in (select fadname from tad group by fadname HAVING count(*) > 1)
		and tbcsample.fadid <> (select min(fadid) from tad as tad2 where fadname = tad.fadname)

;

update tpapersample
join tad on tad.fadid = tpapersample.fadid
set tpapersample.fadid = (select min(fadid) from tad as tad2 where fadname = tad.fadname)
where tad.fadname in (select fadname from tad group by fadname HAVING count(*) > 1)
		and tpapersample.fadid <> (select min(fadid) from tad as tad2 where fadname = tad.fadname)

;
