<?php
namespace Api\Controller;
use Think\Controller;

class NjHwAdController extends Controller {
	
	
	
	#获取发布记录列表
	public function interface_record(){
		set_time_limit(600);
		session_write_close();
		$xxUrl = 'https://service.91xianxia.com/interface/record';
		
		$getData = array();
		if(I('create_time') && I('end_time')){
			$getData['create_time'] = I('create_time');
			$getData['end_time'] = I('end_time');
		}else{
			$getData['create_time'] = date('Y-m-d',time()-86400*7);
			$getData['end_time'] = date('Y-m-d');
		}

		$getHeader = array('token:13e3890d-0211-11ea-9f7e-7cd30abdf0fc', 'vendorId:1');

		$reqData = http($xxUrl,$getData,'GET',$getHeader,500);
		
		$reqDataArr = json_decode($reqData,true);
		
		foreach($reqDataArr['data']['list'] as $Data){
			$pointInfo = M('huwai_point')->where(array('space_id'=>$Data['space_id']))->find();
			$mediaInfo = M('tmedia')->field('fid,fmedianame')->where(array('fmediacode'=>'hw_'.$pointInfo['fregionid']))->find();
			
			if(!$pointInfo){
				echo $Data['space_id']."\n";
			}
			
			$fadclasscode = M('tadclass')->cache(true,600)->where(array('fadclass'=>array('like','%'.$Data['trade_type_second'].'%')))->order('LENGTH(fcode) desc')->getField('fcode');
			if(!$fadclasscode) $fadclasscode = '2301';
			#var_dump($fadclasscode);
			$addData = array();
			
			$addData['fissuedate'] = date('Y-m-d',strtotime($Data['monitor_time']));
			$addData['fissuedatetime'] = $Data['monitor_time'];
			$addData['fadname'] = $Data['theme'];
			$addData['fadbrand'] = $Data['brand'];
			$addData['fadclasscode'] = $fadclasscode;
			$addData['fadclasscode_v2'] = '3203';
			
			
			$addData['fimgs'] = implode(',',$Data['imgs']);
			$addData['fmediaid'] = $mediaInfo['fid'];
			$addData['identify'] = md5($mediaInfo['fid'].'_'.$Data['record_id']);
			$addData['spread_info'] = json_encode($Data);
			$addData['fcreatetime'] = date('Y-m-d H:i:s');
			$addData['point_id'] = intval(M('huwai_point')->where(array('fsource'=>'南京接口','space_id'=>$Data['space_id']))->getField('fid'));
			
			$addData['fsource'] = '南京接口';
			$addData['is_illegal'] = $Data['is_illegal'];
			if(count($Data['illegal_info']) > 0){
				$addData['fillegalcontent'] = $Data['illegal_reason'];
				$addData['fillegaltypecode'] = 30;
				$fexpressioncodesArr = [];  
				foreach($Data['illegal_info'] as $te1){
					$fexpressioncodesArr[] = $te1['illegal_code'];
				}
				$addData['fexpressioncodes'] = implode(';',$fexpressioncodesArr);

			}
			
				
			if( M('huwai_ad')->where(array('identify'=>$addData['identify']))->count() == 0){
				try{//
					M('huwai_ad')->add($addData);
				}catch( \Exception $error) {//
					
				}
			}else{
				M('huwai_ad')->where(array('identify'=>$addData['identify']))->save(array('point_id'=>$addData['point_id']));
			}
		}
		
		echo date('Y-m-d H:i:s'). ' end';

	}
	
	
	#获取点位信息
	public function interface_point(){
		set_time_limit(600);
		session_write_close();
		$xxUrl = 'https://service.91xianxia.com/interface/point';
		$getData = array();
		
		if(I('create_time') && I('end_time')){
			$getData['create_time'] = I('create_time');
			$getData['end_time'] = I('end_time');
		}else{
			$getData['create_time'] = date('Y-m-d',time()-86400*7);
			$getData['end_time'] = date('Y-m-d');
		}

		$getHeader = array('token:13e3890d-0211-11ea-9f7e-7cd30abdf0fc', 'vendorId:1');

		$reqData = http($xxUrl,$getData,'GET',$getHeader,60);
		
		#echo $reqData;
		$reqDataArr = json_decode($reqData,true);
		
		foreach($reqDataArr['data']['list'] as $Data){
			

			$addData = array();
			
			$regionInfo = M('tregion')->where(array('fname'=>$Data['district']))->find();
			
			$addData['space_id'] = $Data['space_id'];
			$addData['fname'] = $Data['name'];
			$addData['fregionid'] = $regionInfo['fid'];
			$addData['flat'] = $Data['lat'];
			$addData['flng'] = $Data['lng'];
			$addData['faddress'] = $Data['address'];
			$addData['link_phone'] = $Data['link_phone'];
			$addData['fsource'] = '南京接口';
			if(M('huwai_point')->where(array('fsource'=>'南京接口','space_id'=>$Data['space_id']))->count() == 0){
				try{//
					M('huwai_point')->add($addData);
				}catch( \Exception $error) {//
					
				}
			}
				

			$ck_media = M('tmedia')->where(array('fmediacode'=>'hw_'.$regionInfo['fid']))->count();
			
			if(!$ck_media){
				$mediaowner = M('tmediaowner')->master(true)->where(array('fcreditcode'=>'hw_'.$regionInfo['fid']))->find();
				if($mediaowner){
					$mediaownerid = $mediaowner['fid'];
				}else{
					
					$a_e_data = array();
					$a_e_data['fregaddr'] = '';//注册地址
					$a_e_data['fofficeaddr'] = '';//办公地址
					$a_e_data['fname'] = $regionInfo['ffullname'].'_户外';//媒介机构名称
					$a_e_data['flinkman'] = '';//联系人
					$a_e_data['ftel'] = '';//联系电话
					$a_e_data['fcreditcode'] = 'hw_'.$regionInfo['fid'];//企业信用代码
					$a_e_data['fenterprise'] = '';//设立主体
					
					$a_e_data['flicence'] = '';//特许经营许可证
					$a_e_data['fregionid'] = $regionInfo['fid'];//行政区划ID
					
					$a_e_data['fstate'] = 1;//状态，-1删除，0-无效，1-有效
					$a_e_data['fcreator'] = '南京户外接口';//创建人
					$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
					$a_e_data['fmodifier'] = '南京户外接口';//修改人
					$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
					$mediaownerid = M('tmediaowner')->add($a_e_data);//添加数据
					
				}
				
				$a_e_data = array();
				$a_e_data['fmediaclassid'] = '05';//媒介类别id
				
				$a_e_data['fmediaownerid'] = $mediaownerid;//媒介机构ID
				$a_e_data['media_region_id'] = $regionInfo['fid'];
				$a_e_data['fdeviceid'] = 0;//采集设备ID
				$a_e_data['fmediacode'] = 'hw_'.$regionInfo['fid'];//媒介代码
				$a_e_data['fmedianame'] = $regionInfo['ffullname'].'(南京户外)';//媒介名称
				$a_e_data['faddr'] = '';//媒介地址

				$a_e_data['fcollecttype'] = 30;//采集方式

				$a_e_data['fchannelid'] = 0;//通道号

				$a_e_data['priority'] = 1;//优先级
				
				$a_e_data['auto_ext_day_ago'] = 0;
				
				$a_e_data['fcreator'] = '南京户外接口';//创建人
				$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
				$a_e_data['fmodifier'] = '南京户外接口';//修改人
				$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
				
				$media_id = M('tmedia')->add($a_e_data);//添加数据
				if($media_id){
					M('tmedia')->where(array('fid'=>$media_id))->save(array('main_media_id'=>$media_id));
				}			
			}	
		
		}
		
		
		echo date('Y-m-d'). ' end';
		
		
	}
	
	
	#获取违法数据
	public function interface_illegal(){
		set_time_limit(600);
		session_write_close();
		$xxUrl = 'https://service.91xianxia.com/interface/illegal';
		$getData = array();
		
		if(I('create_time') && I('end_time')){
			$getData['create_time'] = I('create_time');
			$getData['end_time'] = I('end_time');
		}else{
			$getData['create_time'] = date('Y-m-d',time()-86400*7);
			$getData['end_time'] = date('Y-m-d');
		}

		$getHeader = array('token:13e3890d-0211-11ea-9f7e-7cd30abdf0fc', 'vendorId:1');

		$reqData = http($xxUrl,$getData,'GET',$getHeader,60);
		
		$reqDataArr = json_decode($reqData,true);
		
		foreach($reqDataArr['data']['list'] as $Data){
			$illSamAdd = array();
			$illSamAdd['identify'] = 'nanjing_huwai';
			$illSamAdd['fmedia_class'] = 5;
			$illSamAdd['fsample_id'] = $Data['record_id'];
			$illSamAdd['fcustomer'] = 320100;
			$illSamAdd['unique_k'] = '';
			
			
			
			
			
			
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}