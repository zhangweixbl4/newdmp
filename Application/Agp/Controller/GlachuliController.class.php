<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 国家局立案处理
 * by zw
 */

class GlachuliController extends BaseController{
  /**
  * 处理结果列表
  * by zw
  */
  public function lajieguo_list(){
    Check_QuanXian(['lachuli']);
    session_write_close();
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $agpDataMerge = $ALL_CONFIG['agpDataMerge'];
    $cusHosts = json_decode($ALL_CONFIG['agpCustomerMarge'],true);

    $outtype = I('outtype');//导出类型
    if(!empty($outtype)){
      $p  = I('page', 1);//当前第几页ks
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页ks
      $pp = 20;//每页显示多少记录
    }
    
    if($agpDataMerge == 1 || $agpDataMerge == 2){
      $cusHosts = array_intersect(S(session('regulatorpersonInfo.fcode').'checkCustomer'),$cusHosts);
      foreach ($cusHosts as $key => $value) {
        if($_SERVER['HTTP_HOST'] != $value && $system_num != $value){
          $joinArr[] = $this->returnListSql($value);
        }else{
          $joinArr[] = $this->returnListSql();
        }
      }
      $joinStr = '('.implode(') union all(', $joinArr).')';
    }else{
      $joinStr = $this->returnListSql($system_num);
    }

    $whereStr = $this->returnListWhere();

    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id'.$wheres)
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 10 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->where($whereStr)
      ->count();//查询满足条件的总记录数

    //安徽特定数据
    if($system_num == '340000'){
      $addfield = ',a.fprice,a.fnumber';
    }

    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus,a.fstatus2,a.fstatus3,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,ifnull(datediff(now(),a.fresult_datetime),0) as ladiffday,a.fadowner as fadownername,tn.ffullname,a.favifilename,a.fjpgfilename,a.fresult_datetime,a.fresult_unit,a.fresult_user,ifnull(db.dbcount,0) dbcount,a.fcustomer'.$addfield)
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id'.$wheres)
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('('.$joinStr.') as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 10 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->join('(select count(*) dbcount,fillegal_ad_id from tbn_illegal_ad_attach where fstate = 0 and ftype = 30 group by fillegal_ad_id) db on a.fid = db.fillegal_ad_id','left')
      ->where($whereStr)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();

    if($agpDataMerge == 1 || $agpDataMerge == 2){
      foreach ($do_tia as $key => $value) {
        $do_tia[$key]['customerName'] = A('Core')->returnSystemType($value['fcustomer'],$system_num);//所属系统类别
      }
    }
 
    if(!empty($outtype)){
        if(empty($do_tia)){
          $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
        }

        $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-立案调查处理列表';//文档内部标题名称
        $outdata['datalie'] = [
          '序号'=>'key',
          '地域'=>'ffullname',
          '发布媒体'=>'fmedianame',
          '媒体分类'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1','电视'],
              ['{fmedia_class} == 2','广播'],
              ['{fmedia_class} == 3','报纸'],
              ['{fmedia_class} == 13','互联网'],
            ]
          ],
          '广告类别'=>'fadclass',
          '广告名称'=>'fad_name',
          '播出总条次'=>'fcount',
          '播出周数'=>'diffweek',
          '播出时间段'=>[
            'type'=>'zwhebing',
            'data'=>'{fstarttime}~{fendtime}'
          ],
          '违法表现代码'=>'fillegal_code',
          '违法表现'=>'fexpressions',
          '涉嫌违法原因'=>'fillegal',
          '证据下载地址'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1 || {fmedia_class} == 2','{favifilename}'],
              ['','{fjpgfilename}']
            ]
          ],
          '查看状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fview_status} == 0','未查看'],
              ['{fview_status} == 10','已查看'],
            ]
          ],
          '处理状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus} == 0','未处理'],
              ['{fstatus} == 10','已上报'],
              ['{fstatus} == 30','数据复核'],
            ]
          ],
          '处理结果'=>'fresult',
          '处理时间'=>'fresult_datetime',
          '处理机构'=>'fresult_unit',
          '处理人'=>'fresult_user',
          '立案状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus2} == 15','待处理'],
              ['{fstatus2} == 16','已处理'],
              ['{fstatus2} == 17','处理中'],
              ['{fstatus2} == 20','撤消'],
            ]
          ],
          '复核状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus3} == 10','复核中'],
              ['{fstatus3} == 20','复核失败'],
              ['{fstatus3} == 30','复核通过待调整'],
              ['{fstatus3} == 40','复核通过已调整'],
              ['{fstatus3} == 50','复核有异议'],
            ]
          ],
        ];
        if($system_num == '340000'){
          $outdata['datalie']['文书编号'] = 'fnumber';
          $outdata['datalie']['罚没金额'] = 'fprice';
        }
        $outdata['lists'] = $do_tia;
        $ret = A('Api/Function')->outdata_xls($outdata);

        D('Function')->write_log('立案调查处理列表',1,'导出成功');
        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

      }else{
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
      }
  }

  /**
  * 上报立案处理结果
  * by zw
  */
  public function sczhengju(){
    $selfid     = I('selfid');//违法广告ID组
    $attachinfo = I('attachinfo');//上传的文件信息
    $submit     = I('submit');//保存是1，否则为空
    $fnumber = I('fnumber');//文书编号
    $fprice = I('fprice');//罚没金额
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $islafile = $ALL_CONFIG['islafile'];

    $attach     = [];

    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }

    //安徽特定
    if((string)$system_num == '340000'){
      if((empty($fnumber) || empty($fprice)) && empty($submit)){
        $this->ajaxReturn(array('code'=>1,'msg'=>'结案时文书编号和罚没金额必填'));
      }
    }else{
      if(empty($attachinfo) && !empty($islafile)){
        $this->ajaxReturn(array('code'=>1,'msg'=>'请上传文件'));
      }
    }
    
    $where_tia['fstatus']   = 10;
    $where_tia['fstatus2']  = array('in',array(15,17));
    $where_tia['fresult']   = array('like','%立案%');
    foreach ($selfid as $key => $value) {
      $where_tia['fid']   = $value;
      $do_tia = M('tbn_illegal_ad')->field('fstatus2')->where($where_tia)->find();
      if(!empty($do_tia)){
        $data_tia['flatime'] = date('Y-m-d H:i:s');//立案处理时间
        $data_tia['fprice']          = $fprice;//罚没金额
        $data_tia['fnumber']         = $fnumber;//文书编号
        if(empty($submit)){
          $data_tia['fstatus2']   = 16;//已处理
        }
        M('tbn_illegal_ad')->where($where_tia)->save($data_tia);
        //上传附件
        $attach_data['ftype']  = 20;//类型
        $attach_data['fillegal_ad_id']  = $value;//违法广告
        $attach_data['fuploader']       = session('regulatorpersonInfo.fid');//上传人
        $attach_data['fupload_time']    = date('Y-m-d H:i:s');//上传时间
        foreach ($attachinfo as $key2 => $value2){
          $attach_data['fattach']     = $value2['fattachname'];
          $attach_data['fattach_url'] = $value2['fattachurl'];
          array_push($attach,$attach_data);
        }
      }
    }
    if(!empty($attach)){
      M('tbn_illegal_ad_attach')->addAll($attach);
    }
    D('Function')->write_log('立案调查处理',1,'操作成功','tbn_illegal_ad',0);
    $this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
  }

  /**
  * 查看案件证据
  * by zw
  */
  public function fjjieguo(){
    $system_num = getconfig('system_num');

    $fid = I('fid');//违法广告ID
    $where_tia['a.fillegal_ad_id'] = $fid;
    $where_tia['a.fstate']         = 0;
    $where_tia['a.ftype']         = ['in',[0,10,20]];

    //证据附件
    $do_tia = M('tbn_illegal_ad_attach')->field('a.fattach,a.fattach_url')->alias('a')->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id')->where($where_tia)->order('a.fid asc')->select();

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','zjfjdata'=>$do_tia,'lafjdata'=>$do_tia));
  }

  /**
  * 撤消立案处理结果
  * by zw
  */
  public function cxjieguo(){
    $selfid     = I('fid');//违法广告ID组
    $where_tia['fstatus']   = 10;
    $where_tia['fstatus2']  = 17;
    $where_tia['fresult']   = array('like','%立案%');
    $data_tia['fstatus']        = 0;
    $data_tia['fstatus2']       = 0;
    $data_tia['fstatus3']       = 0;
    $data_tia['fview_status']   = 0;
    $data_tia['fresult']        = '';
    $data_tia['flatime']        = null;
    $data_tia['fresult_datetime']  = null;
    $data_tia['fresult_unit']   = '';
    $data_tia['fresult_user']   = '';
    
    foreach ($selfid as $key => $value) {
      $where_tia['fid'] = $value;
      $do_tia = M('tbn_illegal_ad')->where($where_tia)->save($data_tia);//更改状态
      if(!empty($do_tia)){
        M()->execute('update tbn_illegal_ad_attach set fstate=10 where fstate=0 and fillegal_ad_id='.$value);
        M()->execute('update tbn_data_check_attach set fstate=10 where fstate=0 and fillegal_ad_id='.$value);
        M()->execute('update tbn_case_send set fstatus=0 where fstatus<>0 and fillegal_ad_id='.$value.' and fid in(select fid from (select max(fid) fid from tbn_case_send where fstatus<>0 and fillegal_ad_id='.$value.' ) a)');
      }
    }
    if(!empty($do_tia)){
      D('Function')->write_log('立案调查处理',1,'撤消成功','tbn_illegal_ad',$do_tia);
      $this->ajaxReturn(array('code'=>0,'msg'=>'撤消成功'));
    }else{
      D('Function')->write_log('立案调查处理',0,'撤消失败','tbn_illegal_ad',0,M('tbn_illegal_ad')->getlastsql());
      $this->ajaxReturn(array('code'=>1,'msg'=>'撤消失败'));
    }
  }

  /**
  * 生成unionsql
  * by zw
  */
  public function returnListSql($system_num = ''){
    $iscontain = I('iscontain');//是否包含下属地区
    $area = I('area');//所属地区
    $netadtype  = I('netadtype');//平台类别

    $ALL_CONFIG = getconfig('ALL',$system_num);
    $system_num = $ALL_CONFIG['system_num'];
    $illadTimeoutTips = $ALL_CONFIG['illadTimeoutTips'];//线索处理超时提示，0无，1有
    $againIssueTips = $ALL_CONFIG['againIssueTips'];//上月已做案件处罚后，本月还在继续播放的，是否需要进行提醒，0不需要，1需要
    $illSupervise = $ALL_CONFIG['illSupervise'];//是否需要案件处理的执法监督功能，0不需要，1需要
    $ischeck = $ALL_CONFIG['ischeck'];
    $illDistinguishPlatfo = $ALL_CONFIG['illDistinguishPlatfo'];//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode($ALL_CONFIG['media_class'],true);//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）
    $isexamine = $ALL_CONFIG['isexamine'];
    $isrelease = $ALL_CONFIG['isrelease'];

    //媒体类别格式化
    foreach ($media_class as $value) {
      $mClasses[] = (int)$value;
    }

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $where_time = gettimecondition($years,$timetypes,$timeval,'b.fissue_date',0,$isrelease);

    $where_time .= ' and a.fstatus = 10';
    $where_time .= ' and a.fresult like "%立案%"';
    $where_time .= ' and a.fstatus2 in(15,17)';

    //是否抽查模式
    if(!empty($ischeck)){
        $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
        //如果抽查表有数据
        if(!empty($spot_check_data)){
            $dates = [];//定义日期数组
            foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                $year_month = '';
                $date_str = [];
                $year_month = substr($spot_check_data_val['fmonth'],0,7);
                if(!empty($spot_check_data_val['condition'])){
                  $date_str = explode(',',$spot_check_data_val['condition']);
                }
                foreach ($date_str as $date_str_val){
                    $dates[] = $year_month.'-'.$date_str_val;
                }
            }
          $where_time   .= ' and b.fissue_date in ("'.implode('","', $dates).'")';
        }else{
          $where_time   .= ' and 1=0';
        }
    }
    
    if(!empty($netadtype)){
      $where_time .= ' and a.fplatform = '.$netadtype;
    }
    
    if(!empty($area)){//所属地区
      if($area != '100000'){
        if(!empty($iscontain)){
          $tregion_len = get_tregionlevel($area);
          if($tregion_len == 1 || $tregion_len == 6){//国家级
            $where_time .= ' and a.fregion_id = '.$area;
          }elseif($tregion_len == 2){//省级
            $where_time .= ' and a.fregion_id like "'.substr($area,0,2).'%"';
          }elseif($tregion_len == 4){//市级
            $where_time .= ' and a.fregion_id like "'.substr($area,0,4).'%"';
          }
        }else{
          $where_time .= ' and a.fregion_id = '.$area;
        }
      }
    }

    if(in_array($isexamine, [10,20,30,40,50])){
      $where_time .= ' and a.fexamine = 10';
    }
    if(!empty($illDistinguishPlatfo)){
      $where_time .= ' and a.fmedia_class in('.implode(',', $mClasses).')';
    }

    return 'select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue b,tbn_illegal_ad a where '.$where_time.' and a.fid = b.fillegal_ad_id and a.fcustomer = "'.$system_num.'" group by fillegal_ad_id';
  }

  /**
  * 生成where条件
  * by zw
  */
  private function returnListWhere(){
    $whereStr = '1=1';
    $search_type  = I('search_type');//搜索类别
    $search_val   = I('search_val');//搜索值
    $adclass   = I('adclass');//广告类别名
    $mclass   = I('mclass');//媒体类别

    if(!empty($search_val)){
      if($search_type == 10){//媒体分类
        $whereStr .= ' and a.fmedia_class = '.$search_val;
      }elseif($search_type == 20){//广告类别
        $whereStr .= ' and b.ffullname like "%'.$search_val.'%"';
      }elseif($search_type == 30){//广告名称
        $whereStr .= ' and a.fad_name like "%'.$search_val.'%"';
      }elseif($search_type == 40){//发布媒体
        $whereStr .= ' and c.fmedianame like "%'.$search_val.'%"';
      }elseif($search_type == 50){//违法原因
        $whereStr .= ' and a.fillegal like "%'.$search_val.'%"';
      }elseif($search_type == 60){//广告主
        $whereStr .= ' and tor.fname like "%'.$search_val.'%"';
      }
    }

    if(!empty($adclass)){
      $whereStr .= ' and b.ffullname like "%'.$adclass.'%"';
    }

    if(!empty($mclass)){
      $whereStr .= ' and a.fmedia_class = "'.$mclass.'"';
    }

    return $whereStr;
  }


}