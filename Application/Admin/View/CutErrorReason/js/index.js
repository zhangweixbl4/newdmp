Vue.prototype.$ELEMENT = { size: 'mini', zIndex: 3000 }

const vm = new Vue({
    el: '#app',
    components:{
        reasonInfo
    },
    data: function () {
        return{
            loading: false,
            where: {
                keyword: '',
                mediaClass: '',
            },
            table: [],
            currentId: 0,
            showInfo: false,
        }
    },
    created: function () {
        this.search()
    },
    methods: {
        search: function () {
            this.loading = true
            $.post('getCutErrorReason', this.where).then((res) => {
                this.loading = false
                this.table = res
            })
        },
        resetWhere: function () {
            this.where = {}
            this.search()
        },
        delItem: function (id) {
            this.$confirm('确认删除?').then(() => {
                this.loading = true
                $.post('deleteCutErrorReason', {id}).then((res) => {
                    this.loading = true
                    this.search()
                    if (res.code == 0){
                        this.$message.success(res.msg)
                    } else {
                        this.$message.error(res.msg)
                    }
                })
            }).catch(() => {})
        },
        closeInfo: function () {
            this.search()
            this.currentId = 0
            this.showInfo = false
        },
        openInfoPage: function (id) {
            this.currentId = +id
            this.showInfo = true
            this.$nextTick(() => {
                this.$refs.reasonInfo.dataInit()
            })
        }
    }
})