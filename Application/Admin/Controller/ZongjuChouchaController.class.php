<?php

namespace Admin\Controller;
use Think\Controller;
use Think\Exception;


class ZongjuChouchaController extends BaseController {





	/*开始汇总*/	
    public function start_gather(){
		session_write_close();
		$fid = I('fid');
		$chouchaInfo = M('zongju_data_childconfirm')->where(array('fid'=>$fid))->find();
		if($chouchaInfo['gather_group']){//判断之前是否汇总过
			$this->ajaxReturn(array('code'=>-1,'msg'=>'当前状态不允许汇总'));
			//M('z_gather_task')->where(array('fgroup'=>$chouchaInfo['gather_group']))->delete();//如果汇总过，就删除之前的汇总数据
		}
		$condition = json_decode($chouchaInfo['condition'],true);//抽查的详细日期
		
		//var_dump($condition);
		$mediaList = M('tmedia')
							->field('tmedia.fid,tmedia.fmedianame,left(tmedia.fmediaclassid,2) as media_class')
							->join('tmedialabel on tmedialabel.fmediaid = tmedia.fid')
							->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
							
							->where(array('tlabel.flabel'=>'国家局','left(tmedia.fmediaclassid,2)'=>array('in','01,02,03'),'tmedia.priority'=>array('egt',0)))
							->select();
		$fgroup = createNoncestr(8);//分组，随机字符串					
		$addData = array();//需要新增的数据
		foreach($mediaList as $media){//循环媒体
			$chouchaDates = array();//初始化需要抽查的日期
			if($media['media_class'] == '01'){//判断媒体类型
				$chouchaDates = $condition['tv'];//如果该媒体是电视，使用电视的抽查日期
			}elseif($media['media_class'] == '02'){//判断媒体类型
				$chouchaDates = $condition['bc'];//如果该媒体是广播，使用广播的抽查日期
			}elseif($media['media_class'] == '03'){//判断媒体类型
				$chouchaDates = $condition['paper'];//如果该媒体是报纸，使用报纸的抽查日期
			}else{
				continue;
			}

			foreach($chouchaDates as $chouchaDate){
				$chouchaDate = intval($chouchaDate);//日期转为整数
				if($chouchaDate < 10){//如果日期小于10
					$chouchaDate = '0'.$chouchaDate;//前面拼接0
				}else{//如果日期大于等于10
					$chouchaDate = strval($chouchaDate);//转为字符串
				}
				$addData[] = array(
									'fdate' => date('Y-m-',strtotime($chouchaInfo['fstarttime'])).$chouchaDate,
									'fmediaid' => $media['fid'],
									'fgroup' => $fgroup,
									'fstate' => '0',
									'fcreatetime' => date('Y-m-d H:i:s'),
									'fcustomer'=>100000,
									);
			}
			
		}
		//var_dump($addData);
		M()->startTrans();//开启事务
		M('z_gather_task')->addAll($addData);
		M('zongju_data_childconfirm')->where(array('fid'=>$chouchaInfo['fid']))->save(array('gather_group' => $fgroup));
		M()->commit();//提交事务
		
		//var_dump(M('zongju_data_childconfirm')->getLastSql());
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'提交成功'));
	}
	
	public function chouqu(){
		
		
		session_write_close();
		$fid = I('fid');
		$chouchaInfo = M('zongju_data_childconfirm')->where(array('fid'=>$fid))->find();
		if($chouchaInfo['chouqu_state'] == 1){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'当前状态不允许抽取数据'));
		}

		$condition = json_decode($chouchaInfo['condition'],true);//抽查的详细日期
		
		$mediaList = M('tmedia')
							->field('tmedia.fid,tmedia.fmedianame,left(tmedia.fmediaclassid,2) as media_class')
							->join('tmedialabel on tmedialabel.fmediaid = tmedia.fid')
							->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
							
							->where(array('tlabel.flabel'=>'国家局','left(tmedia.fmediaclassid,2)'=>array('in','01,02,03'),'tmedia.priority'=>array('egt',0)))
							->select();				
		$addData = array();//需要新增的数据
		foreach($mediaList as $media){//循环媒体
			$chouchaDates = array();//初始化需要抽查的日期
			if($media['media_class'] == '01'){//判断媒体类型
				$chouchaDates = $condition['tv'];//如果该媒体是电视，使用电视的抽查日期
			}elseif($media['media_class'] == '02'){//判断媒体类型
				$chouchaDates = $condition['bc'];//如果该媒体是广播，使用广播的抽查日期
			}elseif($media['media_class'] == '03'){//判断媒体类型
				$chouchaDates = $condition['paper'];//如果该媒体是报纸，使用报纸的抽查日期
			}else{
				continue;
			}

			foreach($chouchaDates as $chouchaDate){
				$chouchaDate = intval($chouchaDate);//日期转为整数
				if($chouchaDate < 10){//如果日期小于10
					$chouchaDate = '0'.$chouchaDate;//前面拼接0
				}else{//如果日期大于等于10
					$chouchaDate = strval($chouchaDate);//转为字符串
				}
				
				
				
				$addData[] = array(
									'forgid'=>'20100000',
									'fmediaclass'=>$media['media_class'],
									'fmediaid'=>$media['fid'],
									'fdate'=>date('Y-m-',strtotime($chouchaInfo['fstarttime'])).$chouchaDate,
									'fcreatetime'=>date('Y-m-d H:i:s')
								);
				
			}
			
		}
		
		
		
		M()->startTrans();//开启事务

		
		$rr = M('make_customer_ad_issue')->addAll($addData,array(),true);
		if($rr){
			$inC = count($addData);
		}else{
			$inC = 0;
		}
		M('zongju_data_childconfirm')->where(array('fid'=>$chouchaInfo['fid']))->save(array('chouqu_state' => 1));
		
		M()->commit();//提交事务
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'提交成功 '.$inC.' 个媒体天'));
	}
	
	/*查看汇总进度*/
	public function gather_progress(){
		session_write_close();
		$fid = I('fid');
		$chouchaInfo = M('zongju_data_childconfirm')->where(array('fid'=>$fid))->find();//查询抽查的详情
		
		//var_dump(0/0);
		if($chouchaInfo['gather_group']){
			$count_z = M('z_gather_task')->where(array('fgroup'=>$chouchaInfo['gather_group']))->count();//总数
			$count_1 = M('z_gather_task')->where(array('fstate'=>1,'fgroup'=>$chouchaInfo['gather_group']))->count();//已汇总完成数
			
			$progress0 = $count_1 / $count_z * 100;

		}else{
			
			$progress0 = 0;
		}
		$progress_arr = explode('.',strval($progress0));
		$progress = floatval(intval($progress_arr[0]).'.'.intval(substr($progress_arr[1],0,2)));
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','progress'=>$progress));
	}
	
	
	
	/*同步至违法确认*/
	public function to_illegal_ad_issue_review(){
		
		if(S('to_illegal_ad_issue_review')){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'服务器繁忙'));
		}
		S('to_illegal_ad_issue_review',1,360);
		session_write_close();
		set_time_limit(1200);
		$fid = I('fid');
		$chouchaInfo = M('zongju_data_childconfirm')->where(array('fid'=>$fid))->find();//查询抽查的详情
		$condition = json_decode($chouchaInfo['condition'],true);//抽查的详细日期
		$mediaList = M('tmedia')
							->field('tmedia.media_region_id,tmedia.fid,tmedia.fmedianame,left(tmedia.fmediaclassid,2) as media_class')
							->join('tmedialabel on tmedialabel.fmediaid = tmedia.fid')
							->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
							
							->where(array('tlabel.flabel'=>array('in',array('国家局','国家局_临时')),'left(tmedia.fmediaclassid,2)'=>array('in','01,02,03'),'tmedia.priority'=>array('egt',0)))
							->select();
		$illegalIssueList = array();
		foreach($mediaList as $media){

			if($media['media_class'] == '01'){//判断媒体类型
				
				foreach($condition['tv'] as $chouchaDate){
					$chouchaDate = intval($chouchaDate);//日期转为整数
					if($chouchaDate < 10){//如果日期小于10
						$chouchaDate = '0'.$chouchaDate;//前面拼接0
					}else{//如果日期大于等于10
						$chouchaDate = strval($chouchaDate);//转为字符串
					}
					$issueDate = date('Y-m-',strtotime($chouchaInfo['fstarttime'])).$chouchaDate;
					
					$issueTable = 'ttvissue_'.date('Ym',strtotime($issueDate)).'_'.substr($media['media_region_id'],0,2);
					
					$illegalIssueList0 = M($issueTable)
													->alias('issuetable')
													->field('
																issuetable.fmediaid as media_id,
																issuetable.fstarttime as start_time,
																issuetable.fendtime as end_time,
																issuetable.fsampleid as sam_id,
																(select fadname from tad where fadid = sam.fadid) as ad_name,
																(select fadclasscode from tad where fadid = sam.fadid) as fadclasscode,
																
																FROM_UNIXTIME(issuetable.fissuedate) as issue_date,
																"" as paper_jpg
																
																
																
															')
													->join('ttvsample as sam on sam.fid = issuetable.fsampleid')
													->where(array('sam.fillegaltypecode'=>30,'issuetable.fissuedate'=>strtotime($issueDate),'issuetable.fmediaid'=>$media['fid']))
													->select();
					$illegalIssueList = array_merge($illegalIssueList,$illegalIssueList0);
				}

				
				
			}elseif($media['media_class'] == '02'){//判断媒体类型
				foreach($condition['bc'] as $chouchaDate){
					$chouchaDate = intval($chouchaDate);//日期转为整数
					if($chouchaDate < 10){//如果日期小于10
						$chouchaDate = '0'.$chouchaDate;//前面拼接0
					}else{//如果日期大于等于10
						$chouchaDate = strval($chouchaDate);//转为字符串
					}
					$issueDate = date('Y-m-',strtotime($chouchaInfo['fstarttime'])).$chouchaDate;
					
					$issueTable = 'tbcissue_'.date('Ym',strtotime($issueDate)).'_'.substr($media['media_region_id'],0,2);
					
					$illegalIssueList0 = M($issueTable)
													->alias('issuetable')
													->field('
																issuetable.fmediaid as media_id,
																issuetable.fstarttime as start_time,
																issuetable.fendtime as end_time,
																issuetable.fsampleid as sam_id,
																(select fadname from tad where fadid = sam.fadid) as ad_name,
																(select fadclasscode from tad where fadid = sam.fadid) as fadclasscode,
																FROM_UNIXTIME(issuetable.fissuedate) as issue_date,
																"" as paper_jpg
																
																
																
															')
													->join('tbcsample as sam on sam.fid = issuetable.fsampleid')
													->where(array('sam.fillegaltypecode'=>30,'issuetable.fissuedate'=>strtotime($issueDate),'issuetable.fmediaid'=>$media['fid']))
													->select();
					$illegalIssueList = array_merge($illegalIssueList,$illegalIssueList0);
				}
				
				
				
			}elseif($media['media_class'] == '03'){//判断媒体类型
				foreach($condition['paper'] as $chouchaDate){
					$chouchaDate = intval($chouchaDate);//日期转为整数
					if($chouchaDate < 10){//如果日期小于10
						$chouchaDate = '0'.$chouchaDate;//前面拼接0
					}else{//如果日期大于等于10
						$chouchaDate = strval($chouchaDate);//转为字符串
					}
					$issueDate = date('Y-m-',strtotime($chouchaInfo['fstarttime'])).$chouchaDate;
					
					$issueTable = 'tpaperissue';
					
					$illegalIssueList0 = M($issueTable)
													->alias('issuetable')
													->field('
																issuetable.fmediaid as media_id,
																null as start_time,
																null as end_time,
																issuetable.fpapersampleid as sam_id,
																(select fadname from tad where fadid = sam.fadid) as ad_name,
																(select fadclasscode from tad where fadid = sam.fadid) as fadclasscode,
																issuetable.fissuedate as issue_date,
																sam.fjpgfilename as paper_jpg
																
																
																
															')
													->join('tpapersample as sam on sam.fpapersampleid = issuetable.fpapersampleid')
													->where(array('sam.fillegaltypecode'=>30,'issuetable.fissuedate'=>$issueDate,'issuetable.fmediaid'=>$media['fid']))
													->select();
					$illegalIssueList = array_merge($illegalIssueList,$illegalIssueList0);
				}
				
				
				
				
				
				
			}else{
				continue;
			}
			
		
		}
		
		$w_count = 0;
		M()->startTrans();//开启事务
		foreach($illegalIssueList as $illegalIssue){//循环查询到的违法记录
			if(substr($illegalIssue['fadclasscode'],0,2) == '23') continue; //如果广告分类是23，直接结束本次循环
			$ckWhere = array();
			$ckWhere['media_id'] = $illegalIssue['media_id'];
			if($illegalIssue['paper_jpg']){//如果是报纸
				$ckWhere['paper_jpg'] = $illegalIssue['paper_jpg'];
			}else{//电视、广播
				$ckWhere['start_time'] = $illegalIssue['start_time'];
				$ckWhere['end_time'] = $illegalIssue['end_time'];
				
			}
			$ck = M('illegal_ad_issue_review')->where($ckWhere)->count();	//查询数据有没有重复，用于排重
			$illegalIssue['fcustomer'] = 100000;
			if($ck == 0){
				$w_state = M('illegal_ad_issue_review')->add($illegalIssue);//写入数据
				if($w_state) $w_count ++;
			}
			
		}
		M()->commit();//提交事务
		S('to_illegal_ad_issue_review',null);
		$this->ajaxReturn(array('code'=>0,'msg'=>'成功新增了'.$w_count.'条待确认违法广告'));
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}