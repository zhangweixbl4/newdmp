<?php
namespace Adinput\Model;



class AuditTaskModel{



	/*新增电视任务*/
	public function add_tv_task($tvTaskInfo){
		$user_info = get_user_info();//获取用户信息
		$rr = M('ttvsample')->where(array('fid'=>$tvTaskInfo['fid']))->save(array('finspectstate'=>2));//修改电视任务为已接收状态
		if($rr == 0) return 0;
		$a_taskInfo = array();
		$a_taskInfo['mediaclass'] = '电视';//媒体类型
		$a_taskInfo['sample_table_name'] = 'ttvsample';//样本表的名字
		$a_taskInfo['sample_id'] = $tvTaskInfo['fid'];//样本ID
		$a_taskInfo['task_state'] = 1;//任务状态
		$a_taskInfo['money'] = C('AUDIT_PRICE_N') + (C('AUDIT_PRICE_L') * $tvTaskInfo['fadlen']);//任务金额
		$a_taskInfo['receive_time'] = date('Y-m-d H:i:s');//接收任务时间
		$a_taskInfo['user_id'] = $user_info['wx_id'];//用户ID
		$task_id = M('ad_audit_task')->add($a_taskInfo);//添加任务表
		return $task_id;//返回任务ID
	}
	
	/*新增广播任务*/
	public function add_bc_task($bcTaskInfo){
		$user_info = get_user_info();//获取用户信息
		$rr = M('tbcsample')->where(array('fid'=>$bcTaskInfo['fid']))->save(array('finspectstate'=>2));//修改任务为已接收状态
		if($rr == 0) return 0;
		$a_taskInfo = array();
		$a_taskInfo['mediaclass'] = '广播';//媒体类型
		$a_taskInfo['sample_table_name'] = 'tbcsample';//样本表的名字
		$a_taskInfo['sample_id'] = $bcTaskInfo['fid'];//样本ID
		$a_taskInfo['task_state'] = 1;//任务状态
		$a_taskInfo['money'] = C('AUDIT_PRICE_N') + (C('AUDIT_PRICE_L') * $bcTaskInfo['fadlen']);//任务金额
		$a_taskInfo['receive_time'] = date('Y-m-d H:i:s');//接收任务时间
		$a_taskInfo['user_id'] = $user_info['wx_id'];//用户ID
		$task_id = M('ad_audit_task')->add($a_taskInfo);//添加任务表
		return $task_id;//返回任务ID
	}
	
	/*新增报纸任务*/
	public function add_paper_task($paperTaskInfo){
		$user_info = get_user_info();//获取用户信息
		$rr = M('tpapersample')->where(array('fpapersampleid'=>$paperTaskInfo['fpapersampleid']))->save(array('finspectstate'=>2));//修改任务为已接收状态


		if($rr == 0) return 0;
		$a_taskInfo = array();
		$a_taskInfo['mediaclass'] = '报纸';//媒体类型
		$a_taskInfo['sample_table_name'] = 'tpapersample';//样本表的名字
		$a_taskInfo['sample_id'] = $paperTaskInfo['fpapersampleid'];//样本ID
		$a_taskInfo['task_state'] = 1;//任务状态
		$a_taskInfo['money'] = C('AUDIT_PRICE_N');//任务金额
		$a_taskInfo['receive_time'] = date('Y-m-d H:i:s');//接收任务时间
		$a_taskInfo['user_id'] = $user_info['wx_id'];//用户ID
		$task_id = M('ad_audit_task')->add($a_taskInfo);//添加任务表
		return $task_id;//返回任务ID
	}
	
	
	
	



}