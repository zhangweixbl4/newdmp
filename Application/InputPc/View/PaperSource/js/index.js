Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}
const vm = new Vue({
  el: "#app",
  data() {
    return {
      form: {
        fmediaid: '',
        fissuedate: ''
      },
      mediaList: [],
      tabelData: [],
      tableHeight: '400px',
      last_issuedate: '',
      pageList: [],
      pagetypeList: [],
      preview: false,
      loading: false,
      activeRow: {},
      upload: null,
      uploadOptions: null,
      cropper: null,
      ownMedia: [],
      upOptions: {
        up_server: ''
      },
      pickerOptions: {
        disabledDate(time) {
          return time.getTime() > Date.now();
        }
      },
    }
  },
  computed: {
    title() {
      let row = this.activeRow
      return `编号：${row.fpage || '未选择'} / 类别：${row.fpagetype || '未选择'} / 有无广告：${this.haveAd(row.noad)}`
    },
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight()
      document.querySelector('#app').style.opacity = 1
      this.cropper = new Cropper(document.getElementById('imageView'), {
        autoCropArea: 1,
        viewMode: 3,
        dragMode: 'move',
        modal: false,
        autoCrop: false,
        background: false
      })
    })
    window.onresize = () => {
      this.getTableHeight()
    }
  },
  created() {
    this.getToken()
    // 50分钟更新一次 token
    setInterval(() => {
      this.getToken()
    }, 3000000)
    this.getOwnMediaList()
  },
  methods: {
    getToken() {
      $.post('/InputPc/PaperSource/get_uploadtoken').then(res => {
        if (res.code === 0) {
          this.upload = res.data
          this.uploadOptions = res.data.val
          this.upOptions.up_server = res.data.up_server
        }
      })
    },
    setActiveRow(row, event, column) {
      this.setRow(row)
    },
    addRow() {
      let lastPage = [...this.tabelData].pop() ? [...this.tabelData].pop().fpage : ''
      let a = new RegExp(/^[a-zA-Z]{1,1}[0-9]*[0-9]$/)
      let b = new RegExp(/^[0-9]{0,1}[0-9]*[0-9]$/)
      if (a.test(lastPage)) {
        let head = lastPage.substr(lastPage, 1)
        let body = lastPage.substring(1)
        let bodyLength = body.length
        let num = Number(body)
        for (let i = 0; i < 4; i++){
          num += 1
          let result = num
          let length = bodyLength - num.toString().length
          for (let i = 1; i <= length; i++){
            result = `0${result}`
          }
          let _fpage = `${head}${result}`
          this.tabelData.push({
            fpage: _fpage,
            fpagetype: "",
            furl: "",
            noad: "",
            disabled: false,
            loading: false
          })
        }
      } else if (b.test(lastPage)) {
        let body = lastPage
        let bodyLength = body.length
        let num = Number(body)
        for (let i = 0; i < 4; i++){
          num += 1
          let result = num
          let length = bodyLength - num.toString().length
          for (let i = 1; i <= length; i++){
            result = `0${result}`
          }
          let _fpage = `${result}`
          this.tabelData.push({
            fpage: _fpage,
            fpagetype: "",
            furl: "",
            noad: "",
            disabled: false
          })
        }
      } else {
        for (let i = 0; i < 4; i++){
          this.tabelData.push({
            fpage: "",
            fpagetype: "",
            furl: "",
            noad: "",
            disabled: false,
            loading: false
          })
        }
      }
    },
    /**
     * 获取上次列表
     */
    getList() {
      this.loading = true
      $.post('/InputPc/PaperSource/get_papersourcelist', this.form)
        .then(res => {
          if (res.code === 0) {
            this.tabelData = res.data.map(i => {
              if (i.fcreatetime) {
                let now = +new Date()
                let time = +new Date(i.fcreatetime)
                i.disabled = (now - time) >= 20 * 60 * 1000
              } else {
                i.disabled = false
              }
              i.loading = false
              return i
            })
            if (this.tabelData.length) {
              this.setRow(this.tabelData[0])
            }
          }
          this.loading = false
      })
    },
    setRow(row) {
      this.activeRow = row
      if (row.furl === this.activeRow.furl) return false
      this.cropper.replace(this.activeRow.furl)
    },
    /**
     * 设置表格高度
     */
    getTableHeight(){
      let formHeight = this.$refs.form.$el.clientHeight
      this.tableHeight = document.body.clientHeight - formHeight - 110 + 'px'
    },
    /**
     * 如果有 query 时请求接口，没有时获取当前用户历史上传媒体列表
     * @param {string} query 关键字
     */
    handleMediaSearch(query) {
      if (query) {
        this.getMediaList(query)
      } else {
        this.mediaList = this.ownMedia
      }
    },
    getQuery() {
      let inputValue = this.$refs.mediaSelect.$el.getElementsByTagName('input')[0].value
      this.handleMediaSearch(inputValue)
    },
    /**
     * 获取媒体列表
     * @param {String} query 模糊搜索关键字
     */
    getMediaList(query) {
      $.get(`/index.php?m=Api&c=Media&a=get_media_list&fmediaclassid=03&media_name=${query || ''}`)
        .then(res => {
          if (res.code === 0) {
            this.mediaList = res.value
          }
        })
    },
    /**
     * 获取用户历史上传媒体列表
     */
    getOwnMediaList() {
      $.post('/InputPc/PaperSource/get_media_list')
        .then(res => {
          if (res.code === 0) {
            this.mediaList = res.value
            this.ownMedia = res.value
          }
        })
    },
    /**
     * 处理数组，将字符串数组转换为对象数组
     * @param {Array} arr 待处理数组
     */
    handleArr(arr = []) {
      const result = []
      const array = arr
      array.forEach(item => {
        const obj = {
          label: item,
          value: item,
        }
        result.push(obj)
      })
      return result
    },
    /**
     * 根据选择的媒体的id去获取该媒体的相关信息
     * @param {String} mediaid 媒体id
     */
    handleMediaSelect(mediaid) {
      if(this.form.fissuedate) this.getList()
      $.get(`/InputPc/PaperSource/paper_prompt?mediaId=${mediaid}`)
        .then(res => {
          this.last_issuedate = res.last_issuedate
          this.pageList = this.handleArr(res.pageList.length ? res.pageList : res.allpageList)
          this.pagetypeList = this.handleArr(res.pagetypeList.lenght ? res.pagetypeList : res.allpagetypeList)
          this.allpageList = this.handleArr(res.allpageList)
          this.allpagetypeList = this.handleArr(res.allpagetypeList)
        })
    },
    /**
     * 获取当前关键字是否能完全匹配上数组中某项的 value 值
     * @param {String} val 带匹配字符串
     * @param {Array} arr 源数组
     */
    getIndex(val, arr) {
      let index = -1
      arr.forEach((item, i) => {
        if(item.value === val) index = i
      })
      return index
    },
    /**
     * 关键字过滤
     * @param {String} query 关键字
     * @param {Function} cb 回调函数，返回筛选后的项
     * @param {String} type 代处理数组变量名
     */
    querySearch(query, cb, type) {
      if (query) {
        let result = this[`all${type}`].filter(i => (i.value.indexOf(query) !== -1))
        cb(result)
      } else {
        return cb(this[type])
      }
    },
    checkData() {
      let result = true
      // 筛选已经上传图片的项
      let uploadData = this.tabelData.filter(i => (i.furl && !i.disabled))
      if (uploadData.length <= 0) {
        this.$message.error('请上传报纸后重试')
        return {
          result: false
        }
      }
      // 判断上传图片的项中其他信息是否完整
      uploadData = uploadData.map(i => {
        delete i.loading
        delete i.disabled
        result = i.furl && i.fpage && i.noad
        return i
      })

      if (!result) {
        this.$message.error('请完善报纸信息后重试')
        return {
          result: false
        }
      }
      return {
        result: true,
        data: uploadData
      }
    },
    haveAd(noad) {
      if (noad === '0') {
        return '有广告'
      }
      if (noad === '1') {
        return '无广告'
      }
      return '未选择'
    },
    deleteFile(id) {
      $.post('/InputPc/PaperSource/del_papersource', { fid: id })
        .then(res => {
          if (res.code !== 0) this.$message.error(res.msg)
      })
    },
    deleteImage(row) {
      row.furl = ''
      if(row.fid) this.deleteFile(row.fid)
    },
    /**
     * 提交
     */
    submit() {
      let checkResult = this.checkData()
      if (!checkResult.result) return false
      let uploadData = checkResult.data
      $.post('/InputPc/PaperSource/add_papersource', { ...this.form, data: uploadData })
        .then(res => {
          if (res.code === 0) {
            this.$message.success(res.msg)
            this.getList()
          } else {
            this.$message.error(res.msg)
          }
      })
    },
    /**
     * 设置文件上传的文件名
     * @param {Object} file 所选的文件对象
     */
    setKey(file, row) {
      const extArr = file.name.split('.')
      const ext = extArr.length > 1 ? extArr.pop() : ''
      this.uploadOptions.key = ext ? `${file.uid}.${ext}` : file.uid
      row.loading = true
    },
    uploadSuccess(response, file, fileList, row) {
      row.furl = `${this.upload.bucket_url}${response.key}`
      this.setRow(row)
      row.loading = false
    },
    uploadError() {
      this.$message.error('上传失败')
    }
  },
})
