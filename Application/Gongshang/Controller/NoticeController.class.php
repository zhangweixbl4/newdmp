<?php
namespace Gongshang\Controller;
use Think\Controller;

class NoticeController extends Controller {
    public function index(){

        $this->display();
    }
    public function direction(){
        $Statelist=array(
            1=>array("scode"=>"1","stype"=>"待指导"),
            2=>array("scode"=>"2","stype"=>"已指导"),
            3=>array("scode"=>"3","stype"=>"未反馈"),
            4=>array("scode"=>"4","stype"=>"已反馈")
        );
        $this->assign('Statelist',$Statelist);//状态列表

        $directList=array(
          1=>array("num"=>"1","ad_id"=>"1","adName"=>"xx广告名称","mediaName"=>"XX媒体名称","directTimes"=>"","newSendTime"=>"","directType"=>"","newState"=>"","newSign"=>"","feedbackResult"=>"","feedbackTime"=>"")
        );
        $this->assign('directList',$directList);//数据列表
        $this->display();
    }
    public function zeling(){
        $Statelist=array(
            1=>array("scode"=>"1","stype"=>"待签发"),
            2=>array("scode"=>"2","stype"=>"已签发"),
            3=>array("scode"=>"2","stype"=>"已驳回")
        );
        $this->assign('Statelist',$Statelist);//状态列表
        $this->display();
    }
    public function zelingLog(){
        $Statelist=array(
            1=>array("scode"=>"1","stype"=>"待审批"),
            2=>array("scode"=>"2","stype"=>"已签发"),
            3=>array("scode"=>"3","stype"=>"已驳回")
        );
        $this->assign('Statelist',$Statelist);//状态列表

        $noticeList=array(
            1=>array("num"=>"1","notice_id"=>"1","adName"=>"xx广告名称","mediaName"=>"XX媒体名称","directType"=>"","directName"=>"","directPerson"=>"","directType"=>"","directTimes"=>"","state"=>""));
        $this->assign('noticeList',$noticeList);//数据列表

        $this->display();
    }

    public function moban(){
        $Typelist=array(
            1=>array("scode"=>"1","stype"=>"建议"),
            2=>array("scode"=>"2","stype"=>"告诫"),
            3=>array("scode"=>"2","stype"=>"改正"),
            4=>array("scode"=>"2","stype"=>"约谈"),
            5=>array("scode"=>"2","stype"=>"停播"),
        );
        $this->assign('Typelist',$Typelist);//状态列表

        $mobanList=array(
            1=>array("num"=>"1","moban_id"=>"1","mobanName"=>"xx名称","mobanType"=>"","writer"=>"","newdate"=>""));
        $this->assign('mobanList',$mobanList);//数据列表

        $this->display();
    }
    public function mediaFeedback(){
        $Statelist=array(
            1=>array("scode"=>"1","stype"=>"已反馈"),
            2=>array("scode"=>"2","stype"=>"未反馈")
        );
        $this->assign('Statelist',$Statelist);//状态列表
        $this->display();
    }
}