<?php
namespace Agp\Controller;
use Think\Controller;


/**
 * 报告任务
 */

class FreportTaskController extends BaseController
{
	//报告列表
	public function index()
	{
		$fregulatorpid = session('regulatorpersonInfo.fregulatorpid');//机构ID
		$title = I('title');
		$status = I('status');
		$ctime = I('ctime');
		$p 			= I('page', 1);//当前第几页
		$pp 		= 20;//每页显示多少记录

		if($title){
			$where['title'] = array('like','%'.$title.'%');
		}
		if(!empty($status)){
			$where['status'] = $status;
		}else{
			$where['status'] = array('neq',0);
		}
		if($ctime){
			$where['ctime'] = array('BETWEEN',array($ctime[0],$ctime[1]));
		}
		$where['fregulatorpid'] = $fregulatorpid;

		$count = M('f_report_task')->where($where)->count();

		$Page 	= new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数

		$res = M('f_report_task')->where($where)->page($p,$pp)->order('fid desc')->select();

		$this->ajaxReturn(array('code'=>0,'msg'=>'数据获取成功','data'=>array('count'=>$count,'list'=>$res)));//返回ajax

	}

	//添加报告
	public function addReport()
	{
		$fregulatorpid = session('regulatorpersonInfo.fregulatorpid');//机构ID

		$title 	= I('title');
		$content = I('content');
		$type 	= I('type');
		$dtime 	= I('dtime');
		$ffiles = I('ffiles');

		$data['title'] 		= $title;
		$data['content'] 	= $content;
		$data['type'] 		= $type;
		$data['fregulatorpid'] = $fregulatorpid;
		$data['ctime'] 		= date('Y-m-d H:i:s');
		$data['status'] 	= 1;
		$data['dtime'] 		= $dtime;
		$data['fileurl'] 	= $ffiles;
		$res = M('f_report_task')->add($data);

		if($res){
			D('Function')->write_log('报告任务',1,'添加成功','f_report_task',$res);
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));//返回ajax
		}else{
			D('Function')->write_log('报告任务',0,'添加失败','f_report_task',0,M('f_report_task')->getlastsql());
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败'));//返回ajax
		}

	}

	/**
	 * 删除任务
	 */
	public function delReport(){
		$fid = I('fid');
		$fregulatorpid = session('regulatorpersonInfo.fregulatorpid');//机构ID

		$where_rtk['fid'] = $fid;
		$where_rtk['fregulatorpid'] = $fregulatorpid;
		$data_rtk['status'] = 0;
		$do_ftk = M('f_report_task')->where($where_rtk)->save($data_rtk);
		if(!empty($do_ftk)){
			D('Function')->write_log('报告任务',1,'删除成功','f_report_task',$fid);
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
		}else{
			D('Function')->write_log('报告任务',0,'删除失败','f_report_task',0,M('f_report_task')->getlastsql());
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败'));
		}
	}

}