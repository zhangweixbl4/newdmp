<?php
namespace Open\Controller;
use Think\Controller;

class TvissueController extends BaseController {
	


	public function add_edit_tvissue(){
		$tvissue_data = file_get_contents('php://input');//获取post数据
		$tvissue_data = json_decode($tvissue_data,true);//把json转化成数组
		//var_dump($tvissue_data);
		$id = $tvissue_data['id'];//广告发布ID
		
		$a_e_data = array();
		
		$a_e_data['ftvsampleid'] = $tvissue_data['ftvsampleid'];//样本ID
		$tvsample_count = M('ttvsample')->where(array('fid'=>$a_e_data['ftvsampleid']))->count();//查询电视样本
		if($tvsample_count == 0) $this->ajaxReturn(array('code'=>10016,'msg'=>'添加或修改数据失败,样本ID有错误'));
		
		$a_e_data['ftvmodelid'] = intval($tvissue_data['ftvmodelid']);//模型ID

		$a_e_data['fmediaid'] = $tvissue_data['fmediaid'];//媒体ID
		$media_count = M('tmedia')->where(array('fid'=>$a_e_data['fmediaid']))->count();
		if($media_count == 0) $this->ajaxReturn(array('code'=>10016,'msg'=>'添加或修改数据失败,媒体ID有错误'));
		
		$a_e_data['fissuedate'] = date("Y-m-d H:i:s",strtotime($tvissue_data['fissuedate']));//发布日期
		
		
		
		$a_e_data['fstarttime'] = date("Y-m-d H:i:s",strtotime($tvissue_data['fstarttime']));//发布开始时间
		$a_e_data['fendtime'] = date("Y-m-d H:i:s",strtotime($tvissue_data['fendtime']));//发布结束时间
		$a_e_data['flength'] = intval($tvissue_data['flength']);//时长
		$a_e_data['fissuetype'] = $tvissue_data['fissuetype'];//发布类型
		if($a_e_data['fissuetype'] == '') $this->ajaxReturn(array('code'=>10015,'msg'=>'添加或修改数据失败,发布类型不能为空'));
		
		$a_e_data['famounts'] = intval($tvissue_data['famounts']);//金额
		$a_e_data['fquantity'] = intval($tvissue_data['fquantity']);//数量
		if($a_e_data['fquantity'] == 0) $a_e_data['fquantity'] == 1;//如果数量为0，则默认为1
		
		$a_e_data['fissample'] = intval($tvissue_data['fissample']);//是否样本
		$a_e_data['faudiosimilar'] = intval($tvissue_data['faudiosimilar']);//音频相似度
		$a_e_data['fvideosimilar'] = intval($tvissue_data['fvideosimilar']);//视频相似度
		
		$a_e_data['fadsort'] = $tvissue_data['fadsort'];//广告段排序，非必填
		$a_e_data['fpriorad'] = $tvissue_data['fpriorad'];//前一广告，非必填
		$a_e_data['fnextad'] = $tvissue_data['fnextad'];//后一广告，非必填
		$a_e_data['fpriorprg'] = $tvissue_data['fpriorprg'];//前一节目，非必填
		$a_e_data['fnextprg'] = $tvissue_data['fnextprg'];//后一节目，非必填
		
		$a_e_data['fstate'] = $tvissue_data['fstate'];//状态
		$person_info = $GLOBALS['person_info'];
		
		if($id == '' ){
			$a_e_data['fcreator'] = $person_info['fname'];//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = $person_info['fname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			$rr = M('ttvissue')->add($a_e_data);
			if($rr > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'添加广告发布成功','id'=>$rr));
			if($rr == 0) $this->ajaxReturn(array('code'=>10011,'msg'=>'添加广告发布失败,原因未知'));
		}else{
			$a_e_data['fmodifier'] = $person_info['fname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('ttvissue')->where(array('id'=>$id))->save($a_e_data);
			if($rr > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'修改广告发布成功'));
			if($rr == 0) $this->ajaxReturn(array('code'=>10012,'msg'=>'修改广告发布失败,原因未知'));
		}
		
		
	
	}
	
	/*获取电视发布记录*/
	public function get_tvissue_list(){
		
		$tvissue_data = file_get_contents('php://input');//获取post数据
		$tvissue_data = json_decode($tvissue_data,true);//把json转化成数组
		$ftvsampleid = intval($tvissue_data['ftvsampleid']);//电视广告样本ID
		$p = intval($tvissue_data['p']);//需要返回的页数
		if($p == 0) $p = 1;
		$pp = intval($tvissue_data['pp']);//需要返回的条数
		if($pp <= 0) $pp = 10;
		if($pp > 100) $pp = 100;
		
		if($ftvsampleid == '')  $this->ajaxReturn(array('code'=>10025,'msg'=>'查询失败,缺少样本ID'));
		
		$fcreatetime_s = $tvissue_data['fcreatetime_s'];//发布日期 开始
		if($fcreatetime_s == '') $fcreatetime_s = '2015-01-01';
		$fcreatetime_e = $tvissue_data['fcreatetime_e'];//发布日期 结束
		if($fcreatetime_e == '') $fcreatetime_e = date('Y-m-d H:i:s');
		
		$where = array();
		$where['ftvsampleid'] = $ftvsampleid;
		$where['fcreatetime'] = array('between',$fcreatetime_s.','.$fcreatetime_e);
		
		$count = M('ttvissue')->where($where)->count();
		
		$tvissueList = M('ttvissue')->where($where)->page($p,$pp)->select();

		$this->ajaxReturn(array('code'=>0,'msg'=>'','tvissueList'=>$tvissueList,'count'=>$count));


		
	}	
	
}