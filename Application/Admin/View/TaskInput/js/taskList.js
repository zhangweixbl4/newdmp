Vue.prototype.$ELEMENT = { size: "mini", zIndex: 3000 };
var viewerInstance = {
  destroy: function() {}
};
var vm = new Vue({
  el: "#app",
  data: function() {
    return {
      tableHeight: "400px",
      mediaClassArr: [
        { text: "电视", value: 1 },
        { text: "广播", value: 2 },
        { text: "报刊", value: 3 }
        // {text: '互联网', value: 13},
      ],
      netPlatform: [
        { text: "PC", value: 1 },
        { text: "移动端", value: 2 },
        { text: "微信端", value: 9 }
      ],
      taskStateArr: [
        { text: "初始", value: 0 },
        { text: "任务中", value: 1 },
        { text: "已完成", value: 2 },
        { text: "被退回", value: 4 },
        { text: "等待撤回审核", value: 5 }
      ],
      qualityStateArr: [
        { text: "未质检", value: 0 },
        { text: "正在质检", value: 1 },
        { text: "已质检", value: 2 }
      ],
      adClassArr: adClassArr,
      adClassTree: adClassTree,
      illegalTypeArr: illegalTypeArr,
      regionTree: regionTree,
      loading: true,
      where: {},
      page: 1,
      tableData: [],
      total: 0,
      taskInfo: {},
      showTaskInfo: false,
      role: role,
      searchTempWhere: {},
      type: type,
      isNetAd: "0",
      selectedRow: [],
      pageSize: 20,
      groupArr: [],
      lowPriority: "0",
      highPriority: "10000000",
      taskGroup: "",
      taskGroupDialog: false,
      pramaData: {},
      groupType: ""
    };
  },
  components: {
    taskInfo,
    taskInfo2,
    netAdTask,
    netAdTask2
  },
  created: function() {
    this.getLastDate();
    this.sendSearchReq();
    this.getGrop();
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight();
      document.querySelector("#app").style.opacity = 1;
    });
    window.onresize = () => {
      this.getTableHeight();
    };
  },
  methods: {
    getLastDate: function(val) {
      const arr = [];
      let startTime = moment().format("YYYY-MM-") + "01";
      let endTime = moment().format("YYYY-MM-") + moment().daysInMonth();
      let today = moment().format("YYYY-MM-DD");
      let tomorrow = moment()
        .add(1, "days")
        .format("YYYY-MM-DD");
      arr.push(startTime);
      arr.push(endTime);
      if (val) {
        Vue.set(this.where, "timeRangeArrCreate", [today, tomorrow]);
      } else {
        Vue.set(this.where, "timeRangeArr", arr);
        Vue.set(this.where, "timeRangeArr2", arr);
        Vue.set(this.where, "timeRangeArrCreate", arr);
      }
    },
    search: function() {
      this.page = 1;
      this.where.onlyThisLevel = this.searchTempWhere.onlyThisLevel ? 1 : 0;
      this.where.cutError = this.searchTempWhere.cutError ? 1 : 0;
      if (
        this.searchTempWhere.searchAdClassArr1 &&
        this.searchTempWhere.searchAdClassArr1.length !== 0
      ) {
        this.where.adClass = this.searchTempWhere.searchAdClassArr1[
          this.searchTempWhere.searchAdClassArr1.length - 1
        ];
      } else {
        this.where.adClass = "";
      }
      if (
        this.searchTempWhere.searchAdClassArr2 &&
        this.searchTempWhere.searchAdClassArr2.length !== 0
      ) {
        this.where.adClass2 = this.searchTempWhere.searchAdClassArr2[
          this.searchTempWhere.searchAdClassArr2.length - 1
        ];
      } else {
        this.where.adClass2 = "";
      }
      if (
        this.searchTempWhere.region &&
        this.searchTempWhere.region.length !== 0
      ) {
        this.where.regionId = this.searchTempWhere.region[
          this.searchTempWhere.region.length - 1
        ];
      } else {
        this.where.regionId = "";
      }

      this.sendSearchReq();
    },
    sendSearchReq: function() {
      this.loading = true;
      if (this.lowPriority != "" && this.highPriority != "") {
        this.where.fpriority = [this.lowPriority, this.highPriority];
      } else {
        this.where.fpriority = [];
      }
      if (!this.highPriority && this.lowPriority) {
        const strr = "EGT";
        const str2 = strr.replace(/\s*/g, "");
        this.where.fpriority = [str2, this.lowPriority];
      }
      if (!this.lowPriority && this.highPriority) {
        const str = "ELT";
        const str1 = str.replace(/\s*/g, "");
        this.where.fpriority = [str1, this.highPriority];
      }
      var vm = this;
      const data = {
        where: this.where,
        page: this.page,
        pageSize: this.pageSize,
        isNetAd: this.isNetAd
      };
      if(this.isNetAd == 0) delete data.major_key
      this.pramaData = data;
      $.post("", data, function(res) {
        if (res.code == -2 && res.msg == "need log in again") {
          vm.$message.error("登录状态过期,请再次登录");
        }
        vm.tableData = res.data;
        vm.total = +res.total;
        vm.loading = false;
      });
    },
    resetForm: function(formName) {
      if (this.isNetAd == "1") {
        (this.where = {
          task_state: 0
        }),
          (this.lowPriority = "1");
        this.highPriority = "1000000";
        this.getLastDate(true);
      } else {
        (this.where = {}), (this.lowPriority = "0");
        this.highPriority = "10000000";
        this.getLastDate();
      }
      this.page = 1;
      this.sendSearchReq();
    },
    pageChange: function(page) {
      this.page = page;
      this.sendSearchReq();
    },
    openTaskInfo: function(row) {
      this.loading = true;
      $.post("", {
        taskid: row.taskid,
        taskType: row.media_class != 13 ? 1 : 2
      }).then(res => {
        this.loading = false;
        if (res.code == 0) {
          this.taskInfo = res.data;
          this.showTaskInfo = true;
          this.$nextTick(() => {
            this.$refs.task.dataInit();
          });
        }
      });
    },
    tableRowClassName: function(row) {
      if (row.row.task_state == 4) {
        return "errorTask";
      }
      if (row.row.fillegaltypecode != 0) {
        return "illegalTask";
      }
    },
    closeTaskInfo: function(reload) {
      this.showTaskInfo = false;
      this.taskInfo = {};
      if (reload) this.sendSearchReq();
    },
    searchAlias: function(queryString, cb) {
      $.post("/Api/User/searchUserByAlias", { keyword: queryString }, function(
        res
      ) {
        cb(res.data);
      });
    },
    searchMediaLabel: function(queryString, cb) {
      $.post("/Api/Label/searchMediaLabel", { keyword: queryString }, function(
        res
      ) {
        cb(res);
      });
    },
    baseFilter: function(value, arr) {
      var res = "";
      this[arr].forEach(function(item) {
        if (item.value == value) {
          res = item.text;
        }
      });
      return res;
    },
    selectRow: function(val) {
      this.selectedRow = val;
    },
    pageSizeChange: function(val) {
      this.pageSize = val;
      this.search();
    },
    unlockTask: function() {
      if (this.selectedRow.length === 0) {
        return this.$alert("请至少选择一行");
      } else {
        this.$confirm("确认解锁这" + this.selectedRow.length + "项任务?").then(
          () => {
            this.loading = true;
            const ids = this.selectedRow.map(v => v.taskid).join(",");
            $.post("unlockTask", {
              ids,
              adType: this.isNetAd
            }).then(res => {
              this.loading = false;
              if (res.code == 0) {
                this.$message.success(res.msg);
              } else {
                this.$message.error(res.msg);
              }
              this.sendSearchReq();
            });
          }
        );
      }
    },
    deleteTask: function() {
      if (this.selectedRow.length === 0) {
        return this.$alert("请至少选择一行");
      } else {
        this.$confirm("确认删除这" + this.selectedRow.length + "项任务?").then(
          () => {
            this.loading = true;
            const ids = this.selectedRow.map(v => v.taskid).join(",");
            $.post("deleteTask", {
              ids,
              adType: this.isNetAd
            }).then(res => {
              this.loading = false;
              if (res.code == 0) {
                this.$message.success(res.msg);
              } else {
                this.$message.error(res.msg);
              }
              this.sendSearchReq();
            });
          }
        );
      }
    },
    priorityCommand(val) {
      this.changePriority(val);
    },
    groupCommand(val) {
      this.changeGroup(val);
    },
    changePriority(selectType) {
      let data = {};
      if (selectType === "0" && this.selectedRow.length === 0) {
        return this.$alert("请至少选择一行");
      } else {
        this.$prompt("请输入设定的优先级", "提示", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          inputPattern: /\d/,
          inputErrorMessage: "请输入设定的优先级(数字)"
        }).then(val => {
          this.loading = true;
          const ids = this.selectedRow.map(v => v.taskid).join(",");
          if (selectType === "0") {
            data = {
              ids,
              adType: this.isNetAd,
              priority: val.value,
              isAll: 1
            };
          } else {
            data = {
              ...this.pramaData,
              isAll: 2,
              priority: val.value
            };
          }
          $.post("changePriority", data)
            .then(res => {
              if (res.code == 0) {
                this.$message.success(res.msg);
              } else {
                this.$message.error(res.msg);
                this.loading = false;
              }
              this.loading = false;
              this.sendSearchReq();
            })
            .catch(err => {
              this.loading = false;
              this.$message.error("修改失败，发生错误。");
            });
        });
      }

      // if (this.selectedRow.length === 0){
      //     return this.$alert('请至少选择一行')
      // } else {
      //     this.$prompt('请输入设定的优先级', '提示', {
      //         confirmButtonText: '确定',
      //         cancelButtonText: '取消',
      //         inputPattern: /\d/,
      //         inputErrorMessage: '请输入设定的优先级(数字)'
      //     }).then((val) => {
      //         this.loading = true
      //         const ids = this.selectedRow.map((v) => v.taskid).join(',')
      //         $.post('changePriority', {
      //             ids,
      //             adType: this.isNetAd,
      //             priority: val.value
      //         }).then((res) => {
      //             this.loading = false
      //             if (res.code == 0){
      //                 this.$message.success(res.msg)
      //             } else {
      //                 this.$message.error(res.msg)
      //             }
      //             this.sendSearchReq()
      //         })
      //     })
      // }
    },
    changeGroup(val) {
      this.groupType = val;
      if (val === "0" && this.selectedRow.length === 0) {
        return this.$alert("请至少选择一行");
      } else {
        this.taskGroupDialog = true;
      }
    },
    submitGroup() {
      let data = {};
      if (this.groupType === "0") {
        const ids = this.selectedRow.map(v => v.taskid);
        data = {
          ids,
          group_id: this.taskGroup,
          adType: this.isNetAd,
          isAll: 1
        };
      } else {
        data = this.pramaData;
        data.group_id = this.taskGroup;
        data.isAll = 2;
      }
      $.post("/Admin/TaskInput/setGroupId", JSON.stringify(data))
        .then(res => {
          if (res.code == 0) {
            this.handleClose();
            this.$message.success(res.msg);
          } else {
            this.handleClose();
            this.$message.error(res.msg);
          }
          this.sendSearchReq();
        })
        .catch(err => {
          this.handleClose();
          this.$message.error("修改失败，发生错误。");
        });
    },
    checkTaskFlow: function(row) {
      layer.open({
        title: "任务流程",
        type: 2,
        area: ["60%", "60%"],
        content:
          "checkTaskFlow?taskid=" + row.taskid + "&adType=" + this.isNetAd
      });
    },
    //获取分组
    getGrop() {
      $.post("/Admin/Tag/getList").then(res => {
        if (res.code == 0) {
          this.groupArr = res.data;
        }
      });
    },
    handleClose() {
      this.taskGroupDialog = false;
    },
    groupFilter(value) {
      if (!value) {
        return "";
      } else {
        let arr = this.groupArr.filter(i => {
          return i.group_id == value;
        });
        return arr.length > 0 ? arr[0].group_name : "";
      }
    },
    getTableHeight() {
      this.tableHeight = `${document.body.clientHeight -
        this.$refs.search.$el.clientHeight -
        100}px`;
    }
  },
  computed: {
    selectNumber() {
      if (this.selectedRow.length === 0) {
        return "";
      } else {
        return this.selectedRow.length;
      }
    }
  },
  filters: {
    mediaClassFilter: function(value) {
      try {
        return vm.baseFilter(value, "mediaClassArr");
      } catch (e) {}
    },
    illegalTypeFilter: function(value) {
      try {
        return vm.baseFilter(value, "illegalTypeArr");
      } catch (e) {}
    },
    taskStateFilter: function(value) {
      try {
        return vm.baseFilter(value, "taskStateArr");
      } catch (e) {}
    },
    qualityStateFilter: function(value) {
      try {
        return vm.baseFilter(value, "qualityStateArr");
      } catch (e) {}
    },
    netPlatformFilter: function(value) {
      try {
        return vm.baseFilter(value, "netPlatform");
      } catch (e) {}
    }
  }
});
