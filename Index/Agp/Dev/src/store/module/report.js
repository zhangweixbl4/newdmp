const pntype = [{
  label: '日报',
  value: '10',
  img: './static/img/day.jpg'
}, {
  label: '周报',
  value: '20',
  img: './static/img/week.jpg'
}, {
  label: '半月报',
  value: '25',
  img: './static/img/month.jpg'
},{
  label: '月报',
  value: '30',
  img: './static/img/month.jpg'
}, {
  label: '互联网报告',
  value: '40',
  img: './static/img/net.jpg'
}, {
  label: '专项报告',
  value: '50',
  img: './static/img/month.jpg'
}]

const pntype2 = {
  '10': '日报',
  '20': '周报',
  '25': '半月报',
  '30': '月报',
  '50': '专项报告',
  '60': '复核报告',
  '70': '季报',
  '75': '半年报',
  '80': '年报',
  '21': '预警报告',
  '40': '互联网报告',
  '61': '跟踪报告',
  '90': '汇总报告',
  '81': '自定义'
}
const pnfiletype = [{
  label: 'WORD',
  value: '10',
  img: './static/img/word.jpg'
}, {
  label: 'EXCEL',
  value: '30',
  img: './static/img/excel.jpg'
}]

const pnfiletype2 = {
  '10': {
    label: 'WORD',
    img: './static/img/word.jpg'
  },
  '30': {
    label: 'EXCEL',
    img: './static/img/excel.jpg'
  }
}

export {
  pntype,
  pntype2,
  pnfiletype,
  pnfiletype2
}
