<?php
//管理机构管理控制器

/*
SELECT concat('20',fid),concat('20',fpid),'20',concat(fname,'局'),concat('20',fid),concat(fname,'局'),fid
from tregion

where fid not in(990000,100000) and fpid <> 100000 and (select count(*) from tregulator where fregionid = tregion.fid) = 0;



INSERT INTO `hzsj_dmp`.`tregulator` 
			(`fid`, `fpid`, `ftype`, `fname`, `fcode`, `ffullname`,`fregionid`) 
				VALUES 
('20130209','20130200','20','曹妃甸区局','20130209','曹妃甸区局','130209')

			



*/

namespace Admin\Controller;
use Think\Controller;
class RegulatorController extends BaseController {

    public function index(){
		
		if(A('Admin/Authority','Model')->authority('200171') === 0){
			exit('没有权限');
		}
		$regionList = A('Api/Region')->get_region_treelist();
		$this->assign('regionList',$regionList);

		$regulatorList = M('tregulator')->where(array('fpid'=>0))->select();
		$this->assign('regulatorList',$regulatorList);
		$mediaclassList = M('tmediaclass')->cache(true,600)->field('fid,fclass')->where(array('fpid'=>''))->select();
		$this->assign('mediaclassList',$mediaclassList);//媒介类型列表	
		$this->display();
	}
	
	/*添加、修改组织机构*/
	public function add_edit_regulator(){
		$fid= I('fid');//机构ID
		$fid = intval($fid);//转为数字
		$a_e_data = array();
		$a_e_data['fname'] = I('fname');//机构名称
		
		$a_e_data['ffullname'] = I('ffullname');//机构全称
		$a_e_data['fdescribe'] = I('fdescribe');//机构说明
		$a_e_data['fregionid'] = I('fregionid');//行政区划
		$a_e_data['fpid'] = intval(I('fpid',0));//上级ID
		$a_e_data['fcode'] = I('fcode');//机构编码
		$a_e_data['special_brand'] = str_replace(array('，'),array(','),I('special_brand'));//个性化品牌
		$a_e_data['special_home_page'] = I('special_home_page');//
		$a_e_data['special_logo'] = I('special_logo');// 
		$a_e_data['special_web_title'] = I('special_web_title');//
		
		
		
		
		$a_e_data['fkind'] = I('fkind');//机构类型(0-根节点，1-机构，2-部门）
		$a_e_data['fstate'] = I('fstate',1);//状态，-1删除，0-无效，1-有效
		
		if($fid == 0){//判断是修改还是新增
			if(A('Admin/Authority','Model')->authority('200174') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));
			}
			$is_repeat = M('tregulator')->where(array('fcode'=>$a_e_data['fcode']))->count();//查询机构代码是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'机构代码重复'));//返回机构代码重复
			
			$a_e_data['fcreator'] = session('personInfo.fid').'_'.session('personInfo.fname');//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			$rr = M('tregulator')->add($a_e_data);//添加数据
			
			if($rr > 0){//判断是否添加成功
				M('tregulator')->where(array('fid'=>$rr))->save(array('fid'=>$a_e_data['fcode']));//修改数据
				$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));//返回成功
				
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));//返回失败
			}
			
		}else{//修改
			if(A('Admin/Authority','Model')->authority('200175') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));
			}
			$is_repeat = M('tregulator')->where(array('fid'=>array('neq',$fid),'fcode'=>$a_e_data['fcode']))->count();//查询机构代码是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'机构代码重复'));//返回机构代码重复
			
			$a_e_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tregulator')->where(array('fid'=>$fid))->save($a_e_data);//修改数据
			//var_dump(M('tregulator')->getLastSql());
			if($rr > 0){//判断是否修改成功
				M('tregulator')->where(array('fid'=>$fid))->save(array('fid'=>$a_e_data['fcode']));//修改数据
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));//返回失败
			}
			
		}
		
		
		
	}
	
	/*添加机构关联的媒介*/
	public function add_correlation_media(){
		session_write_close();
		$fregulatorcode = I('fregulatorcode');//监管机构ID
		$fmediaid = I('fmediaid');//媒介ID
		$fcustomer = I('fcustomer');
		$alldata = [];//待添加的所有媒体权限数据

		if(empty($fmediaid)){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有选择媒体'));
		}
		$do_region = M('tregulator')
			->alias('a')
			->field('a.fname,a.fid,b.flevel')
			->join('tregion b on b.fid = a.fregionid and a.fstate = 1 and a.fstate = 1 and a.fkind = 1 and a.ftype = 20')
			->where(['b.fid'=>$fcustomer])
			->find();//客户信息

		$do_media = M('tmedia')
			->alias('c')
			->field('c.fid mid')
			// ->join('tmedia c on c.media_region_id = a.fregionid and a.fstate = 1 and a.fkind = 1 and a.ftype = 20')
			->where(['c.fid'=>['in',$fmediaid],'c.fstate'=>1])
			->select();

		$do_trview = M('tregulator')
			->alias('a')
			->field('a.fname,a.fid,a.fkind,b.flevel')
			->join('tregion b on b.fid = a.fregionid and a.fstate = 1 and a.fstate = 1 and a.fkind = 1 and a.ftype = 20')
			->where(['a.fid'=>$fregulatorcode])
			->find();//被设置机构级别

		foreach ($do_media as $value) {
			$a_data['fregulatorcode'] = $fregulatorcode;//监管机构code
			$a_data['fmediaid'] = $value['mid'];//媒介ID
			$a_data['fcreator'] = session('personInfo.fname');//创建人
			$a_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_data['fmodifier'] = session('personInfo.fname');//修改人
			$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$a_data['fcustomer'] = $fcustomer;//
			$a_data['fstate'] = 1;//状态为1有效
			$alldata[] = $a_data;

			// if($fcustomer == 100000){
			// 	$addarr3[] = array(
			// 		'freg_id'			=>$value['fid'],
			// 		'freg'				=>$value['fname'],
			// 		'fmedia_id'			=>$value['mid'],
			// 		'fgrant_reg_id'		=>$do_region['fid'],
			// 		'fgrant_reg'		=>$do_region['fname'],
			// 		'fgrant_user'		=>'ADMIN',
			// 		'fgrant_time'		=>date('Y-m-d H:i:s'),
			// 		'fcustomer'			=>$fcustomer,
			// 	);
			// }else{
			// 	$addarr3[] = array(
			// 		'freg_id'			=>$fregulatorcode,
			// 		'freg'				=>$do_trview['fname'],
			// 		'fmedia_id'			=>$value['mid'],
			// 		'fgrant_reg_id'		=>$do_region['fid'],
			// 		'fgrant_reg'		=>$do_region['fname'],
			// 		'fgrant_user'		=>'ADMIN',
			// 		'fgrant_time'		=>date('Y-m-d H:i:s'),
			// 		'fcustomer'			=>$fcustomer,
			// 	);
			// }
			$addarr3[] = $value['mid'];
			
		}

		// if(($fcustomer != 100000 && $do_trview['fkind'] == 1 && $do_region['flevel'] != $do_trview['flevel'] || $fcustomer == 100000) && !empty($addarr3)){
		// 	M('tbn_media_grant')->addAll($addarr3);
		// }

		$rr = M('tregulatormedia')->addAll($alldata);
		if($rr > 0){//判断是否添加成功
			if($do_trview['fkind'] == 1){
				foreach ($addarr3 as $key => $value) {
					$dd = A('Api/IllegalAdIssue')->grantlog($fcustomer,$value);
				}
			}
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));//返回成功
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知','alldata'=>$alldata,'do_media'=>$do_media));//返回失败
		}
	}
	/*删除机构关联的媒介*/
	public function del_correlation_media(){
		session_write_close();
		if(A('Admin/Authority','Model')->authority('200173') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));
		}
		$fregulatorcode = I('fregulatorcode');//监管机构code
		$fmediaid = I('fmediaid');//媒介ID
		$fcustomer = I('fcustomer');
		if(empty($fmediaid)){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有选择媒体'));
		}

		$do_region = M('tregulator')
			->alias('a')
			->field('a.fname,a.fid,b.flevel')
			->join('tregion b on b.fid = a.fregionid and a.fstate = 1 and a.fkind = 1 and a.ftype = 20')
			->where(['b.fid'=>$fcustomer])
			->find();//客户级别

		//删除交办的媒体
		if($fcustomer == 100000){
			M('tbn_media_grant')->where(['fgrant_reg_id'=>$do_region['fid'],'fcustomer'=>$fcustomer,'fmedia_id'=>['in',$fmediaid]])->delete();
		}else{
			M('tbn_media_grant')->where(['freg_id'=>$fregulatorcode,'fgrant_reg_id'=>$do_region['fid'],'fcustomer'=>$fcustomer,'fmedia_id'=>['in',$fmediaid]])->delete();
		}

		$rr = M('tregulatormedia')->where(array('fcustomer'=>$fcustomer,'fregulatorcode'=>$fregulatorcode,'fmediaid'=>array('in',$fmediaid)))->delete();
		
		if($rr > 0){//判断是否修改成功
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));//返回成功
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败'));//返回失败
		}
	}
	
	/*更新机构关联的媒介状态*/
	public function update_correlation_media_state(){
		session_write_close();
		$fregulatorcode = I('fregulatorcode');//监管机构code
		$fmediaid = I('fmediaid');//媒介ID
		$fcustomer = I('fcustomer');
		$fstate = I('fstate');
		if(empty($fmediaid)){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有选择媒体'));
		}
		$a_data = array();

		$a_data['fmodifier'] = session('personInfo.fname');//修改人
		$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$a_data['fstate'] = $fstate;//状态为-1删除
		
		$rr = M('tregulatormedia')->where(array('fcustomer'=>$fcustomer,'fregulatorcode'=>$fregulatorcode,'fmediaid'=>array('in',$fmediaid)))->save($a_data);
		
		if($rr > 0){//判断是否修改成功
			$this->ajaxReturn(array('code'=>0,'msg'=>'保存成功'));//返回成功
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'保存失败'));//返回失败
		}
	}

	/*更新机构关联的媒介是否外部媒体*/
	public function update_correlation_isoutmedia(){
		session_write_close();
		$fregulatorcode = I('fregulatorcode');//监管机构code
		$fmediaid = I('fmediaid');//媒介ID
		$fcustomer = I('fcustomer');
		$fisoutmedia = I('fisoutmedia');
		if(empty($fmediaid)){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有选择媒体'));
		}
		$a_data = array();

		$a_data['fmodifier'] = session('personInfo.fname');//修改人
		$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$a_data['fisoutmedia'] = $fisoutmedia;
		
		$rr = M('tregulatormedia')->where(array('fcustomer'=>$fcustomer,'fregulatorcode'=>$fregulatorcode,'fmediaid'=>array('in',$fmediaid)))->save($a_data);
		
		if($rr > 0){//判断是否修改成功
			$this->ajaxReturn(array('code'=>0,'msg'=>'保存成功'));//返回成功
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'保存失败'));//返回失败
		}
	}
	
	/*获取关联媒介列表*/
	public function get_correlation_media_list(){
		if(A('Admin/Authority','Model')->authority('200172') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));
		}
		$fregulatorcode = I('fregulatorcode');//监管机构code
		$fcustomer = I('fcustomer');
		$regulatormediaList = M('tregulatormedia')
												->field('tregulatormedia.*,tmedia.fmedianame,tregion.fname regionname,tmediaclass.fclass')
												->join('tmedia on tmedia.fid = tregulatormedia.fmediaid and tmedia.fstate = 1')
												->join('tregion on tmedia.media_region_id = tregion.fid')
												->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')
												->where(array('fcustomer'=>$fcustomer,'tregulatormedia.fstate'=>array('in',array(0,1)),'fregulatorcode'=>$fregulatorcode))
												->order('tregulatormedia.fstate desc,left(tmedia.fmediaclassid,2) asc,tregion.fid asc')
												->select();//关联列表
		$this->ajaxReturn(array('code'=>0,'msg'=>'','regulatormediaList'=>$regulatormediaList));
	}
	
	/*异步加载机构列表*/
	public function ajax_regulator_list(){
		
		$fid = I('fid');//获取机构ID
		$regulatorList = M('tregulator')->where(array('fpid'=>$fid))->order('fkind asc,fid asc')->select();
		if(!$regulatorList){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'无数据'));
		}
		$this->assign('regulatorList',$regulatorList);
		
		$this->display();
	}
	
	/*机构详情*/
	public function ajax_regulator_details(){
		
		$fid = I('fid');//获取机构ID
		
		$regulatorDetails = M('tregulator')->field('tregulator.*,tregion.ffullname as region_fullname')->join('tregion on tregion.fid = tregulator.fregionid')->where(array('tregulator.fid'=>$fid))->find();
		$this->ajaxReturn(array('code'=>0,'regulatorDetails'=>$regulatorDetails));
	}
	
	
	/*修正机构的全称*/
	public function adjust_full_name(){
		set_time_limit(0);
		$mList = M('tregulator')->where(array('fid'=>array('gt',0)))->select();//查询所有机构
		foreach($mList as $m){
			$ffullname = $this->adjust_full_name_2($m['fid'],$m['fname']);//计算全名，使用fid和fname
			M('tregulator')->where(array('fid'=>$m['fid']))->save(array('ffullname'=>$ffullname));
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>''));
	}
	/*修改机构的全称，计算全称*/
	private function adjust_full_name_2($fid = '',$fname = '',$num = 0){
		$v = M('tregulator')->where(array('fid'=>$fid))->find();//查询机构信息
		if ($v['fpid'] == 0 || $num > 20){
			return $fname;//如果上级ID等于0或者查询次数大于20次，直接返回
		}	
		if ($v['fpid'] != 0){
			$c_fname = M('tregulator')->where(array('fid'=>$v['fpid']))->getField('fname');
			return $this->adjust_full_name_2($v['fpid'],$c_fname . '>' . $fname, $num+1);
		}	
	}
	
	/*监管机构搜索接口*/
	public function get_regulator_list(){
		$fname = I('fname');//获取监管机构名称

		$where = array();
		$where['fstate'] = array('neq',-1);
		$where['fname'] = array('like','%'.$fname.'%');
		$where['fpid'] = array('gt',0);
		if($fname != ''){
			$regulatorList = M('tregulator')->field('fcode,fname')->where($where)->limit(10)->select();//查询监管机构列表
		}
		$this->ajaxReturn(array('code'=>0,'value'=>$regulatorList));
		
	}
	
	
	/*获取上级监管机构列表*/
	public function get_pid_list(){
		$fcode = I('fcode');//获取点击的监管机构

		$num = 0;
		$fpid = array();
		
		while($fcode != 0 && $num < 10){
			$info = M('tregulator')->where(array('fcode'=>$fcode))->find();
			if($info['fpid'] > 0 ){
				$fpid[] = $info['fpid'];
			}
			$fcode = $info['fpid'];
			$num++;
		}
		sort($fpid);
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','fpid'=>$fpid));
	}
	
	/**
	 * 菜单权限管理列表
	 * by zw
	 */
	public function tremenulist(){
		if(I('menutype') == ''){
			$_GET['menutype'] = 20;
		}
		$menutype 	= I('menutype');//菜单类型
		$treid 		= I('fid');//机构id
		$fcustomer 		= I('fcustomer');//机构id

		$allmenu = D('Menu')->m_allmenulist2(0,$menutype);//所有菜单列表

		$tremenu = D('Menu')->m_alljgmenulist($menutype,$treid,[],$fcustomer);//获取机构的菜单权限

		$delmenuid = [];
		foreach ($allmenu as $key => $value) {
			foreach ($value['children'] as $key2 => $value2) {
				$iss = in_array($value2['menu_id'], $tremenu);
				if(empty($iss)){
					$delmenuid[] = $value2['parent_id'];
				}
			}
		}
		$delmenuid = array_unique($delmenuid);
		$tremenu = array_merge(array_diff($tremenu, $delmenuid));

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','menulist'=>$allmenu,'tremenulist'=>$tremenu));

		// $this->assign('menutypelist',$do_nme);
		// $this->assign('menulist',json_encode($allmenu));
		// $this->assign('tremenulist',json_encode($tremenu));
		// $this->display();
	}

	/**
	 * 更新机构菜单权限
	 * by zw
	 */
	public function updatetremenu(){
		$selid 			= I('selid');//选中的项的参数，fmenuid菜单id，fmenupid父id，selstate选中状态1为选中0为不选中
		$fregulatorid 	= I('fregulatorid');//机构id
		$menutype 		= I('menutype');//菜单类型
		$fcustomer 		= I('fcustomer');//菜单类型
		if(is_array($selid)){
			$addarr = [];
			$delarr = [];
			foreach ($selid as $key => $value) {
				if($value['selstate'] == 1){
					$addarr[] = array(
						'fregulatorid'	=>$fregulatorid,
						'fmenutype'		=>$menutype,
						'fmenuid'		=>$value['fmenuid'],
						'fmenupid'		=>$value['fmenupid'],
						'fcreator'		=>session('personInfo.fid'),
						'fcreatetime'	=>date('Y-m-d H:i:s'),
						'fstate'		=>1,
						'fcustomer'		=>$fcustomer,
					);
				}else{
					$delarr[] = $value['fmenuid'];
				}
			}
			if(!empty($addarr)){
				M('tregulatormenu')->addAll($addarr);
			}
			if(!empty($delarr)){
				M('tregulatormenu')->where(array('fmenuid'=>array('in',$delarr),'fregulatorid'=>$fregulatorid,'fmenutype'=>$menutype,'fcustomer'=>$fcustomer))->delete();
			}

			$this->ajaxReturn(array('code'=>0,'msg'=>'设置成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'设置失败'));
		}
	}
	
	/*获取上级和本机机构客户ID*/
	public function get_customer_list(){
		$fregionid = I('fregionid');//地域
		$fcode = I('fcode');//机构编号
		$region_list = [];//返回值定义

		$where_tr['a.fcode'] = $fcode;
		$do_tr = M('tregulator')
			->alias('a')
			->field('a.fkind,b.fregionid')
			->join('tregulator b on a.fpid = b.fid','left')
			->where($where_tr)
			->find();
		if((substr($fcode,0,2) == 20 && strlen($fcode) == 8)||$do_tr['fkind'] == 2){
			if($do_tr['fkind'] == 2){
				$fregionid = $do_tr['fregionid'];
			}
			$num = 0;
			while($num < 5 && $fregionid != -1){
				$ss = M('tregion')
					->field('fid,fpid,fname1 as fname')
					->where(array('fid'=>$fregionid))
					->find();
				if($ss['fid'] == 100000){
					$ss['fname'] = '国家';
				}
				$fregionid = $ss['fpid'];
				$region_list[] = $ss;
				$num++;
			}
		}else{
			$ss = M('tregulator')
				->field('fcode as fid,fpid,fname')
				->where(array('fcode'=>$fcode))
				->find();
			$region_list[] = $ss;
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','region_list'=>$region_list));

	}

	/*获取菜单类型*/
	public function get_menu_type(){
		$do_nme = M('new_menutype')->field('me_id,me_name,me_state,me_content,me_type')->where('me_state=10')->order('me_id desc')->select();//菜单类型列表
		$this->ajaxReturn(array('code'=>0,'msg'=>'','data'=>$do_nme));
	}

}