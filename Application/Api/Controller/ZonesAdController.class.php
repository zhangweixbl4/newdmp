<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;

class ZonesAdController extends Controller {
	
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}
	
	public function tv_ad(){
		set_time_limit(120);
		
		
		$ZonesAdTvId = A('Common/System','Model')->important_data('ZonesAdTvId');//获取处理过的id
		$ZonesAdTvId = intval($ZonesAdTvId);

		$samInfo = M('ttvsample')->cache(true,600)->where(array('fillegaltypecode'=>30,'fstate'=>1,'fid'=>array('gt',$ZonesAdTvId)))->find();//查询样本信息
		
		A('Common/System','Model')->important_data('ZonesAdTvId',$samInfo['fid']);//写入新的处理过的id
		if(!$samInfo){//判断是否处理到最后了
			A('Common/System','Model')->important_data('ZonesAdTvId','0');
			exit;
		}
		
		
		$adInfo = M('tad')->cache(true,600)->where(array('fadid'=>$samInfo['fadid']))->find();//查询广告信息
		
		$adSamList = M('ttvsample')
								->cache(true,600)
								->field('ttvsample.issue_relation,ttvsample.fid,tad.fadname,tad.fadclasscode')
								->join('tad on tad.fadid = ttvsample.fadid')
								->where(array('tad.fadname'=>$adInfo['fadname'],'fillegaltypecode'=>30,'ttvsample.fstate'=>1))
								->select();
		
		$samIdList = array();
		foreach($adSamList as $adSam){
			if($adSam['issue_relation']){
				$issue_relation .= $adSam['issue_relation'];//拼接字符串
				$samIdList[] = $adSam['fid'];
			}
			
			
			
		}	
		
		if(!$issue_relation){
			exit;
		}
		
		

		
		$issue_relation_arr = explode(',',$issue_relation);//把字符串转为数组
		$issue_relation_arr = array_filter($issue_relation_arr);//把数组中的空元素去掉
		
		$issue_relation_arr = array_unique($issue_relation_arr);//把数组中的重复去掉
		
		//var_dump($samIdList);
		
		
		$issueList = array();
		foreach($issue_relation_arr as $issue_table_name){
			
			try{//
				$tIssueList = M($issue_table_name)->field('fmediaid,fissuedate,fregionid')->where(array('fsampleid'=>array('in',$samIdList)))->select();
			}catch(Exception $e) { //
				$tIssueList = array();
			} 
			
			$issueList = array_merge($issueList,$tIssueList);//合并查询结果
			
		}
		

		A('Api/ZonesAd','Model')->ad_month($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'01');//汇总计算跨地区月广告数据
		A('Api/ZonesAd','Model')->ad_year($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'01');//汇总计算跨地区年广告数据
		A('Api/ZonesAd','Model')->ad_half_year($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'01');//汇总计算跨地区半年广告数据
		A('Api/ZonesAd','Model')->ad_half_month($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'01');//汇总计算跨地区半月广告数据
		A('Api/ZonesAd','Model')->ad_half_quarter($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'01');//汇总计算跨地区季度广告数据
		A('Api/ZonesAd','Model')->ad_half_week($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'01');//汇总计算跨地区周广告数据
		

	}
	
	
	
	public function bc_ad(){
		set_time_limit(120);
		header("Content-type: text/html; charset=utf-8");
		
		$ZonesAdTvId = A('Common/System','Model')->important_data('ZonesAdBcId');//获取处理过的id
		$ZonesAdTvId = intval($ZonesAdTvId);

		$samInfo = M('tbcsample')->cache(true,600)->where(array('fillegaltypecode'=>30,'fstate'=>1,'fid'=>array('gt',$ZonesAdTvId)))->find();//查询样本信息
		
		A('Common/System','Model')->important_data('ZonesAdBcId',$samInfo['fid']);//写入新的处理过的id
		if(!$samInfo){//判断是否处理到最后了
			A('Common/System','Model')->important_data('ZonesAdBcId','0');
			exit;
		}
		
		
		$adInfo = M('tad')->cache(true,600)->where(array('fadid'=>$samInfo['fadid']))->find();//查询广告信息
		
		$adSamList = M('tbcsample')
								->cache(true,600)
								->field('tbcsample.issue_relation,tbcsample.fid,tad.fadname,tad.fadclasscode')
								->join('tad on tad.fadid = tbcsample.fadid')
								->where(array('tad.fadname'=>$adInfo['fadname'],'fillegaltypecode'=>30,'tbcsample.fstate'=>1))
								->select();
		
		$samIdList = array();
		foreach($adSamList as $adSam){
			if($adSam['issue_relation']){
				$issue_relation .= $adSam['issue_relation'];//拼接字符串
				$samIdList[] = $adSam['fid'];
			}
			
			
			
		}	
		
		if(!$issue_relation){
			exit;
		}
		
		

		
		$issue_relation_arr = explode(',',$issue_relation);//把字符串转为数组
		$issue_relation_arr = array_filter($issue_relation_arr);//把数组中的空元素去掉
		
		$issue_relation_arr = array_unique($issue_relation_arr);//把数组中的重复去掉
		
		//var_dump($samIdList);
		
		
		$issueList = array();
		foreach($issue_relation_arr as $issue_table_name){
			
			try{//
				$tIssueList = M($issue_table_name)->field('fmediaid,fissuedate,fregionid')->where(array('fsampleid'=>array('in',$samIdList)))->select();
			}catch(Exception $e) { //
				$tIssueList = array();
			} 
			
			$issueList = array_merge($issueList,$tIssueList);//合并查询结果
			
		}

		A('Api/ZonesAd','Model')->ad_month($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'02');//汇总计算跨地区月广告数据
		A('Api/ZonesAd','Model')->ad_year($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'02');//汇总计算跨地区年广告数据
		A('Api/ZonesAd','Model')->ad_half_year($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'02');//汇总计算跨地区半年广告数据
		A('Api/ZonesAd','Model')->ad_half_month($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'02');//汇总计算跨地区半月广告数据
		A('Api/ZonesAd','Model')->ad_half_quarter($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'02');//汇总计算跨地区季度广告数据
		A('Api/ZonesAd','Model')->ad_half_week($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'02');//汇总计算跨地区周广告数据
		

	}
	
	
	
	public function paper_ad(){
		set_time_limit(120);
		header("Content-type: text/html; charset=utf-8");
		
		$ZonesAdTvId = A('Common/System','Model')->important_data('ZonesAdPaperId');//获取处理过的id
		$ZonesAdTvId = intval($ZonesAdTvId);

		$samInfo = M('tpapersample')->cache(true,600)->where(array('fillegaltypecode'=>30,'fstate'=>1,'fpapersampleid'=>array('gt',$ZonesAdTvId)))->find();//查询样本信息
		
		A('Common/System','Model')->important_data('ZonesAdPaperId',$samInfo['fpapersampleid']);//写入新的处理过的id
		if(!$samInfo){//判断是否处理到最后了
			A('Common/System','Model')->important_data('ZonesAdPaperId','0');
			exit;
		}
		
		
		$adInfo = M('tad')->cache(true,600)->where(array('fadid'=>$samInfo['fadid']))->find();//查询广告信息
		
		$adSamList = M('tpapersample')
								->cache(true,600)
								->field('tpapersample.fpapersampleid,tad.fadname,tad.fadclasscode')
								->join('tad on tad.fadid = tpapersample.fadid')
								->where(array('tad.fadname'=>$adInfo['fadname'],'fillegaltypecode'=>30,'tpapersample.fstate'=>1))
								->select();

		$samIdList = array();
		foreach($adSamList as $adSam){
			$samIdList[] = $adSam['fpapersampleid'];
		}	

		
		

		
			
		$tIssueList = M('tpaperissue')->field('fmediaid,fissuedate')->where(array('fpapersampleid'=>array('in',$samIdList)))->select();
		$issueList = array();
		foreach($tIssueList as $issue){
			
			$Dissue = array(
							'fmediaid'	=>	$issue['fmediaid'],
							'fissuedate'	=>	strtotime($issue['fissuedate']),
							'fregionid'	=> M('tmedia')->cache(true,86400)->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')->where(array('tmedia.fid'=>$issue['fmediaid']))->getField('tmediaowner.fregionid'),
							
							);
			
			$issueList[] = $Dissue;
			
		}
		
		
		

		A('Api/ZonesAd','Model')->ad_month($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'03');//汇总计算跨地区月广告数据
		A('Api/ZonesAd','Model')->ad_year($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'03');//汇总计算跨地区年广告数据
		A('Api/ZonesAd','Model')->ad_half_year($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'03');//汇总计算跨地区半年广告数据
		A('Api/ZonesAd','Model')->ad_half_month($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'03');//汇总计算跨地区半月广告数据
		A('Api/ZonesAd','Model')->ad_half_quarter($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'03');//汇总计算跨地区季度广告数据
		A('Api/ZonesAd','Model')->ad_half_week($issueList,$adInfo['fadname'],$adInfo['fadclasscode'],'03');//汇总计算跨地区周广告数据
		

	}
	
	
	
	
	
	
	
	
	
	
}