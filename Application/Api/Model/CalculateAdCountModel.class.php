<?php
namespace Api\Model;
use Think\Exception;


class CalculateAdCountModel{


	public function CalculateAdCount($region_id,$media_id,$media_class,$fad_class_code,$start_time,$end_time,$confirmed){

		
		$start_month = date('n',$start_time);//开始月份
		$end_month = date('n',$end_time);//结束月份
		$year = date('Y',$start_time);//年份
		
		$samList = array();
		for($ii=intval($start_month);$ii<=$end_month;$ii++){//循环区间月份
		
			
			$provinceId = substr($region_id,0,2);
			if($ii < 10){
				$monthCode = '0'.strval($ii);
			}else{
				$monthCode = strval($ii);
			}
			if($confirmed == '_confirmed' ){
				$zongju_data_confirm = M('zongju_data_confirm')->cache(true,120)->field('fid')->where(array('fmonth'=>$year.'-'.$monthCode.'-01','dmp_confirm_state'=>2))->find();
				//var_dump(M('zongju_data_confirm')->getLastSql());
				if(!$zongju_data_confirm) continue;//如果没有确认，退出本次循环
			}
			if($media_class == '01'){
				$mtab = 'tv';
			}elseif($media_class == '02'){
				$mtab = 'bc';
			}
			$issue_table_name = 't'.$mtab.'issue_'.$year.$monthCode.'_'.$provinceId;//发布记录表的名称
			try{
				$samLL = M($issue_table_name)
												->cache(true,3600*2)
												->alias('issue_table')
												->field('
															
															issue_table.fsampleid as samid,
															issue_table.fmediaid,
															left(tad.fadclasscode,2) as ad_class_code,
															sample_table.fillegaltypecode as illcode
														')
												->join('t'.$mtab.'sample as sample_table on sample_table.fid = issue_table.fsampleid')
												->join('tad on tad.fadid = sample_table.fadid')
												->where(array(
															
															'issue_table.fissuedate'=>array('between',array($start_time,$end_time)),
															'issue_table.fregionid'=>$region_id,
															
															
															))
															
												->group('issue_table.fsampleid,left(tad.fadclasscode,2),issue_table.fmediaid')			
												->select();//先把所有类别查出来
				
				$samL = array();
				foreach($samLL as $LL){//通过循环查找符合条件的类别，避免多次查询数据库，降低数据库压力
					if($LL['ad_class_code'] == $fad_class_code && $LL['fmediaid'] == $media_id){//寻找广告类别和媒介id匹配的数据
						$samL[] = $LL;
					}
				}							

				//exit;
				//file_put_contents('LOG/ccc_sql',M($issue_table_name)->getLastSql()."\n",FILE_APPEND);								
												
			}catch(Exception $error) {
				$samidL = array();
			} 
			$samList = array_merge($samList,$samL);
			
												
										
		}
		foreach($samList as $sam){
			if(intval($sam['illcode']) == 30){
				$samCount['ill'][$sam['samid']] = 1;//严重违法
			}elseif(intval($sam['illcode']) == 20){
				$samCount['cill'][$sam['samid']] = 1;//一般违法
			}else{	
				$samCount['nill'][$sam['samid']] = 1;//不违法
			}
			
			
		}
		//var_dump($samCount);
		$adCount['ill'] = count($samCount['ill']);//严重违法
		$adCount['ill_total'] = count($samCount['ill']) + count($samCount['cill']);//严重违法 + 一般违法
		
		
		$adCount['nill'] = count($samCount['nill']);
		
		return $adCount;		
		

	}


}



