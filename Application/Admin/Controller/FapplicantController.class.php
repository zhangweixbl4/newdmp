<?php
//存公证模块
namespace Admin\Controller;
use Think\Controller;
class FapplicantController extends BaseController {

    public function index(){
        $this->display();
	}

    /**
     * 已公证订单列表
     * by zw
     */
    public function finishEvidenceList(){
        $fstatus = I('fstatus');// 1公证中，2已公证
        $pageIndex   = I('pageIndex') ? I('pageIndex') : 1;
        $pageSize    = I('pageSize') ? I('pageSize') : 1000;
        $limitIndex  = ($pageIndex-1)*$pageSize;

        if(!empty($fstatus)){
            $where['fstatus'] = $fstatus;
        }

        $count = M('tb_applicant')
            ->where($where)
            ->count();

        $dataSet = M('tb_applicant')
            ->field('fapplicantid,ftype,fentname,fcreditid,fegalperson,fpostusername,fpostmobile,fstatus,fcreatetime,fmoney,fzkmoney,fremark,flog,fpostaddress,fusername,fidcard')
            ->where($where)
            ->order('fstatus asc,fcreatetime desc')
            ->limit($limitIndex,$pageSize)
            ->select();
        $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$dataSet]);
    }

    /**
     * 公证订单详情
     * by zw
     */
    public function evidenceView(){
        $fapplicantid = I('fapplicantid');//公证主键
        if(empty($fapplicantid)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数错误']);
        }

        $where['fapplicantid'] = $fapplicantid;
        $dataSet = M('tb_applicant')
            ->field('fapplicantid,fnotarialtype,ftype,fnotarials,fispost,fusage,fentname,fcreditid,fenturl,fegalperson,fegalidcard,fegalidcardurl,fegalidcardurl2,fusername,fmobile,fidcard,fidcardurl,fidcardurl2,fidcardfeelurl,fposition,fmandateurl,fpostusername,fpostmobile,fpostaddress,fremark,fstatus,fcreatetime,fmoney,fzkmoney,flog')
            ->where($where)
            ->find();

        if(!empty($dataSet)){
            $whereEE['a.fapplicantid'] = $dataSet['fapplicantid'];
            $dataEE = M('tb_applicant_evis')
                ->alias('a')
                ->field('b.*')
                ->join('tb_evidence b on a.fevidenceid = b.id')
                ->where($whereEE)
                ->select();
            $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$dataSet,'data2'=>$dataEE]);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'信息不存在']);
        }
    }

    /**
     * 公证订单修改
     * by zw
     */
    public function evidenceEdit(){
        $fapplicantid = I('fapplicantid');//公证主键
        $fpostusername = I('fpostusername');//收件人
        $fpostmobile = I('fpostmobile');//收件人手机
        $fpostaddress = I('fpostaddress');//邮寄地址
        $fremark = I('fremark');//备注
        $fzkmoney = I('fzkmoney');//实付公证费
        if(empty($fapplicantid)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数错误']);
        }

        $view_at = M('tb_applicant')->where(['fapplicantid'=>$fapplicantid,'fstatus'=>1])->find();
        if(empty($view_at)){
            $this->ajaxReturn(['code'=>1,'msg'=>'当前状态不允许修改']);
        }

        $log = '';
        if($fpostusername != $view_at['fpostusername']){
            $data['fpostusername'] = $fpostusername;
            $log .= session('personInfo.fname')."（管理员：".session('personInfo.fid')."）于".date('Y-m-d H:i:s')."将收件人从【".$view_at['fpostusername']."】更改为【".$fpostusername."】；";
        }
        if($fpostmobile != $view_at['fpostmobile']){
            $data['fpostmobile'] = $fpostmobile;
            $log .= session('personInfo.fname')."（管理员：".session('personInfo.fid')."）于".date('Y-m-d H:i:s')."将收件人手机号从【".$view_at['fpostmobile']."】更改为【".$fpostmobile."】；";
        }
        if($fpostaddress != $view_at['fpostaddress']){
            $data['fpostaddress'] = $fpostaddress;
            $log .= session('personInfo.fname')."（管理员：".session('personInfo.fid')."）于".date('Y-m-d H:i:s')."将邮寄地址从【".$view_at['fpostaddress']."】更改为【".$fpostaddress."】；";
        }
        if($fremark != $view_at['fremark']){
            $data['fremark'] = $fremark;
            $log .= session('personInfo.fname')."（管理员：".session('personInfo.fid')."）于".date('Y-m-d H:i:s')."将备注从【".$view_at['fremark']."】更改为【".$fremark."】；";
        }
        if($fzkmoney != $view_at['fzkmoney']){
            $data['fzkmoney'] = $fzkmoney;
            $log .= session('personInfo.fname')."（管理员：".session('personInfo.fid')."）于".date('Y-m-d H:i:s')."将实付金额从【".$view_at['fzkmoney']."】更改为【".$fzkmoney."】；";
        }
        if(!empty($log)){
            $data['flog'] = $view_at['flog'].$log;
        }
        $data['fedituserid'] = session('personInfo.fid');
        $data['fedituser'] = session('personInfo.fname');
        $data['fedittime'] = date('Y-m-d H:i:s');

        $save_at = M('tb_applicant')->where(['fapplicantid'=>$fapplicantid])->save($data);
        if(!empty($save_at)){
            $this->ajaxReturn(['code'=>0,'msg'=>'修改成功']);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'修改失败，请刷新重新尝试']);
        }
    }

    /**
     * 公证确认完成
     * by zw
     */
    public function evidenceFinish(){
        $fapplicantid = I('fapplicantid');//公证主键
        if(empty($fapplicantid)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数错误']);
        }
        $log = session('personInfo.fname')."（管理员：".session('personInfo.fid')."）于".date('Y-m-d H:i:s')."将公证申请确认完成；";
        $save_at = M('tb_applicant')->where(['fapplicantid'=>$fapplicantid,'fstatus'=>1])->save(['fstatus'=>2,'flog'=>['exp','concat(flog,"'.$log.'")']]);
        if(!empty($save_at)){
            $this->ajaxReturn(['code'=>0,'msg'=>'确认完成']);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'信息不存在，请刷新后重试']);
        }
    }

    /**
     * 跟踪信息添加
     * by zw
     */
    public function evidencegenzong(){
        $fapplicantid = I('fapplicantid');//公证主键
        $content = I('content');//日志内容
        if(empty($fapplicantid) || empty($content)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数错误']);
        }

        $log = session('personInfo.fname')."（管理员：".session('personInfo.fid')."）于".date('Y-m-d H:i:s')."添加跟踪信息：".$content."；";
        $save_at = M('tb_applicant')->where(['fapplicantid'=>$fapplicantid])->save(['flog'=>['exp','concat(flog,"'.$log.'")']]);
        if(!empty($save_at)){
            $this->ajaxReturn(['code'=>0,'msg'=>'提交成功']);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'信息不存在，请刷新后重试']);
        }
    }

    /**
     * 删除公证信息
     * by zw
     */
    public function evidenceDel(){
        $fapplicantid = I('fapplicantid');//公证主键
        if(empty($fapplicantid)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数错误']);
        }

        $save_at = M('tb_applicant')->where(['fapplicantid'=>$fapplicantid,'fstatus'=>1])->save(['fstatus'=>-1]);
        if(!empty($save_at)){
            $this->ajaxReturn(['code'=>0,'msg'=>'确认完成']);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'该状态下不允许此操作']);
        }

    }
}