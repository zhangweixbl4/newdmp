<?php
namespace Open\Controller;
use Think\Controller;

class PaperissueController extends BaseController {
	


	public function add_edit_paperissue(){
		$paperissue_data = file_get_contents('php://input');//获取post数据
		$paperissue_data = json_decode($paperissue_data,true);//把json转化成数组
		//var_dump($paperissue_data);
		$id = $paperissue_data['id'];//广告发布ID
		
		$a_e_data = array();
		
		$a_e_data['fpapersampleid'] = $paperissue_data['fpapersampleid'];//样本ID
		$papersample_count = M('tpapersample')->where(array('fid'=>$a_e_data['fpapersampleid']))->count();//查询报纸样本
		if($papersample_count == 0) $this->ajaxReturn(array('code'=>10016,'msg'=>'添加或修改数据失败,样本ID有错误'));
		
		$a_e_data['fpapermodelid'] = intval($paperissue_data['fpapermodelid']);//模型ID

		$a_e_data['fmediaid'] = $paperissue_data['fmediaid'];//媒体ID
		$media_count = M('tmedia')->where(array('fid'=>$a_e_data['fmediaid']))->count();
		if($media_count == 0) $this->ajaxReturn(array('code'=>10016,'msg'=>'添加或修改数据失败,媒体ID有错误'));
		
		$a_e_data['fissuedate'] = date("Y-m-d H:i:s",strtotime($paperissue_data['fissuedate']));//发布日期
		
		
		
		$a_e_data['fstarttime'] = date("Y-m-d H:i:s",strtotime($paperissue_data['fstarttime']));//发布开始时间
		$a_e_data['fendtime'] = date("Y-m-d H:i:s",strtotime($paperissue_data['fendtime']));//发布结束时间
		$a_e_data['flength'] = intval($paperissue_data['flength']);//时长
		$a_e_data['fissuetype'] = $paperissue_data['fissuetype'];//发布类型
		if($a_e_data['fissuetype'] == '') $this->ajaxReturn(array('code'=>10015,'msg'=>'添加或修改数据失败,发布类型不能为空'));
		
		$a_e_data['famounts'] = intval($paperissue_data['famounts']);//金额
		$a_e_data['fquantity'] = intval($paperissue_data['fquantity']);//数量
		if($a_e_data['fquantity'] == 0) $a_e_data['fquantity'] == 1;//如果数量为0，则默认为1
		
		$a_e_data['fissample'] = intval($paperissue_data['fissample']);//是否样本
		$a_e_data['faudiosimilar'] = intval($paperissue_data['faudiosimilar']);//音频相似度
		$a_e_data['fvideosimilar'] = intval($paperissue_data['fvideosimilar']);//视频相似度
		
		$a_e_data['fadsort'] = $paperissue_data['fadsort'];//广告段排序，非必填
		$a_e_data['fpriorad'] = $paperissue_data['fpriorad'];//前一广告，非必填
		$a_e_data['fnextad'] = $paperissue_data['fnextad'];//后一广告，非必填
		$a_e_data['fpriorprg'] = $paperissue_data['fpriorprg'];//前一节目，非必填
		$a_e_data['fnextprg'] = $paperissue_data['fnextprg'];//后一节目，非必填
		
		$a_e_data['fstate'] = $paperissue_data['fstate'];//状态
		$person_info = $GLOBALS['person_info'];
		
		if($id == '' ){
			$a_e_data['fcreator'] = $person_info['fname'];//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = $person_info['fname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			$rr = M('tpaperissue')->add($a_e_data);
			if($rr > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'添加广告发布成功','id'=>$rr));
			if($rr == 0) $this->ajaxReturn(array('code'=>10011,'msg'=>'添加广告发布失败,原因未知'));
		}else{
			$a_e_data['fmodifier'] = $person_info['fname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tpaperissue')->where(array('id'=>$id))->save($a_e_data);
			if($rr > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'修改广告发布成功'));
			if($rr == 0) $this->ajaxReturn(array('code'=>10012,'msg'=>'修改广告发布失败,原因未知'));
		}
		
		
	
	}
	
	/*获取报纸发布记录*/
	public function get_paperissue_list(){
		
		$paperissue_data = file_get_contents('php://input');//获取post数据
		$paperissue_data = json_decode($paperissue_data,true);//把json转化成数组
		$fpapersampleid = intval($paperissue_data['fpapersampleid']);//报纸广告样本ID
		$p = intval($paperissue_data['p']);//需要返回的页数
		if($p == 0) $p = 1;
		$pp = intval($paperissue_data['pp']);//需要返回的条数
		if($pp <= 0) $pp = 10;
		if($pp > 100) $pp = 100;
		
		if($fpapersampleid == '')  $this->ajaxReturn(array('code'=>10025,'msg'=>'查询失败,缺少样本ID'));
		
		$fissuedate_s = $paperissue_data['fissuedate_s'];//发布日期 开始
		if($fissuedate_s == '') $fissuedate_s = '2015-01-01';
		$fissuedate_e = $paperissue_data['fissuedate_e'];//发布日期 结束
		if($fissuedate_e == '') $fissuedate_e = date('Y-m-d H:i:s');
		
		$where = array();
		$where['fpapersampleid'] = $fpapersampleid;
		$where['fissuedate'] = array('between',$fissuedate_s.','.$fissuedate_e);
		
		$count = M('tpaperissue')->where($where)->count();
		
		$paperissueList = M('tpaperissue')->where($where)->page($p,$pp)->select();

		$this->ajaxReturn(array('code'=>0,'msg'=>'','paperissueList'=>$paperissueList,'count'=>$count));


		
	}	

	public function get_paperissue(){

		$papersample_data = file_get_contents('php://input');//获取post数据
		$papersample_data = json_decode($papersample_data,true);//把json转化成数组

		$fmediaid = $papersample_data['mediaid'];
		if($fmediaid == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'未选择媒介'));
		$fcreatetime_s = date('Y-m-d H:i:s',strtotime($papersample_data['start_date']));//创建时间 开始
		if($fcreatetime_s == '') $fcreatetime_s = '2015-01-01';
		$fcreatetime_e = date('Y-m-d H:i:s',strtotime($papersample_data['end_date'])+86399);//创建时间 结束
		if($fcreatetime_e == '') $fcreatetime_e = date('Y-m-d H:i:s');

		$where['paper.fmediaid'] = $fmediaid;
		$where['paper.fissuedate'] = array('between',$fcreatetime_s.','.$fcreatetime_e);

		$paperissueList = M('tpaperissue')
								->alias('paper')
								->field('
										paper.*,
										tpapersize.fsize,
										tpapersample.fjpgfilename,
										tpapersample.fversion,
										tpapersample.fspokesman,
										tpapersample.fapprovalid,
										tpapersample.fapprovalunit,
										tad.fadname as adname,
										tad.fbrand as brand,
										tad.fadclasscode as adclass_code,
										tadclass.fadclass as adclass
										
										
										
										')
								->join('tpapersize on tpapersize.fcode = paper.fsize')
								->join('tpapersample on tpapersample.fpapersampleid = paper.fpapersampleid')
								
								->join('tad on tad.fadid = tpapersample.fadid')
								->join('tadowner on tadowner.fid = tad.fadowner')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								
								->where($where)->select();
		$count = M('tpaperissue')
								->alias('paper')

								->join('tpapersize on tpapersize.fcode = paper.fsize')
								->join('tpapersample on tpapersample.fpapersampleid = paper.fpapersampleid')
								->where($where)->count();

		$this->ajaxReturn(array('code'=>0,'msg'=>'','paperissueList'=>$paperissueList,'count'=>$count));

	}
	
	/*获取报纸发布记录*/
	public function add_paper_issue(){

		$post_data = file_get_contents('php://input');//获取post数据
		$post_data = json_decode($post_data,true);//把json转化成数组
		
		$mediaid = intval($post_data['mediaid']);//媒体ID
		$mediaInfo = M('tmedia')->field('fid,fmedianame')->where(array('fid'=>$mediaid))->find();
		if(intval($mediaInfo['fid']) == 0){
			$this->ajaxReturn(array('code'=>10027,'msg'=>'查询不到这个媒体','mediaid'=>$mediaid));
		}
		
		$wx_id = $post_data['wx_id'];//微信ID
		$wxInfo = M('ad_input_user')->field('wx_id,nickname')->where(array('wx_id'=>$wx_id))->find();//查询微信用户信息
		if(intval($wxInfo['wx_id']) == 0){
			$this->ajaxReturn(array('code'=>10027,'msg'=>'查询不到这个微信用户'));
		}
		
		if($post_data['issuedate'] == '') $this->ajaxReturn(array('code'=>10027,'msg'=>'未选择发布时间'));
		$issuedate = date('Y-m-d',strtotime($post_data['issuedate']));//发布日期

		$issue_list = $post_data['issueList'];//发布记录，数组
		M()->startTrans();//开启事务方法
		foreach($issue_list as $ke => $issue){//循环发布记录/*******************************************************开始添加发布记录*/
			$issue_data = array();
			$issue_data['fpapersampleid'] = $issue['sampleid'];//样本ID
			$issue_data['fmediaid'] = $mediaid;//媒介ID
			$issue_data['fissuedate'] = $issuedate;//发布日期
			$issue_data['fpage'] = $issue['page']?$issue['page']:'1';//版面
			$issue_data['fpagetype'] = $issue['pagetype'];//版面类别
			$issue_data['fsize'] = $issue['size'];//规格
			$issue_data['fissuetype'] = $issue['issuetype']?$issue['issuetype']:'黑白';//发布类型
			$issue_data['famounts'] = intval($issue['amounts']);//金额
			$issue_data['fquantity'] = intval($issue['quantity']);//数量
			
			$issue_data['fcreator'] = $wxInfo['nickname'];//创建人
			$issue_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$issue_data['fmodifier'] = $wxInfo['nickname'];//修改人
			$issue_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			if(intval($issue['issample']) == 1) $issue_data['fissample'] = 1;//是否样本，1是，0不是
				
			$sampleInfo = M('tpapersample')->field('fpapersampleid')->where(array('fpapersampleid'=>$issue['sampleid']))->find();
					
			if($sampleInfo){
				$issue_add = A('Open/SampleModel','Model')->add_paperissue($issue_data);//新增电视发布并获取信息
				if($issue_add) $issueList[] = $issue_add;
			}else{
				M()->rollback();//事务回滚方法
				$this->ajaxReturn(array('code'=>0,'msg'=>'样本不存在','sampleid'=>$issue['sampleid']));
			}
			
		}/*结束添加发布记录*****************************************************************************************结束添加发布记录*/
		M()->commit();//事务提交方法
		$this->ajaxReturn(array('code'=>0,'msg'=>'','issueCount'=>count($issueList),'issueList'=>$issueList));

	}

}