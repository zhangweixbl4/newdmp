<?php
namespace Api\Model;
use Think\Exception;


class SynIssueModel{



	/*媒介发布数据校验*/
    public function media_issue_check_do($month,$mediaId){
		
		$mediaInfo = M('tmedia')
								->cache(true,5)
								->field('tmedia.fid,left(tmedia.fmediaclassid,2) as media_class,tmediaowner.fregionid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>$mediaId))
								->find();
    
		if($mediaInfo['media_class'] == '01'){
			
			$table_name = 'ttvissue_'.date('Ym',strtotime($month)).'_'.substr($mediaInfo['fregionid'],0,2);//计算分表的名称
			
			try{//
				$er_count = M($table_name)->where(array('fregionid'=>array('neq',$mediaInfo['fregionid']),'fmediaid'=>$mediaInfo['fid']))->count();//查询是否有行政区划已经修改过的媒介发布记录
				if($er_count > 0){
					M($table_name)->where(array('fregionid'=>array('neq',$mediaInfo['fregionid']),'fmediaid'=>$mediaInfo['fid']))->save(array('fregionid'=>$mediaInfo['fregionid']));//修改分表发布记录的行政区划
					M('ttvissue')->where(array('fregionid'=>array('neq',$mediaInfo['fregionid']),'fmediaid'=>$mediaInfo['fid']))->save(array('fregionid'=>$mediaInfo['fregionid']));//修改总表的发布记录的行政区划
					
				}
				
			}catch(Exception $error) { //
				
			} 
			
			
			
			
		}elseif($mediaInfo['media_class'] == '02'){
			
			
			$table_name = 'tbcissue_'.date('Ym',strtotime($month)).'_'.substr($mediaInfo['fregionid'],0,2);//计算分表的名称

			try{//
				$er_count = M($table_name)->where(array('fregionid'=>array('neq',$mediaInfo['fregionid']),'fmediaid'=>$mediaInfo['fid']))->count();//查询是否有行政区划已经修改过的媒介发布记录
				if($er_count > 0){
					M($table_name)->where(array('fregionid'=>array('neq',$mediaInfo['fregionid']),'fmediaid'=>$mediaInfo['fid']))->save(array('fregionid'=>$mediaInfo['fregionid']));//修改分表发布记录的行政区划
					M('ttvissue')->where(array('fregionid'=>array('neq',$mediaInfo['fregionid']),'fmediaid'=>$mediaInfo['fid']))->save(array('fregionid'=>$mediaInfo['fregionid']));//修改总表的发布记录的行政区划
				}
				
			}catch(Exception $error) { //
				
			} 
			
			
			
			
			
			
		}




	}
	
	
	public function check_issue($fmediaid,$fdate){

		$month = date('Ym',strtotime($fdate));//月份
		
		$mediaInfo = M('tmedia')->field('fid,left(fmediaclassid,2) media_class,media_region_id')->where(array('fid'=>$fmediaid))->find();
		
		if($mediaInfo['media_class'] == '01'){
			$type = 'tv';
		}elseif($mediaInfo['media_class'] == '02'){
			
			$type = 'bc';
		}else{
			exit('mediaid error');
		}
		

		$table_postfix = '_'.$month.'_'.substr($mediaInfo['media_region_id'],0,2);//表后缀
		$issue_table = 't'.$type.'issue'.$table_postfix; #发布记录分表名称

		try{//
			$issueList = M($issue_table)
								->field('*,count(*) as count')
								->where(array('fmediaid'=>$fmediaid,'fissuedate'=>strtotime($fdate)))
								->group('fmediaid,fstarttime')
								->select(); #查询发布记录分表记录集
		}catch(Exception $error) { //
			$issueList = [];
		} 
		$startTimeList = [];
		foreach($issueList as $issue){
			$startTimeList[] = date('Y-m-d H:i:s',$issue['fstarttime']);
			if($issue['count'] > 1) { #判断是否有重复的分表记录
				
				
				$delCount = M($issue_table)
									->where(array(
													'fmediaid'=>$fmediaid,
													'fstarttime'=>$issue['fstarttime'],
													'fid'=>array('neq',$issue['fid'])
												))
									->delete();
				
			}
			$check = M('t'.$type.'issue')
								->field('f'.$type.'issueid')
								->where(array('fmediaid'=>$fmediaid,'fstarttime'=>date('Y-m-d H:i:s',$issue['fstarttime'])))
								
								->select(); #查询大表是否有这条记录
			
			if(count($check) == 1){
				
				continue; #如果大表有且只有一条记录 那就 过
			}elseif(count($check) > 1){ #如果大表有 且多于一条记录 那就 清除多于记录
				
				$needDel = [];
				foreach($check as $key => $check0){
					if($key > 0 ) $needDel[] = $check0['f'.$type.'issueid'];
				}
				$delCount = M('t'.$type.'issue')->where(array('f'.$type.'issueid'=>array('in',$needDel)))->delete();
				
				
			}elseif(count($check) == 0){
				
				$a_data = array();
				$a_data['fmediaid'] = $fmediaid;//媒介ID
				$a_data['f'.$type.'sampleid'] = $issue['fsampleid'];//样本ID
				$a_data['f'.$type.'modelid'] = 20000;//样本ID
				
				
				$a_data['fissuedate'] = date('Y-m-d',$issue['fstarttime']);//发布日期
				$a_data['fstarttime'] = date('Y-m-d H:i:s',$issue['fstarttime']);//发布开始时间
				$a_data['fendtime'] = date('Y-m-d H:i:s',intval($issue['fendtime']));//发布结束时间
				$a_data['flength'] = $issue['flength'];//发布时长
				$a_data['faudiosimilar'] = $issue['faudiosimilar'];//相似度得分
				$a_data['fregionid'] = $issue['fregionid'];//地区
				
				$a_data['fu0'] = ($issue['fstarttime'] * 1000) - (strtotime(date('Y-m-d',$issue['fstarttime'])) * 1000);
				$a_data['fduration'] = $issue['flength'] * 1000;

				$a_data['fcreator'] = $issue['fcreator'];//创建人
				$a_data['fcreatetime'] = date('Y-m-d H:i:s',$issue['fcreatetime']);//创建时间
				$a_data['fmodifier'] = $issue['fcreator'];//修改人
				$a_data['fmodifytime'] = date('Y-m-d H:i:s',$issue['fcreatetime']);//修改时间

				M('t'.$type.'issue')->add($a_data);
			}
		}
		if(count($startTimeList) > 0){
			$aa = M('t'.$type.'issue')
								->where(array('fmediaid'=>$fmediaid,'fissuedate'=>$fdate,'fstarttime'=>array('not in',$startTimeList)))
								->save(array('f'.$type.'modelid'=>0));
								#->select();
			
		}

		return true;
		
		

		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}



