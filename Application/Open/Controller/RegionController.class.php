<?php
namespace Open\Controller;
use Think\Controller;
class RegionController extends BaseController {
	/*获取地区列表*/
    public function get_region_list(){
		$region_data = file_get_contents('php://input');//获取post数据
		$region_data = json_decode($region_data,true);//把json转化成数组
		if($region_data['model'] == 'get_all'){//判断是否获取全部
			$regionList = M('tregion')->field('')->where()->select();//查询地区列表
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取的是全部列表','regionList'=>$regionList));
		}
		$fid = intval($region_data['fid']);//地区ID
		if($fid == 0) $fid = 100000;
		$regionList = M('tregion')->field('fid,fname')->where(array('fpid'=>$fid))->select();//查询地区列表
		$regionDetails = M('tregion')->field('fid,left(fname,2) as fname,ffullname')->where(array('fid'=>$fid))->find();//查询地区详情
		if(!$regionDetails) $regionDetails = array('fid'=>100000,'fname'=>'全国','ffullname'=>'全国');
		$this->ajaxReturn(array('code'=>0,'msg'=>'','regionList'=>$regionList,'regionDetails'=>$regionDetails));
	}
	/**
	 * @Des: 获取行政区划
	 * @Edt: yuhou.wang
	 * @Date: 2019-12-10 15:42:38
	 * @param {type} 
	 * @return: 
	 */
    public function getRegionTree(){
		$regionTree = A('Common/Region','Model')->getRegionTree();
		$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$regionTree]);
	}
}