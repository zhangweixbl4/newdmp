<?php
namespace Gongshang\Controller;
use Think\Controller;
class MediaController extends BaseController {

    public function index(){
		$p = I('p',1);//当前第几页
		$pp = 36;//每页显示多少记录
		$keyword = I('keyword');//搜索关键词

		$region_id = session('regulatorpersonInfo.regionid');//地区ID
		if(I('region_id') != '') $region_id = I('region_id');
		$this_region_id = I('this_region_id');//是否只查询本级地区
//		if($region_id == 100000) $region_id = 0;//如果传值等于100000，相当于查全国
		$mediaowner_id = intval(I('mediaowner_id'));//媒介机构ID
		$mediaclass = I('mediaclass');//媒介类型
		$fdeviceid = I('fdeviceid');//设备ID
		$fcollectedid = I('fcollectedid');//采集点
		$fmediaid  = I('fmediaid');// 媒体ID

		$where = array();//查询条件
		$where['tmedia.fstate'] = array('neq',-1);
		$where['tmedia.priority'] = array('egt',0);
		

		if($fmediaid == ''){
			$media=$this->get_access_media();//获取权限判断后的最终媒体列表
			if($media){
				$where['tmedia.fid'] = array('in', $media);//发布媒介
			}
		}else{
			$where['tmedia.fid'] = $fmediaid;//发布媒介
		}

		//如果只查询当前级
		if($this_region_id == 'true'){
			$where['tmediaowner.fregionid'] = $region_id;//地区搜索条件
		}else{
			if($region_id !=100000 && (I('region_id') ||empty($media))){//不是国家工商局并且有地区检索或者没有media
				$region_id_rtrim = rtrim($region_id, '00');//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmediaowner.fregionid,' . $region_id_strlen . ')'] = $region_id_rtrim;//地区搜索条件
			}
		}

		if($mediaclass){
			$where['left(tmedia.fmediaclassid,2)'] = array('in',$mediaclass);
		}
		if($mediaowner_id > 0){
			$where['tmediaowner.fid'] = $mediaowner_id;
		}

		if($keyword != ''){
			$where['tmedia.fmedianame'] = array('like','%'.$keyword.'%');
		} 
		if($fdeviceid != ''){
			$where['tmedia.fdeviceid'] = $fdeviceid;
		}
		if($fcollectedid != ''){
			$where['tcollect.fid'] = $fcollectedid;
		}
//		aa($where);
		$count = M('tmedia')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid','left')//连接媒介机构
							->join('tdevice on tdevice.fid = tmedia.fdeviceid','left')//连接设备表
							->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')//连接媒介类型表
							->join('tcollect on tcollect.fid = tdevice.fcollectid')
							->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$mediaList = M('tmedia')
								->field('tmedia.*,left(tmedia.fmediaclassid,2) as media_classid,tdevice.fname as device_name,tmediaclass.fclass as mediaclass_name,tmediaclass.icon as mediaclass_icon')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid','left')//连接媒介机构
								->join('tdevice on tdevice.fid = tmedia.fdeviceid','left')//连接设备表
								->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')//连接媒介类型表
								->join('tcollect on tcollect.fid = tdevice.fcollectid')
								->where($where)
								->limit($Page->firstRow.','.$Page->listRows)
								->order('tmedia.fdpid desc,tmedia.fid desc')
								->select();//查询媒介列表

		$mediaList = list_k($mediaList,$p,$pp);//为列表加上序号
		foreach($mediaList as $key => $media){
			$mediaList[$key] = $media;
			$mediaList[$key]['savepath'] = U('Admin/Media/paper_media',array('url'=>sys_auth($media['fsavepath'])));
		}
		
		
		//var_dump($mediaList);
		$collecttypeList = M('tcollecttype')->where()->select();//采集方式
		$mediaclassList = M('tmediaclass')->field('fid,fclass,icon')->where(array('fpid'=>''))->select();

		$this->assign('regionid',session('regulatorpersonInfo.regionid'));
		$this->assign('mediaList',$mediaList);//媒介列表
		$this->assign('collecttypeList',$collecttypeList);//采集方式列表
		$this->assign('mediaclassList',$mediaclassList);//媒介类型列表
		

		$this->assign('page',$Page->show());//分页输出
		
		$this->display();

	}
	
	public function get_media_list(){
		
		$media_name = I('media_name');//获取媒介名称
		$fmediaclassid = I('fmediaclassid');
		$regionid = intval(I('regionid'));
		$where = array();
		$where['tmedia.fstate'] = array('neq',-1);
		$where['fmedianame'] = array('like','%'.$media_name.'%');
		if($fmediaclassid != ''){
			$fmediaclassidList = explode('|',$fmediaclassid);
			$where['left(fmediaclassid,2)'] = array('in',$fmediaclassidList);//媒介类型
		}
		

		$media=$this->get_access_media();//获取权限判断后的最终媒体列表
		if($media){
			$where['tmedia.fid'] = array('in', $media);//发布媒介
		}

		

		$mediaList = M('tmedia')->field('tmedia.fid,fmedianame')
													->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
													->where($where)->order('tmedia.fsort desc')->limit(10)->select();//查询媒介列表
		//var_dump(M('tmedia')->getLastSql());
		$this->ajaxReturn(array('code'=>0,'value'=>$mediaList));
		
	}
	
	
	/*电视、广播媒体直播*/
	public function live(){
		$this->display();
	}
	
	/*添加、编辑媒介*/
	public function add_edit_media(){

		$fid= I('fid');//设备ID
		$fid = intval($fid);//转为数字
		$a_e_data = array();
		$a_e_data['fmediaclassid'] = I('fmediaclassid');//媒介类别id
		
		$a_e_data['fmediaownerid'] = I('fmediaownerid');//媒介机构ID
		$a_e_data['fdeviceid'] = I('fdeviceid');//采集设备ID
		$a_e_data['fmediacode'] = I('fmediacode');//媒介代码
		$a_e_data['fmedianame'] = I('fmedianame');//媒介名称
		$a_e_data['faddr'] = I('faddr');//媒介地址
		$a_e_data['fpostcode'] = I('fpostcode');//邮编
		if(mb_strlen($a_e_data['fpostcode']) > 6) $this->ajaxReturn(array('code'=>-1,'msg'=>'邮政编码错误'));//返回邮政编码错误
		$a_e_data['femail'] = I('femail');//电子邮件
		$a_e_data['fcollecttype'] = intval(I('fcollecttype'));//采集方式
		$a_e_data['fliveparam'] = I('fliveparam');//直播参数
		$a_e_data['firparam'] = I('firparam');//红外参数
		$a_e_data['fsavepath'] = I('fsavepath');//保存位置
		$a_e_data['fchannelid'] = I('fchannelid');//通道号
		if(I('expire_date')) $a_e_data['expire_date'] = I('expire_date');//到期时间
		$a_e_data['remark'] = I('remarks');//备注

		
		
		$device_info = M('tdevice')->where(array('fid'=>$a_e_data['fdeviceid']))->find();//查询设备信息
		
		if(intval($a_e_data['fchannelid']) > 0 && $device_info['fdevicetypeid'] == 3){
			
			$device_serial = $device_info['fcode'];//获取设备序列号
			
			A('Common/Shilian','Model')->device_update($device_serial,$a_e_data['fchannelid'],$a_e_data['fmedianame']);
			
			$shilian_get_dpid = A('Common/Shilian','Model')->get_dpid($device_serial,$a_e_data['fchannelid']);//拾联平台号
			
			$shilian_get_dpid = intval($shilian_get_dpid['dpid']);//拾联平台号
			
			if ($shilian_get_dpid > 0) A('Common/Infrared','Model')->add_device($device_info['infrared_code'],$a_e_data['fchannelid'],$shilian_get_dpid);//添加红外设备
			
		}else{
			$ir_param = explode('|',$a_e_data['firparam']);
			A('Common/Infrared','Model')->add_device($ir_param[0],$ir_param[1],0);//添加红外设备
		} 

		
		
		$save_date = intval(I('save_date'));
		if($save_date == 0) $option = 2;
		if($save_date > 0)  $option = 1;

		$save_time = A('Common/Shilian','Model')->save_time($device_serial,$a_e_data['fchannelid'],$option,$save_date*86400 );
		

		
		$a_e_data['fdpid'] = $shilian_get_dpid;
		$a_e_data['live_m3u8'] = $save_time['html5_url'];
		
		
		$a_e_data['fstate'] = I('fstate',1);//状态，-1删除，0-无效，1-有效
		
		if($fid == 0){//判断是修改还是新增
			
			$is_repeat = M('tmedia')->where(array('fmediacode'=>$a_e_data['fmediacode']))->count();//查询媒体代码是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'媒体代码重复'));//返回媒体代码重复
			
			$a_e_data['fcreator'] = '系统最高管理员';//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = '系统最高管理员';//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tmedia')->add($a_e_data);//添加数据
			
			if($rr > 0){//判断是否添加成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));//返回失败
			}
			
		}else{//修改
			
			$is_repeat = M('tmedia')->where(array('fid'=>array('neq',$fid),'fmediacode'=>$a_e_data['fmediacode']))->count();//查询企业信用代码是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'媒体代码重复'));//返回媒体代码重复
			$a_e_data['fmodifier'] = '系统最高管理员';//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tmedia')->where(array('fid'=>$fid))->save($a_e_data);//修改数据
			if($rr > 0){//判断是否修改成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));//返回失败
			}
			
		}
			
		
	}
	
	/*报纸详情*/
	public function paper_media(){
		//str_replace(array('{yyyy}','{MM}','{dd}'),array(date('Y'),date('m'),date('d')),$mediaList[$key]['fsavepath'])
		$savepath = sys_auth(I('url'),'DECODE');//原始路径
		$time = time();//当前时间
		$target_url = '';//目标URL
		$num = 0;
		while($target_url == '' && $num < 10){//如果目标路径等于空和次数小于10
			$url = str_replace(array('{yyyy}','{MM}','{dd}'),array(date('Y',$time),date('m',$time),date('d',$time)),$savepath);//URL
			$header = get_headers($url,1);//获取头部
			if(!strstr($header[0],'404')){//判断返回码是否包含404
				$target_url = $url;//目标地址等于URL
			}
			$time = $time - 86400;
			$num++;
		}
		if($target_url == ''){
			echo '最近'.$num.'天没有找到该报纸素材';
		}else{
			header("Location:".$target_url); 
			exit;
		}
		
	}
	
		
	/*媒介详情*/
	public function ajax_media_details(){
		
		$fid = I('fid');//获取媒介ID
		$mediaDetails = M('tmedia')->where(array('fid'=>$fid))->find();//查询媒介详情
		$mediaDetails['mediaclass_fullname'] = M('tmediaclass')->where(array('fid'=>$mediaDetails['fmediaclassid']))->getField('ffullname');//查询媒介类别全称
		$mediaDetails['mediaowner_name'] = M('tmediaowner')->where(array('fid'=>$mediaDetails['fmediaownerid']))->getField('fname');//查询媒介机构名称
		$deviceInfo = M('tdevice')->where(array('fid'=>$mediaDetails['fdeviceid']))->find();//采集设备
		$inputUser = M('ad_input_user')->field('nickname')->where(array('wx_id'=>$mediaDetails['flockuserid']))->find();//锁定的广告分离用户信息
		$mediaDetails['device_name'] = $deviceInfo['fname'];//设备名称
		
		$mediaDetails['live_url'] = U('Api/Media/live',array('chnID'=>$mediaDetails['fdpid']));//直播地址
		$mediaDetails['live_m3u8_url'] = U('Api/Media/m3u8_live',array('fdpid'=>$mediaDetails['fdpid'],'fid'=>$mediaDetails['fid'],'volume'=>30));//直播地址(m3u8)
		$mediaDetails['mobile_live_url'] = U('Api/Media/mobile_live@'.$_SERVER['HTTP_HOST'],array('device_serial'=>$deviceInfo['fcode'],'chan_no'=>$mediaDetails['fchannelid']));//直播地址
		$mediaDetails['lock_nickname'] = strval($inputUser['nickname']);//锁定的用户昵称
		
		
		$mediaDetails['collecttype_name'] = M('tcollecttype')->where(array('fcollecttypecode'=>$mediaDetails['fcollecttype']))->getField('fcollecttype');//采集方式名称
		$device_serial =  M('tdevice')->where(array('fid'=>$mediaDetails['fdeviceid']))->getField('fcode');//通过设备ID获取设备序列号
		$save_time = A('Common/Shilian','Model')->save_time($device_serial,$mediaDetails['fchannelid']);//
		
		$mediaDetails['save_date'] = intval($save_time['minute']) / 86400;
		
		if($mediaDetails['fcollecttype'] == 24){
			$mediaDetails['live_m3u8_url'] = U('Api/Media/flv_live',array('flv'=>urlencode($mediaDetails['live_m3u8'])));//直播地址(flv)
		}	
		if($mediaDetails['fcollecttype'] == 25 && substr($mediaDetails['fmediaclassid'],0,2) == '02'){
			$mediaDetails['live_m3u8_url'] = U('Api/Media/m3u8_bc_live',array('mediaId'=>$mediaDetails['fid']));//直播地址( )
		} 
		
		$deviceDetails['device_run_state'] = '未知';
		if($mediaDetails['fdpid'] > 0){
			$device_run_state = A('Common/Shilian','Model')->device_checkonline(array($mediaDetails['fdpid']));
			if($device_run_state['ret'] === 0 ){
				$device_run_state = $device_run_state['state'][0];
				//var_dump($device_run_state);
			}
			
		}
		
		
		if($device_run_state === true) $mediaDetails['device_run_state'] = '在线';
		if($device_run_state === false) $mediaDetails['device_run_state'] = '离线';
		
		
		$this->ajaxReturn(array('code'=>0,'mediaDetails'=>$mediaDetails));
	}
	
	
	/*删除媒介*/
	public function ajax_media_del(){
		$fid = I('fid');//获取媒介ID
		M('tmedia')->where(array('fid'=>$fid))->save(array('fstate'=>-1,'flockuserid'=>null));
		$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'.$fid));
		
	}
	
	
	/*获取媒体回放M3U8地址*/

	public function get_playback_url(){
		
		$media_id = I('media_id');
		$start_time = I('start_time');
		$end_time = I('end_time');

		$html5_url = A('Common/Shilian','Model')->get_playback_url($media_id,$start_time,$end_time);
		$mediaInfo = M('tmedia')->where(array('fid'=>$media_id))->find();
		$media_name = $mediaInfo['fmedianame'];
		
		$this->assign('html5_url',$html5_url);
		$this->assign('media_name',$media_name);
		
		$this->display();								
		
	}
	
	/*解除锁定分离广告用户*/
	public function remove_lock_user(){
		$media_id = I('media_id');
		$rr = M('tmedia')->where(array('fid'=>$media_id))->save(array('flockuserid'=>NULL));//修改锁定用户字段
		if($rr > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'解锁成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'解锁失败'));
		}
		
	}
	
	
	
	/*素材生成*/
	public function source_make(){
		

		
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$keyword = I('keyword');//搜索关键词
		$Start_s = I('Start_s');//开始时间
		if(!$Start_s){
			$Start_s = date('Y-m-d',time()-86400*30);
			$_GET['Start_s'] = $Start_s;
		}
		$Start_e = I('Start_e');//结束时间
		if(!$Start_e){
			$Start_e = date('Y-m-d');
			$_GET['Start_e'] = $Start_e;
		}
		$where = array();
		if($keyword != ''){
			$where['source_make.source_name|tmedia.fmedianame'] = array('like','%'.$keyword.'%');
		}
		
		$where['source_make.start_time'] = array( 'between',array(date('Y-m-d',strtotime($Start_s)),date('Y-m-d',strtotime($Start_e)+86400)));
		$where['source_make.creater_id'] = session('regulatorpersonInfo.fid');
		$count = M('source_make')
									->join('tmedia on tmedia.fid = source_make.media_id')
									->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		
		$sourceList = M('source_make')
									->field('source_make.*,tmedia.fmedianame')
									->join('tmedia on tmedia.fid = source_make.media_id')
									->where($where)
									->limit($Page->firstRow.','.$Page->listRows)
									->order('source_make.source_id desc')
									->select();
		//var_dump(M('source_make')->getLastSql());
		
		
		
		$sourceList = list_k($sourceList,$p,$pp);//为列表加上序号
		$this->assign('page',$Page->show());//分页输出
		$this->assign('sourceList',$sourceList);
		$this->display();
	}
	
	
	/*创建素材*/
	public function add_source_make(){
		session_write_close();
		$media_id = I('media_id');//媒介id
		$source_name = I('source_name');//素材名称
		
		
		$Sstart = I('Sstart');//开始时间
		$Send = I('Send');//结束时间
		$Sstart_time = strtotime($Sstart);
		$Send_time = strtotime($Send);
		
		if(($Send_time - $Sstart_time) > 86400 ){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'不允许超过24小时'));
		}elseif(($Send_time - $Sstart_time) < 5){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'不允许小于5秒'));
		}
		
		
		$mediaInfo = M('tmedia')->where(array('fid'=>$media_id))->find();
		
		if(!$mediaInfo){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介选择有错误'));
		}
		
		if(!$source_name){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'素材名称不能为空'));
		}
		
		$a_data = array();
		$a_data['media_id'] = $media_id;
		$a_data['source_name'] = $source_name;
		$a_data['start_time'] = $Sstart;
		$a_data['end_time'] = $Send;
		$a_data['source_state'] = 1;
		$a_data['create_time'] = date('Y-m-d H:i:s');
		$a_data['creater_id'] = session('regulatorpersonInfo.fid');
		$a_data['validity_time'] = time() + 86400 * 30;
		
		
		$source_id = M('source_make')->add($a_data);
		
		
		
		/**************************************************************************************************/
		$start_time = strtotime($Sstart);
		$end_time = strtotime($Send);
		
		
		if(substr($mediaInfo['fmediaclassid'],0,2) == '01'){//判断是电视
			
			$cut_url = C('TV_ISSUE_CUT_URL');//剪辑电视广告片段的url
			if(C('SELF_M3U8')){
				$m3u8_url = 'http://'.$_SERVER['HTTP_HOST'].U('Api/Media/get_media_m3u8',array('media_id'=>$media_id,'start_time'=>$start_time,'end_time'=>$end_time));
			}else{
				$m3u8_url = A('Common/Shilian','Model')->get_playback_url($media_id,$start_time,$end_time);
			}
			
			$back_url = 'http://'.$_SERVER['HTTP_HOST'] . U('Gongshang/Media/edit_tv_ad_part') ;

			$post_data = array(
								'tvissue_id'=>$source_id,
								'start_time'=>$start_time,
								'end_time'=>$end_time,
								'm3u8_url'=>urlencode($m3u8_url),
								'back_url'=>urlencode($back_url),
								//'file_name'=>urlencode(date('YmdHis',$start_time).'_'.$adInfo['fadname'].'_'.$ftvissueid),
								
								
									);
								
		}elseif(substr($mediaInfo['fmediaclassid'],0,2) == '02'){//判断是广播
			$cut_url = C('BC_ISSUE_CUT_URL');//剪辑广播广告片段的url
			
			if($mediaInfo['fcollecttype'] == 10){
				if(C('SELF_M3U8')){
					$m3u8_url = 'http://'.$_SERVER['HTTP_HOST'].U('Api/Media/get_media_m3u8',array('media_id'=>$media_id,'start_time'=>$start_time,'end_time'=>$end_time));
				}else{
					$m3u8_url = A('Common/Shilian','Model')->get_playback_url($media_id,$start_time,$end_time);
				}
			}
			
			if($mediaInfo['fcollecttype'] == 25 || $mediaInfo['fcollecttype'] == 26){
				$m3u8_url = $mediaInfo['fsavepath'].'&start='.$start_time.'&end='.$end_time;
			}
			
			$back_url = 'http://'.$_SERVER['HTTP_HOST'] . U('Gongshang/Media/edit_bc_ad_part') ;
			$post_data = array(
								'bcissue_id'=>$source_id,
								'start_time'=>$start_time,
								'end_time'=>$end_time,
								'm3u8_url'=>urlencode($m3u8_url),
								'back_url'=>urlencode($back_url),
								//'file_name'=>urlencode(date('YmdHis',$start_time).'_'.$adInfo['fadname'].'_'.$ftvissueid),
								
								
									);
			
		}						
								
		$ret = http($cut_url,$post_data,'GET',false,1);
		
		
		
		
		
		/**************************************************************************************************/
		
		
		
		
		
		
		
		if($source_id){
			$this->ajaxReturn(array('code'=>0,'msg'=>'已进入创建流程'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'创建失败,未知原因'));
		}
		
		
		
		
	}
	
	/*修改发布记录视频片段地址*/
	public function edit_tv_ad_part(){

		$post_data = file_get_contents('php://input');
		
		$post_data = json_decode($post_data,true);
		
		
		M('source_make')
					->where(array('source_id'=>$post_data['tvissue_id']))
					->save(array('source_url'=>$post_data['cut_mp4_url'],'validity_time'=>time()+(86400*30),'source_state'=>2));
		
	}
	
	/*修改发布记录视频片段地址*/
	public function edit_bc_ad_part(){

		$post_data = file_get_contents('php://input');
		
		$post_data = json_decode($post_data,true);
		
		
		M('source_make')
					->where(array('source_id'=>$post_data['bcissue_id']))
					->save(array('source_url'=>$post_data['cut_mp4_url'],'validity_time'=>time()+(86400*30),'source_state'=>2));
		
		
	}
	
	/*根据id查询状态*/
	public function get_source_make_state(){
		$sourceIdList = I('sourceIdList');
		$sourceList = array();
		if($sourceIdList){
			$sourceList = M('source_make')->field('source_id,source_state,fail_cause,source_url')->where(array('source_id'=>array('in',$sourceIdList)))->select();
		}
		
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','sourceList'=>$sourceList));
	}
	
	/*删除素材*/
	public function del_source(){
		
		
		$source_id = I('source_id');
		
		$where = array();
		$where['source_id'] = $source_id;
		$where['creater_id'] = session('regulatorpersonInfo.fid');
		
		$del_state = M('source_make')->where($where)->delete();
		
		if($del_state){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败'));
		}
		
		
	}
	
	
	
	
	
	
	
	
	
}