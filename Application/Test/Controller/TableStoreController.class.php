<?php

namespace Test\Controller;
use Think\Controller;


class TableStoreController extends Controller{
	
	
	public function test(){
		
		
		$startPK = array (
			array('fmediaid', 11000010004010),
			array('start_time',1534608000),
			array('end_time',1534718000),
			array('fdate', '2018-08-18'),
		);
		$endPK = array (
			array('fmediaid', 11000010004096),
			array('start_time',1534708000),
			array('end_time',1534908000),
			array('fdate', '2018-08-18'),
		);
		
		$response = A('Common/TableStore','Model')->getRange('ts_key',$startPK,$endPK,10);

		var_dump($response);
		
		
	}

	
	public function putRow(){
		
		
		
		$table_name = 'ts_key2';
		$primary_key = array(
								array('fid',intval(rand(1000000,9999999))),
								//array('start_time',intval(rand(10000000,999999999))),
								//array('end_time',intval(rand(10000000,999999999))),
							);
		$attribute_columns = array(array('ts_url','afdfdafdf'),array('create_time',date('Y-m-d H:i:s')));
		

		$response = A('Common/TableStore','Model')->putRow($table_name,$primary_key,$attribute_columns);

		var_dump($response);
		

	}
	
	public function batchWriteRow(){
		
		
		
		
		$table_name = 'ts_key2';
		
		$dataList = array();
		$dataList[] = array ( // 第一行
						"operation_type" => 'PUT',            //操作是PUT
						'condition' => 'IGNORE',
						'primary_key' => array (
							array('fid',intval(rand(1000000,9999999))),
							
						),
						'attribute_columns' => array(array('ts_url','afdfdafdf'),array('create_time',date('Y-m-d H:i:s'))),
					);
		$dataList[] = array ( // 第一行
						"operation_type" => 'PUT',            //操作是PUT
						'condition' => 'IGNORE',
						'primary_key' => array (
							array('fid',intval(rand(1000000,9999999))),
						
						),
						'attribute_columns' => array(array('ts_url','afdfdafdf'),array('create_time',date('Y-m-d H:i:s'))),
					);
					
		
		
		$rows = $dataList;
		

		$response = A('Common/TableStore','Model')->batchWriteRow($table_name,$rows);

		var_dump($response);
		

	}
	
	



	
	
	

}