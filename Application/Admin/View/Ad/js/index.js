Vue.prototype.$ELEMENT = { size: "mini", zIndex: 3000 };
const search = {
  template: `
    <span>
      <a class="el-icon-search" style="color: #1AB394;font-size: 16px;vertical-align: text-bottom;font-weight: bold;" @click="() => $emit('click')" title="以该字段为条件进行搜索"></a>
      <slot></slot>
    </span>
  `
}
var vm = new Vue({
  el: "#app",
  data() {
    return {
      where: {
        adName: '', //广告名称
        adClass: '', //广告类别
        fmediaid: '', //媒体名称
        adClass2: '', //商业类别
        adBrand: '', //品牌
        fadid: '', //广告id
        adOwner: '', //广告主
        fcreator: '', //创建人
        fcreatetime: '', //创建日期
        fmodifier: '', //修改人
        fmodifytime: '', //修改日期
        issueDate: [], // 发布日期,
        state: '',
        fstate: '',
        fregionid: '', // 地域 id
        fmediaclass: '', // 媒体类型
        fregionid_only: '2', // 仅本级,
        adNameSearchType: '1'
      },
      fmediaid: [],
      clickflag: null,
      sourceWhere: '',
      adLoading: false,
      tableLoading: false,
      showAdInfo: false,
      showHistory: false,
      historyLoading: true,
      tableData: [],
      moreParms: false,
      adClassArr: adClassArr,
      adClassTree: adClassTree,
      regionTree: regionTree,
      props: {
        checkStrictly: true,
        emitPath: false
      },
      mediaList: [],
      adClass: [],
      adClass2: [],
      page: 1,
      pageSize: 20,
      total: 0,
      table: [],
      adInfo: {},
      searchAdOwnerLoading: false,
      searchAdOwnerRes: [],
      orderBy: "fmodifytime",
      orderType: "desc",
      tableHeight: '400px',
      multipleSelection: [],
      showMerge: false,
      mainSample: '',
      preWhere: {},
      fstateMap: {
        '1': '已确认',
        '2': '待确认',
        '9': '未确认',
      },
    }
  },
  components: {
    adInfo,
    search
  },
  computed: {
    canMerge() {
      return this.multipleSelection.length >= 2
    }
  },
  created() {
    this.getList()
    this.sourceWhere = JSON.stringify(this.where)
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight()
      document.querySelector('#app').style.opacity = 1
    })
    window.addEventListener('resize', () => {
      this.getTableHeight()
    })
  },
  methods: {
    history({fadid}) {
      this.showHistory = true
      this.historyLoading = true
      fly.post('/Admin/Ad/getHistory', { fadid }).then(res => {
        this.tableData = res.data.data.data || []
        this.historyLoading = false
      }).catch(() => {
        this.historyLoading = false
      })
    },
    toggleParms() {
      this.moreParms = !this.moreParms
      this.$nextTick(() => {
        this.getTableHeight()
      })
    },
    rowClick(row, column, event) {
      if (this.clickflag) {
        this.clickflag = clearTimeout(this.clickflag)
      }
      this.clickflag = setTimeout(() => {
        this.$refs.table.toggleRowSelection(row)
      },300)
    },
    getList(page) {
      this.page = page || 1
      this.tableLoading = true;
      let data = {
        where: this.where,
        page: this.page,
        pageSize: this.pageSize,
        order: this.orderBy,
        orderType: this.orderType
      }
      this.preWhere = data.where
      fly.post("/Admin/Ad/index", Qs.stringify(data)).then(({data}) => {
        if (data.code == 0) {
          this.table = data.data.map(i => {
            i._fadid = i.fadid.slice(-4)
            i._fadname = this.setHightColor(i.fadname, this.preWhere.adName)
            i._fbrand = this.setHightColor(i.fbrand, this.preWhere.adBrand)
            i._fname = this.setHightColor(i.fname, this.preWhere.adOwner)
            i._fcreator = this.setHightColor(i.fcreator, this.preWhere.fcreator)
            i._fmodifier = this.setHightColor(i.fmodifier, this.preWhere.fmodifier)
            i.fadclasscode = $h.getAdClassCode(i.fadclasscode)
            i.fadclasscode_v2 = $h.getAdClassCode(i.fadclasscode_v2)
            return i
          })
          this.total = +data.total
        } else {
          this.$message.error(data.msg)
        }
        this.tableLoading = false
      })
    },
    deleteTheme({ fadid = '', fadname = '' }) {
      let _fadid = fadid && fadname ? fadid : this.multipleSelection.map(i => i.fadid).join(',')
      let message = ''
      const h = this.$createElement
      if (fadid && fadname) {
        message = h('p', null, [
          h('span', null, '确认删除广告内容：'),
          h('span', { style: 'color: #F56C6C' }, fadname),
          h('span', null, '？')
        ])
      } else {
        message = `确认删除所选的 ${this.multipleSelection.length} 条广告内容？`
      }
      this.$msgbox({
        title: '确 认',
        message,
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'danger',
        closeOnClickModal: false,
        closeOnPressEscape: false,
      }).then(() => {
        fly.post('/Admin/Ad/delAd', { fadids: _fadid }).then(({ data }) => {
          if (data.code === 0) {
            $h.notify.success(data.msg ,this)
            this.getList(this.page)
          }else{
            $h.notify.error(data.msg ,this)
          }
        })
      }).catch(() => {
      })
    },
    resetWhere() {
      let source = JSON.parse(this.sourceWhere)
      source.state = this.where.state
      this.where = source
      this.adClass = []
      this.adClass2 = []
      this.page = 1
      this.getList()
    },
    pageSizeChange(pageSize) {
      this.pageSize = pageSize
      this.getList(this.page)
    },
    openAdInfo(row) {
      if (this.clickflag) {
        this.clickflag = clearTimeout(this.clickflag)
      }
      fly.post("/Admin/Ad/getDetail", Qs.stringify({ fadid: row.fadid })).then(({data}) => {
        if (data.code == 0) {
          this.adInfo = data.adDetails
          this.showAdInfo = true
        } else {
          this.$message.error("获取详情失败")
          this.closeAdInfo()
        }
      })
    },
    closeAdInfo() {
      this.showAdInfo = false
      this.adInfo = {}
    },
    addAd() {
      this.adInfo = {
        fstate: "1"
      };
      this.showAdInfo = true
    },
    sortChange(row) {
      this.orderBy = row.prop;
      if (row.order[0] === "a") {
        this.orderType = "asc";
      } else {
        this.orderType = "desc"
      }
      this.getList(this.page)
    },
    searchMediaName(queryString) {
      let data = {
        name: queryString,
        fregionid: this.where.fregionid,
        fmediaclass: this.where.fmediaclass,
        fregionid_only: this.where.fregionid_only,
      }
      fly.post("/Api/Media/searchMediaByName", Qs.stringify(data)).then(({data}) => {
          this.mediaList = data.data
        }
      )
    },
    getTableHeight() {
      let formHeight = this.$refs.sform.$el.clientHeight
      this.tableHeight = document.body.clientHeight - formHeight - 154 + 'px'
    },
    handleAdclassChange(val, name) {
      this.$set(this, name, val)
      this.$set(this.where, name, [...val].pop())
    },
    handleSearch(name, val) {
      if (name === 'adClass' || name === 'adClass2') {
        this.handleAdclassChange(val,name)
      } else if (name === 'fcreatetime' || name === 'fmodifytime') {
        let _val = Array(2).fill(val.substr(0,10))
        this.$set(this.where, name, _val)
      }else {
        this.$set(this.where, name, val)
      }
      this.getList()
    },
    handleSelectionChange(val) {
      this.multipleSelection = val
    },
    submitMerge() {
      let data = {
        mainfadid: this.mainSample,
        fadids: this.multipleSelection.map(i => i.fadid).join(',')
      }
      fly.post('/Admin/Ad/mergeAd', data).then(({data}) => {
        if (data.code === 0) {
          $h.notify.success(data.msg, this)
          this.resetTable()
          this.beforeClose()
          this.getList(this.page)
        } else {
          $h.notify.error(data.msg,this)
        }
      }).catch(e => console.error(e))
    },
    resetTable() {
      this.mainSample = ''
      this.$refs.table.clearSelection()
      this.multipleSelection = []
    },
    beforeClose() {
      this.showMerge = false
    },
    setHightColor(str,query) {
      return str ? str.split(query).join(`<span style="color: #F56C6C;">${query}</span>`) : str
    },
  }
});
