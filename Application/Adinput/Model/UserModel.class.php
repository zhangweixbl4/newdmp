<?php
namespace Adinput\Model;



class UserModel{


	/*用户余额增加或减少*/
	public function money($wx_id,$money = 0,$explain = ''){
		//var_dump($money);
		if($money != 0){
			M('ad_input_user')->where(array('wx_id'=>$wx_id))->save(array('money'=>array('exp','money + '.$money)));
			//var_dump(M('ad_input_user')->getLastSql());
			M('ad_input_user_money')->add(array(
												'money'=>$money,
												'time'=>date('Y-m-d H:i:s'),
												'explain'=>$explain,
												'wx_id'=>$wx_id,
											));
		}
		
	}



}