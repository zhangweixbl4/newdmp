<?php
namespace Open\Model;



class BcsampleModel{


    /*处理违法表现*/
    public function bcsampleillegal($fid,$fexpressioncodes_array,$fcreator){

		M('tbcsampleillegal')->where(array('fsampleid'=>$fid ))->delete();//删除之前与这条样本关联的违法表现
		$fillegaltypecode = 0;//初始化违法类型代码
		foreach($fexpressioncodes_array as $k => $v){//循环违法表现代码
			$illegal_info = M('tillegal')->where(array('fcode'=>$v))->find();
			if($illegal_info){
				$bcsampleillegal = array();
				$bcsampleillegal['fsampleid'] = $fid ;
				$bcsampleillegal['fsequence'] = $k;//排序的序号
				$bcsampleillegal['fillegalcode'] = $illegal_info['fcode'];
				$bcsampleillegal['fexpression'] = $illegal_info['fexpression'];//违法表现
				$bcsampleillegal['fconfirmation'] = $illegal_info['fconfirmation'];//认定依据
				$bcsampleillegal['fpunishment'] = $illegal_info['fpunishment'];//处罚依据
				$bcsampleillegal['fpunishmenttype'] = $illegal_info['fpunishmenttype'];//处罚种类及幅度
				$bcsampleillegal['fcreator'] = $fcreator;//创建人
				$bcsampleillegal['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
				$bcsampleillegal['fmodifier'] = $fcreator;//修改人
				$bcsampleillegal['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
				$bcsampleillegal['fstate'] = 1;//状态
				M('tbcsampleillegal')->add($bcsampleillegal);//添加
				
				$fexpressioncodes .= $illegal_info['fcode'].';';//把合规的违法表现代码加入字符串
				$fexpression .= $illegal_info['fexpression'].';'."\n";//把合规的违法表现加入字符串
				$fconfirmation .= $illegal_info['fconfirmation'].';'."\n";//把合规的认定依据加入字符串
				$fpunishment .= $illegal_info['fpunishment'].';'."\n";//把合规的处罚依据加入字符串
				$fpunishmenttype .= $illegal_info['fpunishmenttype'].';'."\n";//把合规的处罚种类及幅度加入字符串
				if($illegal_info['fillegaltype'] > $fillegaltypecode) $fillegaltypecode = $illegal_info['fillegaltype'];//获取违法类型代码
			}
		}
		$return_data = array();
		$return_data['fexpressioncodes'] = rtrim($fexpressioncodes,';');//去掉末尾分号
		$return_data['fexpressions'] = rtrim($fexpression,';'."\n");//去掉末尾分号
		$return_data['fconfirmations'] = rtrim($fconfirmation,';'."\n");//去掉末尾分号
		$return_data['fpunishments'] = rtrim($fpunishment,';'."\n");//去掉末尾分号
		$return_data['fpunishmenttypes'] = rtrim($fpunishmenttype,';'."\n");//去掉末尾分号
		$return_data['fillegaltypecode'] = $fillegaltypecode;//违法类型代码
		return $return_data;
    }



}