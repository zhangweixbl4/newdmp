<?php
namespace Agp\Controller;
use Think\Controller;
use OpenSearch\Client\OpenSearchClient;
use OpenSearch\Client\DocumentClient;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;

class OutWordFJController extends BaseController{

    /**
     * 获取历史报告列表
     * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（count-记录总数，list-列表数据）
     * by zw
     */
    public function index() {
        session_write_close();
        header("Content-type:text/html;charset=utf-8");

        $p                  = I('page', 1);//当前第几页
        $pp                 = 20;//每页显示多少记录
        $pnname             = I('pnname');// 报告名称
        $pntype             = I('pntype');// 报告类型
        $pnfiletype         = I('pnfiletype');// 文件类型
        $pnendtime          = I('pnendtime');//报告时间
        $system_num = getconfig('system_num');

        if (!empty($pnname)) {
            $where['pnname'] = array('like', '%' . $pnname . '%');//报告名称
        }
        if (!empty($pntype)) {
            $where['pntype'] = $pntype;//报告类型
        }else{
            $where['pntype'] = array('in',array(10,20,30));//报告类型
        }
        if (!empty($pnfiletype)) {
            $where['pnfiletype'] = $pnfiletype;//文件类型
        }
        if(!empty($pnendtime)){
            $where['pnendtime'] = array('between',array($pnendtime[0],$pnendtime[1]));
        }   
        $where['pntrepid'] = session('regulatorpersonInfo.fregulatorpid');
        $where['fcustomer']  = $system_num;

        $count = M('tpresentation')
            ->where($where)
            ->count();

        
        $data = M('tpresentation')
            ->where($where)
            ->order('pncreatetime desc')
            ->page($p,$pp)
            ->select();
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
    }

    /**
     * 获取复核报告列表
     * by zw
     */
    public function fuhe_baogao() {
        session_write_close();
        
        $p                  = I('page', 1);//当前第几页
        $pp                 = 20;//每页显示多少记录
        $pnname             = I('pnname');// 报告名称
        $pnfiletype         = I('pnfiletype');// 文件类型
        $pncreatetime       = I('pnstarttime');//生成时间
        $system_num = getconfig('system_num');

        if (!empty($pnname)) {
            $where['pnname'] = array('like', '%' . $pnname . '%');//报告名称
        }
        if (!empty($pnfiletype)) {
            $where['pnfiletype'] = $pnfiletype;//文件类型
        }
        if(!empty($pncreatetime)){
            $where['pnstarttime'] = array('between',array($pncreatetime[0],$pncreatetime[1]));
        }  

        $where['pntrepid'] = session('regulatorpersonInfo.fregulatorpid');
        $where['pntype'] = 60;//报告类型
        $where['fcustomer']  = $system_num;

        $count = M('tpresentation')
            ->where($where)
            ->count();

        
        $data = M('tpresentation')
            ->where($where)
            ->order('pnendtime desc')
            ->page($p,$pp)
            ->select();
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
    }

    
   
    /**
     * 生成复核数据
     * by zw
     */
    public function create_fuhepresentation($daytime,$focus = 0,$treid){
        header("Content-type: text/html; charset=utf-8"); 
        session_write_close();
        $system_num = getconfig('system_num');
        $do_tr = M('tregulator')->field('tregulator.fname,tregion.fname1 as regionname,tregulator.fregionid,tregion.flevel')->join('tregion on tregulator.fregionid=tregion.fid')->where(array('tregulator.fid' => $treid))->find();//获取机构信息
        if(!empty($do_tr)){
            $pnname = $do_tr['regionname'].date('Y年m月',strtotime($daytime)).'复核报告';
            $do_tn = M('tpresentation')->field('pnid')->where('pnname="'.$pnname.'" and fcustomer = "'.$system_num.'" and pnfocus='.$focus)->find();
            if(!empty($do_tn)){
                return ;
            }

            $do_tn = M('tregion')->alias('a')->field('a.fname1,a.fid')->join('tregulator b on a.fid=b.fregionid and b.fkind=1 and b.ftype=20')->where('(a.fpid='.$do_tr['fregionid'].' or a.fid='.$do_tr['fregionid'].') and b.fstate=1')->select();
            if(!empty($do_tn)){
                $allcount       = 0;
                $allyfhcount    = 0;
                $allcgfhcount   = 0;
                $allfhcount     = 0;
                foreach ($do_tn as $key => $value) {
                    $addhtml .= '<tr><td align="center">'.($key+1).'</td>';
                    $addhtml .= '<td align="center">'.$value['fname1'].'</td>';
                    $do_tv = M('ttvsample')
                        ->alias('a')
                        ->field('freviewstate,review_state')
                        ->join('tmedia b on a.fmediaid=b.fid  and b.fid=b.main_media_id')
                        ->join('tmediaowner c on b.fmediaownerid=c.fid ')
                        ->where('DATE_FORMAT(a.fissuedate,"%Y-%m")="'.date('Y-m',strtotime($daytime)).'" and fregionid='.$value['fid'])
                        ->select();
                    $count      = count($do_tv);
                    $yfhcount   = 0;
                    $cgfhcount  = 0;
                    $allcount   += $count;
                    foreach ($do_tv as $key2 => $value2) {
                        if($value2['freviewstate']>0){
                            $allfhcount += 1;
                        }
                        if($value2['freviewstate']==30){
                            $yfhcount += 1;
                            $allyfhcount += 1;
                        }
                        if($value2['review_state']==10){
                            $cgfhcount += 1;
                            $allcgfhcount += 1;
                        }
                    }

                    $do_bc = M('tbcsample')
                        ->alias('a')
                        ->field('a.freviewstate,a.review_state')
                        ->join('tmedia b on a.fmediaid=b.fid  and b.fid=b.main_media_id')
                        ->join('tmediaowner c on b.fmediaownerid=c.fid ')
                        ->where('DATE_FORMAT(a.fissuedate,"%Y-%m")="'.date('Y-m',strtotime($daytime)).'" and fregionid='.$value['fid'])
                        ->select();
                    $count      += count($do_bc);
                    $allcount   += $count;
                    foreach ($do_bc as $key2 => $value2) {
                        if($value2['freviewstate']>0){
                            $allfhcount += 1;
                        }
                        if($value2['freviewstate']==30){
                            $yfhcount += 1;
                            $allyfhcount += 1;
                        }
                        if($value2['review_state']==10){
                            $cgfhcount += 1;
                            $allcgfhcount += 1;
                        }
                    }

                    $do_paper = M('tpapersample')
                        ->alias('a')
                        ->field('a.freviewstate,a.review_state')
                        ->join('tmedia b on a.fmediaid=b.fid  and b.fid=b.main_media_id')
                        ->join('tmediaowner c on b.fmediaownerid=c.fid ')
                        ->where('DATE_FORMAT(a.fissuedate,"%Y-%m")="'.date('Y-m',strtotime($daytime)).'" and fregionid='.$value['fid'])
                        ->select();
                    $count      += count($do_paper);
                    $allcount   += $count;
                    foreach ($do_paper as $key2 => $value2) {
                        if($value2['freviewstate']>0){
                            $allfhcount += 1;
                        }
                        if($value2['freviewstate']==30){
                            $yfhcount += 1;
                            $allyfhcount += 1;
                        }
                        if($value2['review_state']==10){
                            $cgfhcount += 1;
                            $allcgfhcount += 1;
                        }
                    }
                    $addhtml .= '<td align="center">'.$count.'</td>';
                    $addhtml .= '<td align="center">'.$yfhcount.'</td>';
                    $addhtml .= '<td align="center">'.(round($yfhcount/$count,3)*100).'%</td>';
                    $addhtml .= '<td align="center">'.$cgfhcount.'</td>';
                    $addhtml .= '<td align="center">'.(round($cgfhcount/$yfhcount,3)*100).'%</td>';
                    $addhtml .= '</tr>';
                }
            }
            if(empty($addhtml)){
                $addhtml = '<tr><td align="center" colspan="7"></td></tr>';
            }
            $html = '<body style="font-size:18px; line-height:24px;"><div align="center" style="font-size:30px; font-weight:bold; padding:10px;">广告复核报告</div>
                <table style="border:0;margin:0;border-collapse:collapse;border-spacing:0;" width="100%">
                    <tr>
                        <td align="left">'.$do_tr['regionname'].'广告监测中心</td>
                        <td align="right">'.date('Y年m月d日',strtotime($daytime)).'</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-top:3px solid #333;">&nbsp;&nbsp;'.$do_tr['regionname'].'广告监管部门对'.date(strtotime($daytime),'Y年m月').'在广告监测平台提供广告监测数据进行复核，复核情况如下：</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;&nbsp;<span style="font-weight:bold;">一、概况</span></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;&nbsp;全省（含省、市两级）传统媒体（电视、广播、报纸）在'.date('Y年m月1日',strtotime($daytime)).'至'.date('d', strtotime(date('Y-m-01', strtotime($daytime)) . ' +2 month -1 day')).'日共发布各类广告'.$allcount.'条，抽取了'.$allfhcount.'条广告进行复核，广告复核率为'.(round($yfhcount/$count,3)*100).'%，中复核的广告中共发现有'.($yfhcount-$cgfhcount).'条广告的违法认定信息或广告基本信息不符合规定要求，复核通过率为'.(round($cgfhcount/$yfhcount,3)*100).'%。</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;&nbsp;<span style="font-weight:bold;">二、各监管部复核情况</span></td>
                    </tr>
                </table>
                <table border="1" cellpadding="0" width="100%" cellspacing="0" style="border-collapse: collapse;">
                    <tr>
                        <td align="center">序号</td><td align="center">监管机构</td><td align="center">发布数</td><td align="center">复核数</td><td align="center">复核率</td><td align="center">通过数</td><td align="center">通过率</td>
                    </tr>
                    '.$addhtml.'
                </table>
                </body>
            ';

            $date = date('Ymdhis');
            $savefilename = 'FJ'.$date.'.doc';
            $savefile = './Public/Word/'.$savefilename;

            //将文档保存
            word_start();
            echo $html;
            word_save($savefile);
            ob_flush();//每次执行前刷新缓存
            flush();

            $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$savefilename,$savefile,array('Content-Disposition' => 'attachment;filename='.$pnname.'.doc'));//上传云
            unlink($savefile);//删除文件

            //将生成记录保存
            $data['pnname']             = $pnname;
            $data['pntype']             = 60;
            $data['pnfiletype']         = 10;
            $data['pnstarttime']        = date('Y-m-d',strtotime($daytime));
            $data['pncreatetime']       = date('Y-m-d H:i:s');
            $data['pnurl']              = $ret['url'];
            $data['pnhtml']             = $html;
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $treid;
            $data['pntreid']            = $treid;
            $data['pncreatepersonid']   = 0;
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
        }
     
    }


    /**
     * 获取跟踪报告列表
     * by zw
     */
    public function genzong_baogao() {
        session_write_close();
        $system_num = getconfig('system_num');
        $p                  = I('page', 1);//当前第几页
        $pp                 = 20;//每页显示多少记录
        $pnname             = I('pnname');// 报告名称
        $pnfiletype         = I('pnfiletype');// 文件类型
        $pncreatetime       = I('pnstarttime');//生成时间

        if (!empty($pnname)) {
            $where['pnname'] = array('like', '%' . $pnname . '%');//报告名称
        }
        if (!empty($pnfiletype)) {
            $where['pnfiletype'] = $pnfiletype;//文件类型
        }
        if(!empty($pncreatetime)){
            $where['pnstarttime'] = array('between',array($pncreatetime[0],$pncreatetime[1]));
        }   

        $where['pntrepid'] = session('regulatorpersonInfo.fregulatorpid');
        $where['pntype'] = 61;//报告类型
        $where['fcustomer']  = $system_num;

        $count = M('tpresentation')
            ->where($where)
            ->count();

        
        $data = M('tpresentation')
            ->where($where)
            ->order('pnendtime desc')
            ->page($p,$pp)
            ->select();
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
        
    }

    /**
     * 生成跟踪报告
     * by zw
     */
    public function create_genzongpresentation($daytime,$focus = 0,$treid){
        session_write_close();
        $system_num = getconfig('system_num');
        $do_tr = M('tregulator')->field('tregulator.fname,tregion.fname1 as regionname,tregulator.fregionid,tregion.flevel')->join('tregion on tregulator.fregionid=tregion.fid')->where(array('tregulator.fid' => $treid))->find();//获取机构信息
        if(!empty($do_tr)){
            $pnname = $do_tr['regionname'].date('Y年m月',strtotime($daytime)).'跟踪报告';
            
            $do_tn = M('tpresentation')->where('pnname="'.$pnname.'" and pntrepid='.$treid.' and fcustomer = "'.$system_num.'" and pnfocus='.$focus)->find();
            if(!empty($do_tn)){
                return ;
            }

            $where_td['_string'] = 'fid in (select ftk_tregisterid from fj_tregistertrack where date_format(ftk_createtime,"%Y-%m")="'.date('Y-m',strtotime($daytime)).'" and ftk_trepid='.$treid.') ';
            $do_td = M('fj_tregister')
                ->alias('a')
                ->field('*,TIMESTAMPDIFF(day,fregistertime,now()) as diffdays,ifnull(b.fflowname,"线索办结") as fflowname')
                ->join('fj_tregisterflow b on a.fid=b.fregisterid and fsendstatus=0','left')
                ->where($where_td)
                ->select();

            foreach ($do_td as $key => $value) {
                $addhtml .= '<tr><td align="center">'.($key+1).'</td>';
                $addhtml .= '<td align="center">'.$value['fnumber'].'</td>';
                $addhtml .= '<td align="center">'.$value['fcluename'].'</td>';
                if($value['ffromtype']==10){
                    $addhtml .= '<td align="center">广告监测</td>';
                }elseif($value['ffromtype']==20){
                    $addhtml .= '<td align="center">网络或书面投申诉举报</td>';
                }elseif($value['ffromtype']==30){
                    $addhtml .= '<td align="center">上级机关交办</td>';
                }elseif($value['ffromtype']==40){
                    $addhtml .= '<td align="center">其他机关移送</td>';
                }
                $addhtml .= '<td align="center">'.$value['fregisterregulatorpname'].'</td>';
                $addhtml .= '<td align="center">'.$value['fregisterregulatorname'].'</td>';
                $addhtml .= '<td align="center">'.$value['fregisterpersonname'].'</td>';
                $addhtml .= '<td align="center">'.$value['diffdays'].'</td>';
                if($value['fpersontype']==10){
                    $addhtml .= '<td align="center">个人</td>';
                }elseif($value['fpersontype']==20){
                    $addhtml .= '<td align="center">单位</td>';
                }elseif($value['fpersontype']==30){
                    $addhtml .= '<td align="center">移送机关</td>';
                }elseif($value['fpersontype']==0){
                    $addhtml .= '<td align="center">其他</td>';
                }
                $addhtml .= '<td align="center">'.$value['fpersonname'].'</td>';
                $addhtml .= '<td align="center">'.$value['fpersoncard'].'</td>';
                $addhtml .= '<td align="center">'.$value['fpersonmobile'].'</td>';
                $addhtml .= '<td align="center">'.$value['fpersonpostcode'].'</td>';
                $addhtml .= '<td align="center">'.$value['fpersonadress'].'</td>';
                $addhtml .= '<td align="center">'.$value['fpersonmail'].'</td>';
                $addhtml .= '<td align="center">'.$value['freportedpeople'].'</td>';
                $addhtml .= '<td align="center">'.$value['filladdress'].'</td>';
                $addhtml .= '<td align="center">'.$value['fappeal'].'</td>';
                $addhtml .= '<td align="center">'.$value['fregistercontents'].'</td>';
                $addhtml .= '<td align="center">'.date('Y-m-d',strtotime($value['fregistertime'])).'</td>';
                $addhtml .= '<td align="center">'.$value['fflowname'].'</td>';
                $addhtml .= '<td align="center">'.date('Y-m-d',strtotime($value['fdealtime'])).'</td>';
                $addhtml .= '<td align="center">'.$value['fpunish'].'</td></tr>';
            }

            if(empty($addhtml)){
                $addhtml = '<tr><td align="center" colspan="23">暂无信息</td></tr>';
            }

            $html = '<body style="font-size:18px; line-height:24px;"><div align="center" style="font-size:30px; font-weight:bold; padding:10px;">'.date('Y年m月',strtotime($daytime)).'案件线索跟踪报告</div>
                <table style="border:0;margin:0;border-collapse:collapse;border-spacing:0;" width="100%">
                    <tr>
                        <td align="left" style="font-size:18px;">监管机构：'.$do_tr['fname'].'</td>
                    </tr>
                    <tr>
                        <td style="font-size:18px;">'.date('Y年m月1日',strtotime($daytime)).'至'.date('d', strtotime(date('Y-m-01', strtotime($daytime)) . ' +1 month -1 day')).'日</td>
                    </tr>
                </table>
                <table border="1" cellpadding="0" width="100%" cellspacing="0" style="border-collapse: collapse;">
                    <tr>
                        <td align="center">序号</td>
                        <td align="center">文号</td>
                        <td align="center">线索名称</td>
                        <td align="center">来源类型</td>
                        <td align="center">登记机构</td>
                        <td align="center">登记单位</td>
                        <td align="center">登记人</td>
                        <td align="center">办理天数</td>
                        <td align="center">举报人性质</td>
                        <td align="center">举报人姓名</td>
                        <td align="center">证件号码</td>
                        <td align="center">联系电话</td>
                        <td align="center">邮政编码</td>
                        <td align="center">联系地址</td>
                        <td align="center">邮箱地址</td>
                        <td align="center">被举报人</td>
                        <td align="center">违法地址</td>
                        <td align="center">诉求事项</td>
                        <td align="center">摘要内容</td>
                        <td align="center">登记日期</td>
                        <td align="center">流程环节</td>
                        <td align="center">反馈时间</td>
                        <td align="center">处罚方式</td>
                    </tr>
                    '.$addhtml.'
                </table>
                </body>
            ';
            $date = date('Ymdhis');
            $savefilename = 'FJ'.$date.'.doc';
            $savefile = './Public/Word/'.$savefilename;

            //将文档保存
            word_start();
            echo $html;
            word_save($savefile);
            ob_flush();//每次执行前刷新缓存
            flush();

            $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$savefilename,$savefile,array('Content-Disposition' => 'attachment;filename='.$pnname.'.doc'));//上传云
            unlink($savefile);//删除文件

            //将生成记录保存
            $data['pnname']             = $pnname;
            $data['pntype']             = 61;
            $data['pnfiletype']         = 10;
            $data['pnstarttime']        = date('Y-m-d',strtotime($daytime));
            $data['pncreatetime']       = date('Y-m-d H:i:s');
            $data['pnurl']              = $ret['url'];
            $data['pnhtml']             = $html;
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $treid;
            $data['pntreid']            = $treid;
            $data['pncreatepersonid']   = 0;
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
        }
 
    }

    public function create_daypresentation(){
        $daytime = I('daytime')?I('daytime'):date('Y-m-d');
        $focus   = I('focus',0);
        $this->create_daypresentation2($daytime,$focus,session('regulatorpersonInfo.fregulatorpid'));
    }

    //日报
    public function create_daypresentation3($daytime,$focus,$treid){
        session_write_close();
        $system_num = getconfig('system_num');
        header("Content-type:text/html;charset=utf-8");
        vendor('PHPWord.PHPWord');
        vendor('PHPWord.PHPWord.IOFactory');

        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);

        $tredata = M('tregulator')
            ->field('a.*,b.fname1')
            ->alias('a')
            ->join('tregion b on a.fregionid = b.fid')
            ->where('a.fid = '.$treid)
            ->find();
        
        $where_ma['tmedia.fstate'] = 1;
        $where_ma['_string'] = 'tmedia.main_media_id = tmedia.fid';
        $do_ma = M('tmedia')
            ->field('tmedia.fid,fmediaclassid, (case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')
            ->join('tregulatormedia ta on tmedia.fid=ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'"')
            ->where($where_ma)
            ->order('media_region_id asc')
            ->select();

        /* *************************正式开始文档编辑**************************** */
        $PHPWord = new \PHPWord();//引入组件
        $PHPWord->setDefaultFontSize(10);//默认字体大小

        $fontStyle1 = array('color'=>'333333', 'size'=>24, 'bold'=>true);//样式配置1
        $fontStyle2 = array('color'=>'333333', 'size'=>18);//样式配置2
        $fontStyle3 = array('color'=>'333333', 'size'=>14, 'bold'=>true);//样式配置3
        $fontStyle4 = array('color'=>'333333', 'size'=>14);//样式配置4

        $lineStyle1 = array('align'=>'left','indentFirstLineChars'=>200,'spacing'=>50);
        $lineStyle2 = array('align'=>'center','spacing'=>100);

        $sectionStyle = array(
            'orientation' => null,//文档方向
        );//页面样式
        

        $section = $PHPWord->createSection($sectionStyle);//创建文档

        $section->addText('广告监测日报',$fontStyle1,$lineStyle2);//标题
        $section->addText($daytime,$fontStyle2,$lineStyle2);//日期
        /*---------------------按媒体生成---------------------------*/
        $tb_tvissue_nowmonth = gettable('tv',strtotime($daytime),$tredata['fregionid']);
        $tb_tvissue_nevmonth = gettable('tv',strtotime("$daytime -1 month"),$tredata['fregionid']);
        $tb_bcissue_nowmonth = gettable('bc',strtotime($daytime),$tredata['fregionid']);
        $tb_bcissue_nevmonth = gettable('bc',strtotime("$daytime -1 month"),$tredata['fregionid']);

        foreach ($do_ma as $key => $value2) {
            //组合查询条件
            $where ='';
            $day_start = strtotime($daytime);
            $day_end = strtotime($daytime) + 86400;//日期结束
            $where .= ' and a.fmediaid='.$value2['fid'];

            //判断媒体类型
            if(substr($value2['fmediaclassid'],0,2)=='01'){
                $mediatype = 'tv';
                $where .= " and a.fissuedate>=$day_start and a.fissuedate<$day_end ";
            }elseif(substr($value2['fmediaclassid'],0,2)=='02'){
                $mediatype = 'bc';
                $where .= " and a.fissuedate>=$day_start and a.fissuedate<$day_end ";
            }elseif(substr($value2['fmediaclassid'],0,2)=='03'){
                $mediatype = 'paper';
                $where .= " and UNIX_TIMESTAMP(a.fissuedate)>=$day_start and UNIX_TIMESTAMP(a.fissuedate)<$day_end ";
            }elseif(substr($value2['fmediaclassid'],0,2)=='13'){
                $mediatype = 'net';
                $where .= " and UNIX_TIMESTAMP(a.fissuedate)>=$day_start and UNIX_TIMESTAMP(a.fissuedate)<$day_end ";
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
            }

            //如果是电视类型
            if($mediatype=='tv'){

                //电视发布统计数据
                $iesql = 'select 
                    count(*) as ggsl,
                    sum(case when b.fillegaltypecode>0 then 1 else 0 end) as wfggsl,
                    sum(case when left(c.fadclasscode,2)="22" then 1 else 0 end) as gyggsl,
                    sum(flength) as ggscmj,
                    sum(case when b.fillegaltypecode>0 then flength else 0 end) as wfggscmj,
                    sum(case when left(c.fadclasscode,2)="22" then flength else 0 end) as gyggscmj
                    from ((select fsampleid,fmediaid,flength,fissuedate from '.$tb_tvissue_nevmonth.') union all (select fsampleid,fmediaid,flength,fissuedate from '.$tb_tvissue_nowmonth.')) as a,ttvsample b , tad c 
                    where a.fsampleid=b.fid and b.fadid=c.fadid and c.fadid<>0 '.$where.' 
                    group by a.fmediaid';

                //电视发布违法信息
                $sesql = 'select x.*,y.fadname from ttvsample x join tad y on x.fadid=y.fadid and y.fadid<>0 where x.fid in(select fsampleid from '.$tb_tvissue_nowmonth.' as a where 1=1 '.$where.' group by fsampleid) and fillegaltypecode>0';
            }elseif($mediatype=='bc'){
                $iesql = 'select 
                count(*) as ggsl,
                sum(case when b.fillegaltypecode>0 then 1 else 0 end) as wfggsl,
                sum(case when left(c.fadclasscode,2)="22" then 1 else 0 end) as gyggsl,
                sum(flength) as ggscmj,
                sum(case when b.fillegaltypecode>0 then flength else 0 end) as wfggscmj,
                sum(case when left(c.fadclasscode,2)="22" then flength else 0 end) as gyggscmj
                from ((select fsampleid,fmediaid,flength,fissuedate from '.$tb_bcissue_nevmonth.') union all (select fsampleid,fmediaid,flength,fissuedate from '.$tb_bcissue_nowmonth.')) as a,tbcsample b , tad c
                where a.fsampleid=b.fid and b.fadid=c.fadid and c.fadid<>0 '.$where.' 
                group by a.fmediaid';//广播发布统计数据

                $sesql = 'select x.*,y.fadname from tbcsample x join tad y on x.fadid=y.fadid and y.fadid<>0 where x.fid in(select fsampleid from '.$tb_bcissue_nowmonth.' as a where 1=1 '.$where.' group by fsampleid) and fillegaltypecode>0';
            }elseif($mediatype=='paper'){
                $iesql = 'select 
                count(*) as ggsl,
                sum(case when b.fillegaltypecode>0 then 1 else 0 end) as wfggsl,
                sum(case when left(c.fadclasscode,2)="22" then 1 else 0 end) as gyggsl,
                0 as ggscmj,
                0 as wfggscmj,
                0 as gyggscmj
                from tpaperissue a,tpapersample b, tad c
                where a.fpapersampleid=b.fpapersampleid and b.fadid=c.fadid and c.fadid<>0 '.$where.' 
                group by a.fmediaid';//报纸发布统计数据

                $sesql = 'select x.*,y.fadname from tpapersample x join tad y on x.fadid=y.fadid and y.fadid<>0 where x.fpapersampleid in(select fpapersampleid from tpaperissue a where 1=1 '.$where.' group by fpapersampleid) and fillegaltypecode>0';//报纸发布违法信息
            }elseif($mediatype=='net'){
                $iesql = 'select 
                count(*) as ggsl,
                sum(case when fillegaltypecode>0 then 1 else 0 end) as wfggsl,
                sum(case when left(fadclasscode,2)="22" then 1 else 0 end) as gyggsl,
                0 as ggscmj,
                0 as wfggscmj,
                0 as gyggscmj
                from tnetissue a 
                where 1=1  and finputstate=2  and fmediaid='.$value2['fid'].' and datediff("'.$daytime.'",FROM_UNIXTIME(net_created_date/1000))=0 
                group by fmediaid';//报纸发布统计数据

                $sesql = 'select * from tnetissue a where major_key in (select min(major_key) as major_key from tnetissue where fmediaid='.$value2['fid'].'  and finputstate=2 and fillegaltypecode>0 and datediff("'.$daytime.'",FROM_UNIXTIME(net_created_date/1000))=0 group by fadname)';//报纸发布违法信息
            }

            $do_ie = M()->query($iesql);//获取统计结果
            $do_se = M()->query($sesql);//获取所有违法信息
            $section->addTextBreak(2);
            $section->addText($value2['fmedianame'],$fontStyle3,$lineStyle1);//媒体名称
            $section->addText(
                '发布商业广告'.($do_ie[0]['ggsl']-$do_ie[0]['gyggsl']).'条次 时长'.($do_ie[0]['ggscmj']-$do_ie[0]['gyggscmj']).'秒,涉嫌违法'.$do_ie[0]['wfggsl'].'条次 时长'.$do_ie[0]['wfggscmj'].'秒,公益广告'.$do_ie[0]['gyggsl'].'条次 时长'.$do_ie[0]['gyggscmj'].'秒',$fontStyle4,$lineStyle1
            );//汇总

            //违法内容信息
            if(count($do_se)>0){
                $section->addText('涉嫌违法广告',$fontStyle3);//违法广告标头
                foreach ($do_se as $key => $value) {
                    $section->addTextBreak();
                    $section->addText(($key+1).'.'.$value['fadname'],null,$lineStyle1);//违法广告名称
                    if($mediatype=='tv'){//电视版面
                        $do_ie2 = M()->query('select fstarttime,flength from ((select fstarttime,flength,fsampleid,fissuedate from '.$tb_tvissue_nevmonth.') union all (select fstarttime,flength,fsampleid,fissuedate from '.$tb_tvissue_nowmonth.')) as a where fsampleid='.$value['fid'].' and fissuedate='.strtotime($daytime).' GROUP BY fstarttime');//0609
                        foreach ($do_ie2 as $key2 => $value2) {
                            $fstarttimes .= date('H:i:s',$value2['fstarttime']).'   ';
                        }
                        $section->addText('发布时间：'.$fstarttimes.'  时长:'.$do_ie2[0]['flength'].'秒',null,$lineStyle1);
                        $section->addText('涉嫌违法内容：'.$value['fillegalcontent'],null,$lineStyle1);
                        $section->addText('违法表现：'.$value['fexpressions'],null,$lineStyle1);
                        $section->addText('近期发布情况(15天内发布情况)：',null,$lineStyle1);

                        //建15天发布情况表
                        $styleTable = array('borderSize'=>1, 'alignMent' => 'center','cellMarginTop'=>80, 'cellMarginLeft'=>0, 'cellMarginRight'=>0, 'cellMarginBottom'=>80);
                        $styleFirstRow = array('bgColor'=>'#ffffff');//首行背景色
                        $PHPWord->addTableStyle('tableStyle', $styleTable, $styleFirstRow);//将自定义样式写入组件内备用
                        $table = $section->addTable('tableStyle');//设置表格样式

                        $cellfontStyle = array('align'=>'center','color'=>'777777');
                        $cellStyle = array(
                            'valign'=>'center',
                            'borderTopSize'=>1,
                            'borderTopColor'=>'333333',
                            'borderLeftSize'=>1,
                            'borderLeftColor'=>'333333',
                            'borderBottomSize'=>1,
                            'borderBottomColor'=>'333333',
                            'borderRightSize'=>1,
                            'borderRightColor'=>'333333'
                        );
                        $table->addRow(100);
                        $table->addCell(2000,$cellStyle)->addText('日期',$cellfontStyle);
                        for($x=14; $x>=1;$x--){
                            $nowtime = date('Y-m-d H:i:s',strtotime($daytime)-3600*24*$x);
                            $table->addCell(2000,$cellStyle)->addText(date('d',strtotime($nowtime)),$cellfontStyle);
                        }
                        $table->addCell(2000,$cellStyle)->addText(date('d',strtotime($daytime)).'(当日)',$cellfontStyle);
                        $table->addRow(100);
                        $table->addCell(2000,$cellStyle)->addText('发布',$cellfontStyle);
                        for($x=14; $x>=0;$x--){
                            $nowtime = date('Y-m-d H:i:s',strtotime($daytime)-3600*24*$x);
                            $nowcount = M(gettable('tv',strtotime($nowtime),$tredata['fregionid']))->where('fsampleid='.$value['fid'].' and datediff("'.$nowtime.'",from_unixtime(fissuedate))=0')->count();
                            $table->addCell(2000,$cellStyle)->addText($nowcount,$cellfontStyle);
                        }

                    }elseif($mediatype=='bc'){//广播版面
                        $do_ie2 = M()->query('select fstarttime,flength from ((select fstarttime,flength,fsampleid,fissuedate from '.$tb_bcissue_nevmonth.') union all (select fstarttime,flength,fsampleid,fissuedate from '.$tb_bcissue_nowmonth.')) as a where fsampleid='.$value['fid'].' and fissuedate='.strtotime($daytime).' GROUP BY fstarttime');
                        foreach ($do_ie2 as $key2 => $value2) {
                            $fstarttimes .= date('H:i:s',$value2['fstarttime']).'   ';
                        }
                        $section->addText('发布时间：'.$fstarttimes.'  时长:'.$do_ie2[0]['flength'].'秒');
                        $section->addText('涉嫌违法内容：'.$value['fillegalcontent']);
                        $section->addText('违法表现：'.$value['fexpressions']);
                        $section->addText('近期发布情况(15天内发布情况)：');

                        //建15天发布情况表
                        $styleTable = array('borderSize'=>1, 'alignMent' => 'center','cellMarginTop'=>80, 'cellMarginLeft'=>0, 'cellMarginRight'=>0, 'cellMarginBottom'=>80);
                        $styleFirstRow = array('bgColor'=>'#ffffff');//首行背景色
                        $PHPWord->addTableStyle('tableStyle', $styleTable, $styleFirstRow);//将自定义样式写入组件内备用
                        $table = $section->addTable('tableStyle');//设置表格样式

                        $cellfontStyle = array('align'=>'center','color'=>'777777');
                        $cellStyle = array(
                            'valign'=>'center',
                            'borderTopSize'=>1,
                            'borderTopColor'=>'333333',
                            'borderLeftSize'=>1,
                            'borderLeftColor'=>'333333',
                            'borderBottomSize'=>1,
                            'borderBottomColor'=>'333333',
                            'borderRightSize'=>1,
                            'borderRightColor'=>'333333'
                        );
                        $table->addRow(100);
                        $table->addCell(2000,$cellStyle)->addText('日期',$cellfontStyle);
                        for($x=14; $x>=1;$x--){
                            $nowtime = date('Y-m-d H:i:s',strtotime($daytime)-3600*24*$x);
                            $table->addCell(2000,$cellStyle)->addText(date('d',strtotime($nowtime)),$cellfontStyle);
                        }
                        $table->addCell(2000,$cellStyle)->addText(date('d',strtotime($daytime)).'(当日)',$cellfontStyle);
                        $table->addRow(100);
                        $table->addCell(2000,$cellStyle)->addText('发布',$cellfontStyle);
                        for($x=14; $x>=0;$x--){
                            $nowtime = date('Y-m-d',strtotime($daytime)-3600*24*$x);
                            $nowcount = M(gettable('bc',strtotime($nowtime),$tredata['fregionid']))->where('fsampleid='.$value['fid'].' and datediff("'.$nowtime.'",from_unixtime(fissuedate))=0')->count();
                            $table->addCell(2000,$cellStyle)->addText($nowcount,$cellfontStyle);
                        }

                    }elseif($mediatype=='paper'){//报纸版面
                        $do_ie2 = M()->query('select fpage from tpaperissue where fpapersampleid='.$value['fpapersampleid'].' and datediff("'.$daytime.'",fissuedate)=0');
                        foreach ($do_ie2 as $key2 => $value2) {
                            $fpages .= $value2['fpage'].'  ';
                        }
                        $section->addText('发布版面：'.$fpages.'  版本:'.$value['fversion']);
                        $section->addText('涉嫌违法内容：'.$value['fillegalcontent']);
                        $section->addText('违法表现：'.$value['fexpressions']);
                        $section->addText('近期发布情况(15天内发布情况)：');
                        $table = $section->addTable('tableStyle');//设置表格样式
                        $cellfontStyle = array('align'=>'center','color'=>'777777');
                        $cellStyle = array(
                            'valign'=>'center',
                            'borderTopSize'=>1,
                            'borderTopColor'=>'333333',
                            'borderLeftSize'=>1,
                            'borderLeftColor'=>'333333',
                            'borderBottomSize'=>1,
                            'borderBottomColor'=>'333333',
                            'borderRightSize'=>1,
                            'borderRightColor'=>'333333'
                        );
                        $table->addRow(100);
                        $table->addCell(2000,$cellStyle)->addText('日期',$cellfontStyle);
                        for($x=14; $x>=1;$x--){
                            $nowtime = date('Y-m-d H:i:s',strtotime($daytime)-3600*24*$x);
                            $table->addCell(2000,$cellStyle)->addText(date('d',strtotime($nowtime)),$cellfontStyle);
                        }
                        $table->addCell(2000,$cellStyle)->addText(date('d',strtotime($daytime)).'(当日)',$cellfontStyle);
                        $table->addRow(100);
                        $table->addCell(2000,$cellStyle)->addText('发布',$cellfontStyle);
                        for($x=14; $x>=0;$x--){
                            $nowtime = date('Y-m-d H:i:s',strtotime($daytime)-3600*24*$x);
                            $nowcount = M('tpaperissue')->where('fpapersampleid='.$value['fpapersampleid'].' and datediff("'.$nowtime.'",fissuedate)=0')->count();
                            $table->addCell(2000,$cellStyle)->addText($nowcount,$cellfontStyle);
                        }
                    }elseif($mediatype=='net'){//互联网
                        $section->addText('最早发现时间：'.date('Y-m-d',$value['net_created_date']/1000));
                        $section->addText('涉嫌违法内容：'.$value['fillegalcontent'],null,$lineStyle1);
                        $section->addText('近期发布情况(15天内发布情况)：',null,$lineStyle1);

                        //建15天发布情况表
                        $styleTable = array('borderSize'=>1, 'alignMent' => 'center','cellMarginTop'=>80, 'cellMarginLeft'=>0, 'cellMarginRight'=>0, 'cellMarginBottom'=>80);
                        $styleFirstRow = array('bgColor'=>'#ffffff');//首行背景色
                        $PHPWord->addTableStyle('tableStyle', $styleTable, $styleFirstRow);//将自定义样式写入组件内备用
                        $table = $section->addTable('tableStyle');//设置表格样式

                        $cellfontStyle = array('align'=>'center','color'=>'777777');
                        $cellStyle = array(
                            'valign'=>'center',
                            'borderTopSize'=>1,
                            'borderTopColor'=>'333333',
                            'borderLeftSize'=>1,
                            'borderLeftColor'=>'333333',
                            'borderBottomSize'=>1,
                            'borderBottomColor'=>'333333',
                            'borderRightSize'=>1,
                            'borderRightColor'=>'333333'
                        );
                        $table->addRow(100);
                        $table->addCell(2000,$cellStyle)->addText('日期',$cellfontStyle);//0611
                        for($x=14; $x>=1;$x--){
                            $nowtime = date('Y-m-d H:i:s',strtotime($daytime)-3600*24*$x);
                            $table->addCell(2000,$cellStyle)->addText(date('d',strtotime($nowtime)),$cellfontStyle);
                        }
                        $table->addCell(2000,$cellStyle)->addText(date('d',strtotime($daytime)).'(当日)',$cellfontStyle);
                        $table->addRow(100);
                        $table->addCell(2000,$cellStyle)->addText('发布',$cellfontStyle);
                        for($x=14; $x>=0;$x--){
                            $nowtime = date('Y-m-d H:i:s',strtotime($daytime)-3600*24*$x);
                            $nowcount = M('tnetissue')->where('fadname="'.$value['fadname'].'" and datediff("'.$nowtime.'",net_created_date)=0  and finputstate=2 ')->count();
                            $table->addCell(2000,$cellStyle)->addText($nowcount,$cellfontStyle);
                        }

                    }
                }
            }

        }

        // 保存文件
        $objWriter = \PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
        $date = date('Ymdhis');
        $savefilename = 'FJ'.$date.'.docx';
        $savefile = './Public/Word/'.$savefilename;

        $objWriter->save($savefile);
        
        $pnname = $tredata['fname1'].date('Y',strtotime($daytime)).'年'.date('m',strtotime($daytime)).'月'.date('d',strtotime($daytime)).'日监测日报';

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$savefilename,$savefile,array('Content-Disposition' => 'attachment;filename='.$pnname.'.docx'));//上传云
        unlink($savefile);//删除文件

        $data['pnname']             = $pnname;
        $data['pntype']             = 10;
        $data['pnfiletype']         = 10;
        $data['pnstarttime']        = $daytime;
        $data['pnendtime']          = $daytime;
        $data['pncreatetime']       = date('Y-m-d H:i:s');
        $data['pnurl']              = $ret['url'];
        $data['pnfocus']            = $focus;
        $data['pntrepid']           = $tredata['fid'];
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
        }

    }

    //日报（固定数据版）
    public function create_daypresentation2($daytime,$focus,$treid){
        session_write_close();
        $system_num = getconfig('system_num');
        header("Content-type:text/html;charset=utf-8");
        vendor('PHPWord.PHPWord');
        vendor('PHPWord.PHPWord.IOFactory');

        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);
        $tredata = M('tregulator')
            ->field('a.*,b.fname1')
            ->alias('a')
            ->join('tregion b on a.fregionid = b.fid')
            ->where('a.fid = '.$treid)
            ->find();
        
        $where_ma['tmedia.fstate'] = 1;
        $where_ma['_string'] = 'tmedia.main_media_id = tmedia.fid';
        $do_ma = M('tmedia')
            ->field('tmedia.fid,fmediaclassid, (case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')
            ->join('tregulatormedia ta on tmedia.fid=ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'"')
            ->where($where_ma)
            ->order('media_region_id asc,fmediaclassid asc')
            ->select();
        /* *************************正式开始文档编辑**************************** */
        $PHPWord = new \PHPWord();//引入组件
        $PHPWord->setDefaultFontSize(10);//默认字体大小

        $fontStyle1 = array('color'=>'333333', 'size'=>24, 'bold'=>true);//样式配置1
        $fontStyle2 = array('color'=>'333333', 'size'=>18);//样式配置2
        $fontStyle3 = array('color'=>'333333', 'size'=>14, 'bold'=>true);//样式配置3
        $fontStyle4 = array('color'=>'333333', 'size'=>14);//样式配置4

        $lineStyle1 = array('align'=>'left','indentFirstLineChars'=>200,'spacing'=>50);
        $lineStyle2 = array('align'=>'center','spacing'=>100);

        $sectionStyle = array(
            'orientation' => null,//文档方向
        );//页面样式

        $section = $PHPWord->createSection($sectionStyle);//创建文档

        $section->addText('广告监测日报',$fontStyle1,$lineStyle2);//标题
        $section->addText($daytime,$fontStyle2,$lineStyle2);//日期
        /*---------------------按媒体生成---------------------------*/

        //所有媒体基础数据
        $mediaids = [];
        foreach ($do_ma as $key => $value) {
            $mediaids[] = $value['fid'];
            $mediadata[$value['fid']]['fmediaid'] = $value['fid'];
            $mediadata[$value['fid']]['fmediaclassid'] = $value['fmediaclassid'];
            $mediadata[$value['fid']]['fmedianame'] = $value['fmedianame'];
            $mediadata[$value['fid']]['ztccount'] = 0 ;
            $mediadata[$value['fid']]['zsccount'] = 0 ;
            $mediadata[$value['fid']]['wftccount'] = 0 ;
            $mediadata[$value['fid']]['wfsccount'] = 0 ;
            $mediadata[$value['fid']]['gytccount'] = 0 ;
            $mediadata[$value['fid']]['gysccount'] = 0 ;
        }

        //获取广告大类数组
        $where_adclass['length(fcode)'] = 2;
        $where_adclass['fcode'] = ['neq','22'];
        $adclass_arr = M('tadclass')->where($where_adclass)->getfield('fcode',true);

        //获取商业广告统计结果
        $where_sy['fad_class_code'] = ['in',$adclass_arr];
        $where_sy['fmediaid'] = ['in',$mediaids];
        $where_sy['a.fdate'] = $daytime;
        $where_sy['a.fcustomer'] = $system_num;
        $do_sy = M('tbn_ad_summary_day_v3')
            ->alias('a')
            ->field('fmediaid,sum(fad_times_long_ad) ztccount,sum(fad_play_len_long_ad) zsccount')
            ->where($where_sy)
            ->group('fmediaid')
            ->select();
        foreach ($do_sy as $key => $value) {
            $mediadata[$value['fmediaid']]['ztccount'] = $value['ztccount'];
            $mediadata[$value['fmediaid']]['zsccount'] = $value['zsccount'];
        }

        //违法广告量
        $where_wf['n.fissue_date'] = $daytime;
        $where_wf['m.fcustomer'] = $system_num;
        $where_wf['m.fstatus3'] = ['notin',[30,40]];
        $where_wf['m.fmedia_id'] = ['in',$mediaids];
        $do_wf = M('tbn_illegal_ad')
            ->alias('m')
            ->field('m.fmedia_id,count(*) wftccount,SUM(UNIX_TIMESTAMP(n.fendtime) - UNIX_TIMESTAMP(n.fstarttime)) wfsccount')
            ->join('tbn_illegal_ad_issue n on m.fid = n.fillegal_ad_id ')
            ->where($where_wf)
            ->group('m.fmedia_id')
            ->select();
        foreach ($do_wf as $key => $value) {
            $mediadata[$value['fmedia_id']]['wftccount'] = $value['wftccount'];
            $mediadata[$value['fmedia_id']]['wfsccount'] = $value['wfsccount'];
        }

        //获取商业广告统计结果
        $where_gy['fmediaid'] = ['in',$mediaids];
        $where_gy['fdate'] = $daytime;
        $where_gy['fcustomer'] = $system_num;
        $do_gy = M('tbn_ad_summary_day_v3_gongyi')
            ->field('fmediaid,sum(fad_times) ztccount,sum(fad_play_len) zsccount')
            ->where($where_gy)
            ->select();
        foreach ($do_sy as $key => $value) {
            $mediadata[$value['fmediaid']]['gytccount'] = $value['ztccount'];
            $mediadata[$value['fmediaid']]['gysccount'] = $value['zsccount'];
        }

        foreach ($mediadata as $key2 => $value2) {
            $section->addTextBreak(2);
            $section->addText($value2['fmedianame'],$fontStyle3,$lineStyle1);//媒体名称
            $section->addText(
                '发布商业广告'.$value2['ztccount'].'条次,时长'.$value2['zsccount'].'秒；涉嫌违法'.$value2['wftccount'].'条次，时长'.$value2['wfsccount'].'秒；公益广告'.$value2['gytccount'].'条次 时长'.$value2['gysccount'].'秒',$fontStyle4,$lineStyle1
            );//汇总

            //违法内容信息
            if($value2['wftccount']>0){
                $section->addText('涉嫌违法广告',$fontStyle3);//违法广告标头

                $where_hda['fmediaid'] = $value2['fmediaid'];
                $where_hda['fissuedate'] = strtotime($daytime);
                $where_hda['fillegaltypecode'] = 30;
                $do_hda = M('hzsj_dmp_ad')->field('identify,fsampleid,fadname,fmediaclassid,fexpressioncodes,fillegalcontent,flength,fstarttime')->where($where_hda)->select();
                $adarr = [];
                foreach ($do_hda as $key => $value){
                    $adarr[$value['fsampleid']]['fadname'] = $value['fadname'];
                    $adarr[$value['fsampleid']]['fmediaclassid'] = $value['fmediaclassid'];
                    $adarr[$value['fsampleid']]['flength'] = $value['flength'];
                    $adarr[$value['fsampleid']]['fexpressioncodes'] = $value['fexpressioncodes'];
                    $adarr[$value['fsampleid']]['fillegalcontent'] = $value['fillegalcontent'];
                    $adarr[$value['fsampleid']]['fstarttime'][] = date('H:i:s',$value['fstarttime']);
                }

                $num = 0;//序号
                foreach ($adarr as $key => $value) {
                    $num ++;
                    $section->addTextBreak();
                    $section->addText($num.'.'.$value['fadname'],null,$lineStyle1);//违法广告名称
                    $fstarttimes = '';
                    foreach ($value['fstarttime'] as $value3) {
                        $fstarttimes .= date('H:i:s',$value3).'   ';
                    }
                    if((int)$value['fmediaclassid'] == 1 || (int)$value['fmediaclassid'] == 2){
                        $section->addText('发布时间：'.$fstarttimes.'  时长:'.$value['flength'].'秒',null,$lineStyle1);
                    }elseif((int)$value['fmediaclassid'] == 3){
                        $do_view = M('tpapersample')
                            ->alias('a')
                            ->field('ifnull(a.fversion,"暂无") fversion,ifnull(b.fpage,"暂无") fpage')
                            ->join('tpaperissue b on a.fpapersampleid = b.fpapersampleid')
                            ->where(['fpapersampleid'=>$key])
                            ->find();
                        if(!empty($do_view)){
                            $section->addText('发布版面：'.$do_view['fpage'].'  版本:'.$do_view['fversion'],null,$lineStyle1);
                        }
                    }
                    $section->addText('涉嫌违法内容：'.$value['fillegalcontent'],null,$lineStyle1);
                    $section->addText('违法表现代码：'.$value['fexpressioncodes'],null,$lineStyle1);

                    //<<--------获取媒体15天的发布条次--------
                    $where_hda3['fmediaid'] = $value2['fmediaid'];
                    $where_hda3['fissuedate'] = ['between',[strtotime($daytime."-15 day"),strtotime($daytime)]];
                    $where_hda3['fillegaltypecode'] = 30;
                    $where_hda3['fsampleid'] = $key;
                    $do_hda3 = M('hzsj_dmp_ad')->field('fissuedate,count(*) acount')->where($where_hda3)->order('fissuedate asc')->group('fissuedate')->select();

                    $section->addText('近期发布情况(15天内发布情况)：',null,$lineStyle1);
                    //建15天发布情况表
                    $styleTable = array('borderSize'=>1, 'alignMent' => 'center','cellMarginTop'=>80, 'cellMarginLeft'=>0, 'cellMarginRight'=>0, 'cellMarginBottom'=>80);
                    $styleFirstRow = array('bgColor'=>'#ffffff');//首行背景色
                    $PHPWord->addTableStyle('tableStyle', $styleTable, $styleFirstRow);//将自定义样式写入组件内备用
                    $table = $section->addTable('tableStyle');//设置表格样式

                    $cellfontStyle = array('align'=>'center','color'=>'777777');
                    $cellStyle = array(
                        'valign'=>'center',
                        'borderTopSize'=>1,
                        'borderTopColor'=>'333333',
                        'borderLeftSize'=>1,
                        'borderLeftColor'=>'333333',
                        'borderBottomSize'=>1,
                        'borderBottomColor'=>'333333',
                        'borderRightSize'=>1,
                        'borderRightColor'=>'333333'
                    );
                    $table->addRow(100);
                    $table->addCell(2000,$cellStyle)->addText('日期',$cellfontStyle);

                    $wfdata = [];
                    foreach ($do_hda3 as $key => $value) {
                        $wfdata[date('d',$value['fissuedate'])] = $value['acount'];
                    }

                    for($i = 14;$i>=0;$i--){
                        $table->addCell(2000,$cellStyle)->addText(date('d',strtotime($daytime."-".$i." day")),$cellfontStyle);
                    }
                    $table->addRow(100);
                    $table->addCell(2000,$cellStyle)->addText('发布',$cellfontStyle);
                    for($i = 14;$i>=0;$i--){
                        $daydata = $wfdata[date('d',strtotime($daytime."-".$i." day"))]?$wfdata[date('d',strtotime($daytime."-".$i." day"))]:0;
                        $table->addCell(2000,$cellStyle)->addText($daydata,$cellfontStyle);
                    }
                }
               
            }

        }

        // 保存文件
        $objWriter = \PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
        $date = date('Ymdhis');
        $savefilename = 'FJ'.$date.'.docx';
        $savefile = './Public/Word/'.$savefilename;

        $objWriter->save($savefile);
        
        $pnname = $tredata['fname1'].date('Y',strtotime($daytime)).'年'.date('m',strtotime($daytime)).'月'.date('d',strtotime($daytime)).'日监测日报';

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$savefilename,$savefile,array('Content-Disposition' => 'attachment;filename='.$pnname.'.docx'));//上传云
        unlink($savefile);//删除文件

        $data['pnname']             = $pnname;
        $data['pntype']             = 10;
        $data['pnfiletype']         = 10;
        $data['pnstarttime']        = $daytime;
        $data['pnendtime']          = $daytime;
        $data['pncreatetime']       = date('Y-m-d H:i:s');
        $data['pnurl']              = $ret['url'];
        $data['pnfocus']            = $focus;
        $data['pntrepid']           = $tredata['fid'];
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
        }

    }

    public function create_weekpresentation(){
        $s_time = I('s_time');
        if(!empty($s_time)){
            $do_dt = M()->query('select weekofyear("'.$s_time.'") as dt');
            $week = date('Y',strtotime($s_time)).'第'.$do_dt[0]['dt'].'周';
        }else{
           $week = date('Y第W周');
        }
        $focus = I('focus',0);
        $this->create_weekpresentation2($week,$focus,session('regulatorpersonInfo.fregulatorpid'));
    }

    /**
     * 生成广告监测报告周报
     * by zw
     */
    public function create_weekpresentation3($week,$focus,$treid){
        session_write_close();
        $system_num = getconfig('system_num');
        header("Content-type: text/html; charset=utf-8");
        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);

        $tredata = M('tregulator')
            ->field('a.*,b.fname1')
            ->alias('a')
            ->join('tregion b on a.fregionid = b.fid')
            ->where('a.fid = '.$treid)
            ->find();

        $week = preg_replace('# #','',$week);
        $week_date = preg_match_all('/\d+/', $week, $temp);
        $weekday = $this->weekday($temp[0][0],$temp[0][1]);
        if($temp[0][1] < 10){
            $temp2 = '0'.$temp[0][1];
        }else{
            $temp2 = $temp[0][1];
        }

        $week_num = $temp[0][0].$temp2;
        $week_start = strtotime($weekday['start']);
        $week_end = strtotime($weekday['end']);//日期结束

        $where_ma['UNIX_TIMESTAMP(a.fdate)'] = array('between',array($week_start,$week_end));
        $tillegalad_mod = M('tbn_ad_summary_day')
            ->alias('a')
            ->field("
                 (case when a.fmedia_class_code='01' then '电视' when fmedia_class_code='02' then '广播' when fmedia_class_code='03' then '报纸' when fmedia_class_code='13' then '互联网' end) as fmedia_class_code, SUM(fad_times) AS fad_times, ifnull(c.fad_illegal_times,0) as fad_illegal_times, IFNULL(ROUND(c.fad_illegal_times/ SUM(fad_times)*100,2),0.00) as tcwfl,SUM(fad_play_len) AS fad_play_len, ifnull(c.fad_illegal_play_len,0) as fad_illegal_play_len, IFNULL(ROUND(c.fad_illegal_play_len/ SUM(fad_play_len)*100,2),0.00) as scwfl
            ")
            ->join('tregulatormedia ta on a.fmediaid=ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'"')
            ->join('tmedia on a.fmediaid = tmedia.fid and tmedia.main_media_id = tmedia.fid')
            ->join('(select fmedia_id,count(*) as fad_illegal_times, sum(TIMESTAMPDIFF(SECOND,fstarttime,fendtime)) as fad_illegal_play_len from tbn_illegal_ad_issue a where UNIX_TIMESTAMP(fissue_date) between '.$week_start.' AND '.$week_end.' group by fmedia_id) c on a.fmediaid=c.fmedia_id','left')
            ->where($where_ma)
            ->group('a.fmedia_class_code')
            ->select();

        $html = '<div style="margin:0 auto;text-align:center;">';
        $html.= '<div align="center"><p style="color:red;font-size:18px;">广告监测周报</p></div>';
        $html.= '<p align="center">'.$weekday['start'].'至'.$weekday['end'].'</p>';
        $html.= '<p>监测总体统计情况</p>';
        $html .= '  <table border="1" cellspacing="0" width="50%" height="150" style="margin-top:100px;margin: 0 auto;">
        <tbody>
        <tr>
            <th rowspan="2">媒体类别</th>
            <th colspan="3">广告条次</th>
            <th colspan="3">广告时间(秒)</th>
         </tr>
        <tr style="text-align: center;">
            <th>总条次</th>
            <th>涉嫌违法条次</th>
            <th>条次违法率名</th>
            <th>总时长</th>
            <th>违法时长</th>
            <th>时长违法率</th>
         </tr>';
        if(!empty($tillegalad_mod)){
            foreach ($tillegalad_mod as $key => $value){
                 $html .= "
                 <tr style='text-align: center;'>
                <td >".$value['fmedia_class_code']."</td>
                <td >".$value['fad_times']."</td>
                <td>".$value['fad_illegal_times']."</td>
                <td>".$value['tcwfl']."%</td>
                <td>".$value['fad_play_len']."</td>
                <td >".$value['fad_illegal_play_len']."</td>
                <td >".$value['scwfl']."%</td>
                </tr>";
            }
        }else{
            $html .= "<tr> <td style='text-align: center;' colspan='7'>暂无数据</td> </tr>";
        }
        $html .= '</tbody></table>';

        $where_ma2['UNIX_TIMESTAMP(a.fdate)'] = array('between',array($week_start,$week_end));
        $tillegalad_mod2 = M('tbn_ad_summary_day')
            ->alias('a')
            ->field("
                 (case when instr(b.fmedianame,'（') > 0 then left(b.fmedianame,instr(b.fmedianame,'（') -1) else b.fmedianame end) as fmedianame,(case when a.fmedia_class_code='01' then '电视' when fmedia_class_code='02' then '广播' when fmedia_class_code='03' then '报纸' when fmedia_class_code='13' then '互联网' end) as fmedia_class_code, SUM(fad_times) AS fad_times, ifnull(c.fad_illegal_times,0) as fad_illegal_times, IFNULL(ROUND(c.fad_illegal_times/ SUM(fad_times)*100,2),0.00) as tcwfl,SUM(fad_play_len) AS fad_play_len, ifnull(c.fad_illegal_play_len,0) as fad_illegal_play_len, IFNULL(ROUND(c.fad_illegal_play_len/ SUM(fad_play_len)*100,2),0.00) as scwfl
            ")
            ->join('tregulatormedia ta on a.fmediaid=ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'"')
            ->join('tmedia b on a.fmediaid = b.fid and b.main_media_id = b.fid')
            ->join('(select fmedia_id,count(*) as fad_illegal_times, sum(TIMESTAMPDIFF(SECOND,fstarttime,fendtime)) as fad_illegal_play_len from tbn_illegal_ad_issue a where UNIX_TIMESTAMP(fissue_date) between '.$week_start.' AND '.$week_end.' group by fmedia_id) c on a.fmediaid=c.fmedia_id','left')
            ->where($where_ma2)
            ->group('a.fmediaid')
            ->select();

        $html.= '<p>媒体发布涉嫌严重违法广告情况</p>';
        $html .= '
        <table border="1" cellspacing="0" width="70%" height="150" class="table" style="margin-top:100px;margin: 0 auto;">
        <tbody>
            <tr>
                <th rowspan="2">编号</th>
                <th rowspan="2">媒体名称</th>
                <th rowspan="2">媒体类别</th>
                <th colspan="3">广告条次</th>
                <th colspan="3">广告时间(秒)</th>
             </tr>
            <tr>
                <th>监测条次</th>
                <th>涉嫌违法条次</th>
                <th>条次违法率</th>
                <th>总时长</th>
                <th>违法时长</th>
                <th>时长违法率</th>
             </tr>';
        $num = 0;
        if(!empty($tillegalad_mod2)){
            foreach ($tillegalad_mod2 as $key2 =>$value2){
                $html .= '<tr style="text-align: center;">
                    <td >'.++$num.'</td>
                    <td>'.$value2['fmedianame'].'</td>
                    <td >'.$value2['fmedia_class_code'].'</td>
                    <td>'.$value2['fad_times'].'</td>
                    <td>'.$value2['fad_illegal_times'].'</td>
                    <td>'.$value2['tcwfl'].'%</td>
                    <td >'.$value2['fad_play_len'].'</td>
                    <td >'.$value2['fad_illegal_play_len'].'</td>
                    <td >'.$value2['scwfl'].'%</td>
                    </tr> ';
            }
         }else{
            $html .= '<tr style="text-align: center;"><td colspan="9">暂无数据</td></tr>';
         }
        $html .= '</tbody></table></div>';

        $date = date('Ymdhis');
        $savefilename = 'FJ'.$date.'.doc';
        $savefile = './Public/Word/'.$savefilename;

        $regulatorpname = $tredata['fname'];//机构名
        $pnname = $regulatorpname.$week.'周报';

        //将文档保存
        word_start();
        echo $html;
        word_save($savefile);
        ob_flush();//每次执行前刷新缓存
        flush();

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$savefilename,$savefile,array('Content-Disposition' => 'attachment;filename='.$pnname.'.doc'));//上传云
        unlink($savefile);//删除文件

         //将生成记录保存
       $data['pnname']          = $pnname;
       $data['pntype']          = 20;
       $data['pnfiletype']      = 10;
       $data['pnstarttime']     = $weekday['start'];
       $data['pnendtime']       = $weekday['end'];
       $data['pncreatetime']    = date('Y-m-d H:i:s');
       $data['pnurl']           = $ret['url'];
       $data['pnhtml']          = $html;
       $data['pnfocus']            = $focus;
       $data['pntrepid']           = $tredata['fid'];
       $data['fcustomer']  = $system_num;
       $do_tn = M('tpresentation')->add($data);
       if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
       }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
       }
    }

    /**
     * 生成广告监测报告周报(固定数据)
     * by zw
     */
    public function create_weekpresentation2($week,$focus,$treid){
        session_write_close();
        $system_num = getconfig('system_num');
        header("Content-type: text/html; charset=utf-8");
        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);

        $week = preg_replace('# #','',$week);
        $week_date = preg_match_all('/\d+/', $week, $temp);
        $weekday = $this->weekday($temp[0][0],$temp[0][1]);
        if($temp[0][1] < 10){
            $temp2 = '0'.$temp[0][1];
        }else{
            $temp2 = $temp[0][1];
        }

        $week_num = $temp[0][0].$temp2;
        $week_start = $weekday['start'];
        $week_end = $weekday['end'];//日期结束

        $tredata = M('tregulator')
            ->field('a.*,b.fname1')
            ->alias('a')
            ->join('tregion b on a.fregionid = b.fid')
            ->where('a.fid = '.$treid)
            ->find();

        $where_ma['tmedia.fstate'] = 1;
        $where_ma['_string'] = 'tmedia.main_media_id = tmedia.fid';
        $do_ma = M('tmedia')
            ->field('tmedia.fid,fmediaclassid, (case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')
            ->join('tregulatormedia ta on tmedia.fid=ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'"')
            ->where($where_ma)
            ->order('media_region_id asc,fmediaclassid asc')
            ->select();

        //所有媒体基础数据
        $mediaids = [];
        foreach ($do_ma as $key => $value) {
            $mediaids[] = $value['fid'];
            $mediadata[$value['fid']]['fmediaid'] = $value['fid'];
            $mediadata[$value['fid']]['fmediaclassid'] = $value['fmediaclassid'];
            $mediadata[$value['fid']]['fmedianame'] = $value['fmedianame'];
            $mediadata[$value['fid']]['ztccount'] = 0;
            $mediadata[$value['fid']]['zsccount'] = 0;
            $mediadata[$value['fid']]['wfztccount'] = 0;
            $mediadata[$value['fid']]['wfzsccount'] = 0;
        }

        //<<--------媒体类别统计数据输出--------
        $sqlstr = '
            select fmedia_class_code,sum(ztccount) ztccount,sum(zsccount) zsccount,sum(wftc) wfztccount,sum(wfsc) wfzsccount 
                from (
                    SELECT fmediaid,fdate,fmedia_class_code, SUM(fad_times_long_ad) ztccount, SUM(fad_play_len_long_ad) zsccount
                    from tbn_ad_summary_day_v3 
                    where fmediaid in ('.implode(',', $mediaids).') 
                    and fdate between "'.$week_start.'" and "'.$week_end.'" 
                    and tbn_ad_summary_day_v3.fcustomer = "'.$system_num.'"
                    GROUP BY fmediaid,fdate
                    ) a
                LEFT JOIN (
                    SELECT m.fmedia_id,n.fissue_date, COUNT(*) wftc, SUM(UNIX_TIMESTAMP(n.fendtime) - UNIX_TIMESTAMP(n.fstarttime)) wfsc
                    FROM tbn_illegal_ad m,tbn_illegal_ad_issue n
                    WHERE m.fid = n.fillegal_ad_id 
                    and m.fstatus3 not in(30,40)
                    and fissue_date BETWEEN "'.$week_start.'" and "'.$week_end.'" 
                    and m.fcustomer = '.$system_num.'
                    GROUP BY m.fmedia_id,n.fissue_date) b 
                ON a.fmediaid = b.fmedia_id and a.fdate = b.fissue_date 
            group by fmedia_class_code
        ';
        $do_data = M()->query($sqlstr);

        $html = '<div style="margin:0 auto;text-align:center;">';
        $html.= '<div align="center"><p style="color:red;font-size:18px;">广告监测周报</p></div>';
        $html.= '<p align="center">'.$weekday['start'].'至'.$weekday['end'].'</p>';
        $html.= '<p>监测总体统计情况</p>';
        $html .= '  <table border="1" cellspacing="0" width="50%" height="150" style="margin-top:100px;margin: 0 auto;">
        <tbody>
        <tr>
            <th rowspan="2">媒体类别</th>
            <th colspan="3">广告条次</th>
            <th colspan="3">广告时间(秒)</th>
         </tr>
        <tr style="text-align: center;">
            <th>总条次</th>
            <th>涉嫌违法条次</th>
            <th>条次违法率</th>
            <th>总时长</th>
            <th>违法时长</th>
            <th>时长违法率</th>
         </tr>';
        if(!empty($do_data)){
            foreach ($do_data as $key => $value){
                if($value['fmedia_class_code'] == 1){
                    $mediaclass = '电视';
                }elseif($value['fmedia_class_code'] == 2){
                    $mediaclass = '广播';
                }elseif($value['fmedia_class_code'] == 3){
                    $mediaclass = '报纸';
                }else{
                    $mediaclass = '';
                }
                $ztccount = $value['ztccount']?$value['ztccount']:0;
                $wfztccount = $value['wfztccount']?$value['wfztccount']:0;
                $tcwfl = ($wfztccount ==0 || $ztccount == 0)?'0.00':round($wfztccount/$ztccount,4)*100;
                $zsccount = $value['zsccount']?$value['zsccount']:0;
                $wfzsccount = $value['wfzsccount']?$value['wfzsccount']:0;
                $scwfl = ($zsccount==0 || $wfzsccount == 0)?'0.00':round($wfzsccount/$zsccount,4)*100;

                $html .= "
                <tr style='text-align: center;'>
                <td >".$mediaclass."</td>
                <td >".$ztccount."</td>
                <td>".$wfztccount."</td>
                <td>".$tcwfl."%</td>
                <td>".$zsccount."</td>
                <td >".$wfzsccount."</td>
                <td >".$scwfl."%</td>
                </tr>";
            }
        }else{
            $html .= "<tr> <td style='text-align: center;' colspan='7'>暂无数据</td> </tr>";
        }
        $html .= '</tbody></table>';
        //--------媒体类别统计数据输出-------->>

        //<<--------媒体统计数据输出--------
        $sqlstr = '
            select a.fmediaid,sum(ztccount) ztccount,sum(zsccount) zsccount,sum(wftc) wfztccount,sum(wfsc) wfzsccount 
                from (
                    SELECT fmediaid,fdate, SUM(fad_times_long_ad) ztccount, SUM(fad_play_len_long_ad) zsccount
                    from tbn_ad_summary_day_v3 
                    where fmediaid in ('.implode(',', $mediaids).') 
                    and fdate between "'.$week_start.'" and "'.$week_end.'" 
                    and tbn_ad_summary_day_v3.fcustomer = "'.$system_num.'"
                    GROUP BY fmediaid,fdate
                    ) a
                LEFT JOIN (
                    SELECT m.fmedia_id,n.fissue_date, COUNT(*) wftc, SUM(UNIX_TIMESTAMP(n.fendtime) - UNIX_TIMESTAMP(n.fstarttime)) wfsc
                    FROM tbn_illegal_ad m,tbn_illegal_ad_issue n
                    WHERE m.fid = n.fillegal_ad_id 
                    and m.fstatus3 not in(30,40) 
                    and fissue_date BETWEEN "'.$week_start.'" and "'.$week_end.'" 
                    and m.fcustomer = '.$system_num.'
                    GROUP BY m.fmedia_id,n.fissue_date) b 
                ON a.fmediaid = b.fmedia_id and a.fdate = b.fissue_date 
            group by a.fmediaid
        ';
        $do_data = M()->query($sqlstr);

        foreach ($do_data as $key => $value) {
            $mediadata[$value['fmediaid']]['ztccount'] = $value['ztccount'];
            $mediadata[$value['fmediaid']]['wfztccount'] = $value['wfztccount'];
            $mediadata[$value['fmediaid']]['zsccount'] = $value['zsccount'];
            $mediadata[$value['fmediaid']]['wfzsccount'] = $value['wfzsccount'];
        }
        $html.= '<p>媒体发布涉嫌严重违法广告情况</p>';
        $html .= '
        <table border="1" cellspacing="0" width="70%" height="150" class="table" style="margin-top:100px;margin: 0 auto;">
        <tbody>
            <tr>
                <th rowspan="2">编号</th>
                <th rowspan="2">媒体名称</th>
                <th rowspan="2">媒体类别</th>
                <th colspan="3">广告条次</th>
                <th colspan="3">广告时间(秒)</th>
             </tr>
            <tr>
                <th>监测条次</th>
                <th>涉嫌违法条次</th>
                <th>条次违法率</th>
                <th>总时长</th>
                <th>违法时长</th>
                <th>时长违法率</th>
             </tr>';
        $num = 0;
        if(!empty($mediadata)){
            foreach ($mediadata as $key =>$value){
                $fmediaclassid = (int)substr($value['fmediaclassid'],0,2);
                if($fmediaclassid == 1){
                    $mediaclass = '电视';
                }elseif($fmediaclassid == 2){
                    $mediaclass = '广播';
                }elseif($fmediaclassid == 3){
                    $mediaclass = '报纸';
                }else{
                    $mediaclass = '';
                }

                $ztccount = $value['ztccount']?$value['ztccount']:0;
                $wfztccount = $value['wfztccount']?$value['wfztccount']:0;
                $tcwfl = ($wfztccount ==0 || $ztccount == 0)?'0.00':round($wfztccount/$ztccount*100,2);
                $zsccount = $value['zsccount']?$value['zsccount']:0;
                $wfzsccount = $value['wfzsccount']?$value['wfzsccount']:0;
                $scwfl = ($zsccount==0 || $wfzsccount == 0)?'0.00':round($wfzsccount/$zsccount*100,2);

                $html .= '<tr style="text-align: center;">
                    <td >'.++$num.'</td>
                    <td>'.$value['fmedianame'].'</td>
                    <td >'.$mediaclass.'</td>
                    <td>'.$ztccount.'</td>
                    <td>'.$wfztccount.'</td>
                    <td>'.$tcwfl.'%</td>
                    <td >'.$zsccount.'</td>
                    <td >'.$wfzsccount.'</td>
                    <td >'.$scwfl.'%</td>
                    </tr> ';
            }
         }else{
            $html .= '<tr style="text-align: center;"><td colspan="9">暂无数据</td></tr>';
         }
        $html .= '</tbody></table></div>';
        //--------媒体统计数据输出-------->>
        
        $date = date('Ymdhis');
        $savefilename = 'FJ'.$date.'.doc';
        $savefile = './Public/Word/'.$savefilename;

        $regulatorpname = $tredata['fname'];//机构名
        $pnname = $regulatorpname.$week.'周报';

        //将文档保存
        word_start();
        echo $html;
        word_save($savefile);
        ob_flush();//每次执行前刷新缓存
        flush();

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$savefilename,$savefile,array('Content-Disposition' => 'attachment;filename='.$pnname.'.doc'));//上传云
        unlink($savefile);//删除文件

         //将生成记录保存
       $data['pnname']          = $pnname;
       $data['pntype']          = 20;
       $data['pnfiletype']      = 10;
       $data['pnstarttime']     = $weekday['start'];
       $data['pnendtime']       = $weekday['end'];
       $data['pncreatetime']    = date('Y-m-d H:i:s');
       $data['pnurl']           = $ret['url'];
       $data['pnhtml']          = $html;
       $data['pnfocus']            = $focus;
       $data['pntrepid']           = $tredata['fid'];
       $data['fcustomer']  = $system_num;
       $do_tn = M('tpresentation')->add($data);
       if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
       }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
       }
    }

    /**
     * 生成广告监测报告月报
     * by zw
     */
    public function create_monthpresentation(){
        $date = I('daytime',date('Y-m-d'));
        $focus = I('focus',0);

        $this->public_presentation2($date,$focus,session('regulatorpersonInfo.fregulatorpid'),'month');
    }

    /**
     * 生成广告监测报告季报
     * by zw
     */
    public function create_seasonpresentation(){
        $date = I('daytime',date('Y-m-d'));
        $focus = I('focus',0);

        $this->public_presentation2($date,$focus,session('regulatorpersonInfo.fregulatorpid'),'season');
    }

     /**
     * 生成广告监测报告半年报
     * by zw
     */
    public function create_halfyearpresentation(){
        $date = I('daytime',date('Y-m-d'));
        $focus = I('focus',0);

        $this->public_presentation2($date,$focus,session('regulatorpersonInfo.fregulatorpid'),'halfyear');
    }

    /**
     * 生成广告监测报告年报
     * by zw
     */
    public function create_yearpresentation(){
        $date = I('daytime',date('Y-m-d'));
        $focus = I('focus',0);

        $this->public_presentation2($date,$focus,session('regulatorpersonInfo.fregulatorpid'),'year');
    }

    /**
     * 生成广告监测报告自定义报（只适用固定数据）
     * by zw
     */
    public function create_custompresentation(){
        $date = I('daytime');
        $focus = I('focus',0);

        $this->public_presentation2($date,$focus,session('regulatorpersonInfo.fregulatorpid'),'custom');
    }

    public static function weekday($year,$week) {
        $year_start = mktime(0,0,0,1,1,$year);
        $year_end = mktime(0,0,0,12,31,$year);

        $start = $year_start;//把第一天做为第一周的开始
        // 判断第一天是否为第一周的开始
        if (intval(date('w',$year_start))==1){
            $week++;
        }
        if ($week==1){
            $weekday['start'] = date('Y-m-d',$start);
        }else{
            $weekday['start'] = date('Y-m-d',strtotime('+'.($week-1).' monday',$start));
        }// 第几周的结束时间
        $weekday['end'] = date('Y-m-d',strtotime('+1 sunday',strtotime($weekday['start'])));
        if (date('Y',strtotime($weekday['end']))!=$year){
            $weekday['end'] = date('Y-m-d',$year_end);
        }
        return $weekday;
    }

    public function is_have_table($table_name){
        $isTable = M()->query('SHOW TABLES LIKE "'.$table_name.'"');
        if(empty($isTable)){
            return true;
        }else{
            return false;
        }
    }

    /*******文件生成类方法结束*****/
    //获取月份差
    function get_month_diff( $date1, $date2, $tags='-' ){
        $date1 = explode($tags,$date1);
        $date2 = explode($tags,$date2);
        return ($date2[0] - $date1[0]) * 12 + ($date2[1] - $date1[1]);
    }

    //获取天数差
    function get_day_diff($a,$b){
     $a_dt=getdate($a);
     $b_dt=getdate($b);
     $a_new=mktime(12,0,0,$a_dt['mon'],$a_dt['mday'],$a_dt['year']);
     $b_new=mktime(12,0,0,$b_dt['mon'],$b_dt['mday'],$b_dt['year']);
     return round(abs($a_new-$b_new)/86400);
    }

    /**
     * 月报、季报、半年报、年报公共部分生成（实时数据）
     * by zw
     */
    public function public_presentation($date,$focus,$treid,$types){
        session_write_close();
        $system_num = getconfig('system_num');
        header("Content-type: text/html; charset=utf-8");
        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);

        $mediaclass = ['01' => '电视','02' => '广播','03' => '报纸'];//包括的媒体类别
        $nowtime_arr = explode("-" ,$date);//传入时间分段
        $px_arr = ['↑','↓'];

        $tredata = M('tregulator')
            ->field('a.*,b.fname1')
            ->alias('a')
            ->join('tregion b on a.fregionid = b.fid')
            ->where('a.fid = '.$treid)
            ->find();
        $regulatorpname = $tredata['fname'];//机构名

        //配置文档基础项
        if($types == 'month'){
            $season = $nowtime_arr[1];//第几月
            $nowstarttime = date('Y-m-01',mktime(0,0,0,($season - 1) *1 +1,1,date('Y',strtotime($date))));//本环开始时间
            $nowendtime = date('Y-m-t',mktime(0,0,0,$season * 1,1,date('Y',strtotime($date))));//本环结束时间
            $prevstarttime = date('Y-m-01',mktime(0,0,0,($season - 2) *1 +1,1,date('Y',strtotime($date))));//上环开始时间
            $prevendtime = date('Y-m-t',mktime(0,0,0,($season - 1) * 1,1,date('Y',strtotime($date))));// 上环结束时间
            $monthcount = $this->get_month_diff($nowstarttime,$nowendtime,'-');//月份差
            $stitle = '月';
            $date1 = date('Y年',strtotime($date)).$season.$stitle;
            $pntype = 30;//报告类型
            $reportname = $regulatorpname.date('Y年',strtotime($date)).$season.'月份月报';//报告名称
        }elseif($types == 'season'){
            $season = ceil((date('n',strtotime($date)))/3);//第几季度
            $nowstarttime = date('Y-m-01',mktime(0,0,0,($season - 1) *3 +1,1,date('Y',strtotime($date))));//本环开始时间
            $nowendtime = date('Y-m-t',mktime(0,0,0,$season * 3,1,date('Y',strtotime($date))));//本环结束时间
            $prevstarttime = date('Y-m-01',mktime(0,0,0,($season - 2) *3 +1,1,date('Y',strtotime($date))));//上环开始时间
            $prevendtime = date('Y-m-t',mktime(0,0,0,($season - 1) * 3,1,date('Y',strtotime($date))));// 上环结束时间
            $monthcount = $this->get_month_diff($nowstarttime,$nowendtime,'-');//月份差
            $stitle = '季度';
            $date1 = date('Y年',strtotime($date)).'第'.$season.$stitle;
            $pntype = 70;//报告类型
            $reportname = $regulatorpname.date('Y年',strtotime($date)).'第'.$season.'季度季报';//报告名称
        }elseif($types == 'halfyear'){
            $season = $nowtime_arr[1]<7?1:2;//1上半年/2下半年
            $nowstarttime = date('Y-m-01',mktime(0,0,0,($season - 1) *6 +1,1,date('Y',strtotime($date))));//本环开始时间
            $nowendtime = date('Y-m-t',mktime(0,0,0,$season * 6,1,date('Y',strtotime($date))));//本环结束时间
            $prevstarttime = date('Y-m-01',mktime(0,0,0,($season - 2) *6 +1,1,date('Y',strtotime($date))));//上环开始时间
            $prevendtime = date('Y-m-t',mktime(0,0,0,($season - 1) * 6,1,date('Y',strtotime($date))));// 上环结束时间
            $monthcount = $this->get_month_diff($nowstarttime,$nowendtime,'-');//月份差
            $stitle = $nowtime_arr[1]<7?'上半年':'下半年';
            $date1 = date('Y年',strtotime($date)).$stitle;
            $pntype = 75;//报告类型
            $reportname = $regulatorpname.date('Y年',strtotime($date)).$stitle.'半年报';//报告名称
        }elseif($types == 'year'){
            $season = 1;//第几期
            $nowstarttime = date('Y-01-01',strtotime($date));//本环开始时间
            $nowendtime = date('Y-12-31',strtotime($date));//本环结束时间
            $prevstarttime = date('Y-01-01',strtotime($date.'-12 month'));//上环开始时间
            $prevendtime = date('Y-12-31',strtotime($date.'-12 month'));// 上环结束时间
            $monthcount = 12;//月份差
            $stitle = '年';
            $date1 = date('Y年',strtotime($date));
            $pntype = 80;//报告类型
            $reportname = $regulatorpname.date('Y年',strtotime($date)).'年报';//报告名称
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
        }
        
        $html = '<div style="margin:0 auto;"> <style>.tb tr td{ background:#ffffff; text-align:center; border-bottom:1px solid #333333;border-right:1px solid #333333;}</style>';
        $html.= '<div align="center"><p style="color:red;font-size:30px;">'.$regulatorpname.'</p></div>';
        $html.= '<div align="center"><p style="color:red;font-size:18px;">广告监测报告</p></div>';
        $html.= '<p align="center" style="color:red;font-size:12px;">（'.$nowtime_arr[0].'年第'.$season.'期）</p>';
        $html.= '<p align="right">'.$date1.'</p>';
        $html.= '<p>一、广告监测情况综述</p>';

        //所有媒体
        $mediares = M('tmedia')
            ->alias('t')
            ->field('t.fid,t.fmediaownerid,t.fmediaclassid, (case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,tw.fregionid')
            ->join('tmediaowner as tw on t.fmediaownerid = tw.fid')
            ->join('tregulatormedia ta on t.fid=ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'"')
            ->where('t.fid = t.main_media_id')
            ->select();

        //媒体按行政区划分组
        $allmediaids = [];
        $alltvmediaids = [];
        $allbcmediaids = [];
        $allpapermediaids = [];
        $mediadata['国家级']['meidas'] = [];
        $mediadata['省属']['meidas'] = [];
        $mediadata['市属']['meidas'] = [];
        $mediadata['县属']['meidas'] = [];
        foreach($mediares as $val){
            $allmediaids[] = $val['fid'];
            if(substr($val['fmediaclassid'],0,2) == '01'){
                $alltvmediaids[] = $val['fid'];
            }elseif(substr($val['fmediaclassid'],0,2) == '02'){
                $allbcmediaids[] = $val['fid'];
            }elseif(substr($val['fmediaclassid'],0,2) == '03'){
                $allpapermediaids[] = $val['fid'];
            }
            $tregion_len = get_tregionlevel($val['fregionid']);
            if($tregion_len == 1){//国家级
                $mediadata['国家级']['meidas'][] = $val;
                $mediadata['国家级']['meidastr'] .= $mediadata['国家级']['meidastr']?'、'.$val['fmedianame']:$val['fmedianame'];
            }elseif($tregion_len == 2){//省级
                $mediadata['省属']['meidas'][] = $val;
                $mediadata['省属']['meidastr'] .= $mediadata['省属']['meidastr']?'、'.$val['fmedianame']:$val['fmedianame'];
            }elseif($tregion_len == 4){//市级
                $mediadata['市属']['meidas'][] = $val;
                $mediadata['市属']['meidastr'] .= $mediadata['市属']['meidastr']?'、'.$val['fmedianame']:$val['fmedianame'];
            }elseif($tregion_len == 6){//县级
                $mediadata['县属']['meidas'][] = $val;
                $mediadata['县属']['meidastr'] .= $mediadata['县属']['meidastr']?'、'.$val['fmedianame']:$val['fmedianame'];
            }
        }
        if(empty($allmediaids)){
            $allmediaids[] = 0;
        }

        $str = '';
        foreach ($mediadata as $key => $value) {
            if(!empty($value['meidas'])){
                $str .= $str?'，'.$key.'媒体'.count($mediadata[$key]['meidas']).'家（'.$mediadata[$key]['meidastr'].'）':$key.'媒体'.count($mediadata[$key]['meidas']).'家（'.$mediadata[$key]['meidastr'].'）';
            }
        }

        $html.= '<p> 广告监测中心'.($types == 'halfyear'?$stitle:'本'.$stitle).'共监测'.count($mediares).'家媒体。其中'.$str.'。 </p>';

        //本环记录总数
        $sqlstr1_1 = '
            select sum(fad_times) as zong_fad_times 
            from tbn_ad_summary_day a
            inner join tregulatormedia ta on a.fmediaid = ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'" 
            inner join tmedia b on a.fmediaid = b.fid and b.fid = b.main_media_id
            where fdate between "'.$nowstarttime.'" and "'.$nowendtime.'" 
        ';
        $do_data1_1 = M()->query($sqlstr1_1)[0]['zong_fad_times'];

        //上环记录总数
        $sqlstr1_2 = '
            select sum(fad_times) as zong_fad_times 
            from tbn_ad_summary_day a
            inner join tregulatormedia ta on a.fmediaid = ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'" 
            inner join tmedia b on a.fmediaid = b.fid and b.fid = b.main_media_id
            where fdate between "'.$prevstarttime.'" and "'.$prevendtime.'" 
        ';
        $do_data1_2 = M()->query($sqlstr1_2)[0]['zong_fad_times'];

        //本环违法记录总条次数
        $sqlstr2_1 = '
            select count(*) as zong_wf_fad_times,sum(case when c.fmedia_class = 1 or c.fmedia_class = 2 then (UNIX_TIMESTAMP(fendtime)-UNIX_TIMESTAMP(fstarttime)) else 0 end) as zong_fad_lenth  
            from tbn_illegal_ad_issue a
            inner join tregulatormedia ta on a.fmedia_id = ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'" 
            inner join tmedia b on a.fmedia_id = b.fid and b.fid = b.main_media_id
            inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
            where a.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" 
        ';
        $do_data2_1 = M()->query($sqlstr2_1)[0];

        //本环违法记录总条次数
        $sqlstr2_2 = '
            select count(*) as zong_wf_fad_times,sum(case when c.fmedia_class = 1 or c.fmedia_class = 2 then (UNIX_TIMESTAMP(fendtime)-UNIX_TIMESTAMP(fstarttime)) else 0 end) as zong_fad_lenth 
            from tbn_illegal_ad_issue a
            inner join tregulatormedia ta on a.fmedia_id = ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'" 
            inner join tmedia b on a.fmedia_id = b.fid and b.fid = b.main_media_id
            inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
            where a.fissue_date between "'.$prevstarttime.'" and "'.$prevendtime.'" 
        ';
        $do_data2_2 = M()->query($sqlstr2_2)[0];

        //总数环比
        $hb_ = $do_data1_1-$do_data1_2;
        if($hb_==0){
            $hb_str='无变化';
        }elseif($hb_>0){
            $hb_str='上升'.$hb_.'条次';
        }else{
            $hb_str='下降'.abs($hb_).'条次';
        }

        //违法率环比
        $hb_illegality = round(($do_data2_1['zong_wf_fad_times']/$do_data1_1)*100,2) - round(($do_data2_2['zong_wf_fad_times']/$do_data1_2)*100,2);
        if($hb_illegality==0){
            $hb_illegality_str='无变化';
        }elseif($hb_illegality>0){
            $hb_illegality_str='上升'.$hb_illegality.'个百分点';
        }else{
            $hb_illegality_str='下降'.abs($hb_illegality).'个百分点';
        }

        $html.= '<p>'.($types == 'halfyear'?$stitle:'本'.$stitle).'，监测中心监测媒体发布的各类广告'.$do_data1_1.'条次，环比'.$hb_str.'，其中涉嫌违法广告'.$do_data2_1['zong_wf_fad_times'].'条次，违法时长'.$do_data2_1['zong_fad_lenth'].'秒，监测违法率为'.round(($do_data2_1['zong_wf_fad_times']/$do_data1_1)*100,2).'%，环比'.$hb_illegality_str.'。</p>';

        //----------------------------分割线-----------------------

        $html.= '<h4>(一)分类监测情况</h4>';
        foreach ($mediadata as $mdatakey => $mdataval) {
            if(count($mdataval['meidas'])>0){
                $html .= ' 
                    <p align="center" border="1" style="font-size:18px; font-weight:bold; padding:10px;">'.$mdatakey.'媒体</p> 
                    <table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
                    <tr> 
                     <td>媒体类别</td> 
                     <td>监测条次</td> 
                     <td>环比</td> 
                     <td>涉嫌违法条次</td> 
                     <td>环比</td> 
                     <td>违法率</td> 
                     <td>环比</td> 
                    </tr>';

                $medias4 = [];
                $tvmedia = [];
                $bcmedia = [];
                $papermedia = [];
                foreach($mdataval['meidas'] as $v){
                    $medias4[] = $v['fid'];
                    if(substr($v['fmediaclassid'],0,2) == '01'){
                        $tvmedia[] = $v['fid'];
                    }elseif(substr($v['fmediaclassid'],0,2) == '02'){
                        $bcmedia[] = $v['fid'];
                    }elseif(substr($v['fmediaclassid'],0,2) == '03'){
                        $papermedia[] = $v['fid'];
                    }
                }

                //本环按媒体类别统计数据
                $sqlstr3_1 = '
                    select sum(fad_times) as mt_fad_times,fmedia_class_code 
                    from tbn_ad_summary_day a
                    where fdate between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmediaid in ('.join(',',(empty($medias4)?[0]:$medias4)).')
                    group by fmedia_class_code
                    order by fmedia_class_code
                ';
                $do_data3_1 = M()->query($sqlstr3_1);
                foreach ($do_data3_1 as $key => $value) {
                    $now_mt_data[$value['fmedia_class_code']]['mt_fad_times'] = $value['mt_fad_times'];
                    $now_mt_data[$value['fmedia_class_code']]['mt_fad_lenth'] = 0;
                    $now_mt_data[$value['fmedia_class_code']]['mt_wf_fad_times'] = 0;
                    $mt_data[$value['fmedia_class_code']]['now_mt_fad_times'] += $value['mt_fad_times'];
                }

                //上环按媒体类别统计数据
                $sqlstr3_2 = '
                    select sum(fad_times) as mt_fad_times,fmedia_class_code 
                    from tbn_ad_summary_day a
                    where fdate between "'.$prevstarttime.'" and "'.$prevendtime.'" and a.fmediaid in ('.join(',',(empty($medias4)?[0]:$medias4)).')
                    group by fmedia_class_code
                    order by fmedia_class_code
                ';
                $do_data3_2 = M()->query($sqlstr3_2);
                foreach ($do_data3_2 as $key => $value) {
                    $prev_mt_data[$value['fmedia_class_code']]['mt_fad_times'] = $value['mt_fad_times'];
                    $prev_mt_data[$value['fmedia_class_code']]['mt_fad_lenth'] = 0;
                    $prev_mt_data[$value['fmedia_class_code']]['mt_wf_fad_times'] = 0;
                    $mt_data[$value['fmedia_class_code']]['prev_mt_fad_times'] += $value['mt_fad_times'];
                }

                //本环违法记录总条次数
                $sqlstr4_1 = '
                    select count(*) as mt_wf_fad_times,sum(case when c.fmedia_class = 1 or c.fmedia_class = 2 then (UNIX_TIMESTAMP(fendtime)-UNIX_TIMESTAMP(fstarttime)) else 0 end) as mt_fad_lenth,(case when length(c.fmedia_class)<2 then concat("0",c.fmedia_class) else c.fmedia_class end) as fmedia_class
                    from tbn_illegal_ad_issue a
                    inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
                    where a.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmedia_id in ('.join(',',(empty($medias4)?[0]:$medias4)).')
                    group by c.fmedia_class
                    order by c.fmedia_class
                ';
                $do_data4_1 = M()->query($sqlstr4_1);
                foreach ($do_data4_1 as $key => $value) {
                    $now_mt_data[$value['fmedia_class']]['mt_fad_lenth'] = $value['mt_fad_lenth'];
                    $now_mt_data[$value['fmedia_class']]['mt_wf_fad_times'] = $value['mt_wf_fad_times'];
                    $mt_data[$value['fmedia_class']]['now_mt_wf_fad_times'] += $value['mt_wf_fad_times'];
                }

                //本环违法记录总条次数
                $sqlstr4_2 = '
                    select count(*) as mt_wf_fad_times,sum(case when c.fmedia_class = 1 or c.fmedia_class = 2 then (UNIX_TIMESTAMP(fendtime)-UNIX_TIMESTAMP(fstarttime)) else 0 end) as mt_fad_lenth,(case when length(c.fmedia_class)<2 then concat("0",c.fmedia_class) else c.fmedia_class end) as fmedia_class
                    from tbn_illegal_ad_issue a
                    inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
                    where a.fissue_date between "'.$prevstarttime.'" and "'.$prevendtime.'" and a.fmedia_id in ('.join(',',(empty($medias4)?[0]:$medias4)).')
                    group by c.fmedia_class
                    order by c.fmedia_class
                ';
                $do_data4_2 = M()->query($sqlstr4_2);
                foreach ($do_data4_2 as $key => $value) {
                    $prev_mt_data[$value['fmedia_class']]['mt_fad_lenth'] = $value['mt_fad_lenth'];
                    $prev_mt_data[$value['fmedia_class']]['mt_wf_fad_times'] = $value['mt_wf_fad_times'];
                    $mt_data[$value['fmedia_class']]['prev_mt_wf_fad_times'] += $value['mt_wf_fad_times'];
                }

                //按媒体类别展示数据
                foreach ($mediaclass as $key => $value) {
                    if(!empty($now_mt_data[$key]['mt_fad_times'])){
                        $html .= '<tr>';
                        $html .= '<td>'.$value.'</td>';
                        $html .= '<td>'.$now_mt_data[$key]['mt_fad_times'].'</td>';

                        if(!empty($prev_mt_data[$key]['mt_fad_times'])){
                            $hb_total = $now_mt_data[$key]['mt_fad_times'] - $prev_mt_data[$key]['mt_fad_times'];
                        }else{
                            $hb_total = 0;
                        }
                        if($hb_total==0){
                            $hb_total = '无变化';
                        }elseif ($hb_total>0) {
                            $hb_total = $hb_total.$px_arr[0];
                        }else{
                            $hb_total = abs($hb_total).$px_arr[1];
                        }

                        $html .= '<td>'.$hb_total.'</td>';
                        $html .= '<td>'.$now_mt_data[$key]['mt_wf_fad_times'].'</td>';

                        if(!empty($prev_mt_data[$key]['mt_fad_times'])){
                            $hb_wfggsl = $now_mt_data[$key]['mt_wf_fad_times'] - $prev_mt_data[$key]['mt_wf_fad_times'];
                        }else{
                            $hb_wfggsl = 0;
                        }

                        $html .= '<td>'.(($hb_wfggsl==0)?'无变化':($hb_wfggsl>0?$hb_wfggsl.$px_arr[0]:abs($hb_wfggsl).$px_arr[1])).'</td>';
                        $html .= '<td>'.round($now_mt_data[$key]['mt_wf_fad_times']/$now_mt_data[$key]['mt_fad_times']*100,2).'%</td>';

                        if(!empty($prev_mt_data[$key]['mt_fad_times'])){
                            $hb_ggslwfl = round(($now_mt_data[$key]['mt_wf_fad_times']/$now_mt_data[$key]['mt_fad_times'])*100,2) - round(($prev_mt_data[$key]['mt_wf_fad_times']/$prev_mt_data[$key]['mt_fad_times'])*100,2);
                        }else{
                            $hb_ggslwfl = 0;
                        }
                        if($hb_ggslwfl==0){
                            $hb_ggslwfl = '无变化';
                        }elseif ($hb_ggslwfl>0) {
                            $hb_ggslwfl = $hb_ggslwfl.'%'.$px_arr[0];
                        }else{
                            $hb_ggslwfl = abs($hb_ggslwfl).'%'.$px_arr[1];
                        }

                        $html .= '<td>'.$hb_ggslwfl.'</td></tr>';
                    }
                }
                $html .= '</table>';

                //清空数据
                unset($now_mt_data);
                unset($prev_mt_data);

                //---------------------------

                $html .= '<p>媒体：</p>';
                $html .= ' 
                    <table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
                    <tr> 
                     <td>序号</td> 
                     <td>媒体名称</td> 
                     <td>监测条次</td> 
                     <td>总时长</td> 
                     <td>涉嫌违法条次</td> 
                     <td>违法时长</td> 
                     <td>监测违法率</td>
                    </tr>';

                $adv_totals1 = 0;
                $illegal_total1 = 0;
                $num = 0;

                //本环按媒体汇总
                $sqlstr5_1 = '
                    select (case when instr(b.fmedianame,"（") > 0 then left(b.fmedianame,instr(b.fmedianame,"（") -1) else b.fmedianame end) as fmedianame,sum(fad_times) as m_fad_times,sum(fad_play_len) as m_fad_lenth,ifnull(m_ad_issuecount,0) as m_ad_issuecount,ifnull(m_ad_fad_lenth,0) as m_ad_fad_lenth,ifnull(m_ad_issuecount,0)/sum(fad_times) as jcwfl
                    from tbn_ad_summary_day a
                    inner join tregulatormedia ta on a.fmediaid = ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'" 
                    inner join tmedia b on a.fmediaid = b.fid and b.fid = b.main_media_id
                    left join (select a1.fmedia_id,count(1) as m_ad_issuecount,sum(case when c1.fmedia_class = 1 or c1.fmedia_class = 2 then (UNIX_TIMESTAMP(fendtime)-UNIX_TIMESTAMP(fstarttime)) else 0 end) as m_ad_fad_lenth from tbn_illegal_ad_issue a1 inner join tbn_illegal_ad c1 on a1.fillegal_ad_id = c1.fid where a1.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" and a1.fmedia_id in ('.join(',',(empty($medias4)?[0]:$medias4)).') group by a1.fmedia_id) c on c.fmedia_id = a.fmediaid
                    where fdate between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmediaid in ('.join(',',(empty($medias4)?[0]:$medias4)).')
                    group by a.fmediaid
                    order by jcwfl desc,b.fmediaclassid asc,b.media_region_id asc
                ';
                $do_data5_1 = M()->query($sqlstr5_1);

                $m_fad_times = 0;
                $m_fad_lenth = 0;
                $m_ad_issuecount = 0;
                $m_ad_fad_lenth = 0;
                foreach($do_data5_1 as $mlist1_key => $value){
                    $m_fad_times += $value['m_fad_times'];
                    $m_fad_lenth += $value['m_fad_lenth'];
                    $m_ad_issuecount += $value['m_ad_issuecount'];
                    $m_ad_fad_lenth += $value['m_ad_fad_lenth'];
                    $num++;
                    $html .= '<tr>';
                    $html .= '<td>'.$num.'</td>';
                    $html .= '<td>'.$value['fmedianame'].'</td>';
                    $html .= '<td>'.$value['m_fad_times'].'</td>';
                    $html .= '<td>'.$value['m_fad_lenth'].'</td>';
                    $html .= '<td>'.$value['m_ad_issuecount'].'</td>';
                    $html .= '<td>'.$value['m_ad_fad_lenth'].'</td>';
                    $html .= '<td>'.round(($value['m_ad_issuecount']/$value['m_fad_times'])*100,2).'%</td></tr>';
                }
                $num++;
                $html .= '<tr>';
                $html .= '<td>'.$num.'</td>';
                $html .= '<td>小计</td>';
                $html .= '<td>'.$m_fad_times.'</td>';
                $html .= '<td>'.$m_fad_lenth.'</td>';
                $html .= '<td>'.$m_ad_issuecount.'</td>';
                $html .= '<td>'.$m_ad_fad_lenth.'</td>';
                $html .= '<td>'.(round(($m_ad_issuecount/$m_fad_times),4)*100).'%</td></tr>';
                $html .= '</table>';

                for($i = 0; $i<=$monthcount; $i++){
                    $now_date = $nowtime_arr[0].$nowtime_arr[1].'_'.$regionid;
                    $istable = $this->is_have_table('ttvissue_'.$now_date);
                    if(empty($istable)){//判断是否有表
                        if(!empty($tvmedia)){
                            //电视媒体公益广告发布情况
                            $sqlstr6_1 = 'SELECT
                                    count(
                                    CASE
                                    WHEN FROM_UNIXTIME(tv.fstarttime, "%H:%i:%S") >= "19:00:00"
                                    AND FROM_UNIXTIME(tv.fstarttime, "%H:%i:%S") <= "21:00:00" THEN
                                        1
                                    ELSE
                                        NULL
                                    END
                                ) AS gyhjsl,
                                count(*) - count(
                                    CASE
                                    WHEN FROM_UNIXTIME(tv.fstarttime, "%H:%i:%S") >= "19:00:00"
                                    AND FROM_UNIXTIME(tv.fstarttime, "%H:%i:%S") <= "21:00:00" THEN
                                        1
                                    ELSE
                                        NULL
                                    END
                                ) AS othersl,
                                 (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as tmname
                                FROM
                                ttvsample as ts
                                JOIN ttvissue_'.$now_date.' AS tv ON tv.fsampleid = ts.fid
                                JOIN tmedia AS tm ON tm.fid = ts.fmediaid  and tm.fid=tm.main_media_id
                                JOIN tad ON tad.fadid = ts.fadid  and tad.fadid<>0
                                WHERE
                                tad.fadclasscode = 2202
                                AND tv.fmediaid IN ('.join(',',(empty($tvmedia)?[0]:$tvmedia)).')
                                group by tv.fmediaid';
                            $do_data6_1 = M()->query($sqlstr6_1);
                            foreach ($do_data6_1 as $key => $value) {
                                if(!empty($do_data6['电视'][$value['tmname']])){
                                    $do_data6['电视'][$value['tmname']]['gyhjsl'] += $value['gyhjsl'];
                                    $do_data6['电视'][$value['tmname']]['othersl'] += $value['othersl'];
                                }else{
                                    $do_data6['电视'][$value['tmname']]['gyhjsl'] = $value['gyhjsl'];
                                    $do_data6['电视'][$value['tmname']]['othersl'] = $value['othersl'];
                                }
                            }
                        }

                        if(!empty($bcmedia)){
                            //广播媒体公益广告发布情况
                            $sqlstr6_2 = 'SELECT
                                    count(
                                    CASE
                                    WHEN FROM_UNIXTIME(bc.fstarttime, "%H:%i:%S") >= "11:00:00"
                                    AND FROM_UNIXTIME(bc.fstarttime, "%H:%i:%S") <= "13:00:00" THEN
                                        1
                                    ELSE
                                        NULL
                                    END
                                ) AS gyhjsl,
                                count(*) - count(
                                    CASE
                                    WHEN FROM_UNIXTIME(bc.fstarttime, "%H:%i:%S") >= "11:00:00"
                                    AND FROM_UNIXTIME(bc.fstarttime, "%H:%i:%S") <= "13:00:00" THEN
                                        1
                                    ELSE
                                        NULL
                                    END
                                ) AS othersl,
                                 (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as tmname
                                FROM
                                tbcsample as tb
                                JOIN tbcissue_'.$now_date.' AS bc ON bc.fsampleid = tb.fid
                                JOIN tmedia AS tm ON tm.fid = tb.fmediaid and tm.fid=tm.main_media_id
                                JOIN tad ON tad.fadid = tb.fadid  and tad.fadid<>0
                                WHERE
                                tad.fadclasscode = 2202 
                                AND bc.fmediaid IN ('.join(',',(empty($bcmedia)?[0]:$bcmedia)).')
                                group by bc.fmediaid';
                            $do_data6_2 = M()->query($sqlstr6_2);
                            foreach ($do_data6_2 as $key => $value) {
                                if(!empty($do_data6['广播'][$value['tmname']])){
                                    $do_data6['广播'][$value['tmname']]['gyhjsl'] += $value['gyhjsl'];
                                    $do_data6['广播'][$value['tmname']]['othersl'] += $value['othersl'];
                                }else{
                                    $do_data6['广播'][$value['tmname']]['gyhjsl'] = $value['gyhjsl'];
                                    $do_data6['广播'][$value['tmname']]['othersl'] = $value['othersl'];
                                }
                            }
                        }
                    }
                }

                if(!empty($papermedia)){
                    //报纸媒体公益广告发布情况
                    $sqlstr6_3 = 'SELECT
                            count(*) AS gyggsl,
                             (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as tmname
                            FROM
                            tpapersample as tpaper
                            JOIN tpaperissue AS paper ON paper.fpapersampleid = tpaper.fpapersampleid
                            JOIN tmedia AS tm ON tm.fid = tpaper.fmediaid and tm.fid=tm.main_media_id
                            JOIN tad ON tad.fadid = tpaper.fadid  and tad.fadid<>0
                            WHERE
                            tad.fadclasscode = 2202
                            AND paper.fmediaid IN ('.join(',',(empty($papermedia)?[0]:$papermedia)).') and date(paper.fissuedate) between "'.$nowstarttime.'" and "'.$nowendtime.'"
                            group by paper.fmediaid';
                    $do_data6_3 = M()->query($sqlstr6_3);
                    foreach ($do_data6_3 as $key => $value) {
                        $do_data6['报纸'][$value['tmname']]['gyggsl'] = $value['gyggsl'];
                    }
                }
                

                foreach ($do_data6 as $key => $value) {
                    if($key == '电视' || $key == '广播'){
                        $html .= '<p>'.$key.'媒体公益广告发布情况表</p>';
                        $html.= '  
                        <table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
                        <tr> 
                         <td rowspan=2  >媒体名称</td>  
                         <td colspan=2  style="border-bottom:1px solid #333333;">时间段（发布条次）</td>  
                        </tr> 
                        <tr>
                            <td>黄金时段（19:00-21:00）</td>
                            <td>其他时段</td>
                        </tr>
                        ';
                        foreach($value  as $key2 => $v_tv){
                            $html.= '   
                            <tr>
                            <td>'.$key2.'</td>
                            <td>'.$v_tv['gyhjsl'].'</td>
                            <td>'.$v_tv['othersl'].'</td>
                            </tr>
                            ';
                        }
                        $html .= '</table>';
                    }else{
                        $html .= '<p>'.$key.'媒体公益广告发布情况表</p>';
                        $html.= '  
                            <table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
                            <tr> 
                            <td>媒体名称</td>  
                            <td>公益广告条次</td>
                            </tr>
                        ';
                        foreach($value  as $key2 => $v_tv){
                            $html.= '   
                            <tr>
                                <td>'.$key2.'</td>
                                <td>'.$v_tv['gyggsl'].'</td>
                            </tr>';
                        }
                        $html .= '</table>';
                    }
                }
               
                $html .= '</table>';
                unset($do_data6);
            }
        }

        //----------------------分割线---------

        $html.= '<h3>（二）行业监测情况</h3>';

        $hytype_array = ['13','16','08','06','04','03','02','01'];//八大重点行业类别
        //本环行业监测数
        $sqlstr7_1 = '
            select sum(fad_times) as lb_fad_times,fad_class_code,b.fadclass 
            from tbn_ad_summary_day a
            inner join tadclass b on a.fad_class_code = b.fcode
            where fdate between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmediaid in ('.join(',',(empty($allmediaids)?[0]:$allmediaids)).') and fad_class_code in ('.join(',',(empty($hytype_array)?[0]:$hytype_array)).')
            group by fad_class_code
            order by fad_class_code
        ';
        $do_data7_1 = M()->query($sqlstr7_1);
        foreach ($do_data7_1 as $key => $value) {
            $do_data7[$value['fad_class_code']]['lb_fad_times'] = $value['lb_fad_times'];
            $do_data7[$value['fad_class_code']]['fadclass'] = $value['fadclass'];
            $do_data7[$value['fad_class_code']]['lb_wf_fad_times'] = 0;
        }

        //本环违法记录总条次数
        $sqlstr7_2 = '
            select count(*) as lb_wf_fad_times,left(fad_class_code,2) as fad_class_code2
            from tbn_illegal_ad_issue a
            inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
            where a.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmedia_id in ('.join(',',(empty($allmediaids)?[0]:$allmediaids)).') and LEFT(fad_class_code,2) in ('.join(',',(empty($hytype_array)?[0]:$hytype_array)).')
            group by left(fad_class_code,2)
        ';
        $do_data7_2 = M()->query($sqlstr7_2);
        foreach ($do_data7_2 as $key => $value) {
            if(!empty($do_data7[$value['fad_class_code2']])){
                $do_data7[$value['fad_class_code2']]['lb_wf_fad_times'] = $value['lb_wf_fad_times'];
            }
        }

        //上环行业监测数
        $sqlstr8_1 = '
            select sum(fad_times) as lb_fad_times,fad_class_code,b.fadclass 
            from tbn_ad_summary_day a
            inner join tadclass b on a.fad_class_code = b.fcode
            where fdate between "'.$prevstarttime.'" and "'.$prevendtime.'" and a.fmediaid in ('.join(',',(empty($allmediaids)?[0]:$allmediaids)).') and fad_class_code in ('.join(',',(empty($hytype_array)?[0]:$hytype_array)).')
            group by fad_class_code
            order by fad_class_code
        ';
        $do_data8_1 = M()->query($sqlstr8_1);
        foreach ($do_data8_1 as $key => $value) {
            $do_data8[$value['fad_class_code']]['lb_fad_times'] = $value['lb_fad_times'];
            $do_data8[$value['fad_class_code']]['fadclass'] = $value['fadclass'];
            $do_data8[$value['fad_class_code']]['lb_wf_fad_times'] = 0;
        }

        //上环违法记录总条次数
        $sqlstr8_2 = '
            select count(*) as lb_wf_fad_times,left(fad_class_code,2) as fad_class_code2
            from tbn_illegal_ad_issue a
            inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
            where a.fissue_date between "'.$prevstarttime.'" and "'.$prevendtime.'" and a.fmedia_id in ('.join(',',(empty($allmediaids)?[0]:$allmediaids)).') and LEFT(fad_class_code,2) in ('.join(',',(empty($hytype_array)?[0]:$hytype_array)).')
            group by left(fad_class_code,2)
        ';
        $do_data8_2 = M()->query($sqlstr8_2);
        foreach ($do_data8_2 as $key => $value) {
            if(!empty($do_data8[$value['fad_class_code2']])){
                $do_data8[$value['fad_class_code2']]['lb_wf_fad_times'] = $value['lb_wf_fad_times'];
            }
        }

        $html .= ' 
            <table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
            <tr>
            <td>主类别</td> 
            <td>监测条次</td> 
            <td>环比</td> 
            <td>涉嫌违法条次</td> 
            <td>环比</td> 
            <td>违法率</td> 
            <td>环比</td>
            </tr>';
        foreach($do_data7 as $key => $val){
            $ad_totals += $val['lb_fad_times'];
            $ad_totals2 += $do_data8[$key]['lb_fad_times'];
            $ad_wfggsls += $val['lb_wf_fad_times'];
            $ad_wfggsls2 += $do_data8[$key]['lb_wf_fad_times'];

            if(!empty($do_data8[$key])){
                $lb_fad_times_str = ($do_data8[$key]['lb_fad_times'] == $val['lb_fad_times'])?'无变化':(($val['lb_fad_times']-$do_data8[$key]['lb_fad_times'])>0?($val['lb_fad_times']-$do_data8[$key]['lb_fad_times']).$px_arr[0]:abs($val['lb_fad_times']-$do_data8[$key]['lb_fad_times']).$px_arr[1]);
                
                $lb_wf_fad_times_str = ($do_data8[$key]['lb_wf_fad_times'] == $val['lb_wf_fad_times'])?'无变化':(($val['lb_wf_fad_times']-$do_data8[$key]['lb_wf_fad_times'])>0?($val['lb_wf_fad_times']-$do_data8[$key]['lb_wf_fad_times']).$px_arr[0]:abs($val['lb_wf_fad_times']-$do_data8[$key]['lb_wf_fad_times']).$px_arr[1]);

                $lb_wfl_fad_times_str = (round($do_data8[$key]['lb_wf_fad_times']/$do_data8[$key]['lb_fad_times']*100,2) == round($val['lb_wf_fad_times']/$val['lb_fad_times']*100,2)?'无变化':((round($val['lb_wf_fad_times']/$val['lb_fad_times']*100,2)-round($do_data8[$key]['lb_wf_fad_times']/$do_data8[$key]['lb_fad_times']*100,2))>0?(round($val['lb_wf_fad_times']/$val['lb_fad_times']*100,2)-round($do_data8[$key]['lb_wf_fad_times']/$do_data8[$key]['lb_fad_times']*100,2)).'%'.$px_arr[0]:abs((round($val['lb_wf_fad_times']/$val['lb_fad_times']*100,2)-round($do_data8[$key]['lb_wf_fad_times']/$do_data8[$key]['lb_fad_times']*100,2))).'%'.$px_arr[1]));
            }else{
                $lb_fad_times_str = '无变化';
                $lb_wf_fad_times_str = '无变化';
                $lb_wfl_fad_times_str = '无变化';
            }

            $html .= '
                <tr> 
                    <td>'.$val['fadclass'].'</td> 
                    <td>'.$val['lb_fad_times'].'</td> 
                    <td>'.$lb_fad_times_str.'</td>
                    <td>'.$val['lb_wf_fad_times'].'</td> 
                    <td>'.$lb_wf_fad_times_str.'</td> 
                    <td>'.round($val['lb_wf_fad_times']/$val['lb_fad_times']*100,2).'%</td>
                    <td>'.$lb_wfl_fad_times_str.'</td>
                </tr>';
        }
        if(empty($ad_totals2)){
            $all_lb_fad_times_str = '无变化';
            $all_lb_wf_fad_times_str = '无变化';
            $all_lb_wfl_fad_times_str = '无变化';
        }else{
            $all_lb_fad_times_str = ($ad_totals == $ad_totals2)?'无变化':(($ad_totals-$ad_totals2)>0?($ad_totals-$ad_totals2).$px_arr[0]:abs($ad_totals-$ad_totals2).$px_arr[1]);
            $all_lb_wf_fad_times_str = ($ad_wfggsls == $ad_wfggsls2)?'无变化':(($ad_wfggsls-$ad_wfggsls2)>0?($ad_wfggsls-$ad_wfggsls2).$px_arr[0]:abs($ad_wfggsls-$ad_wfggsls2).$px_arr[1]);
            $all_lb_wfl_fad_times_str = (round($ad_wfggsls2/$ad_totals2*100,2) == round($ad_wfggsls/$ad_totals*100,2)?'无变化':((round($ad_wfggsls/$ad_totals*100,2)-round($ad_wfggsls2/$ad_totals2*100,2))>0?(round($ad_wfggsls/$ad_totals*100,2)-round($ad_wfggsls2/$ad_totals2*100,2)).'%'.$px_arr[0]:abs((round($ad_wfggsls/$ad_totals*100,2)-round($ad_wfggsls2/$ad_totals2*100,2))).'%'.$px_arr[1]));
        }

        $html .= '
                <tr> 
                    <td>八类重点行业累计</td> 
                    <td>'.$ad_totals.'</td> 
                    <td>'.$all_lb_fad_times_str.'</td>
                    <td>'.$ad_wfggsls.'</td> 
                    <td>'.$all_lb_wf_fad_times_str.'</td> 
                    <td>'.round($val['ad_wfggsls']/$val['ad_totals']*100,2).'%</td>
                    <td>'.$all_lb_wfl_fad_times_str.'</td>
                </tr>';
        $html .= '</table>';

        //-----------------------分割线-------------

        $sqlstr9_1 = '
            select sum(fad_times) as lb_fad_times,a.fad_class_code,b.fadclass,ifnull(c.lb_wf_fad_times,0) as lb_wf_fad_times,(c.lb_wf_fad_times/sum(fad_times)) as fad_wfl
            from tbn_ad_summary_day a
            inner join tadclass b on a.fad_class_code = b.fcode
            left join (select count(*) as lb_wf_fad_times,left(fad_class_code,2) as fad_class_code2
            from tbn_illegal_ad_issue a
            inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
            where a.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmedia_id in ('.join(',',(empty($allmediaids)?[0]:$allmediaids)).')
            group by left(fad_class_code,2)) c on a.fad_class_code = c.fad_class_code2
            where fdate between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmediaid in ('.join(',',(empty($allmediaids)?[0]:$allmediaids)).') 
            group by fad_class_code
            order by fad_wfl desc
        ';
        $do_data9_1 = M()->query($sqlstr9_1);

        $html.= '<p></p>各大类广告情况表（按违法率排名）：';
        $html .= ' 
            <table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
            <tr>
            <td>序号</td>
            <td>主类别</td> 
            <td>监测条次</td> 
            <td>涉嫌违法条次</td> 
            <td>监测违法率</td>
            </tr>';
        $num = 0;
        $maxwf_times = 0;
        $maxwf_times2 = 0;
        foreach($do_data9_1 as $key => $val){
            if($maxwf_times<$val['lb_wf_fad_times']){
                $maxwf_times2 = $maxwf_times;
                $maxwf_fadclass2 = $maxwf_fadclass;
                $maxwf_times = $val['lb_wf_fad_times'];
                $max_times = $val['lb_fad_times'];
                $maxwf_fadclass = $val['fadclass'];
            }
            $num += 1;
            $ad_totals3 += $val['lb_fad_times'];
            $ad_wfggsls3 += $val['lb_wf_fad_times'];
            $html .= '
                <tr> 
                    <td>'.$num.'</td> 
                    <td>'.$val['fadclass'].'</td> 
                    <td>'.$val['lb_fad_times'].'</td> 
                    <td>'.$val['lb_wf_fad_times'].'</td> 
                    <td>'.round($val['fad_wfl']*100,2).'%</td>
                </tr>';
        }

        $num += 1;
        $html .= '<tr>';
        $html .= '<td>'.$num.'</td>';
        $html .= '<td>小计</td>';
        $html .= '<td>'.$ad_totals3.'</td>';
        $html .= '<td>'.$ad_wfggsls3.'</td>';
        $html .= '<td>'.round(($ad_wfggsls3/$ad_totals3)*100,2).'%</td></tr>';
        $html .= '</table>';
        
        $tv_wfl = ($mt_data['01']['now_mt_wf_fad_times']/$mt_data['01']['now_mt_fad_times'] == $mt_data['01']['prev_mt_wf_fad_times']/$mt_data['01']['prev_mt_fad_times'])?'无变化':(($mt_data['01']['now_mt_wf_fad_times']/$mt_data['01']['now_mt_fad_times'] - $mt_data['01']['prev_mt_wf_fad_times']/$mt_data['01']['prev_mt_fad_times']>0)?'上升'.round(($mt_data['01']['now_mt_wf_fad_times']/$mt_data['01']['now_mt_fad_times'] - $mt_data['01']['prev_mt_wf_fad_times']/$mt_data['01']['prev_mt_fad_times'])*100,2).'个百分点':'下降'.round(abs($mt_data['01']['now_mt_wf_fad_times']/$mt_data['01']['now_mt_fad_times'] - $mt_data['01']['prev_mt_wf_fad_times']/$mt_data['01']['prev_mt_fad_times'])*100,2).'个百分点');
        $bc_wfl = ($mt_data['02']['now_mt_wf_fad_times']/$mt_data['02']['now_mt_fad_times'] == $mt_data['02']['prev_mt_wf_fad_times']/$mt_data['02']['prev_mt_fad_times'])?'无变化':(($mt_data['02']['now_mt_wf_fad_times']/$mt_data['02']['now_mt_fad_times'] - $mt_data['02']['prev_mt_wf_fad_times']/$mt_data['02']['prev_mt_fad_times']>0)?'上升'.round(($mt_data['02']['now_mt_wf_fad_times']/$mt_data['02']['now_mt_fad_times'] - $mt_data['02']['prev_mt_wf_fad_times']/$mt_data['02']['prev_mt_fad_times'])*100,2).'个百分点':'下降'.round(abs($mt_data['02']['now_mt_wf_fad_times']/$mt_data['02']['now_mt_fad_times'] - $mt_data['02']['prev_mt_wf_fad_times']/$mt_data['02']['prev_mt_fad_times'])*100,2).'个百分点');
        $pa_wfl = ($mt_data['03']['now_mt_wf_fad_times']/$mt_data['03']['now_mt_fad_times'] == $mt_data['03']['prev_mt_wf_fad_times']/$mt_data['03']['prev_mt_fad_times'])?'无变化':(($mt_data['03']['now_mt_wf_fad_times']/$mt_data['03']['now_mt_fad_times'] - $mt_data['03']['prev_mt_wf_fad_times']/$mt_data['03']['prev_mt_fad_times']>0)?'上升'.round(($mt_data['03']['now_mt_wf_fad_times']/$mt_data['03']['now_mt_fad_times'] - $mt_data['03']['prev_mt_wf_fad_times']/$mt_data['03']['prev_mt_fad_times'])*100,2).'个百分点':'下降'.round(abs($mt_data['03']['now_mt_wf_fad_times']/$mt_data['03']['now_mt_fad_times'] - $mt_data['03']['prev_mt_wf_fad_times']/$mt_data['03']['prev_mt_fad_times'])*100,2).'个百分点');

        $html .='<h3>二、广告监测警示</h3>';
        $html .='<p>（一）本'.$stitle.'广告监测违法率</p>
                <p>本'.$stitle.'，各地区媒体广告监测违法率环比'.$hb_illegality_str.'，其中报刊'.$pa_wfl.'、电视'.$tv_wfl.'、广播'.$bc_wfl.'。本'.$stitle.'违法广告类别主要集中在'.$maxwf_fadclass.'和'.$maxwf_fadclass2.'两个行业，累计涉嫌违法'.($maxwf_times+$maxwf_times2).'条次<p>';
        $html .='<p>（二）'.$maxwf_fadclass.'广告违法情况较严重</p>
                <p>本'.$stitle.'共监测'.$maxwf_fadclass.'广告'.$max_times.'条次，其中涉嫌违法广告'.$maxwf_times.'条次，监测违法率为'.round($maxwf_times/$max_times*100,2).'%，高居各行业榜首。</p>';

        $html .='<h3>三、典型违法广告案例</h3>';

        $sqlstr10_1 = '
                    select count(*) as dx_wf_fad_times,fsample_id,c.fexpressions,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,c.fad_name,ts.fadclass
                    from tbn_illegal_ad_issue a
                    inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
                    inner join tmedia ta on a.fmedia_id = ta.fid
                    inner join tadclass ts on c.fad_class_code = ts.fcode
                    where a.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmedia_id in ('.join(',',(empty($alltvmediaids)?[0]:$alltvmediaids)).')
                    group by c.fad_name
                    order by dx_wf_fad_times desc
                    limit 5
                ';
        $do_data10_1 = M()->query($sqlstr10_1);

        $sqlstr10_2 = '
                    select count(*) as dx_wf_fad_times,fsample_id,c.fexpressions,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,c.fad_name,ts.fadclass
                    from tbn_illegal_ad_issue a
                    inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
                    inner join tmedia ta on a.fmedia_id = ta.fid
                    inner join tadclass ts on c.fad_class_code = ts.fcode
                    where a.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmedia_id in ('.join(',',(empty($allbcmediaids)?[0]:$allbcmediaids)).')
                    group by c.fad_name
                    order by dx_wf_fad_times desc
                    limit 5
                ';
        $do_data10_2 = M()->query($sqlstr10_2);

        $sqlstr10_3 = '
                    select count(*) as dx_wf_fad_times,fsample_id,c.fexpressions,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,c.fad_name,ts.fadclass
                    from tbn_illegal_ad_issue a
                    inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
                    inner join tmedia ta on a.fmedia_id = ta.fid
                    inner join tadclass ts on c.fad_class_code = ts.fcode
                    where a.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmedia_id in ('.join(',',(empty($allpapermediaids)?[0]:$allpapermediaids)).')
                    group by c.fad_name
                    order by dx_wf_fad_times desc
                    limit 5
                ';
        $do_data10_3 = M()->query($sqlstr10_3);

        $array = ['一','二','三','四','五'];
        if($do_data10_1){
            $html .='<h3>电视</h3>';
            foreach($do_data10_1 as $k => $val){
                $html.='<p>('.$array[$k].')、'.$val['fad_name'].'  ('.$val['fadclass'].')</p>';
                $html.='<p>发布媒体：'.$val['fmedianame'].'</p>';
                $html.='<p>违法表现：“'.$val['fexpressions'].'”</p>';
                $html.='<p>违法次数：该广告累计涉嫌违法'.$val['dx_wf_fad_times'].'条次</p>';
            }
        }

        if($do_data10_2){
            $html .='<h3>广播</h3>';
            foreach($do_data10_2 as $k => $val){
                $html.='<p>('.$array[$k].')、'.$val['fad_name'].'  ('.$val['fadclass'].')</p>';
                $html.='<p>发布媒体：'.$val['fmedianame'].'</p>';
                $html.='<p>违法表现：“'.$val['fexpressions'].'”</p>';
                $html.='<p>违法次数：该广告累计涉嫌违法'.$val['dx_wf_fad_times'].'条次</p>';
            }
        }

        if($do_data10_3){
            $html .='<h3>报纸</h3>';
            foreach($do_data10_3 as $k => $val){
                $html.='<p>('.$array[$k].')、'.$val['fad_name'].'  ('.$val['fadclass'].')</p>';
                $html.='<p>发布媒体：'.$val['fmedianame'].'</p>';
                $html.='<p>违法表现：“'.$val['fexpressions'].'”</p>';
                $html.='<p>违法次数：该广告累计涉嫌违法'.$val['dx_wf_fad_times'].'条次</p>';
            }
        }

        $sqlstr11_1 = '
                    select count(*) as dx_wf_fad_times,fsample_id,c.fexpressions,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,c.fad_name,c.fadowner,sum(case when c.fmedia_class = 1 or c.fmedia_class = 2 then (UNIX_TIMESTAMP(fendtime)-UNIX_TIMESTAMP(fstarttime)) else 0 end) as fad_lenth
                    from tbn_illegal_ad_issue a
                    inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
                    inner join tmedia ta on a.fmedia_id = ta.fid
                    where a.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmedia_id in ('.join(',',(empty($allmediaids)?[0]:$allmediaids)).')
                    group by c.fad_name
                    order by dx_wf_fad_times desc
                    limit 10
                ';
        $do_data11_1 = M()->query($sqlstr11_1);
        $html .= '<h3>四、广告监测情况统计表</h3>';
        $html .= '<p>前十位涉嫌违法广告排名表</p>';
        $html .= ' 
            <table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
            <tr>
            <td>序号</td>
            <td>广告名称</td> 
            <td>广告主</td> 
            <td>媒体名称</td> 
            <td>涉嫌违法条次</td> 
            <td>违法时长</td> 
            </tr>';
        foreach ($do_data11_1 as $key => $value) {
            $html .= '
            <tr> 
                <td>'.($key + 1).'</td> 
                <td>'.$value['fad_name'].'</td>
                <td>'.$value['fadowner'].'</td>
                <td>'.$value['fmedianame'].'</td>
                <td>'.$value['dx_wf_fad_times'].'</td>
                <td>'.$value['fad_lenth'].'</td>
            </tr>';
        }

        $month_date = date('Ymdhis');
        $savefile = './Public/Word/FJ'.$month_date.'.doc';

        //将文档保存
        word_start();
        echo $html;
        word_save($savefile);
        ob_flush();//每次执行前刷新缓存
        flush();
        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/FJ'.$month_date.'.doc',$savefile,array('Content-Disposition' => 'attachment;filename='.$reportname.'.doc'));//上传云
        unlink($savefile);//删除文件

        //将生成记录保存
        $data['pnname']             = $reportname;
        $data['pntype']             = $pntype;
        $data['pnfiletype']         = 10;
        $data['pnstarttime']        = $nowstarttime;
        $data['pnendtime']          = $nowendtime;
        $data['pncreatetime']       = date('Y-m-d H:i:s');
        $data['pnurl']              = $ret['url'];
        $data['pnhtml']             = $html;
        $data['pnfocus']            = $focus;
        $data['pntrepid']           = $tredata['fid'];
        $data['fcustomer']          = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
        }
        
    }

    /**
     * 月报、季报、半年报、年报公共部分生成（固定数据）
     * by zw
     */
    public function public_presentation2($date,$focus,$treid,$types){
        session_write_close();
        $system_num = getconfig('system_num');
        header("Content-type: text/html; charset=utf-8");
        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);

        $mediaclass = ['01' => '电视','02' => '广播','03' => '报纸'];//包括的媒体类别
        $px_arr = ['↑','↓'];

        $tredata = M('tregulator')
            ->field('a.*,b.fname1')
            ->alias('a')
            ->join('tregion b on a.fregionid = b.fid')
            ->where('a.fid = '.$treid)
            ->find();
        $regulatorpname = $tredata['fname'];//机构名

        //配置文档基础项
        if($types == 'month'){
            $nowtime_arr = explode("-" ,$date);//传入时间分段
            $season = $nowtime_arr[1];//第几月

            $nowstarttime = date('Y-m-01',mktime(0,0,0,($season - 1) *1 +1,1,date('Y',strtotime($date))));//本环开始时间
            $nowendtime = date('Y-m-t',mktime(0,0,0,$season * 1,1,date('Y',strtotime($date))));//本环结束时间
            $prevstarttime = date('Y-m-01',mktime(0,0,0,($season - 2) *1 +1,1,date('Y',strtotime($date))));//上环开始时间
            $prevendtime = date('Y-m-t',mktime(0,0,0,($season - 1) * 1,1,date('Y',strtotime($date))));// 上环结束时间

            $stitle = '月';
            $date1 = date('Y年',strtotime($date)).$season.$stitle;
            $pntype = 30;//报告类型
            $reportname = $regulatorpname.date('Y年',strtotime($date)).$season.'月份月报';//报告名称
            $headerqikan = $nowtime_arr[0].'年第'.$season.'期';
        }elseif($types == 'season'){
            $nowtime_arr = explode("-" ,$date);//传入时间分段
            $season = ceil((date('n',strtotime($date)))/3);//第几季度

            $nowstarttime = date('Y-m-01',mktime(0,0,0,($season - 1) *3 +1,1,date('Y',strtotime($date))));//本环开始时间
            $nowendtime = date('Y-m-t',mktime(0,0,0,$season * 3,1,date('Y',strtotime($date))));//本环结束时间
            $prevstarttime = date('Y-m-01',mktime(0,0,0,($season - 2) *3 +1,1,date('Y',strtotime($date))));//上环开始时间
            $prevendtime = date('Y-m-t',mktime(0,0,0,($season - 1) * 3,1,date('Y',strtotime($date))));// 上环结束时间

            $stitle = '季度';
            $date1 = date('Y年',strtotime($date)).'第'.$season.$stitle;
            $pntype = 70;//报告类型
            $reportname = $regulatorpname.date('Y年',strtotime($date)).'第'.$season.'季度季报';//报告名称
            $headerqikan = $nowtime_arr[0].'年第'.$season.'期';
        }elseif($types == 'halfyear'){
            $nowtime_arr = explode("-" ,$date);//传入时间分段
            $season = $nowtime_arr[1]<7?1:2;//1上半年/2下半年

            $nowstarttime = date('Y-m-01',mktime(0,0,0,($season - 1) *6 +1,1,date('Y',strtotime($date))));//本环开始时间
            $nowendtime = date('Y-m-t',mktime(0,0,0,$season * 6,1,date('Y',strtotime($date))));//本环结束时间
            $prevstarttime = date('Y-m-01',mktime(0,0,0,($season - 2) *6 +1,1,date('Y',strtotime($date))));//上环开始时间
            $prevendtime = date('Y-m-t',mktime(0,0,0,($season - 1) * 6,1,date('Y',strtotime($date))));// 上环结束时间

            $stitle = $nowtime_arr[1]<7?'上半年':'下半年';
            $date1 = date('Y年',strtotime($date)).$stitle;
            $pntype = 75;//报告类型
            $reportname = $regulatorpname.date('Y年',strtotime($date)).$stitle.'半年报';//报告名称
            $headerqikan = $nowtime_arr[0].'年第'.$season.'期';
        }elseif($types == 'year'){
            $nowtime_arr = explode("-" ,$date);//传入时间分段
            $season = 1;//第几期

            $nowstarttime = date('Y-01-01',strtotime($date));//本环开始时间
            $nowendtime = date('Y-12-31',strtotime($date));//本环结束时间
            $prevstarttime = date('Y-01-01',strtotime($date.'-12 month'));//上环开始时间
            $prevendtime = date('Y-12-31',strtotime($date.'-12 month'));// 上环结束时间

            $stitle = '年';
            $date1 = date('Y年',strtotime($date));
            $pntype = 80;//报告类型
            $reportname = $regulatorpname.date('Y年',strtotime($date)).'年报';//报告名称
            $headerqikan = $nowtime_arr[0].'年第'.$season.'期';
        }elseif($types == 'custom'){
            if(strtotime($date[0])>strtotime($date[1])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'时间选择有误'));
            }
            $nowtime_arr = $date;

            $nowstarttime = $date[0];//本环开始时间
            $nowendtime = $date[1];//本环结束时间
            $dayc = $this->get_day_diff(strtotime($date[1]),strtotime($date[0]));
            $prevstarttime = date('Y-m-d',strtotime($date[0].'-1 day'));//上环开始时间
            $prevendtime = date('Y-m-d',strtotime($prevstarttime.'-'.$dayc.' day'));// 上环结束时间

            $stitle = '期';
            $date1 = date('Y年m月d日',strtotime($nowstarttime)).'至'.date('Y年m月d日',strtotime($nowendtime));
            $pntype = 81;//报告类型
            $reportname = $regulatorpname.$date1.'报告';//报告名称
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
        }
        
        $html = '<div style="margin:0 auto;"> <style>.tb tr td{ background:#ffffff; text-align:center; border-bottom:1px solid #333333;border-right:1px solid #333333;}</style>';
        $html.= '<div align="center"><p style="color:red;font-size:30px;">'.$regulatorpname.'</p></div>';
        $html.= '<div align="center"><p style="color:red;font-size:18px;">广告监测报告</p></div>';
        if(!empty($headerqikan)){
            $html.= '<p align="center" style="color:red;font-size:12px;">（'.$headerqikan.'）</p>';
        }
        $html.= '<p align="right">'.$date1.'</p>';
        $html.= '<p>一、广告监测情况综述</p>';

        //所有媒体
        $mediares = M('tmedia')
            ->alias('t')
            ->field('t.fid,t.fmediaownerid,t.fmediaclassid, (case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,tw.fregionid')
            ->join('tmediaowner as tw on t.fmediaownerid = tw.fid')
            ->join('tregulatormedia ta on t.fid=ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'"')
            ->where($where)
            ->select();

        //媒体按行政区划分组
        $allmediaids = [];
        $alltvmediaids = [];
        $allbcmediaids = [];
        $allpapermediaids = [];
        $mediadata['国家级']['meidas'] = [];
        $mediadata['省属']['meidas'] = [];
        $mediadata['市属']['meidas'] = [];
        $mediadata['县属']['meidas'] = [];
        foreach($mediares as $val){
            $allmediaids[] = $val['fid'];
            if(substr($val['fmediaclassid'],0,2) == '01'){
                $alltvmediaids[] = $val['fid'];
            }elseif(substr($val['fmediaclassid'],0,2) == '02'){
                $allbcmediaids[] = $val['fid'];
            }elseif(substr($val['fmediaclassid'],0,2) == '03'){
                $allpapermediaids[] = $val['fid'];
            }
            $tregion_len = get_tregionlevel($val['fregionid']);
            if($tregion_len == 1){//国家级
                $mediadata['国家级']['meidas'][] = $val;
                $mediadata['国家级']['meidastr'] .= $mediadata['国家级']['meidastr']?'、'.$val['fmedianame']:$val['fmedianame'];
            }elseif($tregion_len == 2){//省级
                $mediadata['省属']['meidas'][] = $val;
                $mediadata['省属']['meidastr'] .= $mediadata['省属']['meidastr']?'、'.$val['fmedianame']:$val['fmedianame'];
            }elseif($tregion_len == 4){//市级
                $mediadata['市属']['meidas'][] = $val;
                $mediadata['市属']['meidastr'] .= $mediadata['市属']['meidastr']?'、'.$val['fmedianame']:$val['fmedianame'];
            }elseif($tregion_len == 6){//县级
                $mediadata['县属']['meidas'][] = $val;
                $mediadata['县属']['meidastr'] .= $mediadata['县属']['meidastr']?'、'.$val['fmedianame']:$val['fmedianame'];
            }
        }
        if(empty($allmediaids)){
            $allmediaids[] = 0;
        }

        $str = '';
        foreach ($mediadata as $key => $value) {
            if(!empty($value['meidas'])){
                $str .= $str?'，'.$key.'媒体'.count($mediadata[$key]['meidas']).'家（'.$mediadata[$key]['meidastr'].'）':$key.'媒体'.count($mediadata[$key]['meidas']).'家（'.$mediadata[$key]['meidastr'].'）';
            }
        }

        $html.= '<p> 广告监测中心'.($types == 'halfyear'?$stitle:'本'.$stitle).'共监测'.count($mediares).'家媒体。其中'.$str.'。 </p>';

        //本环记录总数
        $sqlstr1_1 = '
            select sum(fad_times_long_ad) as zong_fad_times 
            from tbn_ad_summary_day_v3 a
            inner join tregulatormedia ta on a.fmediaid = ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'" 
            inner join tmedia b on a.fmediaid = b.fid and b.fid = b.main_media_id
            where fdate between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fcustomer = "'.$system_num.'"
        ';
        $do_data1_1 = M()->query($sqlstr1_1)[0]['zong_fad_times'];

        //上环记录总数
        $sqlstr1_2 = '
            select sum(fad_times_long_ad) as zong_fad_times 
            from tbn_ad_summary_day_v3 a
            inner join tregulatormedia ta on a.fmediaid = ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'" 
            inner join tmedia b on a.fmediaid = b.fid and b.fid = b.main_media_id
            where fdate between "'.$prevstarttime.'" and "'.$prevendtime.'" and a.fcustomer = "'.$system_num.'"
        ';
        $do_data1_2 = M()->query($sqlstr1_2)[0]['zong_fad_times'];

        //本环违法记录总条次数
        $sqlstr2_1 = '
            select count(*) as zong_wf_fad_times,sum(case when c.fmedia_class = 1 or c.fmedia_class = 2 then (UNIX_TIMESTAMP(fendtime)-UNIX_TIMESTAMP(fstarttime)) else 0 end) as zong_fad_lenth  
            from tbn_illegal_ad_issue a
            inner join tregulatormedia ta on a.fmedia_id = ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'" 
            inner join tmedia b on a.fmedia_id = b.fid and b.fid = b.main_media_id
            inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
            where a.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" 
        ';
        $do_data2_1 = M()->query($sqlstr2_1)[0];

        //本环违法记录总条次数
        $sqlstr2_2 = '
            select count(*) as zong_wf_fad_times,sum(case when c.fmedia_class = 1 or c.fmedia_class = 2 then (UNIX_TIMESTAMP(fendtime)-UNIX_TIMESTAMP(fstarttime)) else 0 end) as zong_fad_lenth 
            from tbn_illegal_ad_issue a
            inner join tregulatormedia ta on a.fmedia_id = ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'" 
            inner join tmedia b on a.fmedia_id = b.fid and b.fid = b.main_media_id
            inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
            where a.fissue_date between "'.$prevstarttime.'" and "'.$prevendtime.'" 
        ';
        $do_data2_2 = M()->query($sqlstr2_2)[0];

        //总数环比
        $hb_ = $do_data1_1-$do_data1_2;
        if($hb_==0){
            $hb_str='无变化';
        }elseif($hb_>0){
            $hb_str='上升'.$hb_.'条次';
        }else{
            $hb_str='下降'.abs($hb_).'条次';
        }

        //违法率环比
        $hb_illegality = round(($do_data2_1['zong_wf_fad_times']/$do_data1_1)*100,2) - round(($do_data2_2['zong_wf_fad_times']/$do_data1_2)*100,2);
        if($hb_illegality==0){
            $hb_illegality_str='无变化';
        }elseif($hb_illegality>0){
            $hb_illegality_str='上升'.$hb_illegality.'个百分点';
        }else{
            $hb_illegality_str='下降'.abs($hb_illegality).'个百分点';
        }

        $html.= '<p>'.($types == 'halfyear'?$stitle:'本'.$stitle).'，监测中心监测媒体发布的各类广告'.$do_data1_1.'条次，环比'.$hb_str.'，其中涉嫌违法广告'.$do_data2_1['zong_wf_fad_times'].'条次，违法时长'.$do_data2_1['zong_fad_lenth'].'秒，监测违法率为'.round(($do_data2_1['zong_wf_fad_times']/$do_data1_1)*100,2).'%，环比'.$hb_illegality_str.'。</p>';

        //----------------------------分割线-----------------------

        $html.= '<h4>(一)分类监测情况</h4>';
        foreach ($mediadata as $mdatakey => $mdataval) {
            if(count($mdataval['meidas'])>0){
                $html .= ' 
                    <p align="center" border="1" style="font-size:18px; font-weight:bold; padding:10px;">'.$mdatakey.'媒体</p> 
                    <table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
                    <tr> 
                     <td>媒体类别</td> 
                     <td>监测条次</td> 
                     <td>环比</td> 
                     <td>涉嫌违法条次</td> 
                     <td>环比</td> 
                     <td>违法率</td> 
                     <td>环比</td> 
                    </tr>';

                $medias4 = [];
                $tvmedia = [];
                $bcmedia = [];
                $papermedia = [];
                foreach($mdataval['meidas'] as $v){
                    $medias4[] = $v['fid'];
                    if(substr($v['fmediaclassid'],0,2) == '01'){
                        $tvmedia[] = $v['fid'];
                    }elseif(substr($v['fmediaclassid'],0,2) == '02'){
                        $bcmedia[] = $v['fid'];
                    }elseif(substr($v['fmediaclassid'],0,2) == '03'){
                        $papermedia[] = $v['fid'];
                    }
                }

                //本环按媒体类别统计数据
                $sqlstr3_1 = '
                    select sum(fad_times_long_ad) as mt_fad_times,fmedia_class_code 
                    from tbn_ad_summary_day_v3 a
                    where fdate between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmediaid in ('.join(',',(empty($medias4)?[0]:$medias4)).') and a.fcustomer = "'.$system_num.'"
                    group by fmedia_class_code
                    order by fmedia_class_code
                ';
                $do_data3_1 = M()->query($sqlstr3_1);
                foreach ($do_data3_1 as $key => $value) {
                    $now_mt_data[$value['fmedia_class_code']]['mt_fad_times'] = $value['mt_fad_times'];
                    $now_mt_data[$value['fmedia_class_code']]['mt_fad_lenth'] = 0;
                    $now_mt_data[$value['fmedia_class_code']]['mt_wf_fad_times'] = 0;
                    $mt_data[$value['fmedia_class_code']]['now_mt_fad_times'] += $value['mt_fad_times'];
                }

                //上环按媒体类别统计数据
                $sqlstr3_2 = '
                    select sum(fad_times_long_ad) as mt_fad_times,fmedia_class_code 
                    from tbn_ad_summary_day_v3 a
                    where fdate between "'.$prevstarttime.'" and "'.$prevendtime.'" and a.fmediaid in ('.join(',',(empty($medias4)?[0]:$medias4)).') and a.fcustomer = "'.$system_num.'"
                    group by fmedia_class_code
                    order by fmedia_class_code
                ';
                $do_data3_2 = M()->query($sqlstr3_2);
                foreach ($do_data3_2 as $key => $value) {
                    $prev_mt_data[$value['fmedia_class_code']]['mt_fad_times'] = $value['mt_fad_times'];
                    $prev_mt_data[$value['fmedia_class_code']]['mt_fad_lenth'] = 0;
                    $prev_mt_data[$value['fmedia_class_code']]['mt_wf_fad_times'] = 0;
                    $mt_data[$value['fmedia_class_code']]['prev_mt_fad_times'] += $value['mt_fad_times'];
                }

                //本环违法记录总条次数
                $sqlstr4_1 = '
                    select count(*) as mt_wf_fad_times,sum(case when c.fmedia_class = 1 or c.fmedia_class = 2 then (UNIX_TIMESTAMP(fendtime)-UNIX_TIMESTAMP(fstarttime)) else 0 end) as mt_fad_lenth,(case when length(c.fmedia_class)<2 then concat("0",c.fmedia_class) else c.fmedia_class end) as fmedia_class
                    from tbn_illegal_ad_issue a
                    inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
                    where a.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmedia_id in ('.join(',',(empty($medias4)?[0]:$medias4)).')
                    group by c.fmedia_class
                    order by c.fmedia_class
                ';
                $do_data4_1 = M()->query($sqlstr4_1);
                foreach ($do_data4_1 as $key => $value) {
                    $now_mt_data[$value['fmedia_class']]['mt_fad_lenth'] = $value['mt_fad_lenth'];
                    $now_mt_data[$value['fmedia_class']]['mt_wf_fad_times'] = $value['mt_wf_fad_times'];
                    $mt_data[$value['fmedia_class']]['now_mt_wf_fad_times'] += $value['mt_wf_fad_times'];
                }

                //本环违法记录总条次数
                $sqlstr4_2 = '
                    select count(*) as mt_wf_fad_times,sum(case when c.fmedia_class = 1 or c.fmedia_class = 2 then (UNIX_TIMESTAMP(fendtime)-UNIX_TIMESTAMP(fstarttime)) else 0 end) as mt_fad_lenth,(case when length(c.fmedia_class)<2 then concat("0",c.fmedia_class) else c.fmedia_class end) as fmedia_class
                    from tbn_illegal_ad_issue a
                    inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
                    where a.fissue_date between "'.$prevstarttime.'" and "'.$prevendtime.'" and a.fmedia_id in ('.join(',',(empty($medias4)?[0]:$medias4)).')
                    group by c.fmedia_class
                    order by c.fmedia_class
                ';
                $do_data4_2 = M()->query($sqlstr4_2);
                foreach ($do_data4_2 as $key => $value) {
                    $prev_mt_data[$value['fmedia_class']]['mt_fad_lenth'] = $value['mt_fad_lenth'];
                    $prev_mt_data[$value['fmedia_class']]['mt_wf_fad_times'] = $value['mt_wf_fad_times'];
                    $mt_data[$value['fmedia_class']]['prev_mt_wf_fad_times'] += $value['mt_wf_fad_times'];
                }

                //按媒体类别展示数据
                foreach ($mediaclass as $key => $value) {
                    if(!empty($now_mt_data[$key]['mt_fad_times'])){
                        $html .= '<tr>';
                        $html .= '<td>'.$value.'</td>';
                        $html .= '<td>'.$now_mt_data[$key]['mt_fad_times'].'</td>';

                        if(!empty($prev_mt_data[$key]['mt_fad_times'])){
                            $hb_total = $now_mt_data[$key]['mt_fad_times'] - $prev_mt_data[$key]['mt_fad_times'];
                        }else{
                            $hb_total = 0;
                        }
                        if($hb_total==0){
                            $hb_total = '无变化';
                        }elseif ($hb_total>0) {
                            $hb_total = $hb_total.$px_arr[0];
                        }else{
                            $hb_total = abs($hb_total).$px_arr[1];
                        }

                        $html .= '<td>'.$hb_total.'</td>';
                        $html .= '<td>'.$now_mt_data[$key]['mt_wf_fad_times'].'</td>';

                        if(!empty($prev_mt_data[$key]['mt_fad_times'])){
                            $hb_wfggsl = $now_mt_data[$key]['mt_wf_fad_times'] - $prev_mt_data[$key]['mt_wf_fad_times'];
                        }else{
                            $hb_wfggsl = 0;
                        }

                        $html .= '<td>'.(($hb_wfggsl==0)?'无变化':($hb_wfggsl>0?$hb_wfggsl.$px_arr[0]:abs($hb_wfggsl).$px_arr[1])).'</td>';
                        $html .= '<td>'.round($now_mt_data[$key]['mt_wf_fad_times']/$now_mt_data[$key]['mt_fad_times']*100,2).'%</td>';

                        if(!empty($prev_mt_data[$key]['mt_fad_times'])){
                            $hb_ggslwfl = round(($now_mt_data[$key]['mt_wf_fad_times']/$now_mt_data[$key]['mt_fad_times'])*100,2) - round(($prev_mt_data[$key]['mt_wf_fad_times']/$prev_mt_data[$key]['mt_fad_times'])*100,2);
                        }else{
                            $hb_ggslwfl = 0;
                        }
                        if($hb_ggslwfl==0){
                            $hb_ggslwfl = '无变化';
                        }elseif ($hb_ggslwfl>0) {
                            $hb_ggslwfl = $hb_ggslwfl.'%'.$px_arr[0];
                        }else{
                            $hb_ggslwfl = abs($hb_ggslwfl).'%'.$px_arr[1];
                        }

                        $html .= '<td>'.$hb_ggslwfl.'</td></tr>';
                    }
                }
                $html .= '</table>';

                //清空数据
                unset($now_mt_data);
                unset($prev_mt_data);

                //---------------------------

                $html .= '<p>媒体：</p>';
                $html .= ' 
                    <table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
                    <tr> 
                     <td>序号</td> 
                     <td>媒体名称</td> 
                     <td>监测条次</td> 
                     <td>总时长</td> 
                     <td>涉嫌违法条次</td> 
                     <td>违法时长</td> 
                     <td>监测违法率</td>
                    </tr>';

                $adv_totals1 = 0;
                $illegal_total1 = 0;
                $num = 0;

                //本环按媒体汇总
                $sqlstr5_1 = '
                    select (case when instr(b.fmedianame,"（") > 0 then left(b.fmedianame,instr(b.fmedianame,"（") -1) else b.fmedianame end) as fmedianame,sum(fad_times_long_ad) as m_fad_times,sum(fad_play_len_long_ad) as m_fad_lenth,ifnull(m_ad_issuecount,0) as m_ad_issuecount,ifnull(m_ad_fad_lenth,0) as m_ad_fad_lenth,ifnull(m_ad_issuecount,0)/sum(fad_times_long_ad) as jcwfl
                    from tbn_ad_summary_day_v3 a
                    inner join tregulatormedia ta on a.fmediaid = ta.fmediaid and ta.fisoutmedia=0 and ta.fregulatorcode = '.$tredata['fid'].' and ta.fcustomer = "'.$system_num.'" 
                    inner join tmedia b on a.fmediaid = b.fid and b.fid = b.main_media_id
                    left join (select a1.fmedia_id,count(1) as m_ad_issuecount,sum(case when c1.fmedia_class = 1 or c1.fmedia_class = 2 then (UNIX_TIMESTAMP(fendtime)-UNIX_TIMESTAMP(fstarttime)) else 0 end) as m_ad_fad_lenth from tbn_illegal_ad_issue a1 inner join tbn_illegal_ad c1 on a1.fillegal_ad_id = c1.fid where a1.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" and a1.fmedia_id in ('.join(',',(empty($medias4)?[0]:$medias4)).') group by a1.fmedia_id) c on c.fmedia_id = a.fmediaid
                    where fdate between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmediaid in ('.join(',',(empty($medias4)?[0]:$medias4)).') and a.fcustomer = "'.$system_num.'"
                    group by a.fmediaid
                    order by jcwfl desc,b.fmediaclassid asc,b.media_region_id asc
                ';
                $do_data5_1 = M()->query($sqlstr5_1);

                $m_fad_times = 0;
                $m_fad_lenth = 0;
                $m_ad_issuecount = 0;
                $m_ad_fad_lenth = 0;
                foreach($do_data5_1 as $mlist1_key => $value){
                    $m_fad_times += $value['m_fad_times'];
                    $m_fad_lenth += $value['m_fad_lenth'];
                    $m_ad_issuecount += $value['m_ad_issuecount'];
                    $m_ad_fad_lenth += $value['m_ad_fad_lenth'];
                    $num++;
                    $html .= '<tr>';
                    $html .= '<td>'.$num.'</td>';
                    $html .= '<td>'.$value['fmedianame'].'</td>';
                    $html .= '<td>'.$value['m_fad_times'].'</td>';
                    $html .= '<td>'.$value['m_fad_lenth'].'</td>';
                    $html .= '<td>'.$value['m_ad_issuecount'].'</td>';
                    $html .= '<td>'.$value['m_ad_fad_lenth'].'</td>';
                    $html .= '<td>'.round(($value['m_ad_issuecount']/$value['m_fad_times'])*100,2).'%</td></tr>';
                }
                $num++;
                $html .= '<tr>';
                $html .= '<td>'.$num.'</td>';
                $html .= '<td>小计</td>';
                $html .= '<td>'.$m_fad_times.'</td>';
                $html .= '<td>'.$m_fad_lenth.'</td>';
                $html .= '<td>'.$m_ad_issuecount.'</td>';
                $html .= '<td>'.$m_ad_fad_lenth.'</td>';
                $html .= '<td>'.(round(($m_ad_issuecount/$m_fad_times),4)*100).'%</td></tr>';
                $html .= '</table>';

                //汇总公益广告量
                if(!empty($medias4)){
                    $sqlstr6_1 = '
                        select a.fmediaid,(case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as tmname,a.fmedia_class_code,ifnull(sum(fad_times),0) gytccount,ifnull(sum(fad_times_1921),0) gytccount1921 from tbn_ad_summary_day_v3_gongyi a
                        inner join tmedia AS tm ON tm.fid = a.fmediaid and tm.fid=tm.main_media_id
                        where a.fcustomer = '.$system_num.' and a.fdate between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmediaid in ('.implode(',', $medias4).')
                        group by a.fmediaid
                        order by a.fmedia_class_code asc
                    ';
                    $do_data6_1 = M()->query($sqlstr6_1);
                    foreach ($do_data6_1 as $key => $value) {
                        if($value['fmedia_class_code'] == 1){
                            $do_data6['电视'][$value['tmname']]['gyhjsl'] = $value['gytccount1921'];
                            $do_data6['电视'][$value['tmname']]['othersl'] = $value['gytccount'] - $value['gytccount1921'];
                        }elseif($value['fmedia_class_code'] == 2){
                            $do_data6['广播'][$value['tmname']]['gyhjsl'] = $value['gytccount1921'];
                            $do_data6['广播'][$value['tmname']]['othersl'] = $value['gytccount'] - $value['gytccount1921'];
                        }else{
                            $do_data6['报纸'][$value['tmname']]['gyggsl'] = $value['gytccount'];
                        }
                    }
                }

                foreach ($do_data6 as $key => $value) {
                    if($key == '电视' || $key == '广播'){
                        $html .= '<p>'.$key.'媒体公益广告发布情况表</p>';
                        $html.= '  
                        <table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
                        <tr> 
                         <td rowspan=2  >媒体名称</td>  
                         <td colspan=2  style="border-bottom:1px solid #333333;">时间段（发布条次）</td>  
                        </tr> 
                        <tr>
                            <td>黄金时段（19:00-21:00）</td>
                            <td>其他时段</td>
                        </tr>
                        ';
                        foreach($value  as $key2 => $v_tv){
                            $html.= '   
                            <tr>
                            <td>'.$key2.'</td>
                            <td>'.$v_tv['gyhjsl'].'</td>
                            <td>'.$v_tv['othersl'].'</td>
                            </tr>
                            ';
                        }
                        $html .= '</table>';
                    }else{
                        $html .= '<p>'.$key.'媒体公益广告发布情况表</p>';
                        $html.= '  
                            <table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
                            <tr> 
                            <td>媒体名称</td>  
                            <td>公益广告条次</td>
                            </tr>
                        ';
                        foreach($value  as $key2 => $v_tv){
                            $html.= '   
                            <tr>
                                <td>'.$key2.'</td>
                                <td>'.$v_tv['gyggsl'].'</td>
                            </tr>';
                        }
                        $html .= '</table>';
                    }
                }
               
                $html .= '</table>';
                unset($do_data6);
            }
        }

        //----------------------分割线---------

        $html.= '<h3>（二）行业监测情况</h3>';

        $hytype_array = ['13','16','08','06','04','03','02','01'];//八大重点行业类别
        //本环行业监测数
        $sqlstr7_1 = '
            select sum(fad_times_long_ad) as lb_fad_times,fad_class_code,b.fadclass 
            from tbn_ad_summary_day_v3 a
            inner join tadclass b on a.fad_class_code = b.fcode
            where fdate between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmediaid in ('.join(',',(empty($allmediaids)?[0]:$allmediaids)).') and fad_class_code in ('.join(',',(empty($hytype_array)?[0]:$hytype_array)).') and a.fcustomer = "'.$system_num.'"
            group by fad_class_code
            order by fad_class_code
        ';
        $do_data7_1 = M()->query($sqlstr7_1);
        foreach ($do_data7_1 as $key => $value) {
            $do_data7[$value['fad_class_code']]['lb_fad_times'] = $value['lb_fad_times'];
            $do_data7[$value['fad_class_code']]['fadclass'] = $value['fadclass'];
            $do_data7[$value['fad_class_code']]['lb_wf_fad_times'] = 0;
        }

        //本环违法记录总条次数
        $sqlstr7_2 = '
            select count(*) as lb_wf_fad_times,left(fad_class_code,2) as fad_class_code2
            from tbn_illegal_ad_issue a
            inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
            where a.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmedia_id in ('.join(',',(empty($allmediaids)?[0]:$allmediaids)).') and LEFT(fad_class_code,2) in ('.join(',',(empty($hytype_array)?[0]:$hytype_array)).')
            group by left(fad_class_code,2)
        ';
        $do_data7_2 = M()->query($sqlstr7_2);
        foreach ($do_data7_2 as $key => $value) {
            if(!empty($do_data7[$value['fad_class_code2']])){
                $do_data7[$value['fad_class_code2']]['lb_wf_fad_times'] = $value['lb_wf_fad_times'];
            }
        }

        //上环行业监测数
        $sqlstr8_1 = '
            select sum(fad_times_long_ad) as lb_fad_times,fad_class_code,b.fadclass 
            from tbn_ad_summary_day_v3 a
            inner join tadclass b on a.fad_class_code = b.fcode
            where fdate between "'.$prevstarttime.'" and "'.$prevendtime.'" and a.fmediaid in ('.join(',',(empty($allmediaids)?[0]:$allmediaids)).') and fad_class_code in ('.join(',',(empty($hytype_array)?[0]:$hytype_array)).') and a.fcustomer = "'.$system_num.'"
            group by fad_class_code
            order by fad_class_code
        ';
        $do_data8_1 = M()->query($sqlstr8_1);
        foreach ($do_data8_1 as $key => $value) {
            $do_data8[$value['fad_class_code']]['lb_fad_times'] = $value['lb_fad_times'];
            $do_data8[$value['fad_class_code']]['fadclass'] = $value['fadclass'];
            $do_data8[$value['fad_class_code']]['lb_wf_fad_times'] = 0;
        }

        //上环违法记录总条次数
        $sqlstr8_2 = '
            select count(*) as lb_wf_fad_times,left(fad_class_code,2) as fad_class_code2
            from tbn_illegal_ad_issue a
            inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
            where a.fissue_date between "'.$prevstarttime.'" and "'.$prevendtime.'" and a.fmedia_id in ('.join(',',(empty($allmediaids)?[0]:$allmediaids)).') and LEFT(fad_class_code,2) in ('.join(',',(empty($hytype_array)?[0]:$hytype_array)).')
            group by left(fad_class_code,2)
        ';
        $do_data8_2 = M()->query($sqlstr8_2);
        foreach ($do_data8_2 as $key => $value) {
            if(!empty($do_data8[$value['fad_class_code2']])){
                $do_data8[$value['fad_class_code2']]['lb_wf_fad_times'] = $value['lb_wf_fad_times'];
            }
        }

        $html .= ' 
            <table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
            <tr>
            <td>主类别</td> 
            <td>监测条次</td> 
            <td>环比</td> 
            <td>涉嫌违法条次</td> 
            <td>环比</td> 
            <td>违法率</td> 
            <td>环比</td>
            </tr>';
        foreach($do_data7 as $key => $val){
            $ad_totals += $val['lb_fad_times'];
            $ad_totals2 += $do_data8[$key]['lb_fad_times'];
            $ad_wfggsls += $val['lb_wf_fad_times'];
            $ad_wfggsls2 += $do_data8[$key]['lb_wf_fad_times'];

            if(!empty($do_data8[$key])){
                $lb_fad_times_str = ($do_data8[$key]['lb_fad_times'] == $val['lb_fad_times'])?'无变化':(($val['lb_fad_times']-$do_data8[$key]['lb_fad_times'])>0?($val['lb_fad_times']-$do_data8[$key]['lb_fad_times']).$px_arr[0]:abs($val['lb_fad_times']-$do_data8[$key]['lb_fad_times']).$px_arr[1]);
                
                $lb_wf_fad_times_str = ($do_data8[$key]['lb_wf_fad_times'] == $val['lb_wf_fad_times'])?'无变化':(($val['lb_wf_fad_times']-$do_data8[$key]['lb_wf_fad_times'])>0?($val['lb_wf_fad_times']-$do_data8[$key]['lb_wf_fad_times']).$px_arr[0]:abs($val['lb_wf_fad_times']-$do_data8[$key]['lb_wf_fad_times']).$px_arr[1]);

                $lb_wfl_fad_times_str = (round($do_data8[$key]['lb_wf_fad_times']/$do_data8[$key]['lb_fad_times']*100,2) == round($val['lb_wf_fad_times']/$val['lb_fad_times']*100,2)?'无变化':((round($val['lb_wf_fad_times']/$val['lb_fad_times']*100,2)-round($do_data8[$key]['lb_wf_fad_times']/$do_data8[$key]['lb_fad_times']*100,2))>0?(round($val['lb_wf_fad_times']/$val['lb_fad_times']*100,2)-round($do_data8[$key]['lb_wf_fad_times']/$do_data8[$key]['lb_fad_times']*100,2)).'%'.$px_arr[0]:abs((round($val['lb_wf_fad_times']/$val['lb_fad_times']*100,2)-round($do_data8[$key]['lb_wf_fad_times']/$do_data8[$key]['lb_fad_times']*100,2))).'%'.$px_arr[1]));
            }else{
                $lb_fad_times_str = '无变化';
                $lb_wf_fad_times_str = '无变化';
                $lb_wfl_fad_times_str = '无变化';
            }

            $html .= '
                <tr> 
                    <td>'.$val['fadclass'].'</td> 
                    <td>'.$val['lb_fad_times'].'</td> 
                    <td>'.$lb_fad_times_str.'</td>
                    <td>'.$val['lb_wf_fad_times'].'</td> 
                    <td>'.$lb_wf_fad_times_str.'</td> 
                    <td>'.round($val['lb_wf_fad_times']/$val['lb_fad_times']*100,2).'%</td>
                    <td>'.$lb_wfl_fad_times_str.'</td>
                </tr>';
        }
        if(empty($ad_totals2)){
            $all_lb_fad_times_str = '无变化';
            $all_lb_wf_fad_times_str = '无变化';
            $all_lb_wfl_fad_times_str = '无变化';
        }else{
            $all_lb_fad_times_str = ($ad_totals == $ad_totals2)?'无变化':(($ad_totals-$ad_totals2)>0?($ad_totals-$ad_totals2).$px_arr[0]:abs($ad_totals-$ad_totals2).$px_arr[1]);
            $all_lb_wf_fad_times_str = ($ad_wfggsls == $ad_wfggsls2)?'无变化':(($ad_wfggsls-$ad_wfggsls2)>0?($ad_wfggsls-$ad_wfggsls2).$px_arr[0]:abs($ad_wfggsls-$ad_wfggsls2).$px_arr[1]);
            $all_lb_wfl_fad_times_str = (round($ad_wfggsls2/$ad_totals2*100,2) == round($ad_wfggsls/$ad_totals*100,2)?'无变化':((round($ad_wfggsls/$ad_totals*100,2)-round($ad_wfggsls2/$ad_totals2*100,2))>0?(round($ad_wfggsls/$ad_totals*100,2)-round($ad_wfggsls2/$ad_totals2*100,2)).'%'.$px_arr[0]:abs((round($ad_wfggsls/$ad_totals*100,2)-round($ad_wfggsls2/$ad_totals2*100,2))).'%'.$px_arr[1]));
        }

        $html .= '
                <tr> 
                    <td>八类重点行业累计</td> 
                    <td>'.$ad_totals.'</td> 
                    <td>'.$all_lb_fad_times_str.'</td>
                    <td>'.$ad_wfggsls.'</td> 
                    <td>'.$all_lb_wf_fad_times_str.'</td> 
                    <td>'.round($val['ad_wfggsls']/$val['ad_totals']*100,2).'%</td>
                    <td>'.$all_lb_wfl_fad_times_str.'</td>
                </tr>';
        $html .= '</table>';

        //-----------------------分割线-------------

        $sqlstr9_1 = '
            select sum(fad_times_long_ad) as lb_fad_times,a.fad_class_code,b.fadclass,ifnull(c.lb_wf_fad_times,0) as lb_wf_fad_times,(c.lb_wf_fad_times/sum(fad_times_long_ad)) as fad_wfl
            from tbn_ad_summary_day_v3 a
            inner join tadclass b on a.fad_class_code = b.fcode
            left join (select count(*) as lb_wf_fad_times,left(fad_class_code,2) as fad_class_code2
            from tbn_illegal_ad_issue a
            inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
            where a.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmedia_id in ('.join(',',(empty($allmediaids)?[0]:$allmediaids)).')
            group by left(fad_class_code,2)) c on a.fad_class_code = c.fad_class_code2
            where fdate between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmediaid in ('.join(',',(empty($allmediaids)?[0]:$allmediaids)).') and a.fcustomer = "'.$system_num.'"
            group by fad_class_code
            order by fad_wfl desc
        ';
        $do_data9_1 = M()->query($sqlstr9_1);

        $html.= '<p></p>各大类广告情况表（按违法率排名）：';
        $html .= ' 
            <table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
            <tr>
            <td>序号</td>
            <td>主类别</td> 
            <td>监测条次</td> 
            <td>涉嫌违法条次</td> 
            <td>监测违法率</td>
            </tr>';
        $num = 0;
        $maxwf_times = 0;
        $maxwf_times2 = 0;
        foreach($do_data9_1 as $key => $val){
            if($maxwf_times<$val['lb_wf_fad_times']){
                $maxwf_times2 = $maxwf_times;
                $maxwf_fadclass2 = $maxwf_fadclass;
                $maxwf_times = $val['lb_wf_fad_times'];
                $max_times = $val['lb_fad_times'];
                $maxwf_fadclass = $val['fadclass'];
            }
            $num += 1;
            $ad_totals3 += $val['lb_fad_times'];
            $ad_wfggsls3 += $val['lb_wf_fad_times'];
            $html .= '
                <tr> 
                    <td>'.$num.'</td> 
                    <td>'.$val['fadclass'].'</td> 
                    <td>'.$val['lb_fad_times'].'</td> 
                    <td>'.$val['lb_wf_fad_times'].'</td> 
                    <td>'.round($val['fad_wfl']*100,2).'%</td>
                </tr>';
        }

        $num += 1;
        $html .= '<tr>';
        $html .= '<td>'.$num.'</td>';
        $html .= '<td>小计</td>';
        $html .= '<td>'.$ad_totals3.'</td>';
        $html .= '<td>'.$ad_wfggsls3.'</td>';
        $html .= '<td>'.round(($ad_wfggsls3/$ad_totals3)*100,2).'%</td></tr>';
        $html .= '</table>';

        $tv_wfl = ($mt_data['01']['now_mt_wf_fad_times']/$mt_data['01']['now_mt_fad_times'] == $mt_data['01']['prev_mt_wf_fad_times']/$mt_data['01']['prev_mt_fad_times'])?'无变化':(($mt_data['01']['now_mt_wf_fad_times']/$mt_data['01']['now_mt_fad_times'] - $mt_data['01']['prev_mt_wf_fad_times']/$mt_data['01']['prev_mt_fad_times']>0)?'上升'.round(($mt_data['01']['now_mt_wf_fad_times']/$mt_data['01']['now_mt_fad_times'] - $mt_data['01']['prev_mt_wf_fad_times']/$mt_data['01']['prev_mt_fad_times'])*100,2).'个百分点':'下降'.round(abs($mt_data['01']['now_mt_wf_fad_times']/$mt_data['01']['now_mt_fad_times'] - $mt_data['01']['prev_mt_wf_fad_times']/$mt_data['01']['prev_mt_fad_times'])*100,2).'个百分点');
        $bc_wfl = ($mt_data['02']['now_mt_wf_fad_times']/$mt_data['02']['now_mt_fad_times'] == $mt_data['02']['prev_mt_wf_fad_times']/$mt_data['02']['prev_mt_fad_times'])?'无变化':(($mt_data['02']['now_mt_wf_fad_times']/$mt_data['02']['now_mt_fad_times'] - $mt_data['02']['prev_mt_wf_fad_times']/$mt_data['02']['prev_mt_fad_times']>0)?'上升'.round(($mt_data['02']['now_mt_wf_fad_times']/$mt_data['02']['now_mt_fad_times'] - $mt_data['02']['prev_mt_wf_fad_times']/$mt_data['02']['prev_mt_fad_times'])*100,2).'个百分点':'下降'.round(abs($mt_data['02']['now_mt_wf_fad_times']/$mt_data['02']['now_mt_fad_times'] - $mt_data['02']['prev_mt_wf_fad_times']/$mt_data['02']['prev_mt_fad_times'])*100,2).'个百分点');
        $pa_wfl = ($mt_data['03']['now_mt_wf_fad_times']/$mt_data['03']['now_mt_fad_times'] == $mt_data['03']['prev_mt_wf_fad_times']/$mt_data['03']['prev_mt_fad_times'])?'无变化':(($mt_data['03']['now_mt_wf_fad_times']/$mt_data['03']['now_mt_fad_times'] - $mt_data['03']['prev_mt_wf_fad_times']/$mt_data['03']['prev_mt_fad_times']>0)?'上升'.round(($mt_data['03']['now_mt_wf_fad_times']/$mt_data['03']['now_mt_fad_times'] - $mt_data['03']['prev_mt_wf_fad_times']/$mt_data['03']['prev_mt_fad_times'])*100,2).'个百分点':'下降'.round(abs($mt_data['03']['now_mt_wf_fad_times']/$mt_data['03']['now_mt_fad_times'] - $mt_data['03']['prev_mt_wf_fad_times']/$mt_data['03']['prev_mt_fad_times'])*100,2).'个百分点');

        $html .='<h3>二、广告监测警示</h3>';
        $html .='<p>（一）本'.$stitle.'广告监测违法率</p>
                <p>本'.$stitle.'，各地区媒体广告监测违法率环比'.$hb_illegality_str.'，其中报刊'.$pa_wfl.'、电视'.$tv_wfl.'、广播'.$bc_wfl.'。本'.$stitle.'违法广告类别主要集中在'.$maxwf_fadclass.'和'.$maxwf_fadclass2.'两个行业，累计涉嫌违法'.($maxwf_times+$maxwf_times2).'条次<p>';

        $html .='<p>（二）'.$maxwf_fadclass.'广告违法情况较严重</p>
                <p>本'.$stitle.'共监测'.$maxwf_fadclass.'广告'.$max_times.'条次，其中涉嫌违法广告'.$maxwf_times.'条次，监测违法率为'.round($maxwf_times/$max_times*100,2).'%，高居各行业榜首。</p>';

        $html .='<h3>三、典型违法广告案例</h3>';
        $sqlstr10_1 = '
                    select count(*) as dx_wf_fad_times,fsample_id,c.fexpressions,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,c.fad_name,ts.fadclass
                    from tbn_illegal_ad_issue a
                    inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
                    inner join tmedia ta on a.fmedia_id = ta.fid
                    inner join tadclass ts on c.fad_class_code = ts.fcode
                    where a.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmedia_id in ('.join(',',(empty($alltvmediaids)?[0]:$alltvmediaids)).')
                    group by c.fad_name
                    order by dx_wf_fad_times desc
                    limit 5
                ';
        $do_data10_1 = M()->query($sqlstr10_1);

        $sqlstr10_2 = '
                    select count(*) as dx_wf_fad_times,fsample_id,c.fexpressions,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,c.fad_name,ts.fadclass
                    from tbn_illegal_ad_issue a
                    inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
                    inner join tmedia ta on a.fmedia_id = ta.fid
                    inner join tadclass ts on c.fad_class_code = ts.fcode
                    where a.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmedia_id in ('.join(',',(empty($allbcmediaids)?[0]:$allbcmediaids)).')
                    group by c.fad_name
                    order by dx_wf_fad_times desc
                    limit 5
                ';
        $do_data10_2 = M()->query($sqlstr10_2);

        $sqlstr10_3 = '
                    select count(*) as dx_wf_fad_times,fsample_id,c.fexpressions,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,c.fad_name,ts.fadclass
                    from tbn_illegal_ad_issue a
                    inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
                    inner join tmedia ta on a.fmedia_id = ta.fid
                    inner join tadclass ts on c.fad_class_code = ts.fcode
                    where a.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmedia_id in ('.join(',',(empty($allpapermediaids)?[0]:$allpapermediaids)).')
                    group by c.fad_name
                    order by dx_wf_fad_times desc
                    limit 5
                ';
        $do_data10_3 = M()->query($sqlstr10_3);

        $array = ['一','二','三','四','五'];
        if($do_data10_1){
            $html .='<h3>电视</h3>';
            foreach($do_data10_1 as $k => $val){
                $html.='<p>('.$array[$k].')、'.$val['fad_name'].'  ('.$val['fadclass'].')</p>';
                $html.='<p>发布媒体：'.$val['fmedianame'].'</p>';
                $html.='<p>违法表现：“'.$val['fexpressions'].'”</p>';
                $html.='<p>违法次数：该广告累计涉嫌违法'.$val['dx_wf_fad_times'].'条次</p>';
            }
        }

        if($do_data10_2){
            $html .='<h3>广播</h3>';
            foreach($do_data10_2 as $k => $val){
                $html.='<p>('.$array[$k].')、'.$val['fad_name'].'  ('.$val['fadclass'].')</p>';
                $html.='<p>发布媒体：'.$val['fmedianame'].'</p>';
                $html.='<p>违法表现：“'.$val['fexpressions'].'”</p>';
                $html.='<p>违法次数：该广告累计涉嫌违法'.$val['dx_wf_fad_times'].'条次</p>';
            }
        }

        if($do_data10_3){
            $html .='<h3>报纸</h3>';
            foreach($do_data10_3 as $k => $val){
                $html.='<p>('.$array[$k].')、'.$val['fad_name'].'  ('.$val['fadclass'].')</p>';
                $html.='<p>发布媒体：'.$val['fmedianame'].'</p>';
                $html.='<p>违法表现：“'.$val['fexpressions'].'”</p>';
                $html.='<p>违法次数：该广告累计涉嫌违法'.$val['dx_wf_fad_times'].'条次</p>';
            }
        }

        $sqlstr11_1 = '
                    select count(*) as dx_wf_fad_times,fsample_id,c.fexpressions,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,c.fad_name,c.fadowner,sum(case when c.fmedia_class = 1 or c.fmedia_class = 2 then (UNIX_TIMESTAMP(fendtime)-UNIX_TIMESTAMP(fstarttime)) else 0 end) as fad_lenth
                    from tbn_illegal_ad_issue a
                    inner join tbn_illegal_ad c on a.fillegal_ad_id = c.fid and c.fstatus3 not in(30,40) and c.fcustomer = '.$system_num.'
                    inner join tmedia ta on a.fmedia_id = ta.fid
                    where a.fissue_date between "'.$nowstarttime.'" and "'.$nowendtime.'" and a.fmedia_id in ('.join(',',(empty($allmediaids)?[0]:$allmediaids)).')
                    group by c.fad_name
                    order by dx_wf_fad_times desc
                    limit 10
                ';
        $do_data11_1 = M()->query($sqlstr11_1);
        $html .= '<h3>四、广告监测情况统计表</h3>';
        $html .= '<p>前十位涉嫌违法广告排名表</p>';
        $html .= ' 
            <table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
            <tr>
            <td>序号</td>
            <td>广告名称</td> 
            <td>广告主</td> 
            <td>媒体名称</td> 
            <td>涉嫌违法条次</td> 
            <td>违法时长</td> 
            </tr>';
        foreach ($do_data11_1 as $key => $value) {
            $html .= '
            <tr> 
                <td>'.($key + 1).'</td> 
                <td>'.$value['fad_name'].'</td>
                <td>'.$value['fadowner'].'</td>
                <td>'.$value['fmedianame'].'</td>
                <td>'.$value['dx_wf_fad_times'].'</td>
                <td>'.$value['fad_lenth'].'</td>
            </tr>';
        }

        $month_date = date('Ymdhis');
        $savefile = './Public/Word/FJ'.$month_date.'.doc';

        //将文档保存
        word_start();
        echo $html;
        word_save($savefile);
        ob_flush();//每次执行前刷新缓存
        flush();
        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/FJ'.$month_date.'.doc',$savefile,array('Content-Disposition' => 'attachment;filename='.$reportname.'.doc'));//上传云
        unlink($savefile);//删除文件

        //将生成记录保存
        $data['pnname']             = $reportname;
        $data['pntype']             = $pntype;
        $data['pnfiletype']         = 10;
        $data['pnstarttime']        = $nowstarttime;
        $data['pnendtime']          = $nowendtime;
        $data['pncreatetime']       = date('Y-m-d H:i:s');
        $data['pnurl']              = $ret['url'];
        $data['pnhtml']             = $html;
        $data['pnfocus']            = $focus;
        $data['pntrepid']           = $tredata['fid'];
        $data['fcustomer']          = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
        }
        
    }
    
}