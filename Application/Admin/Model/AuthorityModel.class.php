<?php
namespace Admin\Model;



class AuthorityModel{


    /*判断权限*/
    public function authority($authority_code){

		$authorityInfo = M('tperson')->cache(true,5)->field('role_list,authority_list,fisadmin')->where(array('fid'=>session('personInfo.fid')))->find();//查询权限

		$role_list = explode(',',$authorityInfo['role_list']);//角色转为数组
		if($role_list){
			$role_authority_list = M('admin_role')->where(array('role_id'=>array('in',$role_list)))->select();//查询角色列表
		}
		
		$authority_list = array();
		$authority_list = explode(',',$authorityInfo['authority_list']);//人员所拥有的的权限
		
		foreach($role_authority_list as $role_authority){//循环角色
			$authority_list =  array_merge($authority_list,explode(',',$role_authority['authority_code_list']));//把角色所拥有的权限合并到人员权限
		}
		if(in_array(strval($authority_code),$authority_list) || intval($authorityInfo['fisadmin']) === 1){//判断是否拥有该权限
			return 'true';	
		}else{
			return 0;
		}
    }
	
	/*获取地区权限列表*/
	public function region_authority(){
		
		$personInfo = M('tperson')->cache(true,5)->where(array('fid'=>session('personInfo.fid')))->find();
		$region_authority_tmp = explode(',',$personInfo['region_authority']);
		foreach($region_authority_tmp as $region_authority_tmp0){
			if($region_authority_tmp0){
				$region_authority[] = substr($region_authority_tmp0,0,2);
			}
		}
		
		return $region_authority;
	}



}