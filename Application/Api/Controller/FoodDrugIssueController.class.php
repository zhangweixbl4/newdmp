<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;

class FoodDrugIssueController extends Controller {
	
	/**
	* 生成广告片段
	* @Param	Int $issueid 发布记录id
	* @Param	Varchar $table_name 表名
	* @Return	void 返回的数据
	* @Date		2018年7月6日 下午2:17:28
	* @Author	yuhou.wang
	*/
	public function create_ad_part(){
		session_write_close();
		$issueid = I('fid');//发布记录id
		//$table_name = I('table_name');//表名
		$fstarttime = I('fstarttime',date('Y-m-01'));// 发布日期起始
		//$fendtime = I('fendtime',date('Y-m-d'));// 发布日期结束
		$tableName = 'food_drug_issue_'.date('Ym',strtotime($fstarttime));//按查询日期确定表名
		$hasTable = M()->query('SHOW TABLES LIKE "'.$tableName.'"');//判断表是否存在
		if (!$hasTable){
			$this->ajaxReturn(array('code'=>1,'msg'=>'数据表不存在'));
		}
		//$table_name = 'food_drug_issue_201806';
		$back_url = U('Api/FoodDrugIssue/edit_ad_part@'.$_SERVER['HTTP_HOST'],array('issueid'=>$issueid,'table_name'=>$table_name)) ;
		$cut_url = C('ISSUE_CUT_URL');//剪辑广告片段的url
		$issueInfo = M($table_name)
			->cache(true,6)
			->field('fmediaid,fstarttime,fendtime,fsampleid')
			->where(array('fid'=>$issueid))
			->find();
		$start_time = $issueInfo['fstarttime'];
		$end_time = $issueInfo['fendtime'];
		$mediaInfo = M('tmedia')
			->cache(true,300)
			->field('left(fmediaclassid,2) as media_class')
			->where(array('fid'=>$issueInfo['fmediaid']))
			->find();
		if($mediaInfo['media_class'] == '01'){
			$tb = 'tv';
			$file_type = 'mp4';
		}elseif($mediaInfo['media_class'] == '02'){
			$tb = 'bc';
			$file_type = 'mp3';
		}else{
			$tb = 'tv';
		}
		$adInfo = M('t'.$tb.'sample')
			->cache(true,60)
			->field('tad.fadname')
			->join('tad on tad.fadid = t'.$tb.'sample.fadid')
			->where(array('t'.$tb.'sample.fid'=>$issueInfo['fsampleid']))
			->find();
		$m3u8_url = A('Common/Media','Model')->get_m3u8($issueInfo['fmediaid'],$start_time,$end_time);
		$post_data = array(
			'start_time'=>$start_time,
			'end_time'=>$end_time,
			'm3u8_url'=>urlencode($m3u8_url),
			'file_type'=>$file_type,
			'back_url'=>urlencode($back_url),
			'file_name'=>urlencode(date('YmdHis',$start_time).'_'.$adInfo['fadname'].'_'.$table_name.'_'.$issueid),
		);
		$ret = http($cut_url,$post_data,'GET',false,60);
		//var_dump($ret);
		$this->ajaxReturn(array('code'=>0,'msg'=>'正在生成'));						
	}
	
	/*修改发布记录视频片段地址*/
	public function edit_ad_part(){
		$post_data = file_get_contents('php://input');
		$post_data = json_decode($post_data,true);
		S('issue_m3u8_'.$_GET['table_name'].'_'.$_GET['issueid'],$post_data,864000);
	}
	
	/*获取广告片段生成状态*/
	public function get_ad_part(){
		session_write_close();
		$issueid = I('fid');//发布记录id
		$fstarttime = I('fstarttime',date('Y-m-01'));// 发布日期起始
		//$fendtime = I('fendtime',date('Y-m-d'));// 发布日期结束
		$tableName = 'food_drug_issue_'.date('Ym',strtotime($fstarttime));//按查询日期确定表名
		$hasTable = M()->query('SHOW TABLES LIKE "'.$tableName.'"');//判断表是否存在
		if (!$hasTable){
			$this->ajaxReturn(array('code'=>1,'msg'=>'数据表不存在'));
		}
		$data = S('issue_m3u8_'.$table_name.'_'.$issueid);
		if($data && intval($data['code']) >= 0){
			$this->ajaxReturn($data);
		}else{
			$this->ajaxReturn(array('code'=>-100,'msg'=>'请等待'));
		}
	}
}