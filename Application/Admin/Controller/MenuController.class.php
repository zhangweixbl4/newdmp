<?php
namespace Admin\Controller;
use Think\Controller;
class MenuController extends BaseController {
	/**
	 * 菜单管理列表
	 * by zw
	 */
	public function index(){
		if(I('menutype') == ''){
			$_GET['menutype'] = 20;
		}
		$menutype 	= I('menutype');//菜单类型

		$do_nme = M('new_menutype')->field('me_id,me_name,me_state,me_content')->where('me_state=10')->order('me_id asc')->select();//菜单类型列表

		$do_nme2 = M('new_menutype')->field('me_id,me_name,me_state,me_content')->where('me_state=10 and me_id='.$menutype)->find();//选中的菜单类型详情

		$allmenu = D('Menu')->m_allmenulist(0,$menutype);//所有菜单列表

		$this->assign('menutypelist',$do_nme);
		$this->assign('menutypeview',$do_nme2);
		$this->assign('menulist',json_encode($allmenu));
		$this->display();
	}

	/**
	 * 添加新菜单类型动作
	 * by zw
	 */
	public function addmenutype_action(){
		$me_name 		= I('me_name');//类型名称
		$me_state 		= I('me_state');//状态
		$me_content 	= I('me_content');//备注

		$db_nme = M('new_menutype');
		$data_nme['me_name'] 			= $me_name;
		$data_nme['me_state'] 			= $me_state;
		$data_nme['me_content'] 		= $me_content;
		$data_nme['me_createtime'] 		= date('Y-m-d H:i:s');
		$data_nme['me_createpersonid'] 	= session('personInfo.fid');
		
		$do_nme = $db_nme->add($data_nme);
		if(!empty($do_nme)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功','data'=>$do_nme));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'添加失败'));
		}
	}

	/**
	 * 修改新菜单类型动作
	 * by zw
	 */
	public function editmenutype_action(){
		$me_id 			= I('me_id');//菜单类型id
		$me_name 		= I('me_name');//类型名称
		$me_state 		= I('me_state');//状态
		$me_content 	= I('me_content');//备注

		$db_nme = M('new_menutype');
		$data_nme['me_name'] 			= $me_name;
		$data_nme['me_state'] 			= $me_state;
		$data_nme['me_content'] 		= $me_content;
		$data_nme['me_edittime'] 		= date('Y-m-d H:i:s');
		$data_nme['me_editpersonid'] 	= session('personInfo.fid');
		
		$do_nme = $db_nme->where('me_id='.$me_id)->save($data_nme);
		if(!empty($do_nme)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'修改失败'));
		}
	}

	/**
	 * 添加新菜单动作
	 * by zw
	 */
	public function addmenu_action(){
		$menu_name 		= I('menu_name');//菜单名称
		$menu_url 		= I('menu_url');//菜单链接
		$parent_id 		= I('parent_id');//父级ID
		$menu_sort 		= I('menu_sort');//排序
		$menu_state 	= I('menu_state');//状态
		$icon 			= I('icon');//图标
		$menu_default 	= I('menu_default')?I('menu_default'):0;//是否默认
		$menu_type 		= I('menu_type');//菜单类型
		$menu_code 		= I('menu_code');//菜单标识
		$menu_content 	= I('menu_content');//备注

		$db_nmu = M('new_agpmenu');
		$data_nmu['menu_name'] 			= $menu_name;
		$data_nmu['menu_url'] 			= $menu_url;
		$data_nmu['parent_id'] 			= $parent_id;
		$data_nmu['menu_sort'] 			= $menu_sort;
		$data_nmu['menu_state'] 		= $menu_state;
		$data_nmu['icon'] 				= $icon;
		$data_nmu['menu_default'] 		= $menu_default;
		$data_nmu['menu_type'] 			= $menu_type;
		$data_nmu['menu_code'] 			= $menu_code;
		$data_nmu['menu_content'] 		= $menu_content;
		$data_nmu['menu_createtime'] 	= date('Y-m-d H:i:s');
		$data_nmu['menu_createpersonid'] = session('personInfo.fid');
		
		$do_nmu = $db_nmu->add($data_nmu);
		if(!empty($do_nmu)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功','data'=>$do_nmu));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'添加失败'));
		}
	}

	/**
	 * 修改新菜单动作
	 * by zw
	 */
	public function editmenu_action(){
		$menu_id 		= I('menu_id');//菜单ID
		$menu_name 		= I('menu_name');//菜单名称
		$menu_url 		= I('menu_url');//菜单链接
		$parent_id 		= I('parent_id');//父级ID
		$menu_sort 		= I('menu_sort');//排序
		$menu_state 	= I('menu_state');//状态
		$icon 			= I('icon');//图标
		$menu_default 	= I('menu_default')?I('menu_default'):0;//是否默认
		$menu_type 		= I('menu_type');//菜单类型
		$menu_code 		= I('menu_code');//菜单标识
		$menu_content 	= I('menu_content');//备注

		$db_nmu = M('new_agpmenu');

		$data_nmu['menu_name'] 			= $menu_name;
		$data_nmu['menu_url'] 			= $menu_url;
		$data_nmu['parent_id'] 			= $parent_id;
		$data_nmu['menu_sort'] 			= $menu_sort;
		$data_nmu['menu_state'] 		= $menu_state;
		$data_nmu['icon'] 				= $icon;
		$data_nmu['menu_default'] 		= $menu_default;
		$data_nmu['menu_type'] 			= $menu_type;
		$data_nmu['menu_code'] 			= $menu_code;
		$data_nmu['menu_content'] 		= $menu_content;
		$data_nmu['menu_edittime'] 		= date('Y-m-d H:i:s');
		$data_nmu['menu_editpersonid'] 	= session('personInfo.fid');

		$do_nmu = $db_nmu->where('menu_id='.$menu_id)->save($data_nmu);
		if(!empty($do_nmu)){
			$do_nmu2 = $db_nmu->field('menu_type')->where('menu_id='.$menu_id)->find();
			if($do_nmu2['menu_type']!=$menutype){//更改的菜单类型与实际不符时，需要同步其他表
				M()->execute('update tregulatormenu set fmenutype='.$menu_type.' where fmenuid='.$menu_id);//更新所有拥有主菜单机构的菜单权限类型
				M()->execute('update tregulatorpersonmenu set fmenutype='.$menu_type.' where fmenuid='.$menu_id);//更新所有拥有主菜单人员的菜单权限类型

				$db_nmu->execute('update new_agpmenu set menu_type='.$menu_type.' where parent_id='.$menu_id);//更新主菜单下的所有子菜单类型
				$do_nmu3 = $db_nmu->field('menu_id')->where('parent_id='.$menu_id)->select();//读取所有子菜单
				foreach ($do_nmu3 as $key => $value) {
					M()->execute('update tregulatormenu set fmenutype='.$menu_type.' where fmenuid='.$value['menu_id']);//更新所有拥有主菜单机构的菜单权限类型
					M()->execute('update tregulatorpersonmenu set fmenutype='.$menu_type.' where fmenuid='.$value['menu_id']);//更新所有拥有主菜单人员的菜单权限类型
				}
			}
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'修改失败'));
		}
	}

	/**
	 * 删除新菜单动作
	 * by zw
	 */
	public function delmenu_action(){
		$menu_id = I('menu_id');//菜单ID

		$db_nmu = M('new_agpmenu');
		$do_nmu2 = $db_nmu->where('parent_id='.$menu_id)->count();
		if(!empty($do_nmu2)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'有未删除的子菜单'));
		}
		$do_nmu = $db_nmu->where('menu_id='.$menu_id)->delete();
		if(!empty($do_nmu)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败'));
		}
	}
}