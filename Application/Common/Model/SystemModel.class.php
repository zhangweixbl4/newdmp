<?php
namespace Common\Model;
use Think\Exception;

class SystemModel{
	
	/*获取地区代码的前面部分*/
	public function get_region_left($regionId){
		$regionId = strval($regionId);
		
		$regionIdLength = strlen($regionId);
		$regionLastId = substr($regionId,-2);
		$regionLeftId = substr($regionId,0,$regionIdLength - 2);
	
		if($regionLastId === '00'){
			return $this->get_region_left($regionLeftId);
		}else{
			
			return $regionId;
		}
		
	}
	
	/*写入修改日志*/
	public function write_save_log(){
		
	}
	
	/*获取、设置系统重要参数*/
    public function important_data($key_name,$value = null,$vv = null){
		
		$lock = $vv['lock'];
		if($lock){
			
			M('important_data')->where(array('fname'=>$key_name))->save(array('fadditional'=>rand(1000000,99999999)));
			
		}

		$important_data_info = M('important_data')->master(true)->lock($lock)->where(array('fname'=>$key_name))->find();//查询信息

		if($value === null){//判断是查询还是写入
			return $important_data_info['fvalue'];//如果是查询，直接返回value
			
		}else{//如果是写入
			if(!$important_data_info){
				try{//尝试新增数据
					$fid = M('important_data')->add(array('fname'=>$key_name,'fvalue'=>$value));//尝试新增数据
				}catch(Exception $e) { //如果新增失败则执行修改
					$save_start = M('important_data')->where(array('fname'=>$key_name))->save(array('fvalue'=>$value));
				} 
			}else{
				$save_start = M('important_data')->where(array('fname'=>$key_name))->save(array('fvalue'=>$value));
			}
			return;
		}
		
		

		
		
    }
	
	/*拼装insert into sql*/
	
	public function insert_sql($tableName,$a_data_all){
		if(!$a_data_all) return false;
		
		foreach($a_data_all as $kField => $aData){
			if($kField == 0){//第0个元素，获取字段内容
				foreach($aData as $field => $llll){//循环字段内容
					$fields[] = '`'.$field.'`';
				}
				$fields = implode(',',$fields);
			}
			$valList = array();
			foreach($aData as $val){
				
				if( is_string($val)){//判断是否为字符串
					$valList[] = "'".$val."'";//前后加上引号
				}else{
					$valList[] = $val;
				}
			}
			$vals = '('.implode(',',$valList).')';//字段值用逗号拼接
			$valueList[] = $vals;
			
		}
		$values = implode(',',$valueList);
		
		
		$e_sql = 'INSERT INTO `'.$tableName.'` ('.$fields.') VALUES '.$values;
		return $e_sql;
	}
	
	
	/*异步GET请求*/
	function async_get($url){  
		
		$url_pieces = parse_url($url);  //解析url  
		
		$path =(isset($url_pieces['path']))?$url_pieces['path']:'/';  //设置正确的路径
		$port =(isset($url_pieces['port']))?$url_pieces['port']:'80';  //设置正确的端口号  
		
		if($fp =fsockopen($url_pieces['host'],$port,$errno,$errstr,30)){  //用fsockopen()尝试连接  
			$send = "HEAD $path HTTP/1.1\r\n";  
			$send .= 'HOST:'.$url_pieces['host']."\r\n"; 
			$send .= "CONNECTION: CLOSE\r\n\r\n";  
			fwrite($fp,$send);  
			fclose($fp); 
			return true;
		}else{
			return false;
		}
		   
	}
	
	
	/*异步GET请求（使用nodejs）*/
	public function async_get_node($urlArr){
		
		if(strstr(php_uname(),'Linux')){
			$cmd = '/home/nodeJs '.$_SERVER['DOCUMENT_ROOT'].'/Public/nodeJs/async.js';
		}else{
			$cmd = $_SERVER['PATH_TRANSLATED'].'\Public\nodeJs\node '.$_SERVER['PATH_TRANSLATED'].'\Public\nodeJs\async.js';
		}

		$urlStr = implode(',',$urlArr);
		exec($cmd.' '.$urlStr,$aa,$bb);
		//var_dump($cmd.' '.$urlStr);
		
		return json_decode($aa[0],true);

	}
	
	
	/*API请求日志记录*/
	public function api_call_log(){
	
		$fname = MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME ;
		$fdate = date('Y-m-d') ;
		$fip = get_client_ip();
		
		$aInfo = M('api_call_log')->where(array('fname'=>$fname,'fdate'=>$fdate,'fip'=>$fip))->find();
		
		
		if($aInfo){

			M('api_call_log')->where(array('fid'=>$aInfo['fid']))->save(array('fcount'=>array('exp', 'fcount + 1'),'last_update_time'=>date('Y-m-d H:i:s')));

		}else{
			try{//尝试新增数据
				M('api_call_log')->add(array(
											'fcount'=>'1',
											'last_update_time'=>date('Y-m-d H:i:s'),
											'fname'=>$fname,
											'fdate'=>$fdate,
											'fip'=>$fip
											
											));
			
			}catch( \Exception $e) { //如果新增失败则获取id
				
				M('api_call_log')
								->where(array('fname'=>$fname,'fdate'=>$fdate,'fip'=>$fip))
								->save(array('fcount'=>array('exp', 'fcount + 1'),'last_update_time'=>date('Y-m-d H:i:s')));
			
			} 
			
		}
		
		
		
		
		
		
		
		
		
	
	}
	

}