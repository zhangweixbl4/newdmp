<?php
namespace InputPc\Controller;
use Think\Controller;
class NetsamController extends BaseController {
	

	

    public function index(){
		
		$wx_id = intval(session('wx_id'));	
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$keyword = I('keyword');//搜索关键词
		$adclass_code = I('adclass_code');
		$inputstate = I('inputstate');//录入状态
		$inspectstate = I('inspectstate');//初审状态
		$fillegaltypecode = I('fillegaltypecode');//违法类型
		$net_platform = I('net_platform');//平台类型
		
		
		$fissuedate_s = I('fissuedate_s');
		if($fissuedate_s == ''){
			$fissuedate_s = date('Y-m-d',time()-30*86400);
			$_GET['fissuedate_s'] = $fissuedate_s;
		}	
		$fissuedate_s = strtotime($fissuedate_s);
		
		$fissuedate_e = I('fissuedate_e');
		if($fissuedate_e == ''){
			$fissuedate_e = date('Y-m-d');
			$_GET['fissuedate_e'] = $fissuedate_e;
		}
		$fissuedate_e = strtotime($fissuedate_e);		
		
		cookie('net_ad_get_next_task',array('keyword'=>$keyword,'adclass_code'=>$adclass_code,'inputstate'=>$inputstate,'inspectstate'=>$inspectstate));
		$where = array();
		
		if($net_platform == 1){
			$where['net_platform'] = 1;
		} elseif($net_platform == 2){
			$where['net_platform'] = 2;
			$where['source_type'] = array('neq',4);
		} elseif($net_platform == 3){
			$where['net_platform'] = 2;
			$where['source_type'] = 4;
		}
		
		
		
		
		$where['_string'] .= '(finputstate = 1 or ';//信息未录入
		$where['_string'] .= 'finspectstate = 1 or ';//违法未判定
		
		$where['_string'] .= 'finputuser = '.$wx_id.' or ';//自己录入的
		$where['_string'] .= 'finspectuser = '.$wx_id.') ';//自己判定的
		$user_data_authority = str_replace(
											array('tad.fadclasscode','sample.fissuedate'),
											array('sample.fadclasscode','sample.net_created_date'),
											A('InputPc/Task','Model')->user_data_authority($wx_id)
											
											);
		
		$where['_string'] .= $user_data_authority;//数据权限控制
		//var_dump($user_data_authority);
		if($fillegaltypecode != ''){
			$where['sample.fillegaltypecode'] = $fillegaltypecode;//违法类型
		}
		
		$where['isad'] = 1;
		//$where['source_type'] = 4;
		
		if($inputstate != ''){
			$where['finputstate'] = $inputstate;//录入状态
		} 
		if($inspectstate != ''){
			$where['finspectstate'] = $inspectstate;//初审状态
		} 
		
		
		if($adclass_code != ''){
			$adclass_code_strlen = strlen($adclass_code);//获取code长度
			$where['left(sample.fadclasscode,'.$adclass_code_strlen.')'] = $adclass_code;//广告分类搜索条件

		
		}
		if($keyword != ''){
			$where['sample.fadname|tmedia.fmedianame'] = array('like','%'.$keyword.'%');
		} 
		$where['(sample.net_created_date) / 1000'] = array('between',array($fissuedate_s,$fissuedate_e));

		$count = M('tnetissue')
								
								->alias('sample')
								->join('tadclass on tadclass.fcode = sample.fadclasscode')
								->join('tmedia on tmedia.fid = sample.fmediaid')
								->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
								
								
								->where($where)->count();
		
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$samList = M('tnetissue')
								->alias('sample')
								->field('
										sample.major_key,
										sample.fadname,

										tadclass.ffullname as adclass_fullname,
										tillegaltype.fillegaltype,
										
										tmedia.fmedianame,
										
										sample.net_created_date,
										sample.net_platform,
										sample.finputstate,
										sample.finspectstate,
										sample.finputuser,
										sample.finspectuser
										
										')
								->join('tadclass on tadclass.fcode = sample.fadclasscode')
								->join('tmedia on tmedia.fid = sample.fmediaid')
								->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
								->order('rand()')
								->limit($Page->firstRow.','.$Page->listRows)
								->where($where)->select();
		//var_dump($samList);
		//var_dump(M('tnetissue')->getLastSql());
		$samList = list_k($samList,$p,$pp);//为列表加上序号
		$this->assign('samList',$samList);
		$this->assign('wx_id',$wx_id);
		
		$illegaltypeList = M('tillegaltype')->cache(true,600)->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		$this->assign('illegaltypeList',$illegaltypeList);//违法类型数据列表
		$this->assign('page',$Page->show());//分页输出
		//exit;
		$this->display();
	}
	
	
	public function sample_edit(){
		$major_key = I('major_key');//获取样本ID
		$wx_id = intval(session('wx_id'));
		
		$task_wx_id = intval(S('net_task_'.$fid));
		if($wx_id != $task_wx_id && $task_wx_id != 0){
			$task_wx_info = M('ad_input_user')->cache(true,86400)->field('wx_id,nickname')->where(array('wx_id'=>$task_wx_id))->find();
		}
		$this->assign('task_wx_info',$task_wx_info);

		S('net_task_'.$fid,$wx_id,300);

		$NetAdDetails = M('tnetissue')
										->field('
													tnetissue.*,
													tadowner.fname as adowner_name,
													tadclass.ffullname,
													tillegaltype.fillegaltype,
													tmedia.fmedianame,
													tmedia.fmediacode,
													(select nickname from ad_input_user where wx_id = tnetissue.finputuser) as finputusername
													')
										->join('tadclass on tadclass.fcode = tnetissue.fadclasscode')
										->join('tmedia on tmedia.fid = tnetissue.fmediaid')
										->join('tadowner on tadowner.fid = tnetissue.fadowner','left')
										->join('tillegaltype on tillegaltype.fcode = tnetissue.fillegaltypecode')
										->where(array('tnetissue.major_key'=>$major_key))
										->find();//查询样本详情
		$NetAdDetails['adowner_name'] = M('tdomain')->where(array('fdomain'=>$NetAdDetails['net_advertiser']))->getField('fname');
		$NetAdDetails['fmedianame2'] = M('tdomain')->where(array('fdomain'=>$NetAdDetails['fmediacode']))->getField('fname');
		
		$NetAdDetails['thumb_url_true'] = str_replace('adlife.com.cn','hz-data.com',$NetAdDetails['thumb_url_true']);
		$NetAdDetails['net_url'] = str_replace('adlife.com.cn','hz-data.com',$NetAdDetails['net_url']);
		
		//var_dump(M('tdomain')->getLastSql());


		
		$NetAdillegal = M('tillegal')
												->field('fcode as fillegalcode,fexpression')
												->where(array('fcode'=>array('in',explode(',',$NetAdDetails['fexpressioncodes']))))->select();
		// var_dump($papersampleDetails);
		
		$NetAdDetails['article_content'] = str_replace(array('mmbiz.qpic.cn','width: auto !important;'),array('hz-weixin-img.oss-cn-hangzhou.aliyuncs.com',''),$NetAdDetails['article_content']);
		

		$this->assign('user_other_authority_1002',A('InputPc/Task','Model')->user_other_authority('1002'));//违法初审权限
	
		
		
		
		$this->assign('NetAdDetails',$NetAdDetails);
		$this->assign('NetAdillegal',$NetAdillegal);
		$illegaltypeList = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
		$this->assign('illegaltypeList',$illegaltypeList);//违法类型数据列表
		$this->assign('IllegalList',$IllegalList);//违法表现数据列表
		$this->display();
	}
	
	
	/*信息录入*/
	public function edit_sample(){
		$wx_id = intval(session('wx_id'));
		$wxInfo = M('ad_input_user')->field('wx_id,nickname,input_seniority')->where(array('wx_id'=>$wx_id))->find();
		if(	!A('InputPc/Task','Model')->user_other_authority('1001')){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
		}
		$major_key = intval(I('major_key'));//获取样本ID
	
		$netAdInfo = M('tnetissue')->where(array('major_key'=>$major_key))->find();
		
		
		
		
		$fadname = I('fadname');//广告名
		$fbrand = I('fbrand');// 广告品牌
		$adclass_code = I('adclass_code');//广告分类code
		$adowner_name = I('adowner_name');//广告主
		$fspokesman = I('fspokesman');//代言人

		$fadmanuno = I('fadmanuno');//广告中标识的生产批准文号
		$fmanuno = I('fmanuno');//生产批准文号
		$fadapprno = I('fadapprno');//广告中标识的广告批准文号
		$fapprno = I('fapprno');//广告批准文号
		$fadent = I('fadent');//广告中标识的生产企业（证件持有人）名称
		$fent = I('fent');//生产企业（证件持有人）名称
		$fentzone = I('fentzone');//生产企业（证件持有人）所在地区
		
		$fmedianame2 = I('fmedianame2');
		
		
		
		$a_e_data['fadname'] = $fadname;//广告名称
		$a_e_data['fbrand'] = $fbrand;//品牌
		$a_e_data['fspokesman'] = $fspokesman;//代言人
		$a_e_data['fadclasscode'] = $adclass_code;//代言人
		$a_e_data['fadowner'] = A('Common/Adowner','Model')->get_ad_owner_id($adowner_name,$wxInfo['nickname']);

		$a_e_data['finputstate'] = 2;//录入状态
		$a_e_data['finputuser'] = $wx_id;//信息录入人
		$a_e_data['fmodifier'] = $wxInfo['nickname'];//修改人
		$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		
		$a_e_data['fadmanuno'] = $fadmanuno;//广告中标识的生产批准文号
		$a_e_data['fmanuno'] = $fmanuno;//生产批准文号
		$a_e_data['fadapprno'] = $fadapprno;//广告中标识的广告批准文号
		$a_e_data['fapprno'] = $fapprno;//广告批准文号
		$a_e_data['fadent'] = $fadent;//广告中标识的生产企业（证件持有人）名称
		$a_e_data['fent'] = $fent;//生产企业（证件持有人）名称
		$a_e_data['fentzone'] = $fentzone;//生产企业（证件持有人）所在地区
		if(M('tadclass')->cache(true,86400)->where(array('fcode'=>$adclass_code))->count() == 0){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'广告类别错误'));
		} 

		if($fadname == ''){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'请输入广告名称'));
		}

		$rr = M('tnetissue')->where(array('major_key'=>$major_key,'finputstate'=>1))->save($a_e_data);//修改数据		
		//var_dump(M('tnetissue')->getLastSql());
		
		
		if(M('tdomain')->where(array('fdomain'=>$netAdInfo['net_advertiser']))->count() == 0){
			M('tdomain')->add(array('fname'=>$adowner_name,'fdomain'=>$netAdInfo['net_advertiser']));
		}else{
			M('tdomain')->where(array('fdomain'=>$netAdInfo['net_advertiser']))->save(array('fname'=>$adowner_name));
		}
		
		
		
		if($fmedianame2 != ''){
			$mediaInfo = M('tmedia')->cache(true,5)->where(array('fid'=>$netAdInfo['fmediaid']))->find();//查询媒介信息
			
			if(M('tdomain')->where(array('fdomain'=>$mediaInfo['fmediacode']))->count() == 0){
				M('tdomain')->add(array('fname'=>$fmedianame2,'fdomain'=>$mediaInfo['fmediacode']));
			}else{
				M('tdomain')->where(array('fdomain'=>$mediaInfo['fmediacode']))->save(array('fname'=>$fmedianame2));
			}
			
			if($mediaInfo['fmedianame'] !=  $fmedianame2){
				$e_media_state = M('tmedia')->where(array('fmediacode'=>$mediaInfo['fmediacode']))->save(array('fmedianame'=>$fmedianame2));
			}
		}
		
		
		
		
		if($rr > 0){
			A('InputPc/Task','Model')->change_task_count($wx_id,'net_input',1);
			
			
			$next_task = $this->get_next_task();

			if($next_task){
				$next_url = U('InputPc/Netsam/sample_edit',array('major_key'=>$next_task['major_key']));
			}
			
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功','next_url'=>strval($next_url)));
		}else{

			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败,原因未知','next_url'=>''));
		}

	}
	
	/*编辑电视广告样本违法*/
	public function edit_sample_illegal(){
		$wx_id = intval(session('wx_id'));
		$wxInfo = M('ad_input_user')->field('wx_id,nickname,audit_seniority')->where(array('wx_id'=>$wx_id))->find();
		if(	!A('InputPc/Task','Model')->user_other_authority('1002')){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
		}
		$major_key = intval(I('major_key'));//获取样本ID


		$fillegalcontent = I('fillegalcontent');//违法内容
		$fexpressioncodes = I('fexpressioncodes');//违法代码
		$fillegaltypecode = I('fillegaltype');//违法类型

		if(intval($fillegaltypecode) > 0 && ($fillegalcontent == '' || $fexpressioncodes == '')){//选择了违法程度，没有填写违法内容或者没有选择违法表现
			$this->ajaxReturn(array('code'=>-1,'msg'=>'选择错误,没有填写违法内容或没有选择违法表现'));
		}
		
		if(intval($fillegaltypecode) == 0 && ($fillegalcontent != '' || $fexpressioncodes != '')){//选择了不违法，但是填写了违法内容或者选择了违法表现
			$this->ajaxReturn(array('code'=>-1,'msg'=>'选择错误,不违法不允许填写违法内容或选择违法表现'));
		}
		

		$illegal['fillegalcontent'] = $fillegalcontent;//违法内容
		$illegal['fexpressioncodes'] = $fexpressioncodes;//违法代码
		
		$illegal['finspectstate'] = 2;//判定状态
		$illegal['finspectuser'] = $wx_id;//违法审查人
		$illegal['fmodifier'] = $wxInfo['nickname'];//修改人
		$illegal['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$illegal['fillegaltypecode'] = $fillegaltypecode;//违法类型
		
		
		$rr_illegal = M('tnetissue')->where(array('major_key'=>$major_key,'finspectstate'=>1))->save($illegal);//修改数据

		if($rr_illegal > 0){
			A('InputPc/Task','Model')->change_task_count($wx_id,'net_inspect',1);

			$next_task = $this->get_next_task('inspect');

			if($next_task){
				$next_url = U('InputPc/Netsam/sample_edit',array('major_key'=>$next_task['major_key']));
			}
			
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功','next_url'=>strval($next_url)));
		}else{

			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败,原因未知','next_url'=>''));
		}

	}
	
	/*退回重新录入*/
	public function back_sample(){
		$wx_id = intval(session('wx_id'));
		$major_key = intval(I('major_key'));//广告样本ID

		$where = array();
		$where['finputuser'] = $wx_id;
		$where['major_key'] = $major_key;
		
		$rr = M('tnetissue')->where($where)->save(array('finputstate'=>1,'finputuser'=>0));
		if($rr > 0){
			A('InputPc/Task','Model')->change_task_count($wx_id,'net_input',-1);
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败'));
		}
		
	}
	
	/*退回重新初审*/
	public function back_sample_illegal(){
		$wx_id = intval(session('wx_id'));
		$major_key = intval(I('major_key'));//广告样本ID
		$where = array();
		$where['finspectuser'] = $wx_id;
		$where['major_key'] = $major_key;
		
		$rr = M('tnetissue')->where($where)->save(array('finspectstate'=>1,'finspectuser'=>0));
		if($rr > 0){
			A('InputPc/Task','Model')->change_task_count($wx_id,'net_inspect',-1);
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败'));
		}
		
	}
	
	
	/*提交剪辑错误*/
	public function is_not_ad(){
		$wx_id = intval(session('wx_id'));
		$major_key = intval(I('major_key'));//广告样本ID
		
		$where = array();
		$where['major_key'] = $major_key;
		$where['isad'] = 1;

		
		$e_data = array();
		$e_data['isad'] = 0;
		$e_data['notaduser'] = $wx_id;
		
		$e_state = M('tnetissue')->where($where)->save($e_data);
		
		if($e_state){
			$this->ajaxReturn(array('code'=>0,'msg'=>'标记成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'标记失败'));
		}
		
	}
	
	
	
	/*获取下一个任务*/
	public function get_next_task($type){
		$type = strval($type);
		$wx_id = intval(session('wx_id'));	
		
		
		$net_ad_get_next_task = cookie('net_ad_get_next_task');
		
		$keyword = $net_ad_get_next_task['keyword'];

		$adclass_code = $net_ad_get_next_task['adclass_code'];
		
		$where['_string'] .= '(finputstate = 1 or ';//信息未录入
		$where['_string'] .= 'finspectstate = 1 or ';//违法未判定
		
		$where['_string'] .= 'finputuser = '.$wx_id.' or ';//自己录入的
		$where['_string'] .= 'finspectuser = '.$wx_id.') ';//自己判定的
		//$where['_string'] .= A('InputPc/Task','Model')->user_data_authority($wx_id);//数据权限控制
		
		$user_data_authority = str_replace(
											array('tad.fadclasscode','sample.fissuedate'),
											array('sample.fadclasscode','sample.net_created_date'),
											A('InputPc/Task','Model')->user_data_authority($wx_id)
											
											);
		
		$where['_string'] .= $user_data_authority;//数据权限控制
		$where['isad'] = 1;
		
		if(!$type){
			$where['finputstate'] = 1;//录入状态
		}elseif($type == 'inspect'){
			$where['finspectstate'] = 1;//初审状态
		} 
		

		if($adclass_code != ''){
			$adclass_code_strlen = strlen($adclass_code);//获取code长度
			$where['left(sample.fadclasscode,'.$adclass_code_strlen.')'] = $adclass_code;//广告分类搜索条件

		
		}
		if($keyword != ''){
			$where['sample.fadname|tmedia.fmedianame'] = array('like','%'.$keyword.'%');
		} 
		
		

		$samInfo = M('tnetissue')
								->alias('sample')
								->field('
										sample.major_key
										
										')
								->join('tadclass on tadclass.fcode = sample.fadclasscode')
								->join('tmedia on tmedia.fid = sample.fmediaid')
								->order('rand()')
								->where($where)->find();
		return $samInfo;
	}
	
	
	/*退回重新录入*/
	public function back_input(){
		
		if(	!A('InputPc/Task','Model')->user_other_authority('1002')){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
		}
		
		$wx_id = intval(session('wx_id'));
		$major_key = I('major_key');//主键ID
		$back_input_reason = I('back_input_reason');//错误原因
		
		$netAdInfo = M('tnetissue')->where(array('major_key'=>$major_key))->find();
		
		if($netAdInfo['finputstate'] != 2){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该广告状态不允许退回'));
		}
		
		$e_data = array();
		$e_data['finputstate'] = 1;
		$e_data['finputuser'] = 0;
		$e_data['back_input_reason'] = $back_input_reason;
		$e_data['back_input_user'] = $wx_id;
		
		
		$e_state = M('tnetissue')->where(array('major_key'=>$major_key,'finputstate'=>2))->save($e_data);
		
		if($e_state){
			A('InputPc/Task','Model')->change_task_count($wx_id,'input_err',1);
			$this->ajaxReturn(array('code'=>0,'msg'=>'退回成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'退回失败,原因未知'));
		}
		
		
		
		
		
		
		
		
	}
	
	
	
	
	

	

}