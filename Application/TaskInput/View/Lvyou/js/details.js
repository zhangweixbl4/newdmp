Vue.prototype.$ELEMENT = {
  size: "mini",
  zIndex: 3000
};

const vm = new Vue({
  el: "#app",
  // components: { commonHeader },
  data() {
    const _destinationList = destinationList.map(i => {
      return {
        label: i,
        value: i
      };
    });
    return {
      details: {},
      form: {
        shen_source_id: "",
        __id: "",
        platform: "",
        download_time: "",
        download_url: "",
        proname: "",
        score: "",
        bad_comment_count: "",
        travelcount: "",
        depart_province: "",
        departurecityname: "",
        destination: "",
        vendor_name: "",
        vendor_brand: "",
        days: "",
        traffic_type: "",
        type: "",
        min_price: "",
        max_price: "",
        trip_remark: "",
        self_pay_num: "",
        shoping_num: "",
        snapshot_img: ""
      },
      regionList: regionList,
      tripTypes: tripTypes,
      destinationList: _destinationList,
      fileUp: fileUp,
      uploading: false,
      rules: {
        proname: [
          { required: true, message: "请输入产品名称", trigger: "blur" }
        ],
        score: [{ required: true, message: "请输入评分", trigger: "blur" }],
        bad_comment_count: [
          { required: true, message: "请输入差评数量", trigger: "blur" }
        ],
        travelcount: [
          { required: true, message: "请输入出行人数", trigger: "blur" }
        ],
        depart_province: [
          { required: true, message: "请选择出发省份", trigger: "blur" }
        ],
        departurecityname: [
          { required: true, message: "请选择出发地", trigger: "blur" }
        ],
        destination: [
          { required: true, message: "请选择目的地", trigger: "change" }
        ],
        vendor_name: [
          { required: true, message: "请输入旅行社名称", trigger: "blur" }
        ],
        vendor_brand: [
          { required: true, message: "请输入旅行社品牌", trigger: "blur" }
        ],
        days: [{ required: true, message: "请输入行程天数", trigger: "blur" }],
        days: [
          { required: true, message: "请选择交通方式", trigger: "change" }
        ],
        type: [{ required: true, message: "请输入出游类型", trigger: "blur" }],
        min_price: [
          { required: true, message: "请输入最低价", trigger: "blur" }
        ],
        max_price: [
          { required: true, message: "请输入最高价", trigger: "blur" }
        ],
        trip_remark: [
          { required: true, message: "请输入行程说明", trigger: "blur" }
        ],
        self_pay_num: [
          { required: true, message: "请输入自费项目数量", trigger: "blur" }
        ],
        shoping_num: [
          { required: true, message: "请输入购物行程数量", trigger: "blur" }
        ]
      }
    };
  },
  created() {
    this.getDetails();
    this.uploadImg();
  },
  mounted() {
    this.$nextTick(() => {
      document.querySelector("#wrap").style.opacity = 1;
    });
  },
  methods: {
    submit() {
      let fid = getQueryString("fid");
      let data = {
        ...this.form
      };
      $.post(`/TaskInput/Lvyou/edit?fid=${fid}`, data).then(res => {
        if (res.code === 0) {
          this.$notify.success({
            message: "操作成功",
            duration: 1500,
            onClose: () => {
              closePage();
            }
          });
        }
      });
    },
    querySearch(queryString, cb) {
      let list = [...this.destinationList];
      let result = [];
      if (queryString) {
        result = list.filter(i => i.label.indexOf(queryString) !== -1);
        return cb(result);
      }
      // 调用 callback 返回建议列表的数据
      cb(list);
    },
    getDetails() {
      let fid = getQueryString("fid");
      $.post("/TaskInput/Lvyou/data_details", { fid }).then(res => {
        if (res.code === 0) {
          Object.keys(this.form).forEach(i => {
            this.$set(this.form, i, res.data_details[i]);
          });
        }
      });
    },
    setKey(file) {
      const name = `${file.uid || file.lastModified}.${file.type
        .split("/")
        .pop()}`;
      this.fileUp.val.key = name;
      return name;
    },
    uploadSuccess(response, file, fileList) {
      this.uploading = false;
      this.form.snapshot_img = this.fileUp.bucket_url + response.key;
      this.$notify.success({
        message: "文件上传完成！",
        duration: 1500
      });
    },
    uploadError() {
      this.uploading = false;
      this.$notify.error({
        message: "上传出错！",
        duration: 1500
      });
    },
    uploadProgress() {
      this.uploading = true;
    },
    uploadImg() {
      document.addEventListener("paste", event => {
        var docum = $(event.srcElement);
        console.log(event)
        if (docum.attr("id") == "up_snapshot_img_paste") {
          tvalue = docum.val();
          let items = event.clipboardData && event.clipboardData.items;
          let file = null;
          if (items && items.length) {
            // 检索剪切板items
            for (let i = 0; i < items.length; i++) {
              if (items[i].type.indexOf("image") !== -1) {
                file = items[i].getAsFile();

                break;
              }
            }
          }
          if (file) {
            //此时file就是剪切板中的图片文件
            let formData = new FormData();
            formData.append("file", file);
            formData.append("token", this.fileUp.val.token);
            formData.append("key", this.setKey(file));
            $.ajax({
              type: "POST",
              data: formData,
              dataType: "JSON",
              async: false,
              cache: false,
              contentType: false,
              processData: false,
              url: this.fileUp.up_server
            }).then(res => {
              this.form.snapshot_img = this.fileUp.bucket_url + res.key;
              this.$notify.success({
                message: "剪切板中的图像已上传",
                duration: 3000
              });
              setTimeout(function() {
                docum.val(tvalue);
              }, 100);
            });
          } else {
            this.$notify.error({
              message: "你粘贴的不是图片",
              duration: 3000
            });
            setTimeout(function() {
              docum.val(tvalue);
            }, 100);
          }
        }
      });
    }
  }
});
