<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>电视样本录入</title>

    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->

    <link rel="shortcut icon" href="favicon.ico">
    <link href="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/css/bootstrap.min.css-v=3.3.5.css" rel="stylesheet">
    <link href="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/css/animate.min.css" rel="stylesheet">
    <link href="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/css/style.min.css-v=4.0.0.css" rel="stylesheet">
    <link href="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="/Application/Admin/View/Public/tvsample.css" rel="stylesheet">
    <link href="/Application/Admin/View/Public/page.css" rel="stylesheet">
    <style>
        .video_active{
            color:#1ab394;
        }
    </style>
</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal m-t" action="{:U()}" id="Form">
                    {:form_hidden()}
                    <table class="table choose">
                        <tbody>
                            <tr>
                                <td style="width:10%" class="text-right">任务名称：</td>
                                <td style="width:40%">
                                    <input placeholder="请输入查询关键字" type="text" name="keyword" class="form-control" value="{$Think.get.keyword}">
                                </td>
                                <td style="width:10%" class="text-right"></td>
                                <td style="width:40%">
                                    <button class="btn btn-primary pull-right" id="addTask" type="button" data-toggle="modal" data-target="#addTaskModal"><i class="fa fa-plus"></i> 添加任务</button>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    任务状态：
                                </td>
                                <td>
                                    <select class="form-control" name="task_state">
                                        <option value="">全部</option>
                                        <option value="0" <eq name="Think.get.task_state" value="0">selected</eq>>未接收</option>
                                        <option value="1" <eq name="Think.get.task_state" value="1">selected</eq>>已接收</option>
                                        <option value="2" <eq name="Think.get.task_state" value="2">selected</eq>>已录入</option>
                                        <option value="3" <eq name="Think.get.task_state" value="3">selected</eq>>审核通过</option>
                                        <option value="4" <eq name="Think.get.task_state" value="4">selected</eq>>审核未通过</option>
                                        <option value="5" <eq name="Think.get.task_state" value="5">selected</eq>>任务退回</option>
                                        <option value="6" <eq name="Think.get.task_state" value="6">selected</eq>>系统收回</option>
                                    </select>
                                </td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-primary" id="Submit"><i class="fa fa-search"></i> 搜索</button>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="container-fluid">
                    <form class="form-horizontal m-t" action="{:U()}" id="Form2" enctype="multipart/form-data">
                        <div class="fixed-table-container">
                            <table class="table table-hover table-condensed table-striped text-center" style="margin-bottom:0px;">
                                <thead>
                                    <tr>
                                        <th class="text-center"><a target="_blank" href="{:U('Adinput/VideoCutting/login')}">序号</a></th>
                                        <th class="text-center">媒体名称</th>
                                        <th class="text-center">起始时间</th>
                                        <th class="text-center">结束时间</th>
                                        <th class="text-center">持续时间</th>
                                        <th class="text-center">添加时间</th>
                                        <th class="text-center">提交时间</th>
                                        <th class="text-center">审核时间</th>
                                        <th class="text-center">提交用户</th>
                                        <th class="text-center">操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <!-- {//:var_dump($taskList)}   -->
                                    <foreach name="taskList" item="task">
                                        <tr class="mediaTr" data="{$task.Index}">
                                            <td class="text-center">{$task.k}</td>
                                            <td class="text-center">{$task.fmedianame}</td>
                                            <td class="text-center">{:date()}{$task.Start|date='Y-m-d H:i:s',###}</td>
                                            <td class="text-center">{$task.Stop|date='Y-m-d H:i:s',###}</td>
                                            <td class="text-center">{:s_to_m_time($task['Duration']/10)}</td>
                                            <td class="text-center">
                                                <eq name="task.Code" value="0">{$task.Add|date='Y-m-d H:i:s',###}</eq>
                                                <if condition="$task.Code gt '0' "><span class="text-danger">添加失败</span></if>
                                                <eq name="task.Code" value=""><span class="text-success">正在添加</span></eq>
                                            </td>
                                            <td class="text-center">
                                                <neq name="task.Commit" value="">{$task.Commit|date='Y-m-d H:i:s',###}</neq>
                                            </td>
                                            <td>
                                                <neq name="task.Verify" value="">{$task.Verify|date='Y-m-d H:i:s',###}</neq>
                                            </td>
                                            <td>{$task.nickname}</td>
                                            <td>
                                                <neq name="task.Commit" value="">
                                                    <button type="button" class="btn btn-xs btn-primary pass" data="{$task.Index}"><i class="fa fa-check"></i> 通过</button>
                                                    <button type="button" class="btn btn-xs btn-warning back" data="{$task.Index}"><i class="fa fa-close"></i> 不通过</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                                </neq>
                                                <button type="button" class="btn btn-xs btn-danger del" <eq name="task.Code" value="">disabled</eq> data="{$task.Index}" alt="<eq name="task.Code" value="">正在添加的任务无法删除</eq>"><i class="fa fa-trash"></i> 删除</button>
                                            </td>
                                        </tr>
                                    </foreach>
                                    <empty name="taskList">
                                        <tr class="h5 text-center text-primary">
                                            <td colspan="9">没有找到您想要的数据！</td>
                                        </tr>
                                    </empty>
                                </tbody>
                            </table>
                        </div>
                        <div class="pagin">{$page}</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--详细信息弹窗-->
    <div class="modal inmodal" id="infoModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-lg" style="width:1000px;">
            <div class="modal-content animated bounceIn">
                <div class="modal-header">
                    <button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title"></h4>
                    <h3></h3>
                </div>
                <div class="container-fluid">
                    <div class="col-sm-8">
                        <div id="Video" style="width:598px;height:489px"></div>
                    </div>
                    <div class="col-sm-4" id="infoDiv" style="height:489px;overflow-y:auto">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">视频列表</a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">详情</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <table class="table">
                                    <tbody id="VideoList">

                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile">
                                <table class="table choose" style="margin:0;">
                                    <tr>
                                        <th width="90px">媒体名称</th>
                                        <td id="fmedianame"></td>
                                    </tr>
                                    <tr>
                                        <th>起始时间</th>
                                        <td id="Start"></td>
                                    </tr>
                                    <tr>
                                        <th>结束时间</th>
                                        <td id="Stop"></td>
                                    </tr>
                                    <tr>
                                        <th>持续时间</th>
                                        <td id="Duration"></td>
                                    </tr>
                                    <tr>
                                        <th>添加时间</th>
                                        <td id="Add"></td>
                                    </tr>
                                    <tr>
                                        <th>提交时间</th>
                                        <td id="Commit"></td>
                                    </tr>
                                    <tr>
                                        <th>审核时间</th>
                                        <td id="Verify"></td>
                                    </tr>
                                    <tr>
                                        <th>提交用户</th>
                                        <td id="User"></td>
                                    </tr>
                                    <tr>
                                        <th>跳转</th>
                                        <td id="">
                                            <input name="seconds" type="text" id="seconds" class="form-control input-sm" value="20" size="5" maxlength="5">
                                            <!-- <input type="button" name="button" class="btn btn-xs btn-primary" id="button" value="跳转" onclick="CKobject.getObjectById('ckplayer').videoSeek(document.getElementById('seconds').value);"> -->
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" id="close">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 添加任务 -->
    <div class="modal inmodal fade" id="addTaskModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content animated">
                <div class="modal-header">
                    <button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">添加任务</h4>
                </div>
                <div class="container-fluid">
                    <form class="form-horizontal m-t">
                        <div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2">媒体名称：</label>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm col-sm-5" id="media" placeholder="请输入媒体名称">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu dropdown-menu-right" role="menu"></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2">起始时间：</label>
                            <div class="col-sm-5">
                                <input id="Sstart" placeholder="请选择开始时间" name="receive_time_s" minlength="2" type="text" class="form-control input-sm laydate-icon"
                                    autocomplete="off" value="{$Think.get.receive_time_s}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2">结尾时间：</label>
                            <div class="col-sm-5">
                                <input id="Send" placeholder="请选择结束时间" name="receive_time_e" minlength="2" type="text" class="form-control input-sm laydate-icon"
                                    autocomplete="off" value="{$Think.get.receive_time_e}">
                            </div>
                        </div>
                        <div class="form-group" id="backMsg" style="display:none">
                            <label class="col-sm-3 control-label col-sm-offset-2 text-danger">提示：</label>
                            <div class="col-sm-5 text-danger msg" style="padding-top:7px;"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary save"><i class="fa-li fa fa-spinner fa-spin"  style="display:none;position: inherit;"></i> 添加</button>
                </div>
            </div>
        </div>
    </div>

    <script src="http://publichz.oss-cn-hangzhou.aliyuncs.com/jquery/jquery-3.1.1.min.js"></script>
    <script src="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/js/bootstrap.min.js-v=3.3.5"></script>
    <script src="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/js/contabs.min.js"></script>
    <script src="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/laydate/laydate.js"></script>
    <script src="http://publichz.oss-cn-hangzhou.aliyuncs.com/bootstrap-3.3.7-dist/suggest/bootstrap-suggest.min.js"></script>
    <script type="text/javascript" src="http://publichz.oss-cn-hangzhou.aliyuncs.com/m3u8_player/js/offlights.js" charset="utf-8"></script>
    <script type="text/javascript" src="http://publichz.oss-cn-hangzhou.aliyuncs.com/m3u8_player/ckplayer/ckplayer.js" charset="utf-8"></script>
    <script src="/Application/Admin/View/Public/common.js"></script>
    <script>
        "use strict";
        var getstart = function (){
            return CKobject.getObjectById('ckplayer').getStatus().time;
        }
        $(function(){
            let run = null;
            let video_start_time;
            let videoplay = function videoplay(url,start) {
                var video_url = url;//m3u8地址
                var flashvars = { f: 'http://publichz.oss-cn-hangzhou.aliyuncs.com/m3u8_player/m3u8/m3u8.swf', a: video_url, s: 4, c: 0, p: 1 };
                var params = { bgcolor: '#FFF', allowFullScreen: true, allowScriptAccess: 'always', wmode: 'transparent' };
                var video = [video_url];
                CKobject.embed('http://publichz.oss-cn-hangzhou.aliyuncs.com/m3u8_player/ckplayer/ckplayer.swf', 'Video', 'ckplayer', '100%', '100%', false, flashvars, video, params);
                let Stime = 0;
                let now_time;
                let startVideo = $('.video_list').eq(0);
                let Etime;
                run = setInterval(function(){
                    Etime = getstart();
                    now_time = Number(Etime)+Number(start);
                    // console.log('现在播放到：'+now_time+'，下一个开始：'+Number($('.video_active').next().find('.video_list').attr('start'))+'，上一个开始：'+Number($('.video_active').find('.video_list').attr('start')));
                    console.log(Number($('.video_active').find('.video_list').attr('start'))+','+Number(now_time))
                    if($('.video_active').length <= 0){
                        if(Number(startVideo.attr('stop'))>Number(now_time) && Number(startVideo.attr('start'))<Number(now_time)){
                            startVideo.parent().addClass('video_active');
                        }
                    }else{
                        if(Number($('.video_active').next().find('.video_list').attr('start'))>Number(now_time) && Number($('.video_active').find('.video_list').attr('start'))<=Number(now_time)){

                        }else{
                            $('.video_active').removeClass('video_active').next().addClass('video_active');
                        }
                    }
                },500)
            }
            $(document).on('click','.video_list',function(){
                let s =Number($(this).attr('start')) - Number(video_start_time);
                CKobject.getObjectById('ckplayer').videoSeek(s);
                $('.video_active').removeClass('video_active');
                $(this).parent().addClass('video_active');
            })
            $('#infoModal .close,#close').click(function () {
                $('#infoModal').modal('hide');
                $('#Video').empty();
                $('.video_active').removeClass('video_active');
                clearInterval(run);
            })
            $('.mediaTr').click(function () {
                let id = $(this).attr('data');
                let list = '';
                $.post('{:U("Admin/VideoCuttingTask/ajax_task_details")}', { 'Index': id }, function (req) {
                    if (req.code == 0) {
                        $('#infoModal .modal-title').text(req.taskInfo.fmedianame);
                        $('#infoModal h3').text(req.taskInfo.Start_time + ' ~ ' + req.taskInfo.Stop_time);
                        $('#fmedianame').text(req.taskInfo.fmedianame);
                        $('#Start').text(req.taskInfo.Start_time);
                        $('#Stop').text(req.taskInfo.Stop_time);
                        $('#Duration').text(req.taskInfo.Duration);
                        $('#Add').text(req.taskInfo.Add_time);
                        $('#Commit').text(req.taskInfo.Commit);
                        $('#Verify').text(req.taskInfo.Verify);
                        $('#User').text(req.taskInfo.User);
                        video_start_time = Math.floor(req.taskInfo.Start);
                        for(let i = 0;i<req.partLast.length;i++){
                            list += '\
                            <tr>\
                                <td class="video_list" start="'+Math.floor(req.partLast[i].Start)+'" stop="'+Math.floor(req.partLast[i].Stop)+'"><i class="fa fa-play-circle-o"></i> '+req.partLast[i].Start_time+' ~ '+req.partLast[i].Stop_time+'</td>\
                            </tr>\
                            '
                        }
                        $('#VideoList').html(list);
                        videoplay(req.taskInfo.m3u8_url,req.taskInfo.Start);
                    }
                })
                $('#infoModal').modal('show')
            })
        })
        $(function () {
            $('#addTaskModal').show();
            var bsSuggest = $("#media").bsSuggest({
                url: "/index.php?m=Api&c=Media&a=get_live_list&media_name=",				//URL请求地址
                getDataMethod: "url",		//URL方式请求
                idField: "fdpid",           //每组数据的哪个字段作为 data-id，优先级高于 indexId 设置（推荐）
                keyField: "fmedianame",        //每组数据的哪个字段作为输入框内容，优先级高于 indexKey 设置（推荐）
                allowNoKeyword: true,     //是否允许无关键字时请求数据
                showBtn: false,           //是否显示下拉按钮
                effectiveFields: ['fmedianame'],            //有效显示于列表中的字段，非有效字段都会过滤，默认全部，对自定义getData方法无效
                showHeader: false, //显示 header
            })
            $("#media")
                .on('onDataRequestSuccess', function (event, result) {
                    //当 AJAX 请求数据成功时触发，并传回结果到第二个参数
                })
                .on('onSetSelectValue', function (e, keyword) {

                    //当从下拉菜单选取值时触发，并传回设置的数据到第二个参数
                })
                .on('onUnsetSelectValue', function (e) {
                    //当设置了 idField，且自由输入内容时触发（与背景警告色显示同步）
                });
            $('#addTaskModal').hide();
        })
        $(function () {
            $('#Submit').click(function () {
                $('#Form').submit();
            })
            $('.save').click(function () {
                let media = $('#media').attr('data-id');
                let start = $('#Sstart').val();
                let stop = $('#Send').val();
                if (media && start && stop) {
                    $('#backMsg').hide()
                    $('.save').attr('disabled', 'true');
                    $('.fa-li').show();
                    $.post("{:U('Admin/VideoCuttingTask/task_add')}", { 'media': media, 'start': start, 'stop': stop }, function (req) {

                        if (req.code == 0) {
                            location.reload();
                        } else {
                            $('.save').removeAttr('disabled');
                            $('.fa-li').hide();
                            $('#backMsg').show().find('.msg').text(req.msg);
                        }
                    })
                } else {
                    $('#backMsg').show().find('.msg').text('请填写完整信息后提交!');
                }
            })
            $('.del').click(function (event) {
                let task = $(this).attr('data');
                Delete('确定要删除该任务吗？', function () {
                    $.post('{:U("Admin/VideoCuttingTask/task_delete")}', { 'task': task }, function (req) {
                        $('#Delte_yes').attr('disabled', 'true');
                        if (req.code == 0) {
                            location.reload();
                        } else {
                            $('#deleteModal').find('h2').text(req.msg);
                            setTimeout(function () {
                                location.reload();
                            }, 1000)
                        }
                    })
                }, function () {
                    // location.reload();
                })
                event.stopPropagation();
            })
            $('.back').click(function (event) {
                let task = $(this).attr('data');
                Delete('确认该任务审核不通过？', function () {
                    $.post('{:U("Admin/VideoCuttingTask/task_verify")}', { 'task': task, 'approve': 'false' }, function (req) {
                        $('#Delte_yes').attr('disabled', 'true');
                        if (req.code == 0) {
                            location.reload();
                        } else {
                            $('#deleteModal').find('h2').text(req.msg);
                            $('#Delte_yes').hide();
                        }
                    })
                }, function () {
                })
                event.stopPropagation();
            })
            $('.pass').click(function (event) {
                let task = $(this).attr('data');
                Delete('确认该任务通过审核？', function () {
                    $.post('{:U("Admin/VideoCuttingTask/task_verify")}', { 'task': task, 'approve': 'true' }, function (req) {
                        $('#Delte_yes').attr('disabled', 'true');
                        if (req.code == 0) {
                            location.reload();
                        } else {
                            $('#deleteModal').find('h2').text(req.msg);
                            $('#Delte_yes').hide();
                        }
                    })
                }, function () {
                    location.reload();
                })
                event.stopPropagation();
            })
            
            $('#addTaskModal').find('.close').click(function () {
                $('#addTaskModal').modal('hide');
                $('.save').removeAttr('disabled');
                $('#addTaskModal').find('input').val('');
                $('#media').attr('alt', '');
            })
        })
        $(function () {
            laydate.skin('molv');
            let Sstart = {
                elem: '#Sstart',
                format: 'YYYY-MM-DD hh:mm:ss',
                min: '1970-01-01 00:00:00', //设定最小日期为当前日期
                max: '2099-06-16 23:59:59', //最大日期
                istime: true,
                istoday: false,
                choose: function (datas) {
                    Send.min = datas; //开始日选好后，重置结束日的最小日期
                    Send.start = datas //将结束日的初始值设定为开始日
                }
            };
            let Send = {
                elem: '#Send',
                format: 'YYYY-MM-DD hh:mm:ss',
                min: '1970-01-01 00:00:00',
                max: '2099-06-16 23:59:59',
                istime: true,
                istoday: false,
                choose: function (datas) {
                    Sstart.max = datas; //结束日选好后，重置开始日的最大日期
                }
            };
            laydate(Sstart);
            laydate(Send);
            $('#Sstart').focus(function () {
                Send.choose($('#Send').val())
            })
            $('#Send').focus(function () {
                Sstart.choose($('#Sstart').val())
            })
            
        })
    </script>

</body>

</html>