<?php
  header('Content-type: text/html; charset=utf-8');
  include_once('CAS.php');
  phpCAS::client(CAS_VERSION_2_0,'10.0.101.100',8088,'cas');
  phpCAS::setNoCasServerValidation();
  phpCAS::forceAuthentication();
  phpCAS::handleLogoutRequests();
   
  if (isset($_REQUEST['logout'])) {
    $param=array("service"=>'http://'.$_SERVER['HTTP_HOST'].'/Index/Agp/source/login.php');//退出登录后返回
    phpCAS::logout($param);
  }else{
    $usercode = phpCAS::getUser();
    if(!empty($usercode)){
      $key = 'd7ccf6dbf4a4061bc650c8b008c22741';
      $times = date('Y-m-d');
      $token = md5($key.$times.$usercode);
      $gourl = 'http://'.$_SERVER['HTTP_HOST'].'/Index/Agp/#/loginjump?code='.$usercode.'&token='.$token ;
      header('Location: '.$gourl); 
    }
  }
 
 /**
   * 发送HTTP请求方法，目前只支持CURL发送请求
   * @param  string  $url    请求URL
   * @param  array   $params 请求参数
   * @param  string  $method 请求方法GET/POST
   * @return array   $data   响应数据
   */
  function http($url, $params = array(), $method = 'GET',$header = false,$CURLOPT_TIMEOUT = 2,$CURLOPT_HEADER = false){
    $msectime = msectime();
    $http_log = '';
    $http_log .= date('H:i:s').'  '.$url.'  ';
    set_time_limit(360);
    $opts = array(
      CURLOPT_TIMEOUT        => $CURLOPT_TIMEOUT,
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_SSL_VERIFYHOST => false,
    );
    switch(strtoupper($method)){
      case 'GET':
        $getQuerys = !empty($params) ? '?'. http_build_query($params) : '';
        $opts[CURLOPT_URL] = $url . $getQuerys;
        //var_dump($opts[CURLOPT_URL]);
        break;
      case 'POST':
        $opts[CURLOPT_URL] = $url;
        $opts[CURLOPT_POST] = 1;
        $opts[CURLOPT_POSTFIELDS] = $params;
        break;
      case "DELETE":
        $opts[CURLOPT_URL] = $url;
        $opts[CURLOPT_CUSTOMREQUEST] = 'DELETE';
                $opts[CURLOPT_POSTFIELDS] = $params;
                break;

    }
    /* 初始化并执行curl请求 */
    $ch = curl_init();
    // CURLOPT_HEADER启用时会将头文件的信息作为数据流输出。
    curl_setopt ( $ch, CURLOPT_HEADER, $CURLOPT_HEADER );// yuhou.wang

    if($header) curl_setopt ( $ch, CURLOPT_HTTPHEADER, $header );

    curl_setopt_array($ch, $opts);
    $data   = curl_exec($ch);
    $err    = curl_errno($ch);
    $errmsg = curl_error($ch);
    curl_close($ch);

    $http_log .= msectime() - $msectime;
    $http_log .= "\n";
    //file_put_contents('LOG/http_log'.date('Ymd').'.log',$http_log,FILE_APPEND);
    if ($err > 0) {
      return $errmsg;
    }else {
      return $data;
    }
  }

  //返回当前的毫秒时间戳
  function msectime() {
     list($msec, $sec) = explode(' ', microtime());
     $msectime =  (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
     return $msectime;
  }
?>
