<?php
//监管人员个人中心
namespace Gongshang\Controller;
use Think\Controller;
class GongshangController extends BaseController {

	public function my(){
		
		$my_fid = session('regulatorpersonInfo.fid');
		$myInfo = M('tregulatorperson')
							->field('tregulatorperson.*,tregulator.ffullname as org_fullname,tregion.ffullname as region_fullname')
							->join('tregulator on tregulator.fcode = tregulatorperson.fregulatorid')
							->join('tregion on tregion.fid = tregulatorperson.fregionid','LEFT')
							->where(array('tregulatorperson.fid'=>$my_fid))->find();
		//var_dump($myInfo);
		
		$login_log_list = M('gongshang_login_log')
											->join('ip on ip.ip = gongshang_login_log.login_ip','LEFT')
											->where(array('person_id'=>$my_fid))->order('login_time desc')->limit(5)->select();
		//var_dump($login_log_list);
		$this->assign('login_log_list',$login_log_list);
		$this->assign('myInfo',$myInfo);
		$this->assign('file_up',file_up());//上传文件所需的参数
		// var_dump(file_up());
		$this->display();
	}
	
	public function edit_my_info(){
		$my_fid = session('regulatorpersonInfo.fid');
		$fregionid = I('fregionid');//行政区划
		$fduties = I('fduties');//职务
		$fmobile = I('fmobile');//手机号
		$fmail = I('fmail');//电子邮箱
		$is_repeat = M('tregulatorperson')->where(array('fid'=>array('neq',$my_fid),'fmobile'=>$fmobile))->count();//查询手机号是否重复
		if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'手机号码重复,修改失败'));//返回手机号码重复
		$e_data = array();
		$e_data['fregionid'] = $fregionid;
		$e_data['fduties'] = $fduties;
		$e_data['fmobile'] = $fmobile;
		$e_data['fmail'] = $fmail;

		$e_data['fmodifier'] = session('regulatorpersonInfo.fname');//修改人
		$e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$rr = M('tregulatorperson')->where(array('fid'=>$my_fid))->save($e_data);
		//var_dump(M('tregulatorperson')->getLastSql());
		if($rr > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));
		}

	}
	
	
	public function edit_my_password(){
		$my_fid = session('regulatorpersonInfo.fid'); 
		$myInfo = M('tregulatorperson')->where(array('tregulatorperson.fid'=>$my_fid))->find();
		
		$old_password = I('old_password');//旧密码
		$new_password = I('new_password');//新密码
		if($old_password == $new_password){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'原密码和新密码相同,没有做修改'));
		}
		
		if(md5($old_password) != $myInfo['fpassword']){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'原密码错误,修改失败'));
		}
		
		$e_data = array();
		$e_data['fmodifier'] = session('regulatorpersonInfo.fname');//修改人
		$e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$e_data['fpassword'] = md5($new_password);//密码
		
		$rr = M('tregulatorperson')->where(array('tregulatorperson.fid'=>$my_fid))->save($e_data);//修改密码
		
		if($rr > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));
		}
		
		
	}
	
	public function edit_my_avatar(){
		$my_fid = session('regulatorpersonInfo.fid');
		$favatar = I('favatar');//头像地址
		$e_data['favatar'] = $favatar;

		$e_data['fmodifier'] = session('regulatorpersonInfo.fname');//修改人
		$e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$rr = M('tregulatorperson')->where(array('fid'=>$my_fid))->save($e_data);
		if($rr > 0 ){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));
		}
	}
	

}