<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 国家局重点案件线索列表
 * by zw
 */

class GckzdanjianController extends BaseController{
  /**
  * 重点线索线索接收列表
  * by zw
  */
  public function jszdanjian_list(){
    session_write_close();

    $p  = I('page', 1);//当前第几页
    $pp = 20;//每页显示多少记录
    $area = I('area');
    $system_num = getconfig('system_num');//客户编号
    $fstate = I('fstate');//状态
    $model = I('model')?I('model'):0;//模板类型

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $where_time = gettimecondition($years,$timetypes,$timeval,'a.fcreate_time',-1);
    $where_tia['_string'] = $where_time;

    $where_tia['a.fstatus']       = array('in',array(20,30));
    $where_tia['a.fdomain_isout'] = 0;
    $where_tia['a.fcustomer'] = $system_num;
    $where_tia['a.fmodel_type']   = $model;

    if($fstate != -1){//违法广告处理状态
      if($fstate == 30){
        $where_tia['a.fstatus'] = 30;
      }else{
        $where_tia['a.fstatus'] = 20;
      }
    }

    if(!empty($area)){
      if($area != 100000){
        $where_tia['c.frece_reg_id'] = '20'.$area;
      }
    }

    $count = M('tbn_illegal_zdad a')
      ->join('(select fillegal_ad_id from tbn_zdcase_send where fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) b on a.fid = b.fillegal_ad_id')
      ->join('(select * from tbn_zdcase_send where fid in(SELECT MAX(fid) fid FROM tbn_zdcase_send GROUP BY fillegal_ad_id) ) c on a.fid = c.fillegal_ad_id') 
      ->where($where_tia)
      ->count();//查询满足条件的总记录数
    
    $do_tia = M('tbn_illegal_zdad a')
      ->field('a.*,c.frece_reg')
      ->join('(select fillegal_ad_id from tbn_zdcase_send where fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) b on a.fid = b.fillegal_ad_id')
      ->join('(select * from tbn_zdcase_send where fid in(SELECT MAX(fid) fid FROM tbn_zdcase_send GROUP BY fillegal_ad_id) ) c on a.fid = c.fillegal_ad_id') 
      ->where($where_tia)
      ->order('a.fid desc')
      ->page($p,$pp)
      ->select();
  
    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
  }

  /**
  * 重点线索线索交办列表
  * by zw
  */
  public function jbzdanjian_list(){
    session_write_close();

    $p  = I('page', 1);//当前第几页
    $pp = 20;//每页显示多少记录
    $area = I('area');
    $system_num = getconfig('system_num');//客户编号
    $fstate = I('fstate');//状态
    $model = I('model')?I('model'):0;//模板类型

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $where_time = gettimecondition($years,$timetypes,$timeval,'a.fcreate_time',-1);
    $where_tia['_string'] = $where_time;

    $where_tia['a.fstatus']       = array('in',array(20,30));
    $where_tia['a.fdomain_isout'] = 0;
    $where_tia['a.fcustomer'] = $system_num;
    $where_tia['a.fmodel_type']   = $model;
    $where_tia['fsend_regid']  = session('regulatorpersonInfo.fregulatorpid');
    
    if(!empty($area)){
      if($area != 100000){
        $where_tia['c.frece_reg_id']  = '20'.$area;
      }
    }

    if($fstate != -1){//违法广告处理状态
      if($fstate == 30){
        $where_tia['a.fstatus'] = 30;
      }else{
        $where_tia['a.fstatus'] = 20;
      }
    }
    $count = M('tbn_illegal_zdad a')
      ->join('(select * from tbn_zdcase_send where fid in(SELECT MAX(fid) fid FROM tbn_zdcase_send GROUP BY fillegal_ad_id) ) c on a.fid = c.fillegal_ad_id') 
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    $do_tia = M('tbn_illegal_zdad a')
      ->field('a.*,c.frece_reg')
      ->join('(select * from tbn_zdcase_send where fid in(SELECT MAX(fid) fid FROM tbn_zdcase_send GROUP BY fillegal_ad_id) ) c on a.fid = c.fillegal_ad_id') 
      ->where($where_tia)
      ->order('a.fid desc')
      ->page($p,$pp)
      ->select();

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
  }

  /**
  * 重点线索查证信息获取
  * by zw
  */
  public function view_czzdanjian(){
    $system_num = getconfig('system_num');//客户编号

    $fid = I('fid');//线索ID
    $islook = I('islook');//是否查看，1表示已查看

    $where_tia['a.fid']           = $fid;
    $where_tia['a.fcustomer']     = $system_num;

    $do_tia = M('tbn_illegal_zdad a')
      ->field('*')
      ->where($where_tia)
      ->find();

    //获取所有附件
    $where_tia2['b.fid']         = $fid;
    $where_tia2['b.fdomain_isout'] = 0;
    $where_tia2['b.fcustomer'] = $system_num;
    $where_tia2['a.fstate'] = 0;

    $do_tiza = M('tbn_illegal_zdad_attach')
      ->alias('a')
      ->field('a.fid,a.fattach,a.fattach_url,a.ftype')
      ->join('tbn_illegal_zdad b on a.fillegal_ad_id=b.fid')
      ->where($where_tia2)
      ->select();

    $do_tia['files']['jbfiles'] = [];
    $do_tia['files']['clfiles'] = [];
    $do_tia['files']['czfiles'] = [];
    foreach ($do_tiza as $key => $value) {
      if($value['ftype'] == 0){
        $do_tia['files']['jbfiles'][] = $value;
      }elseif($value['ftype'] == 10){
        $do_tia['files']['clfiles'][] = $value;
      }elseif($value['ftype'] == 20){
        $do_tia['files']['czfiles'][] = $value;
      }
    }

    if(!empty($do_tia)){
      //已查看
      if(!empty($islook)){
        M('tbn_illegal_zdad')->where(['fid'=>$fid])->save(['fview_status'=>10]);
      }
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_tia));
    }else{
      $this->ajaxReturn(array('code'=>1,'msg'=>'获取失败'));
    }
  }

}