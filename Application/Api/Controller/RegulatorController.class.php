<?php
namespace Api\Controller;
use Think\Controller;

class RegulatorController extends Controller {
	

	public function get_regulator_list(){
		
		$fname = I('fname');//获取监管机构名称
		$ftype = I('ftype');//机构类型
		
		$where = array();
		if($ftype != '') $where['ftype'] = $ftype;
		$where['fstate'] = array('neq',-1);
		$where['fname'] = array('like','%'.$fname.'%');
		$where['fkind'] = 1;//只查机构，不查部门

		$regulatorList = M('tregulator')->cache(true,86400)->field('fcode,fname')->where($where)->limit(10)->select();//查询媒介列表

		$this->ajaxReturn(array('code'=>0,'value'=>$regulatorList));
		
	}

	
}