let userAuthority = [
						{'name':'快剪一般任务','code':'6001'},
						{'name':'快剪抽检','code':'6002'},
						{'name':'旅游数据处理','code':'6003'},
						{'name':'样本数据检查','code':'6004'}
					]
let userInfo = {
    template: `<div v-loading="loading" class="userInfo">
    <el-form :model="userInfo" :disabled="type != 'input'">
        <el-form-item label="微信昵称">
            {{userInfo.nickname}}
        </el-form-item>
        <el-form-item label="别名">
            <el-input v-model="userInfo.alias"></el-input>
        </el-form-item>
        <el-form-item label="手机">
            <el-input v-model="userInfo.mobile"></el-input>
        </el-form-item>
        <el-form-item label="邮箱">
            <el-input v-model="userInfo.mail"></el-input>
        </el-form-item>
        <el-form-item label="分组">
            <el-select v-model="userInfo.group_id" placeholder="请选择" filterable>
                <el-option
                        v-for="item in userInfo.group_list"
                        :key="item.value"
                        :label="item.label"
                        :value="item.value">
                </el-option>
            </el-select>
        </el-form-item>
        <el-form-item label="密码">
            <el-input v-model="userInfo.password1" type="password"></el-input>
        </el-form-item>
        <el-form-item label="组长">
            <el-radio-group v-model="userInfo.is_leader">
                <el-radio label="1">组长</el-radio>
                <el-radio label="0">组员</el-radio>
            </el-radio-group>
        </el-form-item>
        <el-form-item label="标签">
            <el-checkbox-group v-model="userInfo.user_tag">
                <el-checkbox :label="item" v-for="item in userTags" :key="item">{{item}}</el-checkbox>
            </el-checkbox-group>
        </el-form-item>
		
		<el-form-item label="个人权限">
            <el-checkbox-group v-model="userInfo.userAuthority">
				<el-checkbox :label="item.code" v-for="item in userAuthority" :key="item.code">{{item.name}}</el-checkbox>
            </el-checkbox-group>
        </el-form-item>
		
		
        <el-form-item label="可切换的分组">
            <div>
                <el-button v-for="(item,index) in userInfo.haveGroupList" :key="item.value" @click="delHaveGroup(index)" title="点击删除">{{item.label}}</el-button>
            </div>
            <el-select v-model="checkedGroup" placeholder="请选择分组" filterable>
                <el-option
                        v-for="item in userInfo.group_list"
                        :key="item.value"
                        :label="item.label"
                        :value="item">
                </el-option>
            </el-select>
            <el-button v-if="checkedGroup" @click="addGroup">添加</el-button>
        </el-form-item>
    </el-form>
    <div>
        <el-button type="primary" @click="type='input'" v-if="type != 'input'">修改</el-button>
        <el-button type="success" @click="save" v-else>保存</el-button>
    </div>
</div>`,
    props: ['wxid'],
    data: function () {
        return {
            loading: true,
            userInfo: {},
            type: 'check',
            userTags,
            checkedGroup: null,
        }
    },
    methods: {
        dataInit: function(){
            this.loading = false
            this.userInfo = {}
            this.type = 'check'
            this.checkedGroup = null
            if (this.wxid){
                this.getInfo()
            }
        },
        getInfo: function () {
            this.loading = true
            $.post('getUserInfo', {
                wx_id: this.wxid
            }).then((res) => {
                this.loading = false
                this.userInfo = res.data
            })
        },
        save: function () {
            if (this.type !== 'input'){
                return false
            }
            this.$confirm('确认保存?').then(() => {
                this.loading = true
                let have_group_id = this.userInfo.haveGroupList.map(v => v.value)
                this.userInfo.have_group_id = have_group_id.join(',')
                $.post('saveUserInfo', {
                    userInfo: this.userInfo
                }).then((res) => {
                    if (res.code == 0){
                        this.$message.success(res.msg)
                    } else{
                        this.$message.error(res.msg)
                    }
                    this.loading = false
                    this.type = 'check'
                    this.$emit('save')
                })
            }).catch(() => {})
        },
        delHaveGroup: function (index) {
            this.userInfo.haveGroupList.splice(index, 1)
        },
        addGroup: function () {
            // 检查重复
            if (this.checkedGroup){
                let res = this.userInfo.haveGroupList.some((v) => v.value == this.checkedGroup.value)
                if (!res){
                    this.userInfo.haveGroupList.push(this.checkedGroup)
                } else{
                    this.$message.warning('重复, 请选择其他分组')
                }
            }
        }
    }
}