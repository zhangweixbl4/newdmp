<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 国家局重点案件线索交办
 * by zw
 */

class GjbzdanjianController extends BaseController{
  /**
  * 重点线索交办
  * by zw
  */
  public function jbzdanjian(){
    $ALL_CONFIG = getconfig('ALL');
    $system_num = $ALL_CONFIG['system_num'];
    $ischazheng = $ALL_CONFIG['ischazheng'];//是否有查证功能
    $model          = I('model')?I('model'):0;//模板类型
    
    $fstatus        = I('fstatus'); //提交方式,0草稿箱，10提交
    $fad_name       = I('fad_name'); //交办标题
    $fill_type      = I('fill_type'); //线索类型
    $attachinfo     = I('attachinfo'); //上传附件
    $tregionids     = I('tregionids'); //接收机构行政区划ID组
    $fill_content   = I('fill_content'); //案件描述
    $fisresult      = I('fisresult'); //是否需要反馈，0不需要，10需要
    $fresult_endtime = I('fresult_endtime'); //反馈结束时间
    $fexpressioncodes = I('fexpressioncodes'); //违法表现代码
    $fexpressions = I('fexpressions'); //违法表现

    $attach = [];
    $add_sendarr = [];
    $regionid = substr(session('regulatorpersonInfo.regionid'), 0,2);

    if(empty($tregionids) && $fstatus == 10){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请添加查办案件机构'));
    }
    $data_tiz['fad_name']       = $fad_name;
    $data_tiz['fmodel_type']    = $model;
    $data_tiz['fill_type']      = $fill_type;
    $data_tiz['fsend_regid']   = session('regulatorpersonInfo.fregulatorpid');
    $data_tiz['fsend_regname']   = session('regulatorpersonInfo.regulatorpname');
    $data_tiz['fsend_userid']   = session('regulatorpersonInfo.fid');
    $data_tiz['fsend_username'] = session('regulatorpersonInfo.fname');
    $data_tiz['fill_content'] = $fill_content;
    $data_tiz['fexpressioncodes'] = $fexpressioncodes;
    $data_tiz['fexpressions'] = $fexpressions;
    $data_tiz['fisresult']    = $fisresult;
    $data_tiz['fcreate_time'] = date('Y-m-d H:i:s');
    $data_tiz['fview_status'] = 0;
    $data_tiz['fcustomer'] = $system_num;

    if($fstatus==10){
      $data_tiz['fsend_time'] = date('Y-m-d H:i:s');
      $data_tiz['fstatus']    = 20;
    }else{
      $data_tiz['fstatus']    = 0;
    }

    if(!empty($fisresult)){
      $data_tiz['fresult_endtime'] = $fresult_endtime;
    }

    foreach ($tregionids as $key => $value) {
      $do_tr = M('tregulator')->cache(true,600)->field('fid,fname')->where('fregionid='.$value.' and fstate=1 and ftype=20 and fkind=1')->find();
      if(!empty($do_tr)){
        if($regionid == substr($value, 0,2) || $regionid == 10){
          $data_tiz['fdomain_isout'] = 0;
        }else{
          $data_tiz['fdomain_isout'] = 10;
        }
        $data_tiz['fget_regid']   = $do_tr['fid'];
        $data_tiz['fget_regname']   = $do_tr['fname'];
        $do_tiz = M('tbn_illegal_zdad')->add($data_tiz);

        //提交时生成派发记录
        if(!empty($do_tiz)){
          $add_send = [
            'fillegal_ad_id'=>$do_tiz,
            'frece_reg_id'=>$do_tr['fid'],
            'frece_reg'=>$do_tr['fname'],
            'frece_time'=>date('Y-m-d H:i:s'),
            'fsend_reg_id'=>session('regulatorpersonInfo.fregulatorpid'),
            'fsend_reg'=>session('regulatorpersonInfo.regulatorpname'),
            'fstatus'=>0,
          ];

          if($fstatus == 10){
            $add_send['fsend_time'] = date('Y-m-d H:i:s');
          }
          $add_sendarr[] = $add_send;
        }

        //上传附件
        if(!empty($attachinfo)&&!empty($do_tiz)){
          $attach_data['fillegal_ad_id']    = $do_tiz;//重点线索ID
          $attach_data['fuploader']         = session('regulatorpersonInfo.fname');//上传人
          $attach_data['fupload_time']      = date('Y-m-d H:i:s');//上传时间
          $attach_data['ftype']             = 0;//附件类型
          foreach ($attachinfo as $key2 => $value2){
            $attach_data['fattach']     = $value2['fattachname'];
            $attach_data['fattach_url'] = $value2['fattachurl'];
            array_push($attach,$attach_data);
          }
        }

      }
    }

    if(!empty($add_sendarr)){
      M('tbn_zdcase_send')->addall($add_sendarr);
    }

    if(!empty($attach)){
      M('tbn_illegal_zdad_attach')->addAll($attach);
    }
    D('Function')->write_log('重点案件线索交办',1,'交办成功','tbn_illegal_zdad',0,M('tbn_illegal_zdad')->getlastsql());
    $this->ajaxReturn(array('code'=>0,'msg'=>'交办成功'));
  }

  /**
  * 重点线索线索草稿箱列表
  * by zw
  */
  public function cgxzdanjian_list(){
    session_write_close();

    $p  = I('page', 1);//当前第几页
    $pp = 20;//每页显示多少记录
    $system_num = getconfig('system_num');//客户编号
    $model = I('model')?I('model'):0;//模板类型

    $where_tia['a.fsend_userid'] = session('regulatorpersonInfo.fid');
    $where_tia['a.fstatus']     = 0;
    $where_tia['a.fcustomer']   = $system_num;
    $where_tia['a.fmodel_type']   = $model;

    $count = M('tbn_illegal_zdad')
      ->alias('a')
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    
    $do_tia = M('tbn_illegal_zdad')
      ->alias('a')
      ->field('a.*')
      ->where($where_tia)
      ->order('a.fid desc')
      ->page($p,$pp)
      ->select();
  
    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
  }

  /**
  * 重点线索草稿信息获取
  * by zw
  */
  public function view_jbzdanjian(){
    $system_num = getconfig('system_num');//客户编号
    $fid = I('fid');//线索ID

    $where_tia['a.fsend_userid'] = session('regulatorpersonInfo.fid');
    $where_tia['a.fid'] = $fid;
    $where_tia['a.fstatus'] = 0;
    $where_tia['a.fcustomer'] = $system_num;

    $do_tia = M('tbn_illegal_zdad a')
      ->field('a.*')
      ->where($where_tia)
      ->find();//交办信息

    $do_tia['fregion_getid'] = substr($do_tia['fget_regid'],(strlen($do_tia['fget_regid'])-6),6);

    $where_tiza['fillegal_ad_id'] = $fid;
    $where_tiza['ftype'] = 0;
    $where_tiza['fstate'] = 0;
    $do_tiza = M('tbn_illegal_zdad_attach')
      ->field('fid,fattach,fattach_url')
      ->where($where_tiza)
      ->select();//附件

    if(!empty($do_tia)){
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取','data'=>$do_tia,'filedata'=>$do_tiza));
    }else{
      $this->ajaxReturn(array('code'=>1,'msg'=>'获取失败'));
    }
  }

  /**
  * 重点线索草稿提交
  * by zw
  */
  public function edit_jbzdanjian(){
    $system_num = getconfig('system_num');

    $fid            = I('fid');//线索ID
    $fstatus        = I('fstatus'); //提交方式,0草稿箱，10提交
    $fad_name       = I('fad_name'); //交办标题
    $fill_type      = I('fill_type'); //线索类型
    $attachinfo     = I('attachinfo'); //上传附件
    $fill_content   = I('fill_content'); //案件描述
    $cxczcontent    = I('cxczcontent'); //重新查证说明
    $fisresult      = I('fisresult'); //是否需要反馈，0不需要，10需要
    $fresult_endtime = I('fresult_endtime'); //反馈结束时间
    $fexpressioncodes = I('fexpressioncodes'); //违法表现代码
    $fexpressions = I('fexpressions'); //违法表现
   
    $attach = [];
    $regionid = substr(session('regulatorpersonInfo.regionid'), 0,2);

    $data_tiz['fad_name']       = $fad_name;
    $data_tiz['fill_type']      = $fill_type;
    $data_tiz['fregion_id']     = session('regulatorpersonInfo.regionid');
    $data_tiz['fsend_regid']    = session('regulatorpersonInfo.fregulatorpid');
    $data_tiz['fsend_regname']  = session('regulatorpersonInfo.regulatorpname');
    $data_tiz['fsend_userid']   = session('regulatorpersonInfo.fid');
    $data_tiz['fsend_username'] = session('regulatorpersonInfo.fname');
    $data_tiz['fexpressioncodes'] = $fexpressioncodes;
    $data_tiz['fexpressions'] = $fexpressions;
    $data_tiz['fill_content'] = $fill_content;
    $data_tiz['fisresult']    = $fisresult;

    if($fstatus==10){
      $data_tiz['fsend_time'] = date('Y-m-d H:i:s');
      $data_tiz['fstatus']    = 20;
    }else{
      $data_tiz['fstatus']    = 0;
    }
    
    if(!empty($fisresult)){
      $data_tiz['fresult_endtime'] = $fresult_endtime;
    }

    //保存
    $where_tiz['fid'] = $fid;
    $where_tiz['fcustomer'] = $system_num;
    $where_tiz['fsend_userid'] = session('regulatorpersonInfo.fid');
    $do_tiz = M('tbn_illegal_zdad')
      ->where($where_tiz)
      ->save($data_tiz);

    if(!empty($do_tiz)){
      M()->execute('update tbn_illegal_zdad_attach set fstate=10 where fstate=0 and fillegal_ad_id='.$fid.' and ftype=0');

      //上传附件
      $attach_data['fillegal_ad_id']  = $fid;//重点线索ID
      $attach_data['fuploader']       = session('regulatorpersonInfo.fname');//上传人
      $attach_data['fupload_time']    = date('Y-m-d H:i:s');//上传时间
      $attach_data['ftype']           = 0;//附件类型
      foreach ($attachinfo as $key2 => $value2){
        $attach_data['fattach']     = $value2['fattachname'];
        $attach_data['fattach_url'] = $value2['fattachurl'];
        array_push($attach,$attach_data);
      }
    }

    if(!empty($attach)){
      M('tbn_illegal_zdad_attach')->addAll($attach);
    }
    D('Function')->write_log('重点案件线索草稿箱',1,'提交成功','tbn_illegal_zdad',$fid,M('tbn_illegal_zdad')->getlastsql());
    $this->ajaxReturn(array('code'=>0,'msg'=>'提交成功'));
  }

   /**
  * 重点线索交办信息删除
  * by zw
  */
  public function del_jbzdanjian(){
    $system_num = getconfig('system_num');//客户编号

    $fid = I('fid');//线索ID

    $where_tia['fsend_userid'] = session('regulatorpersonInfo.fid');
    $where_tia['fid']         = $fid;
    $where_tia['fstatus']     = 0;
    $where_tia['fcustomer']   = $system_num;

    $do_tia = M('tbn_illegal_zdad')->where($where_tia)->delete();

    if(!empty($do_tia)){
      D('Function')->write_log('重点案件线索删除',1,'删除成功','tbn_illegal_zdad',$fid,M('tbn_illegal_zdad')->getlastsql());
      $this->ajaxReturn(array('code'=>0,'msg'=>'删除成功','data'=>$do_tia));
    }else{
      D('Function')->write_log('重点案件线索删除',0,'删除失败','tbn_illegal_zdad',$fid,M('tbn_illegal_zdad')->getlastsql());
      $this->ajaxReturn(array('code'=>1,'msg'=>'删除失败'));
    }
  }

}