<?php
namespace Common\Model;

use Think\Model;
use Think\Exception;


class AlitongxinModel{
	
	
	
	
	/*发送登录通知短信*/
	public function login_sms($mobile,$location){
		$sendValue = array('location'=>$location);
		$ret = $this->sendSms($mobile,'SMS_123673522',$sendValue);
		if($ret['Code'] == 'OK'){
			return array('code'=>0,'msg'=>'发送成功');
		}else{
			return array('code'=>-1,'msg'=>$ret['Message']);
		}
	}
	
	
	/*发送验证码短信*/
	public function identifying_sms($mobile,$code,$product){
		$sendValue = array('code'=>$code,'product'=>$product);
		$ret = $this->sendSms($mobile,'SMS_62535332',$sendValue);
		if($ret['Code'] == 'OK'){
			return array('code'=>0,'msg'=>'发送成功');
		}else{
			return array('code'=>-1,'msg'=>$ret['Message']);
		}
	}

	/*内部人员新任务短信*/
	public function usertask_sms($mobile,$title){
		$sendValue = array('title'=>$title,'time'=>date('Y-m-d'));
		$ret = $this->sendSms($mobile,'SMS_155845253',$sendValue);
		if($ret['Code'] == 'OK'){
			return array('code'=>0,'msg'=>'发送成功');
		}else{
			return array('code'=>-1,'msg'=>$ret['Message']);
		}
	}
	
	/*发送警报短信*/
	public function alarm($mobile,$title,$time,$content){
		$sendValue = array('title'=>$title,'time'=>$time,'content'=>$content);
		$ret = $this->sendSms($mobile,'SMS_160480123',$sendValue);
		if($ret['Code'] == 'OK'){
			return array('code'=>0,'msg'=>'发送成功');
		}else{
			return array('code'=>-1,'msg'=>$ret['Message']);
		}
	}
	
	/*
	* 案件任务提醒
	* by zw 
	*/
	public function illtask_sms($mobile,$mc,$bh,$pt){
		$sendValue = array('mc'=>$mc,'bh'=>$bh,'pt'=>$pt);
		$ret = $this->sendSms($mobile,'SMS_175536856',$sendValue);
		if($ret['Code'] == 'OK'){
			return array('code'=>0,'msg'=>'发送成功');
		}else{
			return array('code'=>-1,'msg'=>$ret['Message']);
		}
	}
	
	/*
	* 南京线索任务待办提醒
	* by zw 
	* sl 数量，jbh交办编号，yxq于几号前办理
	*/
	public function nj_illtask_sms($mobile,$sl,$jbh,$yxq){
		$sendValue = array('sl'=>$sl,'jbh'=>$jbh,'yxq'=>$yxq);
		$ret = $this->sendSms($mobile,'SMS_181864595',$sendValue,'南京市局广告处');
		if($ret['Code'] == 'OK'){
			return array('code'=>0,'msg'=>'发送成功');
		}else{
			return array('code'=>-1,'msg'=>$ret['Message']);
		}
	}

	/*
	* 南京线索任务催办提醒
	* by zw 
	* sl 数量，dh交办单号
	*/
	public function nj_illtask_cb_sms($mobile,$sl,$dh){
		$sendValue = array('sl'=>$sl,'dh'=>$dh);
		$ret = $this->sendSms($mobile,'SMS_181864594',$sendValue,'南京市局广告处');
		if($ret['Code'] == 'OK'){
			return array('code'=>0,'msg'=>'发送成功');
		}else{
			return array('code'=>-1,'msg'=>$ret['Message']);
		}
	}
	
	/*发送DMP监测点故障短信*/
	public function monitoring_fault($mobile,$name,$time){
		$sendValue = array('name'=>$name,'time'=>$time);
		$ret = $this->sendSms($mobile,'SMS_149421867',$sendValue);
		if($ret['Code'] == 'OK'){
			return array('code'=>0,'msg'=>'发送成功');
		}else{
			return array('code'=>-1,'msg'=>$ret['Message']);
		}
	}
	

	
	/*发送登录通知*/
	public function sendSms($mobile,$TemplateCode,$sendValue,$SignName=''){
		if(!$SignName) $SignName = C('SMS_SIGNNAME');//短信签名
		if(strlen($mobile) != 11){
			return array('code'=>-1,'msg'=>'手机号错误');
		}
		
		$tongxin = array();
		$tongxin['SignatureMethod'] = 'HMAC-SHA1';//建议固定值：HMAC-SHA1
		$tongxin['SignatureNonce'] = createNoncestr();//用于请求的防重放攻击，每次请求唯一
		$tongxin['AccessKeyId'] = C('AL_OSSID');
		$tongxin['SignatureVersion'] = '1.0';//建议固定值：1.0
		$tongxin['Timestamp'] = gmdate('Y-m-d\TH:i:s\Z');//格式为：date('Y-m-d\TH:i:s\Z')；时区为：GMT

		
		
		$tongxin['Action'] = 'SendSms';//API的命名，固定值，如发送短信API的值为：SendSms
		$tongxin['Version'] = '2017-05-25';//API的版本，固定值，如短信API的值为：2017-05-25
		$tongxin['RegionId'] = 'cn-hangzhou';//API支持的RegionID，如短信API的值为：cn-hangzhou
		$tongxin['PhoneNumbers'] = $mobile;//发送手机号
		$tongxin['SignName'] = $SignName;//短信签名
		$tongxin['TemplateParam'] = json_encode($sendValue);//发送主体内容
		$tongxin['TemplateCode'] = $TemplateCode;//短信模板
		$Signature = $this->sign($tongxin,C('AL_OSSKEY'));//计算签名内容
		$tongxin['Signature'] = $Signature;
		$objectxml = http('http://dysmsapi.aliyuncs.com/',$tongxin,'GET');
		$objectxml = simplexml_load_string($objectxml);//将xml转换成 对象  
		$xmljson = json_encode($objectxml );//将对象转换个JSON  
		$xmlarray =json_decode($xmljson,true);//将json转换成数组 
		
		$log_data = array();
		$log_data['mobile_no'] = $mobile;
		$log_data['template_no'] = $TemplateCode;
		$log_data['send_value'] = json_encode($sendValue,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
		$log_data['send_time'] = date('Y-m-d H:i:s');
		$log_data['ret_message'] = $xmlarray['Message'];
		$log_data['requestid'] = $xmlarray['RequestId'];
		$log_data['ret_bizid'] = $xmlarray['BizId'];
		$log_data['ret_code'] = $xmlarray['Code'];
			
		M('sms_log')->add($log_data);
		
		return $xmlarray;
    }
	
	/*签名*/
	public function sign($arrData,$key){
		foreach($arrData as $k => $data){
			$tmpArr[] = urlencode($k).'='.urlencode($data);
		}
		sort($tmpArr, SORT_STRING);//按照字典序排序
		$tmpStr = implode( '&',$tmpArr );//数组转为字符串
		$tmpStr = 'GET&'.urlencode('/').'&'.urlencode($tmpStr);

		$tmpStr = str_replace(array('+','*','%7E'),array('%20','%2A','~'),$tmpStr);
		$Sign = hash_hmac('sha1',$tmpStr,$key.'&', true);//哈希加密
		$Sign = base64_encode($Sign);
		return $Sign;
	}
	
	
	/*发送临时授权码短信*/
	public function auth_sms($mobile,$code,$product,$valid){
		$sendValue = array('code'=>$code,'product'=>$product,'valid'=>$valid);
		$ret = $this->sendSms($mobile,'SMS_151547255',$sendValue);
		if($ret['Code'] == 'OK'){
			return array('code'=>0,'msg'=>'发送成功');
		}else{
			return array('code'=>-1,'msg'=>$ret['Message']);
		}
	}


	
    

}