<?php
namespace Open\Controller;
use Think\Controller;

class BcsampleController extends BaseController {
	



	public function add_edit_bcsample(){
		$bcsample_data = file_get_contents('php://input');//获取post数据
		$bcsample_data = json_decode($bcsample_data,true);//把json转化成数组
		//var_dump($bcsample_data);
		$fid = $bcsample_data['fid'];//广告样本ID
		
		$a_e_data = array();

		$a_e_data['fmediaid'] = intval($bcsample_data['fmediaid']);//媒体ID
		$media_count = M('tmedia')->where(array('fid'=>$a_e_data['fmediaid']))->count();
		if($media_count == 0) $this->ajaxReturn(array('code'=>10016,'msg'=>'添加或修改数据失败,媒体ID有错误'));
		
		$a_e_data['fissuedate'] = date("Y-m-d H:i:s",strtotime($bcsample_data['fissuedate']));//发布日期
		
		$a_e_data['fadid'] = $bcsample_data['fadid'];//广告ID
		$ad_count = M('tad')->where(array('fadid'=>$a_e_data['fadid']))->count();
		if($ad_count == 0) $this->ajaxReturn(array('code'=>10016,'msg'=>'添加或修改数据失败,广告ID有错误'));
		
		$a_e_data['fversion'] = $bcsample_data['fversion'];//版本说明
		
		$a_e_data['fspokesman'] = $bcsample_data['fspokesman'];//代言人
		
		$a_e_data['fadlen'] = intval($bcsample_data['fadlen']);//样本长度
		$a_e_data['fillegaltypecode'] = intval($bcsample_data['fillegaltypecode']);//违法类型代码
		$a_e_data['fillegalcontent'] = $bcsample_data['fillegalcontent'];//涉嫌违法内容
		$a_e_data['fexpressioncodes'] = $bcsample_data['fexpressioncodes'];//违法表现代码，数组
		
		
		$a_e_data['favifilename'] = $bcsample_data['favifilename'];//视频文件名
		$a_e_data['fstate'] = $bcsample_data['fstate'];//状态
		
		$person_info = $GLOBALS['person_info'];//获取接口请求人的信息

		
		if($fid == '' ){
			$a_e_data['fcreator'] = $person_info['fname'];//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = $person_info['fname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			$rr = M('tbcsample')->add($a_e_data);
			$illegal = A('Open/bcsample','Model')->bcsampleillegal($rr,$a_e_data['fexpressioncodes'],$person_info['fname']);//添加广播广告违法表现对应表并获取冗余字段
			M('tbcsample')->where(array('fid'=>$rr))->save($illegal);//修改违法表现冗余字段
			if($rr > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'添加广播广告样本成功','fid'=>$rr));
			if($rr == 0) $this->ajaxReturn(array('code'=>10011,'msg'=>'添加广播广告样本失败,原因未知'));
		}else{
			$a_e_data['fmodifier'] = $person_info['fname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tbcsample')->where(array('fid'=>$fid))->save($a_e_data);
			$illegal = A('Open/bcsample','Model')->bcsampleillegal($fid,$a_e_data['fexpressioncodes'],$person_info['fname']);//添加广播广告违法表现对应表并获取冗余字段
			M('tbcsample')->where(array('fid'=>$fid))->save($illegal);//修改违法表现冗余字段
			if($rr > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'修改广播广告样本成功'));
			if($rr == 0) $this->ajaxReturn(array('code'=>10012,'msg'=>'修改广播广告样本失败,原因未知'));
		}
		
		
	
	}
	
			/*获取广播样本*/
	public function get_bcsample_list(){

		$bcsample_data = file_get_contents('php://input');//获取post数据
		
		$bcsample_data = json_decode($bcsample_data,true);//把json转化成数组
		$p = intval($bcsample_data['p']);//需要返回的页数
		if($p == 0) $p = 1;
		$pp = intval($bcsample_data['pp']);//需要返回的条数
		if($pp <= 0) $pp = 10;
		if($pp > 100) $pp = 100;
		
		$fcreatetime_s = $bcsample_data['fcreatetime_s'];//创建时间 开始
		if($fcreatetime_s == '') $fcreatetime_s = '2015-01-01';
		$fcreatetime_e = $bcsample_data['fcreatetime_e'];//创建时间 结束
		if($fcreatetime_e == '') $fcreatetime_e = date('Y-m-d H:i:s');

		$where = array();
		$where['fcreatetime'] = array('between',$fcreatetime_s.','.$fcreatetime_e);
		if($bcsample_data['fid'] != '') $where['tbcsample.fid'] = $bcsample_data['fid'];//样本ID
		if($bcsample_data['fmediaid'] != '') $where['tbcsample.fmediaid'] = $bcsample_data['fmediaid'];//媒介ID
		if($bcsample_data['fadid'] != '') $where['tbcsample.fadid'] = $bcsample_data['fadid'];//广告ID
		if($bcsample_data['fillegaltypecode'] != '') $where['tbcsample.fillegaltypecode'] = $bcsample_data['fillegaltypecode'];//违法类型
		
		if($bcsample_data['fversion'] != '') $where['tbcsample.fversion'] = array('like','%'.$bcsample_data['fversion'].'%');//版本说明
		if($bcsample_data['fspokesman'] != '') $where['tbcsample.fspokesman'] = array('like','%'.$bcsample_data['fspokesman'].'%');//代言人
		
		$count = M('tbcsample')->where($where)->count();
		
		$bcsampleList = M('tbcsample')->where($where)->page($p,$pp)->select();

		$this->ajaxReturn(array('code'=>0,'msg'=>'','bcsampleList'=>$bcsampleList,'count'=>$count));
		

	}
	
}