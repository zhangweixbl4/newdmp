Vue.prototype.$ELEMENT = {
  size: "mini",
  zIndex: 3000
};
let vm = new Vue({
  el: "#app",
  data() {
    let sday = dayjs().startOf('day').format('YYYY-MM-DD')
    let eday = dayjs().endOf('day').format('YYYY-MM-DD')
    let smonth = dayjs().startOf('month').format('YYYY-MM')
    let emonth = dayjs().endOf('month').format('YYYY-MM')
    return {
      userTags,
      groupList,
      fieldList,
      loading: false,
      table: [],
      sourceTabel: '',
      where: {
        userTag: ["积分排名"],
        filtertype: '2'
      },
      tableHeight: '400px',
      day: [sday, eday],
      month: [smonth, emonth],
      sourceWhere: ''
    }
  },
  computed: {
    FieldList() {
      return fieldList[this.where.filtertype]
    },
    dateType() {
      switch (this.where.filtertype) {
        case '1':
          return 'day'
        case '2':
          return 'day'
        case '3':
          return 'month'
        case '4':
          return 'month'
      }
    }
  },
  created() {
    this.sourceWhere = JSON.stringify(this.where)
    this.getList()
    console.log(this.FieldList)
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight()
    })
    window.addEventListener('resize', () => {
      this.getTableHeight()
    })
  },
  methods: {
    sortChange({ column, prop, order }) {
      if (!order) {
        this.table = JSON.parse(this.sourceTabel)
      } else {
        this.table.sort((a, b) => {
          const _a = a[prop]
          const _b = b[prop]
          if (prop === 'alias') {
            if (order === 'ascending') {
              return _a.localeCompare(_b, "zh-Hans-CN", {sensitivity: 'accent'})
            } else {
              return _b.localeCompare(_a, "zh-Hans-CN", {sensitivity: 'accent'})
            }
          } else {
            if (order === 'ascending') {
              return (_a - _b)
            } else {
              return (_b - _a)
            }
          }
        })
      }
    },
    getList() {
      this.loading = true
      let data = {
        timeRangeArr: this.getTime(),
        ...this.where
      }
      $fly.post("", Qs.stringify(data)).then(res => {
        this.loading = false
        this.table = res
        this.sourceTabel = JSON.stringify(res)
      })
    },
    getTime() {
      switch (this.where.filtertype) {
        case '1':
          return [dayjs(`${this.day[0]}`).startOf('day').valueOf(), dayjs(`${this.day[1]}`).endOf().valueOf()]
        case '2':
          return [dayjs(`${this.day[0]}`).startOf('day').valueOf(), dayjs(`${this.day[1]}`).endOf().valueOf()]
        case '3':
          return [dayjs(`${this.month[0]}`).startOf('month').valueOf(), dayjs(`${this.month[1]}`).endOf('month').valueOf()]
        case '4':
          return [dayjs(`${this.month[0]}`).startOf('year').valueOf(), dayjs(`${this.month[1]}`).endOf('year').valueOf()]
      }
    },
    resetWhere() {
      this.where = JSON.parse(this.sourceWhere)
      this.getList()
    },
    exportExcel() {
      let data = {
        timeRangeArr: this.getTime(),
        ...this.where
      }
      $fly.post("exportScoreExcel", Qs.stringify(data)).then(res => {
        window.open(res)
      });
    },
    getTableHeight() {
      let formHeight = this.$refs.sform.$el.clientHeight
      this.tableHeight = document.body.clientHeight - formHeight - 74 + 'px'
    },
  }
})
