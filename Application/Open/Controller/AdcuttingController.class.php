<?php
namespace Open\Controller;
use Think\Controller;

class AdcuttingController extends BaseController {
	


	/*获取可做剪辑任务的媒体列表*/
	public function get_media(){
		//echo '{"code":2}';
		//exit;
		$input_data = file_get_contents('php://input');//获取post数据
		$input_data = json_decode($input_data,true);//把json转化成数组
		$wx_id = intval($input_data['wx_id']);//用户ID
		
		if($wx_id == 0){//判断传入的微信ID是否有值
			$this->ajaxReturn(array('code'=>1015,'msg'=>'wx_id不能为空'));
		}
		$userInfo = M('ad_input_user')->where(array('wx_id'=>$wx_id))->find();
		if(!$userInfo){//判断传入的微信ID是否能查询到
			$this->ajaxReturn(array('code'=>1017,'msg'=>'查询不到此wx_id'));
		}
		
		
		$lockMedia = M('tmedia')->field('tmedia.fid')->where(array('flockuserid'=>$wx_id))->find();

		if($lockMedia){//判断该微信ID是否有锁定媒体
			$haslock = intval($lockMedia['fid']);
		
			$mediaList = M('tmedia')->field('tmedia.fid as mediaid,tmedia.fmedianame as medianame,left(tmedia.fmediaclassid,2) as media_class')->where(array('tmedia.fid'=>$haslock))->select();
			$mediaList[0]['taskcount'] = M('tseperatetask')->where(array('tseperatetask.fmediaid'=>$haslock,'tseperatetask.fstate'=>array('in','0,1')))->count();
						
		}else{

			$where = array();
			$where['tmedia.flockuserid'] = array('exp','is null');//锁定用户字段等于NULL
			//$where['tmedia.fdpid'] = array('gt',0);//平台ID大于0 
			$haslock = 0;
			$where['tseperatetask.fstate'] = array('in','0,1');
			$media_relation = array_filter(explode(',',$userInfo['media_relation']));
			//var_dump($media_relation);
			if(count($media_relation) > 0){
				$where['tseperatetask.fmediaid'] = array('in',$media_relation);
			}
			
			$mediaList = M('tseperatetask')
									->field('tmedia.fid as mediaid,tmedia.fmedianame as medianame,left(tmedia.fmediaclassid,2) as media_class,count(tmedia.fid) as taskcount')
									->join('tmedia on tseperatetask.fmediaid = tmedia.fid')
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									
									->group('tmedia.fid')
									->order('tmediaowner.fregionid,tmedia.fmedianame')
									->where($where)->select();//查询媒体列表
			//var_dump(M('tseperatetask')->getLastSql());						
		}
		
								
								
								
								
		//var_dump(M('tseperatetask')->getLastSql());						
		$this->ajaxReturn(array('code'=>0,'msg'=>'','haslock'=>$haslock,'mediaList'=>$mediaList));
		
		

	}
	
	/*通过wx_id 和mediaid锁定媒体*/
	public function lock_media(){
		
		$input_data = file_get_contents('php://input');//获取post数据
		$input_data = json_decode($input_data,true);//把json转化成数组
		
		$wx_id = intval($input_data['wx_id']);//用户ID
		$mediaid = intval($input_data['mediaid']);//媒体ID
		if($wx_id == 0){//判断传入的微信ID是否有值
			$this->ajaxReturn(array('code'=>1015,'msg'=>'wx_id不能为空'));
		}
		if($mediaid == 0){//判断传入的媒体ID是否有值
			$this->ajaxReturn(array('code'=>1015,'msg'=>'mediaid不能为空'));
		}
		$media = M('tmedia')->field('')->where(array('fid'=>$mediaid))->find();//查询媒体信息
		if(M('ad_input_user')->where(array('wx_id'=>$wx_id))->count() == 0){//判断传入的微信ID是否能查询到
			$this->ajaxReturn(array('code'=>1017,'msg'=>'查询不到此wx_id'));
		}
		if(!$media){//判断传入的媒体ID是否能查询到
			$this->ajaxReturn(array('code'=>1017,'msg'=>'查询不到此mediaid'));
		}
		
		$lockMedia = M('tmedia')->field('tmedia.fid')->where(array('flockuserid'=>$wx_id))->find();
		if($lockMedia){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该用户已经锁定其它媒体'));
		}
		if($media['flockuserid'] != ''){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该媒体被其它用户锁定'));
		}
		
		$rr = M('tmedia')->where(array('fid'=>$mediaid))->save(array('flockuserid'=>$wx_id));
		if($rr > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'锁定成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'锁定失败,原因未知'));
		}
		
		
	}
	
	/*解除锁定媒体*/
	public function remove_lock_media(){
		
		$input_data = file_get_contents('php://input');//获取post数据
		$input_data = json_decode($input_data,true);//把json转化成数组
		
		$wx_id = intval($input_data['wx_id']);//用户ID
		
		$rr = M('tmedia')->where(array('flockuserid'=>$wx_id))->save(array('flockuserid'=>null));
		
		if($rr > 0){
			
			$this->ajaxReturn(array('code'=>0,'msg'=>'解除锁定成功'));
			
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'解除锁定失败,原因未知'));
		}
		
		
	}
	
	/*获取任务列表*/
	public function get_task(){
		$input_data = file_get_contents('php://input');//获取post数据
		$input_data = json_decode($input_data,true);//把json转化成数组
		
		$mediaid = intval($input_data['mediaid']);//媒体ID
		
		
		$where = array();
		$where['tseperatetask.fstate'] = array('in','0,1');//任务状态，只查询未处理和正在处理的任务
		$where['tseperatetask.fmediaid'] = $mediaid;
		
		
		$taskCount = M('tseperatetask')
										->join('tmedia on tmedia.fid = tseperatetask.fmediaid')->where($where)->count();//查询任务数量
		$taskList = M('tseperatetask')
										->field('tseperatetask.*,tmedia.fmedianame,left(tmedia.fmediaclassid,2) as fmediaclassid')
										->join('tmedia on tmedia.fid = tseperatetask.fmediaid')->where($where)->order('tseperatetask.fissuedate asc')->select();//查询任务列表
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','taskCount'=>$taskCount,'taskList'=>$taskList));

	}
	
	/*修改任务状态*/
	public function edit_task(){
		$input_data = file_get_contents('php://input');//获取post数据
		$input_data = json_decode($input_data,true);//把json转化成数组
		
		$wx_id = intval($input_data['wx_id']);//微信ID
		if(M('ad_input_user')->where(array('wx_id'=>$wx_id))->count() == 0){//判断传入的微信ID是否能查询到
			$this->ajaxReturn(array('code'=>1017,'msg'=>'查询不到此wx_id'));
		}
		
		$seperatetaskid = intval($input_data['seperatetaskid']);//任务ID
		$state = intval($input_data['state']);//任务状态
		
		$taskInfo = M('tseperatetask')->where(array('fseperatetaskid'=>$seperatetaskid))->find();//查询任务详细信息
		//var_dump($taskInfo);
		//exit;
		if(intval($taskInfo['fuserid']) == 0){//判断该任务是否有人接
			$rr = M('tseperatetask')->where(array('fseperatetaskid'=>$seperatetaskid))->save(array('fuserid'=>$wx_id,'fstate'=>$state));//修改用户ID和状态
		}else{
			if(intval($taskInfo['fuserid']) != $wx_id){//判断该任务是否与该用户匹配
				//暂时不判断 $this->ajaxReturn(array('code'=>-1,'msg'=>'该任务与该用户不匹配,执行失败'));
			}
			$rr = M('tseperatetask')->where(array('fseperatetaskid'=>$seperatetaskid))->save(array('fuserid'=>$wx_id,'fstate'=>$state));//修改用户ID和状态
		}
		
		if($rr > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'状态修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'状态修改失败,原因未知'));
		}
	}
	
	/*验证用户密码*/
	public function user_login(){
		$input_data = file_get_contents('php://input');//获取post数据
		$input_data = json_decode($input_data,true);//把json转化成数组
		$group_id = intval(I('get.group_id'));//分组id
		$user = $input_data['user'];
		$password = $input_data['password'];
		//var_dump($user);
		$where = array();
		$where['mobile|mail'] = $user;
		$where['password'] = md5($password);
		$userInfo = M('ad_input_user')->where($where)->find();

		$authority = array();
		if(A('InputPc/Task','Model')->user_other_authority(6001,$userInfo['wx_id'])) $authority[] = 'cut_task';
		if(A('InputPc/Task','Model')->user_other_authority(6002,$userInfo['wx_id'])) $authority[] = 'cut_inspect';
		
		if($userInfo){
			$this->ajaxReturn(array(
									'code'=>0,
									'msg'=>'登录成功',
									'authority'=>$authority,
									'wx_id'=>$userInfo['wx_id'],
									'nickname'=>$userInfo['nickname']
									
									));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'登录失败'));
		}
		
		
		
	}
	
	/*获取可做剪辑任务的媒体列表*/
	public function get_paper(){

		$input_data = file_get_contents('php://input');//获取post数据
		$input_data = json_decode($input_data,true);//把json转化成数组
		$wx_id = intval($input_data['wx_id']);//用户ID
		
		if($wx_id == 0){//判断传入的微信ID是否有值
			$this->ajaxReturn(array('code'=>1015,'msg'=>'wx_id不能为空'));
		}
		
		if(M('ad_input_user')->where(array('wx_id'=>$wx_id))->count() == 0){//判断传入的微信ID是否能查询到
			$this->ajaxReturn(array('code'=>1017,'msg'=>'查询不到此wx_id'));
		}

		$lockMedia = M('tmedia')->field('tmedia.fid')->where(array('flockuserid'=>$wx_id))->find();

		if($lockMedia){//判断该微信ID是否有锁定媒体
			$where = array();
			$where['tmedia.fid'] = intval($lockMedia['fid']);
			$haslock = intval($lockMedia['fid']);
			$where['tpapersource.fstate'] = array('in','0,1');
		
			$mediaList = M('tpapersource')
									->field('tmedia.fid as mediaid,tmedia.fmedianame as medianame,left(tmedia.fmediaclassid,2) as media_class,count(tmedia.fid) as taskcount')
									->join('tmedia on tmedia.fid = tpapersource.fmediaid')
									->group('tmedia.fid')
									->where($where)->select();//查询媒体列表
						
		}else{

			$where = array();
			$where['tmedia.flockuserid'] = array('exp','is null');//锁定用户字段等于NULL
			//$where['tmedia.fdpid'] = array('gt',0);//平台ID大于0 
			$haslock = 0;
			$where['tpapersource.fstate'] = array('in','0,1');
		
			$mediaList = M('tpapersource')
									->field('tmedia.fid as mediaid,tmedia.fmedianame as medianame,left(tmedia.fmediaclassid,2) as media_class,count(tmedia.fid) as taskcount')
									->join('tmedia on tmedia.fid = tpapersource.fmediaid')
									->group('tmedia.fid')
									->where($where)->select();//查询媒体列表
			//var_dump(M('tseperatetask')->getLastSql());						
		}			
								
								
								
		//var_dump(M('tseperatetask')->getLastSql());						
		$this->ajaxReturn(array('code'=>0,'msg'=>'','haslock'=>$haslock,'mediaList'=>$mediaList));

	}
	
	/*修改报纸任务状态*/
	public function edit_paper_task(){

		$input_data = file_get_contents('php://input');//获取post数据
		$input_data = json_decode($input_data,true);//把json转化成数组
		
		$wx_id = intval($input_data['wx_id']);//微信ID
		if(M('ad_input_user')->where(array('wx_id'=>$wx_id))->count() == 0){//判断传入的微信ID是否能查询到
			$this->ajaxReturn(array('code'=>1017,'msg'=>'查询不到此wx_id'));
		}
		
		$seperatetaskid = intval($input_data['seperatetaskid']);//任务ID
		$state = intval($input_data['state']);//任务状态
		
		$taskInfo = M('tpapersource')->where(array('fid'=>$seperatetaskid))->find();//查询任务详细信息
		// var_dump($taskInfo);
		// exit;
		if(intval($taskInfo['fuserid']) == 0){//判断该任务是否有人接
			$rr = M('tpapersource')->where(array('fid'=>$seperatetaskid))->save(array('fuserid'=>$wx_id,'fstate'=>$state));//修改用户ID和状态
		}else{
			if(intval($taskInfo['fuserid']) != $wx_id){//判断该任务是否与该用户匹配
				//暂时不判断 $this->ajaxReturn(array('code'=>-1,'msg'=>'该任务与该用户不匹配,执行失败'));
			}
			$rr = M('tpapersource')->where(array('fid'=>$seperatetaskid))->save(array('fstate'=>$state,'fuserid'=>$wx_id));//修改用户ID和状态
		}
		
		if($rr > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'状态修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'状态修改失败,原因未知'));
		}

	}


}