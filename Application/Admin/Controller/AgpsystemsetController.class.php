<?php
//客户平台配置
namespace Admin\Controller;
use Think\Controller;
class AgpsystemsetController extends BaseController {

    /**
     * 客户列表
     * by zw
     */
    public function index(){
        $p = I('p',1);//当前第几页
        $pp = 10;//每页显示多少记录

        $keyword = I('keyword');//客户名称或编号

        $where_cr['cr_name|cr_num'] = array('like','%'.$keyword.'%');
        //记录总数
        $count = M('customer')
            ->where($where_cr)
            ->count();

        $Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数

        //数据列表
        $do_cr = M('customer')
            ->alias('a')
            ->field('a.*,(case when cr_status=1 then "开启" else "关闭" end) as cr_status2,fname')
            ->join('tperson b on a.cr_edituserid = b.fid','left')
            ->where($where_cr)
            ->order('cr_id asc')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();

        $this->assign('keyword',$keyword);
        $this->assign('data',$do_cr);
        $this->assign('page',$Page->show());//分页输出
        $this->display();
    }

    /**
     * 客户添加动作
     * by zw
     */
    public function customer_addaction(){
        $data_cr['cr_name'] = I('cr_name');
        $data_cr['cr_num'] = I('cr_num');
        $data_cr['cr_url'] = I('cr_url');
        $data_cr['cr_status'] = I('cr_status');
        $data_cr['cr_content'] = I('cr_content');
        $data_cr['cr_createuserid'] = session('personInfo.fid');
        $data_cr['cr_createtime'] = date('Y-m-d H:i:s');
        $data_cr['cr_edituserid'] = session('personInfo.fid');
        $data_cr['cr_edittime'] = date('Y-m-d H:i:s');
        if(strlen($data_cr['cr_num']) > 6){
            $data_cr['cr_orgid'] = $data_cr['cr_num'];
        }else{
            $data_cr['cr_orgid'] = '20'.$data_cr['cr_num'];
        }

        $do_cr = M('customer')->add($data_cr);
        if($do_cr){
            $this->ajaxReturn(array('code' => 0,'msg'=>'添加成功'));
        }else{
            $this->ajaxReturn(array('code' => -1,'msg'=>'添加失败'));
        }
    }

    /**
     * 客户修改界面
     * by zw
     */
    public function customer_editform(){
        $where_cr['cr_id'] = I('cr_id');

        $do_cr = M('customer')->where($where_cr)->find();
        if($do_cr){
            $this->ajaxReturn(array('code' => 0,'msg'=>'获取成功','data'=>$do_cr));
        }else{
            $this->ajaxReturn(array('code' => -1,'msg'=>'获取失败'));
        }
    }

    /**
     * 客户修改动作
     * by zw
     */
    public function customer_editaction(){
        $data_cr['cr_name'] = I('cr_name');
        $data_cr['cr_num'] = I('cr_num');
        $data_cr['cr_url'] = I('cr_url');
        $data_cr['cr_status'] = I('cr_status');
        $data_cr['cr_content'] = I('cr_content');
        $data_cr['cr_edituserid'] = session('personInfo.fid');
        $data_cr['cr_edittime'] = date('Y-m-d H:i:s');
        if(strlen($data_cr['cr_num']) > 6){
            $data_cr['cr_orgid'] = $data_cr['cr_num'];
        }else{
            $data_cr['cr_orgid'] = '20'.$data_cr['cr_num'];
        }

        $where_cr['cr_id'] = I('cr_id');
        $do_cr = M('customer')->where($where_cr)->save($data_cr);
        if($do_cr){
            $this->ajaxReturn(array('code' => 0,'msg'=>'保存成功'));
        }else{
            $this->ajaxReturn(array('code' => -1,'msg'=>'保存失败'));
        }
    }

    /**
     * 客户删除动作
     * by zw
     */
    public function customer_delaction(){
        $cr_id = I('cr_id');
        if($cr_id == 1 || empty($cr_id)){
            $this->ajaxReturn(array('code' => -1,'msg'=>'删除失败'));
        }
        $where_cr['cr_id'] = $cr_id;

        $do_cr = M('customer')->where($where_cr)->delete();
        if($do_cr){
            $this->ajaxReturn(array('code' => 0,'msg'=>'删除成功'));
        }else{
            $this->ajaxReturn(array('code' => -1,'msg'=>'删除失败'));
        }
    }

    /**
     * 参数列表
     * by zw
     */
    public function systemset(){
        $cr_id = I('cr_id');//客户ID

        $where_cr['cst_crid'] = $cr_id;
        $do_cr = M('customer_systemset')
            ->alias('a')
            ->field('a.*,b.fname')
            ->join('tperson b on a.cst_edituserid = b.fid','left')
            ->where($where_cr)
            ->order('cst_id asc')
            ->select();

        $this->assign('cr_id',$cr_id);
        $this->assign('data',$do_cr);
        $this->display();
    }

    /**
     * 参数添加动作
     * by zw
     */
    public function set_addaction(){
        $data_cr['cst_crid'] = I('cst_crid');
        $data_cr['cst_name'] = trim(I('cst_name'));
        if(I('cst_type') == 'json'){
            $data_cr['cst_val'] = json_encode(json_decode(trim($_POST['cst_val']),true));
        }else{
            $data_cr['cst_val'] = trim($_POST['cst_val']);
        }
        $data_cr['cst_type'] = I('cst_type');
        $data_cr['cst_content'] = I('cst_content');
        $data_cr['cst_createuserid'] = session('personInfo.fid');
        $data_cr['cst_createtime'] = date('Y-m-d H:i:s');
        $data_cr['cst_edituserid'] = session('personInfo.fid');
        $data_cr['cst_edittime'] = date('Y-m-d H:i:s');

        $do_cr = M('customer_systemset')->add($data_cr);
        if($do_cr){
            $this->ajaxReturn(array('code' => 0,'msg'=>'添加成功'));
        }else{
            $this->ajaxReturn(array('code' => -1,'msg'=>'添加失败'));
        }
    }

    /**
     * 参数修改界面
     * by zw
     */
    public function set_editform(){
        $where_cr['cst_id'] = I('cst_id');

        $do_cr = M('customer_systemset')->where($where_cr)->find();
        if($do_cr){
            $do_cr['cst_val'] = html_entity_decode($do_cr['cst_val']);
            $this->ajaxReturn(array('code' => 0,'msg'=>'获取成功','data'=>$do_cr));
        }else{
            $this->ajaxReturn(array('code' => -1,'msg'=>'获取失败'));
        }
    }

    /**
     * 参数修改动作
     * by zw
     */
    public function set_editaction(){

        $data_cr['cst_crid'] = I('cst_crid');
        $data_cr['cst_name'] = trim(I('cst_name'));
        if(I('cst_type') == 'json'){
            $data_cr['cst_val'] = json_encode(json_decode(trim($_POST['cst_val']),true));
        }else{
            $data_cr['cst_val'] = trim($_POST['cst_val']);
        }
        $data_cr['cst_type'] = I('cst_type');
        $data_cr['cst_content'] = I('cst_content');
        $data_cr['cst_edituserid'] = session('personInfo.fid');
        $data_cr['cst_edittime'] = date('Y-m-d H:i:s');

        $where_cr['cst_id'] = I('cst_id');
        $do_cr = M('customer_systemset')->where($where_cr)->save($data_cr);
        if($do_cr){
            $this->ajaxReturn(array('code' => 0,'msg'=>'保存成功'));
        }else{
            $this->ajaxReturn(array('code' => -1,'msg'=>'保存失败'));
        }
    }

    /**
     * 参数删除动作
     * by zw
     */
    public function set_delaction(){
        $cst_id = I('cst_id');
        
        $where_cr['cst_id'] = $cst_id;

        $do_cr = M('customer_systemset')->where($where_cr)->delete();
        if($do_cr){
            $this->ajaxReturn(array('code' => 0,'msg'=>'删除成功'));
        }else{
            $this->ajaxReturn(array('code' => -1,'msg'=>'删除失败'));
        }
    }

    /**
     * 获取默认参数
     * by zw
     */
    public function set_getset(){
        $cst_crid = I('cst_crid');//客户ID
        $where_cr['cst_crid'] = $cst_crid;
        $do_cr = M('customer_systemset')
            ->where($where_cr)
            ->count();
        if(!empty($do_cr)){
            $this->ajaxReturn(array('code' => -1,'msg'=>'获取失败，已存在配置，请先删除'));
        }else{
            $where_mcr['cst_crid'] = 1;
            $do_mcr = M('customer_systemset')
                ->where($where_mcr)
                ->select();
            if(!empty($do_mcr)){
                $adddata = [];
                foreach ($do_mcr as $key => $value) {
                    $adddata[$key]['cst_crid'] = $cst_crid;
                    $adddata[$key]['cst_name'] = $value['cst_name'];
                    $adddata[$key]['cst_val'] = $value['cst_val'];
                    $adddata[$key]['cst_type'] = $value['cst_type'];
                    $adddata[$key]['cst_content'] = $value['cst_content'];
                    $adddata[$key]['cst_createtime'] = date('Y-m-d H:i:s');
                    $adddata[$key]['cst_createuserid'] = session('personInfo.fid');
                    $adddata[$key]['cst_edittime'] = date('Y-m-d H:i:s');
                    $adddata[$key]['cst_edituserid'] = session('personInfo.fid');
                }
                if(!empty($adddata)){
                    M('customer_systemset')->addall($adddata);
                }
            }
            $this->ajaxReturn(array('code' => 0,'msg'=>'获取完成'));
        }

    }


/*    批量配置
    public function batch_set(){
        $cst_name = I('cst_name',0);//配置名称
        $cst_val = I('cst_val',0);//值
        $cst_type = I('cst_type',0);//变量值类型（ int，varchar）
        $cst_content = I('cst_content',0);//说明
        $cst_createuserid = session('personInfo.fid');
        $cst_createtime = date('Y-m-d H:i:s');
        $cst_edituserid = session('personInfo.fid');
        $cst_edittime = date('Y-m-d H:i:s');
        $cr_ids = I('cr_ids');
        foreach($cr_ids as $cr_id){
            echo 1;
        }
    }

    获取所有客户
    public function get_customer(){
        $customer = M('customer')->field("cr_id,cr_name")->where(['cr_num'=>['NEQ',0]])->select();
        if(!empty($customer)){
            $this->ajaxReturn([
                'code'=>0,
                'data'=>$customer,
                'msg'=>'接收成功！'
             ]);
        }else{
            $this->ajaxReturn([
                'code'=>1,
                'msg'=>'接收失败'
            ]);
        }
    }*/

}

