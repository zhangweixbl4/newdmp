<?php
namespace Gongshang\Model;



class ReportMakeModel{

	//区域统计，统计监测情况
	public function issue_count($region_id,$start_date,$end_date){


		if($region_id == 100000) $region_id = '000000';

		$startTime = date('Y-m-d H:i:s',strtotime($start_date));//开始时间
		$endTime = date('Y-m-d H:i:s',strtotime($end_date) + 86399);//结束时间

		$region_id_rtrim = rtrim($region_id,'00');//去掉地区后面的00
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位

		$where = array();
		if($region_id == '340000'){
			$where['tmedia.fid'] = array('not in',C('ANHUI_MEDIA'));
			$where['tregion.fid'] = $region_id;
		}else{
			$where['left(tregion.fid,'.$tregion_len.')'] = $region_id_rtrim;
		}	
		
		if($startTime && $endTime){
			$where['issue.fissuedate'] = array('between',$startTime.','.$endTime);//时间查询条件
		}
		
		
		$tvissue = M('ttvissue')
							->alias('issue')
							->field('	
										count(1) as illegal_0_count,
										count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegal_count,
										count(case when ttvsample.fillegaltypecode = 10 then 1 else null end) as illegal_10_count,
										count(case when ttvsample.fillegaltypecode = 20 then 1 else null end) as illegal_20_count,
										count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as illegal_30_count,
										count(distinct tmedia.fid) as media_count
									')
							->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
							->join('tmedia on tmedia.fid = issue.fmediaid')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							->where($where)
							->select();
		
		$bcissue = M('tbcissue')
							->alias('issue')
							->field('	
										count(1) as illegal_0_count,
										count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegal_count,
										count(case when tbcsample.fillegaltypecode = 10 then 1 else null end) as illegal_10_count,
										count(case when tbcsample.fillegaltypecode = 20 then 1 else null end) as illegal_20_count,
										count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as illegal_30_count,
										count(distinct tmedia.fid) as media_count
									')
							->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
							->join('tmedia on tmedia.fid = issue.fmediaid')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							->where($where)
							->select();
		
		$paperissue = M('tpaperissue')
							->alias('issue')
							->field('	
										count(1) as illegal_0_count,
										count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegal_count,
										count(case when tpapersample.fillegaltypecode = 10 then 1 else null end) as illegal_10_count,
										count(case when tpapersample.fillegaltypecode = 20 then 1 else null end) as illegal_20_count,
										count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as illegal_30_count,
										count(distinct tmedia.fid) as media_count
									')
							->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
							->join('tmedia on tmedia.fid = issue.fmediaid')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							->where($where)
							->select();
		
		
		
			

		$data['tv'] = $tvissue[0];//电视数据
		$data['bc'] = $bcissue[0];//广播数据
		$data['paper'] = $paperissue[0];//报纸数据
		return $data;

	}
	
	
	//媒介统计,统计各个媒介的发布情况，违法情况等
	public function issue_count2($region_id,$start_date,$end_date){


		if($region_id == 100000) $region_id = '000000';

		$startTime = date('Y-m-d H:i:s',strtotime($start_date));//开始时间
		$endTime = date('Y-m-d H:i:s',strtotime($end_date) + 86399);//结束时间
		

		$region_id_rtrim = rtrim($region_id,'00');//去掉地区后面的00
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位

		$where = array();
		if($region_id == '340000'){
			$where['tmedia.fid'] = array('not in',C('ANHUI_MEDIA'));
			$where['tregion.fid'] = $region_id;
		}else{
			$where['left(tregion.fid,'.$tregion_len.')'] = $region_id_rtrim;
		}
		
		if($startTime && $endTime){
			$where['issue.fissuedate'] = array('between',$startTime.','.$endTime);//时间查询条件
		}
		
		
		$tvissue = M('ttvissue')//查询电视发布
							->alias('issue')
							->field('	
										tmedia.fmedianame as medianame,
										"电视" as mediaclass,
										count(case when issue.ftvissueid > 0 then 1 end) as sl,
										count(case when ttvsample.fillegaltypecode > 0 then 1 end) as wfsl,
										count(case when ttvsample.fillegaltypecode = 30 then 1 end) as yzwfsl,
										count(case when tad.fadclasscode = "2202" then 1 end) as gyggsl,
										count(case when left(tad.fadclasscode,2) in ("01","02","03","06","13") THEN 1 END) AS wdlsl,
										count(case when left(tad.fadclasscode,2) in ("01","02","03","06","13") and ttvsample.fillegaltypecode > 0 THEN 1 END) AS wdlwfsl,
										count(case when left(tad.fadclasscode,2) in ("01","02","03","06","13") and ttvsample.fillegaltypecode = 30 THEN 1 END) AS wdlyzwfsl

									')
							->join('ttvsample on ttvsample.fid = issue.ftvsampleid')//连接样本表
							->join('tad on tad.fadid = ttvsample.fadid')//连接广告表
							->join('tmedia on tmedia.fid = issue.fmediaid')//连接媒介表
							->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')//连接媒介类型表
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构表
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							->where($where)//查询条件
							->group('tmedia.fid')
							->select();
			
		foreach($tvissue as $key => $issue){//循环计算违法率、发布率等
			$tvissue[$key]['wfl'] = round($issue['wfsl'] / $issue['sl'] * 100 ,2) . '％';//违法率
			$tvissue[$key]['yzwfl'] = round($issue['yzwfsl'] / $issue['sl'] * 100 ,2) . '％';//严重违法率
			$tvissue[$key]['gyggfbl'] = round($issue['gyggsl'] / $issue['sl'] * 100 ,2) . '％';//公益广告发布率
			$tvissue[$key]['wdlwfl'] = round($issue['wdlwfsl'] / $issue['wdlsl'] * 100 ,2) . '％';//五大类违法率
			$tvissue[$key]['wdlyzwfl'] = round($issue['wdlyzwfsl'] / $issue['wdlsl'] * 100 ,2) . '％';//五大类严重违法率
			unset($key);
			unset($issue);
		}					
							
		$bcissue = M('tbcissue')//查询广播发布
							->alias('issue')
							->field('	
										tmedia.fmedianame as medianame,
										"广播" as mediaclass,
										count(case when issue.fbcissueid > 0 then 1 end) as sl,
										count(case when tbcsample.fillegaltypecode > 0 then 1 end) as wfsl,
										count(case when tbcsample.fillegaltypecode = 30 then 1 end) as yzwfsl,
										count(case when tad.fadclasscode = "2202" then 1 end) as gyggsl,
										count(case when left(tad.fadclasscode,2) in ("01","02","03","06","13") THEN 1 END) AS wdlsl,
										count(case when left(tad.fadclasscode,2) in ("01","02","03","06","13") and tbcsample.fillegaltypecode > 0 THEN 1 END) AS wdlwfsl,
										count(case when left(tad.fadclasscode,2) in ("01","02","03","06","13") and tbcsample.fillegaltypecode = 30 THEN 1 END) AS wdlyzwfsl

									')
							->join('tbcsample on tbcsample.fid = issue.fbcsampleid')//连接样本表
							->join('tad on tad.fadid = tbcsample.fadid')//连接广告表
							->join('tmedia on tmedia.fid = issue.fmediaid')//连接媒介表
							->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')//连接媒介类型表
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构表
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							->where($where)//查询条件
							->group('tmedia.fid')
							->select();
							
		foreach($bcissue as $key => $issue){//循环计算违法率、发布率等
			$bcissue[$key]['wfl'] = round($issue['wfsl'] / $issue['sl'] * 100 ,2) . '％';//违法率
			$bcissue[$key]['yzwfl'] = round($issue['yzwfsl'] / $issue['sl'] * 100 ,2) . '％';//严重违法率
			$bcissue[$key]['gyggfbl'] = round($issue['gyggsl'] / $issue['sl'] * 100 ,2) . '％';//公益广告发布率
			$bcissue[$key]['wdlwfl'] = round($issue['wdlwfsl'] / $issue['wdlsl'] * 100 ,2) . '％';//五大类违法率
			$bcissue[$key]['wdlyzwfl'] = round($issue['wdlyzwfsl'] / $issue['wdlsl'] * 100 ,2) . '％';//五大类严重违法率
			unset($key);
			unset($issue);
		}

		$paperissue = M('tpaperissue')//查询报纸发布
							->alias('issue')
							->field('	
										tmedia.fmedianame as medianame,
										"报纸" as mediaclass,
										count(case when issue.fpaperissueid > 0 then 1 end) as sl,
										count(case when tpapersample.fillegaltypecode > 0 then 1 end) as wfsl,
										count(case when tpapersample.fillegaltypecode = 30 then 1 end) as yzwfsl,
										count(case when tad.fadclasscode = "2202" then 1 end) as gyggsl,
										count(case when left(tad.fadclasscode,2) in ("01","02","03","06","13") THEN 1 END) AS wdlsl,
										count(case when left(tad.fadclasscode,2) in ("01","02","03","06","13") and tpapersample.fillegaltypecode > 0 THEN 1 END) AS wdlwfsl,
										count(case when left(tad.fadclasscode,2) in ("01","02","03","06","13") and tpapersample.fillegaltypecode = 30 THEN 1 END) AS wdlyzwfsl

									')
							->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')//连接样本表
							->join('tad on tad.fadid = tpapersample.fadid')//连接广告表
							->join('tmedia on tmedia.fid = issue.fmediaid')//连接媒介表
							->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')//连接媒介类型表
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构表
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							->where($where)//查询条件
							->group('tmedia.fid')
							->select();
							
		foreach($paperissue as $key => $issue){//循环计算违法率、发布率等
			$paperissue[$key]['wfl'] = round($issue['wfsl'] / $issue['sl'] * 100 ,2) . '％';//违法率
			$paperissue[$key]['yzwfl'] = round($issue['yzwfsl'] / $issue['sl'] * 100 ,2) . '％';//严重违法率
			$paperissue[$key]['gyggfbl'] = round($issue['gyggsl'] / $issue['sl'] * 100 ,2) . '％';//公益广告发布率
			$paperissue[$key]['wdlwfl'] = round($issue['wdlwfsl'] / $issue['wdlsl'] * 100 ,2) . '％';//五大类违法率
			$paperissue[$key]['wdlyzwfl'] = round($issue['wdlyzwfsl'] / $issue['wdlsl'] * 100 ,2) . '％';//五大类严重违法率
			unset($key);
			unset($issue);
		}
		
		$issue = array_merge($tvissue,$bcissue,$paperissue);
		foreach($issue as $key => $item){
			$issue[$key]['id'] = $key + 1;
		}

		return $issue;

	}
	
	/*五大类广告发布情况统计*/
	public function issue_count3($region_id,$start_date,$end_date){
		
		if($region_id == 100000) $region_id = '000000';

		$startTime = date('Y-m-d H:i:s',strtotime($start_date));//开始时间
		$endTime = date('Y-m-d H:i:s',strtotime($end_date) + 86399);//结束时间
		

		$region_id_rtrim = rtrim($region_id,'00');//去掉地区后面的00
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位

		$where = array();
		if($region_id == '340000'){
			$where['tmedia.fid'] = array('not in',C('ANHUI_MEDIA'));
			$where['tregion.fid'] = $region_id;
		}else{
			$where['left(tregion.fid,'.$tregion_len.')'] = $region_id_rtrim;
		}
		$where['left(tad.fadclasscode,2)'] = array('in','01,02,03,06,13');//五大类分类查询条件
		
		if($startTime && $endTime){
			$where['issue.fissuedate'] = array('between',$startTime.','.$endTime);//时间查询条件
		}
		
		$tvissue = M('ttvissue')
							->alias('issue')
							->field('	
										count(1) as illegal_0_count,
										count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegal_count,
										count(case when ttvsample.fillegaltypecode = 10 then 1 else null end) as illegal_10_count,
										count(case when ttvsample.fillegaltypecode = 20 then 1 else null end) as illegal_20_count,
										count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as illegal_30_count,
										count(distinct tmedia.fid) as media_count
									')
							->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
							->join('tad on tad.fadid = ttvsample.fadid')//连接广告表
							->join('tmedia on tmedia.fid = issue.fmediaid')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							->where($where)
							->select();
							
		$bcissue = M('tbcissue')
							->alias('issue')
							->field('	
										count(1) as illegal_0_count,
										count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegal_count,
										count(case when tbcsample.fillegaltypecode = 10 then 1 else null end) as illegal_10_count,
										count(case when tbcsample.fillegaltypecode = 20 then 1 else null end) as illegal_20_count,
										count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as illegal_30_count,
										count(distinct tmedia.fid) as media_count
									')
							->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
							->join('tad on tad.fadid = tbcsample.fadid')//连接广告表
							->join('tmedia on tmedia.fid = issue.fmediaid')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							->where($where)
							->select();
							
		$paperissue = M('tpaperissue')
							->alias('issue')
							->field('	
										count(1) as illegal_0_count,
										count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegal_count,
										count(case when tpapersample.fillegaltypecode = 10 then 1 else null end) as illegal_10_count,
										count(case when tpapersample.fillegaltypecode = 20 then 1 else null end) as illegal_20_count,
										count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as illegal_30_count,
										count(distinct tmedia.fid) as media_count
									')
							->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
							->join('tad on tad.fadid = tpapersample.fadid')//连接广告表
							->join('tmedia on tmedia.fid = issue.fmediaid')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							->where($where)
							->select();
							
							
		$data['tv'] = $tvissue[0];//电视数据
		$data['bc'] = $bcissue[0];//广播数据
		$data['paper'] = $paperissue[0];//报纸数据	
		return $data;
						
	}
	
	//媒介统计,统计各个媒介的五大类违法情况、严重违法情况
	public function issue_count4($region_id,$start_date,$end_date){


		if($region_id == 100000) $region_id = '000000';

		$startTime = date('Y-m-d H:i:s',strtotime($start_date));//开始时间
		$endTime = date('Y-m-d H:i:s',strtotime($end_date) + 86399);//结束时间
		

		$region_id_rtrim = rtrim($region_id,'00');//去掉地区后面的00
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位

		$where = array();
		if($region_id == '340000'){
			$where['tmedia.fid'] = array('not in',C('ANHUI_MEDIA'));
			$where['tregion.fid'] = $region_id;
		}else{
			$where['left(tregion.fid,'.$tregion_len.')'] = $region_id_rtrim;
		}
		$where['left(tad.fadclasscode,2)'] = array('in','01,02,03,06,13');//五大类分类查询条件

		
		if($startTime && $endTime){
			$where['issue.fissuedate'] = array('between',$startTime.','.$endTime);//时间查询条件
			$where['sample.fillegaltypecode'] = array('gt',0);
		}
		
		
		$tvissue = M('ttvissue')//查询电视发布
							->alias('issue')
							->field('	
										tmedia.fmedianame as medianame,
										"电视" as mediaclass,
										count(case when left(tad.fadclasscode,2) = "01" and sample.fillegaltypecode > 0 THEN 1 END) AS wfsl1,
										count(case when left(tad.fadclasscode,2) = "01" and sample.fillegaltypecode = 30 THEN 1 END) AS yzwfsl1,
										count(case when left(tad.fadclasscode,2) = "02" and sample.fillegaltypecode > 0 THEN 1 END) AS wfsl2,
										count(case when left(tad.fadclasscode,2) = "02" and sample.fillegaltypecode = 30 THEN 1 END) AS yzwfsl2,
										count(case when left(tad.fadclasscode,2) = "03" and sample.fillegaltypecode > 0 THEN 1 END) AS wfsl3,
										count(case when left(tad.fadclasscode,2) = "03" and sample.fillegaltypecode = 30 THEN 1 END) AS yzwfsl3,
										count(case when left(tad.fadclasscode,2) = "06" and sample.fillegaltypecode > 0 THEN 1 END) AS wfsl4,
										count(case when left(tad.fadclasscode,2) = "06" and sample.fillegaltypecode = 30 THEN 1 END) AS yzwfsl4,
										count(case when left(tad.fadclasscode,2) = "13" and sample.fillegaltypecode > 0 THEN 1 END) AS wfsl5,
										count(case when left(tad.fadclasscode,2) = "13" and sample.fillegaltypecode = 30 THEN 1 END) AS yzwfsl5
									')
							->join('ttvsample as sample on sample.fid = issue.ftvsampleid')//连接样本表
							->join('tad on tad.fadid = sample.fadid')//连接广告表
							->join('tmedia on tmedia.fid = issue.fmediaid')//连接媒介表
							->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')//连接媒介类型表
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构表
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							->where($where)//查询条件
							->group('tmedia.fid')
							->select();
							
		$bcissue = M('tbcissue')//查询广播发布
							->alias('issue')
							->field('	
										tmedia.fmedianame as medianame,
										"广播" as mediaclass,
										count(case when left(tad.fadclasscode,2) = "01" and sample.fillegaltypecode > 0 THEN 1 END) AS wfsl1,
										count(case when left(tad.fadclasscode,2) = "01" and sample.fillegaltypecode = 30 THEN 1 END) AS yzwfsl1,
										count(case when left(tad.fadclasscode,2) = "02" and sample.fillegaltypecode > 0 THEN 1 END) AS wfsl2,
										count(case when left(tad.fadclasscode,2) = "02" and sample.fillegaltypecode = 30 THEN 1 END) AS yzwfsl2,
										count(case when left(tad.fadclasscode,2) = "03" and sample.fillegaltypecode > 0 THEN 1 END) AS wfsl3,
										count(case when left(tad.fadclasscode,2) = "03" and sample.fillegaltypecode = 30 THEN 1 END) AS yzwfsl3,
										count(case when left(tad.fadclasscode,2) = "06" and sample.fillegaltypecode > 0 THEN 1 END) AS wfsl4,
										count(case when left(tad.fadclasscode,2) = "06" and sample.fillegaltypecode = 30 THEN 1 END) AS yzwfsl4,
										count(case when left(tad.fadclasscode,2) = "13" and sample.fillegaltypecode > 0 THEN 1 END) AS wfsl5,
										count(case when left(tad.fadclasscode,2) = "13" and sample.fillegaltypecode = 30 THEN 1 END) AS yzwfsl5
									')
							->join('tbcsample as sample on sample.fid = issue.fbcsampleid')//连接样本表
							->join('tad on tad.fadid = sample.fadid')//连接广告表
							->join('tmedia on tmedia.fid = issue.fmediaid')//连接媒介表
							->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')//连接媒介类型表
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构表
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							->where($where)//查询条件
							->group('tmedia.fid')
							->select();
							
		$paperissue = M('tpaperissue')//查询电视发布
							->alias('issue')
							->field('	
										tmedia.fmedianame as medianame,
										"报纸" as mediaclass,
										count(case when left(tad.fadclasscode,2) = "01" and sample.fillegaltypecode > 0 THEN 1 END) AS wfsl1,
										count(case when left(tad.fadclasscode,2) = "01" and sample.fillegaltypecode = 30 THEN 1 END) AS yzwfsl1,
										count(case when left(tad.fadclasscode,2) = "02" and sample.fillegaltypecode > 0 THEN 1 END) AS wfsl2,
										count(case when left(tad.fadclasscode,2) = "02" and sample.fillegaltypecode = 30 THEN 1 END) AS yzwfsl2,
										count(case when left(tad.fadclasscode,2) = "03" and sample.fillegaltypecode > 0 THEN 1 END) AS wfsl3,
										count(case when left(tad.fadclasscode,2) = "03" and sample.fillegaltypecode = 30 THEN 1 END) AS yzwfsl3,
										count(case when left(tad.fadclasscode,2) = "06" and sample.fillegaltypecode > 0 THEN 1 END) AS wfsl4,
										count(case when left(tad.fadclasscode,2) = "06" and sample.fillegaltypecode = 30 THEN 1 END) AS yzwfsl4,
										count(case when left(tad.fadclasscode,2) = "13" and sample.fillegaltypecode > 0 THEN 1 END) AS wfsl5,
										count(case when left(tad.fadclasscode,2) = "13" and sample.fillegaltypecode = 30 THEN 1 END) AS yzwfsl5
									')
							->join('tpapersample as sample on sample.fpapersampleid = issue.fpapersampleid')//连接样本表
							->join('tad on tad.fadid = sample.fadid')//连接广告表
							->join('tmedia on tmedia.fid = issue.fmediaid')//连接媒介表
							->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')//连接媒介类型表
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构表
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							->where($where)//查询条件
							->group('tmedia.fid')
							->select();
							
							
							
		
		
		$issue = array_merge($tvissue,$bcissue,$paperissue);
		foreach($issue as $key => $item){
			$issue[$key]['id'] = $key + 1;
		}

		return $issue;

	}
	//公益广告统计
	public function issue_count5($region_id,$start_date,$end_date){


		if($region_id == 100000) $region_id = '000000';

		$startTime = date('Y-m-d H:i:s',strtotime($start_date));//开始时间
		$endTime = date('Y-m-d H:i:s',strtotime($end_date) + 86399);//结束时间
		

		$region_id_rtrim = rtrim($region_id,'00');//去掉地区后面的00
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位

		$where = array();
		if($region_id == '340000'){
			$where['tmedia.fid'] = array('not in',C('ANHUI_MEDIA'));
			$where['tregion.fid'] = $region_id;
		}else{
			$where['left(tregion.fid,'.$tregion_len.')'] = $region_id_rtrim;
		}

		
		if($startTime && $endTime){
			$where['issue.fissuedate'] = array('between',$startTime.','.$endTime);//时间查询条件
		}
		
		
		$tvissue = M('ttvissue')//查询电视发布
							->alias('issue')
							->field('	
										count(1) AS sl,
										count(case when tad.fadclasscode = "2202" THEN 1 END) AS gyggsl
										
									')
							->join('ttvsample as sample on sample.fid = issue.ftvsampleid')//连接样本表
							->join('tad on tad.fadid = sample.fadid')//连接广告表
							->join('tmedia on tmedia.fid = issue.fmediaid')//连接媒介表
							->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')//连接媒介类型表
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构表
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							->where($where)//查询条件
							->select();
		$bcissue = M('tbcissue')//查询电视发布
							->alias('issue')
							->field('	
										count(1) AS sl,
										count(case when tad.fadclasscode = "2202" THEN 1 END) AS gyggsl
										
									')
							->join('tbcsample as sample on sample.fid = issue.fbcsampleid')//连接样本表
							->join('tad on tad.fadid = sample.fadid')//连接广告表
							->join('tmedia on tmedia.fid = issue.fmediaid')//连接媒介表
							->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')//连接媒介类型表
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构表
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							->where($where)//查询条件
							->select();
		$paperissue = M('tpaperissue')//查询电视发布
							->alias('issue')
							->field('	
										count(1) AS sl,
										count(case when tad.fadclasscode = "2202" THEN 1 END) AS gyggsl
										
									')
							->join('tpapersample as sample on sample.fpapersampleid = issue.fpapersampleid')//连接样本表
							->join('tad on tad.fadid = sample.fadid')//连接广告表
							->join('tmedia on tmedia.fid = issue.fmediaid')//连接媒介表
							->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')//连接媒介类型表
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构表
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							->where($where)//查询条件
							->select();
							
		
							
							
							
		
		
		$issue = array_merge($tvissue,$bcissue,$paperissue);
		$data['tv'] = $tvissue[0];//电视数据
		$data['bc'] = $bcissue[0];//广播数据
		$data['paper'] = $paperissue[0];//报纸数据	
		
		return $data;


	}

	//根据区域统计的广告情况
	public function region_issue($region_id,$start_date,$end_date){

		if($region_id == 100000) $region_id = '000000';

		$startTime = date('Y-m-d H:i:s',strtotime($start_date));//开始时间
		$endTime = date('Y-m-d H:i:s',strtotime($end_date) + 86399);//结束时间

		$region_id_rtrim = rtrim($region_id,'00');//去掉地区后面的00
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位

		//where条件
		$where = array();
		if($region_id == '340000'){
			$where['tmedia.fid'] = array('not in',C('ANHUI_MEDIA'));
			$where['tregion.fid'] = $region_id;
			$media_where['tmediaowner.fregionid'] = $region_id;
		}else{
			$where['left(tregion.fid,'.$tregion_len.')'] = $region_id_rtrim;
			$media_where['left(tmediaowner.fregionid,'.$tregion_len.')'] = $region_id_rtrim;
		}	
		if($startTime && $endTime){
			$where['issue.fissuedate'] = array('between',$startTime.','.$endTime);//时间查询条件
		}
		
		//统计媒体数
		
		$media_count = M('tmedia')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->where($media_where)->count();

		$tv_issue = M('ttvissue')
							->alias('issue')
							->field('	
										tregion.fid,
										tregion.fname as dymc,
										count(1) as ztc,
										count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as wftc
									')
							->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
							->join('tmedia on tmedia.fid = issue.fmediaid')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							// ->join('tregion as region on region.fid =  concat(left(tregion.fid,'.($tregion_len + 2).'),"'.str_pad('',6 - ($tregion_len + 2),'0').'")') //连接地区表（监管机构的下一级地区）
							->where($where)
							->group('tregion.fid')
							->select();
		$tv_res = $this->count_illegality($tv_issue);//计算违法率
		$tv_mediatype = $tv_res['mediatype'];//电视统计情况
		$tv_mediatype['percent'] = round($tv_mediatype['illegal']/$tv_mediatype['total']*100,2).'％';
		$tv_mediatype['total'] = (string)$tv_mediatype['total'];
		$tv_mediatype['illegal'] = (string)$tv_mediatype['illegal'];
		$tv_mediatype = array_merge(array('mediaclass' => '电视'),$tv_mediatype);
		$tvissue = $tv_res['issue'];
		$tvissue = $this->region_sort($tvissue,'tcwfl',SORT_DESC);//根据违法率从高到低排序

		$bc_issue = M('tbcissue')
							->alias('issue')
							->field('	
										tregion.fid,
										tregion.fname as dymc,
										count(1) as ztc,
										count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as wftc
									')
							->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
							->join('tmedia on tmedia.fid = issue.fmediaid')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							// ->join('tregion as region on region.fid =  concat(left(tregion.fid,'.($tregion_len + 2).'),"'.str_pad('',6 - ($tregion_len + 2),'0').'")') //连接地区表（监管机构的下一级地区）
							->where($where)
							->group('tregion.fid')
							->select();
		$bc_res = $this->count_illegality($bc_issue);//计算违法率
		$bc_mediatype = $bc_res['mediatype'];//广播统计情况
		$bc_mediatype['percent'] = round($bc_mediatype['illegal']/$bc_mediatype['total']*100,2).'％';
		$bc_mediatype['total'] = (string)$bc_mediatype['total'];
		$bc_mediatype['illegal'] = (string)$bc_mediatype['illegal'];
		$bc_mediatype = array_merge(array('mediaclass' => '广播'),$bc_mediatype);
		$bcissue = $bc_res['issue'];
		$bcissue = $this->region_sort($bcissue,'tcwfl',SORT_DESC);//根据违法率从高到低排序
		
		$paper_issue = M('tpaperissue')
							->alias('issue')
							->field('	
										tregion.fid,
										tregion.fname as dymc,
										count(1) as ztc,
										count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as wftc
									')
							->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
							->join('tmedia on tmedia.fid = issue.fmediaid')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
							// ->join('tregion as region on region.fid =  concat(left(tregion.fid,'.($tregion_len + 2).'),"'.str_pad('',6 - ($tregion_len + 2),'0').'")') //连接地区表（监管机构的下一级地区）
							->where($where)
							->group('tregion.fid')
							->select();
		$paper_res = $this->count_illegality($paper_issue);//计算违法率
		$paper_mediatype = $paper_res['mediatype'];//电视统计情况
		$paper_mediatype['percent'] = round($paper_mediatype['illegal']/$paper_mediatype['total']*100,2).'％';
		$paper_mediatype['total'] = (string)$paper_mediatype['total'];
		$paper_mediatype['illegal'] = (string)$paper_mediatype['illegal'];
		$paper_mediatype = array_merge(array('mediaclass' => '报刊'),$paper_mediatype);
		$paperissue = $paper_res['issue'];
		$paperissue = $this->region_sort($paperissue,'tcwfl',SORT_DESC);//根据违法率从高到低排序

		$issue = array_merge($tv_issue,$bc_issue,$paper_issue);
		foreach ($issue as $key => $value) {
			$issue_arr[$value['fid']]['dymc'] = $value['dymc'];
			$issue_arr[$value['fid']]['ztc'] += $value['ztc'];
			$issue_arr[$value['fid']]['wftc'] += $value['wftc'];			
		}
		$res = $this->count_illegality($issue_arr,1);//计算违法率
		$mediatype = $res['mediatype'];//报纸统计情况
		$mediatype['percent'] = round($mediatype['illegal']/$mediatype['total']*100,2).'％';
		$mediatype['total'] = (string)$mediatype['total'];
		$mediatype['illegal'] = (string)$mediatype['illegal'];
		$mediatype = array_merge(array('mediaclass' => '合计'),$mediatype);
		if($res['issue']){
			$issue_arr = $this->region_sort($res['issue'],'tcwfl',SORT_DESC);//根据违法率从高到低排序
		}else{
			$issue_arr = array();
		}	

		$data['issue'] = $issue_arr;
		$data['tv'] = $tvissue?$tvissue:array();//电视数据
		$data['bc'] = $bcissue?$bcissue:array();//广播数据
		$data['paper'] = $paperissue?$paperissue:array();//报纸数据
		$data['mediatype'] = $mediatype;
		$data['tv_mediatype'] = $tv_mediatype;
		$data['bc_mediatype'] = $bc_mediatype;
		$data['paper_mediatype'] = $paper_mediatype;
		$data['media_count'] = $media_count;//媒介数量
		return $data;

	}

	//计算违法率
	public function count_illegality($array,$sort=0){

		$sum = 0;
		foreach ($array as $key => $value) {
			$mediatype['total'] += $value['ztc'];
			$mediatype['illegal'] += $value['wftc'];
			if($value['wftc'] == 0) continue;
			$issue[$key]['dymc'] = $value['dymc'];
			$issue[$key]['ztc'] = (string)$value['ztc'];
			$issue[$key]['wftc'] = (string)$value['wftc'];
			$issue[$key]['tcwfl'] = round($value['wftc']/$value['ztc']*100,2).'％';
		}
		if($mediatype['total'] == '') $mediatype['total'] = 0;
		if($mediatype['illegal'] == '') $mediatype['illegal'] = 0;
		if($sort == 1){
			foreach ($issue as $key => $value) {
				$issue_arr[] = $value;
			}
			return array('issue' => $issue_arr,'mediatype' => $mediatype);
		}else{
			return array('issue' => $issue,'mediatype' => $mediatype);
		}	

	}

	public function region_sort($arrays,$sort_key,$sort_order=SORT_ASC,$sort_type=SORT_NUMERIC ){   
		if(is_array($arrays)){   
			foreach ($arrays as $array){   
				if(is_array($array)){   
					$key_arrays[] = $array[$sort_key];   
				}else{   
					return false;   
				}   
			}   
		}else{   
			return false;   
		}  
		
		array_multisort($key_arrays,$sort_order,$sort_type,$arrays); 

		$sum = 0;
		foreach($arrays as $array_arr) {
			$sum += 1;
			$array_array[] = array_merge(array('pm' => (string)$sum),$array_arr);			
		} 
		return $array_array;   
	}

	public function getRegionStr($array){

		foreach ($array as $key => $value) {
			$str .= $value['dymc'].'、';
			$region_count = $key + 1;
		}
		$str = mb_substr($str,0,-3);
		return array('str' => $str,'region_count' => $region_count);

	}

	public function count_adclass($region_id,$start_date,$end_date){

		if($region_id == 100000) $region_id = '000000';

		$startTime = date('Y-m-d H:i:s',strtotime($start_date));//开始时间
		$endTime = date('Y-m-d H:i:s',strtotime($end_date) + 86399);//结束时间

		$region_id_rtrim = rtrim($region_id,'00');//去掉地区后面的00
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位

		$where = array();
		// if($region_id == '340000'){
		// 	$where['tmedia.fid'] = array('not in',C('ANHUI_MEDIA'));
		// 	$where['tmediaowner.fregionid'] = $region_id;
		// }else{
		// 	$where['left(tmediaowner.fregionid,'.$tregion_len.')'] = $region_id_rtrim;
		// }
		$where['tmediaowner.fregionid'] = $region_id;
		if($startTime && $endTime){
			$where['issue.fissuedate'] = array(array('egt',$startTime),array('lt',$endTime),'and');//时间查询条件
		}

		$tv_issue = M('ttvissue')
							->alias('issue')
							->field('	
										adclass.fadclass,
										adclass.fcode,
										count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_illegality
									')
							->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
							->join('tad on tad.fadid = ttvsample.fadid')
							->join('tadclass on tadclass.fcode = tad.fadclasscode')
							->join('tadclass as adclass on adclass.fcode = left(tadclass.fcode,2)')
							->join('tmedia on tmedia.fid = issue.fmediaid')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->where($where)
							->group('adclass.fcode')
							->select();
        $bc_issue = M('tbcissue')
							->alias('issue')
							->field('	
										adclass.fadclass,
										adclass.fcode,
										count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_illegality
									')
							->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
							->join('tad on tad.fadid = tbcsample.fadid')
							->join('tadclass on tadclass.fcode = tad.fadclasscode')
							->join('tadclass as adclass on adclass.fcode = left(tadclass.fcode,2)')
							->join('tmedia on tmedia.fid = issue.fmediaid')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->where($where)
							->group('adclass.fcode')
							->select();
        $paper_issue = M('tpaperissue')
							->alias('issue')
							->field('	
										adclass.fadclass,
										adclass.fcode,
										count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_illegality
									')
							->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
							->join('tad on tad.fadid = tpapersample.fadid')
							->join('tadclass on tadclass.fcode = tad.fadclasscode')
							->join('tadclass as adclass on adclass.fcode = left(tadclass.fcode,2)')
							->join('tmedia on tmedia.fid = issue.fmediaid')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->where($where)
							->group('adclass.fcode')
							->select();
        $issue = array_merge($tv_issue,$bc_issue,$paper_issue);
        foreach ($issue as $key => $value) {
        	$issue_arr[$value['fcode']]['serious_illegality'] += $value['serious_illegality'];
        	$issue_arr[$value['fcode']]['name'] = $value['fadclass'];
        	$total += $value['serious_illegality'];
        }
        foreach ($issue_arr as $key => $value) {
        	$issue_arr[$key]['ratio'] = round($value['serious_illegality']/$total*100,2).'％';
        	$adclass[] = $issue_arr[$key];
        }
        $adclass = my_sort($adclass,'ratio',SORT_DESC);
        return $adclass;

	}

	public function getAdcase($regulator,$start_date,$end_date){

		$startTime = date('Y-m-d H:i:s',strtotime($start_date));//开始时间
		$endTime = date('Y-m-d H:i:s',strtotime($end_date) + 86399);//结束时间

		$where['tillegaladflow.fcreatetime'] = array('between',$startTime.','.$endTime);//时间查询条件
		$where['tillegaladflow.fcreateregualtorid'] = $regulator;
		$where['tillegaladflow.fflowname'] = '线索审核';
		$where['tillegalad.fillegaltypecode'] = 30;
		$adcaseList = M('tillegaladflow')
								->field('
										tregion.fname as dymc,
										count(1) as xspfl,
										count(case when tadcase.fstate = 3 then 1 else null end) as xsck,
										count(case when tadcase.fstate = 4 then 1 else null end) as xscl,
										count(case when tadcase.fstate = 4 and tadcase.fpentype = 10 then 1 else null end) as xstb,
										count(case when tadcase.fstate = 4 and tadcase.fpentype = 20 then 1 else null end) as xsla
										')
								->join('tillegalad on tillegalad.fillegaladid = tillegaladflow.fillegaladid')
								->join('tadcase on tadcase.fillegaladid = tillegalad.fillegaladid')
								->join('tregulator on tregulator.fid = tillegaladflow.fregulatorid')
								->join('tregion on tregion.fid = tregulator.fregionid')
								->where($where)
								->where('tillegaladflow.fcreateregualtorid != tillegaladflow.fregulatorid')
								->group('tillegaladflow.fregulatorid')
								->select();
		if(!$adcaseList){
			$data['adcase'] = $adcaseList;
			$data['adcase_sum'] = 0;//派发量
			$data['adcase_check_sum'] = 0;//检查量
			$data['adcase_check_ratio'] = '0％';//检查率
			$data['adcase_xstb'] = 0;//停播量
			$data['adcase_xstb_ratio'] = '0％';//停播率
			$data['adcase_xsla'] = 0;//立案调查量
			$data['adcase_xsla_ratio'] = '0％';//立案调查率
			return $data;
		}
		foreach ($adcaseList as $key => $value) {
			$adcase_arr[$key]['dymc']  = $value['dymc'];
			$adcase_arr[$key]['xspfl'] = (string)$value['xspfl'];
			$adcase_arr[$key]['xsck']  = (string)$value['xsck'];
			$adcase_arr[$key]['xsckl'] = round($value['xsck']/$value['xspfl']*100,2).'％';
			$adcase_arr[$key]['xscl']  = (string)$value['xscl'];
			$adcase_arr[$key]['xscll'] = round($value['xscl']/$value['xspfl']*100,2).'％';
			$adcase_sum += $value['xspfl'];
			$adcase_check_sum += $value['xsck'];
			$adcase_xstb += $value['xstb'];
			$adcase_xsla += $value['xsla'];
		}
		$adcase = $this->region_sort($adcase_arr,'xscll',SORT_ASC);

		$data['adcase'] = $adcase;
		$data['adcase_sum'] = $adcase_sum;//派发量
		$data['adcase_check_sum'] = $adcase_check_sum;//检查量
		$data['adcase_check_ratio'] = round($adcase_check_sum/$adcase_sum*100,2).'％';//检查率
		$data['adcase_xstb'] = $adcase_xstb;//停播量
		$data['adcase_xstb_ratio'] = round($adcase_xstb/$adcase_sum*100,2).'％';//停播率
		$data['adcase_xsla'] = $adcase_xsla;//立案调查量
		$data['adcase_xsla_ratio'] = round($adcase_xsla/$adcase_sum*100,2).'％';//立案调查率
		return $data;

	}

	public function issue_data($data){

		$tv_where = $bc_where = $paper_where = '';
		$tv_day   = $bc_day   = $paper_day   = '';
		$i = $j = $n = 0;
		$tv_sum = $bc_sum = $paper_sum = 0;//总发布量
		$tv_illegal_sum = $bc_illegal_sum = $paper_illegal_sum = 0;//总违法发布量
		$tv_length = $bc_length = 0;//总时长
		$tv_illegal_length = $bc_illegal_length = 0;//违法总时长
		$tv_ill = $bc_ill = $paper_ill = array();//违法发布记录
		$mediaList = M('tmedia')->where(['priority'=>array('EGT',0)])->getField('fid,fmedianame',true);
		$adclassList = M('tadclass')->where(['fpcode'=>''])->field('fcode,fadclass,ffullname')->select();
		$allAdclass = M('tadclass')->getField('fcode,ffullname',true);
		$ad_class = $five_class_illegal = array();
		foreach ($adclassList as $key => $value) {
			$ad_class[$value['fcode']]['code'] = $value['fcode'];
			$ad_class[$value['fcode']]['adclass'] = str_replace('类',' ',$value['fadclass']);
			$ad_class[$value['fcode']]['count'] = 0;
			$ad_class[$value['fcode']]['illegal_count'] = 0;
			$ad_class[$value['fcode']]['total'] = 0;
			$ad_class[$value['fcode']]['illegal_total'] = 0;
		}
 
		//where条件
		foreach ($data as $key => $value) {
			if(substr($value['fmediaclassid'],0,2) == '01' && $value['date']){
				$date = implode('","',$value['date']);
				foreach ($value['date'] as $k => $v) {
					$tv_day .= date('j号',strtotime($v)).',';
				}
				$tv_where .= '(tmedia.fid = '.$value['mediaid'].' and issue.fissuedate in("'.$date.'")) or ';
				$tv_media_group[$i]['fmedianame'] = $mediaList[$value['mediaid']];
				$tv_media_group[$i]['day'] = trim($tv_day,',');
				$tv_day = '';
				$i++;
			}
			if(substr($value['fmediaclassid'],0,2) == '02' && $value['date']){
				$date = implode('","',$value['date']);
				foreach ($value['date'] as $k => $v) {
					$bc_day .= date('j号',strtotime($v)).',';
				}
				$bc_where .= '(tmedia.fid = '.$value['mediaid'].' and issue.fissuedate in("'.$date.'")) or ';
				$bc_media_group[$j]['fmedianame'] = $mediaList[$value['mediaid']];
				$bc_media_group[$j]['day'] = trim($bc_day,',');
				$bc_day = '';
				$j++;
			}
			if(substr($value['fmediaclassid'],0,2) == '03' && $value['date']){
				$date = implode('","',$value['date']);
				foreach ($value['date'] as $k => $v) {
					$paper_day .= date('j号',strtotime($v)).',';
				}
				$paper_where .= '(tmedia.fid = '.$value['mediaid'].' and issue.fissuedate in("'.$date.'")) or ';
				$paper_media_group[$n]['fmedianame'] = $mediaList[$value['mediaid']];
				$paper_media_group[$n]['day'] = trim($paper_day,',');
				$paper_day = '';
				$n++;
			}

		}

		if($tv_where){
			$tv_where = substr($tv_where,0,-3);
			$tv_count = M('ttvissue')
							->alias('issue')
							->field('
									left(tad.fadclasscode,2) as fcode,
									count(1) as count,
									count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_ill_count,
									sum(issue.flength) as total,
									sum(case when ttvsample.fillegaltypecode = 30 then issue.flength else 0 end) as serious_ill_total
								')
							->join('tmedia on tmedia.fid = issue.fmediaid')
							->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
							->join('tad on tad.fadid = ttvsample.fadid')
							->where($tv_where)
							->group('left(fcode,2)')
							->select();
			foreach ($tv_count as $key => $value) {
				$tv_sum += $value['count'];
				$tv_illegal_sum += $value['serious_ill_count'];
				$tv_length += $value['total'];
				$tv_illegal_length += $value['serious_ill_total'];
				//分类统计			
				$ad_class[$value['fcode']]['count'] += $value['count'];
				$ad_class[$value['fcode']]['illegal_count'] += $value['serious_ill_count'];
				$ad_class[$value['fcode']]['total'] += $value['total'];
				$ad_class[$value['fcode']]['illegal_total'] += $value['serious_ill_total'];
			}
			$tv_where = '('.$tv_where.') and ttvsample.fillegaltypecode = 30';
			$tv = M('ttvissue')
					->alias('issue')
					->field('
							tmedia.fid as fmediaid,tmedia.fmedianame,
							issue.fissuedate,issue.flength,issue.fstarttime,
							tad.fadname,
							ttvsample.fexpressioncodes,ttvsample.fexpressions,ttvsample.fillegaltypecode,
							tad.fadclasscode as fcode
							')
					->join('tmedia on tmedia.fid = issue.fmediaid')
					->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
					->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
					->join('tad on tad.fadid = ttvsample.fadid')
					->where($tv_where)
					->select();
			$tv_ad_class = array();
			foreach ($tv as $key => $value) {				
				$value['ffullname'] = $allAdclass[$value['fcode']];
				$tv[$key]['ffullname'] = $value['ffullname'];
				$code = substr($value['fcode'],0,2);//截取分类code前2位			
				//替换fexpressioncodes
				if($value['fexpressioncodes']){
					$str = str_replace('20160', '0', $value['fexpressioncodes']);
					$value['fexpressioncodes'] = str_replace('20161','1',$str);
				}
				array_push($tv_ill, $value);
				//广告五大类
				if(in_array($code,C('five_class'))){
				 	$five_class_illegal[$value['fmediaid']]['fadname'] = $value['fadname'];
				 	$five_class_illegal[$value['fmediaid']]['ffullname'] = $value['ffullname'];
				 	$five_class_illegal[$value['fmediaid']]['fexpressioncodes'] = $value['fexpressioncodes'];
				 	$five_class_illegal[$value['fmediaid']]['count'] += 1;
				}
			}
		}else{
			$tv = array();
		}

		if($bc_where){
			$bc_where = substr($bc_where,0,-3);
			$bc_count = M('tbcissue')
							->alias('issue')
							->field('
									left(tad.fadclasscode,2) as fcode,
									count(1) as count,
									count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_ill_count,
									sum(issue.flength) as total,
									sum(case when tbcsample.fillegaltypecode = 30 then issue.flength else 0 end) as serious_ill_total
								')
							->join('tmedia on tmedia.fid = issue.fmediaid')
							->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
							->join('tad on tad.fadid = tbcsample.fadid')
							->where($bc_where)
							->group('left(fcode,2)')
							->select();
			//file_put_contents('LOG/report', M('tbcissue')->getLastSql());				
			foreach ($bc_count as $key => $value) {
				$bc_sum += $value['count'];
				$bc_illegal_sum += $value['serious_ill_count'];
				$bc_length += $value['total'];
				$bc_illegal_length += $value['serious_ill_total'];
				//分类统计			
				$ad_class[$value['fcode']]['count'] += $value['count'];
				$ad_class[$value['fcode']]['illegal_count'] += $value['serious_ill_count'];
				$ad_class[$value['fcode']]['total'] += $value['total'];
				$ad_class[$value['fcode']]['illegal_total'] += $value['serious_ill_total'];
			}
			$bc_where = '('.$bc_where.') and tbcsample.fillegaltypecode = 30';
			$bc = M('tbcissue')
					->alias('issue')
					->field('
							tmedia.fid as fmediaid,tmedia.fmedianame,
							issue.fissuedate,issue.flength,issue.fstarttime,
							tad.fadname,
							tbcsample.fexpressioncodes,tbcsample.fexpressions,tbcsample.fillegaltypecode,
							tad.fadclasscode as fcode
							')
					->join('tmedia on tmedia.fid = issue.fmediaid')
					->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
					->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
					->join('tad on tad.fadid = tbcsample.fadid')
					->where($bc_where)
					->select();
			foreach ($bc as $key => $value) {
				$value['ffullname'] = $allAdclass[$value['fcode']];
				$bc[$key]['ffullname'] = $value['ffullname'];
				$code = substr($value['fcode'],0,2);//截取分类code前2位
				//替换fexpressioncodes
				if($value['fexpressioncodes']){
					$str = str_replace('20160', '0', $value['fexpressioncodes']);
					$value['fexpressioncodes'] = str_replace('20161','1',$str);
				}
				array_push($bc_ill, $value);
				//广告五大类
				if(in_array($code,C('five_class'))){
				 	$five_class_illegal[$value['fmediaid']]['fadname'] = $value['fadname'];
				 	$five_class_illegal[$value['fmediaid']]['ffullname'] = $value['ffullname'];
				 	$five_class_illegal[$value['fmediaid']]['fexpressioncodes'] = $value['fexpressioncodes'];
				 	$five_class_illegal[$value['fmediaid']]['count'] += 1;
				}
			}
		}
		else{
			$bc = array();
		}
		
		if($paper_where){
			$paper_where = substr($paper_where,0,-3);
			$paper_count = M('tpaperissue')
							->alias('issue')
							->field('
									left(tad.fadclasscode,2) as fcode,
									count(1) as count,
									count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_ill_count
								')
							->join('tmedia on tmedia.fid = issue.fmediaid')
							->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
							->join('tad on tad.fadid = tpapersample.fadid')
							->where($paper_where)
							->group('left(fcode,2)')
							->select();
			foreach ($paper_count as $key => $value) {
				$paper_sum += $value['count'];
				$paper_illegal_sum += $value['serious_ill_count'];
				//分类统计			
				$ad_class[$value['fcode']]['count'] += $value['count'];
				$ad_class[$value['fcode']]['illegal_count'] += $value['serious_ill_count'];
			}
			$paper_where = '('.$paper_where.') and tpapersample.fillegaltypecode = 30';
			$paper = M('tpaperissue')
					->alias('issue')
					->field('
							tmedia.fid as fmediaid,tmedia.fmedianame,
							issue.fissuedate,
							tad.fadname,
							tpapersample.fexpressioncodes,tpapersample.fexpressions,tpapersample.fillegaltypecode,
							tad.fadclasscode as fcode
							')
					->join('tmedia on tmedia.fid = issue.fmediaid')
					->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
					->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
					->join('tad on tad.fadid = tpapersample.fadid')
					->where($paper_where)
					->select();
			foreach ($paper as $key => $value) {
				$value['ffullname'] = $allAdclass[$value['fcode']];
				$paper[$key]['ffullname'] = $value['ffullname'];
				$code = substr($value['fcode'],0,2);//截取分类code前2位
				//替换fexpressioncodes
				$str = str_replace('20160', '0', $value['fexpressioncodes']);
				$value['fexpressioncodes'] = str_replace('20161','1',$str);
				array_push($paper_ill, $value);
				//广告五大类
				if(in_array($code,C('five_class'))){
				 	$five_class_illegal[$value['fmediaid']]['fadname'] = $value['fadname'];
				 	$five_class_illegal[$value['fmediaid']]['ffullname'] = $value['ffullname'];
				 	$five_class_illegal[$value['fmediaid']]['fexpressioncodes'] = $value['fexpressioncodes'];
				 	$five_class_illegal[$value['fmediaid']]['count'] += 1;
				}
			}
		}else{
			$paper = array();
		}

		return array(
				'tv' 				=> $tv,					//电视发布记录
				'tv_ill'			=> $tv_ill,				//电视违法发布记录
				'tv_media_group' 	=> $tv_media_group,		//电视媒体频道
				'tv_sum' 			=> $tv_sum,				//电视监测发布总数
				'tv_illegal_sum' 	=> $tv_illegal_sum,		//电视违法发布总数
				'tv_length' 		=> $tv_length,			//电视总时长
				'tv_illegal_length' => $tv_illegal_length,	//电视违法总时长

				'bc' 				=> $bc,					//广播发布记录
				'bc_ill'			=> $bc_ill,				//电视违法发布记录
				'bc_media_group' 	=> $bc_media_group,		//广播媒体频道
				'bc_sum' 			=> $bc_sum,				//广播监测发布总数
				'bc_illegal_sum' 	=> $bc_illegal_sum,		//广播违法发布总数
				'bc_length'			=> $bc_length,			//广播总时长
				'bc_illegal_length' => $bc_illegal_length,	//广播违法总时长

				'paper' 			=> $paper,				//报纸发布记录
				'paper_ill'			=> $paper_ill,				//电视违法发布记录	
				'paper_media_group' => $paper_media_group,	//报纸媒体频道			
				'paper_sum' 		=> $paper_sum,			//报纸监测发布总数
				'paper_illegal_sum' => $paper_illegal_sum,	//报纸违法发布总数
				'ad_class' 			=> $ad_class,  			//广告类别统计
				'five_class_illegal'=> $five_class_illegal 	//违法广告五大类
				);

	}

	public function get_issue_data($data){

		$tv_where = $bc_where = $paper_where = '';
		$tv_day   = $bc_day   = $paper_day   = '';
		foreach ($data as $key => $value) {
			if(substr($value['fmediaclassid'],0,2) == '01' && $value['date']){
				$date = implode('","',$value['date']);
				foreach ($value['date'] as $k => $v) {
					$tv_day[$value['mediaid']]['day'] .= date('j号',strtotime($v)).',';
				}
				$tv_where .= '(tmedia.fid = '.$value['mediaid'].' and issue.fissuedate in("'.$date.'")) or ';
			}
			if(substr($value['fmediaclassid'],0,2) == '02' && $value['date']){
				$date = implode('","',$value['date']);
				foreach ($value['date'] as $k => $v) {
					$bc_day[$value['mediaid']]['day'] .= date('j号',strtotime($v)).',';
				}
				$bc_where .= '(tmedia.fid = '.$value['mediaid'].' and issue.fissuedate in("'.$date.'")) or ';
			}
			if(substr($value['fmediaclassid'],0,2) == '03' && $value['date']){
				$date = implode('","',$value['date']);
				foreach ($value['date'] as $k => $v) {
					$paper_day[$value['mediaid']]['day'] .= date('j号',strtotime($v)).',';
				}
				$paper_where .= '(tmedia.fid = '.$value['mediaid'].' and issue.fissuedate in("'.$date.'")) or ';
			}

		}

		if($tv_where){
			$tv_where = substr($tv_where,0,-3);
			$tv = M('ttvissue')
					->alias('issue')
					->field('
							tmedia.fid as fmediaid,tmedia.fmedianame,
							issue.fissuedate,								
							tad.fadname,
							ttvsample.fexpressioncodes,ttvsample.fexpressions,
							tadclass.ffullname,tadclass.fcode,
							count(1) as count,
							count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegal_count,
							count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_ill_count,
							sum(issue.flength) as total,
							sum(case when ttvsample.fillegaltypecode > 0 then issue.flength else 0 end) as illegal_total,
							sum(case when ttvsample.fillegaltypecode = 30 then issue.flength else 0 end) as serious_ill_total
							')
					->join('tmedia on tmedia.fid = issue.fmediaid')
					->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
					->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
					->join('tad on tad.fadid = ttvsample.fadid')
					->join('tadclass on tadclass.fcode = tad.fadclasscode')
					->where($tv_where)
					->group('issue.fmediaid,issue.ftvsampleid')
					->select();
			$tv_sum 			= 0;
			$tv_illegal_sum 	= 0;
			$tv_length 			= 0;
			$tv_illegal_length 	= 0;
			$tv_ad_class =$tv_five_class 		= array();
			foreach ($tv as $key => $value) {
				//替换fexpressioncodes
				if($value['fexpressioncodes']){
					$str = str_replace('20160', '0', $value['fexpressioncodes']);
					$tv[$key]['fexpressioncodes'] = str_replace('20161','1',$str);
				}

				$name 									= explode('>',$value['ffullname']);
				$code 									= substr($value['fcode'],0,2);

				$tv_media_group[$value['fmediaid']]['fmedianame'] 	= $value['fmedianame'];
				$tv_media_group[$value['fmediaid']]['day']			= $tv_day[$value['fmediaid']]['day'];

				$tv[$key]['fname'] 						= $name[0];
				$tv_sum 								+= $value['count'];
				$tv_illegal_sum 						+= $value['serious_ill_count'];
				$tv_length 								+= $value['total'];
				$tv_illegal_length 						+= $value['serious_ill_total'];

				//广告类别分组
				$tv_ad_class[$code]['fcode'] 			= $code;
				$tv_ad_class[$code]['fname'] 			= $name[0];
				$tv_ad_class[$code]['count'] 			+= $value['count'];
				$tv_ad_class[$code]['illegal_count'] 	+= $value['serious_ill_count'];
				$tv_ad_class[$code]['total'] 			+= $value['total'];
				$tv_ad_class[$code]['illegal_total'] 	+= $value['serious_ill_total'];

				//广告五大类
				if(in_array($code,C('five_class'))){
					array_push($tv_five_class,$value); 
				}
			}
			$tv_ad_class = array_values($tv_ad_class);//数组索引值重新从0开始
		}else{
			$tv_ad_class = $tv_five_class = array();
		}

		if($bc_where){
			$bc_where = substr($bc_where,0,-3);
			$bc = M('tbcissue')
					->alias('issue')
					->field('
							tmedia.fid as fmediaid,tmedia.fmedianame,
							issue.fissuedate,									
							tad.fadname,
							tbcsample.fexpressioncodes,tbcsample.fexpressions,
							tadclass.ffullname,tadclass.fcode,
							count(1) as count,
							count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegal_count,
							count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_ill_count,
							sum(issue.flength) as total,
							sum(case when tbcsample.fillegaltypecode > 0 then issue.flength else 0 end) as illegal_total,
							sum(case when tbcsample.fillegaltypecode = 30 then issue.flength else 0 end) as serious_ill_total
							')
					->join('tmedia on tmedia.fid = issue.fmediaid')
					->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
					->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
					->join('tad on tad.fadid = tbcsample.fadid')
					->join('tadclass on tadclass.fcode = tad.fadclasscode')
					->where($bc_where)
					->group('issue.fmediaid,issue.fbcsampleid')
					->select();
			$bc_sum 			= 0;
			$bc_illegal_sum 	= 0;
			$bc_length 			= 0;
			$bc_illegal_length 	= 0;
			$bc_ad_class = $bc_five_class 		= array();
			foreach ($bc as $key => $value) {
				//替换fexpressioncodes
				if($value['fexpressioncodes']){
					$str = str_replace('20160', '0', $value['fexpressioncodes']);
					$bc[$key]['fexpressioncodes'] = str_replace('20161','1',$str);
				}

				$name 									= explode('>',$value['ffullname']);
				$code 									= substr($value['fcode'],0,2);
				
				$bc_media_group[$value['fmediaid']]['fmedianame'] 	= $value['fmedianame'];
				$bc_media_group[$value['fmediaid']]['day']			= $bc_day[$value['fmediaid']]['day'];

				$bc[$key]['fname']						= $name[0];
				$bc_sum 								+= $value['count'];
				$bc_illegal_sum 						+= $value['serious_ill_count'];
				$bc_length 								+= $value['total'];
				$bc_illegal_length 						+= $value['serious_ill_total'];

				//广告类别分组
				$bc_ad_class[$code]['fcode'] 			= $code;
				$bc_ad_class[$code]['fname'] 			= $name[0];
				$bc_ad_class[$code]['count'] 			+= $value['count'];
				$bc_ad_class[$code]['illegal_count'] 	+= $value['serious_ill_count'];
				$bc_ad_class[$code]['total'] 			+= $value['total'];
				$bc_ad_class[$code]['illegal_total'] 	+= $value['serious_ill_total'];

				//广告五大类
				if(in_array($code,C('five_class'))){
					array_push($bc_five_class,$value); 
				}
			}
			$bc_ad_class = array_values($bc_ad_class);//数组索引值重新从0开始
		}else{
			$bc_ad_class = $bc_five_class = array();
		}

		if($paper_where){
			$paper_where = substr($paper_where,0,-3);
			$paper = M('tpaperissue')
					->alias('issue')
					->field('
							tmedia.fid as fmediaid,tmedia.fmedianame,
							issue.fissuedate,									
							tad.fadname,
							tpapersample.fexpressioncodes,tpapersample.fexpressions,
							tadclass.ffullname,tadclass.fcode,
							count(1) as count,
							count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegal_count,
							count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_ill_count
							')
					->join('tmedia on tmedia.fid = issue.fmediaid')
					->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
					->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
					->join('tad on tad.fadid = tpapersample.fadid')
					->join('tadclass on tadclass.fcode = tad.fadclasscode')
					->where($paper_where)
					->group('issue.fmediaid,issue.fpapersampleid')
					->select();
			$paper_sum 			= 0;
			$paper_illegal_sum 	= 0;
			$paper_ad_class = $paper_five_class 	= array();
			foreach ($paper as $key => $value) {
				//替换fexpressioncodes
				if($value['fexpressioncodes']){
					$str = str_replace('20160', '0', $value['fexpressioncodes']);
					$paper[$key]['fexpressioncodes'] = str_replace('20161','1',$str);
				}

				$name 									= explode('>',$value['ffullname']);
				$code 									= substr($value['fcode'],0,2);
				
				$paper_media_group[$value['fmediaid']]['fmedianame'] 	= $value['fmedianame'];
				$paper_media_group[$value['fmediaid']]['day']			= $paper_day[$value['fmediaid']]['day'];

				$paper[$key]['fname']					= $name[0];
				$paper_sum 								+= $value['count'];
				$paper_illegal_sum 						+= $value['serious_ill_count'];

				//广告类别分组
				$paper_ad_class[$code]['fcode'] 		= $code;
				$paper_ad_class[$code]['fname'] 		= $name[0];
				$paper_ad_class[$code]['count'] 		+= $value['count'];
				$paper_ad_class[$code]['illegal_count'] += $value['serious_ill_count'];
				$paper_ad_class[$code]['total'] 		= 0;
				$paper_ad_class[$code]['illegal_total'] = 0;

				//广告五大类
				if(in_array($code,C('five_class'))){
					array_push($paper_five_class,$value); 
				}
			}
			$paper_ad_class = array_values($paper_ad_class);//数组索引值重新从0开始
		}else{
			$paper_ad_class = $paper_five_class = array();
		}

		//合计数据 
		$ad_class = array_merge($tv_ad_class,$bc_ad_class,$paper_ad_class);//合并广告类别
		//相同广告类别相加
		foreach ($ad_class as $key => $value) {
			$adClass[$value['fcode']]['fname'] 			= $value['fname'];
			$adClass[$value['fcode']]['count'] 			+= $value['count'];
			$adClass[$value['fcode']]['illegal_count'] 	+= $value['illegal_count'];
			$adClass[$value['fcode']]['total'] 			+= $value['total'];
			$adClass[$value['fcode']]['illegal_total'] 	+= $value['illegal_total'];
		}

		$five_class = array_merge($tv_five_class,$bc_five_class,$paper_five_class);//合并广告五大类
		//分离出严重违法和违法的数据
		$serious_illegal = array();
		$illegal = array();
		foreach ($five_class as $key => $value) {
			if($value['serious_ill_count'] > 0){
				array_push($serious_illegal,$value);
			}elseif($value['illegal_count'] > 0){
				array_push($illegal,$value);
			}else{
				continue;
			}
		}
		if($serious_illegal) $serious_illegal = my_sort($serious_illegal,'serious_ill_count',SORT_DESC);
		if($illegal) $illegal = my_sort($illegal,'illegal_count',SORT_DESC);
		// $five_class_illegal = array_merge($serious_illegal,$illegal);//合并严重违法广告和一般违法广告
		$five_class_illegal = $serious_illegal;

		return array(
				'tv' 				=> $tv,					//电视发布记录（媒体，广告名分组）
				'tv_media_group' 	=> $tv_media_group,		//电视媒体频道
				'tv_sum' 			=> $tv_sum,				//电视监测发布总数
				'tv_illegal_sum' 	=> $tv_illegal_sum,		//电视违法发布总数
				'tv_length' 		=> $tv_length,			//电视总时长
				'tv_illegal_length' => $tv_illegal_length,	//电视违法总时长
				'bc' 				=> $bc,					//广播发布记录（媒体，广告名分组）
				'bc_media_group' 	=> $bc_media_group,		//广播媒体频道
				'bc_sum' 			=> $bc_sum,				//广播监测发布总数
				'bc_illegal_sum' 	=> $bc_illegal_sum,		//广播违法发布总数
				'bc_length'			=> $bc_length,			//广播总时长
				'bc_illegal_length' => $bc_illegal_length,	//广播违法总时长
				'paper' 			=> $paper,				//报纸发布记录（媒体，广告名分组）							
				'paper_media_group' => $paper_media_group,	//报纸媒体频道			
				'paper_sum' 		=> $paper_sum,			//报纸监测发布总数
				'paper_illegal_sum' => $paper_illegal_sum,	//报纸违法发布总数
				'ad_class' 			=> $adClass,  			//广告类别统计
				'five_class_illegal'=> $five_class_illegal 	//违法广告五大类
				);

	}
	
}
