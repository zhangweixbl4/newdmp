Vue.use(VueViewer.default)
Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}
var vm = new Vue({
  el: '#app',
  data: function () {
    return {
      mediaClassArr: [{
          text: '电视',
          value: 1
        },
        {
          text: '广播',
          value: 2
        },
        {
          text: '报刊',
          value: 3
        },
        {
          text: '互联网',
          value: 13
        },
      ],
      netPlatform: [{
          text: 'PC',
          value: 1
        },
        {
          text: '移动端',
          value: 2
        },
        {
          text: '微信端',
          value: 9
        },
      ],
      // TODO:状态需要整合精简
      taskStateArr: [{
          text: '任务中',
          value: 1
        },
        {
          text: '录入任务中',
          value: 11
        },
        {
          text: '审核任务中',
          value: 21
        },
        {
          text: '已完成',
          value: 2
        },
        {
          text: '录入已完成',
          value: 12
        },
        {
          text: '审核已完成',
          value: 22
        },
        {
          text: '被退回',
          value: 4
        },
        {
          text: '录入被退回',
          value: 13
        },
        {
          text: '审核被退回',
          value: 23
        },
        {
          text: '等待撤回审核',
          value: 5
        },
      ],
      qualityStateArr: [{
          text: '未质检',
          value: 0
        },
        {
          text: '正在质检',
          value: 1
        },
        {
          text: '已质检',
          value: 2
        },
      ],
      adClassArr: adClassArr,
      adClassTree: adClassTree,
      illegalTypeArr: illegalTypeArr,
      regionTree: regionTree,
      loading: false,
      where: {},
      page: 1,
      pageSize: 20,
      tableData: [],
      total: 0,
      taskInfo: {},
      showTaskInfo: false,
      role: role,
      searchTempWhere: {},
      type: type,
      isNetAd: '1',
      tableHeight: '400px'
    }
  },
  components: {
    taskInfo,
    taskInfo2,
    netAdTask,
    netAdTask2,
    outdoorAdCheck,
  },
  created: function () {
    this.getLastDate()
    this.sendSearchReq()
  },
  computed: {
    searchUrl: function () {
      if (this.role == 'user') {
        return 'myTaskList'
      }
      if (this.role == 'inspector') {
        return 'inspectorTaskList'
      }
    }
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight()
    })
    window.onresize = () => {
      this.getTableHeight()
    }
  },
  methods: {
    getTableHeight(){
      let formHeight = this.$refs.whereForm.$el.clientHeight
      this.tableHeight = document.body.clientHeight - formHeight - 50 + 'px'
    },
    getLastDate: function () {
      const arr = []
      let startTime = moment().format('YYYY-MM-') + '01'
      let endTime = moment().format('YYYY-MM-') + moment().daysInMonth()
      arr.push(moment(startTime).valueOf())
      arr.push(moment(endTime).valueOf())
      Vue.set(this.where, 'timeRangeArr', arr)
    },
    search: function () {
      this.page = 1
      this.where.onlyThisLevel = this.searchTempWhere.onlyThisLevel ? 1 : 0
      if (this.searchTempWhere.searchAdClassArr1 && this.searchTempWhere.searchAdClassArr1.length !== 0) {
        this.where.adClass = this.searchTempWhere.searchAdClassArr1[this.searchTempWhere.searchAdClassArr1.length - 1]
      }
      if (this.searchTempWhere.searchAdClassArr2 && this.searchTempWhere.searchAdClassArr2.length !== 0) {
        this.where.adClass2 = this.searchTempWhere.searchAdClassArr2[this.searchTempWhere.searchAdClassArr2.length - 1]
      }
      if (this.searchTempWhere.region && this.searchTempWhere.region.length !== 0) {
        this.where.regionId = this.searchTempWhere.region[this.searchTempWhere.region.length - 1]
      }

      this.sendSearchReq()
    },
    sendSearchReq: function () {
      if (this.loading) {
        return false
      }
      this.loading = true
      var vm = this
      $.post(this.searchUrl, {
        where: this.where,
        page: this.page,
        pageSize: this.pageSize,
        isNetAd: this.isNetAd
      }, function (res) {
        if (res.code == -2 && res.msg == 'need log in again') {
          vm.$message.error('登录状态过期,请再次登录')
          parent.location.assign('/Adinput/VideoCutting/login')
        }
        vm.tableData = res.data
        vm.total = +res.total
        vm.loading = false
      })
    },
    resetForm: function (formName) {
      this.page = 1
      this.where = {}
      this.searchTempWhere = {}
      this.getLastDate()
      this.sendSearchReq()
    },
    pageChange: function (page) {
      this.page = page
      this.sendSearchReq()
    },
    openTaskInfo: function (row) {
      this.loading = true
      $.post('index', {
        taskid: row.taskid,
        taskType: this.isNetAd
      }).then((res) => {
        this.loading = false
        if (res.code == 0) {
          this.taskInfo = res.data
          this.showTaskInfo = true
          this.$nextTick(() => {
            this.$refs.task.dataInit()
          })
        }
      })
    },
    tableRowClassName: function (row) {
      if (row.row.task_state == 4 || row.row.task_state.substr(-1, 1) == 3) {
        return 'errorTask'
      }
      if (row.row.fillegaltypecode != 0) {
        return 'illegalTask'
      }
    },
    closeTaskInfo: function () {
      this.showTaskInfo = false
      this.taskInfo = {}
    },
    searchAlias: function (queryString, cb) {
      $.post("/Api/User/searchUserByAlias", {
        keyword: queryString
      }, function (res) {
        cb(res.data);
      });
    },
    baseFilter: function (value, arr) {
      var res = ''
      this[arr].forEach(function (item) {
        if (item.value == value) {
          res = item.text
        }
      })
      return res
    }
  },
  filters: {
    mediaClassFilter: function (value) {
      try {
        return vm.baseFilter(value, 'mediaClassArr')
      } catch (e) {

      }
    },
    illegalTypeFilter: function (value) {
      try {
        return vm.baseFilter(value, 'illegalTypeArr')
      } catch (e) {

      }
    },
    taskStateFilter: function (value) {
      try {
        return vm.baseFilter(value, 'taskStateArr')
      } catch (e) {

      }
    },
    qualityStateFilter: function (value) {
      try {
        return vm.baseFilter(value, 'qualityStateArr')
      } catch (e) {

      }
    },
    netPlatformFilter: function (value) {
      try {
        return vm.baseFilter(value, 'netPlatform')
      } catch (e) {

      }
    },
  }
})