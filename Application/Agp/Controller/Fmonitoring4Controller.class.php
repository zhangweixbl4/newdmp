<?php
namespace Agp\Controller;
use Think\Controller;
use Think\Exception;

import('Vendor.PHPExcel');

/**
 * 监测数据
 */

class Fmonitoring4Controller extends BaseController
{
	/**
	 * 获取监测数据
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function index()
	{
		session_write_close();
		header("Content-type:text/html;charset=utf-8");
		ini_set('memory_limit','2048M');
		ini_set('max_execution_time', 0);//设置超时时间
        $system_num = getconfig('system_num');
		$outtype = I('outtype');//导出类型

		$p = I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录
		if(empty($outtype)){
			$limitstr = ($p-1)*$pp.','.$pp;
		}else{
			$limitstr = '0,999999';
		}

		$fadname 			= I('fadname');// 广告名称
		$fadclasscode 		= I('fadclasscode');// 广告内容类别组
		$area 				= I('area');// 行政区划
		$mclass 			= I('mclass')?I('mclass'):'';//媒体类型
		$fillegaltypecode 	= I('fillegaltypecode');// 违法类型
		$fissuedatest 		= I('fissuedatest');//发布时间起
		$fissuedateed 		= I('fissuedateed');//发布时间止
		$iscontain 			= I('iscontain');//是否包含下属地区
		$netadtype 			= I('netadtype');//平台类型
		$fadowner 			= I('fadowner');//广告主
		$media 				= I('media');//媒体名称
		$fislook 			= I('fislook');//是否已读，0全部，1未读，2已读
		$isout 				= I('isout');//是否监测外，0否，1是
		$isgdowner 			= I('isgdowner');//公司分类：1特定，2非特定
		$fhandlestate 		= I('fhandlestate');//违规类型，待处理、不违规、违规、外包装近似、商标侵权、外包装近似+商标侵权
		$issample 			= I('issample');//0非样本，1仅显示样本

		$whereStr = '1=1';
		if(!empty($isout)){
			$whereStr .= ' and h.ftype = 10';
		}else{
			if(empty($isgdowner)){
				$whereStr .= ' and (h.ftype is null or h.ftype = 1) ';
			}elseif($isgdowner == 1){
				$whereStr .= ' and h.ftype = 1';
			}else{
				$whereStr .= ' and h.ftype is null ';
			}
		}
		//是否已读
		if(!empty($fislook)){
			if($fislook == 1){
				$whereStr .= ' and (a.fislook = 1 and a.fhandlestate = "待确认")';//待处理
			}else{
				$whereStr .= ' and !(a.fislook = 1 and a.fhandlestate = "待确认")';//已处理
			}
		}
		//违规类型
		if(!empty($fhandlestate)){
			if(is_array($fhandlestate)){
				$whereStr .= ' and a.fhandlestate in("'.implode('","', $fhandlestate).'")';
			}else{
				$whereStr .= ' and a.fhandlestate = "'.$fhandlestate.'"';
			}
		}
		//广告主
		if(!empty($fadowner)){
			$fadowner = str_replace(' ','',str_replace(',','，',$fadowner));
			$fadowners = explode('，', $fadowner);
			if(count($fadowners)>1){
				$whereStr .= ' and d.fname in ("'.implode('","', $fadowners).'")';
			}else{
				$whereStr .= ' and d.fname like "%'.$fadowner.'%"';
			}
		}
		//媒体
		if(!empty($media)){
			$whereStr .= ' and b.fmedianame like "%'.$media.'%"';
		}
		//平台类型，用于互联网
		if(!empty($netadtype)){
			$whereStr .= ' and a.issue_platform_type = '.$netadtype;
		}
		//发布时间
		if(!empty($fissuedatest) && !empty($fissuedateed)){
			$whereStr .= ' and a.fissuedate between "'.$fissuedatest.'" and "'.$fissuedateed.'"';
		}
		//违法类型
		if(strlen($fillegaltypecode)!=0){
			$whereStr .= ' and a.fillegaltypecode = '.$fillegaltypecode;
		}
		//广告名称
		if(!empty($fadname)){
			$whereStr .= ' and c.fadname like "%'.$fadname.'%"';
		}
		//广告类别
		if(!empty($fadclasscode)){
			$arr_code = ['08','0801'];
			if($fadclasscode == 1){
				$whereStr .= ' and a.fadclasscode in('.implode(',', $arr_code).')';
			}else{
				$whereStr .= ' and a.fadclasscode not in('.implode(',', $arr_code).')';
			}
		}
		//所属地区
		if(!empty($area)){
			if(!empty($iscontain)){
				$tregion_len = get_tregionlevel($area);
				if($tregion_len == 1){//国家级
					$whereStr .= ' and b.media_region_id = '.$area;
				}elseif($tregion_len == 2){//省级
					$whereStr .= ' and b.media_region_id like "'.substr($area,0,2).'%"';
				}elseif($tregion_len == 4){//市级
					$whereStr .= ' and b.media_region_id like "'.substr($area,0,4).'%"';
				}elseif($tregion_len == 6){//县级
					$whereStr .= ' and b.media_region_id = '.$area;
				}
			}else{
				$whereStr .= ' and b.media_region_id = '.$area;
			}
		}

		//是否只显示样本
		if(!empty($issample)){
			$groupStr = 'group by g.fid';
			$groupWhere = ' and a.fhandlestate = g.fhandlestate';
		}
		
		//媒体类别
		if(!empty($mclass)){
			$whereStr .= ' and a.fmediaclassid = '.(int)$mclass;
		}
		$whereStr .= ' and a.fcustomer = "'.$system_num.'"';

        $countstr = '
			select count(*) mcount from (
				select a.fid
				from tissue_customer a
				inner join tmedia b on a.fmediaid = b.fid and b.fstate = 1
				inner join tad c on a.fadid = c.fadid and c.fstate>0
				inner join tadowner d on a.fadownerid = d.fid
				inner join tregion e on b.media_region_id = e.fid
				inner join tadclass f on a.fadclasscode = f.fcode
				left join tadownerclass h on d.fid = h.fadownerid and h.fcustomer = "'.$system_num.'"
				inner join tsample_wine g on g.fmediaclass = CONVERT(a.fmediaclassid,SIGNED) and a.fuuid = g.fuuid '.$groupWhere.'
				where '.$whereStr.$groupStr.'
			) m 
		';
		$count = M()->query($countstr)[0]['mcount'];

		$sqlstr = '
			select * from (
				select a.fid sid,b.fmedianame,a.fmediaclassid mclass,d.fname fadowner,e.ffullname regionname,(case when a.fmediaclassid = 1 or a.fmediaclassid = 2 then a.fstarttime else a.fissuedate end) fissuedate,a.fendtime,a.fspecifications,c.fadname,f.ffullname fadclass,b.affect_region,b.media_region_id,a.sam_source_path,a.operator_name,a.fillegaltypecode,a.fexpressioncodes,a.fexpressions,a.fillegalcontent,e.fid fregionid,g.flaunchent,ifnull(h.ftype,0) adownertype,ifnull(a.fhandlestate,"") fhandlestate,(case when a.fislook = 1 and a.fhandlestate = "待确认" then 1 else 2 end) fislook,a.fuuid
				from tissue_customer a
				inner join tmedia b on a.fmediaid = b.fid and b.fstate = 1
				inner join tad c on a.fadid = c.fadid and c.fstate>0
				inner join tadowner d on a.fadownerid = d.fid
				inner join tregion e on b.media_region_id = e.fid
				inner join tadclass f on a.fadclasscode = f.fcode
				left join tadownerclass h on d.fid = h.fadownerid and h.fcustomer = "'.$system_num.'"
				inner join tsample_wine g on g.fmediaclass = CONVERT(a.fmediaclassid,SIGNED) and a.fuuid = g.fuuid '.$groupWhere.'
				where '.$whereStr.$groupStr.'
			) m 
			order by fissuedate desc
			limit '.$limitstr.'
		';
		$data = M()->query($sqlstr);

		foreach ($data as $key => $value) {
			$data[$key]['regulatorname'] = M('tregulator')->cache(true,600)->where(['fregionid'=>$value['media_region_id'],'fkind'=>1])->getfield('fname');
			if($value['mclass'] == 1 || $value['mclass'] == 2){
				$data[$key]['flength'] = $value['fspecifications'];
				$data[$key]['fstarttime'] = date('H:i:s',strtotime($value['fissuedate']));
				$data[$key]['fendtime'] = date('H:i:s',strtotime($value['fendtime']));
			}elseif($value['mclass'] == 3){
				$data[$key]['fpage'] = $value['fspecifications'];
			}
			if(empty($value['fillegaltypecode'])){
				$data[$key]['fillegaltype'] = '不违法';
			}else{
				$data[$key]['fillegaltype'] = '违法';
			}
			if(empty($value['fadowner']) || $value['fadowner'] == '广告主不详'){
				$data[$key]['fadowner'] = '公司不详';
			}
		}
		
		if(!empty($outtype)){
			if(empty($data)){
				$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
			}
			if($outtype=='xls'){
		        $outdata['title'] = '茅台集团广告监测内容信息（'.I('fissuedatest').'至'.I('fissuedatest').'）';//文档内部标题名称
		        $outdata['datalie'] = [
		          '序号'=>'key',
		          '发布媒体'=>'fmedianame',
		          '媒体类型'=>[
		            'type'=>'zwif',
		            'data'=>[
		              ['{mclass} == 1','电视'],
		              ['{mclass} == 2','广播'],
		              ['{mclass} == 3','报纸'],
		              ['{mclass} == 13','互联网'],
		            ]
		          ],
		          // '广告经营者'=>'',
		          '广告主'=>'flaunchent',
		          '发布地'=>'regionname',
		          '播出时间'=>'fissuedate',
		          '广告时长/版面'=>[
		            'type'=>'zwif',
		            'data'=>[
		              ['{mclass} == 1 || {mclass} == 2 || {mclass} == 3','{fspecifications}'],
		              ['',''],
		            ]
		          ],
		          '违规类型'=>'fhandlestate',
		          '宣传产品名称'=>'fadname',
		          '广告内容类别'=>'fadclass',
		          '产、销公司'=>'fadowner',
		          '广告覆盖地域'=>'affect_region',
		          '管辖行政机关'=>'regulatorname',
		          // '违法类型'=>[
		          //   'type'=>'zwif',
		          //   'data'=>[
		          //     ['{fillegaltypecode} == 30','违法'],
		          //     ['','不违法'],
		          //   ]
		          // ],
		          // '违法代码'=>'fexpressioncodes',
		          // '涉嫌违法内容'=>'fillegalcontent',
		          '素材地址'=>'sam_source_path',
		          '备注'=>'',
		        ];

		        $outdata['lists'] = $data;
		        $ret = A('Api/Function')->outdata_xls($outdata);

		        D('Function')->write_log('监测数据',1,'excel导出成功');
		        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
			}elseif($outtype=='xml'){
				$this->downloadXml($data);
			}elseif($outtype=='txt'){
				$this->downloadTxt($data);
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'请选择导出类型'));
			}
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
		}
	}

	/**
	 * 样本数据处理
	 * by zw
	 */
	public function checkSampleData(){
		$gid = I('gid');//样本记录id
		$sid = I('sid');//发布记录id
		$fhandlestate = I('fhandlestate');//违规类型，待处理、不违规、违规、外包装近似、商标侵权、外包装近似+商标侵权
		if(empty($gid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'本条广告信息缺失，暂不提供处理'));
		}
		if(empty($fhandlestate)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'请选择违规类型'));
		}

		$viewICR = M('tissue_customer') ->where(['fid'=>$sid]) ->find();//发布记录

		$viewSWE = M('tsample_wine')->where(['fid'=>$gid])->find();//样本记录

		if($viewSWE['fhandlestate'] != $viewICR['fhandlestate']){
			$this->ajaxReturn(array('code'=>1,'msg'=>'该样本与样本违规判定有异，仅能作纠正操作，现样本判定结果为"'.$viewSWE['fhandlestate'].'"'));
		}

		$doICR = M('tsample_wine')->where(['fid'=>$gid])->save(['fhandlestate'=>$fhandlestate,'fmodifier'=>session('regulatorpersonInfo.fname'),'fmodifytime'=>date('Y-m-d H:i:s')]);
		if(!empty($doICR)){
			$do_dd = M()->execute('update tissue_customer set fhandlestate = "'.$fhandlestate.'",fcontent = concat(fcontent,"'.session('regulatorpersonInfo.fname').'于'.date('Y-m-d H:i:s').'将违规类型从'.$viewSWE['fhandlestate'].'处理成'.$fhandlestate.'；") where fuuid = "'.$viewSWE['fuuid'].'" and fhandlestate = "'.$viewSWE['fhandlestate'].'"');//修改茅台发布明细的处理状态
			D('Function')->write_log('茅台明细数据处理',1,'处理成功','tsample_wine',$gid,M('tsample_wine')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'处理成功'));
		}else{
			D('Function')->write_log('茅台明细数据处理',0,'处理失败','tsample_wine',$gid,M('tsample_wine')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'处理失败，请刷新后重试'));
		}
	}

	/**
	 * 广告明细数据处理
	 * by zw
	 */
	public function checkIssueData(){
		$sid = I('sid');//发布记录id
		$fhandlestate = I('fhandlestate');//违规类型，待处理、不违规、违规、外包装近似、商标侵权、外包装近似+商标侵权
		if(empty($sid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
		}
		if(empty($fhandlestate)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'请选择违规类型'));
		}

		$viewICR = M('tissue_customer') ->where(['fid'=>$sid]) ->find();//发布记录

		$saveSql = 'update tissue_customer set fhandlestate = "'.$fhandlestate.'",fcontent = concat(fcontent,"'.session('regulatorpersonInfo.fname').'于'.date('Y-m-d H:i:s').'将违规类型从'.$viewICR['fhandlestate'].'纠正成'.$fhandlestate.'；") where fid = '.$sid;
		$saveICR = M()->execute($saveSql);

		if(!empty($saveICR)){
			D('Function')->write_log('茅台明细数据处理',1,'处理成功','tsample_wine',$sid,$saveSql);
			$this->ajaxReturn(array('code'=>0,'msg'=>'处理成功'));
		}else{
			D('Function')->write_log('茅台明细数据处理',0,'处理失败','tsample_wine',$sid,$saveSql);
			$this->ajaxReturn(array('code'=>1,'msg'=>'处理失败，请刷新后重试'));
		}
	}

	/**
	 * 更新产销公司、产品名称数据
	 * by zw
	 */
	public function updateSampleData(){
		$sid = I('sid');

		if(empty($sid)){
			$this->error('参数缺失!');
		}

		$doICR = M('tissue_customer')->where(['fid'=>$sid])->find();

		if(!empty($doICR)){
			if($doICR['fmediaclassid'] == 1){
				$tbStr = 'tv';
			}elseif($doICR['fmediaclassid'] == 2){
				$tbStr = 'bc';
			}elseif($doICR['fmediaclassid'] == 3){
				$tbStr = 'paper';
			}

			$doSample = M('t'.$tbStr.'sample a')
				->field('a.fadid,b.fadowner,b.fadname,c.fname')
				->join('tad b on a.fadid = b.fadid and b.fstate > 0')
				->join('tadowner c on b.fadowner = c.fid')
				->where(['uuid'=>$doICR['fuuid'],'a.fstate'=>['in',[0,1]]])
				->find();
			if(!empty($doSample)){
				$dataICR['fadid'] = $doSample['fadid'];
				$dataICR['fadownerid'] = $doSample['fadowner'];
				$saveICR = M('tissue_customer')->where(['fid'=>$sid])->save($dataICR);

				$returnData['fadowner'] = $doSample['fname'];
				$returnData['fadname'] = $doSample['fadname'];
			}
		}

		if(!empty($saveICR)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'更新成功','data'=>$returnData));
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'无数据更新','data'=>$returnData));
		}
		
	}

	/**
	 * 获取监测数据详情
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据
	 * by zw
	 */
	public function getmonitoringview()
	{
		$system_num = getconfig('system_num');
		$sid = I('sid');//广告记录id
		if(empty($sid)){
			$this->error('参数缺失!');
		}

		$where_issue['a.fid'] = $sid;
		$issueInfo = M('tissue_customer')
			->alias('a')
			->field('a.fid sid,ifnull(g.fid,0) gid,b.fmedianame,a.fmediaclassid,d.fname fadowner,e.ffullname regionname,(case when a.fmediaclassid = 1 or a.fmediaclassid = 2 then a.fstarttime else a.fissuedate end) fissuedate,a.fstarttime,a.fendtime,a.fspecifications,c.fadname,f.ffullname fadclass,b.affect_region,a.sam_source_path,a.operator_name,a.fmediaid,a.fillegaltypecode,a.fexpressioncodes,a.fexpressions,a.fillegalcontent,e.fid fregionid,a.fsampleid,c.fbrand,ifnull(h.ftype,0) adownertype,ifnull(a.fhandlestate,"") fhandlestate,flaunchent,ftaste,fdegrees,fyear,fkeyword,fsnapshoot')
			->join('tmedia b on a.fmediaid = b.fid and b.fstate = 1')
			->join('tad c on a.fadid = c.fadid and c.fstate>0')
			->join('tadowner d on a.fadownerid = d.fid')
			->join('tregion e on b.media_region_id = e.fid')
			->join('tadclass f on a.fadclasscode = f.fcode')
			->join('tadownerclass h on d.fid = h.fadownerid and h.fcustomer = "'.$system_num.'"','left')
			->join('tsample_wine g on g.fmediaclass = CONVERT(a.fmediaclassid,SIGNED) and a.fuuid = g.fuuid')
			->where($where_issue)
			->find();

		if($issueInfo['fmediaclassid'] == 1 || $issueInfo['fmediaclassid'] == 2){
			$issueInfo['flength'] = $issueInfo['fspecifications'];
		}elseif($value['fmediaclassid'] == 3){
			$issueInfo['fpage'] = $issueInfo['fspecifications'];
		}
		if(empty($issueInfo['fillegaltypecode'])){
			$issueInfo['fillegaltype'] = '不违法';
		}else{
			$issueInfo['fillegaltype'] = '违法';
		}
		if(empty($issueInfo['fadowner']) || $issueInfo['fadowner'] == '广告主不详'){
			$issueInfo['fadowner'] = '公司不详';
		}

		if($issueInfo['fmediaclassid']== 1){
			$data = M('ttvsample')
				->field('fapprovalid, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, "" as fpage, "" as fsize, favifilepng,favifilename,uuid
				')
				->where('fid = '.$issueInfo['fsampleid'])
				->find();
			$issueInfo['issueurl'] = A('Common/Media','Model')->get_m3u8($issueInfo['fmediaid'],strtotime($issueInfo['fstarttime']),strtotime($issueInfo['fendtime']));

			$where2['source_type'] = 10;
			$where2['source_tid'] = $issueInfo['sid'];
			$where2['source_mediaclass'] = '01';
			$where2['validity_time'] = ['gt',time()];
			$data2 = M('source_make')->field('source_url,source_id,source_state')->where($where2)->find();
			if(!empty($data2)){
				$issueInfo['source_url'] = $data2['source_url'];
				$issueInfo['source_id'] 	= $data2['source_id'];
				$issueInfo['source_state'] = $data2['source_state'];
			}
		}elseif($issueInfo['fmediaclassid']== 2){
			$data = M('tbcsample')
				->field('fapprovalid, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, "" as fpage, "" as fsize, favifilepng,favifilename,uuid ')
				->where('fid = '.$issueInfo['fsampleid'])
				->find();

			$issueInfo['issueurl'] = A('Common/Media','Model')->get_m3u8($issueInfo['fmediaid'],strtotime($issueInfo['fstarttime']),strtotime($issueInfo['fendtime']));

			$where2['source_type'] = 10;
			$where2['source_tid'] = $issueInfo['sid'];
			$where2['source_mediaclass'] = '02';
			$where2['validity_time'] = ['gt',time()];
			$data2 = M('source_make')->field('source_url,source_id,source_state')->where($where2)->find();
			if(!empty($data2)){
				$issueInfo['source_url'] = $data2['source_url'];
				$issueInfo['source_id'] 	= $data2['source_id'];
				$issueInfo['source_state'] = $data2['source_state'];
			}
		}elseif($issueInfo['fmediaclassid']== 3){
			$data = M('tpapersample')
				->field('fapprovalid, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, fjpgfilename as favifilepng,fjpgfilename as favifilename,uuid
				')
				->where('fpapersampleid = '.$issueInfo['fsampleid'])
				->find();
		}elseif($issueInfo['fmediaclassid']== 13){
			$data = M('tnetissue')
				->field('fapprovalid, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone,thumb_url_true,net_snapshot,net_original_url,net_target_url,article_content,net_advertiser,ftype,net_original_snapshot,net_platform,tmedia.fmediacode,"" uuid')
				->join('tmedia on tnetissue.fmediaid = tmedia.fid')
				->where('major_key = '.$issueInfo['fsampleid'])
				->find();
		}
		if(!empty($data)){
			$issueInfo = array_merge($issueInfo,$data);
		}

		if($issueInfo['fmediaclassid'] == 1){
			$issueInfo['mclass'] = 'tv';
		}elseif($issueInfo['fmediaclassid'] == 2){
			$issueInfo['mclass'] = 'bc';
		}elseif($issueInfo['fmediaclassid'] == 3){
			$issueInfo['mclass'] = 'paper';
		}elseif($issueInfo['fmediaclassid'] == 13){
			$issueInfo['mclass'] = 'net';
			//广告联盟信息
			if(!empty($issueInfo['trackerids'])){
				$issueInfo['fracker'] = M('tracker')->where(['trackerid'=>['in',explode(',', $issueInfo['trackerids'])]])->group('websitename')->getField('websitename',true);
				$issueInfo['fracker'] = implode(',', $issueInfo['fracker']);
			}else{
				$issueInfo['fracker'] = '';
			}
		}
		$issueInfo['fillegalcontent'] = htmlspecialchars_decode($issueInfo['fillegalcontent']);
		$issueInfo['fstarttime'] = date('H:i:s',strtotime($issueInfo['fstarttime']));
		$issueInfo['fendtime'] = date('H:i:s',strtotime($issueInfo['fendtime']));
		$issueInfo['fissuedate'] = date('Y-m-d',strtotime($issueInfo['fissuedate']));
		$issueInfo['fadlen'] = $issueInfo['flength'];
		$issueInfo['fadowner_name'] = $issueInfo['fadowner'];
		if(!empty($issueInfo)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$issueInfo));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据'));
		}
	}


}