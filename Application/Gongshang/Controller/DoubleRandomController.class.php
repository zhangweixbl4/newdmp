<?php
namespace Gongshang\Controller;
use Think\Controller;

class DoubleRandomController extends BaseController {

	//抽检计划列表
    public function taskListTwo(){

    	//计划列表
    	$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$random_time = I('random_time');//抽检时间
		$region_id = I('region_id');//地区
		$keywords = I('keywords');//关键字
		$fregulatorid = session('regulatorpersonInfo.fregulatorid');

		if($region_id > 100000) $where['tregulator.fregionid'] = $region_id;
		$where['tdsplan.fdsplanname'] = array('like','%'.trim($keywords,' ').'%');	
		$where['tdsplan.fregulatorid'] = $fregulatorid;
		if($random_time){
			$starttime = date('Y-m-d 00:00:00',strtotime($random_time));
			$endtime = date('Y-m-d 00:00:00',strtotime('+1 day',strtotime($starttime)));
			$where['tdsplan.fcreatetime'] = array(array('EGT',$starttime),array('LT',$endtime),'AND');
		}	
    	
    	$count = M('tdsplan')
    					->join('tdstask on tdstask.fdsplanid = tdsplan.fdsplanid')
    					->join('tregulator on tregulator.fid = tdsplan.fregulatorid')
    					->where($where)->count('distinct(tdsplan.fdsplanid)');
    	$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
    	$planList = M('tdsplan')
                            ->field('tdsplan.*,'.
                                    'count(tdstask.fdstaskid) as ext_num,'.//抽查数量
                                    'count(case when tdstask.fstate = 2 then 1 else null end) as check_num,'.//检查数量
                                    'count(case when tdstask.fcheckdisposestyle = tdstask.fdisposestyle and tdstask.fdisposetype = tdstask.fcheckdisposetype then 1 else null end) as qua_num,'.//合格数量
                                    'count(case when tdstask.fchecktime < tdsplan.ffinishtime then 1 else null end) as finish_num')
                            ->join('tdstask on tdstask.fdsplanid = tdsplan.fdsplanid')
                            ->join('tregulator on tregulator.fid = tdsplan.fregulatorid')
                            ->where($where)
    						->group('tdsplan.fdsplanid')
    						->order('tdsplan.fcreatetime desc')
    						->limit($Page->firstRow.','.$Page->listRows)
                            ->select();
    	foreach ($planList as $key => $value) {
    		$planList[$key]['k'] = ($p - 1) * $pp + $key + 1;
    		$planList[$key]['qua_rate'] = number_format($value['qua_num']/$value['check_num']*100,2).'%';//合格率
    		$planList[$key]['check_rate'] = number_format($value['check_num']/$value['ext_num']*100,2).'%';//检查率
    		$planList[$key]['fcreatetime'] = date('Y-m-d H:i',strtotime($value['fcreatetime']));
    	}
    	
    	//任务列表
    	$task_where['tdsplan.fregulatorid'] = $fregulatorid;
    	$task_keywords = I('task_keywords');//关键字
    	$task_region_id = I('task_region_id');//地区
    	$task_random_time = I('task_random_time');//日期
    	$fstate = I('is_check');
    	$fagree = I('is_same');//是否一致
    	if($fagree) $task_where['tdstask.fagree'] = $fagree;
    	if($task_keywords) $task_where['tad.fadname|tmedia.fmedianame|tdsplan.fdsplanname'] = array('like','%'.$task_keywords.'%');
    	if($task_random_time){
    		$starttime = date('Y-m-d H:i:s',strtotime($task_random_time));
			$endtime = date('Y-m-d H:i:s',strtotime($task_random_time)+86400);
			$task_where['tdsplan.fcreatetime'] = array(array('EGT',$starttime),array('LT',$endtime),'and');
    	}
    	if($task_region_id > 100000) $task_where['tregulator.fregionid'] = $task_region_id;
    	if($fstate != '') $task_where['tdstask.fstate'] = $fstate;
    	$taskCount = M('tdstask')
    						->join('tillegalad on tillegalad.fillegaladid = tdstask.fillegaladid')
							->join('tad on tad.fadid = tillegalad.fadid')
							->join('tdsplan on tdsplan.fdsplanid = tdstask.fdsplanid')
							->join('tmedia on tmedia.fid = tdstask.fmediaid')
							->join('tregulator on tregulator.fid = tdstask.fmediaregulatorid')
							->where($task_where)
							->count();
		$taskPage = new \Think\Page($taskCount,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$taskList = M('tdstask')
                            ->field('tdstask.*,
                                    tmedia.fmedianame,
                                    tdsplan.fdsplanname,tdsplan.fcreatetime,tdsplan.ffinishtime,
                                    tillegalad.ffirstissuetime,
                                    tad.fadname')
                            ->join('tillegalad on tillegalad.fillegaladid = tdstask.fillegaladid')
                            ->join('tad on tad.fadid = tillegalad.fadid')
                            ->join('tdsplan on tdsplan.fdsplanid = tdstask.fdsplanid')
                            ->join('tmedia on tmedia.fid = tdstask.fmediaid')
                            ->join('tregulator on tregulator.fid = tdstask.fmediaregulatorid')
                            ->where($task_where)
                            ->order('tdsplan.fcreatetime desc')
							->limit($taskPage->firstRow.','.$taskPage->listRows)
                            ->select();
		$taskList = $this->t_list($taskList,$p,$pp);
		//获取当前工商下管辖的工商局
    	$regulator = M('tregulator')->where(array('fpid' => $fregulatorid))->field('fid,fname')->select();
		$regionid = session('regulatorpersonInfo.regionid');

 		$this->assign('planList',$planList);  	
    	$this->assign('regulator',$regulator);
    	$this->assign('page',$Page->show());//分页输出
    	$this->assign('taskList',$taskList);
    	$this->assign('taskPage',$taskPage->show());//分页输出
    	$this->assign('regionid',$regionid);
		$this->display();

	}

	//获取倒计时
	public function t_list($taskList,$p,$pp){

		foreach($taskList as $key => $value){
            $taskList[$key]['k'] = ($p - 1) * $pp + $key + 1;
            $time = strtotime($value['ffinishtime']) - time();
            $taskList[$key]['time'] = finishTime($time);
            
        }
        return $taskList;

	}

	//抽检任务列表
	public function randomTaskTwo(){

		$fdsplanid = I('fdsplanid');//计划ID
		$fstate = I('fstate');
		$keywords = I('keywords');
		$result = I('result');
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$where['tdstask.fdsplanid'] = $fdsplanid;
		if($fstate != '') $where['tdstask.fstate'] = $fstate;
		if($keywords) $where['tmedia.fmedianame'] = array('like','%'.trim($keywords,' ').'%');
		if($result != '') $where['tdstask.fagree'] = $result;
		$count = M('tdstask')
							->join('tillegalad on tillegalad.fillegaladid = tdstask.fillegaladid')
							->join('tad on tad.fadid = tillegalad.fadid')
							->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
							->join('tmedia on tmedia.fid = tdstask.fmediaid')
							->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')
							->where($where)->count();
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$taskList = M('tdstask')
							->field('tad.fadname,
								tmedia.fmedianame,
								tmediaclass.fclass,
								tillegalad.ffirstissuetime,
								tillegaltype.fillegaltype,
								tdstask.*')
							->join('tillegalad on tillegalad.fillegaladid = tdstask.fillegaladid')
							->join('tad on tad.fadid = tillegalad.fadid')
							->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
							->join('tmedia on tmedia.fid = tdstask.fmediaid')
							->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')
							->limit($Page->firstRow.','.$Page->listRows)
							->where($where)->select();

		$taskList = list_k($taskList,$p,$pp);

		$this->assign('taskList',$taskList);
		$this->assign('page',$Page->show());//分页输出
		$this->display();

	}

	//抽检任务详情
	public function taskDetailsTwo(){

		$fdstaskid = I('fdstaskid');//任务ID

		$where['tdstask.fdstaskid'] = $fdstaskid;
		$taskInfo = M('tdstask')
							->field('tad.fadname,tmedia.fmedianame,tillegaltype.fillegaltype,tillegalad.*,tdstask.*')
							->join('tillegalad on tillegalad.fillegaladid = tdstask.fillegaladid')
							->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
							->join('tmedia on tmedia.fid = tillegalad.fmediaid')
							->join('tad on tad.fadid = tillegalad.fadid')
							->where($where)->find();
		switch ($taskInfo['fmediaclassid']) {
			case 'tv':
				$taskInfo['favifilename']= M('ttvsample')->where(array('fid' => $taskInfo['fsampleid']))->getField('favifilename');
				break;
			case 'bc':
				$taskInfo['favifilename']= M('tbcsample')->where(array('fid' => $taskInfo['fsampleid']))->getField('favifilename');
				break;
			case 'paper':
				$taskInfo['fjpgfilename']= M('tpapersample')->where(array('fpapersampleid' => $taskInfo['fsampleid']))->getField('fjpgfilename');
				break;
			default:
				
				break;
		}
		if($taskInfo['fcheckdisposestyle'] == '线索处理'){
			$type = explode(',',$taskInfo['fcheckdisposetype']);
			$tclueresulttype = M('tclueresulttype')->where(array('fadclueresultid' => array('in',$type)))->field('fadclueresult')->group('fadclueresult')->select();
			foreach ($tclueresulttype as $key => $value) {
				$type_str .= $value['fadclueresult'].',';
			}
			$taskInfo['fcheckdisposetype'] = trim($type_str,',');
		}
		
		$this->assign('taskInfo',$taskInfo);
		$this->assign('tclueresulttype',$tclueresulttype);
		$this->display();

	}

	//检查任务列表
	public function randomTask(){

		$fstate = I('fstate');//任务状态值
		$keywords = I('keywords');//关键字查询
		$is_same = I('is_same');//是否一致
		$fillegaltypecode = I('fillegaltypecode');//违法程度
		$fcheckregulatorid = session('regulatorpersonInfo.fregulatorid');//检查机构ID

		$where['tdstask.fstate'] = array('in',array(0,1));
		if($fstate == 2) $where['tdstask.fstate'] = 2;//状态值为2时改变
		$where['tdstask.fcheckregulatorid'] = $fcheckregulatorid;
		if($keywords) $where['tad.fadname|tmedia.fmedianame'] = array('like','%'.trim($keywords,' ').'%');
		if($is_same) $where['tdstask.fagree'] = $is_same;
		if($fillegaltypecode) $where['tillegalad.fillegaltypecode'] = array('in',$fillegaltypecode);

		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$count = M('tdstask')
							->join('tillegalad on tillegalad.fillegaladid = tdstask.fillegaladid')
							->join('tad on tad.fadid = tillegalad.fadid')
							->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
							->join('tmedia on tmedia.fid = tdstask.fmediaid')
							->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')
							->where($where)->count();
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$taskList = M('tdstask')
							->field('tad.fadname,tmedia.fmedianame,tdsplan.fcreatetime,tillegalad.ffirstissuetime,tillegaltype.fillegaltype,tdstask.*')
							->join('tillegalad on tillegalad.fillegaladid = tdstask.fillegaladid')
							->join('tad on tad.fadid = tillegalad.fadid')
							->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
							->join('tmedia on tmedia.fid = tdstask.fmediaid')
							->join('tdsplan on tdsplan.fdsplanid = tdstask.fdsplanid')
							->limit($Page->firstRow.','.$Page->listRows)
							->order('tdstask.fdstaskid')
							->where($where)->select();		
		$taskList = list_k($taskList,$p,$pp);
		$illegalType = M('tillegaltype')->select();

		$this->assign('taskList',$taskList);
		$this->assign('page',$Page->show());//分页输出
		$this->assign('illegalType',$illegalType);
		$this->display();

	} 

	public function taskDetails(){

		$fdstaskid = I('fdstaskid');//任务ID
		$fstate = M('tdstask')->where(array('fdstaskid' => $fdstaskid))->getField('fstate');
		if($fstate == 0) M('tdstask')->where(array('fdstaskid' => $fdstaskid))->save(array('fstate' => 1));//状态改为正检查

		$where['tdstask.fdstaskid'] = $fdstaskid;
		$taskInfo = M('tdstask')
							->field('tad.fadname,tmedia.fmedianame,tillegaltype.fillegaltype,tillegalad.*,tdstask.*')
							->join('tillegalad on tillegalad.fillegaladid = tdstask.fillegaladid')
							->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
							->join('tmedia on tmedia.fid = tillegalad.fmediaid')
							->join('tad on tad.fadid = tillegalad.fadid')
							->where($where)->find();
		switch ($taskInfo['fmediaclassid']) {
			case 'tv':
				$taskInfo['favifilename']= M('ttvsample')->where(array('fid' => $taskInfo['fsampleid']))->getField('favifilename');
				break;
			case 'bc':
				$taskInfo['favifilename']= M('tbcsample')->where(array('fid' => $taskInfo['fsampleid']))->getField('favifilename');
				break;
			case 'paper':
				$taskInfo['fjpgfilename']= M('tpapersample')->where(array('fpapersampleid' => $taskInfo['fsampleid']))->getField('fjpgfilename');
				break;
			default:
				
				break;
		}
		if($taskInfo['fcheckdisposestyle'] == '线索处理' && $taskInfo['fstate'] == 2){
			$type = explode(',',$taskInfo['fcheckdisposetype']);
			$tclueresulttype = M('tclueresulttype')->where(array('fadclueresultid' => array('in',$type)))->field('fadclueresult')->group('fadclueresult')->select();
			foreach ($tclueresulttype as $key => $value) {
				$type_str .= $value['fadclueresult'].',';
			}
			$taskInfo['fcheckdisposetype'] = trim($type_str,',');
		}
		$tclueresulttype = M('tclueresulttype')->select();//线索处置结果类型

		$this->assign('fstate',$fstate);
		$this->assign('taskInfo',$taskInfo);
		$this->assign('tclueresulttype',$tclueresulttype);
		$this->display();

	}

	//提交任务
	public function ajax_submit(){

		$fdstaskid = I('fdstaskid');//任务ID
		$fdsplanid = I('fdsplanid');//计划ID
		$judgment = I('judgment');//判定结果
		$description = I('description');//备注

		$handle_res = C('handle_res');
		if($judgment == $handle_res['taddirect']){			
			$fcheckdisposetype = I('fdirecttype');
		}elseif($judgment == $handle_res['tadcase']){
			$fcheckdisposetype = implode(',',I('fpentype'));
			if(!$fcheckdisposetype) $this->ajaxReturn(array('code'=>-1,'msg'=>'线索处理原因未选'));
		}
		$tdstaskInfo = M('tdstask')->where(array('fdstaskid' => $fdstaskid))->limit(1)->find();
		if($judgment == $tdstaskInfo['fdisposestyle'] && $fcheckdisposetype == $tdstaskInfo['fdisposetype']){
			$agree = 2;
		}else{
			$agree = 1;
		}

		$task_data['fcheck'] = session('regulatorpersonInfo.fname');
		$task_data['fchecktime'] = date('Y-m-d H:i:s',time());
		$task_data['fcheckdisposestyle'] = $judgment;
		$task_data['fcheckdisposetype'] = $fcheckdisposetype;
		$task_data['fcheckdisposedescirbe'] = $description;
		$task_data['fagree'] = $agree;
		$task_data['fstate'] = 2;
		$result = M('tdstask')->where(array('fdstaskid' => $fdstaskid))->save($task_data);
		if($result){
			//检查数量和抽检数量
			$count = M('tdstask')->field('count(fdstaskid) as all_num,count(case when fstate = 2 then 1 else null end) as check_num')->where(array('fdsplanid' => $fdsplanid))->find();
			if($count['all_num'] == $count['check_num']) M('tdsplan')->where(array('fdsplanid' => $fdsplanid))->save(array('fstate' => 1));
			$this->ajaxReturn(array('code'=>0,'msg'=>'判定成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'判定失败'));
		}

	}

	//处理结果
	public function taskResults(){

		$result = I('result');
		$keywords = I('keywords');
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$fmediaregulatorid = session('regulatorpersonInfo.fregulatorid');//检查机构ID
		// $fmediaregulatorid = 20110000;

		$where['tdstask.fstate'] = 2;
		$where['tdstask.fmediaregulatorid'] = $fmediaregulatorid;
		if($result) $where['tdstask.fagree'] = $result;
		if($keywords) $where['tmedia.fmedianame'] = array('like','%'.trim($keywords,' ').'%');

		$count = M('tdstask')
							->join('tillegalad on tillegalad.fillegaladid = tdstask.fillegaladid')
							->join('tad on tad.fadid = tillegalad.fadid')
							->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
							->join('tmedia on tmedia.fid = tdstask.fmediaid')
							->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')
							->where($where)->count();
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$taskList = M('tdstask')
							->field('tad.fadname,
								tmedia.fmedianame,
								tmediaclass.fclass,
								tillegalad.ffirstissuetime,
								tillegaltype.fillegaltype,
								tdstask.*,
								case when tdstask.fdisposestyle = tdstask.fcheckdisposestyle and tdstask.fdisposetype = tdstask.fcheckdisposetype then 1 else 0 end as result')
							->join('tillegalad on tillegalad.fillegaladid = tdstask.fillegaladid')
							->join('tad on tad.fadid = tillegalad.fadid')
							->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
							->join('tmedia on tmedia.fid = tdstask.fmediaid')
							->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')
							->limit($Page->firstRow.','.$Page->listRows)
							->order('tdstask.fdstaskid')
							->where($where)->select();
		
		$taskList = list_k($taskList,$p,$pp);
		$this->assign('taskList',$taskList);
		$this->assign('page',$Page->show());//分页输出
		$this->display();

	}

	//生成双随机任务
	public function ajax_build_task(){

		$fdsplanname = I('task_name');//计划名称
		$fmediaregulatorid = I('check_org');//主管机构ID
		$fcheckregulatorid = I('send_org');//检查机构ID
		$mediatype = I('media_type');//媒体类型
		$days = I('random_time');//天数
		$fnumber = I('random_count');//条数
		$fcondition = array(
			'fdsplanname' => $fdsplanname,
			'fmediaregulatorid' => $fmediaregulatorid,
			'fcheckregulatorid' => $fcheckregulatorid,
			'mediatype' => $mediatype,
			'days' => $days,
			'fnumber' => $fnumber
		);//计划条件

		$org_num = C('org_num');//随机机构数 
		$fregulatorid = session('regulatorpersonInfo.fregulatorid');//管理机构ID
		$fmanageid = $fmediaregulatorid['fid'][0];//主管机构第一个fid
		$fcheckid = $fcheckregulatorid['fid'][0];//检查机构第一个fid
		$regulator = M('tregulator')->where(array('fpid' => $fregulatorid))->getField('fid,fname');//获取机构ID和名称
		$fregulator = $regulator;
		if(in_array($fmanageid,$org_num) && in_array($fcheckid,$org_num)){//都为随机
			$rand_num = $fmanageid + $fcheckid;//随机数总和
			$regulator_rand = array_rand($regulator,$rand_num);//随机获取
			$fmanage = array_rand($regulator_rand,$fmanageid);//主管机构随机数
			foreach ($fmanage as $key => $value) {
				$fmedia_rand[] = $regulator_rand[$value];//主管机构随机数
			}
			$fcheck_rand = array_diff($regulator_rand,$fmedia_rand);//检查机构随机数
		}elseif(in_array($fmanageid,$org_num)){//主管机构随机
			$fcheck_rand = $fcheckregulatorid['fid'];
			$where['fid'] = array('notin',$fcheck_rand);
			$regulator = M('tregulator')->where(array('fpid' => $fregulatorid))->where($where)->getField('fid,fname');
			$fmedia_rand = array_rand($regulator,$fmanageid);		
		}elseif(in_array($fcheckid,$org_num)){//检查机构随机
			$fmedia_rand = $fmediaregulatorid['fid'];
			$where['fid'] = array('notin',$fmedia_rand);
			$regulator = M('tregulator')->where(array('fpid' => $fregulatorid))->where($where)->getField('fid,fname');
			$fcheck_rand = array_rand($regulator,$fcheckid);
		}else{
			$fmedia_rand = $fmediaregulatorid['fid'];
			$fcheck_rand = $fcheckregulatorid['fid'];
		}
		
		$fillegaladidList = $this->fillegaladidList($fmedia_rand,$mediatype,$days,$fnumber);//获取违法广告ID
		$fcheck_num = count($fillegaladidList);//条数
		$fcheck_org = count($fcheck_rand);//检查机构数

		M()->startTrans();//开启事务
		//添加双随机计划
		$plan_data['fdsplanname'] = $fdsplanname;
		$plan_data['fcondition'] = json_encode($fcondition);
		$plan_data['fregulatorid'] = $fregulatorid;
		$plan_data['fregulator'] = session('regulatorpersonInfo.regulatorname');
		$plan_data['fcreator'] = session('regulatorpersonInfo.fname');
		$plan_data['fcreatetime'] = date('Y-m-d H:i:s',time());
		$plan_data['fstate'] = 0;
		$fdsplanid = M('tdsplan')->add($plan_data);

		//添加双随机任务
		foreach ($fillegaladidList as $key => $value) {
			$fdisposetype = $value['fpentype']?$value['fpentype']:$value['fdirecttype'];
			$data['fdsplanid'] = $fdsplanid;
			$data['fillegaladid'] = $value['fillegaladid'];
			$data['fmediaid'] = $value['fmediaid'];
			$data['fmediaregulatorid'] = $value['fid'];
			$data['fmediaregualtor'] = $fregulator[$value['fid']];
			$data['fdisposestyle'] = $value['fbizname'];
			$data['fdisposetype'] = $fdisposetype;
			// $data['fdisposedescribe'] = $value['fcreateinfo'];
			$data['fcheckregulatorid'] = $fcheck_rand[($key%$fcheck_org)];
			$data['fcheckregualtor'] = $fregulator[$fcheck_rand[($key%$fcheck_org)]];
			$data['fstate'] = 0;
			$task_data[] = $data;
		}
		$fdstaskInfo = M('tdstask')->addAll($task_data);
		if($fdsplanid && $fdstaskInfo){
			M()->commit();//提交事务
			$this->ajaxReturn(array('code'=>0,'msg'=>'生成任务成功'));
		}else{
			M()->rollback();//回滚事务
			$this->ajaxReturn(array('code'=>-1,'msg'=>'生成任务失败'));
		}

	}

	//获取违法广告ID
	public function fillegaladidList($fmedia_rand,$mediatype,$days,$fnumber){
		if($mediatype == '-1') $mediatype = array_rand(C('media_type'),1);//媒介随机
		if($days == '-1') $days = C('random_time')[rand(0,2)];//天数随机
		if($fnumber == '-1') $fnumber = rand(10,30);//条数随机
		$starttime = date('Y-m-d 00:00:00',time()-86400*$days);
		$endtime = date('Y-m-d 00:00:00');

		$where['tillegalad.fstate'] = 2;
		$where['tillegalad.fcreatetime'] = array(array('EGT',$starttime),array('LT',$endtime));
		$where['tillegalad.fmediaclassid'] = $mediatype;
		$where['tillegaladflow.fstate'] = 2;
		$where['tillegaladflow.fflowname'] = array('in',C('handle_type'));
		$where['tregulator.fid'] = array('in',$fmedia_rand);
		$fillegaladidList = M('tillegalad')
							->field('tillegalad.fillegaladid,tillegalad.fmediaid,
								tillegaladflow.fbizname,tillegaladflow.fcreateinfo,
								taddirect.fdirecttype,
								tadcase.fpentype,								
								tregulator.fid')
							->join('tillegaladflow on tillegaladflow.fillegaladid = tillegalad.fillegaladid')
							->join('left join taddirect on taddirect.fillegaladid = tillegalad.fillegaladid')
							->join('left join tadcase on tadcase.fillegaladid = tillegalad.fillegaladid')
							// ->join('tregulatormedia on tregulatormedia.fmediaid = tillegalad.fmediaid')
							->join('tmedia on tmedia.fid = tillegalad.fmediaid')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->join('tregulator on tregulator.fregionid = tmediaowner.fregionid')
							->where($where)
							->select();

		$fillegaladidCount = count($fillegaladidList);
		if($fillegaladidCount == 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'无数据'));
		//随机数超过统计数则随机数更新为统计数
		if($fnumber > $fillegaladidCount) $fnumber = $fillegaladidCount;
		$regulator_num = count($fmedia_rand);//机构数
		$average = floor($fnumber/$regulator_num);//平均数
		$over = $fnumber%$regulator_num;//余数
		//按机构分组
		foreach ($fillegaladidList as $key => $value) {
			$illegaladidList[$value['fid']][] = $value;
		}
		$illegaladidList = array_values($illegaladidList);
		$illegaladidCount = count($illegaladidList);
		//抽取每个媒介等量的数据
		$arr = array();
		foreach ($illegaladidList as $key => $value) {
			$count = count($value);
			$rand = $count < $average?$count:$average;
			$key_rand = array_rand($value,$rand);
			if(is_array($key_rand)){
				foreach ($key_rand as $val) {
					$arr[$value[$val]['fillegaladid']] = $value[$val];
				}
			}else{
				$arr[$value[$key_rand]['fillegaladid']] = $value[$key_rand];
			}
		}
		//抽取余数数量的数据
		if($over > 0){
			$over_rand = array_rand($illegaladidList,$over);
			if(is_array($over_rand)){
				foreach ($over_rand as $value){
					$illegalad_rand_count = count($illegaladidList[$value]);
					if($illegalad_rand_count <= $average) continue;
					$illegalad_key = rand(0,($illegalad_rand_count - 1));
					$fillegaladid = $illegaladidList[$value][$illegalad_key]['fillegaladid'];
					if(array_key_exists($fillegaladid,$arr)) continue;
					$arr[$fillegaladid] = $illegaladidList[$value][$illegalad_key];
				}
			}else{
				for($i = 0;$i < 50;$i++){
					$illegalad_rand_count = count($illegaladidList[$over_rand]);
					$illegalad_key = rand(0,($illegalad_rand_count - 1));
					$fillegaladid = $illegaladidList[$over_rand][$illegalad_key]['fillegaladid'];
					if(array_key_exists($fillegaladid,$arr)){
						continue;
					}else{
						$arr[$fillegaladid] = $illegaladidList[$over_rand][$illegalad_key];
						break;
					}
				}
			}
		}
		//少于抽取数量随机补全数据
		$arr_count = count($arr);
		if($arr_count < $fnumber){
			for($j = 0;$j < ($fnumber - $arr_count);$j++){
				for($m = 0;$m < 50;$m++){
					$illegalad_rand = rand(0,($illegaladidCount-1));
					$illegalad_rand_count = count($illegaladidList[$illegalad_rand]);
					$illegalad_key = rand(0,($illegalad_rand_count - 1));
					$fillegaladid = $illegaladidList[$illegalad_rand][$illegalad_key]['fillegaladid'];
					if(array_key_exists($fillegaladid,$arr)){
					 	continue;
					}else{					
						$arr[$fillegaladid] = $illegaladidList[$illegalad_rand][$illegalad_key];
						break;
					}
				}
			}
		}
		$arr = array_values($arr);
		return $arr;
	}



}