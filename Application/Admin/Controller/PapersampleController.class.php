<?php
//报纸样本控制器
namespace Admin\Controller;
use Think\Controller;
class PapersampleController extends BaseController {

	public function index(){
		
		if(A('Admin/Authority','Model')->authority('200155') === 0){
			exit('没有权限');//验证是否有新增权限
		}
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$keyword = I('keyword');//搜索关键词

		$fadname = I('fadname');// 广告名称
		$fversion = I('fversion');// 版本描述
		$fmediaid  = I('fmediaid');// 媒体ID
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID
		$fcreatetime_s = I('fcreatetime_s');// 建样时间
		if($fcreatetime_s == '') $fcreatetime_s = '2015-01-01';
		$fcreatetime_e = I('fcreatetime_e');// 建样时间
		if($fcreatetime_e == '') $fcreatetime_e = date('Y-m-d');
		$adclass_code = I('adclass_code');
		$fcreator = I('fcreator');//创建人

		$where = array();//查询条件
		$where['tpapersample.fstate'] = array('neq',-1);
		if($fcreator != ''){
			$where['tpapersample.fcreator'] = array('like','%'.$fcreator.'%');
		}
		if($keyword != ''){
			$where['tpapersample.fversion|tad.fadname'] = array('like','%'.$keyword.'%');
		} 
		
		if($fadname != ''){
			$where['tad.fadname'] = array('like','%'.$fadname.'%');//广告名称
		}
		if($fversion != ''){
			$where['tpapersample.fversion'] = array('like','%'.$fversion.'%');//版本描述
		}
		if($fmediaid != ''){
			$where['tpapersample.fmediaid'] = $fmediaid;//媒体ID
		}
		if(is_array($fillegaltypecode)){
			$where['tpapersample.fillegaltypecode'] = array('in',$fillegaltypecode);//违法类型代码
		}
		if($fcreatetime_s != '' || $fcreatetime_e != ''){
			$where['tpapersample.fcreatetime'] = array('between',$fcreatetime_s.','.$fcreatetime_e.' 23:59:59');
		}

		if($adclass_code != '' && $adclass_code != '0'){
			

			$adclass_code_strlen = strlen($adclass_code);//获取code长度
			$where['left(tad.fadclasscode,'.$adclass_code_strlen.')'] = $adclass_code;//广告分类搜索条件
		
		}
		if(strval(intval($fadname)) == $fadname){
			$where = array('tpapersample.fpapersampleid'=>$fadname);
		}
		$count = M('tpapersample')
								->join('tad on tad.fadid = tpapersample.fadid')
								->join('tadowner on tadowner.fid = tad.fadowner')
								->join('tmedia on tmedia.fid = tpapersample.fmediaid')
								->join('tillegaltype on tillegaltype.fcode = tpapersample.fillegaltypecode')
								->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数

		
		$papersampleList = M('tpapersample')
										->field('tpapersample.*,tad.fadname,tadowner.fname as adowner_name,tmedia.fmedianame,tillegaltype.fillegaltype')
										->join('tad on tad.fadid = tpapersample.fadid')
										->join('tadowner on tadowner.fid = tad.fadowner')
										->join('tmedia on tmedia.fid = tpapersample.fmediaid')
										->join('tillegaltype on tillegaltype.fcode = tpapersample.fillegaltypecode')
										->where($where)
										->order('tpapersample.fpapersampleid desc')
										->limit($Page->firstRow.','.$Page->listRows)->select();//查询广告样本列表
		//var_dump(M('tpapersample')->getLastSql());							
		$papersampleList = list_k($papersampleList,$p,$pp);//为列表加上序号
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		//var_dump($illegaltype);
		//var_dump($papersampleList);
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
    	$this->assign('IllegalList',$IllegalList);//违法表现数据列表
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->assign('papersampleList',$papersampleList);//广告样本列表
		$this->assign('illegaltype',$illegaltype);//违法类型
		
		$this->assign('page',$Page->show());//分页输出
		
		//$this->display();
	}
	public function papersample_edit(){
		if(A('Admin/Authority','Model')->authority('200153') === 0){
			exit('没有权限');//验证是否有新增权限
		}
		$fpapersampleid = I('fpapersampleid');//获取样本ID
		$papersampleDetails = M('tpapersample')
										->field('tpapersample.*,tad.fadname,tad.fbrand,tad.fadclasscode,tad.fadclasscode_v2,tadclass.ffullname,hz_ad_class.ffullname fadclassfullname_v2,tad.fadowner,tadowner.fname as adowner_name,tillegaltype.fillegaltype,tmedia.fmedianame')
										->join('tad on tad.fadid = tpapersample.fadid')
										->join('tadclass on tadclass.fcode = tad.fadclasscode')
										->join('hz_ad_class on hz_ad_class.fcode = tad.fadclasscode_v2')
										->join('tadowner on tadowner.fid = tad.fadowner')
										->join('tillegaltype on tillegaltype.fcode = tpapersample.fillegaltypecode')
										->join('tmedia on tmedia.fid = tpapersample.fmediaid')
										->where(array('tpapersample.fpapersampleid'=>$fpapersampleid))
										->find();//查询样本详情
		$papersampleillegal = M('tpapersampleillegal')
												->field('fillegalcode,fexpression')
												->where(array('fsampleid'=>$fpapersampleid))->select();
		$this->assign("papersampleDetails",$papersampleDetails);
		$this->assign("papersampleillegal",$papersampleillegal);
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
		$this->assign('IllegalList',$IllegalList);//违法表现数据列表
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->display();
	}
	
		
	/*样本详情*/
	public function ajax_papersample_details(){
		
		$fpapersampleid = I('fpapersampleid');//获取样本ID
		$papersampleDetails = M('tpapersample')
										->field('tpapersample.*,tad.fadname,tad.fbrand,tad.fadclasscode,tad.fadclasscode_v2,tadclass.ffullname,hz_ad_class.ffullname fadclassfullname_v2,tad.fadowner,tadowner.fname as adowner_name,tillegaltype.fillegaltype,tmedia.fmedianame')
										->join('tad on tad.fadid = tpapersample.fadid')
										->join('tadclass on tadclass.fcode = tad.fadclasscode')
										->join('hz_ad_class on hz_ad_class.fcode = tad.fadclasscode_v2')
										->join('tadowner on tadowner.fid = tad.fadowner')
										->join('tillegaltype on tillegaltype.fcode = tpapersample.fillegaltypecode')
										->join('tmedia on tmedia.fid = tpapersample.fmediaid')
										->where(array('tpapersample.fpapersampleid'=>$fpapersampleid))
										->find();//查询样本详情
		$papersampleillegal = M('tpapersampleillegal')
												->field('fillegalcode,fexpression')
												->where(array('fsampleid'=>$fpapersampleid))->select();
		$this->ajaxReturn(array('code'=>0,'papersampleDetails'=>$papersampleDetails,'papersampleillegal'=>$papersampleillegal));
	}
	
	/*转入广告线索*/
	public function to_adclue(){
		if(A('Admin/Authority','Model')->authority('200154') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
		}
		$fpapersampleid = I('fpapersampleid');//广告样本ID
		
		$papersampleInfo = M('tpapersample')->where(array('fpapersampleid'=>$fpapersampleid))->find();
		if(intval($papersampleInfo['fillegaltypecode']) == 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'转入失败,只有违法广告能转入'));
		}
		$fmediaclass = M('tmedia')->where(array('fid'=>$papersampleInfo['fmediaid']))->getField('fmediaclassid');//媒介类型代码
		$fmediaclass = mb_substr($fmediaclass,0,2);
		$adclue_count = M('tadclue')->where(array('fsampleid'=>$papersampleInfo['fpapersampleid'],'fmediaclass'=>$fmediaclass))->count();
		if($adclue_count > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'此样本已在线索中'));
		
		$a_data = array();
		$a_data['fsupervise'] = 0;//管理机关

		$a_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$a_data['fstate'] = 0;//状态
		$a_data['fmediaclass'] = $fmediaclass;
		$a_data['fmediaid'] = $papersampleInfo['fmediaid'];//媒介id
		$a_data['fissuedate'] = $papersampleInfo['fissuedate'];//发行日期
		$a_data['fsampleid'] = $papersampleInfo['fpapersampleid'];//样本ID
		$a_data['fadid'] = $papersampleInfo['fadid'];//广告ID
		$a_data['fspokesman'] = $papersampleInfo['fspokesman'];//代言人
		$a_data['fversion'] = $papersampleInfo['fversion'];//版本说明
		$a_data['fapprovalid'] = $papersampleInfo['fapprovalid'];//审批号
		$a_data['fapprovalunit'] = $papersampleInfo['fapprovalunit'];//审批单位

		$a_data['fillegaltypecode'] = $papersampleInfo['fillegaltypecode'];//违法类型代码
		$a_data['fillegalcontent'] = $papersampleInfo['fillegalcontent'];//涉嫌违法内容
		$a_data['fexpressioncodes'] = $papersampleInfo['fexpressioncodes'];//违法表现代码
		$a_data['fexpressions'] = $papersampleInfo['fexpressions'];//违法表现
		$a_data['fconfirmations'] = $papersampleInfo['fconfirmations'];//认定依据
		$a_data['fpunishments'] = $papersampleInfo['fpunishments'];//处罚依据
		$a_data['fpunishmenttypes'] = $papersampleInfo['fpunishmenttypes'];//处罚种类及幅度
		$a_data['fjpgfilename'] = $papersampleInfo['fjpgfilename'];//图片路径
		
		
		$rr = M('tadclue')->add($a_data);
		
		if($rr > 0){
			$fregulatorcode = A('Common/Regulatormedia','Model')->get_regulatormedia($papersampleInfo['fmediaid']);//监管机构ID
			A('Common/Adcluesection','Model')->create_adcluesection($rr,0,$fregulatorcode,session('personInfo.fname'));//创建环节
			$this->ajaxReturn(array('code'=>0,'msg'=>'转入线索成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'转入线索失败,原因未知'));
		}
		
		
	}
	
		/*添加、编辑报纸广告样本*/
	public function add_edit_papersample(){
		M()->startTrans();//开启事务方法

		$fpapersampleid = I('fpapersampleid');//广告样本ID
		$fpapersampleid = intval($fpapersampleid);//转为数字
		$fadname = I('fadname');//广告名
		$fbrand = I('fbrand');// 广告品牌
		$adclass_code = I('adclass_code');//广告分类code
		$fadclasscode_v2 = I('fadclasscode_v2');//商业类别code
		$adowner_name = I('adowner_name');//广告主
		$fversion = I('fversion');//版本
		$fmediaid = I('fmediaid');//媒体ID
		$fissuedate = I('fissuedate');//发布日期
		$fspokesman = I('fspokesman');//代言人
		$fillegalcontent = I('fillegalcontent');//违法内容
		$fexpressioncodes = I('fexpressioncodes');//违法代码
		$fapprovalid = I('fapprovalid');//审批号
		$fapprovalunit = I('fapprovalunit');//审批单位
		$fstate = I('fstate');//状态
		$fsource = I('fsource');//来源（0-监测，1抽查）
		$fjpgfilename = I('fjpgfilename');//上传图片
		
		$fadid = A('Adinput/InputTask','Model')->get_ad_id($fadname,$fbrand,$adclass_code,$adowner_name,$fadclasscode_v2,1);//获取广告ID
		
		$a_e_data['fmediaid'] = $fmediaid;//媒介ID
		$a_e_data['fissuedate'] = $fissuedate;//发布日期
		$a_e_data['fadid'] = $fadid;//广告ID
		$a_e_data['fversion'] = $fversion;//版本说明
		$a_e_data['fspokesman'] = $fspokesman;//代言人
		$a_e_data['fillegalcontent'] = $fillegalcontent;//违法内容
		
		$a_e_data['fapprovalid'] = $fapprovalid;//审批号
		$a_e_data['fapprovalunit'] = $fapprovalunit;//审批单位
		$a_e_data['fjpgfilename'] = $fjpgfilename;//图片文件路径
		$a_e_data['fsource'] = $fsource;//来源（0-监测，1抽查）
		$a_e_data['fstate'] = $fstate;//状态（-1删除，0无效，1-有效，2抽查）
		
		if(M('tadclass')->where(array('fcode'=>$adclass_code))->count() == 0){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'广告类别错误'));
		} 
		if($fjpgfilename == ''){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'图片素材路径错误'));
		}
		if($fissuedate == ''){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'发布日期错误'));
		}
		if($fmediaid == ''){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'请选择发布媒介'));
		}
		if($fadname == ''){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'请输入广告名称'));
		}
		if($fpapersampleid == 0){//判断是修改还是新增
			if(A('Admin/Authority','Model')->authority('200151') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
			}
			$a_e_data['fcreator'] = session('personInfo.fname');//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tpapersample')->add($a_e_data);//新增数据
			$papersampleid = $rr;//样本ID赋值	
			
		}else{
			if(A('Admin/Authority','Model')->authority('200152') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
			}
			$a_e_data['fmodifier'] = session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tpapersample')->where(array('fpapersampleid'=>$fpapersampleid))->save($a_e_data);//修改数据
			$papersampleid = $fpapersampleid;//样本ID赋值			
		}
		
		$illegal = A('Open/Papersample','Model')->papersampleillegal($papersampleid,explode(',',$fexpressioncodes),session('personInfo.fname'));//添加报纸广告违法表现对应表并获取冗余字段
		$rr_illegal = M('tpapersample')->where(array('fpapersampleid'=>$papersampleid))->save($illegal);//修改数据
		
		//var_dump($rr_illegal);
		if($rr > 0){
			set_tillegalad_data($papersampleid,'paper');//加入AGP违法广告逻辑
			M()->commit();//事务提交方法
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功'));
		}else{
			M()->rollback();//事务回滚方法
			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败,原因未知'));
		}

	}

}