<?php
namespace Common\Model;


class InfraredModel{
	
	
	/*控制红外媒介*/
	public function ircontrol($cmdIndex,$device_num,$port_num){

		//$device_num = strtoupper($device_num);
		if(!S('ir_'.$device_num.'_'.$port_num)){//判断有没有待执行的命令
			S('ir_'.$device_num.'_'.$port_num,$cmdIndex,600);
			$success = true;
		}else{
			$success = false;
		}
		
		return $success;//返回控制成功状态
	}
	
	/*检查设备是否在线*/
	public function online_check($device_num){
		
		$last_time = S('ir_online_'.$device_num);//从缓存查询最后在线时间
		
		if(!$last_time){//从数据库查询最后在线时间
			$last_time = M('tdevice')->where(array('infrared_code'=>$device_num))->getField('ir_last_time');//从数据库查询最后在线时间
		}
		
		$ir_online = array();
		if($last_time > (time() - 60)){
			$ir_online['online'] = true; 
		}else{
			$ir_online['online'] = false; 
		}
		
		$ir_online['keepaliveTime'] = $last_time;
		
		return $ir_online;
		
	}
	
	
	
	
	
	
	
	
	
	
}