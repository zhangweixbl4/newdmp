<?php
namespace InputPc\Controller;
use Think\Controller;
class BcsamController extends BaseController {
	

	

    public function index(){
		
		$wx_id = intval(session('wx_id'));
		
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$keyword = I('keyword');//搜索关键词
		$adclass_code = I('adclass_code');
		$inputstate = I('inputstate');//录入状态
		$inspectstate = I('inspectstate');//初审状态
		$review_state = I('review_state');//复审状态
		$fillegaltypecode = I('fillegaltypecode');//违法类型
		
		
		$fissuedate_s = I('fissuedate_s');
		if($fissuedate_s == ''){
			$fissuedate_s = date('Y-m-d',time()-30*86400);
			$_GET['fissuedate_s'] = $fissuedate_s;
		}	
		$fissuedate_s = date('Y-m-d',strtotime($fissuedate_s));
		
		$fissuedate_e = I('fissuedate_e');
		if($fissuedate_e == ''){
			$fissuedate_e = date('Y-m-d');
			$_GET['fissuedate_e'] = $fissuedate_e;
		}
		$fissuedate_e = date('Y-m-d',strtotime($fissuedate_e));		
				
		$where = array();
		$where['sample.fissuedate'] = array('between',array($fissuedate_s,$fissuedate_e));
		
		$where['_string'] .= '(finputstate = 1 or ';//信息未录入
		$where['_string'] .= 'finspectstate = 1 or ';//违法未判定
		
		$where['_string'] .= 'finputuser = '.$wx_id.' or ';//自己录入的
		$where['_string'] .= 'finspectuser = '.$wx_id.') ';//自己判定的
		$where['_string'] .= A('InputPc/Task','Model')->user_data_authority($wx_id);//数据权限控制
		$where['sample.fstate'] = 1;
		$where['sample.fcuted'] = 2;
		
		if($fillegaltypecode != ''){
			$where['sample.fillegaltypecode'] = $fillegaltypecode;//违法类型
		}
		if($review_state != '' && A('InputPc/Task','Model')->user_other_authority('1005')){
			$where['_string'] = 'sample.review_state = '.$review_state;
			
		}
		if($inputstate != ''){
			$where['finputstate'] = $inputstate;//录入状态
		} 
		if($inspectstate != ''){
			$where['finspectstate'] = $inspectstate;//初审状态
		} 
		
		
		if($adclass_code != ''){
			$adclass_code_strlen = strlen($adclass_code);//获取code长度
			$where['left(tad.fadclasscode,'.$adclass_code_strlen.')'] = $adclass_code;//广告分类搜索条件
		
		}
		if($keyword != ''){
			$where['tad.fadname|sample.fversion|tmedia.fmedianame'] = array('like','%'.$keyword.'%');
		} 
		

		$count = M('tbcsample')
								->alias('sample')
								->join('tad on tad.fadid = sample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = sample.fmediaid')
								->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
								
								->where($where)->count();
		
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$samList = M('tbcsample')
								->alias('sample')
								->field('
										sample.fid,
										tad.fadname,
										sample.fversion,
										sample.fissuedate,
										tadclass.ffullname as adclass_fullname,
										tillegaltype.fillegaltype,
										tmedia.fmedianame,
										sample.finputstate,
										sample.finspectstate,
										sample.finputuser,
										sample.finspectuser
										
										')
								->join('tad on tad.fadid = sample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = sample.fmediaid')
								->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
								->order('sample.fissuedate desc ,sample.fid desc ')
								->limit($Page->firstRow.','.$Page->listRows)
								->where($where)->select();
		
		//var_dump(M('tbcsample')->getLastSql());
		$samList = list_k($samList,$p,$pp);//为列表加上序号
		$this->assign('samList',$samList);
		$this->assign('wx_id',$wx_id);
		if(	A('InputPc/Task','Model')->user_other_authority('1005') ){
			$this->assign('user_other_authority_1005','true');//违法表现数据列表
		}
		$illegaltypeList = M('tillegaltype')->cache(true,600)->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		$this->assign('illegaltypeList',$illegaltypeList);//违法类型数据列表
		
		$this->assign('page',$Page->show());//分页输出
		$this->display();
	}
	
	
	public function bcsample_edit(){
		$fid = I('fid');//获取样本ID
		$wx_id = intval(session('wx_id'));
		$task_wx_id = intval(S('bc_task_'.$fid));
		if($wx_id != $task_wx_id && $task_wx_id != 0){
			$task_wx_info = M('ad_input_user')->field('wx_id,nickname')->where(array('wx_id'=>$task_wx_id))->find();
		}
		$this->assign('task_wx_info',$task_wx_info);

		S('bc_task_'.$fid,$wx_id,300);
		$bcsampleDetails = M('tbcsample')
										->field('tbcsample.*,tad.fadname,tad.fbrand,tad.fadclasscode,tad.fadowner,tadclass.ffullname,tadowner.fname as adowner_name,tillegaltype.fillegaltype,tmedia.fmedianame')
										->join('tad on tad.fadid = tbcsample.fadid')
										->join('tadclass on tadclass.fcode = tad.fadclasscode')
										->join('tmedia on tmedia.fid = tbcsample.fmediaid')
										->join('tadowner on tadowner.fid = tad.fadowner')
										->join('tillegaltype on tillegaltype.fcode = tbcsample.fillegaltypecode')
										->where(array('tbcsample.fid'=>$fid))
										->find();//查询样本详情
		$bcsampleillegal = M('tbcsampleillegal')
												->field('fillegalcode,fexpression')
												->where(array('fsampleid'=>$fid))->select();
		// var_dump($bcsampleDetails);
		$this->assign('bcsampleDetails',$bcsampleDetails);
		$this->assign('bcsampleillegal',$bcsampleillegal);
		$illegaltypeList = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
		$this->assign('illegaltypeList',$illegaltypeList);//违法类型数据列表
		$this->assign('IllegalList',$IllegalList);//违法表现数据列表
		$this->display();
	}
	
	
	/*添加、编辑电视广告样本*/
	public function edit_bcsample(){
		$wx_id = intval(session('wx_id'));
		$wxInfo = M('ad_input_user')->field('wx_id,nickname,input_seniority')->where(array('wx_id'=>$wx_id))->find();
		if(	!A('InputPc/Task','Model')->user_other_authority('1001')){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
		}
		$fid = I('fid');//广告样本ID
		$fid = intval($fid);//转为数字
		$fadname = I('fadname');//广告名
		$fbrand = I('fbrand');// 广告品牌
		$adclass_code = I('adclass_code');//广告分类code
		$adowner_name = I('adowner_name');//广告主
		$fversion = I('fversion');//版本
		$fspokesman = I('fspokesman');//代言人
		$fapprovalid = I('fapprovalid');//审批号
		$fapprovalunit = I('fapprovalunit');//审批单位
		$fadmanuno = I('fadmanuno');//广告中标识的生产批准文号
		$fmanuno = I('fmanuno');//生产批准文号
		$fadapprno = I('fadapprno');//广告中标识的广告批准文号
		$fapprno = I('fapprno');//广告批准文号
		$fadent = I('fadent');//广告中标识的生产企业（证件持有人）名称
		$fent = I('fent');//生产企业（证件持有人）名称
		$fentzone = I('fentzone');//生产企业（证件持有人）所在地区
		
		if(M('tbcsample')->where(array('fid'=>$fid,'finputstate'=>1))->count() == 0){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'该广告已经录入过了'));
		}
		
		$fadid = A('Common/Ad','Model')->getAdId($fadname,$fbrand,$adclass_code,$adowner_name,$wxInfo['nickname']);//获取广告ID
		

		$a_e_data['fadid'] = $fadid;//广告ID
		$a_e_data['fversion'] = $fversion;//版本说明
		$a_e_data['fspokesman'] = $fspokesman;//代言人
		$a_e_data['fillegalcontent'] = $fillegalcontent;//违法内容
		$a_e_data['fapprovalid'] = $fapprovalid;//审批号
		$a_e_data['fapprovalunit'] = $fapprovalunit;//审批单位
		$a_e_data['finputstate'] = 2;//录入状态
		$a_e_data['finputuser'] = $wx_id;//信息录入人
		$a_e_data['fmodifier'] = $wxInfo['nickname'];//修改人
		$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$a_e_data['fadmanuno'] = $fadmanuno;//广告中标识的生产批准文号
		$a_e_data['fmanuno'] = $fmanuno;//生产批准文号
		$a_e_data['fadapprno'] = $fadapprno;//广告中标识的广告批准文号
		$a_e_data['fapprno'] = $fapprno;//广告批准文号
		$a_e_data['fadent'] = $fadent;//广告中标识的生产企业（证件持有人）名称
		$a_e_data['fent'] = $fent;//生产企业（证件持有人）名称
		$a_e_data['fentzone'] = $fentzone;//生产企业（证件持有人）所在地区
		if(M('tadclass')->where(array('fcode'=>$adclass_code))->count() == 0){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'广告类别错误'));
		} 

		if($fadname == ''){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'请输入广告名称'));
		}

		$rr = M('tbcsample')->where(array('fid'=>$fid,'finputstate'=>1,'fstate'=>1))->save($a_e_data);//修改数据		
		
		if($rr > 0){
			A('InputPc/Task','Model')->change_task_count($wx_id,'bc_input',1);//广播录入数量+1

			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功'));
		}else{

			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败,原因未知'));
		}

	}
	
	/*编辑电视广告样本违法*/
	public function edit_bcsample_illegal(){
		$wx_id = intval(session('wx_id'));
		$wxInfo = M('ad_input_user')->field('wx_id,nickname,audit_seniority')->where(array('wx_id'=>$wx_id))->find();
		if(	!A('InputPc/Task','Model')->user_other_authority('1001')){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
		}
		M()->startTrans();//开启事务方法

		$fid = I('fid');//广告样本ID
		$fid = intval($fid);//转为数字


		$fillegalcontent = I('fillegalcontent');//违法内容
		$fexpressioncodes = I('fexpressioncodes');//违法代码
		$fillegaltypecode = I('fillegaltype');//违法类型

		if(intval($fillegaltypecode) > 0 && ($fillegalcontent == '' || $fexpressioncodes == '')){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'选择错误,提交失败'));
		}
		
		if(intval($fillegaltypecode) == 0 && ($fillegalcontent != '' || $fexpressioncodes != '')){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'选择错误,提交失败'));
		}
		
		$bcsampleid = $fid;//样本ID赋值			
		
		
		$illegal = A('Open/Bcsample','Model')->bcsampleillegal($bcsampleid,explode(',',$fexpressioncodes),$wxInfo['nickname']);//添加电视广告违法表现对应表并获取冗余字段
		$illegal['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$illegal['fillegalcontent'] = $fillegalcontent;//违法内容
		$illegal['finspectstate'] = 2;//判定状态
		$illegal['finspectuser'] = $wx_id;//违法审查人
		$illegal['fmodifier'] = $wxInfo['nickname'];//修改人
		$illegal['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		if($fillegaltypecode != '') $illegal['fillegaltypecode'] = $fillegaltypecode;//违法类型
		
		
		$rr_illegal = M('tbcsample')->where(array('fid'=>$bcsampleid,'finspectstate'=>1,'fstate'=>1))->save($illegal);//修改数据

		if($rr_illegal > 0){
			M()->commit();//事务提交方法
			set_tillegalad_data($bcsampleid,'paper');//加入AGP违法广告逻辑
			A('Common/IllegalAdZongju','Model')->add_illegal_ad(2,$bcsampleid);//国家局违法广告库
			A('InputPc/Task','Model')->change_task_count($wx_id,'bc_inspect',1);//广播审核数量+1
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功'));
		}else{
			M()->rollback();//事务回滚方法
			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败,原因未知'));
		}

	}
	
	/*退回重新录入*/
	public function back_bcsample(){
		$wx_id = intval(session('wx_id'));
		$fid = I('fid');//广告样本ID
		$fid = intval($fid);//转为数字
		$where = array();
		$where['finputuser'] = $wx_id;
		$where['fid'] = $fid;
		
		$rr = M('tbcsample')->where($where)->save(array('finputstate'=>1,'finputuser'=>0));
		if($rr > 0){
			A('InputPc/Task','Model')->change_task_count($wx_id,'bc_input',-1);//广播录入数量-1
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败'));
		}
		
	}
	
	/*退回重新初审*/
	public function back_bcsample_illegal(){
		$wx_id = intval(session('wx_id'));
		$fid = I('fid');//广告样本ID
		$fid = intval($fid);//转为数字
		$where = array();
		$where['finspectuser'] = $wx_id;
		$where['fid'] = $fid;
		
		$rr = M('tbcsample')->where($where)->save(array('finspectstate'=>1,'finspectuser'=>0));
		if($rr > 0){
			A('InputPc/Task','Model')->change_task_count($wx_id,'bc_inspect',-1);//广播审核数量-1
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败'));
		}
		
	}
	

	

}