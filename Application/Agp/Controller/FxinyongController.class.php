<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 信用管理
 */

class FxinyongController extends BaseController
{

	
	/**
	 * 获取信用数据列表
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function index()
	{
		session_write_close();
		$system_num = getconfig('system_num');

		$p 			= I('page', 1);//当前第几页
		$pp 		= 20;//每页显示多少记录
		$medianame 	= I('medianame');//媒体名称
		$n 			= I('n');//年
		$y 			= I('y');//月
		$fmtype		= I('fmtype');//媒体类型
		$regionid	= I('area');//地区

		if(!empty($fmtype)){//媒体类型
			$where['fmtype'] = $fmtype;
		}
		if(!empty($medianame)){//媒体名称
			$where['tmedia.fmedianame'] = array('like','%'.$medianame.'%');
		}
		if(!empty($n)){//年份
			$where['n'] = $n;
		}
		if(!empty($y)){//月份
			$where['y'] = $y;
		}
		if(!empty($regionid)){
			$where['tregion.fid'] = $regionid;
		}

		$count = M('tmedia_credit')
			->join('tmedia on tmedia_credit.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
			->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->join('tmediaowner on tmediaowner.fid=tmedia.fmediaownerid')
			->join('tregion on tmediaowner.fregionid=tregion.fid')
			->where($where)
			->count();

		$Page 	= new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$do_tct = M('tmedia_credit')
			->field('tmedia_credit.*, (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tregion.fname1')
			->join('tmedia on tmedia_credit.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
			->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->join('tmediaowner on tmediaowner.fid=tmedia.fmediaownerid')
			->join('tregion on tmediaowner.fregionid=tregion.fid')
			->where($where)
			->order('fname1 asc,fmedianame asc,n desc, y desc')
			->page($p,$pp)
			->select();

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tct)));
	}

	/**
	 * 更新信用数据
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * by zw
	 */
	public function update_credit(){
		$system_num = getconfig('system_num');

		$n = I('n')?I('n'):date('Y');//年

		if($n < date('Y')){
			$j = 12;
		}else{
			$j = date('m')-1;
		}
		
		$ruleinfo = $this->getRuleInfo();//获取规则
		$number = $ruleinfo['welfare_number']-1;

		$adddata = [];
		for($i=1; $i<=$j; $i++){
			$mediatype = ['tv','bc'];
			foreach ($mediatype as $val) {
				$tb_issue = gettable($val,strtotime($n.'-'.$i.'-01'));
				if($val == 'tv'){
					$sql = 'select 
						"01" as fmtype,
						a.fmediaid,
						FROM_UNIXTIME(a.fstarttime,"%Y") as n,
						FROM_UNIXTIME(a.fstarttime,"%m") as y,
						day(last_day(FROM_UNIXTIME(a.fstarttime,"%Y-%m-%d"))) as tjts,
						count(*) as ggsl,
						sum(case when b.fillegaltypecode>0 then 1 else 0 end) as wfggsl,
						sum(case when c.fadclasscode="2202" then 1 else 0 end) as gyggsl,
						ifnull(sum(case when b.fillegaltypecode>0 then 1 else 0 end)/(count(*)+sum(case when c.fadclasscode="2202" then 2 else 0 end)),0) as ggslwfl,
						sum(flength) as ggscmj,
						sum(case when b.fillegaltypecode>0 then flength else 0 end) as wfggscmj,
						sum(case when c.fadclasscode="2202" then flength else 0 end) as gyggscmj,
						ifnull(sum(case when b.fillegaltypecode>0 then flength else 0 end)/(sum(flength)+sum(case when c.fadclasscode="2202" then flength*2 else 0 end)),0) as ggscmjwfl 
						from 
						(select ttp.fmediaid,fstarttime,fsampleid,flength from '.$tb_issue.',tmedia_temp ttp where '.$tb_issue.'.fmediaid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid').' ) as a,
						ttvsample as b,
						tad c
						where a.fsampleid=b.fid and b.fadid=c.fadid and c.fadid<>0 and left(c.fadclasscode,2) <> "23" 
						group by a.fmediaid,n,y';
				}else{
					$sql = 'select 
						"02" as fmtype,
						a.fmediaid,
						FROM_UNIXTIME(a.fstarttime,"%Y") as n,
						FROM_UNIXTIME(a.fstarttime,"%m") as y,
						day(last_day(FROM_UNIXTIME(a.fstarttime,"%Y-%m-%d"))) as tjts,
						count(*) as ggsl,
						sum(case when b.fillegaltypecode>0 then 1 else 0 end) as wfggsl,
						sum(case when c.fadclasscode="2202" then 1 else 0 end) as gyggsl,
						ifnull(sum(case when b.fillegaltypecode>0 then 1 else 0 end)/(count(*)+sum(case when c.fadclasscode="2202" then 2 else 0 end)),0) as ggslwfl,
						sum(flength) as ggscmj,
						sum(case when b.fillegaltypecode>0 then flength else 0 end) as wfggscmj,
						sum(case when c.fadclasscode="2202" then flength else 0 end) as gyggscmj,
						ifnull(sum(case when b.fillegaltypecode>0 then flength else 0 end)/(sum(flength)+sum(case when c.fadclasscode="2202" then flength*2 else 0 end)),0) as ggscmjwfl 
						from 
						(select ttp.fmediaid,fstarttime,fsampleid,flength from '.$tb_issue.',tmedia_temp ttp where '.$tb_issue.'.fmediaid=ttp.fmediaid  and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid').') as a,
						tbcsample as b,
						tad c
						where a.fsampleid=b.fid and b.fadid=c.fadid and c.fadid<>0 and left(c.fadclasscode,2) <> "23"
						group by a.fmediaid,n,y';
				}
				
				$do_is = M()->query($sql);
				if(!empty($do_is)){
					$adddata = array_merge($adddata,$do_is);
				}
			}

		}

		$tb_issue = 'tpaperissue';
		$sql = 'select 
			"03" as fmtype,
			a.fmediaid,
			year(a.fissuedate) as n,
			month(a.fissuedate) as y,
			day(last_day(a.fissuedate)) as tjts,
			(count(*)+sum(case when c.fadclasscode="2202" then 1 else 0 end)*'.$number.') as ggsl,
			sum(case when b.fillegaltypecode>0 then 1 else 0 end) as wfggsl,
			sum(case when c.fadclasscode="2202" then 1 else 0 end)*'.$number.' as gyggsl,
			ifnull(sum(case when b.fillegaltypecode>0 then 1 else 0 end)/(count(*)+sum(case when c.fadclasscode="2202" then 1 else 0 end)),0)*'.$number.' as ggslwfl,
			0 as ggscmj,
			0 as wfggscmj,
			0 as gyggscmj,
			0 as ggscmjwfl 
			from 
			(select ttp.fmediaid,fissuedate,fpapersampleid,0 as flength from '.$tb_issue.',tmedia_temp ttp where '.$tb_issue.'.fmediaid=ttp.fmediaid  and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid').' and year(fissuedate)='.$n.' and month(fissuedate)<='.$j.' ) as a,
			tpapersample b, 
			tad c
			where a.fpapersampleid=b.fpapersampleid and b.fadid=c.fadid and c.fadid<>0 and left(c.fadclasscode,2) <> "23"
			group by a.fmediaid,n,y';
		$do_is = M()->query($sql);
		if(!empty($do_is)){
			$adddata = array_merge($adddata,$do_is);
		}

		foreach ($adddata as $key => $value) {
			$adddata[$key]['cswflkf'] 		= intval($adddata[$key]['ggslwfl']/$ruleinfo['frequency']*100)*$ruleinfo['frequency_score'];//次数违法率扣分
			$adddata[$key]['scmjwflkf'] 	= intval($adddata[$key]['ggscmjwfl']/$ruleinfo['longtime']*100)*$ruleinfo['longtime_score'];//时长面积违法率扣分
			$adddata[$key]['wfggslkf'] 		= intval($adddata[$key]['wfggsl']/$ruleinfo['number'])*$ruleinfo['number_score'];//违法广告数量扣分
			$adddata[$key]['gyggsljf'] 		= 0;//公益广告数量加分
			$adddata[$key]['gyggfbljf'] 	= 0;//公益广告发布量加分
			$adddata[$key]['byzkf'] 		= $adddata[$key]['cswflkf']+$adddata[$key]['scmjwflkf']+$adddata[$key]['wfggslkf']-$adddata[$key]['gyggsljf'];//本月总扣分
			$adddata[$key]['syzkf'] 		= 0;//上月总扣分
			$adddata[$key]['byxypjkf'] 		= 0;//本月信用评价扣分
			$adddata[$key]['xypjqcf'] 		= 0;//信用评价期初分
			$adddata[$key]['xypjqmf'] 		= 0;//信用评价期未分
			$adddata[$key]['createtime'] 	= date('Y-m-d H:i:s');//创建时间
			$adddata[$key]['createpersonid'] = session('regulatorpersonInfo.fid');//创建人
		}
		M()->execute('delete from tmedia_credit where fmediaid in (select fmediaid from tmedia_temp where ftype=1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid').') and n='.$n.' and y<='.$j);//删除旧信用数据

		$do_tct = M('tmedia_credit')->addAll($adddata);

		$do_tct2 = M('tmedia_credit')
			->field('a.*,ifnull(b.fvalues,0) as fvalues')
			->alias('a')
			->join('(select fmediaid as mid,fmonth as mm,sum(fvalue) as fvalues from tmedia_credit_bonus where fyear='.$n.' group by fmediaid,fmonth) as b on a.fmediaid=b.mid and a.y=b.mm','left')
			->join('tmedia_temp ttp on a.fmediaid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->where(' n='.$n.' and y<='.$j)
			->order('fmediaid asc,y asc')
			->select();

		foreach ($do_tct2 as $key => $value) {
			if($mediaid != $value['fmediaid']){
				$syzkf = 0;
				$xypjqcf = 100;
			}

			//信用评价扣分
			if(!empty($syzkf)){
				$byxypjkf = ($do_tct2[$key]['byzkf'] - $syzkf)/2;
			}else{
				$byxypjkf = 0;
			}

			$do_tct2[$key]['syzkf'] 	= $syzkf;//上月总扣分
			$do_tct2[$key]['jzf'] 		= $do_tct2[$key]['fvalues'];//本月奖惩分
			$do_tct2[$key]['byzkf']		= $do_tct2[$key]['byzkf'] + $byxypjkf - $do_tct2[$key]['fvalues'];//本月总扣分
			$do_tct2[$key]['byxypjkf'] 	= $byxypjkf;//本月信用评价扣分
			$do_tct2[$key]['xypjqcf'] 	= $xypjqcf;//信用评价期初分
			$do_tct2[$key]['xypjqmf'] 	= $xypjqcf - $do_tct2[$key]['byzkf'];//信用评价期末分

			$syzkf = $do_tct2[$key]['byzkf'];//记录本月总扣分，供下月的上月总扣分使用
			$xypjqcf = $do_tct2[$key]['xypjqmf'];//记录本月期末分，供下月的期初分使用
			$mediaid = $value['fmediaid'];
		}
		$do_tct3 = M('tmedia_credit')->addAll($do_tct2,$options=array(),$replace=true);

		if(!empty($do_tct)){
			D('Function')->write_log('信用数据',1,'更新成功','tmedia_credit',$do_tct,M('tmedia_credit')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'更新成功'));
		}else{
			D('Function')->write_log('信用数据',0,'更新失败','tmedia_credit',0,M('tmedia_credit')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'更新失败'));
		}
	}

	public function getRuleInfo(){
		$regionid = session('regulatorpersonInfo.regionid');//行政区划id

		$region_id_rtrim = substr($regionid, 0,2);
	
		$res = M('f_rule_management')->where('fregionid='.$region_id_rtrim)->find();
		if(!$res){
			$res = M('f_rule_management')->where(array('fregionid'=>'001'))->find();
		}
		return $res;
	}

	/**
	 * 信用奖励记录列表
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * by zw
	 */
	public function credit_bonus_list(){
		session_write_close();
		$system_num = getconfig('system_num');

		$p 			= I('page', 1);//当前第几页
		$pp 		= 20;//每页显示多少记录
		$medianame 	= I('medianame');//媒体名称
		$fitemname 	= I('fitemname');//奖惩原因
		$n 			= I('n');//年
		$y 			= I('y');//月
		
		if(!empty($medianame)){//媒体名称
			$where['tmedia.fmedianame'] = array('like','%'.$medianame.'%');
		}
		if(!empty($fitemname)){//奖惩原因
			$where['tmedia_credit_bonus.fitemname'] = array('like','%'.$fitemname.'%');
		}
		if(!empty($n)){//年份
			$where['fyear'] = $n;
		}
		if(!empty($y)){//月份
			$where['fmonth'] = $y;
		}

		$db_tct = M('tmedia_credit_bonus');
		$count 	= $db_tct->join('tmedia on tmedia_credit_bonus.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')->where($where)->count();//查询满足要求的总记录数
		$Page 	= new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$do_tct = $db_tct
			->field('tmedia_credit_bonus.*, (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame')
			->join('tmedia on tmedia_credit_bonus.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
			->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->where($where)
			->order('tmedia_credit_bonus.fid desc')
			->page($p,$pp)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tct)));
	}

	/**
	 * 添加信用奖励记录
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * by zw
	 */
	public function add_credit_bonus(){

		$mediaid 	= I('mediaid');//媒体id
		$n 			= I('n');//年份
		$y 			= I('y');//月份
		$fitemname	= I('fitemname');//奖惩原因
		$fvalue		= I('fvalue');//奖惩得分，小于零为扣分

		$db_tcb	= M('tmedia_credit_bonus');
		$data_tcb['fmediaid'] 		= $mediaid;
		$data_tcb['fyear'] 			= $n;
		$data_tcb['fmonth'] 		= $y;
		$data_tcb['fitemname'] 		= $fitemname;
		$data_tcb['fvalue'] 		= $fvalue;
		$data_tcb['fcreatetime'] 	= date('Y-m-d H:i:s');
		$data_tcb['fcreatorid'] 	= session('regulatorpersonInfo.fid');
		$data_tcb['fcreatorname'] 	= session('regulatorpersonInfo.fname');
		$do_tcb = $db_tcb->add($data_tcb);
		if(!empty($do_tcb)){
			D('Function')->write_log('信用奖励',1,'添加成功','tmedia_credit_bonus',$do_tcb,M('tmedia_credit_bonus')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
		}else{
			D('Function')->write_log('信用奖励',0,'添加失败','tmedia_credit_bonus',0,M('tmedia_credit_bonus')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'添加失败'));
		}
	}

	/**
	 * 删除信用奖励记录
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * by zw
	 */
	public function del_credit_bonus(){
		
		$fid = I('fid');//媒体信用奖励id
		$db_tcb	= M('tmedia_credit_bonus');
		$do_tcb = $db_tcb->where(['fid'=>$fid])->delete();
		if(!empty($do_tcb)){
			D('Function')->write_log('信用奖励',1,'删除成功','tmedia_credit_bonus',$do_tcb,M('tmedia_credit_bonus')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			D('Function')->write_log('信用奖励',0,'删除失败','tmedia_credit_bonus',0,M('tmedia_credit_bonus')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败'));
		}
	}
}