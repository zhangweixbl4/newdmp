<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;

class TempController extends Controller {

	/**
	 * 初始化
	 */
	public function _initialize(){
		header("Content-type: text/html; charset=utf-8");
	}

	/**
	 * 新增的违法广告非重复记录
	 */
	public function add_illadd(){
		session_write_close();
		set_time_limit(600);
		
        $sqlstr = "SELECT c.media_region_id fregion_id,1 fmedia_class,0 fplatform,c.fid fmedia_id,d.fadclasscode fad_class_code,b.fid fsample_id,d.fadname fad_name,e.fname fadowner, b.fexpressioncodes fillegal_code,b.fexpressions fexpressions,b.fillegalcontent fillegal,b.favifilename favifilename,b.is_long_ad is_long_ad,350000 fcustomer,c.fmediaownerid
            FROM ttvissue_201905_35 a
            INNER JOIN ttvsample b ON a.fsampleid = b.fid
            INNER JOIN tmedia c ON b.fmediaid = c.fid
            INNER JOIN tad d ON b.fadid = d.fadid
            LEFT JOIN tadowner e ON d.fadowner = e.fid
            WHERE b.fillegaltypecode >0 AND b.fid NOT IN(
            SELECT f.fsample_id
            FROM tbn_illegal_ad f,tbn_illegal_ad_issue g
            WHERE f.fid = g.fillegal_ad_id AND g.fissue_date BETWEEN '2019-05-01' AND '2019-05-29' AND f.fmedia_class = 1
            )
            GROUP BY b.fid
        ";
        $do_data = M()->query($sqlstr);
        foreach ($do_data as $key => $value) {
            $adddata = [];
            $adddata['fregion_id'] = $value['fregion_id'];
            $adddata['fmedia_class'] = $value['fmedia_class'];
            $adddata['fplatform'] = $value['fplatform'];
            $adddata['fmedia_id'] = $value['fmedia_id'];
            $adddata['fad_class_code'] = $value['fad_class_code'];
            $adddata['fsample_id'] = $value['fsample_id'];
            $adddata['fad_name'] = $value['fad_name'];
            $adddata['fadowner'] = $value['fadowner'];
            $adddata['fillegal_code'] = $value['fillegal_code'];
            $adddata['fexpressions'] = $value['fexpressions'];
            $adddata['fillegal'] = $value['fillegal'];
            $adddata['favifilename'] = $value['favifilename'];
            $adddata['is_long_ad'] = $value['is_long_ad'];
            $adddata['fcustomer'] = $value['fmediaownerid'];
            $adddata['fcustomer'] = $value['fcustomer'];
            $adddata['create_time'] = date('Y-m-d H:i:s');
            $ids = M('tbn_illegal_ad') -> add($adddata);
            $sqlstr = 'select * from ttvissue_201905_35 where fsampleid = '.$value['fsample_id'];
            $issue = [];
            $allissue = [];
            $issue = M()->query($sqlstr);
            foreach ($issue as $key2 => $value2) {
                $addissue['fillegal_ad_id'] = $ids;
                $addissue['fmedia_id'] = $value['fmedia_id'];
                $addissue['fissue_date'] = date("Y-m-d",$value2['fissuedate']);
                $addissue['fstarttime'] = date("Y-m-d H:i:s",$value2['fstarttime']);
                $addissue['fendtime'] = date("Y-m-d H:i:s",$value2['fendtime']);
                $addissue['fpage'] = '';
                $addissue['ftarget_url'] = '';
                $addissue['fmedia_class'] = 1;
                $addissue['fmediaownerid'] = $value['fmediaownerid'];
                $allissue[] = $addissue;
            }
            M('tbn_illegal_ad_issue') -> addall($allissue);
        }


        $sqlstr = "SELECT c.media_region_id fregion_id,2 fmedia_class,0 fplatform,c.fid fmedia_id,d.fadclasscode fad_class_code,b.fid fsample_id,d.fadname fad_name,e.fname fadowner, b.fexpressioncodes fillegal_code,b.fexpressions fexpressions,b.fillegalcontent fillegal,b.favifilename favifilename,b.is_long_ad is_long_ad,350000 fcustomer,c.fmediaownerid
            FROM tbcissue_201905_35 a
            INNER JOIN ttvsample b ON a.fsampleid = b.fid
            INNER JOIN tmedia c ON b.fmediaid = c.fid
            INNER JOIN tad d ON b.fadid = d.fadid
            LEFT JOIN tadowner e ON d.fadowner = e.fid
            WHERE b.fillegaltypecode >0 AND b.fid NOT IN(
            SELECT f.fsample_id
            FROM tbn_illegal_ad f,tbn_illegal_ad_issue g
            WHERE f.fid = g.fillegal_ad_id AND g.fissue_date BETWEEN '2019-05-01' AND '2019-05-29' AND f.fmedia_class = 2
            )
            GROUP BY b.fid
        ";
        $do_data = M()->query($sqlstr);
        foreach ($do_data as $key => $value) {
            $adddata = [];
            $adddata['fregion_id'] = $value['fregion_id'];
            $adddata['fmedia_class'] = $value['fmedia_class'];
            $adddata['fplatform'] = $value['fplatform'];
            $adddata['fmedia_id'] = $value['fmedia_id'];
            $adddata['fad_class_code'] = $value['fad_class_code'];
            $adddata['fsample_id'] = $value['fsample_id'];
            $adddata['fad_name'] = $value['fad_name'];
            $adddata['fadowner'] = $value['fadowner'];
            $adddata['fillegal_code'] = $value['fillegal_code'];
            $adddata['fexpressions'] = $value['fexpressions'];
            $adddata['fillegal'] = $value['fillegal'];
            $adddata['favifilename'] = $value['favifilename'];
            $adddata['is_long_ad'] = $value['is_long_ad'];
            $adddata['fcustomer'] = $value['fmediaownerid'];
            $adddata['fcustomer'] = $value['fcustomer'];
            $adddata['create_time'] = date('Y-m-d H:i:s');
            $ids = M('tbn_illegal_ad') -> add($adddata);
            $sqlstr = 'select * from tbcissue_201905_35 where fsampleid = '.$value['fsample_id'];
            $issue = [];
            $allissue = [];
            $issue = M()->query($sqlstr);
            foreach ($issue as $key2 => $value2) {
                $addissue['fillegal_ad_id'] = $ids;
                $addissue['fmedia_id'] = $value['fmedia_id'];
                $addissue['fissue_date'] = date("Y-m-d",$value2['fissuedate']);
                $addissue['fstarttime'] = date("Y-m-d H:i:s",$value2['fstarttime']);
                $addissue['fendtime'] = date("Y-m-d H:i:s",$value2['fendtime']);
                $addissue['fpage'] = '';
                $addissue['ftarget_url'] = '';
                $addissue['fmedia_class'] = 2;
                $addissue['fmediaownerid'] = $value['fmediaownerid'];
                $allissue[] = $addissue;
            }
            M('tbn_illegal_ad_issue') -> addall($allissue);
        }
	}

	
}
