<?php
namespace Agp\Controller;
use Think\Controller;
use Agp\Model\StatisticalReportModel;
class OutWordTjController extends BaseController{
    /**
     * 获取历史报告列表
     * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（count-记录总数，list-列表数据）
     * by zw
     */
    public function index() {
        session_write_close();
        header("Content-type:text/html;charset=utf-8");

        $p                  = I('page', 1);//当前第几页
        $pp                 = 20;//每页显示多少记录
        $pnname             = I('pnname');// 报告名称
        $pntype             = I('pntype');// 报告类型
        $pnfiletype         = I('pnfiletype');// 文件类型
        $pncreatetime       = I('pnendtime');//报告时间
        $system_num = getconfig('system_num');

        if (!empty($pnname)) {
            $where['pnname'] = array('like', '%' . $pnname . '%');//报告名称
        }
        if (!empty($pntype)) {
            $where['pntype'] = $pntype;//报告类型
        }else{
            $where['pntype'] = array('in',array(10,20,30,70,75,80));//报告类型
        }
        if (!empty($pnfiletype)) {
            $where['pnfiletype'] = $pnfiletype;//文件类型
        }
        if(!empty($pncreatetime)){
            $where['pnendtime'] = array('between',array($pncreatetime[0],$pncreatetime[1]));
        }
        $where['pntrepid']   = session('regulatorpersonInfo.fregulatorpid');
        $where['fcustomer']  = $system_num;

        $count = M('tpresentation')
            ->where($where)
            ->count();

        
        $data = M('tpresentation')
            ->where($where)
            ->order('pnendtime desc')
            ->page($p,$pp)
            ->select();
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
    }

    //报告删除接口
    public function tpresentation_delete() {

        $pnid         = I('pnid');//报告ID
        $user = session('regulatorpersonInfo');//获取用户信息
        $system_num = getconfig('system_num');

        $where['pnid'] = $pnid;
        $where['pntrepid'] = $user['fregulatorpid'];
        $where['fcustomer']  = $system_num;

        $data = M('tpresentation')->where($where)->delete();

        if($data > 0){
            $this->ajaxReturn(array('code'=>0,'msg'=>'删除成功！'));
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'您无权删除此报告！'));
        }
    }

    //日报
    public function create_tjday(){
        $day_time = I('daytime',date('Y-m-d',time()));//开始日期
        //$day_time = '2018-10-12';//开始日期
        $media_type = I('mclass',0);//媒体类别
        if($media_type == 0){
            $media_type = '';
        }

        switch ($media_type){
            case '01':
                $r_name ='电视媒体';
                break;
            case '02':
                $r_name ='广播媒体';
                break;
            case '03':
                $r_name ='报纸媒体';
                break;
            case '13':
                $r_name ='互联网媒体';
                break;
            case '':
                $r_name ='全部媒体';
                break;
        }

        $user = session('regulatorpersonInfo');//获取用户信息
        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型
        $medias = get_owner_media_ids();
        $date_table = date('Ym',strtotime($day_time)).'_'.substr($user['regionid'],0,2);//组合表
        $data_list = $StatisticalReport->report_statistics_media($medias,$media_type,$date_table,$day_time);
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号居中正文",
            "text" => $user['regulatorname'].date('Y年m月d日',strtotime($day_time)).$r_name.'广告发布情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "日报表格zb",
            "data" => $data_list
        ];
        $report_data = json_encode($data);
        //echo $report_data;exit;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $system_num = getconfig('system_num');

//将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].$r_name.date('Y年m月d日',strtotime($day_time)).'日报';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 10;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= $day_time;
            $data['pnendtime'] 		    = $day_time;
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }
    }

    //周报
    public function create_week(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型
        $s_time = I('s_time','2018-07-16');//开始日期
        $e_time = I('e_time','2018-07-21');//结束日期
        $month_s = date('m',strtotime($s_time));
        $month_e = date('m',strtotime($e_time));
        $week_num = intval(date('W',strtotime($s_time)));

        if($month_s != $month_e){
            $days_1 = date('t', strtotime($year."-1-1"));

            $s_time_e = substr($s_time,0,8).date('t', strtotime($s_time));//上月结束时间
            $e_time_s = substr($e_time,0,8).'01';//下月开始时间

            $cycle_arr = [
                [$s_time,$s_time_e],
                [$e_time_s,$e_time]
            ];
        }else{
            $cycle_arr = [
                [$s_time,$e_time]
            ];
        }
        $user = session('regulatorpersonInfo');//获取用户信息

        $tregion_count = M('tregion')->where(['fpid'=>$user['regionid']])->count();

        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级
        if($re_level == 2 || $re_level == 3 || $re_level == 4){
            $dq_name = '市级';
            $r_name = '全市';
            $xj_name = '区县';
        }elseif($re_level == 1){
            $dq_name = '全市';
            $xj_name = '区县级';
        }else{
            $dq_name = '全'.$user['regionname1'];
            $xj_name = '地方';
        }

        $date_str = date('Y年m月d日',strtotime($s_time)).'至'.date('Y年m月d日',strtotime($e_time)).'(第'.$week_num.'周)';//开始年月字符

        //$days = round((strtotime($e_time) - strtotime($s_time) + 1)/86400);//计算天数
        $owner_media_ids = get_owner_media_ids();
        $ct_jczqk_quarter = [];
        $every_region_jczqk_quarter = [];
        $hz_tv_illegal_ad_data_quarter = [];
        $every_region_jczqk_quarter = [];//各地域监测总情况
        $every_ad_class_jczqk_quarter = [];//各广告类别监测总情况
        //本级电视
        $hz_tv_illegal_ad_data_quarter = [];
        //本级广播
        $hz_tb_illegal_ad_data_quarter = [];
        //本级报纸
        $hz_tp_illegal_ad_data_quarter = [];
        //下级电视
        $xj_tv_illegal_ad_data_quarter = [];
        //下级广播
        $xj_tb_illegal_ad_data_quarter = [];
        //下级报纸
        $xj_tp_illegal_ad_data_quarter = [];
        $hz_zdclass_tv_illegal_ad_data_quarter = [];
        $hz_zdclass_tb_illegal_ad_data_quarter = [];
        $hz_zdclass_tp_illegal_ad_data_quarter = [];
        $ct_media_tv_count_hz = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局电视媒体数量

        $ct_media_bc_count_hz = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局广播媒体数量

        $ct_media_paper_count_hz = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局报纸媒体数量

        $ct_media_tv_count_xj = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县电视媒体数量

        $ct_media_bc_count_xj = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县广播媒体数量

        $ct_media_paper_count_xj = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县报纸媒体数量


        $ct_media_count = $ct_media_tv_count_hz + $ct_media_bc_count_hz + $ct_media_paper_count_hz + $ct_media_tv_count_xj + $ct_media_bc_count_xj + $ct_media_paper_count_xj;//全部有数据的媒体数量
        foreach ($cycle_arr as $cycle_arr_val){
            $date_table = date('Ym',strtotime($cycle_arr_val[0])).'_'.substr($user['regionid'],0,2);//组合表


            if(!table_exist("ttvissue_".$date_table) || !table_exist("tbcissue_".$date_table)){
                continue;
            }
            $report_start_date = strtotime($cycle_arr_val[0]);
            $report_end_date = strtotime($cycle_arr_val[1])+86399;
            /*
            * @各媒体监测总情况
            *
            **/

            $ct_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_count_rate','fmediaclass',false,1,get_check_dates());//传统媒体监测总情况


            /*
* @各地域监测总情况
*
**/
            $every_region_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times','fregionid',false,1,get_check_dates());//各地域监测总情况


            /*
* @各广告类型监测总情况
*
**/
            $every_ad_class_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_times','fcode',false,1,get_check_dates());//各地域监测总情况

            //本级电视
            $hz_tv_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',0);

            //本级广播
            $hz_tb_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',0);

            //本级报纸
            $hz_tp_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',0);

            //下级电视
            $xj_tv_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',2);

            //下级广播
            $xj_tb_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',2);

            //下级报纸
            $xj_tp_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',2);


            //四大类 药品、医疗服务、医疗器械、保健食品
            $hz_zdclass_tv_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,['01','02','13','06'],1);
            $hz_zdclass_tb_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,['01','02','13','06'],1);
            $hz_zdclass_tp_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,['01','02','13','06'],1);

        }

        $ct_jczqk = $this->create_data_hzlist($ct_jczqk_quarter,'fclass');
        $every_region_jczqk = $this->create_data_hzlist($every_region_jczqk_quarter,'fregionid');
        $every_ad_class_jczqk = $this->create_data_hzlist($every_ad_class_jczqk_quarter,'fcode');
        $hz_tv_illegal_ad_data = $this->create_data_hzlist2($hz_tv_illegal_ad_data_quarter,'fad_name');
        $hz_tb_illegal_ad_data = $this->create_data_hzlist2($hz_tb_illegal_ad_data_quarter,'fad_name');
        $hz_tp_illegal_ad_data = $this->create_data_hzlist2($hz_tp_illegal_ad_data_quarter,'fad_name');
        $xj_tv_illegal_ad_data = $this->create_data_hzlist2($xj_tv_illegal_ad_data_quarter,'fad_name');
        $xj_tb_illegal_ad_data = $this->create_data_hzlist2($xj_tb_illegal_ad_data_quarter,'fad_name');
        $xj_tp_illegal_ad_data = $this->create_data_hzlist2($xj_tp_illegal_ad_data_quarter,'fad_name');

        $hz_zdclass_tv_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tv_illegal_ad_data_quarter,'fad_name');
        $hz_zdclass_tb_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tb_illegal_ad_data_quarter,'fad_name');
        $hz_zdclass_tp_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tp_illegal_ad_data_quarter,'fad_name');

        //本级电视
        $hz_tv_illegal_ad_data = $this->get_table_data($hz_tv_illegal_ad_data);
        //本级广播
        $hz_tb_illegal_ad_data = $this->get_table_data($hz_tb_illegal_ad_data);
        //本级报纸
        $hz_tp_illegal_ad_data = $this->get_table_data($hz_tp_illegal_ad_data);
        //下级电视
        $xj_tv_illegal_ad_data = $this->get_table_data($xj_tv_illegal_ad_data);
        //下级广播
        $xj_tb_illegal_ad_data = $this->get_table_data($xj_tb_illegal_ad_data);
        //下级报纸
        $xj_tp_illegal_ad_data = $this->get_table_data($xj_tp_illegal_ad_data);

        /*
        * @各媒体监测总情况
        *
        **/
        $ct_fad_times = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times = 0;//定义传统媒体发布违法广告条次
        $every_media_class_data = [];//定义媒体发布监测情况数组
//循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk as $ct_jczqk_val){
            $ct_fad_times += $ct_jczqk_val['fad_times'];
            $ct_fad_illegal_times += $ct_jczqk_val['fad_illegal_times'];
            $ct_fad_count += $ct_jczqk_val['fad_count'];
            $ct_fad_illegal_count += $ct_jczqk_val['fad_illegal_count'];
            $ct_data_val = [
                $ct_jczqk_val['tmediaclass_name'],
                $ct_jczqk_val['fad_times'],
                $ct_jczqk_val['fad_count'],
                $ct_jczqk_val['fad_illegal_times'],
                $ct_jczqk_val['fad_illegal_count'],
                $ct_jczqk_val['fad_illegal_times_rate'].'%',
                $ct_jczqk_val['fad_illegal_count_rate'].'%'
            ];
            if($ct_jczqk_val['fmedia_class_code'] == '01'){
                $every_media_class_data[0] = $ct_data_val;
            }elseif($ct_jczqk_val['fmedia_class_code'] == '02'){
                $every_media_class_data[1] = $ct_data_val;
            }else{
                $every_media_class_data[2] = $ct_data_val;
            }
        }

        $every_media_class_data[3] = [
            '合计',
            strval($ct_fad_times),
            strval($ct_fad_count),
            strval($ct_fad_illegal_times),
            strval($ct_fad_illegal_count),
            round((($ct_fad_illegal_times)/($ct_fad_times))*100,2).'%',
            round((($ct_fad_illegal_count)/($ct_fad_count))*100,2).'%'
        ];//二、各类媒体广告发布情况
        $every_media_class_data = array_values($every_media_class_data);
        /*
* @各地域监测总情况
*
**/
        $every_region_data = [];//定义各地区发布监测情况数组
//循环赋值
        $every_region_illegal_times_data = [
            [''],
            ['']
        ];
        $every_region_jczqk = pxsf($every_region_jczqk,'fad_illegal_times');
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){
            $every_region_fad_times += $every_region_jczqk_val['fad_times'];
            $every_region_fad_illegal_times += $every_region_jczqk_val['fad_illegal_times'];
            $every_region_fad_count += $every_region_jczqk_val['fad_count'];
            $every_region_fad_illegal_count += $every_region_jczqk_val['fad_illegal_count'];
            if($every_region_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_region_illegal_times_data[0],$every_region_jczqk_val['tregion_name']);
                array_push($every_region_illegal_times_data[1],strval($every_region_jczqk_val['fad_illegal_times']));
            }

            $every_region_data[] = [
                strval($every_region_jczqk_key+1),
                $every_region_jczqk_val['tregion_name'],
                $every_region_jczqk_val['fad_times'],
                $every_region_jczqk_val['fad_count'],
                $every_region_jczqk_val['fad_illegal_times'],
                $every_region_jczqk_val['fad_illegal_count'],
                $every_region_jczqk_val['fad_illegal_times_rate'].'%',
                $every_region_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_region_data[] = [
            '',
            '合计',
            strval($every_region_fad_times),
            strval($every_region_fad_count),
            strval($every_region_fad_illegal_times),
            strval($every_region_fad_illegal_count),
            round((($every_region_fad_illegal_times)/($every_region_fad_times))*100,2).'%',
            round((($every_region_fad_illegal_count)/($every_region_fad_count))*100,2).'%'
        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）



        /*
* @各地域电视广播报纸监测总情况
*
**/
        $every_media_all_data = [];
        $every_media_illegal_times_all_data = [];
        foreach (['01','02','03'] as $every_media_val){
            $every_media_data = [];
            $every_media_illegal_times_data = [];
            $every_media_jczqk = [];
            //循环赋值
            $every_media_fad_times = 0;
            $every_media_fad_illegal_times = 0;
            $every_media_fad_count = 0;
            $every_media_fad_illegal_count = 0;
            $every_media_illegal_times_data = [
                [''],
                ['']
            ];
            $every_media_jczqk_quarter = [];
            foreach ($cycle_arr as $cycle_arr_val){
                $date_table = date('Y',strtotime($cycle_arr_val[0])).'_'.$user['regionid'];//组合表
                if(!table_exist("ttvissue_".$date_table) || !table_exist("tbcissue_".$date_table)){
                    continue;
                }
                $report_start_date = strtotime($cycle_arr_val[0]);
                $report_end_date = strtotime($cycle_arr_val[1])+86399;
                $every_media_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,$every_media_val,'','fad_illegal_times','fmediaid',false,1,get_check_dates());//各地域监测总情况

            }
            $every_media_jczqk = $this->create_data_hzlist($every_media_jczqk_quarter,'fid');//fad_illegal_times
            $every_media_jczqk = pxsf($every_media_jczqk,'fad_illegal_times');//重新排序
            foreach ($every_media_jczqk as $every_media_jczqk_key => $every_media_jczqk_val){
                $every_media_fad_times += $every_media_jczqk_val['fad_times'];
                $every_media_fad_illegal_times += $every_media_jczqk_val['fad_illegal_times'];
                $every_media_fad_count += $every_media_jczqk_val['fad_count'];
                $every_media_fad_illegal_count += $every_media_jczqk_val['fad_illegal_count'];

                if($every_media_jczqk_val['fad_illegal_times'] > 0){
                    array_push($every_media_illegal_times_data[0],$every_media_jczqk_val['tmedia_name']);
                    array_push($every_media_illegal_times_data[1],strval($every_media_jczqk_val['fad_illegal_times']));
                }

                $every_media_data[] = [
                    strval($every_media_jczqk_key+1),
                    $every_media_jczqk_val['tregion_name'],
                    $every_media_jczqk_val['tmedia_name'],
                    $every_media_jczqk_val['fad_times'],
                    $every_media_jczqk_val['fad_count'],
                    $every_media_jczqk_val['fad_illegal_times'],
                    $every_media_jczqk_val['fad_illegal_count'],
                    $every_media_jczqk_val['fad_illegal_times_rate'].'%',
                    $every_media_jczqk_val['fad_illegal_count_rate'].'%'
                ];
            }
            /*            $every_media_data[] = [
                            '',
                            '合计',
                            strval($every_media_fad_times),
                            strval($every_media_fad_count),
                            strval($every_media_fad_illegal_times),
                            strval($every_media_fad_illegal_count),
                            round((($every_media_fad_illegal_times)/($every_media_fad_times))*100,2).'%',
                            round((($every_media_fad_illegal_count)/($every_media_fad_count))*100,2).'%'
                        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）*/
            $every_media_all_data[] = $every_media_data;
            $every_media_illegal_times_all_data[] = $every_media_illegal_times_data;
        }


        $every_media_tv_data = $every_media_all_data[0];//电视各媒体
        $every_media_tb_data = $every_media_all_data[1];//广播各媒体
        $every_media_tp_data = $every_media_all_data[2];//报纸各媒体
        $every_media_illegal_times_tv_data = $every_media_illegal_times_all_data[0];//电视各媒体
        $every_media_illegal_times_tb_data = $every_media_illegal_times_all_data[1];//广播各媒体
        $every_media_illegal_times_tp_data =$every_media_illegal_times_all_data[2];//报纸各媒体





        /*
* @各广告类型监测总情况
*
**/

        $every_ad_class_data = [];//定义各地区发布监测情况数组
//循环赋值
        $every_ad_class_illegal_times_data = [
            [''],
            ['']
        ];
        $every_ad_class_jczqk = pxsf($every_ad_class_jczqk,'fad_illegal_times');//重新排序
        foreach ($every_ad_class_jczqk as $every_ad_class_jczqk_key => $every_ad_class_jczqk_val){
            $every_ad_class_fad_times += $every_ad_class_jczqk_val['fad_times'];
            $every_ad_class_fad_illegal_times += $every_ad_class_jczqk_val['fad_illegal_times'];
            $every_ad_class_fad_count += $every_ad_class_jczqk_val['fad_count'];
            $every_ad_class_fad_illegal_count += $every_ad_class_jczqk_val['fad_illegal_count'];
            if($every_ad_class_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_ad_class_illegal_times_data[0],$every_ad_class_jczqk_val['tadclass_name']);
                array_push($every_ad_class_illegal_times_data[1],strval($every_ad_class_jczqk_val['fad_illegal_times']));
            }

            $every_ad_class_data[] = [
                strval($every_ad_class_jczqk_key+1),
                $every_ad_class_jczqk_val['tadclass_name'],
                $every_ad_class_jczqk_val['fad_times'],
                $every_ad_class_jczqk_val['fad_count'],
                $every_ad_class_jczqk_val['fad_illegal_times'],
                $every_ad_class_jczqk_val['fad_illegal_count'],
                $every_ad_class_jczqk_val['fad_illegal_times_rate'].'%',
                $every_ad_class_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_ad_class_data[] = [
            '',
            '合计',
            strval($every_ad_class_fad_times),
            strval($every_ad_class_fad_count),
            strval($every_ad_class_fad_illegal_times),
            strval($every_ad_class_fad_illegal_count),
            round((($every_ad_class_fad_illegal_times)/($every_ad_class_fad_times))*100,2).'%',
            round((($every_ad_class_fad_illegal_count)/($every_ad_class_fad_count))*100,2).'%'
        ];


        $typical_illegal_ad_list_data = [];
        if(!empty($hz_zdclass_tv_illegal_ad_data) || !empty($hz_zdclass_tb_illegal_ad_data) || !empty($hz_zdclass_tp_illegal_ad_data)){
            $typical_illegal_ad_list = array_merge($hz_zdclass_tv_illegal_ad_data,$hz_zdclass_tb_illegal_ad_data,$hz_zdclass_tp_illegal_ad_data);
            $illegal_ad_list_data = [];
            $typical_illegal_ad_list = pxsf($typical_illegal_ad_list,'illegal_times');//排序
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_val){
                $md5 = MD5($typical_illegal_ad_list_val['fad_name'].$typical_illegal_ad_list_val['fmedianame']);
                if(isset($illegal_ad_list_data[$md5])){
                    $illegal_ad_list_data[$md5]['illegal_times'] += $typical_illegal_ad_list_val['illegal_times'];
                    $illegal_ad_list_data[$md5]['illegal_times'] = strval($illegal_ad_list_data[$md5]['illegal_times']);
                }else{

                    $illegal_ad_list_data[$md5] = $typical_illegal_ad_list_val;
                }
            }

            $typical_illegal_ad_list = array_values($illegal_ad_list_data);
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_key => $typical_illegal_ad_list_val){
                $typical_illegal_ad_list_data[] = [
                    strval($typical_illegal_ad_list_key+1),
                    $typical_illegal_ad_list_val['fmedianame'],
                    $typical_illegal_ad_list_val['fad_name'],
                    $typical_illegal_ad_list_val['fadclass'],
                    $typical_illegal_ad_list_val['illegal_times']
                ];
            }
        }



//定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "大标题楷体60",
            "text" => "广告监测周报"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号加粗",
            "text" => $user['regulatorname']
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号加粗带红线",
            "text" => "广告监测中心               ".date('Y年m月d日')
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $user['regulatorname']."广告监测中心对".$date_str.$r_name."部分电视、广播、报刊等".$ct_media_count."家媒体发布广告的情况进行了抽查监测，现将监测情况通报如下："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "一、监测范围及对象"
        ];
        if($ct_media_tv_count_hz != 0){
            $hz_media_count_str .= $ct_media_tv_count_hz."个电视媒体；";
        }
        if($ct_media_bc_count_hz != 0){
            $hz_media_count_str .= $ct_media_bc_count_hz."个广播媒体；";
        }
        if($ct_media_paper_count_hz != 0){
            $hz_media_count_str .= $ct_media_paper_count_hz."个报纸媒体；";
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）".$dq_name."媒体：".$hz_media_count_str
        ];
        if($ct_media_tv_count_xj != 0){
            $xj_media_count_str .= $ct_media_tv_count_xj."个电视媒体；";
        }
        if($ct_media_bc_count_xj != 0){
            $xj_media_count_str .= $ct_media_bc_count_xj."个广播媒体；";
        }
        if($ct_media_paper_count_xj != 0){
            $xj_media_count_str .= $ct_media_paper_count_xj."个报纸媒体；";
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）其他".$tregion_count."个区、县：" . (empty($xj_media_count_str) ? '待添加新媒体': $xj_media_count_str)
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "二、各类媒体广告发布情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "本周共监测广告".$every_media_class_data[3][1]."条次，".$every_media_class_data[3][2]."条数，其中监测发现各类涉嫌违法广告".$every_media_class_data[3][3]."条次，".$every_media_class_data[3][4]."条数。具体情况详见下表："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒介类型汇总表格hz",
            "data" => $every_media_class_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "三、市（各区县）媒体广告发布总体情况"
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "地域排名汇总表格hz",
            "data" => $every_region_data
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各市区县违法广告发布量排名条形图hz",
            "data" => $every_region_illegal_times_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "四、全市电视、广播、报纸广告发布情况（排名按发布违法广告条次由高到底排列）"
        ];

        if(!empty($every_media_tv_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "1、电视"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tv_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tv_data
            ];
        }
        if(!empty($every_media_tb_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "2、广播"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tb_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tb_data
            ];
        }
        if(!empty($every_media_tp_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "3、报纸"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tp_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tp_data
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、全市各类媒体发布的违法广告（全部类别）通报"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）市级媒体"
        ];


        if(!empty($hz_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tv_illegal_ad_data)
            ];
        }
        if(!empty($hz_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tb_illegal_ad_data)
            ];
        }
        if(!empty($hz_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tp_illegal_ad_data)
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）区（县）级媒体"
        ];

        if(!empty($xj_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tv_illegal_ad_data)
            ];
        }
        if(!empty($xj_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tb_illegal_ad_data)
            ];
        }

        if(!empty($xj_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tp_illegal_ad_data)
            ];
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "六、全市各类违法广告比重结构图"
        ];
        if(!empty($every_ad_class_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表hz",
                "data" => $every_ad_class_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "各大类别广告违法占比表hz",
                "data" => $every_ad_class_illegal_times_data
            ];

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "七、药品、医疗服务、医疗器械、保健食品违法广告发布情况"
        ];

        if(!empty($typical_illegal_ad_list_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $typical_illegal_ad_list_data
            ];
        }

        $report_data = json_encode($data);
        //echo $report_data;exit;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $system_num = getconfig('system_num');

//将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].$date_str.'报告';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 20;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',strtotime($s_time));
            $data['pnendtime'] 		    = date('Y-m-d',strtotime($e_time));
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }

    //月报
    public function create_month(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型
        //$month = I('daytime','2018-07');//接收时间  TODO::时间改
        $month = I('daytime',date('Y-m',time()));//接收时间  TODO::时间改
        $month = I('daytime','2018-07');//接收时间  TODO::时间改
        $report_start_date = strtotime($month);//指定月份月初时间戳
        $report_end_date = mktime(23, 59, 59, date('m', strtotime($month))+1, 00);//指定月份月末时间戳
        if($report_end_date > time()){
            $report_end_date = time();

        }
        $date_ym = date('Y年m月',$report_start_date);//年月字符

        $user = session('regulatorpersonInfo');//获取用户信息
        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级
        if($re_level == 2 || $re_level == 3 || $re_level == 4){
            $dq_name = '全市';
            $xj_name = '区县';
        }elseif($re_level == 1){
            $dq_name = '全市';
            $xj_name = '区县级';
        }else{
            $dq_name = '全'.$user['regionname1'];
            $xj_name = '地方';
        }
        $date_table = date('Ym',strtotime($month)).'_'.substr($user['regionid'],0,2);//组合表

        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数
        $owner_media_ids = get_owner_media_ids();
        $ct_media_tv_count = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_end_date);//电视媒体数量

        $ct_media_bc_count = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date);//广播媒体数量

        $ct_media_paper_count = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date);//报纸媒体数量

        $ct_media_count = $ct_media_tv_count + $ct_media_bc_count + $ct_media_paper_count;


/*        $pc_media_count = $StatisticalReport->net_media_counts(trim($user['regionid'],'0'),'1301');//pc媒体数量
        $app_media_count = $StatisticalReport->net_media_counts(trim($user['regionid'],'0'),'1302');//APP媒体数量
        $gzh_media_count = $StatisticalReport->net_media_counts(trim($user['regionid'],'0'),'1303');//公众号数量
        $pc_illegal_situation = $StatisticalReport->net_illegal_situation_customize($owner_media_ids,['net_platform',1],$report_start_date,$report_end_date);//pc违法情形列表*/

        /*
        * @各媒体监测总情况
        *
        **/
        //$net_media_customize = $StatisticalReport->net_media_customize_sum($owner_media_ids,false,$report_start_date,$report_end_date);//互联网监测总情况
        //$net_gzh_customize = $StatisticalReport->net_media_customize_sum($owner_media_ids,['source_type',4],$report_start_date,$report_end_date);//微信广告数量
        //$net_pc_customize = $StatisticalReport->net_media_customize_sum($owner_media_ids,['net_platform',1],$report_start_date,$report_end_date);//pc广告数量
        $ct_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_count_rate','fmediaclass',false,1,get_check_dates());//传统媒体监测总情况
        $ct_fad_times = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times = 0;//定义传统媒体发布违法广告条次
        $every_media_data = [];//定义媒体发布监测情况数组
//循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk as $ct_jczqk_val){
            $ct_fad_times += $ct_jczqk_val['fad_times'];
            $ct_fad_illegal_times += $ct_jczqk_val['fad_illegal_times'];
            $ct_fad_play_len += $ct_jczqk_val['fad_play_len'];
            $ct_fad_illegal_play_len += $ct_jczqk_val['fad_illegal_play_len'];
            $ct_data_val = [
                $ct_jczqk_val['tmediaclass_name'],
                $ct_jczqk_val['fad_times'],
                $ct_jczqk_val['fad_illegal_times'],
                $ct_jczqk_val['fad_illegal_times_rate'].'%',
                $ct_jczqk_val['fad_play_len'],
                $ct_jczqk_val['fad_illegal_play_len'],
                $ct_jczqk_val['fad_illegal_play_len_rate']?$ct_jczqk_val['fad_illegal_play_len_rate'].'%':'0.00'.'%'
            ];
            if($ct_jczqk_val['fmedia_class_code'] == '01'){
                $every_media_data[0] = $ct_data_val;
            }elseif($ct_jczqk_val['fmedia_class_code'] == '02'){
                $every_media_data[1] = $ct_data_val;
            }else{
                $every_media_data[2] = $ct_data_val;
            }
        }
/*        $every_media_data[3] = [
            '互联网',
            $net_media_customize['total'],
            $net_media_customize['wfggsl'],
            $net_media_customize['ggslwfl'].'%',
            '0',
            '0',
            '0.00%'
        ];*/
        $every_media_data[3] = [
            '合计',
            strval($ct_fad_times+$net_media_customize['total']),
            strval($ct_fad_illegal_times+$net_media_customize['wfggsl']),
            round((($ct_fad_illegal_times+$net_media_customize['wfggsl'])/($ct_fad_times+$net_media_customize['total']))*100,2).'%',
            strval($ct_fad_play_len),
            strval($ct_fad_illegal_play_len),
            round(($ct_fad_illegal_play_len/$ct_fad_play_len)*100,2).'%'
        ];

        /*
        * @各地域监测总情况
        *
        **/
        $every_region_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times_rate','fregionid',false,1,get_check_dates());//各地域监测总情况

        $every_region_data = [];//定义各地区发布监测情况数组
        $every_region_num = 0;
//循环赋值
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){
            if($every_region_jczqk_val['fad_illegal_times'] != 0){
                $every_region_num++;
                $every_region_name[] = $every_region_jczqk_val['tregion_name'];
            }
            $every_region_data[] = [
                strval($every_region_jczqk_key+1),
                $every_region_jczqk_val['tregion_name'],
                $every_region_jczqk_val['fad_times'],
                $every_region_jczqk_val['fad_illegal_times'],
                $every_region_jczqk_val['fad_illegal_times_rate'].'%',
                $every_region_jczqk_val['fad_play_len'],
                $every_region_jczqk_val['fad_illegal_play_len'],
                $every_region_jczqk_val['fad_illegal_play_len_rate']?$every_region_jczqk_val['fad_illegal_play_len_rate'].'%':'0.00'.'%'
            ];
        }

        /*
        * @各广告类型监测总情况
        *
        **/
        $every_adclass_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times_rate','fcode',false,1,get_check_dates());//各广告类型监测总情况
        $every_adclass_data = [];//定义各大类发布监测情况数组
        $adclass_illegal_times = 0;//各违法地域数量
//循环赋值
        foreach ($every_adclass_jczqk as $every_adclass_jczqk_key => $every_adclass_jczqk_val){
            if($every_adclass_jczqk_val['fad_illegal_times'] > 0){
                $adclass_illegal_times += $every_adclass_jczqk_val['fad_illegal_times'];
            }
            $every_adclass_data[] = [strval($every_adclass_jczqk_key+1),$every_adclass_jczqk_val['tadclass_name'],$every_adclass_jczqk_val['fad_times'],$every_adclass_jczqk_val['fad_illegal_times'],$every_adclass_jczqk_val['fad_illegal_times_rate'].'%'];
        }

        foreach ($every_adclass_data as $every_adclass_data_key=>$every_adclass_data_val){
            if($every_adclass_data_val[3] > 0){
                $every_adclass_name[] = $every_adclass_data_val[1];
                if($every_adclass_data_key == 0){
                    $every_adclass_rate_des[] = $every_adclass_data_val[1].'广告占涉嫌违法广告总量的'.round(($every_adclass_data_val[3]/$adclass_illegal_times)*100,2).'％';
                }else{
                    $every_adclass_rate_des[] .= $every_adclass_data_val[1].'广告占'.round(($every_adclass_data_val[3]/$adclass_illegal_times)*100,2).'％';
                }
            }
        }

        /*
        * @电视各地域监测总情况
        *
        **/
        $every_tv_region_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'01','','fad_illegal_times_rate','fregionid',false,1,get_check_dates());//电视各地域监测总情况
        $every_tv_region_data = [];//定义各地区发布监测情况数组
        $every_tv_region_num = 0;//电视各违法地域数量
//循环赋值
        foreach ($every_tv_region_jczqk as $every_tv_region_jczqk_key => $every_tv_region_jczqk_val){
            if($every_tv_region_jczqk_val['fad_illegal_times'] != 0){
                $every_tv_region_num++;
                $every_tv_region_name[] = $every_tv_region_jczqk_val['tregion_name'];
            }
            $every_tv_region_data[] = [
                strval($every_tv_region_jczqk_key+1),
                $every_tv_region_jczqk_val['tregion_name'],
                $every_tv_region_jczqk_val['fad_times'],
                $every_tv_region_jczqk_val['fad_illegal_times'],
                $every_tv_region_jczqk_val['fad_illegal_times_rate'].'%',
                $every_tv_region_jczqk_val['fad_play_len'],
                $every_tv_region_jczqk_val['fad_illegal_play_len'],
                $every_tv_region_jczqk_val['fad_illegal_play_len_rate'].'%'
            ];
        }


        /*
        * @电视各广告类型监测总情况
        *
        **/
        $every_tv_adclass_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'01','','fad_illegal_times_rate','fcode',false,1,get_check_dates());//电视各广告类型监测总情况
        $every_tv_adclass_data = [];//定义各地区发布监测情况数组
//循环赋值
        foreach ($every_tv_adclass_jczqk as $every_tv_adclass_jczqk_key => $every_tv_adclass_jczqk_val){
            /*            if($every_tv_adclass_jczqk_val['fad_illegal_times'] != 0){
            $every_tv_adclass_num++;
            $every_tv_adclass_name .= $every_tv_region_jczqk_val['tregion_name'].'、';
            }*/
            $every_tv_adclass_data[] = [
                strval($every_tv_adclass_jczqk_key+1),
                $every_tv_adclass_jczqk_val['tadclass_name'],
                $every_tv_adclass_jczqk_val['fad_times'],
                $every_tv_adclass_jczqk_val['fad_illegal_times'],
                $every_tv_adclass_jczqk_val['fad_illegal_times_rate'].'%',
                $every_tv_adclass_jczqk_val['fad_play_len'],
                $every_tv_adclass_jczqk_val['fad_illegal_play_len'],
                $every_tv_adclass_jczqk_val['fad_illegal_play_len_rate'].'%'
            ];
        }


        /*
        * @广播各地域监测总情况
        *
        **/
        $every_tb_region_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'02','','fad_illegal_times_rate','fregionid',false,1,get_check_dates());//广播各地域监测总情况
        $every_tb_region_data = [];//定义各地区发布监测情况数组
        $every_tb_region_num = 0;//广播各违法地域数量
//循环赋值
        foreach ($every_tb_region_jczqk as $every_tb_region_jczqk_key => $every_tb_region_jczqk_val){
            if($every_tb_region_jczqk_val['fad_illegal_times'] != 0){
                $every_tb_region_num++;
                $every_tb_region_name[] = $every_tb_region_jczqk_val['tregion_name'];
            }
            $every_tb_region_data[] = [
                strval($every_tb_region_jczqk_key+1),
                $every_tb_region_jczqk_val['tregion_name'],
                $every_tb_region_jczqk_val['fad_times'],
                $every_tb_region_jczqk_val['fad_illegal_times'],
                $every_tb_region_jczqk_val['fad_illegal_times_rate'].'%',
                $every_tb_region_jczqk_val['fad_play_len'],
                $every_tb_region_jczqk_val['fad_illegal_play_len'],
                $every_tb_region_jczqk_val['fad_illegal_play_len_rate'].'%'
            ];
        }


        /*
        * @广播各广告类型监测总情况
        *
        **/
        $every_tb_adclass_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'02','','fad_illegal_times_rate','fcode',false,1,get_check_dates());//广播各广告类型监测总情况
        $every_tb_adclass_data = [];//定义各地区发布监测情况数组
//循环赋值
        foreach ($every_tb_adclass_jczqk as $every_tb_adclass_jczqk_key => $every_tb_adclass_jczqk_val){
            /*            if($every_tb_adclass_jczqk_val['fad_illegal_times'] != 0){
            $every_tb_adclass_num++;
            $every_tb_adclass_name .= $every_tb_region_jczqk_val['tregion_name'].'、';
            }*/
            $every_tb_adclass_data[] = [
                strval($every_tb_adclass_jczqk_key+1),
                $every_tb_adclass_jczqk_val['tadclass_name'],
                $every_tb_adclass_jczqk_val['fad_times'],
                $every_tb_adclass_jczqk_val['fad_illegal_times'],
                $every_tb_adclass_jczqk_val['fad_illegal_times_rate'].'%',
                $every_tb_adclass_jczqk_val['fad_play_len'],
                $every_tb_adclass_jczqk_val['fad_illegal_play_len'],
                $every_tb_adclass_jczqk_val['fad_illegal_play_len_rate'].'%'
            ];
        }


        /*
        * @报纸各地域监测总情况
        *
        **/
        $every_tp_region_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'03','','fad_illegal_times_rate','fregionid',false,1,get_check_dates());//报纸各地域监测总情况
        $every_tp_region_data = [];//定义各地区发布监测情况数组
        $every_tp_region_num = 0;//报纸各违法地域数量
//循环赋值
        foreach ($every_tp_region_jczqk as $every_tp_region_jczqk_key => $every_tp_region_jczqk_val){
            if($every_tp_region_jczqk_val['fad_illegal_times'] != 0){
                $every_tp_region_num++;
                $every_tp_region_name[] = $every_tp_region_jczqk_val['tregion_name'];
            }
            $every_tp_region_data[] = [
                strval($every_tp_region_jczqk_key+1),
                $every_tp_region_jczqk_val['tregion_name'],
                $every_tp_region_jczqk_val['fad_times'],
                $every_tp_region_jczqk_val['fad_illegal_times'],
                $every_tp_region_jczqk_val['fad_illegal_times_rate'].'%'
            ];
        }


        /*
        * @报纸各广告类型监测总情况
        *
        **/
        $every_tp_adclass_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'03','','fad_illegal_times_rate','fcode',false,1,get_check_dates());//报纸各广告类型监测总情况
        $every_tp_adclass_data = [];//定义各地区发布监测情况数组
//循环赋值
        foreach ($every_tp_adclass_jczqk as $every_tp_adclass_jczqk_key => $every_tp_adclass_jczqk_val){
            /*            if($every_tb_adclass_jczqk_val['fad_illegal_times'] != 0){
            $every_tb_adclass_num++;
            $every_tb_adclass_name .= $every_tb_region_jczqk_val['tregion_name'].'、';
            }*/
            $every_tp_adclass_data[] = [
                strval($every_tp_adclass_jczqk_key+1),
                $every_tp_adclass_jczqk_val['tadclass_name'],
                $every_tp_adclass_jczqk_val['fad_times'],
                $every_tp_adclass_jczqk_val['fad_illegal_times'],
                $every_tp_adclass_jczqk_val['fad_illegal_times_rate'].'%'
            ];
        }

        /*
        * @PC各媒体监测总情况
        *
        **/
/*        $every_pc_media_data = [];
        $every_pc_media_jczqk = $StatisticalReport->net_media_customize($owner_media_ids,['tns.net_platform',1],'tn.fmediaid',$report_start_date,$report_end_date);//pc媒体广告发布情况
        foreach ($every_pc_media_jczqk as $every_pc_media_jczqk_key=>$every_pc_media_jczqk_val){
            $every_pc_media_data[] = [
                strval($every_pc_media_jczqk_key+1),
                $every_pc_media_jczqk_val['dymc'],
                $every_pc_media_jczqk_val['total'],
                $every_pc_media_jczqk_val['wfggsl'],
                $every_pc_media_jczqk_val['ggslwfl'].'%',
                $every_pc_media_jczqk_val['yzwfggsl'],
                $every_pc_media_jczqk_val['ggslyzwfl'].'%'
            ];
        }*/

        /*
        * @PC各广告类型监测总情况
        *
        **/
      /*  $every_pc_adclass_data = [];
        $every_pc_adclass_jczqk = $StatisticalReport->net_media_customize($owner_media_ids,['tns.net_platform',1],'ac.fcode',$report_start_date,$report_end_date);//pc媒体广告发布情况


        foreach ($every_pc_adclass_jczqk as $every_pc_adclass_jczqk_key=>$every_pc_adclass_jczqk_val){
            if($every_pc_adclass_jczqk_val['wfggsl'] != 0){
                $every_pc_adclass_name[] = $every_pc_adclass_jczqk_val['dymc'];
            }
            $every_pc_adclass_data[] = [
                strval($every_pc_adclass_jczqk_key+1),
                $every_pc_adclass_jczqk_val['dymc'],
                $every_pc_adclass_jczqk_val['total'],
                $every_pc_adclass_jczqk_val['wfggsl'],
                $every_pc_adclass_jczqk_val['ggslwfl'].'%',
                $every_pc_adclass_jczqk_val['yzwfggsl'],
                $every_pc_adclass_jczqk_val['ggslyzwfl'].'%'
            ];
        }*/
        /*
        * @微信各媒体监测总情况
        **/
/*        $every_wx_media_data = [];
        $every_wx_media_jczqk = $StatisticalReport->net_media_customize($owner_media_ids,['tns.net_platform',9],'tn.fmediaid',$report_start_date,$report_end_date);//pc媒体广告发布情况
        foreach ($every_wx_media_jczqk as $every_wx_media_jczqk_key=>$every_wx_media_jczqk_val){

            $every_wx_media_data[] = [
                strval($every_wx_media_jczqk_key+1),
                $every_wx_media_jczqk_val['dymc'],
                $every_wx_media_jczqk_val['total'],
                $every_wx_media_jczqk_val['wfggsl'],
                $every_wx_media_jczqk_val['ggslwfl'].'%',
                $every_wx_media_jczqk_val['yzwfggsl'],
                $every_wx_media_jczqk_val['ggslyzwfl'].'%'
            ];
        }*/

        /*
* @app各媒体监测总情况
**/
/*        $every_app_media_data = [];
        $every_app_media_jczqk = $StatisticalReport->net_media_customize($owner_media_ids,['tns.net_platform',2],'tn.fmediaid',$report_start_date,$report_end_date);//pc媒体广告发布情况
        foreach ($every_app_media_jczqk as $every_app_media_jczqk_key=>$every_app_media_jczqk_val){

            $every_app_media_data[] = [
                strval($every_app_media_jczqk_key+1),
                $every_app_media_jczqk_val['dymc'],
                $every_app_media_jczqk_val['total'],
                $every_app_media_jczqk_val['wfggsl'],
                $every_app_media_jczqk_val['ggslwfl'].'%',
                $every_app_media_jczqk_val['yzwfggsl'],
                $every_app_media_jczqk_val['ggslyzwfl'].'%'
            ];
        }*/


//定义数据数组
        $data = [
            'dotfilename' => 'ReportTemplate.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报告标题一",
            "text" => $user['regulatorname']
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报告标题二",
            "text" => $date_ym."广告监测通报"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "通报对象",
            "text" => $dq_name."工商行政管理局、市场监督管理部门："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报表通报描述",
            "text" => "（".$date_ymd."-".$date_md. "），". $user['regulatorname']."广告监测中心对".$dq_name."所属的媒介广告发布情况进行了监测，现将有关情况通报如下："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "一、监测范围及对象",
            "text" => "一、监测范围及对象"
        ];
/*        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（一）传统媒体广告监测",
            "text" => "（一）传统媒体广告监测"
        ];*/
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "传统媒体广告监测描述",
            "text" => "本期共对".$dq_name."所属的".$ct_media_count."家电视、广播、报纸等传统媒体广告发布情况进行了监测，其中电视媒体".$ct_media_tv_count."家、广播媒体".$ct_media_bc_count."家、报纸媒体".$ct_media_paper_count."家，广告监测范围为".$date_ymd."-".$date_md."（共".$days."天）"
        ];
/*        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（二）互联网媒体广告监测",
            "text" => "（二）互联网媒体广告监测"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "互联网媒体广告监测描述",
            "text" => "本期共对".$dq_name."所属的".$pc_media_count."家主要PC门户网、".$gzh_media_count."个微信公众号媒体、".$app_media_count."个APP客户端媒体，广告监测范围为".$date_ymd."-".$date_md."（共".$days."天）"
        ];*/
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "二、监测总体情况",
            "text" => "二、监测总体情况"
        ];
/*        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（一）传统媒体广告监测情况",
            "text" => "（一）传统媒体广告监测情况"
        ];*/
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "传统媒体广告监测情况描述",
            "text" => "本期共监测到电视、广播、报纸的各类广告".$ct_fad_times."条次，涉嫌违法广告".$ct_fad_illegal_times."条次，条次违法率为".round(($ct_fad_illegal_times/$ct_fad_times)*100,2)."％。"
        ];
/*        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（二）互联网媒体广告监测情况",
            "text" => "（二）互联网媒体广告监测情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "互联网媒体广告监测情况描述",
            "text" => "本期共监测到互联网广告".$net_media_customize['total']."条次，其中涉嫌违法广告".$net_media_customize['wfggsl']."条次，违法率".$net_media_customize['ggslwfl']."%。"
        ];*/
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表1各类媒体广告监测统计情况标题",
            "text" => "表1各类媒体广告监测统计情况"
        ];
        $every_media_data = array_values($every_media_data);
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表1各类媒体广告监测统计情况列表含时长",
            "data" => $every_media_data
        ];
        unset($every_media_data[4]);
        $every_media_data = $this->create_chart_data($every_media_data,0,1,2,3);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告监测情况条状图",
            "data" => $every_media_data[0]
        ];
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告发布量饼状图",
            "data" => $every_media_data[1]
        ];
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告违法发布量饼状图",
            "data" => $every_media_data[2]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "三、传统媒体广告监测情况",
            "text" => "三、传统媒体广告监测情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（一）广告总体监测情况",
            "text" => "（一）广告总体监测情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "广告总体监测情况描述",
            "text" => "本期监测数据显示，发布涉嫌违法广告条次违法率最为严重的".$every_region_num."个".$xj_name."依次为：".implode('、',$every_region_name)."。"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表2本期各地区发布涉嫌违法广告情况标题",
            "text" => "表2 本期各地区发布涉嫌违法广告情况"
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表2本期各地区发布涉嫌违法广告情况列表含时长",
            "data" => $every_region_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表2注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];

        $every_region_data_chart = $this->create_chart_data($every_region_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告监测情况条状图",
            "data" => $every_region_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告发布量饼状图",
            "data" => $every_region_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告违法发布量饼状图",
            "data" => $every_region_data_chart[2]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表3本期各大类违法广告发布总情况标题",
            "text" => '表3本期各大类违法广告发布总情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表3本期各大类违法广告发布总情况列表",
            "data" => $every_adclass_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表3注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];
        $every_adclass_data_chart = $this->create_chart_data($every_adclass_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告监测情况列条状图",
            "data" => $every_adclass_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告发布量饼状图",
            "data" => $every_adclass_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告违法发布量饼状图",
            "data" => $every_adclass_data_chart[2]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "各类媒体广告发布情况描述",
            "text" => '涉嫌违法广告类别主要集中在'.implode('、',$every_adclass_name).'，其中'.implode("、",$every_adclass_rate_des).'。'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（二）电视发布涉嫌违法广告情况",
            "text" => '（二）电视发布涉嫌违法广告情况'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "电视发布涉嫌违法广告情况描述",
            "text" => '本期电视发布涉嫌违法广告条次违法率最为严重的'.$every_tv_region_num.'个地区依次为：'.implode('、',$every_tv_region_name).'。'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表4电视发布涉嫌严重违法广告情况标题",
            "text" => '表4 电视发布涉嫌广告情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表4电视发布涉嫌严重违法广告情况列表",
            "data" => $every_tv_region_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表4注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];
        $every_tv_region_data_chart = $this->create_chart_data($every_tv_region_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "电视广告监测情况条状图",
            "data" => $every_tv_region_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "电视广告发布量饼状图",
            "data" => $every_tv_region_data_chart[1]
        ];
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "电视广告违法发布量饼状图",
            "data" => $every_tv_region_data_chart[2]
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表5电视各广告类型发布涉嫌违法广告情况标题",
            "text" => '表5 电视各广告类型发布涉嫌违法广告情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表5电视各广告类型发布涉嫌违法广告情况列表",
            "data" => $every_tv_adclass_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表5注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];

        $every_tv_adclass_data_chart = $this->create_chart_data($every_tv_adclass_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "电视各广告类型广告监测情况条状图",
            "data" => $every_tv_adclass_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "电视各广告类型广告发布量饼状图",
            "data" => $every_tv_adclass_data_chart[1]
        ];
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "电视各广告类型广告违法发布量饼状图",
            "data" => $every_tv_adclass_data_chart[2]
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（三）广播发布涉嫌违法广告情况",
            "text" => '（三）广播发布涉嫌违法广告情况'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "广播发布涉嫌违法广告情况描述",
            "text" => '本期广播发布涉嫌违法广告条次违法率最为严重的'.$every_tb_region_num.'个地区依次为：'.implode('、',$every_tb_region_name).'。'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表6广播发布涉嫌违法广告情况标题",
            "text" => '表6 广播发布涉嫌违法广告情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表6广播发布涉嫌违法广告情况列表",
            "data" => $every_tb_region_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表6注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];

        $every_tb_region_data_chart = $this->create_chart_data($every_tb_region_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "广播广告监测情况条状图",
            "data" => $every_tb_region_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "广播广告发布量饼状图",
            "data" => $every_tb_region_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "广播广告违法发布量饼状图",
            "data" => $every_tb_region_data_chart[2]
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表7广播各广告类别发布涉嫌违法广告情况标题",
            "text" => '表7 广播各广告类别发布涉嫌违法广告情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表7广播各广告类别发布涉嫌违法广告情况列表",
            "data" => $every_tb_adclass_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表7注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];

        $every_tb_adclass_data_chart = $this->create_chart_data($every_tb_adclass_data);

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "广播各广告类型广告监测情况条状图",
            "data" => $every_tb_adclass_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "广播各广告类型广告发布量饼状图",
            "data" => $every_tb_adclass_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "广播各广告类型广告违法发布量饼状图",
            "data" => $every_tb_adclass_data_chart[2]
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（四）报纸发布涉嫌违法广告情况",
            "text" => '（四）报纸发布涉嫌违法广告情况'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报纸发布涉嫌违法广告情况描述",
            "text" => '本期报纸发布涉嫌违法广告条次违法率最为严重的'.$every_tp_region_num.'个地区依次为：'.implode('、',$every_tp_region_name).'。'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表8报纸发布涉嫌违法广告情况标题",
            "text" => '表8 报纸发布涉嫌违法广告情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表8报纸发布涉嫌违法广告情况列表",
            "data" => $every_tp_region_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表8注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];
        $every_tp_region_data_chart = $this->create_chart_data($every_tp_region_data);

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "报纸广告监测情况条状图",
            "data" => $every_tp_region_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "报纸广告发布量饼状图",
            "data" => $every_tp_region_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "报纸广告违法发布量饼状图",
            "data" => $every_tp_region_data_chart[2]
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表9报纸各广告类型发布涉嫌违法广告情况标题",
            "text" => '表9 报纸各广告类型发布涉嫌违法广告情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表9报纸各广告类型发布涉嫌违法广告情况列表",
            "data" => $every_tp_adclass_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表9注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];
        $every_tp_adclass_data_chart = $this->create_chart_data($every_tp_adclass_data);

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "报纸各广告类型广告监测情况条状图",
            "data" => $every_tp_adclass_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "报纸各广告类型广告发布量饼状图",
            "data" => $every_tp_adclass_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "报纸各广告类型广告违法发布量饼状图",
            "data" => $every_tp_adclass_data_chart[2]
        ];


  /*      $data['content'][] = [
            "type" => "text",
            "bookmark" => "四、互联网广告监测情况",
            "text" => "四、互联网广告监测情况"
        ];
        if($net_media_customize['total'] != 0){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "（一）总体发布情况",
                "text" => "（一）总体发布情况"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "总体发布情况描述",
                "text" => "本期共监测到互联网广告".$net_media_customize['total']."条次，其中违法广告".$net_media_customize['wfggsl']."条，违法率".$net_media_customize['ggslwfl']."%。微信公众号全部类别广告".$net_gzh_customize['total']."条。"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "PC门户网站广告发布情况标题",
                "text" => "1、PC门户网站广告发布情况"
            ];
            if(!empty($every_pc_media_data)){
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "PC门户网站广告发布情况列表",
                    "data" => $every_pc_media_data
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "PC门户网站广告发布情况列表注释",
                    "text" => "（注：按发布涉嫌违法广告情况严重程度从高到低排列）"
                ];
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "总体发布情况描述",
                    "text" => "本期未监测到PC广告"
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "PC门户网站广告类别发布情况标题",
                "text" => "2、PC门户网站广告类别发布情况"
            ];
            if(!empty($every_pc_adclass_data)){
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "PC门户网站广告类别发布情况列表",
                    "data" => $every_pc_adclass_data
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "PC门户网站广告类别发布情况列表注释",
                    "text" => "（注：按发布涉嫌违法广告情况严重程度从高到低排列）"
                ];
                $every_pc_adclass_name = implode('、',$every_pc_adclass_name);
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "PC门户网站广告类别发布情况列表描述",
                    "text" => $every_pc_adclass_name."是本月网络违法广告主要类别。"
                ];
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "总体发布情况描述",
                    "text" => "本期各大类未监测到PC广告"
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "微信公众号发布情况",
                "text" => "微信公众号发布情况"
            ];
            if(!empty($every_wx_media_data)){
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "微信公众号发布情况列表",
                    "data" => $every_wx_media_data
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "微信公众号发布情况列表注释",
                    "text" => "（注：按发布涉嫌违法广告情况严重程度从高到低排列）"
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "（二）涉嫌违法发布情况",
                    "text" => "（二）涉嫌违法发布情况"
                ];

                if($net_pc_customize['wfggsl'] != 0){
                    $ybwf = '其中涉嫌违法广告'.$net_pc_customize['wfggsl'].'条，违法率'.$net_pc_customize['ggslwfl'].'%。';
                    $pc_des = $ybwf;
                }else{
                    $pc_des = '未发现涉嫌违法广告。';
                }
                if($net_gzh_customize['wfggsl'] != 0){
                    $ybwf = '其中涉嫌违法广告'.$net_gzh_customize['wfggsl'].'条，违法率'.$net_gzh_customize['ggslwfl'].'%。';
                    $wx_des = $ybwf;
                }else{
                    $wx_des = '未发现涉嫌违法广告。';
                }
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "涉嫌违法发布情况描述一",
                    "text" => "本月次监测PC门户网站全部类别广告".$net_pc_customize['total']."条，".$pc_des."微信公众号全部类别广告".$net_gzh_customize['total']."条，".$wx_des
                ];
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "总体发布情况描述",
                    "text" => "本期未监测到微信公众号广告"
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "微信公众号发布情况",
                "text" => "4、APP客户端发布情况"
            ];
            if(!empty($every_app_media_data)){
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "微信公众号发布情况列表",
                    "data" => $every_app_media_data
                ];
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "总体发布情况描述",
                    "text" => "本期未监测到APP客户端广告"
                ];
            }

            $every_pc_adclass_des = '';

            foreach ($every_pc_adclass_data as $every_pc_adclass_data_key=>$every_pc_adclass_data_val){
                if($every_pc_adclass_data_val['3'] > 0){
                    $every_pc_adclass_name .= $every_pc_adclass_data_val[1].'、';
                    if($every_pc_adclass_data_key == 0){
                        $every_pc_adclass_des .= $every_pc_adclass_data_val[1].'广告占涉嫌违法广告总量的'.round(($every_pc_adclass_data_val[3]/$net_pc_customize['wfggsl'])*100,2).'％、';
                    }else{
                        $every_pc_adclass_des .= $every_pc_adclass_data_val[1].'广告占'.round(($every_pc_adclass_data_val[3]/$net_pc_customize['wfggsl'])*100,2).'％、';
                    }
                }
            }
            $every_pc_media_des = '';

            foreach ($every_pc_media_data as $every_pc_media_data_key=>$every_pc_media_data_val){
                if($every_pc_media_data_val['3'] > 0){
                    $every_pc_media_name[] = $every_pc_media_data_val[1];
                    if($every_pc_media_data_key == 0){
                        $every_pc_media_des .= $every_pc_media_data_val[1].'发布的占涉嫌违法广告总量的'.round(($every_pc_media_data_val[3]/$net_pc_customize['wfggsl'])*100,2).'％、';
                    }else{
                        $every_pc_media_des .= $every_pc_media_data_val[1].'占'.round(($every_pc_media_data_val[3]/$net_pc_customize['wfggsl'])*100,2).'％、';
                    }
                }else{
                    $not_illegal_media_num++;
                    $not_illegal_media_name[] = $every_pc_media_data_val[1];
                }
            }
            if($every_pc_adclass_des == ''){
                $every_pc_adclass_des = '全部类别下无违法广告';
            }

            if($every_pc_media_des == ''){
                $every_pc_media_des = '所有PC门户网站未发布违法广告';
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "涉嫌违法发布情况描述二",
                    "text" => "其中PC门户网站违法违法分布情况如下：".$every_pc_adclass_des."。".$every_pc_media_des."。"
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "涉嫌违法发布情况描述三",
                    "text" => $not_illegal_media_num."家网站本次监测没有发现违法广告，分别是：".implode('、',$not_illegal_media_name)."。"
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "涉嫌违法发布情况描述四",
                    "text" => "PC门户网站中，".implode('、',$every_pc_media_name)."为主要的违法广告发布网站。"
                ];
                foreach ($every_pc_media_data as $every_pc_media_data_key=>$every_pc_media_data_val){
                    array_splice($every_pc_media_data[$every_pc_media_data_key],0,1);
                    array_splice($every_pc_media_data[$every_pc_media_data_key],1,1);
                    array_splice($every_pc_media_data[$every_pc_media_data_key],2,1);
                    array_splice($every_pc_media_data[$every_pc_media_data_key],2,1);
                    array_splice($every_pc_media_data[$every_pc_media_data_key],2,1);
                }
                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "PC网站违法发布量饼状图",
                    "data" => $every_pc_media_data
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "PC门户网站主要违法情形",
                    "text" => "1、PC门户网站主要违法情形"
                ];

                $every_pc_illegal_situation_data = [];
                foreach ($pc_illegal_situation as $pc_illegal_situation_key => $pc_illegal_situation_val){
                    $every_pc_illegal_situation_data[] = [
                        strval($pc_illegal_situation_key+1),
                        $pc_illegal_situation_val['fillegalcontent'],
                        $pc_illegal_situation_val['illegal_count']
                    ];
                }
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "PC门户网站主要违法情形列表",
                    "data" => $every_pc_illegal_situation_data
                ];
            }

            $every_wx_media_des = '';
            foreach ($every_wx_media_data as $every_wx_media_data_key=>$every_wx_media_data_val){
                if($every_wx_media_data_val['3'] > 0){
                    $every_wx_media_name .= $every_wx_media_data_val[1].'、';
                    if($every_wx_media_data_key == 0){
                        $every_wx_media_des .= $every_wx_media_data_val[1].'发布的占涉嫌违法广告总量的'.round(($every_wx_media_data_val[3]/$net_gzh_customize['wfggsl'])*100,2).'％、';
                    }else{
                        $every_wx_media_des .= $every_wx_media_data_val[1].'占'.round(($every_wx_media_data_val[3]/$net_gzh_customize['wfggsl'])*100,2).'％、';
                    }
                }else{
                    $not_illegal_wx_media_num++;
                    $not_illegal_wx_media_name .= $every_wx_media_data_val[1].'、';
                }
            }
            if($every_wx_media_des == ''){
                $every_wx_media_des = '本次监测微信公众号没有发布涉嫌违法广告数据。';
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "微信公众号违法广告发布情况",
                    "text" => "微信公众号违法广告发布情况"
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "微信公众号违法广告发布情况描述",
                    "text" => $every_wx_media_des
                ];
            }
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "微信公众号违法广告发布情况描述",
                "text" => '本期未检测到互联网数据'
            ];
        }*/

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "落款",
            "text" => $user['regulatorname']
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "时间",
            "text" => $date_end_ymd
        ];

        $report_data = json_encode($data);
        //echo $report_data;exit;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $system_num = getconfig('system_num');

//将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].date('Y年m月',strtotime($month)).'月报';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 30;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }

    //季报
    public function create_quarter(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型
        $quarter = I('quarter',3);//季度
        $year = I('year','2018');//季度
        $user = session('regulatorpersonInfo');//获取用户信息

        $tregion_count = M('tregion')->where(['fpid'=>$user['regionid']])->count();

        //匹配
        $cycle_arr = [];
        switch ($quarter){
            case 1:
                $days_1 = date('t', strtotime($year."-1-1"));
                $days_2 = date('t', strtotime($year."-2-1"));
                $days_3 = date('t', strtotime($year."-3-1"));
                $cycle_arr = [
                    [$year."-1-1",$year."-1-".$days_1],
                    [$year."-2-1",$year."-2-".$days_2],
                    [$year."-3-1",$year."-3-".$days_3]
                ];
                break;
            case 2:
                $days_4 = date('t', strtotime($year."-4-1"));
                $days_5 = date('t', strtotime($year."-5-1"));
                $days_6 = date('t', strtotime($year."-6-1"));
                $cycle_arr = [
                    [$year."-4-1",$year."-4-".$days_4],
                    [$year."-5-1",$year."-5-".$days_5],
                    [$year."-6-1",$year."-6-".$days_6]
                ];
                break;
            case 3:
                $days_7 = date('t', strtotime($year."-7-1"));
                $days_8 = date('t', strtotime($year."-8-1"));
                $days_9 = date('t', strtotime($year."-9-1"));
                $cycle_arr = [
                    [$year."-7-1",$year."-7-".$days_7],
                    [$year."-8-1",$year."-8-".$days_8],
                    [$year."-9-1",$year."-9-".$days_9]
                ];
                break;
            case 4:
                $days_10 = date('t', strtotime($year."-10-1"));
                $days_11 = date('t', strtotime($year."-11-1"));
                $days_12 = date('t', strtotime($year."-12-1"));
                $cycle_arr = [
                    [$year."-10-1",$year."-10-".$days_10],
                    [$year."-11-1",$year."-11-".$days_11],
                    [$year."-12-1",$year."-12-".$days_12]
                ];
                break;
        }

        switch ($quarter){
            case 1:
                break;
                $quarter_str = '一';
            case 2:
                $quarter_str = '二';
                break;
            case 3:
                $quarter_str = '三';
                break;
            case 4:
                $quarter_str = '四';
                break;
        }

        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级
        if($re_level == 2 || $re_level == 3 || $re_level == 4){
            $dq_name = '市级';
            $r_name = '全市';
            $xj_name = '区县';
        }elseif($re_level == 1){
            $dq_name = '全市';
            $xj_name = '区县级';
        }else{
            $dq_name = '全'.$user['regionname1'];
            $xj_name = '地方';
        }

        $date_str = $year.'年第'.$quarter_str.'季度';//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数
        $owner_media_ids = get_owner_media_ids();
        $ct_jczqk_quarter = [];
        $every_region_jczqk_quarter = [];
        $hz_tv_illegal_ad_data_quarter = [];
        $every_region_jczqk_quarter = [];//各地域监测总情况
        $every_ad_class_jczqk_quarter = [];//各广告类别监测总情况
        //本级电视
        $hz_tv_illegal_ad_data_quarter = [];
        //本级广播
        $hz_tb_illegal_ad_data_quarter = [];
        //本级报纸
        $hz_tp_illegal_ad_data_quarter = [];
        //下级电视
        $xj_tv_illegal_ad_data_quarter = [];
        //下级广播
        $xj_tb_illegal_ad_data_quarter = [];
        //下级报纸
        $xj_tp_illegal_ad_data_quarter = [];
        $hz_zdclass_tv_illegal_ad_data_quarter = [];
        $hz_zdclass_tb_illegal_ad_data_quarter = [];
        $hz_zdclass_tp_illegal_ad_data_quarter = [];
        $ct_media_tv_count_hz = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局电视媒体数量

        $ct_media_bc_count_hz = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局广播媒体数量

        $ct_media_paper_count_hz = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局报纸媒体数量

        $ct_media_tv_count_xj = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县电视媒体数量

        $ct_media_bc_count_xj = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县广播媒体数量

        $ct_media_paper_count_xj = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县报纸媒体数量


        $ct_media_count = $ct_media_tv_count_hz + $ct_media_bc_count_hz + $ct_media_paper_count_hz + $ct_media_tv_count_xj + $ct_media_bc_count_xj + $ct_media_paper_count_xj;//全部有数据的媒体数量
        foreach ($cycle_arr as $cycle_arr_val){
            $date_table = date('Ym',strtotime($cycle_arr_val[0])).'_'.substr($user['regionid'],0,2);//组合表

            if(!table_exist("ttvissue_".$date_table) || !table_exist("tbcissue_".$date_table)){
                continue;
            }
            $report_start_date = strtotime($cycle_arr_val[0]);
            $report_end_date = strtotime($cycle_arr_val[1])+86399;
            /*
            * @各媒体监测总情况
            *
            **/

            $ct_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_count_rate','fmediaclass',false,1,get_check_dates());//传统媒体监测总情况


            /*
* @各地域监测总情况
*
**/
            $every_region_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times','fregionid',false,1,get_check_dates());//各地域监测总情况


            /*
            * @各广告类型监测总情况
            *
            **/
            $every_ad_class_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_times','fcode',false,1,get_check_dates());//各地域监测总情况

            //本级电视
            $hz_tv_illegal_ad_data_quarter[] = $StatisticalReport->illegal_ad_order_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',0);

            //本级广播
            $hz_tb_illegal_ad_data_quarter[] = $StatisticalReport->illegal_ad_order_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',0);

            //本级报纸
            $hz_tp_illegal_ad_data_quarter[] = $StatisticalReport->illegal_ad_order_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',0);

            //下级电视
            $xj_tv_illegal_ad_data_quarter[] = $StatisticalReport->illegal_ad_order_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',2);

            //下级广播
            $xj_tb_illegal_ad_data_quarter[] = $StatisticalReport->illegal_ad_order_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',2);

            //下级报纸
            $xj_tp_illegal_ad_data_quarter[] = $StatisticalReport->illegal_ad_order_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',2);


            //四大类 药品、医疗服务、医疗器械、保健食品
            $hz_zdclass_tv_illegal_ad_data_quarter[] = $StatisticalReport->illegal_ad_order_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,['01','02','13','06'],1);
            $hz_zdclass_tb_illegal_ad_data_quarter[] = $StatisticalReport->illegal_ad_order_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,['01','02','13','06'],1);
            $hz_zdclass_tp_illegal_ad_data_quarter[] = $StatisticalReport->illegal_ad_order_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,['01','02','13','06'],1);

        }



        $ct_jczqk = $this->create_data_hzlist($ct_jczqk_quarter,'fclass');
        $every_region_jczqk = $this->create_data_hzlist($every_region_jczqk_quarter,'fregionid');
        $every_ad_class_jczqk = $this->create_data_hzlist($every_ad_class_jczqk_quarter,'fcode');
        $hz_tv_illegal_ad_data = $this->create_data_hzlist2($hz_tv_illegal_ad_data_quarter);
        $hz_tb_illegal_ad_data = $this->create_data_hzlist2($hz_tb_illegal_ad_data_quarter);
        $hz_tp_illegal_ad_data = $this->create_data_hzlist2($hz_tp_illegal_ad_data_quarter);
        $xj_tv_illegal_ad_data = $this->create_data_hzlist2($xj_tv_illegal_ad_data_quarter);
        $xj_tb_illegal_ad_data = $this->create_data_hzlist2($xj_tb_illegal_ad_data_quarter);
        $xj_tp_illegal_ad_data = $this->create_data_hzlist2($xj_tp_illegal_ad_data_quarter);

        $hz_zdclass_tv_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tv_illegal_ad_data_quarter);
        $hz_zdclass_tb_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tb_illegal_ad_data_quarter);
        $hz_zdclass_tp_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tp_illegal_ad_data_quarter);

        //本级电视
        $hz_tv_illegal_ad_data = $this->get_table_data($hz_tv_illegal_ad_data);
        //本级广播
        $hz_tb_illegal_ad_data = $this->get_table_data($hz_tb_illegal_ad_data);
        //本级报纸
        $hz_tp_illegal_ad_data = $this->get_table_data($hz_tp_illegal_ad_data);
        //下级电视
        $xj_tv_illegal_ad_data = $this->get_table_data($xj_tv_illegal_ad_data);
        //下级广播
        $xj_tb_illegal_ad_data = $this->get_table_data($xj_tb_illegal_ad_data);
        //下级报纸
        $xj_tp_illegal_ad_data = $this->get_table_data($xj_tp_illegal_ad_data);

        $hz_zdclass_tv_illegal_ad_data = $this->get_table_data($hz_zdclass_tv_illegal_ad_data);//电视
        $hz_zdclass_tb_illegal_ad_data = $this->get_table_data($hz_zdclass_tb_illegal_ad_data);//广播
        $hz_zdclass_tp_illegal_ad_data = $this->get_table_data($hz_zdclass_tp_illegal_ad_data);//报纸
        /*
        * @各媒体监测总情况
        *
        **/
        $ct_fad_times = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times = 0;//定义传统媒体发布违法广告条次
        $every_media_class_data = [];//定义媒体发布监测情况数组
//循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk as $ct_jczqk_val){
            $ct_fad_times += $ct_jczqk_val['fad_times'];
            $ct_fad_illegal_times += $ct_jczqk_val['fad_illegal_times'];
            $ct_fad_count += $ct_jczqk_val['fad_count'];
            $ct_fad_illegal_count += $ct_jczqk_val['fad_illegal_count'];
            $ct_data_val = [
                $ct_jczqk_val['tmediaclass_name'],
                $ct_jczqk_val['fad_times'],
                $ct_jczqk_val['fad_count'],
                $ct_jczqk_val['fad_illegal_times'],
                $ct_jczqk_val['fad_illegal_count'],
                $ct_jczqk_val['fad_illegal_times_rate'].'%',
                $ct_jczqk_val['fad_illegal_count_rate'].'%'
            ];
            if($ct_jczqk_val['fmedia_class_code'] == '01'){
                $every_media_class_data[0] = $ct_data_val;
            }elseif($ct_jczqk_val['fmedia_class_code'] == '02'){
                $every_media_class_data[1] = $ct_data_val;
            }else{
                $every_media_class_data[2] = $ct_data_val;
            }
        }

        $every_media_class_data[3] = [
            '合计',
            strval($ct_fad_times),
            strval($ct_fad_count),
            strval($ct_fad_illegal_times),
            strval($ct_fad_illegal_count),
            round((($ct_fad_illegal_times)/($ct_fad_times))*100,2).'%',
            round((($ct_fad_illegal_count)/($ct_fad_count))*100,2).'%'
        ];//二、各类媒体广告发布情况
        $every_media_class_data = array_values($every_media_class_data);
        /*
* @各地域监测总情况
*
**/
        $every_region_data = [];//定义各地区发布监测情况数组
//循环赋值
        $every_region_illegal_times_data = [
            [''],
            ['']
        ];
        $every_region_jczqk = pxsf($every_region_jczqk,'fad_illegal_times');
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){
            $every_region_fad_times += $every_region_jczqk_val['fad_times'];
            $every_region_fad_illegal_times += $every_region_jczqk_val['fad_illegal_times'];
            $every_region_fad_count += $every_region_jczqk_val['fad_count'];
            $every_region_fad_illegal_count += $every_region_jczqk_val['fad_illegal_count'];
            if($every_region_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_region_illegal_times_data[0],$every_region_jczqk_val['tregion_name']);
                array_push($every_region_illegal_times_data[1],strval($every_region_jczqk_val['fad_illegal_times']));
            }

            $every_region_data[] = [
                strval($every_region_jczqk_key+1),
                $every_region_jczqk_val['tregion_name'],
                $every_region_jczqk_val['fad_times'],
                $every_region_jczqk_val['fad_count'],
                $every_region_jczqk_val['fad_illegal_times'],
                $every_region_jczqk_val['fad_illegal_count'],
                $every_region_jczqk_val['fad_illegal_times_rate'].'%',
                $every_region_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_region_data[] = [
            '',
            '合计',
            strval($every_region_fad_times),
            strval($every_region_fad_count),
            strval($every_region_fad_illegal_times),
            strval($every_region_fad_illegal_count),
            round((($every_region_fad_illegal_times)/($every_region_fad_times))*100,2).'%',
            round((($every_region_fad_illegal_count)/($every_region_fad_count))*100,2).'%'
        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）



        /*
* @各地域电视广播报纸监测总情况
*
**/
        $every_media_all_data = [];
        $every_media_illegal_times_all_data = [];
        foreach (['01','02','03'] as $every_media_val){
            $every_media_data = [];
            $every_media_illegal_times_data = [];
            $every_media_jczqk = [];
            //循环赋值
            $every_media_fad_times = 0;
            $every_media_fad_illegal_times = 0;
            $every_media_fad_count = 0;
            $every_media_fad_illegal_count = 0;
            $every_media_illegal_times_data = [
                [''],
                ['']
            ];
            $every_media_jczqk_quarter = [];
            foreach ($cycle_arr as $cycle_arr_val){
                $date_table = date('Y',strtotime($cycle_arr_val[0])).'_'.$user['regionid'];//组合表
                if(!table_exist("ttvissue_".$date_table) || !table_exist("tbcissue_".$date_table)){
                    continue;
                }
                $report_start_date = strtotime($cycle_arr_val[0]);
                $report_end_date = strtotime($cycle_arr_val[1])+86399;
                $every_media_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,$every_media_val,'','fad_illegal_times','fmediaid',false,1,get_check_dates());//各地域监测总情况

            }
            $every_media_jczqk = $this->create_data_hzlist($every_media_jczqk_quarter,'fid');//fad_illegal_times
            $every_media_jczqk = pxsf($every_media_jczqk,'fad_illegal_times');//重新排序
            foreach ($every_media_jczqk as $every_media_jczqk_key => $every_media_jczqk_val){
                $every_media_fad_times += $every_media_jczqk_val['fad_times'];
                $every_media_fad_illegal_times += $every_media_jczqk_val['fad_illegal_times'];
                $every_media_fad_count += $every_media_jczqk_val['fad_count'];
                $every_media_fad_illegal_count += $every_media_jczqk_val['fad_illegal_count'];

                if($every_media_jczqk_val['fad_illegal_times'] > 0){
                    array_push($every_media_illegal_times_data[0],$every_media_jczqk_val['tmedia_name']);
                    array_push($every_media_illegal_times_data[1],strval($every_media_jczqk_val['fad_illegal_times']));
                }

                $every_media_data[] = [
                    strval($every_media_jczqk_key+1),
                    $every_media_jczqk_val['tregion_name'],
                    $every_media_jczqk_val['tmedia_name'],
                    $every_media_jczqk_val['fad_times'],
                    $every_media_jczqk_val['fad_count'],
                    $every_media_jczqk_val['fad_illegal_times'],
                    $every_media_jczqk_val['fad_illegal_count'],
                    $every_media_jczqk_val['fad_illegal_times_rate'].'%',
                    $every_media_jczqk_val['fad_illegal_count_rate'].'%'
                ];
            }
            /*            $every_media_data[] = [
                            '',
                            '合计',
                            strval($every_media_fad_times),
                            strval($every_media_fad_count),
                            strval($every_media_fad_illegal_times),
                            strval($every_media_fad_illegal_count),
                            round((($every_media_fad_illegal_times)/($every_media_fad_times))*100,2).'%',
                            round((($every_media_fad_illegal_count)/($every_media_fad_count))*100,2).'%'
                        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）*/
            $every_media_all_data[] = $every_media_data;
            $every_media_illegal_times_all_data[] = $every_media_illegal_times_data;
        }


        $every_media_tv_data = $every_media_all_data[0];//电视各媒体
        $every_media_tb_data = $every_media_all_data[1];//广播各媒体
        $every_media_tp_data = $every_media_all_data[2];//报纸各媒体
        $every_media_illegal_times_tv_data = $every_media_illegal_times_all_data[0];//电视各媒体
        $every_media_illegal_times_tb_data = $every_media_illegal_times_all_data[1];//广播各媒体
        $every_media_illegal_times_tp_data =$every_media_illegal_times_all_data[2];//报纸各媒体





        /*
* @各广告类型监测总情况
*
**/

        $every_ad_class_data = [];//定义各地区发布监测情况数组
//循环赋值
        $every_ad_class_illegal_times_data = [
            [''],
            ['']
        ];
        $every_ad_class_jczqk = pxsf($every_ad_class_jczqk,'fad_illegal_times');//重新排序
        foreach ($every_ad_class_jczqk as $every_ad_class_jczqk_key => $every_ad_class_jczqk_val){
            $every_ad_class_fad_times += $every_ad_class_jczqk_val['fad_times'];
            $every_ad_class_fad_illegal_times += $every_ad_class_jczqk_val['fad_illegal_times'];
            $every_ad_class_fad_count += $every_ad_class_jczqk_val['fad_count'];
            $every_ad_class_fad_illegal_count += $every_ad_class_jczqk_val['fad_illegal_count'];
            if($every_ad_class_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_ad_class_illegal_times_data[0],$every_ad_class_jczqk_val['tadclass_name']);
                array_push($every_ad_class_illegal_times_data[1],strval($every_ad_class_jczqk_val['fad_illegal_times']));
            }

            $every_ad_class_data[] = [
                strval($every_ad_class_jczqk_key+1),
                $every_ad_class_jczqk_val['tadclass_name'],
                $every_ad_class_jczqk_val['fad_times'],
                $every_ad_class_jczqk_val['fad_count'],
                $every_ad_class_jczqk_val['fad_illegal_times'],
                $every_ad_class_jczqk_val['fad_illegal_count'],
                $every_ad_class_jczqk_val['fad_illegal_times_rate'].'%',
                $every_ad_class_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_ad_class_data[] = [
            '',
            '合计',
            strval($every_ad_class_fad_times),
            strval($every_ad_class_fad_count),
            strval($every_ad_class_fad_illegal_times),
            strval($every_ad_class_fad_illegal_count),
            round((($every_ad_class_fad_illegal_times)/($every_ad_class_fad_times))*100,2).'%',
            round((($every_ad_class_fad_illegal_count)/($every_ad_class_fad_count))*100,2).'%'
        ];


        $typical_illegal_ad_list_data = [];
        if(!empty($hz_zdclass_tv_illegal_ad_data) || !empty($hz_zdclass_tb_illegal_ad_data) || !empty($hz_zdclass_tp_illegal_ad_data)){
            $typical_illegal_ad_list = array_merge($hz_zdclass_tv_illegal_ad_data,$hz_zdclass_tb_illegal_ad_data,$hz_zdclass_tp_illegal_ad_data);
            $typical_illegal_ad_list_data = pxsf($typical_illegal_ad_list,'illegal_times');//排序
        }



//定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "大标题楷体60",
            "text" => "广告监测季报"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号加粗",
            "text" => $user['regulatorname']
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号加粗带红线",
            "text" => "广告监测中心               ".date('Y年m月d日')
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $user['regulatorname']."广告监测中心对".$date_str.$r_name."部分电视、广播、报刊等".$ct_media_count."家媒体发布广告的情况进行了抽查监测，现将监测情况通报如下："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "一、监测范围及对象"
        ];
        if($ct_media_tv_count_hz != 0){
            $hz_media_count_str .= $ct_media_tv_count_hz."个电视媒体；";
        }
        if($ct_media_bc_count_hz != 0){
            $hz_media_count_str .= $ct_media_bc_count_hz."个广播媒体；";
        }
        if($ct_media_paper_count_hz != 0){
            $hz_media_count_str .= $ct_media_paper_count_hz."个报纸媒体；";
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）".$dq_name."媒体：".$hz_media_count_str
        ];
        if($ct_media_tv_count_xj != 0){
            $xj_media_count_str .= $ct_media_tv_count_xj."个电视媒体；";
        }
        if($ct_media_bc_count_xj != 0){
            $xj_media_count_str .= $ct_media_bc_count_xj."个广播媒体；";
        }
        if($ct_media_paper_count_xj != 0){
            $xj_media_count_str .= $ct_media_paper_count_xj."个报纸媒体；";
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）其他".$tregion_count."个区、县：".$xj_media_count_str
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "二、各类媒体广告发布情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "本季度共监测广告".$every_media_class_data[3][1]."条次，".$every_media_class_data[3][2]."条数，其中监测发现各类涉嫌违法广告".$every_media_class_data[3][3]."条次，".$every_media_class_data[3][4]."条数。具体情况详见下表："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒介类型汇总表格hz",
            "data" => $every_media_class_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "三、市（各区县）媒体广告发布总体情况"
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "地域排名汇总表格hz",
            "data" => $every_region_data
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各市区县违法广告发布量排名条形图hz",
            "data" => $every_region_illegal_times_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "四、全市电视、广播、报纸广告发布情况（排名按发布违法广告条次由高到底排列）"
        ];

        if(!empty($every_media_tv_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "1、电视"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tv_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tv_data
            ];
        }
        if(!empty($every_media_tb_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "2、广播"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tb_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tb_data
            ];
        }
        if(!empty($every_media_tp_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "3、报纸"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tp_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tp_data
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、全市各类媒体发布的违法广告（全部类别）通报"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）市级媒体"
        ];


        if(!empty($hz_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $this->pmaddkey($hz_tv_illegal_ad_data)
            ];
        }
        if(!empty($hz_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $this->pmaddkey($hz_tb_illegal_ad_data)
            ];
        }
        if(!empty($hz_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $this->pmaddkey($hz_tp_illegal_ad_data)
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）区（县）级媒体"
        ];

        if(!empty($xj_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $this->pmaddkey($xj_tv_illegal_ad_data)
            ];
        }
        if(!empty($xj_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $this->pmaddkey($xj_tb_illegal_ad_data)
            ];
        }

        if(!empty($xj_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $this->pmaddkey($xj_tp_illegal_ad_data)
            ];
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "六、全市各类违法广告比重结构图"
        ];
        if(!empty($every_ad_class_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表hz",
                "data" => $every_ad_class_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "各大类别广告违法占比表hz",
                "data" => $every_ad_class_illegal_times_data
            ];

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "七、药品、医疗服务、医疗器械、保健食品违法广告发布情况"
        ];

        if(!empty($typical_illegal_ad_list_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $typical_illegal_ad_list_data
            ];
        }

        $report_data = json_encode($data);
        //echo $report_data;exit;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $system_num = getconfig('system_num');

//将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].$date_str.'报告';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 70;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',strtotime($cycle_arr[0][0]));
            $data['pnendtime'] 		    = date('Y-m-d',strtotime($cycle_arr[2][1]));
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }

    //年报
    public function create_year(){

        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型
        $year = I('year','2018');//季度
        $days_1 = date('t', strtotime($year."-1-1"));
        $days_2 = date('t', strtotime($year."-2-1"));
        $days_3 = date('t', strtotime($year."-3-1"));
        $days_4 = date('t', strtotime($year."-4-1"));
        $days_5 = date('t', strtotime($year."-5-1"));
        $days_6 = date('t', strtotime($year."-6-1"));
        $days_7 = date('t', strtotime($year."-7-1"));
        $days_8 = date('t', strtotime($year."-8-1"));
        $days_9 = date('t', strtotime($year."-9-1"));
        $days_10 = date('t', strtotime($year."-10-1"));
        $days_11 = date('t', strtotime($year."-11-1"));
        $days_12 = date('t', strtotime($year."-12-1"));
        $cycle_arr = [
            [$year."-1-1",$year."-1-".$days_1],
            [$year."-2-1",$year."-2-".$days_2],
            [$year."-3-1",$year."-3-".$days_3],
            [$year."-4-1",$year."-4-".$days_4],
            [$year."-5-1",$year."-5-".$days_5],
            [$year."-6-1",$year."-6-".$days_6],
            [$year."-7-1",$year."-7-".$days_7],
            [$year."-8-1",$year."-8-".$days_8],
            [$year."-9-1",$year."-9-".$days_9],
            [$year."-10-1",$year."-10-".$days_10],
            [$year."-11-1",$year."-11-".$days_11],
            [$year."-12-1",$year."-12-".$days_12]
        ];

        $user = session('regulatorpersonInfo');//获取用户信息
        $tregion_count = M('tregion')->where(['fpid'=>$user['regionid']])->count();

        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级
        if($re_level == 2 || $re_level == 3 || $re_level == 4){
            $dq_name = '市级';
            $r_name = '全市';
            $xj_name = '区县';
        }elseif($re_level == 1){
            $dq_name = '全市';
            $xj_name = '区县级';
        }else{
            $dq_name = '全'.$user['regionname1'];
            $xj_name = '地方';
        }

        $date_str = $year.'年';//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数
        $owner_media_ids = get_owner_media_ids();


        $ct_jczqk_quarter = [];
        $every_region_jczqk_quarter = [];
        $hz_tv_illegal_ad_data_quarter = [];
        $every_region_jczqk_quarter = [];//各地域监测总情况
        $every_ad_class_jczqk_quarter = [];//各广告类别监测总情况
        //本级电视
        $hz_tv_illegal_ad_data_quarter = [];
        //本级广播
        $hz_tb_illegal_ad_data_quarter = [];
        //本级报纸
        $hz_tp_illegal_ad_data_quarter = [];
        //下级电视
        $xj_tv_illegal_ad_data_quarter = [];
        //下级广播
        $xj_tb_illegal_ad_data_quarter = [];
        //下级报纸
        $xj_tp_illegal_ad_data_quarter = [];
        $hz_zdclass_tv_illegal_ad_data_quarter = [];
        $hz_zdclass_tb_illegal_ad_data_quarter = [];
        $hz_zdclass_tp_illegal_ad_data_quarter = [];
        $ct_media_tv_count_hz = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局电视媒体数量

        $ct_media_bc_count_hz = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局广播媒体数量

        $ct_media_paper_count_hz = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局报纸媒体数量

        $ct_media_tv_count_xj = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县电视媒体数量

        $ct_media_bc_count_xj = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县广播媒体数量

        $ct_media_paper_count_xj = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县报纸媒体数量


        $ct_media_count = $ct_media_tv_count_hz + $ct_media_bc_count_hz + $ct_media_paper_count_hz + $ct_media_tv_count_xj + $ct_media_bc_count_xj + $ct_media_paper_count_xj;//全部有数据的媒体数量
        foreach ($cycle_arr as $cycle_arr_val){
            $date_table = date('Ym',strtotime($cycle_arr_val[0])).'_'.substr($user['regionid'],0,2);//组合表
            if(!table_exist("ttvissue_".$date_table) || !table_exist("tbcissue_".$date_table)){
                continue;
            }
            $report_start_date = strtotime($cycle_arr_val[0]);
            $report_end_date = strtotime($cycle_arr_val[1])+86399;

            /*
* @各媒体监测总情况
*
**/
            $ct_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_count_rate','fmediaclass',false,1,get_check_dates());//传统媒体监测总情况

            /*
* @各地域监测总情况
*
**/
            $every_region_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times','fregionid',false,1,get_check_dates());//各地域监测总情况
            /*
* @各广告类型监测总情况
*
**/
            $every_ad_class_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_times','fcode',false,1,get_check_dates());//各地域监测总情况

            //本级电视
            $hz_tv_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',0);

            //本级广播
            $hz_tb_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',0);

            //本级报纸
            $hz_tp_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',0);

            //下级电视
            $xj_tv_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',2);

            //下级广播
            $xj_tb_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',2);

            //下级报纸
            $xj_tp_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',2);


            //四大类 药品、医疗服务、医疗器械、保健食品
            $hz_zdclass_tv_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,['01','02','13','06'],1);
            $hz_zdclass_tb_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,['01','02','13','06'],1);
            $hz_zdclass_tp_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,['01','02','13','06'],1);



        }

        $ct_jczqk = $this->create_data_hzlist($ct_jczqk_quarter,'fclass');//电视广播报纸汇总数据
        $every_region_jczqk = $this->create_data_hzlist($every_region_jczqk_quarter,'fregionid');//各地域
        $every_ad_class_jczqk = $this->create_data_hzlist($every_ad_class_jczqk_quarter,'fcode');//各类别
        $hz_tv_illegal_ad_data = $this->create_data_hzlist2($hz_tv_illegal_ad_data_quarter,'fad_name');
        $hz_tb_illegal_ad_data = $this->create_data_hzlist2($hz_tb_illegal_ad_data_quarter,'fad_name');
        $hz_tp_illegal_ad_data = $this->create_data_hzlist2($hz_tp_illegal_ad_data_quarter,'fad_name');
        $xj_tv_illegal_ad_data = $this->create_data_hzlist2($xj_tv_illegal_ad_data_quarter,'fad_name');
        $xj_tb_illegal_ad_data = $this->create_data_hzlist2($xj_tb_illegal_ad_data_quarter,'fad_name');
        $xj_tp_illegal_ad_data = $this->create_data_hzlist2($xj_tp_illegal_ad_data_quarter,'fad_name');

        $hz_zdclass_tv_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tv_illegal_ad_data_quarter,'fad_name');
        $hz_zdclass_tb_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tb_illegal_ad_data_quarter,'fad_name');
        $hz_zdclass_tp_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tp_illegal_ad_data_quarter,'fad_name');

        //本级电视
        $hz_tv_illegal_ad_data = $this->get_table_data($hz_tv_illegal_ad_data);
        //本级广播
        $hz_tb_illegal_ad_data = $this->get_table_data($hz_tb_illegal_ad_data);
        //本级报纸
        $hz_tp_illegal_ad_data = $this->get_table_data($hz_tp_illegal_ad_data);
        //下级电视
        $xj_tv_illegal_ad_data = $this->get_table_data($xj_tv_illegal_ad_data);
        //下级广播
        $xj_tb_illegal_ad_data = $this->get_table_data($xj_tb_illegal_ad_data);
        //下级报纸
        $xj_tp_illegal_ad_data = $this->get_table_data($xj_tp_illegal_ad_data);

        /*
        * @各媒体监测总情况
        *
        **/
        $ct_fad_times = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times = 0;//定义传统媒体发布违法广告条次
        $every_media_class_data = [];//定义媒体发布监测情况数组
//循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk as $ct_jczqk_val){
            $ct_fad_times += $ct_jczqk_val['fad_times'];
            $ct_fad_illegal_times += $ct_jczqk_val['fad_illegal_times'];
            $ct_fad_count += $ct_jczqk_val['fad_count'];
            $ct_fad_illegal_count += $ct_jczqk_val['fad_illegal_count'];
            $ct_data_val = [
                $ct_jczqk_val['tmediaclass_name'],
                $ct_jczqk_val['fad_times'],
                $ct_jczqk_val['fad_count'],
                $ct_jczqk_val['fad_illegal_times'],
                $ct_jczqk_val['fad_illegal_count'],
                $ct_jczqk_val['fad_illegal_times_rate'].'%',
                $ct_jczqk_val['fad_illegal_count_rate'].'%'
            ];
            if($ct_jczqk_val['fmedia_class_code'] == '01'){
                $every_media_class_data[0] = $ct_data_val;
            }elseif($ct_jczqk_val['fmedia_class_code'] == '02'){
                $every_media_class_data[1] = $ct_data_val;
            }else{
                $every_media_class_data[2] = $ct_data_val;
            }
        }

        $every_media_class_data[3] = [
            '合计',
            strval($ct_fad_times),
            strval($ct_fad_count),
            strval($ct_fad_illegal_times),
            strval($ct_fad_illegal_count),
            round((($ct_fad_illegal_times)/($ct_fad_times))*100,2).'%',
            round((($ct_fad_illegal_count)/($ct_fad_count))*100,2).'%'
        ];//二、各类媒体广告发布情况
        $every_media_class_data = array_values($every_media_class_data);
        /*
* @各地域监测总情况
*
**/
        $every_region_data = [];//定义各地区发布监测情况数组
//循环赋值
        $every_region_illegal_times_data = [
            [''],
            ['']
        ];
        $every_region_jczqk = pxsf($every_region_jczqk,'fad_illegal_times');
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){
            $every_region_fad_times += $every_region_jczqk_val['fad_times'];
            $every_region_fad_illegal_times += $every_region_jczqk_val['fad_illegal_times'];
            $every_region_fad_count += $every_region_jczqk_val['fad_count'];
            $every_region_fad_illegal_count += $every_region_jczqk_val['fad_illegal_count'];
            if($every_region_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_region_illegal_times_data[0],$every_region_jczqk_val['tregion_name']);
                array_push($every_region_illegal_times_data[1],strval($every_region_jczqk_val['fad_illegal_times']));
            }

            $every_region_data[] = [
                strval($every_region_jczqk_key+1),
                $every_region_jczqk_val['tregion_name'],
                $every_region_jczqk_val['fad_times'],
                $every_region_jczqk_val['fad_count'],
                $every_region_jczqk_val['fad_illegal_times'],
                $every_region_jczqk_val['fad_illegal_count'],
                $every_region_jczqk_val['fad_illegal_times_rate'].'%',
                $every_region_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_region_data[] = [
            '',
            '合计',
            strval($every_region_fad_times),
            strval($every_region_fad_count),
            strval($every_region_fad_illegal_times),
            strval($every_region_fad_illegal_count),
            round((($every_region_fad_illegal_times)/($every_region_fad_times))*100,2).'%',
            round((($every_region_fad_illegal_count)/($every_region_fad_count))*100,2).'%'
        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）



        /*
* @各地域电视广播报纸监测总情况
*
**/
        $every_media_all_data = [];
        $every_media_illegal_times_all_data = [];
        foreach (['01','02','03'] as $every_media_val){
            $every_media_data = [];
            $every_media_illegal_times_data = [];
            $every_media_jczqk = [];
            //循环赋值
            $every_media_fad_times = 0;
            $every_media_fad_illegal_times = 0;
            $every_media_fad_count = 0;
            $every_media_fad_illegal_count = 0;
            $every_media_illegal_times_data = [
                [''],
                ['']
            ];
            $every_media_jczqk_quarter = [];
            foreach ($cycle_arr as $cycle_arr_val){
                $date_table = date('Y',strtotime($cycle_arr_val[0])).'_'.$user['regionid'];//组合表
                if(!table_exist("ttvissue_".$date_table) || !table_exist("tbcissue_".$date_table)){
                    continue;
                }
                $report_start_date = strtotime($cycle_arr_val[0]);
                $report_end_date = strtotime($cycle_arr_val[1])+86399;
                $every_media_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,$every_media_val,'','fad_illegal_times','fmediaid',false,1,get_check_dates());//各地域监测总情况
            }

            $every_media_jczqk = $this->create_data_hzlist($every_media_jczqk_quarter,'fid');//fad_illegal_times
            $every_media_jczqk = pxsf($every_media_jczqk,'fad_illegal_times');//重新排序
            foreach ($every_media_jczqk as $every_media_jczqk_key => $every_media_jczqk_val){
                $every_media_fad_times += $every_media_jczqk_val['fad_times'];
                $every_media_fad_illegal_times += $every_media_jczqk_val['fad_illegal_times'];
                $every_media_fad_count += $every_media_jczqk_val['fad_count'];
                $every_media_fad_illegal_count += $every_media_jczqk_val['fad_illegal_count'];

                if($every_media_jczqk_val['fad_illegal_times'] > 0){
                    array_push($every_media_illegal_times_data[0],$every_media_jczqk_val['tmedia_name']);
                    array_push($every_media_illegal_times_data[1],strval($every_media_jczqk_val['fad_illegal_times']));
                }

                $every_media_data[] = [
                    strval($every_media_jczqk_key+1),
                    $every_media_jczqk_val['tregion_name'],
                    $every_media_jczqk_val['tmedia_name'],
                    $every_media_jczqk_val['fad_times'],
                    $every_media_jczqk_val['fad_count'],
                    $every_media_jczqk_val['fad_illegal_times'],
                    $every_media_jczqk_val['fad_illegal_count'],
                    $every_media_jczqk_val['fad_illegal_times_rate'].'%',
                    $every_media_jczqk_val['fad_illegal_count_rate'].'%'
                ];
            }
            /*            $every_media_data[] = [
                            '',
                            '合计',
                            strval($every_media_fad_times),
                            strval($every_media_fad_count),
                            strval($every_media_fad_illegal_times),
                            strval($every_media_fad_illegal_count),
                            round((($every_media_fad_illegal_times)/($every_media_fad_times))*100,2).'%',
                            round((($every_media_fad_illegal_count)/($every_media_fad_count))*100,2).'%'
                        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）*/
            $every_media_all_data[] = $every_media_data;
            $every_media_illegal_times_all_data[] = $every_media_illegal_times_data;
        }


        $every_media_tv_data = $every_media_all_data[0];//电视各媒体
        $every_media_tb_data = $every_media_all_data[1];//广播各媒体
        $every_media_tp_data = $every_media_all_data[2];//报纸各媒体
        $every_media_illegal_times_tv_data = $every_media_illegal_times_all_data[0];//电视各媒体
        $every_media_illegal_times_tb_data = $every_media_illegal_times_all_data[1];//广播各媒体
        $every_media_illegal_times_tp_data =$every_media_illegal_times_all_data[2];//报纸各媒体





        /*
* @各广告类型监测总情况
*
**/

        $every_ad_class_data = [];//定义各地区发布监测情况数组
//循环赋值
        $every_ad_class_illegal_times_data = [
            [''],
            ['']
        ];
        $every_ad_class_jczqk = pxsf($every_ad_class_jczqk,'fad_illegal_times');//重新排序
        foreach ($every_ad_class_jczqk as $every_ad_class_jczqk_key => $every_ad_class_jczqk_val){
            $every_ad_class_fad_times += $every_ad_class_jczqk_val['fad_times'];
            $every_ad_class_fad_illegal_times += $every_ad_class_jczqk_val['fad_illegal_times'];
            $every_ad_class_fad_count += $every_ad_class_jczqk_val['fad_count'];
            $every_ad_class_fad_illegal_count += $every_ad_class_jczqk_val['fad_illegal_count'];
            if($every_ad_class_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_ad_class_illegal_times_data[0],$every_ad_class_jczqk_val['tadclass_name']);
                array_push($every_ad_class_illegal_times_data[1],strval($every_ad_class_jczqk_val['fad_illegal_times']));
            }

            $every_ad_class_data[] = [
                strval($every_ad_class_jczqk_key+1),
                $every_ad_class_jczqk_val['tadclass_name'],
                $every_ad_class_jczqk_val['fad_times'],
                $every_ad_class_jczqk_val['fad_count'],
                $every_ad_class_jczqk_val['fad_illegal_times'],
                $every_ad_class_jczqk_val['fad_illegal_count'],
                $every_ad_class_jczqk_val['fad_illegal_times_rate'].'%',
                $every_ad_class_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_ad_class_data[] = [
            '',
            '合计',
            strval($every_ad_class_fad_times),
            strval($every_ad_class_fad_count),
            strval($every_ad_class_fad_illegal_times),
            strval($every_ad_class_fad_illegal_count),
            round((($every_ad_class_fad_illegal_times)/($every_ad_class_fad_times))*100,2).'%',
            round((($every_ad_class_fad_illegal_count)/($every_ad_class_fad_count))*100,2).'%'
        ];


        $typical_illegal_ad_list_data = [];
        if(!empty($hz_zdclass_tv_illegal_ad_data) || !empty($hz_zdclass_tb_illegal_ad_data) || !empty($hz_zdclass_tp_illegal_ad_data)){
            $typical_illegal_ad_list = array_merge($hz_zdclass_tv_illegal_ad_data,$hz_zdclass_tb_illegal_ad_data,$hz_zdclass_tp_illegal_ad_data);
            $illegal_ad_list_data = [];
            $typical_illegal_ad_list = pxsf($typical_illegal_ad_list,'illegal_times');//排序
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_val){
                $md5 = MD5($typical_illegal_ad_list_val['fad_name'].$typical_illegal_ad_list_val['fmedianame']);
                if(isset($illegal_ad_list_data[$md5])){
                    $illegal_ad_list_data[$md5]['illegal_times'] += $typical_illegal_ad_list_val['illegal_times'];
                    $illegal_ad_list_data[$md5]['illegal_times'] = strval($illegal_ad_list_data[$md5]['illegal_times']);
                }else{

                    $illegal_ad_list_data[$md5] = $typical_illegal_ad_list_val;
                }
            }

            $typical_illegal_ad_list = array_values($illegal_ad_list_data);
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_key => $typical_illegal_ad_list_val){
                $typical_illegal_ad_list_data[] = [
                    strval($typical_illegal_ad_list_key+1),
                    $typical_illegal_ad_list_val['fmedianame'],
                    $typical_illegal_ad_list_val['fad_name'],
                    $typical_illegal_ad_list_val['fadclass'],
                    $typical_illegal_ad_list_val['illegal_times']
                ];
            }
        }



//定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "大标题楷体60",
            "text" => "广告监测年报"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号加粗",
            "text" => $user['regulatorname']
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号加粗带红线",
            "text" => "广告监测中心               ".date('Y年m月d日')
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $user['regulatorname']."广告监测中心对".$date_str.$r_name."部分电视、广播、报刊等".$ct_media_count."家媒体发布广告的情况进行了抽查监测，现将监测情况通报如下："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "一、监测范围及对象"
        ];
        if($ct_media_tv_count_hz != 0){
            $hz_media_count_str .= $ct_media_tv_count_hz."个电视媒体；";
        }
        if($ct_media_bc_count_hz != 0){
            $hz_media_count_str .= $ct_media_bc_count_hz."个广播媒体；";
        }
        if($ct_media_paper_count_hz != 0){
            $hz_media_count_str .= $ct_media_paper_count_hz."个报纸媒体；";
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）".$dq_name."媒体：".$hz_media_count_str
        ];
        if($ct_media_tv_count_xj != 0){
            $xj_media_count_str .= $ct_media_tv_count_xj."个电视媒体；";
        }
        if($ct_media_bc_count_xj != 0){
            $xj_media_count_str .= $ct_media_bc_count_xj."个广播媒体；";
        }
        if($ct_media_paper_count_xj != 0){
            $xj_media_count_str .= $ct_media_paper_count_xj."个报纸媒体；";
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）其他".$tregion_count."个区、县：".$xj_media_count_str
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "二、各类媒体广告发布情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "本年度共监测广告".$every_media_class_data[3][1]."条次，".$every_media_class_data[3][2]."条数，其中监测发现各类涉嫌违法广告".$every_media_class_data[3][3]."条次，".$every_media_class_data[3][4]."条数。具体情况详见下表："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒介类型汇总表格hz",
            "data" => $every_media_class_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "三、市（各区县）媒体广告发布总体情况"
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "地域排名汇总表格hz",
            "data" => $every_region_data
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各市区县违法广告发布量排名条形图hz",
            "data" => $every_region_illegal_times_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "四、全市电视、广播、报纸广告发布情况（排名按发布违法广告条次由高到底排列）"
        ];

        if(!empty($every_media_tv_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "1、电视"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tv_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tv_data
            ];
        }
        if(!empty($every_media_tb_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "2、广播"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tb_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tb_data
            ];
        }
        if(!empty($every_media_tp_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "3、报纸"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tp_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tp_data
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、全市各类媒体发布的违法广告（全部类别）通报"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）市级媒体"
        ];


        if(!empty($hz_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tv_illegal_ad_data)
            ];
        }
        if(!empty($hz_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tb_illegal_ad_data)
            ];
        }
        if(!empty($hz_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tp_illegal_ad_data)
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）区（县）级媒体"
        ];

        if(!empty($xj_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tv_illegal_ad_data)
            ];
        }
        if(!empty($xj_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tb_illegal_ad_data)
            ];
        }

        if(!empty($xj_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tp_illegal_ad_data)
            ];
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "六、全市各类违法广告比重结构图"
        ];
        if(!empty($every_ad_class_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表hz",
                "data" => $every_ad_class_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "各大类别广告违法占比表hz",
                "data" => $every_ad_class_illegal_times_data
            ];

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "七、药品、医疗服务、医疗器械、保健食品违法广告发布情况"
        ];

        if(!empty($typical_illegal_ad_list_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $typical_illegal_ad_list_data
            ];
        }

        $report_data = json_encode($data);
        //echo $report_data;exit;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $system_num = getconfig('system_num');

            //将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].$date_str.'年度报告';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 80;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',strtotime($cycle_arr[0][1]));
            $data['pnendtime'] 		    = date('Y-m-d',strtotime($cycle_arr[11][1]));
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }



    }


    /*生成图表数据*/
    function create_chart_data($array,$index1 = 1,$index2 = 2,$index3 = 3,$index4 = 4){
        $array1[] = ["类型","发布量","违法发布量","违法率"];
        $array2[] = ["类型","发布量"];
        $array3[] = ["类型","违法发布量"];
        foreach ($array as $array_key=>$array_val){
            if($array_val[3] != 0){
                $array1[] = [
                    $array_val[$index1],
                    $array_val[$index2],
                    $array_val[$index3],
                    $array_val[$index4]
                ];
                $array2[] = [
                    $array_val[$index1],
                    $array_val[$index2]
                ];
                $array3[] = [
                    $array_val[$index1],
                    $array_val[$index3]
                ];
            }
        }
        return [$array1,$array2,$array3];
    }

    /*生成互联网图表数据*/
    function create_net_chart_data($array){
        if(empty($array)){
            return [[],[]];
        }
        $array_data2[] = ["网站","广告量"];

        foreach ($array as $array_key=>$array_val){
            $array_data1[] = [
                $array_val[0],
                $array_val[1],
                $array_val[2],
                $array_val[3],
                $array_val[4]
            ];
            $array_data2[] = [
                $array_val[1],
                $array_val[2]
            ];
        }
        return [$array_data1,$array_data2];
    }

    public function create_data_hzlist($ct_jczqk_quarter,$group){
        $ct_jczqk = [];
        foreach ($ct_jczqk_quarter as $ct_jczqk_quarter_val){
            foreach ($ct_jczqk_quarter_val as $ct_jczqk_quarter_val2){
                $fclass_md5 = '';
                $fclass_md5 = MD5($ct_jczqk_quarter_val2[$group]);
                if(isset($ct_jczqk[$fclass_md5])){
                    $ct_jczqk[$fclass_md5]['fad_times'] +=  $ct_jczqk_quarter_val2['fad_times'];
                    $ct_jczqk[$fclass_md5]['fad_times'] = strval($ct_jczqk[$fclass_md5]['fad_times']);
                    $ct_jczqk[$fclass_md5]['fad_count'] +=  $ct_jczqk_quarter_val2['fad_count'];
                    $ct_jczqk[$fclass_md5]['fad_count'] = strval($ct_jczqk[$fclass_md5]['fad_count']);
                    $ct_jczqk[$fclass_md5]['fad_illegal_times'] +=  $ct_jczqk_quarter_val2['fad_illegal_times'];
                    $ct_jczqk[$fclass_md5]['fad_illegal_times'] = strval($ct_jczqk[$fclass_md5]['fad_illegal_times']);
                    $ct_jczqk[$fclass_md5]['fad_illegal_count'] +=  $ct_jczqk_quarter_val2['fad_illegal_count'];
                    $ct_jczqk[$fclass_md5]['fad_illegal_count'] = strval($ct_jczqk[$fclass_md5]['fad_illegal_count']);
                    $ct_jczqk[$fclass_md5]['fad_illegal_times_rate'] =  round((($ct_jczqk[$fclass_md5]['fad_illegal_times'])/($ct_jczqk[$fclass_md5]['fad_times']))*100,2);
                    $ct_jczqk[$fclass_md5]['fad_illegal_count_rate'] =  round((($ct_jczqk[$fclass_md5]['fad_illegal_count'])/($ct_jczqk[$fclass_md5]['fad_count']))*100,2);
                }else{
                    $ct_jczqk[$fclass_md5] = $ct_jczqk_quarter_val2;
                }
            }
        }
        $ct_jczqk = array_values($ct_jczqk);
        return $ct_jczqk;
    }

    public function create_data_hzlist2($ct_jczqk_quarter){
        $ct_jczqk = [];
        foreach ($ct_jczqk_quarter as $ct_jczqk_quarter_val){
            foreach ($ct_jczqk_quarter_val as $ct_jczqk_quarter_val2){
                $fclass_md5 = '';
                $fclass_md5 = MD5($ct_jczqk_quarter_val2['fmedianame'].$ct_jczqk_quarter_val2['fadclass'].$ct_jczqk_quarter_val2['fad_name']);
                if(isset($ct_jczqk[$fclass_md5])){
                    $ct_jczqk[$fclass_md5]['illegal_times'] +=  $ct_jczqk_quarter_val2['illegal_times'];
                    $ct_jczqk[$fclass_md5]['illegal_times'] = strval($ct_jczqk[$fclass_md5]['illegal_times']);
                    /*                    $ct_jczqk[$fclass_md5]['illegal_count'] +=  $ct_jczqk_quarter_val2['illegal_count'];
                                        $ct_jczqk[$fclass_md5]['illegal_count'] = strval($ct_jczqk[$fclass_md5]['illegal_count']);*/
                }else{
                    $ct_jczqk[$fclass_md5] = $ct_jczqk_quarter_val2;
                }
            }
        }
        $ct_jczqk = array_values($ct_jczqk);
        return $ct_jczqk;
    }

    public function get_table_data($data){
        $data_array = [];
        foreach ($data as $data_val){
            $data_array[] = array_values($data_val);
        }
        return $data_array;

    }

    public function pmaddkey($data,$px_num=3){
        if($px_num){
            $data = pxsf($data,$px_num);
        }
        $res = [];
        foreach ($data as $data_key=>$data_val){
            $array_res = [
                strval($data_key+1),
            ];
            foreach ($data_val as $data_val2){
                $array_res[] = $data_val2;
            }
            $res[] = $array_res;
        }
        return $res;
    }

    /**
     * 获取发布表名
     * by zw
     */
    public function gettable($tb,$tm,$regionid) {
        if(!empty($regionid)){
            $provinceId 	= substr($regionid,0,2);
        }else{
            $provinceId 	= substr(session('regulatorpersonInfo.regionid'),0,2);
        }
        if($tb == 'tv'){
            return 'ttvissue_' . date('Ym',$tm) . '_' . $provinceId;
        }elseif($tb == 'bc'){
            return 'tbcissue_' . date('Ym',$tm) . '_' . $provinceId;
        }elseif($tb == 'paper'){
            return 'tpaperissue_' . date('Ym',$tm) . '_' . $provinceId;
        }
    }

    /**
     * 周预警报表生成
     * by zw
     */
    public function create_yujingreport(){
        session_write_close();
        header("Content-type: text/html; charset=utf-8");
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        $sheet ->setCellValue('A1','序号');
        $sheet ->setCellValue('B1','广告名称');
        $sheet ->setCellValue('C1','广告类别');
        $sheet ->setCellValue('D1','媒体类型');
        $sheet ->setCellValue('E1','违法表现');
        $sheet ->setCellValue('F1','涉嫌违法内容');

        $timestamp = time();
        $starttime = I('starttime')?I('starttime'):date('Y-m-d', strtotime("last week Monday", $timestamp));//周开始时间
        $endtime = I('endtime')?I('endtime'):date('Y-m-d', strtotime("last week Sunday", $timestamp));//周结束时间，默认上周

        $fadclasscode = ['01','02','03','04','05'];//生成的广告大类

        $arr_code = [];
        if(is_array($fadclasscode)){
            foreach ($fadclasscode as $key => $value) {
                $codes = D('Function')->get_next_tadclasscode($value,true);//获取下级广告类别代码
                if(!empty($codes)){
                    $arr_code = array_merge($arr_code,$codes);
                }else{
                    array_push($arr_code,$value);
                }
            }
            $where_tad = ' and c.fadclasscode  in("'.implode('","', $arr_code).'")';
        }

        //电视数据输出
        $tvsql = 'SELECT *
            FROM (
            SELECT c.fadname,b.fillegalcontent,b.fexpressions,d.ffullname
            FROM ttvissue a
            INNER JOIN ttvsample b ON a.ftvsampleid=b.fid
            INNER JOIN tad c ON b.fadid=c.fadid '.$where_tad.'
            inner join tadclass d on c.fadclasscode=d.fcode
            WHERE a.fissuedate BETWEEN "'.$starttime.'" AND "'.$endtime.'" AND fillegaltypecode>0
            GROUP BY a.ftvsampleid,c.fadname,b.fillegalcontent,b.fexpressions,d.ffullname) a
            GROUP BY fadname';
        $tvdata = M()->query($tvsql);

        $nowline = 0;
        foreach ($tvdata as $key => $value) {
            $sheet ->setCellValue('A'.($nowline+2),$nowline+1);
            $sheet ->setCellValue('B'.($nowline+2),$value['fadname']);
            $sheet ->setCellValue('C'.($nowline+2),$value['ffullname']);
            $sheet ->setCellValue('D'.($nowline+2),'电视');
            $sheet ->setCellValue('E'.($nowline+2),$value['fexpressions']);
            $sheet ->setCellValue('F'.($nowline+2),$value['fillegalcontent']);
            $nowline += 1;
        }

        //广播数据输出
        $bcsql = 'SELECT *
            FROM (
            SELECT c.fadname,b.fillegalcontent,b.fexpressions,d.ffullname
            FROM tbcissue a
            INNER JOIN tbcsample b ON a.fbcsampleid=b.fid
            INNER JOIN tad c ON b.fadid=c.fadid '.$where_tad.'
            inner join tadclass d on c.fadclasscode=d.fcode
            WHERE a.fissuedate BETWEEN "'.$starttime.'" AND "'.$endtime.'" AND fillegaltypecode>0
            GROUP BY a.fbcsampleid,c.fadname,b.fillegalcontent,b.fexpressions,d.ffullname) a
            GROUP BY fadname';
        $bcdata = M()->query($bcsql);

        foreach ($bcdata as $key => $value) {
            $sheet ->setCellValue('A'.($nowline+2),$nowline+1);
            $sheet ->setCellValue('B'.($nowline+2),$value['fadname']);
            $sheet ->setCellValue('C'.($nowline+2),$value['ffullname']);
            $sheet ->setCellValue('D'.($nowline+2),'广播');
            $sheet ->setCellValue('E'.($nowline+2),$value['fexpressions']);
            $sheet ->setCellValue('F'.($nowline+2),$value['fillegalcontent']);
            $nowline += 1;
        }

        //报纸数据输出
        $papersql = 'SELECT *
            FROM (
            SELECT c.fadname,b.fillegalcontent,b.fexpressions,d.ffullname
            FROM tpaperissue a
            INNER JOIN tpapersample b ON a.fpapersampleid=b.fpapersampleid
            INNER JOIN tad c ON b.fadid=c.fadid '.$where_tad.'
            inner join tadclass d on c.fadclasscode=d.fcode
            WHERE a.fissuedate BETWEEN "'.$starttime.'" AND "'.$endtime.'" AND fillegaltypecode>0
            GROUP BY a.fpapersampleid,c.fadname,b.fillegalcontent,b.fexpressions,d.ffullname) a
            GROUP BY fadname';
        $paperdata = M()->query($papersql);

        foreach ($paperdata as $key => $value) {
            $sheet ->setCellValue('A'.($nowline+2),$nowline+1);
            $sheet ->setCellValue('B'.($nowline+2),$value['fadname']);
            $sheet ->setCellValue('C'.($nowline+2),$value['ffullname']);
            $sheet ->setCellValue('D'.($nowline+2),'报纸');
            $sheet ->setCellValue('E'.($nowline+2),$value['fexpressions']);
            $sheet ->setCellValue('F'.($nowline+2),$value['fillegalcontent']);
            $nowline += 1;
        }

        $objActSheet =$objPHPExcel->getActiveSheet();

        //给当前活动的表设置名称
        $objActSheet->setTitle(session('regulatorpersonInfo.regionname1').'违法广告预警列表');

        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $date_now = $date;
        $date = date('Ymdhis');
        $savefile = './Public/Xls/'.$date.'.xlsx';
        $objWriter->save($savefile);

        $ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='.session('regulatorpersonInfo.regionname1').$starttime.'至'.$endtime.'违法广告预警报表.xlsx'));//上传云
        unlink($savefile);//删除文件
        echo "<a href='".$ret['url']."' target='_blank'>".session('regulatorpersonInfo.regionname1').$starttime.'至'.$endtime."违法广告预警报表.xlsx</a>";
    }


}