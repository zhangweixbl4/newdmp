<?php
namespace Open\Controller;
use Think\Controller;

class TaskUserController extends BaseController {

    /**
     * 根据token获取用户信息或者返回错误信息
     * @param $token
     */
    public function checkUserToken($token = 0)
    {

        $wx_id = S('task_urer_'.$token);
        if (!$wx_id){
            $msg = '未找到用户信息, 请返回原页面刷新后重新跳转';
            $state = -1;
            $data = [];
        }else{
            S('task_urer_'.$token, null);
            $userInfo = M('ad_input_user')
                ->field('ad_input_user.nickname, ad_input_user.wx_id, ad_input_user.headimgurl, ad_input_user.remark, ad_input_user.input_seniority, ad_input_user.audit_seniority, ad_input_user.cutting_seniority, ad_input_user.video_cutting_token, ad_input_user.video_cutting_login_state, ad_input_user_group.group_name, ad_input_user_group.mediaauthority, ad_input_user_group.classauthority, ad_input_user_group.dateauthority, ad_input_user_group.other_authority')
                ->join('ad_input_user_group on ad_input_user.group_id = ad_input_user_group.group_id','left')
                ->where(['ad_input_user.wx_id' => ['EQ', $wx_id]])
                ->find();
            if (!$userInfo){
                $msg = '未找到该用户';
                $state = -1;
                $data = [];
            }else{
                $msg = '验证成功';
                $state = 0;
                $data = $userInfo;
            }
        }
        $this->ajaxReturn(compact('msg', 'state', 'data'));

    }
}