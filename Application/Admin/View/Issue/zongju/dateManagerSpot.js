const dateManager = {
    props: ['task-type', 'fid'],
    template: `<div  v-loading="loading">
    <div>
        <el-cascader
                v-model="region"
                :options="regionTree"
                change-on-select
                :filterable="true"
                clearable
                placeholder="地区"
        ></el-cascader>
    </div>
    <hr>
    <!--先选择月份-->
    <div v-if="!pickerMonth && taskType === 'add'">
        <el-row>
            <el-col :span="8" :offset="8">
                <h4> 请选择要抽查的月份</h4>
                <el-date-picker
                        v-model="pickerMonth"
                        type="month"
                        placeholder="选择月">
                </el-date-picker>
            </el-col>
        </el-row>
    </div>
    <!--后具体选择日期-->
    <div v-else>
        <el-row align="middle">
            <el-col :span="8">
                <div class="datePicker">
                    <el-button :type="item.selected ? 'success' : 'info'" v-for="item in dateList" :key="item.day" @click="clickDay(item.day)"  :class="{ hideBtn: !item.day }" :disabled="item.disable">{{item.day}}</el-button>
                </div>
            </el-col>
            <el-col :span="14">
                <el-card class="box-card">
                    <div v-for="(date,index) in activeDates" :key="index" class="dateText" @click="deleteDate(index)" title="点击删除" style="cursor: pointer">
                        {{date | dateFilter(pickerMonth)}}
                    </div>
                </el-card>
            </el-col>
        </el-row>
        <div style="margin: 15px 0;" class="text-center">
            <el-button type="primary" @click="makeTask" v-if="taskType==='add'">生成抽查任务</el-button>
            <el-button type="primary" @click="editTask" v-else>修改抽查任务</el-button>
        </div>
    </div>
</div>
`,
    data: function () {
        return {
            regionTree,
            pickerMonth: '',
            classActive: '电视',
            loading: false,
            region: [],
            activeDates: []
        }
    },
    computed: {
        month: function () {
            try {
                return this.pickerMonth.getMonth()
            }catch (e) {

            }
        },
        maxDay: function () {
            return moment(this.pickerMonth).daysInMonth()
        },
        dateList: function () {
            var a = []
            for (var i = 1; i <= this.maxDay; i++){
                var num = i
                if (num < 10){
                    num = '0'+num
                } else {
                    num += ''
                }
                if (this.activeDates.indexOf(num) !== -1){
                    a.push({
                        day: num,
                        selected: true
                    })
                }else {
                    a.push({
                        day: num,
                        selected: false
                    })
                }
            }
            let tempWeekDay = moment(this.pickerMonth).day()
            if (tempWeekDay === 0){
                tempWeekDay = 6
            }else{
                tempWeekDay -= 1
            }
            for (let i = 0; i < tempWeekDay; i++){
                a.unshift({
                    disable: true,
                    day: '',
                    selected: false,
                })
            }
            const dayArr = [
                {
                    day: '周一',
                    selected: false,
                    disable: true,
                },
                {
                    day: '周二',
                    selected: false,
                    disable: true,
                },
                {
                    day: '周三',
                    selected: false,
                    disable: true,
                },
                {
                    day: '周四',
                    selected: false,
                    disable: true,
                },
                {
                    day: '周五',
                    selected: false,
                    disable: true,
                },
                {
                    day: '周六',
                    selected: false,
                    disable: true,
                },
                {
                    day: '周日',
                    selected: false,
                    disable: true,
                },

            ]
            a = dayArr.concat(a)
            return a
        }
    },
    methods: {
        getInfo: function () {
            this.loading = true
            $.post('getSpotCheckTask', {id: this.fid}).then((res) => {
                if (res.code != 0){
                    this.$message.error(res.msg)
                    this.$emit('close-condition')
                } else{
                    this.activeDates = res.data.condition.split(',')
                    this.pickerMonth = new Date(res.data.fmonth)
                    this.loading = false
                }
            })
        },
        dataInit:function () {
            this.activeDates = []
            this.pickerMonth = ''
            this.region = []
            this.classActive = '电视'
        },
        clickDay: function (index) {
            if (index === undefined){
                return false
            }
            var dates = this.activeDates
            var i = dates.indexOf(index)
            if (i === -1){
                dates.push(index)
            } else {
                dates.splice(i, 1)
            }
        },
        deleteDate: function (index) {
            var dates  = this.activeDates
            dates.splice(index, 1)
        },
        makeTask: function () {
            if (this.region.length === 0) {
                this.$message.error('地区不可为空')
                return false
            }
            this.loading = true
            $.post('addSpotCheckTask', {
                condition: this.activeDates.sort().join(','),
                fmonth: moment(this.pickerMonth).format('YYYY-MM-DD'),
                region: this.region[this.region.length - 1]
            }).then((res) => {
                if (res.code == 0) {
                    this.$message.success(res.msg)
                    this.$emit('close-condition')
                    this.dataInit()
                }else {
                    this.$message.error(res.msg)
                }
                this.loading = false
            })
        },
        editTask: function () {
            this.loading = true
            $.post('editSpotCheckTask', {
                condition: this.activeDates.sort().join(','),
                fid: this.fid,
            }).then((res) => {
                if (res.code == 0) {
                    this.$message.success(res.msg)
                    this.$emit('close-condition')
                    this.dataInit()
                }else {
                    this.$message.error(res.msg)
                }
                this.loading = false
            })
        }
    },
    filters: {
        dateFilter: function (value, month) {
            if (value){
                return moment(month).format('YYYY-MM-') + value
            }
            return ''
        }
    }
}