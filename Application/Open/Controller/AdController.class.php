<?php
namespace Open\Controller;
use Think\Controller;
use OpenSearch\Client\OpenSearchClient;
use OpenSearch\Client\DocumentClient;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;

class AdController extends BaseController {
	


	public function add_edit_ad(){
		$ad_data = file_get_contents('php://input');//获取post数据
		$ad_data = json_decode($ad_data,true);//把json转化成数组
		//var_dump($ad_data);
		$fadid = intval($ad_data['fadid']);//广告ID
		
		$a_e_data = array();
		$a_e_data['fadname'] = $ad_data['fadname'];//广告名称
		if($a_e_data['fadname'] == '') $this->ajaxReturn(array('code'=>10015,'msg'=>'添加广告失败,广告名称不能为空'));
		
		$a_e_data['fbrand'] = $ad_data['fbrand'];//广告品牌
		if($a_e_data['fbrand'] == '') $this->ajaxReturn(array('code'=>10015,'msg'=>'添加广告失败,品牌不能为空'));
		
		$a_e_data['fadclasscode'] = $ad_data['fadclasscode'];//广告分类代码
		$adclass_info = M('tadclass')->where(array('fcode'=>$a_e_data['fadclasscode']))->find();
		if(!$adclass_info) $this->ajaxReturn(array('code'=>10016,'msg'=>'添加广告失败,广告类别有错误'));
		
		$a_e_data['fadowner'] = $ad_data['fadowner'];//广告主ID
		$adowner_info = M('tadowner')->where(array('fid'=>$a_e_data['fadowner']))->find();
		if(!$adowner_info) $this->ajaxReturn(array('code'=>10016,'msg'=>'添加广告失败,广告主有错误'));
		
		$a_e_data['fstate'] = $ad_data['fstate'];//状态
		$person_info = $GLOBALS['person_info'];
		
		if($fadid == 0 ){
			$a_e_data['fcreator'] = $person_info['fname'];//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = $person_info['fname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			$rr = M('tad')->add($a_e_data);
			if($rr > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'添加广告成功','fadid'=>$rr));
			if($rr == 0) $this->ajaxReturn(array('code'=>10011,'msg'=>'添加广告失败,原因未知'));
		}else{
			$a_e_data['fmodifier'] = $person_info['fname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tad')->where(array('fadid'=>$fadid))->save($a_e_data);
			if($rr > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'修改广告成功'));
			if($rr == 0) $this->ajaxReturn(array('code'=>10012,'msg'=>'修改广告失败,原因未知'));
		}
		
		
	
	}
	
	
	
	/*获取广告列表（联想）*/
	public function get_ad(){
		
		$ad_data = file_get_contents('php://input');//获取post数据
		$ad_data = json_decode($ad_data,true);//把json转化成数组
		
		$client = A('Common/OpenSearch','Model')->client();//获得OpenSearchClient
		$searchClient = new SearchClient($client);

		$params = new SearchParamsBuilder();
		$params->setStart(0);//起始位置

		$params->setHits(10);//返回数量

		$params->setAppName('ad_name_search');//设置应用名称
		
		
		$where = array();
		
		$where['fadname'] = $ad_data['fadname'];
		
		$setQuery = A('Common/OpenSearch','Model')->setQuery($where);//组装搜索字句
		
		//var_dump($setQuery);
		
		$params->setQuery($setQuery);//设置搜索
		
		


		//var_dump($setFilter);
		
		$params->addSort('fsort', SearchParamsBuilder::SORT_DECREASE);//排序

		$params->setFormat("json");//数据返回json格式
		
		$params->setFetchFields(array(
										'identify',//识别码（哈希）
										'fadname',
									));//设置需返回哪些字段
		
		
		$ret = $searchClient->execute($params->build());//执行查询并返回信息
		

		$result = json_decode($ret->result,true);//
		
		$adList0 = M('tad')
						->cache(true,600)
						->field('	fadid,
									fadname,
									fbrand,
									fadclasscode,
									fadclasscode_v2,
									fadowner,
									(select fname from tadowner where fid = tad.fadowner) as adowner_name
								')
						->where(array('tad.fadname'=>$ad_data['fadname']))
                        ->find();
		
		
		$adNameList = [];
		if ($adList0) $adNameList[] = $adList0;
		foreach($result['result']['items'] as $items){
			if ($ad_data['fadname'] == $items['fadname']) continue;
			$adInfo = M('tad')->cache(true,600)
										->field('	fadid,
													fadname,
													fbrand,
													fadclasscode,
													fadclasscode_v2,
													fadowner,
													(select fname from tadowner where fid = tad.fadowner) as adowner_name
												')
										->where(array('fadname'=>$items['fadname']))
										->find();
			if ($adInfo) $adNameList[] = $adInfo;
			
		}
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','adList'=>$adNameList,'count'=>0));
		
		
	}
	
	/*获取广告*/
	public function get_ad_20190812(){
		$ad_data = file_get_contents('php://input');//获取post数据
		$ad_data = json_decode($ad_data,true);//把json转化成数组
		
		$where = array();
		$where['is_sure'] = ['NEQ', 0];
		
		
		
		if($ad_data['fadid'] != ''){
			$where['tad.fadid'] = $ad_data['fadid'];//广告id
		}	
		if($ad_data['fadname'] != ''){
			$where_fadname = str_replace(array(' ','	'),'%',$ad_data['fadname']);
			
			$where['tad.fadname'] = array('like','%'.$where_fadname.'%');//广告名称
			
			$adList0 = M('tad')
						->cache(true,90)
						->field('	fadid,
									fadname,
									fbrand,
									fadclasscode,
									fadclasscode_v2,
									fadowner,
									(select fname from tadowner where fid = tad.fadowner) as adowner_name
								')
						->where(array('tad.fadname'=>$where_fadname))
                        ->find();
			
		}	 

		$adList1 = M('tad')
						->cache(true,60)
						->field('
									fadid,
									fadname,
									fbrand,
									fadclasscode,
									fadclasscode_v2,
									fadowner,
									(select fname from tadowner where fid = tad.fadowner) as adowner_name
								')
						
						->where($where)
                        
                        ->limit(12)
                        ->select();
		
		$adList = array();
		if($adList0) $adList[] = $adList0;
		
		foreach($adList1 as $ad){
			if($ad['fadname'] != $where_fadname ) $adList[] = $ad;
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'','adList'=>$adList,'count'=>0));
		

	}


    public function adSummaryDay()
    {
        $where = [];
        $month = I('month');
        if (!$month){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '月份(month)为必填项'
            ]);
        }else{
            if (strlen($month) != 7){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '月份格式不合法, 正确形式为2018-07'
                ]);
            }
        // $where['summary.fdate'] = ['LIKE', $month.'%'];
        }
        $mediaClass = I('type');
        if ($mediaClass != 0 && $mediaClass){
            if ($mediaClass != 1 && $mediaClass != 2 && $mediaClass != 3){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '媒体类型错误, 只允许为1, 2 , 3'
                ]);
            }
            $where['left(fmediaclassid, 2)'] = ['EQ', '0'.$mediaClass];
        }else{
            $where['left(fmediaclassid, 2)'] = ['IN', '01,02,03'];

        }
        $where['tmedia.priority'] = ['EGT', 0];
        $regionId = I('region');
        if ($regionId){
            $len = strlen($regionId);
            $where['fmediaownerid'] = ['EXP', " IN (select fid from tmediaowner where left(fregionid, $len) = $regionId )"];
        }
        $mediaId = I('channel');
        if ($mediaId){
            $where['tmedia.fid'] = ['EQ', $mediaId];
            unset($where['fmediaownerid'] );
            $where['left(fmediaclassid, 2)'] = ['IN', '01,02,03'];

        }
        $data = M('tmedia')
            ->field('
            substring(fmediaclassid, 2, 1) type, 
            fid channel, 
            fmedianame `name`
            ')
            ->where($where)
            ->order('substring(tmedia.fmediaclassid, 2, 1) asc')
            ->select();
        // 查到这个月有几天
        $tempDayArr = range(0, 30);
        $dayArr = [];
        foreach ($tempDayArr as $key => $value) {
            $dayArr[strval($key)] = 0;
        }
        foreach ($data as $key => $value) {
            $summaryWhere = [
                'fmediaid' => ['EQ', $value['channel']],
                'fdate' => ['LIKE',  $month.'%']
            ];
            $data[$key]['data'] = [];
            $dayCount = M('tbn_ad_summary_day')->field('sum(fad_times) `sum`, day(fdate) `day`')->where($summaryWhere)->group('fdate')->select();
            // 取得一个不完整的数组, 统计广告次数
            $adTimesArr = [];
            foreach ($dayCount as $datum) {
                $dayIndex = intval($datum['day']) - 1;
                $adTimesArr[$dayIndex] = intval($datum['sum']);
            }

            foreach ($dayArr as $index => $zero) {
                if (!isset($adTimesArr[$index])){
                    $adTimesArr[$index] = 0;
                }
            }
            ksort($adTimesArr);
            $adTimesArr = array_values($adTimesArr);
            $data[$key]['data'] = $adTimesArr;

            // 如果是报纸媒体, 也统计版面数量
            if ($value['type'] === '3'){
                $paperCountArr = M('tpapersource')
                    ->field('day(fissuedate) `day`, count(1) `count`')
                    ->where([
                        'fstate' => ['EQ', 2],
                        'fmediaid' => ['EQ', $value['channel']],
                        'fissuedate' => ['LIKE', $month.'%']
                    ])
                    ->group('fissuedate')
                    ->select();
                $adTimesArr = [];
                foreach ($paperCountArr as $datum) {
                    $dayIndex = intval($datum['day']) - 1;
                    $adTimesArr[$dayIndex] = intval($datum['count']);
                }
                foreach ($dayArr as $index => $zero) {
                    if (!isset($adTimesArr[$index])){
                        $adTimesArr[$index] = 0;
                    }
                }
                ksort($adTimesArr);
                $adTimesArr = array_values($adTimesArr);
                $data[$key]['page'] = $adTimesArr;
            }

        }
        $this->ajaxReturn([
            'dataList' => $data,
            'code' => 0,
            'msg' => '成功'
        ]);
    }


}