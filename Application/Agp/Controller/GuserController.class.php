<?php
namespace Agp\Controller;
use Think\Controller;

class GuserController extends BaseController
{
	/*
	*添加用户
	*/

	public function addUser(){
		$system_num = getconfig('system_num');
		
		if(session('regulatorpersonInfo.fisadmin') != 1){
			$this->ajaxReturn(array('code'=>1,'msg'=>'您无权操作'));
		}
		
		$tregionid = I('tregionid');//行政区划id

		$ftre = M('tregulator')->field('fid,fcode')->where(array('fregionid'=>$tregionid,'fstate'=>1,'fkind'=>1,'ftype'=>20))->find();//监管机构ID
		if(empty($ftre)){
	      $this->ajaxReturn(array('code'=>1,'msg'=>'该区域还未建立相应机构'));
	    }
		//判断创建用户是否到达上线
		$count = M('tregulatorperson')->where(array('fmediaownerid'=>$ftre['fid'],'fregionid'=>$tregionid))->count();
		if($count>=4){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败，添加用户已达上线'));
		}

		$name = I('name');
		$map['fcode|fmobile|fmail'] = $name;
		//判断用户名是否已经存在
		$result = M('tregulatorperson')->where($map)->find();
		if($result){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败，用户名已经存在'));
		}
		$password = I('password');
		if(strlen($password)<6){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'密码长度不得小于6位'));
		}
		if(empty($password)){
			$pwd = md5('123456');
		}else{
			$pwd = md5($password);
		}
		$data['fregulatorid'] = $ftre['fid'];//监管机构ID
		$data['fcode'] = $name;//人员代码
		$data['fname'] = $name;//姓名
		$data['fpassword'] = $pwd;//密码
		$data['fpassword2'] = md5($pwd);//密码
		$data['fcreator'] = session('regulatorpersonInfo.fname');//创建人
		$data['fregionid'] = $tregionid;//行政区划id
		$data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$data['fstate'] = 1;//状态

		$res = M('tregulatorperson')->add($data);

		if($res){
			//设置用户分组
			$data_pl['fpersonid'] = $res;
			$data_pl['fcustomer'] = $system_num;
			$data_pl['fcreator'] = session('regulatorpersonInfo.fid');
			$data_pl['fcreatetime'] = date('Y-m-d H:i:s');
			$data_pl['fstate'] = 1;
			M('tpersonlabel') -> add($data_pl);

			D('Function')->write_log('用户管理',1,'添加成功','tregulatorperson',$res,M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
		}else{
			D('Function')->write_log('用户管理',0,'添加失败','tregulatorperson',0,M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败'));
		}
	}

	/*
	*账户信息
	*/
	public function accountInfo(){
		$use_id = session('regulatorpersonInfo.fid');//用户id
		$res = M('tregulatorperson')->field('fid,fcode,fpassword,fregulatorid')->find($use_id);
		$fregionid = M('tregulator')->where(array('fid'=>$res['fregulatorid']))->getField('fregionid');
		$res['tregion_fname'] = M('tregion')->where(array('fid'=>$fregionid))->getField('fname');
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取账户信息成功','data'=>$res));
	}

	/*
	*查看添加用户
	*/

	public function checkUserList(){
		Check_QuanXian(['glyonghu']);
		$system_num = getconfig('system_num');
		$username 	= I('username');//用户名
		$area 	= I('area');//地区ID
		$iscontain 	= I('iscontain');//是否包含下属地区

		$regionid = session('regulatorpersonInfo.regionid');//行政区划id
		
		$p  = I('page', 1);//当前第几页
        $pp = 20;//每页显示多少记录
        if(session('regulatorpersonInfo.fregulatorlevel') == 20){
			$where['tr.fid'] = array('like',substr(session('regulatorpersonInfo.regionid'),0,2).'%');
		}elseif(session('regulatorpersonInfo.fregulatorlevel') == 10){
			$where['tr.fid'] = array('like',substr(session('regulatorpersonInfo.regionid'),0,4).'%');
		}elseif(session('regulatorpersonInfo.fregulatorlevel') == 0){
			$where['tr.fid'] = array('like',substr(session('regulatorpersonInfo.regionid'),0,6).'%');
		}

		if(!empty($username)){
			$where['tp.fname'] = array('like','%'.$username.'%');
		}
		if(!empty($area)){
			if(!empty($iscontain)){
				$tregion_len = get_tregionlevel($area);
				if($tregion_len == 2){//省级
					$where['t.fregionid'] = array('like',substr($area,0,2).'%');
				}elseif($tregion_len == 4){//市级
					$where['t.fregionid'] = array('like',substr($area,0,4).'%');
				}elseif($tregion_len == 6){//县级
					$where['t.fregionid'] = array('like',substr($area,0,6).'%');
				}
			}else{
				$where['t.fregionid'] = $area;
			}
		}

		$where['t.fcode'] = array('like','20%');
        $where['t.fstate'] 	= 1;
        $where['t.fkind'] 	= 1;
        $where['tp.fstate'] = ['in',[0,1]];
        $where['t.ftype'] = 20;
        $where['_string'] = '(plbcount = 0 or plbcount is null or plbcount2 = 1)';
        $count = M('tregulatorperson')
				->alias('tp')
				->join('tregulator as t on tp.fregulatorid=t.fid')
			  	->join('tregion as tr on t.fregionid = tr.fid')
			  	->join('(select count(*) as plbcount,sum(case when fcustomer = "'.$system_num.'" then 1 else 0 end) as plbcount2,fpersonid from tpersonlabel  where fstate = 1 group by fpersonid) lb on tp.fid = lb.fpersonid','left')
        		->where($where)
        		->count();

        $res = M('tregulatorperson')
        	->alias('tp')
        		->field('
        		t.fname,
                tp.fid,
                tp.fname as user_fname,
                tp.fcode as user_code,
                tp.fisadmin,
                tp.fmobile,
                tp.fstate,
                (case when tr.fname="全国" then "国家" else tr.fname end) as tregion_fname
              ')
        	->join('tregulator as t on tp.fregulatorid=t.fid')
        	->join('tregion as tr on t.fregionid = tr.fid')
        	->join('(select count(*) as plbcount,sum(case when fcustomer = "'.$system_num.'" then 1 else 0 end) as plbcount2,fpersonid from tpersonlabel  where fstate = 1 group by fpersonid) lb on tp.fid = lb.fpersonid','left')
        	->where($where)
        	->order('tr.fid asc,tp.fid asc')
        	->page($p,$pp)
        	->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取用户信息成功','data'=>array('count'=>$count,'list'=>$res)));
	}

	/*
	*删除下级用户
	*/

	public function deleteUser() {
		if(session('regulatorpersonInfo.fisadmin') != 1){
			$this->ajaxReturn(array('code'=>1,'msg'=>'您无权操作'));
		}
		$fid = I('fid');

		if(session('regulatorpersonInfo.fregulatorlevel') == 20){
			$where['fregulatorid'] = array('like','20'.substr(session('regulatorpersonInfo.regionid'),0,2).'%');
		}elseif(session('regulatorpersonInfo.fregulatorlevel') == 10){
			$where['fregulatorid'] = array('like','20'.substr(session('regulatorpersonInfo.regionid'),0,4).'%');
		}elseif(session('regulatorpersonInfo.fregulatorlevel') == 0){
			$where['fregulatorid'] = array('like','20'.substr(session('regulatorpersonInfo.regionid'),0,6).'%');
		}

		$where['fid'] = $fid;
		$res = M('tregulatorperson')->where($where)->delete();
		if($res){
			D('Function')->write_log('用户管理',1,'删除成功','tregulatorperson',$fid,M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			D('Function')->write_log('用户管理',0,'删除失败','tregulatorperson',$fid,M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败'));
		}
	}

	/*
	* 修改用户信息
	*/
	public function edituser(){
		if(session('regulatorpersonInfo.fisadmin') != 1){
			$this->ajaxReturn(array('code'=>1,'msg'=>'您无权操作'));
		}
		$fid 		= I('fid');
		$verify 	= I('verify');//验证码
		$fpassword2 = I('fpassword2');//新密码
		$fpassword3 = I('fpassword3');//确认密码
		$user_fname = I('user_fname');//用户名
		$fmobile 	= I('fmobile');//绑定手机号
		if(!A('Login')->check_verify($verify)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'验证码输入有误'));
		}
		if(session('regulatorpersonInfo.fregulatorlevel') == 20){
			$where['fregulatorid'] = array('like','20'.substr(session('regulatorpersonInfo.regionid'),0,2).'%');
		}elseif(session('regulatorpersonInfo.fregulatorlevel') == 10){
			$where['fregulatorid'] = array('like','20'.substr(session('regulatorpersonInfo.regionid'),0,4).'%');
		}elseif(session('regulatorpersonInfo.fregulatorlevel') == 0){
			$where['fregulatorid'] = array('like','20'.substr(session('regulatorpersonInfo.regionid'),0,6).'%');
		}

		$where_tn['fid'] = $fid;
		$data_tn['fname'] = $user_fname;
		if(!empty($fpassword2)){
			if($fpassword2 != $fpassword3){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'两次密码输入不一致'));
			}
			if(strlen($fpassword2)<6){
				$this->ajaxReturn(array('code'=>1,'msg'=>'密码长度不得小于6位'));
			}
			$data_tn['fpassword'] = md5($fpassword2);
			$data_tn['fpassword2'] = md5(md5($fpassword2));
		}
		$data_tn['fstate'] 		= 1;
		$data_tn['fmobile'] 	= $fmobile;
		$data_tn['fmodifier'] 	= session('regulatorpersonInfo.fname');//修改人
		$data_tn['fmodifytime'] = date('Y-m-d H:i:s');//修改时间

		$do_tn = M('tregulatorperson')
			->where($where_tn)
			->save($data_tn);
		if(!empty($do_tn)){
			$fcode = M('tregulatorperson')->where($where_tn)->getField('fcode');
			S('error_count'.$fcode,0,86400);
			D('Function')->write_log('用户管理',1,'修改成功','tregulatorperson',$fid,M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改完成'));
		}else{
			D('Function')->write_log('用户管理',1,'修改失败','tregulatorperson',$fid,M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败'));
		}

	}

 //     /*
 //    *修改密码
 //    */
 //    public function rePassword2()
 //    {
 //        $fid = I('fid');
 //        $new_password = I('new_password');
 //        $user_fname = I('user_fname');
 //        $data['fid'] = $fid;
 //        $data['fpassword'] = md5($new_password);
 //        $data['fname'] = md5($user_fname);
 //        $res = M('tregulatorperson')->save($data);
 //        if($res){
 //            D('Function')->write_log('用户管理',1,'修改密码成功','tregulatorperson',$fid);
 //            $this->ajaxReturn(array('code'=>0,'msg'=>'修改密码成功'));
 //        }
 //    }

	// /*
	// *修改密码
	// */
	// public function rePassword()
	// {
	// 	$fid = I('fid');

	// 	$new_password = I('new_password');
	// 	$rnew_password = I('rnew_password');
	// 	$old_password = I('old_password');
	// 	$data['fid'] = $fid;
	// 	if($new_password != $rnew_password){
	// 		$this->ajaxReturn(array('code'=>-1,'msg'=>'两次密码输入不一致'));
	// 	}
	// 	if(strlen($new_password)<6){
	// 		$this->ajaxReturn(array('code'=>-1,'msg'=>'密码长度不得小于6位'));
	// 	}
	// 	$fpwd = M('tregulatorperson')->where($data)->getField('fpassword');
	// 	if(md5($old_password) != $fpwd){
	// 		$this->ajaxReturn(array('code'=>-1,'msg'=>'旧密码输入错误'));
	// 	}
	// 	$data['fpassword'] = md5($new_password);
	// 	$res = M('tregulatorperson')->save($data);
	// 	if($res){
	// 		D('Function')->write_log('用户管理',1,'修改密码成功','tregulatorperson',$fid);
	// 		$this->ajaxReturn(array('code'=>0,'msg'=>'修改密码成功'));
	// 	}else{
	// 		D('Function')->write_log('用户管理',0,'修改密码失败','tregulatorperson',$fid,M('tregulatorperson')->getlastsql());
	// 		$this->ajaxReturn(array('code'=>-1,'msg'=>'新密码不能与原密码相同'));
	// 	}
	// }
}