/Agp/Fnetad/auto_addad 淄博自动生成互联网违法广告接口，已停用
/Agp/OutXlsZB/create_yujingreport 淄博周预警报告生成接口，已停用
/Agp/Outtaskfj/reporttask 福建自动生成报告接口
/Api/ShIssue/get_newtadcuttask  新广告剪辑数据获取
/Api/NetAd/net_task_addweight  提升互联网任务权重
/Api/ShIssue/loop_cutimgtask 客户互联网样本对一直未成功返回截图信息的进行轮循检查，并重新推送请求
/Api/ShIssue/update_nettask_priority 根据设置调整任务优先级（暂上海专用），可轮循 30s/次
/Api/ShIssue/retrieval_backtask 轮循检索媒体剪辑任务是否完成（10分钟/次）
/Api/ShIssue/lunxun_push_netsample 轮循推送的互联网已完成的数据到客户表（5秒/次）
/Api/ShIssue/lunxun_push_netsample_sh 轮循推送的互联网已完成的数据到客户表（2秒/次）
/Api/ShIssue/lunxun_push_netsample_shnowtask 轮循推送的互联网已完成的数据到客户表（2秒/次）
/Api/ShIssue/lunxun_cutpic_text 轮循获取截图文本识别内容（5秒/次）
/Api/ShIssue/push_cutpic_task 轮循推送截图文本识别任务（60秒/次）
/Api/ShIssue/lunxun_neibuyilou_priority 修改国家局第三方数据复核计划内部遗漏检查结果的众包处理优先级（15秒/次）
/Api/ShIssue/lunxun_create_task 检测上海当月任务是否已推送，包含传统和互联网（43200秒/次）
/Api/ZJMediaIssue/push_mediaissue 轮循推送浙江平台广告数据，30秒/次
/Api/ZJMediaIssue/create_mediaissue_task 轮循推送浙江平台数据任务，60秒/次
/Api/ShIssue/lunxun_inspectMedia 检测上海当月传统媒体任务是否正常，每天8点跑一次
/Api/IssueData/clearHeBeiIssueData  清理广告数据，接口暂定针对河北,30秒/次
/Api/IllegalAdIssue/illadSmsRegulator  违法广告再次发布提醒，1天/次
/Api/Mobile/smsIllegalCuiBan  违法广告再次发布提醒，1天/次

研发群钉钉机器人ID：6299ea2a7184853c83972cf1df3ae1a838d23d18e1ea207cd168c6b3b92fd8cc
钉钉测试群钉钉机器人ID：ec041dd6012298a46ef94bb1d215959deb5b19434ce5a754ef337dab0890524f
国家局任务组钉钉机器人ID：5469d7c29290a65a5bb3356d2219d833181377e689004490861239e1afc63e64
互联网专项处理组钉钉机器人ID：689558edc68571c9047e40fe7b0098ee621f0c89ed5c56f4d3d2327cbed1a067
法律法规钉钉机器人ID：3691aaeda25b4bb39927881c51d42e769a7ec3f8476f66a5302019acdac16b7e
用户违法广告处理群机器人ID：f39909c52fe6a7d42c8ea886ad12fdd2d656c38b10dcb2969e140361ae23b545

Api/IllegalAdIssue/grantlog 补充交办记录
Api/IllegalAdIssue/create_illad_sendlog 执行“生成违法广告的派发记录”功能 
Api/IllegalAdIssue/illegal_ad_sendlog 生成违法广告的派发记录 
Api/ShIssue/push_finish_backtask 媒体数据检查完成后，通过该接口接收完成状态（提供给马国琪，作上海退回检查完成后的推送）
Api/ShIssue/push_netsample 互联网任务完成后将样本数据推送到客户样本数据表
Api/ShIssue/push_netsample2 互联网任务完成后将样本数据推送到客户样本数据表2
Api/IllegalAdIssue/illadSmsRegulator 互联网任务完成后将样本数据推送到客户样本数据表
