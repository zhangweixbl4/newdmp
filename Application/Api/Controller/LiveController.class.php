<?php
namespace Api\Controller;
use Think\Controller;

class LiveController extends Controller {
	
	public function index(){

		$fdpid = I('fdpid');
		$fid = I('fmediaid');
		$start_time = I('fcreatetime_s');
		$end_time = I('fcreatetime_e');

		$address = array();
		if($fdpid || $fid){
			$mediaWhere = array();
			if($fdpid) $mediaWhere['fdpid'] = $fdpid;
			if($fid) $mediaWhere['fid'] = $fid;
			
			$mediaInfo = M('tmedia')->where($mediaWhere)->field('live_m3u8,fid,fsavepath, fmediaclassid')->find();
			$address['m3u8'] = $mediaInfo['live_m3u8'];
			$address['playback_url'] = '';
			if($start_time && $end_time && $mediaInfo['fid']){
				$startTime = strtotime($start_time);
				$endTime = strtotime($end_time);
				$address['playback_url'] = A('Common/Media','Model')->get_m3u8($mediaInfo['fid'],$startTime,$endTime);

				$address['playback_url_md5'] = md5($address['playback_url']);
				$address['startTime'] = $start_time;
				$address['endTime'] = $end_time;
				$address['mediaType'] = substr($mediaInfo['fmediaclassid'], 1, 1) == 1 ? 'mp4' : 'mp3';
			}
			$this->assign('address',$address);
		}else{
            $this->assign('address',0);
        }
        $this->assign('generateUrl', C('SPLICING_URL'));
        $this->assign('host', $_SERVER['HTTP_HOST']);
		$this->display();

	}

    public function generateMP4Callback($md5Code)
    {
        S('generateMp4_' . $md5Code, file_get_contents('php://input'), 86400);
        dump(S('generateMp4_' . $md5Code));
    }

    public function checkGenerateIsDone($md5Code)
    {
        $res = S('generateMp4_' . $md5Code);
        if (!$res){
            $this->ajaxReturn([
                'code' => -99,
                'msg' => '尚未收到回调'
            ]);
        }else{
            $this->ajaxReturn($res);
        }
	}

	public function play(){
		$this->display();
	}
	
}