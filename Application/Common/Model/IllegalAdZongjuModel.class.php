<?php
namespace Common\Model;
use Think\Exception;


class IllegalAdZongjuModel{



	/*新增违法广告*/
	public function add_illegal_ad($media_class,$sam_id){
		
		
		$sam_where = array();
		if($media_class == 1){
			$sam_table = 'ttvsample';
			$sam_where['ttvsample.fid'] = $sam_id;
		}elseif($media_class == 2){
			$sam_table = 'tbcsample';
			$sam_where['tbcsample.fid'] = $sam_id;
		}elseif($media_class == 3){
			$sam_table = 'tpapersample';
			$sam_where['tpapersample.fpapersampleid'] = $sam_id;
		}
		

		
		$samInfo = M($sam_table)->cache(true,2)
								->master(true)
								->where($sam_where)
								->find();//查询样本信息

		
		if(intval($samInfo['fillegaltypecode']) == 0){//判断是否违法
			return false;//如果不是违法直接返回 false
		}
		
		
		
		
		$adInfo = M('tad')->cache(true,60)->where(array('fadid'=>$samInfo['fadid']))->find();//查询广告信息
		$mediaInfo = M('tmedia')
								->cache(true,600)
								->field('tmedia.fid,left(tmedia.fmediaclassid,2) as media_class,tmediaowner.fregionid,tmedia.fmediaownerid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>$samInfo['fmediaid']))->find();//查询媒介信息
		if(!$mediaInfo) return false;//如果该媒介不存在，就直接返回false
								
		
		$tbn_illegal_ad_data = array();
		$tbn_illegal_ad_data['fregion_id'] = $mediaInfo['fregionid'];//地区id
		$tbn_illegal_ad_data['fmedia_class'] = $media_class;//媒介类别
		$tbn_illegal_ad_data['fmedia_id'] = $mediaInfo['fid'];//媒介id
		$tbn_illegal_ad_data['fad_class_code'] = $adInfo['fadclasscode'];//广告类别
		
		$tbn_illegal_ad_data['fsample_id'] = $sam_id;//样本id
		
		$tbn_illegal_ad_data['fad_name'] = $adInfo['fadname'];//广告名称
		
		$tbn_illegal_ad_data['fadowner'] = M('tadowner')->where(array('fid'=>$adInfo['fadowner']))->getField('fname');//广告主名称
		$tbn_illegal_ad_data['fadownerid'] = $adInfo['fadowner'];//广告主ID
		

		$tbn_illegal_ad_data['fillegal_code'] = $samInfo['fexpressioncodes'];//违法代码
		$tbn_illegal_ad_data['fmediaownerid'] = $mediaInfo['fmediaownerid'];//媒介机构id
		
		if($samInfo['fadlen'] >= 600){
			$tbn_illegal_ad_data['is_long_ad'] = 1;
		}else{
			$tbn_illegal_ad_data['is_long_ad'] = 0;
		}
		
		
		$tbn_illegal_ad_data['fillegal'] = $samInfo['fillegalcontent'];//违法内容
		$tbn_illegal_ad_data['favifilename'] = $samInfo['favifilename'];//视频、音频路径（电视、广播）
		$tbn_illegal_ad_data['fjpgfilename'] = $samInfo['fjpgfilename'];//图片路径（报刊）
		$tbn_illegal_ad_data['fexpressions'] = $samInfo['fexpressions'];//违法表现
		$tbn_illegal_ad_data['fillegaltypecode'] = $samInfo['fillegaltypecode'];//违法程度
		
		
		
		
		$tbn_illegal_ad_data['create_time'] = date('Y-m-d H:i:s');//创建时间
		
		$regulatorcodeList = M('tregulatormedia')
												->cache(true,600)
												->where(array('fmediaid'=>$mediaInfo['fid']))
												->getField('fregulatorcode',true);//查询该违法广告的媒体关联哪些机构
												
												
												
												
		foreach($regulatorcodeList as $regulatorcode){
			
			$illegal_ad_info_0 = $tbn_illegal_ad_data;
			
			if(!in_array($regulatorcode,C('CUSTOMER_COM')))  continue;//退出本次循环
			
			$fregion_id = M('tregulator')->cache(true,3600)->where(array('fcode'=>$regulatorcode))->getField('fregionid');//查询该机构的地区
			if($fregion_id == 100000) continue;//如果是国家局的，则退出本次循环
			if($fregion_id == 370300 && strtotime($samInfo['fissuedate']) < strtotime('2018-11-01')) continue;//如果是淄博的，且样本的发布时间小于指定时间
			$illegal_ad_info_0['fcustomer'] = $fregion_id ;//该违法广告的所属机构的地区
			
			
			
			$check_tbn_illegal_ad = M('tbn_illegal_ad')->where(array('fcustomer'=>$fregion_id,'fmedia_class'=>$media_class,'fsample_id'=>$sam_id,'fstatus'=>array('in',array(0,30))))->count();//查询未处理记录
		
			
			if($check_tbn_illegal_ad == 0){//判断是否有未处理记录

				try{//尝试写入数据
					$tbn_illegal_ad_id = M('tbn_illegal_ad')->add($illegal_ad_info_0);//添加违法广告样本
					#sleep(1);//休息一秒钟，防止只读库数据不一致
					try{
						A('Api/IllegalAdIssue')->illegal_ad_sendlog([$tbn_illegal_ad_id]);//生成违法广告的派发记录
					}catch(Exception $error2) { 
					
					}
				}catch(Exception $error) { //
					$tbn_illegal_ad_id = 0;//
				} 
			}
			if($tbn_illegal_ad_id){//
				$this->add_illegal_issue($sam_id,$tbn_illegal_ad_id,$mediaInfo);
			}
			

		}										
												

		return true;

	}
    

	public function add_illegal_issue($sam_id,$tbn_illegal_ad_id,$mediaInfo,$date = ''){
		
		if($date == ''){
			$date = date('Y-m-d');
		}
		$media_class = intval($mediaInfo['media_class']);

		if($media_class == 1){//电视违法发布记录逻辑
			$samInfo = M('ttvsample')->cache(true,60)->field('fissuedate')->where(array('fid'=>$sam_id))->find();
			$this_month_table = 'ttvissue_'.date('Ym',strtotime($date)).'_'.substr($mediaInfo['fregionid'],0,2);//当月表名
			$that_month_table = 'ttvissue_'.date('Ym', strtotime("-1 months", strtotime($date))).'_'.substr($mediaInfo['fregionid'],0,2);//上月表名
			
			$issue_where = array('fsampleid'=>$sam_id,'fmediaid'=>$mediaInfo['fid']);
			try{//尝试查询数据
				$this_month_list = M($this_month_table)->where($issue_where)->select();//当月违法发布记录
			}catch(Exception $error) { //
				$this_month_list = array();
			} 
			
			try{//尝试查询数据
				$that_month_list = M($that_month_table)->where($issue_where)->select();//上月违法发布记录
			}catch(Exception $error) { //
				$that_month_list = array();
			} 
			$issueList = array_merge($this_month_list,$that_month_list);
			
			$source_issue_list = array();
			foreach($issueList as $issue){
				
				$source_issue_list[] = $issue['fstarttime'];
				if($issue['fissuedate'] < strtotime('2018-11-01')) continue;
				try{//
					
					
					
					$issue_fid = M('tbn_illegal_ad_issue')->add(array(
																
														'fillegal_ad_id'	=>	$tbn_illegal_ad_id,
														'fmedia_id'	=>	$mediaInfo['fid'],
														'fissue_date'	=>	date('Y-m-d',$issue['fissuedate']),
														'fstarttime'	=>	date('Y-m-d H:i:s',$issue['fstarttime']),
														'fendtime'	=>	date('Y-m-d H:i:s',$issue['fendtime']),
														'fmedia_class'	=> 1,
														'fmediaownerid'	=>	$mediaInfo['fmediaownerid'],
															
															));
					
					
				}catch(Exception $error) { //
					$issue_fid = 0;
				} 

			}
			$this_month_table = 'ttvissue_'.date('Ym',strtotime($date)).'_'.substr($mediaInfo['fregionid'],0,2);//当月表名
			$that_month_table = 'ttvissue_'.date('Ym', strtotime("-1 months", strtotime($date))).'_'.substr($mediaInfo['fregionid'],0,2);//上月表名
			
			
			$ill_tab_issue_list = M('tbn_illegal_ad_issue')
														->field('fid,fstarttime')
														->where(array(
																		'fmedia_id'=>$mediaInfo['fid'],
																		'fillegal_ad_id'=>$tbn_illegal_ad_id,
																		'fissue_date'=>array('between',array(
																												date('Y-m-01', strtotime("-1 months", strtotime($date))),
																												
																												date('Y-m-31', strtotime("+1 months", strtotime($date))-86400),
																												
																												)),
																	))
														->select();
			//var_dump(M('tbn_illegal_ad_issue')->getLastSql());											
			foreach($ill_tab_issue_list as $ill_tab_issue){
				
				echo $ill_tab_issue['fid']."\n";
				if(!in_array(strtotime($ill_tab_issue['fstarttime']),$source_issue_list)){//判断违法发布表里面的记录是否在基础数据表存在
					echo '需要删除:'.$ill_tab_issue['fid']."\n";
					//M('tbn_illegal_ad_issue')->where(array('fid'=>$ill_tab_issue['fid']))->delete();
				}
			} 
			
		}elseif($media_class == 2){//广播违法发布记录逻辑
			$samInfo = M('tbcsample')->cache(true,60)->field('fissuedate')->where(array('fid'=>$sam_id))->find();
			$this_month_table = 'tbcissue_'.date('Ym',strtotime($date)).'_'.substr($mediaInfo['fregionid'],0,2);
			$that_month_table = 'tbcissue_'.date('Ym', strtotime("-1 months", strtotime($date))).'_'.substr($mediaInfo['fregionid'],0,2);
			$issue_where = array('fissuedate'=>array('EGT',strtotime($samInfo['fissuedate'])),'fsampleid'=>$sam_id,'fmediaid'=>$mediaInfo['fid']);
			
			try{//尝试查询数据
				$this_month_list = M($this_month_table)->where($issue_where)->select();//当月违法发布记录
			}catch(Exception $error) { //
				$this_month_list = array();
			} 
			
			try{//尝试查询数据
				$that_month_list = M($that_month_table)->where($issue_where)->select();//上月违法发布记录
			}catch(Exception $error) { //
				$that_month_list = array();
			} 
			$issueList = array_merge($this_month_list,$that_month_list);
			$source_issue_list = array();
			foreach($issueList as $issue){
				
				$source_issue_list[] = $issue['fstarttime'];
				if($issue['fissuedate'] < strtotime('2018-11-01')) continue;
				try{//
					M('tbn_illegal_ad_issue')->add(array(
																
														'fillegal_ad_id'	=>	$tbn_illegal_ad_id,
														'fmedia_id'	=>	$mediaInfo['fid'],
														'fissue_date'	=>	date('Y-m-d',$issue['fissuedate']),
														'fstarttime'	=>	date('Y-m-d H:i:s',$issue['fstarttime']),
														'fendtime'	=>	date('Y-m-d H:i:s',$issue['fendtime']),
														'fmedia_class'	=> 2,
														'fmediaownerid'	=>	$mediaInfo['fmediaownerid'],	
															));
					
					
				}catch(Exception $error) { //
					
				} 
			}
			$ill_tab_issue_list = M('tbn_illegal_ad_issue')->field('fid,fstarttime')->where(array(
																									'fmediaid'=>$mediaInfo['fid'],
																									'fillegal_ad_id'=>$tbn_illegal_ad_id,
																									'fissue_date'=>array('between',array(
																												date('Y-m-01', strtotime("-1 months", strtotime($date))),
																												
																												date('Y-m-31', strtotime("+1 months", strtotime($date))-86400),
																												
																												)),
																									
																									))->select();
			foreach($ill_tab_issue_list as $ill_tab_issue){
				
				echo $ill_tab_issue['fid']."\n";
				if(!in_array(strtotime($ill_tab_issue['fstarttime']),$source_issue_list)){//判断违法发布表里面的记录是否在基础数据表存在
					echo '需要删除:'.$ill_tab_issue['fid']."\n";
					//M('tbn_illegal_ad_issue')->where(array('fid'=>$ill_tab_issue['fid']))->delete();
				}
			}
			
		}elseif($media_class == 3){//报刊违法发布记录逻辑
			
			if(M('tbn_illegal_ad_issue')->where(array('fillegal_ad_id'	=>	$tbn_illegal_ad_id))->count() > 0){
				return false;//判断违法发布表是否有该数据
			}
			
			$issueInfo = M('tpaperissue')->field('fpage,fissuedate')->where(array('fpapersampleid'=>$sam_id))->find();
			if(strtotime($issueInfo['fissuedate']) > strtotime('2018-11-01')){
			
				M('tbn_illegal_ad_issue')->add(array(
																
														'fillegal_ad_id'	=>	$tbn_illegal_ad_id,
														'fmedia_id'	=>	$mediaInfo['fid'],
														'fissue_date'	=>	$issueInfo['fissuedate'],
														'fstarttime'	=>	null,
														'fendtime'	=>	null,
														'fpage'		=>	$issueInfo['fpage'],
														'fmedia_class'	=> 3,	
														'fmediaownerid'	=>	$mediaInfo['fmediaownerid'],
															));
		
			}
		
		}
		
		
		
	}




	/*判断总局是否确认*/
	public function zongjuDataConfirm($media_id,$date){
		
		$confirm_state_cache = S('zongjuDataConfirm_'.$media_id.'_'.$date);//从缓存获取
		
		if($confirm_state_cache) return $confirm_state_cache['confirm_state'];//如果有缓存，直接返回
		
		//var_dump('123');
		
		$zongju_data_confirm_where = array();//查询条件
		$zongju_data_confirm_where['fmonth'] = date('Y-m-01',strtotime($date));//查询条件
		
		$zongju_data_confirm = M('zongju_data_confirm')->cache(true,60)->field('dmp_confirm_content,zongju_confirm_content')->where($zongju_data_confirm_where)->find();
		
		$dmp_confirm_content = json_decode($zongju_data_confirm['dmp_confirm_content'],true);
		$zongju_confirm_content = json_decode($zongju_data_confirm['zongju_confirm_content'],true);
		
		$mediaInfo = M('tmedia')
								->cache(true,360)
								->field('fid,left(tmedia.fmediaclassid,2) as fmedia_class_code')
								->where(array('tmedia.fid'=>$media_id))
								->find();
		
		if($mediaInfo['fmedia_class_code'] == '01') $ttab = 'tv';
		if($mediaInfo['fmedia_class_code'] == '02') $ttab = 'bc';
		if($mediaInfo['fmedia_class_code'] == '03') $ttab = 'paper';
		
		$confirm_state = 0;
		foreach($dmp_confirm_content[$ttab] as $dmpLL){
			if($dmpLL['date'] == $date && in_array($media_id,$dmpLL['selected'])){
				$confirm_state = 1;

			}
			
		}
		
		foreach($zongju_confirm_content[$ttab] as $zongjuLL){
			if($zongjuLL['date'] == $date && in_array($media_id,$zongjuLL['selected'])){
				$confirm_state = 2;
			}
			
		}
		
		S('zongjuDataConfirm_'.$media_id.'_'.$date,array('is'=>1,'confirm_state'=>$confirm_state),600);
		
		return $confirm_state;
		
		
	}
	

	/*新增违法广告(总局)*/
	public function add_zongju_illegal($media_class,$sam_id,$identify,$fcustomer = 100000){

		
		$sam_where = array();
		if($media_class == 1){
			$sam_table = 'ttvsample';
			$sam_where['ttvsample.fid'] = $sam_id;
		}elseif($media_class == 2){
			$sam_table = 'tbcsample';
			$sam_where['tbcsample.fid'] = $sam_id;
		}elseif($media_class == 3){
			$sam_table = 'tpapersample';
			$sam_where['tpapersample.fpapersampleid'] = $sam_id;
		}
		

		
		$samInfo = M($sam_table)->cache(true,2)->where($sam_where)->find();//查询样本信息

		
		if(intval($samInfo['fillegaltypecode']) == 0){//判断是否违法
			return false;//如果不是违法直接返回 false
		}
		
		
		
		
		$adInfo = M('tad')->cache(true,60)->where(array('fadid'=>$samInfo['fadid']))->find();//查询广告信息
		$mediaInfo = M('tmedia')
								->cache(true,600)
								->field('tmedia.fid,left(tmedia.fmediaclassid,2) as media_class,tmediaowner.fregionid,tmedia.fmediaownerid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>$samInfo['fmediaid']))->find();//查询媒介信息
		if(!$mediaInfo) return false;//如果该媒介不存在，就直接返回false
								
		
		$tbn_illegal_ad_data = array();
		$tbn_illegal_ad_data['fregion_id'] = $mediaInfo['fregionid'];//地区id
		$tbn_illegal_ad_data['fmedia_class'] = $media_class;//媒介类别
		$tbn_illegal_ad_data['fmedia_id'] = $mediaInfo['fid'];//媒介id
		$tbn_illegal_ad_data['fad_class_code'] = $adInfo['fadclasscode'];//广告类别
		
		$tbn_illegal_ad_data['fsample_id'] = $sam_id;//样本id
		
		$tbn_illegal_ad_data['fad_name'] = $adInfo['fadname'];//广告名称
		$tbn_illegal_ad_data['fadowner'] = M('tadowner')->where(array('fid'=>$adInfo['fadowner']))->getField('fname');//广告主名称
		$tbn_illegal_ad_data['fadownerid'] = $adInfo['fadowner'];//广告主ID
		
		
		$tbn_illegal_ad_data['fillegal_code'] = $samInfo['fexpressioncodes'];//违法代码
		$tbn_illegal_ad_data['fmediaownerid'] = $mediaInfo['fmediaownerid'];//媒介机构id
		$tbn_illegal_ad_data['identify'] = $identify;//标识
		
		if($samInfo['fadlen'] >= 600){//判断是否长广告
			$tbn_illegal_ad_data['is_long_ad'] = 1;
		}
		
		
		$tbn_illegal_ad_data['fillegal'] = $samInfo['fillegalcontent'];//违法内容
		$tbn_illegal_ad_data['favifilename'] = $samInfo['favifilename'];//视频、音频路径（电视、广播）
		$tbn_illegal_ad_data['fjpgfilename'] = $samInfo['fjpgfilename'];//图片路径（报刊）
		$tbn_illegal_ad_data['fexpressions'] = $samInfo['fexpressions'];//违法表现
		$tbn_illegal_ad_data['fillegaltypecode'] = $samInfo['fillegaltypecode'];//违法程度
		$tbn_illegal_ad_data['fcustomer'] = $fcustomer ;//该违法广告的所属机构的地区
		

		$tbn_illegal_ad_data['create_time'] = date('Y-m-d H:i:s');//创建时间
		$tbn_illegal_ad_data['unique_k'] = date('Y-m-d');
		
		
		$tbn_illegal_ad_info = M('tbn_illegal_ad')
												->field('fid')
												->where(array('fcustomer'=>$fcustomer,'fmedia_class'=>$media_class,'fsample_id'=>$sam_id,'identify'=>$identify,'fstatus'=>array('in',array(0,30))))
												->find();//查询记录是否存在
		
			
		if(!$tbn_illegal_ad_info){//
			try{//尝试写入数据
				$tbn_illegal_ad_id = M('tbn_illegal_ad')->add($tbn_illegal_ad_data);//添加违法广告样本
				#sleep(1);//休息一秒钟，防止只读库数据不一致
				try{
					A('Api/IllegalAdIssue')->illegal_ad_sendlog([$tbn_illegal_ad_id]);//生成违法广告的派发记录
				}catch(Exception $error2) { 
				
				}
			}catch(Exception $error) { //
				$tbn_illegal_ad_id = M('tbn_illegal_ad')->where(array('fcustomer'=>$fcustomer,'fmedia_class'=>$media_class,'fsample_id'=>$sam_id,'identify'=>$identify))->getField('fid');//查询记录是否存在;//
			} 
			
		}else{
			$tbn_illegal_ad_id = $tbn_illegal_ad_info['fid'];//违法广告id
		}									
												
												
																					
												

		return $tbn_illegal_ad_id;
		/*
			fregion_id	,
			fmedia_class	,
			fmedia_id	,
			fad_class_code	,
			fsample_id	,
			fad_name	,
			fillegal_code	,
			fmediaownerid	,
			identify	,
			is_long_ad	,
			fillegal	,
			favifilename	,
			fjpgfilename	,
			fexpressions	,
			fillegaltypecode	,
			fcustomer	,
			create_time	,
			unique_k
		*/
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}