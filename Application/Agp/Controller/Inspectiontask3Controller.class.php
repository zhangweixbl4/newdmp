<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 内部数据检查
 */
class Inspectiontask3Controller extends BaseController{
    /**
     * 接收参数
     */
    protected $P;

    /**
     * 初始化
     */
    public function _initialize(){
        parent::_initialize();
        $this->P = json_decode(file_get_contents('php://input'),true);
    }

    /**
     * 上海数据检查-获取广告列表
     */
    public function CheckList(){
        $fid   = $this->P['fid'];
        $fmediaid   = $this->P['fmediaid'];
        $fstarttime = $this->P['fstarttime'];
        $fendtime   = $this->P['fendtime'];
        $pageIndex  = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize   = (int)($this->P['pageSize'] ? $this->P['pageSize']  : 1000);
        $limitIndex = ($pageIndex-1)*$pageSize;
        if($fmediaid && $fstarttime && $fendtime){
            $do_amt = M('tapimediatask')->where(['fid'=>$fid])->find();
            $m3u8_url   = A('Common/Media','Model')->get_m3u8($fmediaid,strtotime($fstarttime),strtotime($fendtime));
            $retList    = A('Common/AdIssue','Model')->getAdIssueByMediaCustomer($fmediaid,$fstarttime,$fendtime,$do_amt['fcustomer']);
            $count      = (int)$retList['count'];//包含非广告时段总条数
            $countAds   = (int)$retList['count'];//仅广告总条数
            $List       = $retList['data'];
            $adList     = [];
            $resultList = [];
            if(!empty($List)){
                foreach($List as $key=>$row){
                    if($key == 0){//第一条
                        $max_endtime = $row['fendtime'];
                        $interval = $row['fstarttime'] - strtotime($fstarttime); //与起始时间间隔大于3秒,则创建一个空时间段
                        if($interval > 3){
                            $adNull = [
                                'fid'              => '',
                                'fsampleid'        => '',
                                'favifilename'     => '',
                                'fmediaclassid'    => '',
                                'fmediaid'         => '',
                                'fmedianame'       => '',
                                'fregionid'        => '',
                                'fadname'          => '查看段',
                                'fadclasscode'     => '',
                                'fadclasscode_v2'  => '',
                                'fadowner'         => '',
                                'fadownername'     => '',
                                'fbrand'           => '',
                                'fspokesman'       => '',
                                'fadclass'         => '',
                                'fstarttime'       => date('Y-m-d H:i:s',strtotime($fstarttime)),
                                'fendtime'         => date('Y-m-d H:i:s',$row['fstarttime']),
                                'flength'          => $interval,
                                'fillegaltypecode' => '0',
                                'fexpressioncodes' => '',
                                'fexpressions'     => '',
                                'fillegalcontent'  => '',
                                'fis_error'        => '0',
                                'fcreator'         => '',
                                'fcreatetime'      => '',
                                'fmodifier'        => '',
                                'fmodifytime'      => ''
                            ];
                            array_push($adList,$adNull);
                            $count++;//创建的空闲时段计入记录总条数
                        }
                    }elseif($key > 0 && $key <= (count($List)-1)){
                        //两个广告时间间隔大于3秒,则创建一个空时间段
                        if($max_endtime<=$List[$key-1]['fendtime']){
                            $max_endtime = $List[$key-1]['fendtime'];
                            $interval = $row['fstarttime'] - $List[$key-1]['fendtime'];
                        }else{
                            $interval = 0 ;
                        }
                       
                        if($interval > 3){
                            $adNull = [
                                'fid'              => '',
                                'fsampleid'        => '',
                                'favifilename'     => '',
                                'fmediaclassid'    => '',
                                'fmediaid'         => '',
                                'fmedianame'       => '',
                                'fregionid'        => '',
                                'fadname'          => '查看段',
                                'fadclasscode'     => '',
                                'fadclasscode_v2'  => '',
                                'fadowner'         => '',
                                'fadownername'     => '',
                                'fbrand'           => '',
                                'fspokesman'       => '',
                                'fadclass'         => '',
                                'fstarttime'       => date('Y-m-d H:i:s',$List[$key-1]['fendtime']),
                                'fendtime'         => date('Y-m-d H:i:s',$row['fstarttime']),
                                'flength'          => $interval,
                                'fillegaltypecode' => '0',
                                'fexpressioncodes' => '',
                                'fexpressions'     => '',
                                'fillegalcontent'  => '',
                                'fis_error'        => '0',
                                'fcreator'         => '',
                                'fcreatetime'      => '',
                                'fmodifier'        => '',
                                'fmodifytime'      => ''
                            ];
                            array_push($adList,$adNull);
                            $count++;//创建的空闲时段计入记录总条数
                        }
                    }
                    $startpos = strpos($row['fmodifier'],'_') == 0 ? 0 : (strpos($row['fmodifier'])+1);
                    $samTable = substr($row['fmediaclassid'],0,2) == '01' ? 'ttvsample' : 'tbcsample';
                    $favifilename = M($samTable)->cache(true,120)->where(['fid'=>$row['fsampleid']])->getField('favifilename');
                    $ad = [
                        'fid'              => $row['fid'],
                        'fsampleid'        => $row['fsampleid'],
                        'favifilename'     => $favifilename,
                        'fmediaclassid'    => $row['fmediaclassid'],
                        'fmediaid'         => $row['fmediaid'],
                        'fmedianame'       => $row['fmedianame'],
                        'fregionid'        => $row['fregionid'],
                        'fadname'          => $row['fadname'],
                        'fadclasscode'     => $row['fadclasscode'],
                        'fadclasscode_v2'  => $row['fadclasscode_v2'],
                        'fadowner'         => $row['fadowner'],
                        'fadownername'     => $row['fadownername'],
                        'fbrand'           => $row['fbrand'],
                        'fspokesman'       => $row['fspokesman'],
                        'fadclass'         => $row['fadclass'],
                        'fstarttime'       => date('Y-m-d H:i:s',$row['fstarttime']),
                        'fendtime'         => date('Y-m-d H:i:s',$row['fendtime']),
                        'flength'          => $row['flength'],
                        'fillegaltypecode' => $row['fillegaltypecode'],
                        'fexpressioncodes' => $row['fexpressioncodes'],
                        'fexpressions'     => $row['fexpressions'],
                        'fillegalcontent'  => $row['fillegalcontent'],
                        'fis_error'        => $row['fis_error'],
                        'fisillegal'       => $row['fisillegal'],
                        'fcreator'         => (substr($row['fcreator'],0,4) == 'add_' || $row['fcreator'] == 916) ? $row['fcreator'] : '',//是否允许编辑删除或标记错误以此为判断依据，为空时不允许编辑修改允许标记
                        // 'fcreator'      => $row['fcreator'] ? preg_replace('/\D/s', '', substr($row['fcreator'],$startpos)) : '',//TODO:部分中文编码导致出错，另只展示数字部分
                        'fcreatetime'      => $row['fcreatetime'] ? $row['fcreatetime'] : '',
                        'fmodifier'        => $row['fmodifier'] ? preg_replace('/\D/s', '', substr($row['fmodifier'],$startpos)) : '',//TODO:部分中文编码导致出错，另只展示数字部分
                        'fmodifytime'      => $row['fmodifytime'] ? $row['fmodifytime'] : ''
                    ];
                    array_push($adList,$ad);
                    //最后一条后的空时间段
                    if($key == (count($List)-1)){
                        $interval = strtotime($fendtime) - $row['fendtime']; //与结束时间间隔大于3秒,则创建一个空时间段
                        if($interval > 3){
                            $adNull = [
                                'fid'              => '',
                                'fsampleid'        => '',
                                'favifilename'     => '',
                                'fmediaclassid'    => '',
                                'fmediaid'         => '',
                                'fmedianame'       => '',
                                'fregionid'        => '',
                                'fadname'          => '查看段',
                                'fadclasscode'     => '',
                                'fadclasscode_v2'  => '',
                                'fadowner'         => '',
                                'fadownername'     => '',
                                'fbrand'           => '',
                                'fspokesman'       => '',
                                'fadclass'         => '',
                                'fstarttime'       => date('Y-m-d H:i:s',$row['fendtime']),
                                'fendtime'         => date('Y-m-d H:i:s',strtotime($fendtime)),
                                'flength'          => $interval,
                                'fillegaltypecode' => '0',
                                'fexpressioncodes' => '',
                                'fexpressions'     => '',
                                'fillegalcontent'  => '',
                                'fis_error'        => '0',
                                'fcreator'         => '',
                                'fcreatetime'      => '',
                                'fmodifier'        => '',
                                'fmodifytime'      => ''
                            ];
                            array_push($adList,$adNull);
                            $count++;//创建的空闲时段计入记录总条数
                        }
                    }
                }
            }else{
                $adNull = [
                    'fid'              => '',
                    'fsampleid'        => '',
                    'favifilename'     => '',
                    'fmediaclassid'    => '',
                    'fmediaid'         => '',
                    'fmedianame'       => '',
                    'fregionid'        => '',
                    'fadname'          => '查看段',
                    'fadclasscode'     => '',
                    'fadclasscode_v2'  => '',
                    'fadowner'         => '',
                    'fadownername'     => '',
                    'fbrand'           => '',
                    'fspokesman'       => '',
                    'fadclass'         => '',
                    'fstarttime'       => $fstarttime,
                    'fendtime'         => $fendtime,
                    'flength'          => strtotime($fendtime) - strtotime($fstarttime),
                    'fillegaltypecode' => '0',
                    'fexpressioncodes' => '',
                    'fexpressions'     => '',
                    'fillegalcontent'  => '',
                    'fis_error'        => '0',
                    'fcreator'         => '',
                    'fcreatetime'      => '',
                    'fmodifier'        => '',
                    'fmodifytime'      => ''
                ];
                array_push($adList,$adNull);
                $count++;//创建的空闲时段计入记录总条数
            }
            $resultList = array_slice($adList,$limitIndex,$pageSize);
            $dataSet = [
                'fmediaid'      => $fmediaid,
                'live_m3u8_url' => $m3u8_url,
                'fad'           => $resultList
            ];
            $countNotAds = $count - $countAds;
            $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$count, 'countAds'=>$countAds,'countNotAds'=>$countNotAds, 'data'=>$dataSet, 'fdstatus' =>$do_amt['fdstatus'],'fcustomer'=>$do_amt['fcustomer']]);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 上海数据检查-添加遗漏的广告
     */
    public function AddAd(){
        $fcustomer = $this->P['fcustomer'];
        $fmediaid = $this->P['fmediaid'];
        $fadname = $this->P['fadname'];
        $fadclasscode = $this->P['fadclasscode'];
        $fadownername = $this->P['fadownername'];
        $fbrand = $this->P['fbrand'];
        $fspokesman = $this->P['fspokesman'];
        $fillegaltypecode = $this->P['fillegaltypecode'];
        $fexpressions = $this->P['fexpressions'];
        $fexpressioncodes = $this->P['fexpressioncodes'];
        $fillegalcontent = $this->P['fillegalcontent'];
        $fstarttime = strtotime($this->P['fstart']);
        $fendtime = strtotime($this->P['fend']);
        $fissuedate = date('Y-m-d',$fstarttime);
        $flength = $fendtime - $fstarttime;
        if($flength<1 || $flength>86400){
            $this->ajaxReturn(['code'=>1, 'msg'=>'播出时段选择有误']);
        }
        if($fadname == '查看段'){
            $this->ajaxReturn(['code'=>1,'msg'=>'请正确输入广告名称']);
        }
        if(substr($fadclasscode, 0 , 2) == 23){
            $this->ajaxReturn(['code'=>1, 'msg'=>'不允许添加其它类广告类别的广告']);
        }

        $sam_source_path = A('Common/Media','Model')->get_m3u8($fmediaid, $fstarttime, $fendtime);
        $mediaInfo = M('tmedia')->where(['fid'=>$fmediaid])->find();
        $fmedianame = $mediaInfo['fmedianame'];
        $fmediaclassid = $mediaInfo['fmediaclassid'];
        $fregionid = $mediaInfo['media_region_id'];
        // 校验是否可检查
        $isAvailable = $this->isMediaDateAvailable($fmediaid,strtotime($fissuedate));
        if(!$isAvailable){
            $this->ajaxReturn(['code'=>1, 'msg'=>'数据处理中，无法提交']);
        }
        //广告主ID
        $fadowner = M('tadowner')
         ->where(array('fname'=>$fadownername))
            ->getField('fid');
        if(empty($fadowner)){
            $fadowner = 0;
        }

        $fuuid = strtolower(createNoncestr(8).'-'.createNoncestr(4).'-'.createNoncestr(4).'-'.createNoncestr(4).'-'.createNoncestr(12));
        $dataAd = [
            'fuserid' => $fcustomer,
            'fmediaid' => $fmediaid,
            'fmedianame' => $fmedianame,
            'fmediaclassid' => $fmediaclassid,
            'fregionid' => $fregionid,
            'fadname' => $fadname,
            'fadclasscode' => $fadclasscode,
            'fadowner' => $fadowner,
            'fadownername' => $fadownername,
            'fbrand' => $fbrand,
            'fillegaltypecode' => $fillegaltypecode,
            'fexpressions' => $fexpressions,
            'fexpressioncodes' => $fexpressioncodes,
            'fillegalcontent' => $fillegalcontent,
            'fspokesman' => $fspokesman,
            'fstarttime' => $fstarttime,
            'fendtime' => $fendtime,
            'fissuedate' => $fissuedate,
            'flength' => $flength,
            'fuuid' => $fuuid,
            'sam_source_path' => $sam_source_path,
            'fcreator' => 'add_'.session('regulatorpersonInfo.fid'),
            'fcreatetime' => date('Y-m-d H:i:s'),
            'fmodifier' => session('regulatorpersonInfo.fid'),
            'fmodifytime' => date('Y-m-d H:i:s')
        ];
        $existWhere = [
            'fmediaid' => $fmediaid,
            'fstarttime' => $fstarttime,
            'fendtime' => $fendtime,
            'fuserid' => $fcustomer,
        ];
        $isExist = M('sh_issue')
            ->where($existWhere)
            ->count() > 0 ? true : false;
        if(!$isExist){
            // 判断添加的广告与已存在的记录时间是否重叠
            $isOverlap = M('sh_issue')
                ->where([
                    'fmediaid' => $fmediaid,
                    '_string' => '(fstarttime<'.$fstarttime.' and fendtime>'.$fstarttime.' or fstarttime<'.$fendtime.' and fendtime>'.$fendtime.')',
                    'fuserid' => $fcustomer,
                ])
                ->count();
            if($isOverlap == 0){
                $ret = M('sh_issue')->add($dataAd);
                if($ret){
                    $this->ajaxReturn(['code'=>0,'msg'=>'添加成功','data'=>$data]);
                }else{
                    $this->ajaxReturn(['code'=>1,'msg'=>'添加出错']);
                }
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'开始时间或结束时间与已有广告重叠，请修改时间']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'已存在']);
        }
    }

    /**
     * 上海数据检查-在现有广告时段内插入新广告
     */
    public function InsertAd(){
        $fcustomer       = $this->P['fcustomer'];
        $fid            = $this->P['fid'];
        $fmediaid        = $this->P['fmediaid'];
        $fadname         = $this->P['fadname'];
        $fadclasscode    = $this->P['fadclasscode'];
        $fadownername    = $this->P['fadownername'];
        $fbrand          = $this->P['fbrand'];
        $fspokesman      = $this->P['fspokesman'];
        $fillegaltypecode = $this->P['fillegaltypecode'];
        $fexpressions = $this->P['fexpressions'];
        $fexpressioncodes = $this->P['fexpressioncodes'];
        $fillegalcontent = $this->P['fillegalcontent'];
        $fstarttime      = strtotime($this->P['fstart']);
        $fendtime        = strtotime($this->P['fend']);
        $fissuedate      = date('Y-m-d',$fstarttime);
        $flength         = $fendtime - $fstarttime;
        if($flength<1 || $flength>86400){
            $this->ajaxReturn(['code'=>1, 'msg'=>'播出时段选择有误']);
        }
        if($fadname == '查看段'){
            $this->ajaxReturn(['code'=>1,'msg'=>'请正确输入广告名称']);
        }
        //广告主ID
        $fadowner = M('tadowner')
         ->where(array('fname'=>$fadownername))
            ->getField('fid');
        if(empty($fadowner)){
            $fadowner = 0;
        }

        $sam_source_path = A('Common/Media','Model')->get_m3u8($fmediaid, $fstarttime, $fendtime);
        $mediaInfo       = M('tmedia')->where(['fid' => $fmediaid])->find();
        $fmedianame      = $mediaInfo['fmedianame'];
        $fmediaclassid   = $mediaInfo['fmediaclassid'];
        $fregionid       = $mediaInfo['media_region_id'];
        // 校验是否允许提交
        $avaTimeStamp    = A('Common/Media','Model')->available_time($fmediaid);
        $isAvailable     = (strtotime($fissuedate) <= $avaTimeStamp) ? true : false;
        if(!$isAvailable){
            $this->ajaxReturn(['code'=>1, 'msg'=>'数据处理中，无法提交']);
        }

        // 查找包含新插入广告的时段，暂只支持在一条广告时段内插入，不支持跨广告时段插入新广告
        // $parentWhere = [
        //     'fmediaid'   => $fmediaid,
        //     'fstarttime' => ['LT',$fstarttime],
        //     'fendtime' => ['GT',$fendtime],
        //     'fuserid' => $fcustomer,
        // ];
        $parentWhere = [
            'fid'   => $fid,
            'fuserid' => $fcustomer,
        ];
        $parentAd = M('sh_issue')
            ->where($parentWhere)
            ->order('fstarttime DESC')
            ->find();
        if($fendtime >= $parentAd['fendtime'] || $fstarttime <= $parentAd['fstarttime']){
            $this->ajaxReturn(['code'=>1, 'msg'=>'请确认起止时间，不支持跨时段插入新广告']);
        }

        $dataAd = [
            'fuserid'        => $fcustomer,
            'fmediaid'        => $fmediaid,
            'fmedianame'      => $fmedianame,
            'fmediaclassid'   => $fmediaclassid,
            'fregionid'       => $fregionid,
            'fadname'         => $fadname,
            'fadclasscode'    => $fadclasscode,
            'fadowner'        => $fadowner,
            'fadownername'    => $fadownername,
            'fbrand'          => $fbrand,
            'fspokesman'      => $fspokesman,
            'fillegaltypecode' => $fillegaltypecode,
            'fexpressions' => $fexpressions,
            'fexpressioncodes' => $fexpressioncodes,
            'fillegalcontent' => $fillegalcontent,
            'fstarttime'      => $fstarttime,
            'fendtime'        => $fendtime,
            'fissuedate'      => $fissuedate,
            'flength'         => $flength,
            'sam_source_path' => $sam_source_path,
            'fcreator'        => 'insert_'.session('regulatorpersonInfo.fid'),
            'fcreatetime'     => date('Y-m-d H:i:s'),
            'fmodifier'       => session('regulatorpersonInfo.fid'),
            'fmodifytime'     => date('Y-m-d H:i:s')
        ];
        // 判断添加的广告与已存在的记录时间是否重叠
        $isOverlap = M('sh_issue')
            ->where([
                'fmediaid' => $fmediaid,
                '_string' => 'fstarttime<'.$fstarttime.' and fendtime>'.$fstarttime.' or fstarttime<'.$fendtime.' and fendtime>'.$fendtime,
                'fuserid' => $fcustomer,
                'fid' => ['neq',$fid],
            ])
            ->find();
        if(!empty($isOverlap)){
            $this->ajaxReturn(['code'=>1,'msg'=>'不允许插入时段有交叉情况']);
        }else{
            M('sh_issue')->add($dataAd);
            $this->ajaxReturn(['code'=>0,'msg'=>'成功']);
        }
    }

    /**
     * 上海数据检查-编辑遗漏的广告
     */
    public function EditAd(){
        $fcustomer = $this->P['fcustomer'];
        $fid = $this->P['fid'];
        $fmediaid = $this->P['fmediaid'];
        $fadname = $this->P['fadname'];
        $fadclasscode = $this->P['fadclasscode'];
        $fadownername = $this->P['fadownername'];
        $fbrand = $this->P['fbrand'];
        $fspokesman = $this->P['fspokesman'];
        $fillegaltypecode = $this->P['fillegaltypecode'];
        $fexpressions = $this->P['fexpressions'];
        $fexpressioncodes = $this->P['fexpressioncodes'];
        $fillegalcontent = $this->P['fillegalcontent'];
        $fstarttime = strtotime($this->P['fstart']);
        $fendtime = strtotime($this->P['fend']);
        $fissuedate = date('Y-m-d',$fstarttime);
        $flength = $fendtime - $fstarttime;
        if($flength<1 || $flength>86400){
            $this->ajaxReturn(['code'=>1, 'msg'=>'播出时段选择有误']);
        }
        if($fadname == '查看段'){
            $this->ajaxReturn(['code'=>1,'msg'=>'请正确输入广告名称']);
        }
        if(substr($fadclasscode, 0 , 2) == 23){
            $this->ajaxReturn(['code'=>1, 'msg'=>'不允许添加其它类广告类别的广告']);
        }
        //广告主ID
        $fadowner = M('tadowner')
         ->where(array('fname'=>$fadownername))
            ->getField('fid');
        if(empty($fadowner)){
            $fadowner = 0;
        }
        
        $sam_source_path = A('Common/Media','Model')->get_m3u8($fmediaid, $fstarttime, $fendtime);
        $mediaInfo = M('tmedia')->where(['fid'=>$fmediaid])->find();
        $fmedianame = $mediaInfo['fmedianame'];
        $fmediaclassid = $mediaInfo['fmediaclassid'];
        $fregionid = $mediaInfo['media_region_id'];
        // 校验是否可检查
        $isAvailable = $this->isMediaDateAvailable($fmediaid,strtotime($fissuedate));
        if(!$isAvailable){
            $this->ajaxReturn(['code'=>1, 'msg'=>'数据处理中，无法提交']);
        }

        $dataAd = [
            'fuserid' => $fcustomer,
            'fmediaid' => $fmediaid,
            'fmedianame' => $fmedianame,
            'fmediaclassid' => $fmediaclassid,
            'fregionid' => $fregionid,
            'fadname' => $fadname,
            'fadclasscode' => $fadclasscode,
            'fadowner' => $fadowner,
            'fadownername' => $fadownername,
            'fbrand' => $fbrand,
            'fspokesman' => $fspokesman,
            'fillegaltypecode' => $fillegaltypecode,
            'fexpressions' => $fexpressions,
            'fexpressioncodes' => $fexpressioncodes,
            'fillegalcontent' => $fillegalcontent,
            'fstarttime' => $fstarttime,
            'fendtime' => $fendtime,
            'fissuedate' => $fissuedate,
            'flength' => $flength,
            'sam_source_path' => $sam_source_path,
            'fmodifier' => session('regulatorpersonInfo.fid'),
            'fmodifytime' => date('Y-m-d H:i:s')
        ];
        $existWhere = [
            'fid' => $fid,
        ];
        $issueInfo = M('sh_issue')
            ->where($existWhere)
            ->find();
        if(empty($issueInfo)){
            $this->ajaxReturn(['code'=>1,'msg'=>'广告不存在']);
        }
        // 判断编辑后的广告与已存在的记录时间是否重叠
        $isOverlap = M('sh_issue')
            ->where([
                'fid' => ['NEQ',$fid],
                'fmediaid' => $fmediaid,
                'fstarttime' => ['LT',$fstarttime],
                'fendtime' => ['GT',$fendtime],
                'fuserid' => $fcustomer,
            ])
            ->find();

        // 判断添加的广告与已存在的记录时间是否重叠
        $where_lap2 = [
            'fmediaid' => $fmediaid,
            '_string' => '(fstarttime<'.$fstarttime.' and fendtime>'.$fstarttime.' or fstarttime<'.$fendtime.' and fendtime>'.$fendtime.')',
            'fuserid' => $fcustomer,
        ];
        if(!empty($isOverlap)){
            $where_lap2['fid'] = ['notin',[$fid,$isOverlap['fid']]];
        }else{
            $where_lap2['fid'] = ['neq',$fid];
        }
        $isOverlap2 = M('sh_issue')
            ->where($where_lap2)
            ->find();
        if(empty($isOverlap2)){
            // 更新当前发布记录
            $ret = M('sh_issue')->where($existWhere)->save($dataAd);
            $data['fid'] = $fid;
            // 更新当天相同样本的所有发布记录
            $dataRelateAd = [
                'fadname' => $fadname,
                'fadclasscode' => $fadclasscode,
                'fadowner' => $fadowner,
                'fadownername' => $fadownername,
                'fbrand' => $fbrand,
                'fillegaltypecode' => $fillegaltypecode,
                'fexpressions' => $fexpressions,
                'fexpressioncodes' => $fexpressioncodes,
                'fillegalcontent' => $fillegalcontent,
                'fmodifier' => session('regulatorpersonInfo.fid'),
                'fmodifytime' => date('Y-m-d H:i:s')
            ];
            if(!empty($issueInfo['fsampleid'])){
                $saveWhere = [
                    'fsampleid' => $issueInfo['fsampleid'],
                    'fmediaid' => $issueInfo['fmediaid'],
                    'fissuedate' => $issueInfo['fissuedate'],
                    'fuserid'=>$fcustomer
                ];
                $retRelate = M('sh_issue')->where($saveWhere)->save($dataRelateAd);
            }
            
            if($ret){
                $this->ajaxReturn(['code'=>0,'msg'=>'编辑成功','data'=>$data]);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'编辑出错']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'开始时间或结束时间与已有广告重叠，请修改时间']);
        }
    }

    /**
     * 上海数据检查-删除遗漏的广告
     */
    public function DelAd(){
        $fid = $this->P['fid'];
        if($fid){
            $del = M('sh_issue')->where(['fid'=>$fid])->delete();
            if(!empty($del)){
                $this->ajaxReturn(['code'=>0,'msg'=>'删除成功']);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'删除失败，记录不存在']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 根据媒介获取可检查日期
     */
    public function getAvaDateByMedia(){
        $fmediaid   = $this->P['fmediaid'];
        if($fmediaid){
            $avaTimeStamp = A('Common/Media','Model')->available_time($fmediaid);
            $data = [
                'avatimestamp' => date('Y-m-d',$avaTimeStamp)
            ];
            $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$data]);
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 内部媒体数据检查任务列表
     */
    public function PlanList(){
        $fmedianame    = $this->P['fmedianame'];
        $fissuedateS = $this->P['fissuedateS'];
        $fissuedateE = $this->P['fissuedateE'];
        $fdstatus    = $this->P['fdstatus'];
        $pageIndex   = (int)($this->P['pageIndex'] ? $this->P['pageIndex'] : 1);
        $pageSize    = (int)($this->P['pageSize'] ? $this->P['pageSize'] : 1000);
        $limitIndex  = ($pageIndex-1)*$pageSize;
        $where = [];

        if(!empty($fissuedateS) && !empty($fissuedateE)){
            $where['a.fissuedate'] = ['BETWEEN',[$fissuedateS,$fissuedateE]];
        }
        if((int)$fdstatus >= 0 &&(int)$fdstatus<= 99){
            $where['a.fdstatus'] = $fdstatus;
        }else{
            $where['a.fdstatus'] = ['in',[2,3,4]];
        }
        if(!empty($fmedianame)){
            $where['b.fmedianame'] = ['like','%'.$fmedianame.'%'];
        }

        $count = M('tapimediatask')
            ->alias('a')
            ->field('a.*,b.fmedianame')
            ->join('tmedia b on a.fmediaid = b.fid')
            ->where($where)
            ->count();

        $dataSet = M('tapimediatask')
                ->alias('a')
                ->field('a.*,b.fmedianame')
                ->join('tmedia b on a.fmediaid = b.fid')
                ->where($where)
                ->limit($limitIndex,$pageSize)
                ->order('a.fdstatus asc,a.fmediaid,a.fissuedate DESC')
                ->select();
        foreach ($dataSet as $key => $value) {
            // $dataSet[$key]['isopen'] = $this->isMediaDateAvailable($value['fmediaid'],strtotime($value['fissuedate']));
            $dataSet[$key]['isopen'] = 1;
        }
        $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$count, 'data'=>$dataSet]);
    }

    /**
     * 开始检查前校验及数据获取
     */
    public function startCheckPlan(){
        $fid        = $this->P['fid'];
        $jiancha    = $this->P['jiancha'];//是否进行重新检查
        // 计划添加或更新状态
        $whereIsPlanExist = ['fid' => $fid];
        $infoPlanExist = M('tapimediatask')->where($whereIsPlanExist)->find();
        $fmediaid = $infoPlanExist['fmediaid'];
        $fissuedate = $infoPlanExist['fissuedate'];
        if(!empty($fmediaid) && !empty($fissuedate)){
            // 获取媒体数据准备就绪的时间戳，判断是否可以检查
            try {
                // 校验是否可检查
                $isAvailable = $this->isMediaDateAvailable($fmediaid,strtotime($fissuedate));
                if($isAvailable){
                    
                    if(!empty($infoPlanExist)){
                        // 抽取数据至sh_issue
                        if($infoPlanExist['fstatus'] == 0 || !empty($jiancha)){
                            $resIssue = A('Api/ShIssue','Model')->CheckIssue($fissuedate,$fmediaid,$infoPlanExist['fcustomer']);
                            $fdstatus = 3;
                        }

                        if(empty($fdstatus)){
                            if($infoPlanExist['fdstatus'] != 4){
                                $fdstatus = 3;
                            }else{
                                $fdstatus = 4;
                            }
                        }
                        
                        // 更新计划表状态为进行中且fstatus抽取状态为1
                        $setPlanExist = [
                            'fstatus'   => 1,
                            'feditor'   => session('regulatorpersonInfo.fid') ? session('regulatorpersonInfo.fid') : 'API',
                            'fmodifytime' => date('Y-m-d H:i:s'),
                            'fdstatus'  => $fdstatus,
                        ];
                        $res = M('tapimediatask')->where($whereIsPlanExist)->save($setPlanExist);
                        // 若是正在检查的任务进行提示但仍允许检查
                        if($infoPlanExist['fdstatus'] == 1){
                            $this->ajaxReturn(['code'=>2, 'msg'=>'该计划正在被检查中，是否仍然检查']);
                        }
                        $this->ajaxReturn(['code'=>0, 'msg'=>'数据已就绪']);
                    }else{
                        $this->ajaxReturn(['code'=>1, 'msg'=>'计划不存在']);
                    }
                }else{
                    $this->ajaxReturn(['code'=>1, 'msg'=>'数据生产中，请稍后检查']);
                }
            } catch (\Throwable $th) {
                $this->ajaxReturn(['code'=>1, 'msg'=>$th]);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 上海数据检查-返回时恢复状态
     */
    public function backPlan(){
        $fid = $this->P['fid'];
        if($fid){
            $planInfo = M('tapimediatask')->where(['fid'=>$fid,'fdstatus'=>3])->save(['fstatus'=>2]);
            if(!empty($planInfo)){
                $this->ajaxReturn(['code'=>0, 'msg'=>'成功']);
            }else{
                $this->ajaxReturn(['code'=>0, 'msg'=>'该状态无需更新']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 上海数据检查-完成检查
     */
    public function submitPlan(){
        $fid = $this->P['fid'];
        if($fid){
            $planInfo = M('tapimediatask')->where(['fid'=>$fid])->find();
            // 校验是否可检查
            $isAvailable = $this->isMediaDateAvailable($planInfo['fmediaid'],$planInfo['fissuedate']);
            if($isAvailable){
                $where = [
                    'fid' => $fid,
                    'fdstatus' => ['in',[2,3]]
                ];
                
                $data = [
                    'fdstatus' => 4,
                    'fmodifier' => session('regulatorpersonInfo.fid'),
                    'fmodifytime' => date('Y-m-d H:i:s')
                ];
                $res = M('tapimediatask')
                    ->where($where)
                    ->save($data);
                if($res){
                    $this->ajaxReturn(['code'=>0, 'msg'=>'成功']);
                }else{
                    $this->ajaxReturn(['code'=>1, 'msg'=>'出错']);
                }
            }else{
                $this->ajaxReturn(['code'=>1, 'msg'=>'数据处理中，无法提交']);
            }
        }else{
            $this->ajaxReturn(['code'=>1, 'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 返回广告类型树 TODO:有待抽象到模型
     */
    public function getAdClassArr()
    {
        $data = M('tadclass')
            ->field('fcode, fpcode, fadclass')
            ->where(['fstate' => ['EQ', 1], 'fcode' => ['NEQ', 2301]])
            ->cache(true)
            ->select();
        $res = [];
        foreach ($data as $key => &$value) {
            if (strlen($value['fcode']) === 2){
                $res[] = [
                    'value' => $value['fcode'],
                    'label' => $value['fadclass'],
                ];
            }
        }
        foreach ($data as $key => &$value2) {
            $value2['value'] = $value2['fcode'];
            $value2['label'] = $value2['fadclass'];
            if (strlen($value2['fcode']) === 4){
                foreach ($res as &$item) {
                    if ($value2['fpcode'] === $item['value']){
                        $item['children'][] = $value2;
                        break;
                    }
                }
            }
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$res]);
    }

    /**
     * 商业类型树 TODO:有待抽象到模型
     */
    public function getAdClassTree()
    {
        $data = M('hz_ad_class')->field('fcode value, fpcode, fname label')->where(['fstatus' => ['EQ', 1]])->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpcode']])){
                $items[$value['fpcode']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$tree]);
    }

    /**
     * 判断媒体日期数据是否就绪
     * @param   Int $fmediaid 媒体ID
     * @param   Int $fissuedate 日期时间戳
     * @return  Int $isAvailable 1-就绪 0-未就绪
     */
    public function isMediaDateAvailable($fmediaid = 0,$fissuedate = 0){
        $isAvailable = 0;
        if($fmediaid && $fissuedate){
            $avaTimeStamp = A('Common/Media','Model')->available_time($fmediaid);
            $isAvailable = ($fissuedate <= $avaTimeStamp) ? 1 : 0;
        }
        return $isAvailable;
    }
}
