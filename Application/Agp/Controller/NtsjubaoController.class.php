<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 投诉举报
 */

class NtsjubaoController extends BaseController
{

	/**
	 *投诉举报操作
	 * @return array|string code-状态（0成功1失败），msg-提示信息）
	 * by zw
	 */
	public function create_register()
	{
		$submit 		= I('submit')?I('submit'):2;//1保存，2提交
		$fwaittreid 	= I('fwaittreid');//待处理单位
		$attachinfo 	= I('attachinfo');//附件信息
		
		$res['fregisterregulatorpid']	= session('regulatorpersonInfo.fregulatorpid');//登记机构id
		$res['fregisterregulatorpname']	= session('regulatorpersonInfo.regulatorpname');//登记机构名称
		$res['fregisterregulatorid']	= session('regulatorpersonInfo.fregulatorid');//登记单位名称
		$res['fregisterregulatorname']	= session('regulatorpersonInfo.regulatorname');//登记单位名称
		$res['fregisterpersonid']		= session('regulatorpersonInfo.fid');//登记人id
		$res['fregisterpersonname']		= session('regulatorpersonInfo.fname');//登记人姓名
		$res['fnumber']					= I('fnumber');//编号
		$res['fpersonname']				= I('fpersonname');//投举方
		$res['fpersontype']				= I('fpersontype');//举报人类别
		$res['fpersonmobile']			= I('fpersonmobile');//举报人联系电话
		$res['fname']					= I('fname');//广告名称
		$res['fadowner']				= I('fadowner');//广告主
		$res['fadclass']				= I('fadclass');//广告类别
		$res['fmediaid']				= I('fmediaid');//发布媒体,下拉选择
		$res['fmediatype']				= I('fmediatype');//媒体类别，1电视，2广播，3报纸，13互联网
		$res['fsendtime'] 				= I('fsendtime');//广告发布时间
		$res['fsendcount'] 				= I('fsendcount');//广告发布次数
		$res['fgivetime'] 				= I('fgivetime');//交办时间
		$res['fendtimes'] 				= I('fendtimes');//回复截止日期
		$res['ffromtype']				= I('ffromtype');//来源分类，10广告监测、20投诉举报、30省局交办、40其他机关移送
		$res['fappeal']					= I('fappeal');//投拆内容
		$res['fregistertime']			= I('fregistertime');//登记时间
		$res['fwaittreid']				= $fwaittreid;//分派接收单位
		$res['fcreatetime'] 			= date('Y-m-d H:i:s');//创建时间

		if($submit==2){//提交
			$res['fstate'] 			= 30;//线索待处理
			$fregisterid = M("nj_tregister")->add($res);//添加登记信息

			/*新增当前流程记录*/
			$sh_list['fregisterid'] 				= $fregisterid;
			$sh_list['fnextflowid'] 				= 0;
			$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
			$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
			$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
			$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
			$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
			$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
			$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
			$sh_list['ffinishtime'] 				= date('Y-m-d H:i:s');
			$sh_list['freason'] 					= '线索分派';
			$sh_list['fflowname'] 					= '线索登记';
			$sh_list['fstate'] 						= 0;
			$sh_list['fsendstatus'] 				= 10;
			$sh_list['feffectivestatus'] 			= 10;
			$flowid = M("nj_tregisterflow")->add($sh_list);

			/*新增下一流程记录*/
			$next_list['fregisterid'] 				= $fregisterid;
			$next_list['fcreateregualtorid'] 		= $fwaittreid;
			$next_list['fflowname'] 				= '线索处理';
			$next_list['fstate'] 					= 30;
			$next_list['fsendstatus'] 				= 0;
			$next_list['fcreatetime'] 				= date('Y-m-d H:i:s');
			$nextflowid = M("nj_tregisterflow")->add($next_list);

			M()->execute('update nj_tregisterflow set fnextflowid='.$nextflowid.' where flowid='.$flowid);

			//短信通知办案用户
			$ptname = M('tregion')->where(['fid'=>$system_num])->getField('fname1',true);
			$mobiles = M('tregulatorperson')->where(['fregulatorid'=>$fwaittreid])->getField('fmobile',true);
			foreach ($mobiles as $value) {
				if(strlen($value) == 11 && is_numeric($value)){
					A('Common/Alitongxin','Model')->illtask_sms($value,$res['fname'],$res['fnumber'],$ptname);
				}
			}
		}else{
			$res['fstate'] 			= 0;//待登记
			$fregisterid = M("nj_tregister")->add($res);

			/*新增当前流程记录*/
			$sh_list['fregisterid'] 				= $fregisterid;
			$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
			$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
			$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
			$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
			$sh_list['fcreatetime'] 				= date('Y-m-d H:i:s');
			$sh_list['fflowname'] 					= '线索登记';
			$sh_list['fstate'] 						= 0;
			$sh_list['fsendstatus'] 				= 0;
			$sh_list['feffectivestatus'] 			= 10;
			$flowid = M("nj_tregisterflow")->add($sh_list);
		}

		//如果上传附件
		if(!empty($attachinfo)&&!empty($fregisterid)){
			$attach_data['fregisterid'] 			= $fregisterid;//线索登记id
			$attach_data['fillegaladflowid'] 		= $flowid;//流程ID
			$attach_data['fuploadtime'] 			= date('Y-m-d H:i:s');//上传时间
			$attach_data['fcreateregualtorid'] 		= session('regulatorpersonInfo.fregulatorpid');//上传机构id
			$attach_data['fcreateuserid'] 			= session('regulatorpersonInfo.fid');//上传用户ID
			foreach ($attachinfo as $key => $value){
				$attach_data['fattachname'] 		= $value['fattachname'];
				$attach_data['fattachurl'] 			= $value['fattachurl'];
				$attach_data['ffilename'] 			= preg_replace('/\..*/','',$value['fattachurl']);
				$attach_data['ffiletype'] 			= preg_replace('/.*\./','',$value['fattachname']);
				$attach[$key] 						= $attach_data;
			}
			$attachid = M('nj_tregisterfile')->addAll($attach);
		}

		if($fregisterid){
			D('Function')->write_log('投拆举报',1,'操作成功','nj_tregister',$fregisterid,M('nj_tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
		}else{
			D('Function')->write_log('投拆举报',0,'操作失败','nj_tregister',0,M('nj_tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
		}
	}

	/**
	 *监测发现处理操作
	 * @return array|string code-状态（0成功1失败），msg-提示信息）
	 * by zw
	 */
	public function create_register2()
	{
		$system_num = getconfig('system_num');
		$submit 		= I('submit')?I('submit'):2;//1保存，2提交
		$fillegaladid  	= I('fillegaladid');//违法广告ID
		$fwaittreid 	= I('fwaittreid');//待处理单位
		$attachinfo 	= I('attachinfo');//附件信息
		$documentids 	= I('documentids');//文书ID组

		$fnumber		= I('fnumber');//编号
		$fgivetime 		= I('fgivetime');//交办时间
		$fregistertime	= I('fregistertime');//登记时间
		$fendtimes 		= I('fendtimes');//回复截止日期

		$where_tia['a.fid'] = $fillegaladid;
		$do_tia = M('tbn_illegal_ad')
			->field('c.*,a.fmedia_id,a.fillegal,a.fad_name,a.fmedia_class,a.fsample_id')
			->alias('a')
			->join('tmedia b on b.fid=a.fmedia_id')
			->join('(select fillegal_ad_id,min(fissue_date) as fissue_date,count(*) as issuecount from tbn_illegal_ad_issue group by fillegal_ad_id) c on a.fid=c.fillegal_ad_id')
			->where($where_tia)
			->find();
		if($do_tia['fmedia_class'] == 1){
			$res['fmediatype'] = '01';

			$where_sv['a.fid'] = $do_tia['fsample_id'];
			$do_sv = M('ttvsample')
				->field('c.fname,d.fcode,d.fpcode')
				->alias('a')
				->join('tad b on a.fadid=b.fadid and b.fadid<>0')
				->join('tadowner c on b.fadowner=c.fid')
				->join('tadclass d on b.fadclasscode=d.fcode')
				->where($where_sv)
				->find();
		}elseif($do_tia['fmedia_class'] == 2){
			$res['fmediatype'] = '02';

			$where_sv['a.fid'] = $do_tia['fsample_id'];
			$do_sv = M('tbcsample')
				->field('c.fname,d.fcode,d.fpcode')
				->alias('a')
				->join('tad b on a.fadid=b.fadid and b.fadid<>0')
				->join('tadowner c on b.fadowner=c.fid')
				->join('tadclass d on b.fadclasscode=d.fcode')
				->where($where_sv)
				->find();
		}elseif($do_tia['fmedia_class'] == 3){
			$res['fmediatype'] = '03';

			$where_sv['a.fpapersampleid'] = $do_tia['fsample_id'];
			$do_sv = M('tpapersample')
				->field('c.fname,d.fcode,d.fpcode')
				->alias('a')
				->join('tad b on a.fadid=b.fadid and b.fadid<>0')
				->join('tadowner c on b.fadowner=c.fid')
				->join('tadclass d on b.fadclasscode=d.fcode')
				->where($where_sv)
				->find();
		}elseif($do_tia['fmedia_class'] == 13){
			$res['fmediatype'] = '13';

			$where_sv['a.major_key'] = $do_tia['fsample_id'];
			$do_sv = M('tnetissue')
				->field('c.fname,d.fcode,d.fpcode')
				->alias('a')
				->join('tadowner c on a.fadowner=c.fid')
				->join('tadclass d on a.fadclasscode=d.fcode')
				->where($where_sv)
				->find();
		}
		
		$res['fregisterregulatorpid']	= session('regulatorpersonInfo.fregulatorpid');//登记机构id
		$res['fregisterregulatorpname']	= session('regulatorpersonInfo.regulatorpname');//登记机构名称
		$res['fregisterregulatorid']	= session('regulatorpersonInfo.fregulatorid');//登记单位名称
		$res['fregisterregulatorname']	= session('regulatorpersonInfo.regulatorname');//登记单位名称
		$res['fregisterpersonid']		= session('regulatorpersonInfo.fid');//登记人id
		$res['fregisterpersonname']		= session('regulatorpersonInfo.fname');//登记人姓名
		$res['fnumber']					= $fnumber;//编号
		$res['fpersonname']				= '监测系统';//投举方
		$res['fpersontype']				= 0;//举报人类别
		$res['fpersonmobile']			= '';//举报人联系电话
		$res['fname']					= $do_tia['fad_name'];//广告名称
		$res['fadowner']				= $do_sv['fname'];//广告主
		if(!empty($do_sv['fpcode'])){
			$res['fadclass']			= $do_sv['fpcode'].','.$do_sv['fcode'];//广告类别
		}else{
			$res['fadclass']			= $do_sv['fcode'];//广告类别
		}
		$res['fmediaid']				= $do_tia['fmedia_id'];//发布媒体,下拉选择
		$res['fsendtime'] 				= $do_tia['fissue_date'];//广告发布时间
		$res['fsendcount'] 				= $do_tia['issuecount'];//广告发布次数
		$res['fgivetime'] 				= $fgivetime;//交办时间
		$res['fendtimes'] 				= $fendtimes;//回复截止日期
		$res['ffromtype']				= 10;//来源分类，10广告监测、20投诉举报、30省局交办、40其他机关移送
		$res['fappeal']					= $do_tia['fillegal'];//投拆内容
		$res['fregistertime']			= $fregistertime;//登记时间
		$res['fwaittreid']				= $fwaittreid;//分派接收单位
		$res['fcreatetime'] 			= date('Y-m-d H:i:s');//创建时间

		if($submit==2){//提交
			$res['fstate'] 			= 30;//线索待处理

			$undertake 	= I('undertake');//承办状态,10自办、20转办
			$result 	= I('result');//处理建议
			if($undertake == 10){//自办
				$fpunish = I('fpunish');//处罚方式
				if(empty($fpunish)){
					$this->ajaxReturn(array('code'=>1,'msg'=>'请选择处罚方式'));
				}
				$res['fpunish'] = $fpunish;
				$res['fdealregulatorpid'] 	= session('regulatorpersonInfo.fregulatorpid');
				$res['fdealregulatorpname'] = session('regulatorpersonInfo.regulatorpname');
				$res['fdealtime'] 			= date('Y-m-d H:i:s');
				$res['fstate'] 	= 40;
				$fregisterid = M("nj_tregister")->add($res);//添加登记信息

				/*新增当前流程记录*/
				$sh_list['fregisterid'] 				= $fregisterid;
				$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
				$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
				$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
				$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
				$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
				$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
				$sh_list['freason'] 		= '线索自办';
				$sh_list['fflowname'] 		= '线索处理';
				$sh_list['fcreateinfo'] 	= $result;
				$sh_list['fchildstate'] 	= $undertake;
				$sh_list['fstate'] 			= 30;
				$sh_list['fsendstatus'] 	= 10;
				$sh_list['feffectivestatus'] = 10;
				$sh_list['fcreatetime'] 	= date('Y-m-d H:i:s');
				$sh_list['ffinishtime'] 	= date('Y-m-d H:i:s');
				$flowid = M('nj_tregisterflow') ->add($sh_list);

				/*新增下一流程记录*/
				$next_list['fregisterid'] 				= $fregisterid;
				$next_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
				$next_list['fcreateregualtorpname'] 	= session('regulatorpersonInfo.regulatorpname');
				$next_list['fcreateregualtorid'] 		= session('regulatorpersonInfo.fregulatorid');
				$next_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
				$next_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
				$next_list['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
				$next_list['freason'] 					= '已完成办结';
				$next_list['fflowname'] 				= '线索办结';
				$next_list['fstate'] 					= 40;
				$next_list['fsendstatus'] 				= 10;
				$next_list['feffectivestatus'] 			= 10;
				$next_list['fcreatetime'] 				= date('Y-m-d H:i:s');
				$next_list['ffinishtime'] 				= date('Y-m-d H:i:s');
				$nextflowid = M("nj_tregisterflow")->add($next_list);
				
				M()->execute('update nj_tregisterflow set fnextflowid='.$nextflowid.' where flowid='.$flowid);
				//更改违法广告的处理状态
				if(!empty($fillegaladid)){
					$data_tid['fview_status'] 	= 10;
					$data_tid['fstatus'] 		= 10;
					$where_tid['fid'] 			= $fillegaladid;
					$where_tid['fstatus'] 		= 0;
					$where_tid['fcustomer'] 	= $system_num;
					M('tbn_illegal_ad')
						->where($where_tid)
						->save($data_tid);
				}

				//短信通知办案用户
				$ptname = M('tregion')->where(['fid'=>$system_num])->getField('fname1',true);
				$mobiles = M('tregulatorperson')->where(['fregulatorid'=>$fwaittreid])->getField('fmobile',true);
				foreach ($mobiles as $value) {
					if(strlen($value) == 11 && is_numeric($value)){
						A('Common/Alitongxin','Model')->illtask_sms($value,$do_tia['fad_name'],$res['fnumber'],$ptname);
					}
				}
			}elseif ($undertake == 20) {//转办下级
				$res['fstate'] 	= 30;
				$fregisterid = M("nj_tregister")->add($res);//添加登记信息

				/*新增当前流程记录*/
				$sh_list['fregisterid'] 				= $fregisterid;
				$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
				$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
				$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
				$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
				$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
				$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
				$sh_list['freason'] 		= '线索转办';
				$sh_list['fflowname'] 		= '线索处理';
				$sh_list['fcreateinfo'] 	= $result;
				$sh_list['fnextflowid'] 	= 0;
				$sh_list['fchildstate'] 	= $undertake;
				$sh_list['fstate'] 			= 30;
				$sh_list['fsendstatus'] 	= 10;
				$sh_list['feffectivestatus'] = 10;
				$sh_list['fcreatetime'] 	= date('Y-m-d H:i:s');
				$sh_list['ffinishtime'] 	= date('Y-m-d H:i:s');
				$flowid = M('nj_tregisterflow')->master(true) ->add($sh_list);

				/*新增下一流程记录*/
				$next_list['fregisterid'] 				= $fregisterid;
				$next_list['fcreateregualtorpid'] 		= $fwaittreid;
				$next_list['fflowname'] 				= '线索转办处理';
				$next_list['fstate'] 					= 30;
				$next_list['fsendstatus'] 				= 0;
				$next_list['fcreatetime'] 				= date('Y-m-d H:i:s');
				$nextflowid = M("nj_tregisterflow")->master(true)->add($next_list);

				M()->execute('update nj_tregisterflow set fnextflowid='.$nextflowid.' where flowid='.$flowid);
			}
			
		}

		if(!empty($documentids) && !empty($fregisterid)){
			foreach ($documentids as $key => $value) {
				$do_nte = M('nj_tregisterfile')
					->where('fid='.$value)
					->find();
				if(!empty($do_nte)){
					M('nj_tregisterfile')->where('fid = '.$value)->save(array('fregisterid'=>$fregisterid));
					M('document')->where('dt_id='.$do_nte['fnumber'])->save(array('dt_tregisterid'=>$fregisterid));
				}
			}
		}

		//如果上传附件
		if(!empty($attachinfo)&&!empty($fregisterid)){
			$attach_data['fregisterid'] 			= $fregisterid;//线索登记id
			$attach_data['fillegaladflowid'] 		= $flowid;//流程ID
			$attach_data['fuploadtime'] 			= date('Y-m-d H:i:s');//上传时间
			$attach_data['fcreateregualtorid'] 		= session('regulatorpersonInfo.fregulatorpid');//上传机构id
			$attach_data['fcreateuserid'] 			= session('regulatorpersonInfo.fid');//上传用户ID
			foreach ($attachinfo as $key => $value){
				$attach_data['fattachname'] 		= $value['fattachname'];
				$attach_data['fattachurl'] 			= $value['fattachurl'];
				$attach_data['ffilename'] 			= preg_replace('/\..*/','',$value['fattachurl']);
				$attach_data['ffiletype'] 			= preg_replace('/.*\./','',$value['fattachname']);
				$attach[$key] 						= $attach_data;
			}
			$attachid = M('nj_tregisterfile')->addAll($attach);
		}

		if($fregisterid){
			M('tbn_illegal_ad')->where('fid = '.$fillegaladid)->save(array('fregisterid'=>$fregisterid));
			D('Function')->write_log('监测发现',1,'操作成功','nj_tregister',$fregisterid,M('nj_tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
		}else{
			D('Function')->write_log('监测发现',0,'操作失败','nj_tregister',0,M('nj_tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
		}
	}

	/**
	 *获取草稿箱列表
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function get_registerlist(){

		$p  = I('page', 1);//当前第几页
    	$pp = 20;//每页显示多少记录

    	$fname 	= I('fname');//广告名称
		$fnumber = I('fnumber');//文号
		$ffromtype 	= I('ffromtype');//来源分类
		$fstate = I('fstate');//状态
		$fmedianame = I('fmedianame');//发布媒体

		if(!empty($fname)){
			$where_fr['a.fname'] = array('like','%'.$fname.'%');
		}
		if(!empty($fnumber)){
			$where_fr['a.fnumber'] = array('like','%'.$fnumber.'%');
		}
		if(!empty($ffromtype)){
			$where_fr['a.ffromtype'] = $ffromtype;
		}
		if(empty($fstate)){
			$where_fr['b.fstate'] = array('in',array(0,13));
		}else{
			if($fstate == 10){
				$where_fr['b.fstate'] = 0;
			}else{
				$where_fr['b.fstate'] = 13;
			}
		}
		if(!empty($fmedianame)){
			$where_fr['c.fmedianame'] = array('like','%'.$fmedianame.'%');
		}

		$where_fr['fsendstatus'] = 0;
		$where_fr['_string'] = '(fcreateregualtorid = '.session('regulatorpersonInfo.fregulatorid').' or fcreateregualtorpid = '.session('regulatorpersonInfo.fregulatorpid').')';

		$count = M('nj_tregister')
			->alias('a')
			->join('nj_tregisterflow b on a.fid=b.fregisterid')
			->join('tmedia c on a.fmediaid=c.fid')
			->where($where_fr)
			->count();

		
		$do_fr = M('nj_tregister')
			->field('a.*,c.fmedianame')
			->alias('a')
			->join('nj_tregisterflow b on a.fid=b.fregisterid')
			->join('tmedia c on a.fmediaid=c.fid')
			->where($where_fr)
			->order('a.fid desc')
			->page($p,$pp)
			->select();

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_fr)));
	}

	/**
	 *获取登记信息
	 * by zw
	*/
	public function get_tregisterview(){
		$fid = I('fid');//登记表ID
		$fstate = I('fstate');//获取指定状态的流程记录，非必要参数
		
		$where_ftr['b.fregisterid'] = $fid;
		if(strlen($fstate)>0){
			$where_ftr['b.fstate'] = $fstate;
		}
		$do_ftr = M('nj_tregister')
			->field('a.*,b.fcreateinfo,b.fchildstate,b.fflowname')
			->alias('a')
			->join('nj_tregisterflow b on a.fid=b.fregisterid')
			->where($where_ftr)
			->order('flowid desc')
			->find();
		if(!empty($do_ftr)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_ftr));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败'));
		}
	}

	/**
	 *草稿箱提交操作
	 * @return array|string code-状态（0成功1失败），msg-提示信息）
	 * by zw
	 */
	public function update_register()
	{

		if(I('fid')){
			$fid = I('fid');//线索登记id
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}
		$submit 		= I('submit')?I('submit'):2;//1保存，2提交
		$fwaittreid 	= I('fwaittreid');//待处理单位
		
		$res['fnumber']					= I('fnumber');//编号
		$res['fpersonname']				= I('fpersonname');//投举方
		$res['fpersontype']				= I('fpersontype');//举报人类别
		$res['fpersonmobile']			= I('fpersonmobile');//举报人联系电话
		$res['fname']					= I('fname');//广告名称
		$res['fadowner']				= I('fadowner');//广告主
		$res['fadclass']				= I('fadclass');//广告类别
		$res['fmediaid']				= I('fmediaid');//发布媒体,下拉选择
		$res['fmediatype']				= I('fmediatype');//媒体类别，1电视，2广播，3报纸，13互联网
		$res['fsendtime'] 				= I('fsendtime');//广告发布时间
		$res['fsendcount'] 				= I('fsendcount');//广告发布次数
		$res['fgivetime'] 				= I('fgivetime');//交办时间
		$res['fendtimes'] 				= I('fendtimes');//回复截止日期
		$res['ffromtype']				= I('ffromtype');//来源分类，10广告监测、20投诉举报、30省局交办、40其他机关移送
		$res['fappeal']					= I('fappeal');//投拆内容
		$res['fregistertime']			= I('fregistertime');//登记时间
		$res['fwaittreid']				= $fwaittreid;//分派接收单位

		$where_fr['a.fid'] = $fid;
		$where_fr['b.fsendstatus'] = 0;
		$where_fr['a.fstate'] = array('in',array(0,13));
		$do_fr = M('nj_tregister')
			->field('a.fid,a.fstate,b.flowid,a.ffillegaladid,a.fregisterpersonid,a.fregisterpersonname,a.fregisterregulatorpid,a.fregisterregulatorpname,a.fregisterregulatorid,a.fregisterregulatorname')
			->alias('a')
			->join('nj_tregisterflow b on a.fid=b.fregisterid and a.fstate=b.fstate')
			->where($where_fr)
			->find();

		$fregisterid = $do_fr['fid'];
		$flowid = $do_fr['flowid'];

		if(!empty($do_fr)){
			if($submit==2){//登记
				$res['fstate'] 			= 30;//线索待处理
				$where_fr2['fid'] = $fregisterid;
				M("nj_tregister")->where($where_fr2)->save($res);//保存登记信息

				/*新增下一流程记录*/
				$next_list['fregisterid'] 				= $fregisterid;
				$next_list['fcreateregualtorid'] 		= $fwaittreid;
				$next_list['fflowname'] 				= '线索处理';
				$next_list['fstate'] 					= 30;
				$next_list['fsendstatus'] 				= 0;
				$next_list['fcreatetime'] 				= date('Y-m-d H:i:s');
				$nextflowid = M("nj_tregisterflow")->add($next_list);

				/*保存当前流程记录*/
				$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
				$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
				$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
				$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
				$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
				$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
				$sh_list['freason'] 					= '线索分派';
				$sh_list['fnextflowid'] 				= $nextflowid;
				$sh_list['fsendstatus'] 				= 10;
				$sh_list['feffectivestatus'] 			= 10;
				$sh_list['ffinishtime'] 				= date('Y-m-d H:i:s');
				M('nj_tregisterflow')
					->where('flowid='.$flowid)
					->save($sh_list);
				
			}else{
				$where_fr2['fid'] = $fregisterid;
				M("nj_tregister")->where($where_fr2)->save($res);//保存登记信息
			}

			D('Function')->write_log('草稿箱',1,'操作成功','nj_tregister',$fregisterid,M('nj_tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
		}else{
			D('Function')->write_log('草稿箱',0,'操作失败','nj_tregister',0,M('nj_tregister')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
		}
	}


	/**
	 * 获取违法广告信息列表
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function index() {
		session_write_close();
		$ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $ischeck = $ALL_CONFIG['ischeck'];

		$p 					= I('page', 1);//当前第几页
		$pp 				= 20;//每页显示多少记录
		$mclass 			= I('mclass')?I('mclass'):0;// 媒体类型
		$fadname 			= I('fadname');// 广告名称
		$fmedianame 		= I('fmedianame');// 发布媒介
		$fadclasscode 		= I('fadclasscode');// 广告内容类别
		$gxrqst 		= I('gxrqst');//更新开始日期
		$gxrqet 		= I('gxrqet');//更新结束日期
		$area 			= I('area');//所属地域

		$where['_string'] = '1=1';
		if ($fadname != '') {
			$where['a.fad_name'] = array('like', '%' . $fadname . '%');//广告名称
		}
		if ($fmedianame != '') {
			$where['d.fmedianame'] = array('like', '%' . $fmedianame . '%');//发布媒介
		}
		if (!empty($fadclasscode)) {
			$arr_code=D('Function')->get_next_tadclasscode($fadclasscode,true);//获取下级广告类别代码
			if($arr_code){
				$where['a.fad_class_code'] = array('in', $arr_code);
			}else{
				$where['a.fad_class_code'] = $fadclasscode;
			}
		}
		$where_time = '1=1';
		if(!empty($gxrqst)&&!empty($gxrqet)){
			$where_time = 'fissue_date between "'.$gxrqst.'" and "'.$gxrqet.'"';
		}
		//是否抽查模式
		if(!empty($ischeck)){
		    $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
		    //如果抽查表有数据
		    if(!empty($spot_check_data)){
		        $dates = [];//定义日期数组
		        foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
		            $year_month = '';
		            $date_str = [];
		            $year_month = substr($spot_check_data_val['fmonth'],0,7);
		            if(!empty($spot_check_data_val['condition'])){
		            	$date_str = explode(',',$spot_check_data_val['condition']);
		            }
		            foreach ($date_str as $date_str_val){
		                $dates[] = $year_month.'-'.$date_str_val;
		            }
		        }
				$where_time 	.= ' and tbn_illegal_ad_issue.fissue_date in ("'.implode('","', $dates).'")';
		    }else{
		        $where_time 	.= ' and 1=0';
		    }
		}
		
		$where['a.fstatus'] = 0;
		if(!empty($mclass)){
			$where['a.fmedia_class'] 	= $mclass;
		}
		if(!empty($area)){
			$where['fregion_id'] = $area;
		}
		$where['a.fregisterid'] = 0;
		$where['a.fcustomer'] 	= $system_num;

		$count = M('tbn_illegal_ad')
			->alias('a')
			->join('tmedia d on  a.fmedia_id=d.fid and d.fid=d.main_media_id')//媒介信息
			->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on a.fid = snd.illad_id')
			->join('tadclass e on a.fad_class_code=e.fcode ')//广告内容列别
			->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime from tbn_illegal_ad_issue where '.$where_time.' group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
			->where($where)
			->count();// 查询满足要求的总记录数

		
		$data = M('tbn_illegal_ad')
			->alias('a')
			->field('
				a.fid,
				a.fmedia_class as mclass,
	            a.fad_name as fadname,
	            d.fmedianame,
	            e.ffullname as adclass_fullname,
	            a.fillegal,
	            a.create_time,
	            x.fstarttime,
	            x.fendtime,
	            x.fcount,
	            (case when fmedia_class=1 then (select fexpressioncodes from ttvsample where fid=fsample_id) when fmedia_class=2 then (select fexpressioncodes from tbcsample where fid=fsample_id ) when fmedia_class=3 then (select fexpressioncodes from tpapersample where fpapersampleid=fsample_id ) end) as fexpressioncodes,
	            (case when fmedia_class=1 then (select fexpressions from ttvsample where fid=fsample_id) when fmedia_class=2 then (select fexpressions from tbcsample where fid=fsample_id ) when fmedia_class=3 then (select fexpressions from tpapersample where fpapersampleid=fsample_id ) end) as fexpressions,
	            (case when fmedia_class=1 then (select fconfirmations from ttvsample where fid=fsample_id) when fmedia_class=2 then (select fconfirmations from tbcsample where fid=fsample_id ) when fmedia_class=3 then (select fconfirmations from tpapersample where fpapersampleid=fsample_id ) end) as fconfirmations
			')
			->join('tmedia d on  a.fmedia_id=d.fid and d.fid=d.main_media_id')//媒介信息
			->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on a.fid = snd.illad_id')
			->join('tadclass e on a.fad_class_code=e.fcode ')//广告内容列别
			->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime from tbn_illegal_ad_issue where '.$where_time.' group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
			->where($where)
			->order('x.fstarttime desc')
			->page($p,$pp)
			->select();//查询违法广告

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	/**
	 *线索处理待办事项
	 * by zw
	 */
	public function waittask_list(){
		$p  = I('page', 1);//当前第几页
    	$pp = 20;//每页显示多少记录

    	$fname 	= I('fname');//广告名称
		$fnumber = I('fnumber');//文号
		$ffromtype 	= I('ffromtype');//来源分类
		$fmedianame = I('fmedianame');//发布媒体
		$djrqst = I('djrqst');//交办开始日期
		$djrqed = I('djrqed');//交办结束日期
		$rwzt 	= I('rwzt');//任务状态，0全部，10未定，20一般，30紧急

		if(!empty($rwzt)){
			if($rwzt == 10){
				$where_fr['_string'] = 'a.fid not in (select ftk_tregisterid from nj_tregistertrack,nj_tregister where nj_tregistertrack.ftk_tregisterid=nj_tregister.fid and (ftk_trepid='.session('regulatorpersonInfo.fregulatorpid').' or ftk_eqtrepid='.session('regulatorpersonInfo.fregulatorpid').') group by ftk_tregisterid)';
			}elseif($rwzt == 20){
				$where_fr['_string'] = 'a.fid in (select ftk_tregisterid from nj_tregistertrack,nj_tregister where nj_tregistertrack.ftk_tregisterid=nj_tregister.fid and (ftk_trepid='.session('regulatorpersonInfo.fregulatorpid').' or ftk_eqtrepid='.session('regulatorpersonInfo.fregulatorpid').') and TIMESTAMPDIFF(day,curdate(),ftk_endtime)>3 group by ftk_tregisterid)';
			}elseif($rwzt == 30){
				$where_fr['_string'] = 'a.fid in (select ftk_tregisterid from nj_tregistertrack,nj_tregister where nj_tregistertrack.ftk_tregisterid=nj_tregister.fid and (ftk_trepid='.session('regulatorpersonInfo.fregulatorpid').' or ftk_eqtrepid='.session('regulatorpersonInfo.fregulatorpid').') and TIMESTAMPDIFF(day,curdate(),ftk_endtime)<=3 group by ftk_tregisterid)';
			}
		}

		if(!empty($fname)){
			$where_fr['a.fname'] = array('like','%'.$fname.'%');
		}
		if(!empty($fnumber)){
			$where_fr['a.fnumber'] = array('like','%'.$fnumber.'%');
		}
		if(!empty($ffromtype)){
			$where_fr['a.ffromtype'] = $ffromtype;
		}
		if(!empty($fmedianame)){
			$where_fr['tm.fmedianame'] = array('like','%'.$fmedianame.'%');
		}
		if(!empty($djrqst)&&!empty($djrqed)){
			$where_fr['a.fgivetime'] = array('between',array($djrqst,$djrqed));
		}

		$where_fr['b.fstate'] = array('in',array(30,33,34,39));
		$where_fr['fsendstatus'] = 0;
		$where_fr['_string'] = '(fcreateregualtorid = '.session('regulatorpersonInfo.fregulatorid').' or fcreateregualtorpid = '.session('regulatorpersonInfo.fregulatorpid').')';

		$count = M('nj_tregister')
			->alias('a')
			->join('tmedia tm on a.fmediaid=tm.fid')
			->join('nj_tregisterflow b on a.fid=b.fregisterid')
			->where($where_fr)
			->count();

		
		$do_fr = M('nj_tregister')
			->field('a.*,b.fflowname,tm.fmedianame')
			->alias('a')
			->join('tmedia tm on a.fmediaid=tm.fid')
			->join('nj_tregisterflow b on a.fid=b.fregisterid')
			->where($where_fr)
			->order('a.fid desc')
			->page($p,$pp)
			->select();

		foreach ($do_fr as $key => $value) {
			$do_fr[$key]['diffdays'] = intval((strtotime($value['fendtimes']) - strtotime(date('Y-m-d')))/3600/24);
			if($value['fstate']!=40){
				$where_ftk['ftk_tregisterid'] = $value['fid'];
				$where_ftk['ftk_eqtrepid|ftk_trepid'] = session('regulatorpersonInfo.fregulatorpid');
				$do_ftk = M('nj_tregistertrack')
					->field('ftk_type,ftk_endtime,(case when TIMESTAMPDIFF(day,curdate(),ftk_endtime)<=3 then 10 else 0 end) as hotstatus')
					->where($where_ftk)
					->order('ftk_endtime asc')
					->select();
				$do_fr[$key]['gz_data'] = $do_ftk?$do_ftk:[];
			}else{
				$do_fr[$key]['gz_data'] = [];
			}
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_fr)));
	}

	/**
	 *线索处理已办事项
	 * by zw
	 */
	public function finishtask_list(){
		$p  = I('page', 1);//当前第几页
    	$pp = 20;//每页显示多少记录

    	$fname 	= I('fname');//广告名称
		$fnumber = I('fnumber');//文号
		$ffromtype 	= I('ffromtype');//来源分类
		$fmedianame = I('fmedianame');//发布媒体
		$djrqst = I('djrqst');//交办开始日期
		$djrqed = I('djrqed');//交办结束日期
		$sfbj 	= I('sfbj');//是否办结,0全部，10已办结，20未办结
		$rwzt 	= I('rwzt');//任务状态，0全部，10未定，20一般，30紧急

		$where_fr['_string'] = '1=1';

		if(!empty($rwzt)){
			if($rwzt == 10){
				$where_fr['_string'] .= ' and a.fid not in (select ftk_tregisterid from nj_tregistertrack,nj_tregister where nj_tregistertrack.ftk_tregisterid=nj_tregister.fid and (ftk_trepid='.session('regulatorpersonInfo.fregulatorpid').' or ftk_eqtrepid='.session('regulatorpersonInfo.fregulatorpid').') group by ftk_tregisterid)';
			}elseif($rwzt == 20){
				$where_fr['_string'] .= ' and a.fid in (select ftk_tregisterid from nj_tregistertrack,nj_tregister where nj_tregistertrack.ftk_tregisterid=nj_tregister.fid and (ftk_trepid='.session('regulatorpersonInfo.fregulatorpid').' or ftk_eqtrepid='.session('regulatorpersonInfo.fregulatorpid').') and TIMESTAMPDIFF(day,curdate(),ftk_endtime)>3 group by ftk_tregisterid)';
			}elseif($rwzt == 30){
				$where_fr['_string'] .= ' and a.fid in (select ftk_tregisterid from nj_tregistertrack,nj_tregister where nj_tregistertrack.ftk_tregisterid=nj_tregister.fid and (ftk_trepid='.session('regulatorpersonInfo.fregulatorpid').' or ftk_eqtrepid='.session('regulatorpersonInfo.fregulatorpid').') and TIMESTAMPDIFF(day,curdate(),ftk_endtime)<=3 group by ftk_tregisterid)';
			}
		}

		if(!empty($fname)){
			$where_fr['a.fname'] = array('like','%'.$fname.'%');
		}
		if(!empty($fnumber)){
			$where_fr['a.fnumber'] = array('like','%'.$fnumber.'%');
		}
		if(!empty($ffromtype)){
			$where_fr['a.ffromtype'] = $ffromtype;
		}
		if(!empty($fmedianame)){
			$where_fr['tm.fmedianame'] = array('like','%'.$fmedianame.'%');
		}
		if(!empty($djrqst)&&!empty($djrqed)){
			$where_fr['a.fgivetime'] = array('between',array($djrqst,$djrqed));
		}
		if(!empty($sfbj)){
			if($sfbj == 10){
				$where_fr['a.fstate'] = 40;
			}elseif($sfbj==20){
				$where_fr['a.fstate'] = array('neq',40);
			}
		}

		$where_fr['_string'] .= ' and exists(select a.fid from nj_tregisterflow where a.fid=nj_tregisterflow.fregisterid and fstate not in(0,13) and fsendstatus=10 and fcreateregualtorid='.session('regulatorpersonInfo.fregulatorid').') ';

		$count = M('nj_tregister')
			->alias('a')
			->join('tmedia tm on a.fmediaid=tm.fid')
			->join('(select * from nj_tregisterflow where flowid in (select max(flowid) as wid from nj_tregisterflow group by fregisterid)) b on a.fid=b.fregisterid')
			->join('(select fregisterid,max(ffinishtime) as ffinishtime from nj_tregisterflow where fsendstatus=10 and fcreateregualtorid='.session('regulatorpersonInfo.fregulatorid').' group by fregisterid) c on a.fid=c.fregisterid')
			->where($where_fr)
			->count();

		
		$do_fr = M('nj_tregister')
			->field('a.*,b.fflowname,tm.fmedianame,c.ffinishtime')
			->alias('a')
			->join('tmedia tm on a.fmediaid=tm.fid')
			->join('(select * from nj_tregisterflow where flowid in (select max(flowid) as wid from nj_tregisterflow group by fregisterid)) b on a.fid=b.fregisterid')
			->join('(select fregisterid,max(ffinishtime) as ffinishtime from nj_tregisterflow where fsendstatus=10 and fcreateregualtorid='.session('regulatorpersonInfo.fregulatorid').' group by fregisterid) c on a.fid=c.fregisterid')
			->where($where_fr)
			->order('a.fid desc')
			->page($p,$pp)
			->select();

		foreach ($do_fr as $key => $value) {
			$do_fr[$key]['diffdays'] = intval((strtotime($value['fendtimes']) - strtotime(date('Y-m-d')))/3600/24);
			if($value['fstate']!=40){
				$where_ftk['ftk_tregisterid'] = $value['fid'];
				$where_ftk['ftk_eqtrepid|ftk_trepid'] = session('regulatorpersonInfo.fregulatorpid');
				$do_ftk = M('nj_tregistertrack')
					->field('ftk_type,ftk_endtime,(case when TIMESTAMPDIFF(day,curdate(),ftk_endtime)<=3 then 10 else 0 end) as hotstatus')
					->where($where_ftk)
					->order('ftk_endtime asc')
					->select();
				$do_fr[$key]['gz_data'] = $do_ftk?$do_ftk:[];
			}else{
				$do_fr[$key]['gz_data'] = [];
			}
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_fr)));
	}

	/**
	 *获取待跟踪列表
	 * by zw
	*/
	public function waittrack_list(){
		$p  = I('page', 1);//当前第几页
    	$pp = 20;//每页显示多少记录

    	$fname 	= I('fname');//广告名称
		$fnumber = I('fnumber');//文号
		$ffromtype 	= I('ffromtype');//来源分类
		$fmedianame = I('fmedianame');//发布媒体
		$djrqst = I('djrqst');//交办开始日期
		$djrqed = I('djrqed');//交办结束日期
		$sfbj 	= I('sfbj');//是否办结,0全部，10已办结，20未办结
		$rwzt 	= I('rwzt');//任务状态，0全部，10未定，20一般，30紧急

    	$where_fr['_string'] = '1=1';

		if(!empty($rwzt)){
			if($rwzt == 10){
				$where_fr['_string'] .= ' and a.fid not in (select ftk_tregisterid from nj_tregistertrack,nj_tregister where nj_tregistertrack.ftk_tregisterid=nj_tregister.fid and (ftk_trepid='.session('regulatorpersonInfo.fregulatorpid').' or ftk_eqtrepid='.session('regulatorpersonInfo.fregulatorpid').') group by ftk_tregisterid)';
			}elseif($rwzt == 20){
				$where_fr['_string'] .= ' and a.fid in (select ftk_tregisterid from nj_tregistertrack,nj_tregister where nj_tregistertrack.ftk_tregisterid=nj_tregister.fid and (ftk_trepid='.session('regulatorpersonInfo.fregulatorpid').' or ftk_eqtrepid='.session('regulatorpersonInfo.fregulatorpid').') and TIMESTAMPDIFF(day,curdate(),ftk_endtime)>3 group by ftk_tregisterid)';
			}elseif($rwzt == 30){
				$where_fr['_string'] .= ' and a.fid in (select ftk_tregisterid from nj_tregistertrack,nj_tregister where nj_tregistertrack.ftk_tregisterid=nj_tregister.fid and (ftk_trepid='.session('regulatorpersonInfo.fregulatorpid').' or ftk_eqtrepid='.session('regulatorpersonInfo.fregulatorpid').') and TIMESTAMPDIFF(day,curdate(),ftk_endtime)<=3 group by ftk_tregisterid)';
			}
		}
		
		if(!empty($fname)){
			$where_fr['a.fname'] = array('like','%'.$fname.'%');
		}
		if(!empty($fnumber)){
			$where_fr['a.fnumber'] = array('like','%'.$fnumber.'%');
		}
		if(!empty($ffromtype)){
			$where_fr['a.ffromtype'] = $ffromtype;
		}
		if(!empty($fmedianame)){
			$where_fr['tm.fmedianame'] = array('like','%'.$fmedianame.'%');
		}
		if(!empty($djrqst)&&!empty($djrqed)){
			$where_fr['a.fgivetime'] = array('between',array($djrqst,$djrqed));
		}
		if(!empty($sfbj)){
			if($sfbj == 10){
				$where_fr['a.fstate'] = 40;
			}elseif($sfbj==20){
				$where_fr['a.fstate'] = array('neq',40);
			}
		}

		$where_fr['a.fstate'] = array('between',array(20,39));
		$count = M('nj_tregister')
			->alias('a')
			->join('tmedia tm on a.fmediaid=tm.fid')
			->join('(select fregisterid,fflowname from nj_tregisterflow where flowid in (select max(flowid) as wid from nj_tregisterflow group by fregisterid)) b on a.fid=b.fregisterid')
			->join('(select fregisterid from nj_tregisterflow where fchildstate=20 and fsendstatus=10 and fcreateregualtorpid='.session('regulatorpersonInfo.fregulatorpid').' group by fregisterid) c on a.fid=c.fregisterid')
			->where($where_fr)
			->count();

		
		$do_fr = M('nj_tregister')
			->field('a.*,b.fflowname,tm.fmedianame')
			->alias('a')
			->join('tmedia tm on a.fmediaid=tm.fid')
			->join('(select fregisterid,fflowname from nj_tregisterflow where flowid in (select max(flowid) as wid from nj_tregisterflow group by fregisterid)) b on a.fid=b.fregisterid')
			->join('(select fregisterid from nj_tregisterflow where fchildstate=20 and fsendstatus=10 and fcreateregualtorpid='.session('regulatorpersonInfo.fregulatorpid').' group by fregisterid) c on a.fid=c.fregisterid')
			->where($where_fr)
			->order('a.fid desc')
			->page($p,$pp)
			->select();

		foreach ($do_fr as $key => $value) {
			$do_fr[$key]['diffdays'] = intval((strtotime($value['fendtimes']) - strtotime(date('Y-m-d')))/3600/24);
			if($value['fstate']!=40){
				$where_ftk['ftk_tregisterid'] = $value['fid'];
				$where_ftk['ftk_eqtrepid|ftk_trepid'] = session('regulatorpersonInfo.fregulatorpid');
				$do_ftk = M('nj_tregistertrack')
					->field('ftk_type,ftk_endtime,(case when TIMESTAMPDIFF(day,curdate(),ftk_endtime)<=3 then 10 else 0 end) as hotstatus')
					->where($where_ftk)
					->order('ftk_endtime asc')
					->select();
				$do_fr[$key]['gz_data'] = $do_ftk?$do_ftk:[];
			}else{
				$do_fr[$key]['gz_data'] = [];
			}
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_fr)));
	}

	/**
	 *获取线索查询列表
	 * by zw
	*/
	public function register_list(){
		$p  = I('page', 1);//当前第几页
    	$pp = 20;//每页显示多少记录

		$fname 	= I('fname');//广告名称
		$fnumber = I('fnumber');//文号
		$ffromtype 	= I('ffromtype');//来源分类
		$fmedianame = I('fmedianame');//发布媒体
		$djrqst = I('djrqst');//交办开始日期
		$djrqed = I('djrqed');//交办结束日期
		$sfbj 	= I('sfbj');//是否办结,0全部，10已办结，20未办结
		$rwzt 	= I('rwzt');//任务状态，0全部，10未定，20一般，30紧急

    	$where_fr['_string'] = '1=1';

		if(!empty($rwzt)){
			if($rwzt == 10){
				$where_fr['_string'] .= ' and a.fid not in (select ftk_tregisterid from nj_tregistertrack,nj_tregister where nj_tregistertrack.ftk_tregisterid=nj_tregister.fid and (ftk_trepid='.session('regulatorpersonInfo.fregulatorpid').' or ftk_eqtrepid='.session('regulatorpersonInfo.fregulatorpid').') group by ftk_tregisterid)';
			}elseif($rwzt == 20){
				$where_fr['_string'] .= ' and a.fid in (select ftk_tregisterid from nj_tregistertrack,nj_tregister where nj_tregistertrack.ftk_tregisterid=nj_tregister.fid and (ftk_trepid='.session('regulatorpersonInfo.fregulatorpid').' or ftk_eqtrepid='.session('regulatorpersonInfo.fregulatorpid').') and TIMESTAMPDIFF(day,curdate(),ftk_endtime)>3 group by ftk_tregisterid)';
			}elseif($rwzt == 30){
				$where_fr['_string'] .= ' and a.fid in (select ftk_tregisterid from nj_tregistertrack,nj_tregister where nj_tregistertrack.ftk_tregisterid=nj_tregister.fid and (ftk_trepid='.session('regulatorpersonInfo.fregulatorpid').' or ftk_eqtrepid='.session('regulatorpersonInfo.fregulatorpid').') and TIMESTAMPDIFF(day,curdate(),ftk_endtime)<=3 group by ftk_tregisterid)';
			}
		}
		
		if(!empty($fname)){
			$where_fr['a.fname'] = array('like','%'.$fname.'%');
		}
		if(!empty($fnumber)){
			$where_fr['a.fnumber'] = array('like','%'.$fnumber.'%');
		}
		if(!empty($ffromtype)){
			$where_fr['a.ffromtype'] = $ffromtype;
		}
		if(!empty($fmedianame)){
			$where_fr['tm.fmedianame'] = array('like','%'.$fmedianame.'%');
		}
		if(!empty($djrqst)&&!empty($djrqed)){
			$where_fr['a.fgivetime'] = array('between',array($djrqst,$djrqed));
		}
		if(!empty($sfbj)){
			if($sfbj == 10){
				$where_fr['a.fstate'] = 40;
			}elseif($sfbj==20){
				$where_fr['a.fstate'] = array('neq',40);
			}
		}
		$where_fr['a.fregisterregulatorpid'] = session('regulatorpersonInfo.fregulatorpid');
		$count = M('nj_tregister')
			->alias('a')
			->join('tmedia tm on a.fmediaid=tm.fid')
			->join('(select * from nj_tregisterflow where flowid in (select max(flowid) as wid from nj_tregisterflow group by fregisterid)) b on a.fid=b.fregisterid')
			->where($where_fr)
			->count();

		
		$do_fr = M('nj_tregister')
			->field('a.*,b.fflowname,tm.fmedianame')
			->alias('a')
			->join('tmedia tm on a.fmediaid=tm.fid')
			->join('(select * from nj_tregisterflow where flowid in (select max(flowid) as wid from nj_tregisterflow group by fregisterid)) b on a.fid=b.fregisterid')
			->where($where_fr)
			->order('a.fid desc')
			->page($p,$pp)
			->select();

		foreach ($do_fr as $key => $value) {
			$do_fr[$key]['diffdays'] = intval((strtotime($value['fendtimes']) - strtotime(date('Y-m-d')))/3600/24);
			if($value['fstate']!=40){
				$where_ftk['ftk_tregisterid'] = $value['fid'];
				$where_ftk['ftk_eqtrepid|ftk_trepid'] = session('regulatorpersonInfo.fregulatorpid');
				$do_ftk = M('nj_tregistertrack')
					->field('ftk_type,ftk_endtime,(case when TIMESTAMPDIFF(day,curdate(),ftk_endtime)<=3 then 10 else 0 end) as hotstatus')
					->where($where_ftk)
					->order('ftk_endtime asc')
					->select();
				$do_fr[$key]['gz_data'] = $do_ftk?$do_ftk:[];
			}else{
				$do_fr[$key]['gz_data'] = [];
			}
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_fr)));
	}

	/**
	 *案件作废
	 * by zw
	 */
	public function register_del(){
		$system_num = getconfig('system_num');
		if(I('fid')){
			$fid = I('fid');//线索登记id
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}
		$result = I('result');//处理建议
		$where_fr['fid'] = $fid;
		$where_fr['fstate'] = 30;
		$where_fr['fregisterregulatorpid'] = session('regulatorpersonInfo.fregulatorpid');
		$do_fr = M('nj_tregister')->where($where_fr)->find();
		if(!empty($do_fr)){
			$res['fstate'] = 5;//线索作废
			$where_fr2['fid'] = $fid;
			$where_fr2['fstate'] = 30;
			$do_fr2 = M("nj_tregister")->where($where_fr2)->save($res);

			if(!empty($do_fr2)){
				/*保存当前流程记录*/
				$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
				$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
				$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
				$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
				$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
				$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
				$sh_list['freason'] 					= '线索作废';
				$sh_list['fcreateinfo'] 				= $result;
				$sh_list['fnextflowid'] 				= 0;
				$sh_list['fsendstatus'] 				= 10;
				$sh_list['feffectivestatus'] 			= 10;
				$sh_list['ffinishtime'] 				= date('Y-m-d H:i:s');
				$where_frl['fregisterid'] = $fid;
				$where_frl['fnextflowid'] = 0;
				$where_frl['fstate'] = 30;
				$where_frl['fcreateregualtorpid'] = 0;
				M('nj_tregisterflow')
					->where($where_frl)
					->save($sh_list);
				$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'案件不存在或已在处理中'));
			}
			
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'案件不存在或已在处理中'));
		}
	}

	/**
	 *保存或提交线索处理操作
	 * by zw
	 */
	public function update_register3()
	{
		$system_num = getconfig('system_num');
		if(I('fid')){
			$fid = I('fid');//线索登记id
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}
		$submit 			= I('submit')?I('submit'):2;//1保存，2提交
		$fwaittreid 		= I('fwaittreid');//待处理部门

		$where_fr['a.fid'] = $fid;
		$where_fr['b.fsendstatus'] = 0;
		$where_fr['_string'] = '(b.fcreateregualtorid ='.session('regulatorpersonInfo.fregulatorid').' or b.fcreateregualtorpid ='.session('regulatorpersonInfo.fregulatorpid').')';
		$do_fr = M('nj_tregister')
			->field('a.fid,a.fstate,b.flowid,a.ffillegaladid,a.fregisterpersonid,a.fregisterpersonname,a.fregisterregulatorpid,a.fregisterregulatorpname,a.fregisterregulatorid,a.fregisterregulatorname')
			->alias('a')
			->join('nj_tregisterflow b on a.fid=b.fregisterid and a.fstate=b.fstate')
			->where($where_fr)
			->find();

		$fregisterid = $do_fr['fid'];
		$flowid = $do_fr['flowid'];
		if(empty($do_fr)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'无权限操作'));
		}else{

			if($do_fr['fstate'] == 30 || $do_fr['fstate'] == 33 || $do_fr['fstate'] == 34){
			//-------------------线索待处理状态处理、下级处理退回、审核不通过重新处理------------------
				$undertake 	= I('undertake');//承办状态,10自办、20转办、30退回
				$result 	= I('result');//处理建议
				if($submit==2){//提交
					if($undertake == 10){//自办
						$fpunish = I('fpunish');//处罚方式
						if(empty($fpunish)){
							$this->ajaxReturn(array('code'=>1,'msg'=>'请选择处罚方式'));
						}
						$res['fpunish'] = $fpunish;
						$res['fdealregulatorpid'] 	= session('regulatorpersonInfo.fregulatorpid');
						$res['fdealregulatorpname'] = session('regulatorpersonInfo.regulatorpname');
						$res['fdealtime'] 			= date('Y-m-d H:i:s');
						$res['fstate'] 	= 39;
						$where_fr2['fid'] = $fregisterid;
						M("nj_tregister")->where($where_fr2)->save($res);//保存登记信息

						/*新增下一流程记录*/
						$next_list['fregisterid'] 				= $fregisterid;
						$next_list['fcreateregualtorpid'] 		= $do_fr['fregisterregulatorpid'];
						$next_list['fcreateregualtorpname'] 	= $do_fr['fregisterregulatorpname'];
						$next_list['fflowname'] 				= '办结审核';
						$next_list['fstate'] 					= 39;
						$next_list['fsendstatus'] 				= 0;
						$next_list['fcreatetime'] 				= date('Y-m-d H:i:s');
						$nextflowid = M("nj_tregisterflow")->add($next_list);

						/*保存当前流程记录*/
						$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
						$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
						$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
						$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
						$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
						$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
						$sh_list['fcreateinfo'] 	= $result;
						$sh_list['freason'] 		= '线索自办';
						$sh_list['fnextflowid'] 	= $nextflowid;
						$sh_list['fsendstatus'] 	= 10;
						$sh_list['feffectivestatus'] 	= 10;
						$sh_list['ffinishtime'] 	= date('Y-m-d H:i:s');
						M('nj_tregisterflow')
							->where('flowid='.$flowid)
							->save($sh_list);

						//更改主线状态
						$data_fwzx['feffectivestatus'] 	= 0;
						$where_fwzx['fregisterid'] 		= $fregisterid;
						$where_fwzx['feffectivestatus'] = 10;
						$where_fwzx['fsendstatus'] 		= 10;
						$where_fwzx['fcreateregualtorpid'] = session('regulatorpersonInfo.fregulatorpid');
						$where_fwzx['flowid'] 			= array('neq',$flowid);
						$where_fwzx['fstate']			= array('in',array(30,33,34));
						M('nj_tregisterflow')
							->where($where_fwzx)
							->save($data_fwzx);

					}elseif ($undertake == 20) {//转办下级
						$res['fstate'] 	= 30;
						$where_fr2['fid'] = $fregisterid;
						M("nj_tregister")->where($where_fr2)->save($res);//保存登记信息

						/*新增下一流程记录*/
						$next_list['fregisterid'] 				= $fregisterid;
						$next_list['fcreateregualtorpid'] 		= $fwaittreid;
						$next_list['fflowname'] 				= '线索转办处理';
						$next_list['fstate'] 					= 30;
						$next_list['fsendstatus'] 				= 0;
						$next_list['fcreatetime'] 				= date('Y-m-d H:i:s');
						$nextflowid = M("nj_tregisterflow")->add($next_list);

						/*保存当前流程记录*/
						$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
						$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
						$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
						$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
						$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
						$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
						$sh_list['fcreateinfo'] 				= $result;
						$sh_list['freason'] 					= '线索转办';
						$sh_list['fnextflowid'] 				= $nextflowid;
						$sh_list['fchildstate'] 				= $undertake;
						$sh_list['fsendstatus'] 				= 10;
						$sh_list['feffectivestatus'] 			= 10;
						$sh_list['ffinishtime'] 				= date('Y-m-d H:i:s');
						M('nj_tregisterflow')
							->where('flowid='.$flowid)
							->save($sh_list);

						//更改主线状态
						$data_fwzx['feffectivestatus'] 	= 0;
						$where_fwzx['fregisterid'] 		= $fregisterid;
						$where_fwzx['feffectivestatus'] = 10;
						$where_fwzx['fsendstatus'] 		= 10;
						$where_fwzx['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');
						$where_fwzx['flowid'] 			= array('neq',$flowid);
						$where_fwzx['fstate']			= array('in',array(30,33,34));
						M('nj_tregisterflow')
							->where($where_fwzx)
							->save($data_fwzx);
						
					}elseif ($undertake == 30) {//退回上级
						$where_fwzx['fregisterid'] = $fregisterid;
						$where_fwzx['feffectivestatus'] = 10;
						$where_fwzx['fsendstatus'] 		= 10;
						$where_fwzx['fstate']			= array('in',array(30,33,34));
						$do_fwzx = M('nj_tregisterflow')
							->where($where_fwzx)
							->order('flowid desc')
							->find();

						if(!empty($do_fwzx)){//如果为上级转办下来，则退回承办部门
							$res['fstate'] 	= 33;
							$where_fr2['fid'] = $fregisterid;
							M("nj_tregister")->where($where_fr2)->save($res);//保存登记信息

							/*新增下一流程记录*/
							$next_list['fregisterid'] 				= $fregisterid;
							$next_list['fcreateregualtorid'] 		= $do_fwzx['fcreateregualtorid'];
							$next_list['fflowname'] 				= '转办退回处理';
							$next_list['fstate'] 					= 33;
							$next_list['fsendstatus'] 				= 0;
							$next_list['fcreatetime'] 				= date('Y-m-d H:i:s');
							$nextflowid = M("nj_tregisterflow")->add($next_list);

							/*保存当前流程记录*/
							$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
							$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
							$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
							$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
							$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
							$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
							$sh_list['fcreateinfo'] 				= $result;
							$sh_list['freason'] 					= '转办线索退回';
							$sh_list['fnextflowid'] 				= $nextflowid;
							$sh_list['fchildstate'] 				= $undertake;
							$sh_list['fsendstatus'] 				= 10;
							$sh_list['ffinishtime'] 				= date('Y-m-d H:i:s');
							M('nj_tregisterflow')
								->where('flowid='.$flowid)
								->save($sh_list);

							M()->execute('update nj_tregisterflow set feffectivestatus=0 where flowid='.$do_fwzx['flowid']);//因转办被退回，被取消主线状态
						}else{//如果为登记用户分派下来
							$where_fwzx2['fregisterid'] 		= $fregisterid;
							$where_fwzx2['feffectivestatus'] 	= 10;
							$where_fwzx2['fsendstatus'] 		= 10;
							$where_fwzx2['fstate']				= array('in',array(0,13));
							$do_fwzx2 = M('nj_tregisterflow')
								->where($where_fwzx2)
								->order('flowid desc')
								->find();

							if(!empty($do_fwzx2)){
								$res['fstate'] 	= 13;
								$where_fr2['fid'] = $fregisterid;
								M("nj_tregister")->where($where_fr2)->save($res);//保存登记信息

								/*新增下一流程记录*/
								$next_list['fregisterid'] 				= $fregisterid;
								$next_list['fcreateregualtorid'] 		= $do_fwzx2['fcreateregualtorid'];
								$next_list['fflowname'] 				= '线索退回处理';
								$next_list['fstate'] 					= 13;
								$next_list['fsendstatus'] 				= 0;
								$next_list['fcreatetime'] 				= date('Y-m-d H:i:s');
								$nextflowid = M("nj_tregisterflow")->add($next_list);

								/*保存当前流程记录*/
								$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
								$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
								$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
								$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
								$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
								$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
								$sh_list['fcreateinfo'] 				= $result;
								$sh_list['freason'] 					= '线索处理退回';
								$sh_list['fnextflowid'] 				= $nextflowid;
								$sh_list['fchildstate'] 				= $undertake;
								$sh_list['fsendstatus'] 				= 10;
								$sh_list['ffinishtime'] 				= date('Y-m-d H:i:s');
								M('nj_tregisterflow')
									->where('flowid='.$flowid)
									->save($sh_list);

								M()->execute('update nj_tregisterflow set feffectivestatus=0 where flowid='.$do_fwzx2['flowid']);//因转办被退回，被取消主线状态
							}else{
								$this->ajaxReturn(array('code'=>0,'msg'=>'审核失败，不允许退回，请重新办理'));
							}
						}

					}
				}else{
					/*保存当前流程记录*/
					$sh_list['fcreateinfo'] 	= $result;
					$sh_list['fchildstate'] 	= $undertake;
					M('nj_tregisterflow')
						->where('flowid='.$flowid)
						->save($sh_list);
				}

				$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
			}elseif($do_fr['fstate'] == 39){
				//-------------------线索自办审核------------------
				$undertake 	= I('undertake');//审核状态,10通过，20不通过
				$result 	= I('result');//处理建议

				if($undertake == 10){
					$res['fstate'] 	= 40;
					$where_fr2['fid'] = $fregisterid;
					M("nj_tregister")->where($where_fr2)->save($res);//保存登记信息

					/*新增下一流程记录*/
					$next_list['fregisterid'] 				= $fregisterid;
					$next_list['fcreateregualtorpid'] 		= $do_fr['fregisterregulatorpid'];
					$next_list['fcreateregualtorpname'] 	= $do_fr['fregisterregulatorpname'];
					$next_list['fcreateregualtorid'] 		= $do_fr['fregisterregulatorid'];
					$next_list['fcreateregualtorname'] 		= $do_fr['fregisterregulatorname'];
					$next_list['fcreateregualtorpersonid'] 	= $do_fr['fregisterpersonid'];
					$next_list['fcreateregualtorpersonname'] = $do_fr['fregisterpersonname'];
					$next_list['freason'] 					= '已完成办结';
					$next_list['fflowname'] 				= '线索办结';
					$next_list['fstate'] 					= 40;
					$next_list['fsendstatus'] 				= 10;
					$next_list['feffectivestatus'] 			= 10;
					$next_list['fcreatetime'] 				= date('Y-m-d H:i:s');
					$next_list['ffinishtime'] 				= date('Y-m-d H:i:s');
					$nextflowid = M("nj_tregisterflow")->add($next_list);

					/*保存当前流程记录*/
					$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
					$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
					$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
					$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
					$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
					$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
					$sh_list['fcreateinfo'] 	= $result;
					$sh_list['freason'] 		= '同意办结';
					$sh_list['fnextflowid'] 	= $nextflowid;
					$sh_list['fsendstatus'] 	= 10;
					$sh_list['feffectivestatus'] 	= 10;
					$sh_list['ffinishtime'] 	= date('Y-m-d H:i:s');
					M('nj_tregisterflow')
						->where('flowid='.$flowid)
						->save($sh_list);

					//更改主线状态
					$data_fwzx['feffectivestatus'] 	= 0;
					$where_fwzx['fregisterid'] 		= $fregisterid;
					$where_fwzx['feffectivestatus'] = 10;
					$where_fwzx['fsendstatus'] 		= 10;
					$where_fwzx['fcreateregualtorpid'] = session('regulatorpersonInfo.fregulatorpid');
					$where_fwzx['flowid'] 			= array('neq',$flowid);
					$where_fwzx['fstate']			= array('in',array(39));
					M('nj_tregisterflow')
						->where($where_fwzx)
						->save($data_fwzx);

					//更改违法广告的处理状态
					if(!empty($do_fr['ffillegaladid'])){
						$data_tid['fview_status'] 	= 10;
						$data_tid['fstatus'] 		= 10;
						$where_tid['fid'] 			= array('in',implode(',', $do_fr['ffillegaladid']));
						$where_tid['fstatus'] 		= 0;
						$where_tid['fcustomer'] 	= $system_num;
						M('tbn_illegal_ad')
							->where($where_tid)
							->save($data_tid);
					}

				}else{
					$res['fstate'] 	= 34;
					$where_fr2['fid'] = $fregisterid;
					M("nj_tregister")->where($where_fr2)->save($res);//保存登记信息

					$where_fwzx['fregisterid'] = $fregisterid;
					$where_fwzx['feffectivestatus'] = 10;
					$where_fwzx['fsendstatus'] 		= 10;
					$where_fwzx['fstate']			= array('in',array(30,33,34));
					$do_fwzx = M('nj_tregisterflow')
						->where($where_fwzx)
						->order('flowid desc')
						->find();

					/*新增下一流程记录*/
					$next_list['fregisterid'] 				= $fregisterid;
					$next_list['fcreateregualtorid'] 		= $do_fwzx['fcreateregualtorid'];
					$next_list['fflowname'] 				= '重新处理';
					$next_list['fstate'] 					= 34;
					$next_list['fsendstatus'] 				= 0;
					$next_list['fcreatetime'] 				= date('Y-m-d H:i:s');
					$nextflowid = M("nj_tregisterflow")->add($next_list);

					/*保存当前流程记录*/
					$sh_list['fcreateregualtorpid'] 		= session('regulatorpersonInfo.fregulatorpid');
					$sh_list['fcreateregualtorpname'] 		= session('regulatorpersonInfo.regulatorpname');
					$sh_list['fcreateregualtorid'] 			= session('regulatorpersonInfo.fregulatorid');
					$sh_list['fcreateregualtorname'] 		= session('regulatorpersonInfo.regulatorname');
					$sh_list['fcreateregualtorpersonid'] 	= session('regulatorpersonInfo.fid');
					$sh_list['fcreateregualtorpersonname'] 	= session('regulatorpersonInfo.fname');
					$sh_list['fcreateinfo'] 				= $result;
					$sh_list['freason'] 					= '办结退回';
					$sh_list['fnextflowid'] 				= $nextflowid;
					$sh_list['fsendstatus'] 				= 10;
					$sh_list['ffinishtime'] 				= date('Y-m-d H:i:s');
					M('nj_tregisterflow')
						->where('flowid='.$flowid)
						->save($sh_list);

					M()->execute('update nj_tregisterflow set feffectivestatus=0 where flowid='.$do_fwzx['flowid']);//因转办被退回，被取消主线状态
				}
				$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
			}

		}

	}

	/**
	 *线索跟踪操作
	 * by zw
	*/
	public function tregistertrack(){
		$fid = I('fid');//登记ID
		$ftk_type = I('ftk_type');//跟踪类型
		$ftk_endtime = I('ftk_endtime');//跟踪截止时间
		$eqtrepid = I('eqtrepid');//跟踪机构

		$where_ftr['fid'] = $fid;
		$do_ftr = M('nj_tregister')->where($where_ftr)->find();
		if(empty($do_ftr)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'无权操作'));
		}

		$do_fte = M('nj_tregisterfile')->field('fnumber')->where('fregisterid='.$fid.' and ftype=20')->order('fid desc')->find();

		$data_ftk['ftk_tregisterid'] = $fid;
		$data_ftk['ftk_type'] = $ftk_type;
		$data_ftk['ftk_endtime'] = $ftk_endtime;
		$data_ftk['ftk_createtime'] = date('Y-m-d H:i:s');
		$data_ftk['ftk_personid'] = session('regulatorpersonInfo.fid');
		$data_ftk['ftk_trepid'] = session('regulatorpersonInfo.fregulatorpid');
		$data_ftk['ftk_eqtrepid'] = I('eqtrepid');
		$data_ftk['ftk_number'] = $do_fte['fnumber'];
		$do_ftk = M('nj_tregistertrack')->add($data_ftk);
		if(!empty($do_ftk)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'已实行跟踪'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'跟踪失败，请查看线索是否办结'));
		}
	}

	/**
	 *获取需要跟踪的机构
	 * by zw
	*/
	public function get_trackuser(){
		$fid = I('fid');//登记ID

		$where_ftw['a.fregisterid'] = $fid;
		$where_ftw['a.fsendstatus'] = 10;
		$where_ftw['a.fcreateregualtorpid'] = session('regulatorpersonInfo.fregulatorpid');
		$where_ftw['a.fchildstate'] = 20;
		$do_ftw = M('nj_tregisterflow')
			->field('b.fcreateregualtorpid')
			->alias('a')
			->join('nj_tregisterflow b on a.fnextflowid=b.flowid ')
			->where($where_ftw)
			->find();
		if(!empty($do_ftw)){
			$do_tr = M('tregulator')->field('fid as eqtrepid,ffullname')->where('fid='.$do_ftw['fcreateregualtorpid'])->find();
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_tr));

	}

	/**
	 *获取办事的用户
	 * by zw
	*/
	public function get_czjurisdiction(){
		$fstate = I('fstate')?I('fstate'):0;//获取当前状态
		$trepid = I('trepid')?I('trepid'):session('regulatorpersonInfo.fregulatorpid');//机构ID

		$where_tn['_string'] = '(b.fid='.$trepid.' or b.fpid='.$trepid.')';
		$do_tn = M('tregulatorperson')
			->alias('a')
			->field('a.fname,a.fid')
			->join('tregulator b on a.fregulatorid=b.fid')
			->where($where_tn)
			->select();

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>count($do_tn),'list'=>$do_tn)));
	}

	/**
	 *获取登记表信息
	 * by zw
	*/
	public function get_tregistertable(){
		$fid = I('fid');//登记表ID

		$where_ftr['fid'] = $fid;
		$where_ftr['fregisterregulatorpid'] = session('regulatorpersonInfo.fregulatorpid');
		$do_ftr = M('nj_tregister')
			->field('fname,fnumber,fregistertime,ffromtype,fpersontype,fpersonname,fpersoncard,fpersoncompany,fpersonmobile,fpersonpostcode,fpersonadress,remark')
			->where($where_ftr)
			->find();

		$where_ftw['fregisterid'] = $fid;
		$where_ftw['feffectivestatus'] = 10;
		$where_ftw['fsendstatus'] = 10;
		$do_ftw = M('nj_tregisterflow')
			->field('a_fstate,a_fcreateinfo,a_ffinishtime,a_fcreateregualtorpersonname,b_fstate,b_fcreateinfo,b_ffinishtime,b_fcreateregualtorpersonname')
			->alias('a')
			->join('nj_tregisterflow b on a.fnextflowid=b.flowid')
			->where($where_ftw)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data1'=>$do_ftr,'data2'=>$do_ftw));
	}

	/**
	 *获取流程列表
	 * by zw
	*/
	public function get_flowlist(){
		$fid = I('fid');//登记表ID

		$where_ftw2['a.fregisterid'] = $fid;
		$where_ftw2['a.fsendstatus'] = 0;
		$do_ftw2 = [];
		$do_ftw2 = M('nj_tregisterflow')
			->field('flowid,fregisterid,fnextflowid,n.fcreateregualtorpid,n.fcreateregualtorpname,n.fcreateregualtorid,n.fcreateregualtorname,0 as fcreateregualtorpersonid,"" as fcreateregualtorpersonname,fcreatetime,ffinishtime,fcreateinfo, "待处理" as freason,fflowname,fstate,fchildstate,fsendstatus,feffectivestatus')
			->alias('a')
			->join('inner join (select a.fid as fcreateregualtorid,a.ffullname as fcreateregualtorname,(case when a.fkind=2 then b.fid else a.fid end) as fcreateregualtorpid,(case when a.fkind=2 then b.ffullname else a.ffullname end) as fcreateregualtorpname from tregulator a left join tregulator b on a.fpid=b.fid) as n on a.fcreateregualtorid=n.fcreateregualtorid ')
			->where($where_ftw2)
			->order('flowid desc')
			->select();
		if(empty($do_ftw2)){
			$do_ftw2 = M('nj_tregisterflow')
				->field('flowid,fregisterid,fnextflowid,n.fcreateregualtorpid,n.fcreateregualtorpname,n.fcreateregualtorid,"" as fcreateregualtorname,0 as fcreateregualtorpersonid,"" as fcreateregualtorpersonname,fcreatetime,ffinishtime,fcreateinfo, "待处理" as freason,fflowname,fstate,fchildstate,fsendstatus,feffectivestatus')
				->alias('a')
				->join('inner join (select a.fid as fcreateregualtorid,a.ffullname as fcreateregualtorname,(case when a.fkind=2 then b.fid else a.fid end) as fcreateregualtorpid,(case when a.fkind=2 then b.ffullname else a.ffullname end) as fcreateregualtorpname from tregulator a left join tregulator b on a.fpid=b.fid) as n on a.fcreateregualtorpid=n.fcreateregualtorid ')
				->where($where_ftw2)
				->order('flowid desc')
				->select();
		}

		$where_ftw['a.fregisterid'] = $fid;
		$where_ftw['a.fsendstatus'] = 10;
		$do_ftw = [];
		$do_ftw = M('nj_tregisterflow')
			->field('a.*')
			->alias('a')
			->where($where_ftw)
			->order('flowid desc')
			->select();

		$do_ftw = array_merge($do_ftw2,$do_ftw);
		if(!empty($do_ftw)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_ftw));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败'));
		}
	}

	/**
	 *获取流程附件
	 * by zw
	*/
	public function get_flowfile(){
		$fid = I('fid');//登记表ID
		$flowid = I('flowid');//流程ID
		$ts = I('ts')?I('ts'):0;//获取方式，0按流程及类别区分，10整合所有流程普通附件，20获取所有流程文书，40获取主流程所有附件
		$data = [];
		$where_flow = ' and 1=1';
		if($ts == 10){
			$where_fte['a.ftype'] = 0;
		}elseif($ts == 20){
			$where_fte['a.ftype'] = 10;
		}elseif($ts == 40){
			$where_flow = ' and feffectivestatus=10';
		}

		$where_fte['a.fregisterid'] = $fid;
		$where_fte['_string'] = '(b.fcreateregualtorid='.session('regulatorpersonInfo.fregulatorid').' or b.fcreateregualtorpid='.session('regulatorpersonInfo.fregulatorpid').' or b.flowid in (select a.flowid from nj_tregisterflow a inner join nj_tregisterflow b on a.fnextflowid=b.flowid where b.fcreateregualtorpid='.session('regulatorpersonInfo.fregulatorpid').' or b.fcreateregualtorid='.session('regulatorpersonInfo.fregulatorid').'))';
		$do_fte = M('nj_tregisterfile')
			->field('a.fid,a.fattachname,a.fattachurl,a.ffilename,a.ffiletype,a.ftype,b.fflowname,(case when b.fsendstatus=0 then 20 else 10 end) as delstate')
			->alias('a')
			->join('nj_tregisterflow b on a.fillegaladflowid=b.flowid')
			->where($where_fte)
			->order('a.fid desc')
			->select();
			// var_dump($do_fte);die;

		$data['files1']=[];
		$data['files2']=[];
		foreach ($do_fte as $key => $value) {
			if($value['ftype'] == 0){
				$data['files1'][] = $value;
			}elseif($value['ftype'] == 10){
				$data['files2'][] = $value;
			}
		}

		$where_ftk['a.fregisterid'] = $fid;
		$where_ftk['a.ftype'] = 20;
		$where_ftk['ftk_trepid|ftk_eqtrepid'] = session('regulatorpersonInfo.fregulatorpid');
		$do_ftk = M('nj_tregisterfile')
			->field('a.fid,a.fattachname,a.fattachurl,a.ffilename,a.ffiletype,a.ftype,"线索跟踪" as fflowname,(case when ftk_personid = '.session('regulatorpersonInfo.fid').' then 20 else 10 end) as delstate')
			->alias('a')
			->join('nj_tregistertrack b on b.ftk_number=a.fnumber and a.fregisterid=b.ftk_tregisterid')
			->where($where_ftk)
			->order('a.fid desc')
			->select();
		if(!empty($do_ftk)){
			$data['files2'] = array_merge($data['files2'],$do_ftk);
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}

	/**
	 *获取待登记违法广告详情（单条/列表）
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function get_tbn_illegal_ad_details(){
		
		$fillegaladid 	= I('fillegaladid');//违法广告id
		if(empty($fillegaladid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}
		if(is_array($fillegaladid)){
			foreach ($fillegaladid as $key => $value) {
				$data[$key] = $this->get_tbn_illegal_adlist_details($value);
			}
		}else{
			$data[0] = $this->get_tbn_illegal_adlist_details($fillegaladid);
		}

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>count($data),'list'=>$data)));
	}

	/**
	 *获取待登记违法广告
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（data-违法广告信息，adclueInfo-样本信息，issue-广告发布信息）
	 * by zw
	 */
	public function get_tbn_illegal_adlist_details($fillegaladid)
	{
		$system_num = getconfig('system_num');
		$where['a.fid'] = $fillegaladid;
		$where['a.fcustomer'] = $system_num;

		$data = M('tbn_illegal_ad')
		->alias('a')
		->field('
			a.fid, a.fmedia_class, a.fsample_id, a.fmedia_id, a.create_time,
			(case when instr(d.fmedianame,"（") > 0 then left(d.fmedianame,instr(d.fmedianame,"（") -1) else d.fmedianame end) as fmedianame,
            e.ffullname as adclass_fullname,
            f.fcreditcode, f.fregaddr, f.flinkman, f.ftel, f.fname as mediaowner_name,a.fillegaltypecode
		')
		->join('tmedia d on  a.fmedia_id=d.fid and d.fid=d.main_media_id')//媒介信息
		->join('tadclass e on a.fad_class_code=e.fcode ')//广告内容列别
		->join('tmediaowner f on f.fid = d.fmediaownerid')
		->where($where)
		->find();
		if(!empty($data)){
			if($data['fmedia_class']==1){
				$data_se = M('ttvsample')
				->field('
					b.fadid, b.favifilepng, b.fadlen, b.fversion, b.fadmanuno, b.fmanuno, b.fadapprno, b.fapprno, b.fadent, b.fent, b.fentzone, b.fillegalcontent, b.fexpressioncodes, b.fexpressions, b.fconfirmations, b.fpunishments, b.favifilename,b.fissuedate,
            		c.fadname, c.fbrand
            	')
				->alias('b')
				->join('tad c on b.fadid = c.fadid and c.fadid<>0')
				->where('fid='.$data['fsample_id'])
				->find();
			}elseif($data['fmedia_class']==2){
				$data_se = M('tbcsample')
				->field('
					b.fadid, b.favifilepng, b.fadlen, b.fversion, b.fadmanuno, b.fmanuno, b.fadapprno, b.fapprno, b.fadent, b.fent, b.fentzone, b.fillegalcontent, b.fexpressioncodes, b.fexpressions, b.fconfirmations, b.fpunishments, b.favifilename,b.fissuedate,
            		c.fadname, c.fbrand
            	')
				->alias('b')
				->join('tad c on b.fadid = c.fadid and c.fadid<>0')
				->where('fid='.$data['fsample_id'])
				->find();
			}elseif($data['fmedia_class']==3){
				$data_se = M('tpapersample')
				->field('
					b.fadid, fjpgfilename as favifilepng,fjpgfilename, b.fadmanuno, b.fmanuno, b.fadapprno, b.fapprno, b.fadent, b.fent, b.fentzone, b.fillegalcontent, b.fexpressioncodes, b.fexpressions, b.fconfirmations, b.fpunishments,b.fissuedate,
	            	c.fadname, c.fbrand,
	            	y.fpage as fadlen
            	')
				->alias('b')
				->join('tad c on b.fadid = c.fadid and c.fadid<>0')
				->join('tpaperissue y on b.fpapersampleid=y.fpapersampleid')
				->where('b.fpapersampleid='.$data['fsample_id'])
				->find();
			}
			$data = array_merge($data,$data_se);
		}

		return $data;
	}

	/**
	 *获取登记任务详情
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	*/
	public function get_registerview(){
		$system_num = getconfig('system_num');
		if(I('fid')){
			$fid = I('fid');
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少参数'));
		}
		$where['a.fid'] = $fid;//登记表ID
		$data = M('nj_tregister')
			->field('a.*,b.flowid,b.fcreateregualtorpid,fsendstatus,b.fchildstate,b.fcreateinfo,tm.fmedianame,fadclass,(case when instr(fadclass,",")>0 then right(fadclass,(LENGTH(fadclass)-instr(fadclass,","))) else fadclass end) as fadclass2')
			->alias('a')
			->join('tmedia tm on a.fmediaid=tm.fid')
			->join('nj_tregisterflow b on a.fid=b.fregisterid')
			->where($where)
			->order('b.flowid desc')
			->find();

		if(!empty($data['fadclass2'])){
			$do_ts = M('tadclass')
				->field('ffullname')
				->where('fcode='.$data['fadclass2'])
				->find();
			$data['fadclassname'] = $do_ts['ffullname'];
		}
		if(!empty($data)){
			if(empty($data['fsendstatus'])){
				if($data['fcreateregualtorpersonid'] == session('regulatorpersonInfo.fid')){
					$data['refiles'] = M('nj_tregisterfile')->where('fillegaladflowid='.$data['flowid'])->order('fid asc')->select();
				}
			}
			
			$data['fillegaladid'] = M('tbn_illegal_ad')->field('fid')->where('fregisterid='.$fid.' and fcustomer="'.$system_num.'"')->order('fid desc')->select();
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('data'=>$data)));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据'));
		}

	}

	/**
	 *创建文书
	 * by zw
	*/
	public function create_document(){
		$fid = I('fid')?I('fid'):0;//登记id
		$flowid = I('flowid')?I('flowid'):0;//流转ID
		$ftype = I('ftype')?I('ftype'):10;//文书类型，10普通文书、20跟踪文书
		//添加文书
		$fddata['dt_tregisterid'] 			= $fid;//登记ID
		$fddata['dt_flowid'] 				= $flowid;//流转ID
		$fddata['dt_name'] 					= I('dt_doctitle');//文书标题
		$fddata['dt_number'] 				= I('dt_number');//文号
		$fddata['dt_receiveregulatorname'] 	= I('dt_receiveregulatorname');//收文单位名称
		$fddata['dt_content'] 				= $_POST['dt_content'];//正文内容
		$fddata['dt_sendtime'] 				= I('dt_sendtime');//发文时间
		$fddata['dt_sendregulatorname'] 	= I('dt_sendregulatorname');//发文单位名称
		$fddata['dt_createtime'] 			= date('Y-m-d H:i:s');//创建时间
		$fddata['dt_createpersonid'] 		= session('regulatorpersonInfo.fid');//创建人id
		$fddata['dt_showstate'] 			= I('dt_showstate');//可见性
		$fddo = M('document')->add($fddata);

		if(!empty($fddo)){
			$uploadurl = A('Npreviewword')->create_document_word2($fddo);//获取文书下载地址
			if(!empty($uploadurl)){
				//上传附件
				$attach_data['fregisterid'] 			= $fid;//线索登记id
				if($ftype != 20){
					$attach_data['fillegaladflowid'] 	= $flowid;//流程ID
				}
				$attach_data['fuploadtime'] 			= date('Y-m-d H:i:s');//上传时间
				$attach_data['fcreateregualtorid'] 		= session('regulatorpersonInfo.fregulatorpid');//上传部门id
				$attach_data['fcreateuserid'] 			= session('regulatorpersonInfo.fid');//上传用户ID
				$attach_data['fattachname'] 		= I('dt_doctitle');
				$attach_data['fattachurl'] 			= $uploadurl;
				$attach_data['ffilename'] 			= I('dt_doctitle');
				$attach_data['ffiletype'] 			= preg_replace('/.*\./','',$uploadurl);
				$attach_data['fnumber'] 			= $fddo;
				$attach_data['ftype'] 				= $ftype;
				$attachid = M('nj_tregisterfile')->add($attach_data);
				$attach_data['fid'] = $attachid;
			}
		}

		if(!empty($attachid)){
			D('Function')->write_log('线索文书',1,'添加成功','document',$fddo,M('document')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功','data'=>$attach_data));
		}else{
			D('Function')->write_log('线索文书',0,'添加失败','document',0,M('document')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'添加失败'));
		}
	}

	/**
	 *删除文书
	 * by zw
	*/
	public function delete_document(){
		$fid = I('fid');//附件ID
		$where_fte['fid'] = $fid;
		$where_fte['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorpid');
		$do_fte = M('nj_tregisterfile')->where($where_fte)->find();
		if(!empty($do_fte)){
			$dt_id = $do_fte['fnumber'];//文书ID
			$do_dt = M('document')->where('dt_createpersonid='.session('regulatorpersonInfo.fid').' and dt_id='.$dt_id)->find();
			if(!empty($do_dt)){
				$dnr_where['dr_name'] 			= $do_dt['dt_number'];
				$dnr_where['dr_state'] 			= 20;
				$dnr_where['dr_regulatorid'] 	= session('regulatorpersonInfo.fregulatorpid');
				$dnr_where['dr_usepersonid'] 	= session('regulatorpersonInfo.fid');
				$dnr_data['dr_state'] 		= 0;
				$dnr_data['dr_getpersonid'] = 0;
				$dnr_data['dr_gettime'] 	= array('exp','null');
				$dnr_data['dr_usepersonid'] = 0;
				$dnr_data['dr_usertime'] 	= array('exp','null');
				$dnr_do = M('document_number')->where($dnr_where)->save($dnr_data);
				if(!empty($dnr_do)){
					M('document')->where('dt_id='.$dt_id)->delete();
					M('nj_tregisterfile')->where('fid='.$fid)->delete();
					D('Function')->write_log('线索文书',1,'删除成功','document',$dt_id,M('document')->getlastsql());
					$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
				}else{
					D('Function')->write_log('线索文书',0,'删除失败，仅有上传人有权删除','document',0,M('document')->getlastsql());
					$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败，仅有上传人有权删除'));
				}
			}else{
				D('Function')->write_log('线索文书',0,'删除失败，仅有上传人有权删除','document',0,M('document')->getlastsql());
				$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败，仅有上传人有权删除'));
			}
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败'));
		}
	}

	/**
	 *创建附件
	 * by zw
	*/
	public function create_files(){
		$fid = I('fid');//登记id
		$flowid = I('flowid');//流程ID
		$attachinfo = I('attachinfo');//附件
		$attach = [];

		//如果上传附件
		if(!empty($attachinfo)&&!empty($fid)){
			$attach_data['fregisterid'] 			= $fid;//线索登记id
			$attach_data['fillegaladflowid'] 		= $flowid;//流程ID
			$attach_data['fuploadtime'] 			= date('Y-m-d H:i:s');//上传时间
			$attach_data['fcreateregualtorid'] 		= session('regulatorpersonInfo.fregulatorpid');//上传部门id
			$attach_data['fcreateuserid'] 			= session('regulatorpersonInfo.fid');//上传用户ID
			foreach ($attachinfo as $key => $value){
				$attach_data['fattachname'] 		= $value['fattachname'];
				$attach_data['fattachurl'] 			= $value['fattachurl'];
				$attach_data['ffilename'] 			= preg_replace('/\..*/','',$value['fattachurl']);
				$attach_data['ffiletype'] 			= preg_replace('/.*\./','',$value['fattachname']);
				$attach[$key] 						= $attach_data;
			}
			$attachid = M('nj_tregisterfile')->addAll($attach);
		}

		if(!empty($attachid)){
			D('Function')->write_log('线索附件',1,'添加成功','nj_tregisterfile',$attachid,M('nj_tregisterfile')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功','data'=>$attachid));
		}else{
			D('Function')->write_log('线索附件',0,'添加失败','nj_tregisterfile',0,M('nj_tregisterfile')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'添加失败'));
		}
	}

	/**
	 *删除附件
	 * by zw
	*/
	public function delete_files(){
		$fid 			= I('fid');//附件id
		$fregisterid 	= I('fregisterid');//登记表ID
		$do_te = M('nj_tregisterfile')->alias('a')->field('fcreateregualtorpersonid')->join('nj_tregisterflow b on a.fillegaladflowid=b.flowid and b.fcreateregualtorpersonid='.session('regulatorpersonInfo.fid'),'left')->where('fid='.$fid.' and a.fregisterid='.$fregisterid)->find();
		if(!empty($do_te)){
			if(empty($do_te['fcreateregualtorpersonid']) || $do_te['fcreateregualtorpersonid']==session('regulatorpersonInfo.fid')){
				M('nj_tregisterfile')->where('fid='.$fid)->delete();
				D('Function')->write_log('线索附件',1,'删除成功','nj_tregisterfile',0,M('nj_tregisterfile')->getlastsql());
				$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
			}else{
				D('Function')->write_log('线索附件',0,'添加失败','nj_tregisterfile',0,M('nj_tregisterfile')->getlastsql());
				$this->ajaxReturn(array('code'=>1,'msg'=>'添加失败'));
			}
		}else{
			D('Function')->write_log('线索附件',0,'添加失败','nj_tregisterfile',0,M('nj_tregisterfile')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'添加失败'));
		}
	}

	/**
	 *查询违法广告的播放记录
	 * by zw
	*/
	public function tiaoci_list() {
		session_write_close();
		$system_num = getconfig('system_num');
        $p = I('page', 1);//当前第几页
        $pp = 10;//每页显示多少记录

        $fissuedatest   = I('fissuedatest');//发布时间起
        $fissuedateed   = I('fissuedateed');//发布时间止
        $adid           = I('adid');//违法广告ID

        $wheres1 = '1=1';
        $wheres1 .= ' and tbn_illegal_ad_issue.fillegal_ad_id='.$adid;

        if(!empty($fissuedatest) && !empty($fissuedateed)){
            $wheres1 .= ' and tbn_illegal_ad_issue.fissue_date between "'.$fissuedatest.'" and "'.$fissuedateed.'"';
        }

        //查询总量
        $count = M()->query('
            SELECT count(*) as tccount
            FROM tbn_illegal_ad,tbn_illegal_ad_issue,tmedia,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd 
            WHERE tbn_illegal_ad.fid = snd.illad_id and tbn_illegal_ad.fid=tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fmedia_id=tbn_illegal_ad_issue.fmedia_id and fcustomer="'.$system_num.'" AND
            tmedia.fid=tbn_illegal_ad_issue.fmedia_id AND
            tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid').' AND 
            tmedia.fid=tmedia.main_media_id AND
             '.$wheres1.'
        ');

        //查询列表信息
        $data = M()->query('
            SELECT tbn_illegal_ad.fid as sid,
                tbn_illegal_ad.fmedia_class,
                tbn_illegal_ad.fad_name,
                 (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,
                tbn_illegal_ad_issue.fstarttime,
                tbn_illegal_ad_issue.fendtime,
                tbn_illegal_ad_issue.fpage,
                tbn_illegal_ad.fillegal,
                tbn_illegal_ad.fexpressions,
                tbn_illegal_ad.fillegal_code,
                tbn_illegal_ad.favifilename,
                tbn_illegal_ad.fjpgfilename,
                (UNIX_TIMESTAMP(tbn_illegal_ad_issue.fendtime)-UNIX_TIMESTAMP(tbn_illegal_ad_issue.fstarttime)) as difftime 
            FROM tbn_illegal_ad,tbn_illegal_ad_issue,tmedia,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd
            WHERE tbn_illegal_ad.fid = snd.illad_id and tbn_illegal_ad.fid=tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fmedia_id=tbn_illegal_ad_issue.fmedia_id and fcustomer="'.$system_num.'" AND
            tmedia.fid=tbn_illegal_ad_issue.fmedia_id AND 
            tmedia.fid=tmedia.main_media_id AND
             '.$wheres1.'
            ORDER BY fstarttime desc
            LIMIT '.($p-1)*$pp.','.$pp);

        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data,'count'=>$count[0]['tccount']));
	}

}