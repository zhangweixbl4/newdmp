const analysis = [{
    path: '/industryAnalysis',
    name: 'industryAnalysis',
    component: () => import('../components/analysis/industryAnalysis'),
    meta: ['数据分析', '行业分析']
  },
  {
    path: '/specialAnalysis',
    name: 'specialAnalysis',
    component: () => import('../components/analysis/specialAnalysis'),
    meta: ['数据分析', '专项分析']
  }
]
export default analysis
