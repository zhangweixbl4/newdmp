<?php
namespace Agp\Controller;
use Think\Controller;
use Agp\Model\StatisticalReportModel;
use Agp\Model\StatisticalModel;

class OutWordController extends BaseController{

    /**
     * 获取历史报告列表
     * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（count-记录总数，list-列表数据）
     * by zw
     */
    public function index() {
        session_write_close();
        header("Content-type:text/html;charset=utf-8");
        $system_num = getconfig('system_num');

        $p                  = I('page', 1);//当前第几页
        $pp                 = 20;//每页显示多少记录
        $pnname             = I('pnname');// 报告名称
        $pntype             = I('pntype');// 报告类型
        $pnfiletype         = I('pnfiletype');// 文件类型
        $pncreatetime       = I('pnendtime');//报告时间

        if (!empty($pnname)) {
            $where['pnname'] = array('like', '%' . $pnname . '%');//报告名称
        }
        if (!empty($pntype)) {
            $where['pntype'] = $pntype;//报告类型
        }else{
            $where['pntype'] = array('in',array(10,20,30,70,80));//报告类型
        }
        if (!empty($pnfiletype)) {
            $where['pnfiletype'] = $pnfiletype;//文件类型
        }
        if(!empty($pncreatetime)){
            $where['pnendtime'] = array('between',array($pncreatetime[0],$pncreatetime[1]));
        }   
        $where['pntrepid'] = session('regulatorpersonInfo.fregulatorpid');
        $where['fcustomer']  = $system_num;

        $count = M('tpresentation')
            ->where($where)
            ->count();

        
        $data = M('tpresentation')
            ->where($where)
            ->order('pnendtime desc')
            ->page($p,$pp)
            ->select();
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
    }

    //报告删除接口
    public function tpresentation_delete() {

        $pnid         = I('pnid');//报告ID
        $user = session('regulatorpersonInfo');//获取用户信息
        $system_num = getconfig('system_num');

        $where['pnid'] = $pnid;
        $where['pntrepid'] = $user['fregulatorpid'];
        $where['fcustomer']  = $system_num;

        $data = M('tpresentation')->where($where)->delete();

        if($data > 0){
            $this->ajaxReturn(array('code'=>0,'msg'=>'删除成功！'));
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'您无权删除此报告！'));
        }
    }

    //通用报告接口 新版月报20181012
    public function create_month(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $system_num = getconfig('system_num');

        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型
        //$month = I('daytime','2018-07');//接收时间  TODO::时间改
        $month = I('daytime',date('Y-m',time()));//接收时间  TODO::时间改
        $report_start_date = strtotime($month);//指定月份月初时间戳
        $report_end_date = mktime(23, 59, 59, date('m', strtotime($month))+1, 00);//指定月份月末时间戳
        if($report_end_date > time()){
            $report_end_date = time();

        }
        $date_ym = date('Y年m月',$report_start_date);//年月字符

        $user = session('regulatorpersonInfo');//获取用户信息
        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级
        if($re_level == 2 || $re_level == 3 || $re_level == 4){
            $dq_name = '全市';
            $xj_name = '区县';
        }elseif($re_level == 1){
            $dq_name = '全省';
            $xj_name = '市级';
        }else{
            $dq_name = '全'.$user['regionname1'];
            $xj_name = '地方';
        }
        $date_table = date('Ym',strtotime($month)).'_'.substr($user['regionid'],0,2);//组合表

        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数
        $owner_media_ids = get_owner_media_ids();
        $ct_media_tv_count = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_end_date);//电视媒体数量

        $ct_media_bc_count = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date);//广播媒体数量

        $ct_media_paper_count = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date);//报纸媒体数量

        $ct_media_count = $ct_media_tv_count + $ct_media_bc_count + $ct_media_paper_count;


        $pc_media_count = $StatisticalReport->net_media_count($owner_media_ids,'net_platform',1,$report_start_date,$report_end_date);//pc媒体数量

        $gzh_media_count = $StatisticalReport->net_media_count($owner_media_ids,'source_type',4,$report_start_date,$report_end_date);//公众号数量
        $pc_illegal_situation = $StatisticalReport->net_illegal_situation_customize($owner_media_ids,['net_platform',1],$report_start_date,$report_end_date);//pc违法情形列表

        /*
        * @各媒体监测总情况
        *
        **/
        $net_media_customize = $StatisticalReport->net_media_customize_sum($owner_media_ids,false,$report_start_date,$report_end_date);//互联网监测总情况
        $net_gzh_customize = $StatisticalReport->net_media_customize_sum($owner_media_ids,['source_type',4],$report_start_date,$report_end_date);//微信广告数量
        $net_pc_customize = $StatisticalReport->net_media_customize_sum($owner_media_ids,['net_platform',1],$report_start_date,$report_end_date);//pc广告数量
        $ct_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_count_rate','fmediaclass',false,1,get_check_dates());//传统媒体监测总情况
        $ct_fad_times = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times = 0;//定义传统媒体发布违法广告条次
        $every_media_data = [];//定义媒体发布监测情况数组
//循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk as $ct_jczqk_val){
            $ct_fad_times += $ct_jczqk_val['fad_times'];
            $ct_fad_illegal_times += $ct_jczqk_val['fad_illegal_times'];
            $ct_fad_play_len += $ct_jczqk_val['fad_play_len'];
            $ct_fad_illegal_play_len += $ct_jczqk_val['fad_illegal_play_len'];
            $ct_data_val = [
                $ct_jczqk_val['tmediaclass_name'],
                $ct_jczqk_val['fad_times'],
                $ct_jczqk_val['fad_illegal_times'],
                $ct_jczqk_val['fad_illegal_times_rate'].'%',
                $ct_jczqk_val['fad_play_len'],
                $ct_jczqk_val['fad_illegal_play_len'],
                $ct_jczqk_val['fad_illegal_play_len_rate']?$ct_jczqk_val['fad_illegal_play_len_rate'].'%':'0.00'.'%'
            ];
            if($ct_jczqk_val['fmedia_class_code'] == '01'){
                $every_media_data[0] = $ct_data_val;
            }elseif($ct_jczqk_val['fmedia_class_code'] == '02'){
                $every_media_data[1] = $ct_data_val;
            }else{
                $every_media_data[2] = $ct_data_val;
            }
        }
        $every_media_data[3] = [
            '互联网',
            $net_media_customize['total'],
            $net_media_customize['wfggsl'],
            $net_media_customize['ggslwfl'].'%',
            '0',
            '0',
            '0.00%'
        ];
        $every_media_data[4] = [
            '合计',
            strval($ct_fad_times+$net_media_customize['total']),
            strval($ct_fad_illegal_times+$net_media_customize['wfggsl']),
            round((($ct_fad_illegal_times+$net_media_customize['wfggsl'])/($ct_fad_times+$net_media_customize['total']))*100,2).'%',
            strval($ct_fad_play_len),
            strval($ct_fad_illegal_play_len),
            round(($ct_fad_illegal_play_len/$ct_fad_play_len)*100,2).'%'
        ];

        /*
        * @各地域监测总情况
        *
        **/
        $every_region_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times_rate','fregionid',false,1,get_check_dates());//各地域监测总情况

        $every_region_data = [];//定义各地区发布监测情况数组
        $every_region_num = 0;
//循环赋值
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){
            if($every_region_jczqk_val['fad_illegal_times'] != 0){
                $every_region_num++;
                $every_region_name[] = $every_region_jczqk_val['tregion_name'];
            }
            $every_region_data[] = [
                strval($every_region_jczqk_key+1),
                $every_region_jczqk_val['tregion_name'],
                $every_region_jczqk_val['fad_times'],
                $every_region_jczqk_val['fad_illegal_times'],
                $every_region_jczqk_val['fad_illegal_times_rate'].'%',
                $every_region_jczqk_val['fad_play_len'],
                $every_region_jczqk_val['fad_illegal_play_len'],
                $every_region_jczqk_val['fad_illegal_play_len_rate']?$every_region_jczqk_val['fad_illegal_play_len_rate'].'%':'0.00'.'%'
            ];
        }

        /*
        * @各广告类型监测总情况
        *
        **/
        $every_adclass_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times_rate','fcode',false,1,get_check_dates());//各广告类型监测总情况
        $every_adclass_data = [];//定义各大类发布监测情况数组
        $adclass_illegal_times = 0;//各违法地域数量
//循环赋值
        foreach ($every_adclass_jczqk as $every_adclass_jczqk_key => $every_adclass_jczqk_val){
            if($every_adclass_jczqk_val['fad_illegal_times'] > 0){
                $adclass_illegal_times += $every_adclass_jczqk_val['fad_illegal_times'];
            }
            $every_adclass_data[] = [strval($every_adclass_jczqk_key+1),$every_adclass_jczqk_val['tadclass_name'],$every_adclass_jczqk_val['fad_times'],$every_adclass_jczqk_val['fad_illegal_times'],$every_adclass_jczqk_val['fad_illegal_times_rate'].'%'];
        }

        foreach ($every_adclass_data as $every_adclass_data_key=>$every_adclass_data_val){
            if($every_adclass_data_val[3] > 0){
                $every_adclass_name[] = $every_adclass_data_val[1];
                if($every_adclass_data_key == 0){
                    $every_adclass_rate_des[] = $every_adclass_data_val[1].'广告占涉嫌违法广告总量的'.round(($every_adclass_data_val[3]/$adclass_illegal_times)*100,2).'％';
                }else{
                    $every_adclass_rate_des[] .= $every_adclass_data_val[1].'广告占'.round(($every_adclass_data_val[3]/$adclass_illegal_times)*100,2).'％';
                }
            }
        }

        /*
        * @电视各地域监测总情况
        *
        **/
        $every_tv_region_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'01','','fad_illegal_times_rate','fregionid',false,1,get_check_dates());//电视各地域监测总情况
        $every_tv_region_data = [];//定义各地区发布监测情况数组
        $every_tv_region_num = 0;//电视各违法地域数量
//循环赋值
        foreach ($every_tv_region_jczqk as $every_tv_region_jczqk_key => $every_tv_region_jczqk_val){
            if($every_tv_region_jczqk_val['fad_illegal_times'] != 0){
                $every_tv_region_num++;
                $every_tv_region_name[] = $every_tv_region_jczqk_val['tregion_name'];
            }
            $every_tv_region_data[] = [
                strval($every_tv_region_jczqk_key+1),
                $every_tv_region_jczqk_val['tregion_name'],
                $every_tv_region_jczqk_val['fad_times'],
                $every_tv_region_jczqk_val['fad_illegal_times'],
                $every_tv_region_jczqk_val['fad_illegal_times_rate'].'%',
                $every_tv_region_jczqk_val['fad_play_len'],
                $every_tv_region_jczqk_val['fad_illegal_play_len'],
                $every_tv_region_jczqk_val['fad_illegal_play_len_rate'].'%'
            ];
        }


        /*
        * @电视各广告类型监测总情况
        *
        **/
        $every_tv_adclass_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'01','','fad_illegal_times_rate','fcode',false,1,get_check_dates());//电视各广告类型监测总情况
        $every_tv_adclass_data = [];//定义各地区发布监测情况数组
//循环赋值
        foreach ($every_tv_adclass_jczqk as $every_tv_adclass_jczqk_key => $every_tv_adclass_jczqk_val){
            /*            if($every_tv_adclass_jczqk_val['fad_illegal_times'] != 0){
            $every_tv_adclass_num++;
            $every_tv_adclass_name .= $every_tv_region_jczqk_val['tregion_name'].'、';
            }*/
            $every_tv_adclass_data[] = [
                strval($every_tv_adclass_jczqk_key+1),
                $every_tv_adclass_jczqk_val['tadclass_name'],
                $every_tv_adclass_jczqk_val['fad_times'],
                $every_tv_adclass_jczqk_val['fad_illegal_times'],
                $every_tv_adclass_jczqk_val['fad_illegal_times_rate'].'%',
                $every_tv_adclass_jczqk_val['fad_play_len'],
                $every_tv_adclass_jczqk_val['fad_illegal_play_len'],
                $every_tv_adclass_jczqk_val['fad_illegal_play_len_rate'].'%'
            ];
        }


        /*
        * @广播各地域监测总情况
        *
        **/
        $every_tb_region_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'02','','fad_illegal_times_rate','fregionid',false,1,get_check_dates());//广播各地域监测总情况
        $every_tb_region_data = [];//定义各地区发布监测情况数组
        $every_tb_region_num = 0;//广播各违法地域数量
//循环赋值
        foreach ($every_tb_region_jczqk as $every_tb_region_jczqk_key => $every_tb_region_jczqk_val){
            if($every_tb_region_jczqk_val['fad_illegal_times'] != 0){
                $every_tb_region_num++;
                $every_tb_region_name[] = $every_tb_region_jczqk_val['tregion_name'];
            }
            $every_tb_region_data[] = [
                strval($every_tb_region_jczqk_key+1),
                $every_tb_region_jczqk_val['tregion_name'],
                $every_tb_region_jczqk_val['fad_times'],
                $every_tb_region_jczqk_val['fad_illegal_times'],
                $every_tb_region_jczqk_val['fad_illegal_times_rate'].'%',
                $every_tb_region_jczqk_val['fad_play_len'],
                $every_tb_region_jczqk_val['fad_illegal_play_len'],
                $every_tb_region_jczqk_val['fad_illegal_play_len_rate'].'%'
            ];
        }


        /*
        * @广播各广告类型监测总情况
        *
        **/
        $every_tb_adclass_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'02','','fad_illegal_times_rate','fcode',false,1,get_check_dates());//广播各广告类型监测总情况
        $every_tb_adclass_data = [];//定义各地区发布监测情况数组
//循环赋值
        foreach ($every_tb_adclass_jczqk as $every_tb_adclass_jczqk_key => $every_tb_adclass_jczqk_val){
            /*            if($every_tb_adclass_jczqk_val['fad_illegal_times'] != 0){
            $every_tb_adclass_num++;
            $every_tb_adclass_name .= $every_tb_region_jczqk_val['tregion_name'].'、';
            }*/
            $every_tb_adclass_data[] = [
                strval($every_tb_adclass_jczqk_key+1),
                $every_tb_adclass_jczqk_val['tadclass_name'],
                $every_tb_adclass_jczqk_val['fad_times'],
                $every_tb_adclass_jczqk_val['fad_illegal_times'],
                $every_tb_adclass_jczqk_val['fad_illegal_times_rate'].'%',
                $every_tb_adclass_jczqk_val['fad_play_len'],
                $every_tb_adclass_jczqk_val['fad_illegal_play_len'],
                $every_tb_adclass_jczqk_val['fad_illegal_play_len_rate'].'%'
            ];
        }


        /*
        * @报纸各地域监测总情况
        *
        **/
        $every_tp_region_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'03','','fad_illegal_times_rate','fregionid',false,1,get_check_dates());//报纸各地域监测总情况
        $every_tp_region_data = [];//定义各地区发布监测情况数组
        $every_tp_region_num = 0;//报纸各违法地域数量
//循环赋值
        foreach ($every_tp_region_jczqk as $every_tp_region_jczqk_key => $every_tp_region_jczqk_val){
            if($every_tp_region_jczqk_val['fad_illegal_times'] != 0){
                $every_tp_region_num++;
                $every_tp_region_name[] = $every_tp_region_jczqk_val['tregion_name'];
            }
            $every_tp_region_data[] = [
                strval($every_tp_region_jczqk_key+1),
                $every_tp_region_jczqk_val['tregion_name'],
                $every_tp_region_jczqk_val['fad_times'],
                $every_tp_region_jczqk_val['fad_illegal_times'],
                $every_tp_region_jczqk_val['fad_illegal_times_rate'].'%'
            ];
        }


        /*
        * @报纸各广告类型监测总情况
        *
        **/
        $every_tp_adclass_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'03','','fad_illegal_times_rate','fcode',false,1,get_check_dates());//报纸各广告类型监测总情况
        $every_tp_adclass_data = [];//定义各地区发布监测情况数组
//循环赋值
        foreach ($every_tp_adclass_jczqk as $every_tp_adclass_jczqk_key => $every_tp_adclass_jczqk_val){
            /*            if($every_tb_adclass_jczqk_val['fad_illegal_times'] != 0){
            $every_tb_adclass_num++;
            $every_tb_adclass_name .= $every_tb_region_jczqk_val['tregion_name'].'、';
            }*/
            $every_tp_adclass_data[] = [
                strval($every_tp_adclass_jczqk_key+1),
                $every_tp_adclass_jczqk_val['tadclass_name'],
                $every_tp_adclass_jczqk_val['fad_times'],
                $every_tp_adclass_jczqk_val['fad_illegal_times'],
                $every_tp_adclass_jczqk_val['fad_illegal_times_rate'].'%'
            ];
        }

        /*
        * @PC各媒体监测总情况
        *
        **/
        $every_pc_media_data = [];
        $every_pc_media_jczqk = $StatisticalReport->net_media_customize($owner_media_ids,['tns.net_platform',1],'tn.fmediaid',$report_start_date,$report_end_date);//pc媒体广告发布情况
        foreach ($every_pc_media_jczqk as $every_pc_media_jczqk_key=>$every_pc_media_jczqk_val){
            $every_pc_media_data[] = [
                strval($every_pc_media_jczqk_key+1),
                $every_pc_media_jczqk_val['dymc'],
                $every_pc_media_jczqk_val['total'],
                $every_pc_media_jczqk_val['wfggsl'],
                $every_pc_media_jczqk_val['ggslwfl'].'%',
                $every_pc_media_jczqk_val['yzwfggsl'],
                $every_pc_media_jczqk_val['ggslyzwfl'].'%'
            ];
        }

        /*
        * @PC各广告类型监测总情况
        *
        **/
        $every_pc_adclass_data = [];
        $every_pc_adclass_jczqk = $StatisticalReport->net_media_customize($owner_media_ids,['tns.net_platform',1],'ac.fcode',$report_start_date,$report_end_date);//pc媒体广告发布情况


        foreach ($every_pc_adclass_jczqk as $every_pc_adclass_jczqk_key=>$every_pc_adclass_jczqk_val){
            if($every_pc_adclass_jczqk_val['wfggsl'] != 0){
                $every_pc_adclass_name[] = $every_pc_adclass_jczqk_val['dymc'];
            }
            $every_pc_adclass_data[] = [
                strval($every_pc_adclass_jczqk_key+1),
                $every_pc_adclass_jczqk_val['dymc'],
                $every_pc_adclass_jczqk_val['total'],
                $every_pc_adclass_jczqk_val['wfggsl'],
                $every_pc_adclass_jczqk_val['ggslwfl'].'%',
                $every_pc_adclass_jczqk_val['yzwfggsl'],
                $every_pc_adclass_jczqk_val['ggslyzwfl'].'%'
            ];
        }
        /*
        * @微信各媒体监测总情况
        **/
        $every_wx_media_data = [];
        $every_wx_media_jczqk = $StatisticalReport->net_media_customize($owner_media_ids,['tns.source_type',4],'tn.fmediaid',$report_start_date,$report_end_date);//pc媒体广告发布情况
        foreach ($every_wx_media_jczqk as $every_wx_media_jczqk_key=>$every_wx_media_jczqk_val){
            /*            if($every_wx_media_jczqk_val['wfggsl'] != 0){
            $every_wx_media_name .= $every_wx_media_jczqk_val['dymc'].'、';
            }*/
            $every_wx_media_data[] = [
                strval($every_wx_media_jczqk_key+1),
                $every_wx_media_jczqk_val['dymc'],
                $every_wx_media_jczqk_val['total'],
                $every_wx_media_jczqk_val['wfggsl'],
                $every_wx_media_jczqk_val['ggslwfl'].'%',
                $every_wx_media_jczqk_val['yzwfggsl'],
                $every_wx_media_jczqk_val['ggslyzwfl'].'%'
            ];
        }

//定义数据数组
        $data = [
            'dotfilename' => 'ReportTemplate.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报告标题一",
            "text" => $user['regulatorpname'].""
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报告标题二",
            "text" => $date_ym."广告监测通报"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "通报对象",
            "text" => $dq_name."工商行政管理局、市场监督管理部门："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报表通报描述",
            "text" => "（".$date_ymd."-".$date_md. "），". $user['regulatorpname']."广告监测中心对".$dq_name."所属的媒介广告发布情况进行了监测，现将有关情况通报如下："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "一、监测范围及对象",
            "text" => "一、监测范围及对象"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（一）传统媒体广告监测",
            "text" => "（一）传统媒体广告监测"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "传统媒体广告监测描述",
            "text" => "本期共对".$dq_name."所属的".$ct_media_count."家电视、广播、报纸等传统媒体广告发布情况进行了监测，其中电视媒体".$ct_media_tv_count."家、广播媒体".$ct_media_bc_count."家、报纸媒体".$ct_media_paper_count."家，广告监测范围为".$date_ymd."-".$date_md."（共".$days."天）"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（二）互联网媒体广告监测",
            "text" => "（二）互联网媒体广告监测"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "互联网媒体广告监测描述",
            "text" => "本期共对".$dq_name."所属的".$pc_media_count."家主要PC门户网及".$gzh_media_count."个微信公众号媒体，广告监测范围为".$date_ymd."-".$date_md."（共".$days."天）"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "二、监测总体情况",
            "text" => "二、监测总体情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（一）传统媒体广告监测情况",
            "text" => "（一）传统媒体广告监测情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "传统媒体广告监测情况描述",
            "text" => "本期共监测到电视、广播、报纸的各类广告".$ct_fad_times."条次，涉嫌违法广告".$ct_fad_illegal_times."条次，条次违法率为".round(($ct_fad_illegal_times/$ct_fad_times)*100,2)."％。"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（二）互联网媒体广告监测情况",
            "text" => "（二）互联网媒体广告监测情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "互联网媒体广告监测情况描述",
            "text" => "本期共监测到互联网广告".$net_media_customize['total']."条次，其中涉嫌违法广告".$net_media_customize['wfggsl']."条次，违法率".$net_media_customize['ggslwfl']."%。"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表1各类媒体广告监测统计情况标题",
            "text" => "表1各类媒体广告监测统计情况"
        ];
        $every_media_data = array_values($every_media_data);
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表1各类媒体广告监测统计情况列表含时长",
            "data" => $every_media_data
        ];
        unset($every_media_data[4]);
        $every_media_data = $this->create_chart_data($every_media_data,0,1,2,3);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告监测情况条状图",
            "data" => $every_media_data[0]
        ];
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告发布量饼状图",
            "data" => $every_media_data[1]
        ];
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告违法发布量饼状图",
            "data" => $every_media_data[2]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "三、传统媒体广告监测情况",
            "text" => "三、传统媒体广告监测情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（一）广告总体监测情况",
            "text" => "（一）广告总体监测情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "广告总体监测情况描述",
            "text" => "本期监测数据显示，发布涉嫌违法广告条次违法率最为严重的".$every_region_num."个".$xj_name."依次为：".implode('、',$every_region_name)."。"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表2本期各地区发布涉嫌违法广告情况标题",
            "text" => "表2 本期各地区发布涉嫌违法广告情况"
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表2本期各地区发布涉嫌违法广告情况列表含时长",
            "data" => $every_region_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表2注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];

        $every_region_data_chart = $this->create_chart_data($every_region_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告监测情况条状图",
            "data" => $every_region_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告发布量饼状图",
            "data" => $every_region_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告违法发布量饼状图",
            "data" => $every_region_data_chart[2]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表3本期各大类违法广告发布总情况标题",
            "text" => '表3本期各大类违法广告发布总情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表3本期各大类违法广告发布总情况列表",
            "data" => $every_adclass_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表3注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];
        $every_adclass_data_chart = $this->create_chart_data($every_adclass_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告监测情况列条状图",
            "data" => $every_adclass_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告发布量饼状图",
            "data" => $every_adclass_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告违法发布量饼状图",
            "data" => $every_adclass_data_chart[2]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "各类媒体广告发布情况描述",
            "text" => '涉嫌违法广告类别主要集中在'.implode('、',$every_adclass_name).'，其中'.implode("、",$every_adclass_rate_des).'。'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（二）电视发布涉嫌违法广告情况",
            "text" => '（二）电视发布涉嫌违法广告情况'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "电视发布涉嫌违法广告情况描述",
            "text" => '本期电视发布涉嫌违法广告条次违法率最为严重的'.$every_tv_region_num.'个地区依次为：'.implode('、',$every_tv_region_name).'。'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表4电视发布涉嫌严重违法广告情况标题",
            "text" => '表4 电视发布涉嫌广告情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表4电视发布涉嫌严重违法广告情况列表",
            "data" => $every_tv_region_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表4注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];
        $every_tv_region_data_chart = $this->create_chart_data($every_tv_region_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "电视广告监测情况条状图",
            "data" => $every_tv_region_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "电视广告发布量饼状图",
            "data" => $every_tv_region_data_chart[1]
        ];
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "电视广告违法发布量饼状图",
            "data" => $every_tv_region_data_chart[2]
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表5电视各广告类型发布涉嫌违法广告情况标题",
            "text" => '表5 电视各广告类型发布涉嫌违法广告情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表5电视各广告类型发布涉嫌违法广告情况列表",
            "data" => $every_tv_adclass_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表5注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];

        $every_tv_adclass_data_chart = $this->create_chart_data($every_tv_adclass_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "电视各广告类型广告监测情况条状图",
            "data" => $every_tv_adclass_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "电视各广告类型广告发布量饼状图",
            "data" => $every_tv_adclass_data_chart[1]
        ];
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "电视各广告类型广告违法发布量饼状图",
            "data" => $every_tv_adclass_data_chart[2]
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（三）广播发布涉嫌违法广告情况",
            "text" => '（三）广播发布涉嫌违法广告情况'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "广播发布涉嫌违法广告情况描述",
            "text" => '本期广播发布涉嫌违法广告条次违法率最为严重的'.$every_tb_region_num.'个地区依次为：'.implode('、',$every_tb_region_name).'。'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表6广播发布涉嫌违法广告情况标题",
            "text" => '表6 广播发布涉嫌违法广告情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表6广播发布涉嫌违法广告情况列表",
            "data" => $every_tb_region_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表6注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];

        $every_tb_region_data_chart = $this->create_chart_data($every_tb_region_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "广播广告监测情况条状图",
            "data" => $every_tb_region_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "广播广告发布量饼状图",
            "data" => $every_tb_region_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "广播广告违法发布量饼状图",
            "data" => $every_tb_region_data_chart[2]
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表7广播各广告类别发布涉嫌违法广告情况标题",
            "text" => '表7 广播各广告类别发布涉嫌违法广告情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表7广播各广告类别发布涉嫌违法广告情况列表",
            "data" => $every_tb_adclass_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表7注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];

        $every_tb_adclass_data_chart = $this->create_chart_data($every_tb_adclass_data);

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "广播各广告类型广告监测情况条状图",
            "data" => $every_tb_adclass_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "广播各广告类型广告发布量饼状图",
            "data" => $every_tb_adclass_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "广播各广告类型广告违法发布量饼状图",
            "data" => $every_tb_adclass_data_chart[2]
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（四）报纸发布涉嫌违法广告情况",
            "text" => '（四）报纸发布涉嫌违法广告情况'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报纸发布涉嫌违法广告情况描述",
            "text" => '本期报纸发布涉嫌违法广告条次违法率最为严重的'.$every_tp_region_num.'个地区依次为：'.implode('、',$every_tp_region_name).'。'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表8报纸发布涉嫌违法广告情况标题",
            "text" => '表8 报纸发布涉嫌违法广告情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表8报纸发布涉嫌违法广告情况列表",
            "data" => $every_tp_region_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表8注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];
        $every_tp_region_data_chart = $this->create_chart_data($every_tp_region_data);

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "报纸广告监测情况条状图",
            "data" => $every_tp_region_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "报纸广告发布量饼状图",
            "data" => $every_tp_region_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "报纸广告违法发布量饼状图",
            "data" => $every_tp_region_data_chart[2]
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表9报纸各广告类型发布涉嫌违法广告情况标题",
            "text" => '表9 报纸各广告类型发布涉嫌违法广告情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表9报纸各广告类型发布涉嫌违法广告情况列表",
            "data" => $every_tp_adclass_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表9注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];
        $every_tp_adclass_data_chart = $this->create_chart_data($every_tp_adclass_data);

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "报纸各广告类型广告监测情况条状图",
            "data" => $every_tp_adclass_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "报纸各广告类型广告发布量饼状图",
            "data" => $every_tp_adclass_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "报纸各广告类型广告违法发布量饼状图",
            "data" => $every_tp_adclass_data_chart[2]
        ];


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "四、互联网广告监测情况",
            "text" => "四、互联网广告监测情况"
        ];
        if($net_media_customize['total'] != 0){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "（一）总体发布情况",
                "text" => "（一）总体发布情况"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "总体发布情况描述",
                "text" => "本期共监测到互联网广告".$net_media_customize['total']."条次，其中违法广告".$net_media_customize['wfggsl']."条，违法率".$net_media_customize['ggslwfl']."%。微信公众号全部类别广告".$net_gzh_customize['total']."条。"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "PC门户网站广告发布情况标题",
                "text" => "1、PC门户网站广告发布情况"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "PC门户网站广告发布情况列表",
                "data" => $every_pc_media_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "PC门户网站广告发布情况列表注释",
                "text" => "（注：按发布涉嫌违法广告情况严重程度从高到低排列）"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "PC门户网站广告类别发布情况标题",
                "text" => "2、PC门户网站广告类别发布情况"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "PC门户网站广告类别发布情况列表",
                "data" => $every_pc_adclass_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "PC门户网站广告类别发布情况列表注释",
                "text" => "（注：按发布涉嫌违法广告情况严重程度从高到低排列）"
            ];
            $every_pc_adclass_name = implode('、',$every_pc_adclass_name);
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "PC门户网站广告类别发布情况列表描述",
                "text" => $every_pc_adclass_name."是本月网络违法广告主要类别。"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "微信公众号发布情况",
                "text" => "3、微信公众号发布情况"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "微信公众号发布情况列表",
                "data" => $every_wx_media_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "微信公众号发布情况列表注释",
                "text" => "（注：按发布涉嫌违法广告情况严重程度从高到低排列）"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "（二）涉嫌违法发布情况",
                "text" => "（二）涉嫌违法发布情况"
            ];

            if($net_pc_customize['wfggsl'] != 0){
                $ybwf = '其中涉嫌违法广告'.$net_pc_customize['wfggsl'].'条，违法率'.$net_pc_customize['ggslwfl'].'%。';
                /*            if($net_pc_customize['yzwfggsl'] != 0){
                $yzwf = '涉嫌严重违法广告'.$net_pc_customize['yzwfggsl'].'条，严重违法率'.$net_pc_customize['ggslyzwfl'].'%，';
                }else{
                $yzwf = '未发现严重违法广告';
                }*/
                $pc_des = $ybwf;
            }else{
                $pc_des = '未发现涉嫌违法广告。';
            }

            if($net_gzh_customize['wfggsl'] != 0){
                $ybwf = '其中涉嫌违法广告'.$net_gzh_customize['wfggsl'].'条，违法率'.$net_gzh_customize['ggslwfl'].'%。';
                /*            if($net_gzh_customize['yzwfggsl'] != 0){
                $yzwf = '涉嫌严重违法广告'.$net_gzh_customize['yzwfggsl'].'条，严重违法率'.$net_gzh_customize['ggslyzwfl'].'%，';
                }else{
                $yzwf = '未发现严重违法广告';
                }*/
                $wx_des = $ybwf;
            }else{
                $wx_des = '未发现涉嫌违法广告。';
            }
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "涉嫌违法发布情况描述一",
                "text" => "本月次监测PC门户网站全部类别广告".$net_pc_customize['total']."条，".$pc_des."微信公众号全部类别广告".$net_gzh_customize['total']."条，".$wx_des
            ];
            $every_pc_adclass_des = '';
            foreach ($every_pc_adclass_data as $every_pc_adclass_data_key=>$every_pc_adclass_data_val){
                if($every_pc_adclass_data_val['3'] > 0){
                    $every_pc_adclass_name .= $every_pc_adclass_data_val[1].'、';
                    if($every_pc_adclass_data_key == 0){
                        $every_pc_adclass_des .= $every_pc_adclass_data_val[1].'广告占涉嫌违法广告总量的'.round(($every_pc_adclass_data_val[3]/$net_pc_customize['wfggsl'])*100,2).'％、';
                    }else{
                        $every_pc_adclass_des .= $every_pc_adclass_data_val[1].'广告占'.round(($every_pc_adclass_data_val[3]/$net_pc_customize['wfggsl'])*100,2).'％、';
                    }
                }
            }
            $every_pc_media_des = '';
            foreach ($every_pc_media_data as $every_pc_media_data_key=>$every_pc_media_data_val){
                if($every_pc_media_data_val['3'] > 0){
                    $every_pc_media_name[] = $every_pc_media_data_val[1];
                    if($every_pc_media_data_key == 0){
                        $every_pc_media_des .= $every_pc_media_data_val[1].'发布的占涉嫌违法广告总量的'.round(($every_pc_media_data_val[3]/$net_pc_customize['wfggsl'])*100,2).'％、';
                    }else{
                        $every_pc_media_des .= $every_pc_media_data_val[1].'占'.round(($every_pc_media_data_val[3]/$net_pc_customize['wfggsl'])*100,2).'％、';
                    }
                }else{
                    $not_illegal_media_num++;
                    $not_illegal_media_name[] = $every_pc_media_data_val[1];
                }
            }
            if($every_pc_adclass_des == ''){
                $every_pc_adclass_des = '全部类别下无违法广告';
            }
            if($every_pc_media_des != ''){
                $every_pc_media_des = '所有PC门户网站未发布违法广告';
            }
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "涉嫌违法发布情况描述二",
                "text" => "其中PC门户网站违法违法分布情况如下：".$every_pc_adclass_des."。".$every_pc_media_des."。"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "涉嫌违法发布情况描述三",
                "text" => $not_illegal_media_num."家网站本次监测没有发现违法广告，分别是：".implode('、',$not_illegal_media_name)."。"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "涉嫌违法发布情况描述四",
                "text" => "PC门户网站中，".implode('、',$every_pc_media_name)."为主要的违法广告发布网站。"
            ];
            foreach ($every_pc_media_data as $every_pc_media_data_key=>$every_pc_media_data_val){
                array_splice($every_pc_media_data[$every_pc_media_data_key],0,1);
                array_splice($every_pc_media_data[$every_pc_media_data_key],1,1);
                array_splice($every_pc_media_data[$every_pc_media_data_key],2,1);
                array_splice($every_pc_media_data[$every_pc_media_data_key],2,1);
                array_splice($every_pc_media_data[$every_pc_media_data_key],2,1);
            }
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "PC网站违法发布量饼状图",
                "data" => $every_pc_media_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "PC门户网站主要违法情形",
                "text" => "1、PC门户网站主要违法情形"
            ];
            $every_pc_illegal_situation_data = [];
            foreach ($pc_illegal_situation as $pc_illegal_situation_key => $pc_illegal_situation_val){
                $every_pc_illegal_situation_data[] = [
                    strval($pc_illegal_situation_key+1),
                    $pc_illegal_situation_val['fillegalcontent'],
                    $pc_illegal_situation_val['illegal_count']
                ];
            }
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "PC门户网站主要违法情形列表",
                "data" => $every_pc_illegal_situation_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "微信公众号违法广告发布情况",
                "text" => "微信公众号违法广告发布情况"
            ];
            $every_wx_media_des = '';
            foreach ($every_wx_media_data as $every_wx_media_data_key=>$every_wx_media_data_val){
                if($every_wx_media_data_val['3'] > 0){
                    $every_wx_media_name .= $every_wx_media_data_val[1].'、';
                    if($every_wx_media_data_key == 0){
                        $every_wx_media_des .= $every_wx_media_data_val[1].'发布的占涉嫌违法广告总量的'.round(($every_wx_media_data_val[3]/$net_gzh_customize['wfggsl'])*100,2).'％、';
                    }else{
                        $every_wx_media_des .= $every_wx_media_data_val[1].'占'.round(($every_wx_media_data_val[3]/$net_gzh_customize['wfggsl'])*100,2).'％、';
                    }
                }else{
                    $not_illegal_wx_media_num++;
                    $not_illegal_wx_media_name .= $every_wx_media_data_val[1].'、';
                }
            }
            if($every_wx_media_des == ''){
                $every_wx_media_des = '本次监测微信公众号没有发布涉嫌违法广告数据。';
            }
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "微信公众号违法广告发布情况描述",
                "text" => $every_wx_media_des
            ];
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "微信公众号违法广告发布情况描述",
                "text" => '本期未检测到互联网数据'
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "落款",
            "text" => $user['regulatorpname'].""
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "时间",
            "text" => $date_end_ymd
        ];

        $report_data = json_encode($data);
        //echo $report_data;exit;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }
//将生成记录保存

            $focus = I('focus',0);
            $pnname = $user['regulatorname'].date('Y年m月',strtotime($month)).'月报';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 30;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }

    //通用报告接口 安徽月报
    public function create_month_anhui(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $system_num = getconfig('system_num');

        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型
        $month = I('daytime',date('Y-m',time()));//接收时间  TODO::时间改
        $report_start_date = strtotime($month);//指定月份月初时间戳
        $report_end_date = mktime(23, 59, 59, date('m', strtotime($month))+1, 00);//指定月份月末时间戳
        if($report_end_date > time()){
            $report_end_date = time();

        }

        //获取上个周期的日期范围
        $last_date = date('Y-m',strtotime(date("Y",strtotime($month)).'-'.(date("m",strtotime($month))-1)));
        $last_s_time = strtotime($last_date);
        $last_e_time = mktime(23, 59, 59, date('m', strtotime($last_date))+1, 00);

        $date_ym = date('Y年m月',$report_start_date);//年月字符
        $user = session('regulatorpersonInfo');//获取用户信息
        $date_table = date('Ym',strtotime($month)).'_'.substr($user['regionid'],0,2);//组合表

        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数

        $ct_media_count = $StatisticalReport->ct_media_count('all',$date_table,$user['regionid'],$report_start_date,$report_end_date,false);//传统媒体数量


        $last_media_class_date = $StatisticalReport->ad_monitor_customize($date_table,$last_s_time,$last_e_time,$user['regionid'],'','','fad_illegal_times_rate','fmediaclass',false,false);//上周期各媒体类型监测总情况
        $last_all_count = 0;
        $last_all_illegal_count = 0;
        $last_all_times = 0;
        $last_all_illegal_times = 0;
        $last_all_play_len = 0;
        $last_all_illegal_play_len = 0;
        foreach ($last_media_class_date as $last_media_class_date_key => $last_media_class_date_val){
            $last_all_count += $last_media_class_date_val['fad_count'];
            $last_all_illegal_count += $last_media_class_date_val['fad_illegal_count'];
            $last_all_times += $last_media_class_date_val['fad_times'];
            $last_all_illegal_times += $last_media_class_date_val['fad_illegal_times'];
            $last_all_play_len += $last_media_class_date_val['fad_play_len'];
            $last_all_illegal_play_len += $last_media_class_date_val['fad_illegal_play_len'];
        }

        $every_tv_media_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$user['regionid'],'01','','fad_illegal_times_rate','fmediaid',false,false);//电视各媒体监测总情况
        $every_tb_media_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$user['regionid'],'02','','fad_illegal_times_rate','fmediaid',false,false);//广播各媒体监测总情况
        $every_tp_media_jczqk = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$user['regionid'],'03','','fad_illegal_times_rate','fmediaid',false,false);//报纸各媒体监测总情况


        $tv_media_count = count($every_tv_media_jczqk);//电视媒体数量
        $tb_media_count = count($every_tb_media_jczqk);//广播媒体数量
        $tp_media_count = count($every_tp_media_jczqk);//报纸媒体数量

        $all_count = 0;
        $all_illegal_count = 0;
        $all_times = 0;
        $all_illegal_times = 0;
        $all_play_len = 0;
        $all_illegal_play_len = 0;


        $tv_count = 0;
        $tv_illegal_count = 0;
        $tv_times = 0;
        $tv_illegal_times = 0;
        $tv_play_len = 0;
        $tv_illegal_play_len = 0;
        $every_tv_media_data = [];
        foreach ($every_tv_media_jczqk as $every_tv_media_jczqk_key => $every_tv_media_jczqk_val){

            $all_count += $every_tv_media_jczqk_val['fad_count'];
            $all_illegal_count += $every_tv_media_jczqk_val['fad_illegal_count'];
            $all_times += $every_tv_media_jczqk_val['fad_times'];
            $all_illegal_times += $every_tv_media_jczqk_val['fad_illegal_times'];
            $all_play_len += $every_tv_media_jczqk_val['fad_play_len'];
            $all_illegal_play_len += $every_tv_media_jczqk_val['fad_illegal_play_len'];

            $tv_count += $every_tv_media_jczqk_val['fad_count'];
            $tv_illegal_count += $every_tv_media_jczqk_val['fad_illegal_count'];
            $tv_times += $every_tv_media_jczqk_val['fad_times'];
            $tv_illegal_times += $every_tv_media_jczqk_val['fad_illegal_times'];
            $tv_play_len += $every_tv_media_jczqk_val['fad_play_len'];
            $tv_illegal_play_len += $every_tv_media_jczqk_val['fad_illegal_play_len'];

            $every_tv_media_data[] = [
                $every_tv_media_jczqk_val['tmedia_name'],
                $every_tv_media_jczqk_val['fad_count'],
                $every_tv_media_jczqk_val['fad_illegal_count'].($every_tv_media_jczqk_val['last_zq_fad_illegal_count']?$every_tv_media_jczqk_val['last_zq_fad_illegal_count']:'(0.00%)'),
                $every_tv_media_jczqk_val['fad_illegal_count_rate'],
                $every_tv_media_jczqk_val['fad_times'],
                $every_tv_media_jczqk_val['fad_illegal_times'].($every_tv_media_jczqk_val['last_zq_fad_illegal_times']?$every_tv_media_jczqk_val['last_zq_fad_illegal_times']:'(0.00%)'),
                $every_tv_media_jczqk_val['fad_illegal_times_rate'],
                $every_tv_media_jczqk_val['fad_play_len'],
                $every_tv_media_jczqk_val['fad_illegal_play_len'].($every_tv_media_jczqk_val['last_zq_fad_illegal_play_len']?$every_tv_media_jczqk_val['last_zq_fad_illegal_play_len']:'(0.00%)'),
                $every_tv_media_jczqk_val['fad_illegal_play_len_rate'],
            ];

        }
        $tb_count = 0;
        $tb_illegal_count = 0;
        $tb_times = 0;
        $tb_illegal_times = 0;
        $tb_play_len = 0;
        $tb_illegal_play_len = 0;
        $every_tb_media_data = [];
        foreach ($every_tb_media_jczqk as $every_tb_media_jczqk_key => $every_tb_media_jczqk_val){

            $all_count += $every_tb_media_jczqk_val['fad_count'];
            $all_illegal_count += $every_tb_media_jczqk_val['fad_illegal_count'];
            $all_times += $every_tb_media_jczqk_val['fad_times'];
            $all_illegal_times += $every_tb_media_jczqk_val['fad_illegal_times'];
            $all_play_len += $every_tb_media_jczqk_val['fad_play_len'];
            $all_illegal_play_len += $every_tb_media_jczqk_val['fad_illegal_play_len'];

            $tb_count += $every_tb_media_jczqk_val['fad_count'];
            $tb_illegal_count += $every_tb_media_jczqk_val['fad_illegal_count'];
            $tb_times += $every_tb_media_jczqk_val['fad_times'];
            $tb_illegal_times += $every_tb_media_jczqk_val['fad_illegal_times'];
            $tb_play_len += $every_tb_media_jczqk_val['fad_play_len'];
            $tb_illegal_play_len += $every_tb_media_jczqk_val['fad_illegal_play_len'];

            $every_tb_media_data[] = [
                $every_tb_media_jczqk_val['tmedia_name'],
                $every_tb_media_jczqk_val['fad_count'],
                $every_tb_media_jczqk_val['fad_illegal_count'].($every_tb_media_jczqk_val['last_zq_fad_illegal_count']?$every_tb_media_jczqk_val['last_zq_fad_illegal_count']:'(0.00%)'),
                $every_tb_media_jczqk_val['fad_illegal_count_rate'],
                $every_tb_media_jczqk_val['fad_times'],
                $every_tb_media_jczqk_val['fad_illegal_times'].($every_tb_media_jczqk_val['last_zq_fad_illegal_times']?$every_tb_media_jczqk_val['last_zq_fad_illegal_times']:'(0.00%)'),
                $every_tb_media_jczqk_val['fad_illegal_times_rate'],
                $every_tb_media_jczqk_val['fad_play_len'],
                $every_tb_media_jczqk_val['fad_illegal_play_len'].($every_tb_media_jczqk_val['last_zq_fad_illegal_play_len']?$every_tb_media_jczqk_val['last_zq_fad_illegal_play_len']:'(0.00%)'),
                $every_tb_media_jczqk_val['fad_illegal_play_len_rate'],
            ];

        }

        $tp_count = 0;
        $tp_illegal_count = 0;
        $tp_times = 0;
        $tp_illegal_times = 0;
        $every_tp_media_data = [];
        foreach ($every_tp_media_jczqk as $every_tp_media_jczqk_key => $every_tp_media_jczqk_val){

            $all_count += $every_tp_media_jczqk_val['fad_count'];
            $all_illegal_count += $every_tp_media_jczqk_val['fad_illegal_count'];
            $all_times += $every_tp_media_jczqk_val['fad_times'];
            $all_illegal_times += $every_tp_media_jczqk_val['fad_illegal_times'];

            $tp_count += $every_tp_media_jczqk_val['fad_count'];
            $tp_illegal_count += $every_tp_media_jczqk_val['fad_illegal_count'];
            $tp_times += $every_tp_media_jczqk_val['fad_times'];
            $tp_illegal_times += $every_tp_media_jczqk_val['fad_illegal_times'];
            $every_tp_media_data[] = [
                $every_tp_media_jczqk_val['tmedia_name'],
                $every_tp_media_jczqk_val['fad_count'],
                $every_tp_media_jczqk_val['fad_illegal_count'].($every_tp_media_jczqk_val['last_zq_fad_illegal_count']?$every_tp_media_jczqk_val['last_zq_fad_illegal_count']:'(0.00%)'),
                $every_tp_media_jczqk_val['fad_illegal_count_rate'],
                $every_tp_media_jczqk_val['fad_times'],
                $every_tp_media_jczqk_val['fad_illegal_times'].($every_tp_media_jczqk_val['last_zq_fad_illegal_count']?$every_tp_media_jczqk_val['last_zq_fad_illegal_count']:'(0.00%)'),
                $every_tp_media_jczqk_val['fad_illegal_times_rate'],
            ];

        }//0824

        //定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $data['content'][] = [
            "type"=>"filltable",
            "bookmark"=>"单个图片",
            "data"=>[
                ["index"=>1,"type"=>"image","content"=>'http://hzaic-gg.oss-cn-hangzhou.aliyuncs.com/1270b73d956a5477eeabd7e3dbc52f46.png']
            ]
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号居中正文",
            "text" => date('Y',$report_start_date).'年第*期'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号居中",
            "text" => '广告监管专刊（总第*期）'
        ];//


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号落款靠右",
            "text" => '安徽省工商行政管理局&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.date('Y年m月d日',time())
        ];

        $data['content'][] = [
            "type"=>"filltable",
            "bookmark"=>"单个图片",
            "data"=>[
                ["index"=>1,"type"=>"image","content"=>'http://hzaic-gg.oss-cn-hangzhou.aliyuncs.com/eab86a320bdb644ca1e990d5d0c0483a.png']
            ]
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号居中标题",
            "text" => '八月份广告监测通报'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体二号居中标题",
            "text" => '省属媒体'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号标题",
            "text" => '一、广告监测概况'
        ];

        $all_illegal_count_rate = round(($all_illegal_count/$all_count)*100,2);
        $all_illegal_times_rate = round(($all_illegal_times/$all_times)*100,2);
        $all_illegal_play_len_rate = round(($all_illegal_play_len/$all_play_len)*100,2);
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号正文",
            "text" => $date_ymd.'至'.$date_md.'安徽广告监测中心共抽查监测我省省属媒体'.$ct_media_count.'家，其中报纸类媒体'.$tp_media_count.'家，电视类媒体'.$tv_media_count.'家，广播类媒体'.$tb_media_count.'家。抽查监测省属媒体广告'.$all_count.'条，其中涉嫌违法广告'.$all_illegal_count.'条，条数违法率为'.$all_illegal_count_rate.'%；抽查监测省属媒体广告'.$all_times.'条次，其中涉嫌违法广告'.$all_illegal_times.'条次，条次违法率为'.$all_illegal_times_rate.'%；抽查监测省属电视、广播类媒体广告总时长为'.$all_play_len.'秒，其中涉嫌违法广告时长'.$all_illegal_play_len.'秒，时长违法率为'.$all_illegal_play_len_rate.'%。'
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号标题",
            "text" => '二、各类媒体广告发布与涉嫌违法情况'
        ];//（1）报纸类

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => '（1）报纸类'
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号正文",
            "text" => intval(date('m',$report_start_date)).'月份安徽广告监测中心对我省'.$tp_media_count.'家省级报纸媒体发布的广告进行了抽查监测，共监测各类广告'.$tp_count.'条，其中涉嫌违法广告'.$tp_illegal_count.'条，条数违法率为'.round(($tp_illegal_count/$tp_count)*100,2).'%；监测各类广告'.$tp_times.'条次，其中涉嫌违法广告'.$tp_illegal_times.'条次，条次违法率为'.round(($tp_illegal_times/$tp_times)*100,2).'%。'
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号居中",
            "text" => '省级报纸涉嫌违法广告发布情况'
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "涉嫌违法广告发布情况不带时长",
            "data" => $every_tp_media_data
        ];


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => '（2）电视类'
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号正文",
            "text" => intval(date('m',$report_start_date)).'月份安徽广告监测中心对我省'.$tv_media_count.'家省级报纸媒体发布的广告进行了抽查监测，共监测各类广告'.$tv_count.'条，其中涉嫌违法广告'.$tv_illegal_count.'条，条数违法率为'.round(($tv_illegal_count/$tv_count)*100,2).'%；监测各类广告'.$tv_times.'条次，其中涉嫌违法广告'.$tv_illegal_times.'条次，条次违法率为'.round(($tv_illegal_times/$tv_times)*100,2).'%。'
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号居中",
            "text" => '省级电视涉嫌违法广告发布情况'
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "涉嫌违法广告发布情况不带时长",
            "data" => $every_tv_media_data
        ];


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => '（3）广播类'
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号正文",
            "text" => intval(date('m',$report_start_date)).'月份安徽广告监测中心对我省'.$tb_media_count.'家省级报纸媒体发布的广告进行了抽查监测，共监测各类广告'.$tb_count.'条，其中涉嫌违法广告'.$tb_illegal_count.'条，条数违法率为'.round(($tb_illegal_count/$tb_count)*100,2).'%；监测各类广告'.$tb_times.'条次，其中涉嫌违法广告'.$tb_illegal_times.'条次，条次违法率为'.round(($tb_illegal_times/$tb_times)*100,2).'%。'
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号居中",
            "text" => '省级广播涉嫌违法广告发布情况'
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "涉嫌违法广告发布情况带时长",
            "data" => $every_tb_media_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号标题",
            "text" => '三、广告监测动态'
        ];

        $all_illegal_count_rate = round(($all_illegal_count/$all_count)*100,2);
        $all_illegal_times_rate = round(($all_illegal_times/$all_times)*100,2);
        $all_illegal_play_len_rate = round(($all_illegal_play_len/$all_play_len)*100,2);

        $last_illegal_count_rate = round(($last_all_illegal_count/$last_all_count)*100,2);
        $last_illegal_times_rate = round(($last_all_illegal_times/$last_all_times)*100,2);
        $last_illegal_play_len_rate = round(($last_all_illegal_play_len/$last_all_play_len)*100,2);

        $hb_illegal_count_rate = $all_illegal_count_rate - $last_illegal_count_rate;

        if($hb_illegal_count_rate == 0){
            $hb_illegal_count_rate = '无变化';
        }elseif($hb_illegal_count_rate > 0){
            $hb_illegal_count_rate = '上升'.$hb_illegal_count_rate.'个百分点';
        }elseif($hb_illegal_count_rate < 0){
            $hb_illegal_count_rate = '下降'.($hb_illegal_count_rate*-1).'个百分点';
        }

        $hb_illegal_times_rate = $all_illegal_times_rate - $last_illegal_times_rate;

        if($hb_illegal_times_rate == 0){
            $hb_illegal_times_rate = '无变化';
        }elseif($hb_illegal_times_rate > 0){
            $hb_illegal_times_rate = '上升'.$hb_illegal_times_rate.'个百分点';
        }elseif($hb_illegal_times_rate < 0){
            $hb_illegal_times_rate = '下降'.($hb_illegal_times_rate*-1).'个百分点';
        }

        $hb_illegal_play_len_rate = $all_illegal_play_len_rate - $last_illegal_play_len_rate;

        if($hb_illegal_play_len_rate == 0){
            $hb_illegal_play_len_rate = '无变化';
        }elseif($hb_illegal_play_len_rate > 0){
            $hb_illegal_play_len_rate = '上升'.$hb_illegal_play_len_rate.'个百分点';
        }elseif($hb_illegal_play_len_rate < 0){
            $hb_illegal_play_len_rate = '下降'.($hb_illegal_play_len_rate*-1).'个百分点';
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号正文",
            "text" => '从监测情况看，省属媒体'.intval(date('m',$report_start_date)).'月份涉嫌违法广告条数违法率'.$all_illegal_count_rate.'%,较'.(intval(date('m',$report_start_date)) - 1).'月份'.$hb_illegal_count_rate.'；条次违法率'.$all_illegal_times_rate.'%，较'.(intval(date('m',$report_start_date)) - 1).'月份'.$hb_illegal_times_rate.'；时长违法率'.$all_illegal_play_len_rate.'%，较'.(intval(date('m',$report_start_date)) - 1).'月'.$hb_illegal_play_len_rate.'。'
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "落款工商局",
            "text" => '安徽省工商行政管理局'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "落款时间",
            "text" => $date_end_ymd.'印'
        ];

        $report_data = json_encode($data);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            //将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].date('Y年m月',strtotime($month)).'月报';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 30;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }

    //互联网报告接口
    public function create_netad_OS(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $system_num = getconfig('system_num');

        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型

        $report_start_date = strtotime(I('s_time',date('Y-m-d')));//开始时间
        $report_end_date = strtotime(I('e_time',date('Y-m-d')))+86399;//结束时间
/*
        $report_start_date = strtotime('2018-10-01');//开始时间
        $report_end_date = strtotime('2018-10-31')+86399;//结束时间*/

        $user = session('regulatorpersonInfo');//获取用户信息
        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);//开始年月字符
        $date_ym = date('Y年m月',$report_start_date);//开始年月字符
        $owner_media_ids = get_owner_media_ids();
        $pc_illegal_situation = $StatisticalReport->net_illegal_situation_customize($user['regionid'],['net_platform',1],$report_start_date,$report_end_date);//pc违法情形列表
        /*
        * @各媒体监测总情况
        *
        **/
        $net_media_customize = $StatisticalReport->net_media_customize_sum($user['regionid'],false,$report_start_date,$report_end_date);//互联网监测总情况

        $net_pc_media_customize = $StatisticalReport->net_media_customize_sum($user['regionid'],['net_platform',1],$report_start_date,$report_end_date);//PC监测总情况

        $net_gzh_media_customize = $StatisticalReport->net_media_customize_sum($user['regionid'],['net_platform',9],$report_start_date,$report_end_date);//微信监测总情况
        $net_app_media_customize = $StatisticalReport->net_media_customize_sum($user['regionid'],['net_platform',2],$report_start_date,$report_end_date);//app监测总情况

        $ct_fad_times = 0;//定义传统媒体发布广告条
        $ct_fad_illegal_times = 0;//定义传统媒体发布违法广告条
        $every_media_data = [];//定义媒体发布监测情况数组

        $every_net_type_data = [
            ['PC门户网站',$net_pc_media_customize['total'],$net_pc_media_customize['wfggsl'],$net_pc_media_customize['ggslwfl'].'%'],
            ['微信公众号',$net_gzh_media_customize['total'],$net_gzh_media_customize['wfggsl'],$net_gzh_media_customize['ggslwfl'].'%'],
            ['移动APP',$net_app_media_customize['total'],$net_app_media_customize['wfggsl'],$net_app_media_customize['ggslwfl'].'%'],
            ['合计',$net_media_customize['total'],$net_media_customize['wfggsl'],$net_media_customize['ggslwfl'].'%']
        ];

        /*
        * @互联网各媒体监测总情况ggslyzwfl
        *
        **/
        $every_net_media_data = [];
        $every_net_media_name = [];
        $every_net_media_jczqk = $StatisticalReport->net_media_customize($user['regionid'],false,'tn.fmediaid',$report_start_date,$report_end_date);//互联网各媒体各媒体监测总情况
        foreach ($every_net_media_jczqk as $every_net_media_jczqk_key=>$every_net_media_jczqk_val){
            if($every_net_media_jczqk_val['wfggsl'] != 0){
                $every_net_media_name[] = $every_net_media_jczqk_val['dymc'];
            }
            $every_net_media_data[] = [
                strval($every_net_media_jczqk_key+1),
                $every_net_media_jczqk_val['dymc'],
                $every_net_media_jczqk_val['total'],
                $every_net_media_jczqk_val['wfggsl'],
                $every_net_media_jczqk_val['ggslwfl'].'%',
                $every_net_media_jczqk_val['yzwfggsl'],
                $every_net_media_jczqk_val['ggslyzwfl'].'%'
            ];
        }

        /*
        * @互联网各广告类别各媒体监测总情况
        *
        **/
        $every_ad_type_data = [];
        $every_ad_type_name = [];
        $every_ad_type_jczqk = $StatisticalReport->net_media_customize($user['regionid'],false,'ac.fcode',$report_start_date,$report_end_date);//互联网各广告类别各媒体监测总情况
        foreach ($every_ad_type_jczqk as $every_ad_type_jczqk_key=>$every_ad_type_jczqk_val){
            if($every_ad_type_jczqk_val['wfggsl'] != 0){
                $every_ad_type_name[] = $every_ad_type_jczqk_val['dymc'];
            }
            $every_ad_type_data[] = [
                strval($every_ad_type_jczqk_key+1),
                $every_ad_type_jczqk_val['dymc'],
                $every_ad_type_jczqk_val['total'],
                $every_ad_type_jczqk_val['wfggsl'],
                $every_ad_type_jczqk_val['ggslwfl'].'%',
                $every_ad_type_jczqk_val['yzwfggsl'],
                $every_ad_type_jczqk_val['ggslyzwfl'].'%'
            ];
        }


        /*
        * @PC各媒体监测总情况
        *
        **/
        $every_pc_media_data = [];
        $every_pc_media_name = [];
        $every_pc_media_id = [];
        $every_pc_media_jczqk = $StatisticalReport->net_media_customize($user['regionid'],['tns.net_platform',1],'tn.fmediaid',$report_start_date,$report_end_date);//pc媒体广告发布情况
        foreach ($every_pc_media_jczqk as $every_pc_media_jczqk_key=>$every_pc_media_jczqk_val){
            if($every_pc_media_jczqk_val['wfggsl'] != 0){
                $every_pc_media_name[] = $every_pc_media_jczqk_val['dymc'];
                $every_pc_media_id[$every_pc_media_jczqk_val['fmediaid']] = $every_pc_media_jczqk_val['dymc'];
            }
            $every_pc_media_data[] = [
                strval($every_pc_media_jczqk_key+1),
                $every_pc_media_jczqk_val['dymc'],
                $every_pc_media_jczqk_val['total'],
                $every_pc_media_jczqk_val['wfggsl'],
                $every_pc_media_jczqk_val['ggslwfl'].'%',
                $every_pc_media_jczqk_val['yzwfggsl'],
                $every_pc_media_jczqk_val['ggslyzwfl'].'%'
            ];
        }

        /*
        * @PC各广告类型监测总情况
        *
        **/
        $every_pc_adclass_data = [];
        $every_pc_adclass_name = [];
        $every_pc_adclass_jczqk = $StatisticalReport->net_media_customize($user['regionid'],['tns.net_platform',1],'ac.fcode',$report_start_date,$report_end_date);//pc媒体广告发布情况


        foreach ($every_pc_adclass_jczqk as $every_pc_adclass_jczqk_key=>$every_pc_adclass_jczqk_val){
            if($every_pc_adclass_jczqk_val['wfggsl'] != 0){
                $every_pc_adclass_name[] = $every_pc_adclass_jczqk_val['dymc'];
            }
            $every_pc_adclass_data[] = [
                strval($every_pc_adclass_jczqk_key+1),
                $every_pc_adclass_jczqk_val['dymc'],
                $every_pc_adclass_jczqk_val['total'],
                $every_pc_adclass_jczqk_val['wfggsl'],
                $every_pc_adclass_jczqk_val['ggslwfl'].'%',
                $every_pc_adclass_jczqk_val['yzwfggsl'],
                $every_pc_adclass_jczqk_val['ggslyzwfl'].'%'
            ];
        }
        /*
        * @微信各媒体监测总情况
        **/
        $every_gzh_media_data = [];
        $every_gzh_media_name = [];
        $every_gzh_media_id = [];
        $every_wx_media_jczqk = $StatisticalReport->net_media_customize($user['regionid'],['tns.source_type',4],'tn.fmediaid',$report_start_date,$report_end_date);//微信媒体广告发布情况
        foreach ($every_wx_media_jczqk as $every_wx_media_jczqk_key=>$every_wx_media_jczqk_val){
            if($every_wx_media_jczqk_val['wfggsl'] != 0){
                $every_gzh_media_name[] = $every_wx_media_jczqk_val['dymc'];
                $every_gzh_media_id[$every_wx_media_jczqk_val['fmediaid']] = $every_wx_media_jczqk_val['dymc'];
            }
            $every_gzh_media_data[] = [
                strval($every_wx_media_jczqk_key+1),
                $every_wx_media_jczqk_val['dymc'],
                $every_wx_media_jczqk_val['total'],
                $every_wx_media_jczqk_val['wfggsl'],
                $every_wx_media_jczqk_val['ggslwfl'].'%',
                $every_wx_media_jczqk_val['yzwfggsl'],
                $every_wx_media_jczqk_val['ggslyzwfl'].'%'
            ];
        }

        /*
        * @微信各广告类别监测总情况
        **/
        $every_gzh_adclass_data = [];
        $every_gzh_adclass_name = [];
        $every_gzh_adclass_jczqk = $StatisticalReport->net_media_customize($user['regionid'],['tns.source_type',4],'ac.fcode',$report_start_date,$report_end_date);//pc媒体广告发布情况
        foreach ($every_gzh_adclass_jczqk as $every_gzh_adclass_jczqk_key=>$every_gzh_adclass_jczqk_val){
            if($every_gzh_adclass_jczqk_val['wfggsl'] != 0){
                $every_gzh_adclass_name[] = $every_gzh_adclass_jczqk_val['dymc'];
            }
            $every_gzh_adclass_data[] = [
                strval($every_gzh_adclass_jczqk_key+1),
                $every_gzh_adclass_jczqk_val['dymc'],
                $every_gzh_adclass_jczqk_val['total'],
                $every_gzh_adclass_jczqk_val['wfggsl'],
                $every_gzh_adclass_jczqk_val['ggslwfl'].'%',
                $every_gzh_adclass_jczqk_val['yzwfggsl'],
                $every_gzh_adclass_jczqk_val['ggslyzwfl'].'%'
            ];
        }


        /*
        * @APP各媒体监测总情况
        **/
        $every_app_media_data = [];
        $every_app_media_name = [];
        $every_app_media_jczqk = $StatisticalReport->net_media_customize($user['regionid'],['tns.net_platform',2],'tn.fmediaid',$report_start_date,$report_end_date);//pc媒体广告发布情况
        foreach ($every_app_media_jczqk as $every_app_media_jczqk_key=>$every_app_media_jczqk_val){
            if($every_app_media_jczqk_val['wfggsl'] != 0){
                $every_gzh_media_name[] = $every_app_media_jczqk_val['dymc'];
                $every_app_media_id[] = $every_app_media_jczqk_val['fmediaid'];
            }
            $every_app_media_data[] = [
                strval($every_app_media_jczqk_key+1),
                $every_app_media_jczqk_val['dymc'],
                $every_app_media_jczqk_val['total'],
                $every_app_media_jczqk_val['wfggsl'],
                $every_app_media_jczqk_val['ggslwfl'].'%',
                $every_app_media_jczqk_val['yzwfggsl'],
                $every_app_media_jczqk_val['ggslyzwfl'].'%'
            ];
        }

        /*
        * @APP各广告类别监测总情况
        **/
        $every_app_adclass_data = [];
        $every_app_adclass_name = [];
        $every_app_adclass_jczqk = $StatisticalReport->net_media_customize($user['regionid'],['tns.net_platform',2],'ac.fcode',$report_start_date,$report_end_date);//pc媒体广告发布情况
        foreach ($every_app_media_jczqk as $every_app_adclass_jczqk_key=>$every_app_adclass_jczqk_val){
            if($every_app_adclass_jczqk_val['wfggsl'] != 0){
                $every_app_adclass_name[] = $every_app_adclass_jczqk_val['dymc'];
            }
            $every_app_adclass_data[] = [
                strval($every_app_adclass_jczqk_key+1),
                $every_app_adclass_jczqk_val['dymc'],
                $every_app_adclass_jczqk_val['total'],
                $every_app_adclass_jczqk_val['wfggsl'],
                $every_app_adclass_jczqk_val['ggslwfl'].'%',
                $every_app_adclass_jczqk_val['yzwfggsl'],
                $every_app_adclass_jczqk_val['ggslyzwfl'].'%'
            ];
        }

//定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "大标题",
            "text" => $user['regulatorpname']
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体二号",
            "text" => $date_ym
        ];

/*        $pc_media_count = $StatisticalReport->net_media_count($user['regionid'],'net_platform',1,$report_start_date,$report_end_date);//pc媒体数量

        $app_media_count = $StatisticalReport->net_media_count($user['regionid'],'net_platform',2,$report_start_date,$report_end_date);//移动App媒体数量

        $gzh_media_count = $StatisticalReport->net_media_count($user['regionid'],'source_type',4,$report_start_date,$report_end_date);//公众号数量*/


        $pc_media_count = $StatisticalReport->net_media_counts(substr($user['regionid'],0,2),'1301');//pc媒体数量

        $app_media_count = $StatisticalReport->net_media_counts(substr($user['regionid'],0,2),'1302');//移动App媒体数量

        $gzh_media_count = $StatisticalReport->net_media_counts(substr($user['regionid'],0,2),'1303');//公众号数量


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号",
            "text" => "本期共监测".$pc_media_count."家PC门户网站、".$gzh_media_count."个微信公众号及".$app_media_count."个APP移动客户端，全部类别广告".$net_media_customize['total']."条，其中违法广告".$net_media_customize['wfggsl']."条，违法率".$net_media_customize['ggslwfl']."%。"
        ];

        if(empty($every_ad_type_name)){
            $every_ad_type_name = '本期各广告类别暂未发现违法广告';
        }else{
            $every_ad_type_name = "违法广告集中在".implode('、',$every_ad_type_name);
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号",
            "text" => $every_ad_type_name
        ];
        if(empty($every_net_media_name)){
            $every_net_media_name = '本期各互联网媒体暂未发现违法广告';
        }else{
            $every_net_media_name = "违法广告集中在".implode('、',$every_net_media_name)."中发布。";
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号",
            "text" => $every_net_media_name
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号加粗居中",
            "text" => $user['regulatorpname']."广告监测中心"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号加粗居中",
            "text" => $date_end_ymd
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号标题",
            "text" => "一、监测总体情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "此次共监测".$pc_media_count."家主要PC门户网站、".$gzh_media_count."个微信公众号及".$app_media_count."个APP移动客户端。"
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "监测情况总条形图",
            "data" => [
                ['类别','总条','违法条','条违法率'],
                ['PC门户网站',$net_pc_media_customize['total'],$net_pc_media_customize['wfggsl'],$net_pc_media_customize['ggslwfl'].'%'],
                ['微信公众号',$net_gzh_media_customize['total'],$net_gzh_media_customize['wfggsl'],$net_gzh_media_customize['ggslwfl'].'%'],
                ['移动APP',$net_app_media_customize['total'],$net_app_media_customize['wfggsl'],$net_app_media_customize['ggslwfl'].'%']
            ]
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "监测情况总列表",
            "data" => $every_net_type_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号标题",
            "text" => "（一）PC门户网站发布情况"
        ];

        if($net_pc_media_customize['wfggsl'] != 0){
            $ybwf = '其中涉嫌违法广告'.$net_pc_media_customize['wfggsl'].'条，违法率'.$net_pc_media_customize['ggslwfl'].'%。';

            $pc_des = $ybwf;
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "PC门户网站全部类别广告".$net_pc_media_customize['total']."条，".$pc_des
            ];

        }else{
            $pc_des = '未发现涉嫌违法广告。';
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "PC门户网站全部类别广告".$net_pc_media_customize['total']."条，".$pc_des
            ];
        }
        $every_pc_media_list = $this->create_net_chart_data($every_pc_media_data);
        if(!empty($every_pc_media_list[1])){
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广告量条形图",
                "data" => $every_pc_media_list[1]
            ];
        }
        if(!empty($every_pc_media_list[0])){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "网站监测情况列表",
                "data" => $every_pc_media_list[0]
            ];
        }




        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号标题",
            "text" => "（二）微信公众号发布情况"
        ];

        if($net_gzh_media_customize['wfggsl'] != 0){
            $ybwf = '其中涉嫌违法广告'.$net_gzh_media_customize['wfggsl'].'条，违法率'.$net_gzh_media_customize['ggslwfl'].'%。';
            $gzh_des = $ybwf;
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "微信公众号全部类别广告".$net_gzh_media_customize['total']."条，".$gzh_des
            ];
        }else{
            $gzh_des = '未发现涉嫌违法广告。';
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "微信公众号全部类别广告".$net_gzh_media_customize['total']."条，".$gzh_des
            ];
        }
        $every_gzh_media_list = $this->create_net_chart_data($every_gzh_media_data);
        if(!empty($every_gzh_media_list[1])){
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广告量条形图",
                "data" => $every_gzh_media_list[1]
            ];
        }

        if(!empty($every_gzh_media_list[0])){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "公众号监测情况列表",
                "data" => $every_gzh_media_list[0]
            ];
        }




        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号标题",
            "text" => "（三）移动设备APP发布情况"
        ];
        if($net_app_media_customize['wfggsl'] != 0){
            $ybwf = '其中涉嫌违法广告'.$net_app_media_customize['wfggsl'].'条，违法率'.$net_app_media_customize['ggslwfl'].'%。';

            $gzh_des = $ybwf;
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "APP移动客户端全部类别广告".$net_app_media_customize['total']."条，".$gzh_des
            ];
        }else{
            $gzh_des = '未发现涉嫌违法广告。';
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "APP移动客户端全部类别广告".$net_app_media_customize['total']."条，".$gzh_des
            ];
        }
        $every_app_media_list = to_string($this->create_net_chart_data($every_app_media_data));
        if(!empty($every_app_media_list[1])){
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广告量条形图",
                "data" => $every_app_media_list[1]
            ];
        }
        if(!empty($every_app_media_list[0])){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "网站监测情况列表",
                "data" => $every_app_media_list[0]
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号标题",
            "text" => "二、主要类别发布情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号标题",
            "text" => "（一）PC门户网站类别广告发布情况"
        ];
        if(empty($every_pc_adclass_name)){
            $every_pc_adclass_name = '本次监测中PC门户网站各广告类别暂未发现涉嫌违法广告';
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => $every_pc_adclass_name
            ];
        }else{
            $every_pc_adclass_name = "本次监测中PC门户网站涉嫌违法广告类别主要集中在".implode('、',$every_pc_adclass_name)."。";
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => $every_pc_adclass_name
            ];
        }
        $every_pc_adclass_list = $this->create_net_chart_data($every_pc_adclass_data);
        if(!empty($every_pc_adclass_list[1])){
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广告量条形图",
                "data" => $every_pc_adclass_list[1]
            ];
        }
        if(!empty($every_pc_adclass_list[0])){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "广告类别监测情况列表",
                "data" => $every_pc_adclass_list[0]
            ];
        }




//微信
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号标题",
            "text" => "（二）微信公众号类别广告发布情况"
        ];
        if(empty($every_gzh_adclass_name)){
            $every_gzh_adclass_name = '本次监测中微信公众号各广告类别暂未发现涉嫌违法广告';
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => $every_gzh_adclass_name
            ];
        }else{
            $every_gzh_adclass_name = "本次监测中微信公众号涉嫌违法广告类别主要集中在".implode('、',$every_gzh_adclass_name)."。";
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => $every_gzh_adclass_name
            ];

            $every_ghz_adclass_list = $this->create_net_chart_data($every_gzh_adclass_data);
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广告量条形图",
                "data" => $every_ghz_adclass_list[1]
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "广告类别监测情况列表",
                "data" => $every_ghz_adclass_list[0]
            ];
        }



//移动APP
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号标题",
            "text" => "（三）移动APP类别广告发布情况"
        ];
        if(empty($every_app_adclass_name)){
            $every_app_adclass_name = '本次监测中移动APP各广告类别暂未发现涉嫌违法广告';
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => $every_app_adclass_name
            ];
        }else{
            $every_app_adclass_name = "本次监测中移动APP涉嫌违法广告类别主要集中在".implode('、',$every_app_adclass_name)."。";
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => $every_app_adclass_name
            ];
            $every_app_adclass_list = $this->create_net_chart_data($every_app_adclass_data);
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广告量条形图",
                "data" => $every_app_adclass_list[1]
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "广告类别监测情况列表",
                "data" => $every_app_adclass_list[0]
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号标题",
            "text" => "三、涉嫌违法广告发布情况"
        ];

        if($net_media_customize['wfggsl'] == 0){
            $wf_des = "本次监测中，没有发现违法广告";
        }else{
            $wf_des = "本次监测中，共发现涉嫌违法广告".$net_media_customize['wfggsl']."条，共监测发现".$net_media_customize['wfggsl']."条；其中,";
            if($net_pc_media_customize['wfggsl'] != 0){
                $wf_des .= "PC门户网站发现涉嫌违法广告".$net_pc_media_customize['wfggsl']."条，共监测发现".$net_pc_media_customize['total']."条；";
            }else{
                $wf_des .= "PC门户网站未发现涉嫌违法广告；";
            }
            if($net_app_media_customize['wfggsl'] != 0){
                $wf_des .= "移动APP发现涉嫌违法广告".$net_app_media_customize['wfggsl']."条，共监测发现".$net_app_media_customize['total']."条；";
            }else{
                $wf_des .= "移动APP未发现涉嫌违法广告；";
            }
            if($net_gzh_media_customize['wfggsl'] != 0){
                $wf_des .= "微信公众号发现涉嫌违法广告".$net_gzh_media_customize['wfggsl']."条，共监测发现".$net_gzh_media_customize['total']."条；";
            }else{
                $wf_des .= "微信公众号未发现涉嫌违法广告；";
            }
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $wf_des
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "违法广告量占比图",
            "data" => to_string([
                ['','涉嫌违法广告条'],
                ['PC门户网站',$net_pc_media_customize['wfggsl']],
                ['移动APP',$net_app_media_customize['wfggsl']],
                ['微信公众号',$net_gzh_media_customize['wfggsl']]
            ])
        ];
        if(!empty($every_pc_media_id)) {

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号标题",
                "text" => "（一）PC门户网站"
            ];
            foreach ($every_pc_media_id as $every_pc_media_id_key => $every_pc_media_id_val) {

                $every_pc_media = $StatisticalReport->net_media_customize_sum($user['regionid'], ['net_platform', 1], $report_start_date, $report_end_date, $every_pc_media_id_key);

                $every_pc_adclass = $StatisticalReport->net_media_customize($user['regionid'], ['tns.net_platform', 1], 'ac.fcode', $report_start_date, $report_end_date, $every_pc_media_id_key);

                $pc_illegal_situation = $StatisticalReport->net_illegal_situation_customize($user['regionid'], ['net_platform', 1], $report_start_date, $report_end_date, $every_pc_media_id_key);

                $class_name = [];
                $fbxs_type = [];
                $every_pc_adclass_wfdata = [];
                foreach ($every_pc_adclass as $every_pc_adclass_key => $every_pc_adclass_val) {
                    $class_name[] = $every_pc_adclass_val['dymc'];
                    if ($every_pc_adclass_val['ftype'] == 'text') {
                        $fbxs_type[] = '文章广告';
                        $ftype = '文章广告';
                    } else {
                        $fbxs_type[] = '图片链接广告';
                        $ftype = '图片链接广告';
                    }
                    $every_pc_adclass_wfdata[] = [
                        strval($every_pc_adclass_key + 1),
                        $every_pc_adclass_val['dymc'],
                        $ftype,
                        $every_pc_adclass_val['total'],
                        $every_pc_adclass_val['wfggsl'],
                        $every_pc_adclass_val['ggslwfl'] . '%'
                    ];

                }
                $pc_illegal_situation_data = [];
                foreach ($pc_illegal_situation as $pc_illegal_situation_key => $pc_illegal_situation_val) {
                    $img_pc = getimages(htmlspecialchars_decode($pc_illegal_situation_val['fillegalcontent']));
                    if($img_pc){
                        $fillegalcontent_pc = $img_pc;
                    }else{
                        $fillegalcontent_pc =  str_replace("&nbsp;","",strip_tags(htmlspecialchars_decode($pc_illegal_situation_val['fillegalcontent'])));
                    }
                    $pc_illegal_situation_data[] = [
                        strval($pc_illegal_situation_key + 1),
                        $pc_illegal_situation_val['fadclass'],
                        $fillegalcontent_pc
                    ];
                }

                $fbxs_type = array_unique($fbxs_type);
                $class_name = implode('、', $class_name);
                $fbxs_type = implode('、', $fbxs_type);

//TODO::0717

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体三号数字序号标题",
                    "text" => $every_pc_media_id_val
                ];

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => "本次监测" . $every_pc_media_id_val . "涉嫌违法发布广告" . $every_pc_media['wfggsl'] . "条，监测发现" . $every_pc_media['total'] . "条，主要发布类别" . $class_name . "，发布形式为" . $fbxs_type . "。"
                ];

                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "单媒体详情列表",
                    "data" => $every_pc_adclass_wfdata
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号居中",
                    "text" => "该网站广告主要违法情形"
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "单媒体违法情形列表",
                    "data" => $pc_illegal_situation_data
                ];
            }
        }

        if(!empty($every_gzh_media_id)){

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号标题",
                "text" => "（二）微信公众号"
            ];
            foreach ($every_gzh_media_id as $every_gzh_media_id_key => $every_gzh_media_id_val){

                $every_gzh_media = $StatisticalReport->net_media_customize_sum($user['regionid'],['net_platform',9],$report_start_date,$report_end_date,$every_gzh_media_id_key);

                $every_gzh_adclass = $StatisticalReport->net_media_customize($user['regionid'],['tns.net_platform',9],'ac.fcode',$report_start_date,$report_end_date,$every_gzh_media_id_key);

                $gzh_illegal_situation = $StatisticalReport->net_illegal_situation_customize($user['regionid'],['net_platform',9],$report_start_date,$report_end_date,$every_gzh_media_id_key);

                $class_name = [];
                $fbxs_type = [];
                $every_gzh_adclass_wfdata = [];
                foreach ($every_gzh_adclass as $every_gzh_adclass_key => $every_gzh_adclass_val){
                    $class_name[] = $every_gzh_adclass_val['dymc'];
                    if($every_gzh_adclass_val['ftype'] == 'text'){
                        $fbxs_type[] = '文章广告';
                        $ftype = '文章广告';
                    }else{
                        $fbxs_type[] = '图片链接广告';
                        $ftype = '图片链接广告';
                    }
                    $every_gzh_adclass_wfdata[] = [
                        strval($every_gzh_adclass_key+1),
                        $every_gzh_adclass_val['dymc'],
                        $ftype,
                        $every_gzh_adclass_val['total'],
                        $every_gzh_adclass_val['wfggsl'],
                        $every_gzh_adclass_val['ggslwfl'].'%'
                    ];

                }
                $gzh_illegal_situation_data = [];
                foreach ($gzh_illegal_situation as $gzh_illegal_situation_key => $gzh_illegal_situation_val){
                    $img_gzh = getimages(htmlspecialchars_decode($gzh_illegal_situation_val['fillegalcontent']));
                    if($img_gzh){
                        $fillegalcontent_gzh = $img_gzh;
                    }else{
                        $fillegalcontent_gzh =  str_replace("&nbsp;","",strip_tags(htmlspecialchars_decode($gzh_illegal_situation_val['fillegalcontent'])));
                    }
                    $gzh_illegal_situation_data[] = [
                        strval($gzh_illegal_situation_key+1),
                        $gzh_illegal_situation_val['fadclass'],
                        $fillegalcontent_gzh
                    ];
                }

                $fbxs_type = array_unique($fbxs_type);
                $class_name = implode('、',$class_name);
                $fbxs_type = implode('、',$fbxs_type);

//TODO::0717

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体三号数字序号标题",
                    "text" => $every_gzh_media_id_val
                ];

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => "本次监测【".$every_gzh_media_id_val."】涉嫌违法发布广告".$every_gzh_media['wfggsl']."条，监测发现".$every_gzh_media['total']."条，主要发布类别".$class_name."，发布形式为".$fbxs_type."。"
                ];

                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "单媒体详情列表",
                    "data" => $every_gzh_adclass_wfdata
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号居中",
                    "text" => "该公众号广告主要违法情形"
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "单媒体违法情形列表",
                    "data" => $gzh_illegal_situation_data
                ];
            }
        }

        if(!empty($every_app_media_id)) {

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号标题",
                "text" => "（三）移动APP"
            ];
//
            foreach ($every_app_media_id as $every_app_media_id_key => $every_app_media_id_val) {

                $every_app_media = $StatisticalReport->net_media_customize_sum($user['regionid'], ['net_platform', 1], $report_start_date, $report_end_date, $every_app_media_id_key);

                $every_app_adclass = $StatisticalReport->net_media_customize($user['regionid'], ['tns.net_platform', 1], 'ac.fcode', $report_start_date, $report_end_date, $every_app_media_id_key);

                $app_illegal_situation = $StatisticalReport->net_illegal_situation_customize($user['regionid'], ['net_platform', 1], $report_start_date, $report_end_date, $every_app_media_id_key);

                $class_name = [];
                $fbxs_type = [];
                $every_app_adclass_wfdata = [];
                foreach ($every_app_adclass as $every_app_adclass_key => $every_app_adclass_val) {
                    $class_name[] = $every_app_adclass_val['dymc'];
                    if ($every_app_adclass_val['ftype'] == 'text') {
                        $fbxs_type[] = '文章广告';
                        $ftype = '文章广告';
                    } else {
                        $fbxs_type[] = '图片链接广告';
                        $ftype = '图片链接广告';
                    }
                    $every_app_adclass_wfdata[] = [
                        strval($every_app_adclass_key + 1),
                        $every_app_adclass_val['dymc'],
                        $ftype,
                        $every_app_adclass_val['total'],
                        $every_app_adclass_val['wfggsl'],
                        $every_app_adclass_val['ggslwfl'] . '%'
                    ];

                }
                $app_illegal_situation_data = [];
                foreach ($app_illegal_situation as $app_illegal_situation_key => $app_illegal_situation_val) {

                    $app_illegal_situation_data[] = [
                        strval($app_illegal_situation_key + 1),
                        $app_illegal_situation_val['fadclass'],
                        $app_illegal_situation_val['fillegalcontent']
                    ];
                }

                $fbxs_type = array_unique($fbxs_type);
                $class_name = implode('、', $class_name);
                $fbxs_type = implode('、', $fbxs_type);

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体三号数字序号标题",
                    "text" => $every_app_media_id_val
                ];

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => "本次监测" . $every_app_media_id_val . "涉嫌违法发布广告" . $every_app_media['wfggsl'] . "条，监测发现" . $every_app_media['total'] . "条，主要发布类别" . $class_name . "，发布形式为" . $fbxs_type . "。"
                ];

                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "单媒体详情列表",
                    "data" => $every_app_adclass_wfdata
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号居中",
                    "text" => "该网站广告主要违法情形"
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "单媒体违法情形列表",
                    "data" => $app_illegal_situation_data
                ];
            }
        }//五、违法广告明细
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号标题",
            "text" => '五、违法广告明细'
        ];
        $pc_illegal_detail_data = $StatisticalReport->illegal_detail_list($report_start_date,$report_end_date,$user['regionid'],1);//pc
        if(!empty($pc_illegal_detail_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号标题",
                "text" => "（一）PC网站"
            ];
            foreach ($pc_illegal_detail_data as $illegal_detail_data_key => $illegal_detail_data_val){
                $img_pc = getimages(htmlspecialchars_decode($illegal_detail_data_val['fillegalcontent']));
                if($img_pc){
                    if(is_img_link($img_pc)){
                        $fillegalcontent_pc = ['index'=>12,'type'=>'image','content'=>$img_pc];
                    }else{
                        $fillegalcontent_pc = ['index'=>12,'type'=>'text','content'=>''];
                    }
                }else{
                    $fillegalcontent_pc = ['index'=>12,'type'=>'text','content'=>str_replace("&nbsp;","",strip_tags(htmlspecialchars_decode($illegal_detail_data_val['fillegalcontent'])))];
                }

                $illegal_detail_list = [
                    ['index'=>2,'type'=>'text','content'=>$illegal_detail_data_val['fmedianame']],
                    ['index'=>4,'type'=>'text','content'=>$illegal_detail_data_val['ffullname']],
                    ['index'=>6,'type'=>'text','content'=>$illegal_detail_data_val['fb_count'].'次'],
                    ['index'=>8,'type'=>'text','content'=>$illegal_detail_data_val['net_original_url']?$illegal_detail_data_val['net_original_url']:''],
                    ['index'=>10,'type'=>'text','content'=>$illegal_detail_data_val['net_target_url']?$illegal_detail_data_val['net_target_url']:''],
                    $fillegalcontent_pc,
                    ['index'=>14,'type'=>'text','content'=>$illegal_detail_data_val['thumb_url_true']?$illegal_detail_data_val['thumb_url_true']:'']
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体三号数字序号标题",
                    "text" => ($illegal_detail_data_key+1).'、'.$illegal_detail_data_val['fadname']
                ];
                $data['content'][] = [
                    "type" => "filltable",
                    "bookmark" => "填表数据南京",//{"index":2,"type":"text","content":"xm"},
                    "data" => $illegal_detail_list
                ];
            }

        }

        $app_illegal_detail_data = $StatisticalReport->illegal_detail_list($report_start_date,$report_end_date,$user['regionid'],2);//移动APP
        if(!empty($app_illegal_detail_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号标题",
                "text" => "（二）移动APP"
            ];
            foreach ($app_illegal_detail_data as $illegal_detail_data_key => $illegal_detail_data_val){
                $img_app = getimages(htmlspecialchars_decode($illegal_detail_data_val['fillegalcontent']));
                if($img_app){
                    if(is_img_link($img_app)){
                        $fillegalcontent_app = ['index'=>12,'type'=>'image','content'=>$img_app];
                    }else{
                        $fillegalcontent_app = ['index'=>12,'type'=>'text','content'=>''];
                    }
                }else{
                    $fillegalcontent_app = ['index'=>12,'type'=>'text','content'=>str_replace("&nbsp;","",strip_tags(htmlspecialchars_decode($illegal_detail_data_val['fillegalcontent'])))];
                }
                $illegal_detail_list = [
                    ['index'=>2,'type'=>'text','content'=>$illegal_detail_data_val['fmedianame']],
                    ['index'=>4,'type'=>'text','content'=>$illegal_detail_data_val['ffullname']],
                    ['index'=>6,'type'=>'text','content'=>$illegal_detail_data_val['fb_count'].'次'],
                    ['index'=>8,'type'=>'text','content'=>$illegal_detail_data_val['net_original_url']?$illegal_detail_data_val['net_original_url']:''],
                    ['index'=>10,'type'=>'text','content'=>$illegal_detail_data_val['net_target_url']?$illegal_detail_data_val['net_target_url']:''],
                    $fillegalcontent_app,
                    ['index'=>14,'type'=>'text','content'=>$illegal_detail_data_val['thumb_url_true']?$illegal_detail_data_val['thumb_url_true']:'']
                ];

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体三号数字序号标题",
                    "text" => ($illegal_detail_data_key+1).'、'.$illegal_detail_data_val['fadname']
                ];
                $data['content'][] = [
                    "type" => "filltable",
                    "bookmark" => "填表数据南京",
                    "data" => $illegal_detail_list
                ];
            }
        }

        $ott_illegal_detail_data = $StatisticalReport->illegal_detail_list($report_start_date,$report_end_date,$user['regionid'],4);//电视盒子
        if(!empty($ott_illegal_detail_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号标题",
                "text" => "（三）电视盒子"
            ];
            foreach ($ott_illegal_detail_data as $illegal_detail_data_key => $illegal_detail_data_val){
                $img_ott = getimages(htmlspecialchars_decode($illegal_detail_data_val['fillegalcontent']));
                if($img_ott){
                    if(is_img_link($img_ott)){
                        $fillegalcontent_ott = ['index'=>12,'type'=>'image','content'=>$img_ott];
                    }else{
                        $fillegalcontent_ott = ['index'=>12,'type'=>'text','content'=>''];
                    }
                }else{
                    $fillegalcontent_ott = ['index'=>12,'type'=>'text','content'=>str_replace("&nbsp;","",strip_tags(htmlspecialchars_decode($illegal_detail_data_val['fillegalcontent'])))];
                }
                $illegal_detail_list = [
                    ['index'=>2,'type'=>'text','content'=>$illegal_detail_data_val['fmedianame']],
                    ['index'=>4,'type'=>'text','content'=>$illegal_detail_data_val['ffullname']],
                    ['index'=>6,'type'=>'text','content'=>$illegal_detail_data_val['fb_count'].'次'],
                    ['index'=>8,'type'=>'text','content'=>$illegal_detail_data_val['net_original_url']?$illegal_detail_data_val['net_original_url']:''],
                    ['index'=>10,'type'=>'text','content'=>$illegal_detail_data_val['net_target_url']?$illegal_detail_data_val['net_target_url']:''],
                    $fillegalcontent_ott,
                    ['index'=>14,'type'=>'text','content'=>$illegal_detail_data_val['thumb_url_true']?$illegal_detail_data_val['thumb_url_true']:'']
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体三号数字序号标题",
                    "text" => ($illegal_detail_data_key+1).'、'.$illegal_detail_data_val['fadname']
                ];
                $data['content'][] = [
                    "type" => "filltable",
                    "bookmark" => "填表数据南京",//{"index":2,"type":"text","content":"xm"},
                    "data" => $illegal_detail_list
                ];
            }
        }

        $wx_illegal_detail_data = $StatisticalReport->illegal_detail_list($report_start_date,$report_end_date,$user['regionid'],9);//微信
        if(!empty($wx_illegal_detail_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号标题",
                "text" => "（四）微信公众号"
            ];
            foreach ($wx_illegal_detail_data as $illegal_detail_data_key => $illegal_detail_data_val){
                $img_gzh = getimages(htmlspecialchars_decode($illegal_detail_data_val['fillegalcontent']));
                if($img_gzh){
                    if(is_img_link($img_gzh)){
                        $fillegalcontent_gzh = ['index'=>12,'type'=>'image','content'=>$img_gzh];
                    }else{
                        $fillegalcontent_gzh = ['index'=>12,'type'=>'text','content'=>''];
                    }
                }else{
                    $fillegalcontent_gzh = ['index'=>12,'type'=>'text','content'=>str_replace("&nbsp;","",strip_tags(htmlspecialchars_decode($illegal_detail_data_val['fillegalcontent'])))];
                }
                $illegal_detail_list = [
                    ['index'=>2,'type'=>'text','content'=>$illegal_detail_data_val['fmedianame']],
                    ['index'=>4,'type'=>'text','content'=>$illegal_detail_data_val['ffullname']],
                    ['index'=>6,'type'=>'text','content'=>$illegal_detail_data_val['fb_count'].'次'],
                    ['index'=>8,'type'=>'text','content'=>$illegal_detail_data_val['net_original_url']?$illegal_detail_data_val['net_original_url']:''],
                    ['index'=>10,'type'=>'text','content'=>$illegal_detail_data_val['net_target_url']?$illegal_detail_data_val['net_target_url']:''],
                    $fillegalcontent_gzh,
                    ['index'=>14,'type'=>'text','content'=>$illegal_detail_data_val['thumb_url_true']?$illegal_detail_data_val['thumb_url_true']:'']
                ];

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体三号数字序号标题",
                    "text" => ($illegal_detail_data_key+1).'、'.$illegal_detail_data_val['fadname']
                ];
                $data['content'][] = [
                    "type" => "filltable",
                    "bookmark" => "填表数据南京",//{"index":2,"type":"text","content":"xm"},
                    "data" => $illegal_detail_list
                ];
            }
        }

        $report_data = json_encode($data);
        //echo $report_data;exit;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
//将生成记录保存
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $focus = I('focus',0);
            $pnname = $user['regulatorname'].date('Y年m月d日',$report_start_date).'至'.date('Y年m月d日',$report_end_date).'互联网报告';//$report_start_date.'至'.$report_end_date
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 40;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }
    }

//互联网报告接口
    public function create_netad(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $system_num = getconfig('system_num');

        $StatisticalReport1 = New StatisticalReportModel();//实例化数据统计模型
        $StatisticalReport = New StatisticalModel();//实例化数据统计模型

        $report_start_date = strtotime(I('s_time',date('Y-m-d')));//开始时间
        $report_end_date = strtotime(I('e_time',date('Y-m-d')))+86399;//结束时间

/*
        $report_start_date = strtotime('2019-08-01');//开始时间
        $report_end_date = strtotime('2019-08-31')+86399;//结束时间*/


        $user = session('regulatorpersonInfo');//获取用户信息
        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);//开始年月字符
        $date_ym = date('Y年m月',$report_start_date);//开始年月字符
        $owner_media_ids = get_owner_media_ids();
        //查找全部媒体，筒体全部互联网媒体数据
        $net_media_ids = $StatisticalReport->net_media_type_ids('13');
        $net_media_id_1 = $StatisticalReport->net_media_type_ids('1301');
        $net_media_id_2 = $StatisticalReport->net_media_type_ids('1302');
        $net_media_id_3 = $StatisticalReport->net_media_type_ids('1303');
        $net_media_id_5 = $StatisticalReport->net_media_type_ids('1305');
        $net_media_type_array = $StatisticalReport->get_media_type(13);//互联网媒体各大类

        $s_date = date('Y-m-d',$report_start_date);
        $e_date = date('Y-m-d',$report_end_date);



        /*
        * @各媒体监测总情况
        *
        **/


        $ct_fad_times = 0;//定义传统媒体发布广告条
        $ct_fad_illegal_times = 0;//定义传统媒体发布违法广告条
        $every_media_data = [];//定义媒体发布监测情况数组



        /*
        * @互联网各媒体监测总情况ggslyzwfl
        *
        **/
        $every_net_media_data = [];
        $every_net_media_name = [];
        $net_media_customize = $StatisticalReport->report_ad_monitor('fmediaid',$net_media_ids,$user['regionid'],$s_date,$e_date,'times_illegal_rate','13');//当前客户全部互联网广告数据统计

        foreach ($net_media_customize as $every_net_media_jczqk_key=>$every_net_media_jczqk_val){
            if(!empty($every_net_media_jczqk_val['tmedia_name'])){
                if($every_net_media_jczqk_val['fad_illegal_times'] != 0){
                    $every_net_media_name[] = $every_net_media_jczqk_val['tmedia_name'];
                }
                $every_net_media_data[] = [
                    strval($every_net_media_jczqk_key+1),
                    $every_net_media_jczqk_val['tmedia_name'],
                    $every_net_media_jczqk_val['fad_times'],
                    $every_net_media_jczqk_val['fad_illegal_times'],
                    $every_net_media_jczqk_val['times_illegal_rate'].'%'
                ];
            }

        }

        /*
        * @互联网各广告类别各媒体监测总情况
        *
        **/
        $every_ad_type_data = [];
        $every_ad_type_name = [];
        $net_class_customize = $StatisticalReport->report_ad_monitor('fad_class_code',$net_media_ids,$user['regionid'],$s_date,$e_date,'times_illegal_rate','13');//当前客户全部互联网广告数据统计


        foreach ($net_class_customize as $every_ad_type_jczqk_key=>$every_ad_type_jczqk_val){
            if(!empty($every_ad_type_jczqk_val['tadclass_name'])){
                if($every_ad_type_jczqk_val['fad_illegal_times'] != 0){
                    $every_ad_type_name[] = $every_ad_type_jczqk_val['tadclass_name'];
                }
                $every_ad_type_data[] = [
                    strval($every_ad_type_jczqk_key+1),
                    $every_ad_type_jczqk_val['tadclass_name'],
                    $every_ad_type_jczqk_val['fad_times'],
                    $every_ad_type_jczqk_val['fad_illegal_times'],
                    $every_ad_type_jczqk_val['times_illegal_rate'].'%'
                ];
            }

        }


        /*
        * @PC各媒体监测总情况
        *
        **/
        $every_pc_media_data = [];
        $every_pc_media_name = [];
        $every_pc_media_id = [];
        $net_pc_media_customize = $StatisticalReport->report_ad_monitor('fmediaid',$net_media_id_1,$user['regionid'],$s_date,$e_date,'times_illegal_rate','13');//PC网站
        foreach ($net_pc_media_customize as $every_pc_media_jczqk_key=>$every_pc_media_jczqk_val){
            if(!empty($every_pc_media_jczqk_val['tmedia_name'])){
                if($every_pc_media_jczqk_val['fad_illegal_times'] != 0){

                    $every_pc_media_name[] = $every_pc_media_jczqk_val['tmedia_name'];
                    $every_pc_media_id[$every_pc_media_jczqk_val['fmediaid']] = $every_pc_media_jczqk_val['tmedia_name'];

                }
                $every_pc_media_data[] = [
                    strval($every_pc_media_jczqk_key+1),
                    $every_pc_media_jczqk_val['tmedia_name'],
                    $every_pc_media_jczqk_val['fad_times'],
                    $every_pc_media_jczqk_val['fad_illegal_times'],
                    $every_pc_media_jczqk_val['times_illegal_rate'].'%'
                ];
            }

        }

        /*
        * @PC各广告类型监测总情况
        *
        **/
        $every_pc_adclass_data = [];
        $every_pc_adclass_name = [];
        $net_pc_class_customize = $StatisticalReport->report_ad_monitor('fad_class_code',$net_media_id_1,$user['regionid'],$s_date,$e_date,'times_illegal_rate','13');//PC网站

        foreach ($net_pc_class_customize as $every_pc_adclass_jczqk_key=>$every_pc_adclass_jczqk_val){
            if(!empty($every_pc_adclass_jczqk_val['tadclass_name'])){
                if($every_pc_adclass_jczqk_val['fad_illegal_times'] != 0){
                    $every_pc_adclass_name[] = $every_pc_adclass_jczqk_val['tadclass_name'];
                }
                $every_pc_adclass_data[] = [
                    strval($every_pc_adclass_jczqk_key+1),
                    $every_pc_adclass_jczqk_val['tadclass_name'],
                    $every_pc_adclass_jczqk_val['fad_times'],
                    $every_pc_adclass_jczqk_val['fad_illegal_times'],
                    $every_pc_adclass_jczqk_val['times_illegal_rate'].'%'
                ];
            }

        }
        /*
        * @微信各媒体监测总情况
        **/
        $every_gzh_media_data = [];
        $every_gzh_media_name = [];
        $every_gzh_media_id = [];

        $net_gzh_media_customize = $StatisticalReport->report_ad_monitor('fmediaid',$net_media_id_3,$user['regionid'],$s_date,$e_date,'times_illegal_rate','13');//微信公众号

        foreach ($net_gzh_media_customize as $every_wx_media_jczqk_key=>$every_wx_media_jczqk_val){
            if(!empty($every_wx_media_jczqk_val['tmedia_name'])){
                if($every_wx_media_jczqk_val['fad_illegal_times'] != 0){
                    $every_gzh_media_name[] = $every_wx_media_jczqk_val['tmedia_name'];
                    $every_gzh_media_id[$every_wx_media_jczqk_val['fmediaid']] = $every_wx_media_jczqk_val['tmedia_name'];
                }
                $every_gzh_media_data[] = [
                    strval($every_wx_media_jczqk_key+1),
                    $every_wx_media_jczqk_val['tmedia_name'],
                    $every_wx_media_jczqk_val['fad_times'],
                    $every_wx_media_jczqk_val['fad_illegal_times'],
                    $every_wx_media_jczqk_val['times_illegal_rate'].'%'
                ];
            }

        }

        /*
        * @微信各广告类别监测总情况
        **/
        $every_gzh_adclass_data = [];
        $every_gzh_adclass_name = [];
        $net_gzh_class_customize = $StatisticalReport->report_ad_monitor('fad_class_code',$net_media_id_3,$user['regionid'],$s_date,$e_date,'times_illegal_rate','13');//微信公众号

        foreach ($net_gzh_class_customize as $every_gzh_adclass_jczqk_key=>$every_gzh_adclass_jczqk_val){
            if(!empty($every_gzh_adclass_jczqk_val['tadclass_name'])){
                if($every_gzh_adclass_jczqk_val['fad_illegal_times'] != 0){
                    $every_gzh_adclass_name[] = $every_gzh_adclass_jczqk_val['tadclass_name'];
                }
                $every_gzh_adclass_data[] = [
                    strval($every_gzh_adclass_jczqk_key+1),
                    $every_gzh_adclass_jczqk_val['tadclass_name'],
                    $every_gzh_adclass_jczqk_val['fad_times'],
                    $every_gzh_adclass_jczqk_val['fad_illegal_times'],
                    $every_gzh_adclass_jczqk_val['times_illegal_rate'].'%'
                ];
            }

        }


        /*
        * @APP各媒体监测总情况
        **/
        $every_app_media_data = [];
        $every_app_media_name = [];

        $net_app_media_customize = $StatisticalReport->report_ad_monitor('fmediaid',$net_media_id_2,$user['regionid'],$s_date,$e_date,'times_illegal_rate','13');//移动APP

        foreach ($net_app_media_customize as $every_app_media_jczqk_key=>$every_app_media_jczqk_val){
            if(!empty($every_app_media_jczqk_val['tmedia_name'])){
                if($every_app_media_jczqk_val['fad_illegal_times'] != 0){
                    $every_gzh_media_name[] = $every_app_media_jczqk_val['tmedia_name'];
                    $every_app_media_id[$every_app_media_jczqk_val['fmediaid']] = $every_app_media_jczqk_val['tmedia_name'];
                }
                $every_app_media_data[] = [
                    strval($every_app_media_jczqk_key+1),
                    $every_app_media_jczqk_val['tmedia_name'],
                    $every_app_media_jczqk_val['fad_times'],
                    $every_app_media_jczqk_val['fad_illegal_times'],
                    $every_app_media_jczqk_val['times_illegal_rate'].'%'
                ];
            }

        }

        /*
        * @APP各广告类别监测总情况
        **/
        $every_app_adclass_data = [];
        $every_app_adclass_name = [];
        $net_app_class_customize = $StatisticalReport->report_ad_monitor('fad_class_code',$net_media_id_2,$user['regionid'],$s_date,$e_date,'times_illegal_rate','13');//移动APP
        foreach ($net_app_class_customize as $every_app_adclass_jczqk_key=>$every_app_adclass_jczqk_val){
            if(!empty($every_app_adclass_jczqk_val['tadclass_name'])){
                if($every_app_adclass_jczqk_val['fad_illegal_times'] != 0){
                    $every_app_adclass_name[] = $every_app_adclass_jczqk_val['tadclass_name'];
                }
                $every_app_adclass_data[] = [
                    strval($every_app_adclass_jczqk_key+1),
                    $every_app_adclass_jczqk_val['tadclass_name'],
                    $every_app_adclass_jczqk_val['fad_times'],
                    $every_app_adclass_jczqk_val['fad_illegal_times'],
                    $every_app_adclass_jczqk_val['times_illegal_rate'].'%'
                ];
            }

        }

//定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "大标题",
            "text" => $user['regulatorpname']
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体二号",
            "text" => $date_ym
        ];

        /*        $pc_media_count = $StatisticalReport->net_media_count($user['regionid'],'net_platform',1,$report_start_date,$report_end_date);//pc媒体数量

                $app_media_count = $StatisticalReport->net_media_count($user['regionid'],'net_platform',2,$report_start_date,$report_end_date);//移动App媒体数量

                $gzh_media_count = $StatisticalReport->net_media_count($user['regionid'],'source_type',4,$report_start_date,$report_end_date);//公众号数量*/


        $pc_media_count = $StatisticalReport->net_media_counts(substr($user['regionid'],0,2),'1301');//pc媒体数量

        $app_media_count = $StatisticalReport->net_media_counts(substr($user['regionid'],0,2),'1302');//移动App媒体数量

        $gzh_media_count = $StatisticalReport->net_media_counts(substr($user['regionid'],0,2),'1303');//公众号数量


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号",
            "text" => "本期共监测".$pc_media_count."家PC门户网站、".$gzh_media_count."个微信公众号及".$app_media_count."个APP移动客户端，全部类别广告".$net_media_customize[count($net_media_customize)-1]['fad_times']."条，其中违法广告".$net_media_customize[count($net_media_customize)-1]['fad_illegal_times']."条，违法率".$net_media_customize[count($net_media_customize)-1]['times_illegal_rate']."%。"
        ];

        if(empty($every_ad_type_name)){
            $every_ad_type_name = '本期各广告类别暂未发现违法广告';
        }else{
            $every_ad_type_name = "违法广告集中在".implode('、',$every_ad_type_name);
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号",
            "text" => $every_ad_type_name
        ];
        if(empty($every_net_media_name)){
            $every_net_media_name = '本期各互联网媒体暂未发现违法广告';
        }else{
            $every_net_media_name = "违法广告集中在".implode('、',$every_net_media_name)."中发布。";
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号",
            "text" => $every_net_media_name
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号加粗居中",
            "text" => $user['regulatorpname']."广告监测中心"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号加粗居中",
            "text" => $date_end_ymd
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号标题",
            "text" => "一、监测总体情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "此次共监测".$pc_media_count."家主要PC门户网站、".$gzh_media_count."个微信公众号及".$app_media_count."个APP移动客户端。"
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "监测情况总条形图",
            "data" => to_string([
                ['类别','总条数','违法条数','条违法率'],
                [
                    'PC门户网站',
                    $net_pc_media_customize[count($net_pc_media_customize)-1]['fad_times'] ? $net_pc_media_customize[count($net_pc_media_customize)-1]['fad_times'] : 0 ,
                    $net_pc_media_customize[count($net_pc_media_customize)-1]['fad_illegal_times'] ? $net_pc_media_customize[count($net_pc_media_customize)-1]['fad_illegal_times'] : 0,
                    $net_pc_media_customize[count($net_pc_media_customize)-1]['times_illegal_rate'] ? $net_pc_media_customize[count($net_pc_media_customize)-1]['times_illegal_rate'].'%' : '0.00%'
                ],
                [
                    '微信公众号',
                    $net_gzh_media_customize[count($net_gzh_media_customize)-1]['fad_times'] ? $net_gzh_media_customize[count($net_gzh_media_customize)-1]['fad_times'] : 0 ,
                    $net_gzh_media_customize[count($net_gzh_media_customize)-1]['fad_illegal_times'] ? $net_gzh_media_customize[count($net_gzh_media_customize)-1]['fad_illegal_times'] : 0,
                    $net_gzh_media_customize[count($net_gzh_media_customize)-1]['times_illegal_rate'] ? $net_gzh_media_customize[count($net_gzh_media_customize)-1]['times_illegal_rate'].'%' : '0.00%'
                ],
                [
                    '移动APP',
                    $net_app_media_customize[count($net_app_media_customize)-1]['fad_times'] ? $net_app_media_customize[count($net_app_media_customize)-1]['fad_times'] : 0 ,
                    $net_app_media_customize[count($net_app_media_customize)-1]['fad_illegal_times'] ? $net_app_media_customize[count($net_app_media_customize)-1]['fad_illegal_times'] : 0,
                    $net_app_media_customize[count($net_app_media_customize)-1]['times_illegal_rate'] ? $net_app_media_customize[count($net_app_media_customize)-1]['times_illegal_rate'].'%' : '0.00%'
                ]
            ])
        ];
        $every_net_type_data = [
            [
                'PC门户网站',
                $net_pc_media_customize[count($net_pc_media_customize)-1]['fad_times'] ? $net_pc_media_customize[count($net_pc_media_customize)-1]['fad_times'] : 0 ,
                $net_pc_media_customize[count($net_pc_media_customize)-1]['fad_illegal_times'] ? $net_pc_media_customize[count($net_pc_media_customize)-1]['fad_illegal_times'] : 0,
                $net_pc_media_customize[count($net_pc_media_customize)-1]['times_illegal_rate'] ? $net_pc_media_customize[count($net_pc_media_customize)-1]['times_illegal_rate'].'%' : '0.00%'
            ],
            [
                '微信公众号',
                $net_gzh_media_customize[count($net_gzh_media_customize)-1]['fad_times'] ? $net_gzh_media_customize[count($net_gzh_media_customize)-1]['fad_times'] : 0 ,
                $net_gzh_media_customize[count($net_gzh_media_customize)-1]['fad_illegal_times'] ? $net_gzh_media_customize[count($net_gzh_media_customize)-1]['fad_illegal_times'] : 0,
                $net_gzh_media_customize[count($net_gzh_media_customize)-1]['times_illegal_rate'] ? $net_gzh_media_customize[count($net_gzh_media_customize)-1]['times_illegal_rate'].'%' : '0.00%'
            ],
            [
                '移动APP',
                $net_app_media_customize[count($net_app_media_customize)-1]['fad_times'] ? $net_app_media_customize[count($net_app_media_customize)-1]['fad_times'] : 0 ,
                $net_app_media_customize[count($net_app_media_customize)-1]['fad_illegal_times'] ? $net_app_media_customize[count($net_app_media_customize)-1]['fad_illegal_times'] : 0,
                $net_app_media_customize[count($net_app_media_customize)-1]['times_illegal_rate'] ? $net_app_media_customize[count($net_app_media_customize)-1]['times_illegal_rate'].'%' : '0.00%'
            ],
            [
                '合计',
                $net_media_customize[count($net_media_customize)-1]['fad_times'] ? $net_media_customize[count($net_media_customize)-1]['fad_times'] : 0 ,
                $net_media_customize[count($net_media_customize)-1]['fad_illegal_times'] ? $net_media_customize[count($net_media_customize)-1]['fad_illegal_times'] : 0,
                $net_media_customize[count($net_media_customize)-1]['times_illegal_rate'] ? $net_media_customize[count($net_media_customize)-1]['times_illegal_rate'].'%' : '0.00%'
            ]
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "监测情况总列表",
            "data" => to_string($every_net_type_data)
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号标题",
            "text" => "（一）PC门户网站发布情况"
        ];

        if($net_pc_media_customize[count($net_pc_media_customize)-1]['fad_illegal_times'] != 0){
            $ybwf = '其中涉嫌违法广告'.$net_pc_media_customize[count($net_pc_media_customize)-1]['fad_illegal_times'].'条，违法率'.$net_pc_media_customize[count($net_pc_media_customize)-1]['times_illegal_rate'].'%。';

            $pc_des = $ybwf;
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "PC门户网站全部类别广告".$net_pc_media_customize[count($net_pc_media_customize)-1]['fad_times']."条，".$pc_des
            ];

        }else{
            $pc_des = '未发现涉嫌违法广告。';
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "PC门户网站全部类别广告".$net_pc_media_customize[count($net_pc_media_customize)-1]['fad_times']."条，".$pc_des
            ];
        }
        $every_pc_media_list = $this->create_net_chart_data($every_pc_media_data);
        if(!empty($every_pc_media_list[1])){
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广告量条形图",
                "data" => to_string($every_pc_media_list[1])
            ];
        }
        if(!empty($every_pc_media_list[0])){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "网站监测情况列表",
                "data" => to_string($every_pc_media_list[0])
            ];
        }




        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号标题",
            "text" => "（二）微信公众号发布情况"
        ];

        if($net_gzh_media_customize[count($net_gzh_media_customize)-1]['fad_illegal_times'] != 0){
            $ybwf = '其中涉嫌违法广告'.$net_gzh_media_customize[count($net_gzh_media_customize)-1]['fad_illegal_times'].'条，违法率'.$net_gzh_media_customize[count($net_gzh_media_customize)-1]['times_illegal_rate'].'%。';
            $gzh_des = $ybwf;
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "微信公众号全部类别广告".$net_gzh_media_customize[count($net_gzh_media_customize)-1]['fad_times']."条，".$gzh_des
            ];
        }else{
            $gzh_des = '未发现涉嫌违法广告。';
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "微信公众号全部类别广告".$net_gzh_media_customize[count($net_gzh_media_customize)-1]['fad_times']."条，".$gzh_des
            ];
        }
        $every_gzh_media_list = $this->create_net_chart_data($every_gzh_media_data);
        if(!empty($every_gzh_media_list[1])){
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广告量条形图",
                "data" => to_string($every_gzh_media_list[1])
            ];
        }

        if(!empty($every_gzh_media_list[0])){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "公众号监测情况列表",
                "data" => to_string($every_gzh_media_list[0])
            ];
        }





    if(!empty($net_app_media_customize[count($net_app_media_customize)-1]['fad_times'])){
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号标题",
            "text" => "（三）移动设备APP发布情况"
        ];
        if($net_app_media_customize[count($net_app_media_customize)-1]['fad_illegal_times'] != 0){
            $ybwf = '其中涉嫌违法广告'.$net_app_media_customize[count($net_app_media_customize)-1]['fad_illegal_times'].'条，违法率'.$net_app_media_customize[count($net_app_media_customize)-1]['times_illegal_rate'].'%。';

            $gzh_des = $ybwf;
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "APP移动客户端全部类别广告".$net_app_media_customize[count($net_app_media_customize)-1]['fad_times']."条，".$gzh_des
            ];
        }else{
            $gzh_des = '未发现涉嫌违法广告。';
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "APP移动客户端全部类别广告".$net_app_media_customize[count($net_app_media_customize)-1]['fad_times']."条，".$gzh_des
            ];
        }

        $every_app_media_list = $this->create_net_chart_data($every_app_media_data);
        if(!empty($every_app_media_list[1])){
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广告量条形图",
                "data" => to_string($every_app_media_list[1])
            ];
        }
        if(!empty($every_app_media_list[0])){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "网站监测情况列表",
                "data" => to_string($every_app_media_list[0])
            ];
        }
    }



        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号标题",
            "text" => "二、主要类别发布情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号标题",
            "text" => "（一）PC门户网站类别广告发布情况"
        ];
        if(empty($every_pc_adclass_name)){
            $every_pc_adclass_name = '本次监测中PC门户网站各广告类别暂未发现涉嫌违法广告';
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => $every_pc_adclass_name
            ];
        }else{
            $every_pc_adclass_name = "本次监测中PC门户网站涉嫌违法广告类别主要集中在".implode('、',$every_pc_adclass_name)."。";
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => $every_pc_adclass_name
            ];
        }
        $every_pc_adclass_list = $this->create_net_chart_data($every_pc_adclass_data);
        if(!empty($every_pc_adclass_list[1])){
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广告量条形图",
                "data" => to_string($every_pc_adclass_list[1])
            ];
        }
        if(!empty($every_pc_adclass_list[0])){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "广告类别监测情况列表",
                "data" => to_string($every_pc_adclass_list[0])
            ];
        }




//微信
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号标题",
            "text" => "（二）微信公众号类别广告发布情况"
        ];
        if(empty($every_gzh_adclass_name)){
            $every_gzh_adclass_name = '本次监测中微信公众号各广告类别暂未发现涉嫌违法广告';
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => $every_gzh_adclass_name
            ];
        }else{
            $every_gzh_adclass_name = "本次监测中微信公众号涉嫌违法广告类别主要集中在".implode('、',$every_gzh_adclass_name)."。";
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => $every_gzh_adclass_name
            ];

            $every_ghz_adclass_list = $this->create_net_chart_data($every_gzh_adclass_data);
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广告量条形图",
                "data" => to_string($every_ghz_adclass_list[1])
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "广告类别监测情况列表",
                "data" => to_string($every_ghz_adclass_list[0])
            ];
        }



//移动APP
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号标题",
            "text" => "（三）移动APP类别广告发布情况"
        ];
        if(empty($every_app_adclass_name)){
            $every_app_adclass_name = '本次监测中移动APP各广告类别暂未发现涉嫌违法广告';
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => $every_app_adclass_name
            ];
        }else{
            $every_app_adclass_name = "本次监测中移动APP涉嫌违法广告类别主要集中在".implode('、',$every_app_adclass_name)."。";
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => $every_app_adclass_name
            ];
            $every_app_adclass_list = $this->create_net_chart_data($every_app_adclass_data);
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广告量条形图",
                "data" => to_string($every_app_adclass_list[1])
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "广告类别监测情况列表",
                "data" => to_string($every_app_adclass_list[0])
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号标题",
            "text" => "三、涉嫌违法广告发布情况"
        ];

        if($net_media_customize[count($net_media_customize)-1]['fad_illegal_times'] == 0){
            $wf_des = "本次监测中，没有发现违法广告";
        }else{
            $wf_des = "本次监测中，共发现涉嫌违法广告".$net_media_customize[count($net_media_customize)-1]['fad_illegal_times']."条，共监测发现".$net_media_customize[count($net_media_customize)-1]['fad_illegal_times']."条；其中,";
            if($net_pc_media_customize[count($net_pc_media_customize)-1]['fad_illegal_times'] != 0){
                $wf_des .= "PC门户网站发现涉嫌违法广告".$net_pc_media_customize[count($net_pc_media_customize)-1]['fad_illegal_times']."条，共监测发现".$net_pc_media_customize[count($net_pc_media_customize)-1]['fad_times']."条；";
            }else{
                $wf_des .= "PC门户网站未发现涉嫌违法广告；";
            }
            if($net_app_media_customize[count($net_app_media_customize)-1]['fad_illegal_times'] != 0){
                $wf_des .= "移动APP发现涉嫌违法广告".$net_app_media_customize[count($net_app_media_customize)-1]['fad_illegal_times']."条，共监测发现".$net_app_media_customize[count($net_app_media_customize)-1]['fad_times']."条；";
            }else{
                $wf_des .= "移动APP未发现涉嫌违法广告；";
            }
            if($net_gzh_media_customize[count($net_gzh_media_customize)-1]['fad_illegal_times'] != 0){
                $wf_des .= "微信公众号发现涉嫌违法广告".$net_gzh_media_customize[count($net_gzh_media_customize)-1]['fad_illegal_times']."条，共监测发现".$net_gzh_media_customize[count($net_gzh_media_customize)-1]['fad_times']."条；";
            }else{
                $wf_des .= "微信公众号未发现涉嫌违法广告；";
            }
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $wf_des
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "违法广告量占比图",
            "data" => to_string([
                ['','涉嫌违法广告条'],
                ['PC门户网站',$net_pc_media_customize[count($net_pc_media_customize)-1]['fad_illegal_times']],
                ['移动APP',$net_app_media_customize[count($net_app_media_customize)-1]['fad_illegal_times']],
                ['微信公众号',$net_gzh_media_customize[count($net_gzh_media_customize)-1]['fad_illegal_times']]
            ])
        ];
        if(!empty($every_pc_media_id)) {

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号标题",
                "text" => "（一）PC门户网站"
            ];
            foreach ($every_pc_media_id as $every_pc_media_id_key => $every_pc_media_id_val) {

                $every_pc_media = $StatisticalReport->report_ad_monitor('fmediaid',[$every_pc_media_id_key],$user['regionid'],$s_date,$e_date);
                $every_pc_adclass = $StatisticalReport->report_ad_monitor('fad_class_code',[$every_pc_media_id_key],$user['regionid'],$s_date,$e_date);


                $pc_illegal_situation = $StatisticalReport->net_illegal_situation_customize_OS($user['regionid'], ['net_platform', 1], $report_start_date, $report_end_date, $every_pc_media_id_key);

                $class_name = [];
                $fbxs_type = [];
                $every_pc_adclass_wfdata = [];
                foreach ($every_pc_adclass as $every_pc_adclass_key => $every_pc_adclass_val) {
                    if(!empty($every_pc_adclass_val['tadclass_name'])){
                        $class_name[] = $every_pc_adclass_val['tadclass_name'];
                        $every_pc_adclass_wfdata[] = [
                            strval($every_pc_adclass_key + 1),
                            $every_pc_adclass_val['tadclass_name'],
                            $every_pc_adclass_val['fad_times'],
                            $every_pc_adclass_val['fad_illegal_times'],
                            $every_pc_adclass_val['times_illegal_rate'] . '%'
                        ];
                    }
                }
                $pc_illegal_situation_data = [];
                foreach ($pc_illegal_situation as $pc_illegal_situation_key => $pc_illegal_situation_val) {
                    $img_pc = getimages(htmlspecialchars_decode($pc_illegal_situation_val['fillegalcontent']));
                    if($img_pc){
                        $fillegalcontent_pc = $img_pc;
                    }else{
                        $fillegalcontent_pc =  str_replace("&nbsp;","",strip_tags(htmlspecialchars_decode($pc_illegal_situation_val['fillegalcontent'])));
                    }
                    $pc_illegal_situation_data[] = [
                        strval($pc_illegal_situation_key + 1),
                        $pc_illegal_situation_val['fadclass'],
                        $fillegalcontent_pc
                    ];
                }

                $fbxs_type = array_unique($fbxs_type);
                $class_name = implode('、', $class_name);
                $fbxs_type = implode('、', $fbxs_type);

//TODO::0717

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体三号数字序号标题",
                    "text" => $every_pc_media_id_val
                ];

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => "本次监测" . $every_pc_media_id_val . "涉嫌违法发布广告" . $every_pc_media[count($every_pc_media)-1]['fad_illegal_times'] . "条，监测发现" . $every_pc_media[count($every_pc_media)-1]['fad_times'] . "条，主要发布类别" . $class_name . "，发布形式为" . $fbxs_type . "。"
                ];

                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "单媒体详情列表去形式",
                    "data" => to_string($every_pc_adclass_wfdata)
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号居中",
                    "text" => "该网站广告主要违法情形"
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "单媒体违法情形列表",
                    "data" => to_string($pc_illegal_situation_data)
                ];
            }
        }

        if(!empty($every_gzh_media_id)){

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号标题",
                "text" => "（二）微信公众号"
            ];
            foreach ($every_gzh_media_id as $every_gzh_media_id_key => $every_gzh_media_id_val){

                $every_gzh_media = $StatisticalReport->report_ad_monitor('fmediaid',[$every_gzh_media_id_key],$user['regionid'],$s_date,$e_date);
                $every_gzh_adclass = $StatisticalReport->report_ad_monitor('fad_class_code',[$every_gzh_media_id_key],$user['regionid'],$s_date,$e_date);


                $gzh_illegal_situation = $StatisticalReport->net_illegal_situation_customize_OS($user['regionid'], ['net_platform', 9], $report_start_date, $report_end_date, $every_gzh_media_id_key);
                $class_name = [];
                $fbxs_type = [];
                $every_gzh_adclass_wfdata = [];
                foreach ($every_gzh_adclass as $every_gzh_adclass_key => $every_gzh_adclass_val){

                    if(!empty($every_gzh_adclass_val['tadclass_name'])){
                        $class_name[] = $every_gzh_adclass_val['tadclass_name'];
                        $every_gzh_adclass_wfdata[] = [
                            strval($every_gzh_adclass_key+1),
                            $every_gzh_adclass_val['tadclass_name'],
                            $every_gzh_adclass_val['fad_times'],
                            $every_gzh_adclass_val['fad_illegal_times'],
                            $every_gzh_adclass_val['times_illegal_rate'].'%'
                        ];
                    }
                }
                $gzh_illegal_situation_data = [];
                foreach ($gzh_illegal_situation as $gzh_illegal_situation_key => $gzh_illegal_situation_val){
                    $img_gzh = getimages(htmlspecialchars_decode($gzh_illegal_situation_val['fillegalcontent']));
                    if($img_gzh){
                        $fillegalcontent_gzh = $img_gzh;
                    }else{
                        $fillegalcontent_gzh =  str_replace("&nbsp;","",strip_tags(htmlspecialchars_decode($gzh_illegal_situation_val['fillegalcontent'])));
                    }
                    $gzh_illegal_situation_data[] = [
                        strval($gzh_illegal_situation_key+1),
                        $gzh_illegal_situation_val['fadclass'],
                        $fillegalcontent_gzh
                    ];
                }

                $fbxs_type = array_unique($fbxs_type);
                $class_name = implode('、',$class_name);
                $fbxs_type = implode('、',$fbxs_type);

//TODO::0717

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体三号数字序号标题",
                    "text" => $every_gzh_media_id_val
                ];

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => "本次监测【".$every_gzh_media_id_val."】涉嫌违法发布广告".$every_gzh_media[count($every_gzh_media)-1]['fad_illegal_times']."条，监测发现".$every_gzh_media[count($every_gzh_media)-1]['fad_times']."条，主要发布类别".$class_name."，发布形式为".$fbxs_type."。"
                ];

                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "单媒体详情列表去形式",
                    "data" => to_string($every_gzh_adclass_wfdata)
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号居中",
                    "text" => "该公众号广告主要违法情形"
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "单媒体违法情形列表",
                    "data" => to_string($gzh_illegal_situation_data)
                ];
            }
        }

        if(!empty($every_app_media_id)) {

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号标题",
                "text" => "（三）移动APP"
            ];
//
            foreach ($every_app_media_id as $every_app_media_id_key => $every_app_media_id_val) {

                $every_app_media = $StatisticalReport->report_ad_monitor('fmediaid',[$every_app_media_id_key],$user['regionid'],$s_date,$e_date);
                $every_app_adclass = $StatisticalReport->report_ad_monitor('fad_class_code',[$every_app_media_id_key],$user['regionid'],$s_date,$e_date);
                $app_illegal_situation = $StatisticalReport->net_illegal_situation_customize_OS($user['regionid'], ['net_platform', 2], $report_start_date, $report_end_date, $every_app_media_id_key);

                $class_name = [];
                $fbxs_type = [];
                $every_app_adclass_wfdata = [];
                foreach ($every_app_adclass as $every_app_adclass_key => $every_app_adclass_val) {

                    if(!empty($every_app_adclass_val['tadclass_name'])){
                        $class_name[] = $every_app_adclass_val['tadclass_name'];
                        $every_app_adclass_wfdata[] = [
                            strval($every_app_adclass_key + 1),
                            $every_app_adclass_val['tadclass_name'],
                            $every_app_adclass_val['fad_times'],
                            $every_app_adclass_val['fad_illegal_times'],
                            $every_app_adclass_val['times_illegal_rate'] . '%'
                         ];
                    }

                }
                $app_illegal_situation_data = [];
                foreach ($app_illegal_situation as $app_illegal_situation_key => $app_illegal_situation_val) {

                    $app_illegal_situation_data[] = [
                        strval($app_illegal_situation_key + 1),
                        $app_illegal_situation_val['fadclass'],
                        $app_illegal_situation_val['fillegalcontent']
                    ];
                }

                $fbxs_type = array_unique($fbxs_type);
                $class_name = implode('、', $class_name);
                $fbxs_type = implode('、', $fbxs_type);

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体三号数字序号标题",
                    "text" => $every_app_media_id_val
                ];

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => "本次监测" . $every_app_media_id_val . "涉嫌违法发布广告" . $every_app_media[count($every_app_media)-1]['fad_illegal_times'] . "条，监测发现" . $every_app_media[count($every_app_media)-1]['fad_times'] . "条，主要发布类别" . $class_name . "，发布形式为" . $fbxs_type . "。"
                ];

                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "单媒体详情列表去形式",
                    "data" => to_string($every_app_adclass_wfdata)
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号居中",
                    "text" => "该网站广告主要违法情形"
                ];
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "单媒体违法情形列表",
                    "data" => to_string($app_illegal_situation_data)
                ];
            }
        }//五、违法广告明细
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号标题",
            "text" => '五、违法广告明细'
        ];

        $pc_illegal_detail_data = $StatisticalReport->illegal_detail_list_OS($s_date,$e_date,$net_media_id_1);//pc
        if(!empty($pc_illegal_detail_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号标题",
                "text" => "（一）PC网站"
            ];
            foreach ($pc_illegal_detail_data as $illegal_detail_data_key => $illegal_detail_data_val){

                $img_pc = getimages(htmlspecialchars_decode($illegal_detail_data_val['fillegalcontent']));

                if($img_pc){
                    if(is_img_link($img_pc)){
                        $fillegalcontent_pc = ['index'=>12,'type'=>'image','content'=>$img_pc];
                    }else{
                        $fillegalcontent_pc = ['index'=>12,'type'=>'text','content'=>''];
                    }
                }else{
                    $fillegalcontent_pc = ['index'=>12,'type'=>'text','content'=>str_replace("&nbsp;","",strip_tags(htmlspecialchars_decode($illegal_detail_data_val['fillegalcontent'])))];
                }

                if(is_img_link($illegal_detail_data_val['net_snapshot'])){
                    $net_snapshot_pc = ['index'=>16,'type'=>'image','content'=>$illegal_detail_data_val['net_snapshot']];
                }else{
                    $net_snapshot_pc = ['index'=>16,'type'=>'text','content'=>$illegal_detail_data_val['net_snapshot']?$illegal_detail_data_val['net_snapshot']:''];
                }

                $illegal_detail_list = [
                    ['index'=>2,'type'=>'text','content'=>$illegal_detail_data_val['fmedianame']],
                    ['index'=>4,'type'=>'text','content'=>$illegal_detail_data_val['ffullname']],
                    ['index'=>6,'type'=>'text','content'=>$illegal_detail_data_val['fb_count'].'次'],
                    ['index'=>8,'type'=>'text','content'=>$illegal_detail_data_val['net_original_url']?$illegal_detail_data_val['net_original_url']:''],
                    ['index'=>10,'type'=>'text','content'=>$illegal_detail_data_val['net_target_url']?$illegal_detail_data_val['net_target_url']:''],
                    $fillegalcontent_pc,
                    ['index'=>14,'type'=>'text','content'=>$illegal_detail_data_val['fexpressions']?$illegal_detail_data_val['fexpressions']:''],
                    $net_snapshot_pc,
                    ['index'=>18,'type'=>'text','content'=>$illegal_detail_data_val['thumb_url_true']?$illegal_detail_data_val['thumb_url_true']:'']
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体三号数字序号标题",
                    "text" => ($illegal_detail_data_key+1).'、'.$illegal_detail_data_val['fadname']
                ];
                $data['content'][] = [
                    "type" => "filltable",
                    "bookmark" => "填表数据南京",//{"index":2,"type":"text","content":"xm"},
                    "data" => $illegal_detail_list
                ];
            }

        }

        $wx_illegal_detail_data = $StatisticalReport->illegal_detail_list_OS($s_date,$e_date,$net_media_id_3);//微信
        if(!empty($wx_illegal_detail_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号标题",
                "text" => "（二）微信公众号"
            ];
            foreach ($wx_illegal_detail_data as $illegal_detail_data_key => $illegal_detail_data_val){
                $img_gzh = getimages(htmlspecialchars_decode($illegal_detail_data_val['fillegalcontent']));
                if($img_gzh){
                    if(is_img_link($img_gzh)){
                        $fillegalcontent_gzh = ['index'=>12,'type'=>'image','content'=>$img_gzh];
                    }else{
                        $fillegalcontent_gzh = ['index'=>12,'type'=>'text','content'=>''];
                    }
                }else{
                    $fillegalcontent_gzh = ['index'=>12,'type'=>'text','content'=>str_replace("&nbsp;","",strip_tags(htmlspecialchars_decode($illegal_detail_data_val['fillegalcontent'])))];
                }

                if(is_img_link($illegal_detail_data_val['net_snapshot'])){
                    $net_snapshot_gzh = ['index'=>16,'type'=>'image','content'=>$illegal_detail_data_val['net_snapshot']];
                }else{
                    $net_snapshot_gzh = ['index'=>16,'type'=>'text','content'=>$illegal_detail_data_val['net_snapshot']?$illegal_detail_data_val['net_snapshot']:''];
                }

                $illegal_detail_list = [
                    ['index'=>2,'type'=>'text','content'=>$illegal_detail_data_val['fmedianame']],
                    ['index'=>4,'type'=>'text','content'=>$illegal_detail_data_val['ffullname']],
                    ['index'=>6,'type'=>'text','content'=>$illegal_detail_data_val['fb_count'].'次'],
                    ['index'=>8,'type'=>'text','content'=>$illegal_detail_data_val['net_original_url']?$illegal_detail_data_val['net_original_url']:''],
                    ['index'=>10,'type'=>'text','content'=>$illegal_detail_data_val['net_target_url'] && $illegal_detail_data_val['net_target_url'] != 'empty' ?$illegal_detail_data_val['net_target_url']:''],
                    $fillegalcontent_gzh,
                    ['index'=>14,'type'=>'text','content'=>$illegal_detail_data_val['fexpressions']?$illegal_detail_data_val['fexpressions']:''],
                    $net_snapshot_gzh,
                    ['index'=>18,'type'=>'text','content'=>$illegal_detail_data_val['thumb_url_true']?$illegal_detail_data_val['thumb_url_true']:'']
                ];

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体三号数字序号标题",
                    "text" => ($illegal_detail_data_key+1).'、'.$illegal_detail_data_val['fadname']
                ];
                $data['content'][] = [
                    "type" => "filltable",
                    "bookmark" => "填表数据南京",//{"index":2,"type":"text","content":"xm"},
                    "data" => $illegal_detail_list
                ];
            }
        }

        $app_illegal_detail_data = $StatisticalReport->illegal_detail_list_OS($s_date,$e_date,$net_media_id_2);//移动APP
        if(!empty($app_illegal_detail_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号标题",
                "text" => "（三）移动APP"
            ];
            foreach ($app_illegal_detail_data as $illegal_detail_data_key => $illegal_detail_data_val){
                $img_app = getimages(htmlspecialchars_decode($illegal_detail_data_val['fillegalcontent']));
                if($img_app){
                    if(is_img_link($img_app)){
                        $fillegalcontent_app = ['index'=>12,'type'=>'image','content'=>$img_app];
                    }else{
                        $fillegalcontent_app = ['index'=>12,'type'=>'text','content'=>''];
                    }
                }else{
                    $fillegalcontent_app = ['index'=>12,'type'=>'text','content'=>str_replace("&nbsp;","",strip_tags(htmlspecialchars_decode($illegal_detail_data_val['fillegalcontent'])))];
                }

                if(is_img_link($illegal_detail_data_val['net_snapshot'])){
                    $net_snapshot_app = ['index'=>16,'type'=>'image','content'=>$illegal_detail_data_val['net_snapshot']];
                }else{
                    $net_snapshot_app = ['index'=>16,'type'=>'text','content'=>$illegal_detail_data_val['net_snapshot']?$illegal_detail_data_val['net_snapshot']:''];
                }

                $illegal_detail_list = [
                    ['index'=>2,'type'=>'text','content'=>$illegal_detail_data_val['fmedianame']],
                    ['index'=>4,'type'=>'text','content'=>$illegal_detail_data_val['ffullname']],
                    ['index'=>6,'type'=>'text','content'=>$illegal_detail_data_val['fb_count'].'次'],
                    ['index'=>8,'type'=>'text','content'=>$illegal_detail_data_val['net_original_url']?$illegal_detail_data_val['net_original_url']:''],
                    ['index'=>10,'type'=>'text','content'=>$illegal_detail_data_val['net_target_url']?$illegal_detail_data_val['net_target_url']:''],
                    $fillegalcontent_app,
                    ['index'=>14,'type'=>'text','content'=>$illegal_detail_data_val['fexpressions']?$illegal_detail_data_val['fexpressions']:''],
                    $net_snapshot_app,
                    ['index'=>18,'type'=>'text','content'=>$illegal_detail_data_val['thumb_url_true']?$illegal_detail_data_val['thumb_url_true']:'']
                ];

                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体三号数字序号标题",
                    "text" => ($illegal_detail_data_key+1).'、'.$illegal_detail_data_val['fadname']
                ];
                $data['content'][] = [
                    "type" => "filltable",
                    "bookmark" => "填表数据南京",
                    "data" => $illegal_detail_list
                ];
            }
        }

        $ott_illegal_detail_data = $StatisticalReport->illegal_detail_list_OS($s_date,$e_date,$net_media_id_5);//电视盒子
        if(!empty($ott_illegal_detail_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "宋体三号标题",
                "text" => "（四）电视盒子"
            ];
            foreach ($ott_illegal_detail_data as $illegal_detail_data_key => $illegal_detail_data_val){
                $img_ott = getimages(htmlspecialchars_decode($illegal_detail_data_val['fillegalcontent']));
                if($img_ott){
                    if(is_img_link($img_ott)){
                        $fillegalcontent_ott = ['index'=>12,'type'=>'image','content'=>$img_ott];
                    }else{
                        $fillegalcontent_ott = ['index'=>12,'type'=>'text','content'=>''];
                    }
                }else{
                    $fillegalcontent_ott = ['index'=>12,'type'=>'text','content'=>str_replace("&nbsp;","",strip_tags(htmlspecialchars_decode($illegal_detail_data_val['fillegalcontent'])))];
                }

                if(is_img_link($illegal_detail_data_val['net_snapshot'])){
                    $net_snapshot_ott = ['index'=>16,'type'=>'image','content'=>$illegal_detail_data_val['net_snapshot']];
                }else{
                    $net_snapshot_ott = ['index'=>16,'type'=>'text','content'=>$illegal_detail_data_val['net_snapshot']?$illegal_detail_data_val['net_snapshot']:''];
                }

                $illegal_detail_list = [
                    ['index'=>2,'type'=>'text','content'=>$illegal_detail_data_val['fmedianame']],
                    ['index'=>4,'type'=>'text','content'=>$illegal_detail_data_val['ffullname']],
                    ['index'=>6,'type'=>'text','content'=>$illegal_detail_data_val['fb_count'].'次'],
                    ['index'=>8,'type'=>'text','content'=>$illegal_detail_data_val['net_original_url']?$illegal_detail_data_val['net_original_url']:''],
                    ['index'=>10,'type'=>'text','content'=>$illegal_detail_data_val['net_target_url']?$illegal_detail_data_val['net_target_url']:''],
                    $fillegalcontent_ott,
                    ['index'=>14,'type'=>'text','content'=>$illegal_detail_data_val['fexpressions']?$illegal_detail_data_val['fexpressions']:''],
                    $net_snapshot_ott,
                    ['index'=>18,'type'=>'text','content'=>$illegal_detail_data_val['thumb_url_true']?$illegal_detail_data_val['thumb_url_true']:'']
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "宋体三号数字序号标题",
                    "text" => ($illegal_detail_data_key+1).'、'.$illegal_detail_data_val['fadname']
                ];
                $data['content'][] = [
                    "type" => "filltable",
                    "bookmark" => "填表数据南京",//{"index":2,"type":"text","content":"xm"},
                    "data" => $illegal_detail_list
                ];
            }
        }



        $report_data = json_encode($data);
        //echo $report_data;exit;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
//将生成记录保存
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $focus = I('focus',0);
            $pnname = $user['regulatorname'].date('Y年m月d日',$report_start_date).'至'.date('Y年m月d日',$report_end_date).'互联网报告';//$report_start_date.'至'.$report_end_date
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 40;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }
    }

    //根据区域统计的广告情况
    public function region_issue($region_id,$start_date,$end_date,$table_hz){

        if($region_id == 100000) $region_id = '000000';

        $startTime = date('Y-m-d H:i:s',strtotime($start_date));//开始时间
        $endTime = date('Y-m-d H:i:s',strtotime($end_date) + 86399);//结束时间

        $region_id_rtrim = rtrim($region_id,'00');//去掉地区后面的00
        $tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位

        //where条件
        $where = array();
        if($region_id == '340000'){
            $where['tmedia.fid'] = array('not in',C('ANHUI_MEDIA'));
            $where['tregion.fid'] = $region_id;
            $media_where['tmediaowner.fregionid'] = $region_id;
        }else{
            //$where['left(tregion.fid,'.$tregion_len.')'] = $region_id_rtrim;
            $where['tregion.fid|tregion.fpid'] = $region_id;
            $media_where['left(tmediaowner.fregionid,'.$tregion_len.')'] = $region_id_rtrim;
        }
/*        if($startTime && $endTime){
            $where['issue.fissuedate'] = array('between',$startTime.','.$endTime);//时间查询条件
        }*/

        //统计媒体数

        $media_count = M('tmedia')
            ->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
            ->where($media_where)->count();
        $tv_issue = M('ttvissue_'.$table_hz)
            ->alias('issue')
            ->field('   
                    tregion.fid,
                    tregion.fname as dymc,
                    count(1) as ztc,
                    count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as wftc,
                    count(case when ttvsample.fillegaltypecode > 20 then 1 else null end) as yzwftc
                                    ')
            ->join('ttvsample on ttvsample.fid = issue.fsampleid')
            ->join('tmedia on tmedia.fid = issue.fmediaid')
            ->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
            ->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
            // ->join('tregion as region on region.fid =  concat(left(tregion.fid,'.($tregion_len + 2).'),"'.str_pad('',6 - ($tregion_len + 2),'0').'")') //连接地区表（监管机构的下一级地区）
            ->where($where)
            ->group('tregion.fid')
            ->select();

        $tv_res = $this->count_illegality($tv_issue);//计算违法率
        $tv_mediatype = $tv_res['mediatype'];//电视统计情况
        $tv_mediatype['percent'] = round($tv_mediatype['illegal']/$tv_mediatype['total']*100,2).'％';
        $tv_mediatype['total'] = (string)$tv_mediatype['total'];
        $tv_mediatype['illegal'] = (string)$tv_mediatype['illegal'];
        $tv_mediatype = array_merge(array('mediaclass' => '电视'),$tv_mediatype);
        $tvissue = $tv_res['issue'];
        $tvissue = $this->region_sort($tvissue,'tcwfl',SORT_DESC);//根据违法率从高到低排序

        $bc_issue = M('tbcissue_'.$table_hz)
            ->alias('issue')
            ->field('   
                    tregion.fid,
                    tregion.fname as dymc,
                    count(1) as ztc,
                    count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as wftc,
                    count(case when tbcsample.fillegaltypecode > 20 then 1 else null end) as yzwftc
                    ')
            ->join('tbcsample on tbcsample.fid = issue.fsampleid')
            ->join('tmedia on tmedia.fid = issue.fmediaid')
            ->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
            ->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
            // ->join('tregion as region on region.fid =  concat(left(tregion.fid,'.($tregion_len + 2).'),"'.str_pad('',6 - ($tregion_len + 2),'0').'")') //连接地区表（监管机构的下一级地区）
            ->where($where)
            ->group('tregion.fid')
            ->select();
        $bc_res = $this->count_illegality($bc_issue);//计算违法率
        $bc_mediatype = $bc_res['mediatype'];//广播统计情况
        $bc_mediatype['percent'] = round($bc_mediatype['illegal']/$bc_mediatype['total']*100,2).'％';
        $bc_mediatype['total'] = (string)$bc_mediatype['total'];
        $bc_mediatype['illegal'] = (string)$bc_mediatype['illegal'];
        $bc_mediatype = array_merge(array('mediaclass' => '广播'),$bc_mediatype);
        $bcissue = $bc_res['issue'];
        $bcissue = $this->region_sort($bcissue,'tcwfl',SORT_DESC);//根据违法率从高到低排序

        $paper_issue = M('tpaperissue')
            ->alias('issue')
            ->field('   
                    tregion.fid,
                    tregion.fname as dymc,
                    count(1) as ztc,
                    count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as wftc,
                    count(case when tpapersample.fillegaltypecode > 20 then 1 else null end) as yzwftc
                    ')
            ->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
            ->join('tmedia on tmedia.fid = issue.fmediaid')
            ->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
            ->join('tregion on tregion.fid = tmediaowner.fregionid') //（媒介机构的详细地区）
            // ->join('tregion as region on region.fid =  concat(left(tregion.fid,'.($tregion_len + 2).'),"'.str_pad('',6 - ($tregion_len + 2),'0').'")') //连接地区表（监管机构的下一级地区）
            ->where($where)
            ->group('tregion.fid')
            ->select();
        $paper_res = $this->count_illegality($paper_issue);//计算违法率
        $paper_mediatype = $paper_res['mediatype'];//电视统计情况
        $paper_mediatype['percent'] = round($paper_mediatype['illegal']/$paper_mediatype['total']*100,2).'％';
        $paper_mediatype['total'] = (string)$paper_mediatype['total'];
        $paper_mediatype['illegal'] = (string)$paper_mediatype['illegal'];
        $paper_mediatype = array_merge(array('mediaclass' => '报刊'),$paper_mediatype);
        $paperissue = $paper_res['issue'];
        $paperissue = $this->region_sort($paperissue,'tcwfl',SORT_DESC);//根据违法率从高到低排序

        $issue = array_merge($tv_issue,$bc_issue,$paper_issue);
        foreach ($issue as $key => $value) {
            $issue_arr[$value['fid']]['dymc'] = $value['dymc'];
            $issue_arr[$value['fid']]['ztc'] += $value['ztc'];
            $issue_arr[$value['fid']]['wftc'] += $value['wftc'];
        }
        $res = $this->count_illegality($issue_arr,1);//计算违法率
        $mediatype = $res['mediatype'];//报纸统计情况
        $mediatype['percent'] = round($mediatype['illegal']/$mediatype['total']*100,2).'％';
        $mediatype['total'] = (string)$mediatype['total'];
        $mediatype['illegal'] = (string)$mediatype['illegal'];
        $mediatype = array_merge(array('mediaclass' => '合计'),$mediatype);
        if($res['issue']){
            $issue_arr = $this->region_sort($res['issue'],'tcwfl',SORT_DESC);//根据违法率从高到低排序
        }else{
            $issue_arr = array();
        }

        $data['issue'] = $issue_arr;

        $data['tv'] = $tvissue?$tvissue:array();//电视数据
        $data['bc'] = $bcissue?$bcissue:array();//广播数据
        $data['paper'] = $paperissue?$paperissue:array();//报纸数据

        $data['net'] = $netissue?$netissue:array();//互联网数据

        $data['mediatype'] = $mediatype;
        $data['tv_mediatype'] = $tv_mediatype;
        $data['bc_mediatype'] = $bc_mediatype;
        $data['paper_mediatype'] = $paper_mediatype;
        $data['media_count'] = $media_count;//媒介数量
        return $data;

    }

    public function count_adclass($region_id,$start_date,$end_date,$table_hz){

        if($region_id == 100000) $region_id = '000000';

        //$startTime = date('Y-m-d H:i:s',strtotime($start_date));//开始时间
        //$endTime = date('Y-m-d H:i:s',strtotime($end_date) + 86399);//结束时间

        $startTime = strtotime($start_date);//开始时间
        $endTime = strtotime($end_date) + 86399;//结束时间

        $region_id_rtrim = rtrim($region_id,'00');//去掉地区后面的00
        $tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位

        $where = array();
        // if($region_id == '340000'){
        //  $where['tmedia.fid'] = array('not in',C('ANHUI_MEDIA'));
        //  $where['tmediaowner.fregionid'] = $region_id;
        // }else{
        //  $where['left(tmediaowner.fregionid,'.$tregion_len.')'] = $region_id_rtrim;
        // }
        $where['tmediaowner.fregionid'] = $region_id;
        if($startTime && $endTime){
            $where['issue.fissuedate'] = array(array('egt',$startTime),array('lt',$endTime),'and');//时间查询条件
        }

        $tv_issue = M('ttvissue_'.$table_hz)
            ->alias('issue')
            ->field('   
                adclass.fadclass,
                adclass.fcode,
                count(1) as ztc,
                count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) as illegality,
                concat(cast(coalesce(round(count(case when ttvsample.fillegaltypecode > 0 then 1 else null end)/count(1),2),0)*100 as char),"%") as wfzb
                                    ')
            ->join('ttvsample on ttvsample.fid = issue.fsampleid')
            ->join('tad on tad.fadid = ttvsample.fadid and tad.fadid<>0')
            ->join('tadclass on tadclass.fcode = tad.fadclasscode')
            ->join('tadclass as adclass on adclass.fcode = left(tadclass.fcode,2)')
            ->join('tmedia on tmedia.fid = issue.fmediaid')
            ->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
            ->where($where)
            ->group('adclass.fcode')
            ->select();


        $bc_issue = M('tbcissue_'.$table_hz)
            ->alias('issue')
            ->field('
                    adclass.fadclass,
                    adclass.fcode,
                    count(1) as ztc,
                    concat(cast(coalesce(round(count(case when tbcsample.fillegaltypecode > 0 then 1 else null end)/count(1),2),0)*100 as char),"%") as wfzb,
                    count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) as illegality
                                    ')
            ->join('tbcsample on tbcsample.fid = issue.fsampleid')
            ->join('tad on tad.fadid = tbcsample.fadid and tad.fadid<>0')
            ->join('tadclass on tadclass.fcode = tad.fadclasscode')
            ->join('tadclass as adclass on adclass.fcode = left(tadclass.fcode,2)')
            ->join('tmedia on tmedia.fid = issue.fmediaid')
            ->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
            ->where($where)
            ->group('adclass.fcode')
            ->select();
        $paper_issue = M('tpaperissue')
            ->alias('issue')
            ->field('   
                    adclass.fadclass,
                    adclass.fcode,
                    count(1) as ztc,
                    concat(cast(coalesce(round(count(case when tpapersample.fillegaltypecode > 0 then 1 else null end)/count(1),2),0)*100 as char),"%") as wfzb,
                    count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) as illegality
                                    ')
            ->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
            ->join('tad on tad.fadid = tpapersample.fadid and tad.fadid<>0')
            ->join('tadclass on tadclass.fcode = tad.fadclasscode')
            ->join('tadclass as adclass on adclass.fcode = left(tadclass.fcode,2)')
            ->join('tmedia on tmedia.fid = issue.fmediaid')
            ->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
            ->where($where)
            ->group('adclass.fcode')
            ->select();
        header("Content-type:text/html;charset=utf-8");
        $issue = array_merge($tv_issue,$bc_issue,$paper_issue);
        foreach ($issue as $key => $value) {
            $issue_arr[$value['fcode']]['illegality'] += $value['illegality'];
            $issue_arr[$value['fcode']]['name'] = $value['fadclass'];
            $total += $value['illegality'];
        }
        foreach ($issue_arr as $key => $value) {
            $zb = round($value['illegality']/$total*100,2);
            if($zb < 0.01){
                $zb = '小于0.01';
            }
            $issue_arr[$key]['ratio'] = $zb.'％';
            $adclass[] = $issue_arr[$key];
        }
        $adclass = my_sort($adclass,'ratio',SORT_DESC);
        return $adclass;

    }

    public function getAdcase($regulator,$start_date,$end_date){

        $startTime = date('Y-m-d H:i:s',strtotime($start_date));//开始时间
        $endTime = date('Y-m-d H:i:s',strtotime($end_date) + 86399);//结束时间

        $where['tbn_illegal_ad.create_time'] = array('between',$startTime.','.$endTime);//时间查询条件
        $where['tbn_illegal_ad.fregion_id'] = $regulator;
        //dump($regulator);exit;
        $where['tbn_illegal_ad.fillegaltypecode'] = 30;

/*        $fsample = $Model->field('fid,fillegaltypecode,fillegalcontent,2 as mediatype')
            ->table('tbcsample')
            ->union('SELECT fpapersampleid as fid,fillegaltypecode,fillegalcontent,3 as mediatype FROM tpapersample')
            ->union('SELECT fid,fillegaltypecode,fillegalcontent,1 as mediatype FROM ttvsample')
            ->fetchSql(true)
            ->select();*/

        $adcaseList = false;/*M('tbn_illegal_ad')
            ->field('
                tregion.fname as dymc,
                count(1) as xspfl,
                count(case when tadcase.fstate = 3 then 1 else null end) as xsck,
                count(case when tadcase.fstate = 4 then 1 else null end) as xscl,
                count(case when tadcase.fstate = 4 and tadcase.fpentype = 10 then 1 else null end) as xstb,
                count(case when tadcase.fstate = 4 and tadcase.fpentype = 20 then 1 else null end) as xsla
                                        ')
            ->join($fsample.' as sample on sample.fid = tbn_illegal_ad.fsample_id')
            ->join('tbn_case_send on tbn_case_send.fillegal_ad_id = tbn_illegal_ad.fid')
            ->join('tregion on tregion.fid = fillegal_ad_id.fregion_id')
            ->where($where)
            ->where('tillegaladflow.fcreateregualtorid != tillegaladflow.fregulatorid')
            ->group('tillegaladflow.fregulatorid')
            ->select();*/
        if(!$adcaseList){
            $data['adcase'] = $adcaseList;
            $data['adcase_sum'] = 0;//派发量
            $data['adcase_check_sum'] = 0;//检查量
            $data['adcase_check_ratio'] = '0％';//检查率
            $data['adcase_xstb'] = 0;//停播量
            $data['adcase_xstb_ratio'] = '0％';//停播率
            $data['adcase_xsla'] = 0;//立案调查量
            $data['adcase_xsla_ratio'] = '0％';//立案调查率
            return $data;
        }
        foreach ($adcaseList as $key => $value) {
            $adcase_arr[$key]['dymc']  = $value['dymc'];
            $adcase_arr[$key]['xspfl'] = (string)$value['xspfl'];
            $adcase_arr[$key]['xsck']  = (string)$value['xsck'];
            $adcase_arr[$key]['xsckl'] = round($value['xsck']/$value['xspfl']*100,2).'％';
            $adcase_arr[$key]['xscl']  = (string)$value['xscl'];
            $adcase_arr[$key]['xscll'] = round($value['xscl']/$value['xspfl']*100,2).'％';
            $adcase_sum += $value['xspfl'];
            $adcase_check_sum += $value['xsck'];
            $adcase_xstb += $value['xstb'];
            $adcase_xsla += $value['xsla'];
        }
        $adcase = $this->region_sort($adcase_arr,'xscll',SORT_ASC);

        $data['adcase'] = $adcase;
        $data['adcase_sum'] = $adcase_sum;//派发量
        $data['adcase_check_sum'] = $adcase_check_sum;//检查量
        $data['adcase_check_ratio'] = round($adcase_check_sum/$adcase_sum*100,2).'％';//检查率
        $data['adcase_xstb'] = $adcase_xstb;//停播量
        $data['adcase_xstb_ratio'] = round($adcase_xstb/$adcase_sum*100,2).'％';//停播率
        $data['adcase_xsla'] = $adcase_xsla;//立案调查量
        $data['adcase_xsla_ratio'] = round($adcase_xsla/$adcase_sum*100,2).'％';//立案调查率
        return $data;

    }

    public function getRegionStr($array){

        foreach ($array as $key => $value) {
            $str .= $value['dymc'].'、';
            $region_count = $key + 1;
        }
        $str = mb_substr($str,0,-3);
        return array('str' => $str,'region_count' => $region_count);

    }

    public function region_sort($arrays,$sort_key,$sort_order=SORT_ASC,$sort_type=SORT_NUMERIC ){
        if(is_array($arrays)){
            foreach ($arrays as $array){
                if(is_array($array)){
                    $key_arrays[] = $array[$sort_key];
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }

        array_multisort($key_arrays,$sort_order,$sort_type,$arrays);

        $sum = 0;
        foreach($arrays as $array_arr) {
            $sum += 1;
            $array_array[] = array_merge(array('pm' => (string)$sum),$array_arr);
        }
        return $array_array;
    }
    //计算违法率
    public function count_illegality($array,$sort=0){

        $sum = 0;
        foreach ($array as $key => $value) {
            $mediatype['total'] += $value['ztc'];
            $mediatype['illegal'] += $value['wftc'];
            if($value['wftc'] == 0) continue;
            $issue[$key]['dymc'] = $value['dymc'];
            $issue[$key]['ztc'] = (string)$value['ztc'];
            $issue[$key]['wftc'] = (string)$value['wftc'];
            $zb = round($value['wftc']/$value['ztc']*100,2);
            if($zb < 0.01){
                $zb = '小于0.01';
            }
            $issue[$key]['tcwfl'] = $zb.'％';
        }
        if($mediatype['total'] == '') $mediatype['total'] = 0;
        if($mediatype['illegal'] == '') $mediatype['illegal'] = 0;
        if($sort == 1){
            foreach ($issue as $key => $value) {
                $issue_arr[] = $value;
            }
            return array('issue' => $issue_arr,'mediatype' => $mediatype);
        }else{
            return array('issue' => $issue,'mediatype' => $mediatype);
        }

    }

    //按类型
    public function source_type($usermedia){
        $where_net['issue.fmediaid'] = ['IN',$usermedia];
        foreach ([1=>'PC网站',2=>'APP',3=>'微信'] as $key=>$val){
            $where_net['issue.net_platform'] = $key;
            $net_issue_tm = M('tnetissueputlog')
                ->alias('issue')
                ->field('
                tnetissue.net_platform,
                tmedia.fid,
                 (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as dymc,
                count(1) as ztc,
                count(case when tnetissue.fillegaltypecode > 0 then 1 else null end) as wftc,
                count(case when tnetissue.fillegaltypecode > 20 then 1 else null end) as yzwftc
                ')
                ->join('tnetissue on tnetissue.major_key = issue.tid  and finputstate=2 ')
                ->join('tmedia on tmedia.fid = issue.fmediaid')
                ->where($where_net)
                ->group('issue.fmediaid')
                ->select();

            $net_res_tm = $this->count_illegality($net_issue_tm);//计算违法率
            $netissue = $net_res_tm['issue'];
            $netissue = $this->region_sort($netissue,'tcwfl',SORT_DESC);//根据违法率从高到低排序
            if($netissue == false){
                $data[$key] = [];
            }else{
                $data[$key] = $netissue;
            }
        }
        return $data;

    }

    public static function weekday($year,$week) {
        $year_start = mktime(0,0,0,1,1,$year);
        $year_end = mktime(0,0,0,12,31,$year);
        // 判断第一天是否为第一周的开始
        if (intval(date('W',$year_start))===1){
            $start = $year_start;//把第一天做为第一周的开始
        }else{
            $week++;
            $start = strtotime('+1 monday',$year_start);//把第一个周一作为开始
        }// 第几周的开始时间
        if ($week===1){
            $weekday['start'] = date('Y-m-d',$start);
        }else{
            $weekday['start'] = date('Y-m-d',strtotime('+'.($week-0).' monday',$start));
        }// 第几周的结束时间
        $weekday['end'] = date('Y-m-d',strtotime('+1 sunday',strtotime($weekday['start'])));
        if (date('Y',strtotime($weekday['end']))!=$year){
            $weekday['end'] = date('Y-m-d',$year_end);
        }
        return $weekday;
    }

    public function is_have_table($table_name){
        $isTable = M()->query('SHOW TABLES LIKE "'.$table_name.'"');
        if(empty($isTable)){
            return true;
        }else{
            return false;
        }
    }

    /*生成图表数据*/
    function create_chart_data($array,$index1 = 1,$index2 = 2,$index3 = 3,$index4 = 4){
        $array1[] = ["类型","发布量","违法发布量","违法率"];
        $array2[] = ["类型","发布量"];
        $array3[] = ["类型","违法发布量"];
        foreach ($array as $array_key=>$array_val){
            if($array_val[3] != 0){
                $array1[] = [
                    $array_val[$index1],
                    $array_val[$index2],
                    $array_val[$index3],
                    $array_val[$index4]
                ];
                $array2[] = [
                    $array_val[$index1],
                    $array_val[$index2]
                ];
                $array3[] = [
                    $array_val[$index1],
                    $array_val[$index3]
                ];
            }
        }
        return [$array1,$array2,$array3];
    }

    /*生成互联网图表数据*/
    function create_net_chart_data($array){
        if(empty($array)){
            return [[],[]];
        }
        $array_data2[] = ["网站","广告量"];

        foreach ($array as $array_key=>$array_val){
            $array_data1[] = [
                $array_val[0],
                $array_val[1],
                $array_val[2],
                $array_val[3],
                $array_val[4]
            ];
            $array_data2[] = [
                $array_val[1],
                $array_val[2]
            ];
        }
        return [$array_data1,$array_data2];
    }


    //周报
    public function create_week(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $system_num = getconfig('system_num');

        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型
        $s_time = I('s_time','2018-07-16');//开始日期
        $e_time = I('e_time','2018-07-21');//结束日期
        $month_s = date('m',strtotime($s_time));
        $month_e = date('m',strtotime($e_time));
        $week_num = intval(date('W',strtotime($s_time)));

        if($month_s != $month_e){
            $days_1 = date('t', strtotime($year."-1-1"));

            $s_time_e = substr($s_time,0,8).date('t', strtotime($s_time));//上月结束时间
            $e_time_s = substr($e_time,0,8).'01';//下月开始时间

            $cycle_arr = [
                [$s_time,$s_time_e],
                [$e_time_s,$e_time]
            ];
        }else{
            $cycle_arr = [
                [$s_time,$e_time]
            ];
        }
        $user = session('regulatorpersonInfo');//获取用户信息

        $tregion_count = M('tregion')->where(['fpid'=>$user['regionid']])->count();

        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级
        if($re_level == 2 || $re_level == 3 || $re_level == 4){
            $dq_name = '市级';
            $r_name = '全市';
            $xj_name = '区县';
        }elseif($re_level == 1){
            $dq_name = '全省';
            $xj_name = '市级';
        }else{
            $dq_name = '全'.$user['regionname1'];
            $xj_name = '地方';
        }

        $date_str = date('Y年m月d日',strtotime($s_time)).'至'.date('Y年m月d日',strtotime($e_time)).'(第'.$week_num.'周)';//开始年月字符

        //$days = round((strtotime($e_time) - strtotime($s_time) + 1)/86400);//计算天数
        $owner_media_ids = get_owner_media_ids();
        $ct_jczqk_quarter = [];
        $every_region_jczqk_quarter = [];
        $hz_tv_illegal_ad_data_quarter = [];
        $every_region_jczqk_quarter = [];//各地域监测总情况
        $every_ad_class_jczqk_quarter = [];//各广告类别监测总情况
        //本级电视
        $hz_tv_illegal_ad_data_quarter = [];
        //本级广播
        $hz_tb_illegal_ad_data_quarter = [];
        //本级报纸
        $hz_tp_illegal_ad_data_quarter = [];
        //下级电视
        $xj_tv_illegal_ad_data_quarter = [];
        //下级广播
        $xj_tb_illegal_ad_data_quarter = [];
        //下级报纸
        $xj_tp_illegal_ad_data_quarter = [];
        $hz_zdclass_tv_illegal_ad_data_quarter = [];
        $hz_zdclass_tb_illegal_ad_data_quarter = [];
        $hz_zdclass_tp_illegal_ad_data_quarter = [];
        $ct_media_tv_count_hz = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局电视媒体数量

        $ct_media_bc_count_hz = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局广播媒体数量

        $ct_media_paper_count_hz = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局报纸媒体数量

        $ct_media_tv_count_xj = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县电视媒体数量

        $ct_media_bc_count_xj = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县广播媒体数量

        $ct_media_paper_count_xj = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县报纸媒体数量


        $ct_media_count = $ct_media_tv_count_hz + $ct_media_bc_count_hz + $ct_media_paper_count_hz + $ct_media_tv_count_xj + $ct_media_bc_count_xj + $ct_media_paper_count_xj;//全部有数据的媒体数量
        foreach ($cycle_arr as $cycle_arr_val){
            $date_table = date('Ym',strtotime($cycle_arr_val[0])).'_'.substr($user['regionid'],0,2);//组合表

            if(!table_exist("ttvissue_".$date_table) || !table_exist("tbcissue_".$date_table)){
                continue;
            }
            $report_start_date = strtotime($cycle_arr_val[0]);
            $report_end_date = strtotime($cycle_arr_val[1])+86399;
            /*
            * @各媒体监测总情况
            *
            **/

            $ct_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_count_rate','fmediaclass');//传统媒体监测总情况


            /*
* @各地域监测总情况
*
**/
            $every_region_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times','fregionid');//各地域监测总情况


            /*
* @各广告类型监测总情况
*
**/
            $every_ad_class_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_times','fcode');//各地域监测总情况

            //本级电视
            $hz_tv_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',0);

            //本级广播
            $hz_tb_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',0);

            //本级报纸
            $hz_tp_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',0);

            //下级电视
            $xj_tv_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',2);

            //下级广播
            $xj_tb_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',2);

            //下级报纸
            $xj_tp_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',2);


            //四大类 药品、医疗服务、医疗器械、保健食品
            $hz_zdclass_tv_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,['01','02','13','06'],1);
            $hz_zdclass_tb_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,['01','02','13','06'],1);
            $hz_zdclass_tp_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,['01','02','13','06'],1);

        }

        $ct_jczqk = $this->create_data_hzlist($ct_jczqk_quarter,'fclass');
        $every_region_jczqk = $this->create_data_hzlist($every_region_jczqk_quarter,'fregionid');
        $every_ad_class_jczqk = $this->create_data_hzlist($every_ad_class_jczqk_quarter,'fcode');
        $hz_tv_illegal_ad_data = $this->create_data_hzlist2($hz_tv_illegal_ad_data_quarter,'fad_name');
        $hz_tb_illegal_ad_data = $this->create_data_hzlist2($hz_tb_illegal_ad_data_quarter,'fad_name');
        $hz_tp_illegal_ad_data = $this->create_data_hzlist2($hz_tp_illegal_ad_data_quarter,'fad_name');
        $xj_tv_illegal_ad_data = $this->create_data_hzlist2($xj_tv_illegal_ad_data_quarter,'fad_name');
        $xj_tb_illegal_ad_data = $this->create_data_hzlist2($xj_tb_illegal_ad_data_quarter,'fad_name');
        $xj_tp_illegal_ad_data = $this->create_data_hzlist2($xj_tp_illegal_ad_data_quarter,'fad_name');

        $hz_zdclass_tv_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tv_illegal_ad_data_quarter,'fad_name');
        $hz_zdclass_tb_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tb_illegal_ad_data_quarter,'fad_name');
        $hz_zdclass_tp_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tp_illegal_ad_data_quarter,'fad_name');

        //本级电视
        $hz_tv_illegal_ad_data = $this->get_table_data($hz_tv_illegal_ad_data);
        //本级广播
        $hz_tb_illegal_ad_data = $this->get_table_data($hz_tb_illegal_ad_data);
        //本级报纸
        $hz_tp_illegal_ad_data = $this->get_table_data($hz_tp_illegal_ad_data);
        //下级电视
        $xj_tv_illegal_ad_data = $this->get_table_data($xj_tv_illegal_ad_data);
        //下级广播
        $xj_tb_illegal_ad_data = $this->get_table_data($xj_tb_illegal_ad_data);
        //下级报纸
        $xj_tp_illegal_ad_data = $this->get_table_data($xj_tp_illegal_ad_data);

        /*
        * @各媒体监测总情况
        *
        **/
        $ct_fad_times = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times = 0;//定义传统媒体发布违法广告条次
        $every_media_class_data = [];//定义媒体发布监测情况数组
//循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk as $ct_jczqk_val){
            $ct_fad_times += $ct_jczqk_val['fad_times'];
            $ct_fad_illegal_times += $ct_jczqk_val['fad_illegal_times'];
            $ct_fad_count += $ct_jczqk_val['fad_count'];
            $ct_fad_illegal_count += $ct_jczqk_val['fad_illegal_count'];
            $ct_data_val = [
                $ct_jczqk_val['tmediaclass_name'],
                $ct_jczqk_val['fad_times'],
                $ct_jczqk_val['fad_count'],
                $ct_jczqk_val['fad_illegal_times'],
                $ct_jczqk_val['fad_illegal_count'],
                $ct_jczqk_val['fad_illegal_times_rate'].'%',
                $ct_jczqk_val['fad_illegal_count_rate'].'%'
            ];
            if($ct_jczqk_val['fmedia_class_code'] == '01'){
                $every_media_class_data[0] = $ct_data_val;
            }elseif($ct_jczqk_val['fmedia_class_code'] == '02'){
                $every_media_class_data[1] = $ct_data_val;
            }else{
                $every_media_class_data[2] = $ct_data_val;
            }
        }

        $every_media_class_data[3] = [
            '合计',
            strval($ct_fad_times),
            strval($ct_fad_count),
            strval($ct_fad_illegal_times),
            strval($ct_fad_illegal_count),
            round((($ct_fad_illegal_times)/($ct_fad_times))*100,2).'%',
            round((($ct_fad_illegal_count)/($ct_fad_count))*100,2).'%'
        ];//二、各类媒体广告发布情况
        $every_media_class_data = array_values($every_media_class_data);
        /*
* @各地域监测总情况
*
**/
        $every_region_data = [];//定义各地区发布监测情况数组
//循环赋值
        $every_region_illegal_times_data = [
            [''],
            ['']
        ];
        $every_region_jczqk = pxsf($every_region_jczqk,'fad_illegal_times');
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){
            $every_region_fad_times += $every_region_jczqk_val['fad_times'];
            $every_region_fad_illegal_times += $every_region_jczqk_val['fad_illegal_times'];
            $every_region_fad_count += $every_region_jczqk_val['fad_count'];
            $every_region_fad_illegal_count += $every_region_jczqk_val['fad_illegal_count'];
            if($every_region_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_region_illegal_times_data[0],$every_region_jczqk_val['tregion_name']);
                array_push($every_region_illegal_times_data[1],strval($every_region_jczqk_val['fad_illegal_times']));
            }

            $every_region_data[] = [
                strval($every_region_jczqk_key+1),
                $every_region_jczqk_val['tregion_name'],
                $every_region_jczqk_val['fad_times'],
                $every_region_jczqk_val['fad_count'],
                $every_region_jczqk_val['fad_illegal_times'],
                $every_region_jczqk_val['fad_illegal_count'],
                $every_region_jczqk_val['fad_illegal_times_rate'].'%',
                $every_region_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_region_data[] = [
            '',
            '合计',
            strval($every_region_fad_times),
            strval($every_region_fad_count),
            strval($every_region_fad_illegal_times),
            strval($every_region_fad_illegal_count),
            round((($every_region_fad_illegal_times)/($every_region_fad_times))*100,2).'%',
            round((($every_region_fad_illegal_count)/($every_region_fad_count))*100,2).'%'
        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）



        /*
* @各地域电视广播报纸监测总情况
*
**/
        $every_media_all_data = [];
        $every_media_illegal_times_all_data = [];
        foreach (['01','02','03'] as $every_media_val){
            $every_media_data = [];
            $every_media_illegal_times_data = [];
            $every_media_jczqk = [];
            //循环赋值
            $every_media_fad_times = 0;
            $every_media_fad_illegal_times = 0;
            $every_media_fad_count = 0;
            $every_media_fad_illegal_count = 0;
            $every_media_illegal_times_data = [
                [''],
                ['']
            ];
            $every_media_jczqk_quarter = [];
            foreach ($cycle_arr as $cycle_arr_val){
                $date_table = date('Ym',strtotime($cycle_arr_val[0])).'_'.substr($user['regionid'],0,2);//组合表
                if(!table_exist("ttvissue_".$date_table) || !table_exist("tbcissue_".$date_table)){
                    continue;
                }
                $report_start_date = strtotime($cycle_arr_val[0]);
                $report_end_date = strtotime($cycle_arr_val[1])+86399;
                $every_media_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,$every_media_val,'','fad_illegal_times','fmediaid');//各地域监测总情况

            }
            $every_media_jczqk = $this->create_data_hzlist($every_media_jczqk_quarter,'fid');//fad_illegal_times
            $every_media_jczqk = pxsf($every_media_jczqk,'fad_illegal_times');//重新排序
            foreach ($every_media_jczqk as $every_media_jczqk_key => $every_media_jczqk_val){
                $every_media_fad_times += $every_media_jczqk_val['fad_times'];
                $every_media_fad_illegal_times += $every_media_jczqk_val['fad_illegal_times'];
                $every_media_fad_count += $every_media_jczqk_val['fad_count'];
                $every_media_fad_illegal_count += $every_media_jczqk_val['fad_illegal_count'];

                if($every_media_jczqk_val['fad_illegal_times'] > 0){
                    array_push($every_media_illegal_times_data[0],$every_media_jczqk_val['tmedia_name']);
                    array_push($every_media_illegal_times_data[1],strval($every_media_jczqk_val['fad_illegal_times']));
                }

                $every_media_data[] = [
                    strval($every_media_jczqk_key+1),
                    $every_media_jczqk_val['tregion_name'],
                    $every_media_jczqk_val['tmedia_name'],
                    $every_media_jczqk_val['fad_times'],
                    $every_media_jczqk_val['fad_count'],
                    $every_media_jczqk_val['fad_illegal_times'],
                    $every_media_jczqk_val['fad_illegal_count'],
                    $every_media_jczqk_val['fad_illegal_times_rate'].'%',
                    $every_media_jczqk_val['fad_illegal_count_rate'].'%'
                ];
            }
            /*            $every_media_data[] = [
                            '',
                            '合计',
                            strval($every_media_fad_times),
                            strval($every_media_fad_count),
                            strval($every_media_fad_illegal_times),
                            strval($every_media_fad_illegal_count),
                            round((($every_media_fad_illegal_times)/($every_media_fad_times))*100,2).'%',
                            round((($every_media_fad_illegal_count)/($every_media_fad_count))*100,2).'%'
                        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）*/
            $every_media_all_data[] = $every_media_data;
            $every_media_illegal_times_all_data[] = $every_media_illegal_times_data;
        }


        $every_media_tv_data = $every_media_all_data[0];//电视各媒体
        $every_media_tb_data = $every_media_all_data[1];//广播各媒体
        $every_media_tp_data = $every_media_all_data[2];//报纸各媒体
        $every_media_illegal_times_tv_data = $every_media_illegal_times_all_data[0];//电视各媒体
        $every_media_illegal_times_tb_data = $every_media_illegal_times_all_data[1];//广播各媒体
        $every_media_illegal_times_tp_data =$every_media_illegal_times_all_data[2];//报纸各媒体





        /*
* @各广告类型监测总情况
*
**/

        $every_ad_class_data = [];//定义各地区发布监测情况数组
//循环赋值
        $every_ad_class_illegal_times_data = [
            [''],
            ['']
        ];
        $every_ad_class_jczqk = pxsf($every_ad_class_jczqk,'fad_illegal_times');//重新排序
        foreach ($every_ad_class_jczqk as $every_ad_class_jczqk_key => $every_ad_class_jczqk_val){
            $every_ad_class_fad_times += $every_ad_class_jczqk_val['fad_times'];
            $every_ad_class_fad_illegal_times += $every_ad_class_jczqk_val['fad_illegal_times'];
            $every_ad_class_fad_count += $every_ad_class_jczqk_val['fad_count'];
            $every_ad_class_fad_illegal_count += $every_ad_class_jczqk_val['fad_illegal_count'];
            if($every_ad_class_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_ad_class_illegal_times_data[0],$every_ad_class_jczqk_val['tadclass_name']);
                array_push($every_ad_class_illegal_times_data[1],strval($every_ad_class_jczqk_val['fad_illegal_times']));
            }

            $every_ad_class_data[] = [
                strval($every_ad_class_jczqk_key+1),
                $every_ad_class_jczqk_val['tadclass_name'],
                $every_ad_class_jczqk_val['fad_times'],
                $every_ad_class_jczqk_val['fad_count'],
                $every_ad_class_jczqk_val['fad_illegal_times'],
                $every_ad_class_jczqk_val['fad_illegal_count'],
                $every_ad_class_jczqk_val['fad_illegal_times_rate'].'%',
                $every_ad_class_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_ad_class_data[] = [
            '',
            '合计',
            strval($every_ad_class_fad_times),
            strval($every_ad_class_fad_count),
            strval($every_ad_class_fad_illegal_times),
            strval($every_ad_class_fad_illegal_count),
            round((($every_ad_class_fad_illegal_times)/($every_ad_class_fad_times))*100,2).'%',
            round((($every_ad_class_fad_illegal_count)/($every_ad_class_fad_count))*100,2).'%'
        ];


        $typical_illegal_ad_list_data = [];
        if(!empty($hz_zdclass_tv_illegal_ad_data) || !empty($hz_zdclass_tb_illegal_ad_data) || !empty($hz_zdclass_tp_illegal_ad_data)){
            $typical_illegal_ad_list = array_merge($hz_zdclass_tv_illegal_ad_data,$hz_zdclass_tb_illegal_ad_data,$hz_zdclass_tp_illegal_ad_data);
            $illegal_ad_list_data = [];
            $typical_illegal_ad_list = pxsf($typical_illegal_ad_list,'illegal_times');//排序
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_val){
                $md5 = MD5($typical_illegal_ad_list_val['fad_name'].$typical_illegal_ad_list_val['fmedianame']);
                if(isset($illegal_ad_list_data[$md5])){
                    $illegal_ad_list_data[$md5]['illegal_times'] += $typical_illegal_ad_list_val['illegal_times'];
                    $illegal_ad_list_data[$md5]['illegal_times'] = strval($illegal_ad_list_data[$md5]['illegal_times']);
                }else{

                    $illegal_ad_list_data[$md5] = $typical_illegal_ad_list_val;
                }
            }

            $typical_illegal_ad_list = array_values($illegal_ad_list_data);
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_key => $typical_illegal_ad_list_val){
                $typical_illegal_ad_list_data[] = [
                    strval($typical_illegal_ad_list_key+1),
                    $typical_illegal_ad_list_val['fmedianame'],
                    $typical_illegal_ad_list_val['fad_name'],
                    $typical_illegal_ad_list_val['fadclass'],
                    $typical_illegal_ad_list_val['illegal_times']
                ];
            }
        }



//定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "大标题楷体60",
            "text" => "广告监测周报"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号加粗",
            "text" => $user['regulatorpname'].""
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号加粗带红线",
            "text" => "广告监测中心               ".date('Y年m月d日')
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $user['regulatorpname']."广告监测中心对".$date_str.$r_name."部分电视、广播、报刊等".$ct_media_count."家媒体发布广告的情况进行了抽查监测，现将监测情况通报如下："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "一、监测范围及对象"
        ];
        if($ct_media_tv_count_hz != 0){
            $hz_media_count_str .= $ct_media_tv_count_hz."个电视媒体；";
        }
        if($ct_media_bc_count_hz != 0){
            $hz_media_count_str .= $ct_media_bc_count_hz."个广播媒体；";
        }
        if($ct_media_paper_count_hz != 0){
            $hz_media_count_str .= $ct_media_paper_count_hz."个报纸媒体；";
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）".$dq_name."媒体：".$hz_media_count_str
        ];
        if($ct_media_tv_count_xj != 0){
            $xj_media_count_str .= $ct_media_tv_count_xj."个电视媒体；";
        }
        if($ct_media_bc_count_xj != 0){
            $xj_media_count_str .= $ct_media_bc_count_xj."个广播媒体；";
        }
        if($ct_media_paper_count_xj != 0){
            $xj_media_count_str .= $ct_media_paper_count_xj."个报纸媒体；";
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）其他".$tregion_count."个区、县：".$xj_media_count_str
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "二、各类媒体广告发布情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "本周共监测广告".$every_media_class_data[3][1]."条次，".$every_media_class_data[3][2]."条数，其中监测发现各类涉嫌违法广告".$every_media_class_data[3][3]."条次，".$every_media_class_data[3][4]."条数。具体情况详见下表："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒介类型汇总表格hz",
            "data" => $every_media_class_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "三、市（各区县）媒体广告发布总体情况"
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "地域排名汇总表格hz",
            "data" => $every_region_data
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各市区县违法广告发布量排名条形图hz",
            "data" => $every_region_illegal_times_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "四、全市电视、广播、报纸广告发布情况（排名按发布违法广告条次由高到底排列）"
        ];

        if(!empty($every_media_tv_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "1、电视"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tv_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tv_data
            ];
        }
        if(!empty($every_media_tb_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "2、广播"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tb_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tb_data
            ];
        }
        if(!empty($every_media_tp_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "3、报纸"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tp_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tp_data
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、全市各类媒体发布的违法广告（全部类别）通报"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）市级媒体"
        ];


        if(!empty($hz_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tv_illegal_ad_data)
            ];
        }
        if(!empty($hz_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tb_illegal_ad_data)
            ];
        }
        if(!empty($hz_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tp_illegal_ad_data)
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）区（县）级媒体"
        ];

        if(!empty($xj_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tv_illegal_ad_data)
            ];
        }
        if(!empty($xj_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tb_illegal_ad_data)
            ];
        }

        if(!empty($xj_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tp_illegal_ad_data)
            ];
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "六、全市各类违法广告比重结构图"
        ];
        if(!empty($every_ad_class_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表hz",
                "data" => $every_ad_class_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "各大类别广告违法占比表hz",
                "data" => $every_ad_class_illegal_times_data
            ];

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "七、药品、医疗服务、医疗器械、保健食品违法广告发布情况"
        ];

        if(!empty($typical_illegal_ad_list_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $typical_illegal_ad_list_data
            ];
        }

        $report_data = json_encode($data);
        //echo $report_data;exit;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

//将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].$date_str.'报告';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 20;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',strtotime($s_time));
            $data['pnendtime'] 		    = date('Y-m-d',strtotime($e_time));
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }



    //季报
    public function create_quarter(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $system_num = getconfig('system_num');

        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型
        $quarter = I('quarter',3);//季度
        $year = I('year','2018');//季度
        $user = session('regulatorpersonInfo');//获取用户信息

        $tregion_count = M('tregion')->where(['fpid'=>$user['regionid']])->count();

        //匹配
        $cycle_arr = [];
        switch ($quarter){
            case 1:
                $days_1 = date('t', strtotime($year."-1-1"));
                $days_2 = date('t', strtotime($year."-2-1"));
                $days_3 = date('t', strtotime($year."-3-1"));
                $cycle_arr = [
                    [$year."-1-1",$year."-1-".$days_1],
                    [$year."-2-1",$year."-2-".$days_2],
                    [$year."-3-1",$year."-3-".$days_3]
                ];
                break;
            case 2:
                $days_4 = date('t', strtotime($year."-4-1"));
                $days_5 = date('t', strtotime($year."-5-1"));
                $days_6 = date('t', strtotime($year."-6-1"));
                $cycle_arr = [
                    [$year."-4-1",$year."-4-".$days_4],
                    [$year."-5-1",$year."-5-".$days_5],
                    [$year."-6-1",$year."-6-".$days_6]
                ];
                break;
            case 3:
                $days_7 = date('t', strtotime($year."-7-1"));
                $days_8 = date('t', strtotime($year."-8-1"));
                $days_9 = date('t', strtotime($year."-9-1"));
                $cycle_arr = [
                    [$year."-7-1",$year."-7-".$days_7],
                    [$year."-8-1",$year."-8-".$days_8],
                    [$year."-9-1",$year."-9-".$days_9]
                ];
                break;
            case 4:
                $days_10 = date('t', strtotime($year."-10-1"));
                $days_11 = date('t', strtotime($year."-11-1"));
                $days_12 = date('t', strtotime($year."-12-1"));
                $cycle_arr = [
                    [$year."-10-1",$year."-10-".$days_10],
                    [$year."-11-1",$year."-11-".$days_11],
                    [$year."-12-1",$year."-12-".$days_12]
                ];
                break;
        }

        switch ($quarter){
            case 1:
                break;
                $quarter_str = '一';
            case 2:
                $quarter_str = '二';
                break;
            case 3:
                $quarter_str = '三';
                break;
            case 4:
                $quarter_str = '四';
                break;
        }

        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级
        if($re_level == 2 || $re_level == 3 || $re_level == 4){
            $dq_name = '市级';
            $r_name = '全市';
            $xj_name = '区县';
        }elseif($re_level == 1){
            $dq_name = '全省';
            $xj_name = '市级';
        }else{
            $dq_name = '全'.$user['regionname1'];
            $xj_name = '地方';
        }

        $date_str = $year.'年第'.$quarter_str.'季度';//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数
        $owner_media_ids = get_owner_media_ids();
        $ct_jczqk_quarter = [];
        $every_region_jczqk_quarter = [];
        $hz_tv_illegal_ad_data_quarter = [];
        $every_region_jczqk_quarter = [];//各地域监测总情况
        $every_ad_class_jczqk_quarter = [];//各广告类别监测总情况
        //本级电视
        $hz_tv_illegal_ad_data_quarter = [];
        //本级广播
        $hz_tb_illegal_ad_data_quarter = [];
        //本级报纸
        $hz_tp_illegal_ad_data_quarter = [];
        //下级电视
        $xj_tv_illegal_ad_data_quarter = [];
        //下级广播
        $xj_tb_illegal_ad_data_quarter = [];
        //下级报纸
        $xj_tp_illegal_ad_data_quarter = [];
        $hz_zdclass_tv_illegal_ad_data_quarter = [];
        $hz_zdclass_tb_illegal_ad_data_quarter = [];
        $hz_zdclass_tp_illegal_ad_data_quarter = [];
        $ct_media_tv_count_hz = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局电视媒体数量

        $ct_media_bc_count_hz = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局广播媒体数量

        $ct_media_paper_count_hz = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局报纸媒体数量

        $ct_media_tv_count_xj = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县电视媒体数量

        $ct_media_bc_count_xj = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县广播媒体数量

        $ct_media_paper_count_xj = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县报纸媒体数量


        $ct_media_count = $ct_media_tv_count_hz + $ct_media_bc_count_hz + $ct_media_paper_count_hz + $ct_media_tv_count_xj + $ct_media_bc_count_xj + $ct_media_paper_count_xj;//全部有数据的媒体数量
        foreach ($cycle_arr as $cycle_arr_val){
            $date_table = date('Ym',strtotime($cycle_arr_val[0])).'_'.substr($user['regionid'],0,2);//组合表

            if(!table_exist("ttvissue_".$date_table) || !table_exist("tbcissue_".$date_table)){
                continue;
            }
            $report_start_date = strtotime($cycle_arr_val[0]);
            $report_end_date = strtotime($cycle_arr_val[1])+86399;
            /*
            * @各媒体监测总情况
            *
            **/

            $ct_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_count_rate','fmediaclass');//传统媒体监测总情况


                        /*
            * @各地域监测总情况
            *
            **/
            $every_region_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times','fregionid');//各地域监测总情况


            /*
            * @各广告类型监测总情况
            *
            **/
            $every_ad_class_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_times','fcode');//各地域监测总情况

            //本级电视
            $hz_tv_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',0);

            //本级广播
            $hz_tb_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',0);

            //本级报纸
            $hz_tp_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',0);

            //下级电视
            $xj_tv_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',2);

            //下级广播
            $xj_tb_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',2);

            //下级报纸
            $xj_tp_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',2);


            //四大类 药品、医疗服务、医疗器械、保健食品
            $hz_zdclass_tv_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,['01','02','13','06'],1);
            $hz_zdclass_tb_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,['01','02','13','06'],1);
            $hz_zdclass_tp_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,['01','02','13','06'],1);

        }



        $ct_jczqk = $this->create_data_hzlist($ct_jczqk_quarter,'fclass');
        $every_region_jczqk = $this->create_data_hzlist($every_region_jczqk_quarter,'fregionid');
        $every_ad_class_jczqk = $this->create_data_hzlist($every_ad_class_jczqk_quarter,'fcode');
        $hz_tv_illegal_ad_data = $this->create_data_hzlist2($hz_tv_illegal_ad_data_quarter,'fad_name');
        $hz_tb_illegal_ad_data = $this->create_data_hzlist2($hz_tb_illegal_ad_data_quarter,'fad_name');
        $hz_tp_illegal_ad_data = $this->create_data_hzlist2($hz_tp_illegal_ad_data_quarter,'fad_name');
        $xj_tv_illegal_ad_data = $this->create_data_hzlist2($xj_tv_illegal_ad_data_quarter,'fad_name');
        $xj_tb_illegal_ad_data = $this->create_data_hzlist2($xj_tb_illegal_ad_data_quarter,'fad_name');
        $xj_tp_illegal_ad_data = $this->create_data_hzlist2($xj_tp_illegal_ad_data_quarter,'fad_name');

        $hz_zdclass_tv_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tv_illegal_ad_data_quarter,'fad_name');
        $hz_zdclass_tb_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tb_illegal_ad_data_quarter,'fad_name');
        $hz_zdclass_tp_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tp_illegal_ad_data_quarter,'fad_name');

        //本级电视
        $hz_tv_illegal_ad_data = $this->get_table_data($hz_tv_illegal_ad_data);
        //本级广播
        $hz_tb_illegal_ad_data = $this->get_table_data($hz_tb_illegal_ad_data);
        //本级报纸
        $hz_tp_illegal_ad_data = $this->get_table_data($hz_tp_illegal_ad_data);
        //下级电视
        $xj_tv_illegal_ad_data = $this->get_table_data($xj_tv_illegal_ad_data);
        //下级广播
        $xj_tb_illegal_ad_data = $this->get_table_data($xj_tb_illegal_ad_data);
        //下级报纸
        $xj_tp_illegal_ad_data = $this->get_table_data($xj_tp_illegal_ad_data);

        /*
        * @各媒体监测总情况
        *
        **/
        $ct_fad_times = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times = 0;//定义传统媒体发布违法广告条次
        $every_media_class_data = [];//定义媒体发布监测情况数组
//循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk as $ct_jczqk_val){
            $ct_fad_times += $ct_jczqk_val['fad_times'];
            $ct_fad_illegal_times += $ct_jczqk_val['fad_illegal_times'];
            $ct_fad_count += $ct_jczqk_val['fad_count'];
            $ct_fad_illegal_count += $ct_jczqk_val['fad_illegal_count'];
            $ct_data_val = [
                $ct_jczqk_val['tmediaclass_name'],
                $ct_jczqk_val['fad_times'],
                $ct_jczqk_val['fad_count'],
                $ct_jczqk_val['fad_illegal_times'],
                $ct_jczqk_val['fad_illegal_count'],
                $ct_jczqk_val['fad_illegal_times_rate'].'%',
                $ct_jczqk_val['fad_illegal_count_rate'].'%'
            ];
            if($ct_jczqk_val['fmedia_class_code'] == '01'){
                $every_media_class_data[0] = $ct_data_val;
            }elseif($ct_jczqk_val['fmedia_class_code'] == '02'){
                $every_media_class_data[1] = $ct_data_val;
            }else{
                $every_media_class_data[2] = $ct_data_val;
            }
        }

        $every_media_class_data[3] = [
            '合计',
            strval($ct_fad_times),
            strval($ct_fad_count),
            strval($ct_fad_illegal_times),
            strval($ct_fad_illegal_count),
            round((($ct_fad_illegal_times)/($ct_fad_times))*100,2).'%',
            round((($ct_fad_illegal_count)/($ct_fad_count))*100,2).'%'
        ];//二、各类媒体广告发布情况
        $every_media_class_data = array_values($every_media_class_data);
        /*
* @各地域监测总情况
*
**/
        $every_region_data = [];//定义各地区发布监测情况数组
//循环赋值
        $every_region_illegal_times_data = [
            [''],
            ['']
        ];
        $every_region_jczqk = pxsf($every_region_jczqk,'fad_illegal_times');
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){
            $every_region_fad_times += $every_region_jczqk_val['fad_times'];
            $every_region_fad_illegal_times += $every_region_jczqk_val['fad_illegal_times'];
            $every_region_fad_count += $every_region_jczqk_val['fad_count'];
            $every_region_fad_illegal_count += $every_region_jczqk_val['fad_illegal_count'];
            if($every_region_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_region_illegal_times_data[0],$every_region_jczqk_val['tregion_name']);
                array_push($every_region_illegal_times_data[1],strval($every_region_jczqk_val['fad_illegal_times']));
            }

            $every_region_data[] = [
                strval($every_region_jczqk_key+1),
                $every_region_jczqk_val['tregion_name'],
                $every_region_jczqk_val['fad_times'],
                $every_region_jczqk_val['fad_count'],
                $every_region_jczqk_val['fad_illegal_times'],
                $every_region_jczqk_val['fad_illegal_count'],
                $every_region_jczqk_val['fad_illegal_times_rate'].'%',
                $every_region_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_region_data[] = [
            '',
            '合计',
            strval($every_region_fad_times),
            strval($every_region_fad_count),
            strval($every_region_fad_illegal_times),
            strval($every_region_fad_illegal_count),
            round((($every_region_fad_illegal_times)/($every_region_fad_times))*100,2).'%',
            round((($every_region_fad_illegal_count)/($every_region_fad_count))*100,2).'%'
        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）



        /*
* @各地域电视广播报纸监测总情况
*
**/
        $every_media_all_data = [];
        $every_media_illegal_times_all_data = [];
        foreach (['01','02','03'] as $every_media_val){
            $every_media_data = [];
            $every_media_illegal_times_data = [];
            $every_media_jczqk = [];
            //循环赋值
            $every_media_fad_times = 0;
            $every_media_fad_illegal_times = 0;
            $every_media_fad_count = 0;
            $every_media_fad_illegal_count = 0;
            $every_media_illegal_times_data = [
                [''],
                ['']
            ];
            $every_media_jczqk_quarter = [];
            foreach ($cycle_arr as $cycle_arr_val){
                $date_table = date('Ym',strtotime($cycle_arr_val[0])).'_'.substr($user['regionid'],0,2);//组合表
                if(!table_exist("ttvissue_".$date_table) || !table_exist("tbcissue_".$date_table)){
                    continue;
                }
                $report_start_date = strtotime($cycle_arr_val[0]);
                $report_end_date = strtotime($cycle_arr_val[1])+86399;
                $every_media_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,$every_media_val,'','fad_illegal_times','fmediaid');//各地域监测总情况

            }
            $every_media_jczqk = $this->create_data_hzlist($every_media_jczqk_quarter,'fid');//fad_illegal_times
            $every_media_jczqk = pxsf($every_media_jczqk,'fad_illegal_times');//重新排序
            foreach ($every_media_jczqk as $every_media_jczqk_key => $every_media_jczqk_val){
                $every_media_fad_times += $every_media_jczqk_val['fad_times'];
                $every_media_fad_illegal_times += $every_media_jczqk_val['fad_illegal_times'];
                $every_media_fad_count += $every_media_jczqk_val['fad_count'];
                $every_media_fad_illegal_count += $every_media_jczqk_val['fad_illegal_count'];

                if($every_media_jczqk_val['fad_illegal_times'] > 0){
                    array_push($every_media_illegal_times_data[0],$every_media_jczqk_val['tmedia_name']);
                    array_push($every_media_illegal_times_data[1],strval($every_media_jczqk_val['fad_illegal_times']));
                }

                $every_media_data[] = [
                    strval($every_media_jczqk_key+1),
                    $every_media_jczqk_val['tregion_name'],
                    $every_media_jczqk_val['tmedia_name'],
                    $every_media_jczqk_val['fad_times'],
                    $every_media_jczqk_val['fad_count'],
                    $every_media_jczqk_val['fad_illegal_times'],
                    $every_media_jczqk_val['fad_illegal_count'],
                    $every_media_jczqk_val['fad_illegal_times_rate'].'%',
                    $every_media_jczqk_val['fad_illegal_count_rate'].'%'
                ];
            }
            /*            $every_media_data[] = [
                            '',
                            '合计',
                            strval($every_media_fad_times),
                            strval($every_media_fad_count),
                            strval($every_media_fad_illegal_times),
                            strval($every_media_fad_illegal_count),
                            round((($every_media_fad_illegal_times)/($every_media_fad_times))*100,2).'%',
                            round((($every_media_fad_illegal_count)/($every_media_fad_count))*100,2).'%'
                        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）*/
            $every_media_all_data[] = $every_media_data;
            $every_media_illegal_times_all_data[] = $every_media_illegal_times_data;
        }


        $every_media_tv_data = $every_media_all_data[0];//电视各媒体
        $every_media_tb_data = $every_media_all_data[1];//广播各媒体
        $every_media_tp_data = $every_media_all_data[2];//报纸各媒体
        $every_media_illegal_times_tv_data = $every_media_illegal_times_all_data[0];//电视各媒体
        $every_media_illegal_times_tb_data = $every_media_illegal_times_all_data[1];//广播各媒体
        $every_media_illegal_times_tp_data =$every_media_illegal_times_all_data[2];//报纸各媒体





        /*
* @各广告类型监测总情况
*
**/

        $every_ad_class_data = [];//定义各地区发布监测情况数组
//循环赋值
        $every_ad_class_illegal_times_data = [
            [''],
            ['']
        ];
        $every_ad_class_jczqk = pxsf($every_ad_class_jczqk,'fad_illegal_times');//重新排序
        foreach ($every_ad_class_jczqk as $every_ad_class_jczqk_key => $every_ad_class_jczqk_val){
            $every_ad_class_fad_times += $every_ad_class_jczqk_val['fad_times'];
            $every_ad_class_fad_illegal_times += $every_ad_class_jczqk_val['fad_illegal_times'];
            $every_ad_class_fad_count += $every_ad_class_jczqk_val['fad_count'];
            $every_ad_class_fad_illegal_count += $every_ad_class_jczqk_val['fad_illegal_count'];
            if($every_ad_class_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_ad_class_illegal_times_data[0],$every_ad_class_jczqk_val['tadclass_name']);
                array_push($every_ad_class_illegal_times_data[1],strval($every_ad_class_jczqk_val['fad_illegal_times']));
            }

            $every_ad_class_data[] = [
                strval($every_ad_class_jczqk_key+1),
                $every_ad_class_jczqk_val['tadclass_name'],
                $every_ad_class_jczqk_val['fad_times'],
                $every_ad_class_jczqk_val['fad_count'],
                $every_ad_class_jczqk_val['fad_illegal_times'],
                $every_ad_class_jczqk_val['fad_illegal_count'],
                $every_ad_class_jczqk_val['fad_illegal_times_rate'].'%',
                $every_ad_class_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_ad_class_data[] = [
            '',
            '合计',
            strval($every_ad_class_fad_times),
            strval($every_ad_class_fad_count),
            strval($every_ad_class_fad_illegal_times),
            strval($every_ad_class_fad_illegal_count),
            round((($every_ad_class_fad_illegal_times)/($every_ad_class_fad_times))*100,2).'%',
            round((($every_ad_class_fad_illegal_count)/($every_ad_class_fad_count))*100,2).'%'
        ];


        $typical_illegal_ad_list_data = [];
        if(!empty($hz_zdclass_tv_illegal_ad_data) || !empty($hz_zdclass_tb_illegal_ad_data) || !empty($hz_zdclass_tp_illegal_ad_data)){
            $typical_illegal_ad_list = array_merge($hz_zdclass_tv_illegal_ad_data,$hz_zdclass_tb_illegal_ad_data,$hz_zdclass_tp_illegal_ad_data);
            $illegal_ad_list_data = [];
            $typical_illegal_ad_list = pxsf($typical_illegal_ad_list,'illegal_times');//排序
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_val){
                $md5 = MD5($typical_illegal_ad_list_val['fad_name'].$typical_illegal_ad_list_val['fmedianame']);
                if(isset($illegal_ad_list_data[$md5])){
                    $illegal_ad_list_data[$md5]['illegal_times'] += $typical_illegal_ad_list_val['illegal_times'];
                    $illegal_ad_list_data[$md5]['illegal_times'] = strval($illegal_ad_list_data[$md5]['illegal_times']);
                }else{

                    $illegal_ad_list_data[$md5] = $typical_illegal_ad_list_val;
                }
            }

            $typical_illegal_ad_list = array_values($illegal_ad_list_data);
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_key => $typical_illegal_ad_list_val){
                $typical_illegal_ad_list_data[] = [
                    strval($typical_illegal_ad_list_key+1),
                    $typical_illegal_ad_list_val['fmedianame'],
                    $typical_illegal_ad_list_val['fad_name'],
                    $typical_illegal_ad_list_val['fadclass'],
                    $typical_illegal_ad_list_val['illegal_times']
                ];
            }
        }



//定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "大标题楷体60",
            "text" => "广告监测季报"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号加粗",
            "text" => $user['regulatorpname'].""
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号加粗带红线",
            "text" => "广告监测中心               ".date('Y年m月d日')
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $user['regulatorpname']."广告监测中心对".$date_str.$r_name."部分电视、广播、报刊等".$ct_media_count."家媒体发布广告的情况进行了抽查监测，现将监测情况通报如下："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "一、监测范围及对象"
        ];
        if($ct_media_tv_count_hz != 0){
            $hz_media_count_str .= $ct_media_tv_count_hz."个电视媒体；";
        }
        if($ct_media_bc_count_hz != 0){
            $hz_media_count_str .= $ct_media_bc_count_hz."个广播媒体；";
        }
        if($ct_media_paper_count_hz != 0){
            $hz_media_count_str .= $ct_media_paper_count_hz."个报纸媒体；";
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）".$dq_name."媒体：".$hz_media_count_str
        ];
        if($ct_media_tv_count_xj != 0){
            $xj_media_count_str .= $ct_media_tv_count_xj."个电视媒体；";
        }
        if($ct_media_bc_count_xj != 0){
            $xj_media_count_str .= $ct_media_bc_count_xj."个广播媒体；";
        }
        if($ct_media_paper_count_xj != 0){
            $xj_media_count_str .= $ct_media_paper_count_xj."个报纸媒体；";
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）其他".$tregion_count."个区、县：".$xj_media_count_str
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "二、各类媒体广告发布情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "本季度共监测广告".$every_media_class_data[3][1]."条次，".$every_media_class_data[3][2]."条数，其中监测发现各类涉嫌违法广告".$every_media_class_data[3][3]."条次，".$every_media_class_data[3][4]."条数。具体情况详见下表："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒介类型汇总表格hz",
            "data" => $every_media_class_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "三、市（各区县）媒体广告发布总体情况"
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "地域排名汇总表格hz",
            "data" => $every_region_data
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各市区县违法广告发布量排名条形图hz",
            "data" => $every_region_illegal_times_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "四、全市电视、广播、报纸广告发布情况（排名按发布违法广告条次由高到底排列）"
        ];

        if(!empty($every_media_tv_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "1、电视"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tv_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tv_data
            ];
        }
        if(!empty($every_media_tb_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "2、广播"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tb_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tb_data
            ];
        }
        if(!empty($every_media_tp_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "3、报纸"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tp_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tp_data
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、全市各类媒体发布的违法广告（全部类别）通报"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）市级媒体"
        ];


        if(!empty($hz_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tv_illegal_ad_data)
            ];
        }
        if(!empty($hz_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tb_illegal_ad_data)
            ];
        }
        if(!empty($hz_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tp_illegal_ad_data)
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）区（县）级媒体"
        ];

        if(!empty($xj_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tv_illegal_ad_data)
            ];
        }
        if(!empty($xj_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tb_illegal_ad_data)
            ];
        }

        if(!empty($xj_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tp_illegal_ad_data)
            ];
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "六、全市各类违法广告比重结构图"
        ];
        if(!empty($every_ad_class_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表hz",
                "data" => $every_ad_class_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "各大类别广告违法占比表hz",
                "data" => $every_ad_class_illegal_times_data
            ];

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "七、药品、医疗服务、医疗器械、保健食品违法广告发布情况"
        ];

        if(!empty($typical_illegal_ad_list_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $typical_illegal_ad_list_data
            ];
        }

        $report_data = json_encode($data);
        //echo $report_data;exit;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

//将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].$date_str.'报告';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 70;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',strtotime($cycle_arr[0][0]));
            $data['pnendtime'] 		    = date('Y-m-d',strtotime($cycle_arr[2][1]));
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }

    //年报
    public function create_year(){

        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $system_num = getconfig('system_num');
            
        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型
        $year = I('year','2018');//季度
        $days_1 = date('t', strtotime($year."-1-1"));
        $days_2 = date('t', strtotime($year."-2-1"));
        $days_3 = date('t', strtotime($year."-3-1"));
        $days_4 = date('t', strtotime($year."-4-1"));
        $days_5 = date('t', strtotime($year."-5-1"));
        $days_6 = date('t', strtotime($year."-6-1"));
        $days_7 = date('t', strtotime($year."-7-1"));
        $days_8 = date('t', strtotime($year."-8-1"));
        $days_9 = date('t', strtotime($year."-9-1"));
        $days_10 = date('t', strtotime($year."-10-1"));
        $days_11 = date('t', strtotime($year."-11-1"));
        $days_12 = date('t', strtotime($year."-12-1"));
        $cycle_arr = [
            [$year."-1-1",$year."-1-".$days_1],
            [$year."-2-1",$year."-2-".$days_2],
            [$year."-3-1",$year."-3-".$days_3],
            [$year."-4-1",$year."-4-".$days_4],
            [$year."-5-1",$year."-5-".$days_5],
            [$year."-6-1",$year."-6-".$days_6],
            [$year."-7-1",$year."-7-".$days_7],
            [$year."-8-1",$year."-8-".$days_8],
            [$year."-9-1",$year."-9-".$days_9],
            [$year."-10-1",$year."-10-".$days_10],
            [$year."-11-1",$year."-11-".$days_11],
            [$year."-12-1",$year."-12-".$days_12]
        ];

        switch ($quarter){
            case 1:
                break;
                $quarter_str = '一';
            case 2:
                $quarter_str = '二';
                break;
            case 3:
                $quarter_str = '三';
                break;
            case 4:
                $quarter_str = '四';
                break;
        }

        $user = session('regulatorpersonInfo');//获取用户信息
        $tregion_count = M('tregion')->where(['fpid'=>$user['regionid']])->count();

        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级
        if($re_level == 2 || $re_level == 3 || $re_level == 4){
            $dq_name = '市级';
            $r_name = '全市';
            $xj_name = '区县';
        }elseif($re_level == 1){
            $dq_name = '全省';
            $xj_name = '市级';
        }else{
            $dq_name = '全'.$user['regionname1'];
            $xj_name = '地方';
        }

        $date_str = $year.'年';//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数
        $owner_media_ids = get_owner_media_ids();


        $ct_jczqk_quarter = [];
        $every_region_jczqk_quarter = [];
        $hz_tv_illegal_ad_data_quarter = [];
        $every_region_jczqk_quarter = [];//各地域监测总情况
        $every_ad_class_jczqk_quarter = [];//各广告类别监测总情况
        //本级电视
        $hz_tv_illegal_ad_data_quarter = [];
        //本级广播
        $hz_tb_illegal_ad_data_quarter = [];
        //本级报纸
        $hz_tp_illegal_ad_data_quarter = [];
        //下级电视
        $xj_tv_illegal_ad_data_quarter = [];
        //下级广播
        $xj_tb_illegal_ad_data_quarter = [];
        //下级报纸
        $xj_tp_illegal_ad_data_quarter = [];
        $hz_zdclass_tv_illegal_ad_data_quarter = [];
        $hz_zdclass_tb_illegal_ad_data_quarter = [];
        $hz_zdclass_tp_illegal_ad_data_quarter = [];
        $ct_media_tv_count_hz = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局电视媒体数量

        $ct_media_bc_count_hz = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局广播媒体数量

        $ct_media_paper_count_hz = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,0);//市局报纸媒体数量

        $ct_media_tv_count_xj = $StatisticalReport->ct_media_count('tv',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县电视媒体数量

        $ct_media_bc_count_xj = $StatisticalReport->ct_media_count('bc',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县广播媒体数量

        $ct_media_paper_count_xj = $StatisticalReport->ct_media_count('paper',$date_table,$owner_media_ids,$report_start_date,$report_end_date,2);//各区县报纸媒体数量


        $ct_media_count = $ct_media_tv_count_hz + $ct_media_bc_count_hz + $ct_media_paper_count_hz + $ct_media_tv_count_xj + $ct_media_bc_count_xj + $ct_media_paper_count_xj;//全部有数据的媒体数量
        foreach ($cycle_arr as $cycle_arr_val){
            $date_table = date('Ym',strtotime($cycle_arr_val[0])).'_'.substr($user['regionid'],0,2);//组合表
            if(!table_exist("ttvissue_".$date_table) || !table_exist("tbcissue_".$date_table)){
                continue;
            }
            $report_start_date = strtotime($cycle_arr_val[0]);
            $report_end_date = strtotime($cycle_arr_val[1])+86399;

            /*
* @各媒体监测总情况
*
**/
            $ct_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_count_rate','fmediaclass');//传统媒体监测总情况

            /*
* @各地域监测总情况
*
**/
            $every_region_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_illegal_times','fregionid');//各地域监测总情况
            /*
* @各广告类型监测总情况
*
**/
            $every_ad_class_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'','','fad_times','fcode');//各地域监测总情况

            //本级电视
            $hz_tv_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',0);

            //本级广播
            $hz_tb_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',0);

            //本级报纸
            $hz_tp_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',0);

            //下级电视
            $xj_tv_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,'',2);

            //下级广播
            $xj_tb_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,'',2);

            //下级报纸
            $xj_tp_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,'',2);


            //四大类 药品、医疗服务、医疗器械、保健食品
            $hz_zdclass_tv_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,1,$report_start_date,$report_end_date,['01','02','13','06'],1);
            $hz_zdclass_tb_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,2,$report_start_date,$report_end_date,['01','02','13','06'],1);
            $hz_zdclass_tp_illegal_ad_data_quarter[] = $StatisticalReport->typical_illegal_ad_hz_list($date_table,$owner_media_ids,3,$report_start_date,$report_end_date,['01','02','13','06'],1);



        }

        $ct_jczqk = $this->create_data_hzlist($ct_jczqk_quarter,'fclass');//电视广播报纸汇总数据
        $every_region_jczqk = $this->create_data_hzlist($every_region_jczqk_quarter,'fregionid');//各地域
        $every_ad_class_jczqk = $this->create_data_hzlist($every_ad_class_jczqk_quarter,'fcode');//各类别
        $hz_tv_illegal_ad_data = $this->create_data_hzlist2($hz_tv_illegal_ad_data_quarter,'fad_name');
        $hz_tb_illegal_ad_data = $this->create_data_hzlist2($hz_tb_illegal_ad_data_quarter,'fad_name');
        $hz_tp_illegal_ad_data = $this->create_data_hzlist2($hz_tp_illegal_ad_data_quarter,'fad_name');
        $xj_tv_illegal_ad_data = $this->create_data_hzlist2($xj_tv_illegal_ad_data_quarter,'fad_name');
        $xj_tb_illegal_ad_data = $this->create_data_hzlist2($xj_tb_illegal_ad_data_quarter,'fad_name');
        $xj_tp_illegal_ad_data = $this->create_data_hzlist2($xj_tp_illegal_ad_data_quarter,'fad_name');

        $hz_zdclass_tv_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tv_illegal_ad_data_quarter,'fad_name');
        $hz_zdclass_tb_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tb_illegal_ad_data_quarter,'fad_name');
        $hz_zdclass_tp_illegal_ad_data = $this->create_data_hzlist2($hz_zdclass_tp_illegal_ad_data_quarter,'fad_name');

        //本级电视
        $hz_tv_illegal_ad_data = $this->get_table_data($hz_tv_illegal_ad_data);
        //本级广播
        $hz_tb_illegal_ad_data = $this->get_table_data($hz_tb_illegal_ad_data);
        //本级报纸
        $hz_tp_illegal_ad_data = $this->get_table_data($hz_tp_illegal_ad_data);
        //下级电视
        $xj_tv_illegal_ad_data = $this->get_table_data($xj_tv_illegal_ad_data);
        //下级广播
        $xj_tb_illegal_ad_data = $this->get_table_data($xj_tb_illegal_ad_data);
        //下级报纸
        $xj_tp_illegal_ad_data = $this->get_table_data($xj_tp_illegal_ad_data);

        /*
        * @各媒体监测总情况
        *
        **/
        $ct_fad_times = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times = 0;//定义传统媒体发布违法广告条次
        $every_media_class_data = [];//定义媒体发布监测情况数组
//循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk as $ct_jczqk_val){
            $ct_fad_times += $ct_jczqk_val['fad_times'];
            $ct_fad_illegal_times += $ct_jczqk_val['fad_illegal_times'];
            $ct_fad_count += $ct_jczqk_val['fad_count'];
            $ct_fad_illegal_count += $ct_jczqk_val['fad_illegal_count'];
            $ct_data_val = [
                $ct_jczqk_val['tmediaclass_name'],
                $ct_jczqk_val['fad_times'],
                $ct_jczqk_val['fad_count'],
                $ct_jczqk_val['fad_illegal_times'],
                $ct_jczqk_val['fad_illegal_count'],
                $ct_jczqk_val['fad_illegal_times_rate'].'%',
                $ct_jczqk_val['fad_illegal_count_rate'].'%'
            ];
            if($ct_jczqk_val['fmedia_class_code'] == '01'){
                $every_media_class_data[0] = $ct_data_val;
            }elseif($ct_jczqk_val['fmedia_class_code'] == '02'){
                $every_media_class_data[1] = $ct_data_val;
            }else{
                $every_media_class_data[2] = $ct_data_val;
            }
        }

        $every_media_class_data[3] = [
            '合计',
            strval($ct_fad_times),
            strval($ct_fad_count),
            strval($ct_fad_illegal_times),
            strval($ct_fad_illegal_count),
            round((($ct_fad_illegal_times)/($ct_fad_times))*100,2).'%',
            round((($ct_fad_illegal_count)/($ct_fad_count))*100,2).'%'
        ];//二、各类媒体广告发布情况
        $every_media_class_data = array_values($every_media_class_data);
        /*
* @各地域监测总情况
*
**/
        $every_region_data = [];//定义各地区发布监测情况数组
//循环赋值
        $every_region_illegal_times_data = [
            [''],
            ['']
        ];
        $every_region_jczqk = pxsf($every_region_jczqk,'fad_illegal_times');
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){
            $every_region_fad_times += $every_region_jczqk_val['fad_times'];
            $every_region_fad_illegal_times += $every_region_jczqk_val['fad_illegal_times'];
            $every_region_fad_count += $every_region_jczqk_val['fad_count'];
            $every_region_fad_illegal_count += $every_region_jczqk_val['fad_illegal_count'];
            if($every_region_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_region_illegal_times_data[0],$every_region_jczqk_val['tregion_name']);
                array_push($every_region_illegal_times_data[1],strval($every_region_jczqk_val['fad_illegal_times']));
            }

            $every_region_data[] = [
                strval($every_region_jczqk_key+1),
                $every_region_jczqk_val['tregion_name'],
                $every_region_jczqk_val['fad_times'],
                $every_region_jczqk_val['fad_count'],
                $every_region_jczqk_val['fad_illegal_times'],
                $every_region_jczqk_val['fad_illegal_count'],
                $every_region_jczqk_val['fad_illegal_times_rate'].'%',
                $every_region_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_region_data[] = [
            '',
            '合计',
            strval($every_region_fad_times),
            strval($every_region_fad_count),
            strval($every_region_fad_illegal_times),
            strval($every_region_fad_illegal_count),
            round((($every_region_fad_illegal_times)/($every_region_fad_times))*100,2).'%',
            round((($every_region_fad_illegal_count)/($every_region_fad_count))*100,2).'%'
        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）



        /*
* @各地域电视广播报纸监测总情况
*
**/
        $every_media_all_data = [];
        $every_media_illegal_times_all_data = [];
        foreach (['01','02','03'] as $every_media_val){
            $every_media_data = [];
            $every_media_illegal_times_data = [];
            $every_media_jczqk = [];
            //循环赋值
            $every_media_fad_times = 0;
            $every_media_fad_illegal_times = 0;
            $every_media_fad_count = 0;
            $every_media_fad_illegal_count = 0;
            $every_media_illegal_times_data = [
                [''],
                ['']
            ];
            $every_media_jczqk_quarter = [];
            foreach ($cycle_arr as $cycle_arr_val){
                $date_table = date('Ym',strtotime($cycle_arr_val[0])).'_'.substr($user['regionid'],0,2);//组合表
                if(!table_exist("ttvissue_".$date_table) || !table_exist("tbcissue_".$date_table)){
                    continue;
                }
                $report_start_date = strtotime($cycle_arr_val[0]);
                $report_end_date = strtotime($cycle_arr_val[1])+86399;
                $every_media_jczqk_quarter[] = $StatisticalReport->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,$every_media_val,'','fad_illegal_times','fmediaid');//各地域监测总情况
            }

            $every_media_jczqk = $this->create_data_hzlist($every_media_jczqk_quarter,'fid');//fad_illegal_times
            $every_media_jczqk = pxsf($every_media_jczqk,'fad_illegal_times');//重新排序
            foreach ($every_media_jczqk as $every_media_jczqk_key => $every_media_jczqk_val){
                $every_media_fad_times += $every_media_jczqk_val['fad_times'];
                $every_media_fad_illegal_times += $every_media_jczqk_val['fad_illegal_times'];
                $every_media_fad_count += $every_media_jczqk_val['fad_count'];
                $every_media_fad_illegal_count += $every_media_jczqk_val['fad_illegal_count'];

                if($every_media_jczqk_val['fad_illegal_times'] > 0){
                    array_push($every_media_illegal_times_data[0],$every_media_jczqk_val['tmedia_name']);
                    array_push($every_media_illegal_times_data[1],strval($every_media_jczqk_val['fad_illegal_times']));
                }

                $every_media_data[] = [
                    strval($every_media_jczqk_key+1),
                    $every_media_jczqk_val['tregion_name'],
                    $every_media_jczqk_val['tmedia_name'],
                    $every_media_jczqk_val['fad_times'],
                    $every_media_jczqk_val['fad_count'],
                    $every_media_jczqk_val['fad_illegal_times'],
                    $every_media_jczqk_val['fad_illegal_count'],
                    $every_media_jczqk_val['fad_illegal_times_rate'].'%',
                    $every_media_jczqk_val['fad_illegal_count_rate'].'%'
                ];
            }
            /*            $every_media_data[] = [
                            '',
                            '合计',
                            strval($every_media_fad_times),
                            strval($every_media_fad_count),
                            strval($every_media_fad_illegal_times),
                            strval($every_media_fad_illegal_count),
                            round((($every_media_fad_illegal_times)/($every_media_fad_times))*100,2).'%',
                            round((($every_media_fad_illegal_count)/($every_media_fad_count))*100,2).'%'
                        ];//三、各市（州）媒体广告发布总体情况  and   各市、州严重违法广告（全部类别）发布量排名（单位：条次）*/
            $every_media_all_data[] = $every_media_data;
            $every_media_illegal_times_all_data[] = $every_media_illegal_times_data;
        }


        $every_media_tv_data = $every_media_all_data[0];//电视各媒体
        $every_media_tb_data = $every_media_all_data[1];//广播各媒体
        $every_media_tp_data = $every_media_all_data[2];//报纸各媒体
        $every_media_illegal_times_tv_data = $every_media_illegal_times_all_data[0];//电视各媒体
        $every_media_illegal_times_tb_data = $every_media_illegal_times_all_data[1];//广播各媒体
        $every_media_illegal_times_tp_data =$every_media_illegal_times_all_data[2];//报纸各媒体





        /*
* @各广告类型监测总情况
*
**/

        $every_ad_class_data = [];//定义各地区发布监测情况数组
//循环赋值
        $every_ad_class_illegal_times_data = [
            [''],
            ['']
        ];
        $every_ad_class_jczqk = pxsf($every_ad_class_jczqk,'fad_illegal_times');//重新排序
        foreach ($every_ad_class_jczqk as $every_ad_class_jczqk_key => $every_ad_class_jczqk_val){
            $every_ad_class_fad_times += $every_ad_class_jczqk_val['fad_times'];
            $every_ad_class_fad_illegal_times += $every_ad_class_jczqk_val['fad_illegal_times'];
            $every_ad_class_fad_count += $every_ad_class_jczqk_val['fad_count'];
            $every_ad_class_fad_illegal_count += $every_ad_class_jczqk_val['fad_illegal_count'];
            if($every_ad_class_jczqk_val['fad_illegal_times'] > 0){
                array_push($every_ad_class_illegal_times_data[0],$every_ad_class_jczqk_val['tadclass_name']);
                array_push($every_ad_class_illegal_times_data[1],strval($every_ad_class_jczqk_val['fad_illegal_times']));
            }

            $every_ad_class_data[] = [
                strval($every_ad_class_jczqk_key+1),
                $every_ad_class_jczqk_val['tadclass_name'],
                $every_ad_class_jczqk_val['fad_times'],
                $every_ad_class_jczqk_val['fad_count'],
                $every_ad_class_jczqk_val['fad_illegal_times'],
                $every_ad_class_jczqk_val['fad_illegal_count'],
                $every_ad_class_jczqk_val['fad_illegal_times_rate'].'%',
                $every_ad_class_jczqk_val['fad_illegal_count_rate'].'%'
            ];
        }
        $every_ad_class_data[] = [
            '',
            '合计',
            strval($every_ad_class_fad_times),
            strval($every_ad_class_fad_count),
            strval($every_ad_class_fad_illegal_times),
            strval($every_ad_class_fad_illegal_count),
            round((($every_ad_class_fad_illegal_times)/($every_ad_class_fad_times))*100,2).'%',
            round((($every_ad_class_fad_illegal_count)/($every_ad_class_fad_count))*100,2).'%'
        ];


        $typical_illegal_ad_list_data = [];
        if(!empty($hz_zdclass_tv_illegal_ad_data) || !empty($hz_zdclass_tb_illegal_ad_data) || !empty($hz_zdclass_tp_illegal_ad_data)){
            $typical_illegal_ad_list = array_merge($hz_zdclass_tv_illegal_ad_data,$hz_zdclass_tb_illegal_ad_data,$hz_zdclass_tp_illegal_ad_data);
            $illegal_ad_list_data = [];
            $typical_illegal_ad_list = pxsf($typical_illegal_ad_list,'illegal_times');//排序
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_val){
                $md5 = MD5($typical_illegal_ad_list_val['fad_name'].$typical_illegal_ad_list_val['fmedianame']);
                if(isset($illegal_ad_list_data[$md5])){
                    $illegal_ad_list_data[$md5]['illegal_times'] += $typical_illegal_ad_list_val['illegal_times'];
                    $illegal_ad_list_data[$md5]['illegal_times'] = strval($illegal_ad_list_data[$md5]['illegal_times']);
                }else{

                    $illegal_ad_list_data[$md5] = $typical_illegal_ad_list_val;
                }
            }

            $typical_illegal_ad_list = array_values($illegal_ad_list_data);
            foreach ($typical_illegal_ad_list as $typical_illegal_ad_list_key => $typical_illegal_ad_list_val){
                $typical_illegal_ad_list_data[] = [
                    strval($typical_illegal_ad_list_key+1),
                    $typical_illegal_ad_list_val['fmedianame'],
                    $typical_illegal_ad_list_val['fad_name'],
                    $typical_illegal_ad_list_val['fadclass'],
                    $typical_illegal_ad_list_val['illegal_times']
                ];
            }
        }



//定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "大标题楷体60",
            "text" => "广告监测年报"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号加粗",
            "text" => $user['regulatorpname'].""
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号加粗带红线",
            "text" => "广告监测中心               ".date('Y年m月d日')
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $user['regulatorpname']."广告监测中心对".$date_str.$r_name."部分电视、广播、报刊等".$ct_media_count."家媒体发布广告的情况进行了抽查监测，现将监测情况通报如下："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "一、监测范围及对象"
        ];
        if($ct_media_tv_count_hz != 0){
            $hz_media_count_str .= $ct_media_tv_count_hz."个电视媒体；";
        }
        if($ct_media_bc_count_hz != 0){
            $hz_media_count_str .= $ct_media_bc_count_hz."个广播媒体；";
        }
        if($ct_media_paper_count_hz != 0){
            $hz_media_count_str .= $ct_media_paper_count_hz."个报纸媒体；";
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）".$dq_name."媒体：".$hz_media_count_str
        ];
        if($ct_media_tv_count_xj != 0){
            $xj_media_count_str .= $ct_media_tv_count_xj."个电视媒体；";
        }
        if($ct_media_bc_count_xj != 0){
            $xj_media_count_str .= $ct_media_bc_count_xj."个广播媒体；";
        }
        if($ct_media_paper_count_xj != 0){
            $xj_media_count_str .= $ct_media_paper_count_xj."个报纸媒体；";
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）其他".$tregion_count."个区、县：".$xj_media_count_str
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "二、各类媒体广告发布情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "本年度共监测广告".$every_media_class_data[3][1]."条次，".$every_media_class_data[3][2]."条数，其中监测发现各类涉嫌违法广告".$every_media_class_data[3][3]."条次，".$every_media_class_data[3][4]."条数。具体情况详见下表："
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "媒介类型汇总表格hz",
            "data" => $every_media_class_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "三、市（各区县）媒体广告发布总体情况"
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "地域排名汇总表格hz",
            "data" => $every_region_data
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各市区县违法广告发布量排名条形图hz",
            "data" => $every_region_illegal_times_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "四、全市电视、广播、报纸广告发布情况（排名按发布违法广告条次由高到底排列）"
        ];

        if(!empty($every_media_tv_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "1、电视"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tv_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tv_data
            ];
        }
        if(!empty($every_media_tb_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "2、广播"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tb_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tb_data
            ];
        }
        if(!empty($every_media_tp_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号标题",
                "text" => "3、报纸"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各媒体违法广告发布量排名列表hz",
                "data" => $every_media_tp_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "违法广告发布量排名通用hz",
                "data" => $every_media_illegal_times_tp_data
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、全市各类媒体发布的违法广告（全部类别）通报"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）市级媒体"
        ];


        if(!empty($hz_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tv_illegal_ad_data)
            ];
        }
        if(!empty($hz_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tb_illegal_ad_data)
            ];
        }
        if(!empty($hz_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($hz_tp_illegal_ad_data)
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）区（县）级媒体"
        ];

        if(!empty($xj_tv_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "1、电视"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tv_illegal_ad_data)
            ];
        }
        if(!empty($xj_tb_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "2、广播"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tb_illegal_ad_data)
            ];
        }

        if(!empty($xj_tp_illegal_ad_data)){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "仿宋三号段落",
                "text" => "3、报纸"
            ];

            $data['content'][] = [
                "type" => "table",
                "bookmark" => "违法广告列表全部类型",
                "data" => $this->pmaddkey($xj_tp_illegal_ad_data)
            ];
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "六、全市各类违法广告比重结构图"
        ];
        if(!empty($every_ad_class_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表hz",
                "data" => $every_ad_class_data
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "各大类别广告违法占比表hz",
                "data" => $every_ad_class_illegal_times_data
            ];

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "七、药品、医疗服务、医疗器械、保健食品违法广告发布情况"
        ];

        if(!empty($typical_illegal_ad_list_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $typical_illegal_ad_list_data
            ];
        }

        $report_data = json_encode($data);
        //echo $report_data;exit;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            //将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].$date_str.'年度报告';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 80;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',strtotime($cycle_arr[0][1]));
            $data['pnendtime'] 		    = date('Y-m-d',strtotime($cycle_arr[11][1]));
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }



    }

    public function create_data_hzlist($ct_jczqk_quarter,$group){
        $ct_jczqk = [];
        foreach ($ct_jczqk_quarter as $ct_jczqk_quarter_val){
            foreach ($ct_jczqk_quarter_val as $ct_jczqk_quarter_val2){
                $fclass_md5 = '';
                $fclass_md5 = MD5($ct_jczqk_quarter_val2[$group]);
                if(isset($ct_jczqk[$fclass_md5])){
                    $ct_jczqk[$fclass_md5]['fad_times'] +=  $ct_jczqk_quarter_val2['fad_times'];
                    $ct_jczqk[$fclass_md5]['fad_times'] = strval($ct_jczqk[$fclass_md5]['fad_times']);
                    $ct_jczqk[$fclass_md5]['fad_count'] +=  $ct_jczqk_quarter_val2['fad_count'];
                    $ct_jczqk[$fclass_md5]['fad_count'] = strval($ct_jczqk[$fclass_md5]['fad_count']);
                    $ct_jczqk[$fclass_md5]['fad_illegal_times'] +=  $ct_jczqk_quarter_val2['fad_illegal_times'];
                    $ct_jczqk[$fclass_md5]['fad_illegal_times'] = strval($ct_jczqk[$fclass_md5]['fad_illegal_times']);
                    $ct_jczqk[$fclass_md5]['fad_illegal_count'] +=  $ct_jczqk_quarter_val2['fad_illegal_count'];
                    $ct_jczqk[$fclass_md5]['fad_illegal_count'] = strval($ct_jczqk[$fclass_md5]['fad_illegal_count']);
                    $ct_jczqk[$fclass_md5]['fad_illegal_times_rate'] =  round((($ct_jczqk[$fclass_md5]['fad_illegal_times'])/($ct_jczqk[$fclass_md5]['fad_times']))*100,2);
                    $ct_jczqk[$fclass_md5]['fad_illegal_count_rate'] =  round((($ct_jczqk[$fclass_md5]['fad_illegal_count'])/($ct_jczqk[$fclass_md5]['fad_count']))*100,2);
                }else{
                    $ct_jczqk[$fclass_md5] = $ct_jczqk_quarter_val2;
                }
            }
        }
        $ct_jczqk = array_values($ct_jczqk);
        return $ct_jczqk;
    }

    public function create_data_hzlist2($ct_jczqk_quarter,$group){
        $ct_jczqk = [];
        foreach ($ct_jczqk_quarter as $ct_jczqk_quarter_val){
            foreach ($ct_jczqk_quarter_val as $ct_jczqk_quarter_val2){
                $fclass_md5 = '';
                $fclass_md5 = MD5($ct_jczqk_quarter_val2[$group]);
                if(isset($ct_jczqk[$fclass_md5])){
                    $ct_jczqk[$fclass_md5]['illegal_times'] +=  $ct_jczqk_quarter_val2['illegal_times'];
                    $ct_jczqk[$fclass_md5]['illegal_times'] = strval($ct_jczqk[$fclass_md5]['illegal_times']);
                    $ct_jczqk[$fclass_md5]['illegal_count'] +=  $ct_jczqk_quarter_val2['illegal_count'];
                    $ct_jczqk[$fclass_md5]['illegal_count'] = strval($ct_jczqk[$fclass_md5]['illegal_count']);
                }else{
                    $ct_jczqk[$fclass_md5] = $ct_jczqk_quarter_val2;
                }
            }
        }
        $ct_jczqk = array_values($ct_jczqk);
        return $ct_jczqk;
    }

    public function get_table_data($data){
        $data_array = [];
        foreach ($data as $data_val){
            $data_array[] = array_values($data_val);
        }
        return $data_array;

    }

    public function pmaddkey($data,$px_num=3){
        if($px_num){
            $data = pxsf($data,$px_num);
        }
        $res = [];
        foreach ($data as $data_key=>$data_val){
            $array_res = [
                strval($data_key+1),
            ];
            foreach ($data_val as $data_val2){
                $array_res[] = $data_val2;
            }
            $res[] = $array_res;
        }
        return $res;
    }

    public function upload_report(){
        $user = session('regulatorpersonInfo');//获取用户信息
        $ffiles = I('ffiles');
        $pnendtime = I('pnendtime');
        $pnfiletype = I('pnfiletype');
        $pnname = I('pnname');
        $pntype = I('pntype');
        
        $system_num = getconfig('system_num');

        if(empty($ffiles[0]['url'])){
            $this->ajaxReturn(array('code'=>-1,'msg'=>'请上传文件'));
        }
        $date_now = date('Y-m-d H:i:s');
        $data['pnname'] 			= $pnname;
        $data['pntype'] 			= $pntype;
        $data['pnfiletype'] 		= $pnfiletype;
        $data['pnstarttime'] 		= $date_now;
        $data['pnendtime'] 		    = $pnendtime;
        $data['pncreatetime'] 		= date('Y-m-d H:i:s');
        $data['pnurl'] 				= $ffiles[0]['url'];
        $data['pnhtml'] 			= '';
        $data['pnfocus']            = 0;
        $data['pntrepid']           = $user['fregulatorpid'];
        $data['pncreatepersonid']   = $user['fid'];
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'上传成功'));
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'上传失败'));
        }
    }

}