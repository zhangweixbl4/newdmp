<?php
namespace Api\Controller;
use Think\Controller;
import('Vendor.PHPExcel');
class FunctionController extends Controller {

	public $oParam;//公共变量

	public function _initialize(){
		header('Access-Control-Allow-Origin:*');//允许所有域名跨域访问
		header("Content-type: text/html; charset=utf-8"); 
		$this->oParam = json_decode(file_get_contents('php://input'),true);//接收数据
	}

	/**
	 * 数据以xls导出公共方法
	 * by zw
	 */
	public function outdata_xls($outdata){
		session_write_close();

		//传参示例
        // $outdata['datalie'] = ['序号','地域','发布媒体'];
        // $outdata['datazd'] = [['type'=>'zwif','data'=>[['{fmedia_class} == 1 || {fmedia_class} == 2 || {fmedia_class} == 3','广告名：{fad_name}'],['','未备']],'ffullname','fmedianame'];

		$title = $outdata['title'];//标题
		$endtitle = $outdata['endtitle'];//结束标题
		$filenames = $outdata['filenames']?$outdata['filenames']:'XLS'.date('Ymdhis').rand(1000,9999);//保存的文件名称
		$sheetname = $outdata['sheetnames']?$outdata['sheetnames']:'Sheet1';//表格1标签名
		$datalists = $outdata['lists'];//需要导出的数据
		$datalie = $outdata['datalie'];//数据列

		$liecount = count($datalie);//计划一共有多少列数据

		$objPHPExcel = new \PHPExcel();  
	    $objPHPExcel
			->getProperties()  //获得文件属性对象，给下文提供设置资源
			->setCreator( "MaartenBalliauw")             //设置文件的创建者
			->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
			->setTitle( "Office2007 XLSX Test Document" )    //设置标题
			->setSubject( "Office2007 XLSX Test Document" )  //设置主题
			->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
			->setKeywords( "office 2007 openxmlphp")        //设置标记
			->setCategory( "Test resultfile");                //设置类别

		$sheet = $objPHPExcel->setActiveSheetIndex(0);

		$nowline = 0;
		if(!empty($title)){
			if(is_array($title)){
				$stitle = $title;
			}else{
				$stitle[] = $title;
			}
			foreach ($stitle as $value) {
				$nowline ++;
				$sheet -> mergeCells(getXlsNum(65).$nowline.':'.getXlsNum(64+$liecount).$nowline);
				$sheet ->getStyle(getXlsNum(65).$nowline.':'.getXlsNum(64+$liecount).$nowline)->applyFromArray(array('font' => array ('bold' => true),'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
				$sheet ->setCellValue(getXlsNum(65).$nowline,$value);
			}
		}

		$lienum = 0;
		$nowline ++;
		foreach ($datalie as $key => $value) {
			$lienum ++;
			$sheet ->setCellValue(getXlsNum((64+$lienum)).$nowline,$key);
		}

		$pattern="/{(.*)}/iU";
		foreach ($datalists as $key => $value) {//生成数据
			$lienum = 0;
			$nowline ++;
			foreach ($datalie as $liekey => $lievalue) {
				$lienum ++;
				if(is_array($lievalue)){
					if($lievalue['type'] == 'zwhebing'){//zwhebing
						$patternstrexec = $lievalue['data'];
						if(!empty($patternstrexec)){
							if(empty($patternarrexec[$lienum])){
    							preg_match_all($pattern,$patternstrexec,$patternarrexec[$lienum]);
							}
    						foreach ($patternarrexec[$lienum][0] as $key3 => $value3) {//获取组装好的判断条件
						      $patternstrexec = str_replace($value3, $value[$patternarrexec[$lienum][1][$key3]], $patternstrexec);
						    }
						    $sheet ->setCellValue(getXlsNum((64+$lienum)).$nowline,$patternstrexec);
						}
					}else{//zwif
						$excel_value = '';
						foreach ($lievalue['data'] as $key2 => $value2) {
							$patternstr = $value2[0];
							if(!empty($patternstr)){
								if(empty($patternarr[$key2][$lienum])){
		    						preg_match_all($pattern,$patternstr,$patternarr[$key2][$lienum]);
								}
							    foreach ($patternarr[$key2][$lienum][0] as $key3 => $value3) {//获取组装好的判断条件
								    $patternstr = str_replace($value3, '$value["'.$patternarr[$key2][$lienum][1][$key3].'"]', $patternstr);
								    eval('$panduan = '.$patternstr.';');
								    if($panduan){
								      	$patternstrexec = $value2[1];
										if(!empty($patternstrexec)){
											if(empty($patternarrexec[$key2][$lienum])){
				    							preg_match_all($pattern,$patternstrexec,$patternarrexec[$key2][$lienum]);
											}
											if(!empty(($patternarrexec[$key2][$lienum][0]))){
												foreach ($patternarrexec[$key2][$lienum][0] as $key3 => $value3) {//获取组装好的判断条件
											      $patternstrexec = str_replace($value3, $value[$patternarrexec[$key2][$lienum][1][$key3]], $patternstrexec);
											    }
											}
											$excel_value = $patternstrexec;
										    $sheet ->setCellValue(getXlsNum((64+$lienum)).$nowline,$excel_value);
										}
							      		break;
							      	}
							    }
							}else{
					      		$patternstrexec2 = $value2[1];
								if(!empty($patternstrexec2)){
									if(empty($patternarrexec2[$key2][$lienum])){
		    							preg_match_all($pattern,$patternstrexec2,$patternarrexec2[$key2][$lienum]);
									}
		    						foreach ($patternarrexec2[$key2][$lienum][0] as $key3 => $value3) {//获取组装好的判断条件
								      $patternstrexec2 = str_replace($value3, $value[$patternarrexec2[$key2][$lienum][1][$key3]], $patternstrexec2);
								    }
								    $excel_value = $patternstrexec2;
								    $sheet ->setCellValue(getXlsNum((64+$lienum)).$nowline,$excel_value);
								}
						      	break;
							}
							if(!empty($excel_value)){
								break;
							}
						}
					}
				}else{
					if($lievalue != 'key'){
						$sheet ->setCellValue(getXlsNum((64+$lienum)).$nowline,$value[$lievalue]);
					}else{
						$sheet ->setCellValue(getXlsNum((64+$lienum)).$nowline,($key+1));
					}
				}
			}
		}

		//结束语
		if(!empty($endtitle)){
			$nowline ++;
			$sheet -> mergeCells(getXlsNum(65).$nowline.':'.getXlsNum(64+$liecount).$nowline);
			$sheet ->getStyle(getXlsNum(65).$nowline.':'.getXlsNum(64+$liecount).$nowline)->applyFromArray(array('alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)));
			$sheet ->setCellValue(getXlsNum(65).$nowline,$endtitle);
		}

		$objActSheet =$objPHPExcel->getActiveSheet();
		//给当前活动的表设置名称
		$objActSheet->setTitle($sheetname);

		$objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$savefile = './LOG/XLS'.date('Ymdhis').rand(1000,9999).'.xlsx';
		$objWriter->save($savefile);

		$ret = A('Common/AliyunOss','Model')->file_up('tem/'.$filenames.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='.$filenames.'.xlsx'));//上传云
		  unlink($savefile);//删除文件
		return $ret;
	}

	/*
    * 获取断流时段
    * by zw
    */
    public function getInterruptFlow(){
    	$getData = $this->oParam;
    	$startTime = $getData['startTime'];
    	$endTime = $getData['endTime'];
    	$mediaId = $getData['mediaId'];
        if(empty($startTime) || empty($endTime) || empty($mediaId)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数有误']);
        }
        $data = http('http://az.hz-data.xyz:8087/channel/'.$mediaId.'/m3u8.gap?start_time='.$startTime.'&end_time='.$endTime,[],'get',false,10);
        $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$data]);
    }

    /*
    * 获取断流时段
    * by zw
    */
    public function getInterruptFlow2(){
        $data = $this->tohttp('http://az.hz-data.xyz:8087/channel/11000010002360/m3u8.gap?start_time=1577721600&end_time=1577807999',[],'POST',false,0);
        dump($data);
    }

    public function uploadTest(){
    	$ret = A('Common/AliyunOss','Model')->file_up('tem/'.time().'.xlsx','./LOG/20200115112550.xlsx',array('Content-Disposition' => 'attachment;filename'.time().'.xlsx'));//上传云
    	var_dump($ret);
    }

    /**
	 * 发送HTTP请求方法，目前只支持CURL发送请求
	 * @param  string  $url    请求URL
	 * @param  array   $params 请求参数
	 * @param  string  $method 请求方法GET/POST
	 * @return array   $data   响应数据
	 */
	public function tohttp($url, $params = array(), $method = 'GET',$header = false,$CURLOPT_TIMEOUT = 2,$CURLOPT_HEADER = false){
		$msectime = msectime();
		$http_log = '';
		$http_log .= date('H:i:s').'	'.$url.'	';
		set_time_limit(360);
		$opts = array(
			CURLOPT_TIMEOUT        => $CURLOPT_TIMEOUT,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_SSL_VERIFYHOST => false,
		);
		switch(strtoupper($method)){
			case 'GET':
				$getQuerys = !empty($params) ? '?'. http_build_query($params) : '';
				$opts[CURLOPT_URL] = $url . $getQuerys;
				//var_dump($opts[CURLOPT_URL]);
				break;
			case 'POST':
				$opts[CURLOPT_URL] = $url;
				$opts[CURLOPT_POST] = 1;
				$opts[CURLOPT_POSTFIELDS] = $params;
				break;
			case "DELETE":
				$opts[CURLOPT_URL] = $url;
				$opts[CURLOPT_CUSTOMREQUEST] = 'DELETE';
                $opts[CURLOPT_POSTFIELDS] = $params;
                break;

		}
		/* 初始化并执行curl请求 */
		$ch = curl_init();
		// CURLOPT_HEADER启用时会将头文件的信息作为数据流输出。
		curl_setopt ( $ch, CURLOPT_HEADER, $CURLOPT_HEADER );// yuhou.wang

		if($header) curl_setopt ( $ch, CURLOPT_HTTPHEADER, $header );

		curl_setopt_array($ch, $opts);
		$dd['data']   = curl_exec($ch);
		$dd['err']    = curl_errno($ch);
		$dd['errmsg'] = curl_error($ch);
		curl_close($ch);

		$http_log .= msectime() - $msectime;
		$http_log .= "\n";
		//file_put_contents('LOG/http_log'.date('Ymd').'.log',$http_log,FILE_APPEND);
		
			return $dd;
	}

    /**
	 * 上传附件参数
	 * by zw
	 */
	public function getFileUp(){
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>file_up()));
	}

}