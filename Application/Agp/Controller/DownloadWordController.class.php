<?php
namespace Agp\Controller;
use Think\Controller;

class DownloadWordController extends BaseController{
	
	/**
	 * 生成广告监测报告（南京）
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据）
	 * by zw
	 */

	public function downLoadAdWords()
	{
		$system_num = getconfig('system_num');
		$regulatorpname = session('regulatorpersonInfo.regulatorpname');//机构名
		$regionid = session('regulatorpersonInfo.regionid');//机构行政区划ID
		$regionid = substr($regionid , 0 , 2);
		$date = I('date_time');//日期
		$date = '2018-03';
		$arr = explode("-" ,$date);
		$date1 = date('Y年m月',strtotime($date));
		$html = '<div style="margin:0 auto;"> <style>.tb tr td{ background:#ffffff; text-align:center; border-bottom:1px solid #333333;border-right:1px solid #333333;}</style>';
		$html.= '<div align="center"><p style="color:red;font-size:30px;">'.$regulatorpname.'</p></div>';
		$html.= '<div align="center"><p style="color:red;font-size:18px;">广告监测报告</p></div>';
		$html.= '<p align="center" style="color:red;font-size:12px;">（'.	$arr[0].'年第'.intval($arr[1]).'期）</p>';
		$html.= '<p align="right">'.$date1.'</p>';
		$html.= '<p>一、广告监测情况综述</p>';

		$usermedia = M('tmedia_temp')->where('ftype=1 and fcustomer='.$system_num.' and fuserid='.session('regulatorpersonInfo.fid'))->getField('fmediaid',true);

		$where['t.fid'] = array('in',$usermedia);
		$where['_string'] = "date_format(t.`fcreatetime`,'%Y-%m')='".$date."'";
		//dump($where);
		//所有媒体
		$res = M('tmedia')
				->alias('t')
				->field('t.fid,t.fmediaownerid,t.fmediaclassid, (case when instr(t.fmedianame,"（") > 0 then left(t.fmedianame,instr(t.fmedianame,"（") -1) else t.fmedianame end) as t.fmedianame,tw.fregionid')
				->join('tmediaowner as tw on t.fmediaownerid = tw.fid')
				->where($where)
				->select();
		$count = count($res);
		
		$attach1 = [];
		$attach2 = [];
		$attach4 = [];
		$attach6 = [];
		foreach($res as $key => $val){
			$tregion_len = get_tregionlevel($val['fregionid']);
			if($tregion_len == 1){//国家级
				array_push($attach1, $val);
				$str1 .= $val['fmedianame'].'、';
			}elseif($tregion_len == 2){//省级
				array_push($attach2, $val);
				$str2 .= $val['fmedianame'].'、';
			}elseif($tregion_len == 4){//市级
				array_push($attach4, $val);
				$str4 .= $val['fmedianame'].'、';
			}elseif($tregion_len == 6){//县级
				array_push($attach6, $val);
				$str6 .= $val['fmedianame'].'、';
			}
		}

		$html.= '<p>
					广告监测中心'.intval($arr[1]).'月共监测'.$count.'家媒体，其中市属媒体'.count($attach4).'家('.$str4.')。省属媒体'.count($attach2).'家('.$str2.')。
		         </p>';
		$regionids = [];
		foreach($res as $val){
			array_push($regionids, $val['fid']);//用户具有权限的媒介id
		}
		$now_date = $arr[0].$arr[1].'_'.$regionid;//当月
		$date_last = date("Y-m", strtotime("-1 months", strtotime($date)));//上个月份
		$last_date = date("Ym", strtotime("-1 months", strtotime($date))).'_'.$regionid;//上月
		
		$sql = 'select tv.fmediaid,tv.fsampleid from ttvissue_'.$now_date.' as tv where tv.fmediaid in ('.join(',',$regionids).')
				union all (select tb.fmediaid,tb.fsampleid from tbcissue_'.$now_date.' as tb where tb.fmediaid in ('.join(',',$regionids).'))
				union all (select tp.fmediaid,tp.fpapersampleid as fsampleid from tpaperissue as tp where tp.fmediaid in ('.join(',',$regionids).') and date_format(tp.`fissuedate`,"%Y-%m")='.$date.')
				';
		$sql1 = 'select tv.fmediaid,tv.fsampleid from ttvissue_'.$last_date.' as tv where tv.fmediaid in ('.join(',',$regionids).')
				union all (select tb.fmediaid,tb.fsampleid from tbcissue_'.$last_date.' as tb where tb.fmediaid in ('.join(',',$regionids).'))
				union all (select tp.fmediaid,tp.fpapersampleid as fsampleid from tpaperissue as tp where tp.fmediaid in ('.join(',',$regionids).') and date_format(tp.`fissuedate`,"%Y-%m")='.$date_last.')
				';
		$n_res = M()->query($sql);//当月总发布量
		$l_res = M()->query($sql1);//上月总发布量
        // dump(M()->getLastSql());
        $zdwf = 0;
        $yzwf = 0;
        $ybwf = 0;
        $arr_fmediaid = [];
        $arr_fsampleid = [];
        $larr_fmediaid = [];
        $larr_fsampleid = [];
        $wfggsl = 0;
		$ggslwfl = 0;
		$yzwfggsl = 0;
		$ggslyzwfl = 0;


		//当月所有媒体发布广告违法情况
        if($n_res){
        	foreach($n_res as $v){
        		$arr_fmediaid[] = $v['fmediaid'];
        		$arr_fsampleid[] = $v['fsampleid'];
        	}

        	//当月广播发布违法情况
        	$sql = 'select count(*) total,
        			ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
        			ifnull(sum(case when fillegaltypecode>30 then 1 else 0 end),0) as yzwfggsl,
        			ifnull(sum(case when fillegaltypecode>20 then 1 else 0 end),0) as ybwfggsl,
        			ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
        			ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl
        			from tbcsample where fid in ('.implode(',', $arr_fsampleid).') and fmediaid in ('.implode(',', $arr_fmediaid).')';
        	$bc_res = M()->query($sql);

        	//当月电视发布违法情况
        	$sql = 'select count(*) total,
        			ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
        			ifnull(sum(case when fillegaltypecode>30 then 1 else 0 end),0) as yzwfggsl,
        			ifnull(sum(case when fillegaltypecode>20 then 1 else 0 end),0) as ybwfggsl,
        			ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
        			ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl
        			from ttvsample where fid in ('.implode(',', $arr_fsampleid).') and fmediaid in ('.implode(',', $arr_fmediaid).')';
        	$tv_res = M()->query($sql);	

        	//当月报刊发布违法情况
        	$sql = 'select count(*) total,
        			ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
        			ifnull(sum(case when fillegaltypecode>30 then 1 else 0 end),0) as yzwfggsl,
        			ifnull(sum(case when fillegaltypecode>20 then 1 else 0 end),0) as ybwfggsl,
        			ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
        			ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl
        			from tpapersample where fpapersampleid in ('.implode(',', $arr_fsampleid).') and fmediaid in ('.implode(',', $arr_fmediaid).')';
        	$paper_res = M()->query($sql);
        }


        //上月所有媒体发布广告违法情况
        if($l_res){
        	foreach($l_res as $v1){
        		$larr_fmediaid[] = $v1['fmediaid'];
        		$larr_fsampleid[] = $v1['fsampleid'];
        	}
        	//上月广播发布违法情况
        	$sql = 'select count(*) total,
        			ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
        			ifnull(sum(case when fillegaltypecode>30 then 1 else 0 end),0) as yzwfggsl,
        			ifnull(sum(case when fillegaltypecode>20 then 1 else 0 end),0) as ybwfggsl,
        			ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
        			ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl
        			from tbcsample where fid in ('.implode(',', $larr_fsampleid).') and fmediaid in ('.implode(',', $larr_fmediaid).')';
        	$lbc_res = M()->query($sql);

        	//上月电视发布违法情况
        	$sql = 'select count(*) total,
        			ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
        			ifnull(sum(case when fillegaltypecode>30 then 1 else 0 end),0) as yzwfggsl,
        			ifnull(sum(case when fillegaltypecode>20 then 1 else 0 end),0) as ybwfggsl,
        			ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
        			ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl
        			from ttvsample where fid in ('.implode(',', $larr_fsampleid).') and fmediaid in ('.implode(',', $larr_fmediaid).')';
        	$ltv_res = M()->query($sql);	

        	//上月报刊发布违法情况
        	$sql = 'select count(*) total,
        			ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
        			ifnull(sum(case when fillegaltypecode>30 then 1 else 0 end),0) as yzwfggsl,
        			ifnull(sum(case when fillegaltypecode>20 then 1 else 0 end),0) as ybwfggsl,
        			ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
        			ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl
        			from tpapersample where fpapersampleid in ('.implode(',', $larr_fsampleid).') and fmediaid in ('.implode(',', $larr_fmediaid).')';
        	$lpaper_res = M()->query($sql);
        }
        	
        //当月违法率
        $zdwf = $bc_res[0]['wfggsl']+$tv_res[0]['wfggsl']+$paper_res[0]['wfggsl'];
        $yzwf = $bc_res[0]['yzwfggsl']+$tv_res[0]['yzwfggsl']+$paper_res[0]['yzwfggsl'];
        $ybwf = $bc_res[0]['ybwfggsl']+$tv_res[0]['ybwfggsl']+$paper_res[0]['ybwfggsl'];
        $illegality = round(($zdwf/count($n_res))*100,2);
        //上月违法率
        $l_illegality = round((($lbc_res[0]['wfggsl']+$ltv_res[0]['wfggsl']+$lpaper_res[0]['wfggsl'])/count($l_res))*100,2);
        
        //环比增加
        $hb_ = count($n_res)-count($l_res);
        if($hb_==0){
        	$hb_str='无变化';
        }elseif($hb_>0){
        	$hb_str='增加'.$hb_.'条次';
        }else{
        	$hb_str='下降'.abs($hb_).'条次';
        }

        //违法率环比
        $hb_illegality = (($bc_res[0]['wfggsl']+$tv_res[0]['wfggsl']+$paper_res[0]['wfggsl']) - ($lbc_res[0]['wfggsl']+$ltv_res[0]['wfggsl']+$lpaper_res[0]['wfggsl']))/($lbc_res[0]['wfggsl']+$ltv_res[0]['wfggsl']+$lpaper_res[0]['wfggsl'])*100;
        if($hb_illegality==0){
        	$hb_illegality_str='无变化';
        }elseif($hb_illegality>0){
        	$hb_illegality_str='增加'.$hb_illegality.'个百分点';
        }else{
        	$hb_illegality_str='下降'.abs($hb_illegality).'个百分点';
        }
        //违法率同比
        $tb_illegality = ($illegality - $l_illegality)/$illegality;
        if($tb_illegality==0){
        	$tb_illegality_str='无变化';
        }elseif($tb_illegality>0){
        	$tb_illegality_str='增加'.$tb_illegality.'个百分点';
        }else{
        	$tb_illegality_str='下降'.abs($tb_illegality).'个百分点';
        }
        

		$html.= '<p>本月，监测中心监测媒体发布的各类广告'.count($n_res).'条次，环比'.$hb_str.'，其中涉嫌违法广告'.$zdwf.'条次（严重违法广告'.$yzwf.'条次，一般违法广告'.$ybwf.'条次），监测违法率为'.$illegality.'%，环比'.$hb_illegality_str.'。</p>';
		$html.= '<h4>(一)分类监测情况</h4>';

		$html .= ' 
    		<div align="center" border="1" style="font-size:18px; font-weight:bold; padding:10px;">市属媒体：</div> 
			<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
			<tr> 
			 <td>媒体类别</td> 
			 <td>监测条次</td> 
			 <td>环比</td> 
			 <td>涉嫌违法广告条次</td> 
			 <td>环比</td> 
			 <td>违法率</td> 
			 <td>环比</td> 
			 <td>严重违法</td>
			 <td>环比</td>
			 <td>严重违法率</td>
			 <td>环比</td>
			</tr>';
			$arr2 = ['↑','↓'];
			$regionids4 = [];
			$shi_res = [];
			$tv_regionid = [];
			$bc_regionid = [];
			$paper_regionid = [];
			$shitv_res = [];
			$shibc_res = [];
			$shipaper_res = [];
			$shiltv_res = [];
			$shilbc_res = [];
			$shilpaper_res = [];
			// dump($n_res);
			// dump($attach4);die;
			foreach($attach4 as $v){
				array_push($regionids4, $v['fid']);//用户具有权限的市媒体id
				//市属电视媒体id
				if(substr($v['fmediaclassid'], 0,2)=='01'){
					array_push($tv_regionid, $v['fid']);
				}
				//市属广播媒体id
				if(substr($v['fmediaclassid'], 0,2)=='02'){
					array_push($bc_regionid, $v['fid']);
				}

				//市属报刊媒体id
				if(substr($v['fmediaclassid'], 0,2)=='03'){
					array_push($paper_regionid, $v['fid']);
				}
			}

			//当月市属媒体所有违法广告信息
			if($n_res){
				foreach($n_res as $val){
					if(in_array($val['fmediaid'], $tv_regionid)){
						array_push($shitv_res, $val);//市属电视媒体所有违法广告信息
					}
					if(in_array($val['fmediaid'], $bc_regionid)){
						array_push($shibc_res, $val);//市属广播媒体所有违法广告信息
					}

					if(in_array($val['fmediaid'], $paper_regionid)){
						array_push($shipaper_res, $val);//市属广播媒体所有违法广告信息
					}
				}
			}
// dump($shibc_res);die;
			//上月市属媒体所有违法广告信息
			if($l_res){
				foreach($l_res as $val){
					if(in_array($val['fmediaid'], $tv_regionid)){
						array_push($shiltv_res, $val);//市属电视媒体所有违法广告信息
					}
					if(in_array($val['fmediaid'], $bc_regionid)){
						array_push($shilbc_res, $val);//市属广播媒体所有违法广告信息
					}

					if(in_array($val['fmediaid'], $paper_regionid)){
						array_push($shilpaper_res, $val);//市属广播媒体所有违法广告信息
					}
				}
			}

			
			//当月报刊监测情况
			if($shinpaper_res){
				foreach($shinpaper_res as $v){
        			$shinpaper_fmediaid[] = $v['fmediaid'];
        			$shinpaper_fsampleid[] = $v['fsampleid'];
        		}
				$sql = 'select count(*) total,bc.fmediaid,bc.fsampleid from tpaperissue bc where bc.fmediaid in ('.implode(',',$paper_regionid).') and date_format(bc.`fissuedate`,"%Y-%m")='.$date;
				$npa_total = M()->query($sql);//当月广播监测数
				$sql = 'select ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from tpapersample where fmediaid in ('.implode(',',$shipaper_fmediaid).') and fpapersampleid in ('.implode(',', $shipaper_fsampleid).')';
				$npa_list = M()->query($sql);
				$npa_list[0]['total'] = $npa_total[0]['total'];
			}
			if($shilpaper_res){
				foreach($shilpaper_res as $v1){
        			$shilpaper_fmediaid[] = $v1['fmediaid'];
        			$shilpaper_fsampleid[] = $v1['fsampleid'];
        		}
				//上月报刊监测情况
				$sql = 'select count(*) total from tpaperissue bc where bc.fmediaid in ('.implode(',',$paper_regionid).') and date_format(bc.`fissuedate`,"%Y-%m")='.$date_last;
				$lpa_total = M()->query($sql);//上月广播监测数
				$sql = 'select ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from tpapersample where fmediaid in ('.implode(',',$shilpaper_fmediaid).') and fpapersampleid in ('.implode(',', $shilpaper_fsampleid).')';
				$lpa_list = M()->query($sql);
				$lpa_list[0]['total'] = $npa_total[0]['total'];
			}
			if($shinpaper_res){
				$hb_total = $npa_list[0]['total'] - $lpa_list[0]['total'];
				if($hb_total==0){
					$hb_total = '无变化';
				}elseif ($hb_total>0) {
					$hb_total = $hb_total.$arr2[0];
				}else{
					$hb_total = $hb_total.$arr2[1];
				}
				$hb_wfggsl = $npa_list[0]['wfggsl'] - $lpa_list[0]['wfggsl'];
				if($hb_wfggsl==0){
					$hb_wfggsl = '无变化';
				}elseif ($hb_wfggsl>0) {
					$hb_wfggsl = $hb_wfggsl.$arr2[0];
				}else{
					$hb_wfggsl = $hb_wfggsl.$arr2[1];
				}
				$hb_ggslwfl = $npa_list[0]['ggslwfl']-$lpa_list[0]['ggslwfl'];
				if($hb_ggslwfl==0){
					$hb_ggslwfl = '无变化';
				}elseif ($hb_ggslwfl>0) {
					$hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[0];
				}else{
					$hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[1];
				}

				$hb_yzwfggsl = $npa_list[0]['yzwfggsl'] - $lpa_list[0]['yzwfggsl'];
				if($hb_yzwfggsl==0){
					$hb_yzwfggsl = '无变化';
				}elseif ($hb_yzwfggsl>0) {
					$hb_yzwfggsl = $hb_yzwfggsl.$arr2[0];
				}else{
					$hb_yzwfggsl = $hb_yzwfggsl.$arr2[1];
				}

				$hb_ggslyzwfl = $npa_list[0]['ggslyzwfl'] - $lpa_list[0]['ggslyzwfl'];
				if($hb_ggslyzwfl==0){
					$hb_ggslyzwfl = '无变化';
				}elseif ($hb_ggslyzwfl>0) {
					$hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[0];
				}else{
					$hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[1];
			}

				$html .= '<tr>';
				$html .= '<td>报刊</td>';
				$html .= '<td>'.$npa_list[0]['total'].'</td>';
				$html .= '<td>'.$hb_total.'</td>';
				$html .= '<td>'.$npa_list[0]['wfggsl'].'</td>';
				$html .= '<td>'.$hb_wfggsl.'</td>';
				$html .= '<td>'.round($npa_list[0]['ggslwfl']*100,2).'%</td>';
				$html .= '<td>'.$hb_ggslwfl.'</td>';
				$html .= '<td>'.$npa_list[0]['yzwfggsl'].'</td>';
				$html .= '<td>'.$hb_yzwfggsl.'</td>';
				$html .= '<td>'.round($npa_list[0]['ggslyzwfl']*100,2).'%</td>';
				$html .= '<td>'.$hb_ggslyzwfl.'</td></tr>';
			}


			//当月电视监测情况
			if($shitv_res){
				foreach($shitv_res as $v){
        			$shitv_fmediaid[] = $v['fmediaid'];
        			$shitv_fsampleid[] = $v['fsampleid'];
        		}
				$sql = 'select count(*) total from ttvissue_'.$now_date.' bc where bc.fmediaid in ('.join(',',$tv_regionid).')';
				$ntv_total = M()->query($sql);//当月电视监测情况
				$sql = 'select ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from ttvsample where fmediaid in ('.implode(',',$shitv_fmediaid).') and fid in ('.implode(',', $shitv_fsampleid).')';
				$ntv_list = M()->query($sql);
				$ntv_list[0]['total'] = $ntv_total[0]['total'];
			}
			if($shiltv_res){
				//上月电视监测情况
				foreach($shiltv_res as $v1){
        			$shiltv_fmediaid[] = $v1['fmediaid'];
        			$shiltv_fsampleid[] = $v1['fsampleid'];
        		}
				$sql = 'select count(*) total from ttvissue_'.$last_date.' bc where bc.fmediaid in ('.join(',',$tv_regionid).')';
				$ltv_total = M()->query($sql);//上月广播监测数
				$sql = 'select ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from ttvsample where fmediaid in ('.implode(',',$shiltv_fmediaid).') and fid in ('.implode(',', $shiltv_fsampleid).')';
				$ltv_list = M()->query($sql);
				$ltv_list[0]['total'] = $ltv_total[0]['total'];
			}
				
				if($shiltv_res){
					$hb_total = $ntv_list[0]['total'] - $ltv_list[0]['total'];
					if($hb_total==0){
						$hb_total = '无变化';
					}elseif ($hb_total>0) {
						$hb_total = $hb_total.$arr2[0];
					}else{
						$hb_total = $hb_total.$arr2[1];
					}
					$hb_wfggsl = $ntv_list[0]['wfggsl'] - $ltv_list[0]['wfggsl'];
					if($hb_wfggsl==0){
						$hb_wfggsl = '无变化';
					}elseif ($hb_wfggsl>0) {
						$hb_wfggsl = $hb_wfggsl.$arr2[0];
					}else{
						$hb_wfggsl = $hb_wfggsl.$arr2[1];
					}
					$hb_ggslwfl = $ntv_list[0]['ggslwfl']-$ltv_list[0]['ggslwfl'];
					if($hb_ggslwfl==0){
						$hb_ggslwfl = '无变化';
					}elseif ($hb_ggslwfl>0) {
						$hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[0];
					}else{
						$hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[1];
					}

					$hb_yzwfggsl = $ntv_list[0]['yzwfggsl'] - $ltv_list[0]['yzwfggsl'];
					if($hb_yzwfggsl==0){
						$hb_yzwfggsl = '无变化';
					}elseif ($hb_yzwfggsl>0) {
						$hb_yzwfggsl = $hb_yzwfggsl.$arr2[0];
					}else{
						$hb_yzwfggsl = $hb_yzwfggsl.$arr2[1];
					}

					$hb_ggslyzwfl = $ntv_list[0]['ggslyzwfl'] - $ltv_list[0]['ggslyzwfl'];
					if($hb_ggslyzwfl==0){
						$hb_ggslyzwfl = '无变化';
					}elseif ($hb_ggslyzwfl>0) {
						$hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[0];
					}else{
						$hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[1];
					}
				
					$html .= '<tr>';
					$html .= '<td>电视</td>';
					$html .= '<td>'.$ntv_list[0]['total'].'</td>';
					$html .= '<td>'.$hb_total.'</td>';
					$html .= '<td>'.$ntv_list[0]['wfggsl'].'</td>';
					$html .= '<td>'.$hb_wfggsl.'</td>';
					$html .= '<td>'.round($ntv_list[0]['ggslwfl']*100,2).'%</td>';
					$html .= '<td>'.$hb_ggslwfl.'</td>';
					$html .= '<td>'.$ntv_list[0]['yzwfggsl'].'</td>';
					$html .= '<td>'.$hb_yzwfggsl.'</td>';
					$html .= '<td>'.round($ntv_list[0]['ggslyzwfl']*100,2).'%</td>';
					$html .= '<td>'.$hb_ggslyzwfl.'</td></tr>';
			}

			

			//当月广播测试情况
			if($shibc_res){
				foreach($shibc_res as $v){
        			$shibc_fmediaid[] = $v['fmediaid'];
        			$shibc_fsampleid[] = $v['fsampleid'];
        		}
				$sql = 'select count(*) total from tbcissue_'.$now_date.' bc where bc.fmediaid in ('.join(',',$bc_regionid).')';
				$nbc_total = M()->query($sql);//当月广播监测数
				$sql = 'select ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from tbcsample where fmediaid in ('.implode(',',$shibc_fmediaid).') and fid in ('.implode(',', $shibc_fsampleid).')';
				$nbc_list = M()->query($sql);
				$nbc_list[0]['total'] = $nbc_total[0]['total'];
			}
			if($shilbc_res){
				//上月广播监测情况
				foreach($shilbc_res as $v1){
        			$shilbc_fmediaid[] = $v1['fmediaid'];
        			$shilbc_fsampleid[] = $v1['fsampleid'];
        		}
  
				$sql = 'select count(*) total from tbcissue_'.$last_date.' bc where bc.fmediaid in ('.implode(',',$bc_regionid).')';
				$lbc_total = M()->query($sql);//上月广播监测数
				$sql = 'select ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from tbcsample where fmediaid in ('.implode(',',$shilbc_fmediaid).') and fid in ('.implode(',', $shilbc_fsampleid).')';
				$lbc_list = M()->query($sql);
				$lbc_list[0]['total'] = $lbc_total[0]['total'];
			}
			if($shibc_res){
				$hb_total = $nbc_list[0]['total'] - $lbc_list[0]['total'];
				if($hb_total==0){
					$hb_total = '无变化';
				}elseif ($hb_total>0) {
					$hb_total = $hb_total.$arr2[0];
				}else{
					$hb_total = $hb_total.$arr2[1];
				}
				$hb_wfggsl = $nbc_list[0]['wfggsl'] - $lbc_list[0]['wfggsl'];
				if($hb_wfggsl==0){
					$hb_wfggsl = '无变化';
				}elseif ($hb_wfggsl>0) {
					$hb_wfggsl = $hb_wfggsl.$arr2[0];
				}else{
					$hb_wfggsl = $hb_wfggsl.$arr2[1];
				}
				$hb_ggslwfl = $nbc_list[0]['ggslwfl']-$lbc_list[0]['ggslwfl'];
				if($hb_ggslwfl==0){
					$hb_ggslwfl = '无变化';
				}elseif ($hb_ggslwfl>0) {
					$hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[0];
				}else{
					$hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[1];
				}

				$hb_yzwfggsl = $nbc_list[0]['yzwfggsl'] - $lbc_list[0]['yzwfggsl'];
				if($hb_yzwfggsl==0){
					$hb_yzwfggsl = '无变化';
				}elseif ($hb_yzwfggsl>0) {
					$hb_yzwfggsl = $hb_yzwfggsl.$arr2[0];
				}else{
					$hb_yzwfggsl = $hb_yzwfggsl.$arr2[1];
				}

				$hb_ggslyzwfl = $nbc_list[0]['ggslyzwfl'] - $lbc_list[0]['ggslyzwfl'];
				if($hb_ggslyzwfl==0){
					$hb_ggslyzwfl = '无变化';
				}elseif ($hb_ggslyzwfl>0) {
					$hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[0];
				}else{
					$hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[1];
				}
			
				$html .= '<tr>';
				$html .= '<td>广播</td>';
				$html .= '<td>'.$nbc_list[0]['total'].'</td>';
				$html .= '<td>'.$hb_total.'</td>';
				$html .= '<td>'.$nbc_list[0]['wfggsl'].'</td>';
				$html .= '<td>'.$hb_wfggsl.'</td>';
				$html .= '<td>'.round($nbc_list[0]['ggslwfl']*100,2).'%</td>';
				$html .= '<td>'.$hb_ggslwfl.'</td>';
				$html .= '<td>'.$nbc_list[0]['yzwfggsl'].'</td>';
				$html .= '<td>'.$hb_yzwfggsl.'</td>';
				$html .= '<td>'.round($nbc_list[0]['ggslyzwfl']*100,2).'%</td>';
				$html .= '<td>'.$hb_ggslyzwfl.'</td></tr>';
			}

			$html .= '</table>';
			
			foreach ($attach4 as $key => &$value2) {
			if(substr($value2['fmediaclassid'], 0,2)=='01'){
				$value2['adv_totals'] += M('ttvissue_'.$now_date)->where('fmediaid='.$value2['fid'])->count();
			}
			if(substr($value2['fmediaclassid'], 0,2)=='02'){
				$value2['adv_totals'] += M('tbcissue_'.$now_date)->where('fmediaid='.$value2['fid'])->count();
			}
			if(substr($value2['fmediaclassid'], 0,2)=='03'){
				$value2['adv_totals'] += M('tpaperissue')->where('fmediaid='.$value2['fid'].' and date_format(`fissuedate`,"%Y-%m")='.$date)->count();
			}
			$value2['illegal_total'] += M('tillegalad')->where(array('fmediaid'=>$value2['fid'],'fillegaltypecode'=>array('gt',10)))->count();//违法数
			$value2['serious_total'] += M('tillegalad')->where(array('fmediaid'=>$value2['fid'],'fillegaltypecode'=>30))->count();//严重违法数
			$value2['commonly_total'] += M('tillegalad')->where(array('fmediaid'=>$value2['fid'],'fillegaltypecode'=>20))->count();//一般违法数
		}
// dump($attach2);die;
		$arr1 = array('01'=>'电视','02'=>'广播','03'=>'报刊');
		foreach($attach4 as $value4){
			$mlist1[substr($value4['fmediaclassid'], 0,2)][] = $value4;
		}
		foreach($mlist1 as $key => $value){
			$html.= '<p>'.$arr1[$key].'媒体：</p>';
			$html .= ' 
			<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
			<tr> 
			 <td>序号</td> 
			 <td>媒体名称</td> 
			 <td>监测数</td> 
			 <td>总违法数</td> 
			 <td>严重违法数</td> 
			 <td>严重违法率</td> 
			 <td>一般违法</td> 
			 <td>监测违法率</td>
			</tr>';
			foreach($value as $key => $val){
				$adv_totals1 += $val['adv_totals'];
				$illegal_total1 += $val['illegal_total'];
				$serious_total1 += $val['serious_total'];
				$commonly_total1 += $val['commonly_total'];
				$num = $key + 1;
				$html .= '<tr>';
				$html .= '<td>'.$num.'</td>';
				$html .= '<td>'.$val['fmedianame'].'</td>';
				$html .= '<td>'.$val['adv_totals'].'</td>';
				$html .= '<td>'.$val['illegal_total'].'</td>';
				$html .= '<td>'.$val['serious_total'].'</td>';
				$html .= '<td>'.round(($val['serious_total']/$val['adv_totals'])*100,2).'%</td>';
				$html .= '<td>'.$val['commonly_total'].'</td>';
				$html .= '<td>'.round(($val['illegal_total']/$val['adv_totals'])*100,2).'%</td></tr>';
			}
			$num = $num+1;
				$html .= '<tr>';
				$html .= '<td>'.$num.'</td>';
				$html .= '<td>小计</td>';
				$html .= '<td>'.$adv_totals1.'</td>';
				$html .= '<td>'.$illegal_total1.'</td>';
				$html .= '<td>'.$serious_total1.'</td>';
				$html .= '<td>'.round(($serious_total1/$ad_totals1)*100,2).'%</td>';
				$html .= '<td>'.$commonly_total1.'</td>';
				$html .= '<td>'.round(($commonly_total1/$ad_totals1)*100,2).'%</td></tr>';
		}
		$html .= '</table>';

		//南京电视媒体公益广告发布情况
		$sql = 'select count(*) as total,m.fid,m.fmedianame,
			sum(case when FROM_UNIXTIME(fstarttime, "%H:%i") >= "17:00" and FROM_UNIXTIME(fstarttime, "%H:%i") <= "21:00" then 1 else 0 end) as wfggsl
			from tmedia as m join ttvissue_'.$now_date.' as bc on m.fid=bc.fmediaid where m.fid in ('.join(',',$regionids4).') group by m.fid';
		$result = M()->query($sql);
		if($result){
			$html .= '<p>南京电视媒体公益广告发布情况表</p>';
        	$html.= '  
				<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
				<tr> 
				 <td rowspan=2  >媒体名称</td>  
				 <td colspan=4  style="border-bottom:1px solid #333333;">时间段（发布条次）</td>  
				</tr> 
				<tr>
					<td>11:00-13:00</td>
				</tr>
				';
				foreach($result as $v1){
					$html.= '   
					<tr>
						<td>'.$v1['fmedianame'].'</td>
						<td>'.$v1['tv_total'].'</td>
					</tr>
					';
				}
			$html .= '</table>';
		}

		//南京广播媒体公益广告发布情况
		$sql = 'select count(*) as total,m.fid,m.fmedianame,
			sum(case when FROM_UNIXTIME(fstarttime, "%H:%i") >= "06:00" and FROM_UNIXTIME(fstarttime, "%H:%i") <= "08:00" then 1 else 0 end) as wfggsl,
			sum(case when FROM_UNIXTIME(fstarttime, "%H:%i") >= "11:00" and FROM_UNIXTIME(fstarttime, "%H:%i") <= "13:00" then 1 else 0 end) as wfggsl1
			from tmedia as m join tbcissue_'.$now_date.' as bc on m.fid=bc.fmediaid where m.fid in ('.join(',',$regionids4).') group by m.fid';
		$result = M()->query($sql);
		if($result){
			$html .= '<p>南京广播媒体公益广告发布情况表</p>';
	        $html.= '  
				<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
				<tr> 
				 <td rowspan=2  >媒体名称</td>  
				 <td colspan=4  style="border-bottom:1px solid #333333;">时间段（发布条次）</td>  
				</tr> 
				<tr>
					<td>6:00-8:00</td>
					<td>11:00-13:00</td>
				</tr>
			';
			foreach($result as $v1){
				$html.= '   
				<tr>
					<td>'.$v1['fmedianame'].'</td>
					<td>'.$v1['wfggsl'].'</td>
					<td>'.$v1['wfggsl1'].'</td>
				</tr>
			';
			}
			$html .= '</table>';
		}
		
		//南京报刊媒体公益广告发布情况
		$sql = 'select count(*) as total,m.fid,m.fmedianame
			from tmedia as m join tpaperissue as bc on m.fid=bc.fmediaid where m.fid in ('.join(',',$regionids4).') and date_format(bc.`fissuedate`,"%Y-%m")='.$date.' group by m.fid';
		$result = M()->query($sql);
		if($result){
			$html .= '<p>南京报刊媒体公益广告发布情况表</p>';
	        $html.= '  
				<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
				<tr> 
				<td>媒体名称</td>  
				<td>广告条次</td>
				</tr>
			';
			foreach($result as $v1){
				$html.= '   
				<tr>
					<td>'.$v1['fmedianame'].'</td>
					<td>'.$v1['total'].'</td>
				</tr>
			';
			}
		
			$html .= '</table>';
		}

		//省属媒体信息

		$html .= ' 
    		<div align="center" style="font-size:18px; font-weight:bold; padding:10px;">省属媒体：</div> 
			<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
			<tr> 
			 <td>媒体类别</td> 
			 <td>监测条次</td> 
			 <td>环比</td> 
			 <td>涉嫌违法广告条次</td> 
			 <td>环比</td> 
			 <td>违法率</td> 
			 <td>环比</td> 
			 <td>严重违法</td>
			 <td>环比</td>
			 <td>严重违法率</td>
			 <td>环比</td>
			</tr>';
			$tv_regionid2 = [];
			$bc_regionid2 = [];
			$paper_regionid2 = [];
			foreach($attach2 as $val){
				//省属电视媒体id
				if(substr($val['fmediaclassid'], 0,2)=='01'){
					array_push($tv_regionid2, $val['fid']);
				}
				//省属广播媒体id
				if(substr($val['fmediaclassid'], 0,2)=='02'){
					array_push($bc_regionid2, $val['fid']);
				}

				//省属报刊媒体id
				if(substr($val['fmediaclassid'], 0,2)=='03'){
					array_push($paper_regionid2, $val['fid']);
				}
			}
		// dump($tv_regionid2);dump($bc_regionid2);dump($paper_regionid2);die;

			//当月报刊监测情况
			if($paper_regionid2){
				$sql = 'select count(*) total from tpaperissue bc where bc.fmediaid in ('.join(',',$paper_regionid2).') and date_format(bc.`fissuedate`,"%Y-%m")='.$date;
				$npa_total = M()->query($sql);//当月报刊监测数
				$sql = 'select ifnull(sum(case when ig.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when ig.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when ig.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when ig.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from tpaperissue bc join tillegalad ig on bc.fmediaid=ig.fmediaid where bc.fmediaid in ('.join(',',$paper_regionid2).') and date_format(bc.`fissuedate`,"%Y-%m")='.$date;
				$npa_list1 = M()->query($sql);
				$npa_list1[0]['total'] = $npa_total[0]['total'];
			
				//上月报刊监测情况
				$sql = 'select count(*) total from tpaperissue bc where bc.fmediaid in ('.join(',',$paper_regionid).') and date_format(bc.`fissuedate`,"%Y-%m")='.$date_last;
				$lpa_total = M()->query($sql);//上月报刊监测数
				$sql = 'select ifnull(sum(case when ig.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when ig.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when ig.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when ig.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from tpaperissue bc join tillegalad ig on bc.fmediaid=ig.fmediaid where bc.fmediaid in ('.join(',',$paper_regionid).') and date_format(bc.`fissuedate`,"%Y-%m")='.$date;
				$lpa_list1 = M()->query($sql);
				$lpa_list1[0]['total'] = $npa_total[0]['total'];
				$hb_total = $npa_list1[0]['total'] - $lpa_list1[0]['total'];
				if($hb_total==0){
					$hb_total = '无变化';
				}elseif ($hb_total>0) {
					$hb_total = $hb_total.$arr2[0];
				}else{
					$hb_total = $hb_total.$arr2[1];
				}
				$hb_wfggsl = $npa_list1[0]['wfggsl'] - $lpa_list1[0]['wfggsl'];
				if($hb_wfggsl==0){
					$hb_wfggsl = '无变化';
				}elseif ($hb_wfggsl>0) {
					$hb_wfggsl = $hb_wfggsl.$arr2[0];
				}else{
					$hb_wfggsl = $hb_wfggsl.$arr2[1];
				}
				$hb_ggslwfl = $npa_list1[0]['ggslwfl']-$lpa_list1[0]['ggslwfl'];
				if($hb_ggslwfl==0){
					$hb_ggslwfl = '无变化';
				}elseif ($hb_ggslwfl>0) {
					$hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[0];
				}else{
					$hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[1];
				}

				$hb_yzwfggsl = $npa_list1[0]['yzwfggsl'] - $lpa_list1[0]['yzwfggsl'];
				if($hb_yzwfggsl==0){
					$hb_yzwfggsl = '无变化';
				}elseif ($hb_yzwfggsl>0) {
					$hb_yzwfggsl = $hb_yzwfggsl.$arr2[0];
				}else{
					$hb_yzwfggsl = $hb_yzwfggsl.$arr2[1];
				}

				$hb_ggslyzwfl = $npa_list1[0]['ggslyzwfl'] - $lpa_list1[0]['ggslyzwfl'];
				if($hb_ggslyzwfl==0){
					$hb_ggslyzwfl = '无变化';
				}elseif ($hb_ggslyzwfl>0) {
					$hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[0];
				}else{
					$hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[1];
			}

				$html .= '<tr>';
				$html .= '<td>报刊</td>';
				$html .= '<td>'.$nbc_list[0]['total'].'</td>';
				$html .= '<td>'.$hb_total.'</td>';
				$html .= '<td>'.$nbc_list[0]['wfggsl'].'</td>';
				$html .= '<td>'.$hb_wfggsl.'</td>';
				$html .= '<td>'.round($nbc_list[0]['ggslwfl']*100,2).'%</td>';
				$html .= '<td>'.$hb_ggslwfl.'</td>';
				$html .= '<td>'.$nbc_list[0]['yzwfggsl'].'</td>';
				$html .= '<td>'.$hb_yzwfggsl.'</td>';
				$html .= '<td>'.round($nbc_list[0]['ggslyzwfl']*100,2).'%</td>';
				$html .= '<td>'.$hb_ggslyzwfl.'</td></tr>';
			}


			//当月电视监测情况
			if($tv_regionid){
				$sql = 'select count(*) total from ttvissue_'.$now_date.' bc where bc.fmediaid in ('.join(',',$tv_regionid2).')';
				$ntv_total = M()->query($sql);//当月广播监测数
				$sql = 'select ifnull(sum(case when ig.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when ig.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when ig.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when ig.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from ttvissue_'.$now_date.' bc join tillegalad ig on bc.fmediaid=ig.fmediaid where bc.fmediaid in ('.join(',',$tv_regionid2).')';
				$ntv_list1 = M()->query($sql);
				$ntv_list1[0]['total'] = $ntv_total[0]['total'];
			
				//上月广播监测情况
				$sql = 'select count(*) total from ttvissue_'.$last_date.' bc where bc.fmediaid in ('.join(',',$tv_regionid2).')';
				$ltv_total = M()->query($sql);//上月广播监测数
				$sql = 'select ifnull(sum(case when ig.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when ig.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when ig.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when ig.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from ttvissue_'.$last_date.' bc join tillegalad ig on bc.fmediaid=ig.fmediaid where bc.fmediaid in ('.join(',',$tv_regionid2).')';
				$ltv_list1 = M()->query($sql);
				$ltv_list1[0]['total'] = $ltv_total[0]['total'];
				$hb_total = $ntv_list1[0]['total'] - $ltv_list1[0]['total'];
				if($hb_total==0){
					$hb_total = '无变化';
				}elseif ($hb_total>0) {
					$hb_total = $hb_total.$arr2[0];
				}else{
					$hb_total = $hb_total.$arr2[1];
				}
				$hb_wfggsl = $ntv_list1[0]['wfggsl'] - $ltv_list1[0]['wfggsl'];
				if($hb_wfggsl==0){
					$hb_wfggsl = '无变化';
				}elseif ($hb_wfggsl>0) {
					$hb_wfggsl = $hb_wfggsl.$arr2[0];
				}else{
					$hb_wfggsl = $hb_wfggsl.$arr2[1];
				}
				$hb_ggslwfl = $ntv_list1[0]['ggslwfl']-$ltv_list1[0]['ggslwfl'];
				if($hb_ggslwfl==0){
					$hb_ggslwfl = '无变化';
				}elseif ($hb_ggslwfl>0) {
					$hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[0];
				}else{
					$hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[1];
				}

				$hb_yzwfggsl = $ntv_list1[0]['yzwfggsl'] - $ltv_list1[0]['yzwfggsl'];
				if($hb_yzwfggsl==0){
					$hb_yzwfggsl = '无变化';
				}elseif ($hb_yzwfggsl>0) {
					$hb_yzwfggsl = $hb_yzwfggsl.$arr2[0];
				}else{
					$hb_yzwfggsl = $hb_yzwfggsl.$arr2[1];
				}

				$hb_ggslyzwfl = $ntv_list1[0]['ggslyzwfl'] - $ltv_list1[0]['ggslyzwfl'];
				if($hb_ggslyzwfl==0){
					$hb_ggslyzwfl = '无变化';
				}elseif ($hb_ggslyzwfl>0) {
					$hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[0];
				}else{
					$hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[1];
				}
			
				$html .= '<tr>';
				$html .= '<td>电视</td>';
				$html .= '<td>'.$nbc_list[0]['total'].'</td>';
				$html .= '<td>'.$hb_total.'</td>';
				$html .= '<td>'.$nbc_list[0]['wfggsl'].'</td>';
				$html .= '<td>'.$hb_wfggsl.'</td>';
				$html .= '<td>'.round($nbc_list[0]['ggslwfl']*100,2).'%</td>';
				$html .= '<td>'.$hb_ggslwfl.'</td>';
				$html .= '<td>'.$nbc_list[0]['yzwfggsl'].'</td>';
				$html .= '<td>'.$hb_yzwfggsl.'</td>';
				$html .= '<td>'.round($nbc_list[0]['ggslyzwfl']*100,2).'%</td>';
				$html .= '<td>'.$hb_ggslyzwfl.'</td></tr>';
			}

			

			//当月广播测试情况
			if($bc_regionid){
				$sql = 'select count(*) total from tbcissue_'.$now_date.' bc where bc.fmediaid in ('.join(',',$bc_regionid2).')';
				$nbc_total = M()->query($sql);//当月广播监测数
				$sql = 'select ifnull(sum(case when ig.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when ig.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when ig.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when ig.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from tbcissue_'.$now_date.' bc join tillegalad ig on bc.fmediaid=ig.fmediaid where bc.fmediaid in ('.join(',',$bc_regionid2).')';
				$nbc_list = M()->query($sql);
				$nbc_list[0]['total'] = $nbc_total[0]['total'];
			
				//上月广播监测情况
				$sql = 'select count(*) total from tbcissue_'.$last_date.' bc where bc.fmediaid in ('.join(',',$bc_regionid2).')';
				$lbc_total = M()->query($sql);//上月广播监测数
				$sql = 'select ifnull(sum(case when ig.fillegaltypecode>0 then 1 else 0 end),0) as wfggsl,
						ifnull(sum(case when ig.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl,
						ifnull(sum(case when ig.fillegaltypecode=30 then 1 else 0 end),0) as yzwfggsl,
						ifnull(sum(case when ig.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl from tbcissue_'.$last_date.' bc join tillegalad ig on bc.fmediaid=ig.fmediaid where bc.fmediaid in ('.join(',',$bc_regionid2).')';
				$lbc_list1 = M()->query($sql);
				$lbc_list1[0]['total'] = $lbc_total[0]['total'];
				$hb_total = $nbc_list1[0]['total'] - $lbc_list1[0]['total'];
				if($hb_total==0){
					$hb_total = '无变化';
				}elseif ($hb_total>0) {
					$hb_total = $hb_total.$arr2[0];
				}else{
					$hb_total = $hb_total.$arr2[1];
				}
				$hb_wfggsl = $nbc_list1[0]['wfggsl'] - $lbc_list1[0]['wfggsl'];
				if($hb_wfggsl==0){
					$hb_wfggsl = '无变化';
				}elseif ($hb_wfggsl>0) {
					$hb_wfggsl = $hb_wfggsl.$arr2[0];
				}else{
					$hb_wfggsl = $hb_wfggsl.$arr2[1];
				}
				$hb_ggslwfl = $nbc_list1[0]['ggslwfl']-$lbc_list1[0]['ggslwfl'];
				if($hb_ggslwfl==0){
					$hb_ggslwfl = '无变化';
				}elseif ($hb_ggslwfl>0) {
					$hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[0];
				}else{
					$hb_ggslwfl = round($hb_ggslwfl*100,2).$arr2[1];
				}

				$hb_yzwfggsl = $nbc_list1[0]['yzwfggsl'] - $lbc_list1[0]['yzwfggsl'];
				if($hb_yzwfggsl==0){
					$hb_yzwfggsl = '无变化';
				}elseif ($hb_yzwfggsl>0) {
					$hb_yzwfggsl = $hb_yzwfggsl.$arr2[0];
				}else{
					$hb_yzwfggsl = $hb_yzwfggsl.$arr2[1];
				}

				$hb_ggslyzwfl = $nbc_list1[0]['ggslyzwfl'] - $lbc_list1[0]['ggslyzwfl'];
				if($hb_ggslyzwfl==0){
					$hb_ggslyzwfl = '无变化';
				}elseif ($hb_ggslyzwfl>0) {
					$hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[0];
				}else{
					$hb_ggslyzwfl = round($hb_ggslyzwfl*100,2).$arr2[1];
				}
			
				$html .= '<tr>';
				$html .= '<td>广播</td>';
				$html .= '<td>'.$nbc_list[0]['total'].'</td>';
				$html .= '<td>'.$hb_total.'</td>';
				$html .= '<td>'.$nbc_list[0]['wfggsl'].'</td>';
				$html .= '<td>'.$hb_wfggsl.'</td>';
				$html .= '<td>'.round($nbc_list[0]['ggslwfl']*100,2).'%</td>';
				$html .= '<td>'.$hb_ggslwfl.'</td>';
				$html .= '<td>'.$nbc_list[0]['yzwfggsl'].'</td>';
				$html .= '<td>'.$hb_yzwfggsl.'</td>';
				$html .= '<td>'.round($nbc_list[0]['ggslyzwfl']*100,2).'%</td>';
				$html .= '<td>'.$hb_ggslyzwfl.'</td></tr>';
			}

			$html .= '</table>';

			
		foreach ($attach2 as $key => &$value2) {
			if(substr($value2['fmediaclassid'], 0,2)=='01'){
				$value2['adv_totals'] += M('ttvissue_'.$now_date)->where('fmediaid='.$value2['fid'])->count();
			}
			if(substr($value2['fmediaclassid'], 0,2)=='02'){
				$value2['adv_totals'] += M('tbcissue_'.$now_date)->where('fmediaid='.$value2['fid'])->count();
			}
			if(substr($value2['fmediaclassid'], 0,2)=='03'){
				$value2['adv_totals'] += M('tpaperissue')->where('fmediaid='.$value2['fid'].' and date_format(`fissuedate`,"%Y-%m")='.$date)->count();
			}
			$value2['illegal_total'] += M('tillegalad')->where(array('fmediaid'=>$value2['fid'],'fillegaltypecode'=>array('gt',10)))->count();//违法数
			$value2['serious_total'] += M('tillegalad')->where(array('fmediaid'=>$value2['fid'],'fillegaltypecode'=>30))->count();//严重违法数
			$value2['commonly_total'] += M('tillegalad')->where(array('fmediaid'=>$value2['fid'],'fillegaltypecode'=>20))->count();//一般违法数
		}
// dump($attach2);die;
		$arr1 = array('01'=>'电视','02'=>'广播','03'=>'报刊');
		foreach($attach2 as $value4){
			$mlist2[substr($value4['fmediaclassid'], 0,2)][] = $value4;
		}
		foreach($mlist2 as $key => $value){
			$html.= '<p>'.$arr1[$key].'媒体：</p>';
			$html .= ' 
			<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
			<tr> 
			 <td>序号</td> 
			 <td>媒体名称</td> 
			 <td>监测数</td> 
			 <td>总违法数</td> 
			 <td>严重违法数</td> 
			 <td>严重违法率</td> 
			 <td>一般违法</td> 
			 <td>监测违法率</td>
			</tr>';
			foreach($value as $key => $val){
				$adv_totals2 += $val['adv_totals'];
				$illegal_total2 += $val['illegal_total'];
				$serious_total2 += $val['serious_total'];
				$commonly_total2 += $val['commonly_total'];
				$num = $key + 1;
				$html .= '<tr>';
				$html .= '<td>'.$num.'</td>';
				$html .= '<td>'.$val['fmedianame'].'</td>';
				$html .= '<td>'.$val['adv_totals'].'</td>';
				$html .= '<td>'.$val['illegal_total'].'</td>';
				$html .= '<td>'.$val['serious_total'].'</td>';
				$html .= '<td>'.round(($val['serious_total']/$val['adv_totals'])*100,2).'%</td>';
				$html .= '<td>'.$val['commonly_total'].'</td>';
				$html .= '<td>'.round(($val['illegal_total']/$val['adv_totals'])*100,2).'%</td></tr>';
			}
			$num = $num+1;
				$html .= '<tr>';
				$html .= '<td>'.$num.'</td>';
				$html .= '<td>小计</td>';
				$html .= '<td>'.$adv_totals2.'</td>';
				$html .= '<td>'.$illegal_total2.'</td>';
				$html .= '<td>'.$serious_total2.'</td>';
				$html .= '<td>'.round(($serious_total2/$ad_totals2)*100,2).'%</td>';
				$html .= '<td>'.$commonly_total1.'</td>';
				$html .= '<td>'.round(($commonly_total2/$ad_totals2)*100,2).'%</td></tr>';
		}
		$html .= '</table>';

		$html.= '<h3>（二）行业监测情况</h3>';
		$where1 = 'where ac.fpcode=" "';
		// $where1 .= ' and bc.fmediaid in ('.join(',',$regionids).')';
		$sql = 'select 
				count(*) total, ac.fadclassid,ac.fadclass,ac.fcode,
				sum(case when bs.fillegaltypecode=20 then 1 else 0 end) as ybwfggsl,
				sum(case when bs.fillegaltypecode=30 then 1 else 0 end) as yzwfggsl,
				ifnull(sum(case when bs.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl,
				sum(case when bs.fillegaltypecode>0 then 1 else 0 end) as wfggsl,
				ifnull(sum(case when bs.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl
				from tadclass as ac left join tad a on ac.fcode=a.fadclasscode left join ((select fid,fadid,fmediaid,fillegaltypecode from tbcsample)
				union all (select fid,fadid,fmediaid,fillegaltypecode from ttvsample)
				union all (select fpapersampleid as fid,fadid,fmediaid,fillegaltypecode from tpapersample)) as bs on
				a.fadid=bs.fadid left join ((select fsampleid,fmediaid from tbcissue_'.$now_date.' where fmediaid in ('.join(',',$regionids).')) 
				union all (select fsampleid,fmediaid from ttvissue_'.$now_date.' where fmediaid in ('.join(',',$regionids).')) 
				union all (select fpapersampleid as fsampleid,fmediaid from tpaperissue where date_format(`fissuedate`,"%Y-%m")='.$date.' and fmediaid in ('.join(',',$regionids).')))
				as bc on bs.fid=bc.fsampleid and bs.fmediaid=bc.fmediaid '.$where1.' group by ac.fadclassid order by ggslwfl desc';
		$sql1 = 'select 
				count(*) total, ac.fadclassid,ac.fadclass,ac.fcode,
				sum(case when bs.fillegaltypecode=20 then 1 else 0 end) as ybwfggsl,
				sum(case when bs.fillegaltypecode=30 then 1 else 0 end) as yzwfggsl,
				ifnull(sum(case when bs.fillegaltypecode=30 then 1 else 0 end)/count(*),0) as ggslyzwfl,
				sum(case when bs.fillegaltypecode>0 then 1 else 0 end) as wfggsl,
				ifnull(sum(case when bs.fillegaltypecode>0 then 1 else 0 end)/count(*),0) as ggslwfl
				from tadclass as ac left join tad a on ac.fcode=a.fadclasscode left join ((select fid,fadid,fmediaid,fillegaltypecode from tbcsample)
				union all (select fid,fadid,fmediaid,fillegaltypecode from ttvsample)
				union all (select fpapersampleid as fid,fadid,fmediaid,fillegaltypecode from tpapersample)) as bs on
				a.fadid=bs.fadid left join ((select fsampleid,fmediaid from tbcissue_'.$last_date.' where fmediaid in ('.join(',',$regionids).')) 
				union all (select fsampleid,fmediaid from ttvissue_'.$last_date.' where fmediaid in ('.join(',',$regionids).')) 
				union all (select fpapersampleid as fsampleid,fmediaid from tpaperissue where date_format(`fissuedate`,"%Y-%m")='.$date_last.' and fmediaid in ('.join(',',$regionids).')))
				as bc on bs.fid=bc.fsampleid and bs.fmediaid=bc.fmediaid '.$where1.' group by ac.fadclassid order by ggslwfl desc';
		// echo $sql;
		$do_ie = M()->query($sql);//获取统计结果
		$last_doie = M()->query($sql1);
		//dump(M()->getLastSql());
		// dump(123);
		// dump($last_doie);
		$html.= '8大重点行业情况表：';
		$array1 = ['13','16','08','06','04','03','02','01'];
		// dump($do_ie);
		// dump($array1);
		$array2 = ['↑','↓'];
		$do_ie1 = [];
		$last_doie1 = [];
		foreach($do_ie as $k=>$val){
			if(in_array($val['fcode'],$array1)){
				array_push($do_ie1, $val);
			}
		}
		foreach($last_doie as $k=>$val){
			if(in_array($val['fcode'],$array1)){
				array_push($last_doie1, $val);
			}
		}
		foreach($last_doie1 as $v2){
			$lastd_totals += $v2['total'];//上月监测总量
			$lastd_wfggsls += $v2['wfggsl'];//上月违法总量
		}
		$lastd_ggslwfl = $lastd_wfggsls/$lastd_totals;//上月总违法率
		// dump($lastd_ggslwfl);
		// dump($do_ie1[0]);dump($last_doie1);die;
		foreach($do_ie1 as &$v){
			foreach($last_doie1 as $v1){
				if($v['fadclassid']==$v1['fadclassid']){
					$v['total_'] = ($v['total'] - $v1['total']==0)?'无变化':(($v['total'] - $v1['total'])>0?$v['total'] - $v1['total'].$array2[0]:(abs($v['total'] - $v1['total'])).$array2[1]);
					$v['wfggsl_'] = ($v['wfggsl'] - $v1['wfggsl']==0)?'无变化':(($v['wfggsl'] - $v1['wfggsl'])>0?$v['wfggsl'] - $v1['wfggsl'].$array2[0]:(abs($v['wfggsl'] - $v1['wfggsl'])).$array2[1]);
					$v['ggslwfl_'] = $v['ggslwfl'] - $v1['ggslwfl'];
				}
			}
		}
		// dump($lastd_wfggsls);dump($lastd_totals);die;
// dump($do_ie1);die;
		$html .= ' 
			<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
			<tr>
			<td>主类别</td> 
			<td>监测条次</td> 
			<td>环比</td> 
			<td>涉嫌违法广告条次</td> 
			<td>环比</td> 
			<td>违法率</td> 
			<td>环比</td>
			</tr>';
		
			foreach($do_ie1 as $key => $val){
				$ad_totals += $val['total'];
				$ad_wfggsls += $val['wfggsl'];
				$html .= '
				<tr> 
					<td>'.$val['fadclass'].'</td> 
					<td>'.$val['total'].'</td> 
					<td>'.$val['total_'].'</td> 
					<td>'.$val['wfggsl'].'</td> 
					<td>'.$val['wfggsl_'].'</td> 
					<td>'.round($val['ggslwfl']*100,2).'%</td> ';
					if($val['ggslwfl_']==0){
						$hb_ggslwfl_ = '无变化';
					}elseif($val['ggslwfl_']>0){
						$hb_ggslwfl_ = round($val['ggslwfl_']*100,2).'%'.$array2[0];
					}else{
						$hb_ggslwfl_ = round(abs($val['ggslwfl_'])*100,2).'%'.$array2[1];
					}
					$html.='
					<td>'.$hb_ggslwfl_.'</td>
				</tr>';
			}
			$ad_ggslwfl = $ad_wfggsls/$ad_totals;//当月违法率
			$hb_ggslwfl = $ad_ggslwfl - $lastd_ggslwfl;//违法率环比

			if($hb_ggslwfl==0){
				$hb_ggslwfl = '无变化';
			}elseif($hb_ggslwfl>0){
				$hb_ggslwfl = round($hb_ggslwfl*100,2).'%'.$array2[0];
			}else{
				$hb_ggslwfl = round(abs($hb_ggslwfl)*100,2).'%'.$array2[1];
			}

			if($ad_totals - $lastd_totals==0){
				$hb_totals = '无变化';
			}elseif($ad_totals - $lastd_totals>0){
				$hb_totals = $ad_totals - $lastd_totals.$array2[0];
			}else{
				$hb_totals = abs($ad_totals - $lastd_totals).$array2[1];
			}

			if($ad_wfggsls - $lastd_wfggsls==0){
				$hb_wfggsls = '无变化';
			}elseif($ad_wfggsls - $lastd_wfggsls>0){
				$hb_wfggsls = $ad_wfggsls - $lastd_wfggsls.$array2[0];
			}else{
				$hb_wfggsls = abs($ad_wfggsls - $lastd_wfggsls).$array2[1];
			}

			$html .= '<tr>';
			$html .= '<td>八类重点行业累计</td>';
			$html .= '<td>'.$ad_totals.'</td>';
			$html .= '<td>'.$hb_totals.'</td>';
			$html .= '<td>'.$ad_wfggsls.'</td>';
			$html .= '<td>'.$hb_wfggsls.'</td>';
			$html .= '<td>'.round($ad_ggslwfl*100,2).'%</td>';
			$html .= '<td>'.$hb_ggslwfl.'</td></tr>';
			

		$html .= '</table>';

		$html.= '23大类广告情况表（按违法率排名）：';

		$html .= ' 
			<table style="width:100%;border:1px solid #666666; margin-top:20px;" align="center" class="tb"> 
			<tr>
			<td>序号</td>
			<td>主类别</td> 
			<td>监测数</td> 
			<td>总违法数</td> 
			<td>严重违法数</td> 
			<td>严重违法率</td> 
			<td>一般违法</td> 
			<td>监测违法率</td>
			</tr>';
			foreach($do_ie as $key => $val){
				$number = $key + 1;
				$ad_totals1 += $val['total'];
				$ad_wfggsls1 += $val['wfggsl'];
				$ad_yzwfggsls1 += $val['yzwfggsl'];
				$ad_ybwfggsls1 += $val['ybwfggsl'];
				$html .= '
				<tr> 
					<td>'.$number.'</td> 
					<td>'.$val['fadclass'].'</td> 
					<td>'.$val['total'].'</td> 
					<td>'.$val['wfggsl'].'</td> 
					<td>'.$val['yzwfggsl'].'</td> 
					<td>'.round($val['ggslyzwfl']*100,2).'%</td> 
					<td>'.$val['ybwfggsl'].'</td> 
					<td>'.round($val['ggslwfl']*100,2).'%</td>
				</tr>';
			}

				$num = $number+1;
				$html .= '<tr>';
				$html .= '<td>'.$num.'</td>';
				$html .= '<td>小计</td>';
				$html .= '<td>'.$ad_totals1.'</td>';
				$html .= '<td>'.$ad_wfggsls1.'</td>';
				$html .= '<td>'.$ad_yzwfggsls1.'</td>';
				$html .= '<td>'.round(($ad_yzwfggsls1/$ad_totals1)*100,2).'%</td>';
				$html .= '<td>'.$ad_ybwfggsls1.'</td>';
				$html .= '<td>'.round(($ad_wfggsls1/$ad_totals1)*100,2).'%</td></tr>';
			

		$html .= '</table>';
		$html .='<h3>二、广告监测警示</h3>';

		foreach ($do_ie as $val){
			$ages[] = $val['wfggsl'];
		}
		array_multisort($ages, SORT_DESC, $do_ie);//按照违法数降序
		$ll_wfggsl = $do_ie[0]['wfggsl']+$do_ie[1]['wfggsl'];

		//本、上月市省报刊媒体违法率总和
		$npa_ggslwfl = $npa_list[0]['ggslwfl'] + $npa_list1[0]['ggslwfl'];
		$lpa_ggslwfl = $lpa_list[0]['ggslwfl'] + $lpa_list1[0]['ggslwfl'];
		$pa_wfl = $npa_ggslwfl - $lpa_ggslwfl;
		if($pa_wfl==0){
			$pa_wfl = '无变化';
		}elseif($pa_wfl > 0){
			$pa_wfl = '上升'.$pa_wfl.'个百分点';
		}else{
			$pa_wfl = '下降'.$pa_wfl.'个百分点';
		}
		//电视
		$ntv_ggslwfl = $ntv_list[0]['ggslwfl'] + $ntv_list1[0]['ggslwfl'];
		$ltv_ggslwfl = $ltv_list[0]['ggslwfl'] + $ltv_list1[0]['ggslwfl'];
		$tv_wfl = $ntv_ggslwfl - $ltv_ggslwfl;

		if($tv_wfl==0){
			$tv_wfl = '无变化';
		}elseif($tv_wfl > 0){
			$tv_wfl = '上升'.$tv_wfl.'个百分点';
		}else{
			$tv_wfl = '下降'.$tv_wfl.'个百分点';
		}
		//广播
		$nbc_ggslwfl = $nbc_list[0]['ggslwfl'] + $nbc_list1[0]['ggslwfl'];
		$lbc_ggslwfl = $lbc_list[0]['ggslwfl'] + $lbc_list1[0]['ggslwfl'];
		$bc_wfl = $nbc_ggslwfl - $lbc_ggslwfl;

		if($bc_wfl==0){
			$bc_wfl = '无变化';
		}elseif($bc_wfl > 0){
			$bc_wfl = '上升'.$bc_wfl.'个百分点';
		}else{
			$bc_wfl = '下降'.$bc_wfl.'个百分点';
		}

		$html .='<p>（一）本月广告监测违法率</p>
				<p>本月，市省媒体广告监测违法率同比'.$tb_illegality_str.'，环比'.$hb_illegality_str.'，其中报刊'.$pa_wfl.'、电视'.$tv_wfl.'、广播'.$bc_wfl.'、本月违法广告类别主要集中在'.$do_ie[0]['fadclass'].'和'.$do_ie[1]['fadclass'].'两个行业，累计涉嫌违法'.$ll_wfggsl.'条次<p>';


		foreach ($do_ie as $val){
			$ages1[] = $val['ggslwfl'];
		}
		array_multisort($ages1, SORT_DESC, $do_ie);//按照违法率数降序
		// dump($do_ie);die;
		$html .='<p>（二）'.$do_ie[0]['fadclass'].'广告违法情况较严重</p>
				<p>本月共监测医疗诊疗服务广告'.$do_ie[0]['total'].'条次，其中涉嫌违法广告'.$do_ie[0]['wfggsl'].'条次，监测违法率为'.round($do_ie[0]['ggslwfl']*100,2).'%，高居各行业榜首。(--)</p>';

		$html .='<h3>三、典型违法广告案例</h3>';


		$map = ' and ig.fillegaltypecode=30';
		$map .= ' and bc.fmediaid in ('.join(',',$regionids).')';
		$array = ['一','二','三','四','五','六','七','八','九','十'];
		//电视类信息
		$sql = 'select count(*) as total,bc.fid,bc.fmediaid,bc.fsampleid,ig.fsampleid,ig.fillegaltypecode,ig.fexpressions,bs.fadid,ad.fadname,ad.fbrand,m.fmedianame,dc.fadclass from ttvissue_'.$now_date.' as bc join tillegalad ig on bc.fsampleid=ig.fsampleid join tbcsample as bs on ig.fsampleid=bs.fid join tad ad on bs.fadid=ad.fadid join tmedia as m on bc.fmediaid=m.fid join tadclass dc 
			on ad.fadclasscode=dc.fcode'.$map.' group by bs.fadid';
		$result = M()->query($sql);//获取统计结果
		if($result){
			$html .='<h3>电视</h3>';
			foreach($result as $k => $val){
				$html.='<p>('.$array[$k].')、'.$val['fadname'].'  ('.$val['fadclass'].')</p>';
				$html.='<p>发布媒体：'.$val['fmedianame'].'</p>';
				$html.='<p>违法表现：“'.$val['fexpressions'].'”</p>';
				$html.='<p>违法次数：该广告累计违法'.$val['total'].'条次</p>';
			}
		}
		

		//广播类信息
		$sql = 'select count(*) as total,bc.fid,bc.fmediaid,bc.fsampleid,ig.fsampleid,ig.fillegaltypecode,ig.fexpressions,bs.fadid,ad.fadname,ad.fbrand,m.fmedianame,dc.fadclass from tbcissue_'.$now_date.' as bc join tillegalad ig on bc.fsampleid=ig.fsampleid join tbcsample as bs on ig.fsampleid=bs.fid join tad ad on bs.fadid=ad.fadid join tmedia as m on bc.fmediaid=m.fid join tadclass dc 
			on ad.fadclasscode=dc.fcode'.$map.' group by bs.fadid';
		$result = M()->query($sql);//获取统计结果
		if($result){
			$html .='<h3>广播</h3>';
			foreach($result as $k => $val){
				$html.='<p>('.$array[$k].')、'.$val['fadname'].'  ('.$val['fadclass'].')</p>';
				$html.='<p>发布媒体：'.$val['fmedianame'].'</p>';
				$html.='<p>违法表现：“'.$val['fexpressions'].'”</p>';
				$html.='<p>违法次数：该广告累计违法'.$val['total'].'条次</p>';
			}
		}

		//报纸类信息
		$map.= ' and date_format(bc.`fissuedate`, "%Y-%m")='.$date;
		$sql = 'select count(*) as total,bc.fpaperissueid as fid,bc.fmediaid,bc.fpapersampleid as fsampleid,bs.fjpgfilename,ig.fsampleid,ig.fillegaltypecode,ig.fexpressions,bs.fadid,ad.fadname,ad.fbrand,m.fmedianame,dc.fadclass from tpaperissue as bc 
			join tillegalad ig on bc.fpapersampleid=ig.fsampleid 
			join tpapersample as bs on ig.fsampleid=bs.fpapersampleid join tad ad on bs.fadid=ad.fadid 
			join tmedia as m on bc.fmediaid=m.fid 
			join tadclass dc on ad.fadclasscode=dc.fcode'.$map.' group by bs.fadid';
		$result = M()->query($sql);//获取统计结果
		// dump($result);die;
		if($result){
			$html .='<h3>报纸</h3>';
			foreach($result as $k => $val){
				$html.='<p>('.$array[$k].')、'.$val['fadname'].'  ('.$val['fadclass'].')</p>';
				$html.='<p>发布媒体：'.$val['fmedianame'].'</p>';
				$html.='<p>违法表现：“'.$val['fexpressions'].'”</p>';
				$html.='<p>违法次数：该广告累计违法'.$val['total'].'条次</p>';
			}
		}


		// dump($result);die;

		$html .= '<h3>四、广告监测情况统计表</h3>';
		$html .= '<p>前十位涉嫌违法广告排名表</p>';

		// $sql = 'select 
		// 		ac.fid,ac.fname,
		// 		sum(case when bs.fillegaltypecode=20 then 1 else 0 end) as ybwfggsl,
		// 		sum(case when bs.fillegaltypecode=30 then 1 else 0 end) as yzwfggsl,
		// 		sum(case when bs.fillegaltypecode>0 then 1 else 0 end) as wfggsl,
		// 		(select m.fmedianame from tmedia m where m.fid in (bc.fmediaid) ) as melist
		// 		from tadowner as ac join tad a on ac.fid=a.fadowner join ((select fid,fadid,fmediaid,fillegaltypecode from tbcsample)
		// 		union all (select fid,fadid,fmediaid,fillegaltypecode from ttvsample)
		// 		union all (select fpapersampleid as fid,fadid,fmediaid,fillegaltypecode from tpapersample)) as bs on a.fadid=bs.fadid join ((select fsampleid,fmediaid from tbcissue_'.$now_date.') 
		// 		union all (select fsampleid,fmediaid from ttvissue_'.$now_date.') 
		// 		union all (select fpapersampleid as fsampleid,fmediaid from tpaperissue where date_format(`fissuedate`,"%Y-%m")='.$date.'))
		// 		as bc on bs.fmediaid=bc.fmediaid and bs.fid=bc.fsampleid where ac.fid>0 group by ac.fid order by wfggsl desc limit 0,10';

		$sql = 'select count(*) total,tb.fid,tb.fmediaid,bs.fid as bsfid,bs.fillegaltypecode,a.fadid,ao.fname as aofname 
				from tbcissue_'.$now_date.' as tb 
				join tbcsample as bs on tb.fsampleid=bs.fid 
				join tad a on bs.fadid=a.fadid 
				join tadowner ao on a.fadowner=ao.fid 
				where bs.fillegaltypecode=20 and tb.fmediaid 
				in ('.join(',',$regionids).')
				GROUP BY aofname UNION ALL 
				select count(*) total,tb.fid,tb.fmediaid,bs.fid as bsfid,bs.fillegaltypecode,a.fadid,ao.fname as aofname 
				from ttvissue_'.$now_date.' as tb 
				join tbcsample as bs on tb.fsampleid=bs.fid 
				join tad a on bs.fadid=a.fadid 
				join tadowner ao on a.fadowner=ao.fid 
				where bs.fillegaltypecode=20 and tb.fmediaid 
				in ('.join(',',$regionids).')
				GROUP BY aofname UNION ALL
				select count(*) total,tb.fpaperissueid as fid,tb.fmediaid,bs.fid as bsfid,bs.fillegaltypecode,a.fadid,ao.fname as aofname 
				from tpaperissue as tb 
				join tbcsample as bs on tb.fpapersampleid=bs.fid 
				join tad a on bs.fadid=a.fadid 
				join tadowner ao on a.fadowner=ao.fid 
				where bs.fillegaltypecode=20 and date_format(tb.`fissuedate`,"%Y-%m")='.$date.' and tb.fmediaid 
				in ('.join(',',$regionids).')
				GROUP BY aofname';
		// $sql = 'select bi.fmediaid from tbcissue_'.$now_date.' as bi join tbcsample as bc on bi.fmediaid=bc.fmediaid where bi.fid in('.join(',',$regionids).') and bc.fexpressioncodes>0';
		$result = M()->query($sql);//获取广告组监测信息
		$sql = 'select count(*) total,tb.fid,tb.fmediaid,bs.fid as bsfid,bs.fillegaltypecode,a.fadid,ao.fname as aofname 
				from tbcissue_'.$now_date.' as tb 
				join tbcsample as bs on tb.fsampleid=bs.fid 
				join tad a on bs.fadid=a.fadid 
				join tadowner ao on a.fadowner=ao.fid 
				where bs.fillegaltypecode=30 and tb.fmediaid 
				in ('.join(',',$regionids).')
				GROUP BY aofname UNION ALL 
				select count(*) total,tb.fid,tb.fmediaid,bs.fid as bsfid,bs.fillegaltypecode,a.fadid,ao.fname as aofname 
				from ttvissue_'.$now_date.' as tb 
				join tbcsample as bs on tb.fsampleid=bs.fid 
				join tad a on bs.fadid=a.fadid 
				join tadowner ao on a.fadowner=ao.fid 
				where bs.fillegaltypecode=30 and tb.fmediaid 
				in ('.join(',',$regionids).')
				GROUP BY aofname UNION ALL
				select count(*) total,tb.fpaperissueid as fid,tb.fmediaid,bs.fid as bsfid,bs.fillegaltypecode,a.fadid,ao.fname as aofname 
				from tpaperissue as tb 
				join tbcsample as bs on tb.fpapersampleid=bs.fid 
				join tad a on bs.fadid=a.fadid 
				join tadowner ao on a.fadowner=ao.fid 
				where bs.fillegaltypecode=30 and date_format(tb.`fissuedate`,"%Y-%m")='.$date.' and tb.fmediaid 
				in ('.join(',',$regionids).')
				GROUP BY aofname';
		// $sql = 'select bi.fmediaid from tbcissue_'.$now_date.' as bi join tbcsample as bc on bi.fmediaid=bc.fmediaid where bi.fid in('.join(',',$regionids).') and bc.fexpressioncodes>0';
		$result1 = [];
		$result2 = [];
		$result1 = M()->query($sql);//获取广告组监测信息
		foreach($result as $val){
			if(empty($result1)){
				$result2[] =array(
						'aofname' => $val['aofname'],
						'yzwfggsl' => 0,
						'ybwfggsl' => $val['total'],
						'fmedname' => M('tmedia')->where('fid in ('.$val['fmediaid'].')')->getField('fmedianame'),
						'wfggsl' => $val['total']
					);
			}else{
				foreach( $result1 as $val1){
				if($val['aofname'] == $val1['aofname']){
					$result2[] =array(
						'aofname' => $val['aofname'],
						'yzwfggsl' => $val1['total'],
						'ybwfggsl' => $val['total'],
						'fmedname' => M('tmedia')->where('fid in ('.$val['fmediaid'].')')->getField('fmedianame'),
						'wfggsl' => $val['total'] + $val1['total']
					);
				}else{
					$result2[] =array(
						'aofname' => $val['aofname'],
						'yzwfggsl' => $val1['total'],
						'ybwfggsl' => $val['total'],
						'fmedname' => M('tmedia')->where('fid in ('.$val['fmediaid'].')')->getField('fmedianame'),
						'wfggsl' => $val['total'] + $val1['total']
					);
					}
				}
			}
			
		}
		// dump($result2);die;
		$html .= ' 
			<table style="width:100%;border:1px solid #666666; margin-top:20px;" border="1" align="center" class="tb"> 
			<tr>
			<td>序号</td>
			<td>广告主</td> 
			<td>一般违法数</td> 
			<td>严重违法数</td> 
			<td>发布媒体</td> 
			<td>合计</td>
			</tr>';
		foreach($result2 as $key => $val){
			$num = $key + 1;
			$hj = $val['ybwfggsl'] + $val['yzwfggsl'];
			$html .= '
			<tr> 
				<td>'.$num.'</td> 
				<td>'.$val['aofname'].'</td>
				<td>'.$val['ybwfggsl'].'</td>
				<td>'.$val['yzwfggsl'].'</td>
				<td>'.$val['fmedname'].'</td>
				<td>'.$hj.'</td>
			</tr>';
		}

		$html .= '</table>';
		$html .= '</div>';


		$date = date('Ymdhis');
        $savefilename = $date.'.doc';
        header("Content-type: text/html; charset=utf-8");
        $savefile = './Public/Word/'.iconv("utf-8","gb2312",$regulatorpname.$date1.'月报').$date.'.doc';

        //将文档保存
		word_start(); 
		echo $html; 
		word_save($savefile); 
		ob_flush();//每次执行前刷新缓存 
		flush();

		// //将生成记录保存
		// $pnname = $regulatorpname.$date.'涉嫌发布违法广告情况表';
  //       $data['pnname'] 			= $pnname;
  //       $data['pntype'] 			= 10;
  //       $data['pnfiletype'] 		= 10;
  //       $data['pnstarttime'] 		= $date;
  //       $data['pncreatetime'] 		= date('Y-m-d H:i:s');
  //       $data['pnurl'] 				= $savefilename;
  //       $data['pnhtml'] 			= $html;
  //       $data['pncreatepersonid'] 	= session('regulatorpersonInfo.fid');
  //       $do_tn = M('tpresentation')->add($data);
  //       if(!empty($do_tn)){
  //       	$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$savefilename));
  //       }else{
  //       	$this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
  //       }
		echo $html;
	}
}