const netAdTask2 = {
  template: `<div v-loading="loading" class="net-ad">
    <div class="header" v-if="(task.task_state == 1 || task.task_state == '11' || task.task_state == 21 || task.task_state == '13' || task.task_state == 23) && type == 'input'">
      <div class="h3">开始任务</div>
      <div class="timer">
        <div class="left">
          <i class="el-icon-warning"></i>
        </div>
        <div class="right">
          <div class="item">
            任务领取时间: {{startTime}}
          </div>
          <div class="item">
            剩余时间: <b>{{remainTime}}</b>秒
          </div>
        </div>
      </div>
      <el-button type="warning" @click="openCutErrorForm">非广告</el-button>
    </div>
    <div class="header">
      <div class="button-area">
        <el-row v-if="(task.task_state == 4 || task.task_state == 13 || task.task_state == 23)" class="task-back">
          <div class="describe">该任务被退回</div>
          <div class="reason">退回理由: {{task.back_reason}}</div>
        </el-row>
        <el-row v-if="task.cut_err_state == 1" class="task-back">
          <div class="describe">该任务为剪辑错误</div>
          <div class="reason">剪辑错误说明: {{task.cut_err_reason}}</div>
        </el-row>
        <el-row v-if="type === 'review' && (task.task_state == '2' || task.task_state == '12' || task.task_state == '22')">
          <el-button type="warning" @click="rejectTask(1)">广告录入有误</el-button>
          <el-button type="warning" @click="rejectTask(2)">违法判定有误</el-button>
        </el-row>
        <el-row v-if="illegalInputType === 'input' && adInputType === 'quality' && (task.task_state == 1 || task.task_state == 11 || task.task_state == 21)">
          <el-button type="warning" @click="rejectTask(1)">广告录入有误</el-button>
        </el-row>
        <el-row v-if="type === 'quality' && (task.task_state == '2' || task.task_state == '22' || task.task_state == 2 || task.task_state == 22 || task.task_state == 5)">
          <el-button type="warning" @click="rejectTask(1)">广告录入有误</el-button>
          <el-button type="warning" @click="rejectTask(2)">违法判定有误</el-button>
          <el-button type="success" @click="passTask">质检通过</el-button>
        </el-row>
      </div>
    </div>
    <div class="content-wrapper">
      <div class="content">
        <div class="h3">广告基本信息</div>
        <div style="font-size: 18px; margin: 10px 0;">任务ID: {{task.taskid}}</div>
        <div style="font-size: 18px; margin: 10px 0;">UUID: {{ task.sam_uuid || task.identify_code || task.uuid}}</div>
        <div style="font-size: 18px; margin: 10px 0;">媒体名称: {{task.media_name}}</div>
        <div style="overflow: auto;">
          <el-row v-if="task.ftype != 2 && task.ftype != 9">
            <el-col :span="24" style="overflow: auto;white-space: nowrap">
              发布页：<a :href="task.net_original_url" target="_blank" style="white-space: nowrap">{{task.net_original_url}}</a></el-col>
          </el-row>
          <el-row v-if="task.ftype !== 9">
            <el-col :span="24" style="overflow: auto;white-space: nowrap">
              落地页：<a :href="task.old_target_url" target="_blank" style="white-space: nowrap">{{task.old_target_url}}</a>
            </el-col>
          </el-row>
          <el-row>
            <el-col :span="24" style="white-space: nowrap;text-overflow：ellipsis;border: 1px solid #eee;">
              原名称：<a :href="task.net_target_url" target="_blank" style="white-space: nowrap">{{ task.fadtext }}</a></el-col>
          </el-row>
        </div>
        <div class="sample1-wrapper">
          <div class="img-wrapper" v-if="task.ftype == 'image'">
            <img :src="task.net_url" alt="广告图片">
          </div>
          <div v-if="task.ftype == 'swf'">
            <object width="100%" id="" type="application/x-shockwave-flash" :data="task.net_url">
              <param name="wmode" value="transparent"></object>
          </div>
          <div v-if="task.ftype == 'flv'">
            <video :src="task.net_url" controls></video>
          </div>
        </div>
        <el-tabs v-model="activeTabName" type="border-card">
          <el-tab-pane label="广告信息录入" name="adInfo">
            <ad-info :task="task" :type="adInputType" ref="adInfo" @next="(val) => submitSuccess(1,val)" @nextTask="() => $emit('next', 1)" :show-button="!isquality"></ad-info>
          </el-tab-pane>
          <el-tab-pane label="违法判定" name="illegalInfo">
            <illegal-info :showtips="true" :task="task" :type="illegalInputType" ref="illegalInfo" @next="submitSuccess(2)" :show-button="false"></illegal-info>
          </el-tab-pane>
        </el-tabs>
        <div style="text-align: center;margin-top: 10px;" v-if="isquality">
          <el-button type="primary" @click="submit()">提交</el-button>
        </div>
      </div>
      <div class="content">
        <div class="h3">
          <span>广告内容</span>
          <div style="font-size: 14px;">
            <a @click="reloadAdIFrame">刷新广告页</a>
            <a :href="task.old_target_url" style="font-size: 14px;" target="_blank">新窗口打开</a>
          </div>
        </div>
        <div class="iframe-wrapper">
          <iframe :src="task.net_target_url" frameborder="0" sandbox="allow-scripts" ref="adIframe"></iframe>
        </div>
      </div>
    </div>
    <el-dialog title="请选择剪辑错误" :visible.sync="showCutErrorForm" :modal="false">
      <submit-cut-error :taskid="task.taskid" :media-class="task.media_class" ref="submitCutError" @success="submitCutErrorSuccess"></submit-cut-error>
    </el-dialog>
  </div>`,
  props: {
    task: {
      type: Object,
      default: () => ({})
    },
  },
  components: {
    submitCutError,
    adInfo,
    illegalInfo,
  },
  data: function () {
    return {
      adClassTree,
      illegalTypeArr,
      adClassArr,
      illegalArr,
      type,
      loading: false,
      remainTime: 1200,
      intervalId: 0,
      viewerInstance: {
        destroy: () => {}
      },
      activeTabName: 'adInfo',
      showCutErrorForm: false,
    }
  },
  computed: {
    isquality() {
      if (this.task.linkTypeArr) {
        const _isquality = this.task.linkTypeArr.filter((v) => v.state === '1')
        const isquality = this.task.linkTypeArr.filter((v) => v.state === '1' && v.type === '2')
        if (isquality.length === 1 && _isquality.length === 2) {
          return false
        } else if(isquality.length === 1 && _isquality.length === 1) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    startTime: function () {
      return this.task.modifytime ? moment.unix(this.task.modifytime).format('YYYY-MM-DD HH:mm:ss') : ''
    },
    adInputType: function () {
      if (this.isquality) return 'input'

      if (this.type !== 'quality' && this.task.linkTypeArr) {
        let res = this.task.linkTypeArr.filter((v) => v.type === '1' && (v.state === '1' || v.state === '3' || v.state === '4'))
        if (res.length > 0) {
          return 'input'
        } else {
          return 'quality'
        }
      }
      return 'quality'
    },
    illegalInputType: function () {
      if (this.type !== 'quality' && this.task.linkTypeArr && this.isquality) {
        let res = this.task.linkTypeArr.filter((v) => v.type === '2' && (v.state === '1' || v.state === '3' || v.state === '4'))
        if (res.length > 0) {
          return 'input'
        } else {
          return 'quality'
        }
      }
      return 'quality'
    }
  },
  methods: {
    submit() {
      const p1 = this.$refs.adInfo.initData()
      const p2 = this.$refs.illegalInfo.initData()
      Promise.all([p1, p2]).then(res => {
        const [p1, p2] = res
        const data = { ...p1, ...p2 }
        let text = data.change ? '检测到修改了录入信息，提交立即扣除录入人积分，确认提交？' : '确认提交?'
        this.$confirm(text).then(() => {
          this.$refs.illegalInfo.setLoading(true)
          $.post('submitJudgeTask', { data }).then((res) => {
            if (res.code == 0) {
              if (this.fromManage) {
                return this.$emit('back', 1)
              }
              this.$emit('next')
              this.imgurl = {}
            } else {
              this.$message({
                message: res.msg,
                type: 'error'
              })
            }
            this.$refs.illegalInfo.setLoading(false)
          })
        }).catch(() => {
          this.$refs.illegalInfo.setLoading(false)
        })
      }).catch(e => {
      })
    },
    dataInit: function () {
      clearInterval(this.intervalId)
      this.loading = false
      this.type = type
      // this.activeTabName = 'adInfo'
      this.showCutErrorForm = false
      this.task.old_target_url = this.task.net_target_url
      if (this.task.net_platform == 9) {
        this.task.net_target_url = '/Api/NetAd/getNetAdSnapshot/adId/' + this.task.major_key
      }
      if (this.task.ftype === 'image') {
        this.viewerInit()
      } else {
        this.viewerInstance.destroy()
      }
      if (this.task.linkTypeArr) {
        if (!this.isquality) {
          this.activeTabName = "adInfo"
        } else {
          this.activeTabName = "illegalInfo"
        }
      }
      try {
        if (this.task.fadclasscode) {
          var adClass = [];
          if (this.task.fadclasscode.length === 4) {
            adClass.push(this.task.fadclasscode.substr(0, 2))
            adClass.push(this.task.fadclasscode)
          }
          if (this.task.fadclasscode.length === 2) {
            adClass.push(this.task.fadclasscode)
          }
          if (adClass.length === 0) {
            Vue.set(this.task, 'fadclasscode', '')
          } else {
            Vue.set(this.task, 'fadclasscode', adClass[adClass.length - 1])
          }
          Vue.set(this.task, 'adCateArr', adClass)
        } else {
          Vue.set(this.task, 'adCateArr', [])
          Vue.set(this.task, 'fadclasscode', '')
        }
        if (this.task.fadclasscode_v2) {
          var adClass2 = [];
          for (var i = 2; i <= this.task.fadclasscode_v2.length; i += 2) {
            adClass2.push(this.task.fadclasscode_v2.substr(0, i))
          }
          if (adClass2.length === 0) {
            Vue.set(this.task, 'fadclasscode_v2', '')
          } else {
            Vue.set(this.task, 'fadclasscode_v2', adClass2[adClass2.length - 1])
          }
          Vue.set(this.task, 'adCateArr2', adClass2)
        } else {
          Vue.set(this.task, 'adCateArr2', [])
          Vue.set(this.task, 'fadclasscode_v2', '')
        }
        if (!this.task.fadclasscode_v2) {
          Vue.set(this.task, 'fadclasscode_v2', 3203)
        }
      } catch (e) {

      }
      // if (this.type == 'input' && this.task.task_state.substr(-1, 1) == 1) {
      //   this.intervalId = setInterval(this.timer, 1000)
      // }
      let tempUrl = this.task.net_target_url
      this.task.net_target_url = ''
      this.$forceUpdate()
      this.$nextTick(() => {
        this.task.net_target_url = tempUrl
        if (this.illegalInputType === 'input') {
          this.$refs.illegalInfo.changeIllegalType()
        }
      })
    },
    timer: function () {
      this.remainTime = moment.unix(+this.task.modifytime + 1200).diff(moment(), 'seconds')
      if (this.remainTime <= 0) {
        let tempId = setInterval(() => {})
        while (tempId > 0) {
          clearInterval(tempId)
          tempId--
        }
        $.post('/Api/TaskInput/back_overtime_task_net').then(() => {
          this.$alert('抱歉, 任务超时被收回, 点击确定开始下一个任务').then(() => {
            this.$emit('next')
          })
        })
      }
    },
    viewerInit: function () {
      this.$nextTick(() => {
        try {
          this.viewerInstance.destroy()
          this.viewerInstance = new Viewer(document.querySelector('.sample1-wrapper img'), {
            inline: true,
            toolbar: false,
            navbar: false
          })
        } catch (e) {}
      })
    },
    submitSuccess: function (type, val) {
      this.$emit('next')
    },
    openCutErrorForm: function () {
      this.showCutErrorForm = true
      this.$nextTick(() => {
        this.$refs.submitCutError.dataInit()
      })
    },
    submitCutErrorSuccess: function () {
      this.$emit('next')
    },
    passTask: function () {
      this.$confirm('任务质检通过?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
      }).then(() => {
        const data = {
          taskid: this.task.taskid,
          taskType: 2,
        }
        this.loading = true
        $.post('/TaskInput/TraditionAd/passTask', data).then((res) => {
          this.loading = false
          if (res.code == -2 && res.msg == 'need log in again') {
            this.$message.error('登录状态过期,请再次登录')
          }
          if (res.code == 0) {
            this.$message({
              type: 'success',
              message: '通过成功'
            })
          } else {
            this.$message({
              type: 'error',
              message: '通过失败'
            })
          }
          this.$emit('back',1)
        })
      }).catch(() => {})
    },
    rejectTask: function (type) {
      this.$prompt('确认退回任务?', '提示', {
        confirmButtonText: '提交',
        cancelButtonText: '取消',
        inputPattern: /\S/,
        inputErrorMessage: '请输入退回说明'
      }).then((value) => {
        const data = {
          taskid: this.task.taskid,
          reason: value.value,
          mediaClass: this.task.media_class,
          linkType: type
        }
        this.loading = true
        $.post('submitRejectTaskRequest', data).then((res) => {
          this.loading = false
          if (res.code == -2 && res.msg == 'need log in again') {
            this.$message.error('登录状态过期,请再次登录')
          }
          if (res.code == 0) {
            this.$message({
              type: 'success',
              message: '成功退回'
            })
          } else {
            this.$message({
              type: 'error',
              message: '退回失败'
            })
          }
          this.$emit('back')
        })
      }).catch(() => {})
    },
    submitRecallRequest: function () {
      this.$confirm('是否发起撤回请求?待质检员审核确认后任务将退回到被退回状态', '是否确认?', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'danger'
      }).then(() => {
        $.post('receiveRecallRequest', {
          taskid: this.task.taskid
        }).then((res) => {
          if (res.code == -2 && res.msg == 'need log in again') {
            this.$message.error('登录状态过期,请再次登录')
          }
          this.$message({
            message: res.msg,
            type: res.type,
          })
          this.$emit('back')
        })
      }).catch(() => {})
    },
    reloadAdIFrame: function () {
      const tempUrl = this.$refs.adIframe.src
      this.$refs.adIframe.src = ''
      this.$refs.adIframe.src = tempUrl
    },
    addConfirmedAd: function () {
      this.$confirm('确认广告信息正确并且合乎规范 ?').then(() => {
        this.$refs.form.validate((res) => {
          if (res) {
            const adInfo = {
              'fadname': this.task.fadname,
              'fadowner': this.task.fadowner,
              'fadclasscode_v2': this.task.adCateArr2[this.task.adCateArr2.length - 1],
              'fadclasscode': this.task.adCateArr[this.task.adCateArr.length - 1],
              'fbrand': this.task.fbrand,
            }
            this.loading = true
            $.post('/Api/Ad/addConfirmedAd', {
              adInfo: adInfo
            }).then((res) => {
              this.loading = false
              if (res.code == 0) {
                this.$message.success(res.msg)
                this.inputAdName()
              } else {
                this.$message.error(res.msg)
              }
            })
          }
        })
      })

    }
  },

}