const submitCutError = {
    template: `<div v-loading="loading" class="cutErrorForm">
    <el-form ref="form" :model="form" label-width="80px">
        <el-form-item label="错误描述">
            <el-input v-model="form.reason"></el-input>
        </el-form-item>
        <el-form-item label="错误类型">
            <el-radio-group v-model="form.errorIds">
                <el-radio :label="item.id" v-for="(item, index) in cutErrorReasonArr" :key="index">
                    <el-tooltip effect="dark" :content="item.description" placement="top">
                        <el-button @click="clickErrorType(item)">{{item.reason}}</el-button>
                    </el-tooltip>
                </el-radio>
            </el-radio-group>
        </el-form-item>
        <el-form-item>
            <el-button type="primary" @click="submit">提交</el-button>
        </el-form-item>
    </el-form>
</div>`,
    props:['id', 'mediaClass'],
    data: function () {
        return {
            loading: false,
            cutErrorReasonArr: [],
            form: {
                reason: '',
                errorIds: '',
            }
        }
    },
    methods: {
        dataInit: function () {
            this.cutErrorReasonArr = []
            this.form = {
                reason: '',
                errorIds: '',
            }
            this.loading = true
            $.post('getCurErrorReason', {mediaClass: this.mediaClass}).then((res) => {
                 this.cutErrorReasonArr = res
                this.loading = false
            })
        },
        clickErrorType: function (item) {
                if (this.mediaClass == 13){
                    this.form.reason += '   ' + item.reason
                }
            this.form.errorIds = item.id
        },
        submit: function(){
            if(this.form.errorIds.length === 0){
                this.$message.error('请至少选择一个错误类型')
                return false
            }
            this.$confirm('确认提交错误?').then(() => {
                this.loading = true
                let url = ''
                let reason = ''
                url = 'cutErr'
                reason = this.form.errorIds + ' ;; ' + this.form.reason
                $.post(url, {
                    id: this.id,
                    reason: reason.trim()
                }).then((res) => {
                    this.loading = false
                    if (res.code == 0){
                        this.$message.success('提交成功')
                        this.$emit('success')
                    } else {
                        this.$message.error('提交失败')
                    }
                })
            })

        }
    }
}