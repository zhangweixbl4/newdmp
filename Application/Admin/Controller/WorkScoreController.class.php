<?php
namespace Admin\Controller;

class WorkScoreController extends BaseController
{
    public function index()
    {
        if(A('Admin/Authority','Model')->authority('302001') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限'
            ]);
        }
        if(IS_POST){
            $formData = I('where');
            $page = I('page', 1);
            $pageSize = I('pageSize', 10);
            $table = $this->processWhere([], $formData, $page, $pageSize);
            $this->ajaxReturn($table);
        }else{
            $userTags = json_encode(explode(',', sys_c('user_tags')));
            $groupList = M('ad_input_user_group')
                ->field('group_id value, group_name label')
                ->cache(true, 60)
                ->select();
            $groupList = json_encode($groupList);
            $workScoreType = json_encode($this->getWorkScoreType2());
            $this->assign(compact('userTags', 'groupList', 'workScoreType'));
            $this->display();
        }
    }

    private function getWorkScoreType()
    {
        $arr = [
            '1' => '本职工作',
            '2' => '快剪',
            '3' => '互联网',
            '4' => '其他',
            '5' => '培训',
            '6' => '商业广告',
            '7' => '报告',
        ];
        return $arr;
    }
    private function getWorkScoreType2()
    {
        $arr = $this->getWorkScoreType();
        $res = [];
        foreach ($arr as $key => $value) {
            $res[] = [
                'label' => $value,
                'value' => $key.'',
            ];
        }
        return $res;
    }

    private function processWhere($where, $formData, $page, $pageSize)
    {
        // 处理ad_input_user表搜索
        $adInputWhere = [];
        if ($formData['wx_id'] != ''){
            $adInputWhere['alias'] = ['LIKE', "%{$formData['wx_id']}%"];
        }
        if ($formData['userTag']){
            $adInputWhere['_string'] = "(";
            foreach ($formData['userTag'] as $formDatum) {
                $adInputWhere['_string'] .= "FIND_IN_SET('$formDatum', user_tag) AND ";
            }
            $adInputWhere['_string'] = rtrim($adInputWhere['_string'], ' AND ');
            $adInputWhere['_string'] .= ')';
        }
        if ($formData['group_id'] != 0){
            $adInputWhere['group_id'] = ['EQ', $formData['group_id']];
        }
        if ($adInputWhere){
            $adInputSubquery = M('ad_input_user')
                ->field('wx_id')
                ->where($adInputWhere)
                ->buildSql();
            $where['wx_id'] = ['EXP', ' IN '.$adInputSubquery];
        }
        // 处理work_score表搜索
        if ($formData['create_time']){
            $where['create_time'] = ['BETWEEN', [$formData['create_time'][0], strtotime('+1 day', $formData['create_time'][1]) - 1]];
        }
        if ($formData['update_time']){
            $where['update_time'] = ['BETWEEN', [$formData['update_time'][0], strtotime('+1 day', $formData['update_time'][1]) - 1]];
        }
        if ($formData['remark'] != ''){
            $where['remark'] = ['LIKE', "%{$formData['remark']}%"];
        }
        if ($formData['state'] != ''){
            $where['state'] = ['EQ', $formData['state']];
        }
        if ($formData['creator'] != ''){
            $where['creator'] = ['EXP', " IN (select fid from tperson where fname like '%{$formData['creator']}%')"];
        }
        if ($formData['modifier'] != ''){
            $where['modifier'] = ['EXP', " IN (select fid from tperson where fname like '%{$formData['modifier']}%')"];
        }
        $total = M('work_score')->where($where)->count();
        $table = M("work_score")
            ->where($where)
            ->order('create_time desc')
            ->page($page, $pageSize)
            ->select();
        $typeTextMap = $this->getWorkScoreType();
        $stateMap = [
            '0' => '未处理',
            '1' => '通过',
            '2' => '驳回',
        ];
        // 处理输出的数据
        foreach ($table as $key => $value) {
            $table[$key]['group_name'] = $this->getGroupName($value['wx_id']);
            $table[$key]['modifier'] = $value['modifier'] ? M('tperson')->where(['fid' => ['EQ', $value['modifier']]])->cache(60)->getField('fname') : '';
            $table[$key]['creator'] = $value['creator'] ? M('tperson')->where(['fid' => ['EQ', $value['creator']]])->cache(60)->getField('fname') : '';
            $table[$key]['wx_id'] = $value['wx_id'] ? M('ad_input_user')->where(['wx_id' => ['EQ', $value['wx_id']]])->cache(60)->getField('alias') : '';
            $table[$key]['create_time'] = $value['create_time'] ? date('Y-m-d H:i:s', $value['create_time']) : '';
            $table[$key]['update_time'] = $value['update_time'] ? date('Y-m-d H:i:s', $value['update_time']) : '';
            $table[$key]['type'] = $typeTextMap[$value['type']];
            $table[$key]['state'] = $stateMap[$value['state']];
            // 前端高亮搜索词
            if ($formData['wx_id'] != ''){
                $table[$key]['wx_id'] = r_highlight($table[$key]['wx_id'], $formData['wx_id']);
            }
            if ($formData['modifier'] != ''){
                $table[$key]['modifier'] = r_highlight($table[$key]['modifier'], $formData['modifier']);
            }
            if ($formData['creator'] != ''){
                $table[$key]['creator'] = r_highlight($table[$key]['creator'], $formData['creator']);
            }
        }
        return compact('total', 'table');
    }

    private function getGroupName($wx_id)
    {
        $userSubQuery = M('ad_input_user')
            ->field('group_id')
            ->where([
                'wx_id' => ['EQ', $wx_id]
            ])
            ->buildSql();
        $groupName = M('ad_input_user_group')
            ->where([
                'group_id' => ['EXP', ' IN '.$userSubQuery]
            ])
            ->cache(60)
            ->getField('group_name');
        return $groupName;
    }

    public function addWorkScoreItem($info)
    {
        if(A('Admin/Authority','Model')->authority('302002') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限'
            ]);
        }
        $info['creator'] = session('personInfo.fid');
        $info['create_time'] = time();
        $info['score_date'] = substr($info['score_date'], 0, 10);
        $info['state'] = 1;
        $res = M('work_score')->add($info);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '添加成功',
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '添加失败',
            ]);
        }
    }

    public function getItemInfo($id)
    {
        $info = M('work_score')->find($id);
        $info['group_name'] = $this->getGroupName($info['wx_id']);
        $info['modifier'] = $info['modifier'] ? M('tperson')->where(['fid' => ['EQ', $info['modifier']]])->cache(60)->getField('fname') : '';
        $info['creator'] = $info['creator'] ? M('tperson')->where(['fid' => ['EQ', $info['creator']]])->cache(60)->getField('fname') : '';
        $info['wx_id'] = $info['wx_id'] ? M('ad_input_user')->where(['wx_id' => ['EQ', $info['wx_id']]])->cache(60)->getField('alias') : '';
        $info['create_time'] = $info['create_time'] ? date('Y-m-d H:i:s', $info['create_time']) : '';
        $info['update_time'] = $info['update_time'] ? date('Y-m-d H:i:s', $info['update_time']) : '';
        $info['score_date'] = $info['score_date'] . '000';
        $this->ajaxReturn($info);
    }

    public function passScoreItem($id)
    {
        $where = [
            'id' => ['EQ', $id],
            'state' => ['EQ', 0],
        ];
        $data = [
            'update_time' => time(),
            'modifier' => session('personInfo.fid'),
            'state' => 1,
        ];
        $res = M('work_score')
            ->where($where)
            ->save($data);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '通过成功',
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '通过失败',
            ]);
        }
    }

    public function rejectScoreItem($id)
    {
        $where = [
            'id' => ['EQ', $id],
            'state' => ['EQ', 0],
        ];
        $data = [
            'update_time' => time(),
            'modifier' => session('personInfo.fid'),
            'state' => 2,
        ];
        $res = M('work_score')
            ->where($where)
            ->save($data);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '驳回成功',
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '驳回失败',
            ]);
        }
    }
}