<?php
/*
 * @Description: 广告主题(广告内容)控制器
 * @LastEditTime: 2020-02-28 13:29:08
 */
namespace Admin\Controller;
use Think\Controller;
class AdController extends BaseController {
	// 接收JSON参数
    protected $P;
	/**
	 * @Des: 初始化
	 * @Edt: yuhou.wang
	 */	
    public function _initialize(){
        parent::_initialize();
        $this->P = json_decode(file_get_contents('php://input'),true);
    }

	/*广告详情*/
	public function ajax_ad_details(){
		$fadid = I('fadid');//获取媒介ID
		$adDetails = M('tad')->where(array('fadid'=>$fadid))->find();//查询广告详情
		$adclassInfo = M('tadclass')->where(array('fcode'=>$adDetails['fadclasscode']))->find();
		$adDetails['adclass'] = $adclassInfo['fadclass'];//查询广告类别的名称
		$adDetails['adclass_fullname'] = $adclassInfo['ffullname'];//查询广告类别的全名称
        $sample = M('task_input')
            ->where([
                'cut_err_state' => ['EQ', 0],
                'fadid' => ['EQ', $fadid],
                'task_state' => ['EQ', 2]
            ])
            ->find();
		$adDetails['adowner_name'] = M('tadowner')->where(array('fid'=>$adDetails['fadowner']))->getField('fname');//查询广告主名称
        if ($sample){
            $adDetails = array_merge($sample, $adDetails);
        }else{
            // 如果没有任务，可能是快剪的任务，所以搜索广播样本表
            $sample = M('tbcsample')
                ->where([
                    'fadid' => ['EQ', $fadid]
                ])
                ->find();
            if ($sample){
                $adDetails['media_class'] = '2';
                $adDetails['source_path'] = $sample['favifilename'];
            }
        }
		if(!$sample){//如果还是没有样本，则可能是互联网任务，所以搜索互联网任务
			$adDetails['net_ad_major_key'] = M('tnetissue')->cache(true,60)->where(array('fadid'=>$fadid))->getField('major_key');
		}
		$this->ajaxReturn(array('code'=>0,'adDetails'=>$adDetails));
    }
    /**
     * @Des: 获取广告详情，列出关联样本
     * @Edt: yuhou.wang
     * @param {type} 
     * @return: 
     */   
    public function getDetail(){
        $fadid = (I('fadid') || I('fadid') == 0) ? I('fadid') : $this->P['fadid'];
        $relatedTables = [
            '01' => ['samtabname'=>'ttvsample','fields'=>'fid,favifilename source_path,fversion_id','key'=>'fid'],
            '02' => ['samtabname'=>'tbcsample','fields'=>'fid,favifilename source_path,fversion_id','key'=>'fid'],
            '03' => ['samtabname'=>'tpapersample','fields'=>'fpapersampleid fid,fjpgfilename source_path,fversion_id','key'=>'fpapersampleid'],
            '13' => ['samtabname'=>'tnetissue','fields'=>'major_key fid,net_url source_path,fversion_id','key'=>'major_key'],
        ];
        if(!empty($fadid) || $fadid == 0){
            $fields = '
                a.fadid,
                a.fadname,
                a.fadowner,
                d.fname adowner_name,
                a.fbrand,
                a.fadclasscode,
                b.fadclass adclass,
                b.ffullname adclass_fullname,
                a.fadclasscode_v2,
                c.fname adclass_v2,
                c.ffullname adclass_v2_fullname,
                a.fcreator,
                a.fcreatetime,
                a.fmodifier,
                a.fmodifytime,
                a.fstate,
                a.comment,
                a.confirmer,
                a.is_sure
            ';
            $adDetails = M('tad')
                ->alias('a')
                ->field($fields)
                ->join('tadclass b ON b.fcode = a.fadclasscode','LEFT')
                ->join('hz_ad_class c ON c.fcode = a.fadclasscode_v2','LEFT')
                ->join('tadowner d ON d.fid = a.fadowner','LEFT')
                ->where(['a.fadid'=>$fadid])
                ->find();
            $sql = M('tad')->getLastSql();
            // 关联样本列表
            $samList = [];
            // 暂每个类别展示近5条样本 TODO:展示版本列表
            $classLimit = 5;
            foreach($relatedTables as $key=>$sam){
                if($key == 13){
                    $whereSam = ['fadid'=>$adDetails['fadid']];
                }else{
                    $whereSam = ['fadid'=>$adDetails['fadid'],'fstate'=>['GT',0]];
                }
                $samlistOri = M($sam['samtabname'])->field($sam['fields'])->where($whereSam)->order($sam['key'].' DESC')->limit($classLimit)->select();
                if(!empty($samlistOri)){
                    foreach($samlistOri as $k=>$samInfo){
                        $fversion = M('tversion')->cache(true)->where(['fid'=>$samInfo['fversion_id']])->getField('fversion');
                        $samInfo['fversion'] = $fversion ? $fversion : $adDetails['fadname'];
                        $samInfo['media_class'] = $key;
                        $samList[] = $samInfo;
                    }
                }
            }
            $adDetails['samList'] = $samList;
            $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','adDetails'=>$adDetails]);
        }
        $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
	}
	
	/*广告添加、修改*/
	public function add_edit_ad(){
		$fadid= I('fadid');//广告ID
        $a_e_data = array();
		$a_e_data['fadname'] = I('fadname');//广告名称
		
		$a_e_data['fbrand'] = I('fbrand');//品牌
		$a_e_data['fname'] = I('fname');//设备名称
		$a_e_data['fadclasscode'] = I('fadclasscode');//广告内容类别代码
		$a_e_data['is_sure'] = I('is_sure', 0);//广告内容类别代码
		$adowner_name = I('adowner_name');//广告主名称
		
		if($adowner_name == ''){//判断是否有输入广告主
			$fadowner = 0;//如果没有输入广告主，广告主ID等于0
		}else{
			$adownerInfo = M('tadowner')->where(array('fname'=>$adowner_name))->find();//查询广告主信息
			if($adownerInfo){//如果有广告主信息
				$fadowner = $adownerInfo['fid'];//赋值广告主ID
			}else{//如果没有广告主ID
				$fadowner = M('tadowner')->add(array(
													'fname'			=> $adowner_name,
													'fregionid'		=> 100000,
													'fcreator' 		=> session('personInfo.fname'),//创建人
													'fcreatetime' 	=> date('Y-m-d H:i:s'),//创建时间
													'fmodifier' 	=> session('personInfo.fname'),//修改人
													'fmodifytime' 	=> date('Y-m-d H:i:s'),//修改时间
													'fstate'		=> 1,
												));//添加广告主
			}
		}
		
		$a_e_data['fadowner'] = intval($fadowner);//广告主
		$a_e_data['fstate'] = I('fstate',1);//状态，-1删除，0-无效，1-有效
		if($fadid == 0){//判断是修改还是新增
			
			if(A('Admin/Authority','Model')->authority('200131') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
			}
			$is_repeat = M('tad')->where(array('fadname'=>$a_e_data['fadname']))->count();//查询广告名称是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'广告名称重复'));//广告名称重复
			$a_e_data['fcreator'] = session('personInfo.fname');//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tad')->add($a_e_data);//添加数据
			
			if($rr > 0){//判断是否添加成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));//返回失败
			}
			
		}else{//修改
			
			if(A('Admin/Authority','Model')->authority('200132') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));//验证是否有新增权限
			}
			//$is_repeat = M('tad')->where(array('fadid'=>array('neq',$fadid),'fadname'=>$a_e_data['fadname']))->count();//查询广告名称是否重复
			//if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'广告名称重复'));//广告名称重复
			//修改名称暂时不验证是否重复
			
			$a_e_data['fmodifier'] = session('personInfo.fname');//修改人
            $a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
            $a_e_data['to_opensearch_time'] = 0;//同步到opensearch状态置为0，即立即同步
			$rr = M('tad')->where(array('fadid'=>['EQ', $fadid]))->save($a_e_data);//修改数据
			if($rr > 0){//判断是否修改成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));//返回失败
			}
		}
	}

    private function getAdClassTree()
    {
        $data = M('hz_ad_class')->field('fcode value, fpcode, fname label')->where(['fstatus' => ['EQ', 1]])->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpcode']])){
                $items[$value['fpcode']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }

    private function getAdClassArr()
    {
        $data = M('tadclass')
            ->field('fcode, fpcode, fadclass')
            ->where(['fstate' => ['EQ', 1]])
            ->cache(true)
            ->select();
        $res = [];
        foreach ($data as $key => &$value) {
            if (strlen($value['fcode']) === 2){
                $res[] = [
                    'value' => $value['fcode'],
                    'label' => $value['fadclass'],
                ];
            }
        }
        foreach ($data as $key => &$value2) {
            $value2['value'] = $value2['fcode'];
            $value2['label'] = $value2['fadclass'];
            if (strlen($value2['fcode']) === 4){
                foreach ($res as &$item) {
                    if ($value2['fpcode'] === $item['value']){
                        $item['children'][] = $value2;
                        foreach ($data as $key2 => &$value3) {
                            if (strlen($value3['fcode']) === 6){
                                foreach ($item['children'] as &$item2) {
                                    $value3['value'] = $value3['fcode'];
                                    $value3['label'] = $value3['fadclass'];
                                    if ($value3['fpcode'] === $item2['value']){
                                        $item2['children'][] = $value3;
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }
        return $res;
    }
    /**
     * @Des: 获取广告内容管理列表
     * @Edt: yuhou.wang
     * @param {type} 
     * @return: 
     * @Date: 2019-09-12 14:42:25
     */
    public function index()
    {
        session_write_close();
        if (IS_POST){
            $page      = I('page', 1);
            $pageSize  = I('pageSize', 10);
            $where     = I('where');
            $order     = I('order');
            $orderType = I('orderType');
            $order     = $order ? $order : 'fmodifytime';
            $orderType = $orderType ? $orderType : 'desc';
            $adWhere = [
                'fstate' => ['GT',0],
                'fshow'  => 1,
            ];
            // 搜索发布记录与样本
            if ($where['issueDate'] || $where['fmediaid'] || $where['mediaName'] || $where['fregionid'] || $where['fmediaclass']){
                $where['issueDate'][0] = $where['issueDate'][0] ? ($where['issueDate'][0]/1000) : strtotime('-1 month');
                $where['issueDate'][1] = $where['issueDate'][1] ? strtotime('+1 day',($where['issueDate'][1]/1000)) : time();
                $mediaClass = $where['fmediaclass'];
                $region = $where['fregionid'];
                $region = substr($region,0,2);
                switch($mediaClass){
                    case '01':
                        $issueTable = 'ttvissue';
                        $where['issueDate'][0] = date('Y-m-d H:i:s',$where['issueDate'][0]);
                        $where['issueDate'][1] = date('Y-m-d H:i:s',$where['issueDate'][1]);
                        $whereIssue = [
                            'fissuedate' => ['BETWEEN',$where['issueDate']],
                        ];
                        if(!empty($where['fmediaid'])){
                            $whereIssue['fmediaid'] = ['IN',$where['fmediaid']];
                        }
                        $issueSubQuery = M($issueTable)
                            ->field('distinct ftvsampleid')
                            ->where($whereIssue)
                            ->buildSql();
                        $sampleSubQuery = M('ttvsample')
                            ->field('distinct fadid')
                            ->where([
                                'fid' => ['EXP', ' IN '.$issueSubQuery]
                            ])
                            ->buildSql();
                        $adWhere['fadid'] = ['EXP', ' IN '.$sampleSubQuery];
                        break;
                    case '02':
                        $issueTable = 'tbcissue';
                        $where['issueDate'][0] = date('Y-m-d H:i:s',$where['issueDate'][0]);
                        $where['issueDate'][1] = date('Y-m-d H:i:s',$where['issueDate'][1]);
                        $whereIssue = [
                            'fissuedate' => ['BETWEEN',$where['issueDate']],
                        ];
                        if(!empty($where['fmediaid'])){
                            $whereIssue['fmediaid'] = ['IN',$where['fmediaid']];
                        }
                        $issueSubQuery = M($issueTable)
                            ->field('distinct fbcsampleid')
                            ->where($whereIssue)
                            ->buildSql();
                        $sampleSubQuery = M('tbcsample')
                            ->field('distinct fadid')
                            ->where([
                                'fid' => ['EXP', ' IN '.$issueSubQuery]
                            ])
                            ->buildSql();
                        $adWhere['fadid'] = ['EXP', ' IN '.$sampleSubQuery];
                        break;
                    case '03':
                        $where['issueDate'][0] = date('Y-m-d H:i:s',$where['issueDate'][0]);
                        $where['issueDate'][1] = date('Y-m-d H:i:s',$where['issueDate'][1]);
                        $whereIssue = [
                            'fissuedate' => ['BETWEEN',$where['issueDate']],
                        ];
                        if(!empty($where['fmediaid'])){
                            $whereIssue['fmediaid'] = ['IN',$where['fmediaid']];
                        }
                        $sampleSubQuery = M('tpapersample')
                            ->field('distinct fadid')
                            ->where($whereIssue)
                            ->buildSql();
                        $adWhere['fadid'] = ['EXP', ' IN '.$sampleSubQuery];
                        break;
                    case '05':
                        break;
                    case '13':
                        $where['issueDate'][0] = $where['issueDate'][0]*1000;//互联网样本发布时间字段为毫秒
                        $where['issueDate'][1] = $where['issueDate'][1]*1000;
                        $whereIssue = [
                            'net_created_date' => ['BETWEEN',$where['issueDate']],
                        ];
                        if(!empty($where['fmediaid'])){
                            $whereIssue['fmediaid'] = ['IN',$where['fmediaid']];
                        }
                        $sampleSubQuery = M('tnetissue')
                            ->field('distinct fadid')
                            ->where($whereIssue)
                            ->buildSql();
                        $adWhere['fadid'] = ['EXP', ' IN '.$sampleSubQuery];
                        break;
                }
            }

            if ($where['adName'] != ''){
                $where['adName'] = trim($where['adName']);
                if($where['adNameSearchType'] == 2){
                    // 精确匹配
                    $adWhere['fadname'] = ['EQ', $where['adName']];
                }else{
                    $adWhere['fadname'] = ['LIKE', '%'.$where['adName'].'%'];
                }
            }
            if ($where['fadid'] != ''){
                $adWhere['fadid'] = ['EQ', $where['fadid']];
            }
            if ($where['adClass'] != ''){
                $adWhere['fadclasscode'] = ['EQ', $where['adClass']];
            }
            if ($where['adClass2'] != ''){
                $adWhere['fadclasscode_v2'] = ['EQ', $where['adClass2']];
            }
            if ($where['confirm'] != ''){
                $adWhere['is_sure'] = ['EQ', $where['confirm']];
            }
            if ($where['adBrand'] != ''){
                $where['adBrand'] = trim($where['adBrand']);
                $adWhere['fbrand'] = ['LIKE', '%'.$where['adBrand'].'%'];
            }
            if ($where['adOwner'] != ''){
                $where['adOwner'] = trim($where['adOwner']);
                $adWhere['fadowner'] = ['EXP', 'IN '.M('tadowner')->field('fid')->where(['fname' => ['LIKE', '%'.$where['adOwner'].'%']])->buildSql()];
            }
            if ($where['adError'] == 'true'){
                $adWhere['is_sure'] = ['NEQ', 0];
                $adWhere['fstate'] = ['EQ', 9];
            }
            if ($where['fstate'] != ''){
                // 状态 1已确认 2待确认
                $adWhere['fstate'] = ['EQ', $where['fstate']];
            }
            if ($where['state'] == 2){
                // 标准库 标签，暂定为2状态
                $adWhere['fstate'] = ['EQ', 1];
            }
            if ($where['fcreatetime']){
                $where['fcreatetime'][1] .= ' 23:59:59';
                $adWhere['fcreatetime'] = ['BETWEEN', $where['fcreatetime']];
            }
            if ($where['fmodifytime']){
                $where['fmodifytime'][1] .= ' 23:59:59';
                $adWhere['fmodifytime'] = ['BETWEEN', $where['fmodifytime']];
            }
            if ($where['fcreator'] != ''){
                $where['fcreator'] = trim($where['fcreator']);
                $adWhere['fcreator'] = ['LIKE', '%'.$where['fcreator'].'%'];
            }
            if ($where['fmodifier'] != ''){
                $where['fmodifier'] = trim($where['fmodifier']);
                $adWhere['fmodifier'] = ['LIKE', '%'.$where['fmodifier'].'%'];
            }
            $total = M('tad')->where($adWhere)->count();
            $sql = M('tad')->getLastSql();
            $data = [];
            if($total > 0){
                $data = M('tad')
                    ->where($adWhere)
                    ->order("$order $orderType")
                    ->page($page, $pageSize)
                    ->select();
                foreach ($data as &$datum) {
                    $datum['fadclasscode_name'] = M('tadclass')->cache(true)->where(['fcode' => ['EQ', $datum['fadclasscode']]])->getField('ffullname');
                    $datum['fadclasscode_name_v2'] = M('hz_ad_class')->cache(true)->where(['fcode' => ['EQ', $datum['fadclasscode_v2']]])->getField('ffullname');
                    $datum['fname'] = M('tadowner')->cache(true, 60)->where(['fid' => ['EQ', $datum['fadowner']]])->getField('fname');
                    $datum['isConfirm'] = $datum['is_sure'] == 0 ? '未确认' : '确认';
                }
            }
            $this->ajaxReturn([
                'data' => $data,
                'total' => $total,
                'code' => 0,
                'adWhere' => $adWhere,
                'where' => $where,
            ]);
        }else{
            $adClassArr = json_encode($this->getAdClassArr());
            $adClassTree = json_encode($this->getAdClassTree());
            $getRegionTree = json_encode(A('Common/Region','Model')->getRegionTree());
            $editAdReason = json_encode(A('Admin/Ad','Model')->getEditAdReason());
            $this->assign('regionTree',$getRegionTree);
            $this->assign(compact('adClassTree', 'adClassArr','editAdReason'));
            $this->display();
        }
    }

	/**
	 * @Des: 编辑广告主题
	 * @Edt: yuhou.wang
	 * @Date: 2019-12-29 03:23:34
	 * @param {Formdata|JSON} $ad_info 更新数据
	 * @return: {JSON} $ret 影响结果
	 */
    public function editAd(){
        $ad_info = I('ad_info') ? I('ad_info') : $this->P;
        $ret = A('Admin/Ad','Model')->editAd($ad_info);
        $this->ajaxReturn($ret);
    }

    public function editAd_V20200114($ad_info)
    {
        $userName = session('personInfo.fname');
        $ad_info['fadowner'] = A('Common/Ad','Model')->getAdOwnerId($ad_info['adowner_name'], $userName);
        $freason = $ad_info['freason'];
        $recordData = [
            'fadid'       => $ad_info['fadid'],
            'wx_id'       => 0,
            'before'      => json_encode(M('tad')->find($ad_info['fadid'])),
            'update_time' => time(),
            'user_name'   => $userName,
            'log'         => $freason,
        ];
        $res = M('tad')
            ->where([
                'fadid' => ['EQ', $ad_info['fadid']]
            ])
            ->save([
                'fadname'            => $ad_info['fadname'],
                'fbrand'             => $ad_info['fbrand'],
                'fadclasscode'       => $ad_info['fadclasscode'],
                'fadowner'           => $ad_info['fadowner'],
                'fadclasscode_v2'    => $ad_info['fadclasscode_v2'],
                'is_sure'            => $ad_info['is_sure'],
                'fstate'             => $ad_info['fstate'],
                'fmodifier'          => $userName,
                'fmodifytime'        => date('Y-m-d H:i:s'),
                'to_opensearch_time' => 0,
            ]);
        // 取消确认同名的广告
        M('tad')
            ->where([
                'fadid' => ['NEQ', $ad_info['fadid']],
                'fadname' => ['EQ', $ad_info['fadname']],
            ])
            ->save([
                'is_sure' => 0,
            ]);
        $logData = [
            'old' => M('tad')->find($ad_info['fadid']),
            'new' => $ad_info
        ];
        if ($res !== false){
            $logData['msg'] = '修改成功';
            A('Common/AliyunLog','Model')->ad_log($logData);
            $recordData['after'] = json_encode(M('tad')->find($ad_info['fadid']));
            M('ad_record')->add($recordData);
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        }else{
            $logData['msg'] = '修改失败';
            A('Common/AliyunLog','Model')->ad_log($logData);
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
    }

    public function getIssueAdSample($where, $fadid)
    {
        if (!$where['mediaName']){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '请输入媒体名',
            ]);
        }
        if (!$where['issueDate']) {
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '请选择发布日期',
            ]);
        }
        $where['issueDate'][0] = substr($where['issueDate'][0], 0, 10);
        $where['issueDate'][1] = substr($where['issueDate'][1], 0, 10);
        if (date('Ym', $where['issueDate'][0]) != date('Ym', $where['issueDate'][1])){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '不支持跨月',
            ]);
        }
        $mediaInfo = M('tmedia')->cache(600)->where(['fmedianame' => ['EQ', $where['mediaName']]])->find();
        if (!$mediaInfo){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '没有找到媒体',
            ]);
        }
        $mediaClass = $mediaInfo['fmediaclassid'][1];
        // 获取行政区划
        $region = M('tmediaowner')
            ->where([
                'fid' => ['EQ', $mediaInfo['fmediaownerid']]
            ])
            ->getField('fregionid');
        $region = substr($region, 0, 2);
        if ($mediaClass == 1){
            $issueTable = 'ttvissue_'.date('Ym', $where['issueDate'][0]) . '_'. $region;
            $issueSubQuery = M($issueTable)
                ->field('distinct fsampleid')
                ->where([
                    'fissuedate' => ['BETWEEN', $where['issueDate']],
                    'fmediaid' => ['EQ', $mediaInfo['fid']],
                ])
                ->buildSql();
            $sample = M('ttvsample')
                ->where([
                    'fid' => ['EXP', ' IN '.$issueSubQuery],
                    'fadid' => ['EQ', $fadid],
                ])
                ->find();
            $sourcePath = $sample['favifilename'];
        }elseif($mediaClass == 2){
            $issueTable = 'tbcissue_'.date('Ym', $where['issueDate'][0]) . '_'. $region;
            $issueSubQuery = M($issueTable)
                ->field('distinct fsampleid')
                ->where([
                    'fissuedate' => ['BETWEEN', $where['issueDate']],
                    'fmediaid' => ['EQ', $mediaInfo['fid']],
                ])
                ->buildSql();
            $sample = M('tbcsample')
                ->where([
                    'fid' => ['EXP', ' IN '.$issueSubQuery],
                    'fadid' => ['EQ', $fadid],
                ])
                ->find();
            $sourcePath = $sample['favifilename'];
        }else{
            $where['issueDate'][0] = date('Y-m-d H:i:s', $where['issueDate'][0]);
            $where['issueDate'][1] = date('Y-m-d H:i:s', $where['issueDate'][1]);
            $sample = M('tpapersample')
                ->where([
                    'fmediaid' => ['EQ', $mediaInfo['fid']],
                    'fissuedate' => ['BETWEEN', $where['issueDate']],
                    'fadid' => ['EQ', $fadid],
                ])
                ->find();
            $sourcePath = $sample['fjpgfilename'];
        }
        $this->ajaxReturn(compact('mediaClass', 'sourcePath'));
    }

    /**
     * @Des: 删除广告(逻辑删除)
     * @Edt: yuhou.wang
     * @param {String} $fadids 将被删除的广告主题ID,多个以","分隔
     * @return: {JSON}
     */    
    public function delAd(){
        $fadids = $this->P['fadids'];
        if(!empty($fadids)){
            // 逻辑删除
            $adRow = [
                'fstate'=>-1,
                'fmodifier_id'=>session('personInfo.fid'),
                'fmodifier'=>session('personInfo.fname'),
                'fmodifytime'=>date('Y-m-d H:i:s'),
                'to_opensearch_time'=>0,
            ];
            $res = M('tad')->where(['fadid'=>['IN',$fadids]])->save($adRow);
            // 记录日志
            $fadidsArr = explode(',',$fadids);
            foreach($fadidsArr as $key=>$val){
                $beforeInfo = M('tad')->where(['fadid'=>$val])->find();
                $recordData = [
                    'fadid' => $val,
                    'wx_id' => session('personInfo.fid'),
                    'before' => json_encode($beforeInfo),
                    'after' => '',
                    'update_time' => time(),
                    'user_name' => session('personInfo.fname'),
                    'log' => '删除广告主题'
                ];
                M('ad_record')->add($recordData);
            }
            $this->ajaxReturn(['code'=>0,'msg'=>'删除成功']);
        }
        $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
    }
    /**
     * @Des: 显示/隐藏
     * @Edt: yuhou.wang
     * @param {String} $fadids 将被显示/隐藏的广告主题ID,多个以","分隔
     * @return: {JSON}
     */    
    public function setShowAd(){
        $fadids = $this->P['fadids'];
        $fshow  = $this->P['fshow'];
        if(!empty($fadids)){
            $adRow = [
                'fshow'        => $fshow,
                'fmodifier_id' => session('personInfo.fid'),
                'fmodifier'    => session('personInfo.fname'),
                'fmodifytime'  => date('Y-m-d H:i:s'),
            ];
            $res = M('tad')->where(['fadid'=>['IN',$fadids]])->save($adRow);
            // 记录日志
            $fadidsArr = explode(',',$fadids);
            foreach($fadidsArr as $key=>$val){
                $recordData = [
                    'fadid'       => $val,
                    'wx_id'       => session('personInfo.fid'),
                    'before'      => '',
                    'after'       => '',
                    'update_time' => time(),
                    'user_name'   => session('personInfo.fname'),
                    'log'         => $fshow == 1 ? '显示广告主题' : '隐藏广告主题',
                ];
                M('ad_record')->add($recordData);
            }
            $this->ajaxReturn(['code'=>0,'msg'=>'操作成功']);
        }
        $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
    }
    /**
     * @Des: 合并广告_v20191229
     * @Edt: yuhou.wang
     * @param {String} fadids 广告ID,多个以半角逗号分隔
     * @param {String} mainfadid 目标广告ID,以此广告为合并目标，其它将被删除
     * @return: {JSON}
     */
    public function mergeAd_v20191229(){
		$fadids    = $this->P['fadids'];
        $mainfadid = $this->P['mainfadid'];
        $mergefids = explode(',',$fadids);
		// 排除目标fadids
        array_splice($mergefids,array_search($mainfadid,$mergefids),1);
        // 更新所有对fadid引用
        $relatedTables = [
            'ttvsample'    => 'fid',
            'tbcsample'    => 'fid',
            'tpapersample' => 'fpapersampleid',
            'tnetissue'    => 'major_key',
            'task_input'   => 'taskid',
            'tnettask'     => 'taskid'
        ];
        foreach($relatedTables as $table=>$key){
            // 记录合并日志
            $records = M($table)->field($key.',fadid')->where(['fadid'=>['IN',$mergefids]])->select();
            if(!empty($records)){
                $beforeInfo = [
                    'table' => $table,
                    'records' => json_encode($records),
                ];
                $recordData = [
                    'fadid'       => $mainfadid,
                    'wx_id'       => session('personInfo.fid'),
                    'before'      => json_encode($beforeInfo),
                    'after'       => '',
                    'update_time' => time(),
                    'user_name'   => session('personInfo.fname'),
                    'log'         => '广告主题合并'
                ];
                M('ad_record')->add($recordData);
                // 更新引用
                M($table)->where(['fadid'=>['IN',$mergefids]])->save(['fadid'=>$mainfadid]);
            }
        }
        // 删除目标外其它广告记录
        $adRow = [
            'fstate'       => -1,
            'comment'      => '已被合并至:'.$mainfadid,
            'fmodifier_id' => session('personInfo.fid'),
            'fmodifier'    => session('personInfo.fname'),
            'fmodifytime'  => date('Y-m-d H:i:s'),
            'to_opensearch_time'=>0,
        ];
        $res = M('tad')->where(['fadid'=>['IN',$mergefids]])->save($adRow);
		$this->ajaxReturn(['code'=>0,'msg'=>'合并成功']);
    }
    /**
     * @Des: 合并广告_V20200117
     * @Edt: yuhou.wang
     * @param {String} fadids 广告ID,多个以半角逗号分隔
     * @param {String} mainfadid 目标广告ID,以此广告为合并目标，其它将被删除
     * @return: {JSON}
     */
    public function mergeAd_V20200117(){
		$fadids    = $this->P['fadids'];
        $mainfadid = $this->P['mainfadid'];
        if((!empty($fadids) || $fadids == 0) && (!empty($mainfadid) || $mainfadid == 0)){
            $mergefids = explode(',',$fadids);
            array_splice($mergefids,array_search($mainfadid,$mergefids),1);// 排除目标fadids
            // 校验目标主题是否待合并或正合并
            $mergeRecord = M('ad_record')->where(['fadid'=>$mainfadid,'merge_state'=>['IN',[0,1]]])->find();
            if(empty($mergeRecord)){
                // 删除目标外其它广告记录
                $comment = '已被合并至:'.$mainfadid;
                $retDel = A('Admin/Ad','Model')->delAd($mergefids,$comment);
                // 创建更新引用异步处理任务
                $task = [
                    'fadid'       => $mainfadid,
                    'wx_id'       => session('personInfo.fid'),
                    'update_time' => time(),
                    'user_name'   => session('personInfo.fname'),
                    'log'         => '广告主题合并',
                    'source_fadid'=> implode(',',$mergefids),
                    'merge_state' => 0,
                ];
                M('ad_record')->add($task);
                $this->ajaxReturn(['code'=>0,'msg'=>'合并提交成功']);
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'目标主题正在合并中，请稍候重试','data'=>$mergeRecord]);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }
    /**
     * @Des: 合并广告_demo
     * @Edt: yuhou.wang
     * @param {String} fadids 广告ID,多个以半角逗号分隔
     * @param {String} mainfadid 目标广告ID,以此广告为合并目标，其它将被删除
     * @return: {JSON}
     */
    public function mergeAd_v20200228(){
		$fadids    = $this->P['fadids'];
        $mainfadid = $this->P['mainfadid'];
        if((!empty($fadids) || $fadids == 0) && (!empty($mainfadid) || $mainfadid == 0)){
            $mergefids = explode(',',$fadids);
            array_splice($mergefids,array_search($mainfadid,$mergefids),1);// 排除目标fadids
            // 校验目标主题是否待合并或正合并
            $mergeRecord = M('ad_record')->where(['fadid'=>$mainfadid,'merge_state'=>['IN',[0,1]]])->find();
            if(empty($mergeRecord)){
                // 隐藏目标外其它广告记录
                $comment = '正在被合并至:'.$mainfadid;
                $retHide = A('Admin/Ad','Model')->hideAd($mergefids,$comment);
                // 删除OpenSearch广告主题联想，页面隐藏不显示已提交合并的主题
                $delOpenAdRet = ['success'=>0,'failure'=>0];
                foreach($mergefids as $key=>$fadid){
                    if($_SERVER['SERVER_NAME'] == 'dmp' || strpos($_SERVER['SERVER_NAME'],'debug') !== false){
                        // 避免测试环境删除正式OpenSearch数据
                        $ret = true;
                    }else{
                        $ret = A('Api/Ad','Model')->delOpenAd($fadid);
                    }
                    if($ret === true){
                        $delOpenAdRet['success']++;
                    }else{
                        $delOpenAdRet['failure']++;
                    }
                }
                // 创建更新引用异步处理任务
                if($delOpenAdRet['success'] == count($mergefids)){
                    // 全部删除成功
                    $task = [
                        'fadid'       => $mainfadid,
                        'wx_id'       => session('personInfo.fid'),
                        'update_time' => time(),
                        'user_name'   => session('personInfo.fname'),
                        'log'         => '广告主题合并',
                        'source_fadid'=> implode(',',$mergefids),
                        'merge_state' => 0,
                    ];
                    M('ad_record')->add($task);
                    $this->ajaxReturn(['code'=>0,'msg'=>'合并提交成功']);
                }else{
                    // 未全部删除成功，则回滚
                    $this->ajaxReturn(['code'=>1,'msg'=>'操作不成功，请联系管理员','data'=>$delOpenAdRet]);
                }
            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'目标主题正在合并中，请稍候重试','data'=>$mergeRecord]);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }
    /**
     * @Des: 合并广告_demo
     * @Edt: yuhou.wang
     * @param {String} fadids 广告ID,多个以半角逗号分隔
     * @param {String} mainfadid 目标广告ID,以此广告为合并目标，其它将被删除
     * @return: {JSON}
     */
    public function mergeAd(){
		$fadids    = $this->P['fadids'];
        $mainfadid = $this->P['mainfadid'];
        $ret = A('Admin/Ad','Model')->mergeAd($fadids,$mainfadid);
        $this->ajaxReturn($ret);
    }
    /**
     * @Des: 合并广告主题-更新引用
     * @Edt: yuhou.wang
     * @Date: 2019-12-29 02:54:27
     * @param {type} 
     * @return: 
     */
    public function mergeAdUpdateRelated(){
        $task = M('ad_record')->master(true)->where(['merge_state'=>1])->order('update_time ASC')->count();
        if($task == 0){
            $task = M('ad_record')->master(true)->where(['merge_state'=>0])->order('update_time ASC')->find();
            if(!empty($task)){
                $data = A('Admin/Ad','Model')->updateRelated($task['id']);
                $this->ajaxReturn(['code'=>0,'msg'=>'合并引用更新完成','data'=>$data]);
            }
            $this->ajaxReturn(['code'=>1,'msg'=>'无主题合并任务','data'=>[]]);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'有主题合并进行中','data'=>$task]);
        }
    }
    /**
     * @Des: 合并广告主题-更新引用_v2
     * @Edt: yuhou.wang
     * @Date: 2019-12-29 02:54:27
     * @param {type} 
     * @return: 
     */
    public function mergeAdUpdateRelated_v2(){
        $task = M('ad_record')->master(true)->where(['merge_state'=>1])->order('update_time ASC')->select();
        if(empty($task)){
            $task = M('ad_record')->master(true)->where(['merge_state'=>0])->order('update_time ASC')->find();
            if(!empty($task)){
                $data = A('Admin/Ad','Model')->updateRelated($task['fadid'],$task['source_fadid']);
                $this->ajaxReturn(['code'=>0,'msg'=>'合并引用更新完成','data'=>$data]);
            }
            $this->ajaxReturn(['code'=>1,'msg'=>'无主题合并任务','data'=>[]]);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'有主题合并进行中','data'=>$task]);
        }
    }
    /**
     * @Des: 合并广告主题-更新引用
     * @Edt: yuhou.wang
     * @Date: 2019-12-29 02:54:27
     * @param {type} 
     * @return: 
     */
    public function mergeAdUpdateRelated_v(){
        // $task = M('ad_record')->master(true)->where(['merge_state'=>1,'log'=>['NEQ','SQL批量_广告主题合并']])->order('update_time ASC')->select();
        session_write_close();
        set_time_limit(600);
        if(I('auto') == '1'){
            header("Content-type: text/html; charset=utf-8");
            echo '<meta http-equiv="refresh"content="0.1"/>';
        }
        if(empty($task)){
            $task = M('ad_record')->master(true)->where(['merge_state'=>0])->order('update_time ASC')->find();
            // $task = M('ad_record')->master(true)->where(['merge_state'=>0,'log'=>['EQ','SQL批量_广告主题合并']])->order('update_time ASC')->find();
            // $task = M('ad_record')->master(true)->where(['merge_state'=>0,'log'=>['EQ','SQL批量_广告主题合并']])->order('RAND()')->find();
            // $task = M('ad_record')->master(true)->where(['merge_state'=>0])->order('RAND()')->find();
            $sql = M('ad_record')->getLastSql();
            if(!empty($task)){
                $data = A('Admin/Ad','Model')->updateRelated($task['fadid'],$task['source_fadid']);
                // $this->ajaxReturn(['code'=>0,'msg'=>'合并引用更新完成','data'=>$data]);
            }
            // $this->ajaxReturn(['code'=>1,'msg'=>'无主题合并任务','data'=>[]]);
        }else{
            // $this->ajaxReturn(['code'=>1,'msg'=>'有主题合并进行中','data'=>$task]);
        }
        echo date('Y-m-d H:i:s');
    }
    /**
     * @Des: 合并广告主题-更新引用补丁
     * @Edt: yuhou.wang
     * @Date: 2019-12-29 05:33:41
     * @param {type} 
     * @return: 
     */
    public function mergeAdUpdateRelatedSp(){
        $ret = A('Admin/Ad','Model')->mergeAdUpdateRelatedSp();
        $this->ajaxReturn($ret);
    }
	/**
	 * @Des: 获取修改广告原因清单
	 * @Edt: yuhou.wang
	 * @Date: 2020-01-13 14:06:13
	 * @param {type} 
	 * @return: 
	 */	
	public function getEditAdReason(){
        $data = A('Admin/Ad','Model')->getEditAdReason();
        $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$data]);
    }
    /**
     * @Des: 获取广告内容(广告主题)操作记录
     * @Edt: yuhou.wang
     * @param {type} $fadid 广告主题ID
     * @return: 
     */   
    public function getHistory(){
        $fadid = (I('fadid') || I('fadid') === 0) ? I('fadid') : $this->P['fadid'];
        $data = A('Admin/Ad','Model')->getHistory($fadid);
        $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$data]);
    }
}