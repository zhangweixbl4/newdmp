Vue.component('adRecheck', {
  template: `
    <div v-loading="loading">
      <el-form ref="sform" inline>
        <el-form-item label="复核状态：">
          <el-select v-model="where.fstatus3" placeholder="" filterable clearable style="width:100%">
            <el-option :label="item.label" :value="item.value" v-for="item in stateMap" :key="item.value"></el-option>
          </el-select>
        </el-form-item>
        <el-form-item label="广告名称：">
          <el-input v-model="where.adName" clearable></el-input>
        </el-form-item>
        <el-form-item label="广告类别：">
          <el-cascader filterable :options="adClassArr" v-model="searchTempWhere.searchAdClassArr1" change-on-select style="width:100%"
          clearable></el-cascader>
        </el-form-item>
        <el-form-item label="媒介类型：">
          <el-select v-model="where.media_class" clearable style="width:100%">
            <el-option label="电视" value="1"></el-option>
            <el-option label="广播" value="2"></el-option>
            <el-option label="报纸" value="3"></el-option>
          </el-select>
        </el-form-item>
        <el-form-item label="媒体名称：">
          <el-input v-model="where.mediaName" clearable></el-input>
        </el-form-item>
        <el-form-item label="地区：" class="regionSearch">
          <el-cascader v-model="searchTempWhere.region" :options="regionTree" change-on-select :filterable="true" clearable style="width:150px"></el-cascader>
          <el-checkbox v-model="searchTempWhere.onlyThisLevel">仅本级</el-checkbox>
        </el-form-item>
        <el-form-item label="平台：">
          <el-select filterable clearable :options="customerArr" @clear="clearCustomer" v-model="searchTempWhere.customer" change-on-select placeholder="请选择" style="width:100%">
            <el-option
              v-for="item in customerArr"
              :key="item.value"
              :label="item.label"
              :value="item.value">
            </el-option>
          </el-select>
        </el-form-item>
        <el-form-item style="text-align: right">
          <el-button type="primary" @click="getTable">搜索</el-button>
          <el-button @click="resetWhere">重置搜索条件</el-button>
        </el-form-item>
      </el-form>
      <el-table border stripe :height="tableHeight" :data="table">
        <el-table-column label="总局意见" width="150">
          <template slot-scope="scope">
            <div v-if="scope.row.tgj_result">
              <div>
                {{scope.row.tgj_trename}}
              </div>
              <div style="white-space: nowrap; text-overflow:ellipsis; overflow:hidden;color: #409eff;cursor:pointer;" @click="checkAttachInfo(scope.row.id, 30)">
                <el-tooltip effect="dark" :content="scope.row.tgj_result" placement="top-start">
                  <span>{{scope.row.tgj_result}}</span>
                </el-tooltip>
              </div>
              <div>
                {{scope.row.tgj_username}}
              </div>
              <div>
                {{scope.row.tgj_time}}
              </div>
            </div>
          </template>
        </el-table-column>
        <el-table-column label="省局意见" width="150">
          <template slot-scope="scope">
            <div v-if="scope.row.tshengj_result">
              <div>
                {{scope.row.tshengj_trename}}
              </div>
              <div>
              <div style="white-space: nowrap; text-overflow:ellipsis; overflow:hidden;color: #409eff;cursor:pointer;" @click="checkAttachInfo(scope.row.id, 20)">
                <el-tooltip effect="dark" :content="scope.row.tshengj_result" placement="top-start">
                  <span>{{scope.row.tshengj_result}}</span>
                </el-tooltip>
              </div>
              <div>
                {{scope.row.tshengj_username}}
              </div>
              <div>
                {{scope.row.tshengj_time}}
              </div>
            </div>
          </template>
        </el-table-column>
        <el-table-column label="市局意见" width="150">
          <template slot-scope="scope">
            <div v-if="scope.row.tshij_result">
              <div>
                {{scope.row.tshij_trename}}
              </div>
              <div>
                <div style="white-space: nowrap; text-overflow:ellipsis; overflow:hidden;color: #409eff;cursor:pointer;" @click="checkAttachInfo(scope.row.id, 10)">
                  <el-tooltip effect="dark" :content="scope.row.tshij_result" placement="top-start">
                    <span>{{scope.row.tshij_result}}</span>
                  </el-tooltip>
                </div>
              </div>
              <div>
                {{scope.row.tshij_username}}
              </div>
              <div>
                {{scope.row.tshij_time}}
              </div>
            </div>
          </template>
        </el-table-column>
        <el-table-column label="区县意见" width="150">
          <template slot-scope="scope">
            <div v-if="scope.row.tquj_result">
              <div>
                {{scope.row.tquj_trename}}
              </div>
              <div>
                <div style="white-space: nowrap; text-overflow:ellipsis; overflow:hidden;color: #409eff;cursor:pointer;"  @click="checkAttachInfo(scope.row.id, 0)">
                  <el-tooltip effect="dark" :content="scope.row.tquj_result" placement="top-start">
                    <span>{{scope.row.tquj_result}}</span>
                  </el-tooltip>
                </div>
              </div>
              <div>
                {{scope.row.tquj_username}}
              </div>
              <div>
                {{scope.row.tquj_time}}
              </div>
            </div>
          </template>
        </el-table-column>
        <el-table-column :label="item.label" :prop="item.value" v-for="(item, index) in tableField" :key="index">
        </el-table-column>
        <el-table-column label="操作" fixed="right">
          <template slot-scope="scope">
            <div v-if="scope.row.fstatus3 == 30">
              <el-button type="text" @click="editAd(scope.row.fid)">
                处理
              </el-button>
              <el-button type="text" @click="check(scope.row.fid, true)">
                复核通过
              </el-button>
              <el-button type="text" @click="check(scope.row.fid, false)">
                复核不通过
              </el-button>
            </div>
          </template>
        </el-table-column>
      </el-table>
      <div style="text-align: right">
        <el-pagination @size-change="pageSizeChange" @current-change="pageChange" :current-page="page" :page-sizes="[10 ,50, 100, 200, 400]"
        :page-size="pageSize" background layout="total, sizes, prev, pager, next, jumper" :total="total">
        </el-pagination>
      </div>
      <el-dialog title="附件列表" :visible.sync="showAttachList">
        <ol>
          <li v-for="(item, index) in attachList" :key="index">
            <a :href="item.fattach_url" target="_blank">{{item.fattach}}</a>
          </li>
        </ol>
      </el-dialog>
      <el-dialog title="详情" width="80%" :visible.sync="showAdInfo">
        <task-info2 :task="adInfo" ref="task" :type="'input'"></task-info2>
      </el-dialog>
    </div>
  `,
  data() {
    return {
      stateMap,
      tableField,
      adClassArr,
      regionTree,
      customerArr,
      adClassTree,
      illegalTypeArr,
      illegalArr,
      where: {
        fstatus3: 30
      },
      searchTempWhere: {},
      table: [],
      page: 1,
      pageSize: 10,
      total: 0,
      loading: false,
      showAdInfo: false,
      showAttachList: false,
      attachList: [],
      adInfo: {},
      currentId: 0,
      tableHeight: '300px'
    }
  },
  created() {
    this.getTable()
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight()
      document.querySelector('#app').style.opacity = 1
    })
    window.onresize = () => {
      this.getTableHeight()
    }
  },
  methods: {
    getTableHeight() {
      let formHeight = this.$refs.sform.$el.clientHeight
      this.tableHeight = document.body.clientHeight - formHeight - 104 + 'px'
    },
    getTable: function () {
      this.page = 1
      this.where.onlyThisLevel = this.searchTempWhere.onlyThisLevel ? 1 : 0
      this.where.customerId = this.searchTempWhere.customer ? this.searchTempWhere.customer : null
      if (this.searchTempWhere.searchAdClassArr1 && this.searchTempWhere.searchAdClassArr1.length !== 0) {
        this.where.adClass = this.searchTempWhere.searchAdClassArr1[this.searchTempWhere.searchAdClassArr1.length - 1]
      }
      if (this.searchTempWhere.region && this.searchTempWhere.region.length !== 0) {
        this.where.regionId = this.searchTempWhere.region[this.searchTempWhere.region.length - 1]
      }
      this.searchReq()
    },
    pageChange: function (page) {
      this.page = page
      this.searchReq()
    },
    pageSizeChange: function (val) {
      this.pageSize = val
      this.getTable()
    },
    resetWhere: function () {
      this.where = {
        fstatus3: 30
      }
      this.searchTempWhere = {}
      this.getTable()
    },
    clearCustomer:function(){
      this.searchTempWhere.customer = null
      this.getTable()
    },
    searchReq: function () {
      this.loading = true
      $.post('', {
        where: this.where,
        page: this.page,
        pageSize: this.pageSize,
      }).then((res) => {
        this.loading = false
        this.table = res.table
        this.total = +res.total
      })
    },
    checkAttachInfo: function (id, lvl) {
      this.loading = true
      $.post('getAttachList', {
        id,
        lvl
      }).then((res) => {
        this.attachList = res
        this.loading = false
        this.showAttachList = true
      })
    },
    editAd: function (id) {
      this.loading = true
      $.post('getInfo', {
        id
      }).then((res) => {
        this.loading = false
        if (res.code == 0) {
          this.adInfo = res.data
          this.showAdInfo = true
          this.$nextTick(() => {
            this.$refs.task.dataInit()
          })
        } else {
          $this.$message(res.msg)
        }
      })
    },
    check: function (id, pass) {
      let url = pass ? 'passCheck' : 'rejectCheck'
      this.$prompt('请输入理由', '', {
        inputPattern: /^[\s\S]*.*[^\s][\s\S]*$/,
        inputErrorMessage: '请输入理由',
        inputType: 'textarea',
        closeOnClickModal: false
      }).then(({value}) => {
        this.loading = true
        $.post(url, {
          id,
          reson: value
        }).then((res) => {
          this.loading = false
          if (res.code == 0) {
            this.$message.success(res.msg)
            this.$emit('update')
          } else {
            this.$message.error(res.msg)
          }
          this.searchReq()
        })
      }).catch(() => {
      })
    },
  },
})