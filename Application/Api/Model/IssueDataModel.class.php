<?php
namespace Api\Model;
use Think\Exception;
use Think\Log;


class IssueDataModel{
	
	
	
	
	/**
	* 根据日期媒介分批获取发布记录并写入 发布清单表
	* @Param	String $month_date 日期 。如"2018-06-03"
	* @Param	Int media_id 媒体ID
	* @Return	void 返回的数据
	*/
    public function issue_data($month_date,$media_id){
	
		$datas_count = 0;
		$w_count = 0;

		ini_set('memory_limit','512M');
		$mediaInfo = M('tmedia')
								->cache(true,86400)
								->field('tmediaowner.fregionid,fmedianame,fmediaclassid')
								->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>$media_id))->find();//查询媒介信息
		
		
		
		$mediaclassid = substr($mediaInfo['fmediaclassid'],0,2);
		
		if($mediaclassid == '01'){
			$media_tab = 'tv';
			$table_name = 'ttvissue_'.date('Ym',strtotime($month_date)).'_'.substr($mediaInfo['fregionid'],0,2);
		}elseif($mediaclassid == '02'){
			$media_tab = 'bc';
			$table_name = 'tbcissue_'.date('Ym',strtotime($month_date)).'_'.substr($mediaInfo['fregionid'],0,2);
		}elseif($mediaclassid == '03'){
			$media_tab = 'paper';
			$table_name = 'tpaperissue';
		}else{
			return false;
		}
		

		$data0ListField = 'fmediaclassid,fsampleid,fmedianame,fregionid,fadname,fadowner,fbrand,fspokesman,fadclasscode,fstarttime,fendtime,sam_source_path';
		$data0ListField .= ',fmediaid,fissuedate,flength,fillegaltypecode,fadmanuno,fmanuno,fadapprno,fapprno,fadent,fent,fentzone,fexpressioncodes,fillegalcontent';
		

		if($media_tab == 'tv' || $media_tab == 'bc'){
			
			try{//
				$data2List = M($table_name)
							->alias('tissue')
							->cache(true,1200)
							->field('
									tissue.fsampleid,
									tissue.fmediaid,
									tissue.fstarttime,
									tissue.fendtime,
									tad.fadname,
									tad.fadowner,
									tad.fbrand,
									tsample.fspokesman,
									tad.fadclasscode,
									tissue.fissuedate,
									tsample.fillegaltypecode,
									tsample.fadmanuno,
									tsample.fmanuno,
									tsample.fadapprno,
									tsample.fapprno,
									tsample.fadent,
									tsample.fent,
									tsample.fentzone,
									tsample.fexpressioncodes,
									tsample.fillegalcontent,
									tsample.favifilename as sam_source_path
									
									
									')

							->join('t'.$media_tab.'sample as tsample on tsample.fid = tissue.fsampleid')
							->join('tad on tad.fadid = tsample.fadid')
							->where(array('tissue.fmediaid'=>$media_id,'tissue.fissuedate'=>strtotime($month_date),'left(tad.fadclasscode,2)'=>array('in','01,02,03,05,06')))
							->select();	
			}catch(Exception $error) {//
				return false;
			}
			
			
			$datas = array();
			$open_datas = array();
			foreach($data2List as $data2){
				
				
				$e_a_data['fmediaclassid'] = $mediaclassid;//媒介类型
				$e_a_data['fsampleid'] = $data2['fsampleid']; //样本id
				$e_a_data['fmediaid'] = $data2['fmediaid']; //媒介id
				$e_a_data['fmedianame'] = $mediaInfo['fmedianame'];//媒介名称
				$e_a_data['fregionid'] = $mediaInfo['fregionid']; //地区id
				$e_a_data['fadname'] = $data2['fadname'];//广告名称
				$e_a_data['fadowner'] = $data2['fadowner'];//广告主
				$e_a_data['fbrand'] = $data2['fbrand']; //品牌
				$e_a_data['fspokesman'] = $data2['fspokesman']; //代言人
				$e_a_data['fadclasscode'] = $data2['fadclasscode']; //广告类别id
				$e_a_data['fstarttime'] = $data2['fstarttime']; //发布开始时间戳
				$e_a_data['fendtime'] = $data2['fendtime']; //发布结束时间戳
				$e_a_data['fissuedate'] = $data2['fissuedate']; //发布日期
				$e_a_data['flength'] = $data2['fendtime'] - $data2['fstarttime']; //发布时长(秒)
				$e_a_data['fillegaltypecode'] = $data2['fillegaltypecode']; //违法类型
				$e_a_data['fadmanuno'] = $data2['fadmanuno']; //广告中标识的生产批准文号
				$e_a_data['fmanuno'] = $data2['fmanuno']; //生产批准文号
				$e_a_data['fadapprno'] = $data2['fadapprno']; //广告中标识的广告批准文号
				$e_a_data['fapprno'] = $data2['fapprno']; //广告批准文号
				$e_a_data['fadent'] = $data2['fadent']; //广告中标识的生产企业（证件持有人）名称
				$e_a_data['fent'] = $data2['fent']; //生产企业（证件持有人）名称
				$e_a_data['fentzone'] = $data2['fentzone']; //生产企业（证件持有人）所在地区
				$e_a_data['fexpressioncodes'] = $data2['fexpressioncodes']; //'违法表现代码，用“;”分隔
				$e_a_data['fillegalcontent'] = strval($data2['fillegalcontent']); //'涉嫌违法内容
				$e_a_data['sam_source_path'] = strval($data2['sam_source_path']); //'素材路径
				
				$identify = md5($media_id.'_'.$data2['fstarttime']); //'识别码
				$e_a_data['identify'] = $identify;
				$e_a_data['primarykey'] = $identify;
				$item = array();
				$item['cmd'] = 'add';
				$item["fields"] = $e_a_data; 
				$open_datas[] = $item;
				/* $attribute_columns = array();
				
				foreach($e_a_data as $field => $value){
					if(!in_array($field,array('identify','fmediaid','fissuedate'))){
						$attribute_columns[] = array($field,$value);
					}
				}
				
				$datas[] = array ( // 第一行
							"operation_type" => 'PUT',            //操作是PUT
							'condition' => 'IGNORE',
							'primary_key' => array (
								array('identify',strval($e_a_data['identify'])),
								array('fmediaid',intval($e_a_data['fmediaid'])),
								array('fissuedate',intval($e_a_data['fissuedate'])),
							),
							'attribute_columns' => $attribute_columns,
						);//组装写入tablestore的数据结构
						
				if(	count($datas) == 200){//判断数据是否达到200条，因为table的最大限制是一次性写入200条
					$response = A('Common/TableStore','Model')->batchWriteRow('tradition_ad_issue2',$datas);//写入数据到tablestore
					
					$datas_count += count($datas);
					$w_count += $response['successNum'];
					
					$datas = array();
				} */			
			}

			/* if(	count($datas) > 0){//
					//var_dump($datas);
					$response = A('Common/TableStore','Model')->batchWriteRow('tradition_ad_issue2',$datas);//写入数据到tablestore
					$datas_count += count($datas);
					$w_count += $response['successNum'];
					$datas = array();
			} */	
			
			$rr = A('Common/OpenSearch','Model')->push('tradition_ad_issue',$open_datas);			
			  
			
		}elseif($media_tab == 'paper'){
		
			
			$data2List = M($table_name)
							->alias('tissue')
							->cache(true,1200)
							->field('
									tissue.fpapersampleid as fsampleid,
									tissue.fmediaid,
									0 as fstarttime ,
									0 as fendtime,
									tad.fadname,
									tad.fadowner,
									tad.fbrand,
									tsample.fspokesman,
									tad.fadclasscode,
									tissue.fissuedate,
									tsample.fillegaltypecode,
									tsample.fadmanuno,
									tsample.fmanuno,
									tsample.fadapprno,
									tsample.fapprno,
									tsample.fadent,
									tsample.fent,
									tsample.fentzone,
									tsample.fexpressioncodes,
									tsample.fillegalcontent,
									tsample.fjpgfilename as sam_source_path
									
									
									')

							->join('tpapersample as tsample on tsample.fpapersampleid = tissue.fpapersampleid')
							->join('tad on tad.fadid = tsample.fadid')
							->where(array('tissue.fmediaid'=>$media_id,'tissue.fissuedate'=>$month_date,'left(tad.fadclasscode,2)'=>array('in','01,02,03,05,06')))
							->select();	
			$datas = array();
			$open_datas = array();
			foreach($data2List as $data2){
				
				$e_a_data['fmediaclassid'] = $mediaclassid;//媒介类型
				$e_a_data['fsampleid'] = $data2['fsampleid']; //样本id
				$e_a_data['fmediaid'] = $data2['fmediaid']; //媒介id
				$e_a_data['fmedianame'] = $mediaInfo['fmedianame'];//媒介名称
				$e_a_data['fregionid'] = $mediaInfo['fregionid']; //地区id
				$e_a_data['fadname'] = $data2['fadname'];//广告名称
				$e_a_data['fadowner'] = $data2['fadowner'];//广告主
				$e_a_data['fbrand'] = $data2['fbrand']; //品牌
				$e_a_data['fspokesman'] = $data2['fspokesman']; //代言人
				$e_a_data['fadclasscode'] = $data2['fadclasscode']; //广告类别id
				$e_a_data['fstarttime'] = $data2['fstarttime']; //发布开始时间戳
				$e_a_data['fendtime'] = $data2['fendtime']; //发布结束时间戳
				$e_a_data['fissuedate'] = strtotime($data2['fissuedate']); //发布日期
				$e_a_data['flength'] = $data2['fendtime'] - $data2['fstarttime']; //发布时长(秒)
				$e_a_data['fillegaltypecode'] = $data2['fillegaltypecode']; //违法类型
				$e_a_data['fadmanuno'] = $data2['fadmanuno']; //广告中标识的生产批准文号
				$e_a_data['fmanuno'] = $data2['fmanuno']; //生产批准文号
				$e_a_data['fadapprno'] = $data2['fadapprno']; //广告中标识的广告批准文号
				$e_a_data['fapprno'] = $data2['fapprno']; //广告批准文号
				$e_a_data['fadent'] = $data2['fadent']; //广告中标识的生产企业（证件持有人）名称
				$e_a_data['fent'] = $data2['fent']; //生产企业（证件持有人）名称
				$e_a_data['fentzone'] = $data2['fentzone']; //生产企业（证件持有人）所在地区
				$e_a_data['fexpressioncodes'] = $data2['fexpressioncodes']; //'违法表现代码，用“;”分隔
				$e_a_data['fillegalcontent'] = strval($data2['fillegalcontent']); //'涉嫌违法内容
				$e_a_data['sam_source_path'] = strval($data2['sam_source_path']); //'素材路径
				
				$identify = md5($media_id.'_'.$month_date.'_'.$data2['sam_source_path']); //'识别码
				$e_a_data['identify'] = $identify;
				$e_a_data['primarykey'] = $identify;
				$item = array();
				$item['cmd'] = 'add';
				$item["fields"] = $e_a_data; 
				$open_datas[] = $item;
				/* $attribute_columns = array();
				foreach($e_a_data as $field => $value){//组装写入tablestore的数据结构
					if(!in_array($field,array('identify','fmediaid','fissuedate'))){
						$attribute_columns[] = array($field,$value);
					}
				}
				
				$datas[] = array ( // 第一行
							"operation_type" => 'PUT',            //操作是PUT
							'condition' => 'IGNORE',
							'primary_key' => array (
								array('identify',$e_a_data['identify']),
								array('fmediaid',intval($fmediaid)),
								array('fissuedate',$e_a_data['fissuedate']),
							),
							'attribute_columns' => $attribute_columns,
						);//组装写入tablestore的数据结构
				if(	count($datas) == 200){//判断数据是否达到200条，因为table的最大限制是一次性写入200条
					$response = A('Common/TableStore','Model')->batchWriteRow('tradition_ad_issue2',$datas);//写入数据到tablestore
					$datas_count += count($datas);
					$w_count += $response['successNum'];
					$datas = array();
				} */		
			}								
			/* if(	count($datas) > 0){//
				$response = A('Common/TableStore','Model')->batchWriteRow('tradition_ad_issue2',$datas);//写入数据到tablestore
				$datas_count += count($datas);
				$w_count += $response['successNum'];
				$datas = array();
			} */
			
			$rr = A('Common/OpenSearch','Model')->push('tradition_ad_issue',$open_datas);
			
		}
		
	
	
	
	
	
	
		echo $media_id.'	'.$mediaInfo['fmedianame'].'	'.$month_date.'	'. (count($open_datas)) .'	'."<br />\n";
	
	
	
	
	
	
	
	
	
	}
	
	
	
	
	/**
	* 根据日期媒介分批获取发布记录并写入 发布清单表，该功能用于写入客户数据
	* @Param	String $month_date 日期 。如"2018-06-03"
	* @Param	Int media_id 媒体ID
	* @Return	void 返回的数据
	*/
    public function make_customer_ad_issue($month_date,$media_id,$forgid){
		
		
		if(!$forgid){
			
			return array('openInputNum'=>0,'sqlInputNum'=>0, 'openDelNum'=>0,'sqlDelNum'=>0 );
		}

		$datas_count = 0;
		$success_count = 0;

		ini_set('memory_limit','512M');
		$mediaInfo = M('tmedia')
								->cache(true,86400)
								->field('tmediaowner.fregionid,fmedianame,fmediaclassid,tmediaowner.fid as fmediaownerid,tmediaowner.fname as fmediaownername')
								->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>$media_id))->find();//查询媒介信息
		
		
		
		$mediaclassid = substr($mediaInfo['fmediaclassid'],0,2);
		
		if($mediaclassid == '01'){
			$media_tab = 'tv';
			$table_name = 'ttvissue_'.date('Ym',strtotime($month_date)).'_'.substr($mediaInfo['fregionid'],0,2);
			$sam_table_name = 't'.$media_tab.'sample';
			/* if(substr($mediaInfo['fregionid'],0,4) == '3703' && strtotime($month_date) >= strtotime('2018-07-01') && strtotime($month_date) <= strtotime('2019-01-01')){
				$table_name = 'ttvissue_2018_370300';
				$sam_table_name = 'ttvsample_370300';
			} */
			
		}elseif($mediaclassid == '02'){
			$media_tab = 'bc';
			$table_name = 'tbcissue_'.date('Ym',strtotime($month_date)).'_'.substr($mediaInfo['fregionid'],0,2);
			$sam_table_name = 't'.$media_tab.'sample';
			/* if(substr($mediaInfo['fregionid'],0,4) == '3703' && strtotime($month_date) >= strtotime('2018-07-01') && strtotime($month_date) <= strtotime('2019-01-01')){
				$table_name = 'tbcissue_2018_370300';
				$sam_table_name = 'tbcsample_370300';
			} */
		}elseif($mediaclassid == '03'){
			$media_tab = 'paper';
			$table_name = 'tpaperissue';
			$sam_table_name = 'tpapersample';
			/* if(substr($mediaInfo['fregionid'],0,4) == '3703' && strtotime($month_date) >= strtotime('2018-07-01') && strtotime($month_date) <= strtotime('2019-01-01')){
				$table_name = 'tpaperissue_370300';
				$sam_table_name = 'tpapersample_370300';
			} */
		}else{
			return false;
		}
		

		$data0ListField = 'fmediaclassid,fsampleid,fmedianame,fregionid,fadname,fadowner,fbrand,fspokesman,fadclasscode,fstarttime,fendtime,sam_source_path';
		$data0ListField .= ',fmediaid,fissuedate,flength,fillegaltypecode,fadmanuno,fmanuno,fadapprno,fapprno,fadent,fent,fentzone,fexpressioncodes,fillegalcontent';
		

		if($media_tab == 'tv' || $media_tab == 'bc'){
			
			try{//
				$data2List = M($table_name)
							->alias('tissue')
							->cache(true,60)
							->field('
									tissue.fsampleid,
									tissue.fmediaid,
									tissue.fstarttime,
									tissue.fendtime,
									tad.fadname,
									tad.fadowner,
									tad.fbrand,
									tsample.fspokesman,
									tad.fadclasscode,
									tad.fadclasscode_v2,
									tissue.fissuedate,
									tsample.fillegaltypecode,
									tsample.fadmanuno,
									tsample.fmanuno,
									tsample.fadapprno,
									tsample.fapprno,
									tsample.fadent,
									tsample.fent,
									tsample.fentzone,
									tsample.fexpressioncodes,
									tsample.fillegalcontent,
									tsample.favifilename as sam_source_path
									
									
									')

							->join($sam_table_name.' as tsample on tsample.fid = tissue.fsampleid')
							->join('tad on tad.fadid = tsample.fadid and tad.fadid > 0')
							->where(array('tissue.fmediaid'=>$media_id,'tissue.fissuedate'=>strtotime($month_date)))
							->group('tissue.fmediaid,tissue.fstarttime')
							->select();	
				
			}catch(Exception $error) {//
				$data2List = array();
				
			}
			
			
			/* $add_datas = array();
			$removeDataList = [];
			$removeDataList[] = array('fmediaid'=>'11000010002975','fadname'=>'本草新说： 葛洪桂龙药膏');
			$removeDataList[] = array('fmediaid'=>'11000010002975','fadname'=>'本草新说：葛洪桂龙药膏');
			$removeDataList[] = array('fmediaid'=>'11000010002978','fadname'=>'本草新说：葛洪桂龙药膏');
			$removeDataList[] = array('fmediaid'=>'11000010002943','fadname'=>'本草新说：葛洪桂龙药膏');
			$removeDataList[] = array('fmediaid'=>'11000010002943','fadname'=>'功夫中医：鹿心骆驼血');
			$removeDataList[] = array('fmediaid'=>'11000010002943','fadname'=>'中医诊病：惠血生胶囊');
			$removeDataList[] = array('fmediaid'=>'11000010006726','fadname'=>'问诊好医生：代谢养元颗粒');
			$removeDataList[] = array('fmediaid'=>'11000010002942','fadname'=>'本草新说： 葛洪桂龙药膏');
			$removeDataList[] = array('fmediaid'=>'11000010002942','fadname'=>'本草新说：葛洪桂龙药膏');
			$removeDataList[] = array('fmediaid'=>'11000010002942','fadname'=>'功夫中医：鹿心骆驼血');
			$removeDataList[] = array('fmediaid'=>'11000010002942','fadname'=>'医药春秋：存元葆鹿茸洋参片');
			$removeDataList[] = array('fmediaid'=>'11000010006723','fadname'=>'扶阳健康正论:鹿血方');
			$removeDataList[] = array('fmediaid'=>'11000010006723','fadname'=>'扶阳健康正论：鹿血方&升阳开胃饮');
			$removeDataList[] = array('fmediaid'=>'11000010006723','fadname'=>'健康是福：茸血补脑液');
			$removeDataList[] = array('fmediaid'=>'11000010006723','fadname'=>'健康是福：茸血补脑液&蒸骨疗法');
			$removeDataList[] = array('fmediaid'=>'11000010006723','fadname'=>'仁心说医事：正气保灵芝益寿胶囊');
			$removeDataList[] = array('fmediaid'=>'11000010006723','fadname'=>'问诊好医生：代谢养元颗粒');
			$removeDataList[] = array('fmediaid'=>'11000010006723','fadname'=>'问诊好医生：代谢养元颗粒&激光溶栓治疗仪');
			$removeDataList[] = array('fmediaid'=>'11000010006723','fadname'=>'问诊好医生：代谢养元颗粒&激光溶栓治疗仪');
			$removeDataList[] = array('fmediaid'=>'11000010002976','fadname'=>'医药春秋：存元葆鹿茸洋参片');
			$removeDataList[] = array('fmediaid'=>'11000010002942','fadname'=>'功夫中医：五加茸血口服液');
			

			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'靶向骨细胞自愈疗法：白云山华佗风痛宝片');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'本草新说： 葛洪桂龙药膏');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'本草新说：葛洪桂龙药膏');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'本草新说：葛洪牌桂龙药膏');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'参芪鹿茸口服液');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'扶阳健康正论:鹿血方');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'扶阳健康正论：鹿血方&升阳开胃饮');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'扶阳健康正论：迅康参茸灵芝胶囊');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'葛洪桂龙药膏');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'功夫中医：百草益寿复合益生菌');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'功夫中医：百草益寿五加茸血口服液');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'功夫中医：鹿心骆驼血');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'功夫中医：五加茸血口服液');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'华夏食医：二十八味环阳方');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'华夏食医：二十八味环阳方&好状态益生菌');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'健康是福：茸血补脑液');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'健康是福：茸血补脑液&蒸骨疗法');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'仁心说医事：正气保灵芝益寿胶囊');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'谈骨论筋话健康:蚂蚁双参通痹丸');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'谈骨论筋话健康：蚂蚁双参通痹丸');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'谈骨论筋话健康：三晋蚂蚁双参通痹丸');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'问诊好医生：代谢养元颗粒');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'问诊好医生：代谢养元颗粒&地龙蛋白饮');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'问诊好医生：代谢养元颗粒&激光溶栓治疗仪');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'医药春秋：存葆元鹿茸洋参片');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'医药春秋：存元葆鹿茸洋参片');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'医药春秋：梅邦虫草精口服液');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'中医论扶阳：参芪鹿茸口服液');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'中医诊病：惠血生胶囊');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'中医诊病：启川胶囊');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'常林瞧病:五加茸血口服液');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'功夫中医:五加茸血口服液');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'挂号中医:惠血生胶囊');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'三阳开泰养生论坛:盈姿宝牌破壁灵芝孢子粉胶囊');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'三阳开泰养生论坛:灵芝孢子油软胶囊');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'雨坤健康正论:海参肽胶囊(非得牌海参肽胶囊)');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'参茸胶囊');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'医药春秋:鹿茸洋参片');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'对话大医生');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'营养大观园');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'清宫长春胶囊');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'诺丽果酵素');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'家里有个中医真好');

			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'深海鱼油蛋白粉');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'牡蛎蛹虫草粉');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'蓝莓叶黄素粉');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'蚂蚁双参通痹丸');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'傅山牌固本延龄丸');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'茸血补脑液');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'固本延龄丸');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'五加茸血口服液');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'惠血生胶囊');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'海参肽胶囊(非得牌海参肽胶囊)');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'十味手参散');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'盈姿宝牌破壁灵芝孢子粉胶囊');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'归元健脑片');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'清晨中医');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'唐愈康');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'大补丸');
			
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'参龙虫草益肾胶囊');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'扶正存元方');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'血糖管理三大处方');
			$removeDataList[] = array('fregionid'=>'130000','fadname'=>'健康是福：');
			
				

			
			

			
			
			



			
			$removeDataList[] = array('fregionid'=>'43','fadname'=>'乐牙仕魔力洁牙泡泡');
			$removeDataList[] = array('fregionid'=>'43','fadname'=>'厚宗堂黑果枸杞');
			$removeDataList[] = array('fregionid'=>'43','fadname'=>'厚宗堂黑枸杞');
			$removeDataList[] = array('fregionid'=>'43','fadname'=>'明园蜂巢蜜');
			$removeDataList[] = array('fregionid'=>'43','fadname'=>'百灸邦远红外磁疗贴');
			$removeDataList[] = array('fregionid'=>'43','fadname'=>'益仕康医用冷敷眼贴');
			
			
			 */
			
			  
 
 

 

			
			
			foreach($data2List as $data2){
				/* $bre = false;
				foreach($removeDataList as $removeData){#判断数据是否需要排除
					if($removeData['fmediaid'] && $data2['fmediaid'] == $removeData['fmediaid'] && strstr($data2['fadname'],$removeData['fadname'])){
						
						$bre = true;
						break;
					}elseif($removeData['fregionid'] && substr($mediaInfo['fregionid'],0,mb_strlen($removeData['fregionid'])) == $removeData['fregionid'] && strstr($data2['fadname'],$removeData['fadname'])){
						$bre = true;
						break;
					}
					
					
					
				}
				if($bre){
					$dmpLogs2 = A('Common/AliyunLog','Model')->dmpLogs2('extract_issue_err',$data2);		
					continue;
				} */
				
				$adownerInfo = M('tadowner')->cache(true,600)->field('fname,fdomain,fregionid')->where(array('fid'=>$data2['fadowner']))->find();
				$e_a_data = array();
				
				$e_a_data['fmediaclassid'] = intval($mediaclassid);//媒介类型
				$e_a_data['fsampleid'] = $data2['fsampleid']; //样本id
				$e_a_data['fmediaid'] = $data2['fmediaid']; //媒介id
				$e_a_data['fmedianame'] = $mediaInfo['fmedianame'];//媒介名称
				$e_a_data['fregionid'] = $mediaInfo['fregionid']; //地区id
				$e_a_data['regionid_array'] = A('Common/Region','Model')->get_region_array($mediaInfo['fregionid']); //地区id列表
				$e_a_data['fadclasscode_v2'] =  A('Common/AdClass','Model')->get_adclass_v2_array($data2['fadclasscode_v2']);//商业广告分类
				
				
				$e_a_data['fadname'] = $data2['fadname'];//广告名称
				$e_a_data['fadowner'] = $adownerInfo['fname'];//广告主
				
				$e_a_data['fbrand'] = $data2['fbrand']; //品牌
				$e_a_data['fspokesman'] = $data2['fspokesman']; //代言人
				$e_a_data['fadclasscode'] = array(strval($data2['fadclasscode']),strval(M('tadclass')->cache(true,600)->where(array('fcode'=>$data2['fadclasscode']))->getField('fpcode'))); //广告类别id
				$e_a_data['fstarttime'] = $data2['fstarttime']; //发布开始时间戳
				$e_a_data['fendtime'] = $data2['fendtime']; //发布结束时间戳
				$e_a_data['fissuedate'] = $data2['fissuedate']; //发布日期
				$e_a_data['flength'] = $data2['fendtime'] - $data2['fstarttime']; //发布时长(秒)
				$e_a_data['fillegaltypecode'] = $data2['fillegaltypecode']; //违法类型
				
				$e_a_data['fexpressioncodes'] = $data2['fexpressioncodes']; //'违法表现代码，用“;”分隔
				$e_a_data['fillegalcontent'] = strval($data2['fillegalcontent']); //'涉嫌违法内容
				$e_a_data['sam_source_path'] = strval($data2['sam_source_path']); //'素材路径
				
				
				$e_a_data['adowner_regionid'] = $adownerInfo['fregionid']; //'广告主地区id
				$e_a_data['fmediaownername'] = $mediaInfo['fmediaownername']; //'媒介机构名称
				$e_a_data['fmediaownerid'] = $mediaInfo['fmediaownerid']; //'媒介机构id
				

				
				

				$e_a_data['identify'] = md5($forgid.'_'.$media_id.'_'.$data2['fstarttime']);
				#file_put_contents('LOG/IssueData',json_encode($e_a_data)."\n\n",FILE_APPEND);	
				$add_datas[] = $e_a_data;
				
			}

			
					
			
		}elseif($media_tab == 'paper'){
		
			
			$data2List = M($table_name)
							->alias('tissue')
							->cache(true,60)
							->field('
									tissue.fpapersampleid as fsampleid,
									tissue.fmediaid,
									0 as fstarttime ,
									0 as fendtime,
									tad.fadname,
									tad.fadowner,
									tad.fbrand,
									tsample.fspokesman,
									tad.fadclasscode,
									tad.fadclasscode_v2,
									tissue.fissuedate,
									tsample.fillegaltypecode,
									tsample.fadmanuno,
									tsample.fmanuno,
									tsample.fadapprno,
									tsample.fapprno,
									tsample.fadent,
									tsample.fent,
									tsample.fentzone,
									tsample.fexpressioncodes,
									tsample.fillegalcontent,
									tsample.fjpgfilename as sam_source_path
									
									
									')

							->join($sam_table_name.' as tsample on tsample.fpapersampleid = tissue.fpapersampleid')
							->join('tad on tad.fadid = tsample.fadid and tad.fadid > 0')
							->where(array('tissue.fmediaid'=>$media_id,'tissue.fissuedate'=>$month_date))
							->group('tissue.fmediaid,tissue.fissuedate,tsample.fjpgfilename')
							->select();	
			$add_datas = array();
			foreach($data2List as $data2){
				
				$adownerInfo = M('tadowner')->cache(true,600)->field('fname,fdomain,fregionid')->where(array('fid'=>$data2['fadowner']))->find();
				
				
				
				$e_a_data['fmediaclassid'] = intval($mediaclassid);//媒介类型
				$e_a_data['fsampleid'] = $data2['fsampleid']; //样本id
				$e_a_data['fmediaid'] = $data2['fmediaid']; //媒介id
				$e_a_data['fmedianame'] = $mediaInfo['fmedianame'];//媒介名称
				$e_a_data['fregionid'] = $mediaInfo['fregionid']; //地区id
				$e_a_data['regionid_array'] = A('Common/Region','Model')->get_region_array($mediaInfo['fregionid']); //地区id列表
				$e_a_data['fadclasscode_v2'] =  A('Common/AdClass','Model')->get_adclass_v2_array($data2['fadclasscode_v2']);//商业广告分类

				$e_a_data['fadname'] = $data2['fadname'];//广告名称
				$e_a_data['fadowner'] = $adownerInfo['fname'];//广告主
				$e_a_data['fbrand'] = $data2['fbrand']; //品牌
				$e_a_data['fspokesman'] = $data2['fspokesman']; //代言人
				$e_a_data['fadclasscode'] = array(strval($data2['fadclasscode']),strval(M('tadclass')->cache(true,600)->where(array('fcode'=>$data2['fadclasscode']))->getField('fpcode'))); //广告类别id; //广告类别id
				$e_a_data['fstarttime'] = $data2['fstarttime']; //发布开始时间戳
				$e_a_data['fendtime'] = $data2['fendtime']; //发布结束时间戳
				$e_a_data['fissuedate'] = strtotime($data2['fissuedate']); //发布日期
				$e_a_data['flength'] = $data2['fendtime'] - $data2['fstarttime']; //发布时长(秒)
				$e_a_data['fillegaltypecode'] = $data2['fillegaltypecode']; //违法类型
				//$e_a_data['fillegaltypecode'] = '0'; //违法类型
				
				
				$e_a_data['fexpressioncodes'] = $data2['fexpressioncodes']; //'违法表现代码，用“;”分隔
				//$e_a_data['fexpressioncodes'] = ''; //'违法表现代码，用“;”分隔
				
				$e_a_data['fillegalcontent'] = strval($data2['fillegalcontent']); //'涉嫌违法内容
				//$e_a_data['fillegalcontent'] = ''; //'涉嫌违法内容
				
				$e_a_data['sam_source_path'] = strval($data2['sam_source_path']); //'素材路径
				
				$e_a_data['adowner_regionid'] = $adownerInfo['fregionid']; //'广告主地区id
				$e_a_data['fmediaownername'] = $mediaInfo['fmediaownername']; //'媒介机构名称
				$e_a_data['fmediaownerid'] = $mediaInfo['fmediaownerid']; //'媒介机构id

				$e_a_data['identify'] = md5($forgid.'_'.$media_id.'_'.$month_date.'_'.$data2['sam_source_path']);
				file_put_contents('LOG/IssueDate',json_encode($e_a_data)."\n\n",FILE_APPEND);
				$add_datas[] = $e_a_data;
			}
			
		}
		#var_dump($month_date,$media_id,$add_datas,$forgid);
		$ret = A('Api/CustomerIssue','Model')->update_issue_data($month_date,$media_id,$add_datas,$forgid);
		return $ret;
	
	}
	
	#抽取户外广告
	public function make_customer_hw_ad_issue($date,$media_id,$forgid){
		if(!$forgid){
			return array('openInputNum'=>0,'sqlInputNum'=>0, 'openDelNum'=>0,'sqlDelNum'=>0 );
		}
		$mediaInfo = M('tmedia')
								->cache(true,86400)
								->field('tmediaowner.fregionid,fmedianame,fmediaclassid,tmediaowner.fid as fmediaownerid,tmediaowner.fname as fmediaownername')
								->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>$media_id))->find();//查询媒介信息
								
		$data2List = M('huwai_ad')
							->alias('tissue')
							->cache(true,60)
							->field('
									*
									
									')

						
							
							->where(array('tissue.fmediaid'=>$media_id,'tissue.fissuedate'=>$date))
							
							->select();	
		
		$add_datas = [];
		foreach($data2List as $data2){
			
			$huwai_point = M('huwai_point')->cache(true,600)->field('fname,flat,flng,faddress,link_phone')->where(array('fid'=>$data2['point_id']))->find();
			$e_a_data = array();
			$e_a_data['fmediaclassid'] = 5;//媒介类型
			$e_a_data['fsampleid'] = $data2['fid']; //样本id
			$e_a_data['fmediaid'] = $data2['fmediaid']; //媒介id
			
			$e_a_data['fmedianame'] = $mediaInfo['fmedianame'];//媒介名称
			$e_a_data['fregionid'] = $mediaInfo['fregionid']; //地区id
			$e_a_data['regionid_array'] = A('Common/Region','Model')->get_region_array($mediaInfo['fregionid']); //地区id列表
			$e_a_data['fadclasscode_v2'] =  A('Common/AdClass','Model')->get_adclass_v2_array($data2['fadclasscode_v2']);//商业广告分类
			$e_a_data['fadname'] = $data2['fadname'];//广告名称
			$e_a_data['fadowner'] = '广告主不详';//广告主
			$e_a_data['fbrand'] = $data2['fadbrand']; //品牌
			$e_a_data['fspokesman'] = ''; //代言人
			if(!$data2['fadclasscode']) $data2['fadclasscode'] = '2301';
			$e_a_data['fadclasscode'] = array(strval($data2['fadclasscode']),strval(M('tadclass')->cache(true,600)->where(array('fcode'=>$data2['fadclasscode']))->getField('fpcode')));; //广告类别id
			$e_a_data['fstarttime'] = strtotime($data2['fissuedatetime']); //发布开始时间戳
			$e_a_data['fendtime'] = strtotime($data2['fissuedatetime']); //发布结束时间戳
			$e_a_data['fissuedate'] = strtotime(date("Y-m-d",strtotime($data2['fissuedatetime']))); //发布日期
			$e_a_data['flength'] = 0; //发布时长(秒)
			
			
			$e_a_data['fillegaltypecode'] = $data2['fillegaltypecode']; //违法类型

            $e_a_data['fexpressioncodes'] = $data2['fexpressioncodes']; //'违法表现代码，用“;”分隔
            $e_a_data['fillegalcontent'] = strval($data2['fillegalcontent']); //'涉嫌违法内容

			$e_a_data['sam_source_path'] = strval($data2['fimgs']); //'素材路径
			$e_a_data['adowner_regionid'] = 100000; //'广告主地区id
			$e_a_data['fmediaownername'] = $mediaInfo['fmediaownername']; //'媒介机构名称
			$e_a_data['fmediaownerid'] = $mediaInfo['fmediaownerid']; //'媒介机构id
			$e_a_data['other_json'] = json_encode(array('huwai_point'=>$huwai_point)); // 点位信息
			
			
			$e_a_data['identify'] = md5($forgid.'_'.$media_id.'_'.$data2['fissuedate'].'_'.$data2['fimgs']);
			$add_datas[] = $e_a_data;
		}
		$ret = A('Api/CustomerIssue','Model')->update_issue_data($date,$media_id,$add_datas,$forgid);
		#var_dump($ret);
		#return $ret;
		return $ret;
			
	}

    /**
     * 根据日期媒介分批获取发布记录并写入 发布清单表，该功能用于写入互联网客户数据
     * @Param	String $month_date 日期 。如"2018-06-03"
     * @Param	Int media_id 媒体ID
     * @Return	void 返回的数据
     */
    public function make_customer_net_ad_issue($date,$media_id,$forgid){

		if(!$forgid){
			return array('openInputNum'=>0,'sqlInputNum'=>0, 'openDelNum'=>0,'sqlDelNum'=>0 );
		}
        ini_set('memory_limit','1024M');//设置内存
        $mediaInfo = M('tmedia')
            ->cache(true,86400)
            ->field('tmediaowner.fregionid,fmedianame,fmediaclassid,tmediaowner.fid as fmediaownerid,tmediaowner.fname as fmediaownername')
            ->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')
            ->where(array('tmedia.fid'=>$media_id))->find();//查询媒介信息
            //截取媒介类别
            $mediaclassid = substr($mediaInfo['fmediaclassid'],0,2);

            $data2List = M('tnetissueputlog')
                ->alias('tissue')
                ->cache(true,60)
                ->field('
						tissue.major_key as id,
						tissue.tid as fsampleid,
						tissue.fmediaid,
						tissue.net_platform as issue_platform_type,
						tissue.source_type,
						tissue.net_created_date as fstarttime ,
						tissue.net_created_date as fendtime,
						tsample.fadname,
						tsample.fadclasscode_v2,
						tsample.fadowner,
						tsample.fbrand,
						tsample.fspokesman,
						tsample.fadclasscode,
						tissue.net_created_date as fissuedate,
						tsample.fillegaltypecode,
						tsample.fillegalcontent,
						tsample.fadmanuno,
						tsample.fmanuno,
						tsample.fadapprno,
						tsample.fapprno,
						tsample.fadent,
						tsample.fent,
						tsample.fentzone,
						
						tsample.net_advertiser,

						
						
						tsample.fexpressioncodes,
						tsample.thumb_url_true as sam_source_path,
						tsample.net_old_url as landing_img,
						tsample.net_target_url as target_url,
						tsample.net_snapshot as issue_page_url,
						tsample.ftype as issue_ad_type
                ')
                ->join('tnetissue as tsample on tsample.major_key = tissue.tid')
                ->where(array('finputstate'=>2,/* 'is_first_broadcast' => 1, */'tissue.fmediaid'=>$media_id,'tissue.fissuedate'=>strtotime($date)))
                ->group('tissue.tid,tissue.net_created_date')
				->select();
            $open_datas = array();
            $data_count = count($data2List);
            foreach($data2List as $data2){
				
				$e_a_data = array();
                $adownerInfo = M('tadowner')->cache(true,600)->field('fname,fdomain,fregionid')->where(array('fid'=>$data2['fadowner']))->find();
                $e_a_data['fmediaclassid'] = intval($mediaclassid);//媒介类型
                $e_a_data['fsampleid'] = $data2['fsampleid']; //样本id
                $e_a_data['fmediaid'] = $data2['fmediaid']; //媒介id
                $e_a_data['issue_platform_type'] = $data2['issue_platform_type']; //平台类型
                $e_a_data['fmedianame'] = $mediaInfo['fmedianame'];//媒介名称
                $e_a_data['fregionid'] = $mediaInfo['fregionid']; //地区id
                $e_a_data['regionid_array'] = A('Common/Region','Model')->get_region_array($mediaInfo['fregionid']); //地区id列表
                $e_a_data['fadclasscode_v2'] =  A('Common/AdClass','Model')->get_adclass_v2_array($data2['fadclasscode_v2']);//商业广告分类
                $e_a_data['fadname'] = $data2['fadname'];//广告名称
                $e_a_data['fadowner'] = $adownerInfo['fname'];//广告主
                $e_a_data['fbrand'] = $data2['fbrand']; //品牌
                $e_a_data['fspokesman'] = $data2['fspokesman']; //代言人
                $e_a_data['fadclasscode'] = array(strval($data2['fadclasscode']),strval(M('tadclass')->cache(true,600)->where(array('fcode'=>$data2['fadclasscode']))->getField('fpcode')));; //广告类别id
                $e_a_data['fstarttime'] = round($data2['fstarttime']/1000); //发布开始时间戳
                $e_a_data['fendtime'] = round($data2['fendtime']/1000); //发布结束时间戳
                $e_a_data['fissuedate'] = strtotime(date("Y-m-d",$data2['fissuedate']/1000)); //发布日期
                $e_a_data['flength'] = $data2['fendtime'] - $data2['fstarttime']; //发布时长(秒)
                $e_a_data['fillegaltypecode'] = $data2['fillegaltypecode']; //违法类型

                $e_a_data['fexpressioncodes'] = $data2['fexpressioncodes']; //'违法表现代码，用“;”分隔
                $e_a_data['fillegalcontent'] = strval($data2['fillegalcontent']); //'涉嫌违法内容
                $e_a_data['sam_source_path'] = strval($data2['sam_source_path']); //'素材路径
                $e_a_data['landing_img'] = strval($data2['landing_img']); //'落地页截图
                $e_a_data['target_url'] = strval($data2['target_url']); //'落地页地址
                $e_a_data['issue_page_url'] = strval($data2['issue_page_url']); //'发布地址截图
                $e_a_data['issue_ad_type'] = strval($data2['issue_ad_type']); //'发布类型

                $e_a_data['adowner_regionid'] = $adownerInfo['fregionid']; //'广告主地区id
                $e_a_data['fmediaownername'] = $mediaInfo['fmediaownername']; //'媒介机构名称
                $e_a_data['fmediaownerid'] = $mediaInfo['fmediaownerid']; //'媒介机构id
                $e_a_data['identify'] = md5($forgid.'_'.$media_id.'_'.$data2['fissuedate'].'_'.$data2['id']);
				$e_a_data['other_json'] = json_encode(array('net_advertiser'=>$data2['net_advertiser']));
				$trackers = M('trackers_record')->where(array('tid'=>$data2['fsampleid']))->getField('trackerid',true); #查询联盟id列表
				$e_a_data['trackerids'] = $trackers;
				$e_a_data['fexpression_codes'] = explode(';',$data2['fexpressioncodes']);
				
				
				
                $add_datas[] = $e_a_data;

            }
        $ret = A('Api/CustomerIssue','Model')->update_issue_data($date,$media_id,$add_datas,$forgid);
		return $ret;
    }
}