<?php
namespace Agp\Controller;
use Think\Controller;

import('Vendor.PHPExcel');

/**
 * 监测数据
 */

class GmonitoringController extends BaseController
{
	/**
	 * 获取监测数据
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function index()
	{
		session_write_close();
		$system_num = getconfig('system_num');

		$p = I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录

		$pxtype 	= I('pxtype')?I('pxtype'):0;//排序方式，0默认
		$pxval 		= I('pxval')?I('pxval'):0;//排序值，0默认
		$fadname 			= I('fadname');// 广告名称
		$fadclasscode 		= I('fadclasscode');// 广告内容类别组
		$tregion 			= I('tregion');// 行政区划组
		$mclass 			= I('mclass')?I('mclass'):'tv';//媒体类型
		$fillegaltypecode 	= I('fillegaltypecode');// 违法类型组
		$fissuedatest 		= I('fissuedatest');//发布时间起
		$fissuedateed 		= I('fissuedateed');//发布时间止

		$fissuedatest = strtotime($fissuedatest);
		$fissuedateed = strtotime($fissuedateed);

		$tb_tvissue = gettable('tv',$fissuedatest);
		$tb_bcissue = gettable('bc',$fissuedatest);
		$orders = '';
		if($pxtype==1){
			if($pxval==0){
				$orders = $orders?$orders.',fmedianame desc':' fmedianame desc';
			}else{
				$orders = $orders?$orders.',fmedianame asc':' fmedianame asc';
			}
		}elseif($pxtype==2){
			if($pxval==0){
				$orders = $orders?$orders.',fissuedate desc':' fissuedate desc';
			}else{
				$orders = $orders?$orders.',fissuedate asc':' fissuedate asc';
			}
		}else{
			if($pxval==0){
				$orders = $orders?$orders.',sid desc':' sid desc';
			}
		}

		if (!empty($fadname)) {
			$where['fadname'] = array('like', '%' . $fadname . '%');//广告名称
		}
		$arr_code = [];
		if(is_array($fadclasscode)){
			foreach ($fadclasscode as $key => $value) {
				$codes = D('Function')->get_next_tadclasscode($value,true);//获取下级广告类别代码
				if(!empty($codes)){
					$arr_code = array_merge($arr_code,$codes);
				}else{
					array_push($arr_code,$value);
				}
			}
			$where['fadclasscode'] = array('in', $arr_code);
		}

		$where['_string'] = '1=1';


		if(is_array($tregion)){//行政区划
			$regionids = [];
			foreach ($regionid as $value) {
				array_push($regionids, $value);
				$regionids = array_merge($regionids,D('Function')->get_nextquhua($value));
			}
			if(!empty($regionids)){
				$where['tmediaowner.fregionid'] = array('in',$regionids);
			}
		}
		if(is_array($fillegaltypecode)){//违法类型组
			$wherestrtv 	.= ' and ttvsample.fillegaltypecode in ('.implode(',',$fillegaltypecode).')';
			$wherestrpaper 	.= ' and tpapersample.fillegaltypecode in ('.implode(',',$fillegaltypecode).')';
			$wherestrbc 	.= ' and tbcsample.fillegaltypecode in ('.implode(',',$fillegaltypecode).')';
			if($mclass=='net'){
				$where['_string'] 	.= ' and tnetissue.fillegaltypecode in ('.implode(',',$fillegaltypecode).')';
			}
		}
		//发布时间条件区间
		$wherestrtv 	.= ' and ttvsample.fstate=1 and '.$tb_tvissue.'.fissuedate>='.$fissuedatest.'';
		$wherestrpaper 	.= ' and tpapersample.fstate=1 and tpaperissue.fissuedate>="'.date('Y-m-d',$fissuedatest).'"';
		$wherestrbc 	.= ' and tbcsample.fstate=1 and '.$tb_bcissue.'.fissuedate>='.$fissuedatest.'';
		if($mclass=='net'){
			$where['_string'] .= ' and tnetissue.net_created_date>='.$fissuedatest*1000;
		}
		
		$wherestrtv 	.= ' and '.$tb_tvissue.'.fissuedate<='.$fissuedateed.'';
		$wherestrpaper 	.= ' and tpaperissue.fissuedate<="'.date('Y-m-d',$fissuedateed).'"';
		$wherestrbc 	.= ' and '.$tb_bcissue.'.fissuedate<='.$fissuedateed.'';
		if($mclass=='net'){
			$where['_string'] .= '  and finputstate=2 and tnetissue.net_created_date<='.$fissuedateed*1000;
		}
		
		if($system_num == '100000'){
			if(session('regulatorpersonInfo.fregulatorlevel') == 20){
				$where['tmediaowner.fregionid'] = array('like',substr(session('regulatorpersonInfo.regionid'),0,2).'%');
			}elseif(session('regulatorpersonInfo.fregulatorlevel') == 10){
				$where['tmediaowner.fregionid'] = array('like',substr(session('regulatorpersonInfo.regionid'),0,4).'%');
			}elseif(session('regulatorpersonInfo.fregulatorlevel') == 0){
				$where['tmediaowner.fregionid'] = array('like',substr(session('regulatorpersonInfo.regionid'),0,6).'%');
			}
		}
		
		if($mclass=='paper'){
			$count = M('tpaperissue')
				->join('tpapersample on tpaperissue.fpapersampleid=tpapersample.fpapersampleid '.$wherestrpaper)
				->join('tad on tpapersample.fadid=tad.fadid')
				->join('tmedia on tpaperissue.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->join('tmediaowner on tmedia.fmediaownerid=tmediaowner.fid')
				->where($where)
				->count();

			
			$data = M('tpaperissue')
				->field('
					"paper" as mclass,
					tad.fadname,
					tadclass.fadclass,
					 (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,
					tmedia.fmediaownerid,
					tillegaltype.fillegaltype,
					tpaperissue.fissuedate,
					tpaperissue.fpaperissueid as sid
				')
				->join('tpapersample on tpaperissue.fpapersampleid=tpapersample.fpapersampleid '.$wherestrpaper)
				->join('tad on tpapersample.fadid=tad.fadid')
				->join('tadclass on tad.fadclasscode=tadclass.fcode')
				->join('tillegaltype on tpapersample.fillegaltypecode=tillegaltype.fcode')
				->join('tmedia on tpaperissue.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->join('tmediaowner on tmedia.fmediaownerid=tmediaowner.fid')
				->where($where)
				->order($orders)
				->page($p,$pp)
				->select();
		}elseif($mclass=='bc'){
			$tb_name = $tb_bcissue;
			$count = M(''.$tb_bcissue.'')
				->join('tbcsample on '.$tb_bcissue.'.fsampleid=tbcsample.fid '.$wherestrbc)
				->join('tad on tbcsample.fadid=tad.fadid')
				->join('tmedia on '.$tb_bcissue.'.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->join('tmediaowner on tmedia.fmediaownerid=tmediaowner.fid')
				->where($where)
				->count();

			
			$data = M(''.$tb_bcissue.'')
				->field('
					"bc" as mclass,
					tad.fadname,
					tadclass.fadclass,
					 (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,
					tillegaltype.fillegaltype,
					FROM_UNIXTIME('.$tb_bcissue.'.fstarttime) as fissuedate,
					'.$tb_bcissue.'.fid as sid
				')
				->join('tbcsample on '.$tb_bcissue.'.fsampleid=tbcsample.fid '.$wherestrbc)
				->join('tad on tbcsample.fadid=tad.fadid')
				->join('tadclass on tad.fadclasscode=tadclass.fcode')
				->join('tillegaltype on tbcsample.fillegaltypecode=tillegaltype.fcode')
				->join('tmedia on '.$tb_bcissue.'.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->join('tmediaowner on tmedia.fmediaownerid=tmediaowner.fid')
				->where($where)
				->order($orders)
				->page($p,$pp)
				->select();
		}elseif($mclass=='tv'){
			$tb_name = $tb_tvissue;
			$count = M($tb_tvissue)
				->join('ttvsample on '.$tb_tvissue.'.fsampleid=ttvsample.fid '.$wherestrtv)
				->join('tad on ttvsample.fadid=tad.fadid')
				->join('tmedia on '.$tb_tvissue.'.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->join('tmediaowner on tmedia.fmediaownerid=tmediaowner.fid')
				->where($where)
				->count();

			
			$data = M(''.$tb_tvissue.'')
				->field('
					"tv" as mclass,
					tad.fadname,
					tadclass.fadclass,
					 (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,
					tillegaltype.fillegaltype,
					FROM_UNIXTIME('.$tb_tvissue.'.fstarttime) as fissuedate,
					'.$tb_tvissue.'.fid as sid
				')
				->join('ttvsample on '.$tb_tvissue.'.fsampleid=ttvsample.fid '.$wherestrtv)
				->join('tad on ttvsample.fadid=tad.fadid')
				->join('tadclass on tad.fadclasscode=tadclass.fcode')
				->join('tillegaltype on ttvsample.fillegaltypecode=tillegaltype.fcode')
				->join('tmedia on '.$tb_tvissue.'.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->join('tmediaowner on tmedia.fmediaownerid=tmediaowner.fid')
				->where($where)
				->order($orders)
				->page($p,$pp)
				->select();
				
		}elseif($mclass=='net'){
			$count = M('tnetissue')
				->join('tmedia on tnetissue.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->count();

			
			$data = M('tnetissue')
				->field('
					"net" as mclass,
					tnetissue.fadname,
					tadclass.fadclass,
					 (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,
					tillegaltype.fillegaltype,
					from_unixtime(tnetissue.net_created_date/1000,"%Y-%m-%d") as fissuedate,
					tnetissue.major_key as sid
				')
				->join('tadclass on tnetissue.fadclasscode=tadclass.fcode')
				->join('tillegaltype on tnetissue.fillegaltypecode=tillegaltype.fcode')
				->join('tmedia on tnetissue.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->join('tmediaowner on tmedia.fmediaownerid=tmediaowner.fid')
				->where($where)
				->order($orders)
				->page($p,$pp)
				->select();
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data,'tbname'=>$tb_name)));
	}

	/**
	*导出数据	
	*/
	public function downloadData()
	{
		$system_num = getconfig('system_num');
		$pxtype 	= I('pxtype')?I('pxtype'):0;//排序方式，0默认
		$pxval 		= I('pxval')?I('pxval'):0;//排序值，0默认
		$fadname 			= I('fadname');// 广告名称
		$fadclasscode 		= I('fadclasscode');// 广告内容类别组
		$tregion 			= I('tregion');// 行政区划组
		$mclass 			= I('mclass')?I('mclass'):'tv';//媒体类型
		$fillegaltypecode 	= I('fillegaltypecode');// 违法类型组
		$fissuedatest 		= I('fissuedatest');//发布时间起
		$fissuedateed 		= I('fissuedateed');//发布时间止

		$fissuedatest = strtotime($fissuedatest);
		$fissuedateed = strtotime($fissuedateed);

		$tb_tvissue = gettable('tv',$fissuedatest);
		// dump($tb_tvissue);die;
		$tb_bcissue = gettable('bc',$fissuedatest);
		$orders = '';
		if($pxtype==1){
			if($pxval==0){
				$orders = $orders?$orders.',fmedianame desc':' fmedianame desc';
			}else{
				$orders = $orders?$orders.',fmedianame asc':' fmedianame asc';
			}
		}elseif($pxtype==2){
			if($pxval==0){
				$orders = $orders?$orders.',fissuedate desc':' fissuedate desc';
			}else{
				$orders = $orders?$orders.',fissuedate asc':' fissuedate asc';
			}
		}else{
			if($pxval==0){
				$orders = $orders?$orders.',sid desc':' sid desc';
			}
		}

		if (!empty($fadname)) {
			$where['fadname'] = array('like', '%' . $fadname . '%');//广告名称
		}
		$arr_code = [];
		if(is_array($fadclasscode)){
			foreach ($fadclasscode as $key => $value) {
				$codes = D('Function')->get_next_tadclasscode($value,true);//获取下级广告类别代码
				if(!empty($codes)){
					$arr_code = array_merge($arr_code,$codes);
				}else{
					array_push($arr_code,$value);
				}
			}
			$where['fadclasscode'] = array('in', $arr_code);
		}

		$where['_string'] = '1=1';

		if(is_array($tregion)){//行政区划
			$regionids = [];
			foreach ($regionid as $value) {
				array_push($regionids, $value);
				$regionids = array_merge($regionids,D('Function')->get_nextquhua($value));
			}
			if(!empty($regionids)){
				$where['tmediaowner.fregionid'] = array('in',$regionids);
			}
		}
		if(is_array($fillegaltypecode)){//违法类型组
			$wherestrtv 	.= ' and ttvsample.fillegaltypecode in ('.implode(',',$fillegaltypecode).')';
			$wherestrpaper 	.= ' and tpapersample.fillegaltypecode in ('.implode(',',$fillegaltypecode).')';
			$wherestrbc 	.= ' and tbcsample.fillegaltypecode in ('.implode(',',$fillegaltypecode).')';
			if($mclass=='net'){
				$where['_string'] 	.= ' and tnetissue.fillegaltypecode in ('.implode(',',$fillegaltypecode).')';
			}
		}
		//发布时间条件区间
		$wherestrtv 	.= ' and ttvsample.fstate=1 and '.$tb_tvissue.'.fissuedate>='.$fissuedatest.'';
		$wherestrpaper 	.= ' and tpapersample.fstate=1 and tpaperissue.fissuedate>="'.date('Y-m-d',$fissuedatest).'"';
		$wherestrbc 	.= ' and tbcsample.fstate=1 and '.$tb_bcissue.'.fissuedate>='.$fissuedatest.'';
		if($mclass=='net'){
			$where['_string'] .= ' and tnetissue.net_created_date>='.$fissuedatest*1000;
		}
		
		$wherestrtv 	.= ' and '.$tb_tvissue.'.fissuedate<='.$fissuedateed.'';
		$wherestrpaper 	.= ' and tpaperissue.fissuedate<="'.date('Y-m-d',$fissuedateed).'"';
		$wherestrbc 	.= ' and '.$tb_bcissue.'.fissuedate<='.$fissuedateed.'';
		if($mclass=='net'){
			$where['_string'] .= '  and finputstate=2  and tnetissue.net_created_date<='.$fissuedateed*1000;
		}
		
		if($mclass=='paper'){
			
			$data = M('tpaperissue')
				->field('
					"paper" as mclass,
					tad.fadname,
					tadclass.fadclass,
					 (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,
					tmedia.fmediaownerid,
					tillegaltype.fillegaltype,
					tpaperissue.fissuedate,
					tpaperissue.fpaperissueid as sid
				')
				->join('tpapersample on tpaperissue.fpapersampleid=tpapersample.fpapersampleid '.$wherestrpaper)
				->join('tad on tpapersample.fadid=tad.fadid')
				->join('tadclass on tad.fadclasscode=tadclass.fcode')
				->join('tillegaltype on tpapersample.fillegaltypecode=tillegaltype.fcode')
				->join('tmedia on tpaperissue.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->join('tmediaowner on tmedia.fmediaownerid=tmediaowner.fid')
				->where($where)
				->order($orders)
				->select();
		}elseif($mclass=='bc'){
			$tb_name = $tb_bcissue;
			
			$data = M(''.$tb_bcissue.'')
				->field('
					"bc" as mclass,
					tad.fadname,
					tadclass.fadclass,
					 (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,
					tillegaltype.fillegaltype,
					FROM_UNIXTIME('.$tb_bcissue.'.fstarttime) as fissuedate,
					'.$tb_bcissue.'.fid as sid
				')
				->join('tbcsample on '.$tb_bcissue.'.fsampleid=tbcsample.fid '.$wherestrbc)
				->join('tad on tbcsample.fadid=tad.fadid')
				->join('tadclass on tad.fadclasscode=tadclass.fcode')
				->join('tillegaltype on tbcsample.fillegaltypecode=tillegaltype.fcode')
				->join('tmedia on '.$tb_bcissue.'.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->join('tmediaowner on tmedia.fmediaownerid=tmediaowner.fid')
				->where($where)
				->order($orders)
				->select();
		}elseif($mclass=='tv'){
			$tb_name = $tb_tvissue;
			
			$data = M(''.$tb_tvissue.'')
				->field('
					"tv" as mclass,
					tad.fadname,
					tadclass.fadclass,
					 (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,
					tillegaltype.fillegaltype,
					FROM_UNIXTIME('.$tb_tvissue.'.fstarttime) as fissuedate,
					'.$tb_tvissue.'.fid as sid
				')
				->join('ttvsample on '.$tb_tvissue.'.fsampleid=ttvsample.fid '.$wherestrtv)
				->join('tad on ttvsample.fadid=tad.fadid')
				->join('tadclass on tad.fadclasscode=tadclass.fcode')
				->join('tillegaltype on ttvsample.fillegaltypecode=tillegaltype.fcode')
				->join('tmedia on '.$tb_tvissue.'.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->join('tmediaowner on tmedia.fmediaownerid=tmediaowner.fid')
				->where($where)
				->order($orders)
				->select();
				
		}elseif($mclass=='net'){
			
			$data = M('tnetissue')
				->field('
					"net" as mclass,
					tnetissue.fadname,
					tadclass.fadclass,
					 (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,
					tillegaltype.fillegaltype,
					from_unixtime(tnetissue.net_created_date/1000,"%Y-%m-%d") as fissuedate,
					tnetissue.major_key as sid
				')
				->join('tadclass on tnetissue.fadclasscode=tadclass.fcode')
				->join('tillegaltype on tnetissue.fillegaltypecode=tillegaltype.fcode')
				->join('tmedia on tnetissue.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->join('tmediaowner on tmedia.fmediaownerid=tmediaowner.fid')
				->where($where)
				->order($orders)
				->select();
		}

		if(count($data)>15000){
			D('Function')->write_log('监测数据',0,'数据量过大，无法导出，请缩小查询范围，请确保数据在15000条以内！','',0);
			$this->ajaxReturn(array('code'=>-1,'msg'=>'数据量过大，无法导出，请缩小查询范围，请确保数据在15000条以内！'));
		}

		$downtype = I('downtype','Excel');//导出文件类型
		if($downtype == 'Excel'){
			if(!$data || empty($data)){

				$this->ajaxReturn(array('code'=>-1,'msg'=>'暂无数据'));
			}
			$this->downloadExcel($data);
		}
	}

	//导出excel表格
	public function downloadExcel($data)
	{
		header("Content-type: text/html; charset=utf-8"); 
	    $objPHPExcel = new \PHPExcel();  
	    $objPHPExcel
	      ->getProperties()  //获得文件属性对象，给下文提供设置资源
	      ->setCreator( "MaartenBalliauw")             //设置文件的创建者
	      ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
	      ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
	      ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
	      ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
	      ->setKeywords( "office 2007 openxmlphp")        //设置标记
	      ->setCategory( "Test resultfile");                //设置类别

	    $sheet = $objPHPExcel->setActiveSheetIndex(0);
		$sheet ->setCellValue('A1','序号');
		$sheet ->setCellValue('B1','广告名称');
		$sheet ->setCellValue('C1','广告内容类别');
		$sheet ->setCellValue('D1','发布媒体');
		$sheet ->setCellValue('E1','媒体类型');
		$sheet ->setCellValue('F1','违法类型');
		$sheet ->setCellValue('G1','发布日期');
		$arr = ['bc'=>'广播','tv'=>'电视','net'=>'互联网','paper'=>'报刊'];
		foreach ($data as $key => $value) {	
			$sheet ->setCellValue('A'.($key+2),$key+1);
			$sheet ->setCellValue('B'.($key+2),$value['fadname']);
			$sheet ->setCellValue('C'.($key+2),$value['fadclass']);
			$sheet ->setCellValue('D'.($key+2),$value['fmedianame']);
			$sheet ->setCellValue('E'.($key+2),$arr[$value['mclass']]);//媒体类型
			$sheet ->setCellValue('F'.($key+2),$value['fillegaltype']);
			$sheet ->setCellValue('G'.($key+2),$value['fissuedate']);

		}

		$objActSheet =$objPHPExcel->getActiveSheet();
		//给当前活动的表设置名称
		$objActSheet->setTitle('Sheet1');

		$objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$date = date('Ymdhis');
		$savefile = './Public/Xls/'.$date.'.xlsx';
		$savefile2 = '/Public/Xls/'.$date.'.xlsx';
		$objWriter->save($savefile);
		D('Function')->write_log('监测数据',1,'导出成功');
		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$savefile2));
	}


	/**
	 * 获取监测数据详情
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据
	 * by zw
	 */
	public function getmonitoringview()
	{
		$system_num = getconfig('system_num');

		$sid 		= I('sid');//广告记录id
		$mclass 	= I('mclass');//媒体类型
		$tb_name 	= I('tbname');//发布表名
		if(empty($sid)||empty($mclass)){
			$this->error('参数缺失!');
		}

		$wherestrtv 	= ' and ttvsample.fstate=1 and '.$tb_name.'.fid='.$sid;//tv
		$wherestrpaper 	= ' and tpapersample.fstate=1 and tpaperissue.fpaperissueid='.$sid;//paper
		$wherestrbc 	= ' and tbcsample.fstate=1 and '.$tb_name.'.fid='.$sid;//bc
		$wherenet['tnetissue.major_key'] = $sid;//net
		$wherenet['_string'] = ' finputstate=2  ';

		if($mclass== 'tv'){
			$data = M(''.$tb_name.'')
				->field('
					tad.fadname, tadclass.fadclass, (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.fid ,tad.fbrand, tadowner.fname, fversion, "tv" as mclass, fadlen, tillegaltype.fillegaltype, fillegalcontent, fexpressioncodes, fexpressions, fconfirmations, fapprovalid, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, FROM_UNIXTIME('.$tb_name.'.fstarttime) as fissuedate, fstarttime, fendtime, "" as fpage, "" as fsize, favifilepng, favifilename, ttvsample.fissuedate as fsamplecreatetime
				')
				->join('ttvsample on '.$tb_name.'.fsampleid=ttvsample.fid '.$wherestrtv)
				->join('tad on ttvsample.fadid=tad.fadid')
				->join('tadclass on tad.fadclasscode=tadclass.fcode')
				->join('tadowner on tad.fadowner=tadowner.fid ')
				->join('tillegaltype on ttvsample.fillegaltypecode=tillegaltype.fcode')
				->join('tmedia on '.$tb_name.'.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->find();
				$data['favifilename'] = A('Common/Media','Model')->get_m3u8($data['fid'],$data['fstarttime'],$data['fendtime']);
		}elseif($mclass== 'paper'){
			$data = M('tpaperissue')
				->field('
					tad.fadname, tadclass.fadclass, (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tad.fbrand, tadowner.fname, fversion, "paper" as mclass , tillegaltype.fillegaltype, fillegalcontent, fexpressioncodes, fexpressions, fconfirmations, fapprovalid, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, tpaperissue.fissuedate, "" as fstarttime, "" as fendtime, fpage, fsize, tpapersample.fjpgfilename as favifilepng,tpapersample.fjpgfilename as favifilename, tpapersample.fissuedate as fsamplecreatetime 
				')
				->join('tpapersample on tpaperissue.fpapersampleid=tpapersample.fpapersampleid '.$wherestrpaper)
				->join('tad on tpapersample.fadid=tad.fadid')
				->join('tadclass on tad.fadclasscode=tadclass.fcode')
				->join('tadowner on tad.fadowner=tadowner.fid ')
				->join('tillegaltype on tpapersample.fillegaltypecode=tillegaltype.fcode')
				->join('tmedia on tpaperissue.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->find();
		}elseif($mclass== 'bc'){
			$data = M(''.$tb_name.'')
				->field('
					tad.fadname, tadclass.fadclass, (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame ,tad.fbrand, tadowner.fname, fversion, "bc" as mclass, fadlen, tillegaltype.fillegaltype, fillegalcontent, fexpressioncodes, fexpressions, fconfirmations, fapprovalid, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, FROM_UNIXTIME('.$tb_name.'.fstarttime) as fissuedate, fstarttime, fendtime, "" as fpage, "" as fsize, favifilepng, favifilename, tbcsample.fissuedate as fsamplecreatetime
				')
				->join('tbcsample on '.$tb_name.'.fsampleid=tbcsample.fid '.$wherestrbc)
				->join('tad on tbcsample.fadid=tad.fadid')
				->join('tadclass on tad.fadclasscode=tadclass.fcode')
				->join('tadowner on tad.fadowner=tadowner.fid ')
				->join('tillegaltype on tbcsample.fillegaltypecode=tillegaltype.fcode')
				->join('tmedia on '.$tb_name.'.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->find();
		}elseif($mclass== 'net'){
			$data = M('tnetissue')
				->field('
					tnetissue.*, (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,fbrand, tadclass.fadclass,tadowner.fname as fadowner_name, "net" as mclass, tillegaltype.fillegaltype, fillegalcontent,net_created_date as fsamplecreatetime
				')
				->join('tadclass on tnetissue.fadclasscode=tadclass.fcode')
				->join('tadowner on tnetissue.fadowner=tadowner.fid ')
				->join('tillegaltype on tnetissue.fillegaltypecode=tillegaltype.fcode')
				->join('tmedia on tnetissue.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->join('tmediaowner on tmedia.fmediaownerid=tmediaowner.fid')
				->where($wherenet)
				->find();
		}
		
		if(!empty($data)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据','data'=>$data));
		}
	}

}