Vue.prototype.$ELEMENT = { size: 'mini', zIndex: 3000 }

let vm = new Vue({
    el: '#app',
    components: {
        sqlItem
    },
    data: function () {
        return {
            table: [],
            total: 0,
            where: {},
            loading: false,
            page: 1,
            showSqlInfo: false,
            currentID: null,
        }
    },
    created: function () {
        this.getTable()
    },
    methods:{
        getTable: function () {
            this.loading = true
            $.post('', {
                where: this.where,
                page: this.page,
            }).then((res) => {
                this.loading = false
                this.table = res.table
                this.total = +res.total
            })
        },
        resetWhere: function () {
            this.where = {}
            this.page = 1
            this.getTable()
        },
        add: function () {
            this.showSqlInfo = true
            this.currentID = null
            this.$nextTick(() => {
                this.$refs.sqlInfo.dataInit()
            })
        },
        pageChange: function (page) {
            this.page = page
            this.getTable()
        },
        closeInfo: function () {
            this.showSqlInfo = false
            this.currentID = null
            this.getTable()
        },
        checkInfo: function (id) {
            this.showSqlInfo = true
            this.currentID = id
            this.$nextTick(() => {
                this.$refs.sqlInfo.dataInit()
            })
        }
    }
})