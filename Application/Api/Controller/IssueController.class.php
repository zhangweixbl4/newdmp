<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;


class IssueController extends Controller {
	
	/*生成广告片段*/
	public function create_tv_ad_part(){
		$ftvissueid = I('ftvissueid');
		$back_url = 'http://'.$_SERVER['HTTP_HOST'] . U('Api/Issue/edit_tv_ad_part') ;
		
		$cut_url = C('TV_ISSUE_CUT_URL');//剪辑电视广告片段的url
		
		M('ttvissue')->where(array('ftvissueid'=>$ftvissueid))->save(array('mp4url_state'=>1));
		
		$issueInfo = M('ttvissue')
							->field('fmediaid,fstarttime,fendtime,ftvsampleid')
							->where(array('ftvissueid'=>$ftvissueid))->find();
		$start_time = strtotime($issueInfo['fstarttime']);
		$end_time = strtotime($issueInfo['fendtime']);
		$adInfo = M('ttvsample')
								->field('tad.fadname')
								->join('tad on tad.fadid = ttvsample.fadid')
								->where(array('ttvsample.fid'=>$issueInfo['ftvsampleid']))
								->find();
							
		
		if(C('SELF_M3U8')){
			$m3u8_url = 'http://'.$_SERVER['HTTP_HOST'].U('Api/Media/get_media_m3u8',array('media_id'=>$issueInfo['fmediaid'],'start_time'=>$start_time,'end_time'=>$end_time));
		}else{
			$m3u8_url = A('Common/Shilian','Model')->get_playback_url($issueInfo['fmediaid'],$start_time,$end_time);
		}	
		$post_data = array(
							'tvissue_id'=>$ftvissueid,
							'start_time'=>$start_time,
							'end_time'=>$end_time,
							'm3u8_url'=>urlencode($m3u8_url),
							'back_url'=>urlencode($back_url),
							'file_name'=>urlencode(date('YmdHis',$start_time).'_'.$adInfo['fadname'].'_'.$ftvissueid),
							
							
								);
		$ret = http($cut_url,$post_data,'GET',false,30);				
								
		$this->ajaxReturn(array('code'=>0,'msg'=>'正在生成'));						
		
		
	}
	
	
	/*修改发布记录视频片段地址*/
	public function edit_tv_ad_part(){

		$post_data = file_get_contents('php://input');
		
		$post_data = json_decode($post_data,true);
		
		
		M('ttvissue')->where(array('ftvissueid'=>$post_data['tvissue_id']))->save(array('fmp4url'=>$post_data['cut_mp4_url'],'validity_time'=>time()+(86400*30),'mp4url_state'=>2));
		
		
	}
	
	/*获取广告片段生成状态*/
	public function get_tv_ad_part(){
		session_write_close();
		$ftvissueid = I('ftvissueid');
		$tvissueDetails = M('ttvissue')
										->field('fmp4url,mp4url_state')
										->where(array('ttvissue.ftvissueid'=>$ftvissueid))
										->find();//查询样本详情
		//var_dump(M('ttvissue')->getLastSql());
		$fmp4url = strval($tvissueDetails['fmp4url']);
		$mp4url_state = strval($tvissueDetails['mp4url_state']);
		
		$ad_part = array(
							'fmp4url'=>$fmp4url,
							'mp4url_state'=>$mp4url_state,
							
							);

		
		
		$this->ajaxReturn(array('code'=>0,'ad_part'=>$ad_part));
	}
	
	
	/************************************************************************以上是视频、以下是音频***********************************************************************/
	
		/*生成广告片段*/
	public function create_bc_ad_part(){
		$fbcissueid = I('fbcissueid');
		$back_url = 'http://'.$_SERVER['HTTP_HOST'] . U('Api/Issue/edit_bc_ad_part') ;
		
		$cut_url = C('BC_ISSUE_CUT_URL');//剪辑电视广告片段的url
		
		M('tbcissue')->where(array('fbcissueid'=>$fbcissueid))->save(array('mp4url_state'=>1));
		
		$issueInfo = M('tbcissue')
							->field('fmediaid,fstarttime,fendtime,fbcsampleid')
							->where(array('fbcissueid'=>$fbcissueid))->find();
		$start_time = strtotime($issueInfo['fstarttime']);
		$end_time = strtotime($issueInfo['fendtime']);
		$adInfo = M('tbcsample')
								->field('tad.fadname')
								->join('tad on tad.fadid = tbcsample.fadid')
								->where(array('tbcsample.fid'=>$issueInfo['fbcsampleid']))
								->find();
								
		$mediaInfo = M('tmedia')->field('fid,fcollecttype,fsavepath')->where(array('fid'=>$issueInfo['fmediaid']))->find();
		
		if($mediaInfo['fcollecttype'] == 10){
			$m3u8_url = A('Common/Shilian','Model')->get_playback_url($issueInfo['fmediaid'],$start_time,$end_time);
		}
		
		if($mediaInfo['fcollecttype'] == 25 || $mediaInfo['fcollecttype'] == 26){
			$m3u8_url = $mediaInfo['fsavepath'].'&start='.$start_time.'&end='.$end_time;
		}
							
		
		
		
		
		$post_data = array(
							'bcissue_id'=>$fbcissueid,
							'start_time'=>$start_time,
							'end_time'=>$end_time,
							'm3u8_url'=>urlencode($m3u8_url),
							'back_url'=>urlencode($back_url),
							'file_name'=>urlencode(date('YmdHis',$start_time).'_'.$adInfo['fadname'].'_'.$fbcissueid),
							
								);
		$ret = http($cut_url,$post_data,'GET',false,10);				
								
		$this->ajaxReturn(array('code'=>0,'msg'=>'正在生成'));						
		
		
	}
	
	
	/*修改发布记录视频片段地址*/
	public function edit_bc_ad_part(){

		$post_data = file_get_contents('php://input');
		
		$post_data = json_decode($post_data,true);
		
		
		M('tbcissue')->where(array('fbcissueid'=>$post_data['bcissue_id']))->save(array('fmp4url'=>$post_data['cut_mp4_url'],'validity_time'=>time()+(86400*30),'mp4url_state'=>2));
		
		
	}
	
	/*获取广告片段生成状态*/
	public function get_bc_ad_part(){
		session_write_close();
		$fbcissueid = I('fbcissueid');
		$bcissueDetails = M('tbcissue')
										->field('fmp4url,mp4url_state')
										->where(array('tbcissue.fbcissueid'=>$fbcissueid))
										->find();//查询样本详情
		//var_dump(M('tbcissue')->getLastSql());
		$fmp4url = strval($bcissueDetails['fmp4url']);
		$mp4url_state = strval($bcissueDetails['mp4url_state']);
		
		$ad_part = array(
							'fmp4url'=>$fmp4url,
							'mp4url_state'=>$mp4url_state,
							
							);

		
		
		$this->ajaxReturn(array('code'=>0,'ad_part'=>$ad_part));
	}
	
	
	/*电视发布记录视频地址*/
	public function tv_v(){
		header("Content-type: text/html; charset=utf-8"); 
		$ftvissueid = I('ftvissueid');
		
		
		echo '<meta http-equiv="refresh" content="2">';
		$issueInfo = M('ttvissue')
							->field('fmediaid,fstarttime,fendtime,ftvsampleid,mp4url_state,validity_time,fmp4url')
							->where(array('ftvissueid'=>$ftvissueid))->find();//查询样本详情
		if(!$issueInfo){
			exit('不存在该记录');
		}

		if(intval($issueInfo['mp4url_state']) == 1 ){
			
			exit('正在生成......');
		}
			
		if(intval($issueInfo['mp4url_state']) == 2 && intval($issueInfo['validity_time']) > time()){//判断是否已生成且没有过期
			header('Location:'.$issueInfo['fmp4url']);
			exit;
		}

		/*下面开始进入剪辑生成环节*/

		$back_url = 'http://'.$_SERVER['HTTP_HOST'] . U('Api/Issue/edit_tv_ad_part') ;//回调地址
		
		$cut_url = C('TV_ISSUE_CUT_URL');//剪辑电视广告片段的url
		M('ttvissue')->where(array('ftvissueid'=>$ftvissueid))->save(array('mp4url_state'=>1));//修改状态为正在生成		
		$start_time = strtotime($issueInfo['fstarttime']);//发布开始时间
		$end_time = strtotime($issueInfo['fendtime']);//发布结束时间
		$adInfo = M('ttvsample')
								->field('tad.fadname')
								->join('tad on tad.fadid = ttvsample.fadid')
								->where(array('ttvsample.fid'=>$issueInfo['ftvsampleid']))
								->find();//查询样本详情、广告名称
							
		
		if(C('SELF_M3U8')){
			$m3u8_url = 'http://'.$_SERVER['HTTP_HOST'].U('Api/Media/get_media_m3u8',array('media_id'=>$issueInfo['fmediaid'],'start_time'=>$start_time,'end_time'=>$end_time));
		}else{
			$m3u8_url = A('Common/Shilian','Model')->get_playback_url($issueInfo['fmediaid'],$start_time,$end_time);
		}
		$file_name = I('n');
		if($file_name == ''){
			$file_name = urlencode(date('YmdHis',$start_time).'_'.$adInfo['fadname'].'_'.$fbcissueid);
		}		
		$post_data = array(
							'tvissue_id'=>$ftvissueid,
							'start_time'=>$start_time,
							'end_time'=>$end_time,
							'm3u8_url'=>urlencode($m3u8_url),
							'back_url'=>urlencode($back_url),
							'file_name'=>$file_name,

								);
		$ret = http($cut_url,$post_data,'GET',false,1);
		
		exit('正在生成......');
													
	}
	
	
	/*电视发布记录视频地址*/
	public function bc_a(){
		header("Content-type: text/html; charset=utf-8"); 
		$fbcissueid = I('fbcissueid');
		
		
		echo '<meta http-equiv="refresh" content="2">';
		$issueInfo = M('tbcissue')
							->field('fmediaid,fstarttime,fendtime,fbcsampleid,mp4url_state,validity_time,fmp4url')
							->where(array('fbcissueid'=>$fbcissueid))->find();//查询样本详情
		if(!$issueInfo){
			exit('不存在该记录');
		}

		if(intval($issueInfo['mp4url_state']) == 1 ){
			
			exit('正在生成......');
		}
			
		if(intval($issueInfo['mp4url_state']) == 2 && intval($issueInfo['validity_time']) > time()){//判断是否已生成且没有过期
			header('Location:'.$issueInfo['fmp4url']);
			exit;
		}

		/*下面开始进入剪辑生成环节*/

		$back_url = 'http://'.$_SERVER['HTTP_HOST'] . U('Api/Issue/edit_bc_ad_part') ;//回调地址
		
		$cut_url = C('BC_ISSUE_CUT_URL');//剪辑电视广告片段的url
		M('tbcissue')->where(array('fbcissueid'=>$fbcissueid))->save(array('mp4url_state'=>1));//修改状态为正在生成		
		$start_time = strtotime($issueInfo['fstarttime']);//发布开始时间
		$end_time = strtotime($issueInfo['fendtime']);//发布结束时间
		$adInfo = M('tbcsample')
								->field('tad.fadname')
								->join('tad on tad.fadid = tbcsample.fadid')
								->where(array('tbcsample.fid'=>$issueInfo['fbcsampleid']))
								->find();//查询样本详情、广告名称
		$mediaInfo = M('tmedia')->field('fid,fcollecttype,fsavepath')->where(array('fid'=>$issueInfo['fmediaid']))->find();						
		if($mediaInfo['fcollecttype'] == 10){
			if(C('SELF_M3U8')){
				$m3u8_url = 'http://'.$_SERVER['HTTP_HOST'].U('Api/Media/get_media_m3u8',array('media_id'=>$issueInfo['fmediaid'],'start_time'=>$start_time,'end_time'=>$end_time));
			}else{
				$m3u8_url = A('Common/Shilian','Model')->get_playback_url($issueInfo['fmediaid'],$start_time,$end_time);
			}
		}
		
		if($mediaInfo['fcollecttype'] == 25 || $mediaInfo['fcollecttype'] == 26){
			$m3u8_url = $mediaInfo['fsavepath'].'&start='.$start_time.'&end='.$end_time;
		}					
		$file_name = I('n');
		if($file_name == ''){
			$file_name = urlencode(date('YmdHis',$start_time).'_'.$adInfo['fadname'].'_'.$fbcissueid);
		}
			
		$post_data = array(
							'bcissue_id'=>$fbcissueid,
							'start_time'=>$start_time,
							'end_time'=>$end_time,
							'm3u8_url'=>urlencode($m3u8_url),
							'back_url'=>urlencode($back_url),
							'file_name'=>$file_name,

								);
		$ret = http($cut_url,$post_data,'GET',false,1);
		
		exit('正在生成......');
													
	}
	
	
	
	/*生成广告片段*/
	public function create_ad_part(){
		session_write_close();
		$issueid = I('issueid');//发布记录id
		$table_name = I('table_name');//表名
		
		$back_url = U('Api/Issue/edit_ad_part@'.$_SERVER['HTTP_HOST'],array('issueid'=>$issueid,'table_name'=>$table_name)) ;
		$cut_url = C('ISSUE_CUT_URL');//剪辑广告片段的url
		
		if($table_name == 'ttvissue'){
			$issueInfo = M($table_name)
							->cache(true,6)
							->field('fmediaid,UNIX_TIMESTAMP(fstarttime) as fstarttime,UNIX_TIMESTAMP(fendtime) as fendtime,ftvsampleid as fsampleid')
							->where(array('ftvissueid'=>$issueid))->find();
		}elseif($table_name == 'tbcissue'){
			$issueInfo = M($table_name)
							->cache(true,6)
							->field('fmediaid,UNIX_TIMESTAMP(fstarttime) as fstarttime,UNIX_TIMESTAMP(fendtime) as fendtime,fbcsampleid as fsampleid')
							->where(array('fbcissueid'=>$issueid))->find();
		}else{
			$issueInfo = M($table_name)
							->cache(true,6)
							->field('fmediaid,fstarttime,fendtime,fsampleid')
							->where(array('fid'=>$issueid))->find();
		}					
							
		$start_time = $issueInfo['fstarttime'];
		$end_time = $issueInfo['fendtime'];
		
		
		$mediaInfo = M('tmedia')
								->cache(true,300)
								->field('left(fmediaclassid,2) as media_class')
								->where(array('fid'=>$issueInfo['fmediaid']))
								->find();
		if($mediaInfo['media_class'] == '01'){
			$tb = 'tv';
			$file_type = 'mp4';
		}elseif($mediaInfo['media_class'] == '02'){
			$tb = 'bc';
			$file_type = 'mp3';
		}else{
			$tb = 'tv';
		}
		
		$adInfo = M('t'.$tb.'sample')
								->cache(true,60)
								->field('tad.fadname')
								->join('tad on tad.fadid = t'.$tb.'sample.fadid')
								->where(array('t'.$tb.'sample.fid'=>$issueInfo['fsampleid']))
								->find();
							
		
		$m3u8_url = A('Common/Media','Model')->get_m3u8($issueInfo['fmediaid'],$start_time,$end_time);
		$post_data = array(
							
							'start_time'=>$start_time,
							'end_time'=>$end_time,
							'm3u8_url'=>urlencode($m3u8_url),
							'file_type'=>$file_type,
							
							'back_url'=>urlencode($back_url),
							'file_name'=>urlencode(date('YmdHis',$start_time).'_'.$adInfo['fadname'].'_'.$table_name.'_'.$issueid),
							
							
								);
					
		$ret = http($cut_url,$post_data,'GET',false,1);				

		$this->ajaxReturn(array('code'=>0,'msg'=>'正在生成','post_data'=>$post_data,'ret'=>$ret,'cut_url'=>$cut_url));						
		
		
	}
	
	/*生成广告片段*/
	public function create_ad_part_issue(){
		session_write_close();
		header("Content-type: text/html; charset=utf-8"); 
		echo '<meta http-equiv="refresh" content="2">';
		
		$issueid = I('issueid');//发布记录id
		$table_name = I('table_name');//表名
		
		$issuePart = S('issue_m3u8_'.$_GET['table_name'].'_'.$_GET['issueid']);
		
		if($issuePart && intval($issuePart['code']) == 0){
			header('Location:'.$issuePart['source_url']);
			exit;
		}elseif($issuePart && intval($issuePart['code']) != 0){
			
			
			exit($issuePart['msg']);
		}
		
		
		
		
		
		
		$back_url = U('Api/Issue/edit_ad_part@'.$_SERVER['HTTP_HOST'],array('issueid'=>$issueid,'table_name'=>$table_name)) ;
		$cut_url = C('ISSUE_CUT_URL');//剪辑广告片段的url
		try{//
			$issueInfo = M($table_name)
							->cache(true,6)
							->field('fmediaid,fstarttime,fendtime,fsampleid')
							->where(array('fid'=>$issueid))->find();
		}catch(Exception $error) { //如果创建失败
			$issueInfo = array();
		} 
		if(!$issueInfo){
			exit('不存在该记录...');
		}
							
							
		$start_time = $issueInfo['fstarttime'];
		$end_time = $issueInfo['fendtime'];
		
		
		$mediaInfo = M('tmedia')
								->cache(true,300)
								->field('left(fmediaclassid,2) as media_class')
								->where(array('fid'=>$issueInfo['fmediaid']))
								->find();
		if($mediaInfo['media_class'] == '01'){
			$tb = 'tv';
			$file_type = 'mp4';
		}elseif($mediaInfo['media_class'] == '02'){
			$tb = 'bc';
			$file_type = 'mp3';
		}else{
			$tb = 'tv';
		}
		
		$adInfo = M('t'.$tb.'sample')
								->cache(true,60)
								->field('tad.fadname')
								->join('tad on tad.fadid = t'.$tb.'sample.fadid')
								->where(array('t'.$tb.'sample.fid'=>$issueInfo['fsampleid']))
								->find();
							
		
		$m3u8_url = A('Common/Media','Model')->get_m3u8($issueInfo['fmediaid'],$start_time,$end_time);
		$post_data = array(
							
							'start_time'=>$start_time,
							'end_time'=>$end_time,
							'm3u8_url'=>urlencode($m3u8_url),
							'file_type'=>$file_type,
							
							'back_url'=>urlencode($back_url),
							'file_name'=>urlencode(date('YmdHis',$start_time).'_'.$adInfo['fadname'].'_'.$table_name.'_'.$issueid),
							
							
							);
		S('issue_m3u8_'.$_GET['table_name'].'_'.$_GET['issueid'],array('code'=>-100,'msg'=>'正在生成中......'),86400);			
		$ret = http($cut_url,$post_data,'GET',false,1);				

							
		exit('正在提交生成......');
		
	}
	
	
	
	
	
	
	/*修改发布记录视频片段地址*/
	public function edit_ad_part(){
		
		$post_data = file_get_contents('php://input');
		$post_data = json_decode($post_data,true);
		S('issue_m3u8_'.$_GET['table_name'].'_'.$_GET['issueid'],$post_data,864000);
		
	}
	
	
	/*获取广告片段生成状态*/
	public function get_ad_part(){
		session_write_close();
		$issueid = I('issueid');//发布记录id
		$table_name = I('table_name');//表名
		
		$data = S('issue_m3u8_'.$table_name.'_'.$issueid);
		
		if($data && intval($data['code']) >= 0){
			$this->ajaxReturn($data);
		}else{
			$this->ajaxReturn(array('code'=>-100,'msg'=>'请等待'));
		}
	}
	
	
	
	
	
}