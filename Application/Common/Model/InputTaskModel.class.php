<?php
namespace Common\Model;
use TaskInput\Model\TaskInputLinkModel;
use Think\Exception;
class InputTaskModel{
    // 不需审核的广告名称(不违法)
    protected $adNameNotIllegal = [
        'IN'  =>['本媒体宣传','本媒体广告','地方语言广告','电视剧','动画片','动漫火车','方言广告','公告','公告启事','公示公告','公益','公益广告','公益宣传','共建文明城市共享美好生活','江西新闻联播','交通广播','交通路况','交通事故','交通信息','节目预告','路况1021','路况信息','路况直播','路况直击','片花'],
        'LIKE'=>['公益%','公益广告%','节目预告%']
    ];
    /**
     * @Des: 获取无需审核的广告ID
     * @Edt: yuhou.wang
     * @Date: 2019-11-27 18:06:51
     * @param {type} 
     * @return: {Array} 
     */ 
    public function getAutoAdids(){
        $where[] = ['fstate'=>['GT',0]];
        $keyWhere = [
            ['fadname'=>['IN',$this->adNameNotIllegal['IN']]],
            ['fadname'=>['LIKE',$this->adNameNotIllegal['LIKE']]],
            '_logic'=>'OR'
        ];
        $where[] = $keyWhere;
        $fadids = M('tad')->cache(true,86400)->where($where)->getField('fadid',true);
        return $fadids;
    } 
    /**
     * @Des: 判断广告名称是否需要审核
     * @Edt: yuhou.wang
     * @Date: 2019-11-27 18:17:43
     * @param {Int} $fadname 广告名称
     * @return: {Boolean} true需审核 false无需审核
     */
    public function isAutoInspect($fadname){
        $fadid = M('tad')->cache(true,3)->where(['fadname'=>$fadname])->getField('fadid');
        $isAutoInspect = true;
        if(!empty($fadid)){
            $isExist = in_array($fadid,$this->getAutoAdids());
            $isAutoInspect = !$isExist;
        }
        return $isAutoInspect;
    }
    /**
     * @Des: 判断广告ID是否需要审核
     * @Edt: yuhou.wang
     * @Date: 2019-11-27 18:17:43
     * @param {Int} $fadname 广告名称
     * @return: {Boolean} true需审核 false无需审核
     */
    public function isAutoInspectByAdid($fadid){
        $isAutoInspect = true;
        if(!empty($fadid)){
            $isExist = in_array($fadid,$this->getAutoAdids());
            $isAutoInspect = !$isExist;
        }
        return $isAutoInspect;
    }

    /**
     * @Des: 创建传统任务及环节
     * @Date: 2019-11-27 10:06:27
     * @param $media_class 媒介类型 1电视 2广播 3报纸
     * @param $sam_id 样本ID
     * @param $typeArr 定义需要创建的环节类型 如：[1,2],[2]
     * @return: $taskid 任务ID
     */
    public function add_task($media_class,$sam_id,$typeArr=[1,2]){
		$sam_where = array();
		if($media_class == 1){
			$sam_table = 'ttvsample';
			$sam_where['ttvsample.fid'] = $sam_id;
		}elseif($media_class == 2){
			$sam_table = 'tbcsample';
			$sam_where['tbcsample.fid'] = $sam_id;
		}elseif($media_class == 3){
			$sam_table = 'tpapersample';
			$sam_where['tpapersample.fpapersampleid'] = $sam_id;
		}
        $samInfo = M($sam_table)
            ->cache(true,1)
			->master(true)
			->where($sam_where)
			->find();
		$adInfo = M('tad')->cache(true,60)->where(array('fadid'=>$samInfo['fadid']))->find();//查询广告信息
		$mediaInfo = M('tmedia')
            ->cache(true,600)
            ->field('tmedia.fid,left(tmedia.fmediaclassid,2) as media_class,tmediaowner.fregionid,tmedia.fmediaownerid,tmedia.priority,tmedia.group_id')
            ->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
            ->where(array('tmedia.fid'=>$samInfo['fmediaid']))
            ->find();//查询媒介信息
		$source_path = $samInfo['favifilename'];
		if($media_class == 3){
			$source_path = $samInfo['fjpgfilename'];
        }
        // 该UUID是否已存在任务
        $taskExist = M('task_input')->where(['sam_uuid'=>$samInfo['uuid']])->count();
        if ($taskExist == 0) {
            $a_data = array();
            $a_data['media_class'] = $media_class; //媒介类型，1电视，2广播，3报刊
            $a_data['tem_ad_name'] = $samInfo['tem_ad_name']; //临时广告名称
            $a_data['fadid']       = $adInfo['fadid']; //广告id
            $a_data['sam_id']      = $sam_id; //样本id
            $a_data['media_id']    = $mediaInfo['fid']; //媒介id
            $a_data['source_path'] = $source_path; //素材路径
            $a_data['issue_date']  = $samInfo['fissuedate'];//发布日期
            $a_data['lasttime']    = time(); //最后更新时间
            $a_data['sam_uuid']    = $samInfo['uuid'];//样本uuid
            $a_data['priority']    = $mediaInfo['priority'];//媒体优先级叠加到任务优先级
            $a_data['group_id']    = $mediaInfo['group_id'];//媒体分组叠加到任务分组
            if(substr($mediaInfo['fregionid'],0,2) == '33'){
                $a_data['group_id'] = 65;//
            }
            try {//尝试查询数据
                $taskid = M('task_input')->add($a_data);//添加任务
                // 记录日志
                if ($taskid) {
                    $taskInputLinkModel = new TaskInputLinkModel();
                    if (strtotime($a_data['issue_date']) >= strtotime('2018-08-01')) {
                        $res = $taskInputLinkModel->newTaskLink($taskid,$media_class,$typeArr);
                    } else {
                        $res = true;
                    }
                    if ($res) {
                        $a_data['taskid'] = $taskid;
                        A('Common/AliyunLog', 'Model')->task_input_log([
                            'type' => 'add',
                            'result' => $taskid,
                            'data' => $a_data,
                            'msg' => '任务添加成功;'
                        ]);
                        // 是否需要录入
                        $isAdExist = M('tad')->where(['fadname'=>$samInfo['tem_ad_name'],'fadid'=>['NEQ',0],'fstate'=>['GT',0]])->order('is_sure DESC,fstate DESC')->getField('fadid');
                        $isAutoInput = !empty($isAdExist) && !empty($a_data['fadid']) && $adInfo['fadname']!='社会公益' && $samInfo['tem_ad_name']!='社会公益' && $adInfo['fadname']!='未知广告' && $samInfo['tem_ad_name']!='未知广告';
                        if($isAutoInput){
                            M('task_input_link')->where(['taskid'=>$taskid,'media_class'=>$media_class,'type'=>1])->save(['state'=>2,'update_time'=>time()]);
                            $flowData = [
                                'taskid'       => $taskid,
                                'flow_content' => '系统自动录入',
                                'flow_time'    => time(),
                                'wx_id'        => 0,
                                'task_info'    => json_encode($a_data),
                            ];
                            $flowRes = M('task_input_flow')->add($flowData);
                        }
                        // 是否需要审核
                        $isNeedInspect = $this->isAutoInspect($samInfo['tem_ad_name']);
                        $isAutoInspect = !$isNeedInspect && !empty($a_data['fadid']) && $adInfo['fadname']!='社会公益' && $samInfo['tem_ad_name']!='社会公益' && $adInfo['fadname']!='未知广告' && $samInfo['tem_ad_name']!='未知广告';
                        if($isAutoInspect){
                            M('task_input_link')->where(['taskid'=>$taskid,'media_class'=>$media_class,'type'=>2])->save(['state'=>2,'update_time'=>time()]);
                            $flowData = [
                                'taskid'       => $taskid,
                                'flow_content' => '系统自动审核',
                                'flow_time'    => time(),
                                'wx_id'        => 0,
                                'task_info'    => json_encode($a_data),
                            ];
                            $flowRes = M('task_input_flow')->add($flowData);
                        }
                        // 若无需录入且无需审核，则将任务完成
                        if($isAutoInput && $isAutoInspect){
                            M('task_input')->where(['taskid'=>$taskid])->save(['task_state'=>2,'syn_state'=>1]);
                            $flowData = [
                                'taskid'       => $taskid,
                                'flow_content' => '系统自动录入审核并完成',
                                'flow_time'    => time(),
                                'wx_id'        => 0,
                                'task_info'    => json_encode($a_data),
                            ];
                            $flowRes = M('task_input_flow')->add($flowData);
                        }
                    } else {
                        A('Common/AliyunLog', 'Model')->task_input_log([
                            'type' => 'add',
                            'result' => $taskid,
                            'data' => $a_data,
                            'msg' => '任务添加失败;'
                        ]);
                    }
                } else {
                    A('Common/AliyunLog', 'Model')->task_input_log([
                        'type' => 'add',
                        'result' => $taskid,
                        'data' => $a_data,
                        'msg' => '任务添加失败;'
                    ]);
                }
            } catch (Exception $error) {
                // 添加失败的时候, 大概率是因为uuid相同, 找到这个任务, 进行同步时的操作
                if ($media_class != 3) {
                    // 找到已有的任务
                    $taskInfo = M('task_input')
                        ->field('taskid, media_class, sam_id, fadid, fversion, fspokesman, fillegaltypecode, fillegalcontent, fexpressioncodes, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, wx_id fmodifier, lasttime fmodifytime, cut_err_state, cut_err_reason, is_long_ad, sam_uuid, wx_id finputuser, wx_id finspectuser')
                        ->where([
                            'sam_uuid' => ['EQ', $samInfo['uuid']],
                            'media_class' => ['EQ', $media_class],
                        ])
                        ->find();
                    $taskInfo['finputstate'] = 2;
                    $taskInfo['finspectstate'] = 2;
                    $taskInfo['fmodifytime'] = date('Y-m-d H:i:s', $taskInfo['fmodifytime']);
                    $taskInfo['fmodifier'] = M('ad_input_user')->cache(true, 60)->where(['wx_id' => ['EQ', $taskInfo['fmodifier']]])->getField('alias') . '_' . $taskInfo['fmodifier'];
                    // 得到分类的字符串名称
                    $sampleClassArr = [
                        '',
                        'tv',
                        'bc',
                        'paper',
                    ];
                    $media_class_str = $sampleClassArr[$media_class];
                    // 判断是否未剪辑错误, 如果是剪辑错误, 那就当做已经处理过了, 把任务内容更新
                    if ($taskInfo['cut_err_state'] != 0) {
                        $flowData = [
                            'taskid' => $taskInfo['taskid'],
                            'flow_content' => '该剪辑错误样本重新推送, 任务信息更新',
                            'flow_time' => time(),
                            'wx_id' => 0,
                            'task_info' => json_encode($taskInfo),
                        ];
                        $data = [
                            'lasttime' => time(),
                            'cut_err_state' => 0,
                            'task_state' => 0,
                            'wx_id' => 0,
                            'syn_state' => 0,
                            'quality_state' => 0,
                            'media_id' => $samInfo['fmediaid'],
                            'issue_date' => $samInfo['fissuedate'],
                            'source_path' => $samInfo['favifilename'],
                        ];
                        $taskUpdate = M('task_input')->where(['taskid' => ['EQ', $taskInfo['taskid']]])->save($data);
                        $data_link = [
                            'wx_id'=>0,
                            'state'=>0,
                            'update_time'=>time(),
                        ];
                        // 更新环节状态 yuhou.wang 20200119
                        $taskLinkUpdate = M('task_input_link')->where(['taskid'=>$taskInfo['taskid'],'media_class'=>$media_class])->save($data_link);
                        M('task_input_flow')->add($flowData);
                        if ($taskUpdate) {
                            A('Common/AliyunLog', 'Model')->task_input_log([
                                'type' => 'save',
                                'result' => $taskUpdate,
                                'data' => $data,
                                'msg' => '添加任务uuid冲突且该任务为剪辑错误时, 任务信息更新成功;'
                            ]);
                        } else {
                            A('Common/AliyunLog', 'Model')->task_input_log([
                                'type' => 'save',
                                'result' => $taskUpdate,
                                'data' => $data,
                                'msg' => '添加任务uuid冲突且该任务为剪辑错误时, 任务信息更新失败;'
                            ]);
                        }
                    } else {
                        // 判断是否违法
                        if ($taskInfo['fillegaltypecode'] != 0) {
                            $expressionCodes = explode(';', $taskInfo['fexpressioncodes']);
                            $expressionsAndConfirmation = M('tillegal')->field('fexpression, fconfirmation')->cache(true)->where(['fcode' => ['IN', $expressionCodes]])->select();
                            $taskInfo['fexpressions'] = '';
                            $taskInfo['fconfirmations'] = '';
                            foreach ($expressionsAndConfirmation as $item) {
                                $taskInfo['fexpressions'] .= $item['fexpression'] . ' ;; ' ;
                                $taskInfo['fconfirmations'] .= $item['fconfirmation'] . ' ;; ' ;
                            }
                        } else {
                            $taskInfo['fillegalcontent'] = '';
                            $taskInfo['fexpressioncodes'] = '';
                            $taskInfo['fexpressions'] = '';
                            $taskInfo['fconfirmations'] = '';
                        }
                        $sampleUpdate = M($sam_table)->where(['fid' => ['EQ', $sam_id]])->save($taskInfo);
                        if ($sampleUpdate) {
                            A('Common/AliyunLog', 'Model')->tv_bc_sample_log([
                                'type' => 'save',
                                'result' => $sampleUpdate,
                                'data' => $samInfo,
                                'media_class' => $media_class,
                                'msg' => '添加任务时uuid冲突, 样本信息更新成功'
                            ]);
                        } else {
                            A('Common/AliyunLog', 'Model')->tv_bc_sample_log([
                                'type' => 'save',
                                'result' => $sampleUpdate,
                                'data' => $samInfo,
                                'media_class' => $media_class,
                                'msg' => '添加任务时uuid冲突, 样本信息更新失败',
                            ]);
                        }
                    }
                } else {
                    // 如果添加失败并且媒体类型为报纸
                    A('Common/AliyunLog', 'Model')->task_input_log([
                        'type' => 'add',
                        'result' => $taskid,
                        'data' => $a_data,
                        'msg' => 'add方法报错且媒体类型不为1 , 2;'
                    ]);
                }
                $taskid = 0;
            }
        }else{
            // 该UUID的任务已存在
            $taskInfo = M('task_input')->where(['sam_uuid'=>$samInfo['uuid']])->order('lasttime DESC')->find();
            $taskid = $taskInfo['taskid'];
            $adInfo = M('tad')->cache(true,120)->where(['fadid'=>$taskInfo['fadid']])->find();
            if($taskInfo['task_state']==2 && !empty($taskInfo['fadid']) && $taskInfo['cut_err_state']==0 && $adInfo['fadname']!='社会公益'){
                // 任务正常完成且无剪辑错误且不是“社会公益”，则同步到该UUID的所有样本
                $res = M('task_input')->where(['taskid'=>$taskInfo['taskid']])->save(['syn_state'=>1]);
            }else{
                // 否则重置任务为初始
                $res = A('TaskInput/TaskInput','Model')->changeTask($media_class,$samInfo['uuid'],0,50);
            }
        }
		return $taskid;
    }
    
	public function add_task_V20191127_OLD($media_class,$sam_id){
		
		//var_dump($sam_id);
		$sam_where = array();
		if($media_class == 1){
			$sam_table = 'ttvsample';
			$sam_where['ttvsample.fid'] = $sam_id;
		}elseif($media_class == 2){
			$sam_table = 'tbcsample';
			$sam_where['tbcsample.fid'] = $sam_id;
		}elseif($media_class == 3){
			$sam_table = 'tpapersample';
			$sam_where['tpapersample.fpapersampleid'] = $sam_id;
		}
		

		
		$samInfo = M($sam_table)->cache(true,1)
								->master(true)
								->where($sam_where)
								->find();//查询样本信息
		
		
		$adInfo = M('tad')->cache(true,60)->where(array('fadid'=>$samInfo['fadid']))->find();//查询广告信息
		$mediaInfo = M('tmedia')
								->cache(true,600)
								->field('tmedia.fid,left(tmedia.fmediaclassid,2) as media_class,tmediaowner.fregionid,tmedia.fmediaownerid,tmedia.priority')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>$samInfo['fmediaid']))->find();//查询媒介信息
		
		$source_path = $samInfo['favifilename'];
		if($media_class == 3){
			$source_path = $samInfo['fjpgfilename'];
		}
		//file_put_contents('LOG/samInfo',$source_path."\n".M($sam_table)->getLastSql()."\n".json_encode($samInfo));
		$a_data = array();
		
		$a_data['media_class'] = $media_class; //媒介类型，1电视，2广播，3报刊
		$a_data['tem_ad_name'] = $samInfo['tem_ad_name']; //临时广告名称
		//var_dump($samInfo['tem_ad_name']);
		
		$a_data['fadid'] = $adInfo['fadid']; //广告id
		$a_data['sam_id'] = $sam_id; //样本id
		$a_data['media_id'] = $mediaInfo['fid']; //媒介id
		$a_data['source_path'] = $source_path; //素材路径
		$a_data['issue_date'] = $samInfo['fissuedate'];//发布日期
		$a_data['lasttime'] = time(); //最后更新时间
		$a_data['sam_uuid'] = $samInfo['uuid'];//样本uuid
		$a_data['priority'] = $mediaInfo['priority'];//媒体优先级叠加到任务优先级
		
		
		
		if(substr($mediaInfo['fregionid'],0,2) == '33'){
			$a_data['group_id'] = 1;//
		}
		
		//var_dump($sam_id);
		
		try{//尝试查询数据
			$taskid = M('task_input')->add($a_data);//添加任务
            // 记录日志
            if ($taskid){
                $taskInputLinkModel = new TaskInputLinkModel();
                if (strtotime($a_data['issue_date']) >= strtotime('2018-08-01')){
                    $res = $taskInputLinkModel->newTaskLink($taskid, $media_class);
                }else{
                    $res = true;
                }
                if ($res){
                    $a_data['taskid'] = $taskid;
                    A('Common/AliyunLog','Model')->task_input_log([
                        'type' => 'add',
                        'result' => $taskid,
                        'data' => $a_data,
                        'msg' => '任务添加成功;'
                    ]);
                }else{
                    A('Common/AliyunLog','Model')->task_input_log([
                        'type' => 'add',
                        'result' => $taskid,
                        'data' => $a_data,
                        'msg' => '任务添加失败;'
                    ]);
                }
            }else{
                A('Common/AliyunLog','Model')->task_input_log([
                    'type' => 'add',
                    'result' => $taskid,
                    'data' => $a_data,
                    'msg' => '任务添加失败;'
                ]);
            }
		}catch(Exception $error) { //
		    // 添加失败的时候, 大概率是因为uuid相同, 找到这个任务, 进行同步时的操作
            if ($media_class != 3){
                // 找到已有的任务
                $taskInfo = M('task_input')
                    ->field('taskid, media_class, sam_id, fadid, fversion, fspokesman, fillegaltypecode, fillegalcontent, fexpressioncodes, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, wx_id fmodifier, lasttime fmodifytime, cut_err_state, cut_err_reason, is_long_ad, sam_uuid, wx_id finputuser, wx_id finspectuser')
                    ->where([
                        'sam_uuid' => ['EQ', $samInfo['uuid']],
						'media_class' => ['EQ', $media_class],
                    ])
                    ->find();
                $taskInfo['finputstate'] = 2;
                $taskInfo['finspectstate'] = 2;
                $taskInfo['fmodifytime'] = date('Y-m-d H:i:s', $taskInfo['fmodifytime'] );
                $taskInfo['fmodifier'] = M('ad_input_user')->cache(true, 60)->where(['wx_id' => ['EQ', $taskInfo['fmodifier']]])->getField('alias') . '_' . $taskInfo['fmodifier'];
                // 得到分类的字符串名称
                $sampleClassArr = [
                    '',
                    'tv',
                    'bc',
                    'paper',
                ];
                $media_class_str = $sampleClassArr[$media_class];
                // 判断是否未剪辑错误, 如果是剪辑错误, 那就当做已经处理过了, 把任务内容更新
                if ($taskInfo['cut_err_state'] != 0){
                    $flowData = [
                        'taskid' => $taskInfo['taskid'],
                        'flow_content' => '该剪辑错误样本重新推送, 任务信息更新',
                        'flow_time' => time(),
                        'wx_id' => 0,
                        'task_info' => json_encode($taskInfo),
                    ];
                    $data = [
                        'lasttime' => time(),
                        'cut_err_state' => 0,
                        'task_state' => 0,
                        'wx_id' => 0,
                        'syn_state' => 0,
                        'quality_state' => 0,
                        'media_id' => $samInfo['fmediaid'],
                        'issue_date' => $samInfo['fissuedate'],
                        'source_path' => $samInfo['favifilename'],
                    ];
                    $taskUpdate = M('task_input')->where(['taskid' => ['EQ', $taskInfo['taskid']]])->save($data);
                    M('task_input_flow')->add($flowData);
                    if ($taskUpdate){
                        A('Common/AliyunLog','Model')->task_input_log([
                            'type' => 'save',
                            'result' => $taskUpdate,
                            'data' => $data,
                            'msg' => '添加任务uuid冲突且该任务为剪辑错误时, 任务信息更新成功;'
                        ]);
                    }else{
                        A('Common/AliyunLog','Model')->task_input_log([
                            'type' => 'save',
                            'result' => $taskUpdate,
                            'data' => $data,
                            'msg' => '添加任务uuid冲突且该任务为剪辑错误时, 任务信息更新失败;'
                        ]);
                    }
                }else{
                    // 判断是否违法
                    if ($taskInfo['fillegaltypecode'] != 0){
                        $expressionCodes = explode(';', $taskInfo['fexpressioncodes']);
                        $expressionsAndConfirmation = M('tillegal')->field('fexpression, fconfirmation')->cache(true)->where(['fcode' => ['IN', $expressionCodes]])->select();
                        $taskInfo['fexpressions'] = '';
                        $taskInfo['fconfirmations'] = '';
                        foreach ($expressionsAndConfirmation as $item) {
                            $taskInfo['fexpressions'] .= $item['fexpression'] . ' ;; ' ;
                            $taskInfo['fconfirmations'] .= $item['fconfirmation'] . ' ;; ' ;
                        }
                    }else{
                        $taskInfo['fillegalcontent'] = '';
                        $taskInfo['fexpressioncodes'] = '';
                        $taskInfo['fexpressions'] = '';
                        $taskInfo['fconfirmations'] = '';
                    }
                    $sampleUpdate = M($sam_table)->where(['fid' => ['EQ', $sam_id]])->save($taskInfo);
                    if ($sampleUpdate){
                        A('Common/AliyunLog','Model')->tv_bc_sample_log([
                            'type' => 'save',
                            'result' => $sampleUpdate,
                            'data' => $samInfo,
                            'media_class' => $media_class,
                            'msg' => '添加任务时uuid冲突, 样本信息更新成功'
                        ]);
                    }else{
                        A('Common/AliyunLog','Model')->tv_bc_sample_log([
                            'type' => 'save',
                            'result' => $sampleUpdate,
                            'data' => $samInfo,
                            'media_class' => $media_class,
                            'msg' => '添加任务时uuid冲突, 样本信息更新失败',
                        ]);
                    }

                }
            }else{
                // 如果添加失败并且媒体类型为报纸
                A('Common/AliyunLog','Model')->task_input_log([
                    'type' => 'add',
                    'result' => $taskid,
                    'data' => $a_data,
                    'msg' => 'add方法报错且媒体类型不为1 , 2;'
                ]);
            }
			$taskid = 0;//
		}
		return $taskid;
	}
	
	/*获取一条任务id*/
	public function get_task(){
		
		$wx_id = intval(session('wx_id'));
		$taskid = M('task_input')->where(array('wx_id'=>$wx_id,'task_state'=>1, 'cut_err_state' => 0))->getField('taskid');
        if($taskid) return $taskid;//如果有正在执行的任务，直接返回正在执行的任务id
        // 获取用户所在组的权限信息
        $userGroupInfo = M('ad_input_user_group')
            ->field('ad_input_user_group.other_authority, ad_input_user_group.mediaauthority, ad_input_user_group.regionauthority, ad_input_user_group.dateauthority, ad_input_user.prefer_list, ad_input_user.priority_list')
            ->join('ad_input_user on ad_input_user.group_id = ad_input_user_group.group_id')
            ->where(array('ad_input_user.wx_id'=>$wx_id))
            ->find();
        // 获取媒体偏好
        $mediaPrefer = $userGroupInfo['prefer_list'];
        // 获取优先级偏好
        $priorityList = $userGroupInfo['priority_list'];

        $searchWhere = [
            'task.task_state' => 0,
            'task.cut_err_state' => 0,
            'link.taskid' => ['EXP', 'is null'],
        ];
        $mediaIds = [];

        // 生成搜索条件及子查询
        if ($userGroupInfo['mediaauthority'] !== 'unrestricted'){
            if (!$userGroupInfo['mediaauthority'] || $userGroupInfo['mediaauthority'] === '""'){
                return false;
            }
            $mediaIds = array_merge($mediaIds, json_decode($userGroupInfo['mediaauthority'], true));
            $searchWhere['task.media_id'] = ['IN', $mediaIds];
        }
        // 地区限制的条件
        if ($userGroupInfo['regionauthority'] !== 'unrestricted'){
            if (!$userGroupInfo['regionauthority'] || $userGroupInfo['regionauthority'] === '""'){
                return false;
            }
            $userGroupInfo['regionauthority'] = json_decode($userGroupInfo['regionauthority'], true);
            // 区分并组合查找本级的和下属的条件, 生成mediaOwner的子查询搜索条件
            $inArr = [];
            $leftArr = [];
            foreach ($userGroupInfo['regionauthority'] as $item) {
                $len = strlen($item);
                if ($len == 6){
                    $inArr[] = $item;
                }else{
                    $leftArr[] = "(LEFT(fregionid, $len) = $item)";
                }
            }
            // 搜索条件有2个部分, 一个是全部地区id的本级媒体, 一个是部分地区id本级及下属媒体,互相之间的关系应该是OR
            $_mediaOwnerWhere = [
                '_logic' => 'OR',
            ];
            if (count($inArr) != 0){
                $_mediaOwnerWhere['fregionid'] = ['IN', $inArr];
            }
            if (count($leftArr) != 0){
                $_string = implode(' OR ', $leftArr);
                $_mediaOwnerWhere['_string'] = $_string;
            }
            // 生成mediaOwner子查询
            $subQuery = M('tmediaowner')->field('fid')->where($_mediaOwnerWhere)->buildSql();
            $_mediaWhere = [
                'fmediaownerid' => ['EXP', 'IN '. $subQuery]
            ];
            $res = M('tmedia')->where($_mediaWhere)->getField('fid', true);
            // 得到允许的媒体id
            $mediaIds = array_merge($mediaIds, $res);
            $searchWhere['task.media_id'] = ['IN', $mediaIds];
        }
        // 加入媒体偏好元素
        if ($mediaPrefer){
            $mediaPrefer = explode(',', $mediaPrefer);
            if ($userGroupInfo['mediaauthority'] !== 'unrestricted') {
                $preferList = array_intersect($mediaPrefer, $mediaIds);
            }else{
                $preferList = $mediaPrefer;
            }
            $preferList = implode(',', $preferList);
        }
        if (!$preferList){
            $preferList = "null";
        }
        // 加入优先级偏好元素
        if (!$priorityList){
            $priorityList = "null";
        }
        // 发布日期限制条件
        if ($userGroupInfo['dateauthority'] !== 'unrestricted'){
            if (!$userGroupInfo['dateauthority'] || $userGroupInfo['dateauthority'] === '""'){
                return false;
            }
            $dateauthority = json_decode($userGroupInfo['dateauthority'], true);
            $dateWhere = [];
            foreach ($dateauthority as $key => $value) {
                $dateWhere[] = ['BETWEEN', explode(' , ', $value)];
            }
            $dateWhere[] = 'OR';
            $searchWhere['task.issue_date'] = $dateWhere;
        }

		$while_num = 0;
		while(!$e_task_state){
			$while_num++;
            $orderArr = [];
            if ($priorityList !== 'null'){
                $orderArr[] = "field(priority, $priorityList) desc";
            }
            if ($preferList !== 'null'){
                $orderArr[] = "field(media_id, $preferList) desc";
            }
            $orderArr[] = 'priority desc';
            $orderArr[] = 'issue_date desc';
			$taskid = M('task_input')
                ->alias('task')
                ->field('task.taskid')
                ->join('left join task_input_link link on task.taskid = link.taskid and task.media_class = link.media_class')
                ->where($searchWhere)
                ->order(implode(',', $orderArr))
                ->limit(100)
                ->select();
			if(!$taskid) return false;//如果没有获取到任务，就返回false
            // 为了保证高优先级任务优先分配, 第一项就不打乱顺序了
            $tempItem = $taskid[0];
            shuffle($taskid);
            $taskid[0] = $tempItem;
            foreach ($taskid as $item) {
                $e_task_state = M('task_input')->where(array('task_state'=>0,'taskid'=>$item['taskid']))->save(array(
                    'wx_id'=>$wx_id,
                    'task_state'=>1,
                    'lasttime'=>time(),
                ));
                if($e_task_state){
                    A('TaskInput/Data','Model')->add_flow($item['taskid'],'领取任务',$wx_id);
                    A('Common/AliyunLog','Model')->task_input_log([
                        'type' => 'save',
                        'result' => $e_task_state,
                        'data' => $item['taskid'],
                        'msg' => '领取任务成功;'
                    ]);
                    return $item['taskid'];

                }
            }
            A('Common/AliyunLog','Model')->task_input_log([
                'type' => 'save',
                'taskids' => $taskid,
                'result' => $e_task_state,
                'msg' => '领取任务失败;'
            ]);

			if($while_num > 10) return false;//如果抢了10次还没有任务，那就返回false
		}
		
		
		
    }
    
	/**
     * 获取互联网广告任务
     */
	public function get_netad_task(){
		$wx_id = intval(session('wx_id'));
        $taskid = M('tnettask')
            ->where(['wx_id'=>$wx_id,'task_state'=>1])
            ->getField('taskid');
        if($taskid) return $taskid;//如果有正在执行的任务，直接返回正在执行的任务id
        // 获取用户所在组的权限信息
        $userGroupInfo = M('ad_input_user_group')
            ->alias('T')
            ->field('T.other_authority, 
                T.mediaauthority, 
                T.regionauthority, 
                T.dateauthority, 
                T.classauthority, 
                U.prefer_list,
                U.priority_list')
            ->join('ad_input_user AS U ON U.group_id = T.group_id')
            ->where(['U.wx_id'=>$wx_id])
            ->find();
        // 获取媒体偏好
        $mediaPrefer = $userGroupInfo['prefer_list'];
        // 获取优先级偏好
        $priorityList = $userGroupInfo['priority_list'] ? $userGroupInfo['priority_list'] : "null";
        $searchWhere = [
            'task_state' => 0,
        ];
        $mediaIds = [];
        // 加入媒体偏好元素
        if ($mediaPrefer){
            $mediaPrefer = explode(',', $mediaPrefer);
            if ($userGroupInfo['mediaauthority'] !== 'unrestricted') {
                $preferList = array_intersect($mediaPrefer, $mediaIds);
            }else{
                $preferList = $mediaPrefer;
            }
            $preferList = implode(',', $preferList);
        }
        if (!$preferList){
            $preferList = "null";
        }
        // 发布日期限制条件
        if ($userGroupInfo['dateauthority'] !== 'unrestricted'){
            if (!$userGroupInfo['dateauthority'] || $userGroupInfo['dateauthority'] === '""'){
                return false;
            }
            $dateauthority = json_decode($userGroupInfo['dateauthority'], true);
            $dateWhere = [];
            foreach ($dateauthority as $key => $value) {
                $date_a_e = explode(' , ', $value);
                foreach($date_a_e as $k=>$v){
                    $date_a_e[$k] = strtotime($v);
                }
                $dateWhere[] = ['BETWEEN', $date_a_e];
            }
            $dateWhere[] = 'OR';
            $searchWhere['net_created_date'] = $dateWhere;
        }
        // 暂时使用classauthority这个字段限制互联网广告的优先级, 后期考虑加个字段限制
        if (!in_array($userGroupInfo['classauthority'], [null, '', '""', 'unrestricted'])){
            $searchWhere['priority'] = ['ELT', $userGroupInfo['classauthority']];
        }

		$while_num = 0;
		while(!$e_task_state){
			$while_num++;
            $taskids = M('tnettask')
                ->where($searchWhere)
                ->order("field(priority, $priorityList) desc, field(media_id, $preferList) desc, priority desc, issue_date desc")
                ->limit(100)
                ->getField('taskid', true);
			if(!$taskids) return false;//如果没有获取到任务，就返回false
            shuffle($taskids);
            foreach ($taskids as $taskid) {
                $e_task_state = M('tnettask')
                    ->where(array('task_state'=>0,'taskid'=>$taskid))
                    ->save([
                            'wx_id'=>$wx_id,
                            'task_state'=>1,
                            'modifytime'=>time()
                        ]
                    );
                if($e_task_state){
                    A('TaskInput/Data','Model')->add_netad_flow($taskid,'领取任务',$wx_id);
                    return $taskid;
                }
            }
			if($while_num >= 10) return false;//如果抢了10次还没有任务，那就返回false
		}
	}


    /**
     * 得到总局确认过的时间和媒体列表
     * @return array
     * {
     *      '2018-01-01': [1,4,5,6 ...]
     *          ...
     * }
     */
    public function processZongjuConfirmMedia()
    {
        $confirmList = M('zongju_data_confirm')->getField('dmp_confirm_content', true);
        $tempArr = [];
        foreach ($confirmList as $item) {
            if (!$item){
                continue;
            }
            $item = json_decode($item, true);
            foreach ($item as $data) {
                foreach ($data as $datum) {
                    $tempArr[$datum['date']] = array_merge($tempArr[$datum['date']] ? $tempArr[$datum['date']] : [], $datum['selected']);
                }
            }
        }
        $tempArr = array_filter($tempArr);
        return $tempArr;
	}
}