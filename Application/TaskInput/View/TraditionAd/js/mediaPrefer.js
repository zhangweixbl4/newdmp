Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}
var vm = new Vue({
  el: '#app',
  data: {
    loading: false,
    memberList: memberList,
    mediaClassTree: mediaClassTree,
    regionTree: regionTree,
    mediaLabelList: mediaLabelList,
    where: {},
    searchTempWhere: {},
    page: 1,
    total: 0,
    pageSize: 50,
    originalMediaList: [],
    activeUser: {},
    originalPreferList: [],
    isIndeterminate: false,
    checkAll: false,
    checkedMemberList: [],
    searchUser: '',
    priorityList: [],
  },
  computed: {
    mediaList: function () {
      return !this.originalMediaList ? [] : this.originalMediaList.map((v, i) => {
        let index = this.preferIds.indexOf(v.value)
        v.selected = index !== -1;
        return v
      })
    },
    preferIds: function () {
      return this.originalPreferList.map((v) => v.value)
    },
    filterMemberList: function () {
      if (this.searchUser) {
        return this.memberList.filter(v => v.label.indexOf(this.searchUser) !== -1)
      } else {
        return this.memberList
      }
    }
  },
  methods: {
    resetWhere: function () {
      this.where = {}
      this.searchTempWhere = {}
      this.page = 1
      this.searchReq()
    },
    search: function () {
      this.where.mediaClass = this.searchTempWhere.mediaClass ? this.searchTempWhere.mediaClass[this.searchTempWhere.mediaClass.length - 1] : ''
      this.where.region = this.searchTempWhere.region ? this.searchTempWhere.region[this.searchTempWhere.region.length - 1] : ''
      this.where.onlyThisLevel = this.searchTempWhere.onlyThisLevel ? 1 : 0
      this.page = 1
      this.searchReq()
    },
    searchReq: function () {
      this.loading = true
      $.post('/Api/Media/searchMedia', {
        where: this.where,
        page: this.page,
        pageSize: this.pageSize,
      }).then((res) => {
        if (res.code == 0) {
          this.originalMediaList = res.data
          this.total = +res.total
        }
        this.loading = false
      })
    },
    clickMediaList: function (item) {
      let index = this.preferIds.indexOf(item.value)
      if (index === -1) {
        this.originalPreferList.push(item)
      } else {
        this.originalPreferList.splice(index, 1)
      }
    },
    removePreferItem: function (index) {
      this.originalPreferList.splice(index, 1)
    },
    savePreferList: function () {
      if (this.checkedMemberList.length === 0) {
        this.$alert('请至少选择一名成员')
        return false
      }
      this.$confirm('确认保存?').then(() => {
        this.loading = true
        $.post('editPreferList', {
          wx_id: this.checkedMemberList,
          list: this.preferIds.join(',')
        }).then((res) => {
          if (res.code == 0) {
            this.$message.success(res.msg)
          } else {
            this.$message.error(res.msg)
          }
          this.loading = false
        })
      }).catch(() => {})
    },
    pageChange: function (page) {
      this.page = page
      this.searchReq()
    },
    allPick: function () {
      this.originalMediaList.forEach((v, i) => {
        let index = this.preferIds.indexOf(v.value)
        if (index === -1) {
          this.originalPreferList.push(v)
        }
      })
    },
    allCancelPick: function () {
      this.originalMediaList.forEach((v, i) => {
        let index = this.preferIds.indexOf(v.value)
        if (index !== -1) {
          this.originalPreferList.splice(index, 1)
        }
      })
    },
    reversePick: function () {
      this.originalMediaList.forEach((v, i) => {
        this.clickMediaList(v)
      })
    },
    handleCheckedMemberChange: function (v) {

    },
    handleCheckedMemberClick: function (item) {
      this.activeUser = item
      this.loading = true
      $.post('getPreferListByWxID', {
        wx_id: item.value
      }).then((res) => {
        this.loading = false
        this.originalPreferList = res.mediaList
        this.priorityList = res.priorityList
      })
    },
    handleCheckAllChange: function () {
      if (this.isIndeterminate) {
        this.checkedMemberList = []
      } else {
        this.checkedMemberList = this.memberList.map((v) => v.value)
      }
      this.isIndeterminate = !this.isIndeterminate
    },
    addPriority: function () {
      this.$prompt('请输入新添加的优先级偏好?', '提示', {
        confirmButtonText: '提交',
        cancelButtonText: '取消',
        inputPattern: /^-?\d+$/,
        inputErrorMessage: '必须是数字'
      }).then((value) => {
        this.priorityList.push(value.value)
      }).catch(() => {})
    },
    deletePriority: function (index) {
      this.priorityList.splice(index, 1)
    },
    savePriority: function () {
      this.loading = true
      let list = this.priorityList.length === 0 ? '' : this.priorityList.join(',')
      $.post('editPriorityList', {
        wx_id: this.checkedMemberList,
        list: list,
      }).then((res) => {
        this.loading = false
        if (res.code == 0) {
          this.$message.success(res.msg)
        } else {
          this.$message.error(res.msg)
        }
      })
    },
    pageSizeChange: function (val) {
      this.pageSize = val
      this.search()
    }
  }
})