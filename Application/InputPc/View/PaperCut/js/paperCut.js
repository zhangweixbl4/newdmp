Vue.prototype.$ELEMENT = { size: 'mini', zIndex: 3000 }
const vm = new Vue({
    el: '#app',
    data: function () {
        return {
            loading: false,
            task: {},
            paperInfo: {},
            cropper: null,
            imgPreview: '',
            showPreview: false,
            issueLoading: false,
            adName: '',
            adList: [],
            viewerInstance: {
                destroy:() => {}
            },
            imgInfo: {},
            itemUrl: '',
            imageScale: null,
            options: [{
                value: '黑白',
                label: '黑白'
                }, {
                value: '彩色',
                label: '彩色'
                }, {
                value: '套红',
                label: '套红'
                }],
            fissuetype: '',
            sizeOpts:[{
                fcode: '1000',
                fsize: '统计录入'
                }],
            fsize:'',
        }
    },
    created: function (){
        this.getTask()
        this.sizeList()
    },
    computed:{
    },
    methods:{
        getTask: function () {
            this.loading = true
            this.task = {}
            this.paperInfo = {}
            this.imgPreview = ''
            this.showPreview = false
            this.issueLoading = false
            this.adName = ''
            this.adList = []
            this.viewerInstance = {destroy:() => {}}
            this.itemUrl = ''
            this.imgInfo = {}
            this.imageScale = null
            $.post().then((res) => {
                this.loading = false
                if (res.code == 0){
                    this.task = res.data
                    $.post(this.task.furl+'?imageInfo').then((res) => {
                        this.imgInfo = res
                    })
                    this.task.furl2 = this.task.furl + '?imageView2/2/w/1300'
                    this.cropperInit()
                    this.issueLoading = true
                    $.post('getIssueListBySourceId', {fid: this.task.fid}).then((issueRes) => {
                        this.issueLoading = false
                        this.adList = issueRes.data
                    })
                } else {
                    this.$message.error(res.msg)
                }
            })
        },
        changeImageWidth: function(val){
            let tempArr = this.task.furl.split('/')
            tempArr.pop()
            tempArr.push(val)
            this.task.furl = tempArr.join('/')
            // this.cropperInit()
        },
        loadError: function () {
            const arr = this.task.furl.split('?')
            // this.task.furl = arr[0]
            // this.cropperInit()
        },
        cropperInit: function () {
            this.$nextTick(() => {
                if (!this.cropper) {
                    const image = document.getElementById('image');
                    this.cropper = new Cropper(image, {
                        autoCropArea: 0.5,
                        viewMode:1,
                        dragMode: 'move',
                    });
                } else {
                    this.cropper.replace(this.task.furl)
                }

            })
        },
        viewerInit: function () {
            this.$nextTick(() => {
                try {
                    this.viewerInstance.destroy()
                    this.viewerInstance = new Viewer(document.querySelector('.previewWrapper img'), {
                        inline: true,
                        toolbar: false,
                        navbar: false
                    })
                }catch (e) {}
            })
        },
        openPreview: function () {
            /*
            预览时候显示的还是base64的压缩图片 this.imgPreview
            生成的任务图片是带参数的原图地址 this.itemUrl
             */
            this.imgPreview = this.cropper.getCroppedCanvas().toDataURL()
            let imgData = this.cropper.getData(true)
            console.log(this.imageScale)
            if(!this.imageScale){
                if (this.imgInfo.orientation && (this.imgInfo.orientation === 'Right-top' || this.imgInfo.orientation === 'Left-bottom')){
                    this.imageScale =  +this.imgInfo.height / this.cropper.getImageData().naturalWidth
                } else{
                    this.imageScale =  +this.imgInfo.width / this.cropper.getImageData().naturalWidth
                }
            }

            let width = Math.ceil(this.imageScale * imgData.width) + 20
            let height = Math.ceil(this.imageScale * imgData.height) + 20
            let x = Math.ceil(this.imageScale * imgData.x) - 10
            let y = Math.ceil(this.imageScale * imgData.y) - 10
            width = this.imgInfo.width < width ? this.imgInfo.width : width
            height = this.imgInfo.height < height ? this.imgInfo.height : height
            x = x > 0 ? x : 0
            y = y > 0 ? y : 0
            this.itemUrl = this.task.furl + `?imageMogr2/auto-orient/crop/!${width}x${height}a${x}a${y}`
            this.showPreview = true
            this.viewerInit()
        },
        closePreview: function () {
            this.showPreview = false
            this.adName = ''
            this.fissuetype = ''
            this.fsize = ''
        },
        addAd: function () {
            if (!this.adName){
                this.$alert('请输入广告名')
                return;
            }
            const item = {
                adName: this.adName,
                url: this.itemUrl,
                fsize:this.fsize,
                fissuetype:this.fissuetype,
            }
            this.addIssue(item)
            this.closePreview()
        },
        showAdInfo: function (item) {
            this.showPreview = item.url
            this.adName = item.adName
            this.showPreview = true
            this.viewerInit()
        },
        delItem: function (scope) {
            this.$confirm('确认删除?').then(() => {
                this.issueLoading = true
                $.post('delPaperIssue', {
                    issueId: scope.row.id
                }).then((res) => {
                    this.issueLoading = false
                    if (res.code == 0){
                        this.adList.splice(scope.$index, 1)
                    }else{
                        this.$message.error(res.msg)
                    }
                })
            }).catch(() => {})
        },
        cropperRotate: function () {
            this.cropper.rotate(90)
        },
        addIssue: function (issue) {
            this.issueLoading = true
            $.post('addPaperIssue', {issue: issue, sourceId: this.task.fid}).then((res) => {
                this.issueLoading = false
                if (res.code == 0) {
                    this.adList.unshift(issue)
                    const index = this.adList.indexOf(issue)
                    this.adList[index].id = res.issueId
                }else{
                    this.$message.error(res.msg)
                }
            })
        },
        submitSource: function () {
            this.$confirm('确认本版面广告全部剪辑完毕, 提交素材?').then(() => {
                this.loading = true
                $.post('submitCutTask', {sourceId: this.task.fid}).then((res) => {
                    this.loading = false
                    if (res.code == 0){
                        this.getTask()
                    } else{
                        this.$message.error(res.msg)
                    }
                })
            }).catch(() => {})
        },
        pageError: function () {
            this.$confirm('确认提交版面错误?').then(() => {
                this.loading = false
                $.post('pageError', {id: this.task.fid}).then((res) => {
                    this.loading = false
                    if(res.code == 0){
                        this.getTask()
                    }else {
                        this.$message.error(res.msg)
                    }
                })
            }).catch(() => {})
        },
        imageError: function () {
            this.$confirm('确认提交图片错误?').then(() => {
                this.loading = false
                $.post('imageError', {id: this.task.fid}).then((res) => {
                    this.loading = false
                    if(res.code == 0){
                        this.getTask()
                    }else {
                        this.$message.error(res.msg)
                    }
                })
            }).catch(() => {})
        },
        sizeList: function(){
            $.post('getPaperSizeList',{}).then((res)=>{
                if(res.code == 0){
                    this.sizeOpts = res.data
                }else {
                    this.$message.error(res.msg)
                }
            })
        },
    }
})