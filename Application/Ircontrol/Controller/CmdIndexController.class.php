<?php
namespace Ircontrol\Controller;
use Think\Controller;

class CmdIndexController extends Controller {
	
	
	
	/*红外控制*/
    public function sendCmdCopy(){
		session_write_close();
		$mediaNum = I('mediaNum');//媒体编号
		$cmdIndex = intval(I('cmdIndex'));//控制命令
		$device_num = I('device_num');//设备序列号
		$port_num = I('port_num');//控制器端口
		$powerONorOFF = I('powerONorOFF');//开关命令
		
		if($powerONorOFF == '0' ) $cmdIndex = 9;
		if($powerONorOFF == '1' ) $cmdIndex = 10;
		if(($device_num == '' || $port_num == '') && $mediaNum != ''){//判断媒介编号是否为空
			$mediaDevice = M('tmedia')
									->cache(true,60)
									->field('tmedia.fchannelid,tdevice.infrared_code')
									->join('tdevice on tdevice.fid = tmedia.fdeviceid')
									->where(array('tmedia.fdpid'=>$mediaNum))
									->find();
									
			$device_num = $mediaDevice['infrared_code'];//设备序列号
			$port_num = $mediaDevice['fchannelid'];//控制器端口
		}
		
		if($cmdIndex && $device_num && $port_num){
			$success = A('Common/Infrared','Model')->ircontrol($cmdIndex,$device_num,$port_num);
		}
		
		if ($success == true){
			$this->ajaxReturn(array('code'=>'200','message'=>'执行命令成功'));
		}else{
			$this->ajaxReturn(array('code'=>'400','message'=>'执行命令失败'));
		}

	}
	
	
	
	/*接收红外控制服务器请求*/
	public function ir_server(){
		
		
		$device_num = ConvertTextToHex(file_get_contents('php://input'));//设备序列号
		
		S('ir_online_'.$device_num,time(),600);//写入最后在线时间
		if(!S('ir_online_last_w_'.$device_num)){//判断是缓存里面有没有最后写入MYSQL最后在线时间的时间
			M('tdevice')->where(array('infrared_code'=>$device_num))->save(array('ir_last_time'=>time()));//最后在线时间写入数据库
			S('ir_online_last_w_'.$device_num,time(),600);//标记写入数据库最后在线时间的时间
		}
		
		$p1 = S('ir_'.$device_num.'_1');//一通道命令
		$p2 = S('ir_'.$device_num.'_2');//二通道命令
		$p3 = S('ir_'.$device_num.'_3');//三通道命令
		$p4 = S('ir_'.$device_num.'_4');//四通道命令
		
		S('ir_'.$device_num.'_1',null);//清空一通道命令
		S('ir_'.$device_num.'_2',null);//清空二通道命令
		S('ir_'.$device_num.'_3',null);//清空三通道命令
		S('ir_'.$device_num.'_4',null);//清空四通道命令
		$ret = '';
		if($p1 > 0 && $p1 <= 8){
			$ret .= chr(111).chr(0) . chr(1) . chr(0) . chr($p1);
		}elseif($p1 == 9){
			$ret .= chr(18).chr(0) . chr(0) . chr(0) . chr(1);
		}elseif($p1 == 10){
			$ret .= chr(17).chr(0) . chr(0) . chr(0) . chr(1);
		}
		
		if($p2 > 0 && $p2 <= 8){
			$ret .= chr(111).chr(0) . chr(2) . chr(0) . chr($p2);
		}elseif($p2 == 9){
			$ret .= chr(18).chr(0) . chr(0) . chr(0) . chr(2);
		}elseif($p2 == 10){
			$ret .= chr(17).chr(0) . chr(0) . chr(0) . chr(2);
		}
		
		if($p3 > 0 && $p3 <= 8){
			$ret .= chr(111).chr(0) . chr(3) . chr(0) . chr($p3);
		}elseif($p3 == 9){
			$ret .= chr(18).chr(0) . chr(0) . chr(0) . chr(3);
		}elseif($p3 == 10){
			$ret .= chr(17).chr(0) . chr(0) . chr(0) . chr(3);
		}
		
		if($p4 > 0 && $p4 <= 8){
			$ret .= chr(111).chr(0) . chr(4) . chr(0) . chr($p4);
		}elseif($p4 == 9){
			$ret .= chr(18).chr(0) . chr(0) . chr(0) . chr(4);
		}elseif($p4 == 10){
			$ret .= chr(17).chr(0) . chr(0) . chr(0) . chr(4);
		}
		
		

		
		//$ret = ConvertTextToHex($ret);
		echo $ret;
		
		
		
	}
	

	
	
}