<?php

namespace Common\Model;
use Think\Exception;



class ZongjuModel{


	/*通过媒介id和日期查询是否确认*/
	public function get_confirm($mediaId,$date){
		
		//var_dump($mediaId);
		//var_dump($date);
		
		$zongju_data_confirm_where = array();
		$zongju_data_confirm_where['fmonth'] = date('Y-m-01',strtotime($date));
		
		$zongju_data_confirm = M('zongju_data_confirm')->cache(true,60)->field('dmp_confirm_content,zongju_confirm_content')->where($zongju_data_confirm_where)->find();
		
		
		$dmp_confirm_content = json_decode($zongju_data_confirm['dmp_confirm_content'],true);//dmp确认数据
		$zongju_confirm_content = json_decode($zongju_data_confirm['zongju_confirm_content'],true);//总局确认数据
		
		$mediaInfo = M('tmedia')
								->cache(true,3600)
								->field('left(tmedia.fmediaclassid,2) as fmedia_class_code')
								->where(array('tmedia.fid'=>$mediaId))
								->find();//查询媒介信息
		
		if($mediaInfo['fmedia_class_code'] == '01') $ttab = 'tv';
		if($mediaInfo['fmedia_class_code'] == '02') $ttab = 'bc';
		if($mediaInfo['fmedia_class_code'] == '03') $ttab = 'paper';
		
		
		foreach($dmp_confirm_content[$ttab] as $dmpLL){
			
			if($dmpLL['date'] == $date && in_array($mediaId,$dmpLL['selected'])){
				$ret['dmp_confirm_state'] = true;
				continue;//结束本次循环
			}
			
		}
		
		foreach($zongju_confirm_content[$ttab] as $zongjuLL){
			if($zongjuLL['date'] == $date && in_array($mediaId,$zongjuLL['selected'])){
				$ret['zongju_confirm_state'] = true;
				continue;//结束本次循环
			}
			
		}
		
		
		return $ret;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

}