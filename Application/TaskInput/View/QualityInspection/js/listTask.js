Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}
Vue.use(VueViewer.default)
const vm = new Vue({
  el: "#app",
  data() {
    return {
      form: {
        pageIndex: 1,
        pageSize: 20,
        fpriority: [0,10]
      },
      loading: false,
      allcount: 0,
      tableData: [],
      disabedTime: '',
      markArr: [],
      isNetAd: '1',
      allMediaList: [],
      allMediaType: [],
      tableHeight: '400px',
      searchStatus: [
        {
          label: '全部',
          value: '-1'
        },{
          label: '待处理',
          value: '0'
        },{
          label: '处理完成',
          value: '2'
        },{
          label: '已传送',
          value: '4'
        },
      ],
      illegalTypeArr: [
        {
          text: '违法',
          value: '30',
          type: 'danger'
        },
        {
          text: '不违法',
          value: '0',
          type: 'success'
        },
      ],
      taskStateArr: [
        {
          text: '任务中',
          value: 1
        },
        {
          text: '录入任务中',
          value: 11
        },
        {
          text: '审核任务中',
          value: 21
        },
        {
          text: '已完成',
          value: 2
        },
        {
          text: '录入已完成',
          value: 12
        },
        {
          text: '审核已完成',
          value: 22
        },
        {
          text: '被退回',
          value: 4
        },
        {
          text: '录入被退回',
          value: 13
        },
        {
          text: '审核被退回',
          value: 23
        },
        {
          text: '等待撤回审核',
          value: 5
        },
      ],
      mediaClassArr: [
        {
        text: '电视',
        value: 1
        },
        {
          text: '广播',
          value: 2
        },
        {
          text: '报纸',
          value: 3
        },
        {
          text: '户外',
          value: 5
        },
        {
          text: '互联网',
          value: 13
        },
      ],
      qualityStateArr: [
        {
          text: '待质检',
          value: '0',
          type: 'primary'
        },
        {
          text: '质检中',
          value: '1',
          type: 'primary'
        },
        {
          text: '质检通过',
          value: '2',
          type: 'success'
        },
        {
          text: '退回录入',
          value: '3',
          type: 'danger'
        },
        {
          text: '退回审核',
          value: '4',
          type: 'danger'
        },
        {
          text: '退回剪辑',
          value: '5',
          type: 'danger'
        },
      ],
      fdstatus: {
        '0': {
          label: '待处理',
          type: 'danger'
        },
        '1': {
          label: '待处理',
          type: 'danger'
        },
        '2': {
          label: '处理完成',
          type: 'waring'
        },
        '4': {
          label: '已传送',
          type: 'success'
        },
      },
      show: true,
      detailDialogVisible: false,
      taskInfo: [],
      timeRangeArr: [],
      inputtimeRangeArr: [],
      judgetimeRangeArr: [],
      createTimeArr: [],
      multipleSelection: [],
      videoShow: true,
      swfShow: true,
      options: [
        {
          value: '10',
          label: '未完成',
          children:[
            {
              value: '0',
              label: '待质检',
            },
            {
              value: '1',
              label: '质检中',
            },
          ]
        },
        {
          value: '12',
          label: '完成',
          children:[
            {
              value: '2',
              label: '质检通过',
            },
            {
              value: '3',
              label: '退回录入',
            },
            {
              value: '4',
              label: '退回审核',
            },
            {
              value: '5',
              label: '退回剪辑',
            },
          ],
        },
      ],
      finspectstateArr: [],
    }
  },
  created() {
    this.getList()
    // this.getMediaList()
    // this.getMediaTypeList()
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight()
      document.querySelector('#app').style.opacity = 1
    })
    window.onresize = () => {
      this.getTableHeight()
    }
  },
  methods: {
    getList(page) {
      if(page) this.form.pageIndex = page
      let data = {
        ...this.form,
        finspecttime: this.timeRangeArr ? this.timeRangeArr.join(',') : '',
        finputtime: this.inputtimeRangeArr ? this.inputtimeRangeArr.join(',') : '',
        fjudgetime: this.judgetimeRangeArr ? this.judgetimeRangeArr.join(',') : '',
        fissue_date: this.createTimeArr ? this.createTimeArr.join(',') : '',
      }
      if(this.finspectstateArr.length > 0) {
        data.finspectstate = this.finspectstateArr[this.finspectstateArr.length - 1]
      }
      if(this.finspectstateArr.length == 1) {
        data.finspectstate = this.finspectstateArr[0]
      }
      this.loading = true
      $.post('/TaskInput/QualityInspection/listTask', JSON.stringify(data))
        .then(res => {
          if(res.code == 0){
            this.tableData = res.data
            this.allcount = res.count
            this.loading = false
          }
      })
    },
  // 重置
    resetForm(){
      this.timeRangeArr = [],
      this.inputtimeRangeArr = [],
      this.judgetimeRangeArr = [],
      this.createTimeArr = [],
      this.finspectstateArr = [],
      this.form = {
        pageIndex: 1,
        pageSize: 20,
        fpriority: [0,10]
      }
      this.getList(1)
    },
    handleSelectionChange(val) {
      this.multipleSelection = val
    },
    // 优先级
    modifyPriority() {
      if( this.multipleSelection.length == 0) {
        this.$message.error('请至少选择一行。')
      } else {
        const id = this.multipleSelection.map(i => i.fid)
        const fid = id.join()
        let data = {
          fid: fid,
        }
        this.$prompt('请输入优先级', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          inputPattern:  /^\d+(?=\.{0,1}\d+$|$)/,
          inputErrorMessage: '输入内容必须为数字'
        }).then(({ value }) => {
          data.fpriority = value
          $.post('/TaskInput/QualityInspection/setPriority', JSON.stringify(data))
            .then(res => {
              if (res.code === 0) {
                this.$notify({
                  message: res.msg,
                  type: 'success',
                  duration: 1000
                });
                this.getList()
              } else {
                this.$notify.error({
                  message: res.msg,
                  duration: 1000
                });
            }
          })
        }).catch(() => {
          this.$notify.info({
            title: '消息',
            message: '取消输入',
            duration: 1000
          });
        })
      }
    },
    // 详情弹窗
    openTaskInfo(row) {
      this.detailDialogVisible = true
      this.videoShow = true
      this.swfShow = true
      const data = {
        id : row.fid,
        fmediaclass: row.fmediaclass,
        taskid: row.taskid,
      }
      $.post('/TaskInput/QualityInspection/getTaskInfo', JSON.stringify(data))
        .then(res => {
          if(res.code == 0){
            this.taskInfo = res.data
          }
      })
    },
    // getMediaList(){
    //   $.post('/TaskInput/NetAd/getUserMediaList')
    //   .then(res=>{
    //     this.allMediaList = res.data
    //   })
    // },
    // 获取媒体分类列表
    // getMediaTypeList(){
    //   $.post('/TaskInput/NetAd/getmediatype')
    //   .then(res=>{
    //     this.allMediaType = res.data
    //   })
    // },
    closeDetail(){
      this.videoShow = false
      this.swfShow = false
    },
    handleSizeChange(val) {
      this.form.pageSize = val
      this.getList()
    },
    getTableHeight(){
      let formHeight = this.$refs.sform.$el.clientHeight
      this.tableHeight = document.body.clientHeight - formHeight - 60 + 'px'
    },
    reloadAdIFrame() {
      const tempUrl = this.$refs.adIframe.src
      this.$refs.adIframe.src = ''
      this.$refs.adIframe.src = tempUrl
    },
    /**
     * 数组过滤
     */
    filterQuality(value){
      const qualityState = this.qualityStateArr.filter((element)=>{
         return element.value == value
       })
       if(qualityState.length == 0) {
        qualityState.push({text: '暂无状态',type: 'info'})
       }
      return qualityState
    },
    filterIllegalType(value) {
      const illegalState = this.illegalTypeArr.filter((element)=> {
        return element.value == value
      })
      if(illegalState == 0) {
        illegalState.push({text: '暂无类型',type: 'info'})
      }
      return illegalState
    },
    filterMediaType(value) {
      value = value || 1
      return this.mediaClassArr.filter((element)=> {
        return element.value == value
      })
    },
  },
})