Vue.prototype.$ELEMENT = { size: 'mini', zIndex: 3000 }

var app = new Vue({
  name: 'app',
  el: '#app',
  data() {
    return {
      adData: DATA,
      ad: '1',
      info: '1',
      cardHeight: 'auto',
      bodyStyle: {
        padding: '10px',
        height: '100%',
      }
    }
  },
  mounted() {
    document.body.style.display = "block"
    this.$refs.image.$el.style.height = `${document.body.offsetHeight - 24}px`
    this.$refs.netIframe.style.height = `${document.body.offsetHeight - 104}px`
  },
})