<?php
namespace Media\Controller;
use Think\Controller;

class AdguideController extends BaseController {
    public function ad_guide(){

		$this->display();
		
	}
	//签发列表
    public function guide_send(){

    	$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录

		$fadname = I('fadname');//广告名称
		$fillegaltypecode = I('fillegaltypecode');//违法类型
		$fcreatetime_s = I('fcreatetime_s')?date('Y-m-d H:i:s',strtotime(I('fcreatetime_s'))):'';//开始时间
		$fcreatetime_e = I('fcreatetime_e')?date('Y-m-d H:i:s',strtotime(I('fcreatetime_e'))):'';//结束时间
		$fmediaclassid = I('ftype');//媒介类型

		$where['tillegaladflow.fstate'] = 1;//默认待处理
		$where['taddirect.fstate'] = 1;
		$where['taddirect.fdirectregulatorid'] = $_SESSION['regulatorpersonInfo']['fregulatorid'];//默认机构ID
		$where['tillegalad.fillegaltypecode'] = array('GT',0);//默认查询所有违法

		$where['tad.fadname'] = array('like','%'.trim($fadname,' ').'%');//广告名称查询
		if($fillegaltypecode) $where['tillegalad.fillegaltypecode'] = $fillegaltypecode;//违法code查询
		if(empty($fcreatetime_s) && $fcreatetime_e){
			$fcreatetime_s = date('Y-m-d 00:00:00',strtotime($fcreatetime_e)-86400*7);
		}elseif($fcreatetime_s && empty($fcreatetime_e)){
			$fcreatetime_e = date('Y-m-d H:i:s',time());
		}
		if($fcreatetime_s && $fcreatetime_e) $where['taddirect.fdirecttime'] = array(array('EGT',$fcreatetime_s),array('LT',$fcreatetime_e));//时间查询
		if($fmediaclassid) $where['tillegalad.fmediaclassid'] = array('in',$fmediaclassid);//媒介类型查询
		
		$count = M('tillegaladflow')
						->join('left join tillegalad on tillegalad.fillegaladid = tillegaladflow.fillegaladid')
						->join('left join taddirect on taddirect.fillegaladid = tillegalad.fillegaladid')
						->join('left join tad on tad.fadid = tillegalad.fadid')
						->join('left join tmedia on tmedia.fid = tillegalad.fmediaid')
						->join('left join tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('left join tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
						->where($where)
						->count();
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$guide_reply = M('tillegaladflow')
						->field('tillegaladflow.fillegaladflowid,tad.fadname,tmediaowner.fname,tmedia.fmedianame,tillegalad.fmediaclassid,tillegaltype.fillegaltype,taddirect.faddirectid,taddirect.fdirecttype,taddirect.fdirecttimes,tillegalad.fissuetimes,tillegalad.ffirstissuetime,tillegalad.flastissuetime')
						->join('left join tillegalad on tillegalad.fillegaladid = tillegaladflow.fillegaladid')
						->join('left join taddirect on taddirect.fillegaladid = tillegalad.fillegaladid')
						->join('left join tad on tad.fadid = tillegalad.fadid')
						->join('left join tmedia on tmedia.fid = tillegalad.fmediaid')
						->join('left join tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('left join tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
						->where($where)
						->limit($Page->firstRow.','.$Page->listRows)
						->select();	
		$guide_reply = list_k($guide_reply,$p,$pp);//获取k值

		$this->assign('guide_reply',$guide_reply);//签发列表
		$this->assign('page',$Page->show());//分页输出
		$this->display();
		
	}
	//签发操作
	public function guide_send_two(){

		$fillegaladflowid = I('fillegaladflowid');//流转ID
		$where['tillegaladflow.fillegaladflowid'] = $fillegaladflowid;
		//指导详情
		$guide_reply_detail = M('tillegaladflow')
						->field('taddirect.faddirectid,taddirect.fillegaladid,taddirect.fdirectcontent,tad.fadname,tmediaowner.fname,tmedia.fmedianame,tadclass.ffullname,tillegalad.fissuetimes,taddirect.fdirecttimes,tillegaltype.fillegaltype,tillegalad.fillegalcontent,tillegalad.fexpressions,tillegalad.fconfirmations,tillegalad.fmediaclassid,tillegalad.fsampleid')
						->join('tillegalad on tillegalad.fillegaladid = tillegaladflow.fillegaladid')
						->join('taddirect on taddirect.fillegaladid = tillegalad.fillegaladid')
						->join('tad on tad.fadid = tillegalad.fadid')
						->join('tadclass on tadclass.fcode = tad.fadclasscode')
						->join('tmedia on tmedia.fid = tillegalad.fmediaid')
						->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
						->where($where)
						->find();
		//根据媒介类型获取素材
		switch ($guide_reply_detail['fmediaclassid']) {
			case 'tv':
				$guide_reply_detail['favifilename']= M('ttvsample')->where(array('fid' => $guide_reply_detail['fsampleid']))->getField('favifilename');
				break;
			case 'bc':
				$guide_reply_detail['favifilename']= M('tbcsample')->where(array('fid' => $guide_reply_detail['fsampleid']))->getField('favifilename');
				break;
			case 'paper':
				$guide_reply_detail['fjpgfilename']= M('tpapersample')->where(array('fpapersampleid' => $guide_reply_detail['fsampleid']))->getField('fjpgfilename');
				break;
			default:
				
				break;
		}

		//发布记录
		if($guide_reply_detail){
			$issue_where = array();
			if($guide_reply_detail['fmediaclassid'] == 'tv'){
				$table = 'ttvissue';
				$field = 'tmedia.fmedianame,issue.fstarttime';
				$issue_where['ftvsampleid'] = $guide_reply_detail['fsampleid'];
			}elseif($guide_reply_detail['fmediaclassid'] == 'bc'){
				$table = 'tbcissue';
				$field = 'tmedia.fmedianame,issue.fstarttime';
				$issue_where['fbcsampleid'] = $guide_reply_detail['fsampleid'];
			}elseif($guide_reply_detail['fmediaclassid'] == 'paper'){
				$table = 'tpaperissue';
				$field = 'tmedia.fmedianame,issue.fpage';
				$issue_where['fpapersampleid'] = $guide_reply_detail['fsampleid'];
			}
			$issue_record = M($table)
								->alias('issue')
								->field($field)
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->where($issue_where)
								->limit(5)
								->order('issue.fissuedate desc')
								->select();
			foreach($issue_record as $key => $issue){
				$issue_record[$key]['k'] = $key + 1;
			}
		}
		//流转信息
		$flowInfo = M('tillegaladflow')->where(array('fillegaladid' => $guide_reply_detail['fillegaladid']))->limit(5)->select();
		$guide_reply_detail['fdirectcontent'] = html_entity_decode($guide_reply_detail['fdirectcontent']);//转换html标签
		//附件信息
		$attach = M('tillegaladattach')->where(array('fillegaladid' => $guide_reply_detail['fillegaladid']))->select();

		$this->assign('fmediaclassid',$guide_reply_detail['fmediaclassid']);//媒介类型
		$this->assign('issue_record',$issue_record);//发布记录
		$this->assign('guide_reply_detail',$guide_reply_detail);//指导详情
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->assign('attach',$attach);//附件
		$this->assign('flowInfo',$flowInfo);//流转信息
		$this->display();

	}
	//指导签发
	public function ajax_guide_send(){
		$faddirectid = I('faddirectid');//指导ID
		$fillegaladflowid = I('fillegaladflowid');//流转ID
		$attachinfo = I('attachinfo');//附件参数
		$fdirectcontent = I('fdirectcontent');//指导文书
		$addirect = M('taddirect')->where(array('faddirectid' => $faddirectid))->find();//指导详情

		$data['fstate'] = 2;//状态值
		$data['fdirectcontent'] = $fdirectcontent;//指导文书
		$data['fsign'] = session('regulatorpersonInfo.fname');
		$data['fsigntime'] = date('Y-m-d H:i:s',time());
		$result = M('taddirect')->where(array('faddirectid' => $faddirectid))->save($data);//更改指导状态并添加签发人信息
		
		//更改上一条流转信息
		$flow_data['fregulatorpersonid'] = session('regulatorpersonInfo.fid');
		$flow_data['fregulatorpersonname'] = session('regulatorpersonInfo.fname');
		$flow_data['ffinishtime'] = date('Y-m-d H:i:s',time());
		$flow_data['fstate'] = 2;
		M('tillegaladflow')->where(array('fillegaladflowid' => $fillegaladflowid,'state' => 1))->save($flow_data);
		//流转信息
		$add_data['fillegaladid'] = $addirect['fillegaladid'];
		$add_data['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');		
		$add_data['fcreateregualtorname'] = session('regulatorpersonInfo.regulatorname');
		$add_data['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
		$add_data['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
		$add_data['fcreatetime'] = date('Y-m-d H:i:s',time());
		// $add_data['fcreateinfo'] = '签发';
		$add_data['fbizname'] = '广告指导';
		$add_data['fflowname'] = '指导签收';
		$add_data['fregulatorid'] = session('regulatorpersonInfo.fregulatorid');
		$add_data['fregulatorname'] = session('regulatorpersonInfo.regulatorname');
		$add_data['fstate'] = 1;
		$fillegaladflowid = M('tillegaladflow')->add($add_data);//添加签发流转信息

		if($attachinfo){
			//附件信息
			foreach ($attachinfo as $key => $value) {
				if($value['id'] == '') $attachlist[] = $value;
			}
			if($attachlist){
				$attach_data['fillegaladid'] = $addirect['fillegaladid'];
				$attach_data['fillegaladflowid'] = $fillegaladflowid;
				$attach_data['fuploadtime'] = date('Y-m-d H:i:s',time());
				$attach_data['fattachuser'] = 1;
				foreach ($attachlist as $key => $value) {
					$attach_data['fattachname'] = $value['name'];
					$attach_data['fattachurl'] = $value['url'];
					$attach_data['ffilename'] = preg_replace('/\..*/','',$value['name']);
					$attach_data['ffiletype'] = preg_replace('/.*\./','',$value['url']);
					$attach[$key] = $attach_data;
				}	
				$attachid = M('tillegaladattach')->addAll($attach);
				if(empty($attachid)) $this->ajaxReturn(array('code'=>-1,'msg'=>'附件添加失败'));
			}
		}

		if($result && $fillegaladflowid){
			$this->ajaxReturn(array('code'=>0,'msg'=>'签发成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'签发失败'));
		}

	}
	//指导退回
	public function ajax_guide_return(){

		$faddirectid = I('faddirectid');//指导ID
		$fillegaladflowid = I('fillegaladflowid');//流转ID
		$fcreateinfo = I('fcreateinfo');//回退信息
		$attachinfo = I('attachinfo');//附件参数
		$fdirectcontent = I('fdirectcontent');//指导文书

		$addirect = M('taddirect')->where(array('faddirectid' => $faddirectid))->find();//指导详情

		$data['fstate'] = 0;
		$data['fdirectcontent'] = $fdirectcontent;
		$result = M('taddirect')->where(array('faddirectid' => $faddirectid))->save($data);//更改指导状态并添加签发人信息

		/*更改违法广告表*/
		$arr['fmodifier'] = session('regulatorpersonInfo.fid');
		$arr['fmodifytime'] = date('Y-m-d H:i:s');
		$arr['fstate'] = 0;
		$res = M("tillegalad")->where(array('fillegaladid' => $addirect['fillegaladid']))->save($arr);

		//更改上一条流转信息
		$flow_data['fregulatorpersonid'] = session('regulatorpersonInfo.fid');
		$flow_data['fregulatorpersonname'] = session('regulatorpersonInfo.fname');
		$flow_data['ffinishtime'] = date('Y-m-d H:i:s',time());
		$flow_data['fstate'] = 2;
		M('tillegaladflow')->where(array('fillegaladflowid' => $fillegaladflowid,'state' => 1))->save($flow_data);
		//流转信息
		$add_data['fillegaladid'] = $addirect['fillegaladid'];
		$add_data['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');		
		$add_data['fcreateregualtorname'] = session('regulatorpersonInfo.regulatorname');
		$add_data['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
		$add_data['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
		$add_data['fcreatetime'] = date('Y-m-d H:i:s',time());
		$add_data['fcreateinfo'] = $fcreateinfo;
		$add_data['fbizname'] = '广告指导';
		$add_data['fflowname'] = '广告指导';
		$add_data['fregulatorid'] = session('regulatorpersonInfo.fregulatorid');
		$add_data['fregulatorname'] = session('regulatorpersonInfo.regulatorname');
		$add_data['fstate'] = 1;
		$fillegaladflowid = M('tillegaladflow')->add($add_data);//添加回退流转信息

		if($attachinfo){
			//附件信息
			foreach ($attachinfo as $key => $value) {
				if($value['id'] == '') $attachlist[] = $value;
			}
			if($attachlist){
				$attach_data['fillegaladid'] = $addirect['fillegaladid'];
				$attach_data['fillegaladflowid'] = $fillegaladflowid;
				$attach_data['fuploadtime'] = date('Y-m-d H:i:s',time());
				$attach_data['fattachuser'] = 1;
				foreach ($attachlist as $key => $value) {
					$attach_data['fattachname'] = $value['name'];
					$attach_data['fattachurl'] = $value['url'];
					$attach_data['ffilename'] = preg_replace('/\..*/','',$value['name']);
					$attach_data['ffiletype'] = preg_replace('/.*\./','',$value['url']);
					$attach[$key] = $attach_data;
				}	
				$attachid = M('tillegaladattach')->addAll($attach);
				if(empty($attachid)) $this->ajaxReturn(array('code'=>-1,'msg'=>'附件添加失败'));
			}
		}

		if($result && $fillegaladflowid){
			$this->ajaxReturn(array('code'=>0,'msg'=>'退回成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'退回失败'));
		}

	}
	//指导保存
	public function ajax_guide_keep(){

		$faddirectid = I('faddirectid');//指导ID
		$fillegaladflowid = I('fillegaladflowid');//流转ID
		$attachinfo = I('attachinfo');//附件参数
		$fdirectcontent = I('fdirectcontent');//文书
		$addirect = M('taddirect')->where(array('faddirectid' => $faddirectid))->find();//指导详情
		$data = array();
		$data['fdirectcontent'] = $fdirectcontent;
		$result = M('taddirect')->where(array('faddirectid' => $faddirectid))->save($data);//添加签发人信息

		if($attachinfo){
			//附件信息
			foreach ($attachinfo as $key => $value) {
				if($value['id'] == '') $attachlist[] = $value;
			}
			if($attachlist){
				$attach_data['fillegaladid'] = $addirect['fillegaladid'];
				$attach_data['fillegaladflowid'] = $fillegaladflowid;
				$attach_data['fuploadtime'] = date('Y-m-d H:i:s',time());
				$attach_data['fattachuser'] = 1;
				foreach ($attachlist as $key => $value) {
					$attach_data['fattachname'] = $value['name'];
					$attach_data['fattachurl'] = $value['url'];
					$attach_data['ffilename'] = preg_replace('/\..*/','',$value['name']);
					$attach_data['ffiletype'] = preg_replace('/.*\./','',$value['url']);
					$attach[$key] = $attach_data;
				}	
				$attachid = M('tillegaladattach')->addAll($attach);
				if(empty($attachid)) $this->ajaxReturn(array('code'=>-1,'msg'=>'附件添加失败'));
			}
		}

		if($result){
			$this->ajaxReturn(array('code'=>0,'msg'=>'保存成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'保存失败'));
		}

	}
	//删除附件
	public function ajax_delete_attach(){

		$fillegaladattachid = I('fillegaladattachid');
		if($fillegaladattachid) M('tillegaladattach')->where(array('fillegaladattachid' => $fillegaladattachid))->delete();
		$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));

	}
	//反馈列表
    public function guide_reply(){
    	
    	$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录

		$fadname = I('fadname');//广告名称
		$fillegaltypecode = I('fillegaltypecode');//违法类型
		$fcreatetime_s = I('fcreatetime_s')?date('Y-m-d H:i:s',strtotime(I('fcreatetime_s'))):'';//开始时间
		$fcreatetime_e = I('fcreatetime_e')?date('Y-m-d H:i:s',strtotime(I('fcreatetime_e'))):'';//结束时间
		$fmediaclassid = I('ftype');//媒介类型

		$where['tillegaladflow.fstate'] = 1;
		$where['taddirect.fstate'] = array('in',array(2,3));
		$where['taddirect.fdirectregulatorid'] = $_SESSION['regulatorpersonInfo']['fregulatorid'];//默认机构ID
		$where['tillegalad.fillegaltypecode'] = array('GT',0);//默认查询所有违法

		$where['tad.fadname'] = array('like','%'.trim($fadname,' ').'%');//广告名称查询
		if($fillegaltypecode) $where['tillegalad.fillegaltypecode'] = $fillegaltypecode;//违法code查询
		if(empty($fcreatetime_s) && $fcreatetime_e){
			$fcreatetime_s = date('Y-m-d 00:00:00',strtotime($fcreatetime_e)-86400*7);
		}elseif($fcreatetime_s && empty($fcreatetime_e)){
			$fcreatetime_e = date('Y-m-d H:i:s',time());
		}
		if($fcreatetime_s && $fcreatetime_e) $where['taddirect.fdirecttime'] = array(array('EGT',$fcreatetime_s),array('LT',$fcreatetime_e));//时间查询
		if($fmediaclassid) $where['tillegalad.fmediaclassid'] = array('in',$fmediaclassid);//媒介类型查询
    	
    	$count = M('tillegaladflow')
							->join('left join tillegalad on tillegalad.fillegaladid = tillegaladflow.fillegaladid')
							->join('left join taddirect on taddirect.fillegaladid = tillegalad.fillegaladid')
							->join('left join tad on tad.fadid = tillegalad.fadid')
							->join('left join tadclass on tadclass.fcode = tad.fadclasscode')
							->join('left join tmedia on tmedia.fid = tillegalad.fmediaid')
							->join('left join tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->join('left join tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
							->where($where)
							->count();
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$guide_reply = M('tillegaladflow')
							->field('taddirect.faddirectid,taddirect.fillegaladid,tad.fadname,tadclass.ffullname,tmediaowner.fname,tmedia.fmedianame,tillegalad.fmediaclassid,tillegaltype.fillegaltype,taddirect.fsigntime,taddirect.fdirecttype,taddirect.fstate,tillegalad.fillegaladid')
							->join('left join tillegalad on tillegalad.fillegaladid = tillegaladflow.fillegaladid')
							->join('left join taddirect on taddirect.fillegaladid = tillegalad.fillegaladid')
							->join('left join tad on tad.fadid = tillegalad.fadid')
							->join('left join tadclass on tadclass.fcode = tad.fadclasscode')
							->join('left join tmedia on tmedia.fid = tillegalad.fmediaid')
							->join('left join tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->join('left join tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
							->where($where)
							->limit($Page->firstRow.','.$Page->listRows)
							->order('taddirect.fsigntime desc')
							->select();
		$guide_reply = list_k($guide_reply,$p,$pp);
		
		$this->assign('guide_reply',$guide_reply);
		$this->assign('page',$Page->show());//分页输出
		$this->display();
		
	}
	//反馈详情
	public function guide_reply_two(){

		$fillegaladid = I('fillegaladid');//违法广告ID
		$faddirectid = I('faddirectid');
		$fstate = M('taddirect')->where(['faddirectid' => $faddirectid])->getField('fstate');
		if($fstate == 2){
			//更改为待反馈状态
			$data['fsignin'] = session('regulatorpersonInfo.fname');
			$data['fsignintime'] = date('Y-m-d H:i:s',time());
			$data['fstate'] = 3;
			M('taddirect')->where(array('fillegaladid' => $fillegaladid))->save($data);

			//添加修改流程信息
			$flow_data['fregulatorpersonid'] = session('regulatorpersonInfo.fid');
			$flow_data['fregulatorpersonname'] = session('regulatorpersonInfo.fname');
			$flow_data['ffinishtime'] = date('Y-m-d H:i:s',time());
			$flow_data['fstate'] = 2;
			M('tillegaladflow')->where(array('fillegaladid' => $fillegaladid,'state' => 1))->save($flow_data);

			$add_data['fillegaladid'] = $fillegaladid;
			$add_data['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');		
			$add_data['fcreateregualtorname'] = session('regulatorpersonInfo.regulatorname');
			$add_data['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
			$add_data['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
			$add_data['fcreatetime'] = date('Y-m-d H:i:s',time());
			$add_data['fbizname'] = '广告指导';
			$add_data['fflowname'] = '指导反馈';
			$add_data['fregulatorid'] = session('regulatorpersonInfo.fregulatorid');
			$add_data['fregulatorname'] = session('regulatorpersonInfo.regulatorname');
			$add_data['fstate'] = 1;
			$fillegaladflowid = M('tillegaladflow')->add($add_data);//添加回退流转信息

		}
		
		$where['tillegaladflow.fillegaladid'] = $fillegaladid;
		$where['tillegaladflow.fstate'] = 1;		
		$guide_reply_detail = M('tillegaladflow')
						->field('taddirect.*,tad.fadname,tmediaowner.fname,tmedia.fmedianame,tadclass.ffullname,tillegalad.fissuetimes,tillegaltype.fillegaltype,tillegalad.fillegalcontent,tillegalad.fexpressions,tillegalad.fconfirmations,tillegalad.fmediaclassid,tillegalad.fmediaid,tillegalad.fsampleid,tillegaladflow.fillegaladflowid')
						->join('tillegalad on tillegalad.fillegaladid = tillegaladflow.fillegaladid')
						->join('taddirect on taddirect.fillegaladid = tillegalad.fillegaladid')
						->join('tad on tad.fadid = tillegalad.fadid')
						->join('tadclass on tadclass.fcode = tad.fadclasscode')
						->join('tmedia on tmedia.fid = tillegalad.fmediaid')
						->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
						->where($where)
						->find();
		switch ($guide_reply_detail['fmediaclassid']) {
			case 'tv':
				$guide_reply_detail['favifilename']= M('ttvsample')->where(array('fid' => $guide_reply_detail['fsampleid']))->getField('favifilename');
				break;
			case 'bc':
				$guide_reply_detail['favifilename']= M('tbcsample')->where(array('fid' => $guide_reply_detail['fsampleid']))->getField('favifilename');
				break;
			case 'paper':
				$guide_reply_detail['fjpgfilename']= M('tpapersample')->where(array('fpapersampleid' => $guide_reply_detail['fsampleid']))->getField('fjpgfilename');
				break;
			default:
				
				break;
		}

		if($guide_reply_detail){
			$issue_where = array();
			if($guide_reply_detail['fmediaclassid'] == 'tv'){
				$table = 'ttvissue';
				$field = 'tmedia.fmedianame,issue.fstarttime';
				$issue_where['ftvsampleid'] = $guide_reply_detail['fsampleid'];
			}elseif($guide_reply_detail['fmediaclassid'] == 'bc'){
				$table = 'tbcissue';
				$field = 'tmedia.fmedianame,issue.fstarttime';
				$issue_where['fbcsampleid'] = $guide_reply_detail['fsampleid'];
			}elseif($guide_reply_detail['fmediaclassid'] == 'paper'){
				$table = 'tpaperissue';
				$field = 'tmedia.fmedianame,issue.fpage';
				$issue_where['fpapersampleid'] = $guide_reply_detail['fsampleid'];
			}
			$issue_record = M($table)
								->alias('issue')
								->field($field)
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->where($issue_where)
								->limit(5)
								->order('issue.fissuedate desc')
								->select();
			foreach($issue_record as $key => $issue){
				$issue_record[$key]['k'] = $key + 1;
			}
		}
		//指导记录
		$flow_where['tillegalad.fmediaid'] = $guide_reply_detail['fmediaid'];
		$flow_where['tillegalad.fsampleid'] = $guide_reply_detail['fsampleid'];
		$flow_where['tillegaladflow.fflowname'] = '指导反馈';
		$flow_where['tillegaladflow.fillegaladflowid'] = array('NEQ',$guide_reply_detail['fillegaladflowid']);
		$flow_where['taddirect.fstate'] = 9;
		$guide_record = M('tillegaladflow')
							->field('taddirect.fdirecttime,taddirect.ffeedbacktime')
							->join('tillegalad on tillegalad.fillegaladid = tillegaladflow.fillegaladid')
							->join('taddirect on taddirect.fillegaladid = tillegalad.fillegaladid')
							->where($flow_where)->limit(5)->select();
		//流转信息
		$flowInfo = M('tillegaladflow')->where(array('fillegaladid' => $guide_reply_detail['fillegaladid']))->limit(5)->select();
		$guide_reply_detail['fdirectcontent'] = html_entity_decode($guide_reply_detail['fdirectcontent']);
		//附件信息
		$attach = M('tillegaladattach')->where(array('fillegaladid' => $guide_reply_detail['fillegaladid']))->select();

		$this->assign('fmediaclassid',$guide_reply_detail['fmediaclassid']);//媒介类型
		$this->assign('issue_record',$issue_record);//发布记录
		$this->assign('guide_reply_detail',$guide_reply_detail);//反馈详情
		$this->assign('attach',$attach);//附件
		$this->assign('guide_record',$guide_record);
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->display();

	}

	// 提交反馈信息
	public function ajax_submit_reply(){

		$fillegaladid = I('fillegaladid');//违法广告ID
		$fillegaladflowid = I('fillegaladflowid');//流转ID
		$faddirectid = I('faddirectid');//指导ID
		$ffeedbackresult = I('ffeedbackresult');//反馈结果
		$attachinfo = I('attachinfo');//附件信息
		$data['ffeedback'] = session('regulatorpersonInfo.fname');
		$data['ffeedbacktime'] = date('Y-m-d H:i:s',time());
		$data['ffeedbackresult'] = $ffeedbackresult;
		$data['fstate'] = 9;
		if(empty($ffeedbackresult)) $this->ajaxReturn(array('code'=>-1,'msg'=>'反馈信息不能为空'));
		$result = M('taddirect')->where(array('faddirectid' => $faddirectid))->save($data);//更新为已反馈

		//修改流程信息
		$flow_data['fcreateinfo'] = $ffeedbackresult;
		$flow_data['fregulatorpersonid'] = session('regulatorpersonInfo.fid');
		$flow_data['fregulatorpersonname'] = session('regulatorpersonInfo.fname');
		$flow_data['ffinishtime'] = date('Y-m-d H:i:s',time());
		$flow_data['fstate'] = 2;
		M('tillegaladflow')->where(array('fillegaladflowid' => $fillegaladflowid,'fstate' => 1))->save($flow_data);

		if($attachinfo){
			//附件信息
			foreach ($attachinfo as $key => $value) {
				if($value['id'] == '') $attachlist[] = $value;
			}
			if($attachlist){
				$attach_data['fillegaladid'] = $fillegaladid;
				$attach_data['fillegaladflowid'] = $fillegaladflowid;
				$attach_data['fuploadtime'] = date('Y-m-d H:i:s',time());
				$attach_data['fattachuser'] = 1;
				foreach ($attachlist as $key => $value) {
					$attach_data['fattachname'] = $value['name'];
					$attach_data['fattachurl'] = $value['url'];
					$attach_data['ffilename'] = preg_replace('/\..*/','',$value['name']);
					$attach_data['ffiletype'] = preg_replace('/.*\./','',$value['url']);
					$attach[$key] = $attach_data;
				}	
				$attachid = M('tillegaladattach')->addAll($attach);
				if(empty($attachid)) $this->ajaxReturn(array('code'=>-1,'msg'=>'附件添加失败'));
			}
		}

		if($result && $fillegaladflowid){
			$this->ajaxReturn(array('code'=>0,'msg'=>'提交成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'提交失败'));
		}

	}

	//指导台账
    public function guide_account(){

    	$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录

		$fadname = I('fadname');//广告名称
		$fillegaltypecode = I('fillegaltypecode');//违法类型
		$fcreatetime_s = I('fcreatetime_s')?date('Y-m-d H:i:s',strtotime(I('fcreatetime_s'))):'';//开始时间
		$fcreatetime_e = I('fcreatetime_e')?date('Y-m-d H:i:s',strtotime(I('fcreatetime_e'))):'';//结束时间
		$fmediaclassid = I('ftype');//媒介类型
		$fstate = I('fstate');//指导状态
		
		$where['tillegaladflow.fstate'] = 1;
		$where['taddirect.fstate'] = array('GT',0);
		$where['taddirect.fdirectregulatorid'] = $_SESSION['regulatorpersonInfo']['fregulatorid'];//默认机构ID
		$where['tillegalad.fillegaltypecode'] = array('GT',0);//默认查询所有违法
		if($fstate == 9){
			$where['tillegaladflow.fstate'] = 2;
			$where['tillegaladflow.fflowname'] = '指导反馈';
		} 

		if($fstate) $where['taddirect.fstate'] = $fstate;
		$where['tad.fadname'] = array('like','%'.trim($fadname,' ').'%');//广告名称查询
		if($fillegaltypecode) $where['tillegalad.fillegaltypecode'] = $fillegaltypecode;//违法code查询
		if(empty($fcreatetime_s) && $fcreatetime_e){
			$fcreatetime_s = date('Y-m-d 00:00:00',strtotime($fcreatetime_e)-86400*7);
		}elseif($fcreatetime_s && empty($fcreatetime_e)){
			$fcreatetime_e = date('Y-m-d H:i:s',time());
		}
		if($fcreatetime_s && $fcreatetime_e) $where['taddirect.fdirecttime'] = array(array('EGT',$fcreatetime_s),array('LT',$fcreatetime_e));//时间查询
		if($fmediaclassid) $where['tillegalad.fmediaclassid'] = array('in',$fmediaclassid);//媒介类型查询
    	
    	$count = M('tillegaladflow')
							->join('left join tillegalad on tillegalad.fillegaladid = tillegaladflow.fillegaladid')
							->join('left join taddirect on taddirect.fillegaladid = tillegalad.fillegaladid')
							->join('left join tad on tad.fadid = tillegalad.fadid')
							->join('left join tadclass on tadclass.fcode = tad.fadclasscode')
							->join('left join tmedia on tmedia.fid = tillegalad.fmediaid')
							->join('left join tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->join('left join tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
							->where($where)
							->count();
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$guide_account = M('tillegaladflow')
							->field('taddirect.faddirectid,taddirect.fillegaladid,tad.fadname,tadclass.ffullname,tmediaowner.fname,tmedia.fmedianame,tillegalad.fmediaclassid,tillegalad.fillegalcontent,tillegaltype.fillegaltype,taddirect.fdirecttime,taddirect.fsigntime,taddirect.ffeedbacktime,taddirect.fdirecttype,taddirect.fstate,tillegaladflow.fillegaladflowid')
							->join('left join tillegalad on tillegalad.fillegaladid = tillegaladflow.fillegaladid')
							->join('left join taddirect on taddirect.fillegaladid = tillegalad.fillegaladid')
							->join('left join tad on tad.fadid = tillegalad.fadid')
							->join('left join tadclass on tadclass.fcode = tad.fadclasscode')
							->join('left join tmedia on tmedia.fid = tillegalad.fmediaid')
							->join('left join tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
							->join('left join tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
							->where($where)
							->limit($Page->firstRow.','.$Page->listRows)
							->order('taddirect.fsigntime desc')
							->select();
		$guide_account = list_k($guide_account,$p,$pp);

		$this->assign('guide_account',$guide_account);
		$this->assign('page',$Page->show());//分页输出
		$this->display();
		
	}

	//台账详情
	public function guide_account_two(){

		$fillegaladflowid = I('fillegaladflowid');
		$where['tillegaladflow.fillegaladflowid'] = $fillegaladflowid;
		$guide_reply_detail = M('tillegaladflow')
						->field('taddirect.*,tad.fadname,tmediaowner.fname,tmedia.fmedianame,tadclass.ffullname,tillegalad.fissuetimes,tillegaltype.fillegaltype,tillegalad.fillegalcontent,tillegalad.fexpressions,tillegalad.fconfirmations,tillegalad.fmediaclassid,tillegalad.fmediaid,tillegalad.fsampleid,tillegaladflow.fillegaladflowid')
						->join('tillegalad on tillegalad.fillegaladid = tillegaladflow.fillegaladid')
						->join('taddirect on taddirect.fillegaladid = tillegalad.fillegaladid')
						->join('tad on tad.fadid = tillegalad.fadid')
						->join('tadclass on tadclass.fcode = tad.fadclasscode')
						->join('tmedia on tmedia.fid = tillegalad.fmediaid')
						->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode')
						->where($where)
						->find();
		switch ($guide_reply_detail['fmediaclassid']) {
			case 'tv':
				$guide_reply_detail['favifilename']= M('ttvsample')->where(array('fid' => $guide_reply_detail['fsampleid']))->getField('favifilename');
				break;
			case 'bc':
				$guide_reply_detail['favifilename']= M('tbcsample')->where(array('fid' => $guide_reply_detail['fsampleid']))->getField('favifilename');
				break;
			case 'paper':
				$guide_reply_detail['fjpgfilename']= M('tpapersample')->where(array('fpapersampleid' => $guide_reply_detail['fsampleid']))->getField('fjpgfilename');
				break;
			default:
				
				break;
		}

		if($guide_reply_detail){
			$issue_where = array();
			if($guide_reply_detail['fmediaclassid'] == 'tv'){
				$table = 'ttvissue';
				$field = 'tmedia.fmedianame,issue.fstarttime';
				$issue_where['ftvsampleid'] = $guide_reply_detail['fsampleid'];
			}elseif($guide_reply_detail['fmediaclassid'] == 'bc'){
				$table = 'tbcissue';
				$field = 'tmedia.fmedianame,issue.fstarttime';
				$issue_where['fbcsampleid'] = $guide_reply_detail['fsampleid'];
			}elseif($guide_reply_detail['fmediaclassid'] == 'paper'){
				$table = 'tpaperissue';
				$field = 'tmedia.fmedianame,issue.fpage';
				$issue_where['fpapersampleid'] = $guide_reply_detail['fsampleid'];
			}
			$issue_record = M($table)
								->alias('issue')
								->field($field)
								->join('tmedia on tmedia.fid = issue.fmediaid')
								->where($issue_where)
								->limit(5)
								->order('issue.fissuedate desc')
								->select();
			foreach($issue_record as $key => $issue){
				$issue_record[$key]['k'] = $key + 1;
			}
		}
		//指导记录
		$flow_where['tillegalad.fmediaid'] = $guide_reply_detail['fmediaid'];
		$flow_where['tillegalad.fsampleid'] = $guide_reply_detail['fsampleid'];
		$flow_where['tillegaladflow.fflowname'] = '指导反馈';
		$flow_where['tillegaladflow.fillegaladflowid'] = array('NEQ',$guide_reply_detail['fillegaladflowid']);
		$flow_where['taddirect.fstate'] = 9;
		$guide_record = M('tillegaladflow')
							->field('taddirect.fdirecttime,taddirect.ffeedbacktime')
							->join('tillegalad on tillegalad.fillegaladid = tillegaladflow.fillegaladid')
							->join('taddirect on taddirect.fillegaladid = tillegalad.fillegaladid')
							->where($flow_where)->limit(5)->select();
		$guide_reply_detail['fdirectcontent'] = html_entity_decode($guide_reply_detail['fdirectcontent']);
		$guide_reply_detail['ffeedbackresult'] = html_entity_decode($guide_reply_detail['ffeedbackresult']);
		//附件信息
		$attach = M('tillegaladattach')->where(array('fillegaladid' => $guide_reply_detail['fillegaladid']))->select();

		$this->assign('fmediaclassid',$guide_reply_detail['fmediaclassid']);//媒介类型
		$this->assign('issue_record',$issue_record);//发布记录
		$this->assign('guide_reply_detail',$guide_reply_detail);//反馈详情
		$this->assign('attach',$attach);//附件
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->assign('guide_record',$guide_record);
		$this->display();

	}
    public function guide_copy(){

		$this->display();
		
	}
	
}