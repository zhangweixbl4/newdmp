<?php
//报纸广告发行控制器
namespace Admin\Controller;
use Think\Controller;
class PaperissueController extends BaseController {

    public function index($data = []){
    	if(A('Admin/Authority','Model')->authority('200161') === 0){
			exit('没有权限');//验证是否有新增权限
		}

		if(!empty($data)){
			$_GET = $data;
		}
		
		if(I('region_id') != '') $region_id = I('region_id');
		$outtype = I('outtype');//导出类型
		if(!empty($outtype)){
			$p = 1;
			$pp = 999999;
			$addFields = '
				,tadowner.fname as fadownername
				,left(tmedia.fmediaclassid,2) mclass
				,tpapersample.fjpgfilename ysscurl
				,tpapersample.fexpressions
				,tpapersample.fillegalcontent
				,tpapersample.fexpressioncodes
				,DATE_FORMAT(tpaperissue.fissuedate,"%Y-%m-%d") issuedate
			';
		}else{
			$p = I('p',1);//当前第几页
			$pp = I('pageSize', 10);//每页显示多少记录
		}
		$keyword = I('keyword');//搜索关键词
		$fadname = I('fadname');// 广告名称
		$fversion = I('fversion');// 版本描述
		$fmediaid  = I('fmediaid');// 媒体ID
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID
		$fstarttime_s = I('fstarttime_s');// 发布日期
		if($fstarttime_s == '') $fstarttime_s = '2015-01-01';
		$fstarttime_e = I('fstarttime_e');// 发布日期
		if($fstarttime_e == '') $fstarttime_e = date('Y-m-d');
		$fcreator = I('fcreator');//创建人
		$sample_id = I('sample_id');
		$merge_media = I('merge_media');//是否合并媒介
		$medialabel = I('medialabel');//媒介标签
		$this_region_id = I('this_region_id');//是否只查询本级地区
		$adclass_code = I('adclass_code');//广告类别

		$where = array();//查询条件
		if(intval($sample_id) > 0){
			$where['tpaperissue.fpapersampleid'] = $sample_id;
		}
		if($region_id > 0 && $fmediaid == ''){
			if($this_region_id == 'true'){
				$where['tmediaowner.fregionid'] = $region_id;//地区搜索条件
			}elseif($region_id != 100000){
				$region_id_rtrim = A('Common/System','Model')->get_region_left($region_id);//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmediaowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}
		}


		$where['tpaperissue.fstate'] = array('neq',-1);
		
		if($fcreator != ''){
			$where['tpaperissue.fcreator'] = array('like','%'.$fcreator.'%');
		}
		if($keyword != ''){
			
		} 
	
		if($fadname != ''){
			$where['tad.fadname'] = array('like','%'.$fadname.'%');//广告名称
		}
		if($adclass_code != '' && $adclass_code != '0'){
			
			$adclass_code_strlen = strlen($adclass_code);//获取code长度
			
			$where['tpapersample.fadid'] = array('exp',"in (select fadid from tad where left(tad.fadclasscode,".$adclass_code_strlen.") = '".$adclass_code."')");//广告id
		}
		if($fversion != ''){
			$where['tpapersample.fversion'] = array('like','%'.$fversion.'%');//版本描述
		}
		
		if($medialabel != ''){
			$mediaIdList = M('tmedialabel')
										->cache(true,60)
										->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
										->where(array('tlabel.flabel'=>$medialabel))
										->getField('fmediaid',true);
			if($mediaIdList){
				$where['tpaperissue.fmediaid'] = array('in',$mediaIdList);	
			}else{
				$where['tpaperissue.fmediaid'] = 0;
			}							
		}
		
		if($fmediaid != ''){
			$where['tpaperissue.fmediaid'] = $fmediaid;//媒体ID
			$_GET['region_fullname'] = '全国';
			$_GET['region_id'] = 100000;
		}
		if(is_array($fillegaltypecode)){
			$where['tpapersample.fillegaltypecode'] = array('in',$fillegaltypecode);//违法类型代码
		}
		if($fstarttime_s != '' || $fstarttime_e != ''){
			$where['tpaperissue.fissuedate'] = array('between',$fstarttime_s.','.$fstarttime_e.' 23:59:59');
		}
		
		if($merge_media == 'true'){//判断是否合并媒介查询
			$pp = 200;
			$count = M('tpaperissue')
								->join('tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
								->join('tad on tad.fadid = tpapersample.fadid')
								->join('tmedia on tmedia.fid = tpaperissue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tillegaltype on tillegaltype.fcode = tpapersample.fillegaltypecode')
									->where($where)->count('distinct tpaperissue.fmediaid');// 查询满足要求的总记录数
			$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数

			
			$paperissueList = M('tpaperissue')
				->field('tmedia.fid as fmediaid,tmedia.fmedianame,count(tpaperissue.fpaperissueid) as issue_count')
				->join('tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
				->join('tad on tad.fadid = tpapersample.fadid')
				->join('tmedia on tmedia.fid = tpaperissue.fmediaid')
				->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
				->join('tillegaltype on tillegaltype.fcode = tpapersample.fillegaltypecode')
				->where($where)
				->group('tpaperissue.fmediaid')
				->limit($Page->firstRow.','.$Page->listRows)->select();//查询广告样本列表
		}else{
			$count = M('tpaperissue')
				->join('tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
				->join('tad on tad.fadid = tpapersample.fadid')
				->join('tadclass on tad.fadclasscode = tadclass.fcode')
				->join('tadowner on tad.fadowner = tadowner.fid')
				->join('tmedia on tmedia.fid = tpaperissue.fmediaid')
				->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
				->join('tillegaltype on tillegaltype.fcode = tpapersample.fillegaltypecode')
				->where($where)->count();// 查询满足要求的总记录数
			$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数

			$paperissueList = M('tpaperissue')
				->field('
					tpaperissue.*,
					tad.fadname,
					tpapersample.fversion,
					tpapersample.fjpgfilename,
					tmedia.fmedianame,
					tillegaltype.fillegaltype,
					tadclass.ffullname as fadclassfullname
					'.$addFields)
				->join('tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
				->join('tad on tad.fadid = tpapersample.fadid')
				->join('tadclass on tad.fadclasscode = tadclass.fcode')
				->join('tadowner on tad.fadowner = tadowner.fid')
				->join('tmedia on tmedia.fid = tpaperissue.fmediaid')
				->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
				->join('tillegaltype on tillegaltype.fcode = tpapersample.fillegaltypecode')
				->where($where)
				->order('tpaperissue.fpaperissueid desc')
				->limit($Page->firstRow.','.$Page->listRows)->select();//查询广告样本列表
		}

		//数据导出
		if(!empty($outtype)){
			if(empty($paperissueList)){
				$this->ajaxReturn(array('code'=>1,'msg'=>'生成失败，暂无数据'));
			}
	        $ret = A('Issue')->outIssueXls($paperissueList,'03');
	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}

		$paperissueList = list_k($paperissueList,$p,$pp);//为列表加上序号
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		//var_dump($paperissueList);
		$this->assign('paperissueList',$paperissueList);//广告发布列表
		$this->assign('illegaltype',$illegaltype);//违法类型
		
		$this->assign('page',$Page->show());//分页输出
		// $this->display();
	}

	
	
	/*广告发布详情*/
	public function ajax_paperissue_details(){
		
		$fpaperissueid = I('fpaperissueid');//获取发布ID
		$fpaperissueid = intval($fpaperissueid);
		$paperissueDetails = M('tpaperissue')
										->field('
												tpaperissue.*,
												tad.fadname,
												tpapersample.fversion,
												tpapersample.fjpgfilename,
												
												tpapersample.fillegalcontent,
												tpapersample.fexpressioncodes,
												tpapersample.fexpressions,
												tpapersample.fadmanuno,
												tpapersample.fmanuno,
												tpapersample.fadapprno,
												tpapersample.fapprno,
												tpapersample.fadent,
												tpapersample.fent,
												tpapersample.fentzone,
												tmedia.fmedianame,
												tillegaltype.fillegaltype,
												tpapersource.furl
												
												')
										->join('tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
										->join('tpapersource on tpapersource.fid = tpapersample.sourceid','LEFT')
										
										
										->join('tad on tad.fadid = tpapersample.fadid')
										->join('tadowner on tadowner.fid = tad.fadowner')
										->join('tillegaltype on tillegaltype.fcode = tpapersample.fillegaltypecode')
										->join('tmedia on tmedia.fid = tpaperissue.fmediaid')
										->where(array('tpaperissue.fpaperissueid'=>$fpaperissueid))
										->find();//查询样本详情
		$this->ajaxReturn(array('code'=>0,'paperissueDetails'=>$paperissueDetails));
	}
}