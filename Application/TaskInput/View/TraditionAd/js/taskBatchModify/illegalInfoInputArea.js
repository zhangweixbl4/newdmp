const illegalInfoInputArea = {
  template: `<div v-loading="loading">
    <el-form ref="adForm" :model="taskForm" :rules="rules" label-width="100px">
      <el-form-item label="违法程度" prop="fillegaltypecode">
        <el-radio-group v-model="taskForm.fillegaltypecode">
          <el-radio :label="item.value" v-for="item in illegalTypeArr" :key="item.value">{{item.text}}</el-radio>
        </el-radio-group>
      </el-form-item>
      <div v-if="taskForm.fillegaltypecode != 0">
        <el-form-item label="违法表现" prop="fexpressioncodes" class="illegalExpression">
          <el-popover placement="top" width="800" trigger="click">
            <div>
              <el-input placeholder="输入关键字进行过滤" v-model="illegalKeyword">
              </el-input>
              <el-tree class="filter-tree" :data="illegalArr" node-key="fcode" accordion show-checkbox
                :filter-node-method="filterNode" @check-change="selectIllegal" selectIllegal ref="illegalSelecter">
              </el-tree>
            </div>
            <el-button slot="reference">点击选择</el-button>
          </el-popover>
          <el-card>
            <div class="text-center" v-if="checkedIllegalArr.length === 0">请选择</div>
            <div v-for="(item,index) in checkedIllegalArr" :key="item.fcode" style="margin: 5px;" title="点击删除" @click="deleteIllegal(index)">
              <b>{{index+1}}.</b> {{item.fexpression}}
            </div>
          </el-card>
        </el-form-item>
        <el-form-item label="违法内容" prop="fillegalcontent">
          <el-input type="textarea" :rows="3" v-model="taskForm.fillegalcontent">
          </el-input>
        </el-form-item>
      </div>
      <el-form-item>
        <el-button type="primary" @click="submitModify">提交</el-button>
      </el-form-item>
    </el-form>
  </div>`,
  data: function () {
    return {
      loading: false,
      illegalKeyword: '',
      taskForm: {},
      illegalTypeArr: illegalTypeArr,
      illegalArr: illegalArr,
      checkedIllegalArr: [],
      rules: {
        fillegaltypecode: [{
          required: true,
          message: '请选择违法程度',
          trigger: 'change'
        }],
        // fillegalcontent: [{
        //   required: true,
        //   message: '请填写违法内容',
        //   trigger: 'change'
        // }],
        fexpressioncodes: [{
          required: true,
          message: '请选择违法表现',
          trigger: 'change'
        }],
      },
    }
  },
  props: [],
  methods: {
    selectIllegal: function (data, isChecked) {
      if (data.fpcode === '') {
        return false
      }
      var index = this.checkedIllegalArr.indexOf(data);
      if (isChecked && index === -1 && data.fpcode !== '') {
        this.checkedIllegalArr.push(data)
      }
      if (!isChecked && index !== -1) {
        this.checkedIllegalArr.splice(index, 1)
      }
      this.changeIllegal()
      if (this.checkedIllegalArr.length > 0) {
        var maxCode = 0
        this.checkedIllegalArr.forEach(function (v) {
          if (v.fillegaltype > maxCode) {
            maxCode = v.fillegaltype
          }
        })
        Vue.set(this.taskForm, 'fillegaltypecode', maxCode)
      }
    },
    deleteIllegal: function (index) {
      this.checkedIllegalArr.splice(index, 1);
      this.$refs.illegalSelecter.setCheckedNodes(this.checkedIllegalArr);
      this.changeIllegal()
    },
    changeIllegal: function () {
      if (this.checkedIllegalArr.length === 0) {
        Vue.set(this.taskForm, 'fexpressioncodes', '')
      }
      var arr = this.checkedIllegalArr.map(function (v) {
        return v.fcode;
      });
      var str = arr.join(';');
      Vue.set(this.taskForm, 'fexpressioncodes', str)
    },
    filterNode(value, data) {
      if (!value) return true;
      return data.label.indexOf(value) !== -1;
    },
    submitModify: function () {
      this.$refs.adForm.validate((res, fields) => {
        if (res) {
          this.$emit('submit', this.taskForm)
        } else {
          var msg = fields[Object.keys(fields)[0]][0].message;
          this.$alert(msg, '提示')
          this.loading = false
        }
      })
    },
  }
}