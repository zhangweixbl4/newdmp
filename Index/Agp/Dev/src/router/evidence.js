const evidence = [{
    path: '/evidenceadd',
    name: 'evidenceadd',
    component: () => import('../components/evidence/evidenceadd'),
    meta: ['存证管理', '新增存证']
  },
  {
    path: '/evidencelist',
    name: 'evidencelist',
    component: () => import('../components/evidence/evidencelist'),
    meta: ['存证管理', '存证列表']
  },
  {
    path: '/evidencecart',
    name: 'evidencecart',
    component: () => import('../components/evidence/evidencecart'),
    meta: ['公证管理', '待公证列表']
  },
  {
    path: '/evidenceapply',
    name: 'evidenceapply',
    component: () => import('../components/evidence/evidenceapply'),
    meta: ['公证管理', '公证申请']
  }
]
export default evidence
