const leagal = [{
    path: '/surrender',
    name: 'surrender',
    component: () => import('../components/leagal/surrender'),
    meta: ['违法处置', '投诉举报']
  },
  {
    path: '/draftbox',
    name: 'draftbox',
    component: () => import('../components/leagal/draftbox'),
    meta: ['违法处置', '草稿箱']
  },
  {
    path: '/monitor',
    name: 'monitor',
    component: () => import('../components/leagal/monitor'),
    meta: ['违法处置', '监测发现']
  },
  {
    path: '/cluetracking',
    name: 'cluetracking',
    component: () => import('../components/leagal/cluetracking'),
    meta: ['违法处置', '线索跟踪']
  },
  {
    path: '/cluedisposal',
    name: 'cluedisposal',
    component: () => import('../components/leagal/cluedisposal'),
    meta: ['违法处置', '线索处理']
  },
  {
    path: '/clueedger',
    name: 'clueedger',
    component: () => import('../components/leagal/clueedger'),
    meta: ['违法处置', '线索台账']
  }
]

export default leagal