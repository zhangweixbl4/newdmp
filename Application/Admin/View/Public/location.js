﻿
function getAddress(obj, obj2, url, name) {
	let a = obj2 + ' .addr_area-list .click_region_name';
	let b = obj2 + " .addr_area-list";
	let c = obj2 + ' .addr_tab';
	$(obj2).css({
		'margin-bottom': '0',
		'position': 'absolute',
		'background-color': '#fff',
		'padding': '10px',
		'border-radius': '3px',
		'border': '1px solid #ccc',
		'z-index': '999999',
		'color':'#666'
	})
	//选择地区事件
	$(document).on('click', a, function () {
		var fid = $(this).attr("fid");//获取点击的地区ID
		$(obj).attr(name, fid);//修改input的fid

		get_region_list(obj, obj2, fid, url);
	});
	//点击tab标签事件
	$(document).on('click', '.ttab', function () {
		var fid = $(this).attr("fid");//获取点击的地区ID
		$(this).nextAll().remove();//移除右侧的ttab
		$(this).remove();//移除自己
		$(obj).attr(name, fid);//修改input的fid
		get_region_list(obj, obj2, fid, url);
	});
	//表单点击事件
	$(obj).click(function () {
		$(obj).attr('readonly', true);
		var val = $(this).val();//获取表单内容，用于判断表单是否有值
		var fid = $(obj).attr(name);//获取存储的地区code
		var areaList = $.trim($(b).html());//获取地区列表,用于判断地区列表是否为空
		$(obj2).parent().css('position', 'relative');//input父元素加上相对定位
		if (val == '' || areaList == '') {
			$(obj2).show();//显示地区选择控件
			$(c).empty();//移除所有已选择的
			get_region_list(obj, obj2, 0, url);//创建全国列表
		} else {
			$(obj2).toggle();//显示地区选择控件
		}
	});
}
//根据地区code创建标签和选择列表
function get_region_list(obj, obj2, fid, url) {
	let a = obj2 + ' .addr_area-list';
	let b = obj2 + ' .addr_tab li';
	let c = obj2 + ' .addr_tab';
	$.post(url, { "fid": fid }, function (req) {
		var listHTML = '';
		//循环地区列表
		for (let i = 0; i < req.regionList.length; i++) {
			listHTML += '<li><a class="click_region_name" fid="' + req.regionList[i].fid + '">' + req.regionList[i].fname + '</a></li>';
		}
		console.log(listHTML)
		$(obj).val(req.regionDetails.ffullname);
		$(obj).change();
		//如果地区列表不为空则把列表加入到 #region-select .tab
		if (listHTML != '') {
			$(a).html(listHTML);//地区列表渲染
			$(b).removeClass('curr');//移除所有tab的选择
			$(c).append('<li class="ttab curr" fid="' + req.regionDetails.fid + '"><a class=""><em>' + req.regionDetails.fname + '</em></a></li>');//创建新的tab
		} else {
			$(obj2).hide();//隐藏地区选择控件
		}

	});
}
