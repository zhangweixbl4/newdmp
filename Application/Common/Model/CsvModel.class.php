<?php
namespace Common\Model;


class CsvModel{



	/*导出数据*/
    public function make($data,$title_arr = array()){
		$title_arr = array_merge(array('序号'),$title_arr);
		if(is_array($title_arr)) $out_data .= iconv('utf-8','gb2312',implode(',',$title_arr))."\n";//标题
		foreach($data as $ke => $veo){//循环数据
			$ii ++ ;
			$vv = implode('^_^|^_^',$veo);//数组转成字符串，并使用分隔符号
			$vv = str_replace(',',"，",$vv);//把小逗号替换成大逗号，因为小逗号是CSV里面的单元格分隔符
			$vv = str_replace('^_^|^_^',",",$vv);//把前面的分隔符替换为小逗号
			$vv = str_replace("\n",";;",$vv);//把换行符替换为两个分号
//			$out_data .= $ii .','. iconv('utf-8','gb2312',$vv)."\n";//转换编码
			$out_data .= $ii .','. iconv("UTF-8","GB2312//IGNORE",$vv)."\n";//转换编码
		}

		return $out_data;
    }
    
    /**
    * 数据集导出为文件
    * @Param	Mixed variable
    * @Return	void 返回的数据
    */
    public function convertFile($data,$title_arr = array()){
    	if(is_array($title_arr)){
    		$out_data .= iconv('utf-8','gb2312',implode(',',$title_arr))."\n";//标题
    	}
    	foreach($data as $key => $value){							//循环数据
    		$vv = implode('^_^|^_^',$value);						//数组转成字符串，并使用分隔符号
    		$vv = str_replace(',','，',$vv);							//把小逗号替换成大逗号，因为小逗号是CSV里面的单元格分隔符
    		$vv = str_replace('^_^|^_^',',',$vv);					//把前面的分隔符替换为小逗号
    		$vv = str_replace('\n',';;',$vv);						//把换行符替换为两个分号
    		$out_data .= iconv("UTF-8","GB2312//IGNORE",$vv)."\n";	//转换编码
    	}
    	return $out_data;
    }
}