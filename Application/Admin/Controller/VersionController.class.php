<?php
/*
 * @Description: 广告版本管理控制器
 * @Author: yuhou.wang
 * @Date: 2019-08-22 16:35:24
 * @LastEditors: yuhou.wang
 * @LastEditTime: 2019-09-16 18:31:39
 */
namespace Admin\Controller;
use Think\Controller;
class VersionController extends BaseController {
	// 接收参数
    protected $P;
	/**
	 * @Des: 初始化
	 * @Edt: yuhou.wang
	 */	
    public function _initialize(){
        parent::_initialize();
        $this->P = json_decode(file_get_contents('php://input'),true);
    }
	/**
	 * @Des: 获取广告类别树形结构
	 * @Edt: chl
	 * @param {type} 
	 * @return: 
	 */	
	private function getAdClassArr(){
			$data = M('tadclass')
					->field('fcode, fpcode, fadclass')
					->where(['fstate' => ['EQ', 1]])
					->cache(true)
					->select();
			$res = [];
			foreach ($data as $key => &$value) {
					if (strlen($value['fcode']) === 2){
							$res[] = [
									'value' => $value['fcode'],
									'label' => $value['fadclass'],
							];
					}
			}
			foreach ($data as $key => &$value2) {
					$value2['value'] = $value2['fcode'];
					$value2['label'] = $value2['fadclass'];
					if (strlen($value2['fcode']) === 4){
							foreach ($res as &$item) {
									if ($value2['fpcode'] === $item['value']){
											$item['children'][] = $value2;
											foreach ($data as $key2 => &$value3) {
													if (strlen($value3['fcode']) === 6){
															foreach ($item['children'] as &$item2) {
																	$value3['value'] = $value3['fcode'];
																	$value3['label'] = $value3['fadclass'];
																	if ($value3['fpcode'] === $item2['value']){
																			$item2['children'][] = $value3;
																			break;
																	}
															}
													}
											}
											break;
									}
							}
					}
			}
			return $res;
	}
	/**
	 * @Des: index 页面方法
	 * @Edt: chl
	 * @param {type} 
	 * @return: 
	 */	
	public function index(){
		$getAdClassArr = json_encode($this->getAdClassArr());
		$this->assign(compact('getAdClassArr'));
		$this->display();
	}
	/**
	 * @Des: 获取版本列表
	 * @Edt: yuhou.wang
	 * @param {type} 
	 * @return: 
	 */	
	public function getList(){
		$fversion     = $this->P['fversion'];
		$fadname      = $this->P['fadname'];
		$fadclasscode = $this->P['fadclasscode'];
		$fadlen       = $this->P['fadlen'];
		$fuuid        = $this->P['fuuid'];
		$fmediaclass  = $this->P['fmediaclass'];
		$pageIndex    = $this->P['pageIndex'] ? $this->P['pageIndex'] : 1;
		$pageSize     = $this->P['pageSize'] ? $this->P['pageSize'] : 20;
		$where = [];
		if(!empty($fversion)){
			$where['a.fversion'] = ['LIKE','%'.$fversion.'%'];
		}
		if(!empty($fadname)){
			$where['b.fadname'] = ['LIKE','%'.$fadname.'%'];
		}
		if(!empty($fadclasscode)){
			$where['b.fadclasscode'] = $fadclasscode;
		}
		if(!empty($fadlen)){
			$fadlenArr = explode(',',$fadlen);
			$fadlenArr[0] = (int)($fadlenArr[0] ? $fadlenArr[0] : 0);
			$fadlenArr[1] = (int)($fadlenArr[1] ? $fadlenArr[1] : 86400);
			$where['a.fadlen'] = ['BETWEEN',$fadlenArr];
		}
		if(!empty($fuuid)){
			$where['a.fuuid'] = $fuuid;
		}
		if(!empty($fmediaclass)){
			$where['a.fmediaclass'] = $fmediaclass;
		}
		$count = M('tversion')
			->alias('a')
			->field('a.fid,a.fversion,a.fadid,b.fadname,b.fadclasscode,d.ffullname,a.fadlen,a.fuuid,a.fmediaid,c.fmedianame,a.fissuedate,a.fsourcefilename,a.fcreator,a.fcreatetime,a.fmodifier,a.fmodifytime')
			->join('tad b ON b.fadid = a.fadid')
			->join('tmedia c ON c.fid = a.fmediaid')
			->join('tadclass d ON d.fcode = b.fadclasscode')
			->where($where)
			->count();
		$list = M('tversion')
			->alias('a')
			->field('a.fid,a.fversion,a.fadid,b.fadname,b.fadclasscode,d.ffullname,a.fadlen,a.fuuid,a.fmediaid,c.fmedianame,a.fissuedate,a.fsourcefilename,a.fcreator,a.fcreatetime,a.fmodifier,a.fmodifytime')
			->join('tad b ON b.fadid = a.fadid')
			->join('tmedia c ON c.fid = a.fmediaid')
			->join('tadclass d ON d.fcode = b.fadclasscode')
			->where($where)
			->page($pageIndex,$pageSize)
			->select();
		$sql = M('tversion')->getLastSql();
		$data = [];
		foreach($list as $key=>$row){
			$data[] = [
				'fid'             => $row['fid'],
				'fversion'        => $row['fversion'],
				'fadid'           => $row['fadid'],
				'fadname'         => $row['fadname'],
				'fadclasscode'    => $row['fadclasscode'],
				'fadclass'        => $row['ffullname'],
				'fadlen'          => $row['fadlen'],
				'fuuid'           => $row['fuuid'],
				'fmediaid'        => $row['fmediaid'],
				'fmedianame'      => $row['fmedianame'],
				'fissuedate'      => date('Y-m-d',strtotime($row['fissuedate'])),
				'fsourcefilename' => $row['fsourcefilename'],
				'fcreator'        => $row['fcreator'],
				'fcreatetime'     => $row['fcreatetime'],
				'fmodifier'       => $row['fmodifier'],
				'fmodifytime'     => $row['fmodifytime'],
			];
		}
		$this->ajaxReturn(['code'=>0,'msg'=>'成功','count'=>$count,'data'=>$data]);
	}
	/**
	 * @Des: 获取版本列表
	 * @Edt: yuhou.wang
	 * @param {type} 
	 * @return: 
	 */	
	public function getList_V20190826(){
		$fversion     = $this->P['fversion'];
		$fadname      = $this->P['fadname'];
		$fadclasscode = $this->P['fadclasscode'];
		$fadlen       = $this->P['fadlen'];
		$fuuid        = $this->P['fuuid'];
		$fmediaclass  = $this->P['fmediaclass'];
		$pageIndex    = $this->P['pageIndex'] ? $this->P['pageIndex'] : 1;
		$pageSize     = $this->P['pageSize'] ? $this->P['pageSize'] : 20;
		$where = [];
		if(!empty($fversion)){
			$where['fversion'] = ['LIKE','%'.$fversion.'%'];
		}
		if(!empty($fadname) || !empty($fadclasscode)){
			$whereAd = [];
			if(!empty($fadname)) $whereAd['fadname'] = ['LIKE','%'.$fadname.'%'];
			if(!empty($fadclasscode)) $whereAd['fadclasscode'] = $fadclasscode;
			$adIds = M('tad')->where($whereAd)->getField('fadid',true);
			$where['fadid'] = ['IN',$adIds];
		}
		if(!empty($fadlen)){
			$where['fadlen'] = ['BETWEEN',$fadlen];
		}
		if(!empty($fuuid)){
			$where['fuuid'] = $fuuid;
		}
		if(!empty($fmediaclass)){
			$whereMedia['fmediaclass'] = $fmediaclass;
			$mediaIds = M('tmedia')->where($whereMedia)->getField('fid',true);
			$where['fmediaid'] = ['IN',$mediaIds];
		}
		$list = M('tversion')
			->alias('a')
			->where($where)
			->page($pageIndex,$pageSize)
			->select();
		$sql = M('tversion')->getLastSql();
		$data = [];
		foreach($list as $key=>$row){
			$adInfo = M('tad')->cache(true,86400)->where(['fadid'=>$row['fadid']])->find();
			$adClassInfo = M('tadclass')->cache(true,86400)->where(['fcode'=>$adInfo['fadclasscode']])->find();
			$mediaInfo = M('tmedia')->cache(true,86400)->where(['fid'=>$row['fmediaid']])->find();
			$data[] = [
				'fid'             => $row['fid'],
				'fversion'        => $row['fversion'],
				'fadid'           => $row['fadid'],
				'fadname'         => $adInfo['fadname'],
				'fadclasscode'    => $adInfo['fadclasscode'],
				'fadclass'        => $adClassInfo['ffullname'],
				'fadlen'          => $row['fadlen'],
				'fuuid'           => $row['fuuid'],
				'fmediaid'        => $row['fmediaid'],
				'fmedianame'      => $mediaInfo['fmedianame'],
				'fissuedate'      => date('Y-m-d',strtotime($row['fissuedate'])),
				'fsourcefilename' => $row['fsourcefilename'],
				'fcreator'        => $row['fcreator'],
				'fcreatetime'     => $row['fcreatetime'],
				'fmodifier'       => $row['fmodifier'],
				'fmodifytime'     => $row['fmodifytime'],
			];
		}
		$this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$data]);
	}
	/**
	 * @Des: 预览导入的版本数量
	 * @Edt: yuhou.wang
	 * @param {Int} fmediaid 媒体ID
	 * @param {Int} fdate 日期范围
	 * @return: 
	 */	
	public function getAddVersionInfo(){
		$fmediaid         = $this->P['fmediaid'];
		$fdate            = $this->P['fdate'];
		$fillegaltypecode = $this->P['fillegaltypecode'];
		$mediaInfo        = M('tmedia')->cache(true,120)->where(['fid' => $fmediaid])->find();
		$mediaclass       = substr($mediaInfo['fmediaclassid'],0,2);
		$issueTable       = 'ttvissue';
		$issueSamId       = 'ftvsampleid';
		$samTable         = 'ttvsample';
		$samId 		      = 'a.fid';
		$fields           = 'a.fadid,a.fadlen,a.uuid,a.fmediaid,a.fissuedate,a.favifilename fsourcefilename,b.fadname';
		switch($mediaclass){
			case '01':
				$issueTable = 'ttvissue';
				$issueSamId = 'ftvsampleid';
				$samTable = 'ttvsample';
				$samId = 'a.fid';
				break;
			case '02':
				$issueTable = 'tbcissue';
				$issueSamId = 'fbcsampleid';
				$samTable = 'tbcsample';
				$samId = 'a.fid';
				break;
			case '03':
				$issueTable = 'tpaperissue';
				$issueSamId = 'fpapersampleid';
				$samTable = 'tpapersample';
				$samId = 'a.fpapersampleid';
				$fields = 'a.fadid,a.uuid,a.fmediaid,a.fissuedate,a.fjpgfilename fsourcefilename,b.fadname';
				break;
		}
		$whereIssue = [
			'fmediaid' => $fmediaid,
			'fissuedate' => ['BETWEEN',$fdate],
		];
		$issueSamIdList = M($issueTable)
			->where($whereIssue)
			->getField('DISTINCT '.$issueSamId,true);
		$issueSql = M($issueTable)->getLastSql();
		if(count($issueSamIdList) > 0){
			$where = [
				'a.fadid'       => ['NEQ',0],
				'a.fversion_id' => ['EQ',0],
				$samId          => ['IN',$issueSamIdList],
			];
			if($fillegaltypecode == '30'){
				// 仅违法
				$where['a.fillegaltypecode'] = $fillegaltypecode;
			}
			$samCount = M($samTable)
				->alias('a')
				->field($fields)
				->join('tad b ON b.fadid = a.fadid')
				->where($where)
				->count();
			$sql = M($samTable)->getLastSql();
			$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','count'=>$samCount]);
		}
		$this->ajaxReturn(['code'=>0,'msg'=>'无数据']);
	}
	/**
	 * @Des: 导入样本创建版本数据
	 * @Edt: yuhou.wang
	 * @param {Int} fmediaid 媒体ID
	 * @param {Int} fdate 日期范围
	 * @return: 
	 */	
	public function addVersion(){
		$fmediaid         = $this->P['fmediaid'];
		$fdate            = $this->P['fdate'];
		$fillegaltypecode = $this->P['fillegaltypecode'];
		$mediaInfo        = M('tmedia')->cache(true,120)->where(['fid' => $fmediaid])->find();
		$mediaclass       = substr($mediaInfo['fmediaclassid'],0,2);
		$issueTable       = 'ttvissue';
		$issueSamId       = 'ftvsampleid';
		$samTable         = 'ttvsample';
		$samId 		      = 'a.fid';
		$fields           = 'a.fadid,a.fadlen,a.uuid,a.fmediaid,a.fissuedate,a.favifilename fsourcefilename,b.fadname';
		switch($mediaclass){
			case '01':
				$issueTable = 'ttvissue';
				$issueSamId = 'ftvsampleid';
				$samTable = 'ttvsample';
				$samId = 'a.fid';
				break;
			case '02':
				$issueTable = 'tbcissue';
				$issueSamId = 'fbcsampleid';
				$samTable = 'tbcsample';
				$samId = 'a.fid';
				break;
			case '03':
				$issueTable = 'tpaperissue';
				$issueSamId = 'fpapersampleid';
				$samTable = 'tpapersample';
				$samId = 'a.fpapersampleid';
				$fields = 'a.fadid,a.uuid,a.fmediaid,a.fissuedate,a.fjpgfilename fsourcefilename,b.fadname';
				break;
		}
		$whereIssue = [
			'fmediaid' => $fmediaid,
			'fissuedate' => ['BETWEEN',$fdate],
		];
		$issueSamIdList = M($issueTable)
			->where($whereIssue)
			->getField('DISTINCT '.$issueSamId,true);
		$issueSql = M($issueTable)->getLastSql();
		if(count($issueSamIdList) > 0){
			$where = [
				'a.fadid' => ['NEQ',0],
				'a.fversion_id' => ['EQ',0],
				$samId => ['IN',$issueSamIdList],
			];
			if($fillegaltypecode == '30'){
				// 仅违法
				$where['a.fillegaltypecode'] = $fillegaltypecode;
			}
			$samList = M($samTable)
				->alias('a')
				->field($fields)
				->join('tad b ON b.fadid = a.fadid')
				->where($where)
				->order('a.fissuedate ASC')
				->group('a.uuid')
				->select();
			$sql = M($samTable)->getLastSql();
			$existCount = 0;
			$data = [];
			foreach($samList as $key=>$row){
				// 排重条件
				$existWhere = [
					'fmediaclass' => $mediaclass,
					'fuuid'       => $row['uuid'],
				];
				$isExist = M('tversion')->where($existWhere)->count();
				if($isExist == 0){
					$data[] = [
						'fmediaclass' 	  => $mediaclass,
						'fversion' 		  => $row['fadname'],
						'fadid'           => $row['fadid'],
						'fadlen'          => $row['fadlen'],
						'fuuid'           => $row['uuid'],
						'fmediaid'        => $row['fmediaid'],
						'fissuedate'      => $row['fissuedate'],
						'fsourcefilename' => $row['fsourcefilename'],
						'fcreator'        => session('personInfo.fname'),
						'fcreatetime'     => date('Y-m-d H:i:s'),
					];
				}else{
					$existCount++;
				}
			}
			$count = count($data);
			if($count > 0){
				$result = M('tversion')->addAll($data);
				// 样本表绑定版本
				foreach($data as $k=>$v){
					$fversion_id = M('tversion')->where(['fuuid'=>$v['fuuid']])->getField('fid');
					$this->bindVersion($mediaclass,$v['fuuid'],$fversion_id);
				}
				$this->ajaxReturn(['code'=>0,'msg'=>'成功添加：'.$count.' 条版本']);
			}else{
				$this->ajaxReturn(['code'=>0,'msg'=>'完成']);
			}
		}
		$this->ajaxReturn(['code'=>0,'msg'=>'无数据']);
	}
	/**
	 * @Des: 修改版本信息
	 * @Edt: yuhou.wang
	 * @param {type} 
	 * @return: 
	 */	
	public function editVersion(){
		$fid          = $this->P['fid'];
		$fversion     = $this->P['fversion'];
		$fadname      = $this->P['fadname'];
		$fadclasscode = $this->P['fadclasscode'];
		$versionInfo  = M('tversion')
			->alias('a')
			->field('a.fid,a.fadid,a.fversion,b.fadname,b.fadclasscode')
			->join('tad b ON b.fadid = a.fadid')
			->where(['a.fid'=>$fid])
			->find();
		$data = [];
		if($versionInfo['fversion'] != $fversion){
			$data['fversion'] = $fversion;
		}
		if($versionInfo['fadname'] != $fadname || $versionInfo['fadclasscode'] != $fadclasscode){
			$fadid = A('Common/Ad','Model')->getAdId($fadname,'',$fadclasscode,'','Version/editVersion','');
			$data['fadid'] = $fadid;
		}
		if(!empty($data)){
			M('tversion')->where(['fid'=>$fid])->save($data);
		}
		$this->ajaxReturn(['code'=>0,'msg'=>'修改成功']);
	}
	/**
	 * @Des: 删除版本
	 * @Edt: yuhou.wang
	 * @param {type} 
	 * @return: 
	 */	
	public function delVersion(){
		$fid = $this->P['fid'];
		M('tversion')->where(['fid'=>['IN',$fid]])->delete();
		$this->ajaxReturn(['code'=>0,'msg'=>'删除版本成功']);
	}
	/**
	 * @Des: 获取版本中样本列表
	 * @Edt: yuhou.wang
	 * @param {type} 
	 * @return: 
	 */	
	public function getSamList(){
		$fid = $this->P['fid'];
		$versionInfo = M('tversion')->where(['fid'=>$fid])->find();
		$samTable = 'ttvsample';
		$fields = 'a.fid fid,a.fadid,b.fadname,b.fadclasscode,d.ffullname,a.fadlen,a.uuid,a.fmediaid,c.fmedianame,a.fissuedate,a.favifilename fsourcefilename,a.fcreator,a.fcreatetime,a.fmodifier,a.fmodifytime';
		switch($versionInfo['fmediaclass']){
			case 1:
				$samTable = 'ttvsample';
				break;
			case 2:
				$samTable = 'tbcsample';
				break;
			case 3:
				$samTable = 'tpapersample';
				$fields = 'a.fpapersampleid fid,a.fadid,b.fadname,b.fadclasscode,d.ffullname,a.uuid,a.fmediaid,c.fmedianame,a.fissuedate,a.fjpgfilename fsourcefilename,a.fcreator,a.fcreatetime,a.fmodifier,a.fmodifytime';
				break;
		}
		$where = [
			'a.fversion_id' => $fid
		];
		$list = M($samTable)
			->alias('a')
			->field($fields)
			->join('tad b ON b.fadid = a.fadid')
			->join('tmedia c ON c.fid = a.fmediaid')
			->join('tadclass d ON d.fcode = b.fadclasscode')
			->where($where)
			->select();
		$sql = M($samTable)->getLastSql();
		$data = [];
		foreach($list as $key=>$row){
			$data[] = [
				'fid'             => $row['fid'],
				'fadid'           => $row['fadid'],
				'fadname'         => $row['fadname'],
				'fadclasscode'    => $row['fadclasscode'],
				'fadclass'        => $row['ffullname'],
				'fadlen'          => $row['fadlen'],
				'fuuid'           => $row['uuid'],
				'fmediaid'        => $row['fmediaid'],
				'fmedianame'      => $row['fmedianame'],
				'fissuedate'      => date('Y-m-d',strtotime($row['fissuedate'])),
				'fsourcefilename' => $row['fsourcefilename'],
				'fcreator'        => $row['fcreator'],
				'fcreatetime'     => $row['fcreatetime'],
				'fmodifier'       => $row['fmodifier'],
				'fmodifytime'     => $row['fmodifytime'],
			];
		}
		$this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$data]);
	}
	/**
	 * @Des: 从版本删除样本
	 * @Edt: yuhou.wang
	 * @param {type} 
	 * @return: 
	 */	
	public function delSam(){
		$fid         = $this->P['fid'];
		$fsampleid   = $this->P['fsampleid'];
		$versionInfo = M('tversion')->where(['fid' => $fid])->find();
		$samTable    = 'ttvsample';
		$samfid      = 'a.fid';
		$fields      = 'a.fadid,a.fadlen,a.uuid,a.fmediaid,a.fissuedate,a.favifilename fsourcefilename,b.fadname';
		switch($versionInfo['fmediaclass']){
			case 1:
				$samTable = 'ttvsample';
				break;
			case 2:
				$samTable = 'tbcsample';
				break;
			case 3:
				$samTable = 'tpapersample';
				$samfid = 'a.fpapersampleid';
				$fields = 'a.fadid,a.uuid,a.fmediaid,a.fissuedate,a.fjpgfilename fsourcefilename,b.fadname';
				break;
		}
		$where = [$samfid => $fsampleid];
		$samInfo = M($samTable)
			->alias('a')
			->field($fields)
			->join('tad b ON b.fadid = a.fadid')
			->where($where)
			->find();
		$samSql = M($samTable)->getLastSql();
		$versionIdInfo = M('tversion')->where(['fuuid'=>$samInfo['uuid'],'fid'=>['NEQ',$fid]])->find();
		if(!empty($versionIdInfo)){
			// 若样本原版本存在，则恢复
			$res = M($samTable)->alias('a')->where([$samfid=>$fsampleid])->save(['fversion_id'=>$versionIdInfo['fid']]);
			$updateSql = M($samTable)->getLastSql();
		}else{
			// 若样本原版本不存在，则创建
			$version = [
				'fmediaclass' 	  => $versionInfo['fmediaclass'],
				'fversion' 		  => $samInfo['fadname'],
				'fadid'           => $samInfo['fadid'],
				'fadlen'          => $samInfo['fadlen'],
				'fuuid'           => $samInfo['uuid'],
				'fmediaid'        => $samInfo['fmediaid'],
				'fissuedate'      => $samInfo['fissuedate'],
				'fsourcefilename' => $samInfo['fsourcefilename'],
				'fcreator'        => 'delSam',
				'fcreatetime'     => date('Y-m-d H:i:s'),
			];
			$fid = $this->createVersion($version);
			$res = M($samTable)->alias('a')->where([$samfid=>$fsampleid])->save(['fversion_id'=>$fid]);
		}
		$this->ajaxReturn(['code'=>0,'msg'=>'删除样本成功']);
	}
	/**
	 * @Des: 合并版本
	 * @Edt: yuhou.wang
	 * @param {type} 
	 * @return: 
	 */	
	public function mergeVersion(){
		$fids = $this->P['fids'];
		$mainfid = $this->P['mainfid'];
		$mainVerInfo = M('tversion')->where(['fid'=>$mainfid])->find();
		// 过滤得到与目标版本相同媒体类型的fid，不同媒体类型的版本直接不处理。TODO：考虑进行提示
		$mergefids = M('tversion')->where(['fid'=>['IN',$fids],'fmediaclass'=>$mainVerInfo['fmediaclass']])->getField('fid',true);
		// 排除目标fid
		array_splice($mergefids,array_search($mainfid,$mergefids),1);
		// 更新样本绑定的版本ID
		$samTable = 'ttvsample';
		switch($mainVerInfo['fmediaclass']){
			case 1:
				$samTable = 'ttvsample';
				break;
			case 2:
				$samTable = 'tbcsample';
				break;
			case 3:
				$samTable = 'tpapersample';
				break;
		}
		$res = M($samTable)->where(['fversion_id'=>['IN',$mergefids]])->save(['fversion_id'=>$mainfid]);
		// 删除目标外其它版本记录
		$del = M('tversion')->where(['fid'=>['IN',$mergefids]])->delete();
		$this->ajaxReturn(['code'=>0,'msg'=>'版本合并成功']);
	}
	/**
	 * @Des: 
	 * @Edt: yuhou.wang
	 * @param {type} 
	 * @return: 
	 */
	private function createVersion($version = []){
		$data = [
			'fmediaclass' 	  => $version['fmediaclass'],
			'fversion' 		  => $version['fversion'],
			'fadid'           => $version['fadid'],
			'fadlen'          => $version['fadlen'],
			'fuuid'           => $version['uuid'],
			'fmediaid'        => $version['fmediaid'],
			'fissuedate'      => $version['fissuedate'],
			'fsourcefilename' => $version['fsourcefilename'],
			'fcreator'        => 'createVersion',
			'fcreatetime'     => date('Y-m-d H:i:s'),
		];
		$fid = M('tversion')->add($data);
		return $fid;
	}
	/**
	 * @Des: 测试用例-添加版本数据
	 * @Edt: yuhou.wang
	 * @param {Int} mediaclass 媒体类别 1-电视 2-广播 3-报纸
	 * @param {Int} num 数据条数
	 * @return: 
	 */	
	public function testAddVersion(){
		$mediaclass = $this->P['mediaclass'] ? $this->P['mediaclass'] : 1;
		$num = $this->P['num'] ? $this->P['num'] : 100;
		$samTable = 'ttvsample';
		$fields = 'a.fadid,a.fadlen,a.uuid,a.fmediaid,a.fissuedate,a.favifilename fsourcefilename,b.fadname';
		switch($mediaclass){
			case 1:
				$samTable = 'ttvsample';
				break;
			case 2:
				$samTable = 'tbcsample';
				break;
			case 3:
				$samTable = 'tpapersample';
				$fields = 'a.fadid,a.uuid,a.fmediaid,a.fissuedate,a.fjpgfilename fsourcefilename,b.fadname';
				break;
		}
		$where = [
			'a.fadid' => ['NEQ',0],
		];
		$samList = M($samTable)
			->alias('a')
			->field($fields)
			->join('tad b ON b.fadid = a.fadid')
			->where($where)
			->order('a.fissuedate DESC')
			->group('a.uuid')
			->limit($num)
			->select();
		$sql = M($samTable)->getLastSql();
		$data = [];
		foreach($samList as $key=>$row){
			$data[] = [
				'fmediaclass' 	  => $mediaclass,
				'fversion' 		  => $row['fadname'],
				'fadid'           => $row['fadid'],
				'fadlen'          => $row['fadlen'],
				'fuuid'           => $row['uuid'],
				'fmediaid'        => $row['fmediaid'],
				'fissuedate'      => $row['fissuedate'],
				'fsourcefilename' => $row['fsourcefilename'],
				'fcreator'        => 'testAddVersion',
				'fcreatetime'     => date('Y-m-d H:i:s'),
			];
		}
		$count = count($data);
		if($count > 0){
			M('tversion')->addAll($data);
			// 样本表绑定版本
			foreach($data as $k=>$v){
				$fversion_id = M('tversion')->where(['fuuid'=>$v['fuuid']])->getField('fid');
				$this->bindVersion($mediaclass,$v['fuuid'],$fversion_id);
			}
			$this->ajaxReturn(['code'=>0,'msg'=>'成功添加：'.$count.' 条版本']);
		}else{
			$this->ajaxReturn(['code'=>1,'msg'=>'无数据']);
		}
	}
	/**
	 * @Des: 测试用例-样本标记版本
	 * @Edt: yuhou.wang
	 * @param {Int} 	$mediaclass 媒体类别 1-电视 2-广播 3-报纸
	 * @param {String} 	$fuuid UUID
	 * @param {Int} 	$fversion_id 版本ID
	 * @return: {Boolen}
	 */	
	public function bindVersion($mediaclass = 1, $fuuid, $fversion_id){
		$samTable = 'ttvsample';
		switch($mediaclass){
			case 1:
			case 01:
			case '01':
				$samTable = 'ttvsample';
				break;
			case 2:
			case 02:
			case '02':
				$samTable = 'tbcsample';
				break;
			case 3:
			case 03:
			case '03':
				$samTable = 'tpapersample';
				break;
		}
		$count = M($samTable)->where(['uuid'=>$fuuid])->save(['fversion_id'=>$fversion_id]);
		return true;
	}
}