<?php
namespace Api\Controller;
use Think\Controller;
class MobileController extends Controller {
	
	
	
	/*发送手机验证码*/
    public function identifying_code(){
		exit;
		$mobile = I('mobile');//获取需要发送验证码的手机号
		if(strlen($mobile) != 11) $this->ajaxReturn(array('code'=>-1,'msg'=>'手机号码错误,发送失败'));
		$identifying_code = rand(100000,999999);
		$rr = A('Common/Alidayu','Model')->verification_code($mobile,$identifying_code);//发送验证码
		//var_dump($rr);
		if($rr['code'] == 0){//判断是否发送成功
			S('identifying_code_'.$mobile,$identifying_code,300);//设置过期时间
		}
		$this->ajaxReturn($rr);
		
		
	}

	/*
	* 南京局发送短信督办系统的线索任务催办短信
	* by zw
	*/
	public function smsIllegalCuiBan(){
		session_write_close();
		set_time_limit(600);
		$system_num = 320100;
		$doNce = M('nj_clue')
			->alias('a')
			->field('b.fcreatetrepid,count(*) bcount,group_concat(a.fnum) fnums,group_concat(a.fid) fids')
			->join('nj_clueflow b on a.fid = b.fclueid and b.fsendstatus = 0')
			->where(['a.fstatus'=>['in',[20,30,40]],'_string'=>'DATEDIFF(a.fendtime,"'.date('Y-m-d').'")<0','a.fcuiban_tip'=>0])
			->group('b.fcreatetrepid')
			->select();
		foreach ($doNce as $key2 => $value2) {
			$mobiles = M('tregulatorperson')
				->alias('a')
				->field('a.fmobile,a.fcode')
				->join('tpersonlabel b on a.fid = b.fpersonid and b.fstate = 1 and b.fcustomer = "'.$system_num.'"')
				->where(['fregulatorid'=>$value2['fcreatetrepid']])
				->select();
			foreach ($mobiles as $key => $value) {
				if(strlen($value['fmobile']) == 11 && is_numeric($value['fmobile'])){
					if(empty(S('nj_cbsendsms_'.$value['fmobile']))){
						$smsstr = A('Common/Alitongxin','Model')->nj_illtask_cb_sms($value['fmobile'],$value2['bcount'],$value2['fnums']);
						if(!empty($value2['fids']) && $smsstr['code'] == 0){
							S('nj_cbsendsms_'.$value['fmobile'],1,86400);
							$saveNce = M('nj_clue')->where('fid in ('.$value2['fids'].')')->save(['fcuiban_tip'=>1]);
						}
					}
				}
			}
		}
		
	}

}