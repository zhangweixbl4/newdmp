Vue.prototype.$ELEMENT = { size: "mini", zIndex: 3000 };

var vm = new Vue({
  el: '#app',
  data() {
    let start = dayjs().startOf('year').format('YYYY-MM-DD')
    let end = dayjs().endOf('year').format('YYYY-MM-DD')
    return {
      loading: false,
      tableData: [],
      form: {
        field_group: 'mediaclass',
        mediaclass: '',
        fregionid: '',
        start_date: start,
        end_date: end
      },
      chart: null,
      duringTime: [start, end],
      chartType: 'tc',
      regionList: regionList,
      mediaType: {
        '1': {
          label: '电视'
        },
        '2': {
          label: '广播'
        },
        '3': {
          label: '报纸'
        },
        '13': {
          label: '互联网'
        },
      },
      illArr: [
        {
          value: '1',
          label: '违法',
        },
        {
          value: '2',
          label: '不违法',
        },
      ],
      type: {
        tc: '条次',
        ts: '条数',
        sc: '时长',
      },
      tableHeight: '300px',
      fieldGroup: {
        mediaclass: '媒介类型',
        fmediaownername: '媒介机构',
        fmedianame: '媒介名称',
        month: '月份',
        region: '地区',
        adclass: '广告类别',
      },
      title: '23',
      tc:[],
      ts:[],
      sc: [],
      xdata: [],
      ydata: [],
      preData: {},
      sourceData: [],
    }
  },
  computed: {
    piedata() {
      return this.tableData.map(i => {
        return {
          name: i.group_0,
          value: i[this.chartType]
        }
      })
    },
    chartsData() {
      let splicArr = ['fmedianame', 'fmediaownername']
      if (splicArr.includes(this.form.field_group)) {
        return {
          xdata: [...this.xdata].splice(0, 30),
          ydata: [...this[this.chartType]].splice(0, 30),
        }
      } else {
        return {
          xdata: this.xdata,
          ydata: this[this.chartType],
        }
      }
    },
    option() {
      let pie = ['mediaclass', 'adclass']
      if (pie.includes(this.form.field_group)) {
        return {
          title : {
            text: '',
          },
          tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
          },
          legend: {
            orient: 'vertical',
            left: 'left',
            data: this.xdata
          },
          series : [
            {
              name: this.type[this.chartType],
              type: 'pie',
              radius : '55%',
              center: ['50%', '60%'],
              data: this.piedata,
              itemStyle: {
                emphasis: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
              }
            }
          ]
        }
      } else {
        return {
          title: {
            text: ''
          },
          tooltip: {},
          legend: {
            padding: [10, 10, 20, 10],
          },
          xAxis: {
            data: this.chartsData.xdata,
            axisLabel: {
              interval: 0,
              rotate: -25,
            },
          },
          yAxis: {},
          series: [{
            name: this.type[this.chartType],
            type: 'bar',
            data: this.chartsData.ydata,
          }]
        }
      }
    }
  },
  mounted() {
    this.$nextTick(() => {
      document.querySelector('#app').style.opacity = 1
      this.search()
      this.getTableHeight()
    })
    window.onresize = () => {
      this.getTableHeight()
    }
  },
  methods: {
    sortChange({ prop, order }) {
      let data = [...this.sourceData]
      if (order) {
        let label = prop === '_sc' ? 'sc' : prop
        data.sort((a, b) => {
          if (order === 'ascending') {
            return a[label] - b[label]
          } else {
            return b[label] - a[label]
          }
        })
      }
      this.dealData(data)
    },
    timeChange(val) {
      let [start, end] = val
      this.form.start_date = start
      this.form.end_date = end
    },
    dealData(list) {
      this.clearData()
      this.tableData = (list || []).map(i => {
        i.tc = +i.tc
        i.ts = +i.ts
        i.sc = +i.sc
        i.month = +dayjs(i.group_0).format('M')
        if(this.preData.field_group === 'month') i.group_0 = dayjs(i.group_0).format('YYYY年M月')
        i._sc = this.dealTime(+i.sc)
        this.tc.push(i.tc)
        this.ts.push(i.ts)
        this.sc.push(i.sc)
        this.xdata.push((i.group_0.length > 6 && this.form.field_group !== 'month') ? `${i.group_0.substr(0,6)}...` : i.group_0)
        return i
      })
      if (this.preData.field_group === 'month') {
        this.tableData = this.tableData.sort((a, b) => {
          return a.month - b.month
        })
      }
      if(!this.sourceData.length) this.sourceData = [...this.tableData]
      this.init()
    },
    search() {
      this.sourceData = []
      this.loading = true
      let _data = {
        ...this.form,
      }
      this.preData = _data
      fly.post('/Admin/Gongyi/data_list',Qs.stringify(_data)).then(({ data }) => {
        if (data.code === 0) {
          this.dealData(data.data_list)
        } else {
          $h.notify.error(data.msg)
        }
        this.loading = false
      }).catch(e => {
        this.loading = false
      })
    },
    init() {
      if(this.chart) this.chart.clear()
      this.chart = echarts.init(this.$refs.chart, 'macarons')
      this.chart.setOption(this.option)
    },
    clearData() {
      this.xdata = []
      this.tc = []
      this.ts = []
      this.sc = []
    },
    getTableHeight() {
      let formHeight = this.$refs.sform.$el.clientHeight
      this.tableHeight = document.body.clientHeight - formHeight - 554 + 'px'
    },
    dealTime(result) {
      let h = Math.floor(result / 3600);
      let m = Math.floor((result / 60 % 60));
      let s = Math.floor((result % 60));
      return result = h + "小时 " + m + "分钟 " + s + "秒";
    }
  },
  watch: {
    chartType() {
      this.init()
    }
  },
})