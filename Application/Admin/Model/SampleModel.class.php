<?php
namespace Admin\Model;



class SampleModel{


   /*样本转语音*/
	public function sample_to_word($uuid){
		//$uuid = '9525da74-951c-11e8-a5d8-00163e08531e';
		
		$startPK = array (
			array('fuuid', $uuid),
			array('fstart', 0),
			array('fend', 0),
			
		);
		$endPK = array (
			array('fuuid', $uuid),
			array('fstart', 99999),
			array('fend', 99999),
			
		);
		
		$tableRet = A('Common/TableStore','Model')->getRange('sample_to_word',$startPK,$endPK,999,'FORWARD');
		if(!$tableRet){
			return $this->to_word($uuid);
		}
		
		$sample_to_word = '';
		foreach($tableRet as $table){
			$sample_to_word .= $table['fword'].'	';
		}
		
		//var_dump($sample_to_word);
		return $sample_to_word;
		
	}
	
	/*样本语音识别*/
	public function to_word($fuuid){
		
		
		$getUrl = 'http://47.96.182.117:8002/sample_to_word/'.$fuuid.'/';
		
		$info = http($getUrl);
		$info = json_decode($info,true);
		
		

		if($info['ret'] !== 0){
			return false;
		}

		$dataList = array();//写入tablestore的数据
		$ots_w_num = 0;
		$sample_to_word = '';
		foreach($info['data']['word'] as $word){//循环list
			$sample_to_word .= $word['word'].'	';
			$dataList[] = array ( // 第一行
							"operation_type" => 'PUT',            //操作是PUT
							'condition' => 'IGNORE',
							'primary_key' => array (
								array('fuuid',$fuuid),
								array('fstart',intval($word['start']*1000)),
								array('fend',intval($word['end']*1000)),
								
							),
							'attribute_columns' => array(
															array('fword',$word['word']),
															array('sam_url',$info['data']['url']),
															//array('createtime',time()),
															
														),
						);//组装写入tablestore的数据结构
			if(	count($dataList) >= 200){//判断数据是否达到200条，因为table的最大限制是一次性写入200条
				$response = A('Common/TableStore','Model')->batchWriteRow('sample_to_word',$dataList);//写入数据到tablestore
				$ots_w_num += $response['successNum'];//写入ots成功的数量
				$dataList = array();//初始化数组
				
			}		

		}
		if(	count($dataList) > 0){//
				$response = A('Common/TableStore','Model')->batchWriteRow('sample_to_word',$dataList);//写入数据到tablestore
				$ots_w_num += $response['successNum'];//写入ots成功的数量
				$dataList = array();
				
		}

		return $sample_to_word;
		
		
	}


}