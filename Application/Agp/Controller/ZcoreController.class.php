<?php
namespace Agp\Controller;
use Think\Controller;

class ZcoreController extends Controller
{
	/*
	 * 获取临时登录密码
	 * by zw
	*/
	public function agp_adminsmssend(){
		$phone = I('post.phone');//手机号
		$verify = I('post.verify');//验证码
		$agp_phone = C('AGP_SIGNJURISDICTION_PHONE');//白名单手机号

		if(!A('Login')->check_verify($verify)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'验证码输入有误'));
		}

		if(!in_array($phone,$agp_phone)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'您无权操作'));
		}

		$auto_num = make_num(8);
		S('admin'.$phone,$auto_num,14400);
		cookie('adminphone',$phone,14400);
		$sms_return = A('Common/Alitongxin','Model')->auth_sms($phone,$auto_num,'获取AGP临时登录密码','240分钟');
		if(empty($sms_return['code'])){
        	$this->ajaxReturn(array('code'=>0,'msg'=>'授权码已发送'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'授权码发送失败，请重新点击发送'));
		}
	}
}
