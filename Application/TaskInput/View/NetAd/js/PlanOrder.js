Vue.prototype.$ELEMENT = {
    size: 'mini',
    zIndex: 3000
  }
  const vm = new Vue({
    el: "#app",
    data() {
      let startTime = moment().startOf('month').format('YYYY-MM-DD')
      let endTime = moment().format('YYYY-MM-DD')
      return {
        date: [startTime, endTime],
        allcount: 0,
        tableData: [],
        tableHeight: '400px',
        allMediaList: [],
        loading: false,
        form: {
          fmediaid: '',
        },
        page: {
          pageIndex: 1,
          pageSize: 20,
        },
        multipleSelection: [],
        fillegaltypecode: {
          '0': '不违法',
          '10': '违法',
        }
      }
    },
    created() {
      this.getList()
      this.getMediaList()
    },
    mounted() {
      this.$nextTick(() => {
        this.getTableHeight()
        document.querySelector('#app').style.opacity = 1
      })
      window.onresize = () => {
        this.getTableHeight()
      }
    },
    methods: {
      getList(page) {
        if (page) this.page.pageIndex = page
        let [sttime, endtime ] = this.date || []
        let data = {
          sttime,
          endtime,
          ...this.form,
          ...this.page
        }
        this.loading = true
        $.post('/TaskInput/NetAd/NetTaskList', JSON.stringify(data))
          .then(res => {
            if (res.code === 0) {
              // 判断地址栏中有没有http
              this.tableData = res.data.map(i => {
                if(i.net_original_url && i.net_original_url.indexOf("http") < 0 && i.net_target_url != '') {
                  i.net_original_url = '//' + i.net_original_url
                }
                if(i.net_target_url && i.net_target_url.indexOf("http") < 0 && i.net_target_url != '') {
                  i.net_target_url = '//' + i.net_target_url
                }
                return i
              })
              this.allcount = res.count
            } else {
              this.$notify.error({
                message: res.msg,
                duration: 1000
              });
            }
            this.loading = false
        })
      },
      getTableHeight() {
        this.tableHeight = `${document.body.clientHeight - 100}px`
      },
      getMediaList(){
        $.post('/TaskInput/NetAd/getUserMediaList')
        .then(res=>{
          this.allMediaList = res.data
        })
      },
      handleSelectionChange(val) {
        this.multipleSelection = val
      },
      submitChangePriority(bool) {
        let ids = this.multipleSelection.map(i => i.taskid)
        let [sttime, endtime ] = this.date || []
        let data = {
          selad: bool ? ids : [],
          sttime,
          endtime,
          ...this.form,
        }
        this.$prompt('请输入优先级', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          inputPattern:  /^\d+(?=\.{0,1}\d+$|$)/,
          inputErrorMessage: '输入内容必须为数字'
        }).then(({ value }) => {
          data.editpriority = value
          $.post('/TaskInput/NetAd/NetTaskPriority', JSON.stringify(data))
            .then(res => {
              if (res.code === 0) {
                this.$notify({
                  message: res.msg,
                  type: 'success',
                  duration: 1000
                });
                this.getList()
              } else {
                this.$notify.error({
                  message: res.msg,
                  duration: 1000
                });
            }
          })
        }).catch(() => {
          this.$message({
            type: 'info',
            message: '取消输入'
          })
        })
      }
    },
  })