<?php
namespace Open\Controller;
use Think\Controller;

class PapersourceController extends BaseController {
	
	public function add_edit_papersource(){

		$papersource_data = file_get_contents('php://input');//获取post数据
		
		$papersource_data = json_decode($papersource_data,true);//把json转化成数组

		$fmediaid = $papersource_data['fmediaid'];//媒体ID
		
		if($fmediaid == ''){//判断是否有传mediaid
			$fmediaid = M('tmedia')->cache(true,120)->where(array('left(fmediaclassid,2)'=>'03','fchannelid'=>$papersource_data['qfid']))->getField('fid');//查询mediaid
		}
		
		$dateTime = date('Y-m-d H:i:s');//当前时间
		$get_client_ip = get_client_ip();
        $mediaInfo = M('tmedia')->cache(true,120)->where(array('fid'=>$fmediaid))->find();//查询媒介信息
		if(!$mediaInfo) $this->ajaxReturn(array('code'=>-1,'msg'=>'媒体不存在'));
		$source_tab = 'tpapersource';//素材表名称
		
		
		if($mediaInfo['fcollecttype'] != '51'){
			$source_tab = 'tpapersource_2';//素材表名称
		}
		
		$a_e_data = array();	
		$a_e_data['fmediaid'] = $fmediaid;//发布媒介ID
		$a_e_data['fissuedate'] = date('Y-m-d H:i:s',strtotime($papersource_data['fissuedate']));//发布日期
		
		
		$a_e_data['fpage'] = $papersource_data['fpage'];//版面
		$a_e_data['fpagetype'] = $papersource_data['fpagetype'];//版面类型
		$a_e_data['priority'] = $mediaInfo['priority'] + 1000;//
			
			


		$a_e_data['furl'] = $papersource_data['furl'];//存储位置
		
		$a_e_data['fcreator'] = $get_client_ip;//创建人
		$a_e_data['fcreatetime'] = $dateTime;//创建时间
		$a_e_data['fmodifier'] = $get_client_ip;//修改人
		$a_e_data['fmodifytime'] = $dateTime;//修改时间
		
		if($a_e_data['furl'] == '') $this->ajaxReturn(array('code'=>10015,'msg'=>'添加数据失败,储存位置不能为空'));
		
		if(strtotime($a_e_data['fissuedate']) < (time()-86400*365) || strtotime($a_e_data['fissuedate']) > (time()+86400)){
			$this->ajaxReturn(array('code'=>10017,'msg'=>'发布日期不能是一年前，也不能是未来日期'));
		}
		
		$sourceInfoWhere = array();
		$sourceInfoWhere['fmediaid'] = $fmediaid;
		$sourceInfoWhere['fissuedate'] = $a_e_data['fissuedate'];
		$sourceInfoWhere['fpage'] = $a_e_data['fpage'];

		
		$sourceInfo = M($source_tab)->field('fid,fstate')->where($sourceInfoWhere)->find();
		if($sourceInfo && in_array($sourceInfo['fstate'],array(1,2))){
			$this->ajaxReturn(array('code'=>10011,'msg'=>'版面重复且众包已在处理'));
		}
		
		if($sourceInfo && in_array($sourceInfo['fstate'],array(0,9,6))){
			M($source_tab)->where(array('fid'=>$sourceInfo['fid']))->save(array('furl'=>$papersource_data['furl']));
			$this->ajaxReturn(array('code'=>0,'msg'=>'重复,但版面已更新'));
		}
		
		
			
		$fid = M($source_tab)->add($a_e_data);//创建
		if($fid){
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功','id'=>$fid));
		}else{
			$this->ajaxReturn(array('code'=>10011,'msg'=>'添加失败,原因未知'));
		}
	
		

	}

	//获取广告报纸的任务列表
	public function get_paper_task(){

		$input_data = file_get_contents('php://input');//获取post数据
		$input_data = json_decode($input_data,true);//把json转化成数组
		
		$mediaid = intval($input_data['mediaid']);//媒体ID
		
		
		$where = array();
		$where['tpapersource.fstate'] = array('in','0,1');//任务状态，只查询未处理和正在处理的任务
		$where['tpapersource.fmediaid'] = $mediaid;
		// $where['tpapersource.fuserid'] = 0;
		
		$taskCount = M('tpapersource')
										->join('tmedia on tmedia.fid = tpapersource.fmediaid')->where($where)->count();//查询任务数量
		$taskList = M('tpapersource')
										->field('tpapersource.*,tmedia.fmedianame')
										->join('tmedia on tmedia.fid = tpapersource.fmediaid')->where($where)->select();//查询任务列表
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','taskCount'=>$taskCount,'taskList'=>$taskList));

	}
	
}