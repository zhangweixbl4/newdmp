<?php
namespace Api\Controller;
use Think\Controller;

class AdbController extends Controller {

    //线索查询生成json
    public function clue_test(){
        $res = M()->query("SELECT
                        a.fad_name as adname,
                        a.create_time as putdate,
                        \"国家局平台\" as sourcename,
                        (case when instr(b.fmedianame,'（') > 0 then left(b.fmedianame,instr(b.fmedianame,'（') -1) else b.fmedianame end) as medianame,
                        1 as level,
                        a.fadowner as adowner,
                        CONCAT(\"0\",a.fmedia_class) as mediaclassid,
                        a.fillegal as illegalcontent,
                        a.fillegal_code as expressioncodes,
                        a.fexpressions as expressions,
                        c.fname as mediaowner,
                        e.ffullname as adclass,
                        '' as `describe`,
                        a.fid as clue_uid,
                        a.fregion_id as region_code
                    FROM
                        tbn_illegal_ad a
                    JOIN tmedia b ON b.fid = a.fmedia_id AND b.fid = b.main_media_id
                    JOIN tmediaowner c ON a.fmediaownerid = c.fid
                    JOIN tadclass e ON a.fad_class_code = e.fcode
                    WHERE
                        fcustomer = 100000 AND  fregion_id = 320100");

        foreach ($res as $k=>$v){
            $codes = explode(';',$v['expressioncodes']);
            $confirmations = M("tillegal")->where(['fcode'=>['in',$codes]])->getField('GROUP_CONCAT(fconfirmation separator ";") as confirmations');
            $res[$k]['confirmations'] = $confirmations;
        }

        $this->ajaxReturn($res);

    }


	/*同步电视数据到分析型数据库*/
	public function tvdata_adb(){
		
		header("Content-type: text/html; charset=utf-8"); 
		
		$where = array();
		$limit = 2000;
		$issueList = M('ttvissue')
									->field('ttvissue.*,tvissuesyn.fid as synfid')
									->join('tvissuesyn on tvissuesyn.fissueid = ttvissue.ftvissueid')
									->where($where)
									->order('tvissuesyn.fid asc')
									->limit($limit)->select();							
		$count_issueList = count($issueList);
		$w_last_id = $issueList[$count_issueList - 1]['synfid'];

		echo '当前主键ID:'.$w_last_id.'		数据集数量:'.$count_issueList.'		';

		$a_data_all = array();
		$min_synfid = 0;//最小的同步表主键id
		$max_synfid = 0;//最大的同步表主键id
		//var_dump($issueList);
		foreach($issueList as $issueKey => $issue){
			if($issueKey == 0){
				$min_synfid = $issue['synfid'];//最小的同步表主键id
			}
			if(intval($issue['synfid']) > $max_synfid){
				$max_synfid = intval($issue['synfid']);//最大的同步表主键id
			}
			
			$samAdInfo = S('tv2adb_sam_'.$issue['ftvsampleid']);
			
			if($samAdInfo === false){
				$samAdInfo = M('ttvsample')
											->field('
														ttvsample.fversion as fsamversion,
														tad.fadname,
														tad.fbrand,
														tadowner.fname as fadownername,
														ttvsample.fillegaltypecode,
														tad.fadclasscode,
														ttvsample.fspokesman,
														ttvsample.fadmanuno,
														ttvsample.fmanuno,
														ttvsample.fadapprno,
														ttvsample.fapprno,
														ttvsample.fadent,
														ttvsample.fent,
														ttvsample.fentzone,
														ttvsample.fexpressioncodes,
														ttvsample.favifilename as fsamfilekey
														')
											->join('tad on tad.fadid = ttvsample.fadid')
											->join('tadowner on tadowner.fid = tad.fadowner')
											->where(array('ttvsample.fid'=>$issue['ftvsampleid']))
											->find();//查询样本及广告信息				
				S('tv2adb_sam_'.$issue['ftvsampleid'],$samAdInfo,86400*15);	//把查询到的数据写入缓存	
			}
			
			$mediaInfo = S('issue2adb_media_'.$issue['fmediaid']);
			if($mediaInfo === false){
				$mediaInfo = M('tmedia')
										->field('tmedia.fmedianame,tmediaowner.fregionid,tmedia.fmediaownerid')
										->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构
										->where(array('tmedia.fid'=>$issue['fmediaid']))
										->find();	//查询媒介、机构、地区等信息
					
				S('issue2adb_media_'.$issue['fmediaid'],$mediaInfo,86400*15);		//把查询到的数据写入缓存		
			}
			
			if(!$samAdInfo || !$mediaInfo){//如果没有对应的样本或没有对应的媒介，则这条信息不导入
				continue;
			}

			
			//注：由于分析型数据库不能兼容数据类型，所有非数字型字段都统一要加【单引号】
			$a_data = array();
			$a_data['fissueid'] = intval($issue['ftvissueid']);//发布id
			$a_data['fsampleid'] = intval($issue['ftvsampleid']);//样本id
			$a_data['fadname'] = strval($samAdInfo['fadname']);//广告名称
			$a_data['fbrand'] = strval($samAdInfo['fbrand']);//品牌
			$a_data['fadclasscode'] = strval($samAdInfo['fadclasscode']);//广告分类code
			$a_data['fsamversion'] = strval($samAdInfo['fsamversion']);//版本说明
			$a_data['fspokesman'] = strval($samAdInfo['fspokesman']);//代言人
			$a_data['fadmanuno'] = strval($samAdInfo['fadmanuno']);//广告中标识的生产批准文号
			$a_data['fmanuno'] = strval($samAdInfo['fmanuno']);//生产批准文号
			$a_data['fadapprno'] = strval($samAdInfo['fadapprno']);//广告中标识的广告批准文号
			$a_data['fapprno'] = strval($samAdInfo['fapprno']);//广告批准文号
			$a_data['fadent'] = strval($samAdInfo['fadent']);//广告中标识的生产企业（证件持有人）名称
			$a_data['fent'] = strval($samAdInfo['fent']);//生产企业（证件持有人）名称
			$a_data['fentzone'] = strval($samAdInfo['fentzone']);//生产企业（证件持有人）所在地区
			$a_data['fsamfilekey'] = strval($samAdInfo['fsamfilekey']);//样本文件路径
			$a_data['fmedianame'] = strval($mediaInfo['fmedianame']);//发布媒介名称
			$a_data['fmediaid'] = intval($issue['fmediaid']);//发布媒介id
			$a_data['fmediaownerid'] = intval($mediaInfo['fmediaownerid']);//发布媒介机构id
			$a_data['fissuedate'] = date('Y-m-d',strtotime($issue['fissuedate']));//发布日期
			$a_data['fstarttime'] = strtotime( $issue['fstarttime']);//发布开始时间
			$a_data['fendtime'] = strtotime( $issue['fendtime']);//发布结束时间
			$a_data['fregionid'] = intval($mediaInfo['fregionid']);//发布媒介地区id
			$a_data['fillegaltypecode'] = intval($samAdInfo['fillegaltypecode']);//违法类型编码
			$a_data['fexpressioncodes'] = strval($samAdInfo['fexpressioncodes']);//违法表现代码，“;”分隔
			$a_data['fissuetype'] = strval($issue['fissuetype']);//发布类型
			$a_data['flengthmsec'] = intval($issue['flength']) * 1000;//发布时长(毫秒)
			$a_data['famounts'] = 0;//刊例价
			$a_data['fadownername'] = strval($samAdInfo['fadownername']);//广告主
			$a_data['fsyntime'] = date('Y-m-d H:i:s');//数据同步时间
			
			$a_data_all[] = $a_data;	
			
		}


		$del_state = M('tvissuesyn')->where(array('fid'=>array('between',array($min_synfid,$max_synfid))))->delete();//同步完成后删除表记录
		
		$plan_in_count = count($a_data_all);//计划写入的数据量
		$e_sql = A('Common/System','Model')->insert_sql('ttvdata',$a_data_all);//组装sql

		if($e_sql){
			$execute_ret = intval(M('ttvdata','',C('ADB_DB'))->execute($e_sql));
		}

		
		echo '计划写入:'.intval($plan_in_count).'		';

		echo '写入成功:'.intval($execute_ret);

		
	}
	
	/*同步广播数据到分析型数据库*/
	public function bcdata_adb(){
		
		header("Content-type: text/html; charset=utf-8"); 
		
		$where = array();
		$limit = 2000;
		$issueList = M('tbcissue')
									->field('tbcissue.*,bcissuesyn.fid as synfid')
									->join('bcissuesyn on bcissuesyn.fissueid = tbcissue.fbcissueid')
									->where($where)
									->order('bcissuesyn.fid asc')
									->limit($limit)->select();							
		$count_issueList = count($issueList);
		$w_last_id = $issueList[$count_issueList - 1]['synfid'];

		echo '当前主键ID:'.$w_last_id.'		数据集数量:'.$count_issueList.'		';

		$a_data_all = array();
		$min_synfid = 0;//最小的同步表主键id
		$max_synfid = 0;//最大的同步表主键id
		foreach($issueList as $issueKey => $issue){
			if($issueKey == 0){
				$min_synfid = $issue['synfid'];//最小的同步表主键id
			}
			if(intval($issue['synfid']) > $max_synfid){
				$max_synfid = intval($issue['synfid']);//最大的同步表主键id
			}
			$samAdInfo = S('bc2adb_sam_'.$issue['fbcsampleid']);
			
			if($samAdInfo === false){
				$samAdInfo = M('tbcsample')
											->field('
														tbcsample.fversion as fsamversion,
														tad.fadname,
														tad.fbrand,
														tadowner.fname as fadownername,
														tbcsample.fillegaltypecode,
														tad.fadclasscode,
														tbcsample.fspokesman,
														tbcsample.fadmanuno,
														tbcsample.fmanuno,
														tbcsample.fadapprno,
														tbcsample.fapprno,
														tbcsample.fadent,
														tbcsample.fent,
														tbcsample.fentzone,
														tbcsample.fexpressioncodes,
														tbcsample.favifilename as fsamfilekey
														')
											->join('tad on tad.fadid = tbcsample.fadid')
											->join('tadowner on tadowner.fid = tad.fadowner')
											->where(array('tbcsample.fid'=>$issue['fbcsampleid']))
											->find();//查询样本及广告信息
				S('bc2adb_sam_'.$issue['fbcsampleid'],$samAdInfo,86400*15);	//把查询到的数据写入缓存								
			}		
			$mediaInfo = S('issue2adb_media_'.$issue['fmediaid']);
			if($mediaInfo === false){
				$mediaInfo = M('tmedia')
										->field('tmedia.fmedianame,tmediaowner.fregionid,tmedia.fmediaownerid')
										->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构
										->where(array('tmedia.fid'=>$issue['fmediaid']))
										->find();	//查询媒介、机构、地区等信息	
			
				S('issue2adb_media_'.$issue['fmediaid'],$mediaInfo,86400*15);		//把查询到的数据写入缓存		
			}
			if(!$samAdInfo || !$mediaInfo){//如果没有对应的样本或没有对应的媒介，则这条信息不导入
				continue;
			}

			
			//注：由于分析型数据库不能兼容数据类型，所有非数字型字段都统一要加【单引号】
			$a_data = array();
			$a_data['fissueid'] = intval($issue['fbcissueid']);//发布id
			$a_data['fsampleid'] = intval($issue['fbcsampleid']);//样本id
			$a_data['fadname'] = strval($samAdInfo['fadname']);//广告名称
			$a_data['fbrand'] = strval($samAdInfo['fbrand']);//品牌
			$a_data['fadclasscode'] = strval($samAdInfo['fadclasscode']);//广告分类code
			$a_data['fsamversion'] = strval($samAdInfo['fsamversion']);//版本说明
			$a_data['fspokesman'] = strval($samAdInfo['fspokesman']);//代言人
			$a_data['fadmanuno'] = strval($samAdInfo['fadmanuno']);//广告中标识的生产批准文号
			$a_data['fmanuno'] = strval($samAdInfo['fmanuno']);//生产批准文号
			$a_data['fadapprno'] = strval($samAdInfo['fadapprno']);//广告中标识的广告批准文号
			$a_data['fapprno'] = strval($samAdInfo['fapprno']);//广告批准文号
			$a_data['fadent'] = strval($samAdInfo['fadent']);//广告中标识的生产企业（证件持有人）名称
			$a_data['fent'] = strval($samAdInfo['fent']);//生产企业（证件持有人）名称
			$a_data['fentzone'] = strval($samAdInfo['fentzone']);//生产企业（证件持有人）所在地区
			$a_data['fsamfilekey'] = strval($samAdInfo['fsamfilekey']);//样本文件路径
			$a_data['fmedianame'] = strval($mediaInfo['fmedianame']);//发布媒介名称
			$a_data['fmediaid'] = intval($issue['fmediaid']);//发布媒介id
			$a_data['fmediaownerid'] = intval($mediaInfo['fmediaownerid']);//发布媒介机构id
			$a_data['fissuedate'] = date('Y-m-d',strtotime($issue['fissuedate']));//发布日期
			$a_data['fstarttime'] = strtotime( $issue['fstarttime']);//发布开始时间
			$a_data['fendtime'] = strtotime( $issue['fendtime']);//发布结束时间
			$a_data['fregionid'] = intval($mediaInfo['fregionid']);//发布媒介地区id
			$a_data['fillegaltypecode'] = intval($samAdInfo['fillegaltypecode']);//违法类型编码
			$a_data['fexpressioncodes'] = strval($samAdInfo['fexpressioncodes']);//违法表现代码，“;”分隔
			$a_data['fissuetype'] = strval($issue['fissuetype']);//发布类型
			$a_data['flengthmsec'] = intval($issue['flength']) * 1000;//发布时长(毫秒)
			$a_data['famounts'] = 0;//刊例价
			$a_data['fadownername'] = strval($samAdInfo['fadownername']);//广告主
			$a_data['fsyntime'] = date('Y-m-d H:i:s');//数据同步时间
			
			$a_data_all[] = $a_data;	
			
		}

		$del_state = M('bcissuesyn')->where(array('fid'=>array('between',array($min_synfid,$max_synfid))))->delete();//同步完成后删除表记录
		
		$plan_in_count = count($a_data_all);//计划写入的数据量
		$e_sql = A('Common/System','Model')->insert_sql('tbcdata',$a_data_all);//组装sql

		if($e_sql){
			$execute_ret = intval(M('tbcdata','',C('ADB_DB'))->execute($e_sql));
		}

		
		echo '计划写入:'.intval($plan_in_count).'		';

		echo '写入成功:'.intval($execute_ret);

		
	}
	
	
	

}