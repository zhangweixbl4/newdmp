<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>设备管理</title>

    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->

    <link rel="shortcut icon" href="favicon.ico">
    <link href="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/css/bootstrap.min.css-v=3.3.5.css" rel="stylesheet">
    <link href="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/css/animate.min.css" rel="stylesheet">
    <link href="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/css/style.min.css-v=4.0.0.css" rel="stylesheet">
    <link href="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="http://publichz.oss-cn-hangzhou.aliyuncs.com/regionSelect/css.css" rel="stylesheet">
    <link href="/Application/Admin/View/Public/equipmentManagement.css" rel="stylesheet">
    <link href="/Application/Admin/View/Public/page.css" rel="stylesheet">
</head>

<body class="gray-bg">
    <div style="padding:8px;">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal m-t" action="{:U()}" id="Form">
                    {:form_hidden()}
                    <div class="container-fluid">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right">
                                        <button id="addBtn" type="button" class="btn btn-primary"><i class="fa fa-plus-square"></i> 新增</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right" style="width:10%">设备名称：</td>
                                    <td style="width:40%"><input placeholder="请输入查询关键字" name="keyword" type="text" class="form-control input-sm"
                                            value="{:I('get.keyword')}"></td>
                                    <td class="text-right" style="width:10%">地区：</td>
                                    <td style="width:40%"><input type="text" placeholder="请选择地区" id="address" name="region_fullname" class="address form-control input-sm"
                                            value="{:I('get.region_fullname')}">
                                        <input type="hidden" name="region_id" id="region_id" value="{:I('get.region_id')}">
                                        <div id="region-select" style="display:none">
                                            <ul class="addr_tab" role="tablist">
                                            </ul>
                                            <ul class="addr_area-list">
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">采集点：</td>
                                    <td>
                                        <div class="input-group">
                                            <input type="text" placeholder="请输入采集点名称" class="form-control input-sm" name="collect_name" id="fcollected2" value="{:I('get.collect_name')}">
                                            <input type="hidden" value="{:I('get.collect_id')}" name="collect_id" id="collect_id">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                <ul class="dropdown-menu dropdown-menu-right" role="menu"></ul>
                                            </div>
                                        </div>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-right">类型：</td>
                                    <td colspan="2">
                                        <div class="checkbox i-checks">
                                            <foreach name="devicetypeList" item="devicetype">
                                                <label>
                                                    <input <if condition=" in_array($devicetype['fid'],I('devicetype')) ">checked</if> type="checkbox" name="devicetype[]" value="{$devicetype.fid}"> 
                                                    <i class="{$devicetype.icon}"></i> {$devicetype.ftype}
                                                </label>
                                            </foreach>
                                        </div>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-primary" id="Submit"><i class="fa fa-search"></i> 搜索</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-content clearfix" style="padding-left:0px;padding-right:0px;">
                <foreach name="deviceList" item="device">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <div class="div_list" style="position:relative;">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 leftBlock text-center" fid="{$device.fid}">
                                <i class="{$device.devicetype_icon} icon"></i><br>
                                <span class="badge">{$device.devicetype_name}</span>
                            </div>
                            <div class="col-lg-8  col-md-8 col-sm-8 col-xs-8 InfoBlock" fid="{$device.fid}">
                                <p class="h4">{$device.fname}</p>
                                <p>设备编码:{$device.fcode}</p>
                                <p class="c_name">采集点名称:{$device.collect_name}</p>
                                <p>媒体数量:{$device.media_count}</p>
                            </div>
                            <span class="badge badge-primary changeBtn" fid="{$device.fid}"><i class="fa fa-pencil-square-o"></i> 修改</span>
                        </div>
                    </div>
                </foreach>
                <empty name="deviceList">
                    <h5 class="h5 text-center text-primary">没有找到您想要的数据！</h5>
                </empty>
                <div class="pagin">{$page}</div>
            </div>
        </div>
    </div>
    <!--详细信息弹窗-->
    <div class="modal inmodal" id="infoModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceIn">
                <div class="modal-header">
                    <button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">设备信息</h4>
                </div>
                <div class="container-fluid">
                    <form class="form-horizontal m-t">
                        <div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2"> 设备名称：</label>
                            <div class="col-sm-5" id="fname"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2"> 设备编码：</label>
                            <div class="col-sm-5" id="fcode"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2"> 采集点地址：</label>
                            <div class="col-sm-5" id="fcollectid"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2"> 设备类型：</label>
                            <div class="col-sm-5" id="fdevicetypeid"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2"> 设备技术参数：</label>
                            <div class="col-sm-5" id="ftechparam"></div>
                        </div>
                        <!--<div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2"> 设备运行参数：</label>
                            <div class="col-sm-5" id="frunparam"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2"> 运行参数更新时间：</label>
                            <div class="col-sm-5" id="frunparamupdatetime"></div>
                        </div>-->
                        <div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2"> 设备状态：</label>
                            <div class="col-sm-5">正在运行</div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2"> 运行状态：</label>
                            <div class="col-sm-5">正常</div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="delBtn" style="float:left" fid=""><i class="fa fa-trash"></i> 删除设备</button>
                    <button type="button" class="btn btn-white" id="close">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!--修改信息弹窗-->
    <div class="modal inmodal" id="changeModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content animated bounceIn">
                <div class="modal-header">
                    <button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">修改信息</h4>
                </div>
                <div class="container-fluid">
                    <form class="form-horizontal m-t">
                        <div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2"><i class="text-danger">*</i> 采集点：</label>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm fcollectid" id="fcollected" placeholder="请输入采集点">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu dropdown-menu-right" role="menu"></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2"><i class="text-danger">*</i> 设备类型：</label>
                            <div class="col-sm-5">
                                <select class="form-control fdevicetypeid" required="" aria-required="true">
                                    <option value="">请选择设备类型</option>
                                    <foreach name="devicetypeList" item="devicetype">
                                        <option value="{$devicetype.fid}">{$devicetype.ftype}</option>
                                    </foreach>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2"><i class="text-danger">*</i> 设备序列号：</label>
                            <div class="col-sm-5">
                                <input placeholder="请输入设备序列号" minlength="2" type="text" class="form-control input-sm fcode" required="" aria-required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2"><i class="text-danger">*</i> 设备名称：</label>
                            <div class="col-sm-5">
                                <input placeholder="请输入设备名称" minlength="2" type="text" class="form-control input-sm fname" required="" aria-required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label col-sm-offset-2"><i class="text-danger">*</i> 设备技术参数：</label>
                            <div class="col-sm-5">
                                <input placeholder="请输入设备技术参数" minlength="2" type="text" class="form-control input-sm ftechparam" required="" aria-required="true">
                            </div>
                        </div>
                        <div class="form-group" id="backMsg" style="display:none">
                            <label class="col-sm-3 control-label col-sm-offset-2 text-danger">提示：</label>
                            <div class="col-sm-5 text-danger msg" style="padding-top:7px;"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary save">保存</button>
                </div>
            </div>
        </div>
    </div>
    <!--设备状态-->
    <div class="modal inmodal" id="deviceModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceIn">
                <div class="modal-header">
                    <button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">设备信息</h4>
                </div>
                <div class="container-fluid">
                    <h4 class="h4 text-center">磁盘占用情况</h4>
                    <div id="disk" style="height:260px;"></div>
                    <div class="col-sm-6">
                        <h4 class="h4 text-center">内存占用情况</h4>
                        <div id="CPU" style="height:260px;"></div>
                    </div>
                    <div class="col-sm-6">
                        <h4 class="h4 text-center">CPU占用情况</h4>
                        <div id="memory" style="height:260px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="http://publichz.oss-cn-hangzhou.aliyuncs.com/jquery/jquery-3.1.1.min.js"></script>
    <script src="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/js/bootstrap.min.js-v=3.3.5"></script>
    <script src="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/js/contabs.min.js"></script>
    <script src="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/js/plugins/iCheck/icheck.min.js"></script>
    <script src="http://publichz.oss-cn-hangzhou.aliyuncs.com/bootstrap-3.3.7-dist/suggest/bootstrap-suggest.min.js"></script>
    <script src="http://publichz.oss-cn-hangzhou.aliyuncs.com/hPlus/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="http://publichz.oss-cn-hangzhou.aliyuncs.com/bootstrap-3.3.7-dist/suggest/bootstrap-suggest.min.js"></script>
    <script src="http://publichz.oss-cn-hangzhou.aliyuncs.com/echarts/echarts.min.js"></script>
    <script src="http://publichz.oss-cn-hangzhou.aliyuncs.com/regionSelect/location.js"></script>
    <script src="/Application/Admin/View/Public/common.js"></script>
    <script>
        $(function () { $(".i-checks").iCheck({ checkboxClass: "icheckbox_square-green", radioClass: "iradio_square-green", }) });
    </script>
    <script>
        $(function () {
            $('.leftBlock').click(function () {
                let fid = $(this).attr('fid');
                $('#deviceModal').modal('show');
                $.post('{:U("Admin/Device/get_device_run_param")}', { 'fid': fid }, function (req) {
                    let diskName = [];
                    let diskFree = [];
                    let diskUse = [];
                    let cpuUse = (req.cpu_use_ratio * 100).toFixed(2);
                    let memoryUse = (req.memory_use_ratio * 100).toFixed(2);
                    for (let i = 0; i < req.disk.length; i++) {
                        diskName.unshift(req.disk[i].name);
                        diskFree.unshift(req.disk[i].free);
                        diskUse.unshift(req.disk[i].use);
                    }
                    let disk = echarts.init(document.getElementById('disk'));
                    let diskOption = {
                        tooltip: {
                            trigger: 'axis',
                            axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                                type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                            }
                        },
                        color: ['#f8ac59', '#1ab394'],
                        legend: {
                            data: ['已使用', '未使用']
                        },
                        grid: {
                            left: '3%',
                            right: '4%',
                            bottom: '3%',
                            containLabel: true
                        },
                        xAxis: {
                            type: 'value'
                        },
                        yAxis: {
                            type: 'category',
                            data: diskName
                        },
                        series: [
                            {
                                name: '已使用',
                                type: 'bar',
                                stack: '总量',
                                label: {
                                    normal: {
                                        show: true,
                                        position: 'insideRight'
                                    }
                                },
                                data: diskUse
                            },
                            {
                                name: '未使用',
                                type: 'bar',
                                stack: '总量',
                                label: {
                                    normal: {
                                        show: true,
                                        position: 'insideRight'
                                    }
                                },
                                data: diskFree
                            }
                        ]
                    };
                    disk.setOption(diskOption);
                    let CPU = echarts.init(document.getElementById('CPU'));
                    let CPUOption = {
                        tooltip: {
                            formatter: "{a} <br/>{b} : {c}%"
                        },
                        toolbox: {
                            feature: {
                            }
                        },
                        series: [
                            {
                                name: '占用率',
                                type: 'gauge',
                                detail: { formatter: '{value}%', textStyle: { fontSize: 30 } },
                                data: [{ value: memoryUse, name: '内存占用率' }],
                                radius: '90%',
                                splitNumber: 20,
                                axisLine: {
                                    lineStyle: {
                                        width: 15,
                                        color: [[0.2, '#1ab394'], [0.8, '#f8ac59'], [1, '#ed5565']]
                                    },
                                },
                                splitLine: {
                                    length: 15
                                },
                                axisTick: {
                                    length: 8
                                },
                                pointer: {
                                    width: 10,
                                    length:80
                                },
                                axisLabel: {
                                    textStyle: {
                                        fontSize: 12
                                    }
                                },
                                //detail: {
                                // textStyle: {
                                //   fontSize: 30
                                //}
                                //}
                            }
                        ]
                    };
                    CPU.setOption(CPUOption);
                    setInterval(function () {
                        $.post('{:U("Admin/Device/get_device_run_param")}', { 'fid': fid }, function (req) {
                            CPUOption.series[0].data[0].value = req.cpu_use_ratio * 100;
                            CPU.setOption(CPUOption, true);
                        })
                    }, 60000);

                    let memory = echarts.init(document.getElementById('memory'));
                    let memoryOption = {
                        tooltip: {
                            formatter: "{a} <br/>{b} : {c}%"
                        },
                        toolbox: {
                            feature: {
                            }
                        },
                        legend: {     //配置legend，这里的data，要对应type为‘bar’的series数据项的‘name’名称，作为图例的说明
                            data: ['预热期', '导入期', '成长期', '成熟期', '衰退期'],
                            selectedMode: false,  //图例禁止点击
                            top: 20,
                            itemWidth: 23,
                            itemHeight: 6,
                            textStyle: {
                                color: '#707070',
                                fontStyle: 'normal',
                                fontWeight: 'normal',
                                fontFamily: 'sans-serif',
                                fontSize: 11,
                            },
                        },
                        series: [
                            {
                                name: '占用率',
                                type: 'gauge',
                                detail: { formatter: '{value}%', textStyle: { fontSize: 30 } },
                                data: [{ value: cpuUse, name: 'CPU占用率' }],
                                radius: '90%',
                                splitNumber: 20,
                                axisLine: {
                                    lineStyle: {
                                        width: 15,
                                        color: [[0.2, '#1ab394'], [0.8, '#f8ac59'], [1, '#ed5565']]
                                    },
                                },
                                splitLine: {
                                    length: 15
                                },
                                axisTick: {
                                    length: 8
                                },
                                pointer: {
                                    width: 10,
                                    length:80
                                },
                                axisLabel: {
                                    textStyle: {
                                        fontSize: 12
                                    }
                                },
                                //detail: {
                                // textStyle: {
                                //      fontSize: 30
                                // }
                                // }
                            }
                        ]
                    };
                    memory.setOption(memoryOption);
                    setInterval(function () {
                        $.post('{:U("Admin/Device/get_device_run_param")}', { 'fid': fid }, function (req) {
                            memoryOption.series[0].data[0].value = req.memory_use_ratio * 100;
                            memory.setOption(memoryOption, true);
                        })
                    }, 60000);
                    $('#deviceModal').on('shown.bs.modal', function () {
                        disk.resize();
                        CPU.resize();
                        memory.resize();
                    })

                })
            })
        })
    </script>
    <script>
        function save(fid) {
            var fname = $('.fname').val();//设备名称
            var fcode = $('.fcode').val();//设备编码或序列号
            var fcollectid = $('.fcollectid').attr('data-id');//采集点编码
            var fdevicetypeid = $('.fdevicetypeid').val();//设备类型编码
            var ftechparam = $('.ftechparam').val();//设备技术参数
            if (fid) {
                var data = {
                    'fcode': fcode,
                    'fname': fname,
                    'fcollectid': fcollectid,
                    'fdevicetypeid': fdevicetypeid,
                    'ftechparam': ftechparam,
                    'fid': fid
                }
                if (fcollectid && fcode && fname && fdevicetypeid && ftechparam) {
                    $.post('{:U("Admin/Device/add_edit_device")}', data, function (req) {
                        if (req.code == 0) {
                            $('#changeModal').modal('hide');//关闭模态框	
                            location.reload();
                        } else {
                            $('#backMsg').find('.msg').text(req.msg);//添加失败显示失败消息
                            $('#backMsg').show();
                        }
                    })
                } else {
                    $('#backMsg').find('.msg').text('请填写完整信息');//添加失败显示失败消息
                    $('#backMsg').show();
                }
            } else {
                var data = {
                    'fcode': fcode,
                    'fname': fname,
                    'fcollectid': fcollectid,
                    'fdevicetypeid': fdevicetypeid,
                    'ftechparam': ftechparam
                }
                if (fcollectid && fcode && fname && fdevicetypeid && ftechparam) {
                    $.post('{:U("Admin/Device/add_edit_device")}', data, function (req) {
                        if (req.code == 0) {
                            $('#changeModal').modal('hide');//关闭模态框	
                            location.reload();
                        } else {
                            $('#backMsg').find('.msg').text(req.msg);//添加失败显示失败消息
                            $('#backMsg').show();
                        }
                    })
                } else {
                    $('#backMsg').find('.msg').text('请填写完整信息');//添加失败显示失败消息
                    $('#backMsg').show();
                }
            }
        }
        $(function () {
            var fid;
            // 添加设备
            $('#addBtn').click(function () {
                fid = '';
                $('#changeModal').modal('show').find('.modal-title').html("添加设备");
                $('#changeModal').modal('show');
                $('.save').click(function () {
                    save(fid);
                })
            })
            // 修改设备
            $('.changeBtn').click(function () {
                $('#changeModal').modal('show').find('.modal-title').html("修改设备");
                fid = $(this).attr('fid');
                $.post('{:U("Admin/Device/ajax_device_details")}', { 'fid': fid }, function (req) {
                    $('.fname').val(req.deviceDetails.fname);//设备名称
                    $('.fcode').val(req.deviceDetails.fcode);//设备编码或序列号
                    $('.fcollectid').attr('data-id', req.deviceDetails.fcollectid);//采集点编码
                    $('.fcollectid').val(req.deviceDetails.collect_name);//采集点
                    $('.fdevicetypeid').val(req.deviceDetails.fdevicetypeid);//设备类型编码
                    $('.ftechparam').val(req.deviceDetails.ftechparam);//设备技术参数
                })
                $('#changeModal').modal('show');
                $('.save').click(function () {
                    save(fid);
                })
            })
        })
        $(function () {
            // 设备详情
            $('.InfoBlock').click(function () {
                let fid = $(this).attr('fid');
                $('#delBtn').attr('fid', fid);//删除
                $.post('{:U("Admin/Device/ajax_device_details")}', { 'fid': fid }, function (req) {
                    $('#fname').text(req.deviceDetails.fname);//设备名称
                    $('#fcode').text(req.deviceDetails.fcode);//设备编码或序列号
                    $('#fcollectid').text(req.deviceDetails.collect_name);//采集点
                    $('#fdevicetypeid').text(req.deviceDetails.devicetype_name);//设备类型编码
                    $('#ftechparam').text(req.deviceDetails.ftechparam);//设备技术参数
                })
                $('#infoModal').modal('show');
            })
            // 删除设备
            $('#delBtn').click(function () {
                let fid = $(this).attr('fid');
                $('#infoModal').modal('hide');
                let name = $(this).parents('#infoModal').find('#fname').text();
                Delete('确定要删除 ' + name + ' 吗？', function () {
                    $.post('{:U("Admin/Device/ajax_device_del")}', { 'fid': fid }, function (req) {
                        if (req.code == 0) {
                            $('#deleteModal .h2').text(req.msg);
                            setTimeout(function () {
                                $('#deleteModal').modal('hide');
                                location.reload();
                            }, 1000);
                        } else {
                            $('#deleteModal .h2').text(req.msg);
                        }
                    })
                })
            })

        })
        $('#Submit').click(function () {
            if ($('#fcollected2').val()) {
            } else {
                $('#collect_id').val('');
            }
            $('#region_id').val($('#address').attr('fid'));
            $('#Form').submit();
        })
        $(function () {
            var bsSuggest = $("#fcollected").bsSuggest({
                url: "/index.php?m=Api&c=Collect&a=get_collect_list&collect_name=",				//URL请求地址
                getDataMethod: "url",		//URL方式请求
                idField: "fid",           //每组数据的哪个字段作为 data-id，优先级高于 indexId 设置（推荐）
                keyField: "fname",        //每组数据的哪个字段作为输入框内容，优先级高于 indexKey 设置（推荐）
                allowNoKeyword: true,     //是否允许无关键字时请求数据
                showBtn: false,           //是否显示下拉按钮
                effectiveFields: ['fname', 'fcode'],            //有效显示于列表中的字段，非有效字段都会过滤，默认全部，对自定义getData方法无效
                // effectiveFieldsAlias: { fname: "设备名称", fcode: "设备编号" },//有效字段的别名对象，用于 header 的显示
                // showHeader: false, //显示 header 
            })
            $("#fcollected")
                .on('onDataRequestSuccess', function (event, result) {
                    //当 AJAX 请求数据成功时触发，并传回结果到第二个参数
                    $('.table-condensed').find('thead').remove();
                    //alert();
                    console.log(result);
                })
                .on('onSetSelectValue', function (e, keyword) {
                    //当从下拉菜单选取值时触发，并传回设置的数据到第二个参数
                    //alert(keyword.id);
                    console.log('onSetSelectValue: ', keyword);
                })
                .on('onUnsetSelectValue', function (e) {
                    //当设置了 idField，且自由输入内容时触发（与背景警告色显示同步）

                    console.log('onUnsetSelectValue');
                });
        })
        $(function () {
            // 关闭弹窗方法
            closeModal('#close,.close', '#changeModal,#infoModal,#deviceModal');
            // 自动补全待选项样式
            $('.dropdown-menu-right').css({ 'width': '244px', "left": "-242px" });
        })
        $(function () {
            var bsSuggest = $("#fcollected2").bsSuggest({
                url: "/index.php?m=Api&c=Collect&a=get_collect_list&collect_name=",				//URL请求地址
                getDataMethod: "url",		//URL方式请求
                idField: "fid",           //每组数据的哪个字段作为 data-id，优先级高于 indexId 设置（推荐）
                keyField: "fname",        //每组数据的哪个字段作为输入框内容，优先级高于 indexKey 设置（推荐）
                allowNoKeyword: true,     //是否允许无关键字时请求数据
                showBtn: false,           //是否显示下拉按钮
                effectiveFields: ['fname', 'fcode'],            //有效显示于列表中的字段，非有效字段都会过滤，默认全部，对自定义getData方法无效
                // effectiveFieldsAlias: { fname: "设备名称", fcode: "设备编号" },//有效字段的别名对象，用于 header 的显示
                // showHeader: false, //显示 header 
            })
            $("#fcollected2")
                .on('onDataRequestSuccess', function (event, result) {
                    //当 AJAX 请求数据成功时触发，并传回结果到第二个参数
                    $('.table-condensed').find('thead').remove();
                    //alert();
                    // console.log(result);
                })
                .on('onSetSelectValue', function (e, keyword) {
                    //当从下拉菜单选取值时触发，并传回设置的数据到第二个参数
                    // console.log(keyword);
                    $('#collect_id').val(keyword.id);
                    // console.log('onSetSelectValue: ', keyword);
                })
                .on('onUnsetSelectValue', function (e) {
                    //当设置了 idField，且自由输入内容时触发（与背景警告色显示同步）

                    // console.log('onUnsetSelectValue');
                });
            $('.dropdown-menu-right').css({ "left": "0px" }).parent().css({ 'display': 'block', 'top': '30px' });
            getAddress('#address', '#region-select', "{:U('Api/Region/get_region_list')}", 'fid');
        })
    </script>

</body>

</html>