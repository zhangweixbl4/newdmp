let task = {
    template: `<div v-loading="loading">
    <el-row>
        <el-col :span="4">日期</el-col>
        <el-col :span="20">
            <el-date-picker
                    v-model="task.task_date"
                    type="date"
                    placeholder="选择日期"
                    format="yyyy 年 MM 月 dd 日"
                    value-format="yyyy-MM-dd"
                    @change="changeTaskDate"
                    :disabled="this.type!=='input'"
                    clearable
            >
            </el-date-picker>
        </el-col>
    </el-row>
    <el-row style="margin-top: 10px;">
        <el-col :span="4">
            备注:
        </el-col>
        <el-col :span="20">
            <el-input v-model="task.remark"></el-input>
        </el-col>
    </el-row>
    <div style="border: 1px solid #DCDFE6;margin: 20px 0;"></div>
    <el-row>
        <el-form :model="where" :inline="true"  @keyup.enter.native="search">
            <el-form-item label="媒体类型">
                <el-cascader
                        v-model="searchTempWhere.mediaClass"
                        :options="mediaClassTree"
                        change-on-select
                ></el-cascader>
            </el-form-item>
            <el-form-item label="媒体名称">
                <el-input v-model="where.mediaName"></el-input>
            </el-form-item>
            <el-form-item label="媒体地区">
                <el-cascader
                        v-model="searchTempWhere.region"
                        :options="regionTree"
                        change-on-select
                        filterable
                ></el-cascader>
            </el-form-item>
            <el-form-item>
                <el-switch
                        v-model="searchTempWhere.onlyThisLevel"
                        active-text="本级">
                </el-switch>
            </el-form-item>
            <el-form-item label="媒介标签">
                <el-select v-model="where.mediaLabel" clearable filterable>
                    <el-option
                            v-for="item in mediaLabelList"
                            :key="item.value"
                            :label="item.label"
                            :value="item.value">
                    </el-option>
                </el-select>
            </el-form-item>
            <el-form-item>
                <el-button type="primary" @click="search">搜索</el-button>
                <el-button type="info" @click="resetWhere">重置</el-button>
                <el-button @click="allPick">当前页全部选择</el-button>
                <el-button @click="allCancelPick">当前页全部取消选择</el-button>
                <el-button @click="reversePick">当前页反选</el-button>
            </el-form-item>
        </el-form>
        <el-button class="mediaBtn" :type="item.selected ? 'success' : 'info'" v-for="(item, index) in  mediaList" @click="clickMediaList(item)" :key="'1'+item.value">{{item.label}}</el-button>
        <el-pagination
                @size-change="pageSizeChange"
                @current-change="pageChange"
                :current-page="page"
                :page-sizes="[50 , 100, 200, 400, 1000]"
                :page-size="pageSize"
                layout="total, sizes, prev, pager, next, jumper"
                :total="total">
        </el-pagination>
        <el-button class="mediaBtn" type="success" v-for="(item, index) in  originalMediaList" @click="clickMediaList(item)" :key="'2'+item.value">{{item.label}}</el-button>
    </el-row>
    <div style="text-align: center;margin-top: 10px;">
        <el-button type="primary" @click="submit">新建任务</el-button>
    </div>
</div>`,
    props: ['id'],
    data: function () {
        return {
            loading: false,
            type: 'input',
            task: {},
            mediaLabelList,
            regionTree,
            mediaClassTree,
            where: {},
            searchTempWhere: {},
            searchMediaList: [],
            originalMediaList: [],
            total: 0,
            page: 1,
            pageSize: 400,
        }
    },
    computed:{
        mediaList: function () {
            return !this.searchMediaList ? [] : this.searchMediaList.map((v,i) => {
                let index = this.selectedMediaIds.indexOf(v.value)
                v.selected = index !== -1;
                return v
            })
        },
        selectedMediaIds: function () {
            return this.originalMediaList.map(v=>v.value)
        }
    },
    methods: {
        dataInit: function () {
            this.where = {}
            this.task = {}
            this.originalMediaList = []
            this.loading = false
            this.searchMediaList = []
            this.searchTempWhere = {}
            this.where = {}
            if (this.id === 0){
                this.type = 'input'
            } else{
                this.type = 'check'
                this.getTask()
            }
        },
        getTask: function () {
            this.loading = true
            $.post('getTask', {id: this.id}).then((res) => {
                this.task = res
            })
        },
        search: function () {
            this.where.mediaClass = this.searchTempWhere.mediaClass ? this.searchTempWhere.mediaClass[this.searchTempWhere.mediaClass.length - 1] : ''
            this.where.region = this.searchTempWhere.region ? this.searchTempWhere.region[this.searchTempWhere.region.length - 1] : ''
            this.where.onlyThisLevel = this.searchTempWhere.onlyThisLevel ? 1 : 0
            this.page = 1
            this.searchReq()
        },
        searchReq: function () {
            if (!this.task.task_date){
                this.$message.warning('请先选择任务日期')
                return false
            }
            this.loading = true
            $.post('searchMedia', {
                where: this.where,
                page: this.page,
                pageSize: this.pageSize,
                task_date: this.task.task_date,
            }).then((res) => {
                if (res.code == 0){
                    this.searchMediaList = res.data
                    this.total = +res.total
                }
                this.loading = false
            })
        },
        resetWhere: function () {
            this.where = {}
            this.searchTempWhere = {}
            this.page = 1
            this.searchReq()
        },
        pageChange: function (page) {
            this.page = page
            this.searchReq()
        },
        pageSizeChange: function (val) {
            this.pageSize = val
            this.getTable()
        },
        clickMediaList: function (item) {
            let index = this.selectedMediaIds.indexOf(item.value)
            if (index === -1){
                if (item.state === '1'){
                    this.$message.warning('当前抽取日期中该媒体已被其他任务列入计划')
                    return false
                }
                this.originalMediaList.push(item)
            }else{
                this.originalMediaList.splice(index,1)
            }
        },
        changeTaskDate: function () {
            this.originalMediaList = []
            this.searchReq()
        },
        allPick: function () {
            this.mediaList.forEach((v,i) => {
                let index = this.selectedMediaIds.indexOf(v.value)
                if (index === -1 && v.state !== '1'){
                    this.originalMediaList.push(v)
                }
            })
        },
        allCancelPick: function () {
            this.mediaList.forEach((v,i) => {
                let index = this.selectedMediaIds.indexOf(v.value)
                if (index !== -1){
                    this.originalMediaList.splice(index,1)
                }
            })
        },
        reversePick: function () {
            this.mediaList.forEach((v,i) => {
                this.clickMediaList(v)
            })
        },
        submit: function () {
            if (!this.task.task_date) {
                this.$message.warning('请选择抽取的日期')
                return false
            }
            if (this.selectedMediaIds.length === 0) {
                this.$message.warning('请至少选择一个媒体')
                return false
            }
            this.task.media_ids = this.selectedMediaIds.join(',')
            $.post('addCustomerAdIssueTask', {task: this.task}).then((res) => {
                if (res.code == 0){
                    this.$emit('success', res.msg)
                } else{
                    this.$emit('error', res.msg)
                }
            })
        }
    }
}