<?php
namespace Agp\Controller;
use Think\Controller;

import('Vendor.PHPExcel');

class GoutxlsController extends BaseController{
	public function _initialize(){
		header("Content-type: text/html; charset=utf-8"); 
	}

	 /**
	 * 导出违法广告线索审核
	 * by zw
	 */
    public function out_xssh_list(){
    	session_write_close();
    	$ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $ischeck = $ALL_CONFIG['ischeck'];

	    $objPHPExcel = new \PHPExcel();  
	    $objPHPExcel
	      ->getProperties()  //获得文件属性对象，给下文提供设置资源
	      ->setCreator( "MaartenBalliauw")             //设置文件的创建者
	      ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
	      ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
	      ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
	      ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
	      ->setKeywords( "office 2007 openxmlphp")        //设置标记
	      ->setCategory( "Test resultfile");                //设置类别

	    $fregionid = I('fregionid');//行政区划
	    $fexamine = I('fexamine')?I('fexamine'):0;//线索审核状态

	    //时间条件筛选
	    $years      = I('years');//选择年份
	    $timetypes  = I('timetypes');//选择时间段
	    $timeval    = I('timeval');//选择时间
	    $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date');

	    //是否抽查模式
		if(!empty($ischeck)){
		    $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
		    //如果抽查表有数据
		    if(!empty($spot_check_data)){
		        $dates = [];//定义日期数组
		        foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
		            $year_month = '';
		            $date_str = [];
		            $year_month = substr($spot_check_data_val['fmonth'],0,7);
		            if(!empty($spot_check_data_val['condition'])){
		            	$date_str = explode(',',$spot_check_data_val['condition']);
		            }
		            foreach ($date_str as $date_str_val){
		                $dates[] = $year_month.'-'.$date_str_val;
		            }
		        }
				$where_time 	.= ' and tbn_illegal_ad_issue.fissue_date in ("'.implode('","', $dates).'")';
		    }else{
		        $where_time 	.= ' and 1=0';
		    }
		}

	    $netadtype  = I('netadtype');//平台类别
	    $search_type  = I('search_type');//搜索类别
	    $search_val   = I('search_val');//搜索值
	    if(!empty($search_val)){
	      if($search_type == 10){//媒体分类
	        $where_tia['a.fmedia_class'] = $search_val;
	      }elseif($search_type == 20){//广告类别
	        $where_tia['b.ffullname'] = array('like','%'.$search_val.'%');
	      }elseif($search_type == 30){//广告名称
	        $where_tia['a.fad_name'] = array('like','%'.$search_val.'%');
	      }elseif($search_type == 40){//发布媒体
	        $where_tia['c.fmedianame'] = array('like','%'.$search_val.'%');
	      }elseif($search_type == 50){//违法原因
	        $where_tia['a.fillegal'] = array('like','%'.$search_val.'%');
	      }
	    }

	    $where_tia['a.fstatus']     = 0;//处理状态，未处理

    	if(empty($fregionid)){
	      if(session('regulatorpersonInfo.fregulatorlevel')==0){
	        $where_tia['a.fregion_id']  = session('regulatorpersonInfo.regionid');
	      }elseif(session('regulatorpersonInfo.fregulatorlevel')==10){
	        $where_tia['a.fregion_id']  = array('like',substr(session('regulatorpersonInfo.regionid'),0,4).'%');
	      }elseif(session('regulatorpersonInfo.fregulatorlevel')==20){
	        $where_tia['a.fregion_id']  = array('like',substr(session('regulatorpersonInfo.regionid'),0,2).'%');
	      }
	    }else{
	      $where_tia['a.fregion_id']  = $fregionid;
	    }

      	$where_tia['a.fexamine'] = $fexamine;
      	
	    if(!empty($netadtype)){
	      $where_tia['a.fplatform']  = $netadtype;
	    }
	    $where_tia['a.fcustomer']  = $system_num;

	    $upname = session('regulatorpersonInfo.regulatorpname').'广告监管平台';

		$sheet = $objPHPExcel->setActiveSheetIndex(0);
		$sheet -> mergeCells('A1:I1');
		$sheet ->getStyle('A1:I1')->applyFromArray(array('font' => array ('bold' => true),'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
		$sheet ->setCellValue('A1',$upname.'-违法广告线索审核表');

	    $data = M('tbn_illegal_ad')
	      ->alias('a')
	      ->field('a.fid,a.fad_name,ifnull(x.fcount,0) as fcount,a.fillegal,a.fexpressions,a.fillegal_code, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,(case when a.fmedia_class=1 then "电视" when a.fmedia_class=2 then "广播" when a.fmedia_class=3 then "报纸" end) as fmedia_class,(case when a.fexamine=0 then "未审核" else "已审核" end) as fexamine')
	      ->join('tadclass b on a.fad_class_code=b.fcode')
	      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
	      ->join('(select distinct(fmediaid) from tmedia_temp where ftype in(0,1) and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid').') as ttp on c.fid=ttp.fmediaid')
	      ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
	      ->where($where_tia)
	      ->order('x.fstarttime desc')
	      ->select();
	      
		$sheet ->setCellValue('A2','序号');
		$sheet ->setCellValue('B2','广告名称');
		$sheet ->setCellValue('C2','发布条次');
		$sheet ->setCellValue('D2','违法原因');
		$sheet ->setCellValue('E2','违法表现代码');
		$sheet ->setCellValue('F2','违法表现');
		$sheet ->setCellValue('G2','发布媒体');
		$sheet ->setCellValue('H2','媒体类别');
		$sheet ->setCellValue('I2','状态');
		foreach ($data as $key => $value) {	
			$sheet ->setCellValue('A'.($key+3),$key+1);
			$sheet ->setCellValue('B'.($key+3),$value['fad_name']);
			$sheet ->setCellValue('C'.($key+3),$value['fcount']);
			$sheet ->setCellValue('D'.($key+3),$value['fillegal']);
			$sheet ->setCellValue('E'.($key+3),$value['fillegal_code']);
			$sheet ->setCellValue('F'.($key+3),$value['fexpressions']);
			$sheet ->setCellValue('G'.($key+3),$value['fmedianame']);
			$sheet ->setCellValue('H'.($key+3),$value['fmedia_class']);
			$sheet ->setCellValue('I'.($key+3),$value['fexamine']);
		}

		$objActSheet =$objPHPExcel->getActiveSheet();
		//给当前活动的表设置名称
		$objActSheet->setTitle('Sheet1');

		$objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$date = date('Ymdhis');
		$savefile = './Public/Xls/'.$date.'.xlsx';
		$objWriter->save($savefile);

		$ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

		D('Function')->write_log('违法广告线索审核',1,'生成成功');
		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		//即时导出下载
		// header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// header('Content-Disposition:attachment;filename="01simple.xlsx"');
		// header('Cache-Control:max-age=0');
		// $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
		// $objWriter->save( 'php://output');
    }

	/**
	 * 导出违法广告信息
	 * by zw
	 */
    public function out_anjianlist(){
    	session_write_close();
    	$ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isrelease = $ALL_CONFIG['isrelease'];
        $ischeck = $ALL_CONFIG['ischeck'];
    	
	    $objPHPExcel = new \PHPExcel();  
	    $objPHPExcel
	      ->getProperties()  //获得文件属性对象，给下文提供设置资源
	      ->setCreator( "MaartenBalliauw")             //设置文件的创建者
	      ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
	      ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
	      ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
	      ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
	      ->setKeywords( "office 2007 openxmlphp")        //设置标记
	      ->setCategory( "Test resultfile");                //设置类别

		//时间条件筛选
		$years      = I('years');//选择年份
		$timetypes  = I('timetypes');//选择时间段
		$timeval    = I('timeval');//选择时间
		$where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date',0,$isrelease);

		//是否抽查模式
		if(!empty($ischeck)){
		    $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
		    //如果抽查表有数据
		    if(!empty($spot_check_data)){
		        $dates = [];//定义日期数组
		        foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
		            $year_month = '';
		            $date_str = [];
		            $year_month = substr($spot_check_data_val['fmonth'],0,7);
		            if(!empty($spot_check_data_val['condition'])){
		            	$date_str = explode(',',$spot_check_data_val['condition']);
		            }
		            foreach ($date_str as $date_str_val){
		                $dates[] = $year_month.'-'.$date_str_val;
		            }
		        }
				$where_time 	.= ' and tbn_illegal_ad_issue.fissue_date in ("'.implode('","', $dates).'")';
		    }else{
		        $where_time 	.= ' and 1=0';
		    }
		}

	    $outype			= I('outype');//导出类型
	    $netadtype  = I('netadtype');//平台类别
	    $search_type  	= I('search_type');//搜索类别
    	$search_val   	= I('search_val');//搜索值
    	$upname = session('regulatorpersonInfo.regulatorpname').'广告监管平台';

	   	if(!empty($search_val)){
	      if($search_type == 10){//媒体分类
	        $where_tia['a.fmedia_class'] = $search_val;
	      }elseif($search_type == 20){//广告类别
	        $where_tia['b.ffullname'] = array('like','%'.$search_val.'%');
	      }elseif($search_type == 30){//广告名称
	        $where_tia['a.fad_name'] = array('like','%'.$search_val.'%');
	      }elseif($search_type == 40){//发布媒体
	        $where_tia['c.fmedianame'] = array('like','%'.$search_val.'%');
	      }elseif($search_type == 50){//违法原因
	        $where_tia['a.fillegal'] = array('like','%'.$search_val.'%');
	      }
	    }

	    if(!empty($netadtype)){
	      $where_tia['a.fplatform']  = $netadtype;
	    }

		$sheet = $objPHPExcel->setActiveSheetIndex(0);
		$sheet -> mergeCells('A1:R1');
		$sheet ->getStyle('A1:R1')->applyFromArray(array('font' => array ('bold' => true),'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));

	    $fieldstr = 'a.fid,a.fmedia_class,(case when a.fmedia_class=1 then "电视" when a.fmedia_class=2 then "广播" when a.fmedia_class=3 then "报纸" else "" end) as fmedia_class2,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal,a.fexpressions,a.favifilename,a.fjpgfilename,a.fresult,a.fresult_datetime,(case when a.fview_status=0 then "未查看" else "已查看" end) as fview_status,(case when a.fstatus=10 then "上报" when a.fstatus=20 then "复核" else "未处理" end) as fstatus,(case when a.fstatus2=16 then "立案处理完成" else "立案处理中" end) as fstatus2,(case when a.fstatus3=30 then "复核成功" else "复核失败" end) as fstatus3,tn.ffullname,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime, diffweek';//查询字段
	    switch ($outype)
		{
			case 10:
				$sheet ->setCellValue('A1',$upname.'-案件线索列表');
				//处理结果上报-案件列表
			  	$where_tia['a.fstatus']   = 0;
			    $where_tia['fwaitregid'] = 0;
			    // $regionid = substr(session('regulatorpersonInfo.regionid'),0,2);
    			// $where_tia['left(a.fregion_id,2)'] = $regionid;
	      		$where_tia['_string'] = 'c.fid in (select distinct(fmediaid) from tmedia_temp ttp where ttp.ftype in (0,1) and fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid').')';
			  	break;  
			case 20:
				$sheet ->setCellValue('A1',$upname.'-案件线索接收列表');
				//处理结果上报-案件接收列表
				$where_tia['a.fstatus']   = 0;
    			$where_tia['fwaitregid'] = session('regulatorpersonInfo.fregulatorpid');
    			break;
    		case 30:
    			$sheet ->setCellValue('A1',$upname.'-案件线索派发列表');
    			//处理结果上报-案件派发列表
				$where_tia['a.fstatus']   = 0;
			    $where_tia['fwaitregid']  = array('neq',session('regulatorpersonInfo.fregulatorpid'));
			    $where_tia['_string']     = 'locate('.session('regulatorpersonInfo.fregulatorpid').',fparticipate_regids)>0';
    			break;
    		case 40:
    			if(session('regulatorpersonInfo.fregulatorlevel')==30){
    				$sheet ->setCellValue('A1',$upname.'-国家数据复核列表');
    			}elseif(session('regulatorpersonInfo.fregulatorlevel')==20){
    				$sheet ->setCellValue('A1',$upname.'-本省数据复核列表');
    			}elseif(session('regulatorpersonInfo.fregulatorlevel')==10){
    				$sheet ->setCellValue('A1',$upname.'-本市数据复核列表');
    			}elseif(session('regulatorpersonInfo.fregulatorlevel')==0){
    				$sheet ->setCellValue('A1',$upname.'-本区县数据复核列表');
    			}
    			//处理结果上报-本地域数据复核列表
				$where_tia['a.fstatus']     = 0;
			    $where_tia['a.fwaitregid']  = 0;
			    $where_tia['_string'] = 'c.fid in (select distinct(fmediaid) from tmedia_temp ttp where ttp.ftype in (0,1) and fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid').')';
			    $regionid = substr(session('regulatorpersonInfo.regionid'),0,2);
			    $where_tia['left(a.fregion_id,2)'] = $regionid;
    			break;
    		case 50:
    			$sheet ->setCellValue('A1',$upname.'-跨地域数据复核列表');
    			//处理结果上报-跨地域数据复核列表
				$where_tia['a.fstatus']     = 0;
			    $where_tia['a.fwaitregid']  = 0;
			    $regionid = substr(session('regulatorpersonInfo.regionid'),0,2);
			    $where_tia['left(a.fregion_id,2)'] = $regionid;
			    $where_tia['_string'] = 'a.fad_name in (SELECT fad_name FROM tbn_illegal_ad,(select distinct(fmediaid) from tmedia_temp where ftype in (0,1) and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid').') ttp WHERE fcustomer="'.$system_num.'" and tbn_illegal_ad.fmedia_id=ttp.fmediaid and left(fregion_id,2)<>'.substr(session('regulatorpersonInfo.regionid'),0,2).' GROUP BY fad_name) ';
    			break;
    		case 110:
    			$sheet ->setCellValue('A1',$upname.'-违法广告清单列表');
    			//处理结果查看
    			$area = I('area');
    			$fstate = I('fstate');
    			$iscontain = I('iscontain');
    			$where_tia['_string'] = 'c.fid in (select distinct(fmediaid) from tmedia_temp ttp where ttp.ftype in (0,1) and fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid').')';

    			if($fstate != -1){
			      $where_tia['a.fstatus']   = $fstate;
			    }
			    if(!empty($area)){
			      if($area != '100000'){

			        if(!empty($iscontain)){
			        	$tregion_len = get_tregionlevel($area);
			          if($tregion_len == 1){//国家级
			            $where_tia['_string'] = ' fregion_id ='.$area;  
			          }elseif($tregion_len == 2){//省级
			            $where_tia['_string'] = ' fregion_id like "'.substr($area,0,2).'%"';
			          }elseif($tregion_len == 4){//市级
			            $where_tia['_string'] = ' fregion_id like "'.substr($area,0,4).'%"';
			          }elseif($tregion_len == 6){//县级
			            $where_tia['_string'] = ' fregion_id = "'.substr($area,0,6).'"';
			          }
			        }else{
			          $where_tia['fregion_id']  = $area;
			        }
			      }
			    }
    			break;
    		case 210:
    			$sheet ->setCellValue('A1',$upname.'-案件线索立案处理情况列表');
    			//立案调查处理
				$where_tia['a.fstatus']   = 10;
			    $where_tia['a.fresult']   = array('like','%立案%');
			    $where_tia['fwaitregid'] = session('regulatorpersonInfo.fregulatorpid');
    			break;
			default:
				$sheet ->setCellValue('A1',$upname);
		  		$where_tia['_string'] = '2=1';
		}

		$isexamine = $ALL_CONFIG['isexamine'];
	    if(in_array($isexamine, [10,20,30,40,50])){
	      $where_tia['a.fexamine'] = 10;
	    }
		$where_tia['a.fcustomer']  = $system_num;

	    $data = M('tbn_illegal_ad')
	      ->alias('a')
	      ->field($fieldstr)
	      ->join('tadclass b on a.fad_class_code=b.fcode')
	      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id')
	      ->join('tregion tn on tn.fid=a.fregion_id')
	      ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
	      ->where($where_tia)
	      ->order('x.fstarttime desc')
	      ->select();

		$sheet ->setCellValue('A2','序号');
		$sheet ->setCellValue('B2','地域');
		$sheet ->setCellValue('C2','发布媒体');
		$sheet ->setCellValue('D2','媒体分类');
		$sheet ->setCellValue('E2','广告类别');
		$sheet ->setCellValue('F2','广告名称');
		$sheet ->setCellValue('G2','播出总条次');
		$sheet ->setCellValue('H2','播出周数');
		$sheet ->setCellValue('I2','播出时间段');
		$sheet ->setCellValue('J2','涉嫌违法原因');
		$sheet ->setCellValue('K2','证据下载地址');
		$sheet ->setCellValue('L2','处理结果');
		$sheet ->setCellValue('M2','处理时间');
		$sheet ->setCellValue('N2','处理机构');
		$sheet ->setCellValue('O2','处理人');
		$sheet ->setCellValue('P2','查看状态');
		$sheet ->setCellValue('Q2','处理状态');
		$sheet ->setCellValue('R2','立案状态');
		foreach ($data as $key => $value) {	
			$sheet ->setCellValue('A'.($key+3),$key+1);
			$sheet ->setCellValue('B'.($key+3),$value['ffullname']);
			$sheet ->setCellValue('C'.($key+3),$value['fmedianame']);
			$sheet ->setCellValue('D'.($key+3),$value['fmedia_class2']);
			$sheet ->setCellValue('E'.($key+3),$value['fadclass']);
			$sheet ->setCellValue('F'.($key+3),$value['fad_name']);
			$sheet ->setCellValue('G'.($key+3),$value['fcount']);
			$sheet ->setCellValue('H'.($key+3),$value['diffweek']);
			$sheet ->setCellValue('I'.($key+3),$value['fstarttime'].'~'.$value['fendtime']);
			$sheet ->setCellValue('J'.($key+3),$value['fillegal']);
			if($value['fmedia_class']==1||$value['fmedia_class']==2){
				$sheet ->setCellValue('K'.($key+3),$value['favifilename']);
			}else{
				$sheet ->setCellValue('K'.($key+3),$value['fjpgfilename']);
			}
			$sheet ->setCellValue('L'.($key+3),$value['fresult']);
			$sheet ->setCellValue('M'.($key+3),$value['fresult_datetime']);
			$sheet ->setCellValue('N'.($key+3),$value['fresult_unit']);
			$sheet ->setCellValue('O'.($key+3),$value['fresult_user']);
			$sheet ->setCellValue('P'.($key+3),$value['fview_status']);
			if($value['fstatus']=='复核'){
				$sheet ->setCellValue('Q'.($key+3),$value['fstatus3']);
			}else{
				$sheet ->setCellValue('Q'.($key+3),$value['fstatus']);
			}
			if(strpos($value['fresult'],'立案')!==false){
	         	$sheet ->setCellValue('R'.($key+3),$value['fstatus2']);
	        }else{
	        	$sheet ->setCellValue('R'.($key+3),'未立案');
	        }
			
		}

		$objActSheet =$objPHPExcel->getActiveSheet();
		//给当前活动的表设置名称
		$objActSheet->setTitle('Sheet1');

		$objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$date = date('Ymdhis');
		$savefile = './Public/Xls/'.$date.'.xlsx';
		$objWriter->save($savefile);

		$ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

		D('Function')->write_log('查办处理情况',1,'生成成功');
		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		//即时导出下载
		// header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// header('Content-Disposition:attachment;filename="01simple.xlsx"');
		// header('Cache-Control:max-age=0');
		// $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
		// $objWriter->save( 'php://output');
    }

    /**
	 * 导出统计分析情况-自定义查询结果
	 * by zw
	 */
    public function out_tjfx_zdycjg_list(){
    	session_write_close();
    	$ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $ischeck = $ALL_CONFIG['ischeck'];

	    $objPHPExcel = new \PHPExcel();  
	    $objPHPExcel
	      ->getProperties()  //获得文件属性对象，给下文提供设置资源
	      ->setCreator( "MaartenBalliauw")             //设置文件的创建者
	      ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
	      ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
	      ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
	      ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
	      ->setKeywords( "office 2007 openxmlphp")        //设置标记
	      ->setCategory( "Test resultfile");                //设置类别

	    $regionids    = I('regionids');//行政区划组
	    $tadclass     = I('tadclass');//广告类别组
	    $mediaclass   = I('mediaclass');//媒体类别组
	    $mediaids     = I('mediaids');//媒体id组
	    $hstarttime   = I('hstarttime');//发布开始时间，天
	    $hendtime     = I('hendtime');//发布结束时间，天
	    $sstarttime   = I('sstarttime');//发布开始时间，秒
	    $sendtime     = I('sendtime');//发布结束时间，秒
	    $tadname      = I('tadname');//广告名称
	    $where_tia = '1=1';
	    //是否抽查模式
		if(!empty($ischeck)){
		    $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
		    //如果抽查表有数据
		    if(!empty($spot_check_data)){
		        $dates = [];//定义日期数组
		        foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
		            $year_month = '';
		            $date_str = [];
		            $year_month = substr($spot_check_data_val['fmonth'],0,7);
		            if(!empty($spot_check_data_val['condition'])){
		            	$date_str = explode(',',$spot_check_data_val['condition']);
		            }
		            foreach ($date_str as $date_str_val){
		                $dates[] = $year_month.'-'.$date_str_val;
		            }
		        }
				$where_tia 	.= ' and a.fissue_date in ("'.implode('","', $dates).'")';
		    }else{
		        $where_tia 	.= ' and 1=0';
		    }
		}

	    if(!empty($regionids)){
	      $where_tia .= ' and b.fregion_id in('.implode(',', $regionids).')';
	    }
	    if(!empty($tadclass)){
	      $where_tia .= ' and left(b.fad_class_code,2) in('.implode(',', $tadclass).')';
	    }
	    if(!empty($mediaclass)){
	      $where_tia .= ' and b.fmedia_class in('.implode(',', $mediaclass).')';
	    }
	    if(!empty($mediaids)){
	      $where_tia .= ' and b.fmedia_id in('.implode(',', $mediaids).')';
	    }
	    if(!empty($tadname)){
	      $where_tia .= ' and b.fad_name like "%'.$tadname.'%"';
	    }
	    if(!empty($hstarttime)||!empty($hendtime)){
	      $hstarttime = $hstarttime?$hstarttime:date('Y-m-d');
	      $hendtime   = $hendtime?$hendtime:date('Y-m-d');
	      $where_tia .= ' and a.fissue_date BETWEEN "'.$hstarttime.'" and "'.$hendtime.'"';
	    }
	    if(!empty($sstarttime)||!empty($sendtime)){
	      $sstarttime = $sstarttime?date('H:i:s',strtotime($sstarttime)):date('H:i:s');
	      $sendtime   = $sendtime?date('H:i:s',strtotime($sendtime)):date('H:i:s');
	      $where_tia .= ' and a.fstarttime>="'.$sstarttime.'" and a.fendtime<="'.$sendtime.'"';
	    }

	    $upname = session('regulatorpersonInfo.regulatorpname').'广告监管平台';

		$sheet = $objPHPExcel->setActiveSheetIndex(0);
		$sheet -> mergeCells('A1:K1');
		$sheet ->getStyle('A1:K1')->applyFromArray(array('font' => array ('bold' => true),'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
		$sheet ->setCellValue('A1',$upname.'-自定义查询结果');

	    $data = M('tbn_illegal_ad_issue')
	      ->alias('a')
	      ->field('b.fid,a.fissue_date,a.fstarttime,a.fendtime,b.fad_name,a.fpage,a.fmedia_class,(case when a.fmedia_class=1 then "电视" when a.fmedia_class=2 then "广播" when a.fmedia_class=3 then "报纸" else "" end) as fmedia_class2,d.ffullname as fadclass, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,b.fillegal,b.favifilename,b.fjpgfilename,(UNIX_TIMESTAMP(a.fendtime)-UNIX_TIMESTAMP(a.fstarttime)) as difftime')
	      ->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')
	      ->join('tmedia c on c.fid=a.fmedia_id and c.fid=c.main_media_id')
	      ->join('(select distinct(fmediaid) from tmedia_temp where ftype in(0,1) and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid').') as ttp on c.fid=ttp.fmediaid')
	      ->join('tadclass d on b.fad_class_code=d.fcode')
	      ->where($where_tia)
	      ->order('a.fid desc')
	      ->select();
		$sheet ->setCellValue('A2','序号');
		$sheet ->setCellValue('B2','媒体分类');
		$sheet ->setCellValue('C2','广告分类');
		$sheet ->setCellValue('D2','广告名称');
		$sheet ->setCellValue('E2','发布媒体');
		$sheet ->setCellValue('F2','播出时间');
		$sheet ->setCellValue('G2','开始时间');
		$sheet ->setCellValue('H2','结束时间');
		$sheet ->setCellValue('I2','时长/版面');
		$sheet ->setCellValue('J2','涉嫌违法原因');
		$sheet ->setCellValue('K2','证据下载地址');
		foreach ($data as $key => $value) {	
			$sheet ->setCellValue('A'.($key+3),$key+1);
			$sheet ->setCellValue('B'.($key+3),$value['fmedia_class2']);
			$sheet ->setCellValue('C'.($key+3),$value['fadclass']);
			$sheet ->setCellValue('D'.($key+3),$value['fad_name']);
			$sheet ->setCellValue('E'.($key+3),$value['fmedianame']);
			$sheet ->setCellValue('F'.($key+3),$value['fissue_date']);
			$sheet ->setCellValue('G'.($key+3),$value['fstarttime']);
			$sheet ->setCellValue('H'.($key+3),$value['fendtime']);
			if($value['fmedia_class']==3){
				$sheet ->setCellValue('I'.($key+3),$value['fpage']);
			}else{
				$sheet ->setCellValue('I'.($key+3),$value['difftime'].'秒');
			}
			$sheet ->setCellValue('J'.($key+3),$value['fillegal']);
			if($value['fmedia_class']==1||$value['fmedia_class']==2){
				$sheet ->setCellValue('K'.($key+3),$value['favifilename']);
			}else{
				$sheet ->setCellValue('K'.($key+3),$value['fjpgfilename']);
			}
		}

		$objActSheet =$objPHPExcel->getActiveSheet();
		//给当前活动的表设置名称
		$objActSheet->setTitle('Sheet1');

		$objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$date = date('Ymdhis');
		$savefile = './Public/Xls/'.$date.'.xlsx';
		$objWriter->save($savefile);

		$ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

		D('Function')->write_log('自定义查询',1,'生成成功');
		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		//即时导出下载
		// header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// header('Content-Disposition:attachment;filename="01simple.xlsx"');
		// header('Cache-Control:max-age=0');
		// $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
		// $objWriter->save( 'php://output');
    }

    /**
	 * 导出统计分析情况-广告名称查询结果
	 * by zw
	 */
    public function out_tjfx_zdycjg_list2(){
    	session_write_close();
    	$ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $ischeck = $ALL_CONFIG['ischeck'];
        $isrelease = $ALL_CONFIG['isrelease'];

    	//国家局系统只显示有打国家局标签媒体的数据
	    if($system_num == '100000'){
			if(session('regulatorpersonInfo.fregulatorlevel')!=30){
				$regionid = session('regulatorpersonInfo.regionid');//机构行政区划ID
				$regionid = substr($regionid , 0 , 2);
				$where['tia.fregion_id'] = array('like',$regionid.'%');
			}
	    }
		if(!empty($isrelease)){
			$where['_string']   = ' tiai.fsend_status = 2';
        }
    	
	    $objPHPExcel = new \PHPExcel();  
	    $objPHPExcel
	      ->getProperties()  //获得文件属性对象，给下文提供设置资源
	      ->setCreator( "MaartenBalliauw")             //设置文件的创建者
	      ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
	      ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
	      ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
	      ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
	      ->setKeywords( "office 2007 openxmlphp")        //设置标记
	      ->setCategory( "Test resultfile");                //设置类别

		$ad_name 	= I('name');//广告名
		$start_date = I('start_date');//开始日期
		$end_date 	= I('end_date');//结束日期
	    if($ad_name){
			$where['tia.fad_name'] = array('like','%'.$ad_name.'%');
		}
		if($start_date && $end_date){
			$where['tiai.fissue_date'] = array('between',array($start_date,$end_date));
		}
		//是否抽查模式
		if(!empty($ischeck)){
		    $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
		    //如果抽查表有数据
		    if(!empty($spot_check_data)){
		        $dates = [];//定义日期数组
		        foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
		            $year_month = '';
		            $date_str = [];
		            $year_month = substr($spot_check_data_val['fmonth'],0,7);
		            if(!empty($spot_check_data_val['condition'])){
		            	$date_str = explode(',',$spot_check_data_val['condition']);
		            }
		            foreach ($date_str as $date_str_val){
		                $dates[] = $year_month.'-'.$date_str_val;
		            }
		        }
				$where['_string'] 	.= ' tbn_illegal_ad_issue.fissue_date in ("'.implode('","', $dates).'")';
		    }else{
		        $where['_string'] 	.= ' 1=0';
		    }
		}
		
	    $upname = session('regulatorpersonInfo.regulatorpname').'广告监管平台';

		$sheet = $objPHPExcel->setActiveSheetIndex(0);
		$sheet -> mergeCells('A1:K1');
		$sheet ->getStyle('A1:K1')->applyFromArray(array('font' => array ('bold' => true),'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
		$sheet ->setCellValue('A1',$upname.'-广告名称查询结果');

	    $data = M('tbn_illegal_ad_issue')
			->alias('tiai')
			->field('
				tia.fid,
				tia.fmedia_class,
				(case when tiai.fmedia_class=1 then "电视" when tiai.fmedia_class=2 then "广播" when tiai.fmedia_class=3 then "报纸" else "" end) as fmedia_class2,
				tc.ffullname as fadclass,
				tia.fad_name,
				 (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,
				tiai.fissue_date,
				tiai.fstarttime,
				tiai.fendtime,
				tiai.fpage,
				tia.fillegal,
				tia.favifilename,
				tia.fjpgfilename,
				(UNIX_TIMESTAMP(tiai.fendtime)-UNIX_TIMESTAMP(tiai.fstarttime)) as difftime
			')
			->join('tbn_illegal_ad tia on tiai.fillegal_ad_id = tia.fid and fcustomer="'.$system_num.'"')
			->join('tmedia tm on tiai.fmedia_id = tm.fid and tm.fid=tm.main_media_id')
			->join('(select distinct(fmediaid) from tmedia_temp where ftype in(0,1) and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid').') as ttp on tm.fid=ttp.fmediaid')
			->join('tregion tn on tia.fregion_id=tn.fid'.$wherestr)
			->join('tadclass tc on tia.fad_class_code = tc.fcode')
			->where($where)
			->select();

		$sheet ->setCellValue('A2','序号');
		$sheet ->setCellValue('B2','媒体分类');
		$sheet ->setCellValue('C2','广告分类');
		$sheet ->setCellValue('D2','广告名称');
		$sheet ->setCellValue('E2','发布媒体');
		$sheet ->setCellValue('F2','播出时间');
		$sheet ->setCellValue('G2','开始时间');
		$sheet ->setCellValue('H2','结束时间');
		$sheet ->setCellValue('I2','时长/版面');
		$sheet ->setCellValue('J2','涉嫌违法原因');
		$sheet ->setCellValue('K2','证据下载地址');
		foreach ($data as $key => $value) {	
			$sheet ->setCellValue('A'.($key+3),$key+1);
			$sheet ->setCellValue('B'.($key+3),$value['fmedia_class2']);
			$sheet ->setCellValue('C'.($key+3),$value['fadclass']);
			$sheet ->setCellValue('D'.($key+3),$value['fad_name']);
			$sheet ->setCellValue('E'.($key+3),$value['fmedianame']);
			$sheet ->setCellValue('F'.($key+3),$value['fissue_date']);
			$sheet ->setCellValue('G'.($key+3),$value['fstarttime']);
			$sheet ->setCellValue('H'.($key+3),$value['fendtime']);
			if($value['fmedia_class']==3){
				$sheet ->setCellValue('I'.($key+3),$value['fpage']);
			}else{
				$sheet ->setCellValue('I'.($key+3),$value['difftime'].'秒');
			}
			$sheet ->setCellValue('J'.($key+3),$value['fillegal']);
			if($value['fmedia_class']==1||$value['fmedia_class']==2){
				$sheet ->setCellValue('K'.($key+3),$value['favifilename']);
			}else{
				$sheet ->setCellValue('K'.($key+3),$value['fjpgfilename']);
			}
		}

		$objActSheet =$objPHPExcel->getActiveSheet();
		//给当前活动的表设置名称
		$objActSheet->setTitle('Sheet1');

		$objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$date = date('Ymdhis');
		$savefile = './Public/Xls/'.$date.'.xlsx';
		$objWriter->save($savefile);

		$ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

		D('Function')->write_log('广告名称查询',1,'生成成功');
		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		//即时导出下载
		// header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// header('Content-Disposition:attachment;filename="01simple.xlsx"');
		// header('Cache-Control:max-age=0');
		// $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
		// $objWriter->save( 'php://output');
    }
}