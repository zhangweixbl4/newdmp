<?php
//行政区划控制器
namespace Admin\Controller;
use Think\Controller;
class RegionController extends BaseController {

	public function index(){
		
		if(A('Admin/Authority','Model')->authority('200201') === 0){
			exit('没有权限');//验证是否有新增权限
		}
		$this->display();
	}

	public function get_region_tree(){
		$result = A('Common/Region','Model')->getRegionTree();

		$this->ajaxReturn(array('code'=>0,'data' => $result,'msg'=>'获取成功'));
	}
	
	public function add(){
		
		$postData = file_get_contents('php://input');
		$postData = json_decode($postData,true);
		
		$fid = $postData['fid']; #行政区划代码
		$fpid = $postData['fpid'] ; #'上级代码',
		$fname = $postData['fname'] ; #'名称',
		$ffullname = $postData['ffullname'] ; #'全名',
		$flevel = $postData['flevel'] ; #'级别，1中央级，2省级，3副省级，4市级，5区县级',
		
		
		$ck = M('tregion')->where(array('fid'=>$fid))->count();
		if($ck > 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'行政区划编码已存在'));
		}

		try{//尝试写入数据
			$add_fid = M('tregion')
								->add(array(
											'fid'=>$fid,
											'fpid'=>$fpid,
											'fname'=>$fname,
											'ffullname'=>$ffullname,
											'fstate'=>1,
											'flevel'=>$flevel,
											'fcreator' => session('personInfo.fid').'_'.session('personInfo.fname'),//创建人
											'fcreatetime' => date('Y-m-d H:i:s'),//创建时间
											'fmodifier' => session('personInfo.fid').'_'.session('personInfo.fname'),//修改人
											'fmodifytime' => date('Y-m-d H:i:s'),//修改时间
									));
		}catch(Exception $error) { //如果写入失败
			$add_fid = false;
			
		} 	
		
									
		if($add_fid){
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));
		}
		

	}
	
	public function edit(){
		$postData = file_get_contents('php://input');
		$postData = json_decode($postData,true);
		
		$fid = $postData['fid']; #行政区划代码
		$fname = $postData['fname'] ; #'名称',
		$ffullname = $postData['ffullname'] ; #'全名',
		$flevel = $postData['flevel'] ; #'级别，1中央级，2省级，3副省级，4市级，5区县级',

		$editState = M('tregion')
							->where(array('fid'=>$fid))
							->save(array(
										'fname'=>$fname,
										'ffullname'=>$ffullname,
										'flevel'=>$flevel,
										'fmodifier' => session('personInfo.fid').'_'.session('personInfo.fname'),//修改人
										'fmodifytime' => date('Y-m-d H:i:s'),//修改时间
								));
									
		if($editState){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));
		}
		
	}
	
	public function del(){
		
		$postData = file_get_contents('php://input');
		$postData = json_decode($postData,true);
		
		$fid = $postData['fid']; #行政区划代码

		$ck = M('tregion')->where(array('fpid'=>$fid))->count();
		
		if($ck > 0 ){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该地区还有下级地区，不允许删除'));
		}
		$delState = M('tregion')->where(array('fid'=>$fid))->delete();
		
		if($delState){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败,原因未知'));
		}
	}
	
	
	/*异步加载地区列表*/
	public function ajax_region_list(){
		
		$fid = I('fid');//获取地区ID
		$regionList = M('tregion')->where(array('fpid'=>$fid))->select();
		if(!$regionList){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'无数据'));exit;
		}
		$this->assign('regionList',$regionList);
		$this->display();
	}
	
	/*地区详情*/
	public function ajax_region_details(){
		
		$postData = file_get_contents('php://input');
		$postData = json_decode($postData,true);
		
		$fid = $postData['fid']; #行政区划代码
		
		$regionDetails = M('tregion')->where(array('fid'=>$fid))->find();
		$this->ajaxReturn(array('code'=>0,'regionDetails'=>$regionDetails));
	}
	
	/*监管机构搜索接口*/
	public function get_region_list(){
		$fname = I('fname');//获取监管机构名称
		$where = array();
		$where['fstate'] = array('neq',-1);
		$where['fname'] = array('like','%'.$fname.'%');
		// $where['fid'] = array('neq','0');
		$where['fpid'] = array('gt',100000);
		if($fname != ''){
			$regionList = M('tregion')->field('fid,fname')->where($where)->limit(10)->select();//查询监管机构列表
		}
		$this->ajaxReturn(array('code'=>0,'value'=>$regionList));
		
	}

	/*获取上级监管机构列表*/
	public function get_pid_list(){
		$fid = I('fid');//获取点击的监管机构
		$num = 0;
		$fpid = array();
		while($fid != 0 && $num < 10){
			$info = M('tregion')->where(array('fid'=>$fid))->find();
			// var_dump($info);exit;
			if($info['fpid'] > 0 ){
				$fpid[] = $info['fpid'];
			}
			$fid = $info['fpid'];
			$num++;
		}
		sort($fpid);
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','fpid'=>$fpid));
	}
	
	/*修正广告分类的全称*/
	public function adjust_full_name(){
		set_time_limit(30);
		$mList = M('tregion')->select();
		
		foreach($mList as $m){
			
			$ffullname = $this->adjust_full_name2($m['fid'],$m['fname']);
			M('tregion')->where(array('fid'=>$m['fid']))->save(array('ffullname'=>$ffullname));
			$n++;
			
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'修正'.$n.'个地区'));
		
	}
	
	
	private function adjust_full_name2($fid = '',$ffullname = '',$num = 0){

		$v = M('tregion')->where(array('fid'=>$fid))->find();
		if($v['fpid'] <= 100000 || $num > 5){
			return $ffullname;
		}else{
			return $this->adjust_full_name2($v['fpid'],M('tregion')->where(array('fid'=>$v['fpid']))->getField('fname').'>'.$ffullname,$num+1 );
 
		}	
			
	}
	

}