<?php
namespace TaskInput\Controller;
use Think\Controller;
class ApiController extends BaseController {
	

	

    public function bc_tv_cut_err(){
		$mediaClass = I('mediaClass');//媒介类别
		$samId = I('samId');//媒介id
		$cut_err_reason = I('cut_err_reason');//剪辑错误的原因
		
		if($mediaClass != 'tv' && $mediaClass != 'bc'){
			
			$this->ajaxReturn(array('code'=>-1,'msg'=>'mediaClass错误'));
			
		}
		
		$ai_aip_url = 'http://47.96.182.117/index/saveUuid';
		
		
		
		$samInfo = M('t'.$mediaClass.'sample')->field('uuid,fmediaid,fstate')->where(array('fid'=>$samId))->find();
		if($samInfo['fstate'] != 1){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该样本状态不允许提交错误'));
		}
		$wx_id = intval(session('wx_id'));	
		$wxInfo = M('ad_input_user')->field('wx_id,nickname')->where(array('wx_id'=>$wx_id))->find();
		$e_data = array();
		$e_data['fmodifier'] = $wxInfo['wx_id'].'_'.$wxInfo['nickname'];
		$e_data['fmodifytime'] = date('Y-m-d H:i:s');
		
		$e_data['abnormal_reason'] = $cut_err_reason;
		$e_data['fstate'] = -2;
		
		
		
		
		M()->startTrans();
		$e_state = M('t'.$mediaClass.'sample')->where(array('fid'=>$samId,'fstate'=>1))->save($e_data);
		
		$issue_relation_list = M('t'.$mediaClass.'sample')->where(array('fid'=>$samId))->getField('issue_relation');//查询该样本的关联发布记录表
		$issue_relation_list = array_filter(explode(',',$issue_relation_list));//转为数组并去掉空数组
		foreach($issue_relation_list as $table_name){//循环关联的发布记录表
			M($table_name)->where(array('fsampleid'=>$samId))->delete();//删除发布记录分表数据
		}
		M('t'.$mediaClass.'issue')->where(array('f'.$mediaClass.'sampleid'=>$samId))->delete();//删除发布记录总表数据
		
		if($e_state){
			$postData = array();
			$postData = array('uuid'=>$samInfo['uuid'],'fid'=>$samInfo['fmediaid'],'error_cause'=>$cut_err_reason);
			
			$ai_api_ret = http($ai_aip_url,$postData,'POST');
			$ai_api_ret = json_decode($ai_api_ret,true);
			
			
			

		}
		
		if($ai_api_ret['code'] === 0){
			M('t'.$mediaClass.'sample')->where(array('fid'=>$samId))->delete();//删除样本
			M()->commit();
			$this->ajaxReturn(array('code'=>0,'msg'=>'成功','issue_relation_list'=>$issue_relation_list));
		}else{
			M()->rollback();
			$this->ajaxReturn(array('code'=>-1,'msg'=>'提交失败,不知道为什么'));
		}
			
		

		
	}
	/**
	 * @Des: 
	 * @Edt: yuhou.wang
	 * @Date: 2019-12-30 16:21:24
	 * @param {type} 
	 * @return: 
	 */
	public function test_add_task($media_class,$sam_id){
		$ret = A('Common/InputTask','Model')->add_task($media_class,$sam_id);
		$this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$ret]);
	}
}