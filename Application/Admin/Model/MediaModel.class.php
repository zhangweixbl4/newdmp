<?php
namespace Admin\Model;



class MediaModel{


    /*媒介标签*/
    public function mediaLabel($mediaId,$label){
		
		$mediaInfo = M('tmedia')->field('tmediaclass.fclass')->join('tmediaclass on tmediaclass.fid = left(tmedia.fmediaclassid,2)')->where(array('tmedia.fid'=>$mediaId))->find();//媒介详情
		$labelInfo = M('tlabel')->where(array('flabelclass'=>$mediaInfo['fclass'],'flabel'=>$label))->find();//查询标签详情
		if($labelInfo){
			return $labelInfo['flabelid'];
		}else{
			$flabelid = M('tlabel')->add(array('flabelclass'=>$mediaInfo['fclass'],'flabel'=>$label));//添加标签表数据记录,并获得标签ID
			return $flabelid;//返回标签ID
		}
    }

    /**
	 * 获取用户的媒介列表，用在用户权限选择
	 * $treid 机构ID
	 * by zw
	 */
    public function m_alljgmedialist($treid,$jgmedia = [], $mediaarr = [], $fcustomer){
    	if(empty($jgmedia)){
    		$trepid 	= $this->get_tregulatoraction($treid);
    		$jgmedia 	= $this->get_parentmedia($trepid,$fcustomer);
    	}
    	$do_tr = M('tregulator')->field('fpid,fkind,fregionid')->where('fid='.$treid)->find();
    	$do_ta = M('tregulatormedia')
	    	->where(' fregulatorcode='.$treid.' and fcustomer = "'.$fcustomer.'"')
	    	->getField('fmediaid',true);
	    if(empty($do_ta) && $do_tr['fkind']==2){
    		$do_ta = $jgmedia;
    	}

    	//如果未授权则继承所属单位权限
    	if(empty($mediaarr)){
    		$mediaarr = $do_ta;
    	}

    	$a 	= $do_ta;
		$b 	= $mediaarr;
		$c 	= array_merge(array_diff($a,array_diff($a, $b)));
    	if($do_tr['fkind']==2){
			$c = $this->m_alljgmedialist($do_tr['fpid'],$jgmedia,$c,$fcustomer);
    	}else{
    		if(!empty($c)){
    			$c = M('tmedia')->field('fid,fmedianame')->where(array('fid'=>array('in',$c)))->select();
    		}
    	}
    	
    	return $c;
    }

    /**
	 * 获取机构的媒介权限，用于登录
	 * $fregulatorid机构ID，$media_jurisdiction用户媒介权限
	 * by zw
	 */
	public function get_parentmedia($fregulatorid,$fcustomer){
		$do_tr = M('tregulator')->field('fregionid')->where('fid='.$fregulatorid)->find();
		//媒体权限
    	$do_tatre = M('tregulatormedia')
    	->where('fregulatorcode='.$fregulatorid.' and fcustomer = "'.$fcustomer.'"')
    	->getField('fmediaid',true);

    	if(empty($do_tatre)){
    		$do_tatre2=[];
    		$do_ta = M('tmedia')->field('tmedia.fid')->join('tmediaowner on tmediaowner.fid=tmedia.fmediaownerid and tmediaowner.fregionid='.$do_tr['fregionid'])->select();
    		foreach ($do_ta as $key => $value) {
    			array_push($do_tatre2, $value['fid']);
    		}
    		$do_tatre = $do_tatre2;
    	}
    	
    	return $do_tatre;
	}

    /**
	 * 获取用户的媒介权限
	 * $personid 用户Id,$fregulatorid 机构ID
	 * by zw
	 */
    public function m_allpersonmedialist($personid,$fcustomer){
    	if(!empty($personid)){
    		$do_nau = M('tregulatorpersonmedia') ->where('fpersonid='.$personid.' and fcustomer = "'.$fcustomer.'"') ->getField('fmediaid',true);
    	}
	   
    	return $do_nau;
    }

    /**
	 * 获取当前单位所属机构
	 * $fid 机构id
	 * by zw
	 */
	public function get_tregulatoraction($fid){
		$datatl=M('tregulator')->field('fpid,fkind')->where('fid='.$fid.' and fstate=1')->find();
		if($datatl['fkind']==1){
			$data = $fid;
		}else{
			$data = $this->get_tregulatoraction($datatl['fpid']);
		}
		return $data;
	}



}