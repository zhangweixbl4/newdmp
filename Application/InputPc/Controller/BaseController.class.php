<?php
namespace InputPc\Controller;
use Think\Controller;
class BaseController extends Controller {
	public function _initialize() {
		$direct = array(//设置无须验证的页面
			'InputPc/Index/index',
			'InputPc/Index/home',
			'InputPc/Login/out_login',
			'InputPc/Paper/add_papersample',
			'InputPc/Paper/addPaperIssue',
		);
		if(!session('wx_id') && !in_array(MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME ,$direct)){
			header('HTTP/1.1 404 Not Found');
			header("status: 404 Not Found");
			$this->ajaxReturn(array('code'=>-2,'msg'=>'登录过期, 请刷新页面重新登录'));
		}
	}
}