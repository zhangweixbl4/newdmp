<?php

namespace Gongshang\Controller;

import('Vendor.PHPExcel');
// import('Vendor.PHPExcel.IOFactory');

class TestController extends SupervisionController{

	public function _initialize(){

		header("Content-type: text/html; charset=utf-8"); 

	}
	public function test(){

	}
	//全省情况
	public function province(){
		echo '<pre>';
		$count = array();
		$where['left(tregion.fid,2)'] = 43;
		$where['issue.fissuedate'] = array('between',array('2018-02-01 00:00:00','2018-02-28 00:00:00'));
		// $where['tmedia.priority'] = array('EGT',0);
		$tv_count = M('ttvissue')
						->alias('issue')
						->field('
								tregion.fname,
								left(tregion.fid,4) as fregionid,
								count(1) as count,
								count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_ill_count,
								count(DISTINCT(ftvsampleid)) as sample_count,
								count(DISTINCT(case when ttvsample.fillegaltypecode = 30 then ftvsampleid else null end)) as sample_ill_count,
								sum(issue.flength) as total,
								sum(case when ttvsample.fillegaltypecode = 30 then issue.flength else 0 end) as serious_ill_total
							')
						->join('tmedia on tmedia.fid = issue.fmediaid')
						->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('tregion on tregion.fid = tmediaowner.fregionid','left')
						->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
						->where($where)
						->group('left(tregion.fid,4)')
						->select();
		foreach ($tv_count as $key => $value) {
			$count[$value['fregionid']]['name'] = $value['fname'];
			$count[$value['fregionid']]['count'] += $value['count'];
			$count[$value['fregionid']]['serious_ill_count'] += $value['serious_ill_count'];
			$count[$value['fregionid']]['sample_count'] += $value['sample_count'];
			$count[$value['fregionid']]['sample_ill_count'] += $value['sample_ill_count'];
			$count[$value['fregionid']]['total'] += $value['total'];
			$count[$value['fregionid']]['serious_ill_total'] += $value['serious_ill_total'];
		}
		$bc_count = M('tbcissue')
						->alias('issue')
						->field('
								tregion.fname,
								left(tregion.fid,4) as fregionid,
								count(1) as count,
								count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_ill_count,
								count(DISTINCT(fbcsampleid)) as sample_count,
								count(DISTINCT(case when tbcsample.fillegaltypecode = 30 then fbcsampleid else null end)) as sample_ill_count,
								sum(issue.flength) as total,
								sum(case when tbcsample.fillegaltypecode = 30 then issue.flength else 0 end) as serious_ill_total
							')
						->join('tmedia on tmedia.fid = issue.fmediaid')
						->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('tregion on tregion.fid = tmediaowner.fregionid','left')
						->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
						->where($where)
						->group('left(tregion.fid,4)')
						->select();
		foreach ($bc_count as $key => $value) {
			$count[$value['fregionid']]['name'] = $value['fname'];
			$count[$value['fregionid']]['count'] += $value['count'];
			$count[$value['fregionid']]['serious_ill_count'] += $value['serious_ill_count'];
			$count[$value['fregionid']]['sample_count'] += $value['sample_count'];
			$count[$value['fregionid']]['sample_ill_count'] += $value['sample_ill_count'];
			$count[$value['fregionid']]['total'] += $value['total'];
			$count[$value['fregionid']]['serious_ill_total'] += $value['serious_ill_total'];
		}
		$paper_count = M('tpaperissue')
						->alias('issue')
						->field('
								tregion.fname,
								left(tregion.fid,4) as fregionid,
								count(1) as count,
								count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_ill_count,
								count(DISTINCT(issue.fpapersampleid)) as sample_count,
								count(DISTINCT(case when tpapersample.fillegaltypecode = 30 then issue.fpapersampleid else null end)) as sample_ill_count
							')
						->join('tmedia on tmedia.fid = issue.fmediaid')
						->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('tregion on tregion.fid = tmediaowner.fregionid','left')
						->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
						->where($where)
						->group('left(tregion.fid,4)')
						->select();
		foreach ($paper_count as $key => $value) {
			$count[$value['fregionid']]['name'] = $value['fname'];
			$count[$value['fregionid']]['count'] += $value['count'];
			$count[$value['fregionid']]['serious_ill_count'] += $value['serious_ill_count'];
			$count[$value['fregionid']]['sample_count'] += $value['sample_count'];
			$count[$value['fregionid']]['sample_ill_count'] += $value['sample_ill_count'];
		}
		Vendor("PHPExcel.IOFactory");
	    $objPHPExcel = new \PHPExcel();     	   
	    $objWriter = new \PHPExcel_Writer_Excel5($objPHPExcel);  //设置保存版本格式  
	    $sheet = $objPHPExcel->getSheet(0);
	    $i = 1;
		foreach ($count as $key => $value) {		
			$count[$key]['issue_rate'] = round($value['serious_ill_count']/$value['count']*100,2).'%';
			$count[$key]['sample_rate'] = round($value['sample_ill_count']/$value['sample_count']*100,2).'%';
			if(isset($value['total'])){
				$count[$key]['total_rate'] = round($value['serious_ill_total']/$value['total']*100,2).'%';
			}
			$sheet->setCellValue('A'.$i,$count[$key]['name']);
			$sheet->setCellValue('B'.$i,$count[$key]['sample_count']);
			$sheet->setCellValue('C'.$i,$count[$key]['sample_ill_count']);
			$sheet->setCellValue('D'.$i,$count[$key]['sample_rate']);
			$sheet->setCellValue('E'.$i,$count[$key]['count']);
			$sheet->setCellValue('F'.$i,$count[$key]['serious_ill_count']);
			$sheet->setCellValue('G'.$i,$count[$key]['issue_rate']);
			$sheet->setCellValue('H'.$i,$count[$key]['total']);
			$sheet->setCellValue('I'.$i,$count[$key]['serious_ill_total']);
			$sheet->setCellValue('J'.$i,$count[$key]['total_rate']);
			$i++;
		}    
		$objWriter->save($_SERVER['DOCUMENT_ROOT'].'/LOG/province.xls');
	}
	//省属
	public function prov(){
		echo '<pre>';
		$media_class = array('01'=>'电视','02'=>'广播','03'=>'报纸');
		$count = array();
		$where['tregion.fid'] = array('in',array(430000,430900,431300,431200,430700,430400));
		$where['issue.fissuedate'] = array('between',array('2018-02-01 00:00:00','2018-02-28 00:00:00'));
		// $where['tmedia.priority'] = array('EGT',0);
		$tv_count = M('ttvissue')
						->alias('issue')
						->field('
								tregion.fname,
								tmedia.fmedianame,
								tmedia.fid as fmediaid,
								left(tmedia.fmediaclassid,2) as fmediaclassid,
								count(1) as count,
								count(case when ttvsample.fillegaltypecode = 30 then 1 else null end) as serious_ill_count,
								count(DISTINCT(ftvsampleid)) as sample_count,
								count(DISTINCT(case when ttvsample.fillegaltypecode = 30 then ftvsampleid else null end)) as sample_ill_count,
								sum(issue.flength) as total,
								sum(case when ttvsample.fillegaltypecode = 30 then issue.flength else 0 end) as serious_ill_total
							')
						->join('tmedia on tmedia.fid = issue.fmediaid')
						->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('tregion on tregion.fid = tmediaowner.fregionid','left')
						->join('ttvsample on ttvsample.fid = issue.ftvsampleid')
						->where($where)
						->group('tmedia.fid')
						->select();
		foreach ($tv_count as $key => $value) {
			$count[$value['fmediaid']]['name'] = $value['fname'];
			$count[$value['fmediaid']]['fmedianame'] = $value['fmedianame'];
			$count[$value['fmediaid']]['mediaclass'] = $media_class[$value['fmediaclassid']];
			$count[$value['fmediaid']]['count'] += $value['count'];
			$count[$value['fmediaid']]['serious_ill_count'] += $value['serious_ill_count'];
			$count[$value['fmediaid']]['sample_count'] += $value['sample_count'];
			$count[$value['fmediaid']]['sample_ill_count'] += $value['sample_ill_count'];
			$count[$value['fmediaid']]['total'] += $value['total'];
			$count[$value['fmediaid']]['serious_ill_total'] += $value['serious_ill_total'];
		}
		$bc_count = M('tbcissue')
						->alias('issue')
						->field('
								tregion.fname,
								tmedia.fmedianame,
								tmedia.fid as fmediaid,
								left(tmedia.fmediaclassid,2) as fmediaclassid,
								count(1) as count,
								count(case when tbcsample.fillegaltypecode = 30 then 1 else null end) as serious_ill_count,
								count(DISTINCT(fbcsampleid)) as sample_count,
								count(DISTINCT(case when tbcsample.fillegaltypecode = 30 then fbcsampleid else null end)) as sample_ill_count,
								sum(issue.flength) as total,
								sum(case when tbcsample.fillegaltypecode = 30 then issue.flength else 0 end) as serious_ill_total
							')
						->join('tmedia on tmedia.fid = issue.fmediaid')
						->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('tregion on tregion.fid = tmediaowner.fregionid','left')
						->join('tbcsample on tbcsample.fid = issue.fbcsampleid')
						->where($where)
						->group('tmedia.fid')
						->select();
		foreach ($bc_count as $key => $value) {
			$count[$value['fmediaid']]['name'] = $value['fname'];
			$count[$value['fmediaid']]['fmedianame'] = $value['fmedianame'];
			$count[$value['fmediaid']]['mediaclass'] = $media_class[$value['fmediaclassid']];
			$count[$value['fmediaid']]['count'] += $value['count'];
			$count[$value['fmediaid']]['serious_ill_count'] += $value['serious_ill_count'];
			$count[$value['fmediaid']]['sample_count'] += $value['sample_count'];
			$count[$value['fmediaid']]['sample_ill_count'] += $value['sample_ill_count'];
			$count[$value['fmediaid']]['total'] += $value['total'];
			$count[$value['fmediaid']]['serious_ill_total'] += $value['serious_ill_total'];
		}
		$paper_count = M('tpaperissue')
						->alias('issue')
						->field('
								tregion.fname,
								tmedia.fmedianame,
								tmedia.fid as fmediaid,
								left(tmedia.fmediaclassid,2) as fmediaclassid,
								count(1) as count,
								count(case when tpapersample.fillegaltypecode = 30 then 1 else null end) as serious_ill_count,
								count(DISTINCT(issue.fpapersampleid)) as sample_count,
								count(DISTINCT(case when tpapersample.fillegaltypecode = 30 then issue.fpapersampleid else null end)) as sample_ill_count
							')
						->join('tmedia on tmedia.fid = issue.fmediaid')
						->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
						->join('tregion on tregion.fid = tmediaowner.fregionid','left')
						->join('tpapersample on tpapersample.fpapersampleid = issue.fpapersampleid')
						->where($where)
						->group('tmedia.fid')
						->select();
		foreach ($paper_count as $key => $value) {
			$count[$value['fmediaid']]['name'] = $value['fname'];
			$count[$value['fmediaid']]['fmedianame'] = $value['fmedianame'];
			$count[$value['fmediaid']]['mediaclass'] = $media_class[$value['fmediaclassid']];
			$count[$value['fmediaid']]['count'] += $value['count'];
			$count[$value['fmediaid']]['serious_ill_count'] += $value['serious_ill_count'];
			$count[$value['fmediaid']]['sample_count'] += $value['sample_count'];
			$count[$value['fmediaid']]['sample_ill_count'] += $value['sample_ill_count'];
			$count[$value['fmediaid']]['total'] += $value['total'];
			$count[$value['fmediaid']]['serious_ill_total'] += $value['serious_ill_total'];
		}
		Vendor("PHPExcel.IOFactory");
	    $objPHPExcel = new \PHPExcel();     	   
	    $objWriter = new \PHPExcel_Writer_Excel5($objPHPExcel);  //设置保存版本格式  
	    $sheet = $objPHPExcel->getSheet(0);
	    $i = 1;
		foreach ($count as $key => $value) {		
			$count[$key]['issue_rate'] = round($value['serious_ill_count']/$value['count']*100,2).'%';
			$count[$key]['sample_rate'] = round($value['sample_ill_count']/$value['sample_count']*100,2).'%';
			if(isset($value['total'])){
				$count[$key]['total_rate'] = round($value['serious_ill_total']/$value['total']*100,2).'%';
			}
			$sheet->setCellValue('A'.$i,$count[$key]['name']);
			$sheet->setCellValue('B'.$i,$count[$key]['fmedianame']);
			$sheet->setCellValue('C'.$i,$count[$key]['mediaclass']);
			$sheet->setCellValue('D'.$i,$count[$key]['sample_count']);
			$sheet->setCellValue('E'.$i,$count[$key]['sample_ill_count']);
			$sheet->setCellValue('F'.$i,$count[$key]['sample_rate']);
			$sheet->setCellValue('G'.$i,$count[$key]['count']);
			$sheet->setCellValue('H'.$i,$count[$key]['serious_ill_count']);
			$sheet->setCellValue('I'.$i,$count[$key]['issue_rate']);
			$sheet->setCellValue('J'.$i,$count[$key]['total']);
			$sheet->setCellValue('K'.$i,$count[$key]['serious_ill_total']);
			$sheet->setCellValue('L'.$i,$count[$key]['total_rate']);
			$i++;
		}    
		$objWriter->save($_SERVER['DOCUMENT_ROOT'].'/LOG/pro.xls');
	}
	public function exportExcel(){  
		ini_set('max_execution_time', '0');
	    Vendor("PHPExcel.IOFactory");
      
	    $template = $_SERVER['DOCUMENT_ROOT'].'/LOG/template.xls';//使用模板  
	    $objPHPExcel = \PHPExcel_IOFactory::load($template);     //加载excel文件,设置模板  	   
	    $objWriter = new \PHPExcel_Writer_Excel5($objPHPExcel);  //设置保存版本格式  

	    //接下来就是写数据到表格里面去  
	    $sheet0 = $objPHPExcel->getSheet(0);
	    $sheet1 = $objPHPExcel->getSheet(1);
	    $sheet2 = $objPHPExcel->getSheet(2);
	    $sheet3 = $objPHPExcel->getSheet(3);
	    $sheet4 = $objPHPExcel->getSheet(4);
	    $sheet5 = $objPHPExcel->getSheet(5);
	    $sheet6 = $objPHPExcel->getSheet(6);  

	    // $Sheet1->setCellValue('B3',"活动名称：江南极客");  
	    // $i = 4;  
	    // foreach ($list as $row) {  
	    //     foreach ($indexKey as $key => $value){  
	    //         //这里是设置单元格的内容  
	    //         $objActSheet->setCellValue($header_arr[$key].$i,$row[$value]);  
	    //     }  
	    //     $i++;  
	    // } 
	    // $data=array();
	    // //从第一行开始读取数据
	    // for($j = 1;$j <= 55;$j++){
	    //     //从A列读取数据
	    //     for($k = 'A';$k <= 'G';$k++){
	    //         // 读取单元格
	    //         $data[$j][] = $objActSheet1->getCell("$k$j")->getValue();
	    //     } 
	    // } 
	    return $sheet2->getCell("A10")->getValue();
	    $objWriter->save($_SERVER['DOCUMENT_ROOT'].'/LOG/test.xls');
	} 
	public function import_excel($file){
	    // 判断文件是什么格式		
	    $type = pathinfo($file); 
	    $type = strtolower($type["extension"]);
	    $type=$type==='csv' ? $type : 'Excel5';
	    ini_set('max_execution_time', '0');
	    Vendor("PHPExcel.IOFactory");
	    // 判断使用哪种格式
	    $objReader = \PHPExcel_IOFactory::createReader($type);
	    $objPHPExcel = $objReader->load($file); 
	    $sheet = $objPHPExcel->getSheet(0); 
	    // 取得总行数 
	    $highestRow = $sheet->getHighestRow();     
	    // 取得总列数      
	    $highestColumn = $sheet->getHighestColumn(); 
	    //循环读取excel文件,读取一条,插入一条
	    $data=array();
	    //从第一行开始读取数据
	    for($j = 1;$j <= $highestRow;$j++){
	        //从A列读取数据
	        for($k = 'A';$k <= $highestColumn;$k++){
	            // 读取单元格
	            $data[$j][] = $sheet->getCell("$k$j")->getValue();
	        } 
	    }  
	    return $data;
	}

	public function demo(){

		echo '<pre>';
		$file = $_SERVER['DOCUMENT_ROOT']."/dmp.xls";
		$data = $this->import_excel($file);
		
		foreach ($data as $key => $value) {
			// if(strlen($value[2]) != 10 || empty($value[4])) continue;
			$res[] = array($value[0],$value[2],$value[4]);
			$name[] = $value[3];
		}
		$time = date('Y-m-d H:i:s');
		$where['fmedianame'] = array('in',$name);
		$tmediaid = M('tmedia')->where($where)->getField('fid',true);
		foreach ($tmediaid as $key => $value) {
			$arr['fmediaid'] = $value;
			$arr['flabelid'] = 242;
			$arr['fcreator'] = 'system';
			$arr['fcreatetime'] = $time;
			$arr['fstate'] = 1;
			$array[] = $arr;
		}
		// var_dump($array);
		M('tmedialabel')->addAll($array);
		echo 1;

	}
	
	public function index(){

        echo '<pre>';
        $where['left(fmediaclassid,2)'] = '03';
        $a = M('tmedia')->where($where)->getField('fmedianame',true);

        $file_path = $_SERVER['DOCUMENT_ROOT']."/paperjson.txt";
        if(file_exists($file_path)){
            $fp = fopen($file_path,"r");
            $str = fread($fp,filesize($file_path));//指定读取大小，这里把整个文件内容读取出来
            $str = str_replace("\r\n","<br />",$str);
        }
        $str = json_decode($str);

        foreach ($str as $key => $value) {
            $b[$value->paperName]['paperName'] = $value->paperName;
            $b[$value->paperName]['paperId'] = $value->paperId;
            $name[] = $value->paperName;
        }

        $result=array_diff($name,$a);

        echo date('Y-m-d H:i:s','1513297508');

	}                                  

}


?>

