<?php
namespace Admin\Controller;
class CutErrorReasonController extends ConfigManagerController
{
    /**
     * 获取一个或者全部理由
     * @param string $id
     */
    public function getCutErrorReason($id = '')
    {
        $where = [
            'state' => ['EQ', 1]
        ];
        $model = M('cut_error_reason');
        if ($id){
            $res = $model->where($where)->find($id);
            $res['mediaClass'] = explode(',', $res['media_class']);
            $this->ajaxReturn($res);
        }else{
            $keyword = I('keyword', '');
            $mediaClass = I('mediaClass', '');
            if ($keyword != ''){
                $where['reason|description'] = ['LIKE', "%$keyword%"];
            }
            if ($mediaClass != ''){
                $where['_string'] = 'FIND_IN_SET('.$mediaClass.', media_class)';
            }
            $res = $model->where($where)->select();
            $mediaClassMap = [
                '1' => '电视',
                '2' => '广播',
                '3' => '报纸',
                '13' => '网络',
            ];
            foreach ($res as $key => $value) {
                $res[$key]['reason'] = r_highlight($value['reason'], $keyword, 'red');
                $res[$key]['description'] = r_highlight($value['description'], $keyword, 'red');
                if ($value['media_class']){
                    $idsArr = explode(',', $value['media_class']);
                    $classArr = [];
                    foreach ($idsArr as $item) {
                        $classArr[] = $mediaClassMap[$item];
                    }
                    $res[$key]['media_class'] = trim(implode(',', $classArr), ',');
                }else{
                    $res[$key]['media_class'] = '无';
                }
            }
            $this->ajaxReturn($res);

        }
    }

    /**
     * 新增理由
     * @param $reason
     */
    public function postCutErrorReason($reason)
    {
        foreach ($reason as $key => $value) {
            $reason[$key] = trim($value);
        }
        $code = 0;
        if (!$reason['reason']){
            $code = -1;
            $msg = '请输入理由';
        }
        if (!$reason['media_class']){
            $code = -1;
            $msg = '请选择媒体';
        }
        if ($code === 0){
            $res = M('cut_error_reason')->add($reason);
            if ($res){
                $msg = '添加成功';
            }else{
                $code = -1;
                $msg = '添加失败';
            }
        }
        $this->ajaxReturn(compact('code', 'msg'));

    }

    /**
     * 修改理由
     * @param $id
     * @param $reason
     */
    public function putCutErrorReason($id, $reason)
    {
        return false;
        foreach ($reason as $key => $value) {
            $reason[$key] = trim($value);
        }
        $code = 0;
        if (!$reason['reason']){
            $code = -1;
            $msg = '请输入理由';
        }
        if (!$reason['media_class']){
            $code = -1;
            $msg = '请选择媒体';
        }
        $reason['media_class'] = trim($reason['media_class'], ',');
        if ($code === 0){
            $res = M('cut_error_reason')->where(['id' => ['EQ', $id]])->save($reason);
            if ($res){
                $msg = '修改成功';
            }else{
                $code = -1;
                $msg = '修改失败';
            }
        }
        $this->ajaxReturn(compact('code', 'msg'));
    }

    /**
     * 删除理由
     * @param $id
     */
    public function deleteCutErrorReason($id)
    {
        $res = M('cut_error_reason')->where(['id' => ['EQ', $id]])->save(['state' => 0]);
        if ($res){
            $msg = '删除成功';
            $code = 0;
        }else{
            $code = -1;
            $msg = '删除失败';
        }
        $this->ajaxReturn(compact('code', 'msg'));
    }

}