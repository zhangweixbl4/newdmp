<?php
//电视样本控制器
namespace Gongshang\Controller;
use Think\Controller;
class TvsampleController extends BaseController {

	public function index(){
		$regulatorpersonInfo = M('tregulatorperson')->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
		$regulatorInfo = M('tregulator')->where(array('fcode'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息
		$region_id = session('regulatorpersonInfo.regionid');
		if(I('region_id') != '') $region_id = I('region_id');
		$p = I('p',1);//当前第几页
		$pp = 36;//每页显示多少记录
		$keyword = I('keyword');//搜索关键词
		$fadname = I('fadname');// 广告名称
		$fversion = I('fversion');// 版本描述
		$fmediaid  = I('fmediaid');// 媒体ID
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID
		$fcreatetime_s = I('fcreatetime_s');// 建样时间
		if($fcreatetime_s == '') $fcreatetime_s = '2015-01-01';
		$fcreatetime_e = I('fcreatetime_e');// 建样时间
		if($fcreatetime_e == '') $fcreatetime_e = date('Y-m-d');
		// $region_id = I('region_id');//地区ID
		$this_region_id = I('this_region_id');//是否只查询本级地区
		


		$where = array();//查询条件
		
		if($region_id == 100000 || $region_id == 0 || $region_id == '') $region_id = 0;//如果传值等于100000，相当于查全国
		
		if($region_id > 0){
			if($this_region_id == 'true'){
				$where['tmediaowner.fregionid'] = $region_id;//地区搜索条件
			}else{
				$region_id_rtrim = rtrim($region_id,'00');//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmediaowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}
		}
		// var_dump($region_id);
		$where['ttvsample.fstate'] = array('neq',-1);

		if($keyword != ''){
			$where['ttvsample.fversion|tad.fadname'] = array('like','%'.$keyword.'%');
		} 
		
		if($fadname != ''){
			$where['tad.fadname'] = array('like','%'.$fadname.'%');//广告名称
		}
		if($fversion != ''){
			$where['ttvsample.fversion'] = array('like','%'.$fversion.'%');//版本描述
		}
		if($fmediaid != ''){
			$where['ttvsample.fmediaid'] = $fmediaid;//媒体ID
		}
		if(is_array($fillegaltypecode)){
			$where['ttvsample.fillegaltypecode'] = array('in',$fillegaltypecode);//违法类型代码
		}
		if($fcreatetime_s != '' || $fcreatetime_e != ''){
			$where['ttvsample.fcreatetime'] = array('between',$fcreatetime_s.','.$fcreatetime_e.' 23:59:59');
		}

		$count = M('ttvsample')
								->join('tad on tad.fadid = ttvsample.fadid')
								->join('tadowner on tadowner.fid = tad.fadowner')
								->join('tmedia on tmedia.fid = ttvsample.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数

		
		$tvsampleList = M('ttvsample')
										->field('ttvsample.*,tad.fadname,tadowner.fname as adowner_name')
										->join('tmedia on tmedia.fid = ttvsample.fmediaid')
										->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
										->join('tad on tad.fadid = ttvsample.fadid')
										->join('tadowner on tadowner.fid = tad.fadowner')
										->where($where)
										->order('ttvsample.fid desc')
										->limit($Page->firstRow.','.$Page->listRows)->select();//查询广告样本列表
									
		$tvsampleList = list_k($tvsampleList,$p,$pp);//为列表加上序号
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		//var_dump($illegaltype);
		//var_dump($tvsampleList);
		$this->assign('regionid',session('regulatorpersonInfo.regionid'));
		$this->assign('tvsampleList',$tvsampleList);//广告样本列表
		$this->assign('illegaltype',$illegaltype);//违法类型
		$this->assign('regulatorInfo',$regulatorInfo);//监管机构信息
		
		$this->assign('page',$Page->show());//分页输出
		// $this->display();
	}
	
	
		
	/*样本详情*/
	public function ajax_tvsample_details(){
		
		$fid = I('fid');//获取样本ID
		$tvsampleDetails = M('ttvsample')
										->field('ttvsample.*,tad.fadname,tadowner.fname as adowner_name,tillegaltype.fillegaltype,tmedia.fmedianame,tmedia.femail')
										->join('tad on tad.fadid = ttvsample.fadid')
										->join('tmedia on tmedia.fid = ttvsample.fmediaid')
										->join('tadowner on tadowner.fid = tad.fadowner')
										->join('tillegaltype on tillegaltype.fcode = ttvsample.fillegaltypecode')
										->where(array('ttvsample.fid'=>$fid))
										->find();//查询样本详情

		$this->ajaxReturn(array('code'=>0,'tvsampleDetails'=>$tvsampleDetails));
	}
	
	/*转入广告线索*/
	public function to_adclue(){
		$fid = I('fid');//广告样本ID
		
		$tvsampleInfo = M('ttvsample')->where(array('fid'=>$fid))->find();
		if(intval($tvsampleInfo['fillegaltypecode']) == 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'转入失败,只有违法广告能转入'));
		}
		$fmediaclass = M('tmedia')->where(array('fid'=>$tvsampleInfo['fmediaid']))->getField('fmediaclassid');//媒介类型代码
		$fmediaclass = mb_substr($fmediaclass,0,2);
		$adclue_count = M('tadclue')->where(array('fsampleid'=>$tvsampleInfo['fid'],'fmediaclass'=>$fmediaclass))->count();
		if($adclue_count > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'此样本已在线索中'));
		
		$a_data = array();
		$a_data['fsupervise'] = 0;//管理机关

		$a_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$a_data['fstate'] = 0;//状态
		$a_data['fmediaclass'] = $fmediaclass;
		$a_data['fmediaid'] = $tvsampleInfo['fmediaid'];//媒介id
		$a_data['fissuedate'] = $tvsampleInfo['fissuedate'];//发行日期
		$a_data['fsampleid'] = $tvsampleInfo['fid'];//样本ID
		$a_data['fadid'] = $tvsampleInfo['fadid'];//广告ID
		$a_data['fspokesman'] = $tvsampleInfo['fspokesman'];//代言人
		$a_data['fversion'] = $tvsampleInfo['fversion'];//版本说明
		$a_data['fapprovalid'] = $tvsampleInfo['fapprovalid'];//审批号
		$a_data['fapprovalunit'] = $tvsampleInfo['fapprovalunit'];//审批单位
		$a_data['fadlen'] = $tvsampleInfo['fadlen'];//广告样本长度
		$a_data['fillegaltypecode'] = $tvsampleInfo['fillegaltypecode'];//违法类型代码
		$a_data['fillegalcontent'] = $tvsampleInfo['fillegalcontent'];//涉嫌违法内容
		$a_data['fexpressioncodes'] = $tvsampleInfo['fexpressioncodes'];//违法表现代码
		$a_data['fexpressions'] = $tvsampleInfo['fexpressions'];//违法表现
		$a_data['fconfirmations'] = $tvsampleInfo['fconfirmations'];//认定依据
		$a_data['fpunishments'] = $tvsampleInfo['fpunishments'];//处罚依据
		$a_data['fpunishmenttypes'] = $tvsampleInfo['fpunishmenttypes'];//处罚种类及幅度
		$a_data['favifilename'] = $tvsampleInfo['favifilename'];//视频路径
		
		
		$rr = M('tadclue')->add($a_data);
		
		if($rr > 0){
			$fregulatorcode = A('Common/Regulatormedia','Model')->get_regulatormedia($tvsampleInfo['fmediaid']);//监管机构ID
			A('Common/Adcluesection','Model')->create_adcluesection($rr,0,$fregulatorcode,session('regulatorpersonInfo.fname'));//创建环节
			$this->ajaxReturn(array('code'=>0,'msg'=>'转入线索成功'));
			
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'转入线索失败,原因未知'));
		}
		
		
	}

	
	
	
	
}