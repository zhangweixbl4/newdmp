<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 数据报告
 */

class FsjbaogaoController extends BaseController
{
	/**
	 * 获取历史报告列表
	 * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function index() {
		
		$p 					= I('page', 1);//当前第几页
		$pp 				= 20;//每页显示多少记录
		$pnname 			= I('pnname');// 报告名称
		$pntype 			= I('pntype');// 报告类型
		$pnfiletype 		= I('pnfiletype');// 文件类型
		$pncreatetime 		= I('pncreatetime');//生成时间
		$system_num = getconfig('system_num');

		if (!empty($pnname)) {
			$where['pnname'] = array('like', '%' . $pnname . '%');//报告名称
		}
		if (!empty($pntype)) {
			$where['pntype'] = $pntype;//报告类型
		}
		if (!empty($pnfiletype)) {
			$where['pnfiletype'] = $pnfiletype;//文件类型
		}
		if(!empty($pncreatetime)){
			$where['_string'] = 'datediff("'.$pncreatetime.'",pncreatetime)=0';//生成时间
		}	

		$where['pntrepid'] = session('regulatorpersonInfo.fregulatorpid');
		$where['fcustomer']  = $system_num;

		$count = M('tpresentation')
			->where($where)
			->count();

		
		$data = M('tpresentation')
			->where($where)
			->order('pnendtime desc')
			->page($p,$pp)
			->select();
		if(!empty($data)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据'));
		}
	}

}