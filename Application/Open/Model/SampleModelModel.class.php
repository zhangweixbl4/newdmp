<?php
namespace Open\Model;



class SampleModelModel{


    /*新增电视发布记录*/
    public function add_tvissue($issue_data){
		
		
		$where = array();
		$where['fmediaid'] = $issue_data['fmediaid'];
		$where['fstarttime'] = array('lt',date('Y-m-d H:i:s',strtotime($issue_data['fendtime'])-1));
		$where['fendtime'] = array('gt',date('Y-m-d H:i:s',strtotime($issue_data['fstarttime'])+1));
		$where['fissuedate'] = date('Y-m-d',strtotime($issue_data['fstarttime']));
		$issueCount = M('ttvissue')->where($where)->count();//查询有无与原有发布记录重叠
		
		$flength = strtotime($issue_data['fendtime']) - strtotime($issue_data['fstarttime']);
		if($flength < 3) return $issue_data['fstarttime'].'的时长小于3秒';
		if($flength > 86400) return $issue_data['fstarttime'].'的时长大于86400秒';
		if($issueCount > 0) return $issue_data['fstarttime'].'_'.$issue_data['fendtime'].'记录已存在';
		if($issueCount == 0 && $flength >= 3 && $flength <= 86400){//没有重叠，且大于4秒
			$issueid = M('ttvissue')->add($issue_data);//添加样本表记录
		}
		

		


		if($issueid > 0){			
			$issueInfo = array();
			$issueInfo['issueid'] = strval($issueid);
			$issueInfo['sampleid'] = strval($issue_data['ftvsampleid']);
			$issueInfo['modelid'] = strval(0);
			$issueInfo['starttime'] = strval($issue_data['fstarttime']);
			$issueInfo['endtime'] = strval($issue_data['fendtime']);
			$issueInfo['u0'] = strval($issue_data['fu0']);
			$issueInfo['duration'] = strval($issue_data['fduration']);
			$issueInfo['audiosimilar'] = strval($issue_data['faudiosimilar']);
			$issueInfo['videosimilar'] = strval($issue_data['fvideosimilar']);
			return $issueInfo;
		}else{
			return '数据层写入失败';
		}
		
    }
	

	
	/*新增广播发布记录*/
    public function add_bcissue($issue_data){

		$where = array();
		$where['fmediaid'] = $issue_data['fmediaid'];
		$where['fstarttime'] = array('lt',date('Y-m-d H:i:s',strtotime($issue_data['fendtime'])-1));
		$where['fendtime'] = array('gt',date('Y-m-d H:i:s',strtotime($issue_data['fstarttime'])+1));
		$where['fissuedate'] = date('Y-m-d',strtotime($issue_data['fstarttime']));
		
		$issueCount = M('tbcissue')->where($where)->count();//查询有无与原有发布记录重叠
		
		//var_dump(M('tbcissue')->getLastSql());
		$flength = strtotime($issue_data['fendtime']) - strtotime($issue_data['fstarttime']);
		
		if($flength < 3) return $issue_data['fstarttime'].'的时长小于3秒';
		if($flength > 86400) return $issue_data['fstarttime'].'的时长大于86400秒';
		if($issueCount > 0) return $issue_data['fstarttime'].'_'.$issue_data['fendtime'].'记录已存在';
		
		if($issueCount == 0 && $flength >= 3 && $flength <= 86400){//没有重叠，且大于4秒
			$issueid = M('tbcissue')->add($issue_data);//添加样本表记录
			
		}

		if($issueid > 0){

			$issueInfo = array();
			$issueInfo['issueid'] = strval($issueid);
			$issueInfo['sampleid'] = strval($issue_data['fbcsampleid']);
			$issueInfo['modelid'] = strval(0);
			$issueInfo['starttime'] = strval($issue_data['fstarttime']);
			$issueInfo['endtime'] = strval($issue_data['fendtime']);
			$issueInfo['u0'] = strval($issue_data['fu0']);
			$issueInfo['duration'] = strval($issue_data['fduration']);
			$issueInfo['audiosimilar'] = strval($issue_data['faudiosimilar']);
			$issueInfo['videosimilar'] = strval($issue_data['fvideosimilar']);
			return $issueInfo;
		}else{
			$issueInfo = array();
			//file_put_contents('LOG/add_bcissue',M('tbcissue')->getLastSql()."\n",FILE_APPEND);
			return '数据层写入失败';			
		}			
		 

		return $issueInfo;
    }

    /*新增报纸发布记录*/
	public function add_paperissue($issue_data){
		if(M('tpaperissue')->where($issue_data)->count() == 0){
			$issueid = M('tpaperissue')->add($issue_data);//添加样本表记录
		}	

		$field = 'fpaperissueid as issueid,fpapersampleid as sampleid,fpage as page,fissuetype as type,famounts as amounts,fquantity as quantity';//需要查询的字段
		$issueInfo = M('tpaperissue')->field($field)->where(array('fpaperissueid'=>$issueid))->find();//查询发布记录信息
		
		return $issueInfo;

	}
	


}