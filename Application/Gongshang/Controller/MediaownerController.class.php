<?php
namespace Gongshang\Controller;
use Think\Controller;
class MediaownerController extends BaseController {
	
	
	

    public function index(){
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$keyword = I('keyword');//搜索关键词
		$this_region_id = I('this_region_id');//是否只查询本级地区
		$region_id = session('regulatorpersonInfo.regionid');//地区ID
		if(I('region_id') != '') $region_id = I('region_id');
		// $region_id = intval(I('region_id'));//搜索地区ID
		if($region_id == 100000) $region_id = 0;//如果传值等于100000，相当于查全国
		$regionid = intval($intval);
		$where = array();//查询条件
		$where['tmediaowner.fstate'] = array('neq',-1);
		$where['tmediaowner.fid'] = array('gt',0);
		if($region_id > 0){
			if($this_region_id == 'true'){
				$where['tmediaowner.fregionid'] = $region_id;//地区搜索条件
			}else{
				$region_id_rtrim = rtrim($region_id,'00');//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmediaowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}
		}
		
		if($keyword != ''){
			$where['tmediaowner.fname'] = array('like','%'.$keyword.'%');
		} 
		
		
		
		$count = M('tmediaowner')->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数

		
		$mediaownerList = M('tmediaowner')
										->field('tmediaowner.*,tmedia.fmediaownerid,count(tmedia.fid) as media_count')
										->join('tmedia on tmedia.fmediaownerid = tmediaowner.fid and tmedia.fstate <> -1','left')
										->where($where)
										->group('tmediaowner.fid')
										->order('tmediaowner.fid desc')
										->limit($Page->firstRow.','.$Page->listRows)->select();//查询媒介机构列表
									
		$mediaownerList = list_k($mediaownerList,$p,$pp);//为列表加上序号
		
		//var_dump($mediaownerList);
		$this->assign('mediaownerList',$mediaownerList);//媒介机构列表
		$this->assign('regionid',session('regulatorpersonInfo.regionid'));
		$this->assign('page',$Page->show());//分页输出
		$this->display();
	}
	
	
	/*添加、编辑媒介机构*/
	public function add_edit_mediaowner(){

		$fid= I('fid');//设备ID
		$fid = intval($fid);//转为数字
		$a_e_data = array();
		$a_e_data['fregaddr'] = I('fregaddr');//注册地址
		
		$a_e_data['fofficeaddr'] = I('fofficeaddr');//办公地址
		$a_e_data['fname'] = I('fname');//媒介机构名称
		$a_e_data['flinkman'] = I('flinkman');//联系人
		$a_e_data['ftel'] = I('ftel');//联系电话
		$a_e_data['fcreditcode'] = I('fcreditcode');//企业信用代码
		$a_e_data['fenterprise'] = I('fenterprise');//设立主体
		
		$a_e_data['flicence'] = I('flicence');//特许经营许可证
		$a_e_data['fregionid'] = I('fregionid');//行政区划ID
		
		$a_e_data['fstate'] = I('fstate',1);//状态，-1删除，0-无效，1-有效
		
		if($fid == 0){//判断是修改还是新增
			
			$is_repeat = M('tmediaowner')->where(array('fcreditcode'=>$a_e_data['fcreditcode']))->count();//查询企业信用代码是否重复
			//if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'企业信用代码重复'));//返回企业信用代码重复
			
			$a_e_data['fcreator'] = '系统最高管理员';//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = '系统最高管理员';//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tmediaowner')->add($a_e_data);//添加数据
			
			if($rr > 0){//判断是否添加成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));//返回失败
			}
			
		}else{//修改
			
			$is_repeat = M('tmediaowner')->where(array('fid'=>array('neq',$fid),'fcreditcode'=>$a_e_data['fcreditcode']))->count();//查询企业信用代码是否重复
			//if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'企业信用代码重复'));//返回企业信用代码重复
			$a_e_data['fmodifier'] = '系统最高管理员';//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tmediaowner')->where(array('fid'=>$fid))->save($a_e_data);//修改数据
			if($rr > 0){//判断是否修改成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));//返回失败
			}
			
		}
			
		
	}
	
	
	/*媒介机构详情*/
	public function ajax_mediaowner_details(){
		
		$fid = I('fid');//获取媒介机构ID
		$mediaownerDetails = M('tmediaowner')->where(array('fid'=>$fid))->find();
		$mediaownerDetails['region_name'] = M('tregion')->where(array('fid'=>$mediaownerDetails['fregionid']))->getField('ffullname');//行政区划全称
		$this->ajaxReturn(array('code'=>0,'mediaownerDetails'=>$mediaownerDetails));
	}
	
	
	/*删除媒介机构*/
	public function ajax_mediaowner_del(){
		$fid = I('fid');//获取媒介机构ID
		$media_count = M('tmedia')->where(array('fmediaownerid'=>$fid))->count();//查询这个媒介机构的媒介数量
		if($media_count > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败,该媒介机构还有关联的媒介'));
		$rr = M('tmediaowner')->where(array('fid'=>$fid))->save(array('fstate'=>-1));
		if($rr > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败,原因未知'));
		}
		
		
	}
	

}