<?php
namespace Api\Model;
use Think\Exception;


class DataGatherModel{


	/*按半月汇总数据*/
    public function gather_half_month($month,$mediaId,$confirmed){
		
		if(strtotime($month) < strtotime('2018-07-01')) return false;
		if($confirmed == '_confirmed' ){
			$zongju_data_confirm = M('zongju_data_confirm')->cache(true,120)->field('fid')->where(array('fmonth'=>date('Y-m-01',strtotime($month)),'dmp_confirm_state'=>2))->find();
			if(!$zongju_data_confirm) return false;//如果没有确认，直接返回
		}
		
		
		$months = array(
							array('st'=>date('Y-m-01',strtotime($month)),'et'=>date('Y-m-15',strtotime($month))),
							array('st'=>date('Y-m-16',strtotime($month)),'et'=>date('Y-m-31',strtotime($month)))
						
						);

		foreach($months as $month_arr){
			
			$where = array();//初始化查询条件
			$where['issue_date'] = array('between',array($month_arr['st'],$month_arr['et']));//查询时间范围
			$where['media_id'] = $mediaId;
			$issueList = M('media_issue_count')->field('adclass_data')->where($where)->select();//查询数据

			$half_month_data = array();
			foreach($issueList as $issue){//循环发布数据
				$adclass_data = json_decode($issue['adclass_data'],true);
				foreach($adclass_data as $adclassData){
					$half_month_data[$adclassData['fmediaid']][$adclassData['ad_class_code']]['fad_times'] += $adclassData['issue_count'];//发布次数
					$half_month_data[$adclassData['fmediaid']][$adclassData['ad_class_code']]['fad_illegal_times'] += $adclassData['illegal_30_count'];//违法发布次数
					$half_month_data[$adclassData['fmediaid']][$adclassData['ad_class_code']]['fad_illegal_times_total'] += $adclassData['illegal_30_count'] + $adclassData['illegal_20_count'];//违法发布次数(总和)
					
					$half_month_data[$adclassData['fmediaid']][$adclassData['ad_class_code']]['fad_play_len'] += $adclassData['issue_duration'];//发布时长
					$half_month_data[$adclassData['fmediaid']][$adclassData['ad_class_code']]['fad_illegal_play_len'] += $adclassData['illegal_30_duration'];//违法发布时长
					$half_month_data[$adclassData['fmediaid']][$adclassData['ad_class_code']]['fad_illegal_play_len_total'] += $adclassData['illegal_30_duration'] + $adclassData['illegal_20_duration'];//违法发布时长(总和)
					
					
				}
				
			}
			
			foreach($half_month_data as $media_id => $ad_class_List){
				$mediaInfo = M('tmedia')
										->cache(true,3600)
										->field('
													tmedia.fmediaclassid,
													tmediaowner.fregionid,
													tmedia.fmediaownerid
												')
										->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
										->where(array('tmedia.fid'=>$media_id))
										->find();
				
				$class_ad_arr = array();//初始化本次包含的分类
				foreach($ad_class_List as $ad_class_code => $ad_class_data){
					
					$this->gather_half_month_write($media_id,$month_arr['st'],$ad_class_code,array(
																									
																									'fad_times'=>$ad_class_data['fad_times'],
																									'fad_illegal_times'=>$ad_class_data['fad_illegal_times'],
																									'fad_illegal_times_total'=>$ad_class_data['fad_illegal_times_total'],
																									
																									'fad_play_len'=>$ad_class_data['fad_play_len'],
																									'fad_illegal_play_len'=>$ad_class_data['fad_illegal_play_len'],
																									'fad_illegal_play_len_total'=>$ad_class_data['fad_illegal_play_len_total'],
																									
																									'fregionid'=>$mediaInfo['fregionid'],
																									'fmediaownerid'=>$mediaInfo['fmediaownerid'],
																									'fmedia_class_code'=>$mediaInfo['fmediaclassid'],
																									'calc_sam'=>0,
																									
																									),$confirmed);	
					$class_ad_arr[] = $ad_class_code;//赋值本次执行的分类数据
				}
				$AdClassList = A('Common/AdClass','Model')->getAdClassCode();

				foreach($AdClassList as $AdClass){
					if(!in_array($AdClass,$class_ad_arr)){
						$del_state = $this->del_ad_class_data('tbn_ad_summary_half_month'.$confirmed,$media_id,$month_arr['st'],$AdClass);//删除没有该分类数据的记录
						
					}
				}
					
				
									
				
			}
		}
	}
	
	
	
	/*半月汇总数据*/
    public function gather_half_month_write($media_id,$date,$class,$countArr,$confirmed){
		if(strtotime($date) < strtotime('2018-07-01')) return false;
		
		if(!$class || !$countArr['fregionid'] || !$countArr['fmedia_class_code']){
			return false;
		}
		$media_id = intval($media_id);//转为int类型
		if(intval($media_id) == 0){
			return false;
		} 
		if($date == '' || !in_array(date('d',strtotime($date)),array('01','16'))){//判断是否传递日期和日期是否传递正确
			return false;
		}else{
			$date = date('Y-m-d',strtotime($date));
		}
		
		$fid = M('tbn_ad_summary_half_month'.$confirmed)->where(array('fmediaid'=>$media_id,'fdate'=>$date,'fad_class_code'=>$class))->getField('fid');
		if(intval($fid) === 0 ){
			try{//尝试新增数据
				$fid = M('tbn_ad_summary_half_month'.$confirmed)->add(array('fmediaid'=>$media_id,'fdate'=>$date,'fad_class_code'=>$class));
			}catch(Exception $e) { //如果新增失败则获取id
				$fid = M('tbn_ad_summary_half_month'.$confirmed)->where(array('fmediaid'=>$media_id,'fdate'=>$date,'fad_class_code'=>$class))->getField('fid');
			} 
		}
		if(substr($countArr['fmedia_class_code'],0,2) == '03'){//如果是报纸
			$countArr['fad_count'] = $countArr['fad_times'];//发布条数
			$countArr['fad_illegal_count'] = $countArr['fad_illegal_times'];//违法发布条数
			$countArr['calc_sam'] = 1;
			
		}
		$save_data = $countArr;

		$save_ret = M('tbn_ad_summary_half_month'.$confirmed)->where(array('fid'=>$fid))->save($save_data);//修改数量

		//var_dump($save_data);
		if($save_ret >= 0){
			return true;
		}else{
			return false;
		}
		
    }
	
	/*按周汇总数据*/
    public function gather_week($week_date,$mediaId,$confirmed){
		if(strtotime($week_date) < strtotime('2018-07-01')) return false;
		$day_week_num = dayweek(strtotime($week_date));
		$s_e_time = weekday($day_week_num['year'],$day_week_num['weeknum']);
		
		$weeknum = $day_week_num['weeknum'];
		if($weeknum < 10){//判断小于10 ，用0补位
			$yearweeknum = strval($day_week_num['year']).'0'.strval($weeknum);
		}else{
			$yearweeknum = strval($day_week_num['year']).strval($weeknum);
		}
		
		
		if($confirmed == '_confirmed' ){
			$zongju_data_confirm = M('zongju_data_confirm')->cache(true,120)->field('fid')->where(array('fmonth'=>date('Y-m-01',$s_e_time['start']),'dmp_confirm_state'=>2))->find();
			if(!$zongju_data_confirm) return false;//如果没有确认，直接返回
			
			$zongju_data_confirm = M('zongju_data_confirm')->cache(true,120)->field('fid')->where(array('fmonth'=>date('Y-m-01',$s_e_time['end']),'dmp_confirm_state'=>2))->find();
			if(!$zongju_data_confirm) return false;//如果没有确认，直接返回
			
			
			
		}
		
		$where = array();//初始化查询条件
		$where['issue_date'] = array('between',array(date('Y-m-d',$s_e_time['start']),date('Y-m-d',$s_e_time['end'])));//查询时间范围
		$where['media_id'] = $mediaId;
		
		$issueList = M('media_issue_count')->field('adclass_data')->where($where)->select();//查询数据
		//var_dump($issueList);
		$week_data = array();
		foreach($issueList as $issue){//循环发布数据
			$adclass_data = json_decode($issue['adclass_data'],true);
			
			foreach($adclass_data as $adclassData){
				//var_dump(1);
				$week_data[$adclassData['fmediaid']][$adclassData['ad_class_code']]['fad_times'] += $adclassData['issue_count'];//发布次数
				$week_data[$adclassData['fmediaid']][$adclassData['ad_class_code']]['fad_illegal_times'] += $adclassData['illegal_30_count'];//违法发布次数
				$week_data[$adclassData['fmediaid']][$adclassData['ad_class_code']]['fad_illegal_times_total'] += $adclassData['illegal_30_count'] + $adclassData['illegal_20_count'];//违法发布次数(总和)
				
				
				$week_data[$adclassData['fmediaid']][$adclassData['ad_class_code']]['fad_play_len'] += $adclassData['issue_duration'];//发布时长
				$week_data[$adclassData['fmediaid']][$adclassData['ad_class_code']]['fad_illegal_play_len'] += $adclassData['illegal_30_duration'];//违法发布时长
				$week_data[$adclassData['fmediaid']][$adclassData['ad_class_code']]['fad_illegal_play_len_total'] += $adclassData['illegal_30_duration'] + $adclassData['illegal_20_duration'];//违法发布时长(总和)
				
				
			}
			
		}
		
		foreach($week_data as $media_id => $ad_class_List){
			$mediaInfo = M('tmedia')
									->cache(true,3600)
									->field('
												tmedia.fmediaclassid,
												tmediaowner.fregionid,
												tmedia.fmediaownerid
											')
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->where(array('tmedia.fid'=>$media_id))
									->find();
			$class_ad_arr = array();//初始化本次包含的分类						
			foreach($ad_class_List as $ad_class_code => $ad_class_data){
				
				$this->gather_week_write($media_id,$yearweeknum,$ad_class_code,array(
																								
																								'fad_times'=>$ad_class_data['fad_times'],
																								'fad_illegal_times'=>$ad_class_data['fad_illegal_times'],
																								'fad_illegal_times_total'=>$ad_class_data['fad_illegal_times_total'],
																								
																								'fad_play_len'=>$ad_class_data['fad_play_len'],
																								'fad_illegal_play_len'=>$ad_class_data['fad_illegal_play_len'],
																								'fad_illegal_play_len_total'=>$ad_class_data['fad_illegal_play_len_total'],
																								
																								'fregionid'=>$mediaInfo['fregionid'],
																								'fmediaownerid'=>$mediaInfo['fmediaownerid'],
																								'fmedia_class_code'=>$mediaInfo['fmediaclassid'],
																								'calc_sam'=>0,
																								
																								),$confirmed);	
				$class_ad_arr[] = $ad_class_code;																				
			}
			$AdClassList = A('Common/AdClass','Model')->getAdClassCode();

			foreach($AdClassList as $AdClass){
				if(!in_array($AdClass,$class_ad_arr)){
					$del_state = M('tbn_ad_summary_week'.$confirmed)->where(array('fmediaid'=>$media_id,'fweek'=>$yearweeknum,'fad_class_code'=>$AdClass))->delete();
					//var_dump($del_state);
				}
			}
								
			
		}
		
	}
	
	
	/*按周汇总数据——写入*/
    public function gather_week_write($media_id,$week,$class,$countArr,$confirmed){
		if(!$class || !$countArr['fregionid'] || !$countArr['fmedia_class_code']){
			return false;
		}
		$media_id = intval($media_id);//转为int类型
		if(intval($media_id) == 0){
			return false;
		} 

		
		$fid = M('tbn_ad_summary_week'.$confirmed)->where(array('fmediaid'=>$media_id,'fweek'=>$week,'fad_class_code'=>$class))->getField('fid');
		if(intval($fid) === 0 ){
			try{//尝试新增数据
				$fid = M('tbn_ad_summary_week'.$confirmed)->add(array('fmediaid'=>$media_id,'fweek'=>$week,'fad_class_code'=>$class));
			}catch(Exception $e) { //如果新增失败则获取id
				$fid = M('tbn_ad_summary_week'.$confirmed)->where(array('fmediaid'=>$media_id,'fweek'=>$week,'fad_class_code'=>$class))->getField('fid');
			} 
		}
		if(substr($countArr['fmedia_class_code'],0,2) == '03'){//如果是报纸
			$countArr['fad_count'] = $countArr['fad_times'];//发布条数
			$countArr['fad_illegal_count'] = $countArr['fad_illegal_times'];//违法发布条数
			$countArr['calc_sam'] = 1;
			
		}
		
		$save_data = $countArr;

		$save_ret = M('tbn_ad_summary_week'.$confirmed)->where(array('fid'=>$fid))->save($save_data);//修改数量
		//var_dump($save_data);
		if($save_ret >= 0){
			return true;
		}else{
			return false;
		}
		
    }
	
	
	
	/*汇总数据，月*/
	public function gather_month($month,$mediaId,$confirmed){
		if(strtotime($month) < strtotime('2018-07-01')) return false;
		$where = array();
		
		$where['fdate'] = array('between',array(date('Y-m-01',strtotime($month)),date('Y-m-31',strtotime($month))));		
		$where['fmediaid'] = $mediaId;		
		
		$gatherMonthList = M('tbn_ad_summary_half_month'.$confirmed)
														->field('
																	
																	sum(fad_times) as fad_times,
																	sum(fad_illegal_times) as fad_illegal_times,
																	sum(fad_illegal_times_total) as fad_illegal_times_total,
																	
																	sum(fad_play_len) as fad_play_len,
																	sum(fad_illegal_play_len) as fad_illegal_play_len,
																	sum(fad_illegal_play_len_total) as fad_illegal_play_len_total,
																	
																	fmediaid,
																	fad_class_code,
																	fregionid,
																	fmediaownerid,
																	fmedia_class_code,
																	concat(left(fdate,7),"-01") as fdate
																	
																	')	
														->where($where)
														->group('left(fdate,7),fad_class_code,fmediaid')
														->select();												
		$class_ad_arr = array();
		foreach($gatherMonthList as $gatherMonth){
			$this->gather_common('tbn_ad_summary_month'.$confirmed,$gatherMonth['fmediaid'],$gatherMonth['fdate'],$gatherMonth['fad_class_code'],array(
																								
																'fad_times'=>$gatherMonth['fad_times'],
																'fad_illegal_times'=>$gatherMonth['fad_illegal_times'],
																'fad_illegal_times_total'=>$gatherMonth['fad_illegal_times_total'],
																
																'fad_play_len'=>$gatherMonth['fad_play_len'],
																'fad_illegal_play_len'=>$gatherMonth['fad_illegal_play_len'],
																'fad_illegal_play_len_total'=>$gatherMonth['fad_illegal_play_len_total'],
																
																'fregionid'=>$gatherMonth['fregionid'],
																'fmediaownerid'=>$gatherMonth['fmediaownerid'],
																'fmedia_class_code'=>$gatherMonth['fmedia_class_code'],
																'calc_sam'=>0,
																
																));	
			
			$class_ad_arr[] = $gatherMonth['fad_class_code'];//赋值本次执行的分类数据
		}
		$AdClassList = A('Common/AdClass','Model')->getAdClassCode();

		foreach($AdClassList as $AdClass){
			if(!in_array($AdClass,$class_ad_arr)){
				$del_state = $this->del_ad_class_data('tbn_ad_summary_month'.$confirmed,$gatherMonth['fmediaid'],$gatherMonth['fdate'],$AdClass);//删除没有该分类数据的记录
				//var_dump($del_state);
			}
		}
		
	}



	/*汇总数据，季度*/
	public function gather_quarter($year,$season,$confirmed){
		if(strtotime($year) < strtotime('2018-07-01')) return false;
		set_time_limit(1200);
		$where = array();
		$month = strval(($season * 3) - 2);
		$start_date = $year.'-'.$month.'-01';
		$end_date = $year.'-'.($month + 2).'-31';
		
		$where['fdate'] = array('between',array($start_date,$end_date));

		
		
		$gatherQuarterList = M('tbn_ad_summary_month'.$confirmed)
														->field('
																	
																	sum(fad_times) as fad_times,
																	sum(fad_illegal_times) as fad_illegal_times,
																	sum(fad_illegal_times_total) as fad_illegal_times_total,
																	
																	sum(fad_play_len) as fad_play_len,
																	sum(fad_illegal_play_len) as fad_illegal_play_len,
																	sum(fad_illegal_play_len_total) as fad_illegal_play_len_total,
																	
																	fmediaid,
																	fad_class_code,
																	fregionid,
																	fmediaownerid,
																	fmedia_class_code
															
																	
																	')	
														->where($where)
														->group('fad_class_code,fmediaid')
														->select();
		
		$class_ad_arr[] = array();//赋值本次执行的分类数据
		foreach($gatherQuarterList as $gatherQuarter){
			$this->gather_common('tbn_ad_summary_quarter'.$confirmed,$gatherQuarter['fmediaid'],$start_date,$gatherQuarter['fad_class_code'],array(
																								
																'fad_times'=>$gatherQuarter['fad_times'],
																'fad_illegal_times'=>$gatherQuarter['fad_illegal_times'],
																'fad_illegal_times_total'=>$gatherQuarter['fad_illegal_times_total'],
																
																'fad_play_len'=>$gatherQuarter['fad_play_len'],
																'fad_illegal_play_len'=>$gatherQuarter['fad_illegal_play_len'],
																'fad_illegal_play_len_total'=>$gatherQuarter['fad_illegal_play_len_total'],
																
																'fregionid'=>$gatherQuarter['fregionid'],
																'fmediaownerid'=>$gatherQuarter['fmediaownerid'],
																'fmedia_class_code'=>$gatherQuarter['fmedia_class_code'],
																'calc_sam'=>0,
																
																));	
			
			$class_ad_arr[$gatherQuarter['fmediaid']][] = $gatherQuarter['fad_class_code'];
		}

		$AdClassList = A('Common/AdClass','Model')->getAdClassCode();
		
		foreach($AdClassList as $AdClass){
			
			foreach($class_ad_arr as $media_id => $class_ad){
				if(!in_array($AdClass,$class_ad)){
					$del_state = $this->del_ad_class_data('tbn_ad_summary_quarter'.$confirmed,$media_id,$start_date,$AdClass);//删除没有该分类数据的记录
					
				}
			}
			
		}
		
		
	}

	/*汇总数据，季度*/
	public function gather_half_year($year,$half_year_n,$confirmed){
		if(strtotime($year) < strtotime('2018-07-01')) return false;
		$where = array();
		$half_year = strval(($half_year_n * 6) - 5);
		$start_date = $year.'-'.$half_year.'-01';
		$end_date = $year.'-'.($half_year + 5).'-31';
		
		$where['fdate'] = array('between',array($start_date,$end_date));

		
		$gatherHalfYearList = M('tbn_ad_summary_month'.$confirmed)
														->field('
																	
																	sum(fad_times) as fad_times,
																	sum(fad_illegal_times) as fad_illegal_times,
																	sum(fad_illegal_times_total) as fad_illegal_times_total,
																	
																	sum(fad_play_len) as fad_play_len,
																	sum(fad_illegal_play_len) as fad_illegal_play_len,
																	sum(fad_illegal_play_len_total) as fad_illegal_play_len_total,
																	
																	fmediaid,
																	fad_class_code,
																	fregionid,
																	fmediaownerid,
																	fmedia_class_code
															
																	
																	')	
														->where($where)
														->group('fad_class_code,fmediaid')
														->select();
		
		$class_ad_arr = array();
		foreach($gatherHalfYearList as $gatherHalfYear){
			$this->gather_common('tbn_ad_summary_half_year'.$confirmed,$gatherHalfYear['fmediaid'],$start_date,$gatherHalfYear['fad_class_code'],array(
																								
																'fad_times'=>$gatherHalfYear['fad_times'],
																'fad_illegal_times'=>$gatherHalfYear['fad_illegal_times'],
																'fad_illegal_times_total'=>$gatherHalfYear['fad_illegal_times_total'],
																
																'fad_play_len'=>$gatherHalfYear['fad_play_len'],
																'fad_illegal_play_len'=>$gatherHalfYear['fad_illegal_play_len'],
																'fad_illegal_play_len_total'=>$gatherHalfYear['fad_illegal_play_len_total'],
																
																'fregionid'=>$gatherHalfYear['fregionid'],
																'fmediaownerid'=>$gatherHalfYear['fmediaownerid'],
																'fmedia_class_code'=>$gatherHalfYear['fmedia_class_code'],
																'calc_sam'=>0,
																
																));	
			
			$class_ad_arr[$gatherHalfYear['fmediaid']][] = $gatherHalfYear['fad_class_code'];	
		}
		$AdClassList = A('Common/AdClass','Model')->getAdClassCode();
		
		foreach($AdClassList as $AdClass){
			
			foreach($class_ad_arr as $media_id => $class_ad){
				if(!in_array($AdClass,$class_ad)){
					$del_state = $this->del_ad_class_data('tbn_ad_summary_half_year'.$confirmed,$media_id,$start_date,$AdClass);//删除没有该分类数据的记录
					
				}
			}
			
		}
		
	}

	









	/*汇总数据写入，月、季度、半年、年*/
    public function gather_common($table_name,$media_id,$fdate,$class,$countArr){
		
		if(!$class || !$countArr['fregionid'] || !$countArr['fmedia_class_code']){
			return false;
		}
		
		
		
		$media_id = intval($media_id);//转为int类型
		if(intval($media_id) == 0){
			return false;
		} 

		
		$fid = M($table_name)->where(array('fmediaid'=>$media_id,'fdate'=>$fdate,'fad_class_code'=>$class))->getField('fid');
		if(intval($fid) === 0 ){
			try{//尝试新增数据
				$fid = M($table_name)->add(array('fmediaid'=>$media_id,'fdate'=>$fdate,'fad_class_code'=>$class));
			}catch(Exception $e) { //如果新增失败则获取id
				$fid = M($table_name)->where(array('fmediaid'=>$media_id,'fdate'=>$fdate,'fad_class_code'=>$class))->getField('fid');
			} 
		}
		if(substr($countArr['fmedia_class_code'],0,2) == '03'){//如果是报纸
			$countArr['fad_count'] = $countArr['fad_times'];//发布条数
			$countArr['fad_illegal_count'] = $countArr['fad_illegal_times'];//违法发布条数
			$countArr['calc_sam'] = 1;
			
		}
		$save_data = $countArr;

		$save_ret = M($table_name)->where(array('fid'=>$fid))->save($save_data);//修改数量
		//var_dump($save_data);
			
		if($save_ret >= 0){
			return true;
		}else{
			return false;
		}
		
    }
	
	
	
	/*删除没有数据的记录*/
	public function del_ad_class_data($table_name,$media_id,$fdate,$class){
		$del_state = M($table_name)->where(array('fmediaid'=>$media_id,'fdate'=>$fdate,'fad_class_code'=>$class))->delete();
		return $del_state;
	}
	
	
	

}



