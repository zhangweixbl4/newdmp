﻿
function getMediaclass(obj, url, name) {
	
	//选择媒介分类事件
	$(document).on('click', '#mediaclass-select .mediaclass-list .click_mediaclass_name', function () {
		var fid = $(this).attr("fid");//获取点击的媒介分类ID
		$(obj).attr(name,fid);//修改input的fid
		
		get_mediaclass_list(obj,fid,url);
	});
	//点击tab标签事件
	$(document).on('click', '.ttab', function () {
		var fid = $(this).attr("fid");//获取点击的媒介分类ID
		$(this).nextAll().remove();//移除右侧的ttab
		$(this).remove();//移除自己
		get_mediaclass_list(obj,fid,url);
	});	
	//表单点击事件
	$(obj).click(function(){
		$(obj).attr('readonly', true);
		var val = $(this).val();//获取表单内容，用于判断表单是否有值
		var fid = $(obj).attr(name);//获取存储的媒介分类code
		var areaList = $.trim($("#mediaclass-select .mediaclass-list").html());//获取媒介分类列表,用于判断媒介分类列表是否为空
		$('#mediaclass-select').parent().css('position', 'relative');//input父元素加上相对定位
		if(val == '' || fid == '' || areaList == ''){
			
			$('#mediaclass-select').show();//显示媒介分类选择控件
			$('#mediaclass-select .tab').empty();//移除所有已选择的
			get_mediaclass_list(obj, 0, url);//创建全部分类列表
		} else {
			$('#mediaclass-select').toggle();//显示媒介分类选择控件
		}	
	});
}
//根据媒介分类code创建标签和选择列表
function get_mediaclass_list(obj,fid,url){
	$.post(url,{"fid":fid},function(req){
		var listHTML = '';
		//循环地区列表
		for (let i = 0; i < req.mediaclassList.length; i++) {
			listHTML += '<li><a class="click_mediaclass_name" fid="'+req.mediaclassList[i].fid+'">'+req.mediaclassList[i].fclass+'</a></li>';
		}
		$(obj).val(req.mediaclassDetails.ffullname);
		//如果媒介分类列表不为空则把列表加入到 #mediaclass-select .tab
		if(listHTML != ''){
			$('#mediaclass-select .mediaclass-list').html(listHTML);//媒介分类列表渲染
			$('#mediaclass-select .tab li').removeClass('curr');//移除所有tab的选择
			$('#mediaclass-select .tab').append('<li class="ttab curr" fid="'+req.mediaclassDetails.fid+'"><a class=""><em>'+req.mediaclassDetails.fclass+'</em></a></li>');//创建新的tab
		}else{
			$('#mediaclass-select').hide();//隐藏媒介分类选择控件
		} 
			
	});
}
