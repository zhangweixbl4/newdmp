<?php
namespace Agp\Controller;
use Think\Controller;
use Think\Exception;

/**
 * 版本明细
 */

class SampleDetailsController extends BaseController
{

    //列表 fissuedate 为创建时间
    public function index()
    {
        header("Content-type:text/html;charset=utf-8");
        ini_set('memory_limit','1024M');
        ini_set('max_execution_time', '120');//设置超时时间
        session_write_close();
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $media_class = json_decode($ALL_CONFIG['media_class'],true);

        $outtype = I('outtype');//导出类型
        $p = I('page', 1);//当前第几页
        $pp = 20;//每页显示多少记录
        if(empty($outtype)){
            $limitstr = ' limit '.($p-1)*$pp.','.$pp;
        }

        $pxtype     = I('pxtype')?I('pxtype'):2;//排序方式，0默认
        $pxval      = I('pxval')?I('pxval'):0;//排序值，0默认
        $fadname            = I('fadname');// 广告名称
        $fadclasscode       = I('fadclasscode');// 广告内容类别组
        $area               = I('area');// 行政区划
        $media              = I('media');//媒体组
        $mclass             = I('mclass')?I('mclass'):'';//媒体类型
        $fillegaltypecode   = I('fillegaltypecode');// 违法类型
        $fissuedatest2      = I('fissuedatest');//发布时间起
        $fissuedateed2      = I('fissuedateed');//发布时间止
        $iscontain          = I('iscontain');//是否包含下属地区
        if(strlen($fillegaltypecode)==0){
            $fillegaltypecode = -1;
        }
        $fissuedatest = strtotime($fissuedatest2);
        $fissuedateed = strtotime($fissuedateed2);

        if(empty($area)){
            $area = session('regulatorpersonInfo.regionid');
        }
        $publicWhere = '1=1';

        if(empty(in_array('13', $media_class))){
            $wherestrnet = '1=0 ';
        }

        $orders = '';
        if($pxtype==1){//1时按发布媒体排序
            if($pxval==0){
                $orders = $orders?$orders.',fmedianame desc':' fmedianame desc';
            }else{
                $orders = $orders?$orders.',fmedianame asc':' fmedianame asc';
            }
        }elseif($pxtype==2){//2时按发布时间排序
            if($pxval==0){
                $orders = $orders?$orders.',fissuedate desc':' fissuedate desc';
            }else{
                $orders = $orders?$orders.',fissuedate asc':' fissuedate asc';
            }
        }else{
            if($pxval==0){
                $orders = $orders?$orders.',fissuedate desc':' fissuedate desc';
            }
        }

        if (!empty($fadname)) {
            $publicWhere .= ' and fadname like "%'.$fadname.'%"';
        }

        if(!empty($media)){
            $publicWhere .= ' and tmedia.fid in ('.implode(',', $media).')';
        }
        //获取非备用媒体数据
        $publicWhere .= ' and tmedia.fid = tmedia.main_media_id and tmedia.fstate = 1';

        $arr_code = [];
        if(is_array($fadclasscode)){
            foreach ($fadclasscode as $key => $value) {
                $codes = D('Function')->get_next_tadclasscode($value,true);//获取下级广告类别代码
                if(!empty($codes)){
                    $arr_code = array_merge($arr_code,$codes);
                }else{
                    array_push($arr_code,$value);
                }
            }
            $publicWhere .= ' and fadclasscode  in('.implode(',', $arr_code).')';
        }

        if($fillegaltypecode>=0){//违法类型
            $publicWhere .= ' and a.fillegaltypecode ='.$fillegaltypecode;
        }

        if(!empty($mclass)){
            $publicWhere .= ' and left(tmedia.fmediaclassid,2) = "'.$mclass.'"';
        }

        if(!empty($fissuedatest2) && !empty($fissuedateed2)){
            $wherestrtv     .= ' and a.fissuedate between "'.$fissuedatest2.'" and "'.$fissuedateed2.'"';
            $wherestrpaper  .= ' and a.fissuedate between "'.$fissuedatest2.'" and "'.$fissuedateed2.'"';
            $wherestrbc     .= ' and a.fissuedate between "'.$fissuedatest2.'" and "'.$fissuedateed2.'"';
            $wherestrnet    .= ' and from_unixtime(a.net_created_date/1000,"%Y-%m-%d") between "'.$fissuedatest2.'" and "'.$fissuedateed2.'"';
        }
        if(!empty($iscontain)){
            $tregion_len = get_tregionlevel($area);
            if($tregion_len == 1){//国家级
                $publicWhere .= ' and tmedia.media_region_id ='.$area;
            }elseif($tregion_len == 2){//省级
                $publicWhere .= ' and tmedia.media_region_id like "'.substr($area,0,2).'%"';
            }elseif($tregion_len == 4){//市级
                $publicWhere .= ' and tmedia.media_region_id like "'.substr($area,0,4).'%"';
            }elseif($tregion_len == 6){//县级
                $publicWhere .= ' and tmedia.media_region_id like "'.substr($area,0,6).'%"';
            }
        }else{
            $publicWhere .= ' and tmedia.media_region_id ='.$area;
        }

        //根据不同的媒体类型，搜索不同的表
        if(empty($mclass)){
            $countsql = 'select sum(acount) as acount from (
            (select count(*) as acount from ttvsample a
                inner join tad on a.fadid=tad.fadid and tad.fadid<>0 
                inner join tadclass on tad.fadclasscode=tadclass.fcode 
                inner join tmedia on a.fmediaid=tmedia.fid
                inner join tadowner on tad.fadowner=tadowner.fid
                inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid  
                where '.$publicWhere.$wherestrtv.')
            union all (select count(*) as acount from tbcsample a
                inner join tad on a.fadid=tad.fadid and tad.fadid<>0 
                inner join tadclass on tad.fadclasscode=tadclass.fcode 
                inner join tmedia on a.fmediaid=tmedia.fid 
                inner join tadowner on tad.fadowner=tadowner.fid
                inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid
                where '.$publicWhere.$wherestrbc.')
            union all (select count(*) as acount from tpapersample a
                inner join tad on a.fadid=tad.fadid and tad.fadid<>0 
                inner join tadclass on tad.fadclasscode=tadclass.fcode 
                inner join tmedia on a.fmediaid=tmedia.fid 
                inner join tadowner on tad.fadowner=tadowner.fid
                inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid  
                where '.$publicWhere.$wherestrpaper.')
            union all (select count(*) as acount from tnetissue a
                inner join tadclass on a.fadclasscode=tadclass.fcode 
                inner join tmedia on a.fmediaid=tmedia.fid 
                inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid 
                where '.$publicWhere.$wherestrnet.')
            ) as a';
            $count = M()->query($countsql);

            $datasql = 'select * from (
            (select "01" as mclass,a.fmediaid,tadclass.ffullname as fadclass,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,a.fissuedate,a.fid as sid,tad.fadname,tad.fadclasscode,a.fexpressioncodes,a.fillegalcontent,a.favifilename 
                from ttvsample a
                inner join tad on a.fadid=tad.fadid and tad.fadid<>0 
                inner join tadclass on tad.fadclasscode=tadclass.fcode 
                inner join tmedia on a.fmediaid=tmedia.fid 
                inner join tadowner on tad.fadowner=tadowner.fid
                inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid 
                where '.$publicWhere.$wherestrtv.') 
            union all (select "02" as mclass,a.fmediaid,tadclass.ffullname as fadclass,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,a.fissuedate,a.fid as sid,tad.fadname,tad.fadclasscode,a.fexpressioncodes,a.fillegalcontent,a.favifilename 
                from tbcsample a
                inner join tad on a.fadid=tad.fadid and tad.fadid<>0 
                inner join tadclass on tad.fadclasscode=tadclass.fcode 
                inner join tmedia on a.fmediaid=tmedia.fid 
                inner join tadowner on tad.fadowner=tadowner.fid
                inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid  
                where '.$publicWhere.$wherestrbc.') 
            union all (select "03" as mclass,a.fmediaid,tadclass.ffullname as fadclass,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,a.fissuedate, a.fpapersampleid as sid,tad.fadname,tad.fadclasscode,a.fexpressioncodes,a.fillegalcontent,a.fjpgfilename favifilename 
                from tpapersample a
                inner join tad on a.fadid=tad.fadid and tad.fadid<>0 
                inner join tadclass on tad.fadclasscode=tadclass.fcode 
                inner join tmedia on a.fmediaid=tmedia.fid 
                inner join tadowner on tad.fadowner=tadowner.fid
                inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid 
                where '.$publicWhere.$wherestrpaper.') 
            union all (select "13" as mclass, a.fmediaid,tadclass.ffullname as fadclass,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id fregionid, (case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype, date_format(FROM_UNIXTIME(a.net_created_date/1000),"%Y-%m-%d %H:%i:%s") as fissuedate, a.major_key as sid, a.fadname, a.fadclasscode,a.fexpressioncodes,a.fillegalcontent,a.net_snapshot favifilename  
                from tnetissue a
                inner join tadclass on a.fadclasscode=tadclass.fcode 
                inner join tmedia on a.fmediaid=tmedia.fid 
                inner join tmedia_temp on tmedia.fid=tmedia_temp.fmediaid and tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').'  
                where '.$publicWhere.$wherestrnet.') 
            ) as a ORDER BY '.$orders.$limitstr;
        } elseif($mclass=='02'){
            $countsql = 'select count(*) as acount from tbcsample a
                inner join tad on a.fadid=tad.fadid and tad.fadid<>0 
                inner join tadclass on tad.fadclasscode=tadclass.fcode 
                inner join tmedia on a.fmediaid=tmedia.fid 
                inner join tadowner on tad.fadowner=tadowner.fid
                inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid 
                where '.$publicWhere.$wherestrbc;
            $count = M()->query($countsql);

            $datasql = 'select "02" as mclass,a.fmediaid,tadclass.ffullname as fadclass,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,a.fissuedate,a.fid as sid,tad.fadname,tad.fadclasscode,a.fexpressioncodes,a.fillegalcontent,a.favifilename 
                from tbcsample a
                inner join tad on a.fadid=tad.fadid and tad.fadid<>0 
                inner join tadclass on tad.fadclasscode=tadclass.fcode 
                inner join tmedia on a.fmediaid=tmedia.fid 
                inner join tadowner on tad.fadowner=tadowner.fid
                inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid 
                where '.$publicWhere.$wherestrbc.' 
                ORDER BY '.$orders.$limitstr;
        } elseif($mclass=='03'){
            $countsql = 'select count(*) as acount from tpapersample a
                inner join tad on a.fadid=tad.fadid and tad.fadid<>0 
                inner join tadclass on tad.fadclasscode=tadclass.fcode 
                inner join tmedia on a.fmediaid=tmedia.fid 
                inner join tadowner on tad.fadowner=tadowner.fid
                inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid 
                where '.$publicWhere.$wherestrpaper;
            $count = M()->query($countsql);

            $datasql = 'select "03" as mclass,a.fmediaid,tadclass.ffullname as fadclass,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,a.fissuedate, a.fpapersampleid as sid,tad.fadname,tad.fadclasscode,a.fexpressioncodes,a.fillegalcontent,a.fjpgfilename favifilename 
                from tpapersample a
                inner join tad on a.fadid=tad.fadid and tad.fadid<>0 
                inner join tadclass on tad.fadclasscode=tadclass.fcode 
                inner join tmedia on a.fmediaid=tmedia.fid 
                inner join tadowner on tad.fadowner=tadowner.fid
                inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid 
                where '.$publicWhere.$wherestrpaper.' 
                ORDER BY '.$orders.$limitstr;
        } elseif($mclass=='13'){
            $countsql = 'select count(*) as acount from tnetissue a
                inner join tadclass on a.fadclasscode=tadclass.fcode 
                inner join tmedia on a.fmediaid=tmedia.fid 
                inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid 
                where '.$publicWhere.$wherestrnet;
            $count = M()->query($countsql);

            $datasql = 'select "13" as mclass, a.fmediaid,tadclass.ffullname as fadclass,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id fregionid, (case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype, date_format(FROM_UNIXTIME(a.net_created_date/1000),"%Y-%m-%d %H:%i:%s") as fissuedate, a.major_key as sid, a.fadname, a.fadclasscode,a.fexpressioncodes,a.fillegalcontent,a.net_snapshot favifilename
                from tnetissue a
                inner join tadclass on a.fadclasscode=tadclass.fcode 
                inner join tmedia on a.fmediaid=tmedia.fid 
                inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid 
                where '.$publicWhere.$wherestrnet.' 
                ORDER BY '.$orders.$limitstr;
        } else{
            $countsql = 'select count(*) as acount from ttvsample a
                inner join tad on a.fadid=tad.fadid and tad.fadid<>0 
                inner join tadclass on tad.fadclasscode=tadclass.fcode 
                inner join tmedia on a.fmediaid=tmedia.fid 
                inner join tadowner on tad.fadowner=tadowner.fid
                inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid 
                where '.$publicWhere.$wherestrtv;
            $count = M()->query($countsql);

            $datasql = 'select "01" as mclass,a.fmediaid,tadclass.ffullname as fadclass,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,a.fissuedate,a.fid as sid,tad.fadname,tad.fadclasscode,a.fexpressioncodes,a.fillegalcontent,a.favifilename
                from ttvsample a
                inner join tad on a.fadid=tad.fadid and tad.fadid<>0 
                inner join tadclass on tad.fadclasscode=tadclass.fcode 
                inner join tmedia on a.fmediaid=tmedia.fid 
                inner join tadowner on tad.fadowner=tadowner.fid
                inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid  
                where '.$publicWhere.$wherestrtv.'
                ORDER BY '.$orders.$limitstr;
        }
        $data = M()->query($datasql);

        if(!empty($outtype)){
            if(empty($data)){
                $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
            }
            $outdata['datalie'] = [
              '序号'=>'key',
              '广告名称'=>'fadname',
              '广告内容类别'=>'fadclass',
              '发布媒体'=>'fmedianame',
              '媒体类型'=>[
                'type'=>'zwif',
                'data'=>[
                  ['{mclass} == "01"','电视'],
                  ['{mclass} == "02"','广播'],
                  ['{mclass} == "03"','报纸'],
                  ['{mclass} == "13"','互联网'],
                ]
              ],
              '发布日期'=>'fissuedate',
              '违法类型'=>'fillegaltype',
              '违法代码'=>'fexpressioncodes',
              '涉嫌违法内容'=>'fillegalcontent',
              '素材地址'=>'favifilename',
            ];
          
            $outdata['lists'] = $data;
            $ret = A('Goutxls')->outdata_xls($outdata);

            D('Function')->write_log('监测数据',1,'excel导出成功');
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count[0]['acount'],'list'=>$data)));
        }
    }

    //详情
    public function details_view()
    {
        $sid        = I('sid');//广告记录id
        $mclass     = I('mclass');//媒体类型
        if(empty($sid)||empty($mclass)){
            $this->error('参数缺失!');
        }

        if($mclass== '01'){
            $data = M('ttvsample')
                ->alias('a')
                ->field(
                    'a.fid as tid,
                    tad.fadname,
                    tadclass.ffullname as fadclass,
                    (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,
                    tmedia.fid ,
                    tad.fbrand, 
                    tadowner.fname, 
                    fversion, 
                    "01" as mclass,
                    fadlen,
                    tillegaltype.fillegaltype,
                    fillegalcontent, 
                    fexpressioncodes, 
                    fexpressions, 
                    fconfirmations, 
                    fapprovalid, 
                    fspokesman, 
                    fadmanuno, 
                    fmanuno, 
                    fadapprno, 
                    fapprno, 
                    fadent, 
                    fent, 
                    fentzone,
                    a.fissuedate,
                    favifilepng,
                    favifilename
                ')
                ->join('tad on a.fadid=tad.fadid and tad.fadid<>0')
                ->join('tadclass on tad.fadclasscode=tadclass.fcode')
                ->join('tadowner on tad.fadowner=tadowner.fid ')
                ->join('tillegaltype on a.fillegaltypecode=tillegaltype.fcode')
                ->join('tmedia on a.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
                ->where('a.fstate=1 and a.fid='.$sid)
                ->find();
        } elseif($mclass== '02'){
            $data = M('tbcsample')
                ->alias('a')
                ->field('a.fid as tid,
                    tad.fadname, tadclass.ffullname as fadclass, (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame ,tmedia.fid ,tad.fbrand, tadowner.fname, fversion, "02" as mclass, fadlen, tillegaltype.fillegaltype, fillegalcontent, fexpressioncodes, fexpressions, fconfirmations, fapprovalid, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone,a.fissuedate, favifilepng,favifilename
                ')
                ->join('tad on a.fadid=tad.fadid and tad.fadid<>0')
                ->join('tadclass on tad.fadclasscode=tadclass.fcode')
                ->join('tadowner on tad.fadowner=tadowner.fid ')
                ->join('tillegaltype on a.fillegaltypecode=tillegaltype.fcode')
                ->join('tmedia on a.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
                ->where('a.fstate=1 and a.fid='.$sid)
                ->find();
        } elseif($mclass== '03'){
            $data = M('tpapersample')
                ->alias('a')
                ->field('a.fpapersampleid as tid,
                    tad.fadname, tadclass.ffullname as fadclass, (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame ,tad.fbrand, tadowner.fname, fversion, "03" as mclass , tillegaltype.fillegaltype, fillegalcontent, fexpressioncodes, fexpressions, fconfirmations, fapprovalid, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, a.fissuedate, a.fjpgfilename as favifilepng,a.fjpgfilename as favifilename
                ')
                ->join('tad on a.fadid=tad.fadid and tad.fadid<>0')
                ->join('tadclass on tad.fadclasscode=tadclass.fcode')
                ->join('tadowner on tad.fadowner=tadowner.fid ')
                ->join('tillegaltype on a.fillegaltypecode=tillegaltype.fcode')
                ->join('tmedia on a.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
                ->where('a.fstate=1 and a.fpapersampleid='.$sid)
                ->find();
        } elseif($mclass== '13'){
            $data = M('tnetissue')
                ->alias('a')
                ->field('a.major_key as tid,
                    a.*,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,fbrand, tadclass.ffullname as fadclass,tadowner.fname as fadowner_name, "13" as mclass, tillegaltype.fillegaltype,a.fillegaltypecode,a.fexpressioncodes, fillegalcontent,FROM_UNIXTIME((a.net_created_date/1000),"%Y-%m-%d") as fissuedate,tmediaowner.fname as fmediaownername
                ')
                ->join('tadclass on a.fadclasscode=tadclass.fcode')
                ->join('tillegaltype on a.fillegaltypecode=tillegaltype.fcode')
                ->join('tmedia on a.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
                ->join('tmediaowner on tmedia.fmediaownerid=tmediaowner.fid')
                ->join('tadowner on a.fadowner=tadowner.fid')
                ->where('a.finputstate=2 and a.major_key='.$sid)
                ->find();
        }

        if(!empty($data)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'无数据','data'=>$data));
        }
    }

}