<?php

namespace Gongshang\Controller;
use Think\Controller;
class ReportController extends BaseController {

	public function index(){

		$regionid = I('region');
		$month 	  = I('month');

		$starttime = date('Y-m-01', strtotime($month));
		$endtime = date('Y-m-d', strtotime("$starttime +1 month -1 day"));
		$where['tmediaowner.fregionid'] = $regionid;
		$where['issue.fissuedate'] = array('between',array($starttime,$endtime));
		$where['tmedia.priority'] = array('EGT',0);

		if($month && $regionid){
			$tmediaList = M('tmedia')
										->field('tmedia.fid,tmedia.fmedianame,tmedia.fmediaclassid,tcollecttype.fcollecttype')
										->join('tcollecttype on tcollecttype.fcollecttypecode = tmedia.fcollecttype','LEFT')
										
									
										->join('tmediaowner on tmedia.fmediaownerid = tmediaowner.fid')
										->where(array('tmediaowner.fregionid'=>$regionid,'tmedia.priority' => array('EGT',0)))->select();//查询改地区所管媒体
			//自定义数组
			//var_dump($tmediaList);
			foreach ($tmediaList as $key => $value) {
				if(substr($value['fmediaclassid'],0,2) == '01') $tvmediaid[] = $value['fid'];//所有电视媒体ID
				if(substr($value['fmediaclassid'],0,2) == '02') $bcmediaid[] = $value['fid'];//所有广播媒体ID
				if(substr($value['fmediaclassid'],0,2) == '03') $papermediaid[] = $value['fid'];//所有报纸媒体ID
				$tmedia[$value['fid']] = $value;//数组key为媒体ID
			}

			$tv = M('ttvissue')
					->alias('issue')
					->field("
							tmedia.fid,
							tmedia.fmedianame,
							tmedia.fmediaclassid,
							
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '01' then issue.fquantity else 0 end) sl1,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '02' then issue.fquantity else 0 end) sl2,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '03' then issue.fquantity else 0 end) sl3,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '04' then issue.fquantity else 0 end) sl4,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '05' then issue.fquantity else 0 end) sl5,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '06' then issue.fquantity else 0 end) sl6,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '07' then issue.fquantity else 0 end) sl7,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '08' then issue.fquantity else 0 end) sl8,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '09' then issue.fquantity else 0 end) sl9,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '10' then issue.fquantity else 0 end) sl10,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '11' then issue.fquantity else 0 end) sl11,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '12' then issue.fquantity else 0 end) sl12,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '13' then issue.fquantity else 0 end) sl13,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '14' then issue.fquantity else 0 end) sl14,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '15' then issue.fquantity else 0 end) sl15,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '16' then issue.fquantity else 0 end) sl16,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '17' then issue.fquantity else 0 end) sl17,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '18' then issue.fquantity else 0 end) sl18,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '19' then issue.fquantity else 0 end) sl19,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '20' then issue.fquantity else 0 end) sl20,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '21' then issue.fquantity else 0 end) sl21,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '22' then issue.fquantity else 0 end) sl22,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '23' then issue.fquantity else 0 end) sl23,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '24' then issue.fquantity else 0 end) sl24,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '25' then issue.fquantity else 0 end) sl25,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '26' then issue.fquantity else 0 end) sl26,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '27' then issue.fquantity else 0 end) sl27,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '28' then issue.fquantity else 0 end) sl28,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '29' then issue.fquantity else 0 end) sl29,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '30' then issue.fquantity else 0 end) sl30,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '31' then issue.fquantity else 0 end) sl31,
							sum(issue.fquantity) sl
							")
					->join('tmedia on tmedia.fid = issue.fmediaid','left')
					->join('tmediaowner on tmedia.fmediaownerid = tmediaowner.fid')
					->join('tregion on tregion.fid = tmediaowner.fregionid')
					->where($where)
					->group('tmedia.fmedianame')
					->having('sum(issue.fquantity) > 0')
					->select();			
			$fid = array();
			foreach ($tv as $key => $value) {
				$fid[] = $value['fid'];//关联查询电视媒体ID
			}
			$diff = array_diff($tvmediaid, $fid);//对比是否有不同
			//根据比对结果加入对应数据
			foreach ($diff as $key => $value) {
				$fid = $value;
				$tv_data['fid'] = $fid;
				$tv_data['fmedianame'] = $tmedia[$fid]['fmedianame'];
				$tv_data['fmediaclassid'] = $tmedia[$fid]['fmediaclassid'];
				$tv_data['fcollecttype'] = $tmedia[$fid]['fcollecttype'];
				//var_dump($tv_data);
				for($i = 1;$i <= 31;$i++){
					$tv_data['sl'.$i] = 0;
				}
				$tv_data['sl'] = 0;
				array_push($tv, $tv_data);
			}

			$bc = M('tbcissue')
					->alias('issue')
					->field("
							tmedia.fid,
							tmedia.fmedianame,
							tmedia.fmediaclassid,
							
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '01' then issue.fquantity else 0 end) sl1,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '02' then issue.fquantity else 0 end) sl2,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '03' then issue.fquantity else 0 end) sl3,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '04' then issue.fquantity else 0 end) sl4,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '05' then issue.fquantity else 0 end) sl5,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '06' then issue.fquantity else 0 end) sl6,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '07' then issue.fquantity else 0 end) sl7,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '08' then issue.fquantity else 0 end) sl8,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '09' then issue.fquantity else 0 end) sl9,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '10' then issue.fquantity else 0 end) sl10,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '11' then issue.fquantity else 0 end) sl11,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '12' then issue.fquantity else 0 end) sl12,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '13' then issue.fquantity else 0 end) sl13,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '14' then issue.fquantity else 0 end) sl14,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '15' then issue.fquantity else 0 end) sl15,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '16' then issue.fquantity else 0 end) sl16,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '17' then issue.fquantity else 0 end) sl17,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '18' then issue.fquantity else 0 end) sl18,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '19' then issue.fquantity else 0 end) sl19,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '20' then issue.fquantity else 0 end) sl20,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '21' then issue.fquantity else 0 end) sl21,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '22' then issue.fquantity else 0 end) sl22,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '23' then issue.fquantity else 0 end) sl23,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '24' then issue.fquantity else 0 end) sl24,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '25' then issue.fquantity else 0 end) sl25,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '26' then issue.fquantity else 0 end) sl26,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '27' then issue.fquantity else 0 end) sl27,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '28' then issue.fquantity else 0 end) sl28,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '29' then issue.fquantity else 0 end) sl29,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '30' then issue.fquantity else 0 end) sl30,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '31' then issue.fquantity else 0 end) sl31,
							sum(issue.fquantity) sl
							")
					->join('tmedia on tmedia.fid = issue.fmediaid')
					->join('tmediaowner on tmedia.fmediaownerid = tmediaowner.fid')
					->join('tregion on tregion.fid = tmediaowner.fregionid')
					->where($where)
					->group('tmedia.fmedianame')
					->having('sum(issue.fquantity) >= 0')
					->select();
			$fid = array();
			foreach ($bc as $key => $value) {
				$fid[] = $value['fid'];//关联查询电视媒体ID
			}
			$diff = array_diff($bcmediaid, $fid);//对比是否有不同
			//根据比对结果加入对应数据
			foreach ($diff as $key => $value) {
				$fid = $value;
				$bc_data['fid'] = $fid;
				$bc_data['fmedianame'] = $tmedia[$fid]['fmedianame'];
				$bc_data['fmediaclassid'] = $tmedia[$fid]['fmediaclassid'];
				$bc_data['fcollecttype'] = $tmedia[$fid]['fcollecttype'];
				for($i = 1;$i <= 31;$i++){
					$bc_data['sl'.$i] = 0;
				}
				$bc_data['sl'] = 0;
				array_push($bc, $bc_data);
			}

			$paper = M('tpaperissue')
					->alias('issue')
					->field("
							tmedia.fid,
							tmedia.fmedianame,
							tmedia.fmediaclassid,
							
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '01' then issue.fquantity else 0 end) sl1,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '02' then issue.fquantity else 0 end) sl2,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '03' then issue.fquantity else 0 end) sl3,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '04' then issue.fquantity else 0 end) sl4,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '05' then issue.fquantity else 0 end) sl5,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '06' then issue.fquantity else 0 end) sl6,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '07' then issue.fquantity else 0 end) sl7,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '08' then issue.fquantity else 0 end) sl8,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '09' then issue.fquantity else 0 end) sl9,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '10' then issue.fquantity else 0 end) sl10,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '11' then issue.fquantity else 0 end) sl11,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '12' then issue.fquantity else 0 end) sl12,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '13' then issue.fquantity else 0 end) sl13,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '14' then issue.fquantity else 0 end) sl14,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '15' then issue.fquantity else 0 end) sl15,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '16' then issue.fquantity else 0 end) sl16,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '17' then issue.fquantity else 0 end) sl17,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '18' then issue.fquantity else 0 end) sl18,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '19' then issue.fquantity else 0 end) sl19,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '20' then issue.fquantity else 0 end) sl20,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '21' then issue.fquantity else 0 end) sl21,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '22' then issue.fquantity else 0 end) sl22,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '23' then issue.fquantity else 0 end) sl23,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '24' then issue.fquantity else 0 end) sl24,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '25' then issue.fquantity else 0 end) sl25,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '26' then issue.fquantity else 0 end) sl26,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '27' then issue.fquantity else 0 end) sl27,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '28' then issue.fquantity else 0 end) sl28,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '29' then issue.fquantity else 0 end) sl29,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '30' then issue.fquantity else 0 end) sl30,
							sum(case when DATE_FORMAT(issue.fissuedate,'%d') = '31' then issue.fquantity else 0 end) sl31,
							sum(issue.fquantity) sl
							")
					->join('tmedia on tmedia.fid = issue.fmediaid')
					->join('tmediaowner on tmedia.fmediaownerid = tmediaowner.fid')
					->join('tregion on tregion.fid = tmediaowner.fregionid')
					->where($where)
					->group('tmedia.fmedianame')
					->having('sum(issue.fquantity) >= 0')
					->select();
			$fid = array();
			foreach ($paper as $key => $value) {
				$fid[] = $value['fid'];//关联查询电视媒体ID
			}
			$diff = array_diff($papermediaid, $fid);//对比是否有不同
			//根据比对结果加入对应数据
			foreach ($diff as $key => $value) {
				
				
				
				$fid = $value;
				$paper_data['fid'] = $fid;
				$paper_data['fmedianame'] = $tmedia[$fid]['fmedianame'];
				$paper_data['fmediaclassid'] = $tmedia[$fid]['fmediaclassid'];
				$paper_data['fcollecttype'] = $tmedia[$fid]['fcollecttype'];
				for($i = 1;$i <= 31;$i++){
					$paper_data['sl'.$i] = 0;
				}
				$paper_data['sl'] = 0;
				array_push($paper, $paper_data);
			}
            $m = date('Ym',strtotime($month));
            foreach($tv as $k => $v) {
                $tmp = M('monthly_report','','mysql://root:root123456ABC!@#@118.31.14.6/broadcast#utf8')->where("channel = '{$v['fid']}' and month=$m")->field('du_sc,exception_type,exception_desc')->order('batch')->select();
                if(!empty($tmp)) {
                    $tv[$k]['du_sc'] = array_column($tmp,'du_sc');
                    $tv[$k]['exception']['type'] = array_column($tmp,'exception_type');
                    $tv[$k]['exception']['desc'] = array_column($tmp,'exception_desc');
                } else {
                    $tv[$k]['du_sc'] = array();
                    $tv[$k]['exception']['type'] = array();
                    $tv[$k]['du_sc'] = array_pad($tv[$k]['du_sc'], 31, 0);
                    $tv[$k]['exception']['type'] = array_pad($tv[$k]['exception']['type'], 31, 0);
                }
            }
            foreach($bc as $k => $v) {
                $tmp = M('monthly_report','','mysql://root:root123456ABC!@#@118.31.14.6/broadcast#utf8')->where("channel = '{$v['fid']}' and month=$m")->field('du_sc')->order('batch')->select();
                if(!empty($tmp)) {
                    $bc[$k]['du_sc'] = array_column($tmp,'du_sc');
                    $bc[$k]['exception']['type'] = array_column($tmp,'exception_type');
                    $bc[$k]['exception']['desc'] = array_column($tmp,'exception_desc');
                } else {
                    $bc[$k]['du_sc'] = array();
                    $bc[$k]['exception']['type'] = array();
                    $bc[$k]['du_sc'] = array_pad($bc[$k]['du_sc'], 31, 0);
                    $bc[$k]['exception']['type'] = array_pad($bc[$k]['exception']['type'], 31, 0);
                }
            }
            foreach($paper as $k => $v) {
                $paper[$k]['du_sc'] = array();
                $paper[$k]['exception']['type'] = array();
                $paper[$k]['du_sc'] = array_pad($paper[$k]['du_sc'], 31, 0);
                $paper[$k]['exception']['type'] = array_pad($paper[$k]['exception']['type'], 31, 0);
            }
            $data = array_merge($tv,$bc,$paper);
		}else{
			$data = array();
		}
		//var_dump($data);
		$this->assign('regionid',session('regulatorpersonInfo.regionid'));
		$this->assign('data',$data);
		$this->display();

	}

	//获取该地区直属管辖的媒体
	public function ajax_get_area(){

		$regionid = session('regulatorpersonInfo.regionid');
		if($regionid == '100000'){
			$region = M('tregion')->field('fid,fname')->where(array('fpid' => $regionid))->select();
		}else{
			$where['fid|fpid'] = $regionid;
			$region = M('tregion')->field('fid,fname')->where($where)->select();
		}

		$this->ajaxReturn(array('code'=>0,'data'=>$region));
	}

}
