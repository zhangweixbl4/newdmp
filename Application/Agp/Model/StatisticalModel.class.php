<?php
namespace Agp\Model;


/**
 * Class StatisticalReportModel
 * @package Agp\Model
 * BY JIM
 * 报告统计模型
 */
class StatisticalModel{

    //日报媒体
    /**查询指定媒介类别的某天广告发布情况
     * @param $medias
     * @param $media_type
     * @param $date_table
     * @param $date
     * @return array
     */
    public function report_statistics_media($medias,$media_type,$date_table,$date){

        $regionid = session('regulatorpersonInfo.regionid');

        if(1){
            $tv_issue_table = 'ttvissue_'.$date_table;
            $bc_issue_table = 'tbcissue_'.$date_table;
            $paper_issue_table = 'tpaperissue';
            $net_issue_table = 'tnetissueputlog';
            //分组条件匹配
            $fields_str = '
            tmedia.fid,
	        tmedia.fmedianame,
	        sum(a.flength) AS flength,
            count(1) as fad_times,
	        count(
		        CASE
		        WHEN ybb.fillegaltypecode > 0 THEN
			    1
		        ELSE
			    NULL
		        END
	        ) AS fad_illegal_times,
	        ROUND(
		        count(
			        CASE
			        WHEN ybb.fillegaltypecode > 0 THEN
				    1
			        ELSE
				    NULL
			        END
		        ) / COUNT(1) * 100,
		        2
	        ) AS illegal_times_rate';


            $where_map['tmedia.fid'] = ['IN',$medias];
            $where_map['_string'] = "DATE_FORMAT(FROM_UNIXTIME(a.fissuedate),'%Y-%m-%d') = '$date'";

            $order = 'fad_illegal_times';
            //电视

            $tv_sql = M($tv_issue_table)
                ->alias('a')
                ->field($fields_str)
                ->join('ttvsample AS ybb ON a.fsampleid = ybb.fid')
                ->join('tad ON ybb.fadid = tad.fadid AND tad.fadid <> 0')
                ->join('tmedia ON a.fmediaid = tmedia.fid AND tmedia.fid = tmedia.main_media_id')
                ->where($where_map)
                ->fetchSql(true)
                ->group('a.fmediaid')
                ->select();


            //广播
            $bc_sql = M($bc_issue_table)
                ->alias('a')
                ->field($fields_str)
                ->join('tbcsample AS ybb ON a.fsampleid = ybb.fid')
                ->join('tad ON ybb.fadid = tad.fadid AND tad.fadid <> 0')
                ->join('tmedia ON a.fmediaid = tmedia.fid AND tmedia.fid = tmedia.main_media_id')
                ->where($where_map)
                ->fetchSql(true)
                ->group('a.fmediaid')
                ->select();




            //报纸
            $paper_fields_str = "
                                tmedia.fid,
                                tmedia.fmedianame,
                                0 AS flength,
                                count(1) as fad_times,
                                count(
                                    CASE
                                    WHEN ybb.fillegaltypecode > 0 THEN
                                    1
                                    ELSE
                                    NULL
                                    END
                                ) AS fad_illegal_times,
                                ROUND(
                                    count(
                                        CASE
                                        WHEN ybb.fillegaltypecode > 0 THEN
                                        1
                                        ELSE
                                        NULL
                                        END
                                    ) / COUNT(1) * 100,
                                    2
                                ) AS illegal_times_rate";

            $where_map['_string'] = "DATE_FORMAT(a.fissuedate,'%Y-%m-%d') = '$date'";

            $paper_sql = M($paper_issue_table)
                ->alias('a')
                ->field($paper_fields_str)
                ->join('tpapersample AS ybb ON a.fpapersampleid = ybb.fpapersampleid')
                ->join('tad ON ybb.fadid = tad.fadid AND tad.fadid <> 0')
                ->join('tmedia ON a.fmediaid = tmedia.fid AND tmedia.fid = tmedia.main_media_id')
                ->where($where_map)
                ->group('a.fmediaid')
                ->fetchSql(true)
                ->select();//


            //互联网
            $net_fields_str = "
                                tmedia.fid,
                                tmedia.fmedianame,
                                0 AS flength,
                                count(1) as fad_times,
                                count(
                                    CASE
                                    WHEN ybb.fillegaltypecode > 0 THEN
                                    1
                                    ELSE
                                    NULL
                                    END
                                ) AS fad_illegal_times,
                                ROUND(
                                    count(
                                        CASE
                                        WHEN ybb.fillegaltypecode > 0 THEN
                                        1
                                        ELSE
                                        NULL
                                        END
                                    ) / COUNT(1) * 100,
                                    2
                                ) AS illegal_times_rate";

            $where_map['_string'] = "DATE_FORMAT(FROM_UNIXTIME(a.net_created_date/1000),'%Y-%m-%d') = '$date'";

            $net_sql = M($net_issue_table)
                ->alias('a')
                ->field($net_fields_str)
                ->join('tnetissue AS ybb ON a.tid = ybb.major_key')
                ->join('tmedia ON a.fmediaid = tmedia.fid AND tmedia.fid = tmedia.main_media_id')
                ->where($where_map)
                ->group('a.fmediaid')
                ->fetchSql(true)
                ->select();//



        }


        if($media_type == '01'){
            $media_res = M('')->table("($tv_sql) as tv")->order($order.' desc')->select();
        }
        elseif($media_type == '02'){
            $media_res = M('')->table("($bc_sql) as bc")->order($order.' desc')->select();
        }
        elseif($media_type == '03'){
            $media_res = M('')->table("($paper_sql) as paper")->order($order.' desc')->select();
        }
        elseif($media_type == '13'){
            $media_res = M('')->table("($net_sql) as net")->order($order.' desc')->select();
        }
        elseif($media_type == ''){

            $all_sql = M('')
                ->field('*')
                ->table("($tv_sql) as tv")
                ->union(array($bc_sql,$paper_sql,$net_sql),true)
                ->fetchSql(true)
                ->select();

            $media_res = M('')
                ->table("($all_sql) as ressql")
                ->order($order.' desc')
                ->select();
        }
        $data = [];
        $list_number = 0;
        $flength = 0;
        $fad_times = 0;
        $fad_illegal_times = 0;
        foreach ($media_res as $media_res_val){
            $flength += $media_res_val['flength'];
            $fad_times += $media_res_val['fad_times'];
            $fad_illegal_times += $media_res_val['fad_illegal_times'];
            $ill_list = $this->medias_ill_list($media_res_val['fid'],$date);

            if(!empty($ill_list)){
                foreach ($ill_list as $ill_list_key=>$ill_list_val){
                    if($ill_list_key > 0){
                        $data[] = [
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            $ill_list_val['fadname'],
                            $ill_list_val['fexpressions'],
                        ];
                    }else{
                        $data[] = [
                            strval(++$list_number),
                            $media_res_val['fmedianame'],
                            strval($media_res_val['flength']),
                            strval($media_res_val['fad_times']),
                            strval($media_res_val['fad_illegal_times']),
                            $media_res_val['illegal_times_rate'].'%',
                            $ill_list_val['fadname'],
                            $ill_list_val['fexpressions'],
                        ];
                    }
                }
            }else{
                $data[] = [
                    strval(++$list_number),
                    $media_res_val['fmedianame'],
                    strval($media_res_val['flength']),
                    strval($media_res_val['fad_times']),
                    strval($media_res_val['fad_illegal_times']),
                    $media_res_val['illegal_times_rate'].'%',
                    '',
                    ''
                ];
            }
        }
        $data[] = [
            strval(++$list_number),
            '合计',
            strval($flength),
            strval($fad_times),
            strval($fad_illegal_times),
            round(($fad_illegal_times/$fad_times)*100,2).'%',
            '',
            ''
        ];
        return $data;
    }

    /**by @jim 20180703 当前区域下传统媒体数量
     * @param $media_type 媒体类别
     * @param $m_table 月份表
     * @param $region_id 行政区域ID
     * @param $s_time 查询开始时间
     * @param $e_time 查询结束时间
     * @param $is_inquire_subordinate 是否包括下级单位
     * @return array|mixed
     */
    //权限范围内
    public function ct_media_count($media_type,$owner_id,$is_inquire_subordinate = 1){

        $regionid = session('regulatorpersonInfo.regionid');
        $system_num = getconfig('system_num');//获取国家局标记
        if(is_array($owner_id)){
            if(!empty($owner_id)){
                $where_str["_string"] = 'tmedia.fid IN ('.join(',',$owner_id).') AND tmediaowner.fregionid = "'.$regionid.'"';
                if($is_inquire_subordinate == 1){
                    $regionid = rtrim($regionid,'0');
                    $where_str["_string"] = "tmedia.fid IN (".join(',',$owner_id).") AND tmediaowner.fregionid LIKE '".$regionid."%' ";
                }
                if($is_inquire_subordinate == 2){

                    $owner_str = rtrim($regionid,'0');
                    $where_str["_string"] = "tmedia.fid IN (".join(',',$owner_id).") AND tmediaowner.fregionid LIKE '".$owner_str."%' AND tmediaowner.fregionid <> '".$regionid."' ";

                }
            }

        }else{
            $where_str = "tmediaowner.fregionid = '".$owner_id."' ";
            if($is_inquire_subordinate == 1){
                $owner_id = rtrim($owner_id,'0');
                $where_str["_string"] = "tmediaowner.fregionid LIKE '".$owner_id."%' ";
            }
            if($is_inquire_subordinate == 2){

                $owner_str = rtrim($owner_id,'0');
                $where_str["_string"] = "tmediaowner.fregionid LIKE '".$owner_str."%' AND tmediaowner.fregionid <> '".$owner_id."' ";
            }

        }
        if($media_type == 'tv'){
            $count = M('tmedia_temp')
                ->join("tmedia ON tmedia.fid = tmedia_temp.fmediaid AND tmedia.fid = tmedia.main_media_id")
                ->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')
                ->where([
                    'tmedia_temp.ftype'=>1,
                    'tmedia_temp.fcustomer'=>$system_num,
                    'tmedia_temp.fuserid'=>session('regulatorpersonInfo.fid'),
                    'left(tmedia.fmediaclassid,2)' => '01'
                ])
                ->where($where_str)
                ->count();
        }elseif($media_type == 'bc'){
            $count = M('tmedia_temp')
                ->join("tmedia ON tmedia.fid = tmedia_temp.fmediaid AND tmedia.fid = tmedia.main_media_id")
                ->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')
                ->where([
                    'tmedia_temp.ftype'=>1,
                    'tmedia_temp.fcustomer'=>$system_num,
                    'tmedia_temp.fuserid'=>session('regulatorpersonInfo.fid'),
                    'left(tmedia.fmediaclassid,2)' => '02'
                ])
                ->where($where_str)
                ->count();
        }elseif($media_type == 'paper'){
            $count = M('tmedia_temp')
                ->join("tmedia ON tmedia.fid = tmedia_temp.fmediaid AND tmedia.fid = tmedia.main_media_id")
                ->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')
                ->where([
                    'tmedia_temp.ftype'=>1,
                    'tmedia_temp.fcustomer'=>$system_num,
                    'tmedia_temp.fuserid'=>session('regulatorpersonInfo.fid'),
                    'left(tmedia.fmediaclassid,2)' => '03'
                ])
                ->where($where_str)
                ->count();
        }elseif($media_type == 'all'){
            $count = M('tmedia_temp')
                ->join("tmedia ON tmedia.fid = tmedia_temp.fmediaid AND tmedia.fid = tmedia.main_media_id")
                ->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')
                ->where([
                    'tmedia_temp.ftype'=>1,
                    'tmedia_temp.fcustomer'=>$system_num,
                    'tmedia_temp.fuserid'=>session('regulatorpersonInfo.fid'),
                    'left(tmedia.fmediaclassid,2)' => ['IN',['01','02','03','13']]
                ])
                ->where($where_str)
                ->count();
        }
        return $count;
    }

    /**by @jim 20180703 当前区域下传统媒体数量
     * @param $media_type 媒体类别
     * @param $m_table 月份表
     * @param $region_id 行政区域ID
     * @param $s_time 查询开始时间
     * @param $e_time 查询结束时间
     * @param $is_inquire_subordinate 是否包括下级单位
     * @return array|mixed
     */
    public function ct_media_ids($media_type,$m_table,$owner_id,$s_time,$e_time,$is_inquire_subordinate = 1){

        $regionid = session('regulatorpersonInfo.regionid');
        if(is_array($owner_id)){
            if(!empty($owner_id)){
                $where_str = 'tmedia.fid IN ('.join(',',$owner_id).') AND tmediaowner.fregionid = "'.$regionid.'"';
                if($is_inquire_subordinate == 1){
                    $regionid = rtrim($regionid,'0');
                    $where_str = "tmedia.fid IN (".join(',',$owner_id).") AND tmediaowner.fregionid LIKE '".$regionid."%' ";
                }
                if($is_inquire_subordinate == 2){

                    $owner_str = rtrim($regionid,'0');
                    $where_str = "tmedia.fid IN (".join(',',$owner_id).") AND tmediaowner.fregionid LIKE '".$owner_str."%' AND tmediaowner.fregionid <> '".$regionid."' ";
                }
            }

        }else{
            $where_str = "tmediaowner.fregionid = '".$owner_id."' ";
            if($is_inquire_subordinate == 1){
                $owner_id = rtrim($owner_id,'0');
                $where_str = "tmediaowner.fregionid LIKE '".$owner_id."%' ";
            }
            if($is_inquire_subordinate == 2){

                $owner_str = rtrim($owner_id,'0');
                $where_str = "tmediaowner.fregionid LIKE '".$owner_str."%' AND tmediaowner.fregionid <> '".$owner_id."' ";
            }

        }

        $tv_sql = "
                    SELECT
                        tv.fmediaid
                    FROM
                        ttvissue_".$m_table." as tv
                    JOIN tmedia ON tmedia.fid = tv.fmediaid
                    JOIN tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid
                    WHERE
                        ".$where_str."
                    AND tmedia.fid = tmedia.main_media_id
                    AND tv.fissuedate > '".$s_time."'
                    AND tv.fissuedate <= '".$e_time."'
                    GROUP BY
                        tv.fmediaid;";
        $bc_sql = "
                    SELECT
                        tb.fmediaid
                    FROM
                        tbcissue_".$m_table." as tb
                    JOIN tmedia ON tmedia.fid = tb.fmediaid
                    JOIN tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid
                    WHERE
                    ".$where_str."
                    AND tmedia.fid = tmedia.main_media_id
                    AND tb.fissuedate > '".$s_time."'
                    AND tb.fissuedate <= '".$e_time."'
                    GROUP BY
                        tb.fmediaid;";
        $paper_sql = "
                    SELECT
                        tp.fmediaid
                    FROM
                        tpaperissue as tp
                    JOIN tmedia ON tmedia.fid = tp.fmediaid
                    JOIN tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid
                    WHERE
                    ".$where_str."
                    AND tmedia.fid = tmedia.main_media_id
                    AND UNIX_TIMESTAMP(tp.fissuedate) > '".$s_time."'
                    AND UNIX_TIMESTAMP(tp.fissuedate) <= '".$e_time."'
                    GROUP BY
                        tp.fmediaid;";
        $medias = [];
        if($media_type == 'tv'){
            $tmedia_ids = M('')->query($tv_sql);
        }elseif($media_type == 'bc'){
            $tmedia_ids = M('')->query($bc_sql);
        }elseif($media_type == 'paper'){
            $tmedia_ids = M('')->query($paper_sql);
        }elseif($media_type == 'all'){
            $tv_sql_res = M('')->query($tv_sql);
            $bc_sql_res = M('')->query($bc_sql);
            $paper_sql_res = M('')->query($paper_sql);
            $tmedia_ids = array_merge($tv_sql_res,$bc_sql_res,$paper_sql_res);
        }
        foreach ($tmedia_ids as $tmedia_ids_val){
            $medias[] = $tmedia_ids_val['fmediaid'];
        }
        return $medias;
    }

    /**by @jim 20180703 当前区域下互联网媒体数量
     * @param $region_id 行政区域ID
     * @param $net_type 来源类别
     * @param $type 互联网类别
     * @param $s_time 查询开始时间
     * @param $e_time 查询结束时间
     * @return mixed
     */
    public function net_media_count($owner_id,$net_type,$type,$s_time,$e_time,$is_inquire_subordinate=true){
        $system_num = getconfig('system_num');//获取国家局标记

        if(is_array($owner_id)){
            if(!empty($owner_id)){
                $where_str = 'tmedia.fid IN ('.join(',',$owner_id).')';
            }
        }else{

            if($is_inquire_subordinate){
                $owner_id = rtrim($owner_id,'0');
            }
            $where_str = "tmediaowner.fregionid LIKE '".$owner_id."%'";

        }

        $sql = 'SELECT COUNT(net_media_count) as net_media_count FROM (
                SELECT
                COUNT(DISTINCT tnetissueputlog.fmediaid) as net_media_count
                FROM
                    tnetissueputlog
                JOIN tnetissue ON tnetissue.major_key = tnetissueputlog.tid  and finputstate=2 
                JOIN tadclass ON tnetissue.fadclasscode = tadclass.fcode
                JOIN tmedia ON tmedia.fid = tnetissueputlog.fmediaid
                JOIN tmedia_temp ON tmedia.fid = tmedia_temp.fmediaid
                JOIN tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid
                WHERE
                    '.$where_str.'
                AND tmedia_temp.ftype=1 
                AND tmedia_temp.fcustomer = "'.$system_num.'"
                AND tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').'
                AND tnetissue.'.$net_type.' = '.$type.'  and finputstate=2 
                AND tmedia.fid = tmedia.main_media_id
                AND tnetissueputlog.net_created_date/1000 > "'.$s_time.'"
                AND tnetissueputlog.net_created_date/1000 <= "'.$e_time.'" GROUP BY tnetissueputlog.fmediaid) as a;';
        $net_media_count = M('')->query($sql);
        return $net_media_count[0]['net_media_count'];
    }


    /**by @jim 20180703 当前区域下互联网媒体数量
     * @param $region_id 行政区域ID
     * @param $net_type 来源类别
     * @param $type 互联网类别
     * @param $s_time 查询开始时间
     * @param $e_time 查询结束时间
     * @return mixed
     */
    public function net_media_counts($owner_id,$media_class){
        $system_num = getconfig('system_num');//获取国家局标记

        $res_sql = "SELECT
                        COUNT(1) as m_count
                    FROM
                        tmedia
                    JOIN tmediaclass ON tmediaclass.fid = tmedia.fmediaclassid
                    JOIN tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid
                    JOIN (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = '".$system_num."' and tmedia_temp.fuserid=".session('regulatorpersonInfo.fid')." ) as ttp on tmedia.fid=ttp.fmediaid
                    WHERE
                        tmediaowner.fregionid LIKE '$owner_id%' AND
                        tmedia.fmediaclassid like '$media_class%';";
        $res = M('')->query($res_sql);
        if(!empty($res)){
            return $res[0]['m_count'];
        }else{
            return 0;
        }

    }

    /*获取客户媒体数量 分区划*/
    public function cus_media_count($group = '',$media_class = ['01','02','03','13'],$level=[1,2,3,4,5]){

        $system_num = getconfig('system_num');//获取国家局标记
        if(!empty($group)){
            $group_f = $group.',';
            $group_by = ' group by '.$group;
        }
        $sql = "SELECT
                    ".$group_f."
                    GROUP_CONCAT((case when instr(tmedia.fmedianame,'（') > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,'（') -1) else tmedia.fmedianame end) separator '、') as tmedia_names,
                    COUNT(1) AS m_count
                FROM
                    tmedia
                JOIN tmediaclass ON tmediaclass.fid = tmedia.fmediaclassid
                JOIN tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid
                JOIN tregion ON tmedia.media_region_id = tregion.fid
                JOIN (
                    SELECT DISTINCT
                        (fmediaid)
                    FROM
                        tmedia_temp
                    WHERE
                        tmedia_temp.fcustomer = ".$system_num."
                    AND tmedia_temp.fuserid = ".session('regulatorpersonInfo.fid')."
                ) AS ttp ON tmedia.fid = ttp.fmediaid
                WHERE
                     tregion.flevel IN(".join(',',$level).")
                AND LEFT(tmedia.fmediaclassid,2) in(".join(',',$media_class).") ".$group_by.";";

        $res = M('')->query($sql);

        if(!empty($res)){

            return $res;

        }else{
            return [];
        }
    }



    /**by @jim 20180703 当前区域下互联网媒体数量
     * @param $region_id 行政区域ID
     * @param $change_field 来源类别查询条件
     * @param $s_time 查询开始时间
     * @param $e_time 查询结束时间
     * @return mixed0918
     */
    public function net_media_customize_sum($owner_id,$change_field = false,$s_time,$e_time,$fmediaid = false,$is_inquire_subordinate=true){
        $system_num = getconfig('system_num');//获取国家局标记

        if(is_array($owner_id)){
            if(!empty($owner_id)){
                $where_str = 'tmedia.fid IN ('.join(',',$owner_id).')';
            }
        }else{

            if($is_inquire_subordinate){
                $owner_id = rtrim($owner_id,'0');
            }
            $where_str = "tmediaowner.fregionid LIKE '".$owner_id."%'";

        }

        if(is_array($change_field)){
            $where = 'AND tns.'.$change_field[0].' = '.$change_field[1];
        }else{
            $where = '';
        }
        if($fmediaid !== false){
            $where .= ' AND tn.fmediaid = '.$fmediaid;
        }
        $sql = 'SELECT
                count(*) total,
                ifnull(sum(
                    CASE
                    WHEN tns.fillegaltypecode = 30 THEN
                        1
                    ELSE
                        0
                    END
                ),0) AS yzwfggsl,
                ifnull(
                    round((sum(
                        CASE
                        WHEN tns.fillegaltypecode = 30 THEN
                            1
                        ELSE
                            0
                        END
                    ) / count(*))*100,2),
                    0
                ) AS ggslyzwfl,
                ifnull(sum(
                    CASE
                    WHEN tns.fillegaltypecode > 0 THEN
                        1
                    ELSE
                        0
                    END
                ),0) AS wfggsl,
                ifnull(
                    round((sum(
                        CASE
                        WHEN tns.fillegaltypecode > 0 THEN
                            1
                        ELSE
                            0
                        END
                    ) / count(*))*100,2),
                    0
                ) AS ggslwfl,
            count(DISTINCT tn.tid) AS ts,
			count(DISTINCT CASE WHEN tns.fillegaltypecode > 0 THEN tn.tid ELSE NULL END ) AS wfts,
			                ifnull(
                    round((count(DISTINCT CASE WHEN tns.fillegaltypecode > 0 THEN tn.tid ELSE NULL END ) / count(DISTINCT tn.tid))*100,2),
                    0
                ) AS tswfl,
			tn.fmediaid,
			tns.ftype
			FROM
                tnetissueputlog tn
            JOIN tnetissue tns ON tns.major_key = tn.tid and tns.finputstate = 2 
            JOIN tmedia ON tmedia.fid = tn.fmediaid
            JOIN tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid
            JOIN tregion ON tregion.fid = tmediaowner.fregionid
            JOIN tadclass ON tadclass.fcode = tns.fadclasscode
            JOIN (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid
            WHERE
            '.$where_str.' '.$where. '
			AND tmedia.fid = tmedia.main_media_id
			AND tn.is_first_broadcast = 1
            AND (tn.net_created_date / 1000) BETWEEN "'.$s_time.'" AND "'.$e_time.'"';
        $net_media_customize = M('')->query($sql);

        return $net_media_customize[0];
    }


    /** by yjn
     * 报告汇总接口
     * @param $fztj  分组统计条件1.fmedia_class_code媒体类别排名 2.fmediaownerid媒介机构排名 3.fregionid地域排名 4.fmediaid媒介排名 5.fad_class_code广告类别排名
     * @param $media_ids 权限下的媒体
     * @param $region 要查询的行政级别
     * @param $s_date 开始日期
     * @param $e_date   结束日期
     * @param string $onther_class 指定排名条件
     * @param string $media_class 指定媒介类型 01电视 02广播 03报纸 13互联网
     * @param array $ad_class 指定广告类别
     * @param int $is_have_self 是否包含本级
     * @param int $is_show_longad 是否显示长广告
     * @param int $is_show_fsend 是否确认数据
     * @param int $region_order 是否强制地域排名
     * @param int $isfixeddata 是否实时数据
     * @param int $ad_pm_type 是监测汇总还是线索汇总
     */
    public function report_ad_monitor($fztj,$media_ids,$region,$s_time,$e_time,$onther_class='times_illegal_rate',$media_class='',$ad_class=[],$is_have_self=1,$is_show_longad=1,$is_show_fsend=1,$region_order = 0,$isfixeddata = 1,$ad_pm_type=2){

        if(empty($media_ids)){
            return [];
        }

        ini_set('memory_limit','3072M');
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type: text/html; charset=utf-8");
        $user = session('regulatorpersonInfo');//获取用户信息
        $system_num = getconfig('system_num');
        $ischeck = getconfig('ischeck');
        $is_manage_media = getconfig('set_manage_region');
        $isrelease = getconfig('isrelease');

        $map = [];//定义查询条件

        if($system_num == '100000' || $isfixeddata != 1){
            //如果是国家局系统或者不查询实时数据，则查询国家局表
            $summary_table = "tbn_ad_summary_day_z";
            $map[$summary_table.'.fcustomer'] = $user['system_num'];

            if($is_show_longad == 1){
                //包含长广告
                $play_len_field = 'in_long_ad_play_len';
                $fad_times_field = 'fad_times';
                $fad_count_field = 'fad_sam_list';
            }else{
                //不包含长广告
                $play_len_field = 'fad_play_len';
                $fad_times_field = 'basic_ad_times';
                $fad_count_field = 'basic_ad_sam_list';
            }
        }else{
            //如果是地方局系统
            $use_open_search = getconfig('use_open_search');
            if($use_open_search == 1) {
                $summary_table = "tbn_ad_summary_day_v3";
                $map[$summary_table.'.fcustomer'] = $user['system_num'];
                if($is_show_longad == 1){
                    //包含长广告
                    $play_len_field = 'fad_play_len_long_ad';
                    $fad_times_field = 'fad_times_long_ad';
                    $fad_count_field = 'fsam_list_long_ad';
                }else{
                    //不包含长广告
                    $play_len_field = 'fad_play_len';
                    $fad_times_field = 'fad_times';
                    $fad_count_field = 'fsam_list';
                }
            }else{
                $summary_table = "tbn_ad_summary_day";
                if($is_show_longad == 1){
                    $play_len_field = 'in_long_ad_play_len';
                    $fad_times_field = 'fad_times';
                    $fad_count_field = 'fad_sam_list';
                }else{
                    $play_len_field = 'fad_play_len';
                    $fad_times_field = 'basic_ad_times';
                    $fad_count_field = 'basic_ad_sam_list';
                }
            }
        }

        $map[$summary_table.'.fdate'] = ['BETWEEN',[$s_time,$e_time]];
        $map['_string'] = 'tmedia.fid = tmedia.main_media_id';
        $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_time,$e_time]];
        $illegal_map['tbn_illegal_ad.fstatus3'] = ['NOT IN',[30,40]];
        $s_timestamp = strtotime($s_time);//开始时间戳
        $e_timestamp = strtotime($e_time);//结束时间戳

        //获取上个周期的日期范围
        if(($e_timestamp - $s_timestamp) <= 0){
            $last_s_time = date('Y-m-d',($s_timestamp-86400));//上周期开始时间戳
            $last_e_time = $last_s_time;//上周期结束时间戳
        }else{
            $last_s_time = date('Y-m-d',($s_timestamp-($e_timestamp-$s_timestamp)-86400));
            $last_e_time = date('Y-m-d',($s_timestamp-86400));
        }

        //浏览权限设置start
        $re_level = $this->judgment_region($user['regionid']);//当前用户行政等级

        //是否需要抽查
        if(!empty($ischeck)){
            $spot_check_data = M('spot_check')->where(['fcustomer'=>$user['system_num']])->select();//查询抽查表数据
            //如果抽查表有数据
            if(!empty($spot_check_data)){
                $dates = [];//定义日期数组
                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                    $condition = trim($spot_check_data_val['condition']);
                    if(!empty($condition)){
                        $year_month = '';
                        $date_str = [];
                        $year_month = substr($spot_check_data_val['fmonth'],0,7);
                        if(!empty($spot_check_data_val['condition'])){
                            $date_str = explode(',',$spot_check_data_val['condition']);
                        }
                        foreach ($date_str as $date_str_val){

                            $dates[] = $year_month.'-'.$date_str_val;
                        }
                    }
                }
                $map[$summary_table.'.fdate'] = [['IN',$dates],['BETWEEN',[$s_time,$e_time]],'AND'];
                $illegal_map['tbn_illegal_ad_issue.fissue_date'] = [['IN',$dates],['BETWEEN',[$s_time,$e_time]],'AND'];
            }else{
                $map['_string'] = '1 = 0';
                $illegal_map['_string'] = '1 = 0';
            }
        }
        //判断用户是查询监测汇总还是线索汇总
        //线索汇总
        if($ad_pm_type == 2){

            if(!empty($isrelease) || $system_num == '100000'){
                $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
            }
            $illegal_map['tbn_illegal_ad.fcustomer'] = $system_num;

            $map['tmedia.fid'] = ['IN',$media_ids];//本级以及下级权限下的媒体
            $illegal_map['tbn_illegal_ad_issue.fmedia_id'] = array('in',$media_ids);//权限媒体
            //控制查询行政级别
            if($region !== false){
                switch ($region){
                    case 0:
                    case 1://全国各省违法线索情况
                        if($re_level == 0){
                            $map["tregion.flevel"] = 1;
                            $illegal_map["tregion.flevel"] = 1;
                        }elseif($re_level == 1){
                            $map['tregion.fid'] = $user['regionid'];
                            $illegal_map['tregion.fid'] = $user['regionid'];
                        }
                        break;
                    case 2://副省级
                        if($re_level == 0){
                            //全国各副省级市违法线索情况
                            $map["tregion.flevel"] = 2;
                            $illegal_map["tregion.flevel"] = 2;
                        }elseif($re_level == 1){
                            //本省各副省级市违法线索情况
                            if($is_have_self == 1){//如果包含本级
                                $map["tregion.flevel"] = ['IN','1,2'];
                                $illegal_map["tregion.flevel"] = ['IN','1,2'];
                                $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                                $illegal_map['tregion.fpid|tregion.fid'] = $user['regionid'];
                            }else{//不包含本级
                                $map["tregion.flevel"] = 2;
                                $illegal_map["tregion.flevel"] = 2;
                                $map["tregion.fpid"] = $user['regionid'];
                                $illegal_map["tregion.fpid"] = $user['regionid'];
                            }
                        }elseif($re_level == 2){//如果是当前用户是副省级只显示自己
                            $map["tregion.fpid"] = $user['regionid'];
                            $illegal_map["tregion.fpid"] = $user['regionid'];
                        }
                        break;
                    case 3://各计划单列市
                        if($re_level == 0){//如果当前用户是国家局
                            //全国各计划单列市违法线索情况
                            $map["tregion.flevel"] = 3;
                            $illegal_map["tregion.flevel"] = 3;
                        }elseif($re_level == 1){//如果对当前用户是省局
                            //本省各计划单列市违法线索情况
                            if($is_have_self == 1){
                                $map["tregion.flevel"] = ['IN','1,3'];
                                $illegal_map["tregion.flevel"] = ['IN','1,3'];
                                $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                                $illegal_map['tregion.fpid|tregion.fid'] = $user['regionid'];
                            }else{
                                $map["tregion.flevel"] = 3;
                                $illegal_map["tregion.flevel"] = 3;
                                $map["tregion.fpid"] = $user['regionid'];
                                $illegal_map["tregion.fpid"] = $user['regionid'];
                            }

                        }elseif($re_level == 3){
                            $map["tregion.fpid"] = $user['regionid'];
                            $illegal_map["tregion.fpid"] = $user['regionid'];
                        }
                        break;
                    case 4://各市
                        if($re_level == 0){
                            //如果是全国各市违法线索情况
                            $map["tregion.flevel"] = 4;
                            $illegal_map["tregion.flevel"] = 4;
                        }elseif($re_level == 1){
                            //本省各市违法线索情况
                            if($is_have_self == 1){//此处包含了副省级、计划单列市、市级
                                $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                                $illegal_map['tregion.fpid|tregion.fid'] = $user['regionid'];
                            }else{
                                //此处包含了副省级、计划单列市、市级
                                $map["tregion.fpid"] = $user['regionid'];
                                $illegal_map["tregion.fpid"] = $user['regionid'];
                            }
                        }elseif($re_level == 4 || $re_level == 3 ||$re_level == 2){//如果当前用户为副省级、计划单列市、市级，只显示自己
                            $map["tregion.fid"] = $user['regionid'];
                            $illegal_map["tregion.fid"] = $user['regionid'];
                        }
                        break;
                    case 5://各区县
                        if($re_level == 0){
                            //全国各区县违法线索情况
                            $map["tregion.flevel"] = 5;
                            $illegal_map["tregion.flevel"] = 5;
                        }elseif($re_level == 1){
                            //本省各区县违法线索情况
                            $fpid = substr($user['regionid'],0,2);
                            $map["tregion.fpid"] = ['like',$fpid.'%'];
                            $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                            $map["tregion.flevel"] = 5;
                            $illegal_map["tregion.flevel"] = 5;
                        }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                            //本市各区县违法线索情况
                            if($is_have_self == 1){
                                $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                                $illegal_map['tregion.fpid|tregion.fid'] = $user['regionid'];
                            }else{
                                $fpid = substr($user['regionid'],0,4);
                                $map["tregion.fpid"] = ['like',$fpid.'%'];
                                $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                                $map["tregion.flevel"] = 5;
                                $illegal_map["tregion.flevel"] = 5;
                            }
                        }elseif($re_level == 5){
                            $map["tregion.fid"] = $user['regionid'];
                            $illegal_map["tregion.fid"] = $user['regionid'];
                            $map["tregion.flevel"] = 5;
                            $illegal_map["tregion.flevel"] = 5;
                        }
                        break;
                }
            }
        }else{

            $map["tregion.flevel"] = ['IN','1,2,3'];//国家局只显示省级，副省级，计划单列市的数据

            //判断是否国家局系统
            $map[$summary_table.'.confirm_state'] = ['GT',0];//确认数据状态要大于零

            $gjj_label_media = $this->get_gjj_label_media();//获取国家局标签媒体

            $map[$summary_table.'.fmediaid'] = array('in',$gjj_label_media);//国家局标签媒体
            $illegal_map['tbn_illegal_ad_issue.fmedia_id'] = array('in',$gjj_label_media);//国家局标签媒体

            //是否显示发布前数据
            if(!empty($isrelease)){
                $illegal_map['tbn_illegal_ad_issue.fsend_status'] = ['IN',[1,2]];
            }else{
                $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
            }
            
            //在监测数据中，只显示国家局客户数据
            $illegal_map['tbn_illegal_ad.fcustomer'] = "100000";
            $map[$summary_table.'.fcustomer'] = "100000";
            if($region !== false){
                switch ($region){
                    case 0:
                        $map["tregion.flevel"] = ['IN',[1,2,3]];
                        $illegal_map["tregion.flevel"] = ['IN',[1,2,3]];
                        break;
                    case 1://省级和国家级的各省监测情况

                        $map["tregion.flevel"] = 1;
                        $illegal_map["tregion.flevel"] = 1;
                        break;
                    case 2://副省级市
                        $map["tregion.flevel"] = 2;
                        $illegal_map["tregion.flevel"] = 2;
                        break;
                    case 3://各计划单列市
                        $map["tregion.flevel"] = 3;
                        $illegal_map["tregion.flevel"] = 3;
                        break;
                    case 4://各市
                        if($re_level == 0){
                            //国家级用户的各市监测情况
                            $map["tregion.flevel"] = 4;
                            $illegal_map["tregion.flevel"] = 4;
                        }else{
                            //市级或者省级用户的各市监测情况
                            if($re_level == 4 || $re_level == 2 || $re_level == 3){
                                $fpid = substr($user['regionid'],0,2).'0000';
                                $map["tregion.fpid"] = $fpid;
                                $illegal_map["tregion.fpid"] = $fpid;
                            }elseif($re_level == 1){
                                $map['tregion.fpid|tregion.fid'] = $user['regionid'];
                                $illegal_map['tregion.fpid|tregion.fid'] = $user['regionid'];
                            }
                        }
                        break;
                    case 5://各区县
                        if($re_level == 0){
                            //如果是全国用户
                            $map["tregion.flevel"] = 5;
                            $illegal_map["tregion.flevel"] = 5;
                        }elseif($re_level == 1){
                            //如果是省级用户
                            $fpid = substr($user['regionid'],0,2);
                            $map["tregion.fpid"] = ['like',$fpid.'%'];
                            $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                            $map["tregion.flevel"] = 5;
                            $illegal_map["tregion.flevel"] = 5;
                        }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                            //$map['tregion.fpid|tregion.fid'] = $user['regionid'];
                            //$illegal_map['tregion.fpid|tregion.fid'] = $user['regionid'];
                            $map['tregion.fpid'] = $user['regionid'];
                            $illegal_map['tregion.fpid'] = $user['regionid'];
                            $map["tregion.flevel"] = 5;
                            $illegal_map['tregion.flevel'] = 5;
                        }elseif($re_level == 5){
                            $fpid = substr($user['regionid'],0,4);
                            $map["tregion.fpid"] = ['like',$fpid.'%'];
                            $illegal_map["tregion.fpid"] = ['like',$fpid.'%'];
                            $map["tregion.flevel"] = 5;
                            $illegal_map["tregion.flevel"] = 5;
                        }
                        break;
                }
            }

        }
        //浏览权限设置end

        //媒介类型
        if($media_class != ''){
            $map[$summary_table.'.fmedia_class_code'] = $media_class;//媒体
            $illegal_map['tbn_illegal_ad.fmedia_class'] = intval($media_class);
        }

        //为空，或者指定类别
        if(!empty($ad_class)){
            $map[$summary_table.'.fad_class_code'] = ['IN',$ad_class];//广告类别
            $illegal_map['left(tbn_illegal_ad.fad_class_code,2)'] = ['IN',$ad_class];//广告类别
        }

        //排名条件
        if($onther_class == ''){
            if($fztj == 'fad_class_code'){
                //如果是广告类别排名的话,默认条次占比排名
                $onther_class =  'fad_illegal_times';
            }else{
                //如果是其他的话,默认条次评分排名
                $onther_class =  'fad_times';
            }
        }
        //如果是地域排名 按行政区划排名
        $onther_class_val = $onther_class;
        if($fztj == 'fregionid' && $region_order == 1){
            $onther_class = 'fregionid';
        }

        $data = [];//定义统计数据空数组

        //按照选定时间实例化表
        $table_model = M($summary_table);

        $id = 'fid';//定义默认表ID
        $name = 'fname';//定义默认单元名称

        if($is_manage_media != 1){
            $ill_region_join = "tregion.fid = tbn_illegal_ad.fregion_id";
            $xs_group_manage = "tbn_illegal_ad.fregion_id";
            $manage_region_join = "tregion.fid = $summary_table.fregionid";
            $manage_id = "$summary_table.fregionid";
        }else{
            $xs_group_manage = "tmedia.manage_region_id";
            $ill_region_join = "tregion.fid = tmedia.manage_region_id";
            $manage_region_join = "tregion.fid = tmedia.manage_region_id";
            $manage_id = "tmedia.manage_region_id";
        }



        //匹配分组条件，连接不同表
        $tregion_name = "tregion.fname1 as tregion_name";
        $group_field = $summary_table.'.'.$fztj;
        $group_by = $summary_table.'.'.$fztj;
        switch ($fztj){
            case 'fmedia_class_code':
                $join_table = 'tmediaclass';//fmedia_class_code媒介类别表fid,
                $name = 'fclass';
                $xs_group = 'tbn_illegal_ad.fmedia_class';
                $xs_group_field = 'tbn_illegal_ad.fmedia_class';
                $hb_field = 'fmedia_class';
                break;
            case 'fmediaownerid':
                $join_table = 'tmediaowner';//fmediaownerid媒介机构表fid
                $xs_group = 'tbn_illegal_ad.fmediaownerid';
                $xs_group_field = 'tbn_illegal_ad.fmediaownerid';
                $hb_field = 'fmediaownerid';
                $menu = '媒介机构';
                break;
            case 'fregionid':
                $join_table = 'tregion';//fregionid行政区划表fid
                $hb_field = 'fregion_id';
                $field_name = 'tregion_name';
                $menu = '地域';
                if($region === 0){

                    $xs_group_field = "CONCAT(left(tbn_illegal_ad.fregion_id,2),'0000') as fregion_id";//1102
                    $xs_group = "left($xs_group_manage,2)";
                    $tregion_name = "(SELECT tregion.fname1 FROM tregion WHERE tregion.fid = CONCAT(left($manage_id,2),'0000') LIMIT 1) AS tregion_name";
                    $group_field = 'CONCAT(left(tbn_ad_summary_day_z.fregionid,2),"0000") as fregionid';
                    $group_by = 'left(tbn_ad_summary_day_z.fregionid,2)';

                }else{

                    $group_field = $summary_table.'.'.$fztj.','.$join_table.'.forder';
                    $xs_group = $xs_group_manage;
                    $xs_group_field = $xs_group_manage." as fregion_id";

                    if($is_manage_media == 1){
                        $group_field = $manage_id.' as fregionid,'.$join_table.'.forder';
                        $group_by = $manage_id;
                    }

                }


                break;
            case 'fmediaid':
                $join_table = 'tmedia';//fmediaid媒介表fid
                $name = 'fmedianame';
                $xs_group = 'tbn_illegal_ad.fmedia_id';
                $xs_group_field = 'tbn_illegal_ad.fmedia_id';
                $hb_field = 'fmedia_id';
                $menu = '媒体';
                break;
            case 'fad_class_code':
                $join_table = 'tadclass';//fad_class_code广告内容类别表fcode
                $id = 'fcode';
                $name = 'fadclass';
                $xs_group = 'left(tbn_illegal_ad.fad_class_code,2)';
                $xs_group_field = 'left(tbn_illegal_ad.fad_class_code,2) as fad_class_code';
                $hb_field = 'fad_class_code';
                $menu = '广告类别';
                $field_name = 'tadclass_name';
                break;
        }

        //审核通过的违法数据
        $isexamine = getconfig('isexamine');
        if(in_array($isexamine, [10,20,30,40])){
            $illegal_map['tbn_illegal_ad.fexamine'] = 10;
        }
        //违法广告条次与时长
        //是否包含违法长广告时长
        if($is_show_longad != 1){
            $illegal_map['_string'] = '(unix_timestamp(tbn_illegal_ad_issue.fendtime) - unix_timestamp(tbn_illegal_ad_issue.fstarttime) < 600 OR tbn_illegal_ad.fmedia_class > 2)';
        }
        $illegal_group = 'tbn_illegal_ad.'.$xs_group;

      /*if(substr($system_num,0,2) == '34'){
            $illegal_count_field = 'fad_name,tbn_illegal_ad.fmedia_id,tbn_illegal_ad.fillegal,tbn_illegal_ad.fillegal_code';
        }else{
            $illegal_count_field = 'fsample_id';
        }*/


        $tbn_illegal_data = M('tbn_illegal_ad_issue')
            ->cache(true,600)
            ->field("
              $xs_group_field,
              COUNT(DISTINCT tbn_illegal_ad.fsample_id) AS fad_illegal_count,
              SUM(
                UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fendtime
                ) - UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fstarttime
                )
                ) AS fad_illegal_play_len,
              COUNT(1) AS fad_illegal_times
            ")
            ->where($illegal_map)
            ->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                $ill_region_join"
            )
            ->join("
                tadclass on 
                tadclass.fcode = tbn_illegal_ad.fad_class_code "
            )
            ->group($xs_group)
            ->select();

        $data = [];
        $data = $table_model
            ->cache(true,600)
            ->field("
                $group_field,
                tmediaclass.fclass as tmediaclass_name,
                tmediaowner.fname as tmediaowner_name,
                $tregion_name,
                tregion.flevel as flevel,
                 (case when instr(tmedia.fmedianame,'（') > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,'（') -1) else tmedia.fmedianame end) as tmedia_name,
                tadclass.fadclass as tadclass_name,
                $summary_table.fmediaid as fmediaid,
                $summary_table.fmediaownerid as fmediaownerid,
                $summary_table.fmedia_class_code as fmedia_class_code,
                GROUP_CONCAT($fad_count_field SEPARATOR ',') as fad_sam_list,
                $summary_table.fad_class_code as fad_class_code,
                sum($summary_table.$fad_times_field) as fad_times,
                sum($summary_table.$play_len_field) as fad_play_len
           ")
            ->join("
                tmediaclass on 
                tmediaclass.fid = $summary_table.fmedia_class_code"
            )
            ->join("
                tmediaowner on 
                tmediaowner.fid = $summary_table.fmediaownerid"
            )
            ->join("
                tmedia on 
                tmedia.fid = $summary_table.fmediaid"
            )
            ->join("
                tregion on 
                $manage_region_join"
            )
            ->join("
                tadclass on 
                tadclass.fcode = $summary_table.fad_class_code"
            )
            ->where($map)
            //->fetchSql(true)
            ->group($group_by)
            ->select();
            //dump($data);exit;

        //循环计算条数
        foreach ($data as $data_key => $data_val){
            $sam_array = explode(',',$data_val['fad_sam_list']);
            $fad_count = count(array_unique($sam_array));
            unset($data[$data_key]['fad_sam_list']);
            $data[$data_key]['fad_count'] = $fad_count;//插入条数
        }

        //组合违法线索相关数据
        foreach ($tbn_illegal_data as $tbn_illegal_data_key=>$tbn_illegal_data_value){
            foreach ($data as $data_key=>$data_value){
                if($tbn_illegal_data_value[$hb_field] == $data_value[$fztj]){
                    unset($tbn_illegal_data_value[$hb_field]);
                    $data[$data_key]['counts_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_count']/$data_value['fad_count'];//条数违法率
                    $data[$data_key]['fad_illegal_count'] = $tbn_illegal_data_value['fad_illegal_count'];//违法条数
                    $data[$data_key]['times_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_times']/$data_value['fad_times'];//条次违法率
                    $data[$data_key]['lens_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_play_len']/$data_value['fad_play_len'];//时长违法率
                    $data[$data_key] = array_merge($tbn_illegal_data_value, $data[$data_key]);//合并数组
                }
            }
        }
        //循环计算
        foreach ($data as $data_key => $data_val){
            if(!isset($data_val['fad_illegal_count'])){
                $data[$data_key]['fad_illegal_count'] = 0;
            }
            if(!isset($data_val['counts_illegal_rate'])){
                $data[$data_key]['counts_illegal_rate'] = 0;
            }

            if(!isset($data_val['fad_illegal_times'])){
                $data[$data_key]['fad_illegal_times'] = 0;
            }
            if(!isset($data_val['times_illegal_rate'])){
                $data[$data_key]['times_illegal_rate'] = 0;
            }
            if(!isset($data_val['fad_illegal_play_len'])){
                $data[$data_key]['fad_illegal_play_len'] = 0;
            }
            if(!isset($data_val['lens_illegal_rate'])){
                $data[$data_key]['lens_illegal_rate'] = 0;
            }
            $all_count['fad_count'] += $data[$data_key]['fad_count'];
            $all_count['fad_illegal_count'] += $data[$data_key]['fad_illegal_count'];
            $all_count['fad_illegal_times'] += $data[$data_key]['fad_illegal_times'];
            $all_count['fad_times'] += $data[$data_key]['fad_times'];
            $all_count['fad_illegal_play_len'] += $data[$data_key]['fad_illegal_play_len'];
            $all_count['fad_play_len'] += $data[$data_key]['fad_play_len'];
        }


        //上个周期的违法广告条次与时长
        if(!empty($dates)){
            $illegal_map['tbn_illegal_ad_issue.fissue_date'] = [['IN',$dates],['BETWEEN',[$last_s_time,$last_e_time]],'AND'];
        }else{
            $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$last_s_time,$last_e_time]];
        }
        $tbn_prv_illegal_data = M('tbn_illegal_ad_issue')
            ->field("
              $xs_group_field,
              COUNT(DISTINCT tbn_illegal_ad.fsample_id) AS fad_illegal_count,
              SUM(
                UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fendtime
                ) - UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fstarttime
                )
                ) AS fad_illegal_play_len,
              COUNT(1) AS fad_illegal_times
            ")
            ->where($illegal_map)
            ->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad.fmedia_id  and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                $ill_region_join"
            )
            ->join("
                tadclass on 
                tadclass.fcode = tbn_illegal_ad.fad_class_code "
            )
            ->group($xs_group)
            ->select();

        //查询上个周期的数据
        if(!empty($dates)){
            $map[$summary_table.'.fdate'] = [['IN',$dates],['BETWEEN',[$last_s_time,$last_e_time]],'AND'];
        }else{
            $map[$summary_table.'.fdate'] = ['BETWEEN',[$last_s_time,$last_e_time]];
        }
        $illegal_prv_ad_mod = $table_model
            ->cache(true,600)
            ->field("
                $group_field,
                GROUP_CONCAT($fad_count_field SEPARATOR ',') as fad_sam_list,
                $summary_table.fad_class_code as fad_class_code,
                sum($summary_table.fad_times) as fad_times,
                sum($summary_table.$play_len_field) as fad_play_len
            ")
            ->join("
            tmediaclass on 
            tmediaclass.fid = $summary_table.fmedia_class_code"
            )
            ->join("
            tmediaowner on 
            tmediaowner.fid = $summary_table.fmediaownerid"
            )
            ->join("
            tmedia on 
            tmedia.fid = $summary_table.fmediaid"
            )
            ->join("
            tregion on 
            $manage_region_join"
            )
            ->join("
            tadclass on 
            tadclass.fcode = $summary_table.fad_class_code"
            )
            ->where($map)
            ->group($group_by)
            ->select();

        //循环计算条数
        foreach ($illegal_prv_ad_mod as $illegal_prv_ad_mod_key => $illegal_prv_ad_mod_val){
            $sam_array = explode(',',$tbn_prv_illegal_data_val['fad_sam_list']);
            $fad_count = count(array_unique($sam_array));
            unset($illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_sam_list']);
            $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_count'] = $fad_count;//插入条数
        }
        //组合上个周期违法线索相关数据
        foreach ($tbn_prv_illegal_data as $tbn_illegal_data_key=>$tbn_illegal_data_value){
            foreach ($illegal_prv_ad_mod as $data_key=>$data_value){
                if($tbn_illegal_data_value[$hb_field] == $data_value[$fztj]){
                    unset($tbn_illegal_data_value[$hb_field]);
                    $illegal_prv_ad_mod[$data_key]['counts_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_count']/$data_value['fad_count'];//条数违法率
                    $illegal_prv_ad_mod[$data_key]['fad_illegal_count'] = $tbn_illegal_data_value['fad_illegal_count'];//违法条数
                    $illegal_prv_ad_mod[$data_key]['times_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_times']/$data_value['fad_times'];//合并数组
                    $illegal_prv_ad_mod[$data_key]['lens_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_play_len']/$data_value['fad_play_len'];//合并数组
                    $illegal_prv_ad_mod[$data_key] = array_merge($tbn_illegal_data_value, $illegal_prv_ad_mod[$data_key]);//合并数组
                }
            }
        }
        foreach ($illegal_prv_ad_mod as $illegal_prv_ad_mod_key => $illegal_prv_ad_mod_val){
            if(!isset($illegal_prv_ad_mod_val['fad_illegal_count'])){

                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_count'] = 0;
            }
            if(!isset($illegal_prv_ad_mod_val['counts_illegal_rate'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['counts_illegal_rate'] = 0;
            }

            if(!isset($illegal_prv_ad_mod_val['fad_illegal_times'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_times'] = 0;
            }

            if(!isset($illegal_prv_ad_mod_val['times_illegal_rate'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['times_illegal_rate'] = 0;
            }
            if(!isset($illegal_prv_ad_mod_val['fad_illegal_play_len'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_play_len'] = 0;
            }
            if(!isset($illegal_prv_ad_mod_val['fad_play_len'])){
                $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_play_len'] = 0;
            }
            $prv_all_count['fad_count'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_count'];
            $prv_all_count['fad_illegal_count'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_count'];

            $prv_all_count['fad_illegal_times'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_times'];
            $prv_all_count['fad_times'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_times'];
            $prv_all_count['fad_illegal_play_len'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_illegal_play_len'];
            $prv_all_count['fad_play_len'] += $illegal_prv_ad_mod[$illegal_prv_ad_mod_key]['fad_play_len'];
        }
        //对比上个周期的数据
        foreach ($illegal_prv_ad_mod as $illegal_prv_ad_val) {
            foreach ($data as $data_key=>$data_value) {

                if ($illegal_prv_ad_val[$fztj] == $data_value[$fztj]) {

                    $data[$data_key]['prv_times_illegal_rate'] = $illegal_prv_ad_val['times_illegal_rate'];


                    if (!$illegal_prv_ad_val['fad_times'] || ($data_value['fad_times'] - $illegal_prv_ad_val['fad_times']) == 0) {
                        $data[$data_key]['hb_fad_times'] = '0';
                    } else {

                        $hb_fad_times = $data_value['fad_times'] - $illegal_prv_ad_val['fad_times'];
                        if ($hb_fad_times < 0) {
                            $data[$data_key]['hb_fad_times'] = ($hb_fad_times * -1).'↓';
                        } else {
                            $data[$data_key]['hb_fad_times'] = $hb_fad_times.'↑';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_times'] || ($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) == 0) {
                        $data[$data_key]['hb_fad_illegal_times'] = '0';
                    } else {

                        $hb_fad_illegal_times = $data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times'];
                        if ($hb_fad_illegal_times < 0) {
                            $data[$data_key]['hb_fad_illegal_times'] = ($hb_fad_illegal_times * -1).'↓';
                        } else {
                            $data[$data_key]['hb_fad_illegal_times'] = $hb_fad_illegal_times.'↑';
                        }
                    }

                    if (!$illegal_prv_ad_val['times_illegal_rate'] || ($data_value['times_illegal_rate'] - $illegal_prv_ad_val['times_illegal_rate']) == 0) {
                        $data[$data_key]['hb_fad_illegal_times_rate'] = '0';
                    } else {

                        $hb_fad_illegal_times_rate = $data_value['times_illegal_rate'] - $illegal_prv_ad_val['times_illegal_rate'];
                        if ($hb_fad_illegal_times_rate < 0) {
                            $data[$data_key]['hb_fad_illegal_times_rate'] = ($hb_fad_illegal_times_rate * -1).'↓';
                        } else {
                            $data[$data_key]['hb_fad_illegal_times_rate'] = $hb_fad_illegal_times_rate.'↑';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_count'] || ($data_value['fad_illegal_count'] - $illegal_prv_ad_val['fad_illegal_count']) == 0) {
                        $data[$data_key]['prv_fad_illegal_count'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_count = round(($data_value['fad_illegal_count'] - $illegal_prv_ad_val['fad_illegal_count']) / $illegal_prv_ad_val['fad_illegal_count'], 4) * 100;
                        if ($prv_fad_illegal_count < 0) {
                            $data[$data_key]['prv_fad_illegal_count'] = '↓' . '(' . $prv_fad_illegal_count . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_count'] = '↑' . '(' . $prv_fad_illegal_count . '%)';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_times'] || ($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) == 0) {
                        $data[$data_key]['prv_fad_illegal_times'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_times = round(($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) / $illegal_prv_ad_val['fad_illegal_times'], 4) * 100;
                        if ($prv_fad_illegal_times < 0) {
                            $data[$data_key]['prv_fad_illegal_times'] = '↓' . '(' . $prv_fad_illegal_times . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_times'] = '↑' . '(' . $prv_fad_illegal_times . '%)';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_play_len'] || ($data_value['fad_illegal_play_len'] - $illegal_prv_ad_val['fad_illegal_play_len']) == 0) {
                        $data[$data_key]['prv_fad_illegal_play_len'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_play_len = round(($data_value['fad_illegal_play_len'] - $illegal_prv_ad_val['fad_illegal_play_len']) / $illegal_prv_ad_val['fad_illegal_play_len'], 4) * 100;

                        if ($prv_fad_illegal_play_len < 0) {
                            $data[$data_key]['prv_fad_illegal_play_len'] = '↓' . '(' . $prv_fad_illegal_play_len . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_play_len'] = '↑' . '(' . $prv_fad_illegal_play_len . '%)';
                        }
                    }
                }
            }
        }

        $array_num = 0;//定义限定数量
        $data_pm = [];//定义图表数据空数组
        $data_count = count($data);//获取数据条数

        $cl_count_all = 0;//线索全部数量
        $ck_count_all = 0;//线索全部数量
        $count_all = 0;//总数
        $times=0;

        //处理数据
        foreach ($data as $data_key=>$data_value){
            $data[$data_key]['counts_illegal_rate'] = round($data_value['counts_illegal_rate'],4)*100;
            $data[$data_key]['times_illegal_rate'] = round($data_value['times_illegal_rate'],4)*100;
            $data[$data_key]['lens_illegal_rate'] = round($data_value['lens_illegal_rate'],4)*100;
            if($fztj == 'fregionid' && $onther_class == 'compre_score'){
                $data[$data_key]['compre_score'] = round(((round($data_value['counts_illegal_rate'],4) + round($data_value['times_illegal_rate'],4))/2*50+round($data_value['lens_illegal_rate'],4)*50),2);
            }
            //循环获取前35条数据
            if($array_num < 35 && $array_num < count($data) && $data_value[$onther_class] > 0){
                $data_pm['name'][] = $data_value[$join_table.'_name'];
                //不同排名条件显示各自的数值
                switch ($onther_class){
                    case 'fregionid':
                        $data_pm['num'][] = $data[$data_key][$onther_class_val];
                        break;
                    case 'fad_illegal_times':
                        $data_pm['num'][] = $data_value['fad_illegal_times'];//违法条次
                        break;
                    case 'times_illegal_rate':
                        $data_pm['num'][] = round($data_value['times_illegal_rate'],4)*100;//条次违法率
                        break;
                    case 'fad_times':
                        $data_pm['num'][] = $data[$data_key]['fad_times'];//条次
                        break;
                    case 'fad_illegal_count':
                        $data_pm['num'][] = $data[$data_key]['fad_illegal_count'];//条数
                        break;
                    case 'counts_illegal_rate':
                        $data_pm['num'][] = $data[$data_key]['counts_illegal_rate'];//条数违法率
                        break;
                    case 'fad_count':
                        $data_pm['num'][] = $data[$data_key]['fad_count'];//条数
                        break;
                    case 'fad_play_len':
                        $data_pm['num'][] = $data[$data_key]['fad_play_len'];//时长
                        break;
                    case 'fad_illegal_play_len':
                        $data_pm['num'][] = $data_value['fad_illegal_play_len'];//违法时长
                        break;
                    case 'lens_illegal_rate':
                        $data_pm['num'][] = round($data_value['lens_illegal_rate'],4)*100;//时长违法率
                        break;
                }
                $array_num++;//fad_count  广告条次：fad_times  广告时长：fad_play_len
            }
        }

        //如果是地域排名 按行政区划排名
        if($fztj == 'fregionid' && $region_order == 1){

            $data = $this->pxsf($data,$onther_class,false);//顺序排序

        }else{

            $data = $this->pxsf($data,$onther_class);//倒叙排序

        }

        /*线索统计模块*/
        //线索查看处理情况tbn_illegal_ad
        //已处理量
        $tbn_illegal_ad_mod = M('tbn_illegal_ad');
        if(!empty($isrelease) || $system_num == '100000'){
            $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
        }

        if(!empty($dates)){
            $illegal_map['tbn_illegal_ad_issue.fissue_date'] = [['IN',$dates],['BETWEEN',[$s_time,$e_time]],'AND'];
        }else{
            $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_time,$e_time]];
        }
        $ycl_count = $tbn_illegal_ad_mod
            ->cache(true,600)
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as cll,
                $xs_group_field
           ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->where($illegal_map)
            ->where(['tbn_illegal_ad.fstatus'=>['gt',0]])
            ->group($xs_group)
            ->select();
        //全部数量
        $cl_count = $tbn_illegal_ad_mod
            //->cache(true,600)
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as clall,
                $xs_group_field
            ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->where($illegal_map)
            ->group($xs_group)
            ->select();
        $xs_data = [];//定义线索统计情况相关数据

        //如果没有被处理的数据
        if(empty($ycl_count)){
            foreach ($cl_count as $cl_val){
                $xs_data[$cl_val[$hb_field]]['cll'] = '0%';
                $xs_data[$cl_val[$hb_field]][$hb_field] = $cl_val[$hb_field];
                $xs_data[$cl_val[$hb_field]]['cl_count'] = 0;
                $xs_data[$cl_val[$hb_field]]['all_count'] = $cl_val['clall'];
            }
        }else{
            //循环获取
            foreach ($ycl_count as $ycl_val){
                foreach ($cl_count as $cl_val){
                    if($ycl_val[$hb_field] == $cl_val[$hb_field]){
                        if($ycl_val['cll'] != 0){
                            $xs_data[$cl_val[$hb_field]][$hb_field] = $cl_val[$hb_field];
                            $xs_data[$cl_val[$hb_field]]['cll'] = (round($ycl_val['cll']/$cl_val['clall'],4)*100).'%';
                            $xs_data[$cl_val[$hb_field]]['cl_count'] = $ycl_val['cll'];
                        }else{
                            $xs_data[$cl_val[$hb_field]]['cll'] = '0%';
                            $xs_data[$cl_val[$hb_field]][$hb_field] = $cl_val[$hb_field];
                            $xs_data[$cl_val[$hb_field]]['cl_count'] = 0;
                        }
                        $xs_data[$cl_val[$hb_field]]['all_count'] = $cl_val['clall'];
                    }
                }
            }
        }
        //已查看量
        $yck_count = $tbn_illegal_ad_mod
            ->cache(true,600)
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as ckl,
                $xs_group_field
            ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->where($illegal_map)
            ->where(['tbn_illegal_ad.fview_status'=>['gt',0]])
            ->group($xs_group)
            ->select();

        //全部数量
        $ck_count = $tbn_illegal_ad_mod
            ->cache(true,600)
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as ckall,
                $xs_group_field
            ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id and tmedia.fid = tmedia.main_media_id"
            )
            ->where($illegal_map)
            ->group($xs_group)
            ->select();

        //如果没有一查看的数量
        if(empty($yck_count)){
            foreach ($ck_count as $ck_val){
                $xs_data[$ck_val[$hb_field]]['ckl'] = '0%';
                $xs_data[$ck_val[$hb_field]]['ck_count'] = 0;
                $xs_data[$ck_val[$hb_field]]['all_count'] = $ck_val['ckall'];
            }
        }else{
            //循环获取
            foreach ($yck_count as $yck_val){
                foreach ($ck_count as $ck_val){
                    if($yck_val[$hb_field] == $ck_val[$hb_field]){
                        if($yck_val['ckl'] != 0){
                            $xs_data[$ck_val[$hb_field]]['ck_count'] = $yck_val['ckl'];
                        }else{
                            $xs_data[$ck_val[$hb_field]]['ck_count'] = 0;
                        }
                        $xs_data[$ck_val[$hb_field]]['ckl'] = (round($yck_val['ckl']/$ck_val['ckall'],4)*100).'%';
                    }
                    $xs_data[$ck_val[$hb_field]]['all_count'] = $ck_val['ckall'];
                }
            }//组建案件查看与处理情况
        }


        //组合线索相关数据
        foreach ($xs_data as $xs_key=>$xs_value){
            foreach ($data as $data_key=>$data_value){
                if($xs_key == $data_value[$fztj]){
                    unset($xs_value[$hb_field]);
                    $cl_count_all += $xs_value['cl_count'];
                    $ck_count_all += $xs_value['ck_count'];
                    $count_all += $xs_value['all_count'];
                    $data[$data_key] = array_merge($xs_value, $data[$data_key]);//合并数组
                }
            }
        }

        //组合广告类别百分比图表数据
        $ad_class_num = 0;
        if($fztj == 'fad_class_code') {
            $data_pm = [];
            $data = $this->pxsf($data, $onther_class);//排序
            foreach ($data as $ad_class_key=>$ad_class_value){
                if($ad_class_value[$onther_class] != 0){
                    $data_pm['data'][] = [
                        'name' => $ad_class_value[$join_table.'_name'],
                        'value' => $ad_class_value[$onther_class]
                    ];
                    $data_pm['name'][] = $ad_class_value[$join_table.'_name'];
                }
            }
        }

        $all_prv_fad_illegal_count = round(($all_count['fad_illegal_count'] - $prv_all_count['fad_illegal_count']) / $prv_all_count['fad_illegal_count'], 4) * 100;
        $all_prv_fad_illegal_play_len = round(($all_count['fad_illegal_play_len'] - $prv_all_count['fad_illegal_play_len']) / $prv_all_count['fad_illegal_play_len'], 4) * 100;
        $all_prv_fad_illegal_times = round(($all_count['fad_illegal_times'] - $prv_all_count['fad_illegal_times']) / $prv_all_count['fad_illegal_times'], 4) * 100;

        $counts_illegal_rate = round($all_count['fad_illegal_count']/$all_count['fad_count'],4)*100;
        $times_illegal_rate = round($all_count['fad_illegal_times']/$all_count['fad_times'],4)*100;
        $lens_illegal_rate = round($all_count['fad_illegal_play_len']/$all_count['fad_play_len'],4)*100;

        $prv_counts_illegal_rate = round($prv_all_count['fad_illegal_count']/$prv_all_count['fad_count'],4)*100;
        $prv_times_illegal_rate = round($prv_all_count['fad_illegal_times']/$prv_all_count['fad_times'],4)*100;
        $prv_lens_illegal_rate = round($prv_all_count['fad_illegal_play_len']/$prv_all_count['fad_play_len'],4)*100;

        if ($all_prv_fad_illegal_count < 0) {
            $all_prv_fad_illegal_count = '↓' . '(' . $all_prv_fad_illegal_count . '%)';
        } else {
            if($all_prv_fad_illegal_count == 0){
                $all_prv_fad_illegal_count = '(0.00%)';

            }else{
                $all_prv_fad_illegal_count = '↑' . '(' . $all_prv_fad_illegal_count . '%)';
            }
        }

        if ($all_prv_fad_illegal_play_len < 0) {
            $all_prv_fad_illegal_play_len = '↓' . '(' . $all_prv_fad_illegal_play_len . '%)';
        } else {
            if($all_prv_fad_illegal_play_len == 0){
                $all_prv_fad_illegal_play_len = '(0.00%)';

            }else{
                $all_prv_fad_illegal_play_len = '↑' . '(' . $all_prv_fad_illegal_play_len . '%)';
            }
        }

        if ($all_prv_fad_illegal_times < 0) {
            $all_prv_fad_illegal_times = '↓' . '(' . $all_prv_fad_illegal_times . '%)';
        } else {
            if($all_prv_fad_illegal_times == 0){
                $all_prv_fad_illegal_times = '(0.00%)';

            }else{
                $all_prv_fad_illegal_times = '↑' . '(' . $all_prv_fad_illegal_times . '%)';
            }
        }

        $compar_fad_count = $all_count['fad_count'] - $prv_all_count['fad_count'];
        if ($compar_fad_count < 0) {
            $compar_fad_count = '减少'.($compar_fad_count*-1);
        } else {
            if($compar_fad_count == 0){
                $compar_fad_count = '无变化';

            }else{
                $compar_fad_count = '增加'.$compar_fad_count;
            }
        }
        $compar_fad_illegal_count = $all_count['fad_illegal_count'] - $prv_all_count['fad_illegal_count'];
        if ($compar_fad_illegal_count < 0) {
            $compar_fad_illegal_count = '减少'.($compar_fad_illegal_count*-1);
        } else {
            if($compar_fad_illegal_count == 0){
                $compar_fad_illegal_count = '无变化';

            }else{
                $compar_fad_illegal_count = '增加'.$compar_fad_illegal_count;
            }
        }
        $compar_illegal_count_rate = $counts_illegal_rate - $prv_counts_illegal_rate;
        if ($compar_illegal_count_rate < 0) {
            $compar_illegal_count_rate = '下降'.($compar_illegal_count_rate*-1).'%';
        } else {
            if($compar_illegal_count_rate == 0){
                $compar_illegal_count_rate = '无变化';

            }else{
                $compar_illegal_count_rate = '上升'.$compar_illegal_count_rate.'%';
            }
        }
        $compar_fad_times = $all_count['fad_times'] - $prv_all_count['fad_times'];
        if ($compar_fad_times < 0) {
            $compar_fad_times = '减少'.($compar_fad_times*-1);
        } else {
            if($compar_fad_times == 0){
                $compar_fad_times = '无变化';

            }else{
                $compar_fad_times = '增加'.$compar_fad_times;
            }
        }
        $compar_fad_illegal_times = $all_count['fad_illegal_times'] - $prv_all_count['fad_illegal_times'];
        if ($compar_fad_illegal_times < 0) {
            $compar_fad_illegal_times = '减少'.($compar_fad_illegal_times*-1);
        } else {
            if($compar_fad_illegal_times == 0){
                $compar_fad_illegal_times = '无变化';

            }else{
                $compar_fad_illegal_times = '增加'.$compar_fad_illegal_times;
            }
        }
        $compar_illegal_times_rate = $times_illegal_rate - $prv_times_illegal_rate;
        if ($compar_illegal_times_rate < 0) {
            $compar_illegal_times_rate = '下降'.($compar_illegal_times_rate*-1).'%';
        } else {
            if($compar_illegal_times_rate == 0){
                $compar_illegal_times_rate = '无变化';
            }else{
                $compar_illegal_times_rate = '上升'.$compar_illegal_times_rate.'%';
            }
        }
        $compar_fad_play_len = $all_count['fad_play_len'] - $prv_all_count['fad_play_len'];
        if ($compar_fad_play_len < 0) {
            $compar_fad_play_len = '减少'.($compar_fad_play_len*-1);
        } else {
            if($compar_fad_play_len == 0){
                $compar_fad_play_len = '无变化';

            }else{
                $compar_fad_play_len = '增加'.$compar_fad_play_len;
            }
        }
        $compar_fad_illegal_play_len = $all_count['fad_illegal_play_len'] - $prv_all_count['fad_illegal_play_len'];
        if ($compar_fad_illegal_play_len < 0) {
            $compar_fad_illegal_play_len = '减少'.($compar_fad_illegal_play_len*-1);
        } else {
            if($compar_fad_illegal_play_len == 0){
                $compar_fad_illegal_play_len = '无变化';

            }else{
                $compar_fad_illegal_play_len = '增加'.$compar_fad_illegal_play_len;
            }
        }
        $compar_illegal_play_len_rate = $lens_illegal_rate - $prv_lens_illegal_rate;
        if ($compar_illegal_play_len_rate < 0) {
            $compar_illegal_play_len_rate = '下降'.($compar_illegal_play_len_rate*-1).'%';
        } else {
            if($compar_illegal_play_len_rate == 0){
                $compar_illegal_play_len_rate = '无变化';
            }else{
                $compar_illegal_play_len_rate = '上升'.$compar_illegal_play_len_rate.'%';
            }
        }
        $compar_prv_com_score = round($all_count['com_score'] - $prv_all_count['prv_com_score'],2);
        if ($compar_prv_com_score < 0) {
            $compar_prv_com_score = '减少'.($compar_prv_com_score*-1);
        } else {
            if($compar_prv_com_score == 0){
                $compar_prv_com_score = '无变化';

            }else{
                $compar_prv_com_score = '增加'.$compar_prv_com_score;
            }
        }

        //统计放在最后一条$all_count     $prv_all_count
            $data[] = [
                'all_xs_count' =>$count_all,
                'titlename'=>'合计',
                "cll"=>(round($cl_count_all/$count_all,4)*100).'%',
                "cl_count" =>$cl_count_all,
                "ck_count"=>$ck_count_all,
                "ckl"=>(round($ck_count_all/$count_all,4)*100).'%',
                "prv_fad_illegal_count"=>$all_prv_fad_illegal_count,
                "prv_fad_illegal_play_len"=>$all_prv_fad_illegal_play_len,
                "prv_fad_illegal_times"=>$all_prv_fad_illegal_times,

                'fad_count'=>$all_count['fad_count'],//广告条数
                'fad_illegal_count'=>$all_count['fad_illegal_count'],//违法广告数量
                'counts_illegal_rate'=>round($all_count['fad_illegal_count']/$all_count['fad_count'],4)*100,
                'fad_times'=>$all_count['fad_times'],//广告条次
                'fad_illegal_times'=>$all_count['fad_illegal_times'],//违法广告条次
                'times_illegal_rate'=>round($all_count['fad_illegal_times']/$all_count['fad_times'],4)*100,
                'fad_play_len'=>$all_count['fad_play_len'],//总时长
                'fad_illegal_play_len'=>$all_count['fad_illegal_play_len'],//违法时长
                'lens_illegal_rate'=>round($all_count['fad_illegal_play_len']/$all_count['fad_play_len'],4)*100,
                "com_score"=>round($all_count['com_score'],2),//综合分

                "compar_fad_count"=>$compar_fad_count,//对比上个周期广告条数
                "compar_fad_illegal_count"=>$compar_fad_illegal_count,//对比上个周期违法广告数量
                "compar_illegal_count_rate"=>$compar_illegal_count_rate,//对比上个周期条数违法率
                "compar_fad_times"=>$compar_fad_times,//对比上个周期广告条次
                "compar_fad_illegal_times"=>$compar_fad_illegal_times,//对比上个周期违法广告条次
                "compar_illegal_times_rate"=>$compar_illegal_times_rate,//对比上个周期条次违法率
                "compar_fad_play_len"=>$compar_fad_play_len,//对比上个周期总时长
                "compar_fad_illegal_play_len"=>$compar_fad_illegal_play_len,//对比上个周期违法时长
                "compar_illegal_play_len_rate"=>$compar_illegal_play_len_rate,//对比上个周期时长违法率
                "compar_prv_com_score"=>$compar_prv_com_score,//对比上个周期综合分
            ];

/*        $finally_data = [
            'count'=> count($data)-1,
            'data_pm'=>$data_pm,//图表数据
            'data_list'=>$data//列表数据
        ];*/
        return $data;

    }

    /**
     * @param $media_ids 指定媒体
     * @param $s_date 开始日期
     * @param $e_date 结束日期
     * @param $region 行政区划
     * @param string $media_class  媒介类型
     * @param array $ad_class 广告类别
     * @param int $is_have_self 是否包含本级
     * @param int $is_show_longad 是否包含长广告
     * @param int $is_show_fsend 是否确认数据
     * @param string $group_field 分组条件
     * @param string $order_field 排序条件
     * @return array
     */
    public function report_illegal_ad_list($media_ids,$s_date,$e_date,$region,$media_class='',$is_have_self=1,$ad_class=[],$is_show_longad=1,$is_show_fsend=1,$group_field='tbn_illegal_ad.fad_name,tbn_illegal_ad.fmedia_id',$order_field='fad_ill_times'){
        if(empty($media_ids)){
            return [];
        }
        $system_num = getconfig('system_num');
        
        //为空，或者指定类别
        if(!empty($ad_class)){
            $map['left(tbn_illegal_ad.fad_class_code,2)'] = ['IN',$ad_class];//广告类别
        }
        //是否包含长广告
        if($is_show_longad != 1){
            $map['_string'] = '(unix_timestamp(tbn_illegal_ad_issue.fendtime) - unix_timestamp(tbn_illegal_ad_issue.fstarttime) < 600 OR tbn_illegal_ad.fmedia_class > 2)';
        }

        //审核通过的违法数据
        $isexamine = getconfig('isexamine');
        if(in_array($isexamine, [10,20,30,40])){
            $map['tbn_illegal_ad.fexamine'] = 10;
        }

        //媒体类型
        if($media_class != ''){
            $map['tbn_illegal_ad.fmedia_class'] = $media_class;
        }
        //违法发布数据确认条件
        if($is_show_fsend == 1){
            $map['tbn_illegal_ad_issue.fsend_status'] = ['IN',[0,1,2]];//无需确认
        }elseif($is_show_fsend == 2){
            $map['tbn_illegal_ad_issue.fsend_status'] = ['IN',[1,2]];//dmp确认
        }else{
            $map['tbn_illegal_ad_issue.fsend_status'] = 2;//国家局确认
        }

        $user = session('regulatorpersonInfo');//获取用户信息

        $ischeck = getconfig('ischeck');
        $map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_date,$e_date]];//发布日期

        //是否需要抽查
        if(!empty($ischeck)){
            $spot_check_data = M('spot_check')->where(['fcustomer'=>$user['system_num']])->select();//查询抽查表数据
            //如果抽查表有数据
            if(!empty($spot_check_data)){
                $dates = [];//定义日期数组
                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                    $condition = trim($spot_check_data_val['condition']);
                    if(!empty($condition)){
                        $year_month = '';
                        $date_str = [];
                        $year_month = substr($spot_check_data_val['fmonth'],0,7);
                        if(!empty($spot_check_data_val['condition'])){
                            $date_str = explode(',',$spot_check_data_val['condition']);
                        }
                        foreach ($date_str as $date_str_val){

                            $dates[] = $year_month.'-'.$date_str_val;
                        }
                    }
                }

                $map['tbn_illegal_ad_issue.fissue_date'] = [['IN',$dates],['BETWEEN',[$s_date,$e_date]],'AND'];
            }else{
                $map['_string'] = '1 = 0';
            }
        }

        $region_id = session('regulatorpersonInfo.regionid');
        $map['tbn_illegal_ad.fstatus3'] = ['NOT IN',[30,40]];
        $re_level = $this->judgment_region($region_id);//当前用户行政等级
        switch ($region){
            case 0:
            case 1://全国各省违法线索情况
                if($re_level == 0){
                    $map["tregion.flevel"] = 1;
                }elseif($re_level == 1){
                    $map['tregion.fid'] = $region_id;
                }
                break;
            case 2://副省级
                if($re_level == 0){
                    //全国各副省级市违法线索情况
                    $map["tregion.flevel"] = 2;
                }elseif($re_level == 1){
                    //本省各副省级市违法线索情况
                    if($is_have_self == 1){//如果包含本级
                        $map["tregion.flevel"] = ['IN','1,2'];
                        $map['tregion.fpid|tregion.fid'] = $region_id;
                    }else{//不包含本级
                        $map["tregion.flevel"] = 2;
                        $illegal_map["tregion.flevel"] = 2;
                        $map["tregion.fpid"] = $region_id;
                    }
                }elseif($re_level == 2){//如果是当前用户是副省级只显示自己
                    $map["tregion.fpid"] = $region_id;
                }
                break;
            case 3://各计划单列市
                if($re_level == 0){//如果当前用户是国家局
                    //全国各计划单列市违法线索情况
                    $map["tregion.flevel"] = 3;
                }elseif($re_level == 1){//如果对当前用户是省局
                    //本省各计划单列市违法线索情况
                    if($is_have_self == 1){
                        $map["tregion.flevel"] = ['IN','1,3'];
                        $illegal_map["tregion.flevel"] = ['IN','1,3'];
                        $map['tregion.fpid|tregion.fid'] = $region_id;
                    }else{
                        $map["tregion.flevel"] = 3;
                        $illegal_map["tregion.flevel"] = 3;
                        $map["tregion.fpid"] = $region_id;
                    }

                }elseif($re_level == 3){
                    $map["tregion.fpid"] = $region_id;
                }
                break;
            case 4://各市
                if($re_level == 0){
                    //如果是全国各市违法线索情况
                    $map["tregion.flevel"] = 4;
                }elseif($re_level == 1){
                    //本省各市违法线索情况
                    if($is_have_self == 1){//此处包含了副省级、计划单列市、市级
                        $map['tregion.fpid|tregion.fid'] = $region_id;
                    }else{
                        //此处包含了副省级、计划单列市、市级
                        $map["tregion.fpid"] = $region_id;
                    }
                }elseif($re_level == 4 || $re_level == 3 ||$re_level == 2){//如果当前用户为副省级、计划单列市、市级，只显示自己
                    $map["tregion.fid"] = $region_id;
                }
                break;
            case 5://各区县
                if($re_level == 0){
                    //全国各区县违法线索情况
                    $map["tregion.flevel"] = 5;
                }elseif($re_level == 1){
                    //本省各区县违法线索情况
                    $fpid = substr($region_id,0,2);
                    $map["tregion.fpid"] = ['like',$fpid.'%'];
                    $map["tregion.flevel"] = 5;
                }elseif($re_level == 4 || $re_level == 2 || $re_level == 3){
                    //本市各区县违法线索情况
                    if($is_have_self == 1){
                        $map['tregion.fpid|tregion.fid'] = $region_id;
                    }else{
                        $fpid = substr($region_id,0,4);
                        $map["tregion.fpid"] = ['like',$fpid.'%'];
                        $map["tregion.flevel"] = 5;
                    }
                }elseif($re_level == 5){
                    $map["tregion.fid"] = $region_id;
                    $map["tregion.flevel"] = 5;
                }
                break;
        }
        if($is_have_self == 2){
            $map["tregion.fid"] = $region_id;
        }
        $map['tbn_illegal_ad_issue.fmedia_id'] = ['IN',$media_ids];//权限媒体
        $map['tbn_illegal_ad.fcustomer'] = $system_num;//客户id
        //查询违法明细
        $res = M('tbn_illegal_ad_issue')
            ->field("
               tbn_illegal_ad.fsample_id,
               tregion.fname1,
               tmedia.fmedianame,
               tbn_illegal_ad.fexpressions,
               tbn_illegal_ad.fillegal_code,
               case when tbn_illegal_ad.fmedia_class = 1 then '电视'
               when tbn_illegal_ad.fmedia_class = 2 then '广播'
               when tbn_illegal_ad.fmedia_class = 3 then '报纸'
               when tbn_illegal_ad.fmedia_class = 13 then '互联网'
               end as fmedia_class,
               tadclass.ffullname,
               tbn_illegal_ad.fad_name,
               count(1) as fad_ill_times,
               tbn_illegal_ad.fillegal,
               IF(tbn_illegal_ad.fview_status > 0,'已查看','未查看') as fview_status,
               IF(tbn_illegal_ad.fstatus > 0,'已处理','未处理') as fcl_status
            ")
            ->join("tbn_illegal_ad  ON tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id")
            ->join("tadclass on tadclass.fcode = tbn_illegal_ad.fad_class_code")
            ->join("tmedia ON tmedia.fid = tbn_illegal_ad_issue.fmedia_id")
            ->join("tregion ON tregion.fid = tbn_illegal_ad.fregion_id")
            ->where($map)
            ->group($group_field)
            ->order($order_field." desc" )
            ->select();

        return $res;
    }


    //样本相关违法详情（目前仅互联网适用）
    public function report_illegal_detail($fsample_id,$media_class='13'){
        $data = M("tnetissue")
            ->field("
            net_target_url,
            net_original_url,
            fillegalcontent,
            thumb_url_true,
            net_snapshot
            ")
            ->where(['major_key'=>$fsample_id])
            ->find();
        return $data;
    }


    public function net_media_type_ids($type){
        $system_num = getconfig('system_num');
        $ids = M('tmedia')
            ->join("tmedia_temp ON tmedia.fid = tmedia_temp.fmediaid AND tmedia.fid = tmedia.main_media_id")
            ->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')
            ->where([
                'tmedia_temp.ftype'=>1,
                'tmedia_temp.fcustomer'=>$system_num,
                'tmedia_temp.fuserid'=>session('regulatorpersonInfo.fid'),
                'tmedia.fmediaclassid' => ['like',$type.'%']
            ])
            ->getField('tmedia.fid',true);
        return $ids;
    }


    /**by @jim 20180703 当前区域下互联网媒体数量
     * @param $region_id 行政区域ID
     * @param $change_field 来源类别查询条件
     * @param $group_field 分组字段
     * @param $s_time 查询开始时间
     * @param $e_time 查询结束时间
     * @return mixed
     */
    public function net_media_customize($owner_id,$change_field = false,$group_field,$s_time,$e_time,$fmediaid = false,$is_inquire_subordinate=true){
        $system_num = getconfig('system_num');//获取国家局标记

        if(is_array($owner_id)){
            if(!empty($owner_id)){
                $where_str = 'tmedia.fid IN ('.join(',',$owner_id).')';
            }
        }else{

            if($is_inquire_subordinate){
                $owner_id = rtrim($owner_id,'0');
            }
            $where_str = "tmediaowner.fregionid LIKE '".$owner_id."%'";

        }

        if(is_array($change_field)){
            $where = 'AND '.$change_field[0].' = '.$change_field[1];
        }else{
            $where = '';
        }
        if($fmediaid !== false){
            $where .= ' AND tn.fmediaid = '.$fmediaid;
        }
        if($group_field == 'tn.fmediaid'){
            $name = 'tmedia.fmedianame';
        }elseif($group_field == 'ac.fcode'){
            $name = 'ac.ffullname';
        }
        $sql = 'SELECT
                '.$name.' AS dymc,
                count(*) total,
                ifnull(
                    sum(
                        CASE
                        WHEN tns.fillegaltypecode = 30 THEN
                            1
                        ELSE
                            0
                        END
                    ),
                    0
                ) AS yzwfggsl,
                ifnull(
                    round(
                        (
                            sum(
                                CASE
                                WHEN tns.fillegaltypecode = 30 THEN
                                    1
                                ELSE
                                    0
                                END
                            ) / count(*)
                        ) * 100,
                        2
                    ),
                    0
                ) AS ggslyzwfl,
                ifnull(
                    sum(
                        CASE
                        WHEN tns.fillegaltypecode > 0 THEN
                            1
                        ELSE
                            0
                        END
                    ),
                    0
                ) AS wfggsl,
                ifnull(
                    round(
                        (
                            sum(
                                CASE
                                WHEN tns.fillegaltypecode > 0 THEN
                                    1
                                ELSE
                                    0
                                END
                            ) / count(*)
                        ) * 100,
                        2
                    ),
                    0
                ) AS ggslwfl,
            count(DISTINCT tn.tid) AS ts,
			count(DISTINCT CASE WHEN tns.fillegaltypecode > 0 THEN tn.tid ELSE NULL END ) AS wfts,
			count(DISTINCT CASE WHEN tns.fillegaltypecode = 30 THEN tn.tid ELSE NULL END ) AS yzwfts,
			ifnull(
                    round((count(DISTINCT CASE WHEN tns.fillegaltypecode > 0 THEN tn.tid ELSE NULL END ) / count(DISTINCT tn.tid))*100,2),
                    0
                ) AS tswfl,
            ifnull(
                    round((count(DISTINCT CASE WHEN tns.fillegaltypecode = 30 THEN tn.tid ELSE NULL END ) / count(DISTINCT tn.tid))*100,2),
                    0
                ) AS tsyzwfl,
                tn.fmediaid,
			    tns.ftype
            FROM
                tnetissueputlog AS tn
            JOIN tnetissue AS tns ON tns.major_key = tn.tid and finputstate=2 
            JOIN tadclass AS ac ON ac.fcode = LEFT (tns.fadclasscode, 2)
            JOIN tmedia ON tmedia.fid = tn.fmediaid
            JOIN tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid
            JOIN tregion ON tregion.fid = tmediaowner.fregionid
            join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid
            WHERE
                '.$where_str.'
            AND (tn.net_created_date / 1000) >= "'.$s_time.'"
            AND tmedia.fid = tmedia.main_media_id
            AND tn.is_first_broadcast = 1
            AND (tn.net_created_date / 1000) < "'.$e_time.'"
            ' .$where. ' 
            GROUP BY
                ' .$group_field. '
            ORDER BY ggslwfl DESC;';

        $net_pc_media_customize = M('')->query($sql);
        return $net_pc_media_customize;
    }

    /**
     * @param $owner_id
     * @param bool $change_field
     * @param $s_time
     * @param $e_time
     * @param bool $fmediaid
     * @param bool $is_inquire_subordinate
     * @return mixed
     */
    public function net_illegal_situation_customize($owner_id,$change_field = false,$s_time,$e_time,$fmediaid = false,$is_inquire_subordinate=true){
        $system_num = getconfig('system_num');//获取国家局标记

        if(is_array($owner_id)){
            if(!empty($owner_id)){
                $where_str = 'tmedia.fid IN ('.join(',',$owner_id).')';
            }
        }else{

            if($is_inquire_subordinate){
                $owner_id = rtrim($owner_id,'0');
            }
            $where_str = "tmediaowner.fregionid LIKE '".$owner_id."%'";

        }

        if(is_array($change_field)){
            $where = 'AND tns.'.$change_field[0].' = '.$change_field[1];
        }else{
            $where = '';
        }
        if($fmediaid !== false){
            $where .= ' AND tn.fmediaid = '.$fmediaid;
        }

        $sql = 'SELECT
                    CASE WHEN tns.fexpressions <> "" THEN 
                    tns.fexpressions
                    ELSE "违法情形未标明" END as fillegalcontent,
                    tns.fillegaltypecode,
                    COUNT(1) as illegal_count,
	                ac.ffullname as fadclass
                FROM
                    tnetissueputlog AS tn
                JOIN tnetissue AS tns ON tns.major_key = tn.tid and finputstate=2 
                JOIN tmedia ON tmedia.fid = tn.fmediaid
                JOIN tmedia_temp ON tmedia.fid = tmedia_temp.fmediaid
                JOIN tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid
                JOIN tadclass AS ac ON ac.fcode = LEFT (tns.fadclasscode, 2)
                JOIN tregion ON tregion.fid = tmediaowner.fregionid
                WHERE
                    '.$where_str.'
                AND tn.net_created_date/1000 >= "'.$s_time.'"
                AND tn.net_created_date/1000 < "'.$e_time.'"
                ' .$where. ' 
                AND tns.fillegaltypecode > 0
                AND tmedia.fid = tmedia.main_media_id
                AND tn.is_first_broadcast = 1
                AND tmedia_temp.ftype=1 
                AND tmedia_temp.fcustomer = "'.$system_num.'"
                AND tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').'
                GROUP BY
                    tns.fexpressions,LEFT (tns.fadclasscode, 2)
                ORDER BY illegal_count desc;';
        $net_illegal_situation_customize = M('')->query($sql);
        return $net_illegal_situation_customize;
    }

    public function net_illegal_situation_customize_OS($owner_id,$change_field = false,$s_time,$e_time,$fmediaid = false,$is_inquire_subordinate=true){
        $system_num = getconfig('system_num');//获取国家局标记

        if(is_array($owner_id)){
            if(!empty($owner_id)){
                $where_str = 'tmedia.fid IN ('.join(',',$owner_id).')';
            }
        }else{

            if($is_inquire_subordinate){
                $owner_id = rtrim($owner_id,'0');
            }
            $where_str = "tmediaowner.fregionid LIKE '".$owner_id."%'";

        }

        if(is_array($change_field)){
            $where = 'AND tns.'.$change_field[0].' = '.$change_field[1];
        }else{
            $where = '';
        }
        if($fmediaid !== false){
            $where .= ' AND tn.fmediaid = '.$fmediaid;
        }

        $sql = 'SELECT
                    CASE WHEN ilad.fexpressions <> "" THEN 
                    ilad.fexpressions
                    ELSE "违法情形未标明" END as fillegalcontent,
                    tns.fillegaltypecode,
                    COUNT(DISTINCT ilad.fid) as illegal_count,
	                ac.ffullname as fadclass
                FROM
                    tnetissueputlog AS tn
                JOIN tnetissue AS tns ON tns.major_key = tn.tid and finputstate=2 
                JOIN tmedia ON tmedia.fid = tn.fmediaid
                JOIN tmedia_temp ON tmedia.fid = tmedia_temp.fmediaid
                JOIN tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid
                JOIN tadclass AS ac ON ac.fcode = LEFT (tns.fadclasscode, 2)
                JOIN tregion ON tregion.fid = tmediaowner.fregionid
                JOIN tbn_illegal_ad as ilad ON ilad.fsample_id = tns.major_key
                WHERE
                    '.$where_str.'
                AND tn.net_created_date/1000 >= "'.$s_time.'"
                AND tn.net_created_date/1000 < "'.$e_time.'"
                ' .$where. ' 
                AND tmedia.fid = tmedia.main_media_id
                AND tmedia_temp.ftype=1 
                AND tmedia_temp.fcustomer = "'.$system_num.'"
                AND tmedia_temp.fuserid='.session('regulatorpersonInfo.fid').'
                GROUP BY
                    tns.fexpressions,LEFT (tns.fadclasscode, 2)
                ORDER BY illegal_count desc;';

        dump($sql);exit;

        $net_illegal_situation_customize = M('')->query($sql);
        return $net_illegal_situation_customize;
    }

    public function net_illegal_situation_list($s_time,$e_time,$media_type='13',$meidas = []){
        $system_num = getconfig('system_num');
        if(!empty($meidas)){
            if(is_array($meidas_map)){
                $meidas_map = ' AND tmedia.fid IN ('.join(',',$meidas).') ';
            }else{
                $meidas_map = " AND tmedia.fid = $meidas ";
            }
        }else{
            $meidas_map = '';
        }
        $sql = "SELECT
                tnetissue.fexpressions as fillegalcontent,
                tadclass.ffullname as fadclass,
                COUNT(1) as illegal_count
                FROM
                  `tbn_illegal_ad_issue`
                INNER JOIN tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid
                INNER JOIN tnetissue ON tnetissue.major_key = tbn_illegal_ad.fsample_id
                INNER JOIN tregion ON tregion.fid = tbn_illegal_ad.fregion_id
                INNER JOIN tmedia ON tmedia.fid = tbn_illegal_ad.fmedia_id
                AND tmedia.fid = tmedia.main_media_id
                JOIN tadclass ON tadclass.fcode = tbn_illegal_ad.fad_class_code
                JOIN tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid
                WHERE
                tmedia_temp.ftype = 1
                ".$meidas_map."
                AND tmedia_temp.fuserid = ".session('regulatorpersonInfo.fid')."
                AND tbn_illegal_ad.fexamine = 10
                AND tbn_illegal_ad.fcustomer = ".$system_num."
                AND tbn_illegal_ad.fmedia_class = '".$media_type."%'
                AND tbn_illegal_ad_issue.fissue_date BETWEEN '".$s_time."' AND '".$e_time."'
                AND tnetissue.fexpressions is not NULL
                GROUP BY tnetissue.fexpressions ORDER BY illegal_count DESC;";
        $net_illegal_situation_customize = M('')->query($sql);
        return $net_illegal_situation_customize;
    }

    /**@针对省级以内 最大时间跨度为月的 的广告统计
     * @param $s_time
     * @param $e_time
     * @param $region_id
     * @param $level
     * @param $media_type
     * @param $ad_type
     * @param $ranking_condition
     * @param $tab_name
     * @param $ad_pm_type
     * @return mixed
     */
    public function ad_monitor_customize($date_table,$s_time,$e_time,$medias,$media_type,$ad_type,$ranking_condition,$tab_name,$ad_class_xl = false,$is_inquire_subordinate = 1,$check_datas = [],$isowner_sample = false){


        $regionid = session('regulatorpersonInfo.regionid');
        //是否需要抽查
        if(!$medias){
            return [];
        }

        if($media_type == '01'){
            $table_select = ['01'=>'ttvissue_'.$date_table];//定义以表名为元素的数组，循环查询
        }elseif($media_type == '02'){
            $table_select = ['02'=>'tbcissue_'.$date_table];//定义以表名为元素的数组，循环查询
        }elseif($media_type == '03'){
            if($isowner_sample){
                $table_select = ['03'=>'tpaperissue_'.$regionid];//定义以表名为元素的数组，循环查询
            }else{
                $table_select = ['03'=>'tpaperissue'];//定义以表名为元素的数组，循环查询
            }
        }elseif($media_type == ''){
            $table_select = ['01'=>'ttvissue_'.$date_table,'02'=>'tbcissue_'.$date_table];//定义以表名为元素的数组，循环查询
            if($isowner_sample){
                $table_select['03'] = 'tpaperissue_'.$regionid;//定义以表名为元素的数组，循环查询
            }else{
                $table_select['03'] = 'tpaperissue';//定义以表名为元素的数组，循环查询
            }
        }

        //匹配分组条件，连接不同表
        switch ($tab_name){
            case 'fmediaclass':
                $group_field = 'tmediaclass.fclass';//以媒体类型分组
                $xs_group = 'tbn_illegal_ad.fmedia_class';
                $tb_name = 'tmediaclass_name';
                $xs_group_field = 'tbn_illegal_ad.fmedia_class';
                $fz_field = 'fmedia_class_code';
                $hb_field = 'fmedia_class';
                break;
            case 'fadname':
                $group_field = 'tad.'.$tab_name;//以广告名称分组
                $xs_group = 'fad_name';
                $xs_group_field = 'tbn_illegal_ad.fad_name';
                $xs_group = 'tbn_illegal_ad.fad_name';
                $tb_name = 'fadname';
                $fz_field = 'fadname';
                $hb_field = 'fad_name';
                break;
            case 'fregionid':
                $group_field = 'tmediaowner.'.$tab_name;//以地域分组
                $xs_group_field = 'tbn_illegal_ad.fregion_id';
                $xs_group = 'tbn_illegal_ad.fregion_id';
                $fz_field = 'fregionid';
                $tb_name = 'tregion_name';
                $hb_field = 'fregion_id';
                break;
            case 'fmediaid':
                $group_field = 'tmedia.fid';//以媒体分组
                $xs_group = 'tbn_illegal_ad.fmedia_id';
                $xs_group_field = 'tbn_illegal_ad.fmedia_id';
                $fz_field = 'fid';
                $tb_name = 'tmedia_name';
                $hb_field = 'fmedia_id';
                break;
            case 'fcode':
                $group_field = 'tadclass.'.$tab_name;//以广告类别分组
                $fz_field = 'fcode';
                $xs_group = 'left(tbn_illegal_ad.fad_class_code,2)';
                $xs_group_field = 'left(tbn_illegal_ad.fad_class_code,2) as fad_class_code';
                $tb_name = 'tadclass_name';
                $hb_field = 'fad_class_code';
                break;
        }




        //分组条件匹配tmediaclass_name tadclass_name tregion_name fad_class_name tmedia_name
        /*        if($tab_name == 'fregionid'){
                    $group_field = 'tmediaowner.'.$tab_name;//以地域分组
                    $xs_group = 'fregion_id';
                    $tb_name = 'tregion_name';
                }elseif($tab_name == 'fcode'){
                    $group_field = 'tadclass.'.$tab_name;//以广告类别分组
                    $xs_group = 'fad_class_code';
                    $tb_name = 'tadclass_name';
                }elseif($tab_name == 'fmediaid'){
                    $group_field = 'tmedia.fid';//以媒体分组
                    $xs_group = 'fmedia_id';
                    $tb_name = 'tmedia_name';
                }elseif($tab_name == 'fadname'){
                    $group_field = 'tad.'.$tab_name;//以广告名称分组
                    $xs_group = 'fad_name';
                    $tb_name = 'fadname';
                }elseif($tab_name == 'fmediaclass'){
                    $group_field = 'tmediaclass.fclass';//以媒体类型分组
                    $xs_group = 'fmedia_class';
                    $tb_name = 'tmediaclass_name';
                }*/
        if($ad_class_xl){
            $fadclasscode = "tad.fadclasscode";
        }else{
            $fadclasscode = "left(tad.fadclasscode,2)";
        }

        foreach ($table_select as $key=>$table){

            $join_issue = 'fsampleid';
            $join_sample = 'fid';
            $map = [];
            $dates = [];//定义日期数组
            $illegal_map = [];




            $map['_string'] = 'tmedia.fid = tmedia.main_media_id';

            /*            if(!empty($ischeck)){
                            $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
                            //如果抽查表有数据
                            if(!empty($spot_check_data)){
                                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                                    $condition = trim($spot_check_data_val['condition']);
                                    if(!empty($condition)){
                                        $year_month = '';
                                        $date_str = [];
                                        $year_month = substr($spot_check_data_val['fmonth'],0,7);
                                        $date_str = explode(',',$spot_check_data_val['condition']);
                                        foreach ($date_str as $date_str_val){
                                            $dates[] = $year_month.'-'.$date_str_val;
                                        }
                                    }
                                }
                                if(empty($dates)){
                                    $map['_string'] .= ' AND 1 = 0';
                                }
                            }else{
                                $map['_string'] .= ' AND 1 = 0';
                            }
                        }*/
            if(is_array($medias)){
                if(!empty($medias)){
                    $map['tmedia.fid'] = ['in',$medias];
                    $illegal_map['tbn_illegal_ad.fmedia_id'] = ['in',$medias];

                }else{
                    return [];
                }
            }
            if($regionid !== '320100'){
                switch ($is_inquire_subordinate){
                    case 1:
                        $map['tregion.fpid|tregion.fid'] = $regionid;//包含本级以及下级
                        break;
                    case 2:
                        $map['tregion.fid'] = $regionid;//只看本级
                        break;
                    case 3:
                        $map['tregion.fpid'] = $regionid;//只看下级
                        break;
                }
            }


            if(is_array($ad_type)){
                foreach ($ad_type as $ad_type_val){
                    $map["tadclass.fcode"][] = ['like',$ad_type_val.'%'];
                    $illegal_map['left(tbn_illegal_ad.fad_class_code,2)'][] = ['like',$ad_type_val.'%'];//广告类别

                }
                $map["tadclass.fcode"][] = 'OR';
                $illegal_map['left(tbn_illegal_ad.fad_class_code,2)'][] = 'OR';
            }else{
                if($ad_type != ''){
                    $map["tadclass.fcode"] = ['like',$ad_type.'%'];
                    $illegal_map['left(tbn_illegal_ad.fad_class_code,2)'] = ['like',$ad_type.'%'];
                }
            }
            //浏览权限设置end
            //电视01  广播02  报纸03
            if($key == '01'){
                /*                if(!empty($dates) && !empty($ischeck)){
                                    $map["DATE_FORMAT(FROM_UNIXTIME($table.fissuedate),'%Y-%m-%d')"] = ['IN',$dates];
                                }*/
                if(!empty($check_datas)){
                    $map["DATE_FORMAT(FROM_UNIXTIME($table.fissuedate),'%Y-%m-%d')"] = ['IN',$check_datas];
                }
                $map["$table.fissuedate"] = ['between',[$s_time,$e_time]];

                if($isowner_sample){
                    $join_table = 'ttvsample_'.$regionid;
                }else{
                    $join_table = 'ttvsample';
                }
                //追加时长相关字段
                $fad_illegal_play_len = "sum(
                CASE
                WHEN $join_table.fillegaltypecode > 0 THEN
                    flength
                ELSE
                    0
                END
                )";
                $fad_play_len = "sum($table.flength)";
                $fad_illegal_play_len_rate = "
                sum(
                CASE
                WHEN $join_table.fillegaltypecode > 0 THEN
                    flength
                ELSE
                    0
                END
                )/sum(flength)";
            }elseif($key == '02'){
                /*                if(!empty($dates) && !empty($ischeck)){
                                    $map["DATE_FORMAT(FROM_UNIXTIME($table.fissuedate),'%Y-%m-%d')"] = ['IN',$dates];
                                }*/
                if(!empty($check_datas)){
                    $map["DATE_FORMAT(FROM_UNIXTIME($table.fissuedate),'%Y-%m-%d')"] = ['IN',$check_datas];
                }
                $map["$table.fissuedate"] = ['between',[$s_time,$e_time]];
                if($isowner_sample){
                    $join_table = 'tbcsample_'.$regionid;
                }else{
                    $join_table = 'tbcsample';
                }
                //追加时长相关字段
                $fad_illegal_play_len = "sum(
                CASE
                WHEN $join_table.fillegaltypecode > 0 THEN
                    flength
                ELSE
                    0
                END
                )";
                $fad_play_len = "sum($table.flength)";
                $fad_illegal_play_len_rate = "
                sum(
                CASE
                WHEN $join_table.fillegaltypecode > 0 THEN
                    flength
                ELSE
                    0
                END
                )/sum(flength)";
            }elseif($key == '03'){
                //因为报纸的字段名和字段类型和电视广播的发布表与样本表有所不同  所以在此做区分
                $join_issue = 'fpapersampleid';
                $join_sample = 'fpapersampleid';
                if($isowner_sample){
                    $join_table = 'tpapersample_'.$regionid;
                }else{
                    $join_table = 'tpapersample';
                }
                /*                if(!empty($dates) && !empty($ischeck)){
                                    $map["DATE_FORMAT($table.fissuedate,'%Y-%m-%d')"] = ['IN',$dates];
                                }*/
                if(!empty($check_datas)){
                    $map["DATE_FORMAT($table.fissuedate,'%Y-%m-%d')"] = ['IN',$check_datas];
                }
                $map["unix_timestamp($table.fissuedate)"] = ['between',[$s_time,$e_time]];
                //设置时长相关默认值 统一列
                //追加时长相关字段
                $fad_illegal_play_len = '0';
                $fad_play_len = '0';
                $fad_illegal_play_len_rate = "0";
            }

            //查询数据
            $fad_times = "count($table.$join_sample)";//广告条次
            //广告违法条次
            $fad_illegal_times = "count(
                CASE
                WHEN $join_table.fillegaltypecode > 0 THEN
                    1
                ELSE
                    NULL
                END
                )";
            //广告条次违法率
            $fad_illegal_times_rate = "count(
                CASE
                WHEN $join_table.fillegaltypecode > 0 THEN
                    $table.$join_issue
                ELSE
                    NULL
                END
                )/count($table.$join_issue)";

            $fad_count = "count(DISTINCT $join_table.$join_sample)";//广告条数
            //广告违法条数
            $fad_illegal_count = "count(DISTINCT CASE WHEN $join_table.fillegaltypecode > 0 THEN $join_table.$join_sample ELSE NULL END )";
            //广告条数违法率
            $fad_illegal_count_rate = "count(DISTINCT CASE WHEN $join_table.fillegaltypecode > 0 THEN $join_table.$join_sample ELSE NULL END )/count(DISTINCT $join_table.$join_sample)";

            $table_model = M($table);

            //统计数量，用来计算
            $all_count = $table_model
                ->field("
                $fad_illegal_times AS fad_illegal_times,
                $fad_times as fad_times,-- 广告条次 --
                round($fad_illegal_times_rate*100,2) as fad_illegal_times_rate,-- 条次违法率 --
                $fad_illegal_count AS fad_illegal_count,-- 广告违法条数 --
                $fad_count as fad_count,-- 广告条数 --
                round($fad_illegal_count_rate*100,2) as fad_illegal_count_rate,-- 条数违法率 --
                $fad_illegal_play_len as fad_illegal_play_len,-- 违法时长 --
                $fad_play_len as fad_play_len,-- 广告时长 --
                CASE 
                WHEN $fad_illegal_play_len_rate = 0 
                THEN 0.00 
                ELSE 
                round($fad_illegal_play_len_rate*100,2) END
                as fad_illegal_play_len_rate
                ")
                ->join("
                $join_table on 
                $join_table.$join_sample = $table.$join_issue"
                )//连接样本表 用于查询违法信息和匹配广告信息
                ->join("
                tad on 
                tad.fadid = $join_table.fadid AND tad.fadid <> 0"
                )//连接广告表 查询广告名称与广告类别
                ->join("
                tadclass on 
                tadclass.fcode = $fadclasscode"
                )//连接广告类别代码表  用于查询广告类别名称
                ->join("
                tmedia on 
                tmedia.fid = $table.fmediaid"
                )//连接媒体表   用于查询媒体名称
                ->join("
                tmediaclass on 
                tmediaclass.fid = left(tmedia.fmediaclassid,2)"
                )//连接媒体类型表  用于查询媒体类型名称
                ->join("
                tmediaowner on 
                tmediaowner.fid = tmedia.fmediaownerid"
                )
                ->join("
                tregion on 
                tregion.fid = tmediaowner.fregionid"
                )//连接地域表//连接媒介机构表 用于查询媒介机构名称
                ->where($map)
                ->find();

            //综合分
            /*
            *
           *地域排名综合得分计算公式为：综合得分=(条次违法率+条数违法率)/2*50+时长违法率*50
           电视、广播媒体排名综合得分计算公式为：综合得分=(条次违法率+条数违法率)/2*50+时长违法率*50
           报纸媒体综合得分计算公式为：综合得分=条次违法率*60+（违法总条次/全部地域违法总条次）*40
            * */
            $all_fad_illegal_times = $all_count['fad_illegal_times'];
            $com_score = "CASE
                WHEN tmediaclass.fid = '03' THEN
                    ($fad_illegal_times_rate * 60 + ($fad_illegal_times / $all_fad_illegal_times)*40)
                ELSE
                    (($fad_illegal_times_rate + $fad_illegal_count_rate) / 2 * 50 + $fad_illegal_play_len_rate*50)
                END";
            $sql[] = $table_model
                ->field("
                    $group_field,
                    tmediaclass.fclass as tmediaclass_name,
                    tmediaowner.fname as tmediaowner_name,
                    tregion.fname1 as tregion_name,
                    tmedia.fmedianame as tmedia_name,
                    tadclass.fadclass as tadclass_name,
                    $table.fmediaid as fmediaid,
                    tmediaclass.fid as fmedia_class_code,
                    tadclass.fcode as fad_class_code,
                    $fad_illegal_times AS fad_illegal_times,
                    $fad_times as fad_times,
                    round($fad_illegal_times_rate*100,2) as fad_illegal_times_rate,
                    $fad_illegal_count AS fad_illegal_count,
                    $fad_count as fad_count,
                    round($fad_illegal_count_rate*100,2) as fad_illegal_count_rate,
                    $fad_illegal_play_len as fad_illegal_play_len,
                    $fad_play_len as fad_play_len,
                    round($fad_illegal_play_len_rate*100,2) as fad_illegal_play_len_rate,
                    round($com_score,2) AS com_score
                ")
                ->join("
                $join_table on 
                $join_table.$join_sample = $table.$join_issue"
                )//连接样本表 用于查询违法信息和匹配广告信息
                ->join("
                tad on 
                tad.fadid = $join_table.fadid AND tad.fadid <> 0"
                )//连接广告表 查询广告名称与广告类别AND tad.fstate = 1 AND tad.fadid <> 0   AND tmedia.fid = tmedia.main_media_id
                ->join("
                tadclass on 
                tadclass.fcode = $fadclasscode"
                )//连接广告类别代码表  用于查询广告类别名称
                ->join("
                tmedia on 
                tmedia.fid = $table.fmediaid"
                )//连接媒体表 用于查询媒体名称
                ->join("
                tmediaclass on 
                tmediaclass.fid = left(tmedia.fmediaclassid,2)"
                )//连接媒体类型表  用于查询媒体类型名称
                ->join("
                tmediaowner on 
                tmediaowner.fid = tmedia.fmediaownerid"
                )//连接媒介机构表 用于查询媒介机构名称
                ->join("
                tregion on 
                tregion.fid = tmediaowner.fregionid"
                )//连接地域表//连接媒介机构表 用于查询媒介机构名称
                ->where($map)
                ->group($group_field)//通过分组条件分组查询
                //->page($page.',15')
                ->buildSql();

            //上个周期时间条件
            $last_date = date('Y-m',strtotime(date("Y",$s_time).'-'.(date("m",$s_time)-1)));
            $last_s_time = strtotime($last_date);
            $last_e_time = mktime(23, 59, 59, date('m', strtotime($last_date))+1, 00);
            if($key == '01' || $key == '02' ){
                $map["$table.fissuedate"] = ['between',[$last_s_time,$last_e_time]];
            }elseif($key == '03'){
                $map["unix_timestamp($table.fissuedate)"] = ['between',[$last_s_time,$last_e_time]];
            }
            //上个周期统计数量，用来计算
            $prv_all_count = $table_model
                ->field("
                $fad_illegal_times AS fad_illegal_times,
                $fad_times as fad_times,-- 广告条次 --
                round($fad_illegal_times_rate*100,2) as fad_illegal_times_rate,-- 条次违法率 --
                $fad_illegal_count AS fad_illegal_count,-- 广告违法条数 --
                $fad_count as fad_count,-- 广告条数 --
                round($fad_illegal_count_rate*100,2) as fad_illegal_count_rate,-- 条数违法率 --
                $fad_illegal_play_len as fad_illegal_play_len,-- 违法时长 --
                $fad_play_len as fad_play_len,-- 广告时长 --
                CASE 
                WHEN $fad_illegal_play_len_rate = 0 
                THEN 0.00 
                ELSE 
                round($fad_illegal_play_len_rate*100,2) END
                as fad_illegal_play_len_rate
                ")
                ->join("
                $join_table on 
                $join_table.$join_sample = $table.$join_issue"
                )//连接样本表 用于查询违法信息和匹配广告信息
                ->join("
                tad on 
                tad.fadid = $join_table.fadid AND tad.fadid <> 0"
                )//连接广告表 查询广告名称与广告类别
                ->join("
                tadclass on 
                tadclass.fcode = $fadclasscode"
                )//连接广告类别代码表  用于查询广告类别名称
                ->join("
                tmedia on 
                tmedia.fid = $table.fmediaid"
                )//连接媒体表   用于查询媒体名称
                ->join("
                tmediaclass on 
                tmediaclass.fid = left(tmedia.fmediaclassid,2)"
                )//连接媒体类型表  用于查询媒体类型名称
                ->join("
                tmediaowner on 
                tmediaowner.fid = tmedia.fmediaownerid"
                )
                ->join("
                tregion on 
                tregion.fid = tmediaowner.fregionid"
                )//连接地域表//连接媒介机构表 用于查询媒介机构名称
                ->where($map)
                ->find();

            $all_fad_illegal_times = $prv_all_count['fad_illegal_times'];
            $com_score = "CASE
                WHEN tmediaclass.fid = '03' THEN
                    ($fad_illegal_times_rate * 60 + ($fad_illegal_times / $all_fad_illegal_times)*40)
                ELSE
                    (($fad_illegal_times_rate + $fad_illegal_count_rate) / 2 * 50 + $fad_illegal_play_len_rate*50)
                END";



            //查询上个周期的数据
            $prv_sql[] = $table_model
                ->field("
                    $group_field,-- 分组条件 --
                    tmediaclass.fclass as tmediaclass_name,-- 媒体类型代码 --
                    tmediaowner.fname as tmediaowner_name,-- 媒介机构名称 --
                    tregion.fname1 as tregion_name,-- 机构名称 --
                    tmedia.fmedianame as tmedia_name,-- 媒体名称 --
                    tadclass.fadclass as tadclass_name,-- 广告类别名称 --
                    $table.fmediaid as fmediaid,-- 媒体id --
                    tmediaclass.fid as fmedia_class_code,-- 媒体类型代码 --
                    tadclass.fcode as fad_class_code,-- 广告类型代码 --
                    $fad_illegal_times AS fad_illegal_times,
                    $fad_times as fad_times,-- 广告条次 --
                    round($fad_illegal_times_rate*100,2) as fad_illegal_times_rate,-- 条次违法率 --
                    $fad_illegal_count AS fad_illegal_count,-- 广告违法条数 --
                    $fad_count as fad_count,-- 广告条数 --
                    round($fad_illegal_count_rate*100,2) as fad_illegal_count_rate,-- 条数违法率 --
                    $fad_illegal_play_len as fad_illegal_play_len,-- 违法时长 --
                    $fad_play_len as fad_play_len,-- 广告时长 --
                    round($fad_illegal_play_len_rate*100,2) as fad_illegal_play_len_rate,-- 时长违法率 --
                    round($com_score,2) AS com_score
                ")
                ->join("
                $join_table on 
                $join_table.$join_sample = $table.$join_issue"
                )//连接样本表 用于查询违法信息和匹配广告信息
                ->join("
                tad on 
                tad.fadid = $join_table.fadid AND tad.fadid <> 0"
                )//连接广告表 查询广告名称与广告类别
                ->join("
                tadclass on 
                tadclass.fcode = $fadclasscode"
                )//连接广告类别代码表  用于查询广告类别名称
                ->join("
                tmedia on 
                tmedia.fid = $table.fmediaid"
                )//连接媒体表   用于查询媒体名称
                ->join("
                tmediaclass on 
                tmediaclass.fid = left(tmedia.fmediaclassid,2)"
                )//连接媒体类型表  用于查询媒体类型名称
                ->join("
                tmediaowner on 
                tmediaowner.fid = tmedia.fmediaownerid"
                )//连接媒介机构表 用于查询媒介机构名称
                ->join("
                tregion on 
                tregion.fid = tmediaowner.fregionid"
                )//连接地域表//连接媒介机构表 用于查询媒介机构名称
                ->where($map)
                ->group($group_field)//通过分组条件分组查询
                //->page($page.',15')
                ->buildSql();
        }
        $group_arr = explode('.',$group_field);
        if(count($sql) == 1){
            $data = M('')->query($sql[0]);
            $prv_data = M('')->query($prv_sql[0]);
        }else{
            $Model = new \Think\Model();
            $data_sql = $Model->table($sql[0])
                ->alias('a')
                ->field('*')
                ->union([$sql[1],$sql[2]],true)
                ->buildSql();
            $data = $Model->table($data_sql)
                ->alias('a')
                ->field("
                    $group_arr[1],
                    tmediaclass_name,
                    tmediaowner_name,
                    tregion_name,
                    tmedia_name,
                    tadclass_name,
                    fmediaid,
                    fmedia_class_code,
                    fad_class_code,
                    sum(fad_illegal_times) AS fad_illegal_times,
                    sum(fad_times) as fad_times,
                    round((sum(fad_illegal_times)/sum(fad_times))*100,2) as fad_illegal_times_rate,
                    sum(fad_illegal_count) AS fad_illegal_count,
                    sum(fad_count) as fad_count,
                    round((sum(fad_illegal_count)/sum(fad_count))*100,2) as fad_illegal_count_rate,
                    sum(fad_illegal_play_len) as fad_illegal_play_len,
                    sum(fad_play_len) as fad_play_len,
                    round((sum(fad_illegal_play_len)/sum(fad_play_len))*100,2) as fad_illegal_play_len_rate,
                    round(AVG(com_score),2) AS com_score
                ")
                ->group($group_arr[1])
                ->select();
            $prv_data_sql = $Model->table($prv_sql[0])
                ->alias('a')
                ->field('*')
                ->union([$prv_sql[1],$prv_sql[2]],true)
                ->buildSql();
            $prv_data = $Model->table($prv_data_sql)
                ->alias('a')
                ->field("
                    $group_arr[1],-- 分组条件 --
                    tmediaclass_name,-- 媒体类型代码 --
                    tmediaowner_name,-- 媒介机构名称 --
                    tregion_name,-- 机构名称 --
                    tmedia_name,-- 媒体名称 --
                    tadclass_name,-- 广告类别名称 --
                    fmediaid,-- 媒体id --
                    fmedia_class_code,-- 媒体类型代码 --
                    fad_class_code,-- 广告类型代码 --
                    sum(fad_illegal_times) AS fad_illegal_times,
                    sum(fad_times) as fad_times,-- 广告条次 --
                    round((sum(fad_illegal_times)/sum(fad_times))*100,2) as fad_illegal_times_rate,-- 条次违法率 --
                    sum(fad_illegal_count) AS fad_illegal_count,-- 广告违法条数 --
                    sum(fad_count) as fad_count,-- 广告条数 --
                    round((sum(fad_illegal_count)/sum(fad_count))*100,2) as fad_illegal_count_rate,-- 条数违法率 --
                    sum(fad_illegal_play_len) as fad_illegal_play_len,-- 违法时长 --
                    sum(fad_play_len) as fad_play_len,-- 广告时长 --
                    round((sum(fad_illegal_play_len)/sum(fad_play_len))*100,2) as fad_illegal_play_len_rate,-- 时长违法率 --
                    round(AVG(com_score),2) AS com_score
                ")
                ->group($group_arr[1])
                ->select();
        }

        //对比上个周期的数据
        foreach ($prv_data as $illegal_prv_ad_val) {
            foreach ($data as $data_key=>$data_value) {
                if ($illegal_prv_ad_val[$group_arr[1]] == $data_value[$group_arr[1]]) {
                    $data[$data_key]['prv_fad_illegal_times_rate'] = $illegal_prv_ad_val['fad_illegal_times_rate'];
                    if (!$illegal_prv_ad_val['fad_illegal_count'] || ($data_value['fad_illegal_count'] - $illegal_prv_ad_val['fad_illegal_count']) == 0) {
                        $data[$data_key]['prv_fad_illegal_count'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_count = round(($data_value['fad_illegal_count'] - $illegal_prv_ad_val['fad_illegal_count']) / $illegal_prv_ad_val['fad_illegal_count'], 4) * 100;
                        if ($prv_fad_illegal_count < 0) {
                            $data[$data_key]['prv_fad_illegal_count'] = '↓' . '(' . $prv_fad_illegal_count . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_count'] = '↑' . '(' . $prv_fad_illegal_count . '%)';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_times'] || ($data_value['fad_times'] - $illegal_prv_ad_val['fad_times']) == 0) {
                        $data[$data_key]['hb_fad_times'] = '0';
                    } else {

                        $hb_fad_times = $data_value['fad_times'] - $illegal_prv_ad_val['fad_times'];
                        if ($hb_fad_times < 0) {
                            $data[$data_key]['hb_fad_times'] = ($hb_fad_times * -1).'↓';
                        } else {
                            $data[$data_key]['hb_fad_times'] = $hb_fad_times.'↑';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_times'] || ($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) == 0) {
                        $data[$data_key]['hb_fad_illegal_times'] = '0';
                    } else {

                        $hb_fad_illegal_times = $data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times'];
                        if ($hb_fad_illegal_times < 0) {
                            $data[$data_key]['hb_fad_illegal_times'] = ($hb_fad_illegal_times * -1).'↓';
                        } else {
                            $data[$data_key]['hb_fad_illegal_times'] = $hb_fad_illegal_times.'↑';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_times_rate'] || ($data_value['fad_illegal_times_rate'] - $illegal_prv_ad_val['fad_illegal_times_rate']) == 0) {
                        $data[$data_key]['hb_fad_illegal_times_rate'] = '0';
                    } else {

                        $hb_fad_illegal_times_rate = $data_value['fad_illegal_times_rate'] - $illegal_prv_ad_val['fad_illegal_times_rate'];
                        if ($hb_fad_illegal_times_rate < 0) {
                            $data[$data_key]['hb_fad_illegal_times_rate'] = ($hb_fad_illegal_times_rate * -1).'↓';
                        } else {
                            $data[$data_key]['hb_fad_illegal_times_rate'] = $hb_fad_illegal_times_rate.'↑';
                        }
                    }



                    if (!$illegal_prv_ad_val['fad_illegal_times'] || ($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) == 0) {
                        $data[$data_key]['prv_fad_illegal_times'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_times = round(($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) / $illegal_prv_ad_val['fad_illegal_times'], 4) * 100;
                        if ($prv_fad_illegal_times < 0) {
                            $data[$data_key]['prv_fad_illegal_times'] = '↓' . '(' . $prv_fad_illegal_times . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_times'] = '↑' . '(' . $prv_fad_illegal_times . '%)';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_count'] || ($data_value['fad_illegal_count'] - $illegal_prv_ad_val['fad_illegal_count']) == 0) {
                        $data[$data_key]['last_zq_fad_illegal_count'] = '(0.00%)';
                    } else {

                        $last_zq_fad_illegal_count = round(($data_value['fad_illegal_count'] - $illegal_prv_ad_val['fad_illegal_count']) / $illegal_prv_ad_val['fad_illegal_count'], 4) * 100;
                        if ($last_zq_fad_illegal_count < 0) {
                            $data[$data_key]['last_zq_fad_illegal_count'] = '(↓' . ($last_zq_fad_illegal_count* -1) . '%)';
                        } else {
                            $data[$data_key]['last_zq_fad_illegal_count'] = '(↑' . $last_zq_fad_illegal_count . '%)';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_times'] || ($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) == 0) {
                        $data[$data_key]['last_zq_fad_illegal_times'] = '(0.00%)';
                    } else {

                        $last_zq_fad_illegal_times = round(($data_value['fad_illegal_times'] - $illegal_prv_ad_val['fad_illegal_times']) / $illegal_prv_ad_val['fad_illegal_times'], 4) * 100;
                        if ($last_zq_fad_illegal_times < 0) {
                            $data[$data_key]['last_zq_fad_illegal_times'] = '(↓' . ($last_zq_fad_illegal_times* -1) . '%)';
                        } else {
                            $data[$data_key]['last_zq_fad_illegal_times'] = '(↑' . $last_zq_fad_illegal_times . '%)';
                        }
                    }

                    if (!$illegal_prv_ad_val['fad_illegal_play_len'] || ($data_value['fad_illegal_play_len'] - $illegal_prv_ad_val['fad_illegal_play_len']) == 0) {
                        $data[$data_key]['last_zq_fad_illegal_play_len'] = '(0.00%)';
                    } else {

                        $last_zq_fad_illegal_play_len = round(($data_value['fad_illegal_play_len'] - $illegal_prv_ad_val['fad_illegal_play_len']) / $illegal_prv_ad_val['fad_illegal_play_len'], 4) * 100;
                        if ($last_zq_fad_illegal_play_len < 0) {
                            $data[$data_key]['last_zq_fad_illegal_play_len'] = '(↓' . ($last_zq_fad_illegal_play_len* -1) . '%)';
                        } else {
                            $data[$data_key]['last_zq_fad_illegal_play_len'] = '(↑' . $last_zq_fad_illegal_play_len . '%)';
                        }
                    }


                    if (!$illegal_prv_ad_val['fad_illegal_play_len'] || ($data_value['fad_illegal_play_len'] - $illegal_prv_ad_val['fad_illegal_play_len']) == 0) {
                        $data[$data_key]['prv_fad_illegal_play_len'] = '(0.00%)';
                    } else {

                        $prv_fad_illegal_play_len = round(($data_value['fad_illegal_play_len'] - $illegal_prv_ad_val['fad_illegal_play_len']) / $illegal_prv_ad_val['fad_illegal_play_len'], 4) * 100;

                        if ($prv_fad_illegal_play_len < 0) {
                            $data[$data_key]['prv_fad_illegal_play_len'] = '↓' . '(' . $prv_fad_illegal_play_len . '%)';
                        } else {
                            $data[$data_key]['prv_fad_illegal_play_len'] = '↑' . '(' . $prv_fad_illegal_play_len . '%)';
                        }
                    }
                }
            }
        }


        /*线索统计模块*/
        //线索查看处理情况tbn_illegal_ad
        //已处理量
        $tbn_illegal_ad_mod = M('tbn_illegal_ad');
        $s_date = date('Y-m-d',$s_time);
        $e_date = date('Y-m-d',$e_time);
        if(!empty($check_datas)){
            $illegal_map["fissue_date"] = ['IN',$check_datas];
        }
        $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_date,$e_date]];
        $ycl_count = $tbn_illegal_ad_mod
            ->cache(true,600)
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as cll,
                $xs_group_field
           ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id"
            )
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->where($illegal_map)
            ->where(['tbn_illegal_ad.fstatus'=>['gt',0]])
            ->group($xs_group)
            ->select();
        //全部数量
        $cl_count = $tbn_illegal_ad_mod
            ->cache(true,600)
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as clall,
                $xs_group_field
            ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id"
            )
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->where($illegal_map)
            ->group($xs_group)
            ->select();
        $xs_data = [];//定义线索统计情况相关数据

        //如果没有被处理的数据
        if(empty($ycl_count)){
            foreach ($cl_count as $cl_val){
                $xs_data[$cl_val[$hb_field]]['cll'] = '0%';
                $xs_data[$cl_val[$hb_field]][$hb_field] = $cl_val[$hb_field];
                $xs_data[$cl_val[$hb_field]]['cl_count'] = 0;
                $xs_data[$cl_val[$hb_field]]['all_count'] = $cl_val['clall'];
            }
        }else{
            //循环获取
            foreach ($ycl_count as $ycl_val){
                foreach ($cl_count as $cl_val){
                    if($ycl_val[$hb_field] == $cl_val[$hb_field]){
                        if($ycl_val['cll'] != 0){
                            $xs_data[$cl_val[$hb_field]][$hb_field] = $cl_val[$hb_field];
                            $xs_data[$cl_val[$hb_field]]['cll'] = (round($ycl_val['cll']/$cl_val['clall'],4)*100).'%';
                            $xs_data[$cl_val[$hb_field]]['cl_count'] = $ycl_val['cll'];
                        }else{
                            $xs_data[$cl_val[$hb_field]]['cll'] = '0%';
                            $xs_data[$cl_val[$hb_field]][$hb_field] = $cl_val[$hb_field];
                            $xs_data[$cl_val[$hb_field]]['cl_count'] = 0;
                        }
                        $xs_data[$cl_val[$hb_field]]['all_count'] = $cl_val['clall'];
                    }
                }
            }
        }
        //已查看量
        $yck_count = $tbn_illegal_ad_mod
            ->cache(true,600)
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as ckl,
                $xs_group_field
            ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id"
            )
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->where($illegal_map)
            ->where(['tbn_illegal_ad.fview_status'=>['gt',0]])
            ->group($xs_group)
            ->select();

        //全部数量
        $ck_count = $tbn_illegal_ad_mod
            ->cache(true,600)
            ->field("
                COUNT(DISTINCT tbn_illegal_ad.fid) as ckall,
                $xs_group_field
            ")
            ->join("
                    tbn_illegal_ad_issue on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id"
            )
            ->where($illegal_map)
            ->group($xs_group)
            ->select();

        //如果没有一查看的数量
        if(empty($yck_count)){
            foreach ($ck_count as $ck_val){
                $xs_data[$ck_val[$hb_field]]['ckl'] = '0%';
                $xs_data[$ck_val[$hb_field]]['ck_count'] = 0;
                $xs_data[$ck_val[$hb_field]]['all_count'] = $ck_val['ckall'];
            }
        }else{
            //循环获取
            foreach ($yck_count as $yck_val){
                foreach ($ck_count as $ck_val){
                    if($yck_val[$hb_field] == $ck_val[$hb_field]){
                        if($yck_val['ckl'] != 0){
                            $xs_data[$ck_val[$hb_field]]['ck_count'] = $yck_val['ckl'];
                        }else{
                            $xs_data[$ck_val[$hb_field]]['ck_count'] = 0;
                        }
                        $xs_data[$ck_val[$hb_field]]['ckl'] = (round($yck_val['ckl']/$ck_val['ckall'],4)*100).'%';
                    }
                    $xs_data[$ck_val[$hb_field]]['all_count'] = $ck_val['ckall'];
                }
            }//组建案件查看与处理情况
        }


        //组合线索相关数据
        foreach ($xs_data as $xs_key=>$xs_value){
            foreach ($data as $data_key=>$data_value){
                if($xs_key == $data_value[$fz_field]){
                    unset($xs_value[$hb_field]);
                    $cl_count_all += $xs_value['cl_count'];
                    $ck_count_all += $xs_value['ck_count'];
                    $count_all += $xs_value['all_count'];
                    $data[$data_key] = array_merge($xs_value, $data[$data_key]);//合并数组
                }
            }
        }
        //对比

        $data = $this->pxsf($data,$ranking_condition);//排序
        $array_num = 0;//定义限定数量
        $data_pm = [];//定义图表数据空数组
        $data_count = count($data);//获取数据条数

        $cl_count_all = 0;//线索全部数量
        $ck_count_all = 0;//线索全部数量
        $count_all = 0;//总数

        //处理排序后的数据
        foreach ($data as $data_key=>$data_value){
            //循环获取前30条数据
            if($array_num < 30 && $array_num < count($data) && $data_value[$ranking_condition] > 0){
                $data_pm['name'][] = $data_value[$tb_name];
                //不同排名条件显示各自的数值
                switch ($ranking_condition){
                    case 'com_score':
                        $data_pm['num'][] = $data[$data_key]['com_score'];//综合
                        break;
                    case 'fad_illegal_play_len':
                        $data_pm['num'][] = $data_value['fad_illegal_play_len'];//违法时长
                        break;
                    case 'fad_illegal_play_len_rate':
                        $data_pm['num'][] = $data[$data_key]['lens_illegal_rate'];//时长违法率
                        break;
                    case 'fad_illegal_times':
                        $data_pm['num'][] = $data_value['fad_illegal_times'];//违法条次
                        break;
                    case 'fad_illegal_times_rate':
                        $data_pm['num'][] = $data[$data_key]['times_illegal_rate'];//条次违法率
                        break;
                    case 'fad_illegal_count':
                        $data_pm['num'][] = $data[$data_key]['fad_illegal_count'];//条数
                        break;
                    case 'fad_illegal_count_rate':
                        $data_pm['num'][] = $data[$data_key]['counts_illegal_rate'];//条数违法率
                        break;
                    case 'fad_count':
                        $data_pm['num'][] = $data[$data_key]['fad_count'];//条数违法率
                        break;
                    case 'fad_times':
                        $data_pm['num'][] = $data[$data_key]['fad_times'];//条数违法率
                        break;
                    case 'fad_play_len':
                        $data_pm['num'][] = $data[$data_key]['fad_play_len'];//条数违法率
                        break;
                }
                $array_num++;//fad_count  广告条次：fad_times  广告时长：fad_play_len
            }
        }
        $data_res['data_pm'] = $data_pm;
        $data_res['data_list'] = $data;
        return $data;

    }

    /*违法线索统计  南京专项报告  */
    public function ad_illegal_customize($s_time,$e_time,$medias,$ad_type){
        $system_num = getconfig('system_num');
        $map['unix_timestamp(tbn_illegal_ad_issue.fissue_date)'] = ['BETWEEN',[$s_time,$e_time]];
        $map['_string'] = 'tmedia.fid = tmedia.main_media_id';
        $map['_string'] = "tbn_illegal_ad.fcustomer='".$system_num."'";
        if(is_array($medias)){
            if(!empty($medias)){
                $map['tbn_illegal_ad_issue.fmedia_id'] = ['IN',$medias];
            }
        }else{
            $region_id = rtrim($medias,'0');
            $map['tregion.fid'] = ['like',$region_id.'%'];
        }

        if(is_array($ad_type)){
            if(!empty($ad_type)){
                $map["tadclass.fcode"] = ['IN',$ad_type];
            }
        }else{
            if($ad_type != ''){
                $map["tadclass.fcode"] = $ad_type;
            }
        }
        $data = M('tbn_illegal_ad_issue')
            ->field("
                    CASE
                WHEN tbn_illegal_ad.fmedia_class = 1 THEN
                    '电视'
                WHEN tbn_illegal_ad.fmedia_class = 2 THEN
                    '广播'
                WHEN tbn_illegal_ad.fmedia_class = 3 THEN
                    '报纸'
                END AS media_class,
                 tmedia.fmedianame,
                 tadclass.ffullname,
                 tbn_illegal_ad.fad_name,
                 tbn_illegal_ad_issue.fissue_date,
                 count(*) total,
                 tbn_illegal_ad.fillegal_code,
                 tbn_illegal_ad.fillegal"
            )//媒介类型  媒体名称  广告类别   广告名称   发布日期   发布条次   违法条款  违法内容
            ->join('tbn_illegal_ad ON tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id')
            ->join('tmedia ON tmedia.fid = tbn_illegal_ad_issue.fmedia_id')
            ->join('tadclass ON tadclass.fcode = LEFT (tbn_illegal_ad.fad_class_code,2)')
            ->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')
            ->join('tregion ON tregion.fid = tmediaowner.fregionid')
            ->where($map)
            ->group('tbn_illegal_ad.fsample_id')
            ->order('total DESC')
            ->select();
        foreach ($data as $data_key=>$data_val){
            $fconfirmation = explode(';',$data_val['fillegal_code']);
            $fillegal_code_arr = [];
            foreach ($fconfirmation as $fconfirmation_val){
                $fillegal_code_arr[] = M('tillegal')->where(['fcode'=>$fconfirmation_val,'fstate'=>1])->getField('fconfirmation');
            }
            $fillegal_code = implode(';',$fillegal_code_arr);
            $data[$data_key]['fillegal_code'] = $fillegal_code;
        }
        return $data;
    }

    /* 互联网违法明细列表 */
    public function illegal_detail_list($s_time,$e_time,$owner_id,$net_platform,$is_inquire_subordinate=true){
        $system_num = getconfig('system_num');//获取国家局标记
        if(is_array($owner_id)){
            if(!empty($owner_id)){
                $where_str = 'tmedia.fid IN ('.join(',',$owner_id).')';
            }
        }else{

            if($is_inquire_subordinate){
                $owner_id = rtrim($owner_id,'0');
            }
            $where_str = "tmediaowner.fregionid LIKE '".$owner_id."%'";

        }
        $sql = "SELECT
                tadclass.ffullname,
                tmedia.fmedianame,
                tnetissue.fadname,
                tnetissue.thumb_url_true,
                tnetissueputlog.net_original_url,
                tnetissueputlog.net_target_url,
                tnetissue.fillegalcontent,
                tnetissue.fexpressioncodes,
                tnetissue.major_key,
                count(1) AS fb_count
            FROM
                tnetissueputlog
            JOIN tnetissue ON tnetissue.major_key = tnetissueputlog.tid and finputstate=2 
            JOIN tmedia ON tmedia.fid = tnetissueputlog.fmediaid
            JOIN tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid
            JOIN tregion ON tregion.fid = tmediaowner.fregionid
            JOIN tadclass ON tadclass.fcode = tnetissue.fadclasscode
            JOIN tmedia_temp ON tmedia.fid = tmedia_temp.fmediaid
            WHERE
            ".$where_str."
            AND tmedia_temp.ftype=1 
            AND tmedia_temp.fcustomer = '".$system_num."' 
            AND tmedia_temp.fuserid=".session('regulatorpersonInfo.fid')."
            AND tnetissue.fillegaltypecode > 0 
            AND tnetissueputlog.net_platform = ".$net_platform."
            AND (tnetissueputlog.net_created_date / 1000) BETWEEN '".$s_time."' AND '".$e_time."'
            AND tmedia.fid = tmedia.main_media_id
            GROUP BY
                tnetissue.major_key
            ORDER BY
                fb_count DESC;";//tnetissueputlog.net_original_url

        $data = M('')->query($sql);

        $fad_name_md5 = [];
        $data_res = [];
        foreach ($data as $data_key=>$data_val){
            $name_md5 = MD5($data_val['major_key']);
            if(!in_array($name_md5,$fad_name_md5)){
                $fad_name_md5[] = $name_md5;
                $data_res[$name_md5] = $data_val;

                /*                $img = getimages(htmlspecialchars_decode($data_res[$name_md5]['fillegalcontent']));
                                if($img){
                                    $data_res[$name_md5]['fillegalcontent'] = $img;
                                }else{
                                    $data_res[$name_md5]['fillegalcontent'] =  str_replace("&nbsp;","",strip_tags(htmlspecialchars_decode($data_res[$name_md5]['fillegalcontent'])));
                                }*/
                if(!empty($data_val['net_original_url'])){
                    $data_res[$name_md5]['net_original_url'] .= '-----'.$data_val['fb_count'].'次;';
                }else{
                    $data_res[$name_md5]['net_original_url'] = '地址不详';
                }
            }else{
                if(!empty($data_val['net_original_url'])){
                    $data_res[$name_md5]['net_original_url'] .= $data_val['net_original_url'].'-----'.$data_val['fb_count'].'次;
                ';
                }else{
                    if(!isset($data_res[$name_md5]['net_original_url']) || $data_res[$name_md5]['net_original_url'] == '地址不详'){
                        $data_res[$name_md5]['net_original_url'] = '地址不详';
                    }
                }
                $data_res[$name_md5]['fb_count'] += $data_val['fb_count'];
            }
        }

        return $this->pxsf(array_values($data_res),$score_rule = 'fb_count');

    }


    /* 互联网违法明细列表 */
    public function illegal_detail_list_OS($s_time,$e_time,$owner_id){
        $system_num = getconfig('system_num');//获取国家局标记

        if(empty($s_time) || empty($e_time) || empty($owner_id)){
            return [];
        }

        $sql = "SELECT
                e.ffullname,
                d.fmedianame,
                c.fadname,
                c.thumb_url_true,
                c.net_original_url,
                c.net_target_url,
                a.fillegal as fillegalcontent,
                c.net_snapshot,
                a.fexpressions,
                a.fillegal_code as fexpressioncodes,
                c.major_key,
                COUNT(DISTINCT b.fid) AS fb_count
            FROM
                tbn_illegal_ad a
            JOIN tbn_illegal_ad_issue b ON a.fid = b.fillegal_ad_id
            JOIN tnetissue c ON c.major_key = a.fsample_id
            JOIN tmedia d ON a.fmedia_id = d.fid
            JOIN tadclass e ON e.fcode = a.fad_class_code
            WHERE
                    a.fmedia_id IN(".join(',',$owner_id).")
            AND	 b.fissue_date BETWEEN '$s_time'
            AND '$e_time'
            AND a.fmedia_class = 13 AND a.fcustomer = '".$system_num."' 
            GROUP BY a.fid;";//tnetissueputlog.net_original_url

        $data = M('')->query($sql);

        $fad_name_md5 = [];
        $data_res = [];
        foreach ($data as $data_key=>$data_val){
            $name_md5 = MD5($data_val['major_key']);
            if(!in_array($name_md5,$fad_name_md5)){
                $fad_name_md5[] = $name_md5;
                $data_res[$name_md5] = $data_val;

                /*                $img = getimages(htmlspecialchars_decode($data_res[$name_md5]['fillegalcontent']));
                                if($img){
                                    $data_res[$name_md5]['fillegalcontent'] = $img;
                                }else{
                                    $data_res[$name_md5]['fillegalcontent'] =  str_replace("&nbsp;","",strip_tags(htmlspecialchars_decode($data_res[$name_md5]['fillegalcontent'])));
                                }*/
                if(!empty($data_val['net_original_url'])){
                    $data_res[$name_md5]['net_original_url'] .= '-----'.$data_val['fb_count'].'次;';
                }else{
                    $data_res[$name_md5]['net_original_url'] = '地址不详';
                }
            }else{
                if(!empty($data_val['net_original_url'])){
                    $data_res[$name_md5]['net_original_url'] .= $data_val['net_original_url'].'-----'.$data_val['fb_count'].'次;
                ';
                }else{
                    if(!isset($data_res[$name_md5]['net_original_url']) || $data_res[$name_md5]['net_original_url'] == '地址不详'){
                        $data_res[$name_md5]['net_original_url'] = '地址不详';
                    }
                }
                $data_res[$name_md5]['fb_count'] += $data_val['fb_count'];
            }
        }

        return $this->pxsf(array_values($data_res),$score_rule = 'fb_count');

    }


    public function pxsf($data = [],$score_rule = 'com_score'){
        $data_count = count($data);
        for($k=0;$k<=$data_count;$k++)
        {
            for($j=$data_count-1;$j>$k;$j--){
                if($data[$j][$score_rule]>$data[$j-1][$score_rule]){
                    $temp = $data[$j];
                    $data[$j] = $data[$j-1];
                    $data[$j-1] = $temp;
                }
            }
        }
        return $data;
    }

    //判断当前用户行政等级,并返回
    public function judgment_region($regionid){
        $level = M('tregion')->where(['fid'=>$regionid])->getField('flevel');
        /*        if($level == 2 || $level == 3){
                    $data['region'] = [
                        '1' =>'省',
                        '2' =>'副省级市',
                        '3' =>'计划单列市',
                        '4' =>'市',
                        '5' =>'区县'
                    ];//选区域

                    $data['region_level'] = [
                        '1' =>'全国',
                        '2' =>'省级',
                        '3' =>'市级',
                        '4' =>'县级'
                    ];//行政级别
                }*/
        return $level;
    }

    //区分省直属媒体与副省级媒体
    public function distinguish_media($medias,$regionid){

        $sql = 'SELECT
                tmediaowner.fregionid,
                (CASE
                WHEN tmediaowner.fregionid = '.substr($regionid,0,2).'0000 THEN
                    1
                WHEN left(tmediaowner.fregionid,4) = '.substr($regionid,0,4).' and tmediaowner.fregionid <> '.$regionid.' THEN
                    2
                ELSE
                    0
		        END) as isszs,
                tmedia.fid
                FROM
                    tmedia
                JOIN tmediaowner ON tmedia.fmediaownerid = tmediaowner.fid
                JOIN tregion ON tregion.fid = tmediaowner.fregionid
                WHERE
                tmedia.fid IN ('.$medias.');';
        $media_mod = M('')->query($sql);
        $medias_data = [];
        foreach ($media_mod as $media_mod_val){
            $medias_data[$media_mod_val['isszs']][] = $media_mod_val['fid'];
        }
        return $medias_data;

    }

    //OS公益查询-电视
    public function os_tv_public_welfare($medias,$s_date,$e_date){
        $system_num = getconfig('system_num');//获取客户编号

        if(!empty($medias) && !empty($s_date) && !empty($e_date)){
            $map['tbn_ad_summary_day_v3_gongyi.fmediaid'] = ['in',$medias];
            $map['tbn_ad_summary_day_v3_gongyi.fdate'] = ['BETWEEN',[$s_date,$e_date]];
            $map['tbn_ad_summary_day_v3_gongyi.fmedia_class_code'] = '01';

            $res  = M('tbn_ad_summary_day_v3_gongyi')
                ->field("
             sum(fad_times_1921) as gysl_1921,
             sum(fad_play_len_1921) as gysc_1921,
             tmedia.fmedianame as tmname
            ")
                ->join("tmedia on tmedia.fid = tbn_ad_summary_day_v3_gongyi.fmediaid")
                ->group('tbn_ad_summary_day_v3_gongyi.fmediaid')
                ->where($map)
                ->where(['fcustomer'=>$system_num])
                ->select();
            return $res;
        }else{
            return [];
        }

    }

    //OS公益查询-广播
    public function os_tb_public_welfare($medias,$s_date,$e_date){
        $system_num = getconfig('system_num');//获取客户编号

        if(!empty($medias) && !empty($s_date) && !empty($e_date)){
            $map['tbn_ad_summary_day_v3_gongyi.fmediaid'] = ['in',$medias];
            $map['tbn_ad_summary_day_v3_gongyi.fdate'] = ['BETWEEN',[$s_date,$e_date]];
            $map['tbn_ad_summary_day_v3_gongyi.fmedia_class_code'] = '02';

            $res  = M('tbn_ad_summary_day_v3_gongyi')
                ->field("
             sum(fad_times_1921) as gysl_1113,
             sum(fad_times_0608) as gysl_0608,
             sum(fad_play_len_1921) as gysc_1113,
             sum(fad_play_len_0608) as gysc_0608,
             tmedia.fmedianame as tmname
            ")
                ->join("tmedia on tmedia.fid = tbn_ad_summary_day_v3_gongyi.fmediaid")
                ->group('tbn_ad_summary_day_v3_gongyi.fmediaid')
                ->where($map)
                ->where(['fcustomer'=>$system_num])
                ->select();
            return $res;
        }else{
            return [];
        }

    }

    //OS公益查询-报纸
    public function os_tp_public_welfare($medias,$s_date,$e_date){
        $system_num = getconfig('system_num');//获取客户编号

        if(!empty($medias) && !empty($s_date) && !empty($e_date)){
            $map['tbn_ad_summary_day_v3_gongyi.fmediaid'] = ['in',$medias];
            $map['tbn_ad_summary_day_v3_gongyi.fdate'] = ['BETWEEN',[$s_date,$e_date]];
            $map['tbn_ad_summary_day_v3_gongyi.fmedia_class_code'] = '03';

            $res  = M('tbn_ad_summary_day_v3_gongyi')
                ->field("
             sum(fad_times) as gysl,
             tmedia.fmedianame as tmname
            ")
                ->join("tmedia on tmedia.fid = tbn_ad_summary_day_v3_gongyi.fmediaid")
                ->group('tbn_ad_summary_day_v3_gongyi.fmediaid')
                ->where($map)
                ->where(['fcustomer'=>$system_num])
                ->select();
            return $res;
        }else{
            return [];
        }

    }


    //电视公益广告查询
    public function tv_public_welfare($medias,$date_table){
        if($medias){
            $sql_tv = 'SELECT
                        count(1) AS gysl,
                    tm.fmedianame AS tmname
                    FROM
                    ttvsample as ts
                    JOIN ttvissue_'.$date_table.' AS tv ON tv.fsampleid = ts.fid
                    JOIN tmedia AS tm ON tm.fid = ts.fmediaid
                    JOIN tad ON tad.fadid = ts.fadid AND tad.fadid <> 0
                    WHERE
                    tad.fadclasscode = 2202
                    AND tv.fmediaid IN ('.join(',',$medias).')
                    AND (from_unixtime(tv.fstarttime, "%H:%i:%S") BETWEEN "19:00:00" AND "21:00:00" OR from_unixtime(tv.fendtime, "%H:%i:%S") BETWEEN "19:00:00" AND "21:00:00") 
                    group by tv.fmediaid';
            $result_tv = M()->query($sql_tv);
            return $result_tv;
        }else{
            return [];
        }

    }

    //广播公益广告查询fstarttime fendtime
    public function tb_public_welfare($medias,$date_table){
        //广播媒体公益广告发布情况

        if($medias){
            $sql_bc = 'SELECT
	            	count(
                    CASE
                    WHEN (from_unixtime(bc.fstarttime, "%H:%i:%S") >= "06:00:00"
                    AND from_unixtime(bc.fstarttime, "%H:%i:%S") <= "08:00:00") 
                    OR (from_unixtime(bc.fendtime, "%H:%i:%S") >= "06:00:00"
                    AND from_unixtime(bc.fendtime, "%H:%i:%S") <= "08:00:00") THEN
                        1
                    ELSE
                        NULL
                    END
                ) AS gysl1,
	            	count(
                    CASE
                    WHEN (from_unixtime(bc.fstarttime, "%H:%i:%S") >= "11:00:00"
                    AND from_unixtime(bc.fstarttime, "%H:%i:%S") <= "13:00:00") 
                    OR (from_unixtime(bc.fendtime, "%H:%i:%S") >= "11:00:00"
                    AND from_unixtime(bc.fendtime, "%H:%i:%S") <= "13:00:00") THEN
                        1
                    ELSE
                        NULL
                    END
                ) AS gysl2,
                tm.fmedianame AS tmname
                FROM
	            tbcsample as tb
                JOIN tbcissue_'.$date_table.' AS bc ON bc.fsampleid = tb.fid
                JOIN tmedia AS tm ON tm.fid = tb.fmediaid
                JOIN tad ON tad.fadid = tb.fadid AND tad.fadid <> 0
                WHERE
	            tad.fadclasscode = 2202 
                AND bc.fmediaid IN ('.join(',',$medias).')
                AND ((from_unixtime(bc.fstarttime, "%H:%i:%S") BETWEEN "06:00:00" AND "08:00:00" OR from_unixtime(bc.fstarttime, "%H:%i:%S") BETWEEN "11:00:00" AND "13:00:00")
                    OR (from_unixtime(bc.fendtime, "%H:%i:%S") BETWEEN "06:00:00" AND "08:00:00" OR from_unixtime(bc.fendtime, "%H:%i:%S") BETWEEN "11:00:00" AND "13:00:00")
                    )
                group by bc.fmediaid';
            $result_bc = M()->query($sql_bc);
            return $result_bc;
        }else{
            return [];
        }


    }

    //报纸公益广告查询
    public function tp_public_welfare($medias,$s_time,$e_time){
        if($medias){
            $sql_paper = 'SELECT
                count(*) AS gysl,
                tm.fmedianame AS tmname
                FROM
	            tpapersample as tpaper
                JOIN tpaperissue AS paper ON paper.fpapersampleid = tpaper.fpapersampleid
                JOIN tmedia AS tm ON tm.fid = tpaper.fmediaid
                JOIN tad ON tad.fadid = tpaper.fadid AND tad.fadid <> 0
                WHERE
	            tad.fadclasscode = 2202
                AND paper.fmediaid IN ('.join(',',$medias).')
                AND UNIX_TIMESTAMP(paper.fissuedate) BETWEEN '.$s_time.' AND '.$e_time.'
                group by paper.fmediaid';
            $result_paper = M()->query($sql_paper);
            return $result_paper;
        }else{
            return [];
        }

    }

    //典型违法广告
    public function typical_illegal_ad($date_table,$medias,$media_type,$s_time,$e_time,$ad_type = ''){
        if($ad_type != ''){
            $ad_type_condition = "AND tad.fadclasscode = $ad_type";
        }else{
            $ad_type_condition = '';
        }
        $join_id = 'fid';
        $fsampleid = 'fsampleid';
        $where = ' ';
        switch ($media_type){
            case 1:
                $table = 'ttvissue_'.$date_table;
                $ybb = 'ttvsample';
                $tp_field = 'ybb.favifilepng as image,';
                $where = ' AND '.$table.'.fissuedate BETWEEN '.$s_time.' AND '.$e_time.' ';
                break;
            case 2:
                $table = 'tbcissue_'.$date_table;
                $ybb = 'tbcsample';
                $tp_field = '';
                $where = ' AND '.$table.'.fissuedate BETWEEN '.$s_time.' AND '.$e_time.' ';
                break;
            case 3:
                $table = 'tpaperissue';
                $ybb = 'tpapersample';
                $join_id = 'fpapersampleid';
                $fsampleid = 'fpapersampleid';
                $tp_field = 'ybb.fjpgfilename as image,';
                $where = ' AND UNIX_TIMESTAMP('.$table.'.fissuedate) BETWEEN '.$s_time.' AND '.$e_time.' ';
                break;
        }

        $sql = "SELECT
                    tad.fadname as fad_name,
                    ".$tp_field."
                    tmedia.fmedianame,
                    tadclass.fadclass,
                    ybb.fexpressions as fillegal,
                    COUNT(1) AS illegal_times
                FROM
                    ".$table."
                JOIN ".$ybb." as ybb ON ybb.".$join_id." = ".$table.".".$fsampleid."//
                JOIN tad ON ybb.fadid = tad.fadid
                AND tad.fadid <> 0
                JOIN tadclass ON tad.fadclasscode = tadclass.fcode
                JOIN tmedia ON ".$table.".fmediaid = tmedia.fid
                JOIN tmediaowner ON tmedia.fmediaownerid = tmediaowner.fid
                WHERE
                tmedia.fid = tmedia.main_media_id
                AND tmedia.fid IN (".join(',',$medias).")
                ".$ad_type_condition."
                AND ybb.fillegaltypecode = 30
                ".$where."
                GROUP BY ybb.".$join_id."
                ORDER BY
                illegal_times DESC;";

        $result = M()->query($sql);

        return $result;

    }

    //违法列表hz
    public function illegal_ad_order_list($date_table,$medias,$media_type,$s_time,$e_time,$ad_type = '',$is_inquire_subordinate = 1,$isowner_sample = false){
        $system_num = getconfig('system_num');
        $regionid = session('regulatorpersonInfo.regionid');

        if($ad_type != ''){
            $ad_type_condition = "AND left(tad.fadclasscode,2) IN (".join(',',$ad_type).")";
        }else{
            $ad_type_condition = '';
        }
        $join_id = 'fid';
        $fsampleid = 'fsampleid';

        $where = ' ';
        //是否需要抽查
        $ischeck = getconfig('ischeck');
        //客户编
        $dates = [];//定义日期数组
        if(!empty($ischeck)){
            $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
            //如果抽查表有数据
            if(!empty($spot_check_data)){
                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                    $condition = trim($spot_check_data_val['condition']);
                    if(!empty($condition)){
                        $year_month = '';
                        $date_str = [];
                        $year_month = substr($spot_check_data_val['fmonth'],0,7);
                        $date_str = explode(',',$spot_check_data_val['condition']);
                        foreach ($date_str as $date_str_val){

                            $dates[] = $year_month.'-'.$date_str_val;
                        }
                    }
                }
            }else{
                $where['_string'] = '1 = 0';
            }
        }
        switch ($media_type){
            case 1:
                $table = 'ttvissue_'.$date_table;
                if($isowner_sample){
                    $ybb = 'ttvsample_'.$regionid;
                }else{
                    $ybb = 'ttvsample';
                }
                $tp_field = "CONCAT(DATE_FORMAT(
                        FROM_UNIXTIME(
                            MIN(".$table.".fissuedate)
                        ),
                        '%Y-%m-%d'
                    ), ',', DATE_FORMAT(
                        FROM_UNIXTIME(
                            MAX(".$table.".fissuedate)
                        ),
                        '%Y-%m-%d'
                    )) AS fissue_date,";
                $where = ' AND '.$table.'.fissuedate BETWEEN '.$s_time.' AND '.$e_time.' ';
                break;
            case 2:
                $table = 'tbcissue_'.$date_table;
                if($isowner_sample){
                    $ybb = 'tbcsample_'.$regionid;
                }else{
                    $ybb = 'tbcsample';
                }
                $tp_field = "CONCAT(DATE_FORMAT(
                        FROM_UNIXTIME(
                            MIN(".$table.".fissuedate)
                        ),
                        '%Y-%m-%d'
                    ), ',', DATE_FORMAT(
                        FROM_UNIXTIME(
                            MAX(".$table.".fissuedate)
                        ),
                        '%Y-%m-%d'
                    )) AS fissue_date,";
                $where = ' AND '.$table.'.fissuedate BETWEEN '.$s_time.' AND '.$e_time.' ';
                break;
            case 3:
                if($isowner_sample){
                    $table = 'tpaperissue_'.$regionid;
                    $ybb = 'tpapersample_'.$regionid;
                }else{
                    $table = 'tpaperissue';
                    $ybb = 'tpapersample';
                }
                $join_id = 'fpapersampleid';
                $fsampleid = 'fpapersampleid';
                $tp_field = 'CONCAT(left(MIN('.$table.'.fissuedate),10),"~",left(MAX('.$table.'.fissuedate)) as fissue_date,';
                $where = ' AND UNIX_TIMESTAMP('.$table.'.fissuedate) BETWEEN '.$s_time.' AND '.$e_time.' ';
                break;
        }

        switch ($is_inquire_subordinate){
            case 0:
                $where .= ' AND tregion.fid = "'.$regionid.'"';//只看本级
                break;
            case 1:
                $where .= ' AND (tregion.fpid = "'.$regionid.'" or tregion.fid = "'.$regionid.'")';//包含本级以及下级
                break;
            case 2:
                $where .= ' AND tregion.fpid = "'.$regionid.'"';//只看下级
                break;
        }
        $sql = "SELECT
                tmedia.fmedianame,
                tad.fadname AS fad_name,
                tadclass.fadclass,
                COUNT(1) AS illegal_times
                FROM
                    ".$table."
                JOIN ".$ybb." as ybb ON ybb.".$join_id." = ".$table.".".$fsampleid."
                JOIN tad ON ybb.fadid = tad.fadid
                AND tad.fadid <> 0
                JOIN tadclass ON left(tad.fadclasscode,2) = tadclass.fcode
                JOIN tmedia ON ".$table.".fmediaid = tmedia.fid
                JOIN tmediaowner ON tmedia.fmediaownerid = tmediaowner.fid
                JOIN tregion ON tregion.fid = tmediaowner.fregionid
                WHERE
                tmedia.fid = tmedia.main_media_id
                AND tmedia.fid IN (".join(',',$medias).")
                ".$ad_type_condition."
                AND ybb.fillegaltypecode = 30
                ".$where."
                GROUP BY tmedia.fid,tadclass.fcode,tad.fadname
                ORDER BY
	            illegal_times DESC";
        $result = M()->query($sql);
        return $result;

    }
    //违法列表hz
    public function typical_illegal_ad_hz_list($date_table,$medias,$media_type,$s_time,$e_time,$ad_type = '',$is_inquire_subordinate = 1,$check_datas = [],$isowner_sample = false){
        $regionid = session('regulatorpersonInfo.regionid');

        if($ad_type != ''){
            $ad_type_condition = "AND left(tad.fadclasscode,2) IN (".join(',',$ad_type).")";
        }else{
            $ad_type_condition = '';
        }
        $join_id = 'fid';
        $fsampleid = 'fsampleid';

        $where = ' ';
        //是否需要抽查
        $ischeck = getconfig('ischeck');
        $system_num = getconfig('system_num');
        $dates = [];//定义日期数组
        if(!empty($ischeck)){
            $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
            //如果抽查表有数据
            if(!empty($spot_check_data)){
                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                    $condition = trim($spot_check_data_val['condition']);
                    if(!empty($condition)){
                        $year_month = '';
                        $date_str = [];
                        $year_month = substr($spot_check_data_val['fmonth'],0,7);
                        $date_str = explode(',',$spot_check_data_val['condition']);
                        foreach ($date_str as $date_str_val){

                            $dates[] = $year_month.'-'.$date_str_val;
                        }
                    }
                }
            }else{
                $where['_string'] = '1 = 0';
            }
        }
        switch ($media_type){
            case 1:
                $table = 'ttvissue_'.$date_table;
                if($isowner_sample){
                    $ybb = 'ttvsample_'.$regionid;
                }else{
                    $ybb = 'ttvsample';
                }
                $tp_field = "CONCAT(DATE_FORMAT(
                        FROM_UNIXTIME(
                            MIN(".$table.".fissuedate)
                        ),
                        '%Y-%m-%d'
                    ), ',', DATE_FORMAT(
                        FROM_UNIXTIME(
                            MAX(".$table.".fissuedate)
                        ),
                        '%Y-%m-%d'
                    )) AS fissue_date,";
                $where = ' AND '.$table.'.fissuedate BETWEEN '.$s_time.' AND '.$e_time.' ';
                if(!empty($check_datas)){
                    $where .= "AND DATE_FORMAT(FROM_UNIXTIME($table.fissuedate),'%Y-%m-%d') IN (".join(',',$check_datas).")";
                }
                break;
            case 2:
                $table = 'tbcissue_'.$date_table;
                if($isowner_sample){
                    $ybb = 'tbcsample_'.$regionid;
                }else{
                    $ybb = 'tbcsample';
                }
                $tp_field = "CONCAT(DATE_FORMAT(
                        FROM_UNIXTIME(
                            MIN(".$table.".fissuedate)
                        ),
                        '%Y-%m-%d'
                    ), ',', DATE_FORMAT(
                        FROM_UNIXTIME(
                            MAX(".$table.".fissuedate)
                        ),
                        '%Y-%m-%d'
                    )) AS fissue_date,";
                $where = ' AND '.$table.'.fissuedate BETWEEN '.$s_time.' AND '.$e_time.' ';
                if(!empty($check_datas)){
                    $where .= "AND DATE_FORMAT(FROM_UNIXTIME($table.fissuedate),'%Y-%m-%d') IN (".join(',',$check_datas).")";
                }
                break;
            case 3:
                if($isowner_sample){
                    $table = 'tpaperissue_'.$regionid;
                    $ybb = 'tpapersample_'.$regionid;
                }else{
                    $table = 'tpaperissue';
                    $ybb = 'tpapersample';
                }
                $join_id = 'fpapersampleid';
                $fsampleid = 'fpapersampleid';
                $tp_field = 'CONCAT(left(MIN('.$table.'.fissuedate),10),"~",left(MAX('.$table.'.fissuedate)) as fissue_date,';
                $where = ' AND UNIX_TIMESTAMP('.$table.'.fissuedate) BETWEEN '.$s_time.' AND '.$e_time.' ';
                if(!empty($check_datas)){
                    $where .= "AND DATE_FORMAT($table.fissuedate,'%Y-%m-%d') IN (".join(',',$check_datas).")";
                }
                break;
        }

        switch ($is_inquire_subordinate){
            case 0:
                $map['tregion.fid'] = $regionid;//只看本级
                break;
            case 1:
                $map['tregion.fpid|tregion.fid'] = $regionid;//包含本级以及下级
                break;
            case 2:
                $map['tregion.fpid'] = $regionid;//只看下级
                break;
        }
        $sql = "SELECT
                tmedia.fmedianame,
                tadclass.fadclass,
                tad.fadname AS fad_name,
                COUNT(1) AS illegal_times,
                COUNT(DISTINCT ybb.".$join_id.") AS illegal_count
                FROM
                    ".$table."
                JOIN ".$ybb." as ybb ON ybb.".$join_id." = ".$table.".".$fsampleid."
                JOIN tad ON ybb.fadid = tad.fadid
                AND tad.fadid <> 0
                JOIN tadclass ON left(tad.fadclasscode,2) = tadclass.fcode
                JOIN tmedia ON ".$table.".fmediaid = tmedia.fid
                JOIN tmediaowner ON tmedia.fmediaownerid = tmediaowner.fid
                WHERE
                tmedia.fid = tmedia.main_media_id
                AND tmedia.fid IN (".join(',',$medias).")
                ".$ad_type_condition."
                AND ybb.fillegaltypecode = 30
                ".$where."
                GROUP BY ybb.".$join_id." 
                ORDER BY tmedia.fid,tadclass.fcode";
        $result = M()->query($sql);
        return $result;

    }

    //违法列表菏泽专用
    public function typical_illegal_ad_heze_list($date_table,$medias,$media_type,$s_time,$e_time,$ad_type = '',$is_inquire_subordinate = 1,$check_datas = []){
        $regionid = session('regulatorpersonInfo.regionid');
        foreach ($check_datas as $check_datas_key=>$check_datas_val){
            $check_datas[$check_datas_key] = "'".$check_datas_val."'";
        }
        if($ad_type != ''){
            $ad_type_condition = "AND left(tad.fadclasscode,2) IN (".join(',',$ad_type).")";
        }else{
            $ad_type_condition = '';
        }
        $join_id = 'fid';
        $fsampleid = 'fsampleid';

        $where = ' ';
        //是否需要抽查
        $system_num = getconfig('system_num');

        switch ($media_type){
            case 1:
                $table = 'ttvissue_'.$date_table;
                $ybb = 'ttvsample';
                $tp_field = "CONCAT(DATE_FORMAT(
                        FROM_UNIXTIME(
                            MIN(".$table.".fissuedate)
                        ),
                        '%Y-%m-%d'
                    ), '~', DATE_FORMAT(
                        FROM_UNIXTIME(
                            MAX(".$table.".fissuedate)
                        ),
                        '%Y-%m-%d'
                    )) AS fissue_date,";
                $media_class = "'电视' as media_type,";
                $where = ' AND '.$table.'.fissuedate BETWEEN '.$s_time.' AND '.$e_time.' ';
                if(!empty($check_datas)){
                    $where .= "AND DATE_FORMAT(FROM_UNIXTIME($table.fissuedate),'%Y-%m-%d') IN (".join(',',$check_datas).")";
                }
                break;
            case 2:
                $table = 'tbcissue_'.$date_table;
                $ybb = 'tbcsample';
                $tp_field = "CONCAT(DATE_FORMAT(
                        FROM_UNIXTIME(
                            MIN(".$table.".fissuedate)
                        ),
                        '%Y-%m-%d'
                    ), '~', DATE_FORMAT(
                        FROM_UNIXTIME(
                            MAX(".$table.".fissuedate)
                        ),
                        '%Y-%m-%d'
                    )) AS fissue_date,";
                $media_class = "'广播' as media_type,";

                $where = ' AND '.$table.'.fissuedate BETWEEN '.$s_time.' AND '.$e_time.' ';
                if(!empty($check_datas)){
                    $where .= "AND DATE_FORMAT(FROM_UNIXTIME($table.fissuedate),'%Y-%m-%d') IN (".join(',',$check_datas).")";
                }
                break;
            case 3:
                $table = 'tpaperissue';
                $ybb = 'tpapersample';
                $join_id = 'fpapersampleid';
                $fsampleid = 'fpapersampleid';
                $tp_field = 'CONCAT(left(MIN('.$table.'.fissuedate),10),"~",left(MAX('.$table.'.fissuedate),10)) as fissue_date,';
                $media_class = '"报纸" as media_type,';
                $where = ' AND UNIX_TIMESTAMP('.$table.'.fissuedate) BETWEEN '.$s_time.' AND '.$e_time.' ';
                if(!empty($check_datas)){
                    $where .= "AND DATE_FORMAT($table.fissuedate,'%Y-%m-%d') IN (".join(',',$check_datas).")";
                }
                break;
        }
        switch ($is_inquire_subordinate){
            case 0:
                $map['tregion.fid'] = $regionid;//只看本级
                $where .=  'AND tregion.fid = '.$regionid.' ';
                break;
            case 1:
                $where .=  'AND tregion.fpid = '.$regionid.' or tregion.fid = '.$regionid.' ';
                break;
            case 2:
                $where .=  'AND tregion.fpid = '.$regionid.' ';
                break;
        }


        /*
         * 	tregion.fname1,
	'电视' AS media_type,
	tmedia.fmedianame,
	tad.fadname AS fad_name,
	tadclass.ffullname,
         * */
        $sql = "SELECT
                tregion.fname1,
                $media_class
                tmedia.fmedianame,
                tad.fadname AS fad_name,
                tadclass.ffullname,
                $tp_field
                COUNT(1) AS illegal_times,
                COUNT(DISTINCT ybb.".$join_id.") AS illegal_count,
                ybb.fillegalcontent,
	            ybb.fconfirmations
                FROM
                    ".$table."
                JOIN ".$ybb." as ybb ON ybb.".$join_id." = ".$table.".".$fsampleid."
                JOIN tad ON ybb.fadid = tad.fadid
                AND tad.fadid <> 0
                JOIN tadclass ON tad.fadclasscode = tadclass.fcode
                JOIN tmedia ON ".$table.".fmediaid = tmedia.fid
                JOIN tmediaowner ON tmedia.fmediaownerid = tmediaowner.fid
                JOIN tregion ON tregion.fid = tmediaowner.fregionid
                WHERE
                tmedia.fid = tmedia.main_media_id
                AND tmedia.fid IN (".join(',',$medias).")
                ".$ad_type_condition."
                AND ybb.fillegaltypecode = 30
                ".$where."
                GROUP BY ybb.".$join_id." 
                ORDER BY illegal_times desc";
        $result = M()->query($sql);
        return $result;

    }

    //违法列表
    public function typical_illegal_ad_list($date_table,$medias,$media_type,$s_time,$e_time,$ad_type = ''){

        if($ad_type != ''){
            $ad_type_condition = "AND tad.fadclasscode = $ad_type";
        }else{
            $ad_type_condition = '';
        }
        $join_id = 'fid';
        $fsampleid = 'fsampleid';

        $where = ' ';
        switch ($media_type){
            case 1:
                $table = 'ttvissue_'.$date_table;
                $ybb = 'ttvsample';
                $tp_field = "DATE_FORMAT(
                        FROM_UNIXTIME(
                            MAX(".$table.".fissuedate)
                        ),
                        '%Y-%m-%d'
                    ) AS fissue_date,";
                $where = ' AND '.$table.'.fissuedate BETWEEN '.$s_time.' AND '.$e_time.' ';
                break;
            case 2:
                $table = 'tbcissue_'.$date_table;
                $ybb = 'tbcsample';
                $tp_field = "DATE_FORMAT(
                        FROM_UNIXTIME(
                            MAX(".$table.".fissuedate)
                        ),
                        '%Y-%m-%d'
                    ) AS fissue_date,";
                $where = ' AND '.$table.'.fissuedate BETWEEN '.$s_time.' AND '.$e_time.' ';
                break;
            case 3:
                $table = 'tpaperissue';
                $ybb = 'tpapersample';
                $join_id = 'fpapersampleid';
                $fsampleid = 'fpapersampleid';
                $tp_field = 'left(MAX('.$table.'.fissuedate),10) as fissue_date,';
                $where = ' AND UNIX_TIMESTAMP('.$table.'.fissuedate) BETWEEN '.$s_time.' AND '.$e_time.' ';
                break;
        }
        $sql = "SELECT
                    tmedia.fmedianame,
                    tadclass.fadclass,
                    tad.fadname as fad_name,
                    COUNT(1) AS illegal_times,
                    ".$tp_field."
                    ybb.fexpressions as fillegal
                FROM
                    ".$table."
                JOIN ".$ybb." as ybb ON ybb.".$join_id." = ".$table.".".$fsampleid."
                JOIN tad ON ybb.fadid = tad.fadid
                AND tad.fadid <> 0
                JOIN tadclass ON tad.fadclasscode = tadclass.fcode
                JOIN tmedia ON ".$table.".fmediaid = tmedia.fid
                JOIN tmediaowner ON tmedia.fmediaownerid = tmediaowner.fid
                WHERE
                tmedia.fid = tmedia.main_media_id
                AND tmedia.fid IN (".join(',',$medias).")
                ".$ad_type_condition."
                AND ybb.fillegaltypecode = 30
                ".$where."
                GROUP BY ybb.".$join_id." 
                ORDER BY tmedia.fid,tadclass.fcode";
        $result = M()->query($sql);
        return $result;

    }

    /**@国家局监测汇总数据查询接口 by yjn
     * @param string $times_table 时间段
     * @param $fztj 分组条件
     * @param $year 年份
     * @param $table_condition 周期
     * @param int $is_show_longad 是否加入长广告
     * @param int $is_show_fsend 是否要加入未发布的数据
     */
    public function  gjj_date($times_table='tbn_ad_summary_month',$fztj='fregionid',$level = 1,$year='2018',$table_condition='07-01',$is_show_longad=2,$is_show_fsend=2,$isfixeddata=0){
        ini_set('memory_limit','1024M');
        set_time_limit(0);
        session_write_close();//停止使用session
        $system_num = getconfig('system_num');
        if($system_num == '100000' ||empty($isfixeddata)){
            $summary_table = "tbn_ad_summary_day_z";
            $map[$summary_table.'.fcustomer'] = $system_num;
        }else{
            $summary_table = "tbn_ad_summary_day";
        }

        if($is_show_longad == 1){
            $play_len_field = 'in_long_ad_play_len';
        }else{
            $play_len_field = 'fad_play_len';
        }

        //根据选择的不同的时间组合不同的条件
        switch ($times_table){
            case 'tbn_ad_summary_week':
                if($table_condition < 10){
                    $table_condition = '0'.$table_condition;
                }
                $cycle_stamp = $this->weekday($year,$table_condition);//周期首末时间戳
                break;
            case 'tbn_ad_summary_half_month':
            case 'tbn_ad_summary_month':
            case 'tbn_ad_summary_quarter':
                $table_condition = $year.'-'.$table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                break;
            case 'tbn_ad_summary_half_year':
            case 'tbn_ad_summary_year':
                $table_condition = $table_condition;//半月，月，季度
                $cycle_stamp = $this->get_stemp($table_condition,$times_table);
                break;
        }

        $s_time = date('Y-m-d',$cycle_stamp['start']);
        $e_time = date('Y-m-d',($cycle_stamp['end']-86400));
        $map = [];//定义查询条件
        $map[$summary_table.'.fdate'] = ['BETWEEN',[$s_time,$e_time]];
        $map['_string'] = 'tmedia.fid = tmedia.main_media_id';
        $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_time,$e_time]];
        $illegal_map['_string'] = "tbn_illegal_ad.fcustomer='".$system_num."'";
        $illegal_map['_string'] = 'tmedia.fid = tmedia.main_media_id';
        $s_timestamp = strtotime($s_time);//开始时间戳
        $e_timestamp = strtotime($e_time);//结束时间戳

        //获取上个周期的日期范围
        if(($e_timestamp - $s_timestamp) <= 0){
            $last_s_time = date('Y-m-d',($s_timestamp-86400));//上周期开始时间戳
            $last_e_time = $last_s_time;//上周期结束时间戳
        }else{
            $last_s_time = date('Y-m-d',($s_timestamp-($e_timestamp-$s_timestamp)-86400));
            $last_e_time = date('Y-m-d',($s_timestamp-86400));
        }

        /*数据控制   如果是国家局*/
        $isshow_count = false;
        if($system_num == '100000'){
            //判断是否国家局系统
            $map[$summary_table.'.confirm_state'] = ['GT',0];
            $gjj_label_media = S('usermedia'.session('regulatorpersonInfo.fid'));

            $gjj_label_media = array_unique($gjj_label_media);
            $map[$summary_table.'.fmediaid'] = array('in',$gjj_label_media);//国家局标签媒体
            if($level){
                if(is_array($level)){
                    $map['tregion.flevel'] = ['IN',$level];//行政等级
                }else{
                    $map['tregion.flevel'] = $level;//行政等级
                }
            }
            $illegal_map['tbn_illegal_ad_issue.fmedia_id'] = array('in',$gjj_label_media);//国家局标签媒体

            //是否包含违法长广告时长
            if($is_show_longad != 1){
                $illegal_map['tbn_illegal_ad.is_long_ad'] = 0;
            }
        }

        $isrelease = getconfig('isrelease');
        if(!empty($isrelease) || $system_num == '100000'){
          //是否显示发布前数据
            if($is_show_fsend == 1){
                $illegal_map['tbn_illegal_ad_issue.fsend_status'] = ['IN',[1,2]];
            }else{
                $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
            }  
        }
        
        $data = [];//定义统计数据空数组

        //按照选定时间实例化表
        $table_model = M($summary_table);

        $id = 'fid';//定义默认表ID
        $name = 'fname';//定义默认单元名称

        //匹配分组条件，连接不同表
        switch ($fztj){
            case 'fmedia_class_code':
                $join_table = 'tmediaclass';//fmedia_class_code媒介类别表fid,
                $xs_group = 'tbn_illegal_ad.fmedia_class';
                $xs_group_field = 'tbn_illegal_ad.fmedia_class';
                $hb_field = 'fmedia_class';
                break;
            case 'flevel':
                $join_table = 'tregion';//fmedia_class_code媒介类别表fid,
                $xs_group = 'tregion.flevel';
                $xs_group_field = 'tregion.flevel';
                $hb_field = 'flevel';
                break;
            case 'fmediaownerid':
                $join_table = 'tmediaowner';//fmediaownerid媒介机构表fid
                $xs_group = 'tbn_illegal_ad.fmediaownerid';
                $xs_group_field = 'tbn_illegal_ad.fmediaownerid';
                $hb_field = 'fmediaownerid';
                break;
            case 'fregionid':
                $join_table = 'tregion';//fregionid行政区划表fid
                $xs_group = 'tbn_illegal_ad.fregion_id';
                $xs_group_field = 'tbn_illegal_ad.fregion_id';
                $hb_field = 'fregion_id';
                break;
            case 'fmediaid':
                $join_table = 'tmedia';//fmediaid媒介表fid
                $xs_group = 'tbn_illegal_ad.fmedia_id';
                $xs_group_field = 'tbn_illegal_ad.fmedia_id';
                $hb_field = 'fmedia_id';
                break;
            case 'fad_class_code':
                $join_table = 'tadclass';//fad_class_code广告内容类别表fcode
                $id = 'fcode';
                $xs_group = 'left(tbn_illegal_ad.fad_class_code,2)';
                $xs_group_field = 'left(tbn_illegal_ad.fad_class_code,2) as fad_class_code';
                $hb_field = 'fad_class_code';
                break;
        }

        //违法广告条次与时长
        $illegal_group = 'tbn_illegal_ad.'.$xs_group;
        $tbn_illegal_data = M('tbn_illegal_ad_issue')
            ->field("
              $xs_group_field,
              tregion.flevel,
              	GROUP_CONCAT(
                CONCAT_WS(
                    '_',
                    tbn_illegal_ad.fsample_id,
                    tbn_illegal_ad.fmedia_class
                ) SEPARATOR ','
            ) AS fsample_ids,
              SUM(
                UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fendtime
                ) - UNIX_TIMESTAMP(
                  tbn_illegal_ad_issue.fstarttime
                )
                ) AS fad_illegal_play_len,
              COUNT(1) AS fad_illegal_times
            ")
            ->where($illegal_map)
            ->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id"
            )
            ->join("
                tadclass on 
                tadclass.fcode = tbn_illegal_ad.fad_class_code "
            )
            ->group($xs_group)
            ->select();

        if($fztj == 'flevel'){
            $group_field = 'tregion.'.$fztj;
        }else{
            $group_field = $summary_table.'.'.$fztj;
        }

        $illegal_ad_mod = $table_model
            ->cache(true,600)
            ->field("
                $group_field,
                tmediaclass.fclass as tmediaclass_name,
                tmediaowner.fname as tmediaowner_name,
                tregion.fname1 as tregion_name,
                tregion.flevel as flevel,
                 (case when instr(tmedia.fmedianame,'（') > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,'（') -1) else tmedia.fmedianame end) as tmedia_name,
                tadclass.fadclass as tadclass_name,
                $summary_table.fmediaid as fmediaid,
                $summary_table.fmediaownerid as fmediaownerid,
                $summary_table.fmedia_class_code as fmedia_class_code,
                $summary_table.fregionid as fregionid,
                GROUP_CONCAT(fad_sam_list SEPARATOR ',') as fad_sam_list,
                $summary_table.fad_class_code as fad_class_code,
                sum($summary_table.fad_times) as fad_times,
                sum($summary_table.$play_len_field) as fad_play_len
           ")
            ->join("
                tmediaclass on 
                tmediaclass.fid = $summary_table.fmedia_class_code"
            )
            ->join("
                tmediaowner on 
                tmediaowner.fid = $summary_table.fmediaownerid"
            )
            ->join("
                tregion on 
                tregion.fid = $summary_table.fregionid"
            )
            ->join("
                tmedia on 
                tmedia.fid = $summary_table.fmediaid"
            )
            ->join("
                tadclass on 
                tadclass.fcode = $summary_table.fad_class_code"
            )
            ->where($map)
            ->group($group_field)
            //->page($page.',15')
            ->select();

        $data = $illegal_ad_mod;
        //循环计算条数
        foreach ($data as $data_key => $data_val){
            $sam_array = explode(',',$data_val['fad_sam_list']);
            $fad_count = count(array_unique($sam_array));
            unset($data[$data_key]['fad_sam_list']);
            $data[$data_key]['fad_count'] = $fad_count;//插入条数
        }

        //循环计算违法条数
        foreach ($tbn_illegal_data as $tbn_illegal_data_key => $tbn_illegal_data_val){
            $sam_array = explode(',',$tbn_illegal_data_val['fsample_ids']);
            $fad_illegal_count = count(array_unique($sam_array));
            unset($tbn_illegal_data[$tbn_illegal_data_key]['fsample_ids']);
            $tbn_illegal_data[$tbn_illegal_data_key]['fad_illegal_count'] = $fad_illegal_count;//插入违法条数
        }
        //组合违法相关数据
        foreach ($tbn_illegal_data as $tbn_illegal_data_key=>$tbn_illegal_data_value){
            foreach ($data as $data_key=>$data_value){

                if($tbn_illegal_data_value[$hb_field] == $data_value[$fztj]){

                    unset($tbn_illegal_data_value[$hb_field]);
                    $data[$data_key]['counts_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_count']/$data_value['fad_count'];//条数违法率
                    $data[$data_key]['times_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_times']/$data_value['fad_times'];//条次违法率
                    $data[$data_key]['lens_illegal_rate'] = $tbn_illegal_data_value['fad_illegal_play_len']/$data_value['fad_play_len'];//时长违法率
                    $data[$data_key] = array_merge($tbn_illegal_data_value, $data[$data_key]);//合并数组
                }
            }
        }
        //循环计算
        foreach ($data as $data_key => $data_val){
            if(!isset($data_val['fad_illegal_count'])){
                $data[$data_key]['fad_illegal_count'] = 0;
            }
            if(!isset($data_val['counts_illegal_rate'])){
                $data[$data_key]['counts_illegal_rate'] = 0;
            }

            if(!isset($data_val['fad_illegal_times'])){
                $data[$data_key]['fad_illegal_times'] = 0;
            }
            if(!isset($data_val['times_illegal_rate'])){
                $data[$data_key]['times_illegal_rate'] = 0;
            }
            if(!isset($data_val['fad_illegal_play_len'])){
                $data[$data_key]['fad_illegal_play_len'] = 0;
            }
            if(!isset($data_val['lens_illegal_rate'])){
                $data[$data_key]['lens_illegal_rate'] = 0;
            }
            $all_count['fad_count'] += $data[$data_key]['fad_count'];
            $all_count['fad_illegal_count'] += $data[$data_key]['fad_illegal_count'];
            $all_count['fad_illegal_times'] += $data[$data_key]['fad_illegal_times'];
            $all_count['fad_times'] += $data[$data_key]['fad_times'];
            $all_count['fad_illegal_play_len'] += $data[$data_key]['fad_illegal_play_len'];
            $all_count['fad_play_len'] += $data[$data_key]['fad_play_len'];
        }

        //处理排序后的数据
        foreach ($data as $data_key=>$data_value){
            $data[$data_key]['counts_illegal_rate'] = round($data_value['counts_illegal_rate'],4)*100;
            $data[$data_key]['times_illegal_rate'] = round($data_value['times_illegal_rate'],4)*100;
            $data[$data_key]['lens_illegal_rate'] = round($data_value['lens_illegal_rate'],4)*100;
        }
        return $data;
    }

    public function summary_illegal_expression($s_date = '2018-07-01',$e_date = '2018-07-31',$is_show_longad=2,$is_show_fsend=2,$isfixeddata = 0){
        $system_num = getconfig('system_num');
        if($system_num == '100000'||empty($isfixeddata)){
            $summary_table = "tbn_ad_summary_day_z";
            $map[$summary_table.'.fcustomer'] = $system_num;
        }else{
            $summary_table = "tbn_ad_summary_day";
        }

        if($system_num == '100000'){
            //判断是否国家局系统
            $map[$summary_table.'.confirm_state'] = ['GT',0];
            $gjj_label_media = S('usermedia'.session('regulatorpersonInfo.fid'));

            $gjj_label_media = array_unique($gjj_label_media);

            $illegal_map['tbn_illegal_ad_issue.fmedia_id'] = array('in',$gjj_label_media);//国家局标签媒体
            

            //是否包含违法长广告时长
            if($is_show_longad != 1){
                $illegal_map['tbn_illegal_ad.is_long_ad'] = 0;
            }
        }

        $isrelease = getconfig('isrelease');
        if(!empty($isrelease) || $system_num == '100000'){
          //是否显示发布前数据
            if($is_show_fsend == 1){
                $illegal_map['tbn_illegal_ad_issue.fsend_status'] = ['IN',[1,2]];
            }else{
                $illegal_map['tbn_illegal_ad_issue.fsend_status'] = 2;
            }  
        }

        $illegal_map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_date,$e_date]];
        $illegal_map['tregion.flevel'] = ['IN',[1,2,3]];
        $illegal_map['_string'] = 'tmedia.fid = tmedia.main_media_id';
        $illegal_map['_string'] = "tbn_illegal_ad.fcustomer='".$system_num."'";



        $tbn_illegal_data = M('tbn_illegal_ad_issue')
            ->field("
                tbn_illegal_ad.fexpressions,
                LEFT (
                    tbn_illegal_ad.fad_class_code,
                    2
                ) AS fad_class_code,
                tadclass.fadclass,
                COUNT(tbn_illegal_ad.fid) AS fad_illegal_times
            ")
            ->where($illegal_map)
            ->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id"
            )
            ->join("
                tadclass on 
                tadclass.fcode = left(tbn_illegal_ad.fad_class_code,2)"
            )
            ->group('tbn_illegal_ad.fexpressions')
            ->order('fad_illegal_times desc')
            ->select();
        return $tbn_illegal_data;
    }

    /**
     * 获取某年第几周的开始日期和结束日期
     * @param int $year
     * @param int $week 第几周;
     */
    public function weekday($year,$week=0){
        $year_start = mktime(0,0,0,1,1,$year);
        $year_end = mktime(0,0,0,12,31,$year);

        // 判断第一天是否为第一周的开始
        if (intval(date('W',$year_start))===1){
            $start = $year_start;//把第一天做为第一周的开始
        }else{
            $week++;
            $start = strtotime('+1 monday',$year_start);//把第一个周一作为开始
        }

        // 第几周的开始时间
        if ($week===1){
            $weekday['start'] = $start;
        }else{
            $weekday['start'] = strtotime('+'.($week-0).' monday',$start);
        }

        // 第几周的结束时间
        $weekday['end'] = strtotime('+1 sunday',$weekday['start']);
        if (date('Y',$weekday['end'])!=$year){
            $weekday['end'] = $year_end;
        }
        return $weekday;
    }

    //获取首末周期时间戳
    public function get_stemp($cycles='2018-01-01',$time_type='tbn_ad_summary_half_month'){
        switch($time_type){
            case 'tbn_ad_summary_half_month':
                $day = substr($cycles,-2);
                $month = substr($cycles,5,2);
                $year = substr($cycles,0,4);
                if($day == '01'){
                    if($month == '02'){
                        $time_string = $year.'-'.$month.'-15';
                    }else{
                        $time_string = $year.'-'.$month.'-16';
                    }
                }else{
                    if($month == '12'){
                        $year = $year+1;
                        $time_string = $year.'-01-01';
                    }else{
                        if($month == '02'){
                            $cycles = $year.'-'.$month.'-15';
                        }
                        $month = ($month+1) < 10 ? '0'.($month+1):($month+1);
                        $time_string = $year.'-'.$month.'-01';
                    }
                }
                
                break;
            case 'tbn_ad_summary_month':
                $month = substr($cycles,5,2);
                $year = substr($cycles,0,4);

                if($month == '12'){
                    $year = $year+1;
                    $time_string = $year.'-01-01';
                }else{
                    $month = ($month+1) < 10 ? '0'.($month+1):($month+1);
                    $time_string = $year.'-'.$month.'-01';
                }
                break;
            case 'tbn_ad_summary_quarter':
                $quarter = substr($cycles,5,2);
                $year = substr($cycles,0,4);
                if($quarter == '10'){
                    $year = $year+1;
                    $time_string = $year.'-01-01';
                }else{
                    $quarter = ($quarter+3) < 10 ? '0'.($quarter+3):($quarter+3);
                    $time_string = $year.'-'.$quarter.'-01';
                }
                break;
            case 'tbn_ad_summary_half_year':
                $half_year = substr($cycles,5,2);
                $year = substr($cycles,0,4);
                if($half_year == '06'){
                    $year = $year+1;
                    $time_string = $year.'-01-01';
                }else{
                    $time_string = $year.'-07-01';
                }
                break;
            case 'tbn_ad_summary_year':
                $year = substr($cycles,0,4);
                $year = $year+1;
                $time_string = $year.'-01-01';
                break;
        }
        $cycle['start'] = strtotime($cycles);
        $cycle['end'] = strtotime($time_string);
        return $cycle;
    }

    function get_media_type($fpid = ''){
        $system_num = getconfig('system_num');
        $map = [];
        if(!empty($fpid)){
            $map['tmediaclass.fpid'] = $fpid;
            $fmediaclassid = "tmedia.fmediaclassid";
        }else{
            $fmediaclassid = "LEFT(tmedia.fmediaclassid, 2)";
        }
        $media_types = M('tmediaclass')
            ->join("tmedia ON  $fmediaclassid = tmediaclass.fid")
            ->join("tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid")
            ->where([
                'tmedia_temp.ftype'=>1,
                'tmedia_temp.fcustomer'=>$system_num,
                'tmedia_temp.fuserid'=>session('regulatorpersonInfo.fid'),
                ])
            ->where($map)
            ->group("tmediaclass.fid")
            ->getField('tmediaclass.fid,tmediaclass.fclass',true);
        if(empty($media_types)){
            return [];
        }
        return $media_types;
    }

    public function net_media_type_array($fpid){
        $data = M('tmediaclass')
            ->where(['fpid' =>$fpid])
            ->getField("fid,fclass",true);
        return $data;
    }

    /*by yjn*/
    public function sta_cule_data($s_fissue_date,$e_fissue_date,$region=0,$fmedia_class=null){

        //$fmedia_class = I("fmedia_class");//媒体类型

        //$is_out_report = I("is_out_report",0);//是否是导出EXCEl

        //$region = I("region",0);//查看级别

        //$s_fsend_datetime = I("s_fsend_datetime");//派发日期开始
        //$e_fsend_datetime = I("e_fsend_datetime");//派发日期结束

        //$fresult_date = I("fresult_date");//处理结束时间

        //$s_fissue_date = I("s_fissue_date");//发布时间开始
        //$e_fissue_date = I("e_fissue_date");//发布时间结束

        $where = " ";
        $ischeck = getconfig('ischeck');
        $isexamine = getconfig('isexamine');
        $system_num = getconfig('system_num');

        if(in_array($isexamine, [10,20,30,40])){
            $where .= " AND a.fexamine = 10 ";
        }

        //是否抽查模式
        if(!empty($ischeck)){
            $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
            //如果抽查表有数据
            if(!empty($spot_check_data)){
                $dates = [];//定义日期数组
                foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
                    $year_month = '';
                    $date_str = [];
                    $year_month = substr($spot_check_data_val['fmonth'],0,7);
                    if(!empty($spot_check_data_val['condition'])){
                        $date_str = explode(',',$spot_check_data_val['condition']);
                    }
                    foreach ($date_str as $date_str_val){
                        $dates[] = $year_month.'-'.$date_str_val;
                    }
                }
                $where  .= ' AND d.fissue_date IN ("'.implode('","', $dates).'")';
            }else{
                $where  .= ' AND 1 = 0';
            }
        }

        switch ($region){
            case 0:
                $where .= " AND b.flevel IN(1,2,3,4,5)";
                break;
            case 1:
                $where .= " AND b.flevel IN(1)";
                break;
            case 2:
                $where .= " AND b.flevel IN(2,3,4)";
                break;
            case 3:
                $where .= " AND b.flevel = 5";
                break;
        }

        //媒体类型
        if(!empty($fmedia_class)){
            $where .= " AND a.fmedia_class = ".$fmedia_class." ";
        }

        //派发日期
        /*        if(!empty($s_fsend_datetime) && !empty($e_fsend_datetime)){
                    $where .= " AND a.fsend_datetime BETWEEN '".$s_fsend_datetime."' AND '".$e_fsend_datetime."' ";
                }*/

        //发布时间
        if(!empty($s_fissue_date) && !empty($e_fissue_date)){
            $where .= " AND d.fissue_date BETWEEN '".$s_fissue_date."' AND '".$e_fissue_date."' ";
        }


        /*      if(!empty($fresult_date)){
                    $fresult_date_map = " AND a.fresult_datetime <= '".$fresult_date."'";
                }else{
                    $fresult_date_map = '';
                }*/

        //统计线索立案以及处理情况 dump($res);exit;
        $res = M("")->query("
        SELECT a.fregion_id,IF (b.flevel = 1,'省级',b.fname1) AS region_name,COUNT(DISTINCT a.fmedia_id) AS media_count,
        GROUP_CONCAT(DISTINCT (SELECT CASE WHEN instr(fmedianame, '（') > 0 THEN LEFT (fmedianame,instr(fmedianame, '（') - 1) ELSE fmedianame
		END as fmedianame from tmedia WHERE fid = a.fmedia_id) SEPARATOR '；<br/>') as medias,COUNT(DISTINCT a.fid) AS cule_count,
        COUNT(DISTINCT (CASE WHEN a.fview_status = 10 THEN a.fid END)) AS ck_count,
        ROUND(COUNT(DISTINCT (CASE WHEN a.fview_status = 10 THEN a.fid END)) / COUNT(DISTINCT a.fid) * 100,2) AS ckl,
        COUNT(DISTINCT (CASE WHEN a.fstatus3 IN (30, 40) THEN a.fid END)) AS pass_count,
        COUNT(DISTINCT (CASE WHEN a.fstatus3 IN (10, 20, 50) THEN a.fid END ) ) AS unpass_count,
        COUNT(DISTINCT ( CASE WHEN a.fstatus = 10 ".$fresult_date_map." THEN a.fid END ) ) AS cl_count,
        ROUND( COUNT( DISTINCT ( CASE WHEN a.fstatus = 10 ".$fresult_date_map."  THEN a.fid END ) ) / 
        (COUNT(DISTINCT a.fid) - COUNT(DISTINCT (CASE WHEN a.fstatus3 IN (30, 40) THEN a.fid END))) * 100,2) AS cll,
        COUNT(DISTINCT ( CASE WHEN fstatus = 10 AND fstatus2 IN (15, 17) THEN a.fid END ) ) AS yla,
        COUNT(DISTINCT (CASE WHEN fstatus = 10 AND fstatus2 = 16 THEN a.fid END ) ) AS yja,
        COUNT(DISTINCT (CASE WHEN fstatus = 10 AND fresult LIKE '%责令停止发布%' THEN a.fid END ) ) AS tzfb,
        COUNT(DISTINCT (CASE WHEN fstatus = 10 AND fresult LIKE '%立案%' THEN a.fid END ) ) AS lian,
        COUNT(DISTINCT (CASE WHEN fstatus = 10 AND fresult LIKE '%移送司法%' THEN a.fid END ) ) AS yjsf,
        COUNT(DISTINCT (CASE WHEN fstatus = 10 AND fresult LIKE '%其他%' THEN a.fid END )) AS others
        FROM tbn_illegal_ad a
        JOIN tregion b ON a.fregion_id = b.fid
        JOIN tmedia c ON c.fid = a.fmedia_id
        JOIN tbn_illegal_ad_issue d ON d.fillegal_ad_id = a.fid
        WHERE 1 = 1
        AND a.fcustomer = ".$system_num.$where."
        GROUP BY fregion_id ORDER BY b.fid DESC;");//fsend_datetime

        $res = pxsf($res,"fregion_id",false);

        $all = [];
        $all['media_count'] = 0;
        $all['medias'] = '';
        $all['cule_count'] = 0;
        $all['ck_count'] = 0;
        $all['pass_count'] = 0;
        $all['unpass_count'] = 0;
        $all['cl_count'] = 0;
        $all['yla'] = 0;
        $all['yja'] = 0;
        $all['tzfb'] = 0;
        $all['lian'] = 0;
        $all['yjsf'] = 0;
        $all['others'] = 0;
        $all['region_name'] = "合计";

        if(!empty($res)){
            foreach ($res as $r_key => $r_val){

                $res[$r_key]['ckl'] =str_replace(".00","", $r_val['ckl']."%");
                $res[$r_key]['cll'] = str_replace(".00","",$r_val['cll']."%");

                $all['media_count'] += $r_val['media_count'];
                $all['medias'] .= $r_val['medias'];
                $all['cule_count'] += $r_val['cule_count'];
                $all['ck_count'] += $r_val['ck_count'];
                $all['pass_count'] += $r_val['pass_count'];
                $all['unpass_count'] += $r_val['unpass_count'];
                $all['cl_count'] += $r_val['cl_count'];
                $all['yla'] += $r_val['yla'];
                $all['yja'] += $r_val['yja'];
                $all['tzfb'] += $r_val['tzfb'];
                $all['lian'] += $r_val['lian'];
                $all['yjsf'] += $r_val['yjsf'];
                $all['others'] += $r_val['others'];
            }
        }

        $all['ckl'] = (round($all['ck_count']/$all['cule_count'],4)*100)."%";
        $all['cll'] =  (round($all['cl_count']/$all['cule_count'],4)*100)."%";
        $res[] = $all;

        return $res;

    }

    //违法广告列表，按广告名称分组
    public function ill_ad_list($s_date,$e_date,$medias,$media_class = 0,$fadclass='',$range = 0,$region_id = -1,$g_type = 0){

        $system_num = getconfig('system_num');
        //审核通过的违法数据
        $isexamine = getconfig('isexamine');

        $map = ' ';

        if($g_type = 1){
            $group_str = ",tbn_illegal_ad.fmedia_id";
        }

        if(!empty($medias) || !empty($s_date) || !empty($e_date)){
            $map .= " AND tmedia.fid IN (".join(',',$medias).") ";
        }else{
            return [];
        }

        if($fadclass != ''){
            $map .= " AND tbn_illegal_ad.fad_class_code like '$fadclass%' ";
        }
        $join_id = 'fid';
        switch ($media_class){
            case 1:
                $ybb = 'ttvsample';
                $tp_field = ',ybb.favifilepng as image';
                break;
            case 2:
                $ybb = 'tbcsample';
                $tp_field = '';
                break;
            case 3:
                $ybb = 'tpapersample';
                $join_id = 'fpapersampleid';
                $tp_field = ',ybb.fjpgfilename as image';
                break;
        }

        if($media_class != 0){
            $map .= " AND tbn_illegal_ad.fmedia_class = $media_class ";
            $join_str = " INNER JOIN ".$ybb." as ybb ON ybb.".$join_id." = tbn_illegal_ad.fsample_id ";
        }else{
            $tp_field = '';
            $join_str = '';
        }

        if(in_array($isexamine, [10,20,30,40])){
            $map .= " AND tbn_illegal_ad.fexamine = 10 ";
        }

        switch ($range){
            case 1:
                $map .= " AND tregion.fid = $region_id ";
                break;
            case 2:
                $map .= " AND tregion.fpid = $region_id ";
                break;
        }

        $sql = "SELECT
                    (case when instr(tmedia.fmedianame,'（') > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,'（') -1) else tmedia.fmedianame end) as fmedianame,
                    tadclass.ffullname,
                    tbn_illegal_ad.fad_name,
                    COUNT(1) AS fad_illegal_times,               
                    GROUP_CONCAT(DISTINCT tbn_illegal_ad_issue.fissue_date) as fissue_date,
                    tbn_illegal_ad.fexpressions,
                    tregion.fname1 as region_name
                    ".$tp_field."
                FROM
                    `tbn_illegal_ad_issue`
                INNER JOIN tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid
                ".$join_str."
                INNER JOIN tregion ON tregion.fid = tbn_illegal_ad.fregion_id
                INNER JOIN tmedia ON tmedia.fid = tbn_illegal_ad.fmedia_id AND tmedia.fid = tmedia.main_media_id
                INNER JOIN tadclass ON tadclass.fcode = tbn_illegal_ad.fad_class_code
                WHERE
                    tbn_illegal_ad_issue.fissue_date BETWEEN '".$s_date."'
                AND '".$e_date."'
                AND tbn_illegal_ad.fstatus3 NOT IN (30, 40)
                AND tbn_illegal_ad.fcustomer = ".$system_num."
                ".$map."
                GROUP BY
                    tbn_illegal_ad.fad_name".$group_str."
                ORDER BY fad_illegal_times DESC;";

        $res = M("")->query($sql);//AND tbn_illegal_ad_issue.fsend_status = 2

        return $res;

    }


}
