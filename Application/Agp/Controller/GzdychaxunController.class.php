<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 国家局自定义查询
 * by zw
 */

class GzdychaxunController extends BaseController{
  /**
  * 自定义查询结果列表
  * by zw
  */
  public function cxjieguo_list(){
    session_write_close();
    $system_num = getconfig('system_num');
    $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）
    
    $p  = I('page', 1);//当前第几页
    $pp = 20;//每页显示多少记录
    $tadclass     = I('tadclass');//广告类别组
    $mediaclass   = I('mediaclass');//媒体类别组
    $mediaids     = I('mediaids');//媒体id组
    $hstarttime   = I('hstarttime');//发布开始时间，天
    $hendtime     = I('hendtime');//发布结束时间，天
    $sstarttime   = I('sstarttime');//发布开始时间，秒
    $sendtime     = I('sendtime');//发布结束时间，秒
    $tadname      = I('tadname');//广告名称
    $where_tia = '1=1';
    if(!empty($tadclass)){
      $where_tia .= ' and left(b.fad_class_code,2) in('.implode(',', $tadclass).')';
    }
    if(!empty($mediaclass)){
      $where_tia .= ' and b.fmedia_class in('.implode(',', $mediaclass).')';
    }
    if(!empty($mediaids)){
      $where_tia .= ' and b.fmedia_id in('.implode(',', $mediaids).')';
    }

    $where_tia .= ' and b.fid in(select fillegal_ad_id illad_id from tbn_case_send where fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id)';

    if(!empty($tadname)){
      $where_tia .= ' and b.fad_name like "%'.$tadname.'%"';
    }
    if(!empty($hstarttime)||!empty($hendtime)){
      $hstarttime = $hstarttime?$hstarttime:date('Y-m-d');
      $hendtime   = $hendtime?$hendtime:date('Y-m-d');
      $where_tia .= ' and a.fissue_date BETWEEN "'.$hstarttime.'" and "'.$hendtime.'"';
    }
    if(!empty($sstarttime)||!empty($sendtime)){
      $sstarttime = $sstarttime?date('H:i:s',strtotime($sstarttime)):date('H:i:s');
      $sendtime   = $sendtime?date('H:i:s',strtotime($sendtime)):date('H:i:s');
      $where_tia .= ' and a.fstarttime>="'.$sstarttime.'" and a.fstarttime<="'.$sendtime.'"';
    }

    $isrelease = getconfig('isrelease');
    if(!empty($isrelease) || $system_num == '100000'){
        $where_tia   .= ' and fsend_status=2';
    }
    if(!empty($illDistinguishPlatfo)){
      $where_tia .= ' and left(c.fmediaclassid,2) in('.implode(',', $media_class).')';
    }

    $count = M('tbn_illegal_ad_issue')
      ->alias('a')
      ->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')
      ->join('tmedia c on c.fid=a.fmedia_id and c.fid=c.main_media_id')
      ->join('tadclass d on b.fad_class_code=d.fcode')
      ->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on b.fid = snd.illad_id')
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    
    $do_tia = M('tbn_illegal_ad_issue')
      ->alias('a')
      ->field('b.fid,a.fissue_date,b.fad_name,a.fstarttime,a.fendtime,a.fpage,a.fmedia_class,d.ffullname as fadclass, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,b.fillegal_code,b.fillegal,b.fexpressions,b.favifilename,b.fjpgfilename,(UNIX_TIMESTAMP(a.fendtime)-UNIX_TIMESTAMP(a.fstarttime)) as difftime')
      ->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')
      ->join('tmedia c on c.fid=a.fmedia_id and c.fid=c.main_media_id')
      ->join('tadclass d on b.fad_class_code=d.fcode')
      ->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on b.fid = snd.illad_id')
      ->where($where_tia)
      ->page($p,$pp)
      ->select();
  
    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
  }

}