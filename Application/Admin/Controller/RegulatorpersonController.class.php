<?php
//监管人员控制器
namespace Admin\Controller;
use Think\Controller;
class RegulatorpersonController extends BaseController {

    public function index(){
		
		$regulatorpersonList = M('tregulatorperson')->where(array('fpid'=>0))->select();
		//var_dump($regulatorpersonList);
		$this->assign('regulatorpersonList',$regulatorpersonList);
		$this->display();
	}
	
	/*添加、修改组织人员*/
	public function add_edit_regulatorperson(){
		$fid= I('fid');//人员ID
		$fid = intval($fid);//转为数字
		$menu = I('menu');
		$fcustomers = I('fcustomers');//设置所属客户，数组
		
		$a_e_data = array();
		$a_e_data['fregulatorid'] = I('fregulatorid');//机构ID
		$a_e_data['fcode'] = I('fcode');//人员编码
		$a_e_data['fname'] = I('fname');//人员名称
		$a_e_data['fduties'] = I('fduties');//人员职务
		$a_e_data['fmobile'] = I('fmobile');//联系手机
		$a_e_data['fisadmin'] = I('fisadmin')?I('fisadmin'):0;//联系手机
		$a_e_data['fdescribe'] = I('fdescribe');//说明	
		$a_e_data['fstate'] = I('fstate',1);//状态，-1删除，0-无效，1-有效
		$fpassword = I('fpassword')?I('fpassword'):'123456';//密码
		if($menu){
			$a_e_data['special_menu'] = json_encode($menu,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT); //个性化菜单，json
		}
		
		if($fid == 0){//判断是修改还是新增
			if(A('Admin/Authority','Model')->authority('200176') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));
			}
			$is_repeat = M('tregulatorperson')->where(array('fcode'=>$a_e_data['fcode']))->count();//查询人员代码是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'人员代码重复'));//返回人员代码重复
			if(!empty($a_e_data['fmobile'])){
				$is_phone = M('tregulatorperson')->where(array('fmobile'=>$a_e_data['fmobile']))->getField('fcode');//查询绑定手机是否重复
				if(!empty($is_phone)) $this->ajaxReturn(array('code'=>-1,'msg'=>'该手机已被账号'.$is_phone.'绑定'));//返回手机已被其他账号绑定
			}
			$a_e_data['fpassword'] = md5($fpassword);//密码
			$a_e_data['fpassword2'] = md5(md5($fpassword));//密码
			$a_e_data['fcreator'] = '系统最高管理员';//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = '系统最高管理员';//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			$rr = M('tregulatorperson')->add($a_e_data);//添加数据

			$personid = $rr;
			//设置用户所属客户
			$adddata = [];
			$where_pl['fpersonid'] = $personid;
			M('tpersonlabel') -> where($where_pl) ->delete();
			foreach ($fcustomers as $key => $value) {
				$adddata[$key]['fpersonid'] = $personid;
				$adddata[$key]['fcustomer'] = $value;
				$adddata[$key]['fcreator'] = session('personInfo.fid');
				$adddata[$key]['fcreatetime'] = date('Y-m-d H:i:s');
				$adddata[$key]['fstate'] = 1;
			}
			M('tpersonlabel') -> addAll($adddata);

			if($rr > 0){//判断是否添加成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));//返回失败
			}
			
		}else{//修改
			if(A('Admin/Authority','Model')->authority('200177') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));
			}
			$is_repeat = M('tregulatorperson')->where(array('fid'=>array('neq',$fid),'fcode'=>$a_e_data['fcode']))->count();//查询人员代码是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'人员代码重复'));//返回人员代码重复
			if(!empty($a_e_data['fmobile'])){
				$is_phone = M('tregulatorperson')->where(array('fid'=>array('neq',$fid),'fmobile'=>$a_e_data['fmobile']))->getField('fcode');//查询绑定手机是否重复
				if(!empty($is_phone)) $this->ajaxReturn(array('code'=>-1,'msg'=>'该手机已被账号'.$is_phone.'绑定'));//返回手机已被其他账号绑定
			}
			if(!empty($fpassword)){
				$a_e_data['fpassword'] = md5($fpassword);//密码
				$a_e_data['fpassword2'] = md5($a_e_data['fpassword']);//密码
			}
			$a_e_data['fmodifier'] = '系统最高管理员';//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tregulatorperson')->where(array('fid'=>$fid))->save($a_e_data);//修改数据
			if($rr){//清除用户端的登录错误次数缓存
				S('error_count'.$a_e_data['fcode'],0,86400);
			}  
			$personid = $fid;
			//设置用户所属客户
			$adddata = [];
			$where_pl['fpersonid'] = $personid;
			M('tpersonlabel') -> where($where_pl) ->delete();
			foreach ($fcustomers as $key => $value) {
				$adddata[$key]['fpersonid'] = $personid;
				$adddata[$key]['fcustomer'] = $value;
				$adddata[$key]['fcreator'] = session('personInfo.fid');
				$adddata[$key]['fcreatetime'] = date('Y-m-d H:i:s');
				$adddata[$key]['fstate'] = 1;
			}
			M('tpersonlabel') -> addAll($adddata);

			if($rr > 0){//判断是否修改成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功','sql'=>M('tregulatorperson')->getlastsql(),'data'=>$a_e_data['fpassword2'],'data2'=>$a_e_data['fpassword']));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));//返回失败
			}
			
		}
		
		
		
	}
	
	/*加载人员列表*/
	public function ajax_regulatorperson_list(){
		$fregulatorid = I('fregulatorid',0);//机构ID
		$p = I('p',1);//分页的第几页
		$pp = 100;//每页显示多少条记录
		$where = array();
		if($fregulatorid > 0) $where['fregulatorid'] = $fregulatorid;
		$where['fstate'] = array('neq',-1);
		$regulatorpersonList = M('tregulatorperson')->field('fname,fid,fcode,fdescribe,fisadmin,fmobile,fregulatorid,fstate')->where($where)->page($p.','.$pp)->select();//查询人员列表
		$this->ajaxReturn(array('code'=>0,'msg'=>'','regulatorpersonList'=>$regulatorpersonList));
	}
	
	/*人员详情*/
	public function ajax_regulatorperson_details(){
		
		$fid = I('fid');//获取人员ID
		
		$regulatorpersonDetails = M('tregulatorperson')->where(array('fid'=>$fid))->find();
		$regulatorpersonDetails['edit_url'] = U('Admin/Regulatorperson/regulatorperson_details',array('fid'=>$fid));
		$regulatorpersonDetails['personlabel'] = $this->getpersonlabel($fid);
		
		$this->ajaxReturn(array('code'=>0,'regulatorpersonDetails'=>$regulatorpersonDetails));
	}
	
	// /*人员详情*/
	// public function regulatorperson_details(){
		
	// 	$fid = I('fid');//获取人员ID
		
	// 	$regulatorpersonDetails = M('tregulatorperson')->where(array('fid'=>$fid))->find();
	// 	$menu = A('Gongshang/MenuList','Model')->clist();
	// 	$special_menu_s = json_decode($regulatorpersonDetails['special_menu'],true);
		
	// 	foreach($special_menu_s as $special_menu){
	// 		foreach($special_menu['menulist'] as $special){
	// 			$special_menu_list[] = $special['menu_id'];
	// 		}
			
	// 	}

	// 	//菜单权限列表
	// 	$do_nme = M('new_menutype')->field('me_id,me_name,me_state,me_content')->where('me_state=10')->order('me_id asc')->select();//菜单类型列表
	// 	$this->assign('menutypelist',$do_nme);
	// 	$this->assign('regulatorpersonDetails',$regulatorpersonDetails);//人员详情
	// 	$this->assign('menu',$menu);//所有菜单
	// 	$this->assign('special_menu_list',$special_menu_list);//自己拥有菜单

	// 	$this->display();
	// }

	/*删除人员*/
	public function ajax_regulatorperson_del(){
		if(A('Admin/Authority','Model')->authority('200178') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));
		}
		$fid = I('fid');//获取人员ID
		M('tregulatorperson')->where(array('fid'=>$fid))->save(array('fstate'=>-1));
		$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'.$fid));
		
	}

	/**
	 * 当前用户的菜单列表
	 * by zw
	 */
	public function get_personmenulist(){
		if(I('menutype') == ''){
			$_GET['menutype'] = 20;
		}
		$menutype 	= I('menutype');//菜单类型id
		$fid 		= I('fid');//当前用户所属部门或机构id
		$personid 	= I('personid');//用户id
		$fcustomer 	= I('fcustomer');//客户编号

		$treid = D('Function')->get_tregulatoraction($fid);//获取当前单位的所属机构

		$allmenu = D('Menu')->m_alljgmenulist2($menutype,$treid,$fcustomer);//获取当前用户所属机构的所有菜单
		$personmenu = D('Menu')->m_allpersonmenulist($menutype,$personid,$fid,[],$fcustomer);//获取机构用户的菜单权限
		$delmenuid = [];
		foreach ($allmenu as $key => $value) {
			foreach ($value['children'] as $key2 => $value2) {
				$iss = in_array($value2['menu_id'], $personmenu);
				if(empty($iss)){
					$delmenuid[] = $value2['parent_id'];
				}
			}
		}
		$delmenuid = array_unique($delmenuid);
		$personmenu = array_merge(array_diff($personmenu, $delmenuid));
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','menulist'=>$allmenu,'personmenulist'=>$personmenu));
	}

	/**
	 * 更新用户菜单权限
	 * by zw
	 */
	public function updatepersonmenu(){
		$selid 		= I('selid');//选中的项的参数，fmenuid菜单id，fmenupid父id，selstate选中状态1为选中0为不选中
		$fpersonid 	= I('fpersonid');//用户id
		$menutype 	= I('menutype');//菜单类型
		$fcustomer 	= I('fcustomer');//客户编号
		if(is_array($selid)){
			$addarr = [];
			$delarr = [];
			foreach ($selid as $key => $value) {
				if($value['selstate'] == 1){
					$addarr[] = array(
						'fpersonid'		=>$fpersonid,
						'fmenutype'		=>$menutype,
						'fmenuid'		=>$value['fmenuid'],
						'fmenupid'		=>$value['fmenupid'],
						'fcreator'		=>session('personInfo.fid'),
						'fcreatetime'	=>date('Y-m-d H:i:s'),
						'fstate'		=>1,
						'fcustomer'		=>$fcustomer,
					);
				}else{
					$delarr[] = $value['fmenuid'];
				}
			}
			if(!empty($addarr)){
				M('tregulatorpersonmenu')->addAll($addarr);
			}
			if(!empty($delarr)){
				M('tregulatorpersonmenu')->where(array('fmenuid'=>array('in',$delarr),'fpersonid'=>$fpersonid,'fmenutype'=>$menutype,'fcustomer'=>$fcustomer))->delete();
			}
			$this->ajaxReturn(array('code'=>0,'msg'=>'设置成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'设置失败'));
		}
	}

	/**
	 * 添加所属客户的用户媒体权限
	 * by zw
	 */
	public function addpersonmedia(){
		$fcustomer = I('fcustomer');//客户编号
		$fid = I('fid');//用户id
		$medialist 	= I('medialist');//选中的项的参数
		M('tregulatorpersonmedia')->where(array('fpersonid'=>$fid,'fcustomer'=>$fcustomer))->delete();
		if(is_array($medialist)){
			$addarr = [];
			foreach ($medialist as $key => $value) {
				$addarr[] = array(
					'fpersonid'		=>$fid,
					'fmediaid'		=>$value,
					'fcreator'		=>session('personInfo.fid'),
					'fcreatetime'	=>date('Y-m-d H:i:s'),
					'fstate'		=>1,
					'fcustomer'		=>$fcustomer,
				);
			}
			if(!empty($addarr)){
				M('tregulatorpersonmedia')->addAll($addarr);
			}
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'设置成功'));
	}

	/**
	 * 获取用户媒体权限设置
	 * by zw
	 */
	public function getpersonmedia(){
		$fregulatorid 	= I('fregulatorid');//当前用户所属部门或机构id
		$fcustomer 	= I('fcustomer');//客户编号
		$fid 	= I('fid');//用户ID
		$allmedia = D('Media')->m_alljgmedialist($fregulatorid,[],[],$fcustomer);//获取当前用户所属机构的所有媒体
		$personmedia = D('Media')->m_allpersonmedialist($fid,$fcustomer);//获取机构用户的媒体权限
		$this->ajaxReturn(array('code'=>0,'msg'=>'设置成功','data' =>array('medialist' => $allmedia, 'personmedialist' => $personmedia)));
	}

	/**
	 * 获取用户所属客户
	 * by zw
	 */
	public function getpersonlabel($personid){
		$where_pl['fpersonid'] = $personid;
		$where_pl['fstate'] = 1;
		$personlabel = M('tpersonlabel')
			->where($where_pl)
			->getField('fcustomer',true);
		$personlabel = $personlabel?$personlabel:[];
		return $personlabel;
	}
	

}