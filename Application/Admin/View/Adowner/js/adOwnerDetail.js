const adOwnerDetail = {
  template: `<el-form :model="adOwnerForm" ref="adOwnerForm" label-width="120px">
    <el-form-item label="广告主名称：">
      <el-input v-if="isEdit" v-model="adOwnerForm.fname" placeholder=""></el-input>
      <template v-else>
        {{info.fname}}
      </template>
    </el-form-item>
    <el-form-item label="所属行政区划：">
      <el-cascader
        style="width: 100%;"
        v-if="isEdit"
        v-model="adOwnerForm.fregionid"
        :options="regionTree"
        :props="{emitPath: false,checkStrictly: true}"></el-cascader>
      <template v-else>
        {{info.region_name}}
      </template>
    </el-form-item>
    <el-form-item label="广告主地址：">
      <el-input v-if="isEdit" v-model="adOwnerForm.faddress" type="textarea" placeholder=""></el-input>
      <template v-else>
        {{info.faddress}}
      </template>
    </el-form-item>
    <el-form-item label="联系人：">
      <el-input v-if="isEdit" v-model="adOwnerForm.flinkman" placeholder=""></el-input>
      <template v-else>
        {{info.flinkman}}
      </template>
    </el-form-item>
    <el-form-item label="联系电话：">
      <el-input v-if="isEdit" v-model="adOwnerForm.ftel" type="number" placeholder=""></el-input>
      <template v-else>
        {{info.ftel}}
      </template>
    </el-form-item>
    <el-form-item label="联系邮箱：">
      <el-input v-if="isEdit" v-model="adOwnerForm.fmail" placeholder=""></el-input>
      <template v-else>
        {{info.fmail}}
      </template>
    </el-form-item>
    <el-form-item label="备注：">
      <el-input v-if="isEdit" v-model="adOwnerForm.fcomment" type="textarea" placeholder=""></el-input>
      <template v-else>
        {{info.fcomment}}
      </template>
    </el-form-item>
    <el-form-item label="状态：" v-if="isEdit">
      <el-radio-group v-model="adOwnerForm.fstate">
        <el-radio-button label="1">有效</el-radio-button>
        <el-radio-button label="0">无效</el-radio-button>
        <el-radio-button label="-1">删除</el-radio-button>
      </el-radio-group>
    </el-form-item>
    <el-form-item label="创建人：" v-if="!isEdit">
      {{info.fcreator}}
    </el-form-item>
    <el-form-item label="创建时间：" v-if="!isEdit">
      {{info.fcreatetime}}
    </el-form-item>
    <el-form-item label="修改人：" v-if="!isEdit">
      {{info.fmodifier}}
    </el-form-item>
    <el-form-item label="修改时间：" v-if="!isEdit">
      {{info.fmodifytime}}
    </el-form-item>
    <el-form-item>
      <el-button type="primary" @click="isEdit = true" v-if="!isEdit">编 辑</el-button>
      <template v-else>
      <el-button type="danger" plain @click="cancel">取 消</el-button>
        <el-button type="primary" @click="submit">保 存</el-button>
      </template>
    </el-form-item>
  </el-form>`,
  props: {
    info: {
      type: Object,
      require: true
    },
    regionTree: {
      type: Array,
      require: true,
    }
  },
  data() {
    return {
      state: {
        '1': {
          label: '有效'
        },
        '0': {
          label: '无效'
        },
        '-1': {
          label: '删除'
        },
      },
      adOwnerForm: {
        fid: '',
        fname: '',
        fregionid: '',
        faddress: '',
        flinkman: '',
        ftel: '',
        fmail: '',
        fstate: '',
        fcomment: '',
      },
      sourceInfo: {},
      isEdit: false
    }
  },
  watch: {
    isEdit() {
      this.setDialogClose()
    },
    'adOwnerForm.fid': {
      handler() {
        this.setDialogClose()
      },
      deep: true
    }
  },
  created() {
    this.sourceInfo = this.info
    this.initData()
  },
  methods: {
    submit() {
      this.$confirm('确认保存该广告主信息？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        let data = {
          ...this.adOwnerForm
        }
        $fly.post('/Admin/Adowner/saveAdowner', data).then(res => {
          this.isEdit = false
          $h.notify.success(res.msg)
          this.$emit('close', true)
        })
      }).catch(() => {})
    },
    cancel() {
      if (this.adOwnerForm.fid) {
        this.isEdit = false
        this.initData()
      } else {
        this.$emit('close',false)
      }
    },
    initData() {
      Object.keys(this.adOwnerForm).forEach(i => {
        this.adOwnerForm[i] = this.sourceInfo[i] || ''
      })
      this.setDialogClose()
    },
    addAdOwver() {
      this.initData()
      this.adOwnerForm.fstate = '1'
      this.isEdit = true
    },
    setDialogClose() {
      this.$emit('set-dialog-close', !this.isEdit)
    },
  },
}