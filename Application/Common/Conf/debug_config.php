<?php

/*该配置文件用于开发、调试环境*/
$config = array(

	/* 数据库设置 */

	'DB_DEPLOY_TYPE' 		=> 	1, // 设置分布式数据库支持
	'DB_RW_SEPARATE' 		=> 	true, // 分布式数据库的读写是否分离
    'DB_TYPE'               =>  'mysql',     // 数据库类型

	'DB_HOST'               =>  'rm-bp1093q2v2sn6n6lg3o.mysql.rds.aliyuncs.com', // 服务器地址
	'DB_NAME'               =>  'hzsj_dmp_20190311',          // 数据库名
	'DB_USER'               =>  'hzsj_dmp',      // 用户名
	'DB_PWD'                =>  '471158226933abcD',          // 密码




	'DB_PORT'               =>  '3306',        // 端口

	'DB_PARAMS'    			=>	array(\PDO::ATTR_CASE => \PDO::CASE_NATURAL),
	'DB_DEBUG'  			=>  true, // 数据库调试模式 开启后可以记录SQL日志




	//'M3U8_DB'				=>	'',//M3U8_DB数据库

	//Redis配置
	'REDIS_HOST' 			=>	'47.99.42.90',//redis服务器地址
	'REDIS_PORT'			=>	'5510',//端口号
	'REDIS_AUTH'			=>	'aCl;T8Q-AoVDgBj^u3yP9j',//密码
	'REDIS_DB'				=>	4,//使用哪一个DB

	'DMP_VERSION'			=>	'数据管理平台 V2.4(开发版)',


	//百度地图配置
	'BAIDU_MAP_AK'			=>	'EWTi9tm6nopG1CfIFkP6T5i1Lj6uvMp7',//百度地图的ak
	'BAIDU_MAP_ZOOM'		=>	5,//地图的缩放级别
	'BAIDU_MAP_CITY'		=>	'中国',//地图展示的省份

    //URL模式
    'URL_ROUTER_ON'   => true, //开启路由
    'URL_MODEL'             =>  2,       // URL访问模式
    'URL_HTML_SUFFIX'       =>  '',  // URL伪静态后缀设置
    //'SHOW_PAGE_TRACE' =>true,
	

	
	/*七牛存储配置*/
	'QINIU_BUCKET'			=>	'datamanagerplatform',
	'QINIU_ACCESSKEY'		=>	'tWVF9H1g6cB3aDkahHsBCJDbOTfM7UTviKhHjq5k',
	'QINIU_SECRETKEY'		=>	'gO26avOKMT1vHyXVuL7r0ZISjD9b2tRB2rPDNET9',
	'QINIU_ENDPOINT'		=>	'http://datamanagerplatform.finesoft.com.cn/',
	'QINIU_UP_URL'			=>	'http://upload.qiniup.com/',
	
	
	//阿里云存储配置
	'AL_OSSID' 				=> 'LTAIEZJAlfFoaNtB',   //oss id
	'AL_OSSKEY' 			=> 'inkCxLTxWsiIODs7MX7gvkatrzUWme',   // oss key
	'ENDPOINT'				=> 'http://oss-cn-hangzhou.aliyuncs.com/',
	'AL_OSSBUCKET_URL' 		=> 'http://ddmmpp.oss-cn-hangzhou.aliyuncs.com/',
	'AL_OSSBUCKET'			=> 'ddmmpp',//阿里云oss的bucket
	'SMS_SIGNNAME'			=> '华治数聚',
	
	//'SELF_M3U8'				=> true,//使用自有m3u8
	
	
	//配置上传的云,目前兼容七牛和阿里
	'MEMORY_YUN'			=>	'QINIU',//    阿里云:ALI,七牛云:QINIU				请使用大写
	
	
	//缓存配置
	'DATA_CACHE_TYPE'		=>	'Redis',//设置缓存方式为Redis   
	'DATA_CACHE_TIME'		=>	7200,//缓存有效期
	

	//百度地图配置
	'BAIDU_MAP_AK'			=>	'EWTi9tm6nopG1CfIFkP6T5i1Lj6uvMp7',//百度地图的ak
	'BAIDU_MAP_ZOOM'		=>	6,//地图的缩放级别
	'BAIDU_MAP_CITY'		=>	'中国',//地图展示的省份

	
	//拾联直播参数配置
	'SHILIAN_SERVER'		=>	'shilian2.hz-data.xyz',//拾联直播服务器地址
	
	'SHILIAN_API_PORT'		=>	18081,//拾联API服务器端口号
	'SHILIAN_ACCOUNT'		=>	'hzsj',//拾联平台账号
	
	'SHILIAN_LIVE_PORT'		=>	39100,//拾联直播服务器端口号


	//'SHILIAN_LIVE_AUTH'		=>	'czFYScb5pAu+Ze7rXhGh/8RxwafVbYWzykybiYXfmlUG5hUR8MkXH/MBaOy+I6hqSFLfYeF/Q/NN6Esb0OglteQwBUedwdmX',//拾联直播授权码
	'SHILIAN_LIVE_AUTH'		=>	'czFYScb5pAu+Ze7rXhGh/+xBIu94o43TykybiYXfmlUG5hUR8MkXH/MBaOy+I6hqSFLfYeF/Q/NN6Esb0OglteQwBUedwdmX',//拾联直播授权码
	
	
	//'SHILIAN_AUTHORIZATION'	=>	'8b7c961c-45d5-11e7-be85-fa163e6c6171',//拾联接口用到的AUTHORIZATION	
	'SHILIAN_AUTHORIZATION'	=>	'6f809d2a-8e9d-11e8-817b-00163e0c900c',//拾联接口用到的AUTHORIZATION		
	
	
	
	'GONGSHANG_ALLOW_LOGIN_ERROR_COUNT' => 5,//工商登录允许错误次数
	
	
	//视频剪辑服务配置

	'ISSUE_CUT_URL'				=>	'http://118.31.51.31/cut_mp4/index.php',//视频剪辑服务地址
	'RAR_FILE_URL'				=>	'http://118.31.51.31/rar_file/index.php',//文件压缩服务地址
	
	
	
	//监测报告
	'REPORT_MAKE_URL'			=>	'http://61.190.254.13:8081/report',//生成监测报告的URL
	'REPORT_URL'			    =>	'http://61.190.254.13:8082/report',//生成监测报告的URL
	
	
	//公共资源
	'PUBLIC_RESOURCE'			=>	'http://publichz.oss-cn-hangzhou.aliyuncs.com/',//公共资源域名
	
	'AI_DOMAIN'		=>	'http://116.62.79.116/',//ai服务器地址
	
	
	
	
	'WX_TOKEN'					=>  '111',
    'WX_APPID'					=>  'wxbe112552649d0c57',// 微信公众帐号APPID
    'WX_APPSECRET' 			    =>  '8823673a8f070149ea2ca878e5a38030',// 微信公众帐号secret
	'WX_QRCODE_URL'				=>	'http://mmbiz.qpic.cn/mmbiz_jpg/wDv99WVVVrExomZktD0nWuLPqlW7oucbYTNDjR0x2EPqoTfdRYOfIDdUOHlUzCibnbZ7OS9r8vSx9IotQiaPZj9w/0',//公众号的二维码地址
	'WX_LOGIN_M'				=>	'20ZEmMEdnU9yZecP5ppPUcYVTeu_0QZ1XjyiKseigYw',//微信登录模板消息
	'WX_BACK_M'					=>	'l_tM6tYjBW1olYAODnUDsRXxZADA5RZ8amrbb7kwMQA',//微信退回模板消息
	'WX_VERIFY_M'				=>	'eII4PU7FFTmbQbrTSt_Sp9AZalDMp4pF4w0Mc3u_OYw',//微信验证码

	
	'OTS_URL'					=> 	'http://hzsjdmp.cn-hangzhou.ots.aliyuncs.com',

	'ALI_LOG_ENDPOINT'			=>	'cn-hangzhou.sls.aliyuncs.com',
	
	'OPEN_SEARCH_URL'			=> 'http://opensearch-cn-hangzhou.aliyuncs.com',
	'OPEN_SEARCH_CUSTOMER'		=> 'hzsj_dmp_ad',
	'ZIZHI_DB'					=> 'mysql://hzsj_dmp2_rw:pBbp1Y9m8T8q2Z50ekx@hzsjrw.rwlb.rds.aliyuncs.com:3306/hzsj_dmp2',
	'IS_TEST'					=> true,
	
);


return $config;