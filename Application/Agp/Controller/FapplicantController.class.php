<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 存公证模块
 * by zw
 */

class FapplicantController extends BaseController {

	/**
     * 接收参数
     */
    protected $P;

    /**
     * 初始化
     */
    public function _initialize(){
        parent::_initialize();
        $this->P = json_decode(file_get_contents('php://input'),true);
    }

     /******************存证管理*********************/
    /**
     * 存证列表
     * by zw
     */
    public function getEvidenceList(){
    	$system_num = getconfig('system_num');
        $fevidenceid = $this->P['fevidenceid'];
        $fstarttime  = $this->P['fstarttime'];
        $fendtime    = $this->P['fendtime'];
        $status      = $this->P['status'];
        $title      = $this->P['title'];
        $pageIndex   = $this->P['pageIndex'] ? $this->P['pageIndex'] : 1;
        $pageSize    = $this->P['pageSize'] ? $this->P['pageSize'] : 1000;
        $limitIndex  = ($pageIndex-1)*$pageSize;
        $where = [];
        $where['b.fusertrepid'] = session('regulatorpersonInfo.fregulatorpid');
        if(!empty($fevidenceid)){
            $where['a.rid'] = ['like','%'.$fevidenceid.'%'];
        }
        if($status != ''){
            $where['a.status'] = $status;
        }else{
        	$where['a.status'] = ['in',[0,1]];
        }
        if(!empty($title)){
            $where['a.title'] = ['like','%'.$title.'%'];
        }
        $where['b.fstatus'] = 1;
        $where['b.fcustomer'] = $system_num;
        if(!empty($fstarttime) && !empty($fendtime)){
	        $where['b.fcreatetime'] = ['BETWEEN',[date('Y-m-d 00:00:00',strtotime($fstarttime)),date('Y-m-d 23:59:59',strtotime($fendtime))]];
        }
        $count = M('tb_evidence')
        	->alias('a')
        	->join('tb_evidence_user b on a.rid = b.fevidencerid')
        	->where($where)
        	->count();

        $dataSet = M('tb_evidence')
            ->field('a.id,a.title,a.url,a.rid,a.md5,a.screenshot,b.fcreatetime cdate,a.status,ifnull(ccount,0) ccount')
        	->alias('a')
            ->join('tb_evidence_user b on a.rid = b.fevidencerid')
            ->join('(select fevidenceid,count(*) ccount from tb_applicant_evis where fuserid ='.session('regulatorpersonInfo.fid').' and fapplicantid = 0 group by fevidenceid) c on a.id = c.fevidenceid','left')
            ->where($where)
            ->order('a.status desc,b.fcreatetime DESC')
            ->limit($limitIndex,$pageSize)
            ->select();
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','count'=>$count,'data'=>$dataSet]);
    }

	/**
     * 新增存证
     * by zw
     */
    public function addEvidence(){
    	$system_num = getconfig('system_num');
        $eviUrl = $this->P['url'];
        $eviTitle = $this->P['title'];
        if(!empty($eviUrl) && !empty($eviTitle)){
            $resInfo = A('Open/Evidence','Model')->addEvidence($eviUrl,$eviTitle,0,0);//存证请求
            
            if(empty($resInfo['code'])){
	            $dataSet = [
	                'fevidencerid' => $resInfo['data']['rid'],
	                'fuser' => session('regulatorpersonInfo.fname'),
	                'fuserid' => session('regulatorpersonInfo.fid'),
	                'fusertrepid' => session('regulatorpersonInfo.fregulatorpid'),
	                'fcreatetime' => date('Y-m-d H:i:s'),
	                'fstatus' => 1,
	                'fcustomer' => $system_num,
	            ];
            	$result = M('tb_evidence_user')->add($dataSet);
                $this->ajaxReturn(['code'=>0,'msg'=>'新增成功']);

            }else{
                $this->ajaxReturn(['code'=>1,'msg'=>'新增失败，请尝试重新增加']);
            }
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 线索或手动存证信息转入公证库
     * by zw
     */
    public function goApplicant(){
    	$id = $this->P['id'];
    	$sampleid = $this->P['sampleid'];
    	$mclass = $this->P['mclass'];
    	if(!empty($id) || !empty($sampleid)){
    		if(!empty($sampleid)){
                if((int)$mclass < 10){
                    $mclass = '0'.$mclass;
                }
    			$id = M('tb_evidence_sam')
    				->alias('a')
    				->join('tb_evidence b on a.frid = b.rid')
    				->where(['a.fmediaclass'=>$mclass,'a.fsample_id'=>$sampleid,'b.status'=>1])
    				->getField('id');
    			if(empty($id)){
    				$this->ajaxReturn(['code'=>1,'msg'=>'线索存证未完成，不允许转入，请稍等片刻后重新尝试']);
    			}
    		}

    		$do_aes = M('tb_applicant_evis')->where(['fevidenceid'=>$id,'fuserid'=>session('regulatorpersonInfo.fid'),'fapplicantid'=>0])->count();
    		if(!empty($do_aes)){
    			$this->ajaxReturn(['code'=>1,'msg'=>'转入失败，该存证信息已存在于待公证记录中']);
    		}

    		$ftype = empty($sampleid)?1:2;
    		$addData = [
	    		'ftype' => $ftype,
	    		'fevidenceid' => $id,
	    		'fuserid' => session('regulatorpersonInfo.fid'),
	    		'fcreatetime' => date('Y-m-d H:i:s'),
	    	];

	    	$do_aes = M('tb_applicant_evis')->add($addData);
	    	if(!empty($do_aes)){
	    		$this->ajaxReturn(['code'=>0,'msg'=>'转入成功，请前往公证管理的待公证标签中检查']);
	    	}else{
	    		$this->ajaxReturn(['code'=>1,'msg'=>'转入失败，请刷新页面后尝试重新转入']);
	    	}
    	}else{
      		$this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
      	}
    	
    }

    /**
     * 删除存证
     * by zw
     */
    public function delEvidence_user(){
    	$system_num = getconfig('system_num');
    	$id = $this->P['id'];
    	if(empty($id)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数错误']);
        }
        $fevidencerid = M('tb_evidence')->where(['id'=>$id])->getField('rid');
    	$trepid = session('regulatorpersonInfo.fregulatorpid');
        $where = [
            'fevidencerid' => $fevidencerid,
            'fusertrepid' => $trepid,
            'fcustomer' => $system_num
        ];
        $del_ee = M('tb_evidence_user')
            ->where($where)
            ->save(['fstatus'=>-1]);
      	if(!empty($del_ee)){
	        $this->ajaxReturn(['code'=>0,'msg'=>'删除成功']);
      	}else{
      		$this->ajaxReturn(['code'=>1,'msg'=>'删除失败']);
      	}
    }

    /******************公证管理*********************/
    /**
     * 删除待公证
     * by zw
     */
    public function delEvidence(){
        $id = $this->P['id'];
        if(empty($id)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数错误']);
        }
        $where = [
            'fevidenceid' => $id,
            'fapplicantid' => 0
        ];
        $del_ee = M('tb_applicant_evis')
            ->where($where)
            ->delete();
        if(!empty($del_ee)){
            $this->ajaxReturn(['code'=>0,'msg'=>'删除成功']);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'删除失败']);
        }
    }

    /**
     * 公证提交
     * by zw
     */
    public function applyEvidence(){
    	$notarizationfee = json_decode(getconfig('notarizationfee'));//公证基础费用和另增费用
    	$fuuids = $this->P['fuuids'];//需要公证的存证关联记录，数组
    	$do_aes = M('tb_applicant_evis')->where(['fuserid'=>session('regulatorpersonInfo.fid'),'fapplicantid'=>0,'fevidenceid'=>['in',$fuuids]])->select();
    	if(count($do_aes)!=count($fuuids) || empty(count($fuuids))){
    		$this->ajaxReturn(['code'=>1,'msg'=>'所选择的公证信息有误，请重新选择']);
    	}

    	//公证费计算验证
    	if(count($do_aes)>$notarizationfee[3]){
    		$this->ajaxReturn(['code'=>1,'msg'=>'公证数据不得超过'.$notarizationfee[3].'个']);
    	}
    	$ewmoney = (count($do_aes)-1)*$notarizationfee[1];
    	$ewmoney = $ewmoney>$notarizationfee[2]?$notarizationfee[2]:$ewmoney;
    	$fmoney = $notarizationfee[0]+$ewmoney;
    	if((float)$fmoney != (float)$this->P['fmoney']){
    		$this->ajaxReturn(['code'=>1,'msg'=>'公证费计算出错，请重新选择']);
    	}

        $data = [
            'fnotarialtype'   => $this->P['fnotarialtype'],//公证书类型（0网页，1文件，2语音，3视频）
            'ftype'           => $this->P['ftype'],//公证类型（0企业，1个人）
            'fnotarials'      => $this->P['fnotarials'],//公证书份数
            'fispost'         => $this->P['fispost'],//是否邮寄 0邮寄 1不邮寄
            'fusage'          => $this->P['fusage'],//用途
            'fentname'        => $this->P['fentname'],//企业名称
            'fcreditid'       => $this->P['fcreditid'],//统一社会信用代码
            'fenturl'         => $this->P['fenturl'],//营业执照照片名
            'fegalperson'     => $this->P['fegalperson'],//法定代表人
            'fegalidcard'     => $this->P['fegalidcard'],//法人身份证号码
            'fegalidcardurl'  => $this->P['fegalidcardurl'],//法人身份证照片正面
            'fegalidcardurl2' => $this->P['fegalidcardurl2'],//法人身份证照片反面
            'fusername'       => $this->P['fusername'],//申办人/代办人姓名
            'fmobile'         => $this->P['fmobile'],//申办人/代办人手机号码
            'fidcard'         => $this->P['fidcard'],//申办人/代办人身份证码
            'fidcardurl'      => $this->P['fidcardurl'],//申办人/代办人身份证正面照片名
            'fidcardurl2'     => $this->P['fidcardurl2'],//申办人/代办人身份证反面照片名
            'fidcardfeelurl'  => $this->P['fidcardfeelurl'],//申办人/代办人手持身份证照片名
            'fposition'       => $this->P['fposition'],//代办人职务（类型为企业且为代办时需要填写）
            'fmandateurl'     => $this->P['fmandateurl'],//代办人委托书URL
            'fpostusername'   => $this->P['fpostusername'],//收件人
            'fpostmobile'     => $this->P['fpostmobile'],//收件人手机
            'fpostaddress'    => $this->P['fpostaddress'],//邮寄地址
            'fremark'         => $this->P['fremark'],//备注
            'fmoney'          => (float)$this->P['fmoney'],//公证费报价
            'fzkmoney'         => (float)$this->P['fmoney'],//实际公证费
            'fuserid'		  => session('regulatorpersonInfo.fid'),
            'fcreatetime'     => date('Y-m-d H:i:s'),
            'flog'	         => session('regulatorpersonInfo.fname')."（用户：".session('regulatorpersonInfo.fid')."）于".date('Y-m-d H:i:s')."提交公证申请；",
        ];

        //公共必要参数
        if(empty($data['fnotarials'])||empty($data['fusage'])||empty($data['fusername'])||empty($data['fmobile'])||empty($data['fidcard'])||empty($data['fidcardurl'])||empty($data['fidcardurl2'])||empty($data['fidcardfeelurl'])){
        	$this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
        //选择性必要参数
        if((empty($data['fentname'])||empty($data['fcreditid'])||empty($data['fenturl'])||empty($data['fegalperson'])||empty($data['fegalidcard'])||empty($data['fegalidcardurl'])||empty($data['fegalidcardurl2'])||empty($data['fposition'])||empty($data['fmandateurl'])) && empty($data['ftype'])){//企业
        	$this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
        if(empty($data['fispost']) && (empty($data['fpostaddress']) || empty($data['fpostusername']) || empty($data['fpostmobile']))){
        	$this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
        
        $fapplicantid = M('tb_applicant')->add($data);
        if(!empty($fapplicantid)){
        	M('tb_applicant_evis')->where(['fuserid'=>session('regulatorpersonInfo.fid'),'fapplicantid'=>0,'fevidenceid'=>['in',$fuuids]])->save(['fapplicantid'=>$fapplicantid]);
        	$this->ajaxReturn(['code'=>0,'msg'=>'提交成功']);
        }else{
        	$this->ajaxReturn(['code'=>1,'msg'=>'提交失败']);
        }
    }

    /**
     * 待公证列表
     * by zw
     */
    public function applyEvidenceList(){
    	//用户存证的数据
        $where['a.fuserid'] = session('regulatorpersonInfo.fid');
       	$where['b.status'] = 1;
       	$where['a.fapplicantid'] = 0;
        $dataSet = M('tb_applicant_evis')
            ->field('a.fuuids,b.id,b.title,b.url,b.rid,b.md5,b.screenshot,b.cdate,b.status')
            ->alias('a')
            ->join('tb_evidence b on a.fevidenceid = b.id')
            ->where($where)
            ->order('a.fcreatetime DESC')
            ->select();
        $this->ajaxReturn(['code'=>0,'msg'=>'成功','count'=>count($dataSet),'data'=>$dataSet]);
    }

    /**
     * 已公证订单列表
     * by zw
     */
    public function finishEvidenceList(){
    	$fstatus = $this->P['fstatus'];
    	$pageIndex   = $this->P['pageIndex'] ? $this->P['pageIndex'] : 1;
        $pageSize    = $this->P['pageSize'] ? $this->P['pageSize'] : 1000;
        $limitIndex  = ($pageIndex-1)*$pageSize;

        $where['fuserid'] = session('regulatorpersonInfo.fid');
        if(!empty($fstatus)){
	       	$where['fstatus'] = $fstatus;
        }

        $count = M('tb_applicant')
        	->where($where)
        	->count();

        $dataSet = M('tb_applicant')
            ->field('fapplicantid,ftype,fnotarials,fispost,fusage,fentname,fcreditid,fenturl,fegalperson,fegalidcard,fegalidcardurl,fegalidcardurl2,fusername,fmobile,fidcard,fidcardurl,fidcardurl2,fidcardfeelurl,fposition,fmandateurl,fpostusername,fpostmobile,fpostaddress,fremark,fstatus,fcreatetime,fmoney,fzkmoney')
            ->where($where)
            ->order('fstatus asc,fcreatetime desc')
            ->limit($limitIndex,$pageSize)
            ->select();
        $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$dataSet]);
    }

    /**
     * 公证订单详情
     * by zw
     */
    public function evidenceView(){
        $fapplicantid = $this->P['fapplicantid'];//公证主键
        if(empty($fapplicantid)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数错误']);
        }

        $where['fapplicantid'] = $fapplicantid;
        $where['fuserid'] = session('regulatorpersonInfo.fid');
        $dataSet = M('tb_applicant')
            ->field('fapplicantid,fnotarialtype,ftype,fnotarials,fispost,fusage,fentname,fcreditid,fenturl,fegalperson,fegalidcard,fegalidcardurl,fegalidcardurl2,fusername,fmobile,fidcard,fidcardurl,fidcardurl2,fidcardfeelurl,fposition,fmandateurl,fpostusername,fpostmobile,fpostaddress,fremark,fstatus,fcreatetime,fmoney,fzkmoney,flog')
            ->where($where)
            ->find();

        if(!empty($dataSet)){
            $whereEE['a.fapplicantid'] = $dataSet['fapplicantid'];
            $dataEE = M('tb_applicant_evis')
                ->alias('a')
                ->field('b.*')
                ->join('tb_evidence b on a.fevidenceid = b.id')
                ->where($whereEE)
                ->select();
            $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$dataSet,'data2'=>$dataEE]);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'信息不存在']);
        }
    }

    /**
     * 公证确认完成
     * by zw
     */
    public function evidenceCheckFinish(){
        $fapplicantid = $this->P['fapplicantid'];//公证主键
        if(empty($fapplicantid)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数错误']);
        }
        $log = session('regulatorpersonInfo.fname')."（用户：".session('regulatorpersonInfo.fid')."）于".date('Y-m-d H:i:s')."将公证申请确认完成；";
        $save_at = M('tb_applicant')->where(['fapplicantid'=>$fapplicantid,'fstatus'=>1,'fuserid'=>session('regulatorpersonInfo.fid')])->save(['fstatus'=>2,'flog'=>['exp','concat(flog,"'.$log.'")']]);
        if(!empty($save_at)){
            $this->ajaxReturn(['code'=>0,'msg'=>'确认完成']);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'信息不存在，请刷新后重试']);
        }

    }

     /**
     * 企业信息列表
     * by zw
     */
   	public function enterpriseList(){
   		$do_eio = M('tb_ent_info')->where(['fhuserid'=>session('regulatorpersonInfo.fid'),'fhstatus'=>1])->order('fid desc')->select();

		$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$do_eio]);
   	}

    /**
     * 企业信息查看
     * by zw
     */
   	public function enterpriseView(){
   		$fid = $this->P['fid'];
   		if(empty($fid)){
   			$this->ajaxReturn(['code'=>1,'msg'=>'参数错误']);
   		}
   		$view_eio = M('tb_ent_info')->where(['fhuserid'=>session('regulatorpersonInfo.fid'),'fid'=>$fid])->find();

   		if(!empty($view_eio)){
   			$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$view_eio]);
   		}else{
   			$this->ajaxReturn(['code'=>1,'msg'=>'获取失败，信息不存在']);
   		}
   	}

   	/**
     * 企业信息删除
     * by zw
     */
   	public function enterpriseDel(){
   		$fid = $this->P['fid'];
   		if(empty($fid)){
   			$this->ajaxReturn(['code'=>1,'msg'=>'参数错误']);
   		}
   		$del_eio = M('tb_ent_info')->where(['fhuserid'=>session('regulatorpersonInfo.fid'),'fid'=>$fid,'fhstatus'=>['in',[0,1]]])->save(['fhstatus'=>-1]);

   		if(!empty($del_eio)){
   			$this->ajaxReturn(['code'=>0,'msg'=>'删除成功']);
   		}else{
   			$this->ajaxReturn(['code'=>1,'msg'=>'删除失败，信息不存在']);
   		}
   	}

    /**
     * 企业信息添加修改管理
     * by zw
     */
   	public function enterpriseModify(){
   		$fid = $this->P['fid'];

   		$data = [
            'fentname' => $this->P['fentname'],//机构名称
            'fcreditid' => $this->P['fcreditid'],//统一社会信用代码
            'flerep' => $this->P['flerep'],//负责人
            'fenturl' => $this->P['fenturl'],//营业执照照片地址
            'fidcard' => $this->P['fidcard'],//负责人身份证号码
            'fidcardurl' => $this->P['fidcardurl'],//负责人身份证照片正面
            'fidcardurl2' => $this->P['fidcardurl2'],//负责人身份证照片反面
            'fupdatetime' => date('Y-m-d H:i:s'),
            'fhuserid' => session('regulatorpersonInfo.fid'),
            'fhuser' => session('regulatorpersonInfo.fname'),
            'fhstatus' => 1,
        ];

        //验证必填项
    	if(empty($data['fidcard']) || empty($data['fidcardurl']) || empty($data['fidcardurl2']) || empty($data['fentname']) || empty($data['fcreditid']) || empty($data['flerep']) || empty($data['fenturl'])){
    		$this->ajaxReturn(['code'=>1,'msg'=>'不允许提交，请将必填项补充完整']);
    	}

    	if(!empty($fid)){
    		M('tb_ent_info')->where(['fhuserid'=>session('regulatorpersonInfo.fid'),'fid'=>$fid])->save($data);
    		$this->ajaxReturn(['code'=>0,'msg'=>'已保存']);
    	}else{
	    	M('tb_ent_info')->add($data);
	    	$this->ajaxReturn(['code'=>0,'msg'=>'已添加']);
    	}
   	}

    /**
     * 个人信息列表
     * by zw
     */
   	public function personList(){
   		$ftype = $this->P['ftype'];
   		if(empty($ftype)){
   			$this->ajaxReturn(['code'=>1,'msg'=>'参数错误']);
   		}

   		$do_pn = M('tb_person')->where(['fuserid'=>session('regulatorpersonInfo.fid'),'ftype'=>$ftype,'fstatus'=>1])->order('fid desc')->select();

		$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$do_pn]);
   	}

    /**
     * 个人信息查看
     * by zw
     */
   	public function personView(){
   		$fid = $this->P['fid'];
   		if(empty($fid)){
   			$this->ajaxReturn(['code'=>1,'msg'=>'参数错误']);
   		}
   		$view_pn = M('tb_person')->where(['fuserid'=>session('regulatorpersonInfo.fid'),'fid'=>$fid])->find();

   		if(!empty($view_pn)){
   			$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$view_pn]);
   		}else{
   			$this->ajaxReturn(['code'=>1,'msg'=>'获取失败，信息不存在']);
   		}
   	}

   	/**
     * 个人信息删除
     * by zw
     */
   	public function personDel(){
   		$fid = $this->P['fid'];
   		if(empty($fid)){
   			$this->ajaxReturn(['code'=>1,'msg'=>'参数错误']);
   		}
   		$del_pn = M('tb_person')->where(['fuserid'=>session('regulatorpersonInfo.fid'),'fid'=>$fid,'fstatus'=>['in',[0,1]]])->save(['fstatus'=>-1]);

   		if(!empty($del_pn)){
   			$this->ajaxReturn(['code'=>0,'msg'=>'删除成功']);
   		}else{
   			$this->ajaxReturn(['code'=>1,'msg'=>'删除失败，信息不存在']);
   		}
   	}

    /**
     * 个人信息添加修改管理
     * by zw
     */
   	public function personModify(){
   		$ftype = $this->P['ftype'];
   		$fid = $this->P['fid'];
   		if(empty($ftype)){
   			$this->ajaxReturn(['code'=>1,'msg'=>'参数错误']);
   		}

   		$data = [
            'ftype' => $this->P['ftype'],//用户类别
            'fname' => $this->P['fname'],//用户名称
            'fmobile' => $this->P['fmobile'],//手机号
            'fidcard' => $this->P['fidcard'],//身份证号
            'fidcardurl' => $this->P['fidcardurl'],//身份证正面图片
            'fidcardurl2' => $this->P['fidcardurl2'],//身份证反面图片
            'fidcardfeelurl' => $this->P['fidcardfeelurl'],//手持身份证图片
            'fposition' => $this->P['fposition'],//职务
            'faddress' => $this->P['faddress'],//地址
            'fnote' => $this->P['fnote'],//备注
            'fcreatetime' => date('Y-m-d H:i:s'),
            'fuserid' => session('regulatorpersonInfo.fid'),
            'fusername' => session('regulatorpersonInfo.fname'),
        ];

        //验证必填项
    	if($ftype == 1 && (empty($data['fidcard']) || empty($data['fidcardurl']) || empty($data['fidcardurl2']) || empty($data['fidcardfeelurl']))){
    		$errormsg = 1;
    	}elseif($ftype == 2 && (empty($data['fposition']) || empty($data['fidcard']) || empty($data['fidcardurl']) || empty($data['fidcardurl2']) || empty($data['fidcardfeelurl']))){
    		$errormsg = 1;
    	}elseif($ftype == 3 && empty($data['faddress'])){
    		$errormsg = 1;
    	}elseif(empty($data['fname']) || empty($data['fmobile'])){
    		$errormsg = 1;
    	}
    	if(!empty($errormsg)){
    		$this->ajaxReturn(['code'=>1,'msg'=>'不允许提交，请将必填项补充完整']);
    	}

    	if(!empty($fid)){
    		$data['fmodifytime'] = date('Y-m-d H:i:s');
    		$data['fmodifyuserid'] = session('regulatorpersonInfo.fid');
    		$data['fmodifyusername'] = session('regulatorpersonInfo.fname');
    		M('tb_person')->where(['fuserid'=>session('regulatorpersonInfo.fid'),'fid'=>$fid])->save($data);
    		$this->ajaxReturn(['code'=>0,'msg'=>'已保存']);
    	}else{
    		$data['fcreatetime'] = date('Y-m-d H:i:s');
    		$data['fuserid'] = session('regulatorpersonInfo.fid');
    		$data['fusername'] = session('regulatorpersonInfo.fname');
	    	M('tb_person')->add($data);
	    	$this->ajaxReturn(['code'=>0,'msg'=>'已添加']);
    	}
   	}

}