Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}
const vm = new Vue({
  el: '#app',
  // components: { commonHeader },
  data() {
    return {
      form: {
        p: 1,
        pp: 20,
        proname: '',
        platform: '',
        depart_province: '',
        destination: '',
        days: '',
        is_low_price: '',
        visual_checked:'',
      },
      sourceform: '',
      tableData: [],
      fids: '',
      allcount: 0,
      loading: false,
      platformList: platformList,
      isLowPrice: {
        '0': '不涉嫌',
        '1': '涉嫌低价',
        '2': '涉嫌不合理低价',
      },
      visualChecked: {
        '0': '未检查',
        '1': '已检查',
      },
      timer: null,
    }
  },
  created() {
    this.sourceform = JSON.stringify(this.form)
    this.getList(1)
  },
  methods: {
    getList(page) {
      if(page) this.form.p = page
      let data = {
        ...this.form
      }
      this.loading = true
      $.post('/TaskInput/Lvyou/data_list', data)
        .then(res => {
          if (res.code == 0) {
            let r = res.dataList
            this.tableData = r
            this.fids = r.map(i => i.fid).join(',')
            this.allcount = res.dataCount
            this.loading = false
            if (this.timer === null) {
              this.timer = setInterval(() => {
                this.getStatus()
              },3000)
            }
          }
      }).catch(e => console.log(e))
    },
    getStatus() {
      $.post('/TaskInput/Lvyou/get_visual_checked', { fids: this.fids })
        .then(res => {
          if (res.code === 0) {
            res.visual_check_list.forEach((i, index) => {
              this.$set(this.tableData[index], 'visual_checked', i.visual_checked)
            })
          }
      })
    },
    resetForm() {
      this.form = JSON.parse(this.sourceform)
      this.getList(1)
    },
  },
})