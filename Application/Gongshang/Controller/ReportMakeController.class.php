<?php
//监测报告生成
namespace Gongshang\Controller;
use Think\Controller;
import('Vendor.PHPExcel');
class ReportMakeController extends BaseController {

	/*监测日报、周报、月报、年报*/
	public function report01(){
		set_time_limit(0);
		
		$regulatorpersonInfo = M('tregulatorperson')->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
		$regulatorInfo = M('tregulator')->where(array('fcode'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息
		$regulatorRegionInfo = M('tregion')->field('fid,fname')->where(array('fid'=>$regulatorInfo['fregionid']))->find();//监管地区信息信息
		session_write_close();//停止使用session
		
		$report_name = I('report_name');//报告名称
		
		$report_type = intval(I('report_type'));//报告类型
		if($report_type === 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'请选择报告类型'));

		if($report_type == 0){//默认报告
			
		}elseif($report_type == 1){//日报
			$report_day = I('report_day');//报告日
			if($report_day == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'请选择您要生成哪一天的日报'));
			if($report_name == '') $report_name = $regulatorInfo['fname'].' '. date('Y年m月d日',strtotime($report_day)).' 监测日报';
			$start_date = date('Y年m月d日',strtotime($report_day));//监测开始时间
			$end_date = date('Y年m月d日',strtotime($report_day));//监测结束时间
		}elseif($report_type == 2){//周报
			$report_week = I('report_week');//报告周
			if($report_week == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'请选择您要生成哪一周的周报'));
			if($report_name == '') $report_name = $regulatorInfo['fname'].' '. date('Y年m月d日',strtotime($report_week)).'至'.date('Y年m月d日',strtotime($report_week) + (86400*7-86399)).' 监测周报';
			$start_date = date('Y年m月d日',strtotime($report_week));//监测开始时间
			$end_date = date('Y年m月d日',strtotime($report_week) + (86400*7-86400));//监测结束时间
		}elseif($report_type == 3){//月报
			$report_month = I('report_month');//报告月份
			if($report_month == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'请选择您要生成几月的月报'));
			if($report_name == '') $report_name = $regulatorInfo['fname'].' '. date('Y年m月',strtotime($report_month)).' 监测月报';
			$start_date = date('Y年m月d日',strtotime($report_month));//监测开始时间
			$end_date = date('Y年m月d日',strtotime('+1 months',strtotime($report_month) - 86399));//监测结束时间
			
			
			
		}elseif($report_type == 4){//年报
			$report_year = I('report_year');//报告年份
			if($report_year == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'请选择您要生成哪一年的年报'));
			if($report_name == '') $report_name = $regulatorInfo['fname'].' '. date('Y年',strtotime($report_year)).' 监测年报';
			
			$start_date = date('Y年m月d日',strtotime($report_year));//监测开始时间
			$end_date = date('Y年m月d日',strtotime('+1 years',strtotime($report_year) - 86399));//监测结束时间
			
			
			
			
		}elseif($report_type == 99){//自定义
			$report_start_date = I('report_start_date');//自定义开始时间
			$report_end_date = I('report_end_date');//自定义结束时间
			
			
			
			if($report_start_date == '' || $report_end_date == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'请正确选择生成报告的周期'));
			if($report_name == '') $report_name = $regulatorInfo['fname'].' '. date('Y年m月d日',strtotime($report_start_date)).'至'.date('Y年m月d日',strtotime($report_end_date)).' 监测报告';
			
			$start_date = date('Y年m月d日',strtotime($report_start_date));//监测开始时间
			$end_date = date('Y年m月d日',strtotime($report_end_date));//监测结束时间

		}
		
		
		$a_data = array();
		$a_data['report_name'] = $report_name;//报告名称
		$a_data['report_type'] = $report_type;//报告类型
		$a_data['fcreator'] = $regulatorpersonInfo['fname'];//创建人
		$a_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$a_data['fmodifier'] = $regulatorpersonInfo['fname'];//修改人
		$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$a_data['regulator_id'] = $regulatorInfo['fid'];//监管机构的ID


		$report_id = M('monitor_report')->add($a_data);
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'已提交生成报告,请稍后查看'),'',0,'CC');
		

		
		$report_data = array();
		$report_data['summary']['reportfilename'] = '';//保存路径
		$report_data['summary']['reportdateseg'] = $start_date.'至'.$end_date;//监测时间区间
		$report_data['summary']['reportname'] = $report_name;//监测报告名称
		//$report_data['summary']['year'] = date('Y',strtotime($report_month));//报告年份
		//$report_data['summary']['month'] = date('m',strtotime($report_month));//报告月份
		$report_data['summary']['department'] = $regulatorInfo['fname'].'广告监测中心';//监测中心名字
		$report_data['summary']['printmonth'] = date('Y年m月d日');//报告生成时间
		
		$issue_count = A('Gongshang/ReportMake','Model')->issue_count($regulatorRegionInfo['fid'],str_replace(array('年','月','日'),array('-','-',''),$start_date),str_replace(array('年','月','日'),array('-','-',''),$end_date));
		
		
		
		$report_data['part1']['text'] .= $report_data['summary']['reportdateseg'].'，'.$report_data['summary']['department'].'对'.$regulatorRegionInfo['fname'].'媒体';
		$report_data['part1']['text'] .= '（'.$issue_count['tv']['media_count'].'个电视频道、'.$issue_count['bc']['media_count'].'个广播频道、'.$issue_count['paper']['media_count'].'份报纸）';
		$report_data['part1']['text'] .= '广告发布情况进行了监测。共监测广告';
		$report_data['part1']['text'] .= ($issue_count['tv']['illegal_0_count'] + $issue_count['bc']['illegal_0_count'] + $issue_count['paper']['illegal_0_count']).'条次，';
		$report_data['part1']['text'] .= '其中涉嫌违法广告'.($issue_count['tv']['illegal_count'] + $issue_count['bc']['illegal_count'] + $issue_count['paper']['illegal_count']).'条次,';
		$report_data['part1']['text'] .= '总违法率'.	round((
														($issue_count['tv']['illegal_count'] + $issue_count['bc']['illegal_count'] + $issue_count['paper']['illegal_count']) / 
														($issue_count['tv']['illegal_0_count'] + $issue_count['bc']['illegal_0_count'] + $issue_count['paper']['illegal_0_count']) * 100
														),2).'％，';
		$report_data['part1']['text'] .= '其中涉嫌严重违法广告'.($issue_count['tv']['illegal_30_count'] + $issue_count['bc']['illegal_30_count'] + $issue_count['paper']['illegal_30_count']).'条次,';
		$report_data['part1']['text'] .= '严重违法率'.	round((
														($issue_count['tv']['illegal_30_count'] + $issue_count['bc']['illegal_30_count'] + $issue_count['paper']['illegal_30_count']) / 
														($issue_count['tv']['illegal_0_count'] + $issue_count['bc']['illegal_0_count'] + $issue_count['paper']['illegal_0_count']) * 100
														),2).'％。';
		
		
		 
		$issue_count2 = A('Gongshang/ReportMake','Model')->issue_count2($regulatorRegionInfo['fid'],str_replace(array('年','月','日'),array('-','-',''),$start_date),str_replace(array('年','月','日'),array('-','-',''),$end_date));
		$report_data['part2']['list'] = $issue_count2;//各个媒介的发布情况，违法情况等

		$issue_count3 = A('Gongshang/ReportMake','Model')->issue_count3($regulatorRegionInfo['fid'],str_replace(array('年','月','日'),array('-','-',''),$start_date),str_replace(array('年','月','日'),array('-','-',''),$end_date));
		
		$report_data['part3']['text'] .= '重点监测药品、医疗器械、化妆品、保健食品、医疗服务五大类广告';
		$report_data['part3']['text'] .= ($issue_count3['tv']['illegal_0_count'] + $issue_count3['bc']['illegal_0_count'] + $issue_count3['paper']['illegal_0_count']).'条次，';
		$report_data['part3']['text'] .= '发现涉嫌违法广告'.($issue_count3['tv']['illegal_count'] + $issue_count3['bc']['illegal_count'] + $issue_count3['paper']['illegal_count']).'条次,';
		$report_data['part3']['text'] .= '违法率为'.	round((
														($issue_count3['tv']['illegal_count'] + $issue_count3['bc']['illegal_count'] + $issue_count3['paper']['illegal_count']) / 
														($issue_count3['tv']['illegal_0_count'] + $issue_count3['bc']['illegal_0_count'] + $issue_count3['paper']['illegal_0_count']) * 100
														),2).'％，';
		$report_data['part3']['text'] .= '其中涉嫌严重违法广告'.($issue_count3['tv']['illegal_30_count'] + $issue_count3['bc']['illegal_30_count'] + $issue_count3['paper']['illegal_30_count']).'条次,';
		$report_data['part3']['text'] .= '严重违法率为'.	round((
														($issue_count3['tv']['illegal_30_count'] + $issue_count3['bc']['illegal_30_count'] + $issue_count3['paper']['illegal_30_count']) / 
														($issue_count3['tv']['illegal_0_count'] + $issue_count3['bc']['illegal_0_count'] + $issue_count3['paper']['illegal_0_count']) * 100
														),2).'％。';
		
		$report_data['part3']['class'] = array(
												array('name'=>'药品'),
												array('name'=>'医疗器械'),
												array('name'=>'化妆品'),
												array('name'=>'保健食品'),
												array('name'=>'医疗服务'),
											);	
												
		$issue_count4 = A('Gongshang/ReportMake','Model')->issue_count4($regulatorRegionInfo['fid'],str_replace(array('年','月','日'),array('-','-',''),$start_date),str_replace(array('年','月','日'),array('-','-',''),$end_date));
		$report_data['part3']['list'] = $issue_count4;//五大类违法媒介
		
		$issue_count5 = A('Gongshang/ReportMake','Model')->issue_count5($regulatorRegionInfo['fid'],str_replace(array('年','月','日'),array('-','-',''),$start_date),str_replace(array('年','月','日'),array('-','-',''),$end_date));
		
		$report_data['part4']['text'] .= '本月共监测公益广告'.($issue_count5['tv']['gyggsl'] + $issue_count5['bc']['gyggsl'] + $issue_count5['paper']['gyggsl'] );
		$report_data['part4']['text'] .= '条次，公益广告发布率为';
		$report_data['part4']['text'] .= 	round(($issue_count5['tv']['gyggsl'] + $issue_count5['bc']['gyggsl'] + $issue_count5['paper']['gyggsl'] ) 
											/ ($issue_count5['tv']['sl'] + $issue_count5['bc']['sl'] + $issue_count5['paper']['sl'] ) * 100 ,2).'％';
		
		//$report_data = json_encode($report_data,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
		$report_data = json_encode($report_data);
		
		//echo $report_data;
		$reporttype = 'xiamengreport';

		$rr = http(C('REPORT_MAKE_URL'),'reporttype='.$reporttype.'&reportparam='.$report_data ,'POST',false,60);
		//var_dump($rr);
		$rr = json_decode($rr,true);
		$e_data = array();
		if($rr['code'] === 0){
			$e_data['report_url'] = $rr['ReportFileName'];//报告URL
			$e_data['make_state'] = 1;//生成成功
		}else{
			$e_data['make_state'] = 2;//生成失败
		}
		M('monitor_report')->where(array('report_id'=>$report_id))->save($e_data);//修改数据库
		
		//sleep(60);
		
		
	}
	
	public function report02(){

		set_time_limit(0);
		
		$regulatorpersonInfo = M('tregulatorperson')->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
		$regulatorInfo = M('tregulator')->where(array('fcode'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息
		$regulatorRegionInfo = M('tregion')->field('fid,fname')->where(array('fid'=>$regulatorInfo['fregionid']))->find();//监管地区信息信息
		session_write_close();//停止使用session

		$report_name = I('report_name');//报告名称
		$report_start_date = I('report_start_date');//开始时间
		$report_end_date = I('report_end_date');//结束时间
		
		$start_date = date('Y年m月d日',strtotime($report_start_date));//监测开始时间
		$end_date = date('Y年m月d日',strtotime($report_end_date));//监测结束时间
		$year_month =  date('Y年m月',strtotime($report_start_date));
		$start_month_day = date('m月d日',strtotime($report_start_date));
		$end_month_day = date('m月d日',strtotime($report_end_date));
		$days = (strtotime($report_end_date) - strtotime($report_start_date))/86400 + 1;

		//按行政级别区分
		$regionid = $regulatorRegionInfo['fid'];
		if($regionid == 100000) $regionid = '000000';
		$region_id_rtrim = rtrim($regionid,'00');//去掉地区后面的00
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位
		if($tregion_len == 0){
			$area_name = '全国';
			$jur_area = '省';
		}elseif($tregion_len == 2) {
			$area_name = '全省';
			$jur_area = '市';
		}elseif($tregion_len == 4){
			$area_name = '全市';
			$jur_area = '县';
		}else{
			$area_name = '';
			$jur_area = '';
		}

		//名字为空自动生成名字
		if(empty($report_name)){
			$report_name = $regulatorInfo['fname'].' '. $start_date.'至'.$end_date.' 监测报告';
		}
		$a_data['report_name'] 	= $report_name;//报告名称
		$a_data['report_type'] 	= 99;//报告类型
		$a_data['fcreator'] 	= $regulatorpersonInfo['fname'];//创建人
		$a_data['fcreatetime'] 	= date('Y-m-d H:i:s');//创建时间
		$a_data['fmodifier'] 	= $regulatorpersonInfo['fname'];//修改人
		$a_data['fmodifytime'] 	= date('Y-m-d H:i:s');//修改时间
		$a_data['regulator_id'] = $regulatorInfo['fid'];//监管机构的ID
		$report_id = M('monitor_report')->add($a_data);	
		$this->ajaxReturn(array('code'=>0,'msg'=>'已提交生成报告,请稍后查看'),'',0,'CC');

		$startDate = str_replace(array('年','月','日'),array('-','-',''),$start_date);
		$endDate = str_replace(array('年','月','日'),array('-','-',''),$end_date);
		//获取指定时间内的发布统计
		$issue_count = A('Gongshang/ReportMake','Model')->region_issue($regulatorRegionInfo['fid'],$startDate,$endDate);
		$adclass 	 = A('Gongshang/ReportMake','Model')->count_adclass($regulatorRegionInfo['fid'],$startDate,$endDate);
		$adcaseInfo  = A('Gongshang/ReportMake','Model')->getAdcase($regulatorInfo['fid'],$startDate,$endDate);
		$mediatype       = $issue_count['mediatype'];
		$tv_mediatype    = $issue_count['tv_mediatype'];
		$bc_mediatype    = $issue_count['bc_mediatype'];
		$paper_mediatype = $issue_count['paper_mediatype'];
		$issue       = $issue_count['issue'];
		$tv_issue 	 = $issue_count['tv'];
		$bc_issue 	 = $issue_count['bc'];
		$paper_issue = $issue_count['paper'];
		$adcase = $adcaseInfo['adcase'];
		$media_count = $issue_count['media_count'];//媒介数量

		//报告内容
		$report_data['summary']['reportfilename'] = '';//保存路径
		$report_data['summary']['reportname'] = '关于'.$year_month.'广告监测情况的通报';
		$report_data['summary']['department'] = $regulatorInfo['fname'];
		$report_data['summary']['printdate'] = date('Y年m月d日');
		$report_data['summary']['sendto'] = $area_name.'工商行政管理局、市场监督管理部门：';
		$report_data['summary']['text'] = $year_month.'，'.$regulatorRegionInfo['fname'].'工商行政管理局广告监测中心对'.$area_name.'所属的媒介广告发布情况进行了监测，现将有关情况通报如下：';

		$report_data['part1']['title'] = '一、监测范围及对象';
		$report_data['part1']['part1.1']['title'] = '（一）传统媒体广告监测';
		$report_data['part1']['part1.1']['text'] = $year_month.'，共对'.$area_name.'所属的'.$media_count.'家电视、广播、报纸等传统媒体广告发布情况进行了监测，广告监测范围为'.$start_month_day.'-'.$end_month_day.'（共'.$days.'天）';
		$report_data['part1']['part1.2']['title'] = '（二）互联网广告监测';
		$report_data['part1']['part1.2']['text'] = '由国家互联网广告监测中心提供相关数据。';

		$report_data['part2']['title'] = '二、监测总体情况';
		$report_data['part2']['text1'] = $year_month.'，共监测到电视、广播、报纸的各类广告'.$mediatype['total'].'条次，涉嫌严重违法广告'.$mediatype['illegal'].'条次，条次违法率为'.$mediatype['percent'].'。';
		$report_data['part2']['table']['title'] = '表1各类媒体广告监测统计情况';
		$report_data['part2']['table']['list'] = array($tv_mediatype,$bc_mediatype,$paper_mediatype,$mediatype);
		$report_data['part2']['text2'] = '涉嫌严重违法广告类别主要集中在'.$adclass[0]['name'].'、'.$adclass[1]['name'].'、'.$adclass[2]['name'].'、'.$adclass[3]['name'].'、'.$adclass[4]['name'].'，其中'.$adclass[0]['name'].'广告占涉嫌严重违法广告总量的'.$adclass[0]['ratio'].'、'.$adclass[1]['name'].'广告占'.$adclass[1]['ratio'].'、'.$adclass[2]['name'].'广告占'.$adclass[2]['ratio'].'、'.$adclass[3]['name'].'广告占'.$adclass[3]['ratio'].'、'.$adclass[4]['name'].'广告占'.$adclass[4]['ratio'].'。';

		$report_data['part3']['title'] = '三、传统媒体广告监测情况';
		$report_data['part3']['part3.1']['title'] = '（一）区域广告市场秩序情况';
		$report_data['part3']['part3.1']['part3.1.1']['title'] = '1.广告市场秩序总体情况';
		$region_str = A('Gongshang/ReportMake','Model')->getRegionStr($issue);
		$report_data['part3']['part3.1']['part3.1.1']['text'] = '监测数据显示，'.$year_month.'，发布涉嫌严重违法广告条次违法率最为严重的'.$region_str['region_count'].'个'.$jur_area.'依次为：'.$region_str['str'].'。';
		$report_data['part3']['part3.1']['part3.1.1']['table']['title'] = '表2 发布涉嫌严重违法广告情况';
		$report_data['part3']['part3.1']['part3.1.1']['table']['title1'] = '（注：按发布涉嫌严重违法广告情况严重程度从高到低排列）';
		$report_data['part3']['part3.1']['part3.1.1']['table']['list'] = $issue;
		$report_data['part3']['part3.1']['part3.1.2']['title'] = '2.各'.$jur_area.'电视发布涉嫌严重违法广告情况';
		$tv_region_str = A('Gongshang/ReportMake','Model')->getRegionStr($tv_issue);
		$report_data['part3']['part3.1']['part3.1.2']['text'] = $year_month.'，电视发布涉嫌严重违法广告条次违法率最为严重的'.$tv_region_str['region_count'].'个地区依次为：'.$tv_region_str['str'].'。';
		$report_data['part3']['part3.1']['part3.1.2']['table']['title'] = '表3 电视发布涉嫌严重违法广告情况';
		$report_data['part3']['part3.1']['part3.1.2']['table']['title1'] = '（注：按发布涉嫌严重违法广告情况严重程度从高到低排列）';
		$report_data['part3']['part3.1']['part3.1.2']['table']['list'] = $tv_issue;
		$report_data['part3']['part3.1']['part3.1.3']['title'] = '3.各'.$jur_area.'广播发布涉嫌严重违法广告情况';
		$bc_region_str = A('Gongshang/ReportMake','Model')->getRegionStr($bc_issue);
		$report_data['part3']['part3.1']['part3.1.3']['text'] = $year_month.'，广播发布涉嫌严重违法广告条次违法率最为严重的'.$bc_region_str['region_count'].'个地区依次为：'.$bc_region_str['str'].'。';
		$report_data['part3']['part3.1']['part3.1.3']['table']['title'] = '表4 广播发布涉嫌严重违法广告情况';
		$report_data['part3']['part3.1']['part3.1.3']['table']['title1'] = '（注：按发布涉嫌严重违法广告情况严重程度从高到低排列）';
		$report_data['part3']['part3.1']['part3.1.3']['table']['list'] = $bc_issue;
		$report_data['part3']['part3.1']['part3.1.4']['title'] = '4.各'.$jur_area.'报纸发布涉嫌严重违法广告情况';
		$paper_region_str = A('Gongshang/ReportMake','Model')->getRegionStr($paper_issue);
		$report_data['part3']['part3.1']['part3.1.4']['text'] = $year_month.'，报纸发布涉嫌严重违法广告条次违法率最为严重的'.$paper_region_str['region_count'].'个地区依次为：'.$paper_region_str['str'].'。';
		$report_data['part3']['part3.1']['part3.1.4']['table']['title'] = '表5 报纸发布涉嫌严重违法广告情况';
		$report_data['part3']['part3.1']['part3.1.4']['table']['title1'] = '（注：按发布涉嫌严重违法广告情况严重程度从高到低排列）';
		$report_data['part3']['part3.1']['part3.1.4']['table']['list'] = $paper_issue;

		$report_data['part4']['title'] = '四、互联网广告监测情况';
		$report_data['part4']['text'] = '由国家互联网广告监测中心提供相关数据！';

		$report_data['part5']['title'] = '五、'.$area_name.'广告监管效能';
		$report_data['part5']['text'] = $year_month.'，'.$regulatorInfo['fname'].'广告监管系统共派发涉嫌严重违法广告线索'.$adcaseInfo['adcase_sum'].'条，截止'.$end_month_day.'，'.$jur_area.'单位共查看'.$adcaseInfo['adcase_check_sum'].'条，查看率为'.$adcaseInfo['adcase_check_ratio'].'，停播'.$adcaseInfo['adcase_xstb'].'条，停播率为'.$adcaseInfo['adcase_xstb_ratio'].'；立案调查'.$adcaseInfo['adcase_xsla'].'条，立案率为'.$adcaseInfo['adcase_xsla_ratio'].'。';
		$report_data['part5']['table']['title'] = '表14 涉嫌严重违法广告线索处理情况统计表（截止'.$end_month_day.'）';
		$report_data['part5']['table']['title1'] = '（注：按发布涉嫌严重违法广告线索处理率从低到高排列）';
		$report_data['part5']['table']['list'] = $adcase;

		$report_data = json_encode($report_data);
		$reporttype = 'sichuanreport';

		$rr = http(C('REPORT_MAKE_URL'),'reporttype='.$reporttype.'&reportparam='.$report_data,'POST',false,6000);
		// var_dump($rr);
		$rr = json_decode($rr,true);
		$e_data = array();
		if($rr['code'] === 0){
			$e_data['report_url'] = $rr['ReportFileName'];//报告URL
			$e_data['make_state'] = 1;//生成成功
		}else{
			$e_data['make_state'] = 2;//生成失败
		}
		M('monitor_report')->where(array('report_id'=>$report_id))->save($e_data);//修改数据库

	}

	public function report03(){

		set_time_limit(0);
	    Vendor("PHPExcel.IOFactory");
		session_write_close();
		// $this->ajaxReturn(array('code'=>0,'msg'=>'已提交生成报告,请稍后查看'),'',0,'CC');
		$regulatorpersonInfo = M('tregulatorperson')->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
		$regulatorInfo = M('tregulator')->where(array('fcode'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息
		$data     = json_decode($_POST['data'],true);//所选数据
		$regionid = $data[0]['region'];//地区ID
		$month 	  = $data[0]['month'];//月份
		$regulatorRegionInfo = M('tregion')->field('fid,fname,ffullname,fname1')->where(array('fid'=>$regionid))->find();

		$region_id_rtrim = rtrim($regionid,'00');//去掉地区后面两位0，判断区
		$region_id_rtrim = rtrim($region_id_rtrim,'000');//去掉后面三位0，判断全国
		$region_id_rtrim = rtrim($region_id_rtrim,'00');//去掉后面两位0，判断市
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位
		if($tregion_len == 0){
			$area_name = '';
		}elseif($tregion_len == 2){
			$area_name = '省';
		}elseif($tregion_len == 4){
			$area_name = '市';
		}elseif($tregion_len == 6){
			$area_name = '（区/县）';
		}else{
			$area_name = '';
		}

		$report_start_date = date('Y-m-01', strtotime($month));//开始时间
		$report_end_date = date('Y-m-d', strtotime("$report_start_date +1 month -1 day"));//结束时间
		if($report_start_date == $report_end_date) $this->ajaxReturn(array('code'=>-1,'msg'=>'开始结束时间相同'));
		$startDate = date('Y-m-d H:i:s',strtotime($report_start_date));
		$endDate = date('Y-m-d H:i:s',strtotime($report_end_date)+86399);
		$report_month = date('n',strtotime($month));//报告时间
		$start_date = date('Y年m月d日',strtotime($report_start_date));//监测开始时间
		$end_date = date('Y年m月d日',strtotime($report_end_date));//监测结束时间

		//自动生成名字
		$report_name = $regulatorRegionInfo['fname1'].' '.$report_month.'月份监测报告';
		$a_data['report_name'] = $report_name;//报告名称
		$a_data['report_type'] = 99;//报告类型
		$a_data['fcreator'] = $regulatorpersonInfo['fname'];//创建人
		$a_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$a_data['fmodifier'] = $regulatorpersonInfo['fname'];//修改人
		$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$a_data['regulator_id'] = $regulatorInfo['fid'];//监管机构的ID
		$report_id = M('monitor_report')->add($a_data);	

		//获取数据
		$issue_data = A('Gongshang/ReportMake','Model')->issue_data($data);
		// $this->ajaxReturn(array('code'=>0,'msg'=>$issue_data));

		$tv_issue_data 	   	= $issue_data['tv'];							 //电视数据
		$tv_ill_data 	   	= $issue_data['tv_ill'];						 //电视违法数据
		$tv_media_group    	= $issue_data['tv_media_group'];				 //电视媒介
		$tv_count 		   	= count($tv_media_group);						 //统计电视媒介数
		$tv_sum 		   	= $issue_data['tv_sum'];						 //电视发布次数
		$tv_illegal_sum    	= $issue_data['tv_illegal_sum'];				 //电视违法发布次数
		$tv_rate			= round($tv_illegal_sum/$tv_sum*100,2).'%';		 //电视发布违法率
		$tv_length 		   	= $issue_data['tv_length'];						 //电视总时长
		$tv_illegal_length 	= $issue_data['tv_illegal_length'];				 //电视违法总时长
		$tv_length_rate		= round($tv_illegal_length/$tv_length*100,2).'%';//电视时长违法率

		$bc_issue_data 		= $issue_data['bc']; 							 //广播数据
		$bc_ill_data 	   	= $issue_data['bc_ill'];						 //广播违法数据
		$bc_media_group 	= $issue_data['bc_media_group'];				 //广播媒介
		$bc_count 			= count($bc_media_group);						 //统计广播媒介数
		$bc_sum				= $issue_data['bc_sum'];						 //广播发布次数
		$bc_illegal_sum		= $issue_data['bc_illegal_sum'];				 //广播违法发布次数
		$bc_rate			= round($bc_illegal_sum/$bc_sum*100,2).'%';		 //广播发布违法率
		$bc_length			= $issue_data['bc_length'];						 //广播总时长
		$bc_illegal_length  = $issue_data['bc_illegal_length'];				 //广播违法总时长
		$bc_length_rate		= round($bc_illegal_length/$bc_length*100,2).'%';//广播时长违法率

		$paper_issue_data 	= $issue_data['paper']; 						 //报纸数据
		$paper_ill_data 	= $issue_data['paper_ill'];						 //报纸违法数据
		$paper_media_group 	= $issue_data['paper_media_group'];				 //报纸媒介
		$paper_count 		= count($paper_media_group);					 //统计报纸媒介数
		$paper_sum			= $issue_data['paper_sum'];					 	 //报纸发布次数
		$paper_illegal_sum	= $issue_data['paper_illegal_sum'];			 	 //报纸违法发布次数
		$paper_rate			= round($paper_illegal_sum/$paper_sum*100,2).'%';//报纸发布违法率

		// //合计
		$total_sum 			= $tv_sum + $bc_sum + $paper_sum;				 
		$total_illegal_sum 	= $tv_illegal_sum + $bc_illegal_sum + $paper_illegal_sum;
		$total_rate			= round($total_illegal_sum/$total_sum*100,2).'%';
		$total_length		= $tv_length + $bc_length;
		$total_ill_length 	= $tv_illegal_length + $bc_illegal_length;
		$total_length_rate	= round($total_ill_length/$total_length*100,2).'%';

		$ad_class 			= array_values($issue_data['ad_class']);//广告类别统计
		$five_class_illegal = array_values($issue_data['five_class_illegal']);//违法广告五大类

		$template = $_SERVER['DOCUMENT_ROOT'].'/Application/Gongshang/Common/template.xls';	//使用模板  
	    $objPHPExcel = \PHPExcel_IOFactory::load($template);     	//加载excel文件,设置模板  	   
	    $objWriter = new \PHPExcel_Writer_Excel5($objPHPExcel);  	//设置保存版本格式  

	    //接下来就是写数据到表格里面去  
	    $sheet0 = $objPHPExcel->getSheet(0);	//表一
	    $sheet1 = $objPHPExcel->getSheet(1);	//表二
	    $sheet2 = $objPHPExcel->getSheet(2);	//表三
	    $sheet3 = $objPHPExcel->getSheet(3);	//表四
	    $sheet4 = $objPHPExcel->getSheet(4);	//表五
	    $sheet5 = $objPHPExcel->getSheet(5);	//表六
	    $sheet6 = $objPHPExcel->getSheet(6); 	//表七

	    //表一 
	    $highestRow = $sheet0->getHighestRow();//表一总行数
	    //设置表头
	    $headtitle = '表一:'.$regulatorRegionInfo['fname1'].$area_name.'级媒体广告监测情况及目录';
	    $sheet0->setCellValue('A1',$headtitle); 

	    $sheet0->setCellValue('A3',$tv_count); 
	    $sheet0->setCellValue('B3',$bc_count);
	    $sheet0->setCellValue('C3',$paper_count); 
	    //监测目录
	    //设置属性
	    $style = array(
	    	'alignment' => array(
	    		'vertical'		=> \PHPExcel_Style_Alignment::VERTICAL_CENTER,		//垂直居中
	    	),
	    	//边框
	    	'borders' => array(
	    		'bottom' 	=> array('style' => \PHPExcel_Style_Border::BORDER_THIN),
	    		'right' 	=> array('style' => \PHPExcel_Style_Border::BORDER_THIN),
	    	),
	    );
	    $top_boder = array('borders'=>array('top'=>array('style' => \PHPExcel_Style_Border::BORDER_THIN),));
	    $bottom_border = array('borders'=>array('bottom'=>array('style' => \PHPExcel_Style_Border::BORDER_THIN),));
	    //电视列表
	    $tv_end_row = $tv_start_row = $highestRow + 1;//设置位置	    
	    $sheet0->getStyle('A'.''.$tv_start_row)->applyFromArray($top_boder);
	    $sheet0->getStyle('B'.''.$tv_start_row)->applyFromArray($top_boder);
	    $sheet0->getStyle('C'.''.$tv_start_row)->applyFromArray($top_boder);
	    if($tv_count > 0){
	    	$sheet0->setCellValue('A'.$tv_start_row,$area_name.'级电视媒体（频道）');
	    	for ($i = 0;$i < $tv_count;$i++) { 
	    		$tv_end_row = $tv_start_row + $i;
	    		$sheet0->setCellValue('B'.$tv_end_row,$tv_media_group[$i]['fmedianame']);
	    		$sheet0->setCellValue('C'.$tv_end_row,$tv_media_group[$i]['day']);
	    		$sheet0->getStyle('B'.''.$tv_end_row)->applyFromArray($bottom_border);
	    		$sheet0->getStyle('C'.''.$tv_end_row)->applyFromArray($bottom_border);
	    	}
	    	$sheet0->mergeCells('A'.''.$tv_start_row.':'.'A'.''.$tv_end_row);//合并单元格
	    }else{
	    	$sheet0->setCellValue('A'.$tv_start_row,$area_name.'级电视媒体（频道）');
	    	$sheet0->setCellValue('B'.$tv_start_row,'');
	    	$sheet0->setCellValue('C'.$tv_start_row,'');
	    }
	    $sheet0->getStyle('A'.''.$tv_start_row.':'.'A'.''.$tv_end_row)->applyFromArray($style);
	    $sheet0->getStyle('B'.''.$tv_start_row.':'.'B'.''.$tv_end_row)->applyFromArray($style);
	    $sheet0->getStyle('C'.''.$tv_start_row.':'.'C'.''.$tv_end_row)->applyFromArray($style);
	    //广播列表
	    $bc_end_row = $bc_start_row = $tv_end_row + 1;
	    if($bc_count > 0){
	    	$sheet0->setCellValue('A'.$bc_start_row,$area_name.'级广播媒体（频率）');
	    	for ($i = 0;$i < $bc_count;$i++) { 
	    		$bc_end_row = $bc_start_row + $i;
	    		$sheet0->setCellValue('B'.$bc_end_row,$bc_media_group[$i]['fmedianame']);
	    		$sheet0->setCellValue('C'.$bc_end_row,$bc_media_group[$i]['day']);
	    		$sheet0->getStyle('B'.''.$bc_end_row)->applyFromArray($bottom_border);
	    		$sheet0->getStyle('C'.''.$bc_end_row)->applyFromArray($bottom_border);
	    	}
	    	$sheet0->mergeCells('A'.''.$bc_start_row.':'.'A'.''.$bc_end_row);//合并单元格
	    }else{
	    	$sheet0->setCellValue('A'.$bc_start_row,$area_name.'级广播媒体（频率）');
	    	$sheet0->setCellValue('B'.$bc_start_row,'');
	    	$sheet0->setCellValue('C'.$bc_start_row,'');
	    }
	    $sheet0->getStyle('A'.''.$bc_start_row.':'.'A'.''.$bc_end_row)->applyFromArray($style);
	    $sheet0->getStyle('B'.''.$bc_start_row.':'.'B'.''.$bc_end_row)->applyFromArray($style);
	    $sheet0->getStyle('C'.''.$bc_start_row.':'.'C'.''.$bc_end_row)->applyFromArray($style);
	    //报纸列表
	    $paper_end_row = $paper_start_row = $bc_end_row + 1;
	    if($paper_count > 0){
	    	$sheet0->setCellValue('A'.$paper_start_row,$area_name.'级报纸');
	    	for ($i = 0;$i < $paper_count;$i++) { 
	    		$paper_end_row = $paper_start_row + $i;
	    		$sheet0->setCellValue('B'.$paper_end_row,$paper_media_group[$i]['fmedianame']);
	    		$sheet0->setCellValue('C'.$paper_end_row,$paper_media_group[$i]['day']);
	    		$sheet0->getStyle('B'.''.$paper_end_row)->applyFromArray($bottom_border);
	    		$sheet0->getStyle('C'.''.$paper_end_row)->applyFromArray($bottom_border);
	    	}
	    	$sheet0->mergeCells('A'.''.$paper_start_row.':'.'A'.''.$paper_end_row);//合并单元格
	    }else{
	    	$sheet0->setCellValue('A'.$paper_start_row,$area_name.'级报纸');
	    	$sheet0->setCellValue('B'.$paper_start_row,'');
	    	$sheet0->setCellValue('C'.$paper_start_row,'');
	    }
	    $sheet0->getStyle('A'.''.$paper_start_row.':'.'A'.''.$paper_end_row)->applyFromArray($style);
	    $sheet0->getStyle('B'.''.$paper_start_row.':'.'B'.''.$paper_end_row)->applyFromArray($style);
	    $sheet0->getStyle('C'.''.$paper_start_row.':'.'C'.''.$paper_end_row)->applyFromArray($style);

	    //表二
	    //设置表头
	    $headtitle = '表二:'.$regulatorRegionInfo['fname1'].$area_name.'级媒体广告监测概表';
	    $sheet1->setCellValue('A1',$headtitle); 
	    //电视
	    $sheet1->setCellValue('B3',$tv_sum);
	    $sheet1->setCellValue('C3',$tv_illegal_sum);
	    $sheet1->setCellValue('D3',$tv_rate);
	    $sheet1->setCellValue('E3',$tv_illegal_length);
	    $sheet1->setCellValue('F3',$tv_length_rate);
	    //广播
	    $sheet1->setCellValue('B4',$bc_sum);
	    $sheet1->setCellValue('C4',$bc_illegal_sum);
	    $sheet1->setCellValue('D4',$bc_rate);
	    $sheet1->setCellValue('E4',$bc_illegal_length);
	    $sheet1->setCellValue('F4',$bc_length_rate);
	    //报纸
	    $sheet1->setCellValue('B5',$paper_sum);
	    $sheet1->setCellValue('C5',$paper_illegal_sum);
	    $sheet1->setCellValue('D5',$paper_rate);
	    //合计
	    $sheet1->setCellValue('B6',$total_sum);
	    $sheet1->setCellValue('C6',$total_illegal_sum);
	    $sheet1->setCellValue('D6',$total_rate);
	    $sheet1->setCellValue('E6',$total_ill_length);
	    $sheet1->setCellValue('F6',$total_length_rate);

	    //表三
	    //设置表头
	    $headtitle = '表三:'.$regulatorRegionInfo['fname1'].$area_name.'级电视媒体涉嫌违法广告统计表';
	    $sheet2->setCellValue('A1',$headtitle); 
	    if($tv_illegal_sum > 0){
	    	if($tv_illegal_sum > 7){
		    	$sheet2->setCellValue('A10',' ');
		    	$sheet2->setCellValue('A10',' ');
		    	$sheet2->unmergeCells('A10:G10');    // 拆分
		    	$sheet2->unmergeCells('A11:G11');    // 拆分
		    }
	    	for($i = 0;$i < $tv_illegal_sum;$i++){
	    		$row = $i + 3;
				
				$fexpressioncodes = $tv_ill_data[$i]['fexpressioncodes'];//违法表现代码
				$fexpression_c = '';
				if($fexpressioncodes){
					$fexpressioncodes_arr = explode(';',$fexpressioncodes);
					foreach($fexpressioncodes_arr as $fexpressioncode){
						$fexpression = M('tillegal')->cache(true,3600)->where(array('fcode'=>$fexpressioncode))->getField('fexpression');
						$fexpression_c .= $fexpressioncode.':'.$fexpression."\n";
					}
				}
				
				
				
	    		$sheet2->setCellValue('A'.$row,$tv_ill_data[$i]['fmedianame']);
	    		$sheet2->setCellValue('B'.$row,$tv_ill_data[$i]['fadname']);
	    		$sheet2->setCellValue('C'.$row,substr($tv_ill_data[$i]['fstarttime'],0,10));
	    		$sheet2->setCellValue('D'.$row,substr($tv_ill_data[$i]['fstarttime'],11,8));
	    		$sheet2->setCellValue('E'.$row,$tv_ill_data[$i]['flength']);
	    		$sheet2->setCellValue('F'.$row,$tv_ill_data[$i]['ffullname']);
	    		$sheet2->setCellValue('G'.$row,$fexpression_c); //违法表现代码
	    		$sheet2->getStyle('A'.$row)->applyFromArray($style);
	    		$sheet2->getStyle('B'.$row)->applyFromArray($style);
	    		$sheet2->getStyle('C'.$row)->applyFromArray($style);
	    		$sheet2->getStyle('D'.$row)->applyFromArray($style);
	    		$sheet2->getStyle('E'.$row)->applyFromArray($style);
	    		$sheet2->getStyle('F'.$row)->applyFromArray($style);
	    		$sheet2->getStyle('G'.$row)->applyFromArray($style);
	    		$sheet2->getStyle('A'.$row)->getAlignment()->setWrapText(false);
	    		$sheet2->getStyle('B'.$row)->getAlignment()->setWrapText(false);
	    		$sheet2->getStyle('C'.$row)->getAlignment()->setWrapText(false);
	    		$sheet2->getStyle('D'.$row)->getAlignment()->setWrapText(false);
	    		$sheet2->getStyle('E'.$row)->getAlignment()->setWrapText(false);
	    		$sheet2->getStyle('F'.$row)->getAlignment()->setWrapText(false);
	    		$sheet2->getStyle('G'.$row)->getAlignment()->setWrapText(false);
	    		$sheet2->getRowDimension($row)->setRowHeight(13.5);//设置行高
	    		$sheet2->getStyle('A'.$row.':'.'G'.$row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//设置水平居中
	    		$sheet2->getStyle('A'.$row.':'.'G'.$row)->getFont()->setSize(10);//字体大小
	    	}
	    	if($row > 9){
		    	$sheet2->setCellValue('A'.($row+1),'附1：广告类别包括，药品、医疗器械、化妆品、房地产、普通食品、保健食品、烟草、酒、电器、交通产品、知识产品、普通商品、医疗服务、金融服务、信息服务、互联网服务、商业招商投资、教育培训服务、会展文体活动服务、普通服务、形象宣传、非商业及其它。');
		    	$sheet2->setCellValue('A'.($row+2),'附2：按国家市场监督管理总局广告司2018年7月启用的《广告监测标准（2018年7月版）》规定的违法行为代码填写。');
		    	$sheet2->mergeCells('A'.''.($row+1).':'.'G'.''.($row+1));//合并单元格
		    	$sheet2->mergeCells('A'.''.($row+2).':'.'G'.''.($row+2));//合并单元格
		    	$sheet2->getStyle('A'.''.($row+1).':'.'G'.''.($row+1))->applyFromArray($style);
		    	$sheet2->getStyle('A'.''.($row+2).':'.'G'.''.($row+2))->applyFromArray($style);
		    	$sheet2->getStyle('A'.''.($row+1))->getFont()->setSize(10);//设置字体大小
		    	$sheet2->getStyle('A'.''.($row+2))->getFont()->setSize(10);//设置字体大小
		    	$sheet2->getStyle('A'.''.($row+1))->getAlignment()->setWrapText(true);//设置自动换行
		    	$sheet2->getStyle('A'.''.($row+2))->getAlignment()->setWrapText(true);//设置自动换行
		    	$sheet2->getRowDimension($row+1)->setRowHeight(30);//设置行高
		    	$sheet2->getRowDimension($row+2)->setRowHeight(30);//设置行高
	    	}
	    }
	    //表四
	    //设置表头
	    $headtitle = '表四:'.$regulatorRegionInfo['fname1'].$area_name.'级广播媒体涉嫌违法广告统计表';
	    $sheet3->setCellValue('A1',$headtitle);
	    if($bc_illegal_sum > 0){
	    	if($bc_illegal_sum > 7){
		    	$sheet3->setCellValue('A10',' ');
		    	$sheet3->setCellValue('A10',' ');
		    	$sheet3->unmergeCells('A10:G10');    // 拆分
		    	$sheet3->unmergeCells('A11:G11');    // 拆分
		    }
	    	for($i = 0;$i < $bc_illegal_sum;$i++){
	    		$row = $i + 3;
				
				$fexpressioncodes = $bc_ill_data[$i]['fexpressioncodes'];//违法表现代码
				$fexpression_c = '';
				if($fexpressioncodes){
					$fexpressioncodes_arr = explode(';',$fexpressioncodes);
					foreach($fexpressioncodes_arr as $fexpressioncode){
						$fexpression = M('tillegal')->cache(true,3600)->where(array('fcode'=>$fexpressioncode))->getField('fexpression');
						$fexpression_c .= $fexpressioncode.':'.$fexpression."\n";
					}
				}
				
	    		$sheet3->setCellValue('A'.$row,$bc_ill_data[$i]['fmedianame']);
	    		$sheet3->setCellValue('B'.$row,$bc_ill_data[$i]['fadname']);
	    		$sheet3->setCellValue('C'.$row,substr($bc_ill_data[$i]['fstarttime'],0,10));
	    		$sheet3->setCellValue('D'.$row,substr($bc_ill_data[$i]['fstarttime'],11,8));
	    		$sheet3->setCellValue('E'.$row,$bc_ill_data[$i]['flength']);
	    		$sheet3->setCellValue('F'.$row,$bc_ill_data[$i]['ffullname']);
	    		$sheet3->setCellValue('G'.$row,$fexpression_c);
	    		$sheet3->getStyle('A'.$row)->applyFromArray($style);
	    		$sheet3->getStyle('B'.$row)->applyFromArray($style);
	    		$sheet3->getStyle('C'.$row)->applyFromArray($style);
	    		$sheet3->getStyle('D'.$row)->applyFromArray($style);
	    		$sheet3->getStyle('E'.$row)->applyFromArray($style);
	    		$sheet3->getStyle('F'.$row)->applyFromArray($style);
	    		$sheet3->getStyle('G'.$row)->applyFromArray($style);
	    		$sheet3->getStyle('A'.$row)->getAlignment()->setWrapText(false);
	    		$sheet3->getStyle('B'.$row)->getAlignment()->setWrapText(false);
	    		$sheet3->getStyle('C'.$row)->getAlignment()->setWrapText(false);
	    		$sheet3->getStyle('D'.$row)->getAlignment()->setWrapText(false);
	    		$sheet3->getStyle('E'.$row)->getAlignment()->setWrapText(false);
	    		$sheet3->getStyle('F'.$row)->getAlignment()->setWrapText(false);
	    		$sheet3->getStyle('G'.$row)->getAlignment()->setWrapText(false);
	    		$sheet3->getRowDimension($row)->setRowHeight(13.5);//设置行高
	    		$sheet3->getStyle('A'.$row.':'.'G'.$row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//设置水平居中
	    		$sheet3->getStyle('A'.$row.':'.'G'.$row)->getFont()->setSize(10);//字体大小
	    	}
	    	if($row > 9){
		    	$sheet3->setCellValue('A'.($row+1),'附1：广告类别包括，药品、医疗器械、化妆品、房地产、普通食品、保健食品、烟草、酒、电器、交通产品、知识产品、普通商品、医疗服务、金融服务、信息服务、互联网服务、商业招商投资、教育培训服务、会展文体活动服务、普通服务、形象宣传、非商业及其它。');
		    	$sheet3->setCellValue('A'.($row+2),'附2：按国家市场监督管理总局广告司2018年7月启用的《广告监测标准（2018年7月版）》规定的违法行为代码填写。');
		    	$sheet3->mergeCells('A'.''.($row+1).':'.'G'.''.($row+1));//合并单元格
		    	$sheet3->mergeCells('A'.''.($row+2).':'.'G'.''.($row+2));//合并单元格
		    	$sheet3->getStyle('A'.''.($row+1).':'.'G'.''.($row+1))->applyFromArray($style);
		    	$sheet3->getStyle('A'.''.($row+2).':'.'G'.''.($row+2))->applyFromArray($style);
		    	$sheet3->getStyle('A'.''.($row+1))->getFont()->setSize(10);//设置字体大小
		    	$sheet3->getStyle('A'.''.($row+2))->getFont()->setSize(10);//设置字体大小
		    	$sheet3->getStyle('A'.''.($row+1))->getAlignment()->setWrapText(true);//设置自动换行
		    	$sheet3->getStyle('A'.''.($row+2))->getAlignment()->setWrapText(true);//设置自动换行
		    	$sheet3->getRowDimension($row+1)->setRowHeight(30);//设置行高
		    	$sheet3->getRowDimension($row+2)->setRowHeight(30);//设置行高
	    	}
	    }
	    //表五
	    //设置表头
	    $headtitle = '表五:'.$regulatorRegionInfo['fname1'].$area_name.'级报纸媒体涉嫌违法广告统计表';
	    $sheet4->setCellValue('A1',$headtitle);
	    if($paper_illegal_sum > 0){
	    	if($paper_illegal_sum > 7){
		    	$sheet4->setCellValue('A10',' ');
		    	$sheet4->setCellValue('A11',' ');
		    	$sheet4->unmergeCells('A10:E10');    // 拆分
		    	$sheet4->unmergeCells('A11:E11');    // 拆分
		    }
	    	for($i = 0;$i < $paper_illegal_sum;$i++){
	    		$row = $i + 3;
				
				$fexpressioncodes = $paper_ill_data[$i]['fexpressioncodes'];//违法表现代码
				$fexpression_c = '';
				if($fexpressioncodes){
					$fexpressioncodes_arr = explode(';',$fexpressioncodes);
					foreach($fexpressioncodes_arr as $fexpressioncode){
						$fexpression = M('tillegal')->cache(true,3600)->where(array('fcode'=>$fexpressioncode))->getField('fexpression');
						$fexpression_c .= $fexpressioncode.':'.$fexpression."\n";
					}
				}
				
				
	    		$sheet4->setCellValue('A'.$row,$paper_ill_data[$i]['fmedianame']);
	    		$sheet4->setCellValue('B'.$row,$paper_ill_data[$i]['fadname']);
	    		$sheet4->setCellValue('C'.$row,substr($paper_ill_data[$i]['fissuedate'],0,10));
	    		$sheet4->setCellValue('D'.$row,$paper_ill_data[$i]['ffullname']);
	    		$sheet4->setCellValue('E'.$row,$fexpression_c);
	    		$sheet4->getStyle('A'.$row)->applyFromArray($style);
	    		$sheet4->getStyle('B'.$row)->applyFromArray($style);
	    		$sheet4->getStyle('C'.$row)->applyFromArray($style);
	    		$sheet4->getStyle('D'.$row)->applyFromArray($style);
	    		$sheet4->getStyle('E'.$row)->applyFromArray($style);
	    		$sheet4->getStyle('A'.$row)->getAlignment()->setWrapText(false);
	    		$sheet4->getStyle('B'.$row)->getAlignment()->setWrapText(false);
	    		$sheet4->getStyle('C'.$row)->getAlignment()->setWrapText(false);
	    		$sheet4->getStyle('D'.$row)->getAlignment()->setWrapText(false);
	    		$sheet4->getStyle('E'.$row)->getAlignment()->setWrapText(false);
	    		$sheet4->getRowDimension($row)->setRowHeight(13.5);//设置行高
	    		$sheet4->getStyle('A'.$row.':'.'E'.$row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//设置水平居中
	    		$sheet4->getStyle('A'.$row.':'.'E'.$row)->getFont()->setSize(10);//字体大小
	    	}
	    	if($row > 9){
		    	$sheet4->setCellValue('A'.($row+1),'附1：广告类别包括，药品、医疗器械、化妆品、房地产、普通食品、保健食品、烟草、酒、电器、交通产品、知识产品、普通商品、医疗服务、金融服务、信息服务、互联网服务、商业招商投资、教育培训服务、会展文体活动服务、普通服务、形象宣传、非商业及其它。');
		    	$sheet4->setCellValue('A'.($row+2),'附2：按国家市场监督管理总局广告司2018年7月启用的《广告监测标准（2018年7月版）》规定的违法行为代码填写。');
		    	$sheet4->mergeCells('A'.''.($row+1).':'.'E'.''.($row+1));//合并单元格
		    	$sheet4->mergeCells('A'.''.($row+2).':'.'E'.''.($row+2));//合并单元格
		    	$sheet4->getStyle('A'.''.($row+1).':'.'E'.''.($row+1))->applyFromArray($style);
		    	$sheet4->getStyle('A'.''.($row+2).':'.'E'.''.($row+2))->applyFromArray($style);
		    	$sheet4->getStyle('A'.''.($row+1))->getFont()->setSize(10);//设置字体大小
		    	$sheet4->getStyle('A'.''.($row+2))->getFont()->setSize(10);//设置字体大小
		    	$sheet4->getStyle('A'.''.($row+1))->getAlignment()->setWrapText(true);//设置自动换行
		    	$sheet4->getStyle('A'.''.($row+2))->getAlignment()->setWrapText(true);//设置自动换行
		    	$sheet4->getRowDimension($row+1)->setRowHeight(30);//设置行高
		    	$sheet4->getRowDimension($row+2)->setRowHeight(30);//设置行高
	    	}
	    }
	    //表六
	    //设置表头
	    $headtitle = '表六:'.$regulatorRegionInfo['fname1'].$area_name.'级各类别广告发布数监测情况统计表';
	    $sheet5->setCellValue('A1',$headtitle);
	    foreach ($ad_class as $key => $value) {
	    	$row = $key + 4;
	    	$sheet5->setCellValue('A'.$row,$value['adclass']);
	    	$sheet5->setCellValue('B'.$row,$value['count']);
	    	$sheet5->setCellValue('C'.$row,$value['illegal_count']);	    	
	    	$sheet5->setCellValue('E'.$row,$value['total']);	    	
	    	$sheet5->setCellValue('F'.$row,$value['illegal_total']);
	    	if($value['count'] > 0){
		    	$class_count_ill_rate = round($value['illegal_count']/$value['count']*100,2).'%';//条次违法率
		    	$class_total_ill_rate = round($value['illegal_total']/$value['total']*100,2).'%';//时长违法率
		    	$sheet5->setCellValue('D'.$row,$class_count_ill_rate);
		    	$sheet5->setCellValue('G'.$row,$class_total_ill_rate);
		    }
		    $class_count += $value['count'];
		    $class_ill_count += $value['illegal_count'];
		    $class_total += $value['total'];
		    $class_ill_total += $value['illegal_total'];
	    }
	    if($class_count > 0){
	    	$sheet5->setCellValue('B27',$class_count);
	    	$sheet5->setCellValue('C27',$class_ill_count);
	    	$sheet5->setCellValue('D27',round($class_ill_count/$class_count*100,2).'%');
	    	$sheet5->setCellValue('E27',$class_total);	    	
	    	$sheet5->setCellValue('F27',$class_ill_total);
	    	$sheet5->setCellValue('G27',round($class_ill_total/$class_total*100,2).'%');
	    }
	    //表七
	    $headtitle = '表七:'.$regulatorRegionInfo['fname1'].$area_name.'级媒体涉嫌违法的典型广告统计表';
    	$sheet6->setCellValue('A1',$headtitle);
	    if($five_class_illegal){
	    	if(count($five_class_illegal) > 4){
	    		$sheet6->setCellValue('A10',' ');
		    	$sheet6->setCellValue('A11',' ');
		    	$sheet6->unmergeCells('A7:D7');    // 拆分
		    	$sheet6->unmergeCells('A8:D8');    // 拆分
	    	}
	    	foreach ($five_class_illegal as $key => $value) {
	    		$row = $key + 3;
				$fexpressioncodes = $value['fexpressioncodes'];//违法表现代码
				$fexpression_c = '';
				if($fexpressioncodes){
					$fexpressioncodes_arr = explode(';',$fexpressioncodes);
					foreach($fexpressioncodes_arr as $fexpressioncode){
						$fexpression = M('tillegal')->cache(true,3600)->where(array('fcode'=>$fexpressioncode))->getField('fexpression');
						$fexpression_c .= $fexpressioncode.':'.$fexpression."\n";
					}
				}
				
	    		$sheet6->setCellValue('A'.$row,($key+1));
	    		$sheet6->setCellValue('B'.$row,$value['fadname']);
	    		$sheet6->setCellValue('C'.$row,$value['ffullname']);
	    		$sheet6->setCellValue('D'.$row,$fexpression_c);
	    		$sheet6->getStyle('A'.$row)->applyFromArray($style);
	    		$sheet6->getStyle('B'.$row)->applyFromArray($style);
	    		$sheet6->getStyle('C'.$row)->applyFromArray($style);
	    		$sheet6->getStyle('D'.$row)->applyFromArray($style);
	    		$sheet6->getStyle('A'.$row)->getAlignment()->setWrapText(false);
	    		$sheet6->getStyle('B'.$row)->getAlignment()->setWrapText(false);
	    		$sheet6->getStyle('C'.$row)->getAlignment()->setWrapText(false);
	    		$sheet6->getStyle('D'.$row)->getAlignment()->setWrapText(false);
	    		$sheet6->getRowDimension($row)->setRowHeight(13.5);//设置行高
	    		$sheet6->getStyle('A'.$row.':'.'D'.$row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//设置水平居中
	    		$sheet6->getStyle('A'.$row.':'.'D'.$row)->getFont()->setSize(10);//字体大小
	    	}
	    	if($row > 6){
		    	$sheet6->setCellValue('A'.($row+1),'附1：广告类别包括，药品、医疗器械、化妆品、房地产、普通食品、保健食品、烟草、酒、电器、交通产品、知识产品、普通商品、医疗服务、金融服务、信息服务、互联网服务、商业招商投资、教育培训服务、会展文体活动服务、普通服务、形象宣传、非商业及其它。');
		    	$sheet6->setCellValue('A'.($row+2),'附2：按国家市场监督管理总局广告司2018年7月启用的《广告监测标准（2018年7月版）》规定的违法行为代码填写。');
		    	$sheet6->mergeCells('A'.''.($row+1).':'.'D'.''.($row+1));//合并单元格
		    	$sheet6->mergeCells('A'.''.($row+2).':'.'D'.''.($row+2));//合并单元格
		    	$sheet6->getStyle('A'.''.($row+1).':'.'D'.''.($row+1))->applyFromArray($style);
		    	$sheet6->getStyle('A'.''.($row+2).':'.'D'.''.($row+2))->applyFromArray($style);
		    	$sheet6->getStyle('A'.''.($row+1))->getFont()->setSize(10);//设置字体大小
		    	$sheet6->getStyle('A'.''.($row+2))->getFont()->setSize(10);//设置字体大小
		    	$sheet6->getStyle('A'.''.($row+1))->getAlignment()->setWrapText(true);//设置自动换行
		    	$sheet6->getStyle('A'.''.($row+2))->getAlignment()->setWrapText(true);//设置自动换行
		    	$sheet6->getRowDimension($row+1)->setRowHeight(30);//设置行高
		    	$sheet6->getRowDimension($row+2)->setRowHeight(30);//设置行高
	    	}
	    }
	    $objPHPExcel->setActiveSheetIndex(0);//设置打开表一
	    $doc_name = date(YmdHis).rand(10,99);
	    $objWriter->save($_SERVER['DOCUMENT_ROOT'].'/LOG/'.$doc_name.'.xls');
		$fileKey = $doc_name.'.xls';
	    $filePath = 'LOG/'.$doc_name.'.xls';
	    
	    $ret = A('Common/Cloud','Model')->file_up($fileKey,$filePath);
	    $e_data = array();
		if($ret['code'] === 0){
			$e_data['report_url'] = $ret['objectUrl'];//报告URL
			$e_data['make_state'] = 1;//生成成功
		}else{
			$e_data['make_state'] = 2;//生成失败
		}
		M('monitor_report')->where(array('report_id'=>$report_id))->save($e_data);//修改数据库
		unlink('LOG/'.$doc_name.'.xls');
		$this->ajaxReturn(array('code'=>0,'msg'=>$ret['objectUrl']));

	}

	public function report031(){

		set_time_limit(0);
		$this->ajaxReturn(array('code'=>0,'msg'=>'已提交生成报告,请稍后查看'),'',0,'CC');
		$regulatorpersonInfo = M('tregulatorperson')->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
		$regulatorInfo = M('tregulator')->where(array('fcode'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息
		// $regulatorRegionInfo = M('tregion')->field('fid,fname,ffullname')->where(array('fid'=>$regulatorInfo['fregionid']))->find();//监管地区信息信息
		session_write_close();//停止使用session

		$data     = I('data');//所选数据
		$regionid = $data[0]['region'];//地区ID
		$month 	  = $data[0]['month'];//月份
		$regulatorRegionInfo = M('tregion')->field('fid,fname,ffullname')->where(array('fid'=>$regionid))->find();//监管地区信息信息

		$region_id_rtrim = rtrim($regionid,'00');//去掉地区后面的00
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位
		if($tregion_len == 0){
			$area_name = '';
		}elseif($tregion_len == 2){
			$area_name = '省';
		}elseif($tregion_len == 4){
			$area_name = '市';
		}elseif($tregion_len == 6){
			$area_name = '县';
		}else{
			$area_name = '';
		}

		$report_start_date = date('Y-m-01', strtotime($month));//开始时间
		$report_end_date = date('Y-m-d', strtotime("$report_start_date +1 month -1 day"));//结束时间
		if($report_start_date == $report_end_date) $this->ajaxReturn(array('code'=>-1,'msg'=>'开始结束时间相同'));
		$startDate = date('Y-m-d H:i:s',strtotime($report_start_date));
		$endDate = date('Y-m-d H:i:s',strtotime($report_end_date)+86399);
		$report_month = date('n',strtotime($month));//报告时间
		$start_date = date('Y年m月d日',strtotime($report_start_date));//监测开始时间
		$end_date = date('Y年m月d日',strtotime($report_end_date));//监测结束时间

		//自动生成名字
		$report_name = $regulatorRegionInfo['fname'].' '.$report_month.'月份监测报告';
		$a_data['report_name'] = $report_name;//报告名称
		$a_data['report_type'] = 99;//报告类型
		$a_data['fcreator'] = $regulatorpersonInfo['fname'];//创建人
		$a_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$a_data['fmodifier'] = $regulatorpersonInfo['fname'];//修改人
		$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$a_data['regulator_id'] = $regulatorInfo['fid'];//监管机构的ID
		$report_id = M('monitor_report')->add($a_data);	
		// $this->ajaxReturn(array('code'=>0,'msg'=>'已提交生成报告,请稍后查看'),'',0,'CC');

		//获取数据
		$issue_data = A('Gongshang/ReportMake','Model')->get_issue_data($data);
		//var_dump($issue_data);

		$tv_issue_data 	   	= $issue_data['tv'];							 //电视数据
		$tv_media_group    	= $issue_data['tv_media_group'];				 //删除重复电视媒介后的数组
		$tv_count 		   	= count($tv_media_group);						 //统计排重后的电视媒介数
		$tv_media_first	    = array_shift($tv_media_group);					 //取出第一个数组
		$tv_sum 		   	= $issue_data['tv_sum'];						 //电视发布次数
		$tv_illegal_sum    	= $issue_data['tv_illegal_sum'];				 //电视违法发布次数
		$tv_rate			= round($tv_illegal_sum/$tv_sum*100,2).'%';		 //电视发布违法率
		$tv_length 		   	= $issue_data['tv_length'];						 //电视总时长
		$tv_illegal_length 	= $issue_data['tv_illegal_length'];				 //电视违法总时长
		$tv_length_rate		= round($tv_illegal_length/$tv_length*100,2).'%';//电视时长违法率

		$bc_issue_data 		= $issue_data['bc']; 							 //广播数据
		$bc_media_group 	= $issue_data['bc_media_group'];				 //删除重复广播媒介后的数组
		$bc_count 			= count($bc_media_group);						 //统计排重后的广播媒介数
		$bc_media_first	    = array_shift($bc_media_group);					 //取出第一个数组
		$bc_sum				= $issue_data['bc_sum'];						 //广播发布次数
		$bc_illegal_sum		= $issue_data['bc_illegal_sum'];				 //广播违法发布次数
		$bc_rate			= round($bc_illegal_sum/$bc_sum*100,2).'%';		 //广播发布违法率
		$bc_length			= $issue_data['bc_length'];						 //广播总时长
		$bc_illegal_length  = $issue_data['bc_illegal_length'];				 //广播违法总时长
		$bc_length_rate		= round($bc_illegal_length/$bc_length*100,2).'%';//广播时长违法率

		$paper_issue_data 	= $issue_data['paper']; 						 //报纸数据
		$paper_media_group 	= $issue_data['paper_media_group'];				 //删除重复报纸媒介后的数组
		$paper_count 		= count($paper_media_group);					 //统计排重后的报纸媒介数
		$paper_media_first	= array_shift($paper_media_group);				 //取出第一个数组
		$paper_sum			= $issue_data['paper_sum'];					 	 //报纸发布次数
		$paper_illegal_sum	= $issue_data['paper_illegal_sum'];			 	 //报纸违法发布次数
		$paper_rate			= round($paper_illegal_sum/$paper_sum*100,2).'%';//报纸发布违法率

		//合计
		$total_sum 			= $tv_sum + $bc_sum + $paper_sum;				 
		$total_illegal_sum 	= $tv_illegal_sum + $bc_illegal_sum + $paper_illegal_sum;
		$total_rate			= round($total_illegal_sum/$total_sum*100,2).'%';
		$total_length		= $tv_length + $bc_length;
		$total_ill_length 	= $tv_illegal_length + $bc_illegal_length;
		$total_length_rate	= round($total_ill_length/$total_length*100,2).'%';

		$ad_class 			= $issue_data['ad_class'];//广告类别统计
		$five_class_illegal = $issue_data['five_class_illegal'];//违法广告五大类

		//拼接html
		$html = '<html style="font-family:仿宋" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">
				<head> 
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				<xml><w:WordDocument><w:View>Print</w:View></xml>
				<style>
					@page
					    {mso-page-border-surround-header:no;
					    mso-page-border-surround-footer:no;}
					@page Section1
					    {size:841.9pt 595.3pt;
					    mso-page-orientation:landscape;
					    margin:89.85pt 72.0pt 89.85pt 72.0pt;
					    mso-header-margin:42.55pt;
					    mso-footer-margin:49.6pt;
					    mso-paper-source:0;
					    layout-grid:15.6pt;}
					div.Section1
					    {page:Section1;}
					table td{text-align:center;padding-left:6px;padding-right:6px;}
				</style>
				</head>
				<body>
				<div class="Section1">
				<h2 style="text-align:center">'.$regulatorRegionInfo['fname'].$area_name.'广告监测报告('.$report_month.'月份)</h2>';

		//表一
		$html .= '<h3>表一:'.$regulatorRegionInfo['fname'].$area_name.$area_name.'级媒体广告监测情况及目录</h3>';
		$html .= '<table border="1" cellspacing="0" width="100%">';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td>电视(个)</td>';
		$html .= '<td>广播频率(个)</td>';
		$html .= '<td>报纸(家)</td>';
		$html .= '</tr>';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td>'.$tv_count.'</td>';
		$html .= '<td>'.$bc_count.'</td>';
		$html .= '<td>'.$paper_count.'</td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '<h3>监测目录</h3>';
		$html .= '<table border="1" cellspacing="0" width="100%">';
		$html .= '<tr style="padding:6px 0">';
		$html .= '<td rowspan="'.$tv_count.'" style="width:25%">'.$area_name.'级电视媒体（频道）</td>';
		$html .= '<td style="padding:6px 0">'.$tv_media_first['fmedianame'].'</td>';
		$html .= '<td style="padding:6px 0">'.trim($tv_media_first['day'],',').'</td>';
		$html .= '</tr>';
		foreach ($tv_media_group as $key => $value) {
			$html .= '<tr style="padding:6px 0">';
			$html .= '<td>'.$value['fmedianame'].'</td>';
			$html .= '<td>'.trim($value['day'],',').'</td>';
			$html .= '</tr>';
		}
		$html .= '<tr style="padding:6px 0">';
		$html .= '<td rowspan="'.$bc_count.'" style="width:25%">'.$area_name.'级广播媒体（频率）</td>';
		$html .= '<td style="padding:6px 0">'.$bc_media_first['fmedianame'].'</td>';
		$html .= '<td style="padding:6px 0">'.trim($bc_media_first['day'],',').'</td>';
		$html .= '</tr>';
		foreach ($bc_media_group as $key => $value) {
			$html .= '<tr style="padding:6px 0">';
			$html .= '<td>'.$value['fmedianame'].'</td>';
			$html .= '<td>'.trim($value['day'],',').'</td>';
			$html .= '</tr>';
		}
		$html .= '<tr style="padding:6px 0">';
		$html .= '<td rowspan="'.$paper_count.'" style="width:25%">'.$area_name.'级报纸</td>';
		$html .= '<td style="padding:6px 0">'.$paper_media_first['fmedianame'].'</td>';
		$html .= '<td style="padding:6px 0">'.trim($paper_media_first['day'],',').'</td>';
		$html .= '</tr>';
		foreach ($paper_media_group as $key => $value) {
			$html .= '<tr style="padding:6px 0">';
			$html .= '<td>'.$value['fmedianame'].'</td>';
			$html .= '<td>'.trim($value['day'],',').'</td>';
			$html .= '</tr>';
		}
		$html .= '</table>';
		$html .= '<br clear=all style="page-break-before:always" mce_style="page-break-before:always">';

		//表二
		$html .= '<h3>表二：'.$regulatorRegionInfo['fname'].$area_name.$area_name.'级媒体广告监测概表<h3>';
		$html .= '<table border="1" cellspacing="0" width="100%">';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td style="width:10.5%">媒体类型</td>';
		$html .= '<td style="width:21%">监测总数（条次）</td>';
		$html .= '<td style="width:26%">涉嫌违法广告（条次）</td>';
		$html .= '<td style="width:13%">条次违法率</td>';
		$html .= '<td style="width:16%">涉嫌违法时长(秒)</td>';
		$html .= '<td style="width:13.5%">时长违法率</td>';
		$html .= '</tr>';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td>电视</td>';
		$html .= '<td>'.$tv_sum.'</td>';
		$html .= '<td>'.$tv_illegal_sum.'</td>';
		$html .= '<td>'.$tv_rate.'</td>';
		$html .= '<td>'.$tv_illegal_length.'</td>';
		$html .= '<td>'.$tv_length_rate.'</td>';
		$html .= '</tr>';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td>广播</td>';
		$html .= '<td>'.$bc_sum.'</td>';
		$html .= '<td>'.$bc_illegal_sum.'</td>';
		$html .= '<td>'.$bc_rate.'</td>';
		$html .= '<td>'.$bc_illegal_length.'</td>';
		$html .= '<td>'.$bc_length_rate.'</td>';
		$html .= '</tr>';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td>报纸</td>';
		$html .= '<td>'.$paper_sum.'</td>';
		$html .= '<td>'.$paper_illegal_sum.'</td>';
		$html .= '<td>'.$paper_rate.'</td>';
		$html .= '<td></td>';
		$html .= '<td></td>';
		$html .= '</tr>';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td>合计</td>';
		$html .= '<td>'.$total_sum.'</td>';
		$html .= '<td>'.$total_illegal_sum.'</td>';
		$html .= '<td>'.$total_rate.'</td>';
		$html .= '<td>'.$total_ill_length.'</td>';
		$html .= '<td>'.$total_length_rate.'</td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '<br clear=all style="page-break-before:always" mce_style="page-break-before:always">';

		//表三
		$html .= '<h3>表三：'.$regulatorRegionInfo['fname'].$area_name.$area_name.'级电视媒体涉嫌违法广告统计表<h3>';
		$html .= '<table border="1" cellspacing="0" width="100%">';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td>发布媒体</td>';
		$html .= '<td>涉嫌违法广告名称</td>';
		$html .= '<td>发布日期</td>';
		$html .= '<td>时长(秒)</td>';
		$html .= '<td>广告类别</td>';
		$html .= '<td style="width:25%">违法行为表现</td>';
		$html .= '</tr>';
		$tv_ill_count = 0;
		foreach ($tv_issue_data as $key => $value) {
			if($value['serious_ill_count'] > 0){
				$html .= '<tr style="padding:8px 0">';
				$html .= '<td>'.$value['fmedianame'].'</td>';
				$html .= '<td>'.$value['fadname'].'('.$value['serious_ill_count'].'次)</td>';
				$html .= '<td>'.$value['fissuedate'].'</td>';
				$html .= '<td>'.$value['illegal_total'].'</td>';
				$html .= '<td>'.$value['fname'].'</td>';
				$html .= '<td>'.$value['fexpressioncodes'].'</td>';
				$html .= '</tr>';
				$tv_ill_count += 1;
			}			
		}
		//数量为0时生成3个空行
		if($tv_ill_count == 0){
			for($i = 0;$i < 3;$i++){
				$html .= '<tr style="height:30px;">';
				$html .= '<td></td><td></td><td></td><td></td><td></td><td></td>';
				$html .= '</tr>';
			}
		}
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td colspan="6" style="text-align:left;">';
		$html .= '附1：广告类别包括，药品、医疗器械、化妆品、房地产、普通食品、保健食品、烟草、酒、电器、交通产品、知识产品、普通商品、医疗服务、金融服务、信息服务、互联网服务、商业招商投资、教育培训服务、会展文体活动服务、普通服务、形象宣传、非商业及其它。';
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td colspan="6" style="text-align:left;">';
		$html .= '附2：按国家市场监督管理总局广告司2018年7月启用的《广告监测标准（2018年7月版）》规定的违法行为代码填写。';
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '<br clear=all style="page-break-before:always" mce_style="page-break-before:always">';

		//表四
		$html .= '<h3>表四：'.$regulatorRegionInfo['fname'].$area_name.$area_name.'级广播媒体涉嫌违法广告统计表<h3>';
		$html .= '<table border="1" cellspacing="0" width="100%">';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td>发布媒体</td>';
		$html .= '<td>涉嫌违法广告名称</td>';
		$html .= '<td>发布日期</td>';
		$html .= '<td>时长(秒)</td>';
		$html .= '<td>广告类别</td>';
		$html .= '<td style="width:25%">违法行为表现</td>';
		$html .= '</tr>';
		$bc_ill_count = 0;
		foreach ($bc_issue_data as $key => $value) {
			if($value['serious_ill_count'] > 0){
				$html .= '<tr style="padding:8px 0">';
				$html .= '<td>'.$value['fmedianame'].'</td>';
				$html .= '<td>'.$value['fadname'].'('.$value['serious_ill_count'].'次)</td>';
				$html .= '<td>'.$value['fissuedate'].'</td>';
				$html .= '<td>'.$value['illegal_total'].'</td>';
				$html .= '<td>'.$value['fname'].'</td>';
				$html .= '<td>'.$value['fexpressioncodes'].'</td>';
				$html .= '</tr>';
				$bc_ill_count += 1;
			}			
		}
		//数量为0时生成3个空行
		if($bc_ill_count == 0){
			for($i = 0;$i < 3;$i++){
				$html .= '<tr style="height:30px;">';
				$html .= '<td></td><td></td><td></td><td></td><td></td><td></td>';
				$html .= '</tr>';
			}
		}
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td colspan="6" style="text-align:left;">';
		$html .= '附1：广告类别包括，药品、医疗器械、化妆品、房地产、普通食品、保健食品、烟草、酒、电器、交通产品、知识产品、普通商品、医疗服务、金融服务、信息服务、互联网服务、商业招商投资、教育培训服务、会展文体活动服务、普通服务、形象宣传、非商业及其它。';
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td colspan="6" style="text-align:left;">';
		$html .= '附2：按国家市场监督管理总局广告司2018年7月启用的《广告监测标准（2018年7月版）》规定的违法行为代码填写。';
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '<br clear=all style="page-break-before:always" mce_style="page-break-before:always">';

		//表 五
		$html .= '<h3>表五：'.$regulatorRegionInfo['fname'].$area_name.$area_name.'级报纸媒体涉嫌违法广告统计表<h3>';
		$html .= '<table border="1" cellspacing="0" width="100%">';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td>发布媒体</td>';
		$html .= '<td>涉嫌违法广告名称</td>';
		$html .= '<td>发布日期</td>';
		$html .= '<td>广告类别</td>';
		$html .= '<td style="width:25%">违法行为表现</td>';
		$html .= '</tr>';
		$paper_ill_count = 0;
		foreach ($paper_issue_data as $key => $value) {
			if($value['serious_ill_count'] > 0){
				$html .= '<tr style="padding:8px 0">';
				$html .= '<td>'.$value['fmedianame'].'</td>';
				$html .= '<td>'.$value['fadname'].'('.$value['serious_ill_count'].'次)</td>';
				$html .= '<td>'.$value['fissuedate'].'</td>';
				$html .= '<td>'.$value['fname'].'</td>';
				$html .= '<td>'.$value['fexpressioncodes'].'</td>';
				$html .= '</tr>';
				$paper_ill_count += 1;
			}			
		}
		//数量为0时生成3个空行
		if($paper_ill_count == 0){
			for($i = 0;$i < 3;$i++){
				$html .= '<tr style="height:30px;">';
				$html .= '<td></td><td></td><td></td><td></td><td></td>';
				$html .= '</tr>';
			}
		}
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td colspan="5" style="text-align:left;">';
		$html .= '附1：广告类别包括，药品、医疗器械、化妆品、房地产、普通食品、保健食品、烟草、酒、电器、交通产品、知识产品、普通商品、医疗服务、金融服务、信息服务、互联网服务、商业招商投资、教育培训服务、会展文体活动服务、普通服务、形象宣传、非商业及其它。';
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td colspan="5" style="text-align:left;">';
		$html .= '附2：按国家市场监督管理总局广告司2018年7月启用的《广告监测标准（2018年7月版）》规定的违法行为代码填写。';
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '<br clear=all style="page-break-before:always" mce_style="page-break-before:always">';

		//表六
		$html .= '<h3>表六：'.$regulatorRegionInfo['fname'].$area_name.$area_name.'级各类别广告发布数监测情况统计表</h3>';
		$html .= '<table border="1" cellspacing="0" width="100%">';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td rowspan="2" style="width:32%">广告类别</td>';
		$html .= '<td colspan="3" style="width:34%;text-align:center">广告条次</td>';
		$html .= '<td colspan="3" style="width:34%;text-align:center">广告时长</td>';
		$html .= '</tr>';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td>总条次</td>';
		$html .= '<td>违法条次</td>';
		$html .= '<td>违法率</td>';
		$html .= '<td>总时长(秒)</td>';
		$html .= '<td>违法时长(秒)</td>';
		$html .= '<td>违法率</td>';
		$html .= '</tr>';
		foreach ($ad_class as $key => $value) {
			$html .= '<tr style="padding:8px 0">';
			$html .= '<td>'.$value['fname'].'</td>';
			$html .= '<td>'.$value['count'].'</td>';
			$html .= '<td>'.$value['illegal_count'].'</td>';
			$html .= '<td>'.round($value['illegal_count']/$value['count']*100,2).'%</td>';
			$html .= '<td>'.$value['total'].'</td>';
			$html .= '<td>'.$value['illegal_total'].'</td>';
			$html .= '<td>'.round($value['illegal_total']/$value['total']*100,2).'%</td>';
			$html .= '</tr>';
		}
		$html .= '</table>';
		$html .= '<p style="font-size:15px;font-weight:normal">广告类别包括：药品、医疗器械、化妆品、房地产、普通食品、保健食品、烟草、酒、电器、交通产品、知识产品、普通商品、医疗服务、金融服务、信息服务、互联网服务、商业招商投资、教育培训服务、会展文体活动服务、普通服务、形象宣传、非商业及其它。</p><br clear=all style="page-break-before:always" mce_style="page-break-before:always">';

		//表7
		$html .= '<h3>表七：'.$regulatorRegionInfo['fname'].$area_name.$area_name.'级媒体涉嫌违法的典型广告统计表</h3>';
		$html .= '<table border="1" cellspacing="0" width="100%">';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td style="width:8%">序号</td>';
		$html .= '<td style="width:16%">广告名称</td>';
		$html .= '<td style="width:16%">广告类别</td>';
		$html .= '<td style="width:35%">违法行为表现</td>';
		$html .= '</tr>';
		$five_class_ill_count = 0;
		foreach ($five_class_illegal as $key => $value) {
			$name = explode('>',$value['ffullname']);
			//优先排序严重违法其次再根据总违法数排序
			if($value['serious_ill_count'] > 0){
				$illegal_str = '严重违法'.$value['serious_ill_count'].'次';
			}else{
				$illegal_str = '违法'.$value['illegal_count'].'次';
			}
			$html .= '<tr style="padding:8px 0">';
			$html .= '<td>'.($key + 1).'</td>';
			$html .= '<td>'.$value['fadname'].'('.$illegal_str.')'.'</td>';
			$html .= '<td>'.$name[0].'</td>';
			$html .= '<td>'.$value['fexpressioncodes'].'</td>';
			$html .= '</tr>';
			$five_class_ill_count += 1;
		}
		//数量为0时生成3个空行
		if($five_class_ill_count == 0){
			for($i = 0;$i < 3;$i++){
				$html .= '<tr style="height:30px;">';
				$html .= '<td></td><td></td><td></td><td></td>';
				$html .= '</tr>';
			}
		}
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td colspan="4" style="text-align:left;">';
		$html .= '附1：广告类别为五大类，药品、医疗器械、化妆品、保健食品、医疗服务。';
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr style="padding:8px 0">';
		$html .= '<td colspan="4" style="text-align:left;">';
		$html .= '附2：按国家市场监督管理总局广告司2018年7月启用的《广告监测标准（2018年7月版）》规定的违法行为代码填写。';
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '</div></body></html>';

		$doc_name = date(YmdHis).rand(10,99);
		file_put_contents('LOG/'.$doc_name.'.doc', $html);
		$fileKey = $doc_name.'.doc';
	    $filePath = 'LOG/'.$doc_name.'.doc';
	    
	    $ret = A('Common/Cloud','Model')->file_up($fileKey,$filePath);
	    $e_data = array();
		if($ret['code'] === 0){
			$e_data['report_url'] = $ret['objectUrl'];//报告URL
			$e_data['make_state'] = 1;//生成成功
		}else{
			$e_data['make_state'] = 2;//生成失败
		}
		M('monitor_report')->where(array('report_id'=>$report_id))->save($e_data);//修改数据库
		unlink('LOG/'.$doc_name.'.doc');
		$this->ajaxReturn(array('code'=>0,'msg'=>'已提交生成报告,请稍后查看'));
	}
	
}
