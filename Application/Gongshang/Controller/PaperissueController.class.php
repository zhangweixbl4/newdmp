<?php
//报纸广告发行控制器
namespace Gongshang\Controller;
use Think\Controller;
class PaperissueController extends BaseController {

    public function index(){
		$regulatorpersonInfo = M('tregulatorperson')->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
		$regulatorInfo = M('tregulator')->where(array('fid'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息

		$region_id = session('regulatorpersonInfo.regionid');//地区ID
		if(I('region_id') != '') $region_id = I('region_id');
		$this_region_id = I('this_region_id');//是否只查询本级地区

		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$keyword = I('keyword');//搜索关键词
		$fadname = I('fadname');// 广告名称
		$fversion = I('fversion');// 版本描述
		$fmediaid  = I('fmediaid');// 媒体ID
		$adclass_code = I('adclass_code');//广告类别
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID
		$fstarttime_s = I('fstarttime_s');// 发布日期
		if($fstarttime_s == ''){
			$fstarttime_s = date('Y-m-d');
			$_GET['fstarttime_s'] = $fstarttime_s;
		}	
		$fstarttime_e = I('fstarttime_e');// 发布日期
		if($fstarttime_e == ''){
			$fstarttime_e = date('Y-m-d');
			$_GET['fstarttime_e'] = $fstarttime_e;
		}

		$where = array();//查询条件

		if($fmediaid == ''){
			//是否有指定的媒体
			$media=$this->get_access_media();//获取权限判断后的最终媒体列表
			if($media){
				$where['tpaperissue.fmediaid'] = array('in', $media);//发布媒介
			}
		}else{
			$where['tpaperissue.fmediaid'] = $fmediaid;//发布媒介
		}

		//如果只查询当前级
		if($this_region_id == 'true'){
			$where['tmediaowner.fregionid'] = $region_id;//地区搜索条件
		}else{
			if($region_id !=100000 && (I('region_id') ||empty($media))){//不是国家工商局并且有地区检索或者没有media
				$region_id_rtrim = rtrim($region_id, '00');//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmediaowner.fregionid,' . $region_id_strlen . ')'] = $region_id_rtrim;//地区搜索条件
			}
		}
		$where['tpaperissue.fstate'] = array('neq',-1);

		//如果没有检索$adclass_code 并且有special_adclass
		if(session('regulatorpersonInfo.special_adclass') &&empty($adclass_code)){

			$where['tadclass.fcode|tadclass.fpcode'] = array('in',session('regulatorpersonInfo.special_adclass'));//code或者pcode 是special_adclass
		}
		//如果有检索$adclass_code
		if(!empty($adclass_code)){
			$where['tadclass.fcode|tadclass.fpcode'] = $adclass_code;//广告名称
		}

		if($keyword != ''){
			
		} 
	
		if($fadname != ''){
			$where['tad.fadname'] = array('like','%'.$fadname.'%');//广告名称
		}
		if($fversion != ''){
			$where['tpapersample.fversion'] = array('like','%'.$fversion.'%');//版本描述
		}
		if(is_array($fillegaltypecode)){
			$where['tpapersample.fillegaltypecode'] = array('in',$fillegaltypecode);//违法类型代码
		}
		if($fstarttime_s != '' || $fstarttime_e != ''){
			$where['tpaperissue.fissuedate'] = array('between',$fstarttime_s.','.$fstarttime_e);
		}
		
		$special_brand = trim($regulatorInfo['special_brand']);
		if($special_brand != ''){
			$special_brand_list = explode(',',$special_brand);
			$where['tad.fbrand'] = array('in',$special_brand_list);//广告品牌
		}
		$count = M('tpaperissue')
								->join('tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
								->join('tad on tad.fadid = tpapersample.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode', 'LEFT')//广告内容列别
								->join('tmedia on tmedia.fid = tpaperissue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->join('tillegaltype on tillegaltype.fcode = tpapersample.fillegaltypecode')
								->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数

		
		$paperissueList = M('tpaperissue')
										->field('tpaperissue.*,tad.fadname,tpapersample.fversion,tpapersample.fjpgfilename,tmedia.fmedianame,tillegaltype.fillegaltype,tadclass.ffullname as adclass_code')
										->join('tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
										->join('tad on tad.fadid = tpapersample.fadid')
										->join('tadclass on tadclass.fcode = tad.fadclasscode', 'LEFT')//广告内容列别
										->join('tmedia on tmedia.fid = tpaperissue.fmediaid')
										->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
										->join('tillegaltype on tillegaltype.fcode = tpapersample.fillegaltypecode')
										->where($where)
										->order('tpaperissue.fissuedate desc')
										->limit($Page->firstRow.','.$Page->listRows)->select();//查询广告样本列表
		//aa(M('tpaperissue')->_sql());
									
		$paperissueList = list_k($paperissueList,$p,$pp);//为列表加上序号
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		//var_dump($paperissueList);
		$this->assign('regionid',session('regulatorpersonInfo.regionid'));
		$this->assign('paperissueList',$paperissueList);//广告发布列表
		$this->assign('illegaltype',$illegaltype);//违法类型
		$this->assign('regulatorInfo',$regulatorInfo);//监管机构信息
		$this->assign('page',$Page->show());//分页输出
		// $this->display();
	}

	
	
	/*广告发布详情*/
	public function ajax_paperissue_details(){
		
		$fpaperissueid = I('fpaperissueid');//获取发布ID
		$fpaperissueid = intval($fpaperissueid);
		$paperissueDetails = M('tpaperissue')
										->field('tpaperissue.*,tad.fadname,tpapersample.fversion,tmedia.fmedianame,tillegaltype.fillegaltype')
										->join('tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
										->join('tad on tad.fadid = tpapersample.fadid')
										->join('tadowner on tadowner.fid = tad.fadowner')
										->join('tillegaltype on tillegaltype.fcode = tpapersample.fillegaltypecode')
										->join('tmedia on tmedia.fid = tpaperissue.fmediaid')
										->where(array('tpaperissue.fpaperissueid'=>$fpaperissueid))
										->find();//查询样本详情
		$this->ajaxReturn(array('code'=>0,'paperissueDetails'=>$paperissueDetails));
	}
}