const taskInfo2 = {
  template: `<div v-loading="loading" class="task-info">
    <div class="ad-text" v-show="showAdText && task.sample_to_word">{{task.sample_to_word}}</div>
    <div style="height: 60px;overflow: auto;">
      <el-row v-if="sameAdNameTaskList.length > 0">
        <div>
          下列为供参考的部分同名广告, 点击按钮查看详情, 红色为已判定的违法广告,
          <span style="color: red;">导入时请注意违法判定</span>
        </div>
        <div class="sameAdNameTaskList">
          <el-button :type="item.fillegaltypecode > 0 ? 'danger' : 'info'" v-for="(item, index) in sameAdNameTaskList"
            @click="checkTask(item.taskid)" :size="item.fillegaltypecode > 0 ? 'medium' : 'mini'" :key="index" plain>{{task.media_class
            === '3' ? '广告' + (index+1) : '广告时长' + item.fadlen}}</el-button>
        </div>
        <el-dialog title="任务详情" :modal="false" :visible="showTaskInfo" @close="closeTaskInfo" width="90%" class="subTaskInfo">
          <task-info :task="checkTaskInfo" ref="task" @next="closeTaskInfo" @back="closeTaskInfo" @fillTaskData="receiveFillTaskData"></task-info>
        </el-dialog>
      </el-row>
    </div>
    <hr>
    <el-row>
      <el-col :span="12">
        <el-row>
          <div class="sample-title">
            <el-button type="text" @click="openCutErrorForm">提交剪辑错误</el-button>
            <div>任务状态: {{task.task_state | taskStateFilter}} {{task.quality_state == 2 ? ' , 已质检' : ''}}</div>
            <div v-if="task.task_state == 1 && type === 'input'">任务领取时间: {{receiveTime}}</div>
          </div>
        </el-row>
        <div class="btnArea">
          <el-row v-if="task.task_state == 4" class="task-back">
            <div class="describe">该任务被退回</div>
            <div class="reason">退回理由: {{task.back_reason}}</div>
          </el-row>
          <el-row v-if="task.cut_err_state == 1" class="task-back">
            <div class="describe">该任务为剪辑错误</div>
            <div class="reason">剪辑错误说明: {{task.cut_err_reason}}</div>
          </el-row>
        </div>
        <el-row>
          <b>任务id</b> : {{task.taskid}}
        </el-row>
        <el-row>
          <b>UUID</b> : {{ task.sam_uuid }}
        </el-row>
        <el-row>
          <b>播出媒体</b> : {{task.media_name}}
          <span v-if="adType == 'tv'" style="display: inline-block;width: 50px;">
            <img :src="task.tv_logo" alt="" style="width: 50px;">
          </span>
        </el-row>
        <el-row>
          <b>发布时间</b> : {{task.issue_date}}
        </el-row>
        <el-row v-if="task.media_class != 3">
          <b>视频/音频长度</b> : {{task.fadlen}}
        </el-row>
        <el-row v-if="task.fromMediaName != ''">
          <b style="color:red;">转播自</b> : {{task.fromMediaName}}
        </el-row>
        <el-row class="sample-content">
          <div class="text-center" v-if="adType === 'paper'">
            <el-button @click="confirmPaper" v-if="!task.paperIsSure" type="info">确认版面无误后, 点击查看广告</el-button>
            <el-button @click="checkPaper" v-else type="info">查看版面</el-button>
          </div>
          <div>
            <div v-if="task.media_class == 1 || task.media_class == 2">
              <el-button v-if="task.sampleSource != 'null'" @click="toggleAliPlayer" v-else type="info">切换播放场景/原视频</el-button>
              <div v-show="showAliPlayer">
                <span>本播放模式前后会多播放11-20秒,属于正常现象</span>
                <div class="prism-player" id="J_prismPlayer"></div>
              </div>
              <div v-show="!showAliPlayer">
                <span v-if="task.sampleSource !== 'null'">如果视频/音频剪辑不完整,先点击上方按钮查看发布场景,如果发布场景也有问题,再提交剪辑错误</span>
                <a :href="task.source_path">视频/音频地址</a>
                <div v-if="adType=='tv'">
                  <video ref="player" controls :src="task.source_path"></video>
                </div>
                <div v-if="adType=='bc'">
                  <audio ref="player" :src="task.source_path" controls></audio>
                </div>
              </div>
            </div>
            <div v-if="adType=='paper'">
              <img :src="task.paperUrl" alt="" v-if="!task.paperIsSure">
              <img :src="task.source_path" alt="" v-else>
            </div>
          </div>
          <div class="rate" v-if="adType!='paper'">
            <b>播放速度</b>
            <el-radio-group v-model="rate" @change="rateChange">
              <el-radio :label="0.75">0.75</el-radio>
              <el-radio :label="1">1</el-radio>
              <el-radio :label="1.25">1.25</el-radio>
              <el-radio :label="1.5">1.5</el-radio>
              <el-radio :label="2.0">2.0</el-radio>
            </el-radio-group>
          </div>
        </el-row>
      </el-col>
      <el-col :span="11" :offset="1">
        <div v-if="taskTip && type != 'review'">
          此任务为<b style="color: red;font-size: 20px"> {{isquality ? '违法判定' : '广告录入'}} </b>任务
        </div>
        <el-button v-if="showFillBtn" @click="fillTaskData">将此任务广告及判定信息导入到正在录入的任务</el-button>
        <el-tabs v-model="activeTabName" type="border-card">
          <el-tab-pane label="广告信息录入" name="adInfo">
            <ad-info v-show="activeTabName === 'adInfo'" :task="task" :type="adInputType" :from-manage="fromManage" ref="adInfo" @inputAdName="searchSameAdNameTask" @focus="() => showAdText = true" @blur="() => showAdText = false" @next="submitSuccess(1)" @close="() => $emit('back', 1)" @nextTask="() => $emit('next', 1)" :show-button="!isquality"></ad-info>
          </el-tab-pane>
          <el-tab-pane label="违法判定" name="illegalInfo">
            <illegal-info v-show="activeTabName === 'illegalInfo'" :ringtype="ringtype" :task="task" :type="illegalInputType" :from-manage="fromManage" ref="illegalInfo" @next="submitSuccess(2)" @focus="() => showAdText = true" @blur="() => showAdText = false" @close="() => $emit('back', 1)" :show-button="false"></illegal-info>
          </el-tab-pane>
        </el-tabs>
        <div style="text-align: center;margin-top: 10px;" v-if="(isquality || ringtype === 'review') || showButton">
          <el-button type="primary" @click="submit()">提交</el-button>
          <el-button type="success" @click="changeLanguage" v-if="task.media_class !== '13'">转交方言</el-button>
        </div>
      </el-col>
    </el-row>
    <el-dialog title="请选择剪辑错误" :visible.sync="showCutErrorForm" :modal="false">
      <submit-cut-error :taskid="task.taskid" :media-class="task.media_class" ref="submitCutError" @success="submitCutErrorSuccess"></submit-cut-error>
    </el-dialog>
  </div>`,
  props: {
    task: {
      type: Object,
      default: () => ({})
    },
    fromManage: {
      type: Boolean,
      default: false
    },
    showButton: {
      type: Boolean,
      default: false
    },
    ringtype: {
      type: String,
      default: ''
    },
    taskTip: {
      type: Boolean,
      default: true
    }
  },
  components: {
    taskInfo: this,
    submitCutError,
    adInfo,
    illegalInfo,
  },
  name: 'taskInfo',
  data: function () {
    return {
      loading: false,
      viewerInstance: {
        destroy: () => {}
      },
      adClassTree,
      illegalTypeArr,
      adClassArr,
      illegalArr,
      type,
      rate: 1,
      activeTabName: 'adInfo',
      remainTime: 1200,
      intervalId: 0,
      sameAdNameTaskList: [],
      showTaskInfo: false,
      checkTaskInfo: {},
      showCutErrorForm: false,
      showFillBtn: false,
      aliPlayer: null,
      showAliPlayer: false,
      showAdText: false
    }
  },
  computed: {
    adType: function () {
      const obj = {
        '1': 'tv',
        '2': 'bc',
        '3': 'paper'
      }
      return obj[this.task.media_class] || null
    },
    isquality() {
      // 如果不是广告录入页面
      if (this.type !== 'input' || this.showButton) {
        return true
      }

      if (this.task.linkTypeArr) {
        // state 1的环节是当前进行中的环节，4是退回的环节
        // type 1录入环节 2判定环节
        const _isquality = this.task.linkTypeArr.filter((v) => v.state === '1')
        const isquality = this.task.linkTypeArr.filter((v) => (v.state === '1' || v.state === '4') && v.type === '2')
        if (isquality.length === 1 && _isquality.length === 2) {
          return false
        } else if(isquality.length === 1 && _isquality.length === 1) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    isIllegal: function () {
      return this.task.fillegaltypecode !== '0'
    },
    receiveTime: function () {
      return this.task.lasttime ? moment.unix(this.task.lasttime).format('YYYY-MM-DD HH:mm:ss') : ''
    },
    adInputType: function () {
      if (this.fromManage) return 'input'
      if(this.ringtype === 'review') return 'quality'
      if (this.isquality) return 'input'

      if (this.type !== 'quality' && this.task.linkTypeArr) {
        let res = this.task.linkTypeArr.filter((v) => v.type === '1' && (v.state === '1' || v.state === '4'))
        if (res.length > 0) {
          return 'input'
        } else {
          return 'quality'
        }
      }
      return 'quality'
    },
    illegalInputType: function () {
      if (this.fromManage) return 'input'
      if (this.type !== 'quality' && this.task.linkTypeArr && this.isquality) {
        let res = this.task.linkTypeArr.filter((v) => v.type === '2' && (v.state === '1' || v.state === '4'))
        if (res.length > 0) {
          return 'input'
        } else {
          return 'quality'
        }
      }
      return 'quality'
    }
  },
  methods: {
    changeLanguage() {
      this.$confirm('确认转交为方言处理?', '提示', {
        confirmButtonText: '继续',
        cancelButtonText: '取消',
        type: 'success',
      }).then(() => {
        $.post('/TaskInput/TraditionAd/setDialect', {
        taskid: this.task.taskid
      }).then((res) => {
        if(res.code == 0) {
            this.$emit('next', 1)
          }
        }).catch(err => {
          this.$message.error(err.msg)
        })
      }).catch(err => {
      })
    },
    submit() {
      const p1 = this.$refs.adInfo.initData()
      const p2 = this.$refs.illegalInfo.initData()
      Promise.all([p1, p2]).then(res => {
        const [p1, p2] = res
        const data = { ...p1, ...p2 }
        let text = data.change ? '检测到修改了录入信息，提交立即扣除录入人积分，确认提交？' : '确认提交?'
        this.$confirm(text).then(() => {
          this.$refs.illegalInfo.setLoading(true)
          $.post('submitJudgeTask', { data }).then((res) => {
            if (res.code == 0) {
              if (this.fromManage) {
                return this.$emit('back', 1)
              }
              this.submitSuccess(2)
              this.imgurl = {}
            } else {
              this.$message({
                message: res.msg,
                type: 'error'
              })
            }
            this.$refs.illegalInfo.setLoading(false)
          })
        }).catch(() => {
          this.$refs.illegalInfo.setLoading(false)
        })
      }).catch(e => {
      })
    },
    dataInit: function () {
      clearInterval(this.intervalId)
      this.remainTime = 1200
      this.showCutErrorForm = false
      this.showFillBtn = false
      this.loading = false
      this.type = type
      this.activeTabName = 'adInfo'
      this.sameAdNameTaskList = []
      this.checkTaskInfo = {}
      this.showAliPlayer = false
      if (this.aliPlayer) {
        this.aliPlayer.dispose()
      }
      if (this.adType === 'paper') {
        if (this.isquality) {
          Vue.set(this.task, 'paperIsSure', true)
        } else {
          Vue.set(this.task, 'paperIsSure', false)
        }
        this.viewerInit()
      } else {
        try {
          this.viewerInstance.destroy()
        } catch (e) {

        }
        if (this.task.sampleSource !== 'null') {
          this.$nextTick(() => {
            this.aliPlayer = new Aliplayer({
              id: 'J_prismPlayer',
              autoplay: false,
              useH5Prism: true,
              width: '100%',
              height: '500px',
              replay: true,
              source: this.task.sampleSource,
              waitingTimeout: 900,
            })
          })
        } else {
          this.aliPlayer = null
        }
        this.rateChange()
      }
      this.task.is_long_ad = this.task.is_long_ad || '0'
      try {
        if (this.task.fadclasscode) {
          Vue.set(this.task, 'adCateArr', this._processAdClass(this.task.fadclasscode))
        } else {
          Vue.set(this.task, 'adCateArr', [])
          Vue.set(this.task, 'fadclasscode', '')
        }
        if (this.task.fadclasscode_v2) {
          Vue.set(this.task, 'adCateArr2', this._processAdClass(this.task.fadclasscode_v2))
        } else {
          Vue.set(this.task, 'adCateArr2', [])
          Vue.set(this.task, 'fadclasscode_v2', '')
        }
        if (!this.task.fadclasscode_v2) {
          Vue.set(this.task, 'fadclasscode_v2', 3203)
        }
      } catch (e) {

      }
      // if (this.type == 'input' && this.task.task_state == 1 && this.runTimer) {
      //   this.intervalId = setInterval(this.timer, 1000)
      // }
      if (this.task.linkTypeArr) {
        if (!this.isquality) {
          this.activeTabName = "adInfo"
        } else {
          this.activeTabName = "illegalInfo"
        }
      }
      this.$nextTick(() => {
        this.$refs.adInfo.inputAdName()
      })
    },
    timer: function () {
      this.remainTime = moment.unix(+this.task.lasttime + 1200).diff(moment(), 'seconds')
      if (this.remainTime <= 0) {
        let tempId = setInterval(() => {})
        while (tempId > 0) {
          clearInterval(tempId)
          tempId--
        }
        $.post('/Api/TaskInput/back_overtime_task').then(() => {
          this.$alert('抱歉, 任务超时被收回, 点击确定开始下一个任务').then(() => {
            this.$emit('next')
          })
        })
      }
    },
    viewerInit: function () {
      this.$nextTick(() => {
        try {
          this.viewerInstance.destroy()
          this.viewerInstance = new Viewer(document.querySelector('.sample-content img'), {
            inline: true,
            toolbar: true,
            navbar: false
          })
        } catch (e) {}
      })
    },
    rateChange: function (value = 1) {
      try {
        this.$refs.player.playbackRate = value
      } catch (e) {}
    },
    reformTask: function () {
      this.$confirm('确认重做任务?').then(() => {
        this.type = 'input'
      }).catch(() => {})
    },
    searchSameAdNameTask: function () {
      if (!this.task.fadname) {
        return false
      }
      let data = {
        adName: this.task.fadname,
        mediaClass: this.task.media_class,
        taskId: this.task.taskid
      }
      if (this.task.media_class != 3) {
        data.adLen = this.task.fadlen
      }
      $.post('/Api/TaskInput/searchSameAdNameTask', data).then((res) => {
        if (res.code == 0) {
          this.sameAdNameTaskList = res.data
        } else if (res.code == -1) {
          this.sameAdNameTaskList = []
        }
      })
    },
    submitCutErrorSuccess: function () {
      this.$emit('next')
    },
    openCutErrorForm: function () {
      this.showCutErrorForm = true
      this.$nextTick(() => {
        this.$refs.submitCutError.dataInit()
      })
    },
    confirmPaper: function () {
      this.task.paperIsSure = true
      this.viewerInit()
    },
    checkPaper: function () {
      this.task.paperIsSure = false
      this.viewerInit()
    },
    submitRecallRequest: function () {
      this.$confirm('是否发起撤回请求?待质检员审核确认后任务将退回到被退回状态', '是否确认?', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'danger'
      }).then(() => {
        $.post('receiveRecallRequest', {
          taskid: this.task.taskid,
          taskType: 1
        }).then((res) => {
          if (res.code == -2 && res.msg == 'need log in again') {
            this.$message.error('登录状态过期,请再次登录')
          }
          this.$message({
            message: res.msg,
            type: res.type,
          })
          this.$emit('back',1)
        })
      }).catch(() => {})
    },
    passTask: function () {
      this.$confirm('任务质检通过?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
      }).then(() => {
        const data = {
          taskid: this.task.taskid,
          taskType: 1,
        }
        this.loading = true
        $.post('passTask', data).then((res) => {
          this.loading = false
          if (res.code == -2 && res.msg == 'need log in again') {
            this.$message.error('登录状态过期,请再次登录')
          }
          if (res.code == 0) {
            this.$message({
              type: 'success',
              message: '通过成功'
            })
          } else {
            this.$message({
              type: 'error',
              message: '通过失败'
            })
          }
          this.$emit('back',1)
        })
      }).catch(() => {})
    },
    rejectTask: function (type) {
      let text = '确认退回任务?'
      if (type == 1) {
        text = '只有广告名称错误才适用于广告信息错误退回, 如果是广告分类等错误, 请点击广告信息区域内的 广告信息有误 按钮提交正确信息'
      }
      this.$prompt(text, '提示', {
        confirmButtonText: '提交',
        cancelButtonText: '取消',
        inputPattern: /\S/,
        inputErrorMessage: '请输入退回说明'
      }).then((value) => {
        let data = {
          taskid: this.task.taskid,
          taskType: 1,
          reason: value.value,
          mediaClass: this.task.media_class,
          linkType: type,
        }
        this.loading = true
        $.post('submitRejectTaskRequest', data).then((res) => {
          this.loading = false
          if (res.code == 0) {
            this.$message({
              type: 'success',
              message: res.msg
            })
          } else {
            this.$message({
              type: 'error',
              message: res.msg
            })
          }
          this.$emit('back',1)
        })
      }).catch(() => {})
    },
    checkTask: function (taskid) {
      this.loading = true
      $('video').each(function () {
        this.pause()
      })
      $('audio').each(function () {
        this.pause()
      })
      $.post('index', {
        taskid: taskid,
        taskType: 1
      }).then((res) => {
        if (res.code == 0) {
          this.checkTaskInfo = res.data
        } else {
          this.$message.warning(res.msg)
        }
        this.showTaskInfo = true
        this.loading = false
        this.$nextTick(() => {
          this.$refs.task.dataInit()
          this.$refs.task.type = 'quality'
          if (this.type === 'input') {
            this.$refs.task.showFillBtn = true
          }
        })
      })
    },
    closeTaskInfo: function () {
      this.$emit('close')
      this.showTaskInfo = false
      this.checkTaskInfo = {}
    },

    fillTaskData: function () {
      this.$confirm('确认将此广告数据添加到正在录入数据?').then(() => {
        const fieldArr = ['fspokesman', 'fversion', 'fmanuno', 'fadmanuno', 'fapprno', 'fadapprno', 'fent', 'fadent', 'fentzone', 'fillegaltypecode', 'fexpressioncodes', 'fillegalcontent', 'is_long_ad', 'checkedIllegalArr', 'fadname', 'fbrand', 'fname', 'fadclasscode', 'fadclasscode_v2', ]
        const data = {}
        fieldArr.forEach((v) => {
          if (this.task.hasOwnProperty(v)) {
            data[v] = this.task[v]
          }
        })
        this.$emit('fillTaskData', data)
        this.$emit('back')
      })
    },
    receiveFillTaskData: function (data) {
      this.task = Object.assign(this.task, data)
      this.$nextTick(() => {
        Vue.set(this.task, 'adCateArr', data.hasOwnProperty('fadclasscode') ? this._processAdClass(data.fadclasscode) : [])
        Vue.set(this.task, 'adCateArr2', data.hasOwnProperty('fadclasscode_v2') ? this._processAdClass(data.fadclasscode_v2) : [])
      })
    },
    submitSuccess: function (type) {
      this.task.linkTypeArr.forEach((v) => {
        if (v.type == type) {
          v.state = 2
        }
      })
      let res = this.task.linkTypeArr.map((v) => {
        if (v.state != 2) {
          return v.type
        } else {
          return null
        }
      })
      res = res.filter((v) => v)
      if (res.length === 0) {
        this.$emit('next')
      } else {
        if (res[0] == 1) {
          this.activeTabName = "adInfo"
        } else {
          this.activeTabName = "illegalInfo"
        }
      }
    },
    _processAdClass: function (data) {
      let adClass = [];
      if (data.length === 6) {
        adClass.push(data.substr(0, 2))
        adClass.push(data.substr(0, 4))
        adClass.push(data)
      }
      if (data.length === 4) {
        adClass.push(data.substr(0, 2))
        adClass.push(data)
      }
      if (data.length === 2) {
        adClass.push(data)
      }
      return adClass
    },
    toggleAliPlayer: function () {
      if (this.aliPlayer) {
        this.aliPlayer.pause()
      }
      $('video').each(function () {
        this.pause()
      })
      $('audio').each(function () {
        this.pause()
      })
      this.showAliPlayer = !this.showAliPlayer
    }
  },
  filters: {
    taskStateFilter: function (value) {
      const arr = [
        '未分配',
        '任务中',
        '已完成',
        '',
        '被退回',
        '等待撤回通过'
      ]
      return arr[+value]
    }
  }
}