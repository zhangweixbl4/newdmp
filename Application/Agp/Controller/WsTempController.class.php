<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 文书模板
 * by yjm
 */

class WsTempController extends BaseController{

    /**
     * 文书模板
     * by jim
     */
    public function ws_temp_edit(){

        $dl_id = I('dl_id',0);
        $user = session('regulatorpersonInfo');//fregulatorid机构ID
        if($dl_id != 0){
            $document_model = M('document_model')->where(['dl_id'=>$dl_id,'dl_state'=>0,'dl_regulatorid'=>$user['fregulatorpid']])->find();
            $document_model['dl_content'] = htmlspecialchars_decode($document_model['dl_content']);
            if($document_model){

                $this->ajaxReturn(['code'=> 0,'msg'=>'获取当前模板id下的模板数据成功','data'=>$document_model]);
            }else{
                $this->ajaxReturn(['code'=> -1,'msg'=>'模板不存在']);
            }
        }else{
            $keyword = I('keyword');//搜索关键词
            $p = I('p',1);//页码
            $area = I('area',$user['regionid']);//地域
            $document_type = I('document_type');//文书类型
            $iscontain = I('iscontain');//是否包含下级地域
            //关键词
            if($keyword != ''){
                $map['document_model.dl_name|document_model.dl_regulatorname|document_model.dl_dename|document_model.dl_doctitle'] = array('like','%'.$keyword.'%');
            }
            //地域查询条件
            if($iscontain == 1){
                $map['tregulator.fregionid'] = ['like',wipe_regionid_zero($area)."%"];
            }else{
                $map['tregulator.fregionid'] = $area;
            }

            //文书类型查询条件
            if($document_type != ''){
                $map['document_model.dl_deid'] = $document_type;
            }
            $map['document_model.dl_state'] = 0;
            //$map['dl_regulatorid'] = $user['fregulatorpid'];
            $document_model = M('document_model')
                ->field("
                    document_model.*
                ")
                ->where($map)
                ->join('tregulator on tregulator.fid = document_model.dl_regulatorid')
                ->page($p.',15')
                ->select();
            //转html
            foreach ($document_model as $document_model_key=>$document_model_val){
                $document_model[$document_model_key]['dl_content'] = htmlspecialchars_decode($document_model_val['dl_content']);
            }
            $count = M('document_model')
                ->where($map)
                ->join('tregulator on tregulator.fid = document_model.dl_regulatorid')
                ->count();// 查询满足要求的总记录数
            if(!empty($document_model)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$document_model)));
            }else{
                $this->ajaxReturn(['code'=> -1,'msg'=>'当前机构下无模板']);
            }
        }

    }

    /**
     * 文书模板
     * by jim
     */
    public function ws_temp_edit1(){
        //TODO::机构修改模板
        $dl_id = I('dl_id',0);
        $user = session('regulatorpersonInfo');//fregulatorid机构ID
        if($dl_id != 0){
            $document_model = M('document_model')->where(['dl_id'=>$dl_id,'dl_state'=>0,'dl_regulatorid'=>$user['fregulatorpid']])->find();
            if($document_model){
                $this->ajaxReturn(['code'=> 0,'msg'=>'获取当前模板id下的模板数据成功','data'=>$document_model]);
            }else{
                $this->ajaxReturn(['code'=> -1,'msg'=>'模板不存在']);
            }
        }else{
            $document_model = M('document_model')->where(['dl_state'=>0,'dl_regulatorid'=>$user['fregulatorpid']])->select();
            if(!empty($document_model)){
                $this->ajaxReturn(['code'=> 0,'msg'=>'获取当前模板列表数据成功','data'=>$document_model]);
            }else{
                $this->ajaxReturn(['code'=> -1,'msg'=>'当前机构下无模板']);
            }
        }

    }



    /**
     * 文书模板
     * by jim
     */
    public function ws_temp_edit_post(){
        $data['dl_id'] = I('dl_id');//文书id
        $data['dl_name'] = I('dl_name');//文书名称
        $data['dl_content'] = $_POST['dl_content'];//文书内容
        $data['dl_doctitle'] = I('dl_doctitle');//文档标题
        $data['dl_receiveregulatorname'] = I('dl_receiveregulatorname');//收文单位
        $data['dl_sendtregulatorname'] = I('dl_sendtregulatorname');//发送单位
        $data['dl_sendtime'] = I('dl_sendtime');//发送时间
        $data['dl_regulatordefalut'] = I('dl_regulatordefalut');//机构默认模板

        $data['dl_whrule'] = I('dl_whrule');//文书规则
        $data['dl_editpersonid'] = session('personInfo.fid');//修改人
        $data['dl_edittime'] = date('Y-m-d H:i:s',time());//修改时间

        $document_model = M('document_model');
        $result = $document_model->save($data); // 更新文书模板操作
        if($result !== false){
            D('Function')->write_log('文书管理',1,'设置成功','document_model',$result,M('document_model')->getlastsql());
            $this->ajaxReturn(array('code' => 0,'msg'=>'设置成功'));
        }else{
            D('Function')->write_log('文书管理',0,'设置失败','document_model',0,M('document_model')->getlastsql());
            $this->ajaxReturn(array('code' => -1,'msg'=>'设置失败'));
        }
    }

    /**
     * 获取文书模板传给前端
     * by jim
     */
    public function get_ws_number(){
        $user = session('regulatorpersonInfo');//fregulatorid机构ID
        $ws_type_ids = I('ws_type_ids','1,2,3,4,5');//;
        $type_map['de_id'] = ['IN',$ws_type_ids];
        $de_ids = M('document_type')->where($type_map)->getField('de_id',true);
        $them_data = [];
        $count = 0;
        foreach ($de_ids as $de_id){
            $them_data = array_merge($them_data,$this->get_them_wh($de_id,$user));
        }
        $count = count($them_data);
        if($count < 1){
            $this->ajaxReturn(['code'=> -1,'msg'=>'当前机构下无模板']);
        }else{
            $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$them_data));
        }
    }

    /**
     * 获取文书模板同时生成文号
     * by jim
     */
    public function get_them_wh($ws_type,$user){

        //收回当前用户24小时内未使用的文号
        $where_dnr['dr_state'] = 10;
        $where_dnr['_string'] = 'TIMESTAMPDIFF(HOUR,dr_gettime,now())>=24';
        M('document_number')->where($where_dnr)->save(['dr_state'=>0,'dr_getpersonid'=>0]);

        $ws_model = M('document_model');
        $model_map['dl_state'] = 0;
        $model_map['dl_deid'] = $ws_type;
        $model_map['dl_regulatorid'] = $user['fregulatorpid'];

        $document_model = $ws_model->where($model_map)->select();//机构文书模板

        if(empty($document_model)){
            //如果没有所属模板，则添加一个
            $document_model_default_all = $ws_model->where(['dl_deid'=>$ws_type,'dl_regulatorid'=>0,'dl_state'=>0])->select();//获取默认模板
            foreach ($document_model_default_all as $document_model_default){
                if($document_model_default){
                    $document_model_default['dl_regulatorid'] = $user['fregulatorpid'];//机构ID
                    $document_model_default['dl_regulatorname'] = $user['regulatorpname'];//机构名称
                    $document_model_default['dl_createpersonid'] = $user['fid'];//创建人
                    $document_model_default['dl_createtime'] = date('Y-m-d H:i:s',time());//创建时间
                    $document_model_default['dl_state'] = 0;
                    unset( $document_model_default['dl_id']);
                    $them_id = $ws_model->add($document_model_default); // 写入模板数据到数据库
                    if(!$them_id){
                        $this->ajaxReturn(array('code' => -1,'msg'=>'自动添加失败'));
                    }else{
                        //$data[0] = $ws_model->where(['dl_id'=>$them_id])->find();//获取默认模板
                        $document_model_default['dl_state'] = 20;//添加模板备份状态
                        $document_model_default['dl_backupid'] = $them_id;//备份的模板ID
                        $bf_them_id = $ws_model->add($document_model_default);//写入备份
                        if(!$bf_them_id){
                            $this->ajaxReturn(array('code' => -1,'msg'=>'模板备份失败'));
                        }
                    }
                }else{
                    $this->ajaxReturn(['code'=> -1,'msg'=>'此类型下无默认模板，请通知管理员添加']);
                }
            }
            $data = $ws_model->where($model_map)->select();//机构文书模板
        }else{
            $data = $document_model;
        }
        foreach ($data as $key=>$val){
            $where_dnr['dr_regulatorid'] = $user['fregulatorpid'];
            $where_dnr['dr_dlid'] = $val['dl_id'];
            $where_dnr['dr_getpersonid'] = 0;
            $where_dnr['dr_state'] = 0;
            $do_dnr = M('document_number')->field('dr_name')->where($where_dnr)->order('dr_id asc')->find();
            if(!empty($do_dnr)){
                $data[$key]['wsh'] = $do_dnr['dr_name'];
            }else{
                $data[$key]['wsh'] = $this->build_wh(['dl_id' => $val['dl_id'],'dl_whrule' => $val['dl_whrule'],'dl_regulatorid' => $user['fregulatorpid']]);
            }
        }
        return $data;
    }

    /*
     * 生成占位号
     * */
    public function build_zw($number = 1,$str = '@4n'){
        $zwf_regular = '/@\dn/';//正则匹配占位符
        if(preg_match($zwf_regular,$str,$zwf_array_string)){
            $num = intval(ltrim(rtrim($zwf_array_string[0], "n"),'@')) - strlen($number);
            $zwf = '';
            while ($num > 0){
                $zwf.='0';
                $num--;
            }
            $zwf .= $number;
            return str_replace($zwf_array_string[0],$zwf,$str);
        }else{
            return $number;
        }

    }

    /**
     * 通过文号规则生成文号
     * by jim
     */
    public function build_wh($data = ['dl_id' => 1,'dl_whrule' => '@y@m@d第@4n号','dl_regulatorid' => 0]){
        header("Content-type: text/html; charset=utf-8");

        //$zwf_array_string[0] = '@5z';
        $wh_string = $data['dl_whrule'];
        //匹配年
        $year = date('Y');
        if(preg_match('/@y/',$wh_string)){
            $wh_string = str_replace("@y",$year,$wh_string);
        }

        //匹配月
        $month = date('m');
        if(preg_match("/@m/",$wh_string)){
            $wh_string = str_replace("@m",$month,$wh_string);
        }

        //匹配日
        $day = date('d');
        if(preg_match("/@d/",$wh_string)){
            $wh_string = str_replace("@d",$day,$wh_string);
        }

        //匹配号
        if(preg_match("/@\dn/",$wh_string)){
            $document_numberuse_mod = M('document_numberuse');//实例化document_numberuse
            $document_numberuse = $document_numberuse_mod
                ->where(['de_dlid'=>$data['dl_id'],'de_year'=>$year])
                ->find();//获取相关模板id当年文号使用情况
            //如果没有使用情况
            if(empty($document_numberuse)){
                //创建使用情况
                $numberuse = [
                    'de_dlid' =>$data['dl_id'],
                    'de_year' =>$year,
                    'de_number' =>1
                ];
                $res = $document_numberuse_mod->add($numberuse);
                if($res){
                    $number = $this->vad_wh_1($data,1,$wh_string);//检测文号是否可用，不可用+1
                }else{
                    $this->ajaxReturn(array('code' => -1,'msg'=>'生成文号时新增使用情况失败'));
                }
            }else{
                $number = $this->vad_wh_1($data,$document_numberuse['de_number'],$wh_string);//检测文号是否可用，不可用+1
                if($document_numberuse['de_number'] != $number){
                    $numberuse = [
                        'de_id' => $document_numberuse['de_id'],
                        'de_number' =>$number
                    ];
                    $save_res = $document_numberuse_mod->save($numberuse);
                    if($save_res == false){
                        $this->ajaxReturn(array('code' => -1,'msg'=>'生成文号时更新使用情况失败'));
                    }
                }
            }
        }

        $wh_string = $this->build_zw($number,$wh_string);
        return $wh_string;
    }

    /**
     * 验证文号是否可用
     * by jim
     */
    public function vad_wh(){
        $dr_name = I('dr_name','');//I('dr_name','');//获取文号名称
        $dr_dlid = I('dr_dlid',0);//339;//获取模板id
        if($dr_dlid == 0){
            $this->ajaxReturn(array('code' => -1,'msg'=>'文号或模板不存在！'));
        }
        $regulatorpersonInfo = session('regulatorpersonInfo');//fregulatorid机构ID
        $document_number = M('document_number')->field('dr_id')
            ->where(['dr_name'=>$dr_name,'dr_dlid'=>$dr_dlid])
            ->find();//获取默认模板
        if(empty($document_number)){
            //为空表示用户修改了文号，如果用户修改了生成的文号，直接给其生成一个新的
            $doc_num = [
                'dr_name' =>$dr_name,//字号名称
                'dr_dlid' => $dr_dlid,//模板id
                'dr_regulatorid' => $regulatorpersonInfo['fregulatorpid'],//机构id
                'dr_getpersonid' => $regulatorpersonInfo['fid'],//领取人id
                'dr_gettime' => date("Y-m-d H:i:s"),//领取时间
                'dr_usepersonid' => $regulatorpersonInfo['fid'],//使用人id
                'dr_usertime' => date("Y-m-d H:i:s"),//使用时间
                'dr_createpersonid' => $regulatorpersonInfo['fid'],//创建人id
                'dr_createtime' => date("Y-m-d H:i:s"),//创建时间
                'dr_state' => 20,
            ];
            $number_res = M('document_number')->add($doc_num);
            if($number_res){
                $this->ajaxReturn(array('code' => 0,'msg'=>'ok'));
            }else{
                $this->ajaxReturn(array('code' => -1,'msg'=>'保存验证时新增文号失败'));
            }
        }else{

            if($document_number['dr_state'] == 0 || ($document_number['dr_getpersonid'] == $regulatorpersonInfo['fid'] && $document_number['dr_state'] == 10)){
                //如果有，且没有被使用或者是本人领取的且没有被收回的,直接领取
                $doc_num = [
                    'dr_id' => $document_number['dr_id'],
                    'dr_dlid' => $dr_dlid,//模板id
                    'dr_regulatorid' => $regulatorpersonInfo['fregulatorpid'],//机构id
                    'dr_usepersonid' => $regulatorpersonInfo['fid'],//使用人id
                    'dr_usertime' => date("Y-m-d H:i:s"),//使用时间
                    'dr_state' => 20,
                ];
                $number_res = M('document_number')->save($doc_num);
                if($number_res !== false){
                    $this->ajaxReturn(array('code' => 0,'msg'=>'ok'));
                }else{
                    $this->ajaxReturn(array('code' => -1,'msg'=>'保存验证时更新使用时状态失败'));
                }

            }else{
                //如果有，且已经被使用了，通知用户
                $this->ajaxReturn(array('code' => -1,'msg'=>'文号已被使用'));
            }
        }
    }

    /**
     * 验证文号是否可用
     * by jim
     */
    public function vad_wh_1($data,$number,$wh_string_t){
        $regulatorpersonInfo = session('regulatorpersonInfo');//fregulatorid机构ID
        $wh_string = $this->build_zw($number,$wh_string_t);
        if(!$wh_string){
            $wh_string = $wh_string_t;
        }
        $document_number = M('document_number')
            ->where(['dr_name'=>$wh_string,'dr_dlid'=>$data['dl_id']])
            ->find();//查询文号表是否有同名文号
        if(empty($document_number)){
            //如果没有同名文号则新建
            $doc_num = [
                'dr_name' =>$wh_string,//字号名称
                'dr_dlid' => $data['dl_id'],//模板id
                'dr_regulatorid' => $data['dl_regulatorid'],//机构id
                'dr_getpersonid' => $regulatorpersonInfo['fid'],//领取人id
                'dr_gettime' => date("Y-m-d H:i:s"),//领取时间
                'dr_createpersonid' => $regulatorpersonInfo['fid'],//创建人id
                'dr_createtime' => date("Y-m-d H:i:s"),//创建时间
                'dr_state' => 10,
            ];
            $document_number_res = M('document_number')->add($doc_num);
            if($document_number_res){
                return $number;
            }else{
                $this->ajaxReturn(array('code' => -1,'msg'=>'自动取号验证时新增文号失败！'));
            }

        }else{
            //如果有且状态不为被使用
            if($document_number['dr_state'] == 0){
                $doc_num = [
                    'dr_id' => $document_number['dr_id'],//id
                    'dr_dlid' => $data['dl_id'],//模板id
                    'dr_regulatorid' => $data['dl_regulatorpid'],//机构id
                    'dr_getpersonid' => $regulatorpersonInfo['fid'],//领取人id
                    'dr_gettime' => date("Y-m-d H:i:s"),//领取时间
                    'dr_state' => 10,
                ];
                $document_number_res = M('document_number')->save($doc_num);
                if($document_number_res !== false){
                    return $number;
                }else{
                    $this->ajaxReturn(array('code' => -1,'msg'=>'自动取号验证时更新文号状态失败！'));
                }
            }else{
                //被使用或者已经被取号则加一再取号
                $wh_string = $this->build_zw($number+1,$wh_string_t);
                if(!$wh_string){
                    $wh_string = $wh_string_t;
                }
                return $this->vad_wh_1($data,$number+1,$wh_string_t);
            }
        }
    }


    //文书类型
    public  function ws_type(){
        //查询当前用户存在的文书模板类型
        $exist_document_types = M('document_model')
            ->field("
            document_type.de_id as value,
            document_type.de_name as name
            ")
            ->join('tregulator on tregulator.fid = document_model.dl_regulatorid')
            ->join('document_type on document_type.de_id = document_model.dl_deid')
            ->where([
                'tregulator.fregionid'=>['like',wipe_regionid_zero()."%"]
            ])
            ->group('document_type.de_id')->select();

        $this->ajaxReturn([
            'code'=>0,
            'msg'=>'获取文书类型成功！',
            'data'=>$exist_document_types
        ]);

    }




}