<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 线索处置
 */

class NcluehandleController extends BaseController
{
	/**
	 * 获取待处理线索列表
	 * by zw
	 */
	public function index() {

		$getData = $this->oParam;
		$pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
		$fnum = $getData['fnum'];//交办单号
		$fadname = $getData['fadname'];//广告名称
	    $fstatus = $getData['fstatus'];//处理状态
	    $fendtime = $getData['fendtime'];//截止日期，数组
	    $fpush_date = $getData['fpush_date'];//下发日期，数组
	    $outtype = $getData['outtype'];//导出类别

	    if(empty($outtype)){
        	$limitIndex = ($pageIndex-1)*$pageSize;
	    }else{
	    	$pageSize = 999999;
	    	$limitIndex = 0;
	    }

	    $where['_string'] = '1=1';
	    if(!empty($fnum)){
	    	$where['a.fnum'] = array('like','%'.$fnum.'%');
	    }
	    if(!empty($fadname)){
	    	$where['a.fadname'] = array('like','%'.$fadname.'%');
	    }
    	switch ($fstatus) {
    		case 20:
    			$where['a.fstatus'] = $fstatus;
    			break;
    		case 30:
    			$where['a.fstatus'] = $fstatus;
    			break;
    		case 40:
    			$where['a.fstatus'] = $fstatus;
    			break;
    		case 21:
    			$where['a.fstatus'] = ['in',[20,30,40]];
    			$where['_string'] .= ' and DATEDIFF(a.fendtime,"'.date('Y-m-d').'")<0';
    			break;
    		default:
				$where['a.fstatus'] = ['in',[20,30,40]];
    			break;
    	}
	    if(!empty($fendtime)){
	    	$where['a.fendtime'] = ['between',[$fendtime[0],$fendtime[1]]];
	    }

		$count = M('nj_clue')
			->alias('a')
			->join('nj_clueflow b on a.fid = b.fclueid and fcreatetrepid = '.session('regulatorpersonInfo.fregulatorpid').' and fsendstatus = 0')
			->where($where)
			->count();
		
		$data = M('nj_clue')
			->alias('a')
			->field('a.*,(case when a.fstatus = 10 then "待登记" when a.fstatus = 20 then "待处理" when a.fstatus = 30 then "核查中" when a.fstatus = 40 then "立案调查中" when a.fstatus = 50 then "已办结" end) fstatuscn,(case when a.fstatus = 20 or a.fstatus = 30 or a.fstatus = 40 then ifnull(DATEDIFF(a.fendtime,"'.date('Y-m-d').'"),0) else 9999 end) diffday')
			->join('nj_clueflow b on a.fid = b.fclueid and fcreatetrepid = '.session('regulatorpersonInfo.fregulatorpid').' and fsendstatus = 0')
			->order('diffday asc,a.fstatus asc,a.fid asc')
			->where($where)
			->limit($limitIndex,$pageSize)
			->select();
		if(!empty($outtype)){
			if(empty($data)){
				$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
			}
	        $outdata['datalie'] = [
	          '序号'=>'fid',
	          '交办单号'=>'fnum',
	          '交办日期'=>'fhandover_date',
	          '交办方式'=>'fhandover_types',
	          '线索来源'=>'fget_types',
	          '移送或提供线索单位'=>'ftipoffs_unit',
	          '投诉或举报方'=>'ftipoffs_user',
	          '投诉或举报方联系方式'=>'ftipoffs_phone',
	          '广告主'=>'fadowner_name',
	          '广告发布者（发布平台）'=>'fadsend_user',
	          '广告类型'=>'fadtypes',
	          '商品/服务类别'=>'fadclass_name',
	          '广告内容'=>'fadname',
	          '刊播时间'=>'fadissue_date',
	          '落地链接'=>'ftarget_url',
	          '发布条目数'=>'fadissue_times',
	          '反馈截止日期'=>'fendtime',
	          '登记机构'=>'finput_regulator',
	          '下发日期'=>'fpush_date',
	          '当前状态'=>'fstatuscn',
	          '反馈机构'=>'fhandle_regulator',
	          '反馈人员'=>'fhandle_user',
	          '办结日期'=>'fhandle_enddate',
	        ];

	        $outdata['lists'] = $data;
	        $ret = A('Api/Function')->outdata_xls($outdata);

	        D('Function')->write_log('南京待办线索导出',1,'excel导出成功');
	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
		}
	}

	/**
	 * 线索处理操作
	 * by zw
	*/
	public function clueAction(){
		$getData = $this->oParam;
		$fid = $getData['fid'];//主键
	    $attachinfo = $getData['attachinfo'];//上传的附件材料
	    $actiontype = $getData['actiontype'];//流转环节，30核查中，40立案调查中，50已办结
	    $fcreateinfo = $getData['fcreateinfo'];//意见
	    $fhandle_result = $getData['fhandle_result'];//处理结果
	    $flian_date = $getData['flian_date'];//立案调查日

	    $attachcount = 0;
	    foreach ($attachinfo as $key => $value) {
			if($value['fattachstatus'] == 1 || $value['fattachstatus'] == 2){
				$attachcount += 1;
			}
	    }
	    if($actiontype == 50){
	    	if(empty($fhandle_result)){
		    	$this->ajaxReturn(['code'=>1,'msg'=>'请选择处理结果']);
	    	}
	    }
	    if($actiontype == 50 || $actiontype == 40){
	    	if(empty($attachcount)){
		    	$this->ajaxReturn(['code'=>1,'msg'=>'请上传附件材料']);
	    	}
	    }
	    if(empty($fcreateinfo)){
	    	$this->ajaxReturn(['code'=>1,'msg'=>'请输入处理意见']);
    	}

		$where['a.fstatus'] = ['in',[20,30,40]];
		$where['a.fid'] = $fid;
		$viewNce = M('nj_clue')
			->alias('a')
			->field('a.*,b.fid bid')
			->join('nj_clueflow b on a.fid = b.fclueid and fcreatetrepid = '.session('regulatorpersonInfo.fregulatorpid').' and fsendstatus = 0')
			->where($where)
			->find();
		if(empty($viewNce)){
			$this->ajaxReturn(['code'=>1,'msg'=>'线索不存在，请刷新页面后重试']);
		}
		if($viewNce['fstatus']>$actiontype){
	   		$this->ajaxReturn(['code'=>1,'msg'=>'流程选择有误']);
	   	}
	   	if($actiontype == 50 && empty($fhandle_result)){
	   		$this->ajaxReturn(['code'=>1,'msg'=>'请选择处理结果']);
	   	}

	   	if($viewNce['fstatus'] != $actiontype){
	   		$dataCe['fstatus'] = $actiontype;
	   	}
	   	if($actiontype == 50){
	   		$dataCe['fhandle_regulatorid'] = session('regulatorpersonInfo.fregulatorpid');
	   		$dataCe['fhandle_regulator'] = session('regulatorpersonInfo.regulatorpname');
	   		$dataCe['fhandle_userid'] = session('regulatorpersonInfo.fid');
	   		$dataCe['fhandle_user'] = session('regulatorpersonInfo.fname');
	   		$dataCe['fhandle_enddate'] = date('Y-m-d');
	   		$dataCe['fhandle_result'] = $fhandle_result;
	   	}
	   	$dataCe['fmodify_time'] = date('Y-m-d H:i:s');
	   	$dataCe['fmodify_user'] = session('regulatorpersonInfo.fname');

		if($viewNce['fstatus'] != $actiontype){//更改环节的情况
			M('nj_clueflow')->where(['fid'=>$viewNce['bid']])->save(['fchildstatus'=>10,'fsendstatus'=>10,'fcreatepersonid'=>session('regulatorpersonInfo.fid'),'fcreateperson'=>session('regulatorpersonInfo.fname'),'ffinishtime'=>date('Y-m-d H:i:s')]);//保存当前流程状态

			$freason = '';
			//验证是否超时处理
			if(strtotime($viewNce['fendtime']) < time()){
				$freason .= '案件处理超时（截止日期为'.$viewNce['fendtime'].'），';
			}

			//添加流程记录
	   		$dataNcfw = [];
	   		switch ($actiontype) {
	   			case 50:
	   				$fflowname = '案件办结';
	   				$freason .= '流转至办结';
	   				break;
	   			case 40:
	   				$fflowname = '立案调查';
	   				$freason .= '流转至立案';
   					$dataCe['fendtime'] = date('Y-m-d',strtotime($flian_date.' +90 day'));
   					$freason .= '，实际立案日期为'.$flian_date.'，反馈截止日期延期至'.$dataCe['fendtime'];
	   				break;
	   			case 30:
	   				$fflowname = '核查';
	   				$freason .= '流转至核查';
	   				if(empty($viewNce['fendtime_status'])){
	   					$dataCe['fendtime'] = date('Y-m-d',strtotime('+30 day'));
	   					$freason .= '，反馈截止日期延期至'.$dataCe['fendtime'];
	   				}
	   				break;
	   		}
	   		$dataNcfw['fclueid'] = $fid;
	   		$dataNcfw['fprvflowid'] = $viewNce['bid'];
	   		$dataNcfw['fcreatetrepid'] = session('regulatorpersonInfo.fregulatorpid');
	   		$dataNcfw['fcreatetrepname'] = session('regulatorpersonInfo.regulatorpname');
	   		$dataNcfw['fcreatepersonid'] = session('regulatorpersonInfo.fid');
	   		$dataNcfw['fcreateperson'] = session('regulatorpersonInfo.fname');
	   		$dataNcfw['fcreatetime'] = date('Y-m-d H:i:s');
	   		$dataNcfw['fcreateinfo'] = $fcreateinfo;
   			$dataNcfw['freason'] = ['exp','concat(freason,"'.$freason.'；")'];
	   		$dataNcfw['fflowname'] = $fflowname;
	   		$dataNcfw['fstatus'] = $actiontype;
	   		if($actiontype == 50){//案件办结的情况
	   			$dataNcfw['fchildstatus'] = 10;
	   			$dataNcfw['fsendstatus'] = 10;
	   		}
	   		$newFlowId = M('nj_clueflow')->add($dataNcfw);//添加下一流程
   			if(empty($newFlowId)){
   				$this->ajaxReturn(['code'=>1,'msg'=>'操作失败，未知原因']);
   			}
	   	}else{//仅保存
			$saveNcfw = M('nj_clueflow')->where(['fid'=>$viewNce['bid']])->save(['fcreateinfo'=>$fcreateinfo,'fcreatepersonid'=>session('regulatorpersonInfo.fid'),'fcreateperson'=>session('regulatorpersonInfo.fname')]);//保存当前流程状态
	   	}

	   	//添加环节附件
	   	if(!empty($attachinfo)){
			//附件材料添加
	   		$attach_data['fclueid'] = $fid;//登记id
	        $attach_data['fflowid'] = $newFlowId?$newFlowId:$viewNce['bid'];//流程ID
	        $attach_data['fuploadtime'] = date('Y-m-d H:i:s');//上传时间
	        $attach_data['ftype'] = $actiontype;//附件类型
	        $attach_data['fcreateuserid'] = session('regulatorpersonInfo.fid');//上传人
	        $attach_data['fcreatetreid'] = session('regulatorpersonInfo.fregulatorpid');//上传机构
	        $attach = [];
	        $attach2 = [];
	        foreach ($attachinfo as $key => $value){
	          $attach_data['ffilename'] = $value['fattachname'];
	          $attach_data['fattachname'] = $value['fattachname'];
	          $attach_data['fattachurl'] = $value['fattachurl'];
	          if($value['fattachstatus'] == 2){
	            $attach[] = $attach_data;
	          }elseif($value['fattachstatus'] == 0){
	          	$attach2[] = $value['fattachurl'];
	          }
	        }
			if(!empty($attach)){
				M('nj_cluefile')->addAll($attach);
			}
			if(!empty($attach2)){
				$where_att['fattachurl'] = array('in',$attach2);
				$where_att['fclueid'] = $fid;
				$where_att['fflowid'] = $newFlowId?$newFlowId:$viewNce['bid'];
				M('nj_cluefile')->where($where_att)->save(['fstatus'=>0]);
			}
		}

	   	$saveNce = M('nj_clue')->where(['fid'=>$fid])->save($dataCe);//保存交处理结果
		$this->ajaxReturn(['code'=>0,'msg'=>'操作成功']);
	}

	/**
	 * 线索查看操作
	 * by zw
	*/
	public function lookAction(){
		$getData = $this->oParam;
		$fid = $getData['fid'];//线索ID
		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
		}
		
		$whereNce['a.fid'] = $fid;
		$whereNce['a.fview_look'] = 0;
		$countNce = M('nj_clue')
			->alias('a')
			->join('nj_clueflow b on a.fid = b.fclueid and fcreatetrepid = '.session('regulatorpersonInfo.fregulatorpid').' and fsendstatus = 0')
			->where($whereNce)
			->count();
		if(!empty($countNce)){
			M('nj_clue')->where(['fid'=>$fid])->save(['fview_look'=>1]);
		}
			
		$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
	}

	/**
	 * 线索延期申请操作
	 * by zw
	*/
	public function clueActionContinueTime(){
		$getData = $this->oParam;
		$fid = $getData['fid'];//主键
	    $attachinfo = $getData['attachinfo'];//附件
	    
	    $where['a.fstatus'] = 40;
		$where['a.fid'] = $fid;
		$viewNce = M('nj_clue')
			->alias('a')
			->field('a.fendtime,b.fid bid,ifnull(DATEDIFF(a.fendtime,"'.date('Y-m-d').'"),0) diffday')
			->join('nj_clueflow b on a.fid = b.fclueid and fcreatetrepid = '.session('regulatorpersonInfo.fregulatorpid').' and fsendstatus = 0')
			->where($where)
			->find();
		if(empty($viewNce)){
			$this->ajaxReturn(['code'=>1,'msg'=>'未在立案中不可申请延期']);
		}
		if($viewNce['diffday']<0 || $viewNce['diffday']>15){
			$this->ajaxReturn(['code'=>1,'msg'=>'已超期或距离截止日期还未到15天内不允许申请']);
		}

		//附件材料添加
   		$attach_data['fclueid'] = $fid;//登记id
        $attach_data['fflowid'] = $viewNce['bid'];//流程ID
        $attach_data['fuploadtime'] = date('Y-m-d H:i:s');//上传时间
        $attach_data['ftype'] = 41;//附件类型
        $attach_data['fcreateuserid'] = session('regulatorpersonInfo.fid');//上传人
        $attach_data['fcreatetreid'] = session('regulatorpersonInfo.fregulatorpid');//上传机构
        $attach = [];
        foreach ($attachinfo as $key => $value){
			$attach_data['ffilename'] = $value['fattachname'];
			$attach_data['fattachname'] = $value['fattachname'];
			$attach_data['fattachurl'] = $value['fattachurl'];
    		$attach[] = $attach_data;
        }
		if(!empty($attach)){
			M('nj_cluefile')->addAll($attach);

			M('nj_clue')->where(['fid'=>$fid])->save(['fendtime'=>['exp','date_add(fendtime, interval 30 day)']]);

			M('nj_clueflow')->where(['fid'=>$viewNce['bid']])->save(['freason'=>['exp','concat(freason,"申请延期至'.date('Y-m-d',strtotime($viewNce['fendtime'].'+30 day')).'；")']]);
			$this->ajaxReturn(['code'=>0,'msg'=>'申请成功']);
		}else{
			$this->ajaxReturn(['code'=>1,'msg'=>'请上传延期申请材料']);
		}
	}
	
}