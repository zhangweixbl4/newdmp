Vue.component('timeline', {
  template: `
      <div class="time-line" ref="timeline">
      <div class="time-line-warp">
          <div class="center-line" :style="{'width':itemWidth+'px'}" v-show="onDrag"></div>
          <ul class="time-line-ul" ref="timelineul" @mousedown="onmousedown" @mousemove="onmousemove" @mouseup="onmouseup" :style="ulwidth">
              <el-tooltip class="item" effect="dark" :content="item.substring(11)" placement="top" v-for="item in timeArr" :key="item">
              <li
                  class="time-line-li"
                  :style="{'width':itemWidth+'px','height': item.substring(18) === '0' ? '30px' : '20px'}"
                  :class="{'active':item === activetime,'heightItem':item.substring(18) === '0'}"
                  @click="reset(item)">
                  <span class="time-item-span" :alt="item.substring(11)"></span>
              </li>
              </el-tooltip>
          </ul>
          </div>
      </div>
  `,
  props: ['activetime'],
  data() {
    return {
      timeArr: [],
      itemWidth: '',
      x: 0,
      px: 0,
      onDrag: false,
      el: null,
      s: 0,
      itemNum: 50000,
      flag: 1,
      isclick: false
    }
  },
  created() {
    this.getTimeArr(this.activetime)
  },
  mounted() {
    this.itemWidth = this.setWidth()
    this.el = this.$refs.timelineul
  },
  methods: {
    onmousedown(e) {
      this.x = e.clientX
      this.px = this.el.offsetLeft
      this.onDrag = true
      this.el.style.cursor = 'move'
      this.$emit('tmousedown')
    },
    onmousemove(e) {
      if (this.onDrag) {
        let nx = e.clientX + this.px - this.x
        this.el.style.left = nx + 'px'
        // 鼠标拖拽横向距离，向左为 + 右 -
        let mw = this.x - e.clientX
        // 计算跳转的秒数，向上取整
        this.s = Math.ceil(mw / this.itemWidth)
        if (this.flag) {
          this.flag = 0
          this.$emit('tmousemove')
          setTimeout(() => {
            this.flag = 1
          }, 500)
        }
      }
    },
    onmouseup(e) {
      this.isclick = Math.abs(this.x - e.clientX) < 2 * this.itemWidth
      if (this.onDrag && !this.isclick) {
        this.$emit('tmouseup', Math.round(this.s))
      }
      this.onDrag = false
      this.el.style.cursor = 'default'
      this.s = 0

    },
    reset(time) {
      // 如果拖动距离小于一格宽度，视为点击事件
      if (this.isclick) {
        this.getTimeArr(time)
        this.$emit('click', time)
      }
    },
    getTimeArr(time) {
      let arr = []
      let b = new Date(time).getTime() - this.itemNum
      for (let i = 0; i < (this.itemNum / 1000 * 2 + 1); i++) {
        arr.push(formatDate(new Date(b + (i * 1000)), 'yyyy-MM-dd hh:mm:ss'))
      }
      this.timeArr = arr
    },
    setWidth() {
      return (this.$refs.timeline.clientWidth / 61)
    },
  },
  computed: {
    ulwidth() {
      return {
        'width': `${this.itemWidth * (this.itemNum/1000*2+1)}px`
      } || {
        'width': '100%'
      }
    }
  },
  watch: {
    activetime(time) {
      let arr = []
      let b = new Date(time).getTime() - this.itemNum
      for (let i = 0; i < (this.itemNum / 1000 * 2 + 1); i++) {
        arr.push(formatDate(new Date(b + (i * 1000)), 'yyyy-MM-dd hh:mm:ss'))
      }
      this.$set(this, 'timeArr', arr)
      if (this.el) {
        this.el.style.left = '50%'
      }
    }
  }
});
Vue.component('aliplayer', {
  template: `
      <div class="aliplayer">
          <div class="prism-player" id="player-con" ref="playercon"></div>
      </div>
  `,
  props: {
    url: {
      default: '',
      type: String
    },
    width: {
      default: '560px',
      type: String
    },
    height: {
      default: '400px',
      type: String
    },
    autoplay: {
      default: false,
      type: Boolean
    }
  },
  data() {
    return {
      player: null,
      timer: null
    }
  },
  mounted() {
    this.$refs.playercon.style.height = this.height
    this.player = new Aliplayer({
      "id": "player-con",
      "source": this.url,
      "width": this.width,
      "height": this.height,
      "autoplay": this.autoplay,
      "isLive": false,
      "rePlay": false,
      "playsinline": true,
      "preload": true,
      "controlBarVisibility": "hover",
      "useH5Prism": true,
      "skinLayout": [{
          "name": "bigPlayButton",
          "align": "blabs",
          "x": 30,
          "y": 80
        },
        {
          "name": "H5Loading",
          "align": "cc"
        },
        {
          "name": "errorDisplay",
          "align": "tlabs",
          "x": 0,
          "y": 0
        },
        {
          "name": "infoDisplay"
        },
        {
          "name": "tooltip",
          "align": "blabs",
          "x": 0,
          "y": 56
        },
        {
          "name": "thumbnail"
        },
        {
          "name": "controlBar",
          "align": "blabs",
          "x": 0,
          "y": 0,
          "children": [{
              "name": "progress",
              "align": "blabs",
              "x": 0,
              "y": 44
            },
            {
              "name": "playButton",
              "align": "tl",
              "x": 15,
              "y": 12
            },
            {
              "name": "setting",
              "align": "tr",
              "x": 15,
              "y": 12
            },
            {
              "name": "timeDisplay",
              "align": "tl",
              "x": 10,
              "y": 7
            },
            {
              "name": "fullScreenButton",
              "align": "tr",
              "x": 10,
              "y": 12
            },
            {
              "name": "volume",
              "align": "tr",
              "x": 5,
              "y": 10
            }
          ]
        }
      ]
    })
    this.player.on('ready', () => {
      this.$emit('success')
    })
    this.player.on('play', () => {
      this.$emit('start')
    })
    this.player.on('pause', () => {
      this.$emit('stop')
    })
    this.player.on('error', e => {
      this.$emit('error', e)
    })
  },
  methods: {
    _play() {
      this.player.play()
    },
    _stop() {
      this.player.pause()
    },
    _seek(time) {
      this.player.seek(time)
    },
    _getCurrentTime() {
      return this.player.getCurrentTime()
    }
  }
});