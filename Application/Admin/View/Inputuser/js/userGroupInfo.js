let userGroupInfo = {
    template: `<div v-loading="loading" class="userInfo">
    <el-form :model="userInfo">
        <el-form-item label="分组">
            <el-select v-model="userInfo.group_id" placeholder="请选择" filterable>
                <el-option
                        v-for="item in groupList"
                        :key="item.value"
                        :label="item.label"
                        :value="item.value">
                </el-option>
            </el-select>
        </el-form-item>
        <el-form-item label="标签">
            <el-checkbox-group v-model="userInfo.user_tag">
                <el-checkbox :label="item" v-for="item in userTags" :key="item">{{item}}</el-checkbox>
            </el-checkbox-group>
        </el-form-item>
        <el-form-item label="可切换的分组">
            <div>
                <el-button v-for="(item,index) in userInfo.haveGroupList" :key="item.value" @click="delHaveGroup(index)" title="点击删除">{{item.label}}</el-button>
            </div>
            <el-select v-model="checkedGroup" placeholder="请选择分组" filterable>
                <el-option
                        v-for="item in groupList"
                        :key="item.value"
                        :label="item.label"
                        :value="item">
                </el-option>
            </el-select>
            <el-button v-if="checkedGroup" @click="addGroup">添加</el-button>
        </el-form-item>
    </el-form>
    <div>
        <el-button type="success" @click="save">保存</el-button>
    </div>
</div>`,
    data: function () {
        return {
            loading: true,
            userInfo: {},
            userTags,
            groupList,
            haveGroupList: [],
            checkedGroup: null,
        }
    },
    props: ['userids'],
    computed: {
        ids: function () {
            return this.userids.map(v => v.wx_id)
        }
    },
    methods: {
        dataInit: function(){
            this.loading = false
            this.userInfo = {
                haveGroupList: [],
                user_tag: [],
            }
            this.haveGroupList = []
            this.checkedGroup = null
        },
        delHaveGroup: function (index) {
            this.userInfo.haveGroupList.splice(index, 1)
        },
        addGroup: function () {
            // 检查重复
            if (this.checkedGroup){
                let res = this.userInfo.haveGroupList.some((v) => v.value == this.checkedGroup.value)
                if (!res){
                    this.userInfo.haveGroupList.push(this.checkedGroup)
                } else{
                    this.$message.warning('重复, 请选择其他分组')
                }
            }
        },
        save: function () {
            this.$confirm('确认保存?').then(() => {
                this.loading = true
                let have_group_id = this.userInfo.haveGroupList.map(v => v.value)
                this.userInfo.have_group_id = have_group_id.join(',')
                $.post('batchModifyInfo', {
                    userInfo: this.userInfo,
                    wxIds: this.ids,
                }).then((res) => {
                    if (res.code == 0){
                        this.$message.success(res.msg)
                    } else{
                        this.$message.error(res.msg)
                    }
                    this.loading = false
                    this.type = 'check'
                    this.$emit('save')
                })
            }).catch(() => {})
        },
    }

}