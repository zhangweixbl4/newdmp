<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;


class HzsjAdController extends Controller {
	
	/*抽取数据*/
	public function h(){
		
		session_write_close();
		set_time_limit(600);
		
		$needGdList = M('tinspect_plan','',C('ZIZHI_DB'))->field('fid,fmedia_id,fissue_date')->where(array('need_gd'=>1))->limit(30)->select();
		
		foreach($needGdList as $gd){
			
			$mediaId = $gd['fmedia_id'];
			$date = $gd['fissue_date'];
			$mediaInfo = M('tmedia')
									->cache(true,60)
									->field('
											tmedia.fid as media_id
											,fmedianame
											,left(fmediaclassid,2) as media_class
											,tmediaowner.fregionid
											,tmediaowner.fname as mediaowner_name
											')
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->where(array('tmedia.fid'=>$mediaId))
									->find();
			

			if($mediaInfo['media_class'] == '01'){
				$wCounts = A('Api/HzsjAd','Model')->tv($mediaInfo,$date);
			}elseif($mediaInfo['media_class'] == '02'){
				$wCounts = A('Api/HzsjAd','Model')->bc($mediaInfo,$date);
			}elseif($mediaInfo['media_class'] == '03'){
				$wCounts = A('Api/HzsjAd','Model')->paper($mediaInfo,$date);
			}elseif($mediaInfo['media_class'] == '13'){
				$wCounts = A('Api/HzsjAd','Model')->net($mediaInfo,$date);
			}else{
				$wCounts = array('count'=>0,'illCount'=>0);
			}
			
			$wCount = $wCounts['count'];
			$illCount = $wCounts['illCount'];
			
			$aa = M('fixed_media_data_log')->add(array('fmediaid'=>$mediaId,'fdate'=>$date,'ftime'=>date('Y-m-d H:i:s'),'fvalue'=>'抽取到分析库完成 '.$wCount.'条','fperson'=>'system'));
			#var_dump(M('fixed_media_data_log')->getLastSql(),$aa);
			M('tinspect_plan','',C('ZIZHI_DB'))
								->where(array('fid'=>$gd['fid']))
								->save(array('need_gd'=>0,'ffinish_ads'=>$wCount,'fillegal_ads'=>$illCount,'ffinish_time'=>date('Y-m-d H:i:s')));
			echo $mediaId .'	'.$date.'	'.$wCount . '	'.$illCount. "\n";
		}
		
	}
	
	/*抽取扩展表*/
	public function ext(){
		header("Content-type: text/html; charset=utf-8");
		session_write_close();
		set_time_limit(600);
		
		
		$media_class = I('media_class');
		
		$where = array();
		$where['is_extend'] = 0;
		$where['sample_id'] = ['gt',0];
		if($media_class) $where['media_class'] = $media_class;
		
		$wait_ext_list = M('hzsj_ad','',C('FENXI_DB'))->field('identify,media_class,fmediaid,sample_id,sample_uuid')->where($where)->limit(200)->select();
		
		$extAll = [];
		foreach($wait_ext_list as $wait_ext){
			$re = array();
			
			if($wait_ext['media_class'] == '互联网'){
				$re = A('Api/HzsjAd','Model')->ext_13($wait_ext);
			}elseif($wait_ext['media_class'] == '电视'){
				$re = A('Api/HzsjAd','Model')->ext_01($wait_ext);
			}elseif($wait_ext['media_class'] == '广播'){
				$re = A('Api/HzsjAd','Model')->ext_02($wait_ext);
			}elseif($wait_ext['media_class'] == '报纸'){
				$re = A('Api/HzsjAd','Model')->ext_03($wait_ext);
			}	
			
			if($re) $extAll[] = $re;
		}
		
		$inCount = M('hzsj_ad_extend','',C('FENXI_DB'))->addAll($extAll,array(),true);
		$extCount = M('hzsj_ad','',C('FENXI_DB'))->where(array('identify'=>['in',array_column($wait_ext_list,'identify')]))->save(array('is_extend'=>1));
		echo date('Y-m-d H:i:s') .' 写入成功 '.$inCount.' 条';

		
		
	}
	
	
}