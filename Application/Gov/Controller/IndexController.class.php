<?php
namespace Gov\Controller;
use Think\Controller;

class IndexController extends Controller {
    public function index(){
		
		
		
		
		$this->assign('menu_list',A('Gov/Menu','Model')->menu_list(0));//管理菜单

		$this->display();
		
	}

	/*左侧菜单*/
	public function left_menu(){
		
		$menu_id = I('menu_id');
		$this->assign('menu_list',A('Gov/Menu','Model')->menu_list($menu_id));//管理菜单

		$this->display();
	}
	
	
}