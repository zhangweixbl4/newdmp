<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;

/**
 * 鸿茅药酒发布清单数据获取(定时任务)
 */
class HmyjController extends Controller {
	
	/**
	 * 初始化
	 */
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
	}

	/**
	 * 数据获取(定时任务调用)
	 */
	public function hmyj_issue(){
		session_write_close();
		set_time_limit(600);
		for($i=0;$i<50;$i++){
			$this->hmyj_issue_do();
		}
	}
	
	/**
	 * 执行
	 */
	public function hmyj_issue_do(){
		session_write_close();
		set_time_limit(80);
		$dateInfo = A('Common/System','Model')->important_data('hmyj_issue');//获取处理过的日期
		$dateInfo = json_decode($dateInfo,true);//转为数组
		if(!$dateInfo){//如果是空的
			$month_date = date('Y-m-d');
			$media_id = 0;
		}else{
			$month_date = $dateInfo['month_date'];//处理日期
			$media_id = $dateInfo['media_id'];//处理媒介id
		}
		$where = [
			'fid' => ['GT',$media_id],
			'fstate' => 1
		];
		$mediaInfo = M('tmedia')
			->cache(true,120)
			->field('fid')
			->where($where)
			->order('fid asc')
			->find();
		if(!$mediaInfo){//判断能否查到媒介
			$limitDate = time() - 86400 * 8;
			if(strtotime($month_date) < $limitDate){//判断日期是否处理到了8天前
				A('Common/System','Model')->important_data('hmyj_issue','{}');
			}else{//近8天内
				A('Common/System','Model')->important_data('hmyj_issue',json_encode([
						'month_date'=>date("Y-m-d",strtotime($month_date) - 86400),//时间减一天
						'media_id'=>0])
					);
			}
			exit;
		}else{
			$ff = A('Common/System','Model')->important_data('hmyj_issue',json_encode(['month_date'=>$month_date,'media_id'=>$mediaInfo['fid']]));
		}
		A('Api/Hmyj','Model')->Hmyj($month_date,$mediaInfo['fid']);
	}
}