<?php
namespace Gongshang\Controller;
use Think\Controller;

class AdclueController extends BaseController {
    public function index(){
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$fcreatetime_s = I('fcreatetime_s');// 创建时间
		if($fcreatetime_s == '') $fcreatetime_s = '2015-01-01';
		$fcreatetime_e = I('fcreatetime_e');// 创建时间
		if($fcreatetime_e == '') $fcreatetime_e = date('Y-m-d');
		$region_id = I('region_id');//地区ID
		$this_region_id = I('this_region_id');//是否只查询本级地区
		if($region_id == 100000) $region_id = 0;//如果传值等于100000，相当于查全国
		$fmediaclass = I('fmediaclass');//媒体类别
		$regulatorpersonInfo = session('regulatorpersonInfo');
		$adclue_source = I('adclue_source');//接收、转办 查询条件
		$ftype = I('ftype');//处置类型
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID
		$fadname = I('fadname');// 广告名称
		
		$where = array();
		if($fadname != ''){
			$where['tad.fadname'] = array('like','%'.$fadname.'%');//广告名称
		}
		if(is_array($fillegaltypecode)){
			$where['tadclue.fillegaltypecode'] = array('in',$fillegaltypecode);//违法类型代码
		}
		if($ftype) $where['tadcluesection.ftype'] = array('in',$ftype);
		if($adclue_source == ''){
			$where['tadcluesection.fsendunit|tadcluesection.freceiveunit'] = $regulatorpersonInfo['fregulatorid'];//发送单位，接收单位  查询条件
		}elseif($adclue_source == 'distribute'){//转办
			$where['tadcluesection.fsendunit'] = $regulatorpersonInfo['fregulatorid'];//发送单位，接收单位  查询条件
		}elseif($adclue_source == 'receive'){//接收
			$where['tadcluesection.freceiveunit'] = $regulatorpersonInfo['fregulatorid'];//发送单位，接收单位  查询条件

		}	
		
		
		if($fmediaclass != '') $where['fmediaclass'] = $fmediaclass;
		if($region_id > 0){
			if($this_region_id == 'true'){
				$where['tmediaowner.fregionid'] = $region_id;//地区搜索条件
			}else{
				$region_id_rtrim = rtrim($region_id,'0');//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmediaowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}
		}
		$where['tadclue.fcreatetime'] = array('between',$fcreatetime_s.','.$fcreatetime_e.' 23:59:59');
		
		$count = M('tadclue')
							->join('tmediaclass on tmediaclass.fid = tadclue.fmediaclass')
							->join('tad on tad.fadid = tadclue.fadid')
							->join('tadclass on tadclass.fcode = tad.fadclasscode')
							->join('tmedia on tmedia.fid = tadclue.fmediaid')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构
							->join('tillegaltype on tillegaltype.fcode = tadclue.fillegaltypecode')
							->join('tadcluesection on tadcluesection.fadclueid = tadclue.fadclueid and tadcluesection.fstate in (0,1,3)')
							->where($where)->count();// 查询满足要求的总记录数
		
		
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$adclueList = M('tadclue')
								->field('
										tadcluesection.fsendunit,
										tadcluesection.freceiveunit,
										tadcluesection.fsendunitname,
										tadcluesection.freceiveunitname,
										tadcluesection.ftype,
										
										tadclue.*,
										tmediaclass.fclass as mediaclass,
										tad.fadname,
										tadclass.ffullname as adclass_fullname,
										tmedia.fmedianame,
										tillegaltype.fillegaltype
									')
								->join('tmediaclass on tmediaclass.fid = tadclue.fmediaclass')
								->join('tad on tad.fadid = tadclue.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = tadclue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构
								->join('tillegaltype on tillegaltype.fcode = tadclue.fillegaltypecode')
								->join('tadcluesection on tadcluesection.fadclueid = tadclue.fadclueid and tadcluesection.fstate in (0,1,3)')
								->where($where)
								->order('tadclue.fstate asc')
								->limit($Page->firstRow.','.$Page->listRows)->select();//查询广告线索列表
		$adclueList = list_k($adclueList,$p,$pp);
		foreach($adclueList as $key => $adclue){
			$adclueList[$key] = $adclue;
			$adclueList[$key]['issue_info'] = A('Gongshang/Adclue','Model')->get_issue_info($adclue['fmediaclass'],$adclue['fsampleid']);
		}
		//var_dump($adclueList);
		$clueresulttypeList = M('tclueresulttype')->where(array('ftate'=>array('neq',-1)))->select();
		$this->assign('clueresulttypeList',$clueresulttypeList);//处罚种类
		$this->assign('adclueList',$adclueList);//线索列表
		$this->assign('regionid',session('regulatorpersonInfo.regionid'));
		
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		$this->assign('illegaltype',$illegaltype);//违法类型
		$this->assign('page',$Page->show());//分页输出
		$this->display();
		
	}
	
	/*线索接收*/
	public function clue_receive(){
		$clueresulttypeList = M('tclueresulttype')->where(array('ftate'=>array('neq',-1)))->select();
		$this->assign('clueresulttypeList',$clueresulttypeList);//处罚种类
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		$this->assign('illegaltype',$illegaltype);//违法类型
		$this->display();

	}
	
	/*线索接收*/
	public function clue_turn(){
		$clueresulttypeList = M('tclueresulttype')->where(array('ftate'=>array('neq',-1)))->select();
		$this->assign('clueresulttypeList',$clueresulttypeList);//处罚种类
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		$this->assign('illegaltype',$illegaltype);//违法类型
		$this->display();
		
	}
	
	/*违法数据复核*/
	public function illegal_data_check(){
		
		
		$this->display();
	}
	
	
	/*处理结果统计*/
	public function results_statistics(){
		
		$this->display();
	}
	
	/*线索详情*/
	public function adclue_details(){
		$where = array();
		$fadclueid = I('fadclueid');//线索ID
		$where['tadclue.fadclueid'] = $fadclueid;
		$adclueInfo = M('tadclue')
								->field('
										tadcluesection.fsendunit,
										tadcluesection.freceiveunit,
										tadcluesection.fsendunitname,
										tadcluesection.freceiveunitname,
										tadcluesection.ftype,
										
										tadclue.*,
										tmediaclass.fclass as mediaclass,
										tad.fadname,
										tadclass.ffullname as adclass_fullname,
										tmedia.fmedianame,
										tmedia.femail as media_mail,
										tillegaltype.fillegaltype
										')
								->join('tmediaclass on tmediaclass.fid = tadclue.fmediaclass')
								->join('tad on tad.fadid = tadclue.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = tadclue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构
								->join('tillegaltype on tillegaltype.fcode = tadclue.fillegaltypecode')
								->join('tadcluesection on tadcluesection.fadclueid = tadclue.fadclueid and tadcluesection.fstate in (0,1,3)')
								->where($where)
								->find();//查询线索详情
		$adclueInfo['issue_info'] = A('Gongshang/Adclue','Model')->get_issue_info($adclueInfo['fmediaclass'],$adclueInfo['fsampleid']);
		$clueresulttypeList = M('tclueresulttype')->where(array('ftate'=>array('neq',-1)))->select();
		$adcluesectionList = M('tadcluesection')
											->field('tadcluesection.*,tregulator.fname as regulator_name')
											->join('tregulator on tregulator.fcode = tadcluesection.freceiveunit')
											->where(array('fadclueid'=>$fadclueid))->order('fadcluesectionid asc')->select();
		$adclueattachList = M('tadclueattach')->where(array('fadclueid'=>$fadclueid,'fstate'=>1))->select();
		//var_dump($adclueattachList);		
		$this->assign('adclueattachList',$adclueattachList);//附件列表									
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
		$this->assign('IllegalList',$IllegalList);//违法表现数据列表
		$this->assign('adcluesectionList',$adcluesectionList);//线索处理环节
		$this->assign('clueresulttypeList',$clueresulttypeList);//处罚种类
		$this->assign('adclueInfo',$adclueInfo);//线索信息
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->display();
	}
	
	/*将线索转为严重线索*/
	public function serious_clue(){

		$fadclueid = I('fadclueid');
		$data['serious_clue'] = 1;
		$result = M('tadclue')->where(array('fadclueid' => $fadclueid))->save($data);
		if($result){
			$this->ajaxReturn(array('code'=>0,'msg'=>'转为严重线索成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'转为严重线索失败'));
		}

	}

	/*上传线索附件*/
	public function up_adclue_file(){
		$fadclueid = I('fadclueid');//线索ID
		$fattachname = I('fattachname');//附件名称
		$fattachfilename = I('fattachfilename');//附件文件名
		$furl = I('furl');//url

		$adclueInfo = M('tadclue')->where(array('fadclueid'=>$fadclueid))->find();//线索信息
		$adcluesectionInfo = M('tadcluesection')->where(array('fadclueid'=>$fadclueid))->order('fadcluesectionid desc')->find();//最后一个环节的信息
		if($adcluesectionInfo['freceiveunit'] != session('regulatorpersonInfo.fregulatorid')){//判断该线索目前是否是自己单位管理
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该线索目前由其它单位处理,您没有权限上传附件'));
		}
		$a_data = array();
		$a_data['fadclueid'] = $adclueInfo['fadclueid'];//线索ID
		$a_data['fadcluesectionid'] = $adcluesectionInfo['fadcluesectionid'];//线索环节ID
		$a_data['fattachname'] = $fattachname;//附件名称
		$a_data['fattachfilename'] = $fattachfilename;//附件文件名
		$a_data['furl'] = $furl;//url
		
		$a_data['fuploader'] = session('regulatorpersonInfo.fname');//上传人
		$a_data['fuploadtime'] = date('Y-m-d H:i:s');//上传时间
		$a_data['fmodifier'] = session('regulatorpersonInfo.fname');//修改人
		$a_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$a_data['fstate'] = 1;//url
		
		
		$rr = M('tadclueattach')->add($a_data);
		if($rr > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'保存附件成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'保存附件失败'));
		}
		
		
	}
	
	/*删除线索附件*/
	public function del_adclue_file(){
		$fadclueattachid = I('fadclueattachid');//附件ID
		$adclueattachInfo = M('tadclueattach')->where(array('fadclueattachid'=>$fadclueattachid))->find();//附件信息
		$adcluesectionInfo = M('tadcluesection')->where(array('fadclueid'=>$adclueattachInfo['fadclueid']))->order('fadcluesectionid desc')->find();//最后一个环节的信息
		//var_dump($adcluesectionInfo['freceiveunit']);
		if($adcluesectionInfo['freceiveunit'] != session('regulatorpersonInfo.fregulatorid')){//判断该线索目前是否是自己单位管理
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该线索目前由其它单位处理,您没有权限删除附件'));
		}
		$e_data = array();
		$e_data['fmodifier'] = session('regulatorpersonInfo.fname');//修改人
		$e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$e_data['fstate'] = -1;//状态，-1为已删除
		
		
		$rr = M('tadclueattach')->where(array('fadclueattachid'=>$fadclueattachid))->save($e_data);
		if($rr > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除附件成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除附件失败'));
		}
		
		
	}
	
	/*查询播出记录*/
	public function get_issue(){
		$fadclueid = I('fadclueid');
		$p = I('p');//分页的第几页
		$pp = 10;//每页显示多少条记录

		$adclueInfo = M('tadclue')->where(array('fadclueid'=>$fadclueid))->find();

		$issueList = A('Gongshang/Adclue','Model')->get_issue_list($adclueInfo['fmediaclass'],$adclueInfo['fsampleid'],$p,$pp);
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','pp'=>$pp,'issueList'=>$issueList));
		
	}
	
	/*转办线索*/
	public function transfer_adclue(){
		$send_to_media = I('send_to_media');//是否发送邮件给媒介
		$media_mail = I('media_mail');//邮件地址
		
		
		$fadclueid = I('fadclueid');//线索ID
		$regulator_code = I('regulator_code');//移送目标监管机构ID
		$fdescribe = I('fdescribe');//说明
		
		$regulatorInfo = M('tregulator')->where(array('fcode'=>$regulator_code))->find();//移送目标监管机构信息
		if(!$regulatorInfo)  $this->ajaxReturn(array('code'=>-1,'msg'=>'移送目标监管机构不存在,移送失败'));
		$adclueInfo = M('tadclue')
								->field('
										tadcluesection.fsendunit,
										tadcluesection.freceiveunit,
										tadcluesection.fsendunitname,
										tadcluesection.freceiveunitname,
										tadcluesection.ftype,
										
										tadclue.*,
										tmediaclass.fclass as mediaclass,
										tad.fadname,
										tadclass.ffullname as adclass_fullname,
										tmedia.fmedianame,
										tmedia.femail as media_mail,
										tillegaltype.fillegaltype
										')
								->join('tmediaclass on tmediaclass.fid = tadclue.fmediaclass')
								->join('tad on tad.fadid = tadclue.fadid')
								->join('tadclass on tadclass.fcode = tad.fadclasscode')
								->join('tmedia on tmedia.fid = tadclue.fmediaid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构
								->join('tillegaltype on tillegaltype.fcode = tadclue.fillegaltypecode')
								->join('tadcluesection on tadcluesection.fadclueid = tadclue.fadclueid and tadcluesection.fstate in (0,1,3)')
								->where(array('tadclue.fadclueid'=>$fadclueid))
								->find();//查询线索详情
		//var_dump($adclueInfo);	
		//exit;
		if($adclueInfo['fstate'] == 1){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该线索已处理,无法移交'));
		}
		$adcluesectionInfo = M('tadcluesection')->where(array('fadclueid'=>$fadclueid))->order('fadcluesectionid desc')->find();//最后一个环节的信息
		if($adcluesectionInfo['freceiveunit'] != session('regulatorpersonInfo.fregulatorid')){//判断该线索目前是否是自己单位管理
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该线索目前由其它单位处理,您没有权限转办'));
		}
		
		
		
		$e_s_data = array();

		$e_s_data['fexecuter'] = session('regulatorpersonInfo.fname');//执行人
		$e_s_data['fexecutetime'] = date('Y-m-d H:i:s');//执行时间
		$e_s_data['fdescribe'] = $fdescribe;//说明
		$e_s_data['fstate'] = 2;//把状态改为已转办

		M('tadclue')->where(array('fadclueid'=>$fadclueid))->save(array('fdescribe',$fdescribe));//修改线索来源
		M('tadcluesection')->where(array('fadcluesectionid'=>$adcluesectionInfo['fadcluesectionid']))->save($e_s_data);//修改原环节信息
		A('Common/Adcluesection','Model')->create_adcluesection($fadclueid,session('regulatorpersonInfo.fregulatorid'),$regulator_code,session('regulatorpersonInfo.fname'));//创建环节
		if($send_to_media == 'true'){
			A('Gongshang/Adclue','Model')->sent_to_adclue_media($adclueInfo,$media_mail);//给媒介发送邮件通知
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'线索移交成功'));
		
	}
	
	
	/*线索处理*/
	public function adclue_handle(){
		
		$fadclueid = I('fadclueid');//线索ID
		$clueresulttype = I('clueresulttype');//处理
		$fdescribe = I('fdescribe');//处理说明
		$adclueInfo = M('tadclue')->where(array('fadclueid'=>$fadclueid))->find();//线索信息
		$adcluesectionInfo = M('tadcluesection')->where(array('fadclueid'=>$fadclueid))->order('fadcluesectionid desc')->find();//最后一个环节的信息
		if($adcluesectionInfo['ftype'] != 1){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该线索目前不是待处置状态,不能提交'));
		}
		if($adcluesectionInfo['freceiveunit'] != session('regulatorpersonInfo.fregulatorid')){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该线索目前由其它单位处理,您没有权限处理'));
		}
		if($adclueInfo['fstate'] == 1){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该线索已处理,无需重复处理'));
		}
		
		
		if(!$clueresulttype){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'处理失败,没有选择处理选项'));
		}
		
		if(in_array(-10,$clueresulttype)){//数据复核
			
		}else{//线索处理
			$fresults = '';//处理结果
			$fresult = 0;//处理结果 ，统计最严重处理的ID
			
			foreach($clueresulttype as $clueresult){//循环处理结果
				$clueresulttypeInfo = M('tclueresulttype')->where(array('fadclueresultid'=>$clueresult))->find();//查询处理结果信息
				$fresults .= $clueresulttypeInfo['fadclueresult'].'、';//拼接处理结果
				if($clueresulttypeInfo['fadclueresultid'] > $fresult)  $fresult = $clueresulttypeInfo['fadclueresultid'];//处理结果最严重的ID
			}
			$fresults = trim($fresults,'、');//去掉最后一个 、
			M('tadclue')->where(array('fadclueid'=>$fadclueid))->save(array(
																			'fstate'=>1,//处理状态，已处理
																			'fhandletime'=>date('Y-m-d H:i:s'),//处理时间
																			'fhandler'=>session('regulatorpersonInfo.fname'),//处理人
																			'fresults'=>$fresults,//处理结果 拼接字段
																			'fdescribe'=>$fdescribe,//说明
																			'fresult'=>$fresult,//处理结果，最严重ID
																		));//修改线索信息
			M('tadcluesection')->where(array('fadcluesectionid'=>$adcluesectionInfo['fadcluesectionid']))->save(array(
																														'fexecuter'=>session('regulatorpersonInfo.fname'),//执行人
																														'fexecutetime'=>date('Y-m-d H:i:s'),//执行时间
																														'fdescribe'=>$fdescribe,//说明
																														'fstate'=>3,//处理状态，已处理
																														
																													));//修改原环节信息
			
			
		}
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'处理完成'));
		
	}
	
	/*数据复核*/
	public function check_illegal(){
		
		
		$fadclueid = I('fadclueid');//线索ID
		$regulatorInfo = M('tregulator')->where(array('fkind'=>1,'fcode'=>session('regulatorpersonInfo.fregulatorid')))->find();
		$parent_regulatorInfo = M('tregulator')->where(array('fkind'=>1,'fcode'=>$regulatorInfo['fpid']))->find();
		
		if(!$parent_regulatorInfo) $this->ajaxReturn(array('code'=>-1,'msg'=>'没有上级机构了,没法复核数据'));
		$regulator_code = $parent_regulatorInfo['fcode'];//移送目标监管机构ID

		$fdescribe = I('fdescribe');//说明
		
		$adclueInfo = M('tadclue')->where(array('fadclueid'=>$fadclueid))->find();//线索信息
		if($adclueInfo['fstate'] == 1){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该线索已处理,不能再复核数据'));
		}
		$adcluesectionInfo = M('tadcluesection')->where(array('fadclueid'=>$fadclueid))->order('fadcluesectionid desc')->find();//最后一个环节的信息
		if($adcluesectionInfo['ftype'] != 1){
			//$this->ajaxReturn(array('code'=>-1,'msg'=>'该线索目前不是待处置状态,不能提交'));//暂无需判断
		}
		if($adcluesectionInfo['freceiveunit'] != session('regulatorpersonInfo.fregulatorid')){//判断该线索目前是否是自己单位管理
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该线索目前由其它单位处理,您没有权限办理'));
		}
		
		$e_s_data = array();

		$e_s_data['fexecuter'] = session('regulatorpersonInfo.fname');//执行人
		$e_s_data['fexecutetime'] = date('Y-m-d H:i:s');//执行时间
		$e_s_data['fdescribe'] = $fdescribe;//说明
		$e_s_data['fstate'] = 2;//把状态改为已转办

		M('tadclue')->where(array('fadclueid'=>$fadclueid))->save(array('fdescribe',$fdescribe));//修改线索
		M('tadcluesection')->where(array('fadcluesectionid'=>$adcluesectionInfo['fadcluesectionid']))->save($e_s_data);//修改原环节信息
		A('Common/Adcluesection','Model')->create_adcluesection($adclueInfo['fadclueid'],session('regulatorpersonInfo.fregulatorid'),$regulator_code,session('regulatorpersonInfo.fname'),2);//创建环节
		$this->ajaxReturn(array('code'=>0,'msg'=>'提交复核成功'));
		
	}
	
	
	/*提交数据复核*/
	public function ajax_check_illegal(){
		$fadclueid = I('fadclueid');//线索ID
		$submit_type = I('submit_type');
		$adcluesectionInfo = M('tadcluesection')->where(array('fadclueid'=>$fadclueid))->order('fadcluesectionid desc')->find();//最后一个环节的信息
		if($adcluesectionInfo['ftype'] != 2 || $adcluesectionInfo['fstate'] == 3){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该线索目前不是待复核状态,不能提交复核'));
		}
		if($adcluesectionInfo['freceiveunit'] != session('regulatorpersonInfo.fregulatorid')){//判断该线索目前是否是自己单位管理
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该线索目前由其它单位处理,您没有权限办理'));
		}
		
		if($submit_type == 'check_illegal'){//提交复核数据
			A('Gongshang/Adclue','Model')->check_illegal();
			$this->ajaxReturn(array('code'=>0,'msg'=>'提交复核成功'));
		}elseif($submit_type == 'maintain_illegal'){//维持原数据
			A('Gongshang/Adclue','Model')->maintain_illegal();
			$this->ajaxReturn(array('code'=>0,'msg'=>'提交复核成功'));
			
		}elseif($submit_type == 'not_illegal'){//不违法
			A('Gongshang/Adclue','Model')->not_illegal();
			$this->ajaxReturn(array('code'=>0,'msg'=>'提交复核成功'));
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}