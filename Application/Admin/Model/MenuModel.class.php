<?php
namespace Admin\Model;



class MenuModel{


    /*获取菜单*/
    public function menu_list(){
		
		$cache_time = 15;
		$menu_1 = M('admin_menu')->where(array('parent_id'=>0,'menu_state'=>0))->cache(true,$cache_time)->order('menu_sort asc,menu_id asc')->select();//查询一级菜单
		
		foreach($menu_1 as $key => $menu){//遍历一级菜单
			$menu_list[$key] = $menu;

			$menu_list[$key]['menu2'] = M('admin_menu')->cache(true,$cache_time)->where(array('parent_id'=>$menu['menu_id'],'menu_state'=>0))->order('menu_sort asc,menu_id asc')->select();//查询二级菜单
			
			foreach($menu_list[$key]['menu2'] as $key2 => $menu2){//遍历二级菜单
				$menu_list[$key]['menu2_list'][$key2] = $menu2;
				//var_dump($menu2);
				$menu_list[$key]['menu2_list'][$key2]['menu3_list'] = M('admin_menu')->cache(true,$cache_time)->where(array('parent_id'=>$menu2['menu_id'],'menu_state'=>0))->order('menu_sort asc,menu_id asc')->select();//查询三级菜单
			}//遍历二级菜单结束
			
			$menu_list[$key]['menu2'] = null;//销毁原二级菜单
			

			
		}//遍历一级菜单结束
		return $menu_list;
    }

    /**
	 * 获取新菜单列表，所有菜单用在菜单管理的列表
	 * $teid 类型id,$pid 父id
	 * by zw
	 */
    public function m_allmenulist($pid = 0 ,$teid = 20){
    	$do_nau = M('new_agpmenu')
	    	->alias('a')
	    	->field('menu_id,menu_name,menu_url,parent_id,menu_sort,menu_state,icon,menu_type,menu_code,menu_content,menu_default,b.acount')
	    	->join('(select parent_id as pid,count(*) as acount from new_agpmenu group by parent_id) b on a.menu_id=b.pid','left')
	    	->where('a.parent_id='.$pid.' and a.menu_type='.$teid.' and a.menu_state=0')
	    	->order('a.menu_sort asc,a.menu_id asc')
	    	->select();
    	foreach ($do_nau as $key => $value) {
    		$datas[$key] = $value;
    		if(!empty($value['acount'])){
    			$data = $this->m_allmenulist($value['menu_id'],$teid);
				if(!empty($data)){
					$datas[$key]['children'] = $data;
				}
    		}
    	}
    	return $datas;
    }

    /**
	 * 获取新菜单列表2，所有菜单，用在机构的菜单权限选择
	 * $teid 类型id,$pid 父id
	 * by zw
	 */
    public function m_allmenulist2($pid = 0 ,$teid = 20){
    	$do_nau = M('new_agpmenu')
	    	->alias('a')
	    	->field('menu_id,menu_name,parent_id,b.acount,menu_content')
	    	->join('(select parent_id as pid,count(*) as acount from new_agpmenu group by parent_id) b on a.menu_id=b.pid','left')
	    	->where('a.parent_id='.$pid.' and a.menu_type='.$teid.' and a.menu_state=0')
	    	->order('a.menu_sort asc,a.menu_id asc')
	    	->select();
    	foreach ($do_nau as $key => $value) {
    		$datas[$key] = $value;
    		if(!empty($value['acount'])){
    			$data = $this->m_allmenulist2($value['menu_id'],$teid);
				if(!empty($data)){
					$datas[$key]['children'] = $data;
				}
    		}
    	}
    	return $datas;
    }

    /**
	 * 获取机构菜单权限
	 * $teid 类型id,$pid 父id,$treid 机构ID
	 * by zw
	 */
    public function m_alljgmenulist($teid = 20 ,$treid ,$menuarr = [],$fcustomer){
    	$do_nau = M('tregulatormenu')
	    	->alias('a')
	    	->field('a.fmenuid')
	    	->join('new_agpmenu c on c.menu_id=a.fmenuid')
	    	->where(' a.fregulatorid='.$treid.' and c.menu_type='.$teid.' and c.menu_state=0 and a.fcustomer = "'.$fcustomer.'"')
	    	->order('c.menu_sort asc,c.menu_id asc')
	    	->select();

	    if(!empty($do_nau)){
	    	foreach ($do_nau as $key => $value) {
	    		array_push($menuarr, $value['fmenuid']);
	    	}
	    }else{
    		$menudata = $this->m_alldefaultmenulist(0,$teid,[]);
	    	$addarr = [];
	    	foreach ($menudata as $key => $value) {
	    		array_push($menuarr,$value['menu_id']);
	    		$addarr[] = array(
	    			'fregulatorid'	=>$treid,
					'fmenutype'		=>$teid,
					'fmenuid'		=>$value['menu_id'],
					'fmenupid'		=>$value['parent_id'],
					'fcreator'		=>session('personInfo.fid'),
					'fcreatetime'	=>date('Y-m-d H:i:s'),
					'fstate'		=>1,
					'fcustomer'		=>$fcustomer,
	    		);
	    	}
	    	if(!empty($addarr)){
				M('tregulatormenu')->addAll($addarr);
			}
	    }
	   
    	return $menuarr;
    }


    /**
	 * 获取用户的所有菜单，用在用户的菜单权限选择
	 * $teid 类型id,$treid 机构id
	 * by zw
	 */
    public function m_alljgmenulist2($teid = 20 ,$treid ,$fcustomer){
	    $menuarr = $this->m_defaultmenu($teid,$treid,[],$fcustomer);

    	if(!empty($menuarr)){
    		$where['_string'] = 'menu_id in (select parent_id from new_agpmenu where parent_id>0 and menu_type='.$teid.' and menu_state=0 and menu_id in ('.implode(',', $menuarr).') group by parent_id) or menu_id in (select menu_id from new_agpmenu where parent_id=0 and menu_type='.$teid.'  and menu_state=0 and menu_id in ('.implode(',', $menuarr).'))';
    		$datalist = M('new_agpmenu')
				->field('menu_id,menu_name,menu_code,parent_id,icon,menu_url,menu_content')
				->where($where)
				->order('menu_sort asc,menu_id asc')
				->select();//获取有子栏目的父目录信息
			foreach ($datalist as $key2 => $value2) {
				$datas[$key2] = $value2;
				$where2['menu_id'] 		= array('in',$menuarr);
				$where2['menu_state'] 	= 0;
				$where2['menu_type'] 	= $teid;
				$where2['parent_id'] 	= $value2['menu_id'];
				$do_tu3 = M('new_agpmenu')
					->field('menu_id,menu_name,menu_code,parent_id,icon,menu_url,menu_content')
					->where($where2)
					->order('menu_sort asc,menu_id asc')
					->select();
				if(!empty($do_tu3)){
					$datas[$key2]['children'] = $do_tu3;
				}
			}

    	}
    	
    	return $datas;
    }

    /**
	 * 获取用户菜单权限
	 * $teid 类型id,$personid 用户ID,$treid 用户所属部门
	 * by zw
	 */
    public function m_allpersonmenulist($teid = 20 ,$personid,$treid ,$menuarr = [],$fcustomer){
    	$do_nau = M('tregulatorpersonmenu')
	    	->alias('a')
	    	->field('fmenuid')
	    	->join('new_agpmenu c on c.menu_id=a.fmenuid')
	    	->where('a.fpersonid='.$personid.' and c.menu_type='.$teid.' and c.menu_state=0 and a.fcustomer = "'.$fcustomer.'"')
	    	->order('c.menu_sort asc,c.menu_id asc')
	    	->select();
	    if(!empty($do_nau)){
	    	foreach ($do_nau as $key => $value) {
	    		array_push($menuarr, $value['fmenuid']);
	    	}
	    }else{
    		$menuarr = $this->m_defaultmenu($teid,$treid,[],$fcustomer);
    		if(!empty($menuarr)){
		    	$addarr = [];
	    		$do_tu = M('new_agpmenu')->field('menu_id,parent_id')->where(array('menu_id'=>array('in',$menuarr)))->order('menu_sort asc,menu_id asc')->select();
	    		if(!empty($do_tu)){
	    			foreach ($do_tu as $key => $value) {
	    				$addarr[] = array(
			    			'fpersonid'	=>$personid,
							'fmenutype'		=>$teid,
							'fmenuid'		=>$value['menu_id'],
							'fmenupid'		=>$value['parent_id'],
							'fcreator'		=>session('personInfo.fid'),
							'fcreatetime'	=>date('Y-m-d H:i:s'),
							'fstate'		=>1,
							'fcustomer'		=>$fcustomer,
			    		);
	    			}
	    			
	    			if(!empty($addarr)){
						M('tregulatorpersonmenu')->addAll($addarr);
					}
	    		}
	    	}
	    }
	    
    	return $menuarr;
    }

    /**
	 * 获取默认菜单权限
	 * $treid 机构id,teid 菜单类型
	 * by zw
	 */
    public function m_defaultmenu($teid = 20 ,$treid ,$menuarr = [] ,$fcustomer){
    	$do_nau = M('tregulatormenu')
	    	->alias('a')
	    	->field('a.fmenuid,a.fmenupid')
	    	->join('new_agpmenu c on c.menu_id=a.fmenuid')
	    	->where(' a.fregulatorid='.$treid.' and c.menu_type='.$teid.' and c.menu_state=0 and a.fcustomer = "'.$fcustomer.'"')
	    	->order('c.menu_sort asc,c.menu_id asc')
	    	->select();
	    if(!empty($do_nau)){
	    	foreach ($do_nau as $key => $value) {
    			array_push($menuarr, $value['fmenuid']);
	    	}
	    }else{
	    	$do_tr = M('tregulator')->field('fpid,fkind')->where('fid='.$treid)->find();
	    	if($do_tr['fkind']!=2){
	    		$menudata = $this->m_alldefaultmenulist(0,$teid,[]);
		    	foreach ($menudata as $key => $value) {
		    		array_push($menuarr,$value['menu_id']);
		    	}
	    	}
	    	
	    }
	   
    	return $menuarr;
    }

    /**
	 * 获取默认菜单
	 * $teid 类型id,$pid 父id
	 * by zw
	 */
    public function m_alldefaultmenulist($pid = 0 ,$teid = 20 ,$menuarr = []){
    	$do_nau = M('new_agpmenu')
	    	->alias('a')
	    	->field('menu_id,parent_id,b.acount')
	    	->join('(select parent_id as pid,count(*) as acount from new_agpmenu group by parent_id) b on a.menu_id=b.pid','left')
	    	->where('a.parent_id='.$pid.' and a.menu_type='.$teid.' and a.menu_state=0 and a.menu_default=0')
	    	->order('a.menu_sort asc,a.menu_id asc')
	    	->select();
	    if(!empty($do_nau)){
	    	foreach ($do_nau as $key => $value) {
	    		array_push($menuarr, $value);
	    		if(!empty($value['acount'])){
	    			$menuarr = $this->m_alldefaultmenulist($value['menu_id'],$teid,$menuarr);
	    		}
	    	}
	    }
	    return $menuarr;
    }
}