<?php

namespace Admin\Controller;
use Think\Controller;
class QuestionsController extends BaseController {

    //首页列表
    public function index()
    {
        //echo "功能开发中...";exit;

        $this->display();

    }

    public function index_list(){
        //echo '功能开发中....';exit;
        $p = I('page',1);
        $keyword = trim(I('keyword'));
        if($keyword != ''){
            $map['common_question.cqn_title|common_question.cqn_content'] = ['like','%'.$keyword.'%'];
        }
        $map['common_question.cqn_deleteuserid'] = ['EQ',0];
        $data = M('common_question')
            ->field('
                common_question.cqn_id,
                common_question.cqn_title,
                common_question.cqn_content,
                common_question.cqn_recontent,
                CASE
		        WHEN common_question.cqn_fcustomer = -1 THEN
			    "全部客户"
		        ELSE
                customer.cr_name
		        END as cqn_fcustomername,
                cqn_nextshow,
                cqn_fcustomer,
                cqn_level,
                tperson.fname as cqn_createusername,
                cqn_createtime
            ')
            ->join('customer on customer.cr_num = common_question.cqn_fcustomer','left')
            ->join('tperson on tperson.fid = common_question.cqn_createuserid')
            ->where($map)
            ->order('cqn_createtime desc')
            ->page($p.',20')
            ->select();
        $count = M('common_question')
            ->field('
                common_question.cqn_id,
            ')
            ->join('customer on customer.cr_num = common_question.cqn_fcustomer','left')
            ->join('tperson on tperson.fid = common_question.cqn_createuserid')
            ->where($map)
            ->count();
        $this->ajaxReturn([
            'code' => 0,
            'data' => ['count'=>$count,'list'=>$data],
            'msg' => '获取数据成功'
        ]);
    }

    //添加常见问题
    public function add_qestion()
    {
        //header("Content-type:text/html;charset=utf-8");
        $user = session('personInfo');;//
        $data = [];
        $data['cqn_title'] = I('cqn_title');//标题
        $data['cqn_content'] = I('cqn_content');//描述
        $data['cqn_recontent'] = I('cqn_recontent');//内容
        $data['cqn_fcustomer'] = I('cqn_fcustomer',-1);//客户id
        $data['cqn_nextshow'] = I('cqn_nextshow',0);//是否下级
        $data['cqn_level'] = I('cqn_level',-1);//下级等级限制
        $data['cqn_createuserid'] = $user['fid'];//用户id
        $data['cqn_createtime'] = date('Y-m-d H:i:s');//创建时间
        $res = M('common_question')->add($data);
        if($res){
            $this->ajaxReturn([
                'code'=>0,
                'msg'=>'添加成功'
            ]);
        }
    }

    //显示常见问题
    public function edit_qestion()
    {
        $cqn_id = I('cqn_id',6);
        $data = M('common_question')->where(['cqn_id'=>$cqn_id])->find();
        if(!empty($data)){
            $this->ajaxReturn([
                'code'=>0,
                'data'=>$data,
                'msg'=>'获取成功'
            ]);
        }

    }

    //编辑常见问题
    public function edit_qestion_post()
    {
        $user = session('personInfo');;//
        $data = [];
        $data['cqn_id'] = I('cqn_id',6);
        $data['cqn_title'] = I('cqn_title','这是标题2');//标题
        $data['cqn_content'] = I('cqn_content','这是描述3');//描述
        $data['cqn_recontent'] = I('cqn_recontent','这才是内容啦！4');//内容
        $data['cqn_fcustomer'] = I('cqn_fcustomer',-1);//客户id
        $data['cqn_nextshow'] = I('cqn_nextshow',0);//是否下级
        $cqn_level = I('cqn_level',-1);//下级等级限制
        $data['cqn_level'] = I('cqn_level',-1);//下级等级限制
        $data['cqn_updateuserid'] = $user['fid'];//用户id
        $data['cqn_updatetime'] = date('Y-m-d H:i:s');//创建时间
        $res = M('common_question')->save($data);
        if($res){
            $this->ajaxReturn([
                'code'=>0,
                'msg'=>'保存成功'
            ]);
        }
    }

    //删除问题
    public function delete_qestion()
    {
        $user = session('personInfo');//
        $data = [];
        $data['cqn_id'] = I('cqn_id');
        $data['cqn_deleteuserid'] = $user['fid'];//用户id
        $data['cqn_deletetime'] = date('Y-m-d H:i:s');//删除时间
        $res = M('common_question')->save($data);
        if($res){
            $this->ajaxReturn([
                'code'=>0,
                'msg'=>'删除成功'
            ]);
        }
    }

    //获取所有客户
    public function get_client(){

        $data = M('customer')->where(['cr_id'=>['NEQ',1]])->select();
        $this->ajaxReturn([
            'code'=>0,
            'data'=>$data,
            'msg'=>'数据获取成功'
        ]);

    }

}