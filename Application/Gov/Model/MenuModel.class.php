<?php
namespace Gov\Model;



class MenuModel{


    /*获取菜单*/
    public function menu_list($menu_id){
		
		$menu_1 = M('gov_menu')->where(array('parent_id'=>$menu_id,'menu_state'=>0))->order('menu_sort asc')->select();//查询一级菜单
		
		foreach($menu_1 as $key => $menu){//遍历一级菜单
			$menu_list[$key] = $menu;

			$menu_list[$key]['menu2'] = M('gov_menu')->where(array('parent_id'=>$menu['menu_id'],'menu_state'=>0))->order('menu_sort asc')->select();//查询二级菜单
			
			foreach($menu_list[$key]['menu2'] as $key2 => $menu2){//遍历二级菜单
				$menu_list[$key]['menu2_list'][$key2] = $menu2;
				
				$menu_list[$key]['menu2_list'][$key2]['menu3_list'] = M('gov_menu')->where(array('parent_id'=>$menu2['menu_id'],'menu_state'=>0))->order('menu_sort asc')->select();//查询三级菜单
			}//遍历二级菜单结束
			
			$menu_list[$key]['menu2'] = null;//销毁原二级菜单

		}//遍历一级菜单结束
		return $menu_list;
    }



}