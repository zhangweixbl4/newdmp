<?php
namespace Test\Controller;

use Think\Controller;


class IytController extends Controller {
	
	public function _initialize() {
		$openid = get_openid();
		
		if(C('IS_TEST') != true && !in_array($openid,['oL7o01QQtClD6ze2P8HZCU5ewbNg','oL7o01aFyqnMBF1KFUz6-7Py0Lzk'])){
			$this->ajaxReturn(array('code'=>-2,'msg'=>'没有权限','openid'=>$openid));
		}
		A('Common/AliyunLog','Model')->dmpLogs();
		
		
	}
	
	
	public function index(){
		$openid = get_openid();
		#if($openid) echo $openid;

		$this->display();
	}
	
	public function g(){
		$postData = file_get_contents('php://input');
		$postData = json_decode($postData,true);
		$fc = $postData['fc'];
		#$va = $postData['va'];
		$va = M('iyt')->where(array('fc'=>$fc))->getField('va');
		$va = strval($va);
		$this->ajaxReturn(array('code'=>0,'msg'=>'','va'=>$va));
		
	}
	
	public function p(){
		$postData = file_get_contents('php://input');
		$postData = json_decode($postData,true);
		$fc = strval($postData['fc']);
		$va = strval($postData['va']);
		$vaT = explode("\n",$va);
		$vaL = [];
		foreach($vaT as $vaa){
			$vaa = trim($vaa);
			if(strlen($vaa) > 1) $vaL[] = $vaa;
		}
		M('iyt')->add(array('fc'=>$fc,'va'=>implode("\n",array_unique($vaL))),array(),true);
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'提交成功'));
		
	}
	
	
	
	
}