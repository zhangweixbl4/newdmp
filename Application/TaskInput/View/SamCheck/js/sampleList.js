Vue.prototype.$ELEMENT = { size: "mini", zIndex: 3000 };
Vue.use(VueClipboard)

const vm = new Vue({
  el: '#app',
  components: { aliplayer },
  data() {
    return {
      tableData:[],
      tableHeight: '400px',
      loading: false,
      videoUrl: '',
      sampleUrl: '',
      mediaDay: {},
      compareDialog: false,
      activeRow: {},
      activeIssue: {},
      issList: [],
      fstate: {
        '0': {
          label: '未检查',
          type: 'danger'
        },
        '2': {
          label: '已检查',
          type: 'success'
        },
      },
    }
  },
  computed: {
    issueUrl() {
      return this.activeIssue.m3u8 || ''
    }
  },
  created() {
    this.getList()
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight()
      document.querySelector('#app').style.opacity = 1
    })
    window.onresize = () => {
      this.getTableHeight()
    }
  },
  methods: {
    getList() {
      let data = {
        fid: $h.getQuery('fid'),
        ...this.form,
      }
      this.loading = true
      fly.post('/TaskInput/SamCheck/mediaDay', Qs.stringify(data)).then(({ data }) => {
        if (data.code === 0) {
          this.videoUrl = data.m3u8_24h
          this.tableData = data.samList.map(i => {
            if (i.samissuedate === data.mediaDay.fdate) i.istoday = true

            return i
          })
          this.mediaDay = data.mediaDay
        } else {
          $h.notify.error(data.msg, this)
        }
        this.loading = false
      })
    },
    reset() {
      this.form = JSON.parse(this.sourceForm)
      this.getList()
    },
    getTableHeight() {
      let formHeight = 0
      this.tableHeight = document.body.clientHeight - formHeight - 44 + 'px'
    },
    beforeClose() {
      this.$refs.samplePlayer._stop()
      this.$refs.issuePlayer._stop()
      this.compareDialog = false
      this.activeRow = {}
      this.activeIssue = {}
      this.sampleUrl = ''
      this.issList = []
    },
    compare(row) {
      this.activeRow = row
      this.$set(row, 'checked', true)
      let { fdate, fmediaid } = this.mediaDay
      let data = {
        fdate: fdate,
        mediaId: fmediaid,
        samId: row.fid
      }
      fly.post('/TaskInput/SamCheck/samDay', Qs.stringify(data)).then(({ data }) => {
        if (data.code === 0) {
          this.issList = data.issueList.map(i => {
            i.issdate = `${i.start.substring(11)} ~ ${i.end.substring(11)}`
            return i
          })
          this.sampleUrl = data.samInfo.favifilename
		  this.samInfo = data.samInfo
          this.activeIssue = data.issueList[0]
        }
      })
      this.compareDialog = true
    },
    rowClick(row, column, event) {
      this.activeIssue = row
    },
    finish() {
      let data = {
        fid: $h.getQuery('fid'),
        fstate: '2'
      }
      this.$confirm('已完成当前媒体所有样本的检查?', '确 认', {
        confirmButtonText: '确 定',
        cancelButtonText: '取 消',
        type: 'warning'
      }).then(() => {
        fly.post('/TaskInput/SamCheck/commitCheck', Qs.stringify(data)).then(({ data }) => {
          if (data.code === 0) {
            $h.notify.success(data.msg,this)
            closePage()
          } else {
            $h.notify.error(data.msg,this)
          }
        })
      }).catch(() => {})
    }
  },
})