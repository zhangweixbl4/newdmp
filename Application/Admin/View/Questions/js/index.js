function $stringify(param) {
  var query = []
  for (var k in param) {
      query.push(k + '=' + param[k])
  }
  return query.join('&')
}

Vue.prototype.$ELEMENT = { size: 'mini', zIndex: 3000 }
var vm = new Vue({
  el: '#app',
  data() {
    return {
      tableData: [],
      showadd: false,
      form: {
        cqn_title: '',
        cqn_content: '',
        cqn_recontent: '',
        cqn_fcustomer: '',
        cqn_nextshow: '0',
        cqn_level: [],
      },
      searchForm: {
        keyword: '',
        page: 1
      },
      clientData: [],
      loading: false,
      bloading: false,
      unlimited: false,
      countAll: '0',
      level: {
        '-1': '无限制',
        '0': '国家',
        '1': '省',
        '2': '副省',
        '3': '计划单列市',
        '4': '市',
        '5': '区县',
      }
    }
  },
  created() {
    this._getClient()
    this._getList(1)
  },
  methods: {
    _getClient() {
      axios.post('/Admin/Questions/get_client')
        .then(res => {
          if (res.data.code === 0) {
            this.clientData = res.data.data
          } else {
            this.$message.error(res.data.msg)
          }
          this.loading = false
        })
    },
    _getList(page) {
      this.loading = true
      let data = Object.assign({}, this.searchForm)
      data.page = page || 1
      axios.post('/Admin/Questions/index_list',$stringify(data))
        .then(res => {
          if (res.data.code === 0) {
            this.tableData = res.data.data.list
            this.countAll = res.data.data.count
          } else {
            this.$message.error(res.data.msg)
          }
          this.loading = false
        })
    },
    _submit() {
      this.bloading = true
      let data = Object.assign({}, this.form)
      data.cqn_fcustomer = this.unlimited ? '-1' : data.cqn_fcustomer
      data.cqn_level = `|${data.cqn_level.join('|')}|`
      let url = data.cqn_id ? '/Admin/Questions/edit_qestion_post' : '/Admin/Questions/add_qestion'
      axios.post(url, $stringify(data))
        .then(res => {
          if (res.data.code === 0) {
            this.$message.success(res.data.msg)
            this._getList()
            this.showadd = false
            this._close()
          } else {
            this.$message.error(res.data.msg)
          }
          this.bloading = false
        })
    },
    _edit(obj) {
      this.showadd = true
      this.form.cqn_id = obj.cqn_id
      Object.keys(this.form).forEach(item => {
        if (item === 'cqn_level') {
            this.form[item] = obj[item].split('|').filter(val => {
              return val
            })
        } else if (item === 'cqn_fcustomer') {
          if (obj['cqn_fcustomer'] === '-1') {
            this.unlimited = true
          } else {
            this.form[item] = obj[item]
          }
        }else {
          this.form[item] = obj[item]
        }
      })
    },
    _dconfirm(obj) {
      this.$confirm('此操作将永久删除该问题, 是否继续?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        this._delete(obj)
      }).catch(() => {
        this.$message({
          type: 'info',
          message: '已取消'
        });          
      });
    },
    _delete(obj) {
      let data = {
        cqn_id: obj.cqn_id
      }
      axios.post('/Admin/Questions/delete_qestion', $stringify(data))
        .then(res => {
          if (res.data.code === 0) {
            this.$message.success(res.data.msg)
            this._getList()
          } else {
            this.$message.error(res.data.msg)
          }
      })
    },
    _close() {
      this.form = {
        cqn_title: '',
        cqn_content: '',
        cqn_recontent: '',
        cqn_fcustomer: '',
        cqn_nextshow: '0',
        cqn_level: []
      }
      this.unlimited = false
      this.showadd = false
    }
  }
})