<?php
namespace Api\Controller;
use Think\Controller;

class IllegalController extends Controller {
	
		/*获取违法表现列表*/
    public function get_illegal_list(){
		$fexpression = I('fexpression');//违法表现
		
		$where = array();
		$where['fexpression'] = array('like','%'.$fexpression.'%');
		$where['fpcode'] = array('neq','');
		
		$illegalList = M('tillegal')->cache(true,86400)->field('fcode,fexpression')->where($where)->select();//查询违法表现列表
		$this->ajaxReturn(array('code'=>0,'msg'=>'','value'=>$illegalList));
		
	}
	
}