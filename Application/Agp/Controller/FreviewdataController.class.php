<?php
namespace Agp\Controller;
use Think\Controller;
/**
 * 国家局数据统计
 * by yjm
 */
class FreviewdataController extends BaseController{

    public function illegal_ad_list(){
        /*
         * `fad_name` varchar(100) DEFAULT '' COMMENT '广告名称',
           `fmedia_id` bigint(20) DEFAULT '0' COMMENT '媒介ID',
         * `fad_class_code` varchar(10) DEFAULT '' COMMENT '广告类别代码',
         *
         * `fsample_id` int(11) DEFAULT '0' COMMENT '样本ID',
         *
         * `create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
         *
         * */
        $system_num = getconfig('system_num');
        $map['tbn_illegal_ad.fcustomer'] = $system_num;

        $fsample = M()->field('fid,fillegaltypecode,fillegalcontent,2 as mediatype')
            ->table('tbcsample')
            ->union('SELECT fpapersampleid as fid,fillegaltypecode,fillegalcontent,3 as mediatype FROM tpapersample')
            ->union('SELECT fid,fillegaltypecode,fillegalcontent,1 as mediatype FROM ttvsample')
            ->fetchSql(true)
            ->select();

        $illegal_ad_list = M('tbn_illegal_ad')
            ->field('
                tbn_illegal_ad.fid as fid,
                 (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as tmedia_name,
                tadclass.ffullname as tadclass_name,
                tbn_illegal_ad.fad_name as fad_name,
                fsample.fillegaltypecode as fillegaltypecode,
                fsample.fillegalcontent as fillegalcontent,
                DATE(tbn_illegal_ad.create_time) as create_time
            ')
            ->where($map)
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad.fmedia_id"
            )
            ->join("
                tadclass on 
                tadclass.fcode = tbn_illegal_ad.fad_class_code"
            )
            ->join("
                ($fsample) as fsample on fsample.fid = tbn_illegal_ad.fsample_id
            ")
            ->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on snd.illad_id = tbn_illegal_ad.fid')
            ->page($_GET['page'].',15')
            ->select();//文书模板列表
        $count = M('tbn_illegal_ad')->where($map)->where($map)->count();// 查询满足要求的总记录数
        $this->ajaxReturn([
            'code' =>0,
            'msg' => '获取成功',
            'data' => [
                'count' =>$count,
                'list' =>$illegal_ad_list
            ]
        ]);

    }

}