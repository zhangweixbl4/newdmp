const credit = [{
    path: '/creditData',
    name: 'creditData',
    component: () => import('../components/credit/creditData'),
    meta: ['信用管理', '信用数据']
  },
  {
    path: '/creditReward',
    name: 'creditReward',
    component: () => import('../components/credit/creditReward'),
    meta: ['信用管理', '信用奖励']
  },
  {
    path: '/fjXingyonggz',
    name: 'fjXingyonggz',
    component: () => import('../components/credit/fjXingyonggz'),
    meta: ['信用管理', '规则管理']
  }
]
export default credit
