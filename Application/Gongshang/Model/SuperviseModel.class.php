<?php
namespace Gongshang\Model;



class SuperviseModel{


    /*区域统计*/
    public function region_count($tvissue,$bcissue,$paperissue){
		
		$issue_arr = array_merge($tvissue,$bcissue,$paperissue); //把三个数组合并起来
		$regionIssue = array();//初始化数组
		foreach($issue_arr as $issue){//循环合并后的数组
			$regionIssue[$issue['region_id']]['issue_count'] += $issue['issue_count'];//发布数量相加
			$regionIssue[$issue['region_id']]['illegal_count'] += $issue['illegal_count'];//违法数量相加
			$regionIssue[$issue['region_id']]['slight_illegal'] += $issue['slight_illegal'];//轻微违法违法数量相加
			$regionIssue[$issue['region_id']]['general_illegal'] += $issue['general_illegal'];//一般违法违法数量相加
			$regionIssue[$issue['region_id']]['serious_illegal'] += $issue['serious_illegal'];//严重违法违法数量相加
			$regionIssue[$issue['region_id']]['name'] = $issue['name'];//地区名称
			$regionIssue[$issue['region_id']]['region_id'] = $issue['region_id'];//地区ID
		}

		foreach($regionIssue as $item){
			$regionIssueArr[] = $item;
		}

		return $regionIssueArr;//返回合并好的数据
    }

    /*分类统计*/
    public function class_count($tvclass,$bcclass,$paperclass){

    	$class_arr = array_merge($tvclass,$bcclass,$paperclass); //把三个数组合并起来
		$classIssue = array();//初始化数组
		foreach($class_arr as $issue){//循环合并后的数组
			$classIssue[$issue['class_code']]['issue_count'] += $issue['issue_count'];//发布数量相加
			$classIssue[$issue['class_code']]['illegal_count'] += $issue['illegal_count'];//违法数量相加
			$classIssue[$issue['class_code']]['slight_illegal'] += $issue['slight_illegal'];//轻微违法违法数量相加
			$classIssue[$issue['class_code']]['general_illegal'] += $issue['general_illegal'];//一般违法违法数量相加
			$classIssue[$issue['class_code']]['serious_illegal'] += $issue['serious_illegal'];//严重违法违法数量相加
			$classIssue[$issue['class_code']]['name'] = $issue['name'];//分类名称
			$classIssue[$issue['class_code']]['class_code'] = $issue['class_code'];//分类CODE
		}

		foreach($classIssue as $item){
			$classIssueArr[] = $item;
		}
		
		return $classIssueArr;//返回合并好的数据

    }

    /*媒介机构统计*/
    public function mediaowner_count($mediaowner_tv,$mediaowner_bc,$mediaowner_paper){

    	$mediaowner_arr = array_merge($mediaowner_tv,$mediaowner_bc,$mediaowner_paper); //把三个数组合并起来
		$mediaowner = array();//初始化数组
		foreach($mediaowner_arr as $issue){//循环合并后的数组
			$mediaowner[$issue['mediaowner_id']]['issue_count'] += $issue['issue_count'];//发布数量相加
			$mediaowner[$issue['mediaowner_id']]['illegal_count'] += $issue['illegal_count'];//违法数量相加
			$mediaowner[$issue['mediaowner_id']]['slight_illegal'] += $issue['slight_illegal'];//轻微违法违法数量相加
			$mediaowner[$issue['mediaowner_id']]['general_illegal'] += $issue['general_illegal'];//一般违法违法数量相加
			$mediaowner[$issue['mediaowner_id']]['serious_illegal'] += $issue['serious_illegal'];//严重违法违法数量相加
			$mediaowner[$issue['mediaowner_id']]['name'] = $issue['name'];//媒介机构名称
			$mediaowner[$issue['mediaowner_id']]['mediaowner_id'] = $issue['mediaowner_id'];//媒介机构名称ID
		}

		foreach($mediaowner as $item){
			$mediaownerArr[] = $item;
		}

		return $mediaownerArr;//返回合并好的数据

    }

    /*媒介统计*/
    public function media_count($media_tv,$media_bc,$media_paper){

    	$media_arr = array_merge($media_tv,$media_bc,$media_paper); //把三个数组合并起来
		$media = array();//初始化数组
		foreach($media_arr as $issue){//循环合并后的数组
			$media[$issue['media_id']]['issue_count'] += $issue['issue_count'];//发布数量相加
			$media[$issue['media_id']]['illegal_count'] += $issue['illegal_count'];//违法数量相加
			$media[$issue['media_id']]['slight_illegal'] += $issue['slight_illegal'];//轻微违法违法数量相加
			$media[$issue['media_id']]['general_illegal'] += $issue['general_illegal'];//一般违法违法数量相加
			$media[$issue['media_id']]['serious_illegal'] += $issue['serious_illegal'];//严重违法违法数量相加
			$media[$issue['media_id']]['name'] = $issue['name'];//媒介名称
			$media[$issue['media_id']]['media_id'] = $issue['media_id'];//媒介ID
		}
		
		foreach($media as $item){
			$mediaArr[] = $item;
		}

		return $mediaArr;//返回合并好的数据

    }


    /*发布条件*/
    public function search_issue($regionid = '',$startTime = '',$endTime = ''){

    	$fregionid = session('regulatorpersonInfo.regionid') == 100000?'000000':session('regulatorpersonInfo.regionid');
		$regionid = $regionid?$regionid:$fregionid;//地区ID
		$startTime = $startTime?date('Y-m-d H:i:s',strtotime($startTime)):'';//开始时间
		$endTime = $endTime?date('Y-m-d H:i:s',strtotime($endTime)):'';//结束时间
		if($startTime == '' && $endTime) $startTime = date('Y-m-d H:i:s',strtotime($endTime)-30*86400);
		if($endTime == '' && $startTime) $endTime = date("Y-m-d 00:00:00",time()+86400);
		$region_id_rtrim = rtrim($regionid,'00');//去掉地区后面的00
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位

		$where = array();
		$where['left(tmediaowner.fregionid,'.$tregion_len.')'] = $region_id_rtrim;
		if($startTime && $endTime){
			$where['issue.fissuedate'] = array(array('egt',$startTime),array('lt',$endTime),'and');
		}
		$search_issue['where'] = $where;

		return $search_issue;

    }

    /*监管条件*/
    public function sup_issue($regionid = '',$startTime = '',$endTime = '',$fadclasscode = ''){

		$startTime = $startTime?date('Y-m-d H:i:s',strtotime($startTime)):'';//开始时间
		$endTime = $endTime?date('Y-m-d H:i:s',strtotime($endTime)):'';//结束时间
		if($startTime == '' && $endTime) $startTime = date('Y-m-d H:i:s',strtotime($endTime)-30*86400);
		if($endTime == '' && $startTime) $endTime = date("Y-m-d 00:00:00",time()+86400);
		$region_id_rtrim = rtrim($regionid,'00');//去掉地区后面的00
		$tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位

		$where = array();
		$where['left(tmediaowner.fregionid,'.$tregion_len.')'] = $region_id_rtrim;
		if($startTime && $endTime){
			$where['issue.fissuedate'] = array(array('egt',$startTime),array('lt',$endTime),'and');
		}
		if($fadclasscode){//广告类别
			$where['left(tad.fadclasscode,2)'] = array('in',$fadclasscode);
		}
		$search_issue['where'] = $where;

		return $search_issue;

    }

    /*媒介机构统计(重点监管)*/
    public function sup_mediaowner_count($mediaowner_tv,$mediaowner_bc,$mediaowner_paper){

    	$mediaowner_arr = array_merge($mediaowner_tv,$mediaowner_bc,$mediaowner_paper); //把三个数组合并起来
		$mediaowner = array();//初始化数组
		foreach($mediaowner_arr as $issue){//循环合并后的数组
			$mediaowner[$issue['mediaowner_id']]['sample_count'] += $issue['sample_count'];//发布数量相加
			$mediaowner[$issue['mediaowner_id']]['issue_count'] += $issue['issue_count'];//发布数量相加
			$mediaowner[$issue['mediaowner_id']]['illegal_count'] += $issue['illegal_count'];//违法数量相加
			$mediaowner[$issue['mediaowner_id']]['slight_illegal'] += $issue['slight_illegal'];//轻微违法违法数量相加
			$mediaowner[$issue['mediaowner_id']]['general_illegal'] += $issue['general_illegal'];//一般违法违法数量相加
			$mediaowner[$issue['mediaowner_id']]['serious_illegal'] += $issue['serious_illegal'];//严重违法违法数量相加
			$mediaowner[$issue['mediaowner_id']]['name'] = $issue['name'];//媒介机构名称
			$mediaowner[$issue['mediaowner_id']]['mediaowner_id'] = $issue['mediaowner_id'];//媒介机构名称ID
		}

		foreach($mediaowner as $item){
			$mediaownerArr[] = $item;
		}

		return $mediaownerArr;//返回合并好的数据

    }

    /*分类统计(重点监管)*/
    public function sup_class_count($tvclass,$bcclass,$paperclass){

    	$class_arr = array_merge($tvclass,$bcclass,$paperclass); //把三个数组合并起来
		$classIssue = array();//初始化数组
		foreach($class_arr as $issue){//循环合并后的数组
			$classIssue[$issue['class_code']]['sample_count'] += $issue['sample_count'];//发布样本相加
			$classIssue[$issue['class_code']]['issue_count'] += $issue['issue_count'];//发布数量相加
			$classIssue[$issue['class_code']]['illegal_count'] += $issue['illegal_count'];//违法数量相加
			$classIssue[$issue['class_code']]['slight_illegal'] += $issue['slight_illegal'];//轻微违法违法数量相加
			$classIssue[$issue['class_code']]['general_illegal'] += $issue['general_illegal'];//一般违法违法数量相加
			$classIssue[$issue['class_code']]['serious_illegal'] += $issue['serious_illegal'];//严重违法违法数量相加
			$classIssue[$issue['class_code']]['name'] = $issue['name'];//分类名称
			$classIssue[$issue['class_code']]['class_code'] = $issue['class_code'];//分类CODE
		}

		foreach($classIssue as $item){
			$classIssueArr[] = $item;
		}
		
		return $classIssueArr;//返回合并好的数据

    }

    /*统计分类总违法数量*/
    public function illegal_class_count($class_count){

    	foreach ($class_count as $value) {
    		$illegal_count += $value['illegal_count'];
    	}

    	return $illegal_count;
    }

    /*星期统计(重点监管)*/
    public function sup_weeks_count($tv_weeks,$bc_weeks,$paper_weeks){

    	$weeks_arr = array_merge($tv_weeks,$bc_weeks,$paper_weeks); //把三个数组合并起来
		$weeksIssue = array();//初始化数组
		foreach($weeks_arr as $issue){//循环合并后的数组
			$weeksIssue[$issue['weeks']]['issue_count'] += $issue['issue_count'];//发布数量相加
			$weeksIssue[$issue['weeks']]['illegal_count'] += $issue['illegal_count'];//违法数量相加
			$weeksIssue[$issue['weeks']]['weeks'] = $issue['weeks'];//weeks
		}

		foreach($weeksIssue as $item){
			$weeksIssueArr[$item['weeks']] = $item;
		}
		
		return $weeksIssueArr;//返回合并好的数据

    }

    /*媒介机构信用统计*/
    public function mediaowner_credit_count($mediaowner_credit_tv='',$mediaowner_credit_bc='',$mediaowner_credit_paper=''){

    	$mediaowner_credit = array_merge($mediaowner_credit_tv,$mediaowner_credit_bc,$mediaowner_credit_paper); //把三个数组合并起来
		$mediaowner = array();//初始化数组
		foreach($mediaowner_credit as $issue){//循环合并后的数组
			$mediaowner[$issue['mediaowner_id']]['issue_count'] += $issue['issue_count'];//发布数量相加
			$mediaowner[$issue['mediaowner_id']]['illegal_count'] += $issue['illegal_count'];//违法数量相加
			$mediaowner[$issue['mediaowner_id']]['serious_illegal'] += $issue['serious_illegal'];//严重违法违法数量相加
			$mediaowner[$issue['mediaowner_id']]['sample_count'] += $issue['sample_count'];//发布样本数量相加
			$mediaowner[$issue['mediaowner_id']]['sample_illegal_count'] += $issue['sample_illegal_count'];//样本违法数量相加
			$mediaowner[$issue['mediaowner_id']]['sample_serious_illegal'] += $issue['sample_serious_illegal'];//样本严重违法违法数量相加
			$mediaowner[$issue['mediaowner_id']]['public_welfare'] += $issue['public_welfare'];//严重违法违法数量相加
			$mediaowner[$issue['mediaowner_id']]['name'] = $issue['name'];//媒介名称
			$mediaowner[$issue['mediaowner_id']]['mediaowner_id'] = $issue['mediaowner_id'];//媒介ID
		}
		
		foreach($mediaowner as $item){
			$mediaownerArr[] = $item;
		}

		return $mediaownerArr;//返回合并好的数据

    }

}