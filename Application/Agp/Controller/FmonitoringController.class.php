<?php
namespace Agp\Controller;
use Think\Controller;
use Think\Exception;

import('Vendor.PHPExcel');

/**
 * 监测数据
 */

class FmonitoringController extends BaseController
{
	/**
	 * 获取监测数据
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function index()
	{
		session_write_close();
		Check_QuanXian(['home','adinquire','publishlist','illegaltadFJ','televisionFJ','broadcastFJ','newspaperFJ','tadsearchFJ','monitoringData','monitoringData2']);
		header("Content-type:text/html;charset=utf-8");
		ini_set('memory_limit','2048M');
		ini_set('max_execution_time', 0);//设置超时时间

		$ALL_CONFIG = getconfig('ALL');
        $ischeck = $ALL_CONFIG['ischeck'];
        $media_class = $ALL_CONFIG['media_class'];
        $system_num = $ALL_CONFIG['system_num'];
        $use_open_search = $ALL_CONFIG['use_open_search'];
        
		$outtype = I('outtype');//导出类型
		$outdata = I('outdata');//读取外部数据

		$p = I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录
		if(empty($outtype)){
			$limitstr = ' limit '.($p-1)*$pp.','.$pp;
		}

		$pxtype 	= I('pxtype')?I('pxtype'):2;//排序方式，0默认
		$pxval 		= I('pxval')?I('pxval'):0;//排序值，0默认
		$fadname 			= I('fadname');// 广告名称
		$fadclasscode 		= I('fadclasscode');// 广告内容类别组
		$area 				= I('area');// 行政区划
		$media 				= I('media');//媒体组
		$mclass 			= I('mclass')?I('mclass'):'';//媒体类型
		$fillegaltypecode 	= I('fillegaltypecode');// 违法类型
		$fissuedatest2 		= I('fissuedatest');//发布时间起
		$fissuedateed2 		= I('fissuedateed');//发布时间止
		$iscontain 			= I('iscontain');//是否包含下属地区
		$netadtype 			= I('netadtype');//平台类型
		$version 			= I('version');//版本说明
		if(strlen($fillegaltypecode)==0){
			$fillegaltypecode = -1;
		}

		$fissuedatest = strtotime($fissuedatest2);
		$fissuedateed = strtotime($fissuedateed2);

		if(empty($area)){
			$area = session('regulatorpersonInfo.regionid');
		}

		if($area == 100000){
            $tb_tvissue = gettable('tv',$fissuedatest,110000);
            $tb_bcissue = gettable('bc',$fissuedatest,110000);
            $where['_string'] = '1=0';
        }else{
            $tb_tvissue = gettable('tv',$fissuedatest,$area);
            $tb_bcissue = gettable('bc',$fissuedatest,$area);
            $where['_string'] = '1=1';
        }
        $tb_paperissue = 'tpaperissue';
		$tb_netissue = 'tnetissueputlog';
		$tb_odissue = 'huwai_ad';
		$tb_tvsam = 'ttvsample';
		$tb_bcsam = 'tbcsample';
		$tb_papersam = 'tpapersample';
		$tb_netsam = 'tnetissue';

        if(empty(in_array('13', $media_class))){
			$wherestrnet = ' and 1=0 ';
		}else{
			$wherestrnet = ' and is_first_broadcast = 1 ';
		}

		$orders = '';
		if($pxtype==1){//1时按发布媒体排序
			if($pxval==0){
				$orders = $orders?$orders.',fmedianame desc':' fmedianame desc';
			}else{
				$orders = $orders?$orders.',fmedianame asc':' fmedianame asc';
			}
		}elseif($pxtype==2){//2时按发布时间排序
			if($pxval==0){
				$orders = $orders?$orders.',fissuedate desc':' fissuedate desc';
			}else{
				$orders = $orders?$orders.',fissuedate asc':' fissuedate asc';
			}
		}else{
			if($pxval==0){
				$orders = $orders?$orders.',fissuedate desc':' fissuedate desc';
			}
		}

		if (!empty($fadname)) {
			$where['_string'] .= ' and fadname like "%'.$fadname.'%"';
		}

		if(!empty($media)){
			$where['_string'] .= ' and tmedia.fid in ('.implode(',', $media).')';
		}
		//获取非备用媒体数据
		$where['_string'] .= ' and tmedia.fid = tmedia.main_media_id';

		$arr_code = [];
		if(is_array($fadclasscode)){
			foreach ($fadclasscode as $key => $value) {
				$codes = D('Function')->get_next_tadclasscode($value,true);//获取下级广告类别代码
				if(!empty($codes)){
					$arr_code = array_merge($arr_code,$codes);
				}else{
					array_push($arr_code,$value);
				}
			}
			$wherestrtv 	.= ' and fadclasscode  in('.implode(',', $arr_code).')';
			$wherestrpaper 	.= ' and fadclasscode  in('.implode(',', $arr_code).')';
			$wherestrbc 	.= ' and fadclasscode  in('.implode(',', $arr_code).')';
			$wherestrnet 	.= ' and fadclasscode  in('.implode(',', $arr_code).')';
			$wherestrod 	.= ' and 1 = 0';
		}
		
		if($fillegaltypecode>0){//违法类型
			$wherestrtv 	.= ' and '.$tb_tvsam.'.fillegaltypecode > 0';
			$wherestrpaper 	.= ' and '.$tb_papersam.'.fillegaltypecode > 0';
			$wherestrbc 	.= ' and '.$tb_bcsam.'.fillegaltypecode > 0';
			$wherestrnet 	.= ' and '.$tb_netsam.'.fillegaltypecode > 0';
			$wherestrod 	.= ' and 1 = 0';
		}elseif($fillegaltypecode == 0){
			$wherestrtv 	.= ' and '.$tb_tvsam.'.fillegaltypecode = 0';
			$wherestrpaper 	.= ' and '.$tb_papersam.'.fillegaltypecode = 0';
			$wherestrbc 	.= ' and '.$tb_bcsam.'.fillegaltypecode = 0';
			$wherestrnet 	.= ' and '.$tb_netsam.'.fillegaltypecode = 0';
		}

		if(!empty($netadtype)){//平台类型
			$wherestrnet .= ' and '.$tb_netissue.'.net_platform = '.$netadtype;
		}

		if(!empty($mclass)){
			if($mclass=='tv'){
				$where['_string'] .= ' and left(tmedia.fmediaclassid,2) = "01"';
			}elseif($mclass=='bc'){
				$where['_string'] .= ' and left(tmedia.fmediaclassid,2) = "02"';
			}elseif($mclass=='paper'){
				$where['_string'] .= ' and left(tmedia.fmediaclassid,2) = "03"';
			}elseif($mclass=='net'){
				$where['_string'] .= ' and left(tmedia.fmediaclassid,2) = "13"';
			}elseif($mclass=='od'){
				$where['_string'] .= ' and left(tmedia.fmediaclassid,2) = "05"';
			}
		}

		if(!empty($fissuedatest2) && !empty($fissuedateed2)){
			$wherestrtv 	.= ' and '.$tb_tvissue.'.fissuedate<='.$fissuedateed.' and '.$tb_tvissue.'.fissuedate>='.$fissuedatest;
			$wherestrpaper 	.= ' and '.$tb_paperissue.'.fissuedate between "'.$fissuedatest2.'" and "'.$fissuedateed2.'"';
			$wherestrbc 	.= ' and '.$tb_bcissue.'.fissuedate<='.$fissuedateed.' and '.$tb_bcissue.'.fissuedate>='.$fissuedatest;
			$wherestrnet 	.= ' and '.$tb_netissue.'.fissuedate<='.$fissuedateed.' and '.$tb_netissue.'.fissuedate>='.$fissuedatest;
			$wherestrod 	.= ' and '.$tb_odissue.'.fissuedate between "'.$fissuedatest2.'" and "'.$fissuedateed2.'"';
		}
		if(!empty($version)){
			$wherestrtv 	.= ' and '.$tb_tvsam.'.fversion like "%'.$version.'%"';
			$wherestrpaper 	.= ' and '.$tb_papersam.'.fversion like "%'.$version.'%"';
			$wherestrbc 	.= ' and '.$tb_bcsam.'.fversion like "%'.$version.'%"';
			$wherestrnet 	.= ' and 1=0 ';
			$wherestrod 	.= ' and 1=0 ';
		}

		//是否抽查模式
		if(!empty($ischeck)){
		    $spot_check_data = M('spot_check')->where(['fcustomer'=>$system_num])->select();//查询抽查表数据
		    //如果抽查表有数据
		    if(!empty($spot_check_data)){
		        $dates = [];//定义日期数组
		        foreach ($spot_check_data as $spot_check_data_key=>$spot_check_data_val){
		            $year_month = '';
		            $date_str = [];
		            $year_month = substr($spot_check_data_val['fmonth'],0,7);
		            if(!empty($spot_check_data_val['condition'])){
		            	$date_str = explode(',',$spot_check_data_val['condition']);
		            }
		            foreach ($date_str as $date_str_val){
		            	$cctime = $year_month.'-'.$date_str_val;
		            	if($fissuedatest<=strtotime($cctime) && $fissuedateed>=strtotime($cctime)){
			                $dates[] = $cctime;
		            	}
		            }
		        }
		        $wherestrtv 	.= ' and from_unixtime('.$tb_tvissue.'.fissuedate,"%Y-%m-%d") in ("'.implode('","', $dates).'")';
				$wherestrpaper 	.= ' and '.$tb_paperissue.'.fissuedate in ("'.implode('","', $dates).'")';
				$wherestrbc 	.= ' and from_unixtime('.$tb_bcissue.'.fissuedate,"%Y-%m-%d") in ("'.implode('","', $dates).'")';
				$wherestrnet 	.= ' and from_unixtime('.$tb_netissue.'.fissuedate,"%Y-%m-%d") in ("'.implode('","', $dates).'")';
				$wherestrod 	.= ' and '.$tb_odissue.'.fissuedate in ("'.implode('","', $dates).'")';
		    }else{
		        $wherestrtv 	.= ' and 1=0';
				$wherestrpaper 	.= ' and 1=0';
				$wherestrbc 	.= ' and 1=0';
				$wherestrnet 	.= ' and 1=0';
				$wherestrod 	.= ' and 1=0';
		    }
		}

		if(!empty($iscontain)){
			$tregion_len = get_tregionlevel($area);
			if($tregion_len == 1){//国家级
				$where['_string'] .= ' and tmedia.media_region_id ='.$area;	
			}elseif($tregion_len == 2){//省级
				$where['_string'] .= ' and tmedia.media_region_id like "'.substr($area,0,2).'%"';
			}elseif($tregion_len == 4){//市级
				$where['_string'] .= ' and tmedia.media_region_id like "'.substr($area,0,4).'%"';
			}elseif($tregion_len == 6){//县级
				$where['_string'] .= ' and tmedia.media_region_id like "'.substr($area,0,6).'%"';
			}
		}else{
			$where['_string'] .= ' and tmedia.media_region_id ='.$area;
		}

		$joins = ' inner join (select distinct(fmediaid) from tmedia_temp where ftype=1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid').' ) as ttp on tmedia.fid=ttp.fmediaid ';

	    //根据不同的媒体类型，搜索不同的表
	    if($mclass == ''){
	    	$countsql = 'select sum(acount) as acount from (
	    	(select count(*) as acount 
	    		from '.$tb_tvissue.' 
				inner join '.$tb_tvsam.' on '.$tb_tvissue.'.fsampleid='.$tb_tvsam.'.fid  and '.$tb_tvsam.'.fstate = 1
				inner join tad on '.$tb_tvsam.'.fadid=tad.fadid and tad.fadid<>0 
				inner join tadowner on tad.fadowner = tadowner.fid
				inner join tadclass on tad.fadclasscode=tadclass.fcode 
				inner join tmedia on '.$tb_tvissue.'.fmediaid=tmedia.fid
				'.$joins.'
				where '.$where['_string'].$wherestrtv.')
			union all (select count(*) as acount 
				from '.$tb_bcissue.' 
				inner join '.$tb_bcsam.' on '.$tb_bcissue.'.fsampleid='.$tb_bcsam.'.fid  and '.$tb_bcsam.'.fstate = 1
				inner join tad on '.$tb_bcsam.'.fadid=tad.fadid  and tad.fadid<>0
				inner join tadowner on tad.fadowner = tadowner.fid
				inner join tadclass on tad.fadclasscode=tadclass.fcode 
				inner join tmedia on '.$tb_bcissue.'.fmediaid=tmedia.fid 
				'.$joins.'
				where '.$where['_string'].$wherestrbc.')
			union all (select count(*) as acount 
				from '.$tb_paperissue.' 
				inner join '.$tb_papersam.' on '.$tb_paperissue.'.fpapersampleid='.$tb_papersam.'.fpapersampleid and '.$tb_papersam.'.fstate = 1
				inner join tad on '.$tb_papersam.'.fadid=tad.fadid and tad.fadid<>0
				inner join tadowner on tad.fadowner = tadowner.fid
				inner join tadclass on tad.fadclasscode=tadclass.fcode 
				inner join tmedia on '.$tb_paperissue.'.fmediaid=tmedia.fid 
				'.$joins.'  
				where '.$where['_string'].$wherestrpaper.')
			union all (select count(*) as acount 
				from '.$tb_netsam.'putlog 
				inner join '.$tb_netsam.' on '.$tb_netsam.'putlog.tid='.$tb_netsam.'.major_key and finputstate=2  
				inner join tadowner on '.$tb_netsam.'.fadowner = tadowner.fid 
				inner join tadclass on '.$tb_netsam.'.fadclasscode=tadclass.fcode 
				inner join tmedia on '.$tb_netsam.'putlog.fmediaid=tmedia.fid 
				'.$joins.'
				where '.$where['_string'].$wherestrnet.')
			union all (select count(*) as acount
				from '.$tb_odissue.' 
				inner join tmedia on '.$tb_odissue.'.fmediaid=tmedia.fid 
				'.$joins.' 
				where '.$where['_string'].$wherestrod.') 
			) as a';
			$count = M()->cache(true,60)->query($countsql);

			$datasql = 'select * from (
			(select "tv" as mclass,'.$tb_tvissue.'.fmediaid,tadclass.ffullname as fadclass,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id as fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,FROM_UNIXTIME('.$tb_tvissue.'.fstarttime) as fissuedate,"" as fpage,FROM_UNIXTIME('.$tb_tvissue.'.fendtime) as fendtime, flength, '.$tb_tvsam.'.fillegalcontent, '.$tb_tvsam.'.fexpressioncodes,'.$tb_tvissue.'.fid as sid,tad.fadname,tad.fadclasscode,tadowner.fname as fadownername,'.$tb_tvsam.'.favifilename ysscurl,"" fbyurl,"" ldyurl 
				from '.$tb_tvissue.'
				inner join '.$tb_tvsam.' on '.$tb_tvissue.'.fsampleid='.$tb_tvsam.'.fid  and '.$tb_tvsam.'.fstate = 1
				inner join tad on '.$tb_tvsam.'.fadid=tad.fadid and tad.fadid<>0
				inner join tadowner on tad.fadowner = tadowner.fid
				inner join tadclass on tad.fadclasscode=tadclass.fcode 
				inner join tmedia on '.$tb_tvissue.'.fmediaid=tmedia.fid 
				inner join tmediaowner on tmedia.fmediaownerid=tmediaowner.fid 
				'.$joins.' 
				where '.$where['_string'].$wherestrtv.') 
			union all (select "bc" as mclass,'.$tb_bcissue.'.fmediaid,tadclass.ffullname as fadclass,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id as fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,FROM_UNIXTIME('.$tb_bcissue.'.fstarttime) as fissuedate,"" as fpage,FROM_UNIXTIME('.$tb_bcissue.'.fendtime) as fendtime, flength, '.$tb_bcsam.'.fillegalcontent, '.$tb_bcsam.'.fexpressioncodes,'.$tb_bcissue.'.fid as sid,tad.fadname,tad.fadclasscode,tadowner.fname as fadownername,'.$tb_bcsam.'.favifilename ysscurl,"" fbyurl,"" ldyurl
				from '.$tb_bcissue.' 
				inner join '.$tb_bcsam.' on '.$tb_bcissue.'.fsampleid='.$tb_bcsam.'.fid  and '.$tb_bcsam.'.fstate = 1
				inner join tad on '.$tb_bcsam.'.fadid=tad.fadid and tad.fadid<>0
				inner join tadowner on tad.fadowner = tadowner.fid
				inner join tadclass on tad.fadclasscode=tadclass.fcode 
				inner join tmedia on '.$tb_bcissue.'.fmediaid=tmedia.fid 
				'.$joins.' 
				where '.$where['_string'].$wherestrbc.') 
			union all (select "paper" as mclass,'.$tb_paperissue.'.fmediaid,tadclass.ffullname as fadclass,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id as fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,'.$tb_paperissue.'.fissuedate, fpage,"" as fendtime, fpage as flength, '.$tb_papersam.'.fillegalcontent, '.$tb_papersam.'.fexpressioncodes,'.$tb_paperissue.'.fpaperissueid as sid,tad.fadname,tad.fadclasscode,tadowner.fname as fadownername,'.$tb_papersam.'.fjpgfilename ysscurl,"" fbyurl,"" ldyurl
				from '.$tb_paperissue.' 
				inner join '.$tb_papersam.' on '.$tb_paperissue.'.fpapersampleid='.$tb_papersam.'.fpapersampleid  and '.$tb_papersam.'.fstate = 1
				inner join tad on '.$tb_papersam.'.fadid=tad.fadid and tad.fadid<>0
				inner join tadowner on tad.fadowner = tadowner.fid
				inner join tadclass on tad.fadclasscode=tadclass.fcode 
				inner join tmedia on '.$tb_paperissue.'.fmediaid=tmedia.fid 
				'.$joins.' 
				where '.$where['_string'].$wherestrpaper.') 
			union all (select "net" as mclass, '.$tb_netsam.'putlog.fmediaid,tadclass.ffullname as fadclass,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id as fregionid, (case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,FROM_UNIXTIME('.$tb_netsam.'putlog.fissuedate,"%Y-%m-%d") as fissuedate, "" as fpage,"" as fendtime, "" as flength, '.$tb_netsam.'.fillegalcontent, '.$tb_netsam.'.fexpressioncodes, '.$tb_netsam.'putlog.major_key as sid, '.$tb_netsam.'.fadname, '.$tb_netsam.'.fadclasscode,tadowner.fname as fadownername,"" ysscurl,'.$tb_netsam.'.net_original_url fbyurl,'.$tb_netsam.'.net_target_url ldyurl
				from '.$tb_netsam.'putlog
				inner join '.$tb_netsam.' on '.$tb_netsam.'putlog.tid='.$tb_netsam.'.major_key  and finputstate=2  
				inner join tadowner on '.$tb_netsam.'.fadowner = tadowner.fid
				inner join tadclass on '.$tb_netsam.'.fadclasscode=tadclass.fcode 
				inner join tmedia on '.$tb_netsam.'putlog.fmediaid=tmedia.fid 
				'.$joins.' 
				where '.$where['_string'].$wherestrnet.') 
			union all (select "od" as mclass, '.$tb_odissue.'.fmediaid,"" fadclass,(case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id as fregionid, "不违法" fillegaltype,'.$tb_odissue.'.fissuedatetime fissuedate, "" as fpage,"" as fendtime, "" as flength, "" fillegalcontent, "" fexpressioncodes, '.$tb_odissue.'.fid as sid, '.$tb_odissue.'.fadname, 0 fadclasscode,"" fadownername,fimgs ysscurl,"" fbyurl,"" ldyurl
				from '.$tb_odissue.' 
				inner join tmedia on '.$tb_odissue.'.fmediaid=tmedia.fid 
				'.$joins.' 
				where '.$where['_string'].$wherestrod.') 
			) as a ORDER BY '.$orders.$limitstr;
	    }elseif($mclass=='tv'){
	    	$countsql = 'select count(*) as acount 
	    		from '.$tb_tvissue.' 
				inner join '.$tb_tvsam.' on '.$tb_tvissue.'.fsampleid='.$tb_tvsam.'.fid  and '.$tb_tvsam.'.fstate = 1
				inner join tad on '.$tb_tvsam.'.fadid=tad.fadid and tad.fadid<>0
				inner join tadowner on tad.fadowner = tadowner.fid
				inner join tadclass on tad.fadclasscode=tadclass.fcode 
				inner join tmedia on '.$tb_tvissue.'.fmediaid=tmedia.fid 
				'.$joins.' 
				where '.$where['_string'].$wherestrtv;
			$count = M()->cache(true,60)->query($countsql);

			$datasql = 'select "tv" as mclass,'.$tb_tvissue.'.fmediaid,tadclass.ffullname as fadclass,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id as fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,FROM_UNIXTIME('.$tb_tvissue.'.fstarttime) as fissuedate,"" as fpage,FROM_UNIXTIME('.$tb_tvissue.'.fendtime) as fendtime, flength, '.$tb_tvsam.'.fillegalcontent, '.$tb_tvsam.'.fexpressioncodes,'.$tb_tvissue.'.fid as sid,tad.fadname,tad.fadclasscode,tadowner.fname as fadownername,'.$tb_tvsam.'.favifilename ysscurl,"" fbyurl,"" ldyurl
				from '.$tb_tvissue.'
				inner join '.$tb_tvsam.' on '.$tb_tvissue.'.fsampleid='.$tb_tvsam.'.fid  and '.$tb_tvsam.'.fstate = 1
				inner join tad on '.$tb_tvsam.'.fadid=tad.fadid  and tad.fadid<>0
				inner join tadowner on tad.fadowner = tadowner.fid
				inner join tadclass on tad.fadclasscode=tadclass.fcode 
				inner join tmedia on '.$tb_tvissue.'.fmediaid=tmedia.fid
				'.$joins.' 
				where '.$where['_string'].$wherestrtv.'
				ORDER BY '.$orders.$limitstr;
	    }elseif($mclass=='bc'){
	    	$countsql = 'select count(*) as acount 
	    		from '.$tb_bcissue.' 
				inner join '.$tb_bcsam.' on '.$tb_bcissue.'.fsampleid='.$tb_bcsam.'.fid  and '.$tb_bcsam.'.fstate = 1
				inner join tad on '.$tb_bcsam.'.fadid=tad.fadid and tad.fadid<>0
				inner join tadowner on tad.fadowner = tadowner.fid
				inner join tadclass on tad.fadclasscode=tadclass.fcode 
				inner join tmedia on '.$tb_bcissue.'.fmediaid=tmedia.fid 
				'.$joins.'
				where '.$where['_string'].$wherestrbc;
			$count = M()->cache(true,60)->query($countsql);

			$datasql = 'select "bc" as mclass,'.$tb_bcissue.'.fmediaid,tadclass.ffullname as fadclass,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id as fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,FROM_UNIXTIME('.$tb_bcissue.'.fstarttime) as fissuedate,"" as fpage,FROM_UNIXTIME('.$tb_bcissue.'.fendtime) as fendtime, flength, '.$tb_bcsam.'.fillegalcontent, '.$tb_bcsam.'.fexpressioncodes,'.$tb_bcissue.'.fid as sid,tad.fadname,tad.fadclasscode,tadowner.fname as fadownername,'.$tb_bcsam.'.favifilename ysscurl,"" fbyurl,"" ldyurl
				from '.$tb_bcissue.' 
				inner join '.$tb_bcsam.' on '.$tb_bcissue.'.fsampleid='.$tb_bcsam.'.fid  and '.$tb_bcsam.'.fstate = 1
				inner join tad on '.$tb_bcsam.'.fadid=tad.fadid and tad.fadid<>0
				inner join tadowner on tad.fadowner = tadowner.fid
				inner join tadclass on tad.fadclasscode=tadclass.fcode 
				inner join tmedia on '.$tb_bcissue.'.fmediaid=tmedia.fid 
				'.$joins.'
				where '.$where['_string'].$wherestrbc.' 
				ORDER BY '.$orders.$limitstr;
	    }elseif($mclass=='paper'){
	    	$countsql = 'select count(*) as acount 
	    		from '.$tb_paperissue.' 
				inner join '.$tb_papersam.' on '.$tb_paperissue.'.fpapersampleid='.$tb_papersam.'.fpapersampleid  and '.$tb_papersam.'.fstate = 1
				inner join tad on '.$tb_papersam.'.fadid=tad.fadid and tad.fadid<>0
				inner join tadowner on tad.fadowner = tadowner.fid
				inner join tadclass on tad.fadclasscode=tadclass.fcode 
				inner join tmedia on '.$tb_paperissue.'.fmediaid=tmedia.fid 
				'.$joins.' 
				where '.$where['_string'].$wherestrpaper;
			$count = M()->cache(true,60)->query($countsql);

			$datasql = 'select "paper" as mclass,'.$tb_paperissue.'.fmediaid,tadclass.ffullname as fadclass,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id as fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,'.$tb_paperissue.'.fissuedate, fpage,"" as fendtime, fpage as flength, '.$tb_papersam.'.fillegalcontent, '.$tb_papersam.'.fexpressioncodes,'.$tb_paperissue.'.fpaperissueid as sid,tad.fadname,tad.fadclasscode,tadowner.fname as fadownername,'.$tb_papersam.'.fjpgfilename ysscurl,"" fbyurl,"" ldyurl
				from '.$tb_paperissue.' 
				inner join '.$tb_papersam.' on '.$tb_paperissue.'.fpapersampleid='.$tb_papersam.'.fpapersampleid  and '.$tb_papersam.'.fstate = 1
				inner join tad on '.$tb_papersam.'.fadid=tad.fadid and tad.fadid<>0
				inner join tadowner on tad.fadowner = tadowner.fid
				inner join tadclass on tad.fadclasscode=tadclass.fcode 
				inner join tmedia on '.$tb_paperissue.'.fmediaid=tmedia.fid 
				'.$joins.'
				where '.$where['_string'].$wherestrpaper.' 
				ORDER BY '.$orders.$limitstr;
	    }elseif($mclass=='net'){
	    	$countsql = 'select count(*) as acount 
	    		from '.$tb_netsam.'putlog 
				inner join '.$tb_netsam.' on '.$tb_netsam.'putlog.tid='.$tb_netsam.'.major_key  and finputstate=2  
				inner join tadowner on '.$tb_netsam.'.fadowner = tadowner.fid
				inner join tadclass on '.$tb_netsam.'.fadclasscode=tadclass.fcode 
				inner join tmedia on '.$tb_netsam.'putlog.fmediaid=tmedia.fid 
				'.$joins.'
				where '.$where['_string'].$wherestrnet;
			$count = M()->cache(true,60)->query($countsql);

			$datasql = 'select "net" as mclass, '.$tb_netsam.'putlog.fmediaid,tadclass.ffullname as fadclass,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id as fregionid, (case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,FROM_UNIXTIME('.$tb_netsam.'putlog.fissuedate,"%Y-%m-%d") as fissuedate, "" as fpage,"" as fendtime, "" as flength, '.$tb_netsam.'.fillegalcontent, '.$tb_netsam.'.fexpressioncodes, '.$tb_netsam.'putlog.major_key as sid, '.$tb_netsam.'.fadname, '.$tb_netsam.'.fadclasscode,tadowner.fname as fadownername,"" ysscurl,'.$tb_netsam.'.net_original_url fbyurl,'.$tb_netsam.'.net_target_url ldyurl
				from '.$tb_netsam.'putlog
				inner join '.$tb_netsam.' on '.$tb_netsam.'putlog.tid='.$tb_netsam.'.major_key  and finputstate=2  
				inner join tadowner on '.$tb_netsam.'.fadowner = tadowner.fid
				inner join tadclass on '.$tb_netsam.'.fadclasscode=tadclass.fcode 
				inner join tmedia on '.$tb_netsam.'putlog.fmediaid=tmedia.fid 
				'.$joins.'
				where '.$where['_string'].$wherestrnet.' 
				ORDER BY '.$orders.$limitstr;
	    }elseif($mclass=='od'){
	    	$countsql = 'select count(*) as acount
				from '.$tb_odissue.' 
				inner join tmedia on '.$tb_odissue.'.fmediaid=tmedia.fid 
				'.$joins.' 
				where '.$where['_string'].$wherestrod;
			$count = M()->cache(true,60)->query($countsql);

			$datasql = 'select "od" as mclass, '.$tb_odissue.'.fmediaid,"" fadclass,(case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.media_region_id as fregionid, "不违法" fillegaltype,'.$tb_odissue.'.fissuedatetime fissuedate, "" as fpage,"" as fendtime, "" as flength, "" fillegalcontent, "" fexpressioncodes, '.$tb_odissue.'.fid as sid, '.$tb_odissue.'.fadname, 0 fadclasscode,"" fadownername,fimgs ysscurl,"" fbyurl,"" ldyurl
				from '.$tb_odissue.' 
				inner join tmedia on '.$tb_odissue.'.fmediaid=tmedia.fid 
				'.$joins.' 
				where '.$where['_string'].$wherestrod.' 
				ORDER BY '.$orders.$limitstr;
	    }

		$data = M()->cache(true,60)->query($datasql);

		foreach ($data as $key => $value) {
			$data[$key]['regionname'] = M('tregion')->cache(true,600)->where(['fid'=>$value['fregionid']])->getfield('ffullname');
		}
		
		if(!empty($outtype)){
			if(empty($data)){
				$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
			}
			if($outtype=='xls'){
		        $outdata['datalie'] = [
		          '序号'=>'key',
		          '广告名称'=>'fadname',
		          '广告主'=>'fadownername',
		          '广告内容类别'=>'fadclass',
		          '发布媒体'=>'fmedianame',
		          '媒体类型'=>[
		            'type'=>'zwif',
		            'data'=>[
		              ['{mclass} == "tv"','电视'],
		              ['{mclass} == "bc"','广播'],
		              ['{mclass} == "paper"','报纸'],
		              ['{mclass} == "od"','户外'],
		              ['{mclass} == "net"','互联网'],
		            ]
		          ],
		          '违法类型'=>'fillegaltype',
		          '发布时间'=>'fissuedate',
		          '结束时间'=>[
		            'type'=>'zwif',
		            'data'=>[
		              ['{mclass} == "tv" || {mclass} == "bc"','{fendtime}'],
		              ['',''],
		            ]
		          ],
		          '时长/版面'=>[
		            'type'=>'zwif',
		            'data'=>[
		              ['{mclass} == "tv" || {mclass} == "bc" || {mclass} == "paper"','{flength}'],
		            ]
		          ],
		          '违法代码'=>'fexpressioncodes',
		          '涉嫌违法内容'=>'fillegalcontent',
		        ];
		        if($mclass == 'tv' || $mclass == 'bc' || $mclass == 'paper' || $mclass == 'od'){
		        	$outdata['datalie']['素材地址'] = 'ysscurl';
		        }elseif($mclass == 'net'){
		        	$outdata['datalie']['发布页'] = 'fbyurl';
		        	$outdata['datalie']['落地页'] = 'ldyurl';
		        }else{
		        	$outdata['datalie']['素材地址'] = 'ysscurl';
		        	$outdata['datalie']['发布页'] = 'fbyurl';
		        	$outdata['datalie']['落地页'] = 'ldyurl';
		        }

		        $outdata['lists'] = $data;
		        $ret = A('Api/Function')->outdata_xls($outdata);

		        D('Function')->write_log('监测数据',1,'excel导出成功');
		        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

			}elseif($outtype=='xml'){
				$this->downloadXml($data);
			}elseif($outtype=='txt'){
				$this->downloadTxt($data);
			}elseif($outtype=='word'){
				$this->downloadWord($data);
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'请选择导出类型'));
			}
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count[0]['acount'],'list'=>$data)));
		}
	}

	//导出xml
	public function downloadXml($data)
	{
		$xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
		$xml .= "<article>\n";
		$arr = ['tv'=>'电视','bc'=>'广播','paper'=>'报刊','net'=>'互联网','od'=>'户外'];
		foreach ($data as $key =>$value) {
			$xml .= "<item>\n";
			$xml .= "<xuhao>" . ($key+1) . "</xuhao>\n";
			$xml .= "<ggmingcheng>" . $value['fadname'] . "</ggmingcheng>\n";
			$xml .= "<ggzhu>" . $value['fadownername'] . "</ggzhu>\n";
			$xml .= "<ggnrleibie>" . $value['fadclass'] . "</ggnrleibie>\n";
			$xml .= "<meiti>" . $value['fmedianame'] . "</meiti>\n";
			$xml .= "<mtleixing>" . $arr[$value['mclass']] . "</mtleixing>\n";
			$xml .= "<wfleixing>" . $value['fillegaltype'] . "</wfleixing>\n";
			$xml .= "<fbshijian>" . $value['fissuedate'] . "</fbshijian>\n";
			$xml .= "<jsshijian>" . $value['fendtime'] . "</jsshijian>\n";
			$xml .= "<scbanmian>" . $value['flength'] . "</scbanmian>\n";
			$xml .= "<wfdaima>" . $value['fexpressioncodes'] . "</wfdaima>\n";
			$xml .= "<wfyuanyin>" . $value['fillegalcontent'] . "</wfyuanyin>\n";
			$xml .= "</item>\n";
		}
		$xml .= "</article>\n";
		
		$date = date('Ymdhis');
		$savefile = './Public/Xls/'.$date.'.xml';
		file_put_contents($savefile,$xml);//生成本地文件
		D('Function')->write_log('监测数据',1,'xml导出成功');

		$ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.xml',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
		unlink($savefile);//删除文件

		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
	}

	//导出txt
	public function downloadTxt($data)
	{
		$txttitle = "序号  广告名称  广告主  广告内容类别  发布媒体  媒体类型  违法类型  发布时间  结束时间  时长/版面  违法代码  违法原因\n";
		$txtcontent = '';
		$arr = ['tv'=>'电视','bc'=>'广播','paper'=>'报刊','net'=>'互联网','od'=>'户外'];
		foreach ($data as $key => $value) {	
			$txtcontent .=($key+1).'.  ';
			$txtcontent .=$value['fadname'].'  ';
			$txtcontent .=$value['fadownername'].'  ';
			$txtcontent .=$value['fadclass'].'  ';
			$txtcontent .=$value['fmedianame'].'  ';
			$txtcontent .=$arr[$value['mclass']].'  ';
			$txtcontent .=$value['fillegaltype'].'  ';
			$txtcontent .=$value['fissuedate'].'  ';
			$txtcontent .=$value['fendtime'].'  ';
			$txtcontent .=$value['flength'].'  ';
			$txtcontent .=$value['fexpressioncodes'].'  ';
			$txtcontent .=$value['fillegalcontent'].'  ';
			$txtcontent .="\n";
		}

		$date = date('Ymdhis');
		$savefile = './Public/Xls/'.$date.'.txt';
		file_put_contents($savefile,$txttitle.$txtcontent);
		D('Function')->write_log('监测数据',1,'txt导出成功');

		$ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.txt',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
		unlink($savefile);//删除文件
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
	}

	//导出xml
	public function downloadWord($data)
	{
        session_write_close();
		header("Content-type: text/html; charset=utf-8"); 
		$arr = ['tv'=>'电视','bc'=>'广播','paper'=>'报刊','net'=>'互联网','od'=>'户外'];
		foreach ($data as $key => $value) {
			$addhtml .= '<tr>
                <td align="center">'.($key+1).'</td>
                <td align="center">' . $value['fadname'] . '</td>
                <td align="center">' . $value['fadownername'] . '</td>
                <td align="center">' . $value['fadclass'] . '</td>
                <td align="center">' . $value['fmedianame'] . '</td>
                <td align="center">' . $arr[$value['mclass']] . '</td>
                <td align="center">' . $value['fillegaltype'] . '</td>
                <td align="center">' . $value['fissuedate'] . '</td>
                <td align="center">' . $value['fendtime'] . '</td>
                <td align="center">' . $value['flength'] . '</td>
                <td align="center">' . $value['fexpressioncodes'] . '</td>
                <td align="center">' . $value['fillegalcontent'] . '</td>
            </tr>';
		}
		if(empty($addhtml)){
            $addhtml = '<tr><td align="center" colspan="12"></td></tr>';
        }

		$html = '<body style="font-size:18px; line-height:24px;">
            <table border="1" cellpadding="0" width="100%" cellspacing="0" style="border-collapse: collapse;">
                <tr>
                    <td align="center">序号</td>
                    <td align="center">广告名称</td>
                    <td align="center">广告主</td>
                    <td align="center">广告内容类别</td>
                    <td align="center">发布媒体</td>
                    <td align="center">媒体类型</td>
                    <td align="center">违法类型</td>
                    <td align="center">发布时间</td>
                    <td align="center">结束时间</td>
                    <td align="center">时长/版面</td>
                    <td align="center">违法代码</td>
                    <td align="center">违法原因</td>
                </tr>
                '.$addhtml.'
            </table>
            </body>
        ';

        $date = date('Ymdhis');
        $savefilename = $date.'.doc';
        $savefile = './Public/Word/'.$date.'.doc';

        //将文档保存
        word_start();
        echo $html;
        word_save($savefile);
        ob_flush();//每次执行前刷新缓存
        flush();

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.doc',$savefile,array('Content-Disposition' => 'attachment;filename='.$date.'.doc'));//上传云
       	unlink($savefile);//删除文件

		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
	}

	/**
	 * 获取监测数据详情
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据
	 * by zw
	 */
	public function getmonitoringview()
	{
		$system_num = getconfig('system_num');
        $use_open_search = getconfig('use_open_search');

		$sid 		= I('sid');//广告记录id
		$mclass 	= I('mclass');//媒体类型
		$tb_name 	= I('tbname');//获取表名用的时间
		$area 		= I('area');//获取地区
		if(empty($sid)||empty($mclass)){
			$this->error('参数缺失!');
		}

		if(empty($area)){
			$area = session('regulatorpersonInfo.regionid');
		}
		
		$area = substr($area,0,2).'0000';
        $fissuedatest = strtotime($tb_name);

		if($area == 100000){
            $tb_tvissue = gettable('tv',$fissuedatest,110000);
            $tb_bcissue = gettable('bc',$fissuedatest,110000);
            $where['_string'] = '1=0';
        }else{
            $tb_tvissue = gettable('tv',$fissuedatest,$area);
            $tb_bcissue = gettable('bc',$fissuedatest,$area);
            $where['_string'] = '1=1';
        }
        $tb_paperissue = 'tpaperissue';
		$tb_netissue = 'tnetissueputlog';
		$tb_odissue = 'huwai_ad';

		$tb_tvsam = 'ttvsample';
		$tb_bcsam = 'tbcsample';
		$tb_papersam = 'tpapersample';
		$tb_netsam = 'tnetissue';

		$wherestrtv 	= ' and '.$tb_tvsam.'.fstate=1 and '.$tb_tvissue.'.fid='.$sid;//tv
		$wherestrpaper 	= ' and '.$tb_papersam.'.fstate=1 and '.$tb_paperissue.'.fpaperissueid='.$sid;//paper
		$wherestrbc 	= ' and '.$tb_bcsam.'.fstate=1 and '.$tb_bcissue.'.fid='.$sid;//bc

		if($mclass== 'tv'){
			$data = M($tb_tvissue)
				->field($tb_tvissue.'.fid as tid,
					tad.fadname, tadclass.ffullname as fadclass, (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.fid ,tad.fbrand, ifnull(tadowner.fname,"广告主不详") as fadowner_name, fversion, fadlen, tillegaltype.fillegaltype,'.$tb_tvsam.'.fillegaltypecode, fillegalcontent, fexpressioncodes, fexpressions, fconfirmations, fapprovalid, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, FROM_UNIXTIME('.$tb_tvissue.'.fissuedate,"%Y-%m-%d") as fissuedate, fstarttime, fendtime, FROM_UNIXTIME(fstarttime) as fstarttime2, FROM_UNIXTIME(fendtime) as fendtime2, "" as fpage, "" as fsize, favifilepng,favifilename
				')
				->join(''.$tb_tvsam.' on '.$tb_tvissue.'.fsampleid='.$tb_tvsam.'.fid '.$wherestrtv)
				->join('tad on '.$tb_tvsam.'.fadid=tad.fadid and tad.fadid<>0')
				->join('tadclass on tad.fadclasscode=tadclass.fcode')
				->join('tadowner on tad.fadowner=tadowner.fid ')
				->join('tillegaltype on '.$tb_tvsam.'.fillegaltypecode=tillegaltype.fcode')
				->join('tmedia on '.$tb_tvissue.'.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->where($where)
				->find();

			$data['issueurl'] = A('Common/Media','Model')->get_m3u8($data['fid'],$data['fstarttime'],$data['fendtime']);
			$data['fstarttime'] = date('H:i:s',$data['fstarttime']);
			$data['fendtime'] = date('H:i:s',$data['fendtime']);

			$where2['source_type'] = 10;
			$where2['source_tid'] = $data['tid'];
			$where2['source_mediaclass'] = '01';
			$where2['validity_time'] = ['gt',time()];
			$data2 = M('source_make')->field('source_url,source_id,source_state')->where($where2)->find();
			if(!empty($data2)){
				$data['source_url'] = $data2['source_url'];
				$data['source_id'] 	= $data2['source_id'];
				$data['source_state'] = $data2['source_state'];
			}
		}elseif($mclass== 'bc'){
			$data = M($tb_bcissue)
				->field($tb_bcissue.'.fid as tid,
					tad.fadname, tadclass.ffullname as fadclass, (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame ,tmedia.fid ,tad.fbrand, ifnull(tadowner.fname,"广告主不详") as fadowner_name, fversion, fadlen, tillegaltype.fillegaltype,'.$tb_bcsam.'.fillegaltypecode, fillegalcontent, fexpressioncodes, fexpressions, fconfirmations, fapprovalid, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, FROM_UNIXTIME('.$tb_bcissue.'.fissuedate,"%Y-%m-%d") as fissuedate,fstarttime,fendtime, FROM_UNIXTIME(fstarttime) as fstarttime2, FROM_UNIXTIME(fendtime) as fendtime2, "" as fpage, "" as fsize, favifilepng,favifilename
				')
				->join(''.$tb_bcsam.' on '.$tb_bcissue.'.fsampleid='.$tb_bcsam.'.fid '.$wherestrbc)
				->join('tad on '.$tb_bcsam.'.fadid=tad.fadid and tad.fadid<>0')
				->join('tadclass on tad.fadclasscode=tadclass.fcode')
				->join('tadowner on tad.fadowner=tadowner.fid ')
				->join('tillegaltype on '.$tb_bcsam.'.fillegaltypecode=tillegaltype.fcode')
				->join('tmedia on '.$tb_bcissue.'.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->where($where)
				->find();

			$data['issueurl'] = A('Common/Media','Model')->get_m3u8($data['fid'],$data['fstarttime'],$data['fendtime']);
			$data['fstarttime'] = date('H:i:s',$data['fstarttime']);
			$data['fendtime'] = date('H:i:s',$data['fendtime']);

			$where2['source_type'] = 10;
			$where2['source_tid'] = $data['tid'];
			$where2['source_mediaclass'] = '02';
			$where2['validity_time'] = ['gt',time()];
			$data2 = M('source_make')->field('source_url,source_id,source_state')->where($where2)->find();
			if(!empty($data2)){
				$data['source_url'] = $data2['source_url'];
				$data['source_id'] 	= $data2['source_id'];
				$data['source_state'] = $data2['source_state'];
			}
		}elseif($mclass== 'paper'){
			$data = M(''.$tb_paperissue.'')
				->field(''.$tb_paperissue.'.fpaperissueid as tid,
					tad.fadname, tadclass.ffullname as fadclass, (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame ,tad.fbrand, ifnull(tadowner.fname,"广告主不详") as fadowner_name, fversion , tillegaltype.fillegaltype,'.$tb_papersam.'.fillegaltypecode, fillegalcontent, fexpressioncodes, fexpressions, fconfirmations, fapprovalid, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, '.$tb_paperissue.'.fissuedate, "" as fstarttime, "" as fendtime, fpage, fsize, '.$tb_papersam.'.fjpgfilename as favifilepng,'.$tb_papersam.'.fjpgfilename as favifilename
				')
				->join(''.$tb_papersam.' on '.$tb_paperissue.'.fpapersampleid='.$tb_papersam.'.fpapersampleid '.$wherestrpaper)
				->join('tad on '.$tb_papersam.'.fadid=tad.fadid and tad.fadid<>0')
				->join('tadclass on tad.fadclasscode=tadclass.fcode')
				->join('tadowner on tad.fadowner=tadowner.fid ')
				->join('tillegaltype on '.$tb_papersam.'.fillegaltypecode=tillegaltype.fcode')
				->join('tmedia on '.$tb_paperissue.'.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->where($where)
				->find();
			$data['fissuedate'] = date('Y-m-d',strtotime($data['fissuedate']));
		}elseif($mclass== 'net'){
			$where[$tb_netissue.'.major_key'] = $sid;//net
			$where[$tb_netissue.'.is_first_broadcast'] = 1;//net

			$data = M(''.$tb_netissue.'')
				->field(''.$tb_netissue.'.major_key as tid,
					'.$tb_netsam.'.*,  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,fbrand, tadclass.ffullname as fadclass,ifnull(tadowner.fname,"广告主不详") as fadowner_name, tillegaltype.fillegaltype,'.$tb_netsam.'.fillegaltypecode,'.$tb_netsam.'.fexpressioncodes, fillegalcontent,FROM_UNIXTIME('.$tb_netissue.'.fissuedate,"%Y-%m-%d") as fissuedate,tmediaowner.fname as fmediaownername,tmedia.fmediacode
				')
				->join(''.$tb_netsam.' on '.$tb_netsam.'.major_key='.$tb_netissue.'.tid and finputstate=2 ')
				->join('tadclass on '.$tb_netsam.'.fadclasscode=tadclass.fcode')
				->join('tillegaltype on '.$tb_netsam.'.fillegaltypecode=tillegaltype.fcode')
				->join('tmedia on '.$tb_netsam.'.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->join('tmediaowner on tmedia.fmediaownerid=tmediaowner.fid')
				->join('tadowner on '.$tb_netsam.'.fadowner=tadowner.fid','left')
				->where($where)
				->find();
		}elseif($mclass== 'od'){
			$where[$tb_odissue.'.fid'] = $sid;//net
			$data = M($tb_odissue)
				->field($tb_odissue.'.fid as tid,
					  (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,fadbrand fbrand, fadname,fissuedatetime fissuedate,tmedia.fmediacode,fimgs favifilename
				')
				->join('tmedia on '.$tb_odissue.'.fmediaid=tmedia.fid and tmedia.fid=tmedia.main_media_id')
				->where($where)
				->find();
		}
		$data['mclass'] = $mclass;
		$data['fillegalcontent'] = htmlspecialchars_decode($data['fillegalcontent']);
		
		if(!empty($data)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据','data'=>$data));
		}
	}

	/**
	 * 获取监测素材列表
	 * by zw
	 */
	public function source_make_list(){
		session_write_close();
		
		$p 				= I('page', 1);//当前第几页
		$pp 			= 20;//每页显示多少记录
		$source_name 	= I('source_name');

		if(!empty($source_name)){
			$where['source_name'] 	= array('like','%'.$source_name.'%');
		}
		$where['creater_id']	= session('regulatorpersonInfo.fid');
		$where['source_type']	= array('in',array(0,10,20)) ;

		$db_sme = M('source_make');
		$count = $db_sme
			->where($where)
			->count();// 查询满足要求的总记录数
		
		$data = $db_sme
			->field('source_make.*,(case when instr(a.fmedianame,"（") > 0 then left(a.fmedianame,instr(a.fmedianame,"（") -1) else a.fmedianame end) as fmedianame')
			->join('tmedia a on source_make.media_id=a.fid and a.fid=a.main_media_id')
			->where($where)
			->order('source_id desc')
			->page($p,$pp)
			->select();//查询复核数据
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	/**
	 * 删除监测素材
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * by zw
	 */
	public function source_make_del(){
		$source_id = I('source_id');//素材ID
		$do_sme = M('source_make')->where('source_id='.$source_id)->delete();
		if(!empty($do_sme)){
			D('Function')->write_log('监测素材',1,'删除成功','source_make',$source_id,M('source_make')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			D('Function')->write_log('监测素材',1,'删除失败','source_make',$source_id,M('source_make')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败'));
		}
	}

}