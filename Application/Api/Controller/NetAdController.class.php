<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;
use Think\Log;
class NetAdController extends Controller {
    private $filter_str = [
        'http://aden.baidu.com/?word=',
        'http://cpro.baidu.com/cpro/ui/uijs.php',
        'https://m.baidu.com/from=',
        'https://cpro.baidu.com/cpro/ui/uijs.php'
    ];
    private $net_alarm = 3;

    public function __construct() {
        header('Access-Control-Allow-Origin:*');//允许所有域名跨域访问
        header('Access-Control-Allow-Headers:*'); // 设置允许自定义请求头的字段
        parent::__construct();
    }

    //落地页标识
    public function add_netissue_identify($net_target_url = '',$tid = false){
        if($tid && (substr($net_target_url,0,4) === 'http')){
            M('tnetissue_identify')->add(['major_key' => $tid,'net_target_url' => $net_target_url]);
        }
    }

    /*通过接口获取网络广告*/
    public function get_net_ad(){
        set_time_limit(600);
        header("Content-type: text/html; charset=utf-8");


        $engineList = array(
            array(
                'api_url'=>'http://spider.web01.hz-data.com/api/iad/crond.xwy',//引擎地址
                'token'=>'159b0f0619592059824ffb6cd12b709d',//token值
                'get_size'=>100,//每次获取的数量
                'source_type'=>1,//类型值
            ),
            array(
                'api_url'=>'http://spider.web02.hz-data.com/api/iad/crond.xwy',//引擎地址
                'token'=>'159b0f0619592059824ffb6cd12b709d',//token值
                'get_size'=>100,//每次获取的数量
                'source_type'=>2,//类型值
            )
        );

        foreach($engineList as $engine){
            //var_dump($engine);
            $last_id = M('tnetissue')->where(array('source_type'=>$engine['source_type']))->max('net_id');
            $last_id = intval($last_id);

            $api_url = $engine['api_url'];
            $get_data = array();
            $get_data['token'] = $engine['token'];
            $get_data['size'] = $engine['get_size'];
            $get_data['last_id'] = $last_id;


            $net_ad_data_json = http($api_url,$get_data,'GET',false,30);//请求接口
            //var_dump($net_ad_data_json);
            $net_ad_data_json = str_replace(//替换url里的域名
                array('127.0.0.1','172.20.4.64'),
                array('spider.web01.adlife.com.cn','spider.web01.adlife.com.cn'),
                $net_ad_data_json
            );//替换域名

            $net_ad_data = json_decode($net_ad_data_json,true);
            $netAdList = $net_ad_data['data'];

            $this->add_data($netAdList,$engine['source_type']);//添加数据

        }





    }

    /*添加数据*/
    private function add_data($netAdList,$source_type){


        foreach($netAdList as $netAd){
            $a_data = array();
            $a_data['api_json'] = json_encode($netAd);//保留原始json
            $a_data['net_id'] = $netAd['id'];//原始ID
            $a_data['net_x'] = $netAd['x'];//广告位置X
            $a_data['net_y'] = $netAd['y'];//广告位置Y
            $a_data['net_width'] = $netAd['width'];//广告宽度
            $a_data['net_height'] = $netAd['height'];//广告高度
            $a_data['net_thumb_width'] = $netAd['thumb_width'];//缩略图宽度
            $a_data['net_thumb_height'] = $netAd['thumb_height'];//广缩略图高度

            $a_data['net_url'] = $netAd['file_url'];//文件URL
            $a_data['net_old_url'] = $netAd['old_file_url'];//原始文件URL
            $a_data['thumb_url_true'] = $netAd['thumb_url_true'];//缩略图URL

            $a_data['net_size'] = $netAd['size'];//文件大小
            $a_data['ftype'] = $netAd['type'];//类型

            $a_data['fadname'] = $netAd['title'];//广告标题
            $a_data['fadclasscode'] = '2301';//广告标题

            $a_data['net_trackers'] = $netAd['trackers'];//跟踪者(广告联盟)
            $a_data['net_platform'] = $netAd['platform'];//平台  1PC，2移动
            $a_data['net_original_url'] = $netAd['original_url'];//广告原始地址
            $a_data['net_target_url'] = $netAd['target_url'];//落地页地址
            $a_data['net_md5'] = $netAd['md5'];//文件MD5码
            $a_data['net_created_date'] = $netAd['created_date'];//广告发现时间

            $a_data['net_machine_location'] = $netAd['machine_location'];//采集服务器编号
            $a_data['net_machine_ip'] = $netAd['machine_ip'];//采集服务器IP
            $a_data['net_shape'] = $netAd['shape'];//广告类型 文件形状 1 竖幅 2 正方形 3 小横幅 4 中横幅 5 大横幅
            $a_data['net_advertiser'] = $netAd['advertiser'];//广告主域名
            $a_data['fmediaid'] = A('Common/NetAd','Model')->get_net_media_id($netAd['publisher']);//媒体域名
            $a_data['net_screen_height'] = $netAd['screen_height'];//采集屏幕高度
            $a_data['net_screen_width'] = $netAd['screen_width'];//采集屏幕宽度
            $a_data['net_body_height'] = $netAd['body_height'];//发布页对象高度
            $a_data['net_body_width'] = $netAd['body_width'];//发布页对象宽度
            $a_data['trackers_info'] = json_encode($netAd['trackers_info']);//trackers_info
            $a_data['net_attribute04'] = $netAd['attribute04'];//移动端标识 IOS  Android
            $a_data['net_url_md5'] = $netAd['url_md5'];//落地页指纹
            $a_data['net_snapshot'] = $netAd['snapshot'];//广告快照
            $a_data['net_ip'] = $netAd['ip'];//广告主服务器IP
            $a_data['net_region'] = $netAd['region'];//广告主服务器所属地区
            $a_data['finputstate'] = 1;//录入状态
            $a_data['finspectstate'] = 1;//初审状态
            $a_data['source_type'] = $source_type;//来源类型
            M('tnetissue')->add($a_data);
        }



    }

    /*公众号文章*/
    public function weixin_article(){

        header("Content-type: text/html; charset=utf-8");
        $dblink = 'mysql://root:Hzsj_123456@es.iad.adlife.com.cn:3306/weixin';

        $last_id = M('tnetissueputlog')->where(array('source_type'=>4,'is_need_update' => 1))->min('net_id');

        //如果有需要更新的
        if($last_id){
            $last_id = $last_id-1;//需要更新的前一位id
        }else{
            //没有更新的则直接查询最后一位
            $last_id = M('tnetissueputlog')->where(array('source_type'=>4))->max('net_id');
            if(!$last_id){
                $last_id = 0;
            }
        }

        $last_id = intval($last_id);
        $netAdList = M('spiderdate','',$dblink)->where(array('fid'=>array('gt',$last_id)))->order('fid asc')->limit(20)->select();

        foreach($netAdList as $netAd){
            $a_data = array();
            $a_data['net_id'] = $netAd['fid'];//原始ID
            $a_data['fadname'] = $netAd['article_title'];//广告标题
            $a_data['fadclasscode'] = '';//
            $a_data['net_platform'] = 9;//平台  1PC，2移动, 9微信
            $a_data['net_created_date'] = $netAd['article_publish_time']*1000;//广告发现时间
            $a_data['article_content'] = $netAd['article_content'];//文章内容
            $a_data['fmediaid'] = A('Common/NetAd','Model')->get_net_media_id($netAd['weixin_name'],4,'1303',$netAd);//媒体域名

            $a_data['net_target_url'] = $netAd['article_origin_url'];//落地页地址
            unset($netAd['article_content']);
            $a_data['api_json'] = json_encode($netAd);//json 数据
            $a_data['thumb_url_true'] = $netAd['article_thumbnail'];//json 数据

            $a_data['finputstate'] = 1;//录入状态
            $a_data['net_advertiser'] = '广告主不详';//广告主域名
            $a_data['fadowner'] = 0;//广告主id
            $a_data['finspectstate'] = 1;//初审状态
            $a_data['source_type'] = 4;//来源类型
            $a_data['ftype'] = 'text';//类型
            $a_data['fmodifier'] = '接口修改';//修改人
            $a_data['fmodifytime'] = date("Y-m-d H:i:s");//更新时间
            $is_exist_log = M('tnetissueputlog')->field('major_key,is_need_update')->where(array('net_id'=>$netAd['fid'],'source_type'=>4))->find();
            $is_exist = M('tnetissue')->field('major_key')->where(array('net_id'=>$a_data['net_id'],'source_type'=>4))->find();
            if(!empty($is_exist)){
                //存在重复数据
                $tid = $is_exist['major_key'];
                if($is_exist_log['is_need_update'] == 1){
                    //TODO::更新操作
                    $tnetissue_res = M('tnetissue')->where(['major_key' => $is_exist['major_key']])->save($a_data);
                }
            }else{
                //不存在重复的数据
                $tid = M('tnetissue')->add($a_data);

                $this->add_netissue_identify($netAd['article_origin_url'],$tid);

                $task_array = [[
                    'major_key' => $tid,//样本id
                    'fmediaid' => $a_data['fmediaid'],//媒介id
                    'net_id' => $a_data['net_id'],//原始id
                    'net_created_date' => $a_data['net_created_date'],//创建时间
                    'ftype' => 'text',//类型
                    'fadclasscode' => ''//广告类型
                ]];
                #$addTaskRes = A('TaskInput/NetAd','Model')->addTask($task_array);

            }
            $tnetissueputlog_data = array(
                'tid'=>$tid,//主表主键
                'net_id'=>$a_data['net_id'],//原始ID
                'net_platform'=>9,//平台  1PC，2移动
                'net_created_date'=>$a_data['net_created_date'],//广告发现时间
                'fmediaid'=>$a_data['fmediaid'],//媒体域名
                'source_type'=>$a_data['source_type'],//媒体域名
                'fissuedate'=>strtotime(date('Y-m-d',$a_data['net_created_date']/1000))//发布当天0点时间戳秒级
            );
            if(empty($is_exist_log)){
                //不存在重复的数据
                $res = M('tnetissueputlog')->add($tnetissueputlog_data);
            }else{
                if($is_exist_log['is_need_update'] == 1){
                    //TODO::更新操作
                    $tnetissueputlog_data['is_need_update'] = 0;
                    $tnetissue_res = M('tnetissueputlog')->where(['major_key' => $is_exist_log['major_key']])->save($tnetissueputlog_data);
                }
            }
        }

    }


    /*数据接收接口*/
    public function receive_net_data(){

        $data_json = $_POST['JsonData'];

        if(empty(trim($data_json))) {
            $this->ajaxReturn([
                'code' => 1,
                'message' => '无数据！'
            ]);
        }else{
            $data = json_decode($data_json,true);
        }
        if(!empty($data['source_type']) && !empty($data['list'])){
            $this->add_net_data($data['list'],$data['source_type']);
            $this->ajaxReturn([
                'code' => 0,
                'message' => count($data['list']).'条数据接收成功！'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => 1,
                'message' => 'json格式错误！'
            ]);
        }
    }

    /*数据接收检查*/
    public function receive_check(){

        $netIds = $_POST['netIds'];

        $source_type = 20;

        if(empty($netIds)){
            $this->ajaxReturn([
                'code' => 1,
                'message' => '广告数据id不能为空！'
            ]);
        }

        $res = M("tnetissueputlog")->where(['net_id'=>['IN',$netIds],'source_type'=>$source_type])->getField('net_id',true);

        $not_insert = array_values(array_diff($netIds,$res));
        if(empty($not_insert)){
            $not_insert = [];
        }

        $this->ajaxReturn([
            'code' => 0,
            'message' => '接收成功！',
            'data'=>[
                'inserted' => $res,
                'notInserted' => $not_insert
            ]
        ]);

    }

    /*数据接收检查*/
    public function receive_count(){

        $source_type = 20;

        $res = M("tnetissueputlog")->where(['source_type'=>$source_type])->count();

        $this->ajaxReturn([
            'total' => $res
        ]);

    }

    /*姚俊男 jim 201806231303 start*/
    /*添加数据*/
    private function add_net_data($netAdList,$source_type,$update_control = 0,$daytime=null){

        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);
        $insert_count = 0;
        $repeat_count = 0;
        //循环数据
        foreach($netAdList as $netAd){
            //定义添加新数据
            $a_data = array();

            $a_data['api_json'] = json_encode($netAd);//保留原始json
            $a_data['net_id'] = $netAd['id'];//原始ID
            $a_data['net_x'] = $netAd['x'];//广告位置X
            $a_data['net_y'] = $netAd['y'];//广告位置Y
            $a_data['net_width'] = $netAd['width'];//广告宽度
            $a_data['net_height'] = $netAd['height'];//广告高度
            $a_data['net_thumb_width'] = $netAd['thumb_width'];//缩略图宽度
            $a_data['net_thumb_height'] = $netAd['thumb_height'];//广缩略图高度
            $a_data['net_url'] = $netAd['file_url'];//文件URL
            $a_data['net_old_url'] = $netAd['old_file_url'];//原始文件URL
            $a_data['thumb_url_true'] = $netAd['thumb_url_true'];//缩略图URL
            $a_data['net_size'] = $netAd['size'];//文件大小
            $a_data['ftype'] = $netAd['type'];//类型
            if(isset($netAd['title']) && $netAd['title'] != ''){
                $title = $netAd['title'];
            }else{
                $title = '默认标题';
            }
            $a_data['fadname'] = $title;//广告标题
            $a_data['fadtext'] = $title;//广告描述
            $a_data['fbrand'] = '';//品牌

            if($netAd['fillegaltypecode'] > 0){
                $a_data['fillegaltypecode'] = 30;//违法类型代码
            }else{
                $a_data['fillegaltypecode'] = $netAd['fillegaltypecode'];//违法类型代码
            }
            if($source_type != 5 && $source_type != 8 && $source_type != 1){
                $advertiser = $netAd['advertiser_na'];
                $publisher = $netAd['publisher_na'];
                $trackers = $netAd['tracker_na'];
                $tracker_list = $netAd['tracker_list'];
            }else{
                $advertiser = $netAd['advertiser'];
                $publisher = $netAd['publisher'];
                $trackers = $netAd['trackers'];
                $tracker_list = $netAd['trackers_info'];
            }
            $a_data['fillegalcontent'] = $netAd['fillegalcontent'];//涉嫌违法内容
            $a_data['fexpressioncodes'] = $netAd['fexpressioncodes'];//违法表现代码

            if(empty($tracker_list)){
                $a_data['net_trackers'] = null;
            }else{
                $a_data['net_trackers'] = json_encode($tracker_list);//跟踪者(广告联盟)
            }

            $a_data['net_platform'] = $netAd['platform'];//平台  1PC，2移动
            $a_data['net_original_url'] = $netAd['original_url'];//广告原始地址
            $a_data['net_target_url'] = $netAd['target_url'];//落地页地址
            $a_data['net_md5'] = $netAd['md5'] ? $netAd['md5'] : md5($netAd['decode_id'].$source_type);//文件MD5码或者解析码
            $a_data['net_created_date'] = $netAd['created_date'];//样本创建时间
            $a_data['net_machine_location'] = $netAd['machine_location'];//采集服务器编号
            $a_data['net_machine_ip'] = $netAd['machine_ip'];//采集服务器IP
            $a_data['net_shape'] = $netAd['shape'];//广告类型 文件形状 1 竖幅 2 正方形 3 小横幅 4 中横幅 5 大横幅

            $a_data['net_advertiser'] = $advertiser;//广告主域名

            $a_data['fmediaid'] = A('Common/NetAd','Model')->get_net_media_id($publisher,$source_type,$this->get_net_mediaclass($netAd['platform']));//媒体域名

            if(!empty($advertiser)){
                $fadowner = A('Common/NetAd','Model')->add_edit_tadowner($advertiser,$a_data['fmediaid']);//添加或者编辑互联网广告主
            }

            $a_data['fadowner'] = $fadowner;//广告主id
            $a_data['net_screen_height'] = $netAd['screen_height'];//采集屏幕高度
            $a_data['net_screen_width'] = $netAd['screen_width'];//采集屏幕宽度
            $a_data['net_body_height'] = $netAd['body_height'];//发布页对象高度
            $a_data['net_body_width'] = $netAd['body_width'];//发布页对象宽度
            $a_data['trackers_info'] = json_encode($tracker_list);//trackers_info
            $a_data['net_attribute04'] = $netAd['attribute04'];//移动端标识 IOS  Android
            $a_data['net_url_md5'] = $netAd['url_md5'] ? $netAd['url_md5'] : md5($netAd['target_url']);//落地页指纹

            $a_data['net_snapshot'] = $netAd['snapshot'];//广告快照

            $a_data['net_original_snapshot'] = isset($netAd['original_snapshot']) ? $netAd['original_snapshot'] : null ;//广告发布来源快照

            $a_data['net_ip'] = $netAd['ip'];//广告主服务器IP
            $a_data['net_region'] = $netAd['region'];//广告主服务器所属地区
            $a_data['material'] = $netAd['material'] ? $netAd['material'] : '';//信息展示方式，空或native， native为信息流
            $a_data['source_type'] = $source_type;//来源类型

            if(empty($daytime)){
                $fissuedate = strtotime(date('Y-m-d',$netAd['created_date']/1000));
            }else{
                $fissuedate = strtotime($daytime);
            }

            //是否存在发布记录  以来源标识、广告id、获取时间 为唯一
            $is_exist_log = M('tnetissueputlog')
                ->field('major_key,is_need_update')
                ->where(array('net_id'=>$netAd['id'],'source_type'=>$source_type,'fissuedate' => $fissuedate))
                ->find();

            //是否存在相同的互联网广告样本，以net_md5为唯一值
            $is_exist = M('tnetissue')
                ->field('major_key')
                ->where(array('net_md5'=>$a_data['net_md5'],'source_type'=>$source_type))
                ->find();

            //如果样本存在重复数据
            if($is_exist){
                $tid = $is_exist['major_key'];
                //查询发布表是否需要更新
                if(($is_exist_log['is_need_update'] == 1 && $update_control == 1) || ($is_exist_log['is_need_update'] == 1 && $source_type != 7)){
                    //TODO::更新操作
                    $tnetissue_res = M('tnetissue')->where(['major_key' => $is_exist['major_key']])->save($a_data);
                }
            }else{
                //如果广告类别存在且不为空，则为已处理数据
                if(isset($netAd['fadclasscode']) && $netAd['fadclasscode'] != '' && $netAd['fadclasscode'] != 'undefined' && $netAd['fadclasscode'] != NULL){

                    $fadclasscode = $netAd['fadclasscode'];
                    $finputstate = 2;
                    $finspectstate = 2;

                }else{

                    $fadclasscode = '';
                    $finputstate = 1;
                    $finspectstate = 1;

                }
                $a_data['fadclasscode'] = $fadclasscode;//广告内容类别
                $a_data['finputstate'] = $finputstate;//录入状态
                $a_data['finspectstate'] = $finspectstate;//初审状态
                $a_data['fmodifier'] = '接口增加';//修改人
                $a_data['fmodifytime'] = date("Y-m-d H:i:s");//更新时间

                //不存在重复的数据
                $tid = M('tnetissue')->add($a_data);

                M('tnetissue')->where(['major_key' => $tid])->save(['marin_major_key'=>$tid]);

                $this->add_netissue_identify($netAd['target_url'],$tid);

                //$tid = M('tnetissue')->getLastInsID();

                //过滤垃圾数据
                $str_num = 0;

                foreach ($this->filter_str as $str){

                    if(strstr($netAd['target_url'],$str) !== false){

                        $str_num++;

                    }

                }


                if($tid && $str_num == 0){
                    //如果需要写入众包
                    if($fadclasscode == '' || $fadclasscode == '2301'){
                        $task_array = [[
                            'major_key' => $tid,//样本id
                            'fmediaid' => $a_data['fmediaid'],//媒介id
                            'net_id' => $a_data['net_id'],//原始id
                            'net_created_date' => $a_data['net_created_date'],//创建时间
                            'ftype' => $a_data['ftype'],//类型
                            'fadclasscode' => $fadclasscode//广告类型
                        ]];
                        #$addTaskRes = A('TaskInput/NetAd','Model')->addTask($task_array);
                    }
                }
            }
            $this->add_trackers_info($tracker_list,$tid);

            $tnetissueputlog_data = array(

                'tid'=>$tid,//主表主键

                'net_id'=>$netAd['id'],//原始ID

                'net_x'=>$netAd['x'],//广告位置X

                'net_y'=>$netAd['y'],//广告位置Y

                'net_width'=>$netAd['width'],//广告宽度

                'net_height'=>$netAd['height'],//广告高度

                'net_thumb_width'=>$netAd['thumb_width'],//缩略图宽度

                'net_thumb_height'=>$netAd['thumb_height'],//广缩略图高度

                'net_url'=>$netAd['file_url'],//文件URL

                'net_old_url'=>$netAd['old_file_url'],//原始文件URL

                'thumb_url_true'=>$netAd['thumb_url_true'],//缩略图URL

                'net_trackers'=>$netAd['trackers'],//跟踪者(广告联盟)

                'net_platform'=>$netAd['platform'],//平台  1PC，2移动

                'net_original_url'=>$netAd['original_url'],//广告原始地址

                'net_original_snapshot' => isset($netAd['original_snapshot']) ? $netAd['original_snapshot'] : null,//广告发布来源快照
                
                'net_target_url'=>$netAd['target_url'],//落地页地址

                'net_md5'=>$netAd['md5'] ? $netAd['md5'] : md5($netAd['decode_id'].$source_type),//文件MD5码

                'net_created_date'=>$netAd['created_date'],//广告发现时间

                'net_machine_location'=>$netAd['machine_location'],//采集服务器编号

                'net_machine_ip'=>$netAd['machine_ip'],//采集服务器IP

                'net_shape'=>$netAd['shape'],//广告类型 文件形状 1 竖幅 2 正方形 3 小横幅 4 中横幅 5 大横幅

                'net_advertiser'=>$advertiser,//广告主域名

                'fmediaid'=>$a_data['fmediaid'],//媒体id

                'net_screen_height'=>$netAd['screen_height'],//采集屏幕高度

                'net_screen_width'=>$netAd['screen_width'],//采集屏幕宽度

                'net_body_height'=>$netAd['body_height'],//发布页对象高度

                'net_body_width'=>$netAd['body_width'],//发布页对象宽度
                'source_type'=>$source_type,
                'fissuedate'=>$fissuedate//发布当天0点时间戳秒级
            );

            if(!$is_exist_log){
                //不存在重复的数据
                $fbid = M('tnetissueputlog')->add($tnetissueputlog_data);
                if($fbid){
                    $insert_count++;
                }
            }else{
                $repeat_count++;
                $fbid = $is_exist_log['major_key'];
                if(($is_exist_log['is_need_update'] == 1 && $update_control == 1) || ($is_exist_log['is_need_update'] == 1 && $source_type != 7)){
                    //TODO::更新操作
                    $tnetissueputlog_data['is_need_update'] = 0;
                    $tnetissue_res = M('tnetissueputlog')->where(['major_key' =>$fbid])->save($tnetissueputlog_data);
                }
            }
        }
        return "新增数据：(".$insert_count.")条， 重复数据：{".$repeat_count."}条";
    }
	
	

    //广告联盟发布记录 $trackers_info
    public function add_trackers_info($tracker_list,$tid){

        $data = [
            'tid' => $tid
        ];
        $trackers_host = [];
        //判断是不是数组
        if(is_array($tracker_list) && !empty($tracker_list)){

            foreach ($tracker_list as $tracker){
                    if(trim($tracker) !== ""){
                        $trackers_host[] = trim($tracker);
                    }
            }

            //是不是为空
            if(!empty($trackers_host)){
                $tracke_list = M('tracker')->where([
                    "trackerhost" => ['IN',$trackers_host]
                ])->select();

                //循环添加发布记录
                foreach ($tracke_list as $tracke){
                    $data['trackerid'] = $tracke['trackerid'];
                    $map = [
                        'tid' => $tid,
                        'trackerid' => $tracke['trackerid']
                    ];
                    //查询是否存在发布记录
                    $exist = M('trackers_record')->where($map)->find();
                    //添加
                    if(empty($exist)){
                        $res = M('trackers_record')->add($data);
                    }
                }
            }
        }
    }

    //广告联盟地域匹配
    public function tracker_region($entname){
        $jc = [
            '110000' => '京',
            '120000' => '津',
            '130000' => '冀',
            '140000' => '晋',
            '150000' => '蒙',
            '210000' => '辽',
            '220000' => '吉',
            '230000' => '黑',
            '310000' => '沪',
            '320000' => '苏',
            '330000' => '浙',
            '340000' => '皖',
            '350000' => '闽',
            '360000' => '赣',
            '370000' => '鲁',
            '410000' => '豫',
            '420000' => '鄂',
            '430000' => '湘',
            '440000' => '粤',
            '450000' => '桂',
            '460000' => '琼',
            '500000' => '渝',
            '510000' => '川',
            '520000' => '黔',
            '530000' => '滇',
            '540000' => '藏',
            '610000' => '陕',
            '620000' => '甘',
            '630000' => '青',
            '640000' => '宁',
            '650000' => '新'
        ];
        $fnames = M("tregion")->getField('fid,fname',true);
        foreach ($fnames as $fname_key=>$fname){
            if(strpos($entname,$fname) !== false){
                return $fname_key;
            }
        }
        return null;
    }

    //广告联盟地域匹配更新
    public function tracker_region_up(){
        $trackers = M("tracker")->select();
        foreach ($trackers as $tracker){
            $res = M("tracker")->where(['trackerid'=>$tracker['trackerid']])->save(['fregion_id' => $this->tracker_region($tracker['entname'])]);
        }
    }

    //根据本地字段更新广告联盟
    public function tracker_updata(){
        lock('tracker_updata_key',true,2);
        ini_set('memory_limit','3072M');
        header("Content-type: text/html; charset=utf-8");
        $time = microtime(true);
        //查询需要更新的前几条
        $net_map = [];
        $net_map["tnetissueputlog.is_need_update"] = 7;
        $res = M("tnetissueputlog")
            ->join("
                tnetissue on 
                tnetissue.major_key = tnetissueputlog.tid")
            ->where($net_map)
            ->group("tnetissue.major_key")
            ->order("tnetissue.major_key")
            ->limit(50000)->getField("tnetissue.major_key,tnetissue.trackers_info",true);
        foreach ($res as $key=>$val){
            $this->add_trackers_info(json_decode($val,true),$key);
            $res_u = M("tnetissueputlog")->where(['tid'=>$key])->save(["is_need_update"=>0]);
        }
        lock('tracker_updata_key',null);
        die('获取并更新'.count($res).'条数据花了'.(microtime(true)-$time).'秒!');
    }

    /*通过接口获取网络广告*/
    public function get_net_ad_jim(){
        lock('add_data_key',true,2);
        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);
        session_write_close();
        $update_control = I('update_control',0);
        //数据接口数组
        $engineList = array(
            array(
                'api_url'=>'http://spider.web01.hz-data.com/api/iad/crond.xwy',//引擎地址
                'token'=>'159b0f0619592059824ffb6cd12b709d',//token值
                'get_size'=>500,//每次获取的数量
                'source_type'=>5,//类型值 南京
            ),
            array(
                'api_url'=>'http://spider.web02.hz-data.com/api/iad/crond.xwy',//引擎地址
                'token'=>'159b0f0619592059824ffb6cd12b709d',//token值
                'get_size'=>500,//每次获取的数量
                'source_type'=>8,//类型值 淄博
            ),
            array(
                'api_url'=>'http://spider.web04.adlife.com.cn/api/iad/crond.xwy',//引擎地址
                'token'=>'159b0f0619592059824ffb6cd12b709d',//token值
                'get_size'=>500,//每次获取的数量
                'source_type'=>1,//类型值 甘肃
            )
        );
        //循环调用接口获取数据
        foreach($engineList as $key=>$engine){

            //$max_last_id = M('')->query('SELECT net_id AS max_last_id FROM `tnetissueputlog` WHERE `source_type` = '.$engine['source_type'].' order by net_id desc LIMIT 1');
            $get_data = array();
            $get_data['last_id'] = $this->get_max_id($engine['source_type'],$update_control);
            if($get_data['last_id'] === false){
                continue;
            }
            //如果是国家7的数据
            if($engine['source_type'] == 7){
                $get_data['TOKEN'] = $engine['token'];
                if($last_id <= 91052837){
                    $get_data['last_id'] = 91052837;
                }
            }else{
                $get_data['token'] = $engine['token'];
            }
            //接口地址
            $api_url = $engine['api_url'];
            //每次获取数量
            $get_data['size'] = $engine['get_size'];
            $net_ad_data_json = http($api_url,$get_data,'GET',false,30);//请求接口
            $net_ad_data = json_decode($net_ad_data_json,true);//获取数据数组
            $netAdList = $net_ad_data['data'];
            //自动更新机制 当获取量为空时 检测数据量是否对应 若没有 更新前一天数据
            if (empty($netAdList)){
                $total = M('tnetissueputlog')->where(['source_type'=>$engine['source_type']])->count();
                if($total < $net_ad_data['total']){
                    //dump('当前表数量'.$total.' - 接口总数量'.$net_ad_data['total'].' - '.$engine['source_type']);exit;
                    if($engine['source_type'] != 7){
                        $this->update_data_jim($engine['source_type']);
                    }
                }
            }else{

                $this->add_net_data($netAdList,$engine['source_type'],$update_control);//添加数据
            }
        }

        //更新近一周首播字段
        $s_date = strtotime(date('Y-m-d',time())) - 86400 * 30;
        $e_date = strtotime(date('Y-m-d',time()));
        $sql = "UPDATE tnetissueputlog
                SET is_first_broadcast = 1
                WHERE
                    major_key IN (
                        SELECT
                            major_key FROM (
                                SELECT
                                    MIN(tnetissueputlog.major_key) AS major_key
                                FROM
                                    tnetissueputlog
                                    WHERE fissuedate BETWEEN ".$s_date." AND ".$e_date."
                                GROUP BY
                                    fissuedate,tid
                            ) AS a
                    ) AND fissuedate  BETWEEN ".$s_date." AND ".$e_date.";";
        M('')->execute($sql);
        lock('add_data_key',null);
    }

    /*通过接口获取网络广告*/
    public function IIAD(){
        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);
        while (true){
            if($this->get_net_ad_jim() === true){
                $this->ajaxReturn(['code'=>0,'msg'=>'导入完成']);
            }
        }
    }
    /*jim 201806231303 end*/

    /**
     * 查看网络广告的快照
     * @param $adId
     */
    public function getNetAdSnapshot($adId)
    {
        $row = M('tnetissue')->field('fadname, article_content')->find($adId);
        $adName = $row['fadname'];
        $articleContent = $row['article_content'];
        $articleContent = str_replace('mmbiz.qpic.cn', 'hz-weixin-img.oss-cn-hangzhou.aliyuncs.com', $articleContent);
        $this->assign(compact('adName', 'articleContent'));
        $this->display('wxArticleTemplate');
    }

    public function update_data_jim($source_type){
        //$H = date('G');
        //if($H > 22 || $H < 8){
        $up_time = date('Y-m-d H:i:s',time()-86400);
        $res = M('tnetissueputlog')
            ->where([
                'source_type'=>$source_type,
                'create_time'=>['GT',$up_time]
            ])
            ->save(['is_need_update' => 1]);
        //}
    }

    public function update_net_ad(){
        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);
        session_write_close();
        $has_update_data = M('tnetissueputlog')
            ->field('major_key,tid,source_type,net_id')
            ->where(['is_need_update' => 1])
            ->select();
        $update_data = [];
        foreach ($has_update_data as $has_update_data_val){
            $update_data[$has_update_data_val['net_id'].'_'.$has_update_data_val['source_type']] = $has_update_data_val;
        }

        if(!empty($has_update_data)) {

            $engineList = array(
                array(
                    'api_url' => 'http://spider.web01.hz-data.com/api/iad/crond.xwy',//引擎地址
                    'token' => '159b0f0619592059824ffb6cd12b709d',//token值
                    'get_size' => 500,//每次获取的数量
                    'source_type' => 5,//类型值 南京
                ),
                array(
                    'api_url' => 'http://spider.web02.hz-data.com/api/iad/crond.xwy',//引擎地址
                    'token' => '159b0f0619592059824ffb6cd12b709d',//token值
                    'get_size' => 500,//每次获取的数量
                    'source_type' => 6,//类型值 淄博
                ),
                //http://resource.adbug.cn/api/v4/iad/crond?size=100&TOKEN=b04b86c026eb5a81f846b0f4af168b5b&last_id=91052837
                array(
                    'api_url' => 'http://resource.adbug.cn/api/v4/iad/crond',//引擎地址
                    'token' => 'b04b86c026eb5a81f846b0f4af168b5b',//token值
                    'get_size' => 500,//每次获取的数量
                    'source_type' => 7,//类型值 国家
                    'last_id' => 91052837
                )
            );
            foreach ($engineList as $key => $engine) {
                $max_last_id = M('')->query('
                SELECT net_id AS max_last_id 
                FROM `tnetissueputlog` 
                WHERE `source_type` = '.$engine['source_type'].'
                AND `is_need_update` = 0
                order by net_id desc LIMIT 1'
                );
                $get_data = array();
                $get_data['last_id'] = $last_id;

                if ($engine['source_type'] == 7) {
                    $get_data['TOKEN'] = $engine['token'];
                    if ($last_id <= 91052837) {
                        $get_data['last_id'] = 91052837;
                    }
                } else {
                    $get_data['token'] = $engine['token'];
                }

                $api_url = $engine['api_url'];

                $get_data['size'] = $engine['get_size'];

                $net_ad_data_json = http($api_url, $get_data, 'GET', false, 30);//请求接口

                $net_ad_data = json_decode($net_ad_data_json, true);
                $netAdList = $net_ad_data['data'];
                $this->update_data_jim($netAdList,$update_data,$engine['source_type']);//更新数据
            }
        }
    }
    /*更新数据*/

    /*查看互联网样本数据*/
    public function net_ad_sample_V20200116(){
        $net_id = I("fid");
        $data = M("tnetissue")->field("*")->where(['major_key'=>$net_id])->find();
        if(!empty($data)){
            $this->assign('data',json_encode($data));
        }else{
            $this->assign('data','null');
        }
        $this->display();
    }
    /**
     * @Des: 获取样本详情
     * @Edt: yuhou.wang
     * @Date: 2020-01-16 11:02:58
     * @param {Int} $fid 互联网样本ID 
     */
    public function net_ad_sample(){
        $major_key = I("fid");
        $data = M("tnetissue")->where(['major_key'=>$major_key])->find();
        $data = $data ? json_encode($data) : '';
        $this->assign('data',$data);
        $this->display();
    }

    /**
     * by zw
     * 互联网数据权重提升
     * @Param   Mixed variable
     * @Return  void 返回的数据
     */
    public function net_task_addweight(){
        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);
        session_write_close();

        $where_tp['a.fthreshold'] = array('egt',0);
        $where_tp['a.fmaincode'] = 0;
        $where_tp['c.fissuedate'] = array('lt',strtotime(date('Y-m-d')));
        $where_tp['left(b.fmediaclassid,2)'] = '13';
        $do_tp = M('tapimedia_map')
            ->alias('a')
            ->field('a.fid,a.fmediaid,a.fthreshold,c.fid as taskid,c.fissuedate,a.fmediacode,a.fmediatype,a.fuserid')
            ->join('tmedia b on b.fid = a.fmediaid')
            ->join('tapimedialist c on a.fid = c.fmediaid and c.fdstatus = 0')
            ->where($where_tp)
            ->select();

        foreach ($do_tp as $key => $value) {
            //初始参数
            $issue_date = date('Y-m-d',$value['fissuedate']);
            $nowmonth = date("Y-m",$value['fissuedate']);
            $month = $nowmonth;
            $fthreshold = $value['fthreshold'];

            //获取子媒体
            $where_ctp['fmediacode'] = $value['fmediacode'];
            $where_ctp['fmediatype'] = $value['fmediatype'];
            $where_ctp['fuserid'] = $value['fuserid'];
            $where_ctp['fmaincode'] = $value['fmediacode'];
            $childmedia = M('tapimedia_map')
                ->field('fmediaid')
                ->where($where_ctp)
                ->select();
            $allmedia = [];
            $allmedia[] = $value['fmediaid'];
            foreach ($childmedia as $key2 => $value2) {
                $allmedia[] = $value2['fmediaid'];
            }

            while($month == $nowmonth && ($fthreshold > 0 || ($fthreshold == 0 && $issue_date == date('Y-m-d',$value['fissuedate'])))){
                //更新任务权重值时需要判断指定日期是否有同一媒体存在客户任务，排除当前任务日期
                if($issue_date != date('Y-m-d',$value['fissuedate'])){
                    $where_tt['fmediaid'] = $value['fid'];
                    $where_tt['fstatus'] = array('neq',-1);
                    $where_tt['fissuedate'] = strtotime($issue_date);
                    $ttcount = M('tapimedialist')
                        ->where($where_tt)
                        ->count();
                    if(!empty($ttcount)){
                        break;
                    }
                }

                //更新任务表权重值
                $where_tk['media_id'] = array('in',$allmedia);
                $where_tk['FROM_UNIXTIME(net_created_date,"%Y-%m-%d")'] = $issue_date;
                $where_tk['priority'] = array('egt',0);
                $tkcount = M('tnettask')
                    ->where($where_tk)
                    ->count();
                if(!empty($tkcount)){
                    $data_tt['priority'] = 99;
                    M('tnettask')->where($where_tk)->save($data_tt);
                    $fthreshold = $fthreshold - $tkcount;
                }
                //更新客户任务状态
                $data_tt2 = [];
                $data_tt2['ftask_starttime'] = strtotime($issue_date);
                $data_tt2['fdstatus'] = 1;
                $data_tt2['fdcount'] = array('exp','fdcount+'.$tkcount);

                $where_tt2['fid'] = $value['taskid'];
                M('tapimedialist') ->where($where_tt2) ->save($data_tt2);

                //获取指定日期的前一天时间，用于循环
                $issue_date = date("Y-m-d",strtotime(' -1 day',strtotime($issue_date)));
                $month = date("Y-m",strtotime($issue_date));
            }
        }

        $this->ajaxReturn(['code'=>0,'msg'=>'更新成功']);
    }



    /*20190319 姚俊男 国家分类获取互联网数据代码段 开始*/

    /**by yjn
     * 登录获取token
     * @param string $username 账号
     * @param string $password 密码
     * @param int $try_times 失败尝试次数
     * @return bool|mixed
     * @test superman use huazhi
     */
    public function get_net_token($try_times = 30,$username = 'huazhi',$password = '123456'){
        header("Content-type: text/html; charset=utf-8");

        $url = 'http://resource.adbug.cn:8888/api/login';
        $headers = array(
            'Content-Type:application/x-www-form-urlencoded'
        );
        $params = [
            'username' => $username,
            'password' => $password
        ];
        $params = http_build_query($params);//创建url格式参数
        $res = http_api($url,$headers,$params);
        if(!empty($res['info']['token'])){
            session('net_token',$res['info']['token']);//令牌写入session
            return session('net_token');
        }else{
            if($try_times == 0){
                return false;
            }else{
                $this->get_net_token($num - 1);
            }
        }
    }



    /**by yjn
     * 获取互联网数据 国家分类
     * @param $publisher 发布媒体
     * @param $last_id 开始获取id
     * @param int $size 每次最大获取量
     * @return bool
     */
    public function call_net_api($publisher,$last_id,$size=50,$url = "http://resource.adbug.cn:8888/api/ads"){
        header("Content-type: text/html; charset=utf-8");
        $token = session('net_token');//获取token
        $try_times = 30;
        //获取token
        if(empty($token)){
            if(!$this->get_net_token($try_times)){
                echo '尝试登录'.$try_times.'次失败！';exit;
            }else{
                $token = $this->get_net_token();
            }
        }
        //$url = 'http://resource.adbug.cn:8888/api/ads';
        $headers = array(
            'Cache-Control:no-cache',
            'Authorization:Bearer '.$token,
            'Content-Type:application/x-www-form-urlencoded'
        );
        //设置参数
        $params = [
            'publisher' => $publisher,
            'last_id' => $last_id,
            'size' => $size
        ];
        $params = http_build_query($params);//创建url格式参数
        $res = http_api($url,$headers,$params);
        if ($res['httpCode'] == 200 && !empty($res["info"]["data"])){
            return $res["info"]["data"];
        }else{
            return false;
        }
    }

    /**by yjn
     * 获取互联网数据 国家分类
     * @param $publisher 发布媒体
     * @param $last_id 开始获取id
     * @param int $size 每次最大获取量
     * @return bool
     */
    public function call_net_api2($publisher,$last_id,$size=50,$url = "http://resource.adbug.cn:8888/api/ads",$daytime = null){
        header("Content-type: text/html; charset=utf-8");
        $token = session('net_token');//获取token
        $try_times = 30;
        //获取token
        if(empty($token)){
            if(!$this->get_net_token($try_times)){
                echo '尝试登录'.$try_times.'次失败！';exit;
            }else{
                $token = $this->get_net_token();
            }
        }
        //$url = 'http://resource.adbug.cn:8888/api/ads';
        $headers = array(
            'Cache-Control:no-cache',
            'Authorization:Bearer '.$token,
            'Content-Type:application/x-www-form-urlencoded'
        );
        //设置参数
        $params = [
            'publisher' => $publisher,
            'last_id' => $last_id,
            'size' => $size
        ];

        //获取时间
        if(!empty($daytime)){
            $date_diff = date_diff(date_create($daytime),date_create('1970-1-1'));
            $params['daytime'] = $date_diff->days;
        }

        $params = http_build_query($params);//创建url格式参数

        $res = http_api($url,$headers,$params);

        if ($res['httpCode'] == 200 && !empty($res["info"]["data"])){
            return $res["info"];
        }else{
            return false;
        }
    }


    /*主动获取互联网数据*/
    public function take_net_data(){
        $t1=microtime(true); //获取程序1，开始的时间

        lock($app_key,null);
        $params = json_decode(file_get_contents('php://input', true),true);
        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);
        header("Content-type: text/html; charset=utf-8");
        session_write_close();

        if(empty($params['mediacode'])){
            $this->ajaxReturn([
                'code'=> -1,
                'message' => "mediacode is not set!"
            ]);
        }

        if(empty($params['daytime'])){
            $this->ajaxReturn([
                'code'=> -1,
                'message' => "daytime is not set!"
            ]);
        }

        $app_key = 'take_net_data';

        lock($app_key,true,2);

        $size = 50;
        //循环获取值
        $data = [];
        $res_num = 1;
        $max_id = 1;
        while ($res_num > 0){

            $res = $this->call_net_api2($params['mediacode'],$max_id,$size,"http://resource.adbug.cn:8888/api/ads2",$params['daytime']);//接口返回数据结果

            if(!empty($res['data'])){
                $data = array_merge($data,$res['data']);
                $max_id = $res['id'];

            }
            $res_num = count($res['data']);
        }

        $add_net_data_res = $this->add_net_data($data,7,0,$params['daytime']);//将获取到的数据写入数据表

        lock($app_key,null);

        $this->ajaxReturn([
            'code'=> 0,
            'message' => "获取成功！".$add_net_data_res,
            'data' => [
                'max_id' => $max_id,
                'get_count' => count($data),
                'call_time' => "用时".round((microtime(true) - $t1),3).'秒'
            ]
        ]);

    }
    /*临时跑趣头条数据*/
    public function get_net_data_qtt(){
        lock($app_key,null);

        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);
        header("Content-type: text/html; charset=utf-8");
        session_write_close();
        $time = microtime(true);
        $update_control = I('update_control',0);
        $app_key = 'app_net_key_'.$update_control;
        lock($app_key,true,2);

        if($update_control == 1){
            $size = 50;
        }else{
            $size = 50;
        }
        //S('app_media',null);
        /*        $fmedias = S('app_media');//从缓存中获取媒体
                if(empty($fmedias)){
                    $fmedias = $this->get_net_medias();//如缓存中没有，则去获取
                }*/
        //循环获取值
        $data = [];
        $number = 0;
        $msg = '';
        foreach ([['fid' => 11000010008073,'fmediacode' => 'com.jifen.qukan']] as $media_key => $fmedia){
            S($fmedia['fmediacode'],null);
            $fmediacode_s = S($fmedia['fmediacode']);//获取缓存中的媒体代码
            //如果缓存中没有，则执行
            if(empty($fmediacode_s)){
                if($fmedia['fid'] != 0 && $fmedia['fid'] != '2147483647'){
                    $last_id = $this->get_max_id(7,$update_control,$fmedia['fid']);
                    if($last_id !== false){
                        $res = $this->call_net_api($fmedia['fmediacode'],$last_id,$size);//接口返回数据结果
                        /*                        if($update_control == 1){
                                                    foreach ($res as $res_key=>$res_value){
                                                        $net_issue =  M("tnetissueputlog")->where([
                                                            'net_id' => $res_value['id'],
                                                            'is_need_update' => ['EQ',1]
                                                        ])->find();
                                                        if(empty($net_issue)){
                                                            unset($res[$res_key]);
                                                        }
                                                    }
                                                    $res = array_values($res);
                                                }*/
                    }else{
                        continue;
                    }
                }else{
                    S('net_media_'.md5($fmedia['fmediacode']),null);
                    S('app_media',null);
                    $last_id = 1;
                    $res = $this->call_net_api($fmedia['fmediacode'],1,1);//接口返回数据结果
                    $fmediaid =  A('Common/NetAd','Model')->get_net_media_id($fmedia['fmediacode'],7,$this->get_net_mediaclass($res[0]['platform']));
                    $fmedias[$media_key]['fid'] = strval($fmediaid);
                    $this->rewrite_app_meida($fmedias,$fmedia['fmediacode'],$fmediaid);
                }

                if ($res){
                    //如果获取到数据，添加到数据集合
                    if(!empty($data)){
                        $data = array_merge($res,$data);
                    }else{
                        $data = $res;
                    }
                    if(count($data) >= $size*20) break;//如果数据量达到一定数量，终止循环
                }else{
                    S($fmedia['fmediacode'],$fmedia['fid'].','.(time()+300),300);//如果当前媒体数据为空，写入缓存，在缓存有效时间内不再获取此媒体
                }
            }else{
                $expire = explode(',',S($fmedia['fmediacode']));
                if(!empty($expire[1])){
                    $msg = ' Has been filtered , It still takes '.($expire[1]-time()).' seconds to expire!';
                }
                echo(++$number.' . [ <span style="color: red;">'.$fmedia['fmediacode'].'</span> ] '.$msg.'<br />');
            }
        }
        $no_data_7 = S("no_data_7");
        if(!empty($data)){
            S("no_data_7",null);//去除无数据警报专用缓存
            $this->add_net_data($data,7,$update_control);//将获取到的数据写入数据表
        }else{

            //无数据警报
            if(empty($no_data_7)){
                S("no_data_7",time());//无数据情况下未设置警报时间缓存则加
            }elseif((time()-$no_data_7) >= ($this->net_alarm * 3600)){

                S("no_data_7",null);//清除无数据警报缓存，防止多次提醒
                $no_data_7 = S("no_data_7");
                if(empty($no_data_7) && (date('H') > 9 && date('H') < 23)){
                    $send_res = A('Common/Alitongxin','Model')->alarm('15179133427','【国家类互联网数据】',date('Y-m-d/H:i'),'已连续'.$this->net_alarm.'小时未获取到数据，请及时上线查看！');
                    $send_res2 = A('Common/Alitongxin','Model')->alarm('13916381265','【国家类互联网数据】',date('Y-m-d/H:i'),'已连续'.$this->net_alarm.'小时未获取到数据，请及时上线查看！');
                }

            }

        }
        //dump($data);
        lock($app_key,null);

        if($update_control == 1){
            die('获取并更新'.count($data).'条数据花了'.(microtime(true)-$time).'秒!');
        }else{
            die('获取并写入'.count($data).'条数据花了'.(microtime(true)-$time).'秒!');
        }
    }

    /**by yjn
     * 获取需要轮询的互联网媒体
     * @param string $filename 文件路径
     * @return array
     */
    public function get_net_medias($key_name = 'app_medias',$filename = './Public/json/app_media.json'){
        $json_string = A('Common/System','Model')->important_data($key_name);
        if(empty($json_string)){
            $json_string = file_get_contents($filename);//读取json内容
            A('Common/System','Model')->important_data($key_name,$json_string);
        }
        $fmedias = json_decode($json_string,true);
        S('app_media',$fmedias,600);
        return $fmedias;
    }


    /**by yjn
     * 获取单个媒体或者单个类别的互联网数据记录最大id（如果有需要更新的数据，则是从需要更新的最初id开始获取），加入了更新机制
     * @param int $source_type
     * @param null $fmeida_id
     * @return int
     */
    public function get_max_id($source_type = 7,$update_control = 0,$fmeida_id = null){

        if(!empty($fmeida_id)){
            $map = ' fmediaid = "'.$fmeida_id.'" and source_type = '.$source_type.' ';
        }else{
            $map = ' source_type = '.$source_type.' ';
        }
        //$max_id_sql = 'SELECT net_id FROM `tnetissueputlog` WHERE `fmediaid` = "'.$fmedia['fid'].'" AND `source_type` = 7 ORDER BY net_id DESC LIMIT 1';

        //查找是否有需要更新的数据
        if($update_control == 1){

            $log_id = M('')->query('
                SELECT net_id 
                FROM `tnetissueputlog` 
                WHERE '. $map .'
                AND `is_need_update` = 1
                order by net_id LIMIT 1'
            );

            $log_id_7 = M('')->query('
                SELECT net_id 
                FROM `tnetissueputlog` 
                WHERE  source_type = 7 
                AND `is_need_update` = 1
                order by net_id LIMIT 1'
            );

            if(empty($log_id_7) && $source_type == 7){
                exit("没有需要更新的数据！");
            }elseif(empty($log_id) && $source_type == 7){
                return false;
            }elseif(empty($log_id)){
                exit("没有需要更新的数据！");
            }

        }else{
            $log_id = [];
        }

        //如果有需要更新的,获取它上一个id主键的广告id进行更新数据
        if(!empty($log_id)){

            $max_last_id = M('')->query('
                SELECT net_id as max_last_id
                FROM `tnetissueputlog` 
                WHERE '. $map .'
                AND `net_id` < '.$log_id[0]['net_id'].'
                order by net_id desc LIMIT 1'
            );
            if(!empty($max_last_id)){
                $last_id = $max_last_id[0]['max_last_id'];//需要更新的前一位id的广告id
            }else{
                $last_id = 1;
            }
        }else{
            //没有更新的则直接查询最后一位
            $max_last_id = M('')->query('
                SELECT net_id AS max_last_id 
                FROM `tnetissueputlog` 
                WHERE '. $map .'
                order by net_id desc LIMIT 1'
            );
            //获取最后的net_id
            if(!empty($max_last_id)){
                $last_id = $max_last_id[0]['max_last_id'];
            }else{
                $last_id = 1;
            }
        }
        if($source_type == 7){
            if($last_id <= 91052837){
                $last_id = 91052837;
            }
        }
        return intval($last_id);
    }

    /**by yjn
     * 获取并写入国家类别的互联网数据
     */
    public function get_net_data(){

        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);
        header("Content-type: text/html; charset=utf-8");
        session_write_close();
        $time = microtime(true);
        $update_control = I('update_control',0);
        $app_key = 'app_net_key_'.$update_control;
        lock($app_key,true,2);

        if($update_control == 1){
            $size = 2;
        }else{
            $size = 50;
        }
        //S('app_media',null);
        $fmedias = S('app_media');//从缓存中获取媒体
        if(empty($fmedias)){
            $fmedias = $this->get_net_medias();//如缓存中没有，则去获取
        }
        //循环获取值
        $data = [];
        $number = 0;
        $msg = '';
        foreach ($fmedias as $media_key => $fmedia){
            S($fmedia['fmediacode'],null);
            $fmediacode_s = S($fmedia['fmediacode']);//获取缓存中的媒体代码
            //如果缓存中没有，则执行
            if(empty($fmediacode_s)){
                if($fmedia['fid'] != 0 && $fmedia['fid'] != '2147483647'){
                    $last_id = $this->get_max_id(7,$update_control,$fmedia['fid']);
                    if($last_id !== false){
                        $res = $this->call_net_api($fmedia['fmediacode'],$last_id,$size);//接口返回数据结果
/*                        if($update_control == 1){
                            foreach ($res as $res_key=>$res_value){
                                $net_issue =  M("tnetissueputlog")->where([
                                    'net_id' => $res_value['id'],
                                    'is_need_update' => ['EQ',1]
                                ])->find();
                                if(empty($net_issue)){
                                    unset($res[$res_key]);
                                }
                            }
                            $res = array_values($res);
                        }*/
                    }else{
                        continue;
                    }
                }else{
                    S('net_media_'.md5($fmedia['fmediacode']),null);
                    S('app_media',null);
                    $last_id = 1;
                    $res = $this->call_net_api($fmedia['fmediacode'],1,1);//接口返回数据结果
                    $fmediaid =  A('Common/NetAd','Model')->get_net_media_id($fmedia['fmediacode'],7,$this->get_net_mediaclass($res[0]['platform']));
                    $fmedias[$media_key]['fid'] = strval($fmediaid);
                    $this->rewrite_app_meida($fmedias,$fmedia['fmediacode'],$fmediaid);
                }
                if ($res){
                    //如果获取到数据，添加到数据集合
                    if(!empty($data)){
                        $data = array_merge($res,$data);
                    }else{
                        $data = $res;
                    }
                    if(count($data) >= $size*20) break;//如果数据量达到一定数量，终止循环
                }else{
                    S($fmedia['fmediacode'],$fmedia['fid'].','.(time()+300),300);//如果当前媒体数据为空，写入缓存，在缓存有效时间内不再获取此媒体
                }
            }else{
                $expire = explode(',',S($fmedia['fmediacode']));
                if(!empty($expire[1])){
                    $msg = ' Has been filtered , It still takes '.($expire[1]-time()).' seconds to expire!';
                }
                echo(++$number.' . [ <span style="color: red;">'.$fmedia['fmediacode'].'</span> ] '.$msg.'<br />');
            }
        }
        $no_data_7 = S("no_data_7");
        if(!empty($data)){
            S("no_data_7",null);//去除无数据警报专用缓存
            $this->add_net_data($data,7,$update_control);//将获取到的数据写入数据表
        }else{

            //无数据警报
            if(empty($no_data_7)){
                S("no_data_7",time());//无数据情况下未设置警报时间缓存则加
            }elseif((time()-$no_data_7) >= ($this->net_alarm * 3600)){

                S("no_data_7",null);//清除无数据警报缓存，防止多次提醒
                $no_data_7 = S("no_data_7");
                if(empty($no_data_7) && (date('H') > 9 && date('H') < 23)){
                    $send_res = A('Common/Alitongxin','Model')->alarm('15179133427','【国家类互联网数据】',date('Y-m-d/H:i'),'已连续'.$this->net_alarm.'小时未获取到数据，请及时上线查看！');
                    $send_res2 = A('Common/Alitongxin','Model')->alarm('13916381265','【国家类互联网数据】',date('Y-m-d/H:i'),'已连续'.$this->net_alarm.'小时未获取到数据，请及时上线查看！');
                }
            }
        }
        //dump($data);
        lock($app_key,null);

        if($update_control == 1){
            die('获取并更新'.count($data).'条数据花了'.(microtime(true)-$time).'秒!');
        }else{
            die('获取并写入'.count($data).'条数据花了'.(microtime(true)-$time).'秒!');
        }
    }

    //当有媒体新增进来的处理
    public function rewrite_app_meida($fmedias,$media='未知APP',$media_id=0,$key_name = 'app_medias'){
        try{
            A('Common/System','Model')->important_data($key_name,json_encode($fmedias,true));
            $send_res = A('Common/Alitongxin','Model')->alarm('15179133427','【国家类互联网数据】',date('Y-m-d/H:i'),'APP:'.$media.';ID:'.$media_id.';已成功写入！');
        }catch (Exception $e) {
            $send_res = A('Common/Alitongxin','Model')->alarm('15179133427','【国家类互联网数据】',date('Y-m-d/H:i'),'APP '.$media.' ID写入失败！');
        }
    }

    //返回互联网媒体细类
    public function get_net_mediaclass($platform){
        $media_class = '';
        switch ($platform){
            case 1:
                $media_class = '1301';
                break;
            case 2:
                $media_class = '1302';
                break;
            case 4:
                $media_class = '1305';
                break;
            case 9:
                $media_class = '1303';
                break;
        }
        return $media_class;
    }

    /*20190319 姚俊男 国家分类获取互联网数据代码段 结束*/

	#推送需要识别的互联网广告给余洪禹
	public function push_identify(){
		$startTime = time();
		set_time_limit(600);
		$dataList = M('tnetissue_identify')->where(array('push_time'=>0))->limit(10)->select();
		
		foreach($dataList as $data){
			$time = time();
			$ret = http('http://192.168.0.252:8001/ad',array('id'=>'adsource1_'.$data['major_key'],'url'=>$data['net_target_url']),'POST',false,30);
			$qqtime = time() - $time;
			#if ($qqtime > 2) file_put_contents('LOG/confirm_feat_3',date('Y-m-d H:i:s').'	'.$data['major_key'].'	'. $qqtime ."\n",FILE_APPEND);
			$ret_arr = json_decode($ret,true);
			$push_time = 0;
			if($ret_arr['code'] == 200000){
				$push_time = time();
			}
			M('tnetissue_identify')->where(array('fid'=>$data['fid']))->save(array('push_time'=>$push_time,'push_ret'=>$ret));
			
		}
		echo 'SUCCESS	'. (time() - $startTime);
	}
	
	#接收余洪禹推送回来的数据
	public function confirm_feat(){
		A('Common/AliyunLog','Model')->dmpLogs();
		set_time_limit(600);
		session_write_close();
		/* 
			接口服务器：http://dmp.hz-data.com 端口：80 ，路径：/Api/NetAd/confirm_feat
			可以使用内网地址：172.16.72.58，请把dmp.hz-data.com 解析到内网IP，不可直接使用IP请求
			请求方式：POST
			请求数据格式：JSON
			请求参数：（id:推送给识别的时候带的id 前缀带有adsource1_，identify_code：特征码）
			请求示例：[{"id": "15111294","identify_code": "abcd"},{"id": "15111295","identify_code": "abcf"}]

			返回数据格式：JSON
			返回参数：code：只有当code为int0时才是接口正常 其它均为异常 需要重试，msg:提示消息，effect_row：影响行数
			返回示例：{ "code": 0, "msg": "", "effect_row": 3 } 
		*/

		$postData = file_get_contents('php://input');
		#file_put_contents('LOG/aconfirm_feat',$postData);
		$postDataArr = json_decode($postData,true);
		
		$effect_row = 0;
		$a_row = 0;
		foreach($postDataArr as $data){
			
			
			$confirm_feat = A('Api/NetAd','Model')->confirm_feat($data); #
			if($confirm_feat == 'effect_row'){
				$effect_row += 1;
			}elseif($confirm_feat == 'a_row'){
				$a_row += 1;
			}else{
				
			}
			
			
		}

		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','a_row'=>$a_row,'effect_row'=>$effect_row));
		
		
	}
	
	
	#获取余洪禹的广告数据
	public function get_netad21(){
		#A('Common/AliyunLog','Model')->dmpLogs();
		$needGetList = M('tnetissue_identify_2')->where(array('handle_state'=>0))->limit(50)->select();
		$queTime = 0;
		
		foreach($needGetList as $needGet){
			$i_id = $needGet['i_id'];
			$assoc_to_url = 'http://192.168.0.252:8010/ad/v2/assoc_to?id='.$i_id;
			
			$assoc_ret = http($assoc_to_url);
			$assocArr = json_decode($assoc_ret,true);
			$needGet['identify_code'] = $assocArr['assoc']['id'];
			if(strlen($needGet['identify_code']) < 5 || $assocArr['assoc']['result_type'] == 50){
				M('tnetissue_identify_2')->where(array('fid'=>$needGet['fid']))->save(array('handle_state'=>-1));
				continue; #
			}

			
			
			$i_id = $needGet['i_id'];
			$getUrl = 'http://192.168.0.252:8010/ad_source?id='.$i_id;
			$time = time();
			
			$getRet = http($getUrl);
			#var_dump($getUrl,$getRet);
			$queTime += time()-$time;
			
			$retArr = json_decode($getRet,true);
			if (!$retArr['net_id']){
				M('tnetissue_identify_2')->where(array('fid'=>$needGet['fid']))->save(array('handle_state'=>-1));
				continue;
			}
			if(mb_strlen($retArr['net_url']) < 5){
				M('tnetissue_identify_2')->where(array('fid'=>$needGet['fid']))->save(array('handle_state'=>-1));
				continue; #如果URL小于5个字符则不处理
			}
				
			
			
			$netUrlList = explode('|',$retArr['net_url']);
			if(count($netUrlList) > 1){
				M('tnetissue_identify_2')->where(array('fid'=>$needGet['fid']))->save(array('handle_state'=>-1));
				continue; #如果URL个数大于一个则不处理
			}
			
			
			$source_type = $retArr['source_type'];
			if(!$source_type) $source_type = 21;
			$fissuedate = strtotime(date('Y-m-d',$needGet['receive_time']));


			$is_exist = M('tnetissue')
                ->field('major_key')
                ->where(array('net_md5'=>$retArr['net_md5'],'source_type'=>$source_type))
                ->find();//是否存在相同的互联网广告样本，以net_md5为唯一值
			

            $is_exist_log = M('tnetissueputlog')
                ->field('major_key,is_need_update')
                ->where(array('net_id'=>$retArr['net_id'],'source_type'=>$source_type,'fissuedate' => $fissuedate))
                ->find();//是否存在发布记录  以来源标识、广告id、获取时间 为唯一
			
			if(!$is_exist){
				$tnetissue_add = array();

				$tnetissue_add['identify_code'] = $needGet['identify_code'];//识别码
				$tnetissue_add['api_json'] = json_encode($retArr);//保留原始json
				$tnetissue_add['net_id'] = $retArr['net_id'];//原始ID

				$tnetissue_add['net_width'] = $retArr['net_width'];//广告宽度
				$tnetissue_add['net_height'] = $retArr['net_height'];//广告高度
				$tnetissue_add['net_thumb_width'] = $retArr['net_width'];//缩略图宽度
				$tnetissue_add['net_thumb_height'] = $retArr['net_height'];//广缩略图高度
				$tnetissue_add['net_url'] = $retArr['net_url'];//文件URL
				$tnetissue_add['net_old_url'] = $retArr['net_old_url'];//原始文件URL
				$tnetissue_add['thumb_url_true'] = $retArr['net_url'];//缩略图URL
				$tnetissue_add['net_size'] = 0;//文件大小
				$tnetissue_add['ftype'] = '';//类型
				$tnetissue_add['fadname'] = $retArr['adtext'];//广告标题
				$tnetissue_add['fadtext'] = $retArr['adtext'];//广告描述
				$tnetissue_add['fbrand'] = '';//品牌
				
				
				$tnetissue_add['net_platform'] = $retArr['net_platform'];//平台  1PC，2移动
				$tnetissue_add['net_original_url'] = $retArr['net_original_url'];//广告原始地址
				$tnetissue_add['net_target_url'] = $retArr['net_target_url'];//落地页地址
				$tnetissue_add['net_md5'] = $retArr['net_md5'];//文件MD5码或者解析码
				$tnetissue_add['net_created_date'] = time()*1000;//样本创建时间
				
				
				$tnetissue_add['fmediaid'] = A('Common/NetAd','Model')->get_net_media_id($retArr['net_original_url'],$source_type,13);//媒体域名
				$tnetissue_add['fadowner'] = A('Common/Ad','Model')->getAdOwnerId($retArr['adowner'],'YHY_API');//广告主id
				

				$tnetissue_add['net_attribute04'] = $retArr['net_attribute04'];//移动端标识 IOS  Android



				$tnetissue_add['material'] = $retArr['net_material'];//信息展示方式，空或native， native为信息流
				$tnetissue_add['source_type'] = $source_type;//来源类型

				$tnetissue_add['fadclasscode'] = $fadclasscode;//广告内容类别
				$tnetissue_add['finputstate'] = 0;//录入状态
				$tnetissue_add['finspectstate'] = 0;//初审状态
				$tnetissue_add['fmodifier'] = '通过余洪禹接口获取';//修改人
				$tnetissue_add['fmodifytime'] = date("Y-m-d H:i:s");//更新时间
			     
				$tid = M('tnetissue')->add($tnetissue_add); 
				if($tid){ #新增的样本成功了
					M('tnetissue_identify')->add(array(
														'major_key'=>$tid,
														'net_target_url'=>$retArr['net_target_url'],
														'identify_code'=>$needGet['identify_code'],
														'push_time'=>time(),
														'receive_time'=>time(),
														#'is_task'=>0,
														'i_id'=>$needGet['i_id'],
														
												));
					
					$tnetissue_identify = M('tnetissue_identify')->where(array('identify_code'=>$needGet['identify_code'],'is_task'=>1))->find();
					if($tnetissue_identify){ #有任务的情况
						A('Api/NetAd','Model')->copy_sam($tnetissue_identify['major_key'],[$tid]); #同步数据
					}else{ #无任务的情况
						
						$task_array = [$this->addTask($tid)]; #获取 任务 需要传递 的数据
				
						if($task_array){
							$addTaskRes = A('TaskInput/NetAd','Model')->addTask($task_array); #添加任务
							M('tnetissue_identify')->where(array('major_key'=>$tid))->save(array('is_task'=>1)); #把这条记录修改为任务
							M('tnettask')->where(array('major_key'=>$tid))->save(array('identify_code'=>$needGet['identify_code']));
						}
						
						
					}
					
				
				
				
				
				
				
					M('tnetissue')->where(array('major_key'=>$tid))->save(array('marin_major_key'=>$tid)); 
					$net_trackers_list = json_decode($retArr['net_trackers_info'],true);
				
					M('trackers_record')->where(array('tid'=>$tid))->delete();
					foreach($net_trackers_list as $net_tracker){
						$trackerid = A('Api/NetAd','Model')->get_trackerid($net_tracker);
						M('trackers_record')->add(array('tid'=>$tid,'trackerid'=>$trackerid));
					}
				}
				
				
			}else{
				$tid = $is_exist['major_key'];
				
			}
			
			if(!$is_exist_log && $tid){
				$tnetissueputlog_data = array(

					'tid'=>$tid,//主表主键
					'net_id'=>$retArr['net_id'],//原始ID
					
					'net_width'=>$retArr['net_width'],//广告宽度
					'net_height'=>$retArr['net_height'],//广告高度
					'net_thumb_width'=>$retArr['net_width'],//缩略图宽度
					'net_thumb_height'=>$retArr['net_height'],//广缩略图高度
					'net_url'=>$retArr['net_url'],//文件URL
					'net_old_url'=>$retArr['net_old_url'],//原始文件URL
					'thumb_url_true'=>$retArr['net_url'],//缩略图URL
					'fmediaid' => A('Common/NetAd','Model')->get_net_media_id($retArr['net_original_url'],$source_type,$this->get_net_mediaclass($netAd['platform'])),//媒体域名
					'net_platform'=>$retArr['net_platform'],//平台  1PC，2移动
					'net_original_url'=>$retArr['net_original_url'],//广告原始地址
					
					'net_target_url'=>$retArr['net_target_url'],//落地页地址
					'net_md5'=>$retArr['net_md5'],//文件MD5码
					'net_created_date'=>$needGet['receive_time']*1000,//广告发现时间

					'source_type'=>$source_type,
					'fissuedate'=>$fissuedate//发布当天0点时间戳秒级
				);
				
				$tnetLogId = M('tnetissueputlog')->add($tnetissueputlog_data);
				
            }else{
				#var_dump($is_exist_log);
			}
			
			M('tnetissue_identify_2')->where(array('fid'=>$needGet['fid']))->save(array('handle_state'=>1));

		}
		echo 'success';
		
	}
	
	
	
	#发送数据日报
	public function send_daily(){
		
		$date = date('Y-m-d',time()-86400);
		$data1 = M('tnetissue_identify')
										->field('
												count(*) as tssl
												,count(case when receive_time > 0 then 1 else null end) as sbsl
												,count(DISTINCT identify_code) as pchsl	
													')
										->where(array('push_time'=>array('between',array(strtotime($date),strtotime($date)+86399))))
										->find();
									
		echo M('tnetissue_identify')->getLastSql() . "\n";
		
		
		$data2 = M('tnettask')
							->field('
									count(*) as rwsl
									,count(case when task_state = 2 then 1 else null end) as wcrwsl
										')
							->where(array('major_key'=>array('exp','in (select DISTINCT major_key from tnetissue_identify where push_time BETWEEN '.strtotime($date).' and '. (strtotime($date) +86399) .' and is_task = 1)')))
							->find();
							
		echo M('tnettask')->getLastSql() . "\n";
		
		$value = '';
		$value .= '以下是 '.$date.' 网络广告识别处理情况汇总'."\n";
		$value .= '新增网络广告数量:'.$data1['tssl']."\n";
		$value .= '识别数量:'.$data1['sbsl']."\n";
		#$value .= '排重后数量:'.$data1['pchsl']."\n";
		
		
		$value .= '新增任务数量:'.$data2['rwsl']."\n";
		$value .= '已完成任务数量:'.$data2['wcrwsl']."\n";
		
		echo $value;
		
		
		$ddUrl = 'https://oapi.dingtalk.com/robot/send?access_token=b135e638238692fceb422090977d9a8af67c6344976a0c8adef7b7912ea7f811';
		$msgPost = array();
		$msgPost['msgtype'] = 'text';
		$msgPost['text']['content'] = $value;
		$msgPost['at']['atMobiles'] = array('13588258695','17610242387','15858258071');
		$rr = http($ddUrl,json_encode($msgPost),'POST',array('Content-Type:application/json; charset=utf-8'));
	
		echo $rr."\n";
		
	}
	
	#添加广告任务
	private function addTask($major_key){
		$samInfo = M('tnetissue')->master(true)->field('major_key,fmediaid,net_id,net_created_date,"text" as ftype,"" as fadclasscode')->where(array('major_key'=>$major_key))->find();
		return $samInfo;
	}
	
		#按天广告排行
	public function dw_ad_top_list_3(){
		session_write_close();
		set_time_limit(600);
		$today = date('Y-m-d');
		$stt = strtotime($today);
		$ent = strtotime($today.' 23:59:59');
		$sql = "
			select fmediaid,
					0 as fprevious,
					count(*) as fcurrent,
					3 as ftype,
					'".date('Ymd',strtotime($today))."' as fcate
			from tnetissueputlog 
			where fissuedate BETWEEN ".$stt." AND ".$ent." 
			group by fmediaid;";
			
		$dataList = M()->query($sql);

		$w = M('dw_ad_top_list','',C('ZIZHI_DB'))->addAll($dataList,[],true);
		if($w){
			$wCount = count($dataList);
		}else{
			$wCount = 0;
		}
		echo $wCount."\n";
		echo date('Y-m-d H:i:s')."\n";
				
	}
	
	#按月广告排行
	public function dw_ad_top_list_1(){
		session_write_close();
		set_time_limit(600);
		$today = date('Y-m-d');
		$stt = strtotime(date('Y-m-01',strtotime($today)));
		$ent = strtotime("+1 months", $stt)-1;

		$sql = "
			select fmediaid,
					0 as fprevious,
					count(*) as fcurrent,
					1 as ftype,
					'".date('Ym',strtotime($today))."' as fcate
			from tnetissueputlog 
			where fissuedate BETWEEN ".$stt." AND ".$ent." 
			group by fmediaid;";
			
		$dataList = M()->query($sql);

		$w = M('dw_ad_top_list','',C('ZIZHI_DB'))->addAll($dataList,[],true);
		if($w){
			$wCount = count($dataList);
		}else{
			$wCount = 0;
		}
		echo $wCount."\n";
		echo date('Y-m-d H:i:s')."\n";
				
	}
	
	#按周广告排行
	public function dw_ad_top_list_2(){
		session_write_close();
		set_time_limit(600);
		$today = date('Y-m-d');
		
		$dayweek = dayweek(strtotime($today));
		$weekday = weekday($dayweek['year'],$dayweek['weeknum']);
		
		
		$stt = $weekday['start'];
		$ent = $weekday['end'];

		if($dayweek['weeknum'] < 10){
			$we = '0'.$dayweek['weeknum'];
		}else{
			$we = strval($dayweek['weeknum']);
		}
		
		
		$sql = "
			select fmediaid,
					0 as fprevious,
					count(*) as fcurrent,
					2 as ftype,
					'".$dayweek['year'].$we."' as fcate
			from tnetissueputlog 
			where fissuedate BETWEEN ".$stt." AND ".$ent." 
			group by fmediaid;";

		$dataList = M()->query($sql);

		$w = M('dw_ad_top_list','',C('ZIZHI_DB'))->addAll($dataList,[],true);
		if($w){
			$wCount = count($dataList);
		}else{
			$wCount = 0;
		}
		echo $wCount."\n";
		echo date('Y-m-d H:i:s')."\n";
				
	}
	
	/*获取余洪禹的热云数据*/
	public function get_yhysj(){
		session_write_close();
		set_time_limit(600);
		$date = I('get.date',date('Y-m-d',time()-86400));
		$queue = I('get.queue');
		var_dump($date,$queue);
		$s_time = date('Y-m-d H:i:s',strtotime($date));
		$e_time = date('Y-m-d H:i:s',strtotime($date)+86399);
		
		#M('test_table')->addAll([array('fid'=>123,'vvv'=>'a')],array(),false,true);

		for($i=0;$i<100;$i++){
			$url = 'http://192.168.0.252:8010/ad/v2/kw_queue?queue='.$queue.'&from='.urlencode($s_time).'&to='.urlencode($e_time).'&limit=500';
			
			$retData = http($url);
			$retArr = json_decode($retData,true);
			#var_dump($url,$retArr['update_time']);
			$s_time = $retArr['update_time'];
			if(count($retArr['ads']) == 0) break;
			
			$addAllData = [];
			foreach($retArr['ads'] as $data){

				$addAllData[] = array('i_id'=>$data[0],'receive_time'=>strtotime($data[3]));
			}
			
			$wf = M('tnetissue_identify_2')->addAll($addAllData,array(),false,true);
			
			var_dump($wf);
			
			
		}
		
		
		
	}


}