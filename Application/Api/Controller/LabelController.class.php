<?php
namespace Api\Controller;
use Think\Controller;

class LabelController extends Controller {
	
	
	
	/*获取标签列表*/
    public function get_label_list(){
		
		$flabelclass = I('flabelclass','');//获取fcode
		$flabel = I('flabel','');//获取标签
		
		$labelList = M('tlabel')->field('flabelid,flabelclass,flabel')->where(array('flabel'=>array('like','%'.$flabel.'%'),'flabelclass'=>$flabelclass,'fstate'=>1))->select();//查询标签列表
		
		$this->ajaxReturn(array('code'=>0,'labelList'=>$labelList));
		
	}


    public function searchMediaLabel($keyword = '')
    {
        $where = [
            'fstate' => ['EQ', 1],
        ];
        if ($keyword !== ''){
            $where ['flabel'] = ['LIKE', "%$keyword%"];
        }
        $data = M('tlabel')
            ->field('flabel value')
            ->where($where)
            ->group('flabel')
            ->select();
        $this->ajaxReturn($data);
	}

    public function searchMediaLabel2($keyword = '')
    {
        $where = [
            'fstate' => ['EQ', 1],
        ];
        if ($keyword !== ''){
            $where ['flabel'] = ['LIKE', "%$keyword%"];
        }
        $data = M('tlabel')
            ->field('flabel value')
            ->where($where)
            ->group('flabel')
            ->select();
        $this->ajaxReturn([
            'code' => 0,
            'value' => $data,
        ]);
    }
	
	
}