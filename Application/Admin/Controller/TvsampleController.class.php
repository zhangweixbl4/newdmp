<?php
//电视样本控制器
namespace Admin\Controller;
use Think\Controller;
class TvsampleController extends BaseController {

	public function index(){
		if(A('Admin/Authority','Model')->authority('200155') === 0){
			exit('没有权限');
		}
		$p = I('p',1);//当前第几页
		$pp = 36;//每页显示多少记录
		$keyword = I('keyword');//搜索关键词

		$fadname = I('fadname');// 广告名称
		$fversion = I('fversion');// 版本描述
		$fmediaid  = I('fmediaid');// 媒体ID
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID
		$fcreatetime_s = I('fcreatetime_s');// 建样时间
		if($fcreatetime_s == '') $fcreatetime_s = '2015-01-01';
		$fcreatetime_e = I('fcreatetime_e');// 建样时间
		if($fcreatetime_e == '') $fcreatetime_e = date('Y-m-d');
		$adclass_code = I('adclass_code');
		$fcreator = I('fcreator');//创建人
		$uuid = I('uuid');//uuid
		$fadlenSearch = I('fadlenS').','.I('fadlen');//格式：“EGT,30”或“ELT,60”或“BETWEEN,30,60”
		$fadlenSearch = explode(',',$fadlenSearch);

		$where = array();//查询条件
		$where['ttvsample.fstate'] = 1;
		$where['ttvsample.fcuted'] = 2;
		if($fcreator != ''){
			$where['ttvsample.fcreator'] = array('like','%'.$fcreator.'%');
		}
		if($uuid != ''){
			$where['ttvsample.uuid'] = $uuid;
		}
		// 按样本时长查询
		if(count($fadlenSearch) == 2 && !empty($fadlenSearch[0]) && !empty($fadlenSearch[1])){
			// 大于等于或小于等于
			$where['ttvsample.fadlen'] = [$fadlenSearch[0],$fadlenSearch[1]];
		}elseif(count($fadlenSearch) == 3 && !empty($fadlenSearch[0]) && !empty($fadlenSearch[1]) && !empty($fadlenSearch[2])){
			// 区间
			$where['ttvsample.fadlen'] = [$fadlenSearch[0],[$fadlenSearch[1],$fadlenSearch[2]]];
		}

		if($keyword != ''){
			$where['ttvsample.fversion|tad.fadname'] = array('like','%'.$keyword.'%');
		} 
		
		if($fadname != ''){
			$where['tad.fadname'] = array('like','%'.$fadname.'%');//广告名称
		}
		if($fversion != ''){
			$where['ttvsample.fversion'] = array('like','%'.$fversion.'%');//版本描述
		}
		if($fmediaid != ''){
			$where['ttvsample.fmediaid'] = $fmediaid;//媒体ID
		}
		if(is_array($fillegaltypecode)){
			$where['ttvsample.fillegaltypecode'] = array('in',$fillegaltypecode);//违法类型代码
		}
		if($fcreatetime_s != '' || $fcreatetime_e != ''){
			$where['ttvsample.fcreatetime'] = array('between',$fcreatetime_s.','.$fcreatetime_e.' 23:59:59');
		}
		
		if($adclass_code != '' && $adclass_code != '0'){
			

			$adclass_code_strlen = strlen($adclass_code);//获取code长度
			$where['left(tad.fadclasscode,'.$adclass_code_strlen.')'] = $adclass_code;//广告分类搜索条件
		
		}
		if(strval(intval($fadname)) == $fadname){
			$where = array('ttvsample.fid'=>$fadname);
		}
		$count = M('ttvsample')
								->cache(true,120)
								->join('tad on tad.fadid = ttvsample.fadid')
								->join('tadowner on tadowner.fid = tad.fadowner')
								->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数

		
		$tvsampleList = M('ttvsample')
										->cache(true,60)
										->field('ttvsample.*,tad.fadname,tadowner.fname as adowner_name')
										->join('tad on tad.fadid = ttvsample.fadid')
										->join('tadowner on tadowner.fid = tad.fadowner')
										->where($where)
										->order('ttvsample.fid desc')
										->limit($Page->firstRow.','.$Page->listRows)->select();//查询广告样本列表
									
		$tvsampleList = list_k($tvsampleList,$p,$pp);//为列表加上序号
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
    	$this->assign('IllegalList',$IllegalList);//违法表现数据列表
//		var_dump($illegaltype);
		//var_dump($tvsampleList);
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->assign('tvsampleList',$tvsampleList);//广告样本列表
		$this->assign('illegaltype',$illegaltype);//违法类型
		$this->assign('page',$Page->show());//分页输出
		
		//var_dump($tvsampleList);
		//$this->display();
	}
	public function tvsample_edit(){
		if(A('Admin/Authority','Model')->authority('200153') === 0){
			exit('没有权限');
		}
		$fid = I('fid');//获取样本ID
		$tvsampleDetails = M('ttvsample')
										->field('ttvsample.*,tad.fadname,tad.fbrand,tad.fadclasscode,tad.fadclasscode_v2,tad.fadowner,tadclass.ffullname,hz_ad_class.ffullname fadclassfullname_v2,tadowner.fname as adowner_name,tillegaltype.fillegaltype,tmedia.fmedianame')
										->join('tad on tad.fadid = ttvsample.fadid')
										->join('tadclass on tadclass.fcode = tad.fadclasscode')
										->join('hz_ad_class on hz_ad_class.fcode = tad.fadclasscode_v2')
										->join('tmedia on tmedia.fid = ttvsample.fmediaid')
										->join('tadowner on tadowner.fid = tad.fadowner')
										->join('tillegaltype on tillegaltype.fcode = ttvsample.fillegaltypecode')
										->where(array('ttvsample.fid'=>$fid))
										->find();//查询样本详情
		$tvsampleillegal = M('ttvsampleillegal')
												->field('fillegalcode,fexpression')
												->where(array('fsampleid'=>$fid))->select();
		// var_dump($tvsampleDetails);
		$this->assign('tvsampleDetails',$tvsampleDetails);
		$this->assign('tvsampleillegal',$tvsampleillegal);
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
		$this->assign('IllegalList',$IllegalList);//违法表现数据列表
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->display();
	}
	
		
	/*样本详情*/
	public function ajax_tvsample_details(){
		
		$fid = I('fid');//获取样本ID
		$tvsampleDetails = M('ttvsample')
										->field('ttvsample.*,tad.fadname,tad.fbrand,tad.fadclasscode,tad.fadclasscode_v2,tad.fadowner,tadclass.ffullname,hz_ad_class.ffullname fadclassfullname_v2,tadowner.fname as adowner_name,tillegaltype.fillegaltype,tmedia.fmedianame')
										->join('tad on tad.fadid = ttvsample.fadid')
										->join('tadclass on tadclass.fcode = tad.fadclasscode')
										->join('hz_ad_class on hz_ad_class.fcode = tad.fadclasscode_v2')
										->join('tmedia on tmedia.fid = ttvsample.fmediaid')
										->join('tadowner on tadowner.fid = tad.fadowner')
										->join('tillegaltype on tillegaltype.fcode = ttvsample.fillegaltypecode')
										->where(array('ttvsample.fid'=>$fid))
										->find();//查询样本详情
		$tvsampleillegal = M('ttvsampleillegal')
												->field('fillegalcode,fexpression')
												->where(array('fsampleid'=>$fid))->select();
		$tvsampleDetails['sample_to_word'] = A('Admin/Sample','Model')->sample_to_word($tvsampleDetails['uuid']);	
		
			
		
		$this->ajaxReturn(array('code'=>0,'tvsampleDetails'=>$tvsampleDetails,'tvsampleillegal'=>$tvsampleillegal));
	}
	
	/*转入广告线索*/
	public function to_adclue(){
		if(A('Admin/Authority','Model')->authority('200154') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));
		}
		$fid = I('fid');//广告样本ID
		
		$tvsampleInfo = M('ttvsample')->where(array('fid'=>$fid))->find();
		if(intval($tvsampleInfo['fillegaltypecode']) == 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'转入失败,只有违法广告能转入'));
		}
		$fmediaclass = M('tmedia')->where(array('fid'=>$tvsampleInfo['fmediaid']))->getField('fmediaclassid');//媒介类型代码
		$fmediaclass = mb_substr($fmediaclass,0,2);
		$adclue_count = M('tadclue')->where(array('fsampleid'=>$tvsampleInfo['fid'],'fmediaclass'=>$fmediaclass))->count();
		if($adclue_count > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'此样本已在线索中'));
		
		$a_data = array();
		$a_data['fsupervise'] = 0;//管理机关

		$a_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$a_data['fstate'] = 0;//状态
		$a_data['fmediaclass'] = $fmediaclass;
		$a_data['fmediaid'] = $tvsampleInfo['fmediaid'];//媒介id
		$a_data['fissuedate'] = $tvsampleInfo['fissuedate'];//发行日期
		$a_data['fsampleid'] = $tvsampleInfo['fid'];//样本ID
		$a_data['fadid'] = $tvsampleInfo['fadid'];//广告ID
		$a_data['fspokesman'] = $tvsampleInfo['fspokesman'];//代言人
		$a_data['fversion'] = $tvsampleInfo['fversion'];//版本说明
		$a_data['fapprovalid'] = $tvsampleInfo['fapprovalid'];//审批号
		$a_data['fapprovalunit'] = $tvsampleInfo['fapprovalunit'];//审批单位
		$a_data['fadlen'] = $tvsampleInfo['fadlen'];//广告样本长度
		$a_data['fillegaltypecode'] = $tvsampleInfo['fillegaltypecode'];//违法类型代码
		$a_data['fillegalcontent'] = $tvsampleInfo['fillegalcontent'];//涉嫌违法内容
		$a_data['fexpressioncodes'] = $tvsampleInfo['fexpressioncodes'];//违法表现代码
		$a_data['fexpressions'] = $tvsampleInfo['fexpressions'];//违法表现
		$a_data['fconfirmations'] = $tvsampleInfo['fconfirmations'];//认定依据
		$a_data['fpunishments'] = $tvsampleInfo['fpunishments'];//处罚依据
		$a_data['fpunishmenttypes'] = $tvsampleInfo['fpunishmenttypes'];//处罚种类及幅度
		$a_data['favifilename'] = $tvsampleInfo['favifilename'];//视频路径
		
		
		$rr = M('tadclue')->add($a_data);
		
		if($rr > 0){
			$fregulatorcode = A('Common/Regulatormedia','Model')->get_regulatormedia($tvsampleInfo['fmediaid']);//监管机构ID
			A('Common/Adcluesection','Model')->create_adcluesection($rr,0,$fregulatorcode,session('personInfo.fname'));//创建环节
			$this->ajaxReturn(array('code'=>0,'msg'=>'转入线索成功'));
			
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'转入线索失败,原因未知'));
		}
		
		
	}

	/*添加、编辑电视广告样本*/
	public function add_edit_tvsample(){
		M()->startTrans();//开启事务方法

		$fid = I('fid');//广告样本ID
		$fid = intval($fid);//转为数字
		$fadname = I('fadname');//广告名
		$fbrand = I('fbrand');// 广告品牌
		$adclass_code = I('adclass_code');//广告分类code
		$fadclasscode_v2 = I('fadclasscode_v2');//商业类别code
		$adowner_name = I('adowner_name');//广告主
		$fversion = I('fversion');//版本
		$fmediaid = I('fmediaid');//媒体ID
		$fissuedate = I('fissuedate');//发布日期
		$fspokesman = I('fspokesman');//代言人
		$fadlen = I('fadlen');//样本长度
		$fillegalcontent = I('fillegalcontent');//违法内容
		$fexpressioncodes = I('fexpressioncodes');//违法代码
		$fapprovalid = I('fapprovalid');//审批号
		$fapprovalunit = I('fapprovalunit');//审批单位
		$fstate = I('fstate');//状态
		$fsource = I('fsource');//来源（0-监测，1抽查）
		$favifilename = I('favifilename');//上传视频
		$synSameUUID = I('synSameUUID');//同步更新相同 UUID 样本的广告基本信息
		$fadid_old = I('fadid');//更改前的广告ID
		
		$fadid = A('Adinput/InputTask','Model')->get_ad_id($fadname,$fbrand,$adclass_code,$adowner_name,$fadclasscode_v2,1);//获取广告ID
		
		$a_e_data['fmediaid'] = $fmediaid;//媒介ID
		$a_e_data['fissuedate'] = $fissuedate;//发布日期
		$a_e_data['fadid'] = $fadid;//广告ID
		$a_e_data['fversion'] = $fversion;//版本说明
		$a_e_data['fspokesman'] = $fspokesman;//代言人
		$a_e_data['fadlen'] = $fadlen;//样本长度
		$a_e_data['fillegalcontent'] = $fillegalcontent;//违法内容
		
		$a_e_data['fapprovalid'] = $fapprovalid;//审批号
		$a_e_data['fapprovalunit'] = $fapprovalunit;//审批单位
		$a_e_data['favifilename'] = $favifilename;//视频文件路径
		$a_e_data['fsource'] = $fsource;//来源（0-监测，1抽查）
		$a_e_data['fstate'] = $fstate;//状态（-1删除，0无效，1-有效，2抽查）
		
		if(M('tadclass')->where(array('fcode'=>$adclass_code))->count() == 0){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'广告类别错误'));
		} 
		if($favifilename == ''){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'视频素材路径错误'));
		}
		if($fissuedate == ''){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'发布日期错误'));
		}
		if($fmediaid == ''){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'请选择发布媒介'));
		}
		if($fadname == ''){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'请输入广告名称'));
		}
		if($fid == 0){//判断是修改还是新增
			if(A('Admin/Authority','Model')->authority('200151') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));
			}
			$a_e_data['fcreator'] = session('personInfo.fname');//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('ttvsample')->add($a_e_data);//新增数据
			$tvsampleid = $rr;//样本ID赋值	
		}else{
			if(A('Admin/Authority','Model')->authority('200152') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));
			}
			$a_e_data['fmodifier'] = session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('ttvsample')->where(array('fid'=>$fid))->save($a_e_data);//修改数据
			// 同步更新相同UUID样本信息
			if($synSameUUID == 1){
				$uuid = M('ttvsample')->cache(true,120)->where(['fid'=>$fid])->getField('uuid');
				$info = [
					'fmediaid'      => $a_e_data['fmediaid'],
					'fissuedate'    => $a_e_data['fissuedate'],
					'fadid'         => $a_e_data['fadid'],
					'fversion'      => $a_e_data['fversion'],
					'fspokesman'    => $a_e_data['fspokesman'],
					'fadlen'        => $a_e_data['fadlen'],
					'fapprovalid'   => $a_e_data['fapprovalid'],
					'fapprovalunit' => $a_e_data['fapprovalunit'],
					'fmodifier'     => session('personInfo.fname').'_UUID同步更新',
					'fmodifytime'   => date('Y-m-d H:i:s'),
				];
				M('ttvsample')->where(['fid'=>['NEQ',$fid],'uuid'=>$uuid,'fadid'=>$fadid_old])->save($info);
			}
			$tvsampleid = $fid;//样本ID赋值			
		}
		
		$illegal = A('Open/Tvsample','Model')->tvsampleillegal($tvsampleid,explode(',',$fexpressioncodes),session('personInfo.fname'));//添加电视广告违法表现对应表并获取冗余字段
		$rr_illegal = M('ttvsample')->where(array('fid'=>$tvsampleid))->save($illegal);//修改数据
		if($rr > 0){
			set_tillegalad_data($tvsampleid,'tv');//加入AGP违法广告逻辑
			M()->commit();//事务提交方法
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功'));
		}else{
			M()->rollback();//事务回滚方法
			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败,原因未知'));
		}
	}


    public function editIllegalInfo()
    {
        if(A('Admin/Authority','Model')->authority('200153') === 0){
            exit('没有权限');
        }
        $fid = I('fid');//获取样本ID
        $tvsampleDetails = M('ttvsample')
            ->field('ttvsample.*,tad.fadname,tad.fbrand,tad.fadclasscode,tad.fadowner,tadclass.ffullname,tadowner.fname as adowner_name,tillegaltype.fillegaltype,tmedia.fmedianame')
            ->join('tad on tad.fadid = ttvsample.fadid')
            ->join('tadclass on tadclass.fcode = tad.fadclasscode')
            ->join('tmedia on tmedia.fid = ttvsample.fmediaid')
            ->join('tadowner on tadowner.fid = tad.fadowner')
            ->join('tillegaltype on tillegaltype.fcode = ttvsample.fillegaltypecode')
            ->where(array('ttvsample.fid'=>$fid))
            ->find();//查询样本详情
        $tvsampleillegal = M('ttvsampleillegal')
            ->field('fillegalcode,fexpression')
            ->where(array('fsampleid'=>$fid))->select();
        $this->assign('tvsampleDetails',$tvsampleDetails);
        $this->assign('tvsampleillegal',$tvsampleillegal);
        $illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
        $IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
        $this->assign('IllegalList',$IllegalList);//违法表现数据列表
        $this->display();
    }
	
}