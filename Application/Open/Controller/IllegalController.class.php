<?php
namespace Open\Controller;
use Think\Controller;

class IllegalController extends BaseController {
	
	/*获取违法表现列表*/
    public function get_illegal_list(){
		$illegal_data = file_get_contents('php://input');//获取post数据
		$illegal_data = json_decode($illegal_data,true);//把json转化成数组
		if($illegal_data['model'] == 'get_all'){//判断是否获取全部
			$illegalList = M('tillegal')->field('')->where(array('fstate'=>1))->select();//
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取的是全部列表','illegalList'=>$illegalList));
		}
		$fcode = intval($illegal_data['fcode']);//广告分类code

		if($fcode == 0) $fcode = '';
		$illegalList = M('tillegal')->field('fcode,fexpression,fconfirmation,fpunishment,fpunishmenttype,fkeywords,fadclass,fillegaltype')->where(array('fpcode'=>$fcode,'fstate'=>1))->select();//查询广告分类列表
		
		$illegalDetails = M('tillegal')->field('fcode,fexpression,fconfirmation,fpunishment,fpunishmenttype,fkeywords,fadclass,fillegaltype')->where(array('fcode'=>$fcode))->find();//查询广告分类详情
		
		if(!$illegalDetails) $illegalDetails = array('fcode'=>'','fadclass'=>'全部','ffullname'=>'全部');
		$this->ajaxReturn(array('code'=>0,'msg'=>'','illegalList'=>$illegalList,'illegalDetails'=>$illegalDetails));
		
	}
	
	/*获取违法类型列表*/
	public function illegal_type(){
		$illegal_data = file_get_contents('php://input');//获取post数据
		$illegal_data = json_decode($illegal_data,true);//把json转化成数组
		$fcode = $illegal_data['fcode'];
		$where = array();
		$where['fstate'] = 1;
		if($fcode != '') $where['fcode'] = $fcode;
		
		$illegaltypeList = M('tillegaltype')->field('fcode,fillegaltype')->where($where)->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'','illegaltypeList'=>$illegaltypeList));
		
	}
	
}