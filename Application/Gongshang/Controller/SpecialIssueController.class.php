<?php
//样本控制器
namespace Gongshang\Controller;
use Think\Controller;
class SpecialIssueController extends BaseController {

	public function index(){
//		$adclass=array('01','02','06');
//		$special_adclass = implode(",",$adclass);
//		$paperissueList = M('tregulator')->where(array('fid'=>session('regulatorpersonInfo.fregulatorid')))->save(array('special_adclass'=>$special_adclass));
//		exit();
		if(I('media_class') == '' ) $_GET['media_class'] = '01';
		$media_class = I('media_class');//媒体类型，01电视，02广播，03报纸
		if($media_class == '01'){
			A('Gongshang/SpecialTvissue')->index();
		}elseif($media_class == '02'){
			A('Gongshang/SpecialBcissue')->index();
		}elseif($media_class == '03'){
			A('Gongshang/SpecialPaperissue')->index();
		}
		$this->display();
	}


	/*导出到文件*/
	public function export(){
//		$region_id = session('regulatorpersonInfo.regionid');//地区ID
//		if(I('get.region_id') > 100000){
//			$region_id = I('get.region_id');
//		}
		$csv_url = P_U('Gongshang/SpecialIssue/export_csv',array_merge(I('')));
		$this->ajaxReturn(array('code'=>0,'msg'=>'','csv_url'=>$csv_url));
	}

	/*导出到文件*/
	public function export_csv(){
		$file_name = I('file_name');//文件名称
		if($file_name == '') $file_name = '发布记录导出文件';
		$file_type = I('file_type');//文件类型
		if($file_type != 'EXCEL' && $file_type != 'XML' && $file_type != 'TXT'){
			$file_type = 'EXCEL';
		}
		if(I('media_class') == '' ) $_GET['media_class'] = '01';
		
		$media_class = I('media_class');//媒体类型，01电视，02广播，03报纸
		$keyword = I('keyword');//搜索关键词
		$fadname = I('fadname');// 广告名称
		$fversion = I('fversion');// 版本描述
		$fmediaid  = I('fmediaid');// 媒体ID
		$adclass_code = I('adclass_code');//广告类别
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID
		$fstarttime_s = I('fstarttime_s');// 发布日期
		if($fstarttime_s == '') $fstarttime_s = '2015-01-01';
		$fstarttime_e = I('fstarttime_e');// 发布日期
		if($fstarttime_e == '') $fstarttime_e = date('Y-m-d');
		
		$this_region_id = I('this_region_id');//是否只查询本级地区
		$region_id = I('region_id'); 
		$where = array();//查询条件

		if($fmediaid == ''){
			//是否有指定的媒体
			$media=$this->get_access_media();//获取权限判断后的最终媒体列表
			if($media){
				$where['issue.fmediaid'] = array('in', $media);//发布媒介
			}
		}else{
			$where['issue.fmediaid'] = $fmediaid;//发布媒介
		}

		//如果只查询当前级
		if($this_region_id == 'true'){
			$where['tmediaowner.fregionid'] = $region_id;//地区搜索条件
		}else{
			if($region_id !=100000 && (I('region_id') ||empty($media))){//不是国家工商局并且有地区检索或者没有media
				$region_id_rtrim = rtrim($region_id, '00');//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmediaowner.fregionid,' . $region_id_strlen . ')'] = $region_id_rtrim;//地区搜索条件
			}
		}
		$where['issue.fstate'] = array('neq',-1);

		//如果没有检索$adclass_code 并且有special_adclass
		if(session('regulatorpersonInfo.special_adclass') &&empty($adclass_code)){

			$where['tadclass.fcode|tadclass.fpcode'] = array('in',session('regulatorpersonInfo.special_adclass'));//code或者pcode 是special_adclass
		}
		//如果有检索$adclass_code
		if(!empty($adclass_code)){
			$where['tadclass.fcode|tadclass.fpcode'] = $adclass_code;//广告名称
		}

		if($fadname != ''){
			$where['tad.fadname'] = array('like','%'.$fadname.'%');//广告名称
		}

		if($fversion != ''){
			$where['sample.fversion'] = array('like','%'.$fversion.'%');//版本描述
		}
		if(is_array($fillegaltypecode)){
			$where['sample.fillegaltypecode'] = array('in',$fillegaltypecode);//违法类型代码
		}
		if($fstarttime_s != '' || $fstarttime_e != ''){
			$where['issue.fissuedate'] = array('between',$fstarttime_s.','.$fstarttime_e.' 23:59:59');
		}


		if($media_class == '01'){
			$page = 1;
			$tvissueList = array();
			while($page <= 1){
				
				$tvissueList0 = M('ttvissue')->alias('issue')
											->field('
													tmedia.fmedianame,
													tregion.ffullname as region_name,
													\'电视\' mediaclass,
													issue.fstarttime,
													issue.flength,
													issue.fissuetype,
													tadclass.ffullname as adclass_code,
													tad.fadname,
													tad.fadname as tadname,
													sample.fadmanuno,sample.fmanuno,sample.fadapprno,sample.fapprno,sample.fadent,sample.fadent,sample.fent,sample.fentzone,
													tillegaltype.fillegaltype,
													sample.fexpressioncodes,
													sample.fexpressions,
													CONCAT_WS(\'_\', DATE_FORMAT(issue.fstarttime,\'%Y%m%d%H%i%s\'),tad.fadname,CONCAT(issue.ftvissueid,\'.mp4\')),
													CONCAT(\'http://dmp.hz-data.com/Api/Issue/tv_v/ftvissueid/\',issue.ftvissueid,\'/n/\',CONCAT_WS(\'_\', DATE_FORMAT(issue.fstarttime,\'%Y%m%d%H%i%s\'),tad.fadname,issue.ftvissueid))

													')
					->join('ttvsample  as sample on sample.fid = issue.ftvsampleid')
					->join('tad on tad.fadid = sample.fadid')
					->join('tadclass on tadclass.fcode = tad.fadclasscode', 'LEFT')//广告内容列别
					->join('tmedia on tmedia.fid = issue.fmediaid')
					->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
					->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
					->join('tregion on tregion.fid = tmediaowner.fregionid')
					->where($where)
					->order('issue.fstarttime desc')
					->page($page,20000)
					->select();//查询广告样本列表

				$page++;
				if(!$tvissueList0){
					break;
				}
				$tvissueList = array_merge($tvissueList,$tvissueList0);
			}	
			if($file_type == 'XML'){
				header("Content-Disposition: attachment;filename=".$file_name.".xml"); 
				echo arrayToXml($tvissueList);
				exit;
			}			
			$csvContent = A('Common/Csv','Model')->make($tvissueList,array('媒体名称','媒体所在地','媒体分类','发布时间','发布时长（秒）','发布类型','总局数据库查询的产品类别','广告中标识的产品名称','总局数据库查询的产品名称',
				'广告中标识的生产批准文号','总局数据库查询的生产批准文号','广告中标识的广告批准文号','总局数据库查询的广告批准文号','广告中标识的生产企业（证件持有人）名称','总据库查询的生产企业（证件持有人）名称','总局数据库查询的生产企业（证件持有人）所在地区',
				'违法广告分类','违法表现代码','违法广告研判依据','文件名','链接'));
			 
		}
		elseif($media_class == '02'){
			$bcissueList = M('tbcissue')->alias('issue')
				->field('
												tmedia.fmedianame,
												tregion.ffullname as region_name,
												\'广播\' mediaclass,
												issue.fstarttime,
												issue.flength,
												issue.fissuetype,
												tadclass.ffullname as adclass_code,
												tad.fadname,
												tad.fadname as tadname,
												sample.fadmanuno,sample.fmanuno,sample.fadapprno,sample.fapprno,sample.fadent,sample.fadent,sample.fent,sample.fentzone,
												tillegaltype.fillegaltype,
												sample.fexpressioncodes,
												sample.fexpressions,
												CONCAT_WS(\'_\', DATE_FORMAT(issue.fstarttime,\'%Y%m%d%H%i%s\'),tad.fadname,CONCAT(issue.fbcissueid,\'.mp3\')),
												CONCAT(\'http://dmp.hz-data.com/Api/Issue/bc_a/fbcissueid/\',issue.fbcissueid,\'/n/\',CONCAT_WS(\'_\', DATE_FORMAT(issue.fstarttime,\'%Y%m%d%H%i%s\'),tad.fadname,issue.fbcissueid))
												')
										->join('tbcsample as sample on sample.fid = issue.fbcsampleid')
										->join('tad on tad.fadid = sample.fadid')
										->join('tadclass on tadclass.fcode = tad.fadclasscode', 'LEFT')//广告内容列别
										->join('tmedia on tmedia.fid = issue.fmediaid')
										->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
										->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
				                        ->join('tregion on tregion.fid = tmediaowner.fregionid')
										->where($where)
										->order('issue.fbcissueid desc')
										->limit(30000)
										->select();//查询广告样本列表
			if($file_type == 'XML'){
				header("Content-Disposition: attachment;filename=".$file_name.".xml");
				echo arrayToXml($bcissueList);
				exit;
			}
			$csvContent = A('Common/Csv','Model')->make($bcissueList,array('媒体名称','媒体所在地','媒体分类','发布时间','发布时长（秒）','发布类型','总局数据库查询的产品类别','广告中标识的产品名称','总局数据库查询的产品名称',
				'广告中标识的生产批准文号','总局数据库查询的生产批准文号','广告中标识的广告批准文号','总局数据库查询的广告批准文号','广告中标识的生产企业（证件持有人）名称','总据库查询的生产企业（证件持有人）名称','总局数据库查询的生产企业（证件持有人）所在地区',
				'违法广告分类','违法表现代码','违法广告研判依据','文件名','链接'));

		}
		elseif($media_class == '03'){
			$paperissueList = M('tpaperissue')->alias('issue')
										->field('
												tmedia.fmedianame,
												tregion.ffullname as region_name,
												\'报纸\' mediaclass,
												issue.fissuedate,
												issue.fpage,
												issue.fsize,
												issue.fissuetype,
												tadclass.ffullname as adclass_code,
												tad.fadname,
												tad.fadname as tadname,
												sample.fadmanuno,sample.fmanuno,sample.fadapprno,sample.fapprno,sample.fadent,sample.fadent,sample.fent,sample.fentzone,
												tillegaltype.fillegaltype,
												sample.fexpressioncodes,
												sample.fexpressions,
												sample.fjpgfilename
												')
										->join('tpapersample as sample on sample.fpapersampleid = issue.fpapersampleid')
										->join('tad on tad.fadid = sample.fadid')
										->join('tadclass on tadclass.fcode = tad.fadclasscode', 'LEFT')//广告内容列别
										->join('tmedia on tmedia.fid = issue.fmediaid')
										->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
										->join('tillegaltype on tillegaltype.fcode = sample.fillegaltypecode')
										->join('tregion on tregion.fid = tmediaowner.fregionid')
										->where($where)
										->order('issue.fpaperissueid desc')
										->limit(30000)
										->select();//查询广告样本列表


			if($file_type == 'XML'){
				header("Content-Disposition: attachment;filename=".$file_name.".xml");
				echo arrayToXml($paperissueList);
				exit;
			}
			$csvContent = A('Common/Csv','Model')->make($paperissueList,array('媒体名称','媒体所在地','媒体分类','发布时间','版面','规格','发布类型','总局数据库查询的产品类别','广告中标识的产品名称','总局数据库查询的产品名称',
				'广告中标识的生产批准文号','总局数据库查询的生产批准文号','广告中标识的广告批准文号','总局数据库查询的广告批准文号','广告中标识的生产企业（证件持有人）名称','总据库查询的生产企业（证件持有人）名称','总局数据库查询的生产企业（证件持有人）所在地区',
				'违法广告分类','违法表现代码','违法广告研判依据','视频文件'));
		}
		else{

		}
		//var_dump($csvContent);
		
		if($file_type == 'EXCEL'){
			header("Content-Disposition: attachment;filename=".$file_name.".csv"); 
		}elseif($file_type == 'TXT'){
			header("Content-Disposition: attachment;filename=".$file_name.".txt"); 
		}
		
		echo $csvContent;
		exit;
		
	}
	/*获取广告类别列表*/
	public function ajax_adclass_list(){
		$fcode = I('fcode','');//获取fcode
		if($fcode == 0) $fcode ='';
		//如果有special_adclass 并且有fcode
		if(empty($fcode) && session('regulatorpersonInfo.special_adclass')){
			$adclassList = M('tadclass')->field('fcode,fadclass')->where(array('fcode'=>array('in',session('regulatorpersonInfo.special_adclass'))))->select();//列别明细

		}else{
			$adclassList = M('tadclass')->field('fcode,fadclass')->where(array('fpcode'=>$fcode,'fcode'=>array('neq','0')))->select();//列别明细
			$adclassDetails = M('tadclass')->field('fcode,left(fadclass,4) as fadclass,ffullname')->where(array('fcode' => $fcode))->find();//查询广告分类详情
		}
		if(!$adclassDetails) $adclassDetails = array('fcode'=>'','fadclass'=>'全部','ffullname'=>'全部');
		$this->ajaxReturn(array('code'=>0,'adclassList'=>$adclassList,'adclassDetails'=>$adclassDetails));

	}

}