<?php
namespace Open\Controller;
use Think\Controller;
use Think\Exception;

/**
* 国家/地区  广告监测中心大屏显示数据支持接口
* @媒介类型	 01-电视,02-广播,03-报纸 ,13-网站(互联网)
* @Return   国家广告监测大屏 https://datav.aliyun.com/share/ac843436065bbc705594b26f86e0ed5c
* @Return   淄博广告监测大屏 https://datav.aliyun.com/share/46ff848964bfb92a0dc22bbd5926841c
*/
class DatavController extends BaseController {
	private $netNum 	= 863;		//互联网媒介数量
	private $netCount 	= 228041;	//互联网广告总条次
	private $netIllegal = 0;		//互联网广告违法条次

	private $table = 'tbn_ad_summary_day_z';	//统计数据来源表名
	
	/**
	* 构造函数
	*/
	public function _initialize() {
		$_GET['fstarttime'] = '2019-01-01';
		header ( 'Access-Control-Allow-Origin:*' );
		$region_id = intval(I('get.region_id',100000));
		if ($region_id == 100000){//国家局
			$this->netNum = 863;
			$this->netCount = 228041;
		} elseif ($region_id == 370300){//淄博
			$this->netNum = 22;
			$this->netCount = 19114;
		} elseif ($region_id == 130100){//石家庄
			$this->netNum = 0;
			$this->netCount = 0;
		} elseif ($region_id > 0 && $region_id != 100000) {//查询某地区
			$this->netNum = 30;
			$this->netCount = 16356;
		}
	}
	
	/**
	 * 柱状图数据，用于大屏展示[采集广告总量分布]
	 * @Param	int region_id 区域ID
	 * @Return	array $data 返回的数据
	 */
	public function gather_public0() {
		$region_id = intval(I('get.region_id',100000));
		$return_data = array();
		$where = array ();
		// $where ['date'] = date ('Y-01-01', time ());
		if ($region_id > 0 && $region_id != 100000) {//查询某地区
			$region_id_rtrim = A ( 'Common/System', 'Model' )->get_region_left ( $region_id ); // 地区ID去掉末尾的0
			$region_id_strlen = strlen ( $region_id_rtrim ); // 去掉0后还有几位
			$where ['left(tbn_ad_summary_year.fregionid,' . $region_id_strlen . ')'] = $region_id_rtrim; // 地区搜索条件
		}
		$dataSet = M('tbn_ad_summary_year')
			->cache(true,1200)
			->field('left(fmedia_class_code,2) as mediaclass,
                    sum(fad_times) as issue_count,
                    sum(fad_illegal_times) as illegal_issue_count,
                    count(distinct fmediaid) as media_count')
			->where($where)
			->group('left(fmedia_class_code,2)')
			->select(); // 查询电视、广播、报纸本年的发布量、违法发布量
		foreach ( $dataSet as $row ) {
			if ($row ['mediaclass'] == '01') {                      // 发布量
				$retData [1] ['category'] = '电视';                 // 类别
				$retData [1] ['count'] = $row ['issue_count'];      // 发布量
			} elseif ($row ['mediaclass'] == '02') {
				$retData [2] ['category'] = '广播';
				$retData [2] ['count'] = $row ['issue_count'];
			} elseif ($row ['mediaclass'] == '03') {
				$retData [3] ['category'] = '报刊';
				$retData [3] ['count'] = $row ['issue_count'];
			}
        }
        if($region_id == 370300){//临时紧急修改静态数据 20180820
            $retData [3] ['count'] = "1216";
        }
		$retData [13] ['category'] = '互联网'; // 类别
		$retData [13] ['count'] = (string)$this->netCount; // 发布量
		
		$total = $retData [1] ['count'] + $retData [2] ['count'] + $retData [3] ['count'] + $retData[13]['count'];
		//百分比(不带%的数字部分)
		$retData [1] ['percentnum']  = sprintf ( "%.2f", $retData [1] ['count'] / $total * 100 );
		$retData [2] ['percentnum']  = sprintf ( "%.2f", $retData [2] ['count'] / $total * 100 );
		$retData [3] ['percentnum']  = sprintf ( "%.2f", $retData [3] ['count'] / $total * 100 );
		$retData [13] ['percentnum'] = sprintf ( "%.2f", $retData [13] ['count'] / $total * 100 );
		//百分比
		$retData [1] ['percent']  = sprintf ( "%.2f", $retData [1] ['count'] / $total * 100 ).'%';
		$retData [2] ['percent']  = sprintf ( "%.2f", $retData [2] ['count'] / $total * 100 ).'%';
		$retData [3] ['percent']  = sprintf ( "%.2f", $retData [3] ['count'] / $total * 100 ).'%';
		$retData [13] ['percent'] = sprintf ( "%.2f", $retData [13] ['count'] / $total * 100 ).'%';
		
		foreach ( $retData as $key=>$val ) {
			$key_arrays [$key] = $val ['count'];
		}
		array_multisort ( $key_arrays, SORT_DESC, SORT_NUMERIC, $retData );
		
		$this->ajaxReturn ( $retData );
	}
	
	/**
	 * 柱状图数据，用于大屏展示[采集广告总量分布]
	 * @Param	int region_id 区域ID
	 * @Return	array $data 返回的数据
	 */
	public function gather_public1() {
		$region_id = intval(I('get.region_id',100000));
		$return_data = array();
		$where = array ();
		$where ['date'] = date ('Y-01-01', time ());
		if ($region_id > 0 && $region_id != 100000) {//查询某地区
			$region_id_rtrim = A ( 'Common/System', 'Model' )->get_region_left ( $region_id ); // 地区ID去掉末尾的0
			$region_id_strlen = strlen ( $region_id_rtrim ); // 去掉0后还有几位
			$where['left(fregionid,' . $region_id_strlen . ')'] = $region_id_rtrim; // 地区搜索条件
		}
		$dataSet = M($this->table)
			->cache(true,10)
			->field('left(fmedia_class_code,2) as mediaclass,
                    sum(fad_times) as issue_count,
                    sum(fad_illegal_times) as illegal_issue_count,
                    count(distinct fmediaid) as media_count')
			->where($where)
			->group('left(fmedia_class_code,2)')
			->select(); // 查询电视、广播、报纸本年的发布量、违法发布量
		foreach ( $dataSet as $row ) {
			if ($row ['mediaclass'] == '01') {                      // 发布量
				$retData [1] ['category'] = '电视';                 // 类别
				$retData [1] ['count'] = $row ['issue_count'];      // 发布量
			} elseif ($row ['mediaclass'] == '02') {
				$retData [2] ['category'] = '广播';
				$retData [2] ['count'] = $row ['issue_count'];
			} elseif ($row ['mediaclass'] == '03') {
				$retData [3] ['category'] = '报刊';
				$retData [3] ['count'] = $row ['issue_count'];
			} elseif ($row ['mediaclass'] == '13') {
				$retData [13] ['category'] = '互联网';
				$retData [13] ['count'] = $row ['issue_count'];
			}
        }
		$retData [13] ['category'] = '互联网'; // 类别
		$retData [13] ['count'] = (string)$this->netCount; // 发布量

		$total = $retData [1] ['count'] + $retData [2] ['count'] + $retData [3] ['count'] + $retData[13]['count'];
		//百分比(不带%的数字部分)
		$retData [1] ['percentnum']  = sprintf ( "%.2f", $retData [1] ['count'] / $total * 100 );
		$retData [2] ['percentnum']  = sprintf ( "%.2f", $retData [2] ['count'] / $total * 100 );
		$retData [3] ['percentnum']  = sprintf ( "%.2f", $retData [3] ['count'] / $total * 100 );
		$retData [13] ['percentnum'] = sprintf ( "%.2f", $retData [13] ['count'] / $total * 100 );
		//百分比
		$retData [1] ['percent']  = sprintf ( "%.2f", $retData [1] ['count'] / $total * 100 ).'%';
		$retData [2] ['percent']  = sprintf ( "%.2f", $retData [2] ['count'] / $total * 100 ).'%';
		$retData [3] ['percent']  = sprintf ( "%.2f", $retData [3] ['count'] / $total * 100 ).'%';
		$retData [13] ['percent'] = sprintf ( "%.2f", $retData [13] ['count'] / $total * 100 ).'%';
		
		foreach ( $retData as $key=>$val ) {
			$key_arrays [$key] = $val ['count'];
		}
		array_multisort ( $key_arrays, SORT_DESC, SORT_NUMERIC, $retData );
		
		$this->ajaxReturn ( $retData );
	}
	
	/**
	 * 柱状图数据，用于大屏展示[采集广告总量分布]
	 * @Param	int region_id 区域ID
	 * @Return	array $data 返回的数据
	 */
	public function gather_public() {
		$region_id = intval(I('get.region_id',100000));
        $fregulatorcode = intval('20'.$region_id);
		$fstarttime = I('fstarttime',date('Y-01-01',strtotime('-1 year')));
		$fendtime = I('fendtime',date('Y-12-31',strtotime('-1 year')));
		$where['fdate'] = ['BETWEEN',[$fstarttime,$fendtime]];
		if ($region_id > 0 && $region_id != 100000) {//查询某地区
            $tregulatormedia = M('tregulatormedia') //媒介权限过滤
                ->cache(true,120)
                ->where(['fregulatorcode'=>$fregulatorcode])
				->getField('fmediaid',true);
			if($tregulatormedia){
				//机构已授权媒体，只取授权媒体
				$where['fmediaid'] = ['IN',implode(',',$tregulatormedia)];
			}else{
				//机构未授权媒体，则取地域媒体
				$region_id_rtrim = A ( 'Common/System', 'Model' )->get_region_left ( $region_id ); // 地区ID去掉末尾的0
				$region_id_strlen = strlen ( $region_id_rtrim ); // 去掉0后还有几位
				$where['left(fregionid,'.$region_id_strlen.')'] = $region_id_rtrim; // 地区搜索条件
			}
		}
		$dataSet = M($this->table)
			->cache(true,10)
			->field('
				left(fmedia_class_code,2) as mediaclass,
				sum(fad_times) as issue_count,
				sum(fad_illegal_times) as illegal_issue_count,
				count(distinct fmediaid) as media_count')
			->where($where)
			->group('left(fmedia_class_code,2)')
			->select(); // 查询电视、广播、报纸本年的发布量、违法发布量
		foreach ( $dataSet as $row ) {
			if ($row ['mediaclass'] == '01') {
				$retData [1] ['category'] = '电视';
				$retData [1] ['count'] = $row ['issue_count'];
			} elseif ($row ['mediaclass'] == '02') {
				$retData [2] ['category'] = '广播';
				$retData [2] ['count'] = $row ['issue_count'];
			} elseif ($row ['mediaclass'] == '03') {
				$retData [3] ['category'] = '报刊';
				$retData [3] ['count'] = $row ['issue_count'];
			} elseif ($row ['mediaclass'] == '13') {
				$retData [13] ['category'] = '互联网';
				$retData [13] ['count'] = $row ['issue_count'];
			}
        }
		$retData [13] ['category'] = '互联网'; // 类别
		$retData [13] ['count'] = (string)$this->netCount; // 发布量

		$total = $retData [1] ['count'] + $retData [2] ['count'] + $retData [3] ['count'] + $retData[13]['count'];
		//百分比(不带%的数字部分)
		$retData [1] ['percentnum']  = sprintf ( "%.2f", $retData [1] ['count'] / $total * 100 );
		$retData [2] ['percentnum']  = sprintf ( "%.2f", $retData [2] ['count'] / $total * 100 );
		$retData [3] ['percentnum']  = sprintf ( "%.2f", $retData [3] ['count'] / $total * 100 );
		$retData [13] ['percentnum'] = sprintf ( "%.2f", $retData [13] ['count'] / $total * 100 );
		//百分比
		$retData [1] ['percent']  = sprintf ( "%.2f", $retData [1] ['count'] / $total * 100 ).'%';
		$retData [2] ['percent']  = sprintf ( "%.2f", $retData [2] ['count'] / $total * 100 ).'%';
		$retData [3] ['percent']  = sprintf ( "%.2f", $retData [3] ['count'] / $total * 100 ).'%';
		$retData [13] ['percent'] = sprintf ( "%.2f", $retData [13] ['count'] / $total * 100 ).'%';
		
		foreach ( $retData as $key=>$val ) {
			$key_arrays [$key] = $val ['count'];
		}
		array_multisort ( $key_arrays, SORT_DESC, SORT_NUMERIC, $retData );
		
		$this->ajaxReturn ( $retData );
	}
	
	/**
	* 获取各媒介数量，用于大屏展示[监测采集范围]
	* @Param	int region_id 区域ID
	* @Param	int media_class 媒介类型	1-电视,2-广播,3-报纸 ,13-互联网
	* @Return	array $data 返回的数据
	*/
	public function gather_media_count0() {
        $region_id = intval(I('get.region_id',100000));
        $fregulatorcode = intval('20'.$region_id);  //监管机构ID
		$media_class = intval(I('get.media_class',1));
        $where['_string'] = '1=1';
		$mediaClassArr = array(
			'1' => '01',
			'2' => '02',
			'3' => '03',
			'13' => '13'
		);
		if ($region_id > 0 && $region_id != 100000) {//查询某地区
			$region_id_rtrim = A ( 'Common/System', 'Model' )->get_region_left ( $region_id ); // 地区ID去掉末尾的0
			$region_id_strlen = strlen ( $region_id_rtrim ); // 去掉0后还有几位
            $where ['left(tmediaowner.fregionid,' . $region_id_strlen . ')'] = $region_id_rtrim; // 地区搜索条件
            $tregulatormedia = M('tregulatormedia') //媒介权限过滤
                ->cache(true,120)
                ->where(array('fregulatorcode'=>$fregulatorcode))
                ->getField('fmediaid',true);
            $where['tmedia.fid'] = array('in',implode(',',$tregulatormedia));
		}elseif($region_id == 100000){//国家局媒介数量通过标签过滤
			$where['tmedialabel.flabelid'] = array('in','243,244,245');
			$join_label = 'tmedialabel ON tmedialabel.fmediaid = tmedia.fid';
		}
		$where['left(tmedia.fmediaclassid,2)'] = $mediaClassArr[$media_class];
        $where['tmedia.fstate'] = array('EQ',1);
        $where['_string'] .= ' and tmedia.fid=tmedia.main_media_id';
		$issueData = M('tmedia')
			->cache(true,1200)
			->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')
			->join($join_label)
			->where($where)
			->count();
		//互联网媒体(临时处理)
		if ($media_class == 13){
			$issueData = $this->netNum;
		}
		$data[0]['media_count'] = (string)$issueData;
		$this->ajaxReturn($data);
	}
	
	/**
	* 获取各媒介数量，用于大屏展示[监测采集范围]
	* @Param	int region_id 区域ID
	* @Param	int media_class 媒介类型	1-电视,2-广播,3-报纸 ,13-互联网
	* @Return	array $data 返回的数据
	*/
	public function gather_media_count() {
        $region_id = intval(I('get.region_id',100000));
        $fregulatorcode = intval('20'.$region_id);  //监管机构ID
		$media_class = intval(I('get.media_class',1));
        $where['_string'] = '1=1';
		$mediaClassArr = array(
			'1' => '01',
			'2' => '02',
			'3' => '03',
			'13' => '13'
		);
		if ($region_id > 0 && $region_id != 100000) {//查询某地区
			$region_id_rtrim = A ( 'Common/System', 'Model' )->get_region_left ( $region_id ); // 地区ID去掉末尾的0
			$region_id_strlen = strlen ( $region_id_rtrim ); // 去掉0后还有几位
            $where ['left(tmediaowner.fregionid,' . $region_id_strlen . ')'] = $region_id_rtrim; // 地区搜索条件
            $tregulatormedia = M('tregulatormedia') //媒介权限过滤
                ->cache(true,120)
                ->where(['fregulatorcode'=>$fregulatorcode])
                ->getField('fmediaid',true);
            $where['tmedia.fid'] = array('in',implode(',',$tregulatormedia));
		}elseif($region_id == 100000){//国家局媒介数量通过标签过滤
			$where['tmedialabel.flabelid'] = array('in','243,244,245');
			$join_label = 'tmedialabel ON tmedialabel.fmediaid = tmedia.fid';
		}
		$where['left(tmedia.fmediaclassid,2)'] = $mediaClassArr[$media_class];
        $where['tmedia.fstate'] = array('EQ',1);
        $where['_string'] .= ' and tmedia.fid=tmedia.main_media_id';
		$issueData = M('tmedia')
			->cache(true,10)
			->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')
			->join($join_label)
			->where($where)
			->count();
		//互联网媒体(临时处理)
		if ($media_class == 13){
			if(in_array($region_id,[100000,370300,130100])){
				$issueData = $this->netNum;
			}
		}
		$data[0]['media_count'] = (string)$issueData;
		$this->ajaxReturn($data);
	}
	
	/**
	* 获取近30天违法数、立案数(暂未提供)，用于大屏展示[采集趋势]
	* @Param	int $region_id 区域ID
	* @Param	int $days 周期天数
	* @Return	array $data 返回的数据
	*/
	public function gather_public_trend0(){
		$regionId = intval(I('get.region_id',100000));
		$Days = intval(I('days',30));
		$beforDays = $Days - 1;
		$sDate = date('Y-m-d',strtotime('-'.$beforDays.' day'));
		$eDate = date('Y-m-d',time());
		$where = array();
		if($regionId > 0 && $regionId != 100000){
			$region_id_rtrim = A('Common/System','Model')->get_region_left($regionId);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		}
		$where['issue_date'] = array('between',array($sDate,$eDate));
		$DataSet = M('media_issue_count')->cache(true,1200)
					->field('issue_date as x,
							SUM(illegal_20_count)+SUM(illegal_30_count) as y,
							SUM(illegal_20_count) as y20,
							SUM(illegal_30_count) as y30')
					->where($where)
					->group('issue_date')
					->select();
		$data = array();
		$data1 = array();
		$data2 = array();
		$cDate = $sDate;
		//无数据的日期补全0
		for ($i=0; $i<$Days; $i++){
			$cDate = date('Y-m-d',strtotime($sDate.'+'.$i.' day'));
			if ($cDate == $DataSet[$i]['x']){
				//涉嫌违规违法数
				$data1[$i]['x'] = $DataSet[$i]['x'];
				$data1[$i]['y'] = (int)$DataSet[$i]['y'];
				$data1[$i]['s'] = 1;
				//TODO:立案数目前提供0
				$data2[$i]['x'] = $DataSet[$i]['x'];
				$data2[$i]['y'] = 0;
				$data2[$i]['s'] = 2;
			}else{
				//涉嫌违规违法数
				$data1[$i]['x'] = $cDate;
				$data1[$i]['y'] = 0;
				$data1[$i]['s'] = 1;
				//TODO:立案数目前提供0
				$data2[$i]['x'] = $cDate;
				$data2[$i]['y'] = 0;
				$data2[$i]['s'] = 2;
			}
		}
		$data = array_merge($data1,$data2);
		$this->ajaxReturn($data);
	}
	
	/**
	* 获取近30天违法数、立案数(暂未提供)，用于大屏展示[采集趋势]
	* @Param	int $region_id 区域ID
	* @Param	int $days 周期天数
	* @Return	array $data 返回的数据
	*/
	public function gather_public_trend1(){
		$regionId = intval(I('get.region_id',100000));
		$Days = intval(I('days',30));
		$beforDays = $Days - 1;
		$sDate = date('Y-m-d',strtotime('-'.$beforDays.' day'));
		$eDate = date('Y-m-d',time());
		$where = array();
		if($regionId > 0 && $regionId != 100000){
			$region_id_rtrim = A('Common/System','Model')->get_region_left($regionId);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		}
		$where['issue_date'] = array('between',array($sDate,$eDate));
		$DataSet = M('media_issue_count')->cache(true,1200)
					->field('issue_date as x,
							SUM(illegal_20_count)+SUM(illegal_30_count) as y,
							SUM(illegal_20_count) as y20,
							SUM(illegal_30_count) as y30')
					->where($where)
					->group('issue_date')
					->select();
		$data = array();
		$data1 = array();
		$data2 = array();
		$cDate = $sDate;
		//无数据的日期补全0
		for ($i=0; $i<$Days; $i++){
			$cDate = date('Y-m-d',strtotime($sDate.'+'.$i.' day'));
			if ($cDate == $DataSet[$i]['x']){
				//涉嫌违规违法数
				$data1[$i]['x'] = $DataSet[$i]['x'];
				$data1[$i]['y'] = (int)$DataSet[$i]['y'];
				$data1[$i]['s'] = 1;
				//TODO:立案数目前提供0
				$data2[$i]['x'] = $DataSet[$i]['x'];
				$data2[$i]['y'] = 0;
				$data2[$i]['s'] = 2;
			}else{
				//涉嫌违规违法数
				$data1[$i]['x'] = $cDate;
				$data1[$i]['y'] = 0;
				$data1[$i]['s'] = 1;
				//TODO:立案数目前提供0
				$data2[$i]['x'] = $cDate;
				$data2[$i]['y'] = 0;
				$data2[$i]['s'] = 2;
			}
		}
		$data = array_merge($data1,$data2);
		$this->ajaxReturn($data);
	}
	
	/**
	* 获取近30天违法数、立案数(暂未提供)，用于大屏展示[采集趋势]
	* @Param	int $region_id 区域ID
	* @Param	int $days 周期天数
	* @Return	array $data 返回的数据
	*/
	public function gather_public_trend(){
		$region_id = intval(I('get.region_id',100000));
		$Days = intval(I('days',30));
		$beforDays = $Days - 1;
		$sDate = date('Y-m-d',strtotime('-'.$beforDays.' day'));
		$eDate = date('Y-m-d',time());
		$where = [];
		if ($region_id > 0 && $region_id != 100000) {//查询某地区
			if($region_id == 370300){
				//TODO:淄博AGP特殊处理，待完善 2019-01-02
				$join_tmedia_temp = 'tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid ';
				$where['tmedia_temp.ftype'] = 1 ;
				$where['tmedia_temp.fuserid'] = 154 ;
				$where['tbn_illegal_ad.fcustomer'] = $region_id;
			}else{
				$tregulatormedia = M('tregulatormedia') //媒介权限过滤
					->cache(true,120)
					->where(['fregulatorcode'=>$fregulatorcode])
					->getField('fmediaid',true);
				if($tregulatormedia){
					//机构已授权媒体，只取授权媒体
					$where['tbn_illegal_ad_issue.fmedia_id'] = ['IN',implode(',',$tregulatormedia)];
				}else{
					//机构未授权媒体，则取地域媒体
					$region_id_rtrim = A ( 'Common/System', 'Model' )->get_region_left ( $region_id ); // 地区ID去掉末尾的0
					$region_id_strlen = strlen ( $region_id_rtrim ); // 去掉0后还有几位
					$where['left(tbn_illegal_ad.fregion_id,'.$region_id_strlen.')'] = $region_id_rtrim; // 地区搜索条件
				}
			}
		}
		$where['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',array($sDate,$eDate)];
		$DataSet = M('tbn_illegal_ad_issue')
			->field('tbn_illegal_ad_issue.fissue_date as x,
					count(1) as y')
			->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
			->join('tmedia ON tmedia.fid = tbn_illegal_ad_issue.fmedia_id')
			->join($join_tmedia_temp)
			->where($where)
			->group('fissue_date')
			->select();
		$data = [];
		$data1 = [];
		$data2 = [];
		$cDate = $sDate;
		//无数据的日期补全0
		for ($i=0; $i<$Days; $i++){
			$cDate = date('Y-m-d',strtotime($sDate.'+'.$i.' day'));
			if ($cDate == $DataSet[$i]['x']){
				//涉嫌违规违法数
				$data1[$i]['x'] = $DataSet[$i]['x'];
				$data1[$i]['y'] = (int)$DataSet[$i]['y'];
				$data1[$i]['s'] = 1;
				//TODO:立案数目前提供0
				$data2[$i]['x'] = $DataSet[$i]['x'];
				$data2[$i]['y'] = 0;
				$data2[$i]['s'] = 2;
			}else{
				//涉嫌违规违法数
				$data1[$i]['x'] = $cDate;
				$data1[$i]['y'] = 0;
				$data1[$i]['s'] = 1;
				//TODO:立案数目前提供0
				$data2[$i]['x'] = $cDate;
				$data2[$i]['y'] = 0;
				$data2[$i]['s'] = 2;
			}
		}
		$data = array_merge($data1,$data2);
		$this->ajaxReturn($data);
	}
	
	/**
	 * 获取广告按类别分布条次(发布次数)，用于大屏展示[类别分布]
	 * @Param	int $region_id 区域ID
	 * @Return	array $data 返回的数据
	 */
	public function gather_public_category0(){
		$regionId = intval(I('get.region_id',100000));
		$where = array();
		if($regionId > 0 && $regionId != 100000){
			$region_id_rtrim = A('Common/System','Model')->get_region_left($regionId);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(tbn_ad_summary_year.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		}
		$M = M('tbn_ad_summary_year')->cache(true,1200);
		$whereA['LEFT(fmedia_class_code,2)'] = array('EQ','13');//互联网
		$dataA = $M
			->field('SUM(fad_times) AS value')
			->where($where)
			->where($whereA)
			->find();
		$whereB['LEFT(fmedia_class_code,2)'] = array('NEQ','13');//传统广告(非互联网)
		$dataB = $M
			->field('SUM(fad_times) AS value')
			->where($where)
			->where($whereB)
			->find();
		$data[0]['type'] = '互联网广告';
		$data[0]['value'] = $this->netCount;
 		//$data[0]['value'] = (int)$dataA['value'];
		//$data[0]['value'] = (float)sprintf('%.2f',(100*$dataA)/($dataA+$dataB));//百分比
		$data[1]['type'] = '传统广告';
		$data[1]['value'] = (int)$dataB['value'];
		//$data[1]['value'] = (float)sprintf('%.2f',(100*$dataB)/($dataA+$dataB));
		$this->ajaxReturn($data);
	}
	
	/**
	 * 获取广告按类别分布条次(发布次数)，用于大屏展示[类别分布]
	 * @Param	int $region_id 区域ID
	 * @Return	array $data 返回的数据
	 */
	public function gather_public_category(){
		$regionId = intval(I('get.region_id',100000));
		$where = array();
		if($regionId > 0 && $regionId != 100000){
			$region_id_rtrim = A('Common/System','Model')->get_region_left($regionId);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		}
		$M = M($this->table)->cache(true,10);
		$whereA['LEFT(fmedia_class_code,2)'] = array('EQ','13');//互联网
		$dataA = $M
			->field('SUM(fad_times) AS value')
			->where($where)
			->where($whereA)
			->find();
		$whereB['LEFT(fmedia_class_code,2)'] = array('NEQ','13');//传统广告(非互联网)
		$dataB = $M
			->field('SUM(fad_times) AS value')
			->where($where)
			->where($whereB)
			->find();
		$data[0]['type'] = '互联网广告';
		$data[0]['value'] = $this->netCount;
 		// $data[0]['value'] = (int)$dataA['value'];
		//$data[0]['value'] = (float)sprintf('%.2f',(100*$dataA)/($dataA+$dataB));//百分比
		$data[1]['type'] = '传统广告';
		$data[1]['value'] = (int)$dataB['value'];
		//$data[1]['value'] = (float)sprintf('%.2f',(100*$dataB)/($dataA+$dataB));
		$this->ajaxReturn($data);
	}
	
	/**
	 * 获取广告按行业分布条次(发布次数)，用于大屏展示[行业分布]
	 * @Param	int $region_id 区域ID
	 * @Return	array $data 返回的数据
	 */
	public function gather_public_tmt0(){
		$regionId = intval(I('get.region_id',100000));
		$field = 'fregionid,tadclass.fadclass as x, SUM(fad_times) as times';
		$where = array();
		if($regionId > 0 && $regionId != 100000){
			$region_id_rtrim = A('Common/System','Model')->get_region_left($regionId);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(tbn_ad_summary_year.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		}
		$data = array();
		$sumY = 0;
		$dataSet = M('tbn_ad_summary_year')->cache(true,1200)
			->field($field)
			->join('tadclass ON tbn_ad_summary_year.fad_class_code=tadclass.fcode','LEFT')
			->where($where)
			->group('fad_class_code')
			->order('times DESC')
			->select();
		foreach ($dataSet as $key=>$val){
			if (count($data) > 4 || $val['x'] == '其它类'){
				$sumY += intval($val['times']);
			}else{
				array_push($data, array('x'=>$val['x'],'y'=>intval($val['times'])));
			}
		}
		array_push($data, array('x'=>'其它','y'=>$sumY));
		$this->ajaxReturn($data);
	}
	
	/**
	 * 获取广告按行业分布条次(发布次数)，用于大屏展示[行业分布]
	 * @Param	int $region_id 区域ID
	 * @Return	array $data 返回的数据
	 */
	public function gather_public_tmt1(){
		$regionId = intval(I('get.region_id',100000));
		$field = 'fregionid,tadclass.fadclass as x, SUM(fad_times) as times';
		$where = array();
		if($regionId > 0 && $regionId != 100000){
			$region_id_rtrim = A('Common/System','Model')->get_region_left($regionId);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		}
		$data = array();
		$sumY = 0;
		$dataSet = M($this->table)->cache(true,1200)
			->field($field)
			->join('tadclass ON fad_class_code=tadclass.fcode','LEFT')
			->where($where)
			->group('fad_class_code')
			->order('times DESC')
			->select();
		foreach ($dataSet as $key=>$val){
			if (count($data) > 4 || $val['x'] == '其它类'){
				$sumY += intval($val['times']);
			}else{
				array_push($data, array('x'=>$val['x'],'y'=>intval($val['times'])));
			}
		}
		array_push($data, array('x'=>'其它','y'=>$sumY));
		$this->ajaxReturn($data);
	}
	
	/**
	 * 获取广告按行业分布条次(发布次数)，用于大屏展示[行业分布]
	 * @Param	int $region_id 区域ID
	 * @Return	array $data 返回的数据
	 */
	public function gather_public_tmt(){
		$region_id = intval(I('get.region_id',100000));
        $fregulatorcode = intval('20'.$region_id);
		$fstarttime = I('fstarttime',date('Y-01-01',strtotime('-1 year')));
		$fendtime = I('fendtime',date('Y-12-31',strtotime('-1 year')));
		$where['tbn_ad_summary_day_z.fdate'] = ['BETWEEN',[$fstarttime,$fendtime]];
		if ($region_id > 0 && $region_id != 100000) {//查询某地区
			if($region_id == 370300){
				//TODO:淄博AGP特殊处理，待完善 2019-01-02
				$join_tmedia_temp = 'tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid ';
				$where['tmedia_temp.ftype'] = 1 ;
				$where['tmedia_temp.fuserid'] = 154 ;
			}elseif($region_id == 130100){
				//TODO:石家庄AGP特殊处理，待完善 2019-08-23
				$join_tmedia_temp = 'tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid ';
				$where['tmedia_temp.ftype'] = 1 ;
				$where['tmedia_temp.fuserid'] = ['IN',[826,1213]];
			}else{
				$tregulatormedia = M('tregulatormedia') //媒介权限过滤
					->cache(true,120)
					->where(['fregulatorcode'=>$fregulatorcode])
					->getField('fmediaid',true);
				if($tregulatormedia){
					//机构已授权媒体，只取授权媒体
					$where['tbn_ad_summary_day_z.fmediaid'] = ['IN',implode(',',$tregulatormedia)];
				}else{
					//机构未授权媒体，则取地域媒体
					$region_id_rtrim = A ( 'Common/System', 'Model' )->get_region_left ( $region_id ); // 地区ID去掉末尾的0
					$region_id_strlen = strlen ( $region_id_rtrim ); // 去掉0后还有几位
					$where['left(tbn_ad_summary_day_z.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim; // 地区搜索条件
				}
			}
		}
		$data = [];
		$sumY = 0;
		$dataSet = M('tbn_ad_summary_day_z')
			->field('tadclass.fadclass AS x,count(1) AS counts,SUM(fad_times) AS times ')
			->join('tadclass ON tadclass.fcode = LEFT ( tbn_ad_summary_day_z.fad_class_code, 2 )')
			->join('tmedia ON tmedia.fid = tbn_ad_summary_day_z.fmediaid')
			->join($join_tmedia_temp)
			->where($where)
			->group('LEFT ( tbn_ad_summary_day_z.fad_class_code, 2 ) ')
			->order('times DESC')
			->select();
		foreach ($dataSet as $key=>$val){
			if (count($data) > 4 || $val['x'] == '其它类'){
				$sumY += intval($val['times']);
			}else{
				array_push($data, array('x'=>$val['x'],'y'=>intval($val['times'])));
			}
		}
		array_push($data, array('x'=>'其它','y'=>$sumY));
		$this->ajaxReturn($data);
	}
	
	/**
	* 获取按地区违法广告条数/次数,用于大屏展示[涉嫌违法违规地区分布]
	* @Param	int $region_id 区域ID
	* @Param	int $type 要返回的维度,条数或次数 'count'-仅条数 'times'-仅次数 'all'-条数和次数,默认'count'-仅条数
	* @Param	int $top 返回记录Top条数
	* @Return	array $data 返回的数据
	*/
	public function gather_public_illegal_region0(){
		$regionId = intval(I('get.region_id',100000));
		$lRegionId = substr($regionId, 0,2);
		$mRegionId = substr($regionId, 2,2);
		$rRegionId = substr($regionId, 4,2);
		$type = strval(I('get.type','times'));
		$top = intval(I('get.top',11));
		$field = 'fregionid,tregion.fname,SUM(fad_illegal_count) as count,SUM(fad_illegal_times) as times';
		$where['fregionid'] = $regionId;
		$join = 'tregion on tbn_ad_summary_year.fregionid = tregion.fid';
		
		$dataSet = M('tbn_ad_summary_year')->cache(true,1200)
					->field($field)
					->join($join)
					->where($where)
					->group('fregionid')
					->select();
		if ($rRegionId > 0){
			//区(县)级
		}elseif ($mRegionId > 0){
			//地市级
			$strLike = $lRegionId.$mRegionId;
			$dataSubordinate = M('tbn_ad_summary_year')->cache(true,1200)
								->field($field)
								->join($join)
								->where('tbn_ad_summary_year.fregionid <> '.$regionId.' AND tbn_ad_summary_year.fregionid LIKE "'.$strLike.'%"')
								->group('fregionid')
								->select();
			foreach ($dataSubordinate as $val){
				array_push($dataSet, $val);
			}
		}elseif ($lRegionId != '10'){
			//省级
			$strLike = $lRegionId;
			$dataSubordinate = M('tbn_ad_summary_year')->cache(true,1200)
								->field($field)
								->join($join)
								->where('tbn_ad_summary_year.fregionid <> '.$regionId.' AND tbn_ad_summary_year.fregionid LIKE "'.$strLike.'%"')
								->group('fregionid')
								->select();
			foreach ($dataSubordinate as $val){
				array_push($dataSet, $val);
			}
		}elseif ($regionId == '100000'){
			//国家(中央)级
			$dataSubordinate = M('tbn_ad_summary_year')->cache(true,1200)
								->field($field)
								->join($join)
								->where('tbn_ad_summary_year.fregionid <> '.$regionId.' AND RIGHT(tbn_ad_summary_year.fregionid,4) = "0000"')
								->group('fregionid')
								->select();
			foreach ($dataSubordinate as $val){
				array_push($dataSet, $val);
			}
		}
		$data = array();
		foreach ($dataSet as $k=>$v){
			$data[$k]['x'] = $v['fname'];
			if ($type == 'count' || $type == 'times'){
				$data[$k]['y'] = $v[$type];
			}else{
				$data[$k]['y'] = $v['count'];
				$data[$k]['z'] = $v['times'];
			}
		}
		usort($data, array($this,'cmp'));
		array_splice($data, $top);
		$this->ajaxReturn($data);
	}
	
	/**
	* 获取按地区违法广告条数/次数,用于大屏展示[涉嫌违法违规地区分布]
	* @Param	int $region_id 区域ID
	* @Param	int $type 要返回的维度,条数或次数 'count'-仅条数 'times'-仅次数 'all'-条数和次数,默认'count'-仅条数
	* @Param	int $top 返回记录Top条数
	* @Return	array $data 返回的数据
	*/
	public function gather_public_illegal_region1(){
		$regionId = intval(I('get.region_id',100000));
		$lRegionId = substr($regionId, 0,2);
		$mRegionId = substr($regionId, 2,2);
		$rRegionId = substr($regionId, 4,2);
		$type = strval(I('get.type','times'));
		$top = intval(I('get.top',11));
		$field = 'fregionid,tregion.fname,SUM(fad_illegal_times) as times';
		$where['fregionid'] = $regionId;
		$join = 'tregion on tbn_ad_summary_day_z.fregionid = tregion.fid';
		
		$dataSet = M('tbn_ad_summary_day_z')->cache(true,10)
			->field($field)
			->join($join)
			->where($where)
			->group('fregionid')
			->select();
		if ($rRegionId > 0){
			//区(县)级
		}elseif ($mRegionId > 0){
			//地市级
			$strLike = $lRegionId.$mRegionId;
			$dataSubordinate = M('tbn_ad_summary_day_z')->cache(true,1200)
				->field($field)
				->join($join)
				->where('tbn_ad_summary_day_z.fregionid <> '.$regionId.' AND tbn_ad_summary_day_z.fregionid LIKE "'.$strLike.'%"')
				->group('fregionid')
				->select();
			foreach ($dataSubordinate as $val){
				array_push($dataSet, $val);
			}
		}elseif ($lRegionId != '10'){
			//省级
			$strLike = $lRegionId;
			$dataSubordinate = M('tbn_ad_summary_day_z')->cache(true,1200)
				->field($field)
				->join($join)
				->where('tbn_ad_summary_day_z.fregionid <> '.$regionId.' AND tbn_ad_summary_day_z.fregionid LIKE "'.$strLike.'%"')
				->group('fregionid')
				->select();
			foreach ($dataSubordinate as $val){
				array_push($dataSet, $val);
			}
		}elseif ($regionId == '100000'){
			//国家(中央)级
			$dataSubordinate = M('tbn_ad_summary_day_z')->cache(true,1200)
				->field($field)
				->join($join)
				->where('tbn_ad_summary_day_z.fregionid <> '.$regionId.' AND RIGHT(tbn_ad_summary_day_z.fregionid,4) = "0000"')
				->group('fregionid')
				->select();
			foreach ($dataSubordinate as $val){
				array_push($dataSet, $val);
			}
		}
		$data = array();
		foreach ($dataSet as $k=>$v){
			$data[$k]['x'] = $v['fname'];
			if ($type == 'count' || $type == 'times'){
				$data[$k]['y'] = $v[$type];
			}else{
				$data[$k]['y'] = $v['count'];
				$data[$k]['z'] = $v['times'];
			}
		}
		usort($data, array($this,'cmp'));
		array_splice($data, $top);
		$this->ajaxReturn($data);
	}
	
	/**
	* 获取按地区违法广告条数/次数,用于大屏展示[涉嫌违法违规地区分布]
	* @Param	int $region_id 区域ID
	* @Param	int $type 要返回的维度,条数或次数 'count'-仅条数 'times'-仅次数 'all'-条数和次数,默认'count'-仅条数
	* @Param	int $top 返回记录Top条数
	* @Return	array $data 返回的数据
	*/
	public function gather_public_illegal_region(){
		$region_id = intval(I('get.region_id',100000));
		$type = strval(I('get.type','times'));
		$top = intval(I('get.top',10));
        $fregulatorcode = intval('20'.$region_id);
		$fstarttime = I('fstarttime',date('Y-01-01'));
		$fendtime = I('fendtime',date('Y-12-31'));
		$where['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$fstarttime,$fendtime]];
		if ($region_id > 0 && $region_id != 100000) {//查询某地区
			if($region_id == 370300){
				//TODO:淄博AGP特殊处理，待完善 2019-01-02
				$join_tmedia_temp = 'tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid ';
				$where['tmedia_temp.ftype'] = 1 ;
				$where['tmedia_temp.fuserid'] = 154 ;
				$where['tbn_illegal_ad.fcustomer'] = $region_id;
				$where['tbn_illegal_ad.fexamine'] = 10;
			}elseif($region_id == 130100){
				//TODO:石家庄AGP特殊处理，待完善 2019-08-23
				$join_tmedia_temp = 'tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid ';
				$where['tmedia_temp.ftype'] = 1 ;
				$where['tmedia_temp.fuserid'] = ['IN',[826,1213]] ;
				$where['tbn_illegal_ad.fcustomer'] = $region_id;
			}else{
				$tregulatormedia = M('tregulatormedia') //媒介权限过滤
					->cache(true,120)
					->where(['fregulatorcode'=>$fregulatorcode])
					->getField('fmediaid',true);
				if($tregulatormedia){
					//机构已授权媒体，只取授权媒体
					$where['tbn_illegal_ad_issue.fmedia_id'] = ['IN',implode(',',$tregulatormedia)];
				}else{
					//机构未授权媒体，则取地域媒体
					$region_id_rtrim = A ( 'Common/System', 'Model' )->get_region_left ( $region_id ); // 地区ID去掉末尾的0
					$region_id_strlen = strlen ( $region_id_rtrim ); // 去掉0后还有几位
					$where['left(tbn_illegal_ad.fregion_id,'.$region_id_strlen.')'] = $region_id_rtrim; // 地区搜索条件
				}
			}
		}
		$dataSet = M('tbn_illegal_ad_issue')
			->field('tregion.fname,COUNT(1) AS times ')
			->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
			->join('tregion ON tregion.fid = tbn_illegal_ad.fregion_id')
			->join('tmedia ON tmedia.fid = tbn_illegal_ad_issue.fmedia_id')
			->join($join_tmedia_temp)
			->where($where)
			->group('tbn_illegal_ad.fregion_id ')
			->order($type.' DESC')
			->limit($top)
			->select();
		$sql = M('tbn_illegal_ad_issue')->getLastSql();
		$data = array();
		foreach ($dataSet as $k=>$v){
			$data[$k]['x'] = $v['fname'];
			if ($type == 'count' || $type == 'times'){
				$data[$k]['y'] = $v[$type];
			}else{
				$data[$k]['y'] = $v['count'];
				$data[$k]['z'] = $v['times'];
			}
		}
		$this->ajaxReturn($data);
	}
	
	/**
	* 获取按地区按行业违法广告条数/次数,用于大屏展示[涉嫌违法违规行业分布]
	* @Param	int $region_id 区域ID
	* @Param	int $type 要返回的维度,条数或次数 'count'-仅条数 'times'-仅次数 'all'-条数和次数,默认'times'-仅次数
	* @Param	int $top 返回记录Top条数
	* @Return	array $data 返回的数据
	*/
	public function gather_public_illegal_adclass0(){
		$regionId = intval(I('get.region_id',100000));
		$type = strval(I('get.type','times'));
		$top = intval(I('get.top',5));
		$field = 'fregionid,tadclass.fadclass,SUM(fad_illegal_count) as count,SUM(fad_illegal_times) as times';
		$join = 'tadclass on tbn_ad_summary_year.fad_class_code = tadclass.fcode';
		$where = array();
		if($regionId > 0 && $regionId != 100000){
			$region_id_rtrim = A('Common/System','Model')->get_region_left($regionId);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(tbn_ad_summary_year.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		}
		$dataSet = M('tbn_ad_summary_year')->cache(true,1200)
					->field($field)
					->join($join)
					->where($where)
					->group('fad_class_code')
					->order(''.$type.' DESC')
					->limit($top)
					->select();
		$data = array();
		foreach ($dataSet as $k=>$v){
			$data[$k]['x'] = $v['fadclass'];
			if ($type == 'count' || $type == 'times'){
				$data[$k]['y'] = $v[$type];
			}else{
				$data[$k]['y'] = $v['count'];
				$data[$k]['z'] = $v['times'];
			}
		}
		$this->ajaxReturn($data);
	}
	
	/**
	* 获取按地区按行业违法广告条数/次数,用于大屏展示[涉嫌违法违规行业分布]
	* @Param	int $region_id 区域ID
	* @Param	int $type 要返回的维度,条数或次数 'count'-仅条数 'times'-仅次数 'all'-条数和次数,默认'times'-仅次数
	* @Param	int $top 返回记录Top条数
	* @Return	array $data 返回的数据
	*/
	public function gather_public_illegal_adclass1(){
		$regionId = intval(I('get.region_id',100000));
		$type = strval(I('get.type','times'));
		$top = intval(I('get.top',5));
		$field = 'fregionid,tadclass.fadclass,SUM(fad_illegal_times) as times';
		$join = 'tadclass on tbn_ad_summary_day_z.fad_class_code = tadclass.fcode';
		$where = array();
		if($regionId > 0 && $regionId != 100000){
			$region_id_rtrim = A('Common/System','Model')->get_region_left($regionId);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(tbn_ad_summary_day_z.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		}
		$dataSet = M('tbn_ad_summary_day_z')->cache(true,10)
					->field($field)
					->join($join)
					->where($where)
					->group('fad_class_code')
					->order(''.$type.' DESC')
					->limit($top)
					->select();
		$data = array();
		foreach ($dataSet as $k=>$v){
			$data[$k]['x'] = $v['fadclass'];
			if ($type == 'count' || $type == 'times'){
				$data[$k]['y'] = $v[$type];
			}else{
				$data[$k]['y'] = $v['count'];
				$data[$k]['z'] = $v['times'];
			}
		}
		$this->ajaxReturn($data);
	}
	
	/**
	* 获取按地区按行业违法广告条数/次数,用于大屏展示[涉嫌违法违规行业分布]
	* @Param	int $region_id 区域ID
	* @Param	int $type 要返回的维度,条数或次数 'count'-仅条数 'times'-仅次数 'all'-条数和次数,默认'times'-仅次数
	* @Param	int $top 返回记录Top条数
	* @Return	array $data 返回的数据
	*/
	public function gather_public_illegal_adclass(){
		$region_id = intval(I('get.region_id',100000));
		$type = strval(I('get.type','times'));
		$top = intval(I('get.top',5));
        $fregulatorcode = intval('20'.$region_id);
		$fstarttime = I('fstarttime',date('Y-01-01',strtotime('-1 year')));
		$fendtime = I('fendtime',date('Y-12-31',strtotime('-1 year')));
		$where['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$fstarttime,$fendtime]];
		$field = 'tadclass.fadclass,COUNT(1) AS times ';
		if ($region_id > 0 && $region_id != 100000) {//查询某地区
			if($region_id == 370300){
				//TODO:淄博AGP特殊处理，待完善 2019-01-02
				$join_tmedia_temp = 'tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid ';
				$where['tmedia_temp.ftype'] = 1 ;
				$where['tmedia_temp.fuserid'] = 154 ;
				$where['tbn_illegal_ad.fcustomer'] = $region_id;
				$where['tbn_illegal_ad.fexamine'] = 10;
			}elseif($region_id == 130100){
				//TODO:石家庄AGP特殊处理，待完善 2019-08-23
				$join_tmedia_temp = 'tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid ';
				$where['tmedia_temp.ftype'] = 1 ;
				$where['tmedia_temp.fuserid'] = ['IN',[826,1213]] ;
				$where['tbn_illegal_ad.fcustomer'] = $region_id;
			}else{
				$tregulatormedia = M('tregulatormedia') //媒介权限过滤
					->cache(true,120)
					->where(['fregulatorcode'=>$fregulatorcode])
					->getField('fmediaid',true);
				if($tregulatormedia){
					//机构已授权媒体，只取授权媒体
					$where['tbn_illegal_ad_issue.fmedia_id'] = ['IN',implode(',',$tregulatormedia)];
				}else{
					//机构未授权媒体，则取地域媒体
					$region_id_rtrim = A ( 'Common/System', 'Model' )->get_region_left ( $region_id ); // 地区ID去掉末尾的0
					$region_id_strlen = strlen ( $region_id_rtrim ); // 去掉0后还有几位
					$where['left(tbn_illegal_ad.fregion_id,'.$region_id_strlen.')'] = $region_id_rtrim; // 地区搜索条件
				}
			}
		}
		$dataSet = M('tbn_illegal_ad_issue')
			->field('tadclass.fadclass,COUNT(1) AS times ')
			->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
			->join('tadclass ON tadclass.fcode = left(tbn_illegal_ad.fad_class_code,2)')
			->join('tmedia ON tmedia.fid = tbn_illegal_ad_issue.fmedia_id')
			->join($join_tmedia_temp)
			->where($where)
			->group('left(tbn_illegal_ad.fad_class_code,2)')
			->order($type.' DESC')
			->limit($top)
			->select();
		$data = array();
		foreach ($dataSet as $k=>$v){
			$data[$k]['x'] = $v['fadclass'];
			if ($type == 'count' || $type == 'times'){
				$data[$k]['y'] = $v[$type];
			}else{
				$data[$k]['y'] = $v['count'];
				$data[$k]['z'] = $v['times'];
			}
		}
		$this->ajaxReturn($data);
	}
	
	/**
	 * 获取按地区按媒介违法广告条数/次数,用于大屏展示[涉嫌违法违规媒介分布]
	 * @Param	int $region_id 区域ID
	 * @Param	int $type 要返回的维度,条数或次数 'count'-仅条数 'times'-仅次数 'all'-条数和次数,默认'times'-仅次数
	 * @Param	int $top 返回记录Top条数
	 * @Return	array $data 返回的数据
	 */
	public function gather_public_illegal_mediaclass0(){
		$regionId = intval(I('get.region_id',100000));
		$type = strval(I('get.type','times'));
		$top = intval(I('get.top',5));
		$field = 'fregionid,tmedia.fmedianame as name,SUM(fad_illegal_count) as count,SUM(fad_illegal_times) as times';
		$where = array();
		if($regionId > 0 && $regionId != 100000){
			$region_id_rtrim = A('Common/System','Model')->get_region_left($regionId);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(tbn_ad_summary_year.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		}
		$data = array();
		$dataSet = M('tbn_ad_summary_year')->cache(true,1200)
					->field($field)
					->join('tmedia ON tmedia.fid=tbn_ad_summary_year.fmediaid','LEFT')
					->where($where)
					->group('tbn_ad_summary_year.fmediaid')
					->order(''.$type.' DESC')
					->limit($top)
					->select();
		foreach ($dataSet as $k=>$v){
			$data[$k]['x'] = $v['name'];
			if ($type == 'count' || $type == 'times'){
				$data[$k]['y'] = $v[$type];
			}else{
				$data[$k]['y'] = $v['count'];
				$data[$k]['z'] = $v['times'];
			}
		}
		$this->ajaxReturn($data);
	}
	
	/**
	 * 获取按地区按媒介违法广告条数/次数,用于大屏展示[涉嫌违法违规媒介分布]
	 * @Param	int $region_id 区域ID
	 * @Param	int $type 要返回的维度,条数或次数 'count'-仅条数 'times'-仅次数 'all'-条数和次数,默认'times'-仅次数
	 * @Param	int $top 返回记录Top条数
	 * @Return	array $data 返回的数据
	 */
	public function gather_public_illegal_mediaclass1(){
		$regionId = intval(I('get.region_id',100000));
		$type = strval(I('get.type','times'));
		$top = intval(I('get.top',5));
		$field = 'fregionid,tmedia.fmedianame as name,SUM(fad_illegal_times) as times';
		$where = array();
		if($regionId > 0 && $regionId != 100000){
			$region_id_rtrim = A('Common/System','Model')->get_region_left($regionId);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(tbn_ad_summary_day_z.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		}
		$data = array();
		$dataSet = M('tbn_ad_summary_day_z')->cache(true,10)
					->field($field)
					->join('tmedia ON tmedia.fid=tbn_ad_summary_day_z.fmediaid','LEFT')
					->where($where)
					->group('tbn_ad_summary_day_z.fmediaid')
					->order(''.$type.' DESC')
					->limit($top)
					->select();
		foreach ($dataSet as $k=>$v){
			$data[$k]['x'] = $v['name'];
			if ($type == 'count' || $type == 'times'){
				$data[$k]['y'] = $v[$type];
			}else{
				$data[$k]['y'] = $v['count'];
				$data[$k]['z'] = $v['times'];
			}
		}
		$this->ajaxReturn($data);
	}
	
	/**
	* 获取按地区按行业违法广告条数/次数,用于大屏展示[涉嫌违法违规媒体分布]
	* @Param	int $region_id 区域ID
	* @Param	int $type 要返回的维度,条数或次数 'count'-仅条数 'times'-仅次数 'all'-条数和次数,默认'times'-仅次数
	* @Param	int $top 返回记录Top条数
	* @Return	array $data 返回的数据
	*/
	public function gather_public_illegal_mediaclass(){
		$region_id = intval(I('get.region_id',100000));
		$type = strval(I('get.type','times'));
		$top = intval(I('get.top',5));
        $fregulatorcode = intval('20'.$region_id);
		$fstarttime = I('fstarttime',date('Y-01-01',strtotime('-1 year')));
		$fendtime = I('fendtime',date('Y-12-31',strtotime('-1 year')));
		$where['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$fstarttime,$fendtime]];
		if ($region_id > 0 && $region_id != 100000) {//查询某地区
			if($region_id == 370300){
				//TODO:淄博AGP特殊处理，待完善 2019-01-02
				$join_tmedia_temp = 'tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid ';
				$where['tmedia_temp.ftype'] = 1 ;
				$where['tmedia_temp.fuserid'] = 154 ;
				$where['tbn_illegal_ad.fcustomer'] = $region_id;
				$where['tbn_illegal_ad.fexamine'] = 10;
			}elseif($region_id == 130100){
				//TODO:石家庄AGP特殊处理，待完善 2019-08-23
				$join_tmedia_temp = 'tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid ';
				$where['tmedia_temp.ftype'] = 1 ;
				$where['tmedia_temp.fuserid'] = ['IN',[826,1213]] ;
				$where['tbn_illegal_ad.fcustomer'] = $region_id;
			}else{
				$tregulatormedia = M('tregulatormedia') //媒介权限过滤
					->cache(true,120)
					->where(['fregulatorcode'=>$fregulatorcode])
					->getField('fmediaid',true);
				if($tregulatormedia){
					//机构已授权媒体，只取授权媒体
					$where['tbn_illegal_ad_issue.fmedia_id'] = ['IN',implode(',',$tregulatormedia)];
				}else{
					//机构未授权媒体，则取地域媒体
					$region_id_rtrim = A ( 'Common/System', 'Model' )->get_region_left ( $region_id ); // 地区ID去掉末尾的0
					$region_id_strlen = strlen ( $region_id_rtrim ); // 去掉0后还有几位
					$where['left(tbn_illegal_ad.fregion_id,'.$region_id_strlen.')'] = $region_id_rtrim; // 地区搜索条件
				}
			}
		}
		$dataSet = M('tbn_illegal_ad_issue')
			->field('tmedia.fmedianame,COUNT(1) AS times ')
			->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
			->join('tmedia ON tmedia.fid = tbn_illegal_ad_issue.fmedia_id')
			->join($join_tmedia_temp)
			->where($where)
			->group('tbn_illegal_ad_issue.fmedia_id')
			->order($type.' DESC')
			->limit($top)
			->select();
		$data = array();
		foreach ($dataSet as $k=>$v){
			$data[$k]['x'] = $v['fmedianame'];
			if ($type == 'count' || $type == 'times'){
				$data[$k]['y'] = $v[$type];
			}else{
				$data[$k]['y'] = $v['count'];
				$data[$k]['z'] = $v['times'];
			}
		}
		$this->ajaxReturn($data);
	}
	
	/**
	* 获取指定区域按行业类别统计的广告条数和条次，用于大屏展示[广告量分布]
	* @Param	int $region_id 区域ID
	* @Param	int $top 返回记录Top条数
	* @Return	array $data 返回的数据
	*/
	public function gather_public_count_adclass0(){
		$regionId = intval(I('get.region_id',100000));
		$top = intval(I('get.top',5));
		$field = 'fregionid,fad_class_code,tadclass.fadclass as adclass,fad_count, SUM(fad_count) as count, SUM(fad_times) as times';
		$where = array();
		if($regionId > 0 && $regionId != 100000){
			$region_id_rtrim = A('Common/System','Model')->get_region_left($regionId);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(tbn_ad_summary_year.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		}
		$where['left(fad_class_code,2)'] = array('NEQ',23);//排除“其它类”类别
		$data = array();
		$dataSet = M('tbn_ad_summary_year')->cache(true,1200)
			->field($field)
			->join('tadclass ON tbn_ad_summary_year.fad_class_code=tadclass.fcode','LEFT')
			->where($where)
			->group('fad_class_code')
			->order('times DESC')
			->limit($top)
			->select();
		foreach ($dataSet as $key=>$val){
			$data[$key]['area'] = $val['adclass'];
			$data[$key]['pv'] = number_format($val['count']);
			$data[$key]['attribute'] = number_format($val['times']);//TODO:企业数(广告主数),暂时以广告总条次返回
		}
		$this->ajaxReturn($data);
	}
	
	/**
	* 获取指定区域按行业类别统计的广告条数和条次，用于大屏展示[广告量分布]
	* @Param	int $region_id 区域ID
	* @Param	int $top 返回记录Top条数
	* @Return	array $data 返回的数据
	*/
	public function gather_public_count_adclass1(){
		$regionId = intval(I('get.region_id',100000));
		$top = intval(I('get.top',5));
		$field = 'fregionid,fad_class_code,tadclass.fadclass as adclass, SUM(fad_times) as times';
		$where = array();
		if($regionId > 0 && $regionId != 100000){
			$region_id_rtrim = A('Common/System','Model')->get_region_left($regionId);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(tbn_ad_summary_day_z.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		}
		$where['left(fad_class_code,2)'] = array('NEQ',23);//排除“其它类”类别
		$data = array();
		$dataSet = M('tbn_ad_summary_day_z')->cache(true,10)
			->field($field)
			->join('tadclass ON tbn_ad_summary_day_z.fad_class_code=tadclass.fcode','LEFT')
			->where($where)
			->group('fad_class_code')
			->order('times DESC')
			->limit($top)
			->select();
		foreach ($dataSet as $key=>$val){
			$data[$key]['area'] = $val['adclass'];
			$data[$key]['pv'] = number_format($val['count']);
			$data[$key]['attribute'] = number_format($val['times']);//TODO:企业数(广告主数),暂时以广告总条次返回
		}
		$this->ajaxReturn($data);
	}
	
	/**
	* 获取指定区域按行业类别统计的广告条数和条次，用于大屏展示[广告量分布]
	* @Param	int $region_id 区域ID
	* @Param	int $top 返回记录Top条数
	* @Return	array $data 返回的数据
	*/
	public function gather_public_count_adclass(){
		$region_id = intval(I('get.region_id',100000));
		$top = intval(I('get.top',5));
        $fregulatorcode = intval('20'.$region_id);
		$fstarttime = I('fstarttime',date('Y-01-01',strtotime('-1 year')));
		$fendtime = I('fendtime',date('Y-12-31',strtotime('-1 year')));
		$where['tbn_ad_summary_day_z.fdate'] = ['BETWEEN',[$fstarttime,$fendtime]];
		if ($region_id > 0 && $region_id != 100000) {//查询某地区
			if($region_id == 370300){
				//TODO:淄博AGP特殊处理，待完善 2019-01-02
				$join_tmedia_temp = 'tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid ';
				$where['tmedia_temp.ftype'] = 1 ;
				$where['tmedia_temp.fuserid'] = 154 ;
				// $where['tbn_ad_summary_day_z.fregionid'] = $region_id;
			}elseif($region_id == 130100){
				//TODO:石家庄AGP特殊处理，待完善 2019-08-23
				$join_tmedia_temp = 'tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid ';
				$where['tmedia_temp.ftype'] = 1 ;
				$where['tmedia_temp.fuserid'] = ['IN',[826,1213]];
			}elseif($region_id == 320100){
				//TODO:南京AGP特殊处理，待完善 2020-01-03
				$join_tmedia_temp = 'tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid ';
				$where['tmedia_temp.ftype'] = 1 ;
				$where['tmedia_temp.fuserid'] = ['IN',[286]];
			}else{
				$tregulatormedia = M('tregulatormedia') //媒介权限过滤
					->cache(true,120)
					->where(['fregulatorcode'=>$fregulatorcode])
					->getField('fmediaid',true);
				if($tregulatormedia){
					//机构已授权媒体，只取授权媒体
					$where['tbn_ad_summary_day_z.fmedia_id'] = ['IN',implode(',',$tregulatormedia)];
				}else{
					//机构未授权媒体，则取地域媒体
					$region_id_rtrim = A ( 'Common/System', 'Model' )->get_region_left ( $region_id ); // 地区ID去掉末尾的0
					$region_id_strlen = strlen ( $region_id_rtrim ); // 去掉0后还有几位
					$where['left(tbn_ad_summary_day_z.fregion_id,'.$region_id_strlen.')'] = $region_id_rtrim; // 地区搜索条件
				}
			}
		}
		// $where['left(tbn_ad_summary_day_z.fad_class_code,2)'] = array('NEQ',23);//排除“其它类”类别
		$data = [];
		$dataSet = M('tbn_ad_summary_day_z')
			->field('tadclass.fadclass,count(1) AS counts,SUM(fad_times) AS times ')
			->join('tadclass ON tadclass.fcode = LEFT ( tbn_ad_summary_day_z.fad_class_code, 2 )')
			->join('tmedia ON tmedia.fid = tbn_ad_summary_day_z.fmediaid')
			->join($join_tmedia_temp)
			->where($where)
			->group('LEFT ( tbn_ad_summary_day_z.fad_class_code, 2 ) ')
			->order('times DESC')
			->limit($top)
			->select();
		foreach ($dataSet as $key=>$val){
			$data[$key]['area'] = $val['fadclass'];
			$data[$key]['pv'] = number_format($val['counts']);
			$data[$key]['attribute'] = number_format($val['times']);//TODO:企业数(广告主数),暂时以广告总条次返回
		}
		$this->ajaxReturn($data);
	}
	
	/**
	* 获取大屏地图显示的4大指标数据
	* @Param	int $region_id 区域ID
	* @Param	int $id 指标类型 1-广告总量 2-当日广告量 3-涉嫌违规违法广告数量 4-立案数量
	* @Return	array $data 返回的数据
	*/
	public function gather_public_outline0(){
		$regionId = intval(I('get.region_id',100000));
		$id = intval(I('get.id',1));
		$where = array();
		if($regionId > 0 && $regionId != 100000){
			$region_id_rtrim = A('Common/System','Model')->get_region_left($regionId);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(tbn_ad_summary_year.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		}
		//$data = array();
		switch ($id){
			case 1:
				$count = 0;
				$dataTotal = M('tbn_ad_summary_year')->cache(true,1200)
					->field('SUM(fad_times) as times')
					->where($where)
					->select();
				$data[0]['value'] = $dataTotal[0]['times'] + $this->netCount;
				$this->ajaxReturn($data);
				break;
			case 2:
				$count = 0;
				$return_data = array ();
				$month_tab = date ( "Ym" ); // 月份,用于确定发布表的名称
				$where = array ();
				$where ['date'] = date ( 'Y-01-01', time () );
				if ($regionId > 0 && $regionId != 100000) { // 查询某地区
					$region_id_rtrim = A ( 'Common/System', 'Model' )->get_region_left ( $regionId ); // 地区ID去掉末尾的0
					$region_id_strlen = strlen ( $region_id_rtrim ); // 去掉0后还有几位
					$where ['left(tbn_ad_summary_year.fregionid,' . $region_id_strlen . ')'] = $region_id_rtrim; // 地区搜索条件
					
					$table_postfix = '_' . $month_tab . '_' . substr ( $regionId, 0, 2 ); // 表后缀
					
					try {
						$return_data ['today_tv_count'] = M ( 'ttvissue' . $table_postfix )->cache ( true, 600 )->where ( array (
							'left(fregionid,' . $region_id_strlen . ')' => $region_id_rtrim,
							'fissuedate' => strtotime ( date ( 'Y-m-d', time ()))))
							->count (); // 查询今天的电视发布数量
					} catch ( Exception $error ) {
						$return_data ['today_tv_count'] = 0;
					}
					
					try {
						$return_data ['today_bc_count'] = M ( 'tbcissue' . $table_postfix )->cache ( true, 600 )->where ( array (
								'left(fregionid,' . $region_id_strlen . ')' => $region_id_rtrim,
								'fissuedate' => strtotime ( date ( 'Y-m-d', time () ) )
						) )->count (); // 查询今天的广播发布数量
					} catch ( Exception $error ) {
						$return_data ['today_bc_count'] = 0;
					}
					
					$today_paper_count = M ( 'tpaperissue' )->cache ( true, 600 )->join ( 'tmedia on tmedia.fid = tpaperissue.fmediaid' )->join ( 'tmediaowner on tmediaowner.fid = tmedia.fmediaownerid' )->where ( array (
							'left(tmediaowner.fregionid,' . $region_id_strlen . ')' => $region_id_rtrim,
							'fissuedate' => date ( 'Y-m-d' )
						) )->count (); // 报纸今天的发布数量
					$return_data ['today_paper_count'] = intval ( $today_paper_count );
					
				} else { // 查询全国
					$regionList = M ( 'tregion' )->cache ( true, 86400 )->field ( 'left(fid,2) as region_prefix' )->where ( array (
							'right(fid,4)' => '0000'
					) )->select ();
					
					$return_data ['today_tv_count'] = 0;
					$return_data ['today_bc_count'] = 0;
					
					foreach ( $regionList as $region ) { // 循环地区
						if ($region['region_prefix'] != 99){
							$table_postfix = '_' . $month_tab . '_' . $region ['region_prefix']; // 表后缀
							try {
								$today_tv_count = M ( 'ttvissue' . $table_postfix )->cache ( true, 600 )->where ( array (
										'fissuedate' => strtotime ( date ( 'Y-m-d', time () ) )
								) )->count (); // 查询今天的电视发布数量
							} catch ( Exception $error ) {
								$today_tv_count = 0;
							}
							$return_data ['today_tv_count'] += intval ( $today_tv_count );
							
							try {
								$today_bc_count = M ( 'tbcissue' . $table_postfix )->cache ( true, 600 )->where ( array (
										'fissuedate' => strtotime ( date ( 'Y-m-d', time () ) )
								) )->count (); // 查询今天的广播发布数量
							} catch ( Exception $error ) {
								$today_bc_count = 0;
							}
							$return_data ['today_bc_count'] += intval ( $today_bc_count );
						}
					}
					
					$return_data ['today_paper_count'] = intval ( M ( 'tpaperissue' )->cache ( true, 600 )->where ( array (
						'fissuedate' => date ( 'Y-m-d' )
						) )->count () ); // 报纸今天的发布数量
				}
				//全国或指定[地区][当天](电视+广播+报纸) 发布总量
				$count = $return_data ['today_tv_count'] + $return_data ['today_bc_count'] + $return_data ['today_paper_count'];
				$data[0]['value'] = $count;
				$this->ajaxReturn($data);
				break;
			case 3:
				$data[0]['value'] = 0;
				$where['fdate'] = date('Y',time()).'-01-01';
				$dataSet = M('tbn_ad_summary_year')->cache(true,1200)
					->field('SUM(fad_illegal_times) AS count')
					->where($where)
					->select();
				$data[0]['value'] = $dataSet[0]['count'];
				$this->ajaxReturn($data);
				break;
			case 4:
				$data[0]['value'] = '0';//TODO:立案数暂未提供
				$this->ajaxReturn($data);
				break;
			default:
				$data[0]['value'] = '0';
				$this->ajaxReturn($data);
				break;
		}
	}
	
	/**
	* 获取大屏地图显示的4大指标数据
	* @Param	int $region_id 区域ID
	* @Param	int $id 指标类型 1-广告总量 2-当日广告量 3-涉嫌违规违法广告数量 4-立案数量
	* @Return	array $data 返回的数据
	*/
	public function gather_public_outline1(){
		$regionId = intval(I('get.region_id',100000));
		$id = intval(I('get.id',1));
		$where = array();
		if($regionId > 0 && $regionId != 100000){
			$region_id_rtrim = A('Common/System','Model')->get_region_left($regionId);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(tbn_ad_summary_day_z.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		}
		//$data = array();
		switch ($id){
			case 1:
				$count = 0;
				$dataTotal = M('tbn_ad_summary_day_z')->cache(true,1200)
					->field('SUM(fad_times) as times')
					->where($where)
					->select();
				$data[0]['value'] = $dataTotal[0]['times'] + $this->netCount;
				$this->ajaxReturn($data);
				break;
			case 2:
				$count = 0;
				$return_data = array ();
				$month_tab = date ( "Ym" ); // 月份,用于确定发布表的名称
				$where = array ();
				$where ['date'] = date ( 'Y-01-01', time () );
				if ($regionId > 0 && $regionId != 100000) { // 查询某地区
					$region_id_rtrim = A ( 'Common/System', 'Model' )->get_region_left ( $regionId ); // 地区ID去掉末尾的0
					$region_id_strlen = strlen ( $region_id_rtrim ); // 去掉0后还有几位
					$where ['left(tbn_ad_summary_day_z.fregionid,' . $region_id_strlen . ')'] = $region_id_rtrim; // 地区搜索条件
					
					$table_postfix = '_' . $month_tab . '_' . substr ( $regionId, 0, 2 ); // 表后缀
					
					try {
						$return_data ['today_tv_count'] = M ( 'ttvissue' . $table_postfix )->cache ( true, 600 )->where ( array (
							'left(fregionid,' . $region_id_strlen . ')' => $region_id_rtrim,
							'fissuedate' => strtotime ( date ( 'Y-m-d', time ()))))
							->count (); // 查询今天的电视发布数量
					} catch ( Exception $error ) {
						$return_data ['today_tv_count'] = 0;
					}
					
					try {
						$return_data ['today_bc_count'] = M ( 'tbcissue' . $table_postfix )->cache ( true, 600 )->where ( array (
								'left(fregionid,' . $region_id_strlen . ')' => $region_id_rtrim,
								'fissuedate' => strtotime ( date ( 'Y-m-d', time () ) )
						) )->count (); // 查询今天的广播发布数量
					} catch ( Exception $error ) {
						$return_data ['today_bc_count'] = 0;
					}
					
					$today_paper_count = M ( 'tpaperissue' )->cache ( true, 600 )->join ( 'tmedia on tmedia.fid = tpaperissue.fmediaid' )->join ( 'tmediaowner on tmediaowner.fid = tmedia.fmediaownerid' )->where ( array (
							'left(tmediaowner.fregionid,' . $region_id_strlen . ')' => $region_id_rtrim,
							'fissuedate' => date ( 'Y-m-d' )
						) )->count (); // 报纸今天的发布数量
					$return_data ['today_paper_count'] = intval ( $today_paper_count );
					
				} else { // 查询全国
					$regionList = M ( 'tregion' )->cache ( true, 86400 )->field ( 'left(fid,2) as region_prefix' )->where ( array (
							'right(fid,4)' => '0000'
					) )->select ();
					
					$return_data ['today_tv_count'] = 0;
					$return_data ['today_bc_count'] = 0;
					
					foreach ( $regionList as $region ) { // 循环地区
						if ($region['region_prefix'] != 99){
							$table_postfix = '_' . $month_tab . '_' . $region ['region_prefix']; // 表后缀
							try {
								$today_tv_count = M ( 'ttvissue' . $table_postfix )->cache ( true, 600 )->where ( array (
										'fissuedate' => strtotime ( date ( 'Y-m-d', time () ) )
								) )->count (); // 查询今天的电视发布数量
							} catch ( Exception $error ) {
								$today_tv_count = 0;
							}
							$return_data ['today_tv_count'] += intval ( $today_tv_count );
							
							try {
								$today_bc_count = M ( 'tbcissue' . $table_postfix )->cache ( true, 600 )->where ( array (
										'fissuedate' => strtotime ( date ( 'Y-m-d', time () ) )
								) )->count (); // 查询今天的广播发布数量
							} catch ( Exception $error ) {
								$today_bc_count = 0;
							}
							$return_data ['today_bc_count'] += intval ( $today_bc_count );
						}
					}
					
					$return_data ['today_paper_count'] = intval ( M ( 'tpaperissue' )->cache ( true, 600 )->where ( array (
						'fissuedate' => date ( 'Y-m-d' )
						) )->count () ); // 报纸今天的发布数量
				}
				//全国或指定[地区][当天](电视+广播+报纸) 发布总量
				$count = $return_data ['today_tv_count'] + $return_data ['today_bc_count'] + $return_data ['today_paper_count'];
				$data[0]['value'] = $count;
				$this->ajaxReturn($data);
				break;
			case 3:
				$data[0]['value'] = 0;
				$where['fdate'] = date('Y',time()).'-01-01';
				$dataSet = M('tbn_ad_summary_day_z')->cache(true,1200)
					->field('SUM(fad_illegal_times) AS count')
					->where($where)
					->select();
				$data[0]['value'] = $dataSet[0]['count'];
				$this->ajaxReturn($data);
				break;
			case 4:
				$data[0]['value'] = '0';//TODO:立案数暂未提供
				$this->ajaxReturn($data);
				break;
			default:
				$data[0]['value'] = '0';
				$this->ajaxReturn($data);
				break;
		}
	}
	
	/**
	* 获取大屏地图显示的4大指标数据
	* @Param	int $region_id 区域ID
	* @Param	int $id 指标类型 1-广告总量 2-当日广告量 3-涉嫌违规违法广告数量 4-立案数量
	* @Return	array $data 返回的数据
	*/
	public function gather_public_outline(){
		$region_id = intval(I('get.region_id',100000));
		$id = intval(I('get.id',1));
        $fregulatorcode = intval('20'.$region_id);  //监管机构ID
		$fstarttime = I('fstarttime',date('Y-01-01',strtotime('-1 year')));
		$fendtime = I('fendtime',date('Y-12-31',strtotime('-1 year')));
		$where['fdate'] = ['BETWEEN',[$fstarttime,$fendtime]];
		if ($region_id > 0 && $region_id != 100000) {//查询某地区
            $tregulatormedia = M('tregulatormedia') //媒介权限过滤
                ->cache(true,120)
                ->where(['fregulatorcode'=>$fregulatorcode])
				->getField('fmediaid',true);
			if($tregulatormedia){
				//机构已授权媒体，只取授权媒体
				$where['fmediaid'] = ['IN',implode(',',$tregulatormedia)];
			}else{
				//机构未授权媒体，则取地域媒体
				$region_id_rtrim = A ( 'Common/System', 'Model' )->get_region_left ( $region_id ); // 地区ID去掉末尾的0
				$region_id_strlen = strlen ( $region_id_rtrim ); // 去掉0后还有几位
				$where['left(fregionid,'.$region_id_strlen.')'] = $region_id_rtrim; // 地区搜索条件
			}
		}
		switch ($id){
			case 1:
				$count = 0;
				$dataTotal = M($this->table)
					->cache(true,10)
					->field('sum(fad_times) as issue_count')
					->where($where)
					->select();
				$data[0]['value'] = $dataTotal[0]['issue_count'] + $this->netCount;
				$this->ajaxReturn($data);
				break;
			case 2:
				$count = 0;
				$return_data = array ();
				$month_tab = date ( "Ym" ); // 月份,用于确定发布表的名称
				$where = array ();
				$where ['date'] = date ( 'Y-01-01', time () );
				if ($region_id > 0 && $region_id != 100000) { // 查询某地区
					$region_id_rtrim = A('Common/System','Model')->get_region_left($region_id); // 地区ID去掉末尾的0
					$region_id_strlen = strlen($region_id_rtrim); // 去掉0后还有几位
					$whereTvBc['left(fregionid,'.$region_id_strlen.')'] = $region_id_rtrim; // 地区搜索条件
					$whereTvBc['fissuedate'] = strtotime(date('Y-m-d'));// 当天0点时间戳
					$table_postfix = '_' . $month_tab . '_' . substr ( $region_id, 0, 2 ); // 表后缀
					// 查询今天的电视发布数量
					try {
						$return_data['today_tv_count'] = M('ttvissue'.$table_postfix)
							->where($whereTvBc)
							->count();
					} catch ( Exception $error ) {
						$return_data ['today_tv_count'] = 0;
					}
					// 查询今天的广播发布数量
					try {
						$return_data['today_bc_count'] = M( 'tbcissue'.$table_postfix)
							->where($whereTvBc)
							->count (); 
					} catch ( Exception $error ) {
						$return_data ['today_bc_count'] = 0;
					}
					// 报纸今天的发布数量
					$today_paper_count = M ( 'tpaperissue' )
						->join ( 'tmedia on tmedia.fid = tpaperissue.fmediaid' )
						->join ( 'tmediaowner on tmediaowner.fid = tmedia.fmediaownerid' )
						->where ( array (
							'left(tmediaowner.fregionid,' . $region_id_strlen . ')' => $region_id_rtrim,
							'fissuedate' => date ( 'Y-m-d' )
						) )->count (); 
					$return_data ['today_paper_count'] = intval ( $today_paper_count );
					
				} else { // 查询全国
					$regionList = M ( 'tregion' )
						->cache ( true, 86400 )
						->field ( 'left(fid,2) as region_prefix' )
						->where ( array (
							'right(fid,4)' => '0000'
						) )
						->select ();
					
					$return_data ['today_tv_count'] = 0;
					$return_data ['today_bc_count'] = 0;
					
					foreach ( $regionList as $region ) { // 循环地区
						if ($region['region_prefix'] != 99){
							$table_postfix = '_' . $month_tab . '_' . $region ['region_prefix']; // 表后缀
							try {
								$today_tv_count = M ( 'ttvissue' . $table_postfix )
									->cache ( true, 600 )
									->where ( array (
										'fissuedate' => strtotime ( date ( 'Y-m-d', time () ) )
									) )
									->count (); // 查询今天的电视发布数量
							} catch ( Exception $error ) {
								$today_tv_count = 0;
							}
							$return_data ['today_tv_count'] += intval ( $today_tv_count );
							
							try {
								$today_bc_count = M ( 'tbcissue' . $table_postfix )
									->cache ( true, 600 )
									->where ( array (
										'fissuedate' => strtotime ( date ( 'Y-m-d', time () ) )
									) )
									->count (); // 查询今天的广播发布数量
							} catch ( Exception $error ) {
								$today_bc_count = 0;
							}
							$return_data ['today_bc_count'] += intval ( $today_bc_count );
						}
					}
					
					$return_data ['today_paper_count'] = intval ( M ( 'tpaperissue' )
						->cache ( true, 600 )
						->where ( array (
							'fissuedate' => date ( 'Y-m-d' )
						) )
						->count () ); // 报纸今天的发布数量
				}
				//全国或指定[地区][当天](电视+广播+报纸) 发布总量
				$count = $return_data ['today_tv_count'] + $return_data ['today_bc_count'] + $return_data ['today_paper_count'];
				$data[0]['value'] = $count;
				$this->ajaxReturn($data);
				break;
			case 3:
				$data[0]['value'] = 0;
				$whereIllegal['fissue_date'] = ['BETWEEN',[$fstarttime,$fendtime]];
				if ($region_id > 0 && $region_id != 100000) {//查询某地区
					if($region_id == 370300){
						//TODO:淄博AGP特殊处理，待完善 2019-01-02
						$join_tmedia_temp = 'tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid ';
						$whereIllegal['tmedia_temp.ftype'] = 1 ;
						$whereIllegal['tmedia_temp.fuserid'] = 154 ;
						$whereIllegal['tbn_illegal_ad.fcustomer'] = $region_id;
						$whereIllegal['tbn_illegal_ad.fexamine'] = 10;
					}elseif($region_id == 130100){
						//TODO:石家庄AGP特殊处理，待完善 2019-08-23
						$join_tmedia_temp = 'tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid ';
						$whereIllegal['tmedia_temp.ftype'] = 1 ;
						$whereIllegal['tmedia_temp.fuserid'] = ['IN',[826,1213]] ;
						$whereIllegal['tbn_illegal_ad.fcustomer'] = $region_id;
						$whereIllegal['tbn_illegal_ad.fexamine'] = 10;
					}elseif($region_id == 320100){
						//TODO:南京AGP特殊处理，待完善 2020-01-03
						$join_tmedia_temp = 'tmedia_temp ON tmedia_temp.fmediaid = tmedia.fid ';
						$whereIllegal['tmedia_temp.ftype'] = 1 ;
						$whereIllegal['tbn_illegal_ad.fcustomer'] = $region_id;
						$whereIllegal['tbn_illegal_ad.fexamine'] = 10;
						// $where['tmedia_temp.fuserid'] = ['IN',[826,1213]];
					}else{
						$tregulatormedia = M('tregulatormedia') //媒介权限过滤
							->cache(true,120)
							->where(['fregulatorcode'=>$fregulatorcode])
							->getField('fmediaid',true);
						if($tregulatormedia){
							//机构已授权媒体，只取授权媒体
							$whereIllegal['fmediaid'] = ['IN',implode(',',$tregulatormedia)];
						}else{
							//机构未授权媒体，则取地域媒体
							$region_id_rtrim = A ( 'Common/System', 'Model' )->get_region_left ( $region_id ); // 地区ID去掉末尾的0
							$region_id_strlen = strlen ( $region_id_rtrim ); // 去掉0后还有几位
							$whereIllegal['left(fregionid,'.$region_id_strlen.')'] = $region_id_rtrim; // 地区搜索条件
						}
					}
				}
				$dataSet = M('tbn_illegal_ad_issue')
					->join('tbn_illegal_ad ON tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid')
					->join('tmedia ON tmedia.fid = tbn_illegal_ad_issue.fmedia_id')
					->join($join_tmedia_temp)
					->where($whereIllegal)
					->count();
				$sql = M('tbn_illegal_ad_issue')->getLastSql();
				$data[0]['value'] = $dataSet;
				$this->ajaxReturn($data);
				break;
			case 4:
				$data[0]['value'] = '0';//TODO:立案数暂未提供
				$this->ajaxReturn($data);
				break;
			default:
				$data[0]['value'] = '0';
				$this->ajaxReturn($data);
				break;
		}
	}
	
	/**
	* 用于对数组自定义元素排序比较
	* @Param	Mixed variable
	* @Return	void 返回的数据
	* @Date		2018年6月21日 上午11:16:59
	*/
	private function cmp($a,$b){
		//return strcmp((String)$a['y'], (String)$b['y']);
		if ($a['y'] == $b['y']){
			return 0;
		}
		return $a['y'] < $b['y'] ? 1 : -1;
	}

	/**
	 * 临时-导出1-9月各地区广告统计数据
	 * @Param String $type 媒介类型 如：'tv','bc','paper','net'
	 * @Param String $dateS 开始日期年月 如：'201801',默认当前年1月
	 * @Param String $dateE 截止日期年月 如：'201809',默认当前月
	 */
	public function exportData(){
		$type = I('type','tv');
		$dateS = (int)I('dateS',date('Y01'));
		$dateE = (int)I('dateE',date('Ym'));
		set_time_limit(0);
		$month = $dateS;
		$regionArr = M('tregion')->cache(true,120)->field('DISTINCT LEFT(fid,2) AS code')->select();
		$dataSet = [];
		for($month; $month<=$dateE; $month++){
			foreach($regionArr as $key=>$region){
				switch($type){
					case 'tv':
						$typeName = '电视';
						$tableName = "t".$type."issue_".$month."_".$region['code'];
						$samTableName = 't'.$type.'sample';
						$fid = 'fid';
						$fissueSamId = 'fsampleid';
						$samId = 'fid';
						$fadlen = "
							,sum(case when tsam.fadlen >= 0 then tsam.fadlen else 0 end) `总时长`
							,sum(case when tsam.fadlen >= 0 and tsam.fillegaltypecode > 0 then tsam.fadlen else 0 end) `违法时长`";
						break;
					case 'bc':
						$typeName = '广播';
						$tableName = "t".$type."issue_".$month."_".$region['code'];
						$samTableName = 't'.$type.'sample';
						$fid = 'fid';
						$fissueSamId = 'fsampleid';
						$samId = 'fid';
						$fadlen = "
							,sum(case when tsam.fadlen >= 0 then tsam.fadlen else 0 end) `总时长`
							,sum(case when tsam.fadlen >= 0 and tsam.fillegaltypecode > 0 then tsam.fadlen else 0 end) `违法时长`";
						break;
					case 'paper':
						$typeName = '报纸';
						$tableName = "t".$type."issue";
						$samTableName = 't'.$type.'sample';
						$fid = 'fpaperissueid';
						$fissueSamId = 'fpapersampleid';
						$samId = 'fpapersampleid';
						$fadlen = '';
						break;
					case 'net':
						$typeName = '互联网';
						$tableName = "t".$type."issueputlog";
						$samTableName = 't'.$type.'issue';
						$fid = 'major_key';
						$fissueSamId = 'major_key';
						$samId = 'fid';
						$fadlen = '';
						break;
					default:
						$typeName = '电视';
						$tableName = "t".$type."issue_".$month."_".$region['code'];
						$samTableName = 't'.$type.'sample';
						$samId = 'fid';
						$fadlen = "
							,sum(case when tsam.fadlen >= 0 then tsam.fadlen else 0 end) `总时长`
							,sum(case when tsam.fadlen >= 0 and tsam.fillegaltypecode > 0 then tsam.fadlen else 0 end) `违法时长`";
						break;
				}
				$hasTable = M()->query('SHOW TABLES LIKE "'.$tableName.'"');//判断表是否存在
                if ($hasTable) {
                    $sql = "select 
						".$month." as 月份
						,'".$typeName."' as 媒介类型
						,tissue.fmediaid as 媒介ID
						,(select fmedianame from tmedia where fid = tissue.fmediaid) as 媒介名称
						,(select ffullname from tregion join tmedia on tmedia.media_region_id = tregion.fid where tmedia.fid = tissue.fmediaid) as 地区
						
						#条次
						,count(case when tsam.fadlen BETWEEN 0 and 30  then fsampleid else null end) as `0-31秒条次`
						,count(case when tsam.fadlen BETWEEN 31 and 60  then fsampleid else null end) as `31-60秒条次`
						,count(case when tsam.fadlen BETWEEN 61 and 180  then fsampleid else null end) as `61-180秒条次`
						,count(case when tsam.fadlen > 180  then fsampleid else null end) as `181秒以上条次`
						,count(case when tsam.fadlen >= 0  then fsampleid else null end) as `总条次`
						
						#条数
						,count(DISTINCT case when tsam.fadlen BETWEEN 0 and 30  then fsampleid else null end) as `0-31秒条数`
						,count(DISTINCT case when tsam.fadlen BETWEEN 31 and 60  then fsampleid else null end) as `31-60秒条数`
						,count(DISTINCT case when tsam.fadlen BETWEEN 61 and 180  then fsampleid else null end) as `61-180秒条数`
						,count(DISTINCT case when tsam.fadlen > 180  then fsampleid else null end) as `181秒以上条数`
						,count(DISTINCT case when tsam.fadlen >= 0  then fsampleid else null end) as `总条数`
						
						#违法条次
						,count(case when tsam.fadlen BETWEEN 0 and 30 and tsam.fillegaltypecode > 0  then fsampleid else null end) as `0-31秒违法条次`
						,count(case when tsam.fadlen BETWEEN 31 and 60 and tsam.fillegaltypecode > 0  then fsampleid else null end) as `31-60秒违法条次`
						,count(case when tsam.fadlen BETWEEN 61 and 180 and tsam.fillegaltypecode > 0  then fsampleid else null end) as `61-180秒违法条次`
						,count(case when tsam.fadlen > 180 and tsam.fillegaltypecode > 0  then fsampleid else null end) as `181秒以上违法条次`
						,count(case when tsam.fadlen >= 0 and tsam.fillegaltypecode > 0  then fsampleid else null end) as `总违法条次`
						
						#违法条数
						,count(DISTINCT case when tsam.fadlen BETWEEN 0 and 30 and tsam.fillegaltypecode > 0  then fsampleid else null end) as `0-31秒违法条数`
						,count(DISTINCT case when tsam.fadlen BETWEEN 31 and 60 and tsam.fillegaltypecode > 0  then fsampleid else null end) as `31-60秒违法条数`
						,count(DISTINCT case when tsam.fadlen BETWEEN 61 and 180 and tsam.fillegaltypecode > 0  then fsampleid else null end) as `61-180秒违法条数`
						,count(DISTINCT case when tsam.fadlen > 180 and tsam.fillegaltypecode > 0  then fsampleid else null end) as `181秒以上违法条数`
						,count(DISTINCT case when tsam.fadlen >= 0 and tsam.fillegaltypecode > 0  then fsampleid else null end) as `总违法条数`
						
						#时长
						".$fadlen."
						from ".$tableName." as tissue
						join ".$samTableName." as tsam on tsam.".$samId." = tissue.".$fissueSamId."
						group by tissue.fmediaid";
					$dataRow = M()->query($sql);
					foreach($dataRow as $k=>$row){
						if($row['媒介名称'] == ''){
							unset($dataRow[$k]);//删除媒介名称已不存在的记录
						}
					}
                    array_push($dataSet, $dataRow);
                }
			}
		}
		$excelHead = '月份,媒介类型,媒介ID,媒介名称,地区,0-31秒条次,31-60秒条次,61-180秒条次,181秒以上条次,总条次,0-31秒条数,31-60秒条数,61-180秒条数,181秒以上条数,总条数,0-31秒违法条次,31-60秒违法条次,61-180秒违法条次,181秒以上违法条次,总违法条次,0-31秒违法条数,31-60秒违法条数,61-180秒违法条数,181秒以上违法条数,总违法条数,总时长,违法时长';
		foreach($dataSet as $val){
			$fileContent .= A('Common/Csv','Model')->convertFile($val,$excelHead);
		}
		header("Content-Disposition: attachment;filename=导出数据.csv");
		echo $fileContent;
		exit;
	}
	
}