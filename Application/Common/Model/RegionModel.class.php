<?php
namespace Common\Model;



class RegionModel{

	 /**
     * 获取行政区划树
     * @return array
     */
    public function getRegionTree(){

        $data = M('tregion')->cache(true,10)->field('fid value, fpid, fname label')->where(['fstate' => ['EQ', 1], 'fid' => ['NEQ', 100000]])->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpid']])){
                $items[$value['fpid']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }
	
	
	/*获取地区id列表*/
	public function get_region_array($fid = '',$regionid_array = array(),$num = 0){
		$regionid_array[] = $fid;
		$v = M('tregion')->where(array('fid'=>$fid))->find();
		if($fid <= 100000 || $num > 5){
			return $regionid_array;
		}else{
			return $this->get_region_array($v['fpid'],$regionid_array,$num+1 );
		}	
			
	}
    

}