var adInfo = {
  template: `
  <div v-loading="loading" style="padding:0 10px;overflow: auto;height: 100%;box-sizing: border-box;">
    <el-row :gutter="8">
      <el-col :span="12" ref="iframe">
        <div v-if="activeSam.source_path">
          <div v-if="activeSam.media_class === '01' || activeSam.media_class === '02'">
            <video controls="controls" :src="activeSam.source_path" autoplay width="100%" style="height: 300px;"></video>
          </div>
          <div v-if="activeSam.media_class === '03'">
            <img :src="activeSam.source_path" alt="" width="100%">
          </div>
        </div>
        <iframe style="width: 100%;height:100%;border: none;" v-if="activeSam.media_class == 13" :src="iframeSrc"></iframe>
        <div v-if="!activeSam.source_path && activeSam.media_class != 13" style="text-align: center; color: #606266;margin-top: 30%;font-size: 30px;font-weight: bold;">
          未找到对应样本
        </div>
        <el-table :data="adInfo.samList" border stripe @row-click="rowClick" style="margin-bottom: 10px;" v-if="adInfo.samList.length" :row-style="{'cursor': 'pointer'}" :height="tableHeight">
          <el-table-column type="index" width="50"></el-table-column>
          <el-table-column
            label="版本名称">
            <template slot-scope="scope">
              <span :style="{color: activeSam.fid === scope.row.fid ? '#1AB394' : 'inherit'}">{{scope.row.fversion || '无版本名称'}}</span>
            </template>
          </el-table-column>
          <el-table-column
            label="媒体类型"
            width="80px">
            <template slot-scope="scope">
              {{mediaType[scope.row.media_class]}}
            </template>
          </el-table-column>
        </el-table>
      </el-col>
      <el-col :span="12">
        <el-form
          ref="adInfoForm"
          :rules="rules"
          label-width="80px"
          :model="adInfo"
          label-position="left">
          <el-form-item label="广告ID">
            {{adInfo.fadid}}
          </el-form-item>
          <el-form-item label="名称" prop="fadname">
            <el-autocomplete
              v-model="adInfo.fadname"
              :fetch-suggestions="searchAdName"
              placeholder="请输入广告名称"
              @select="autoWriteIn"
              @blur="adNameBlur"
              clearable
            ></el-autocomplete>
          </el-form-item>
          <el-form-item label="品牌">
            <el-input v-model="adInfo.fbrand"></el-input>
          </el-form-item>
          <el-form-item label="类别">
            <el-cascader
              separator=">"
              filterable
              :options="adClassArr"
              :props="{emitPath: false}"
              v-model="adInfo.fadclasscode"
            ></el-cascader>
          </el-form-item>
          <el-form-item v-if="showClassTip">
            <span style="color: red;font-size: 12px;line-height: 20px;">检测到“公益”，请确认是否为公益广告并选择正确广告类别</span>
          </el-form-item>
          <el-form-item label="商业类别">
            <el-cascader
              separator=">"
              :options="adClassTree"
              :props="{emitPath: false}"
              v-model="adInfo.fadclasscode_v2"
              :filterable="true"
            ></el-cascader>
          </el-form-item>
          <el-form-item label="广告主">
            <el-autocomplete
              v-model="adInfo.adowner_name"
              :fetch-suggestions="searchAdOwner"
            ></el-autocomplete>
          </el-form-item>
          <el-form-item label="修改原因" required>
            <div style="display: flex;margin-bottom: 10px;">
              <div style="display: flex;">
                <el-select v-model="params" placeholder="请选择错误字段" clearable>
                  <el-option v-for="(value,key) in paramsMap"
                    :key="key"
                    :label="value"
                    :value="key">
                  </el-option>
                </el-select>
                <el-select v-model="reason" placeholder="请选择错误原因" clearable>
                  <el-option v-for="(value,key) in reasonMap"
                    :key="key"
                    :label="value"
                    :value="key">
                  </el-option>
                </el-select>
              </div>
              <div>
                <el-button type="primary" @click="getReason">添 加</el-button>
              </div>
            </div>
            <div>
              <el-tag type="primary" closable v-for="(i,index) in reasons">{{i}}</el-tag>
            </div>
            <el-input v-model="cosReason" placeholder="如果填了自定义理由，则以自定义理由为准" type="textarea" :rows="3"></el-input>
          </el-form-item>
          <el-form-item v-if="adInfo.fstate == '9'" label-width="0">
          <div style="color: #F56C6C;">该广告需要修改</div>
          <div>{{adInfo.comment}}</div>
          </el-form-item>
          <el-form-item label="状态">
            {{stateMap[adInfo.fstate].label}}
          </el-form-item>
          <el-form-item label="创建人">
            {{adInfo.fcreator}}
          </el-form-item>
          <el-form-item label="创建时间">
            {{adInfo.fcreatetime}}
          </el-form-item>
          <el-form-item label="修改人">
            {{adInfo.fmodifier}}
          </el-form-item>
          <el-form-item label="修改时间">
            {{adInfo.fmodifytime}}
          </el-form-item>
          <el-form-item label-width="0" style="text-align: center;">
            <el-button type="primary" @click="save">
              {{adInfo.fstate === '2' ? '确认并保存' : '保 存'}}
            </el-button>
          </el-form-item>
        </el-form>
      </el-col>
    </el-row>
    <el-dialog
      title="同名主题合并"
      :visible.sync="chooseDialog"
      :close-on-click-modal="false"
      :close-on-press-escape="false"
      :show-close="false"
      modal-append-to-body
      append-to-body
      center
      width="1000px">
      <el-table :data="tableData" border :row-style="rowStyle" @row-click="row => activeTheme = row">
        <el-table-columntype="index">
        </el-table-column>
        <el-table-column prop="fadname" label="广告名称" width="200" show-overflow-tooltip>
          <template slot-scope="scope">
          {{scope.row.fadname}}（{{scope.row._fadid}}）
          </template>
        </el-table-column>
        <el-table-column prop="ffullname" label="类别" show-overflow-tooltip>
        </el-table-column>
        <el-table-column prop="fadclasscode_v2name" label="商业类别" show-overflow-tooltip>
        </el-table-column>
        <el-table-column prop="fadowner" label="广告主" show-overflow-tooltip>
        </el-table-column>
        <el-table-column prop="fcreatetime" label="创建日期" width="170" show-overflow-tooltip>
        </el-table-column>
        <el-table-column prop="fcreator" label="创建人" show-overflow-tooltip>
        </el-table-column>
        <el-table-column prop="fmodifytime" label="修改日期" width="170" show-overflow-tooltip>
        </el-table-column>
        <el-table-column prop="fmodifier" label="修改人" show-overflow-tooltip>
        </el-table-column>
      </el-table>
      <div slot="footer">
        <el-button @click="closeThemeDialog">继续编辑主题</el-button>
        <el-button type="primary" @click="saveTheme">提 交</el-button>
      </div>
    </el-dialog>
  </div>`,
  props: ["ad_info"],
  data() {
    let checkAdname = (rule, value, callback) => {
      if (!value) {
        return callback(new Error('名称不能为空'))
      }
      if (value !== this.sourceName) {
        return callback(new Error('修改广告名称会修改所有的样本广告名, 请注意'))
      }
    }
    return {
      tableData: [],
      chooseDialog: false,
      activeTheme: {},

      adInfo: {
        fadname: ''
      },
      loading: false,
      showClassTip: false,
      adClassArr: adClassArr,
      adClassTree: adClassTree,
      activeSam: {},
      tableHeight: '300px',
      rules: {
        fadname: [{ validator: checkAdname, trigger: 'blur' }],
      },
      params: '',
      reason: '',
      cosReason: '',
      reasons: [],
      paramsMap: {},
      reasonMap: {},
      stateMap: {
        '1': {
          label: '已确认'
        },
        '2': {
          label: '待确认'
        },
        '9': {
          label: '未确认'
        },
      },
      mediaType: {
        '01': '电视',
        '02': '广播',
        '03': '报纸',
        '13': '互联网'
      }
    }
  },
  computed: {
    iframeSrc() {
      return `/Api/NetAd/net_ad_sample?fid=${this.activeSam.fid}`
    },
  },
  created() {
    this.adInfo = {...this.ad_info}
    this.sourceName = this.adInfo.fadname
    if (this.adInfo.samList.length) {
      this.activeSam = this.adInfo.samList[0]
    }
    this.initData()
  },
  mounted() {
    this.$nextTick(() => {
      this.setHeight()
      window.onresize = () => {
        this.setHeight()
      }
    })
  },
  methods: {
    rowStyle({ row }) {
      if (row.fadid === this.activeTheme.fadid) {
        return { 'cursor': 'pointer', 'background-color': '#409EFF', 'color': '#fff'}
      }
      return {'cursor': 'pointer'}
    },
    closeThemeDialog() {
      this.chooseDialog = false
      this.tableData = []
      this.activeTheme = {}
      this.adInfo.mainfadid = ''
    },
    getReason() {
      let reason = ''
      if (this.params && this.reason) {
        reason = `${this.params}${this.reason}:${this.paramsMap[this.params]}${this.reasonMap[this.reason]}`
      }
      this.reasons = Array.from(new Set([...this.reasons, reason]))
      this.params = ''
      this.reason = ''
    },
    initData() {
      const reasonData = localStorage.getItem('reasonData')
      if (reasonData) {
        this.setData(JSON.parse(reasonData))
      } else {
        this.getEditAdReason()
      }
    },
    getEditAdReason() {
      fly.get('/Admin/Ad/getEditAdReason').then(({ data }) => {
        if (data.code === 0) {
          this.setData(data.data)
          localStorage.setItem('reasonData', JSON.stringify(data.data))
        }
      })
    },
    setData(data) {
      this.paramsMap = data[10]
      this.reasonMap = data[20]
    },
    adNameBlur() {
      this.showClassTip = $h.testAdName(this.adInfo.fadname)
    },
    searchAdName(query,cb) {
      fly.get(`/Api/Ad/searchAdByName?adName=${query}`).then(({ data }) => {
        cb(data.data)
      })
    },
    autoWriteIn({value}) {
      fly.post('/Api/Ad/checkAdIsSureByName', Qs.stringify({ adName: value })).then(({ data }) => {
        if (data.code == 0) {
          this.adInfo.fadclasscode = data.data.fadclasscode
          this.adInfo.fadclasscode_v2 = data.data.fadclasscode_v2
          this.adInfo.fbrand = data.data.fbrand
          this.adInfo.adowner_name = data.data.fname
        }
      })
    },
    setHeight() {
      this.tableHeight = `${document.body.offsetHeight - 400}px`
      if (this.$refs.iframe) {
        this.$refs.iframe.$el.style.height = '397px'
        if(this.activeSam.media_class == 13) this.tableHeight = `${document.body.offsetHeight - 397 - 94}px`
      }
    },
    rowClick(row) {
      this.activeSam = row
    },
    saveTheme() {
      this.adInfo.mainfadid = this.activeTheme.fadid
      this.save()
    },
    save() {
      if (!this.adInfo.fadname) {
        return this.$notify.error('请选择填写广告名称')
      }
      let data = {
        ad_info: {
          ...this.adInfo,
        }
      }
      console.log(this.reasons, this.cosReason)
      if (!this.reasons.length && !this.cosReason) {
        this.loading = false
        return this.$notify.error('请选择或填写原因')
      }
      data.ad_info.freason = this.cosReason || this.reasons.join(';')
      if (!$h.checkAdNameAndCode(data.ad_info.fadname, data.ad_info.fadclasscode)) {
        this.loading = false
        return this.$message.error('公益广告请按此格式填写：公益（xxxxxx）')
      }
      fly.post("editAd", Qs.stringify(data)).then(({data}) => {
        if (data.code == 0) {
          this.$emit("save")
          this.$message.success(data.msg)
          this.adLoading = false
          this.$emit("close")
        } else if (data.code == 2) {
          this.chooseDialog = true
          this.tableData = data.data.map(i => {
            i._fadid = i.fadid.slice(-4)
            return i
          })
          this.activeTheme = this.tableData[0] || {}
        }else {
          this.$message.error(data.msg)
          this.adLoading = false
          this.$emit("close")
        }
      })
    },
    searchAdOwner(query, cb) {
      if (!query) {
        cb([]);
        return;
      }
      fly.post("/Api/Adowner/searchAdOwner", { name: query }).then( ({data}) => {
        cb(data.data)
      })
    }
  }
};
