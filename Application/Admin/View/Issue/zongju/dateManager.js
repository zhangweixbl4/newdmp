const dateManager = {
    props: ['task-type', 'fid'],
    template: `<div  v-loading="loading">
    <!--先选择月份-->
    <div v-if="!pickerMonth && taskType === 'add'">
        <el-row>
            <el-col :span="8" :offset="8">
                <h4> 请选择要抽查的月份</h4>
                <el-date-picker
                        v-model="pickerMonth"
                        type="month"
                        placeholder="选择月">
                </el-date-picker>
            </el-col>
        </el-row>
    </div>
    <!--后具体选择日期-->
    <div v-else>
        <el-tabs type="border-card" v-model="classActive">
            <el-tab-pane :label="item.label" :name="item.label" v-for="(item, index) in classArr" :key="item.label">
                <el-row align="middle">
                    <el-col :span="8">
                        <div class="datePicker">
                            <el-button :type="item.selected ? 'success' : 'info'" v-for="item in dateList" :key="item.day" @click="clickDay(item.day)" :disabled="item.disable">{{item.day}}</el-button>
                        </div>
                    </el-col>
                    <el-col :span="14">
                        <el-card class="box-card">
                            <div v-for="(date,index) in classArr[index].prop" :key="index" class="dateText" @click="deleteDate(index)" title="点击删除">
                                {{date | dateFilter(pickerMonth)}}
                            </div>
                        </el-card>
                    </el-col>
                </el-row>
            </el-tab-pane>
        </el-tabs>
        <div style="margin: 15px 0;" class="text-center">
            <el-button type="primary" @click="makeTask" v-if="taskType==='add'">生成抽查任务</el-button>
            <el-button type="primary" @click="editTask" v-else>修改抽查任务</el-button>
        </div>
    </div>
</div>
`,
    data: function () {
        return {
            pickerMonth: '',
            classActive: '电视',
            tvDates: [],
            bcDates: [],
            paperDates: [],
            loading: false
        }
    },
    computed: {
        month: function () {
            try {
                return this.pickerMonth.getMonth()
            }catch (e) {

            }
        },
        maxDay: function () {
            return moment(this.pickerMonth).daysInMonth()
        },
        classArr: function () {
            return [
                {
                    label: '电视',
                    prop: this.tvDates
                },
                {
                    label: '广播',
                    prop: this.bcDates
                },
                {
                    label: '报纸',
                    prop: this.paperDates
                },
            ]
        },
        activeDates: function(){
            var dates
            if (this.classActive == '' || this.classActive == 0){
                return []
            }
            if (this.classActive === '电视'){
                dates = this.tvDates
            }
            if (this.classActive === '广播'){
                dates = this.bcDates
            }
            if (this.classActive === '报纸'){
                dates = this.paperDates
            }
            return dates
        },
        dateList: function () {
            var a = []
            for (var i = 1; i <= this.maxDay; i++){
                var num = i
                if (num < 10){
                    num = '0'+num
                } else {
                    num += ''
                }
                if (this.activeDates.indexOf(num) !== -1){
                    a.push({
                        day: num,
                        selected: true
                    })
                }else {
                    a.push({
                        day: num,
                        selected: false
                    })
                }
            }
            let tempWeekDay = moment(this.pickerMonth).day()
            if (tempWeekDay === 0){
                tempWeekDay = 6
            }else{
                tempWeekDay -= 1
            }
            for (let i = 0; i < tempWeekDay; i++){
                a.unshift({
                    disable: true,
                    day: '',
                    selected: false,
                })

            }
            const dayArr = [
                {
                    day: '周一',
                    selected: false,
                    disable: true,
                },
                {
                    day: '周二',
                    selected: false,
                    disable: true,
                },
                {
                    day: '周三',
                    selected: false,
                    disable: true,
                },
                {
                    day: '周四',
                    selected: false,
                    disable: true,
                },
                {
                    day: '周五',
                    selected: false,
                    disable: true,
                },
                {
                    day: '周六',
                    selected: false,
                    disable: true,
                },
                {
                    day: '周日',
                    selected: false,
                    disable: true,
                },

            ]
            a = dayArr.concat(a)
            return a
        }
    },
    methods: {
        getInfo: function () {
            this.loading = true
            $.post('getAllDays', {id: this.fid}).then((res) => {
                if (res.code != 0){
                    this.$message.error(res.msg)
                    this.$emit('close-condition')
                } else{
                    var condition = JSON.parse(res.data.condition)
                    this.tvDates = condition.tv
                    this.bcDates = condition.bc
                    this.paperDates = condition.paper
                    this.pickerMonth = new Date(res.data.fmonth)
                    this.classActive = '电视'
                    this.loading = false
                }
            })
        },
        dataInit:function () {
            this.tvDates = []
            this.bcDates = []
            this.paperDates = []
            this.pickerMonth = ''
            this.classActive = '电视'
        },
        clickDay: function (index) {
            var dates = this.activeDates
            var i = dates.indexOf(index)
            if (i === -1){
                dates.push(index)
            } else {
                dates.splice(i, 1)
            }
        },
        deleteDate: function (index) {
            var dates  = this.activeDates
            dates.splice(index, 1)
        },
        makeTask: function () {
            if (this.tvDates.length ===0 && this.bcDates.length ===0 && this.paperDates.length ===0) {
                this.$message.error('条件不可为空')
                return false
            }
            var jsonCondition = {
                tv: this.tvDates.sort(),
                bc: this.bcDates.sort(),
                paper: this.paperDates.sort(),
            }

            jsonCondition = JSON.stringify(jsonCondition)
            var vm = this
            this.loading = true
            $.post("addTask", {
                condition: jsonCondition,
                month: moment(this.pickerMonth).format('YYYY-MM-DD')
            }, function (res) {
                if (res.code == 0) {
                    vm.$message.success(res.msg)
                    vm.$emit('close-condition')
                    vm.dataInit()
                }else {
                    vm.$message.error(res.msg)
                }
                vm.loading = false
            })
        },
        editTask: function () {
            if ((this.tvDates == null ||this.tvDates.length ===0) && (this.bcDates == null ||this.bcDates.length ===0) && (this.paperDates == null ||this.paperDates.length ===0)) {
                this.$message.error('条件不可为空')
                return false
            }
            var jsonCondition = {
                tv: this.tvDates.sort(),
                bc: this.bcDates.sort(),
                paper: this.paperDates.sort(),
            }
            jsonCondition = JSON.stringify(jsonCondition)
            var vm = this
            this.loading = true
            $.post("editTask", {
                condition: jsonCondition,
                id: this.fid,
            }, function (res) {
                if (res.code == 0) {
                    vm.$message.success(res.msg)
                    vm.$emit('close-condition')
                    vm.dataInit()
                }else {
                    vm.$message.error(res.msg)
                }
                vm.loading = false
            })
        }
    },
    filters: {
        dateFilter: function (value, month) {
            return moment(month).format('YYYY-MM-') + value
        }
    }
}