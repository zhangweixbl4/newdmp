<?php
namespace Api\Controller;
use Think\Controller;

class DataGatherController extends Controller {
	
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}	
	
	#同步汇总数据到分析型数据库
	public function syn_v3(){
		
		$dataList = M('tbn_ad_summary_day_v3')->where(array('syn_flag'=>0))->limit(1000)->select();
		$wait_write_list = [];
		$fids = [];
		foreach($dataList as $data){
			unset($data['syn_flag']);
			$wait_write_list[] = $data;
			$fids[] = $data['fid'];
			
		}
		#var_dump(C('FENXI_DB'));
		$writeState = M('tbn_ad_summary_day_v3','',C('FENXI_DB'))->addAll($wait_write_list,[],true);//写入分析型数据库汇总数据
		if($writeState){
			M('tbn_ad_summary_day_v3')->where(array('fid'=>array('in',$fids)))->save(array('syn_flag'=>1));
		}
		var_dump($writeState);
		
		#var_dump($fids);
		#var_dump($wait_write_list);
		
	}
	
	/*汇总半月数据*/
	public function gather_half_month(){
		session_write_close();
		set_time_limit(80);
		$month = I('month');//月份，例如2018-04-01表示4月
		$confirmed = I('confirmed');
		
		
		$dateInfo = A('Common/System','Model')->important_data('gather_half_month'.$confirmed);//获取处理过的日期
		$dateInfo = json_decode($dateInfo,true);//转为数组

		
		
		if(!$dateInfo){//如果是空的
			$month = date('Y-m-01',time() - 86400 * 30 * 6);
			$media_id = 0;
		}else{

			$month = $dateInfo['month'];//处理月份
			$media_id = $dateInfo['media_id'];//处理媒介id
		}
		
		
		//$mediaInfo = M('tmedia')->field('fid')->where(array('fid'=>array('gt',$media_id)))->order('fid')->find();//取出一个媒介
		
		$mediaInfoWhere = array();
		$mediaInfoWhere['issue_date'] = array('between',array($month,date('Y-m-31',strtotime($month))));
		$mediaInfoWhere['media_id'] = array('gt',$media_id);
		
		
		$mediaInfo = M('media_issue_count')->cache(true,2)->field('media_id as fid')->where($mediaInfoWhere)->group('media_id')->order('media_id')->find();

		if(!$mediaInfo){//判断能否查到媒介
			if(strtotime($month) > time()){//判断日期是否处理到了最近一天
				A('Common/System','Model')->important_data('gather_half_month'.$confirmed,'{}');
			}else{
				A('Common/System','Model')->important_data('gather_half_month'.$confirmed,
																				json_encode(array(
																									'month'=>date("Y-m-01", strtotime("+1 months", strtotime($month))),//时间戳加一个月
																									'media_id'=>0
																									)
																							)
															);//不是最近一天
			}
			exit;
		}else{
			$ff = A('Common/System','Model')->important_data('gather_half_month'.$confirmed,json_encode(array('month'=>$month,'media_id'=>$mediaInfo['fid'])));


		}
	
		

		A('Api/DataGather','Model')->gather_half_month($month,$mediaInfo['fid'],$confirmed);
		
		
	}
	
	/*汇总周数据*/
	public function gather_week(){
		session_write_close();
		set_time_limit(600);
		$confirmed = I('confirmed');
		$dateInfo = A('Common/System','Model')->important_data('gather_week'.$confirmed);//获取处理过的日期
		$dateInfo = json_decode($dateInfo,true);//转为数组

		if(!$dateInfo){//如果是空的
			$week_date = date('Y-m-d',time() - 86400 * 30 * 3);
			$media_id = 0;
		}else{
			$week_date = $dateInfo['week_date'];//时间戳加7天
			$media_id = $dateInfo['media_id'];//处理媒介id
		}
		
		$mediaInfoWhere = array();
		$mediaInfoWhere['issue_date'] = array('between',array($week_date,date('Y-m-d',strtotime($week_date) + 86400 * 7)));
		$mediaInfoWhere['media_id'] = array('gt',$media_id);
		$mediaInfo = M('media_issue_count')->cache(true,2)->field('media_id as fid')->where($mediaInfoWhere)->group('media_id')->order('media_id')->find();
		
		
		if(!$mediaInfo){//判断能否查到媒介
			if(strtotime($week_date) > time()){//判断日期是否处理到了最近一天
				A('Common/System','Model')->important_data('gather_week'.$confirmed,'{}');
			}else{
				A('Common/System','Model')->important_data('gather_week'.$confirmed,
																				json_encode(array(
																									'week_date'=>date("Y-m-d",strtotime($week_date) + 86400 * 7),//时间戳加一周
																									'media_id'=>0
																									)
																							)
															);//不是最近一天
			}
			exit;
		}else{
			$ff = A('Common/System','Model')->important_data('gather_week'.$confirmed,json_encode(array('week_date'=>$week_date,'media_id'=>$mediaInfo['fid'])));


		}
		
		
		A('Api/DataGather','Model')->gather_week($week_date,$mediaInfo['fid'],$confirmed);
		
		
		
	}
	
	
	
	/*汇总数据，月*/
	public function gather_month(){
		session_write_close();
		set_time_limit(80);
		$month = I('month');//月份，例如2018-04-01表示4月
		$confirmed = I('confirmed');
		$dateInfo = A('Common/System','Model')->important_data('gather_month'.$confirmed);//获取处理过的日期
		$dateInfo = json_decode($dateInfo,true);//转为数组

		
		
		if(!$dateInfo){//如果是空的
			$month = date('Y-m-01',time() - 86400 * 30 * 6);
			$media_id = 0;
		}else{

			$month = $dateInfo['month'];//处理月份
			$media_id = $dateInfo['media_id'];//处理媒介id
		}
		
		
		$mediaInfoWhere = array();
		$mediaInfoWhere['issue_date'] = array('between',array($month,date('Y-m-31',strtotime($month))));
		$mediaInfoWhere['media_id'] = array('gt',$media_id);
		$mediaInfo = M('media_issue_count')->cache(true,2)->field('media_id as fid')->where($mediaInfoWhere)->group('media_id')->order('media_id')->find();

		if(!$mediaInfo){//判断能否查到媒介
			if(strtotime($month) > time()){//判断日期是否处理到了最近一天
				A('Common/System','Model')->important_data('gather_month'.$confirmed,'{}');
			}else{
				A('Common/System','Model')->important_data('gather_month'.$confirmed,
																				json_encode(array(
																									'month'=>date("Y-m-01", strtotime("+1 months", strtotime($month))),//时间戳加一个月
																									'media_id'=>0
																									)
																							)
															);//不是最近一天
			}
			exit;
		}else{
			$ff = A('Common/System','Model')->important_data('gather_month'.$confirmed,json_encode(array('month'=>$month,'media_id'=>$mediaInfo['fid'])));


		}
	
		

		A('Api/DataGather','Model')->gather_month($month,$mediaInfo['fid'],$confirmed);
		
		
		
		
	}
	
	/*汇总数据，年*/
	public function gather_year(){
		session_write_close();
		set_time_limit(3600);
		$confirmed = I('confirmed');
		ini_set('memory_limit','256M');
		if(date('m') == '01'){
			$rand = rand(0,1);
		}else{
			$rand = 0;
		}
		
		$year = strval(intval(date('Y')) - $rand);
		
		echo $year;
		
		$where = array();
		
		$where['fdate'] = array('between',array($year.'-01-01',$year.'-12-31'));		
				
		
		$gatherYearList = M('tbn_ad_summary_half_year'.$confirmed)
														->field('
																	
																	sum(fad_times) as fad_times,
																	sum(fad_illegal_times) as fad_illegal_times,
																	sum(fad_illegal_times_total) as fad_illegal_times_total,
																	
																	sum(fad_play_len) as fad_play_len,
																	sum(fad_illegal_play_len) as fad_illegal_play_len,
																	sum(fad_illegal_play_len_total) as fad_illegal_play_len_total,
																	
																	fmediaid,
																	fad_class_code,
																	fregionid,
																	fmediaownerid,
																	fmedia_class_code,
																	concat(left(fdate,4),"-01-01") as fdate
																	
																	')	
														->where($where)
														
														->group('left(fdate,4),fad_class_code,fmediaid')
														//->page(rand(1,5).',1000')
														->select();
		//var_dump($gatherYearList);												
		
		$class_ad_arr = array();
		foreach($gatherYearList as $gatherYear){
			A('Api/DataGather','Model')->gather_common('tbn_ad_summary_year'.$confirmed,$gatherYear['fmediaid'],$gatherYear['fdate'],$gatherYear['fad_class_code'],array(
																								
																'fad_times'=>$gatherYear['fad_times'],
																'fad_illegal_times'=>$gatherYear['fad_illegal_times'],
																'fad_illegal_times_total'=>$gatherYear['fad_illegal_times_total'],
																
																'fad_play_len'=>$gatherYear['fad_play_len'],
																'fad_illegal_play_len'=>$gatherYear['fad_illegal_play_len'],
																'fad_illegal_play_len_total'=>$gatherYear['fad_illegal_play_len_total'],
																
																'fregionid'=>$gatherYear['fregionid'],
																'fmediaownerid'=>$gatherYear['fmediaownerid'],
																'fmedia_class_code'=>$gatherYear['fmedia_class_code'],
																'calc_sam'=>0,
																
																));	
			
			$class_ad_arr[$gatherYear['fmediaid']][] = $gatherYear['fad_class_code'];	
		}
		$AdClassList = A('Common/AdClass','Model')->getAdClassCode();
		
		foreach($AdClassList as $AdClass){
			
			foreach($class_ad_arr as $media_id => $class_ad){
				if(!in_array($AdClass,$class_ad)){
					$del_state = A('Api/DataGather','Model')->del_ad_class_data('tbn_ad_summary_year'.$confirmed,$media_id,$gatherYear['fdate'],$AdClass);//删除没有该分类数据的记录
					
				}
			}
			
		}
		//echo '1';
		
		
	}
	
	
	/*汇总数据，季度*/
	public function gather_quarter(){
		//exit;
		session_write_close();
		set_time_limit(1200);
		ini_set('memory_limit','256M');
		$confirmed = I('confirmed');
		
		$dateInfo = A('Common/System','Model')->important_data('gather_quarter'.$confirmed);//获取处理过的日期
		$dateInfo = json_decode($dateInfo,true);//转为数组

			
			
		if(!$dateInfo){//如果是空的
			$month =date("Y-m-01", strtotime("-12 months", time()));
			
		}else{

			$month = date("Y-m-01", strtotime("+3 months", strtotime($dateInfo['gather_quarter'])));//时间戳加一个月

		}
		A('Common/System','Model')->important_data('gather_quarter'.$confirmed,json_encode(array('gather_quarter'=>$month)));//写入新的处理过的日期
		if(strtotime($month) > strtotime(date('Y-m-01'))){//判断是否处理到最新日期了
			A('Common/System','Model')->important_data('gather_quarter'.$confirmed,'{}');
			exit;
		}
		$season = intval(ceil((date('n',strtotime($month)))/3));//获取季度	
		
		$year = date('Y',strtotime($month));

			
		A('Api/DataGather','Model')->gather_quarter($year,$season,$confirmed);		
		
	
	}
	
	
	
	/*汇总数据，半年*/
	public function gather_half_year(){
		//exit;
		session_write_close();
		set_time_limit(600);
		ini_set('memory_limit','256M');
		$confirmed = I('confirmed');
		
		$dateInfo = A('Common/System','Model')->important_data('gather_half_year'.$confirmed);//获取处理过的日期
		$dateInfo = json_decode($dateInfo,true);//转为数组
		
			
			
		if(!$dateInfo){//如果是空的
			$month =date("Y-m-01", strtotime("-12 months", time()));
			
		}else{

			$month = date("Y-m-01", strtotime("+6 months", strtotime($dateInfo['gather_half_year'])));//时间戳加一个月

		}
		A('Common/System','Model')->important_data('gather_half_year'.$confirmed,json_encode(array('gather_half_year'=>$month)));//写入新的处理过的日期
		if(strtotime($month) > strtotime(date('Y-m-01'))){//判断是否处理到最新日期了
			A('Common/System','Model')->important_data('gather_half_year'.$confirmed,'{}');
			exit;
		}
		$half_year = intval(ceil((date('n',strtotime($month)))/6));//获取季度	
		
		$year = date('Y',strtotime($month));

				
		A('Api/DataGather','Model')->gather_half_year($year,$half_year,$confirmed);		
		
	
	}
	
	
	/*删除关联媒介错误的记录*/
	public function del_err_data(){
		
		M('media_issue_count')->where('(select count(*) from tmedia where fid = media_issue_count.media_id) = 0')->delete();//删除按天汇总错误数据
		M('tbn_ad_summary_half_month')->where('(select count(*) from tmedia where fid = tbn_ad_summary_half_month.fmediaid) = 0')->delete();//
		M('tbn_ad_summary_half_year')->where('(select count(*) from tmedia where fid = tbn_ad_summary_half_year.fmediaid) = 0')->delete();//
		M('tbn_ad_summary_month')->where('(select count(*) from tmedia where fid = tbn_ad_summary_month.fmediaid) = 0')->delete();//
		M('tbn_ad_summary_quarter')->where('(select count(*) from tmedia where fid = tbn_ad_summary_quarter.fmediaid) = 0')->delete();//
		M('tbn_ad_summary_week')->where('(select count(*) from tmedia where fid = tbn_ad_summary_week.fmediaid) = 0')->delete();//
		M('tbn_ad_summary_year')->where('(select count(*) from tmedia where fid = tbn_ad_summary_year.fmediaid) = 0')->delete();//

		M('tbn_ad_summary_half_month_confirmed')->where('(select count(*) from tmedia where fid = tbn_ad_summary_half_month_confirmed.fmediaid) = 0')->delete();//
		M('tbn_ad_summary_half_year_confirmed')->where('(select count(*) from tmedia where fid = tbn_ad_summary_half_year_confirmed.fmediaid) = 0')->delete();//
		M('tbn_ad_summary_month_confirmed')->where('(select count(*) from tmedia where fid = tbn_ad_summary_month_confirmed.fmediaid) = 0')->delete();//
		M('tbn_ad_summary_quarter_confirmed')->where('(select count(*) from tmedia where fid = tbn_ad_summary_quarter_confirmed.fmediaid) = 0')->delete();//
		M('tbn_ad_summary_week_confirmed')->where('(select count(*) from tmedia where fid = tbn_ad_summary_week_confirmed.fmediaid) = 0')->delete();//
		M('tbn_ad_summary_year_confirmed')->where('(select count(*) from tmedia where fid = tbn_ad_summary_year_confirmed.fmediaid) = 0')->delete();//

		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}