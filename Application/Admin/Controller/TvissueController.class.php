<?php
//电视广告发行控制器
namespace Admin\Controller;
use Think\Controller;
use Think\Exception;


class TvissueController extends BaseController {

    public function index($data = []){
		session_write_close();
    	if(A('Admin/Authority','Model')->authority('200161') === 0){
			exit('没有权限');
		}

		if(!empty($data)){
			$_GET = $data;
		}

		if(I('region_id') != '') $region_id = I('region_id');
		$outtype = I('outtype');//导出类型
		if(!empty($outtype)){
			$p = 1;
			$pp = 999999;
			$addFields = '
				,tadowner.fname as fadownername
				,left(tmedia.fmediaclassid,2) mclass
				,ttvsample.favifilename ysscurl
				,ttvsample.fexpressions
				,ttvsample.fillegalcontent
				,ttvsample.fexpressioncodes
				,from_unixtime(ttvissue.fissuedate,"%Y-%m-%d") issuedate
				,from_unixtime(ttvissue.fstarttime,"%Y-%m-%d %H:%i:%s") starttime
				,from_unixtime(ttvissue.fendtime,"%Y-%m-%d %H:%i:%s") endtime
			';
		}else{
			$p = I('p',1);//当前第几页
			$pp = I('pageSize', 10);//每页显示多少记录
		}

		$keyword = I('keyword');//搜索关键词
		$fadname = I('fadname');// 广告名称
		$fversion = I('fversion');// 版本描述
		$fmediaid  = I('fmediaid');// 媒体ID
		$fmedianame = I('fmedianame');
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID
		$fstarttime_s = strtotime(I('fstarttime_s'));// 发布日期

		$fstarttime_e = strtotime(I('fstarttime_e'));// 发布日期

		$fcreator = I('fcreator');//创建人
		$sample_id = I('sample_id');
		$merge_media = I('merge_media');//是否合并媒介
		$adclass_code = I('adclass_code');//广告类别


		$this_region_id = I('this_region_id');//是否只查询本级地区
		$medialabel = I('medialabel');//媒介标签
		
		$provinceId = '10';

		$where = array();//查询条件
		if(intval($sample_id) > 0){
			$where['ttvissue.fsampleid'] = $sample_id;
		}

		if($region_id > 0 && $fmedianame == ''){//判断是否传入地区查询条件
			$provinceId = substr($region_id,0,2);
			if($this_region_id == 'true'){//判断是否只查询本级
				$where['ttvissue.fregionid'] = $region_id;//地区搜索条件
			}elseif($region_id != 100000){
				$region_id_rtrim = A('Common/System','Model')->get_region_left($region_id);//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(ttvissue.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}

		}


		if($fcreator != ''){
			$where['ttvissue.fcreator'] = array('like','%'.$fcreator.'%');
		}
		if($keyword != ''){

		} 
	
		if($fadname != '' && $adclass_code != '' && $adclass_code != '0'){//既搜索广告名称，又搜索广告分类
			
			$adclass_code_strlen = strlen($adclass_code);//获取code长度
			$where['ttvsample.fadid'] = array('exp',"in (
															select fadid 
															from tad 
															where left(tad.fadclasscode,".$adclass_code_strlen.") = '".$adclass_code."'
															and fadname like '%".$fadname."%'
														)");//广告id
			
		}else{
			if($fadname != ''){//搜索广告名
				$where['ttvsample.fadid'] = array('exp',"in (select fadid from tad where fadname like '%".$fadname."%')");//广告名称
			}
			if($adclass_code != '' && $adclass_code != '0'){//搜索广告分类
				
				$adclass_code_strlen = strlen($adclass_code);//获取code长度
				$where['ttvsample.fadid'] = array('exp',"in (select fadid from tad where left(tad.fadclasscode,".$adclass_code_strlen.") = '".$adclass_code."')");//广告id
			}
			
		}
		
		
		if($fversion != ''){
			$where['ttvsample.fversion'] = array('like','%'.$fversion.'%');//版本描述
		}
		
		
		if($medialabel != ''){
			$mediaIdList = M('tmedialabel')
										->cache(true,60)
										->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
										->where(array('tlabel.flabel'=>$medialabel))
										->getField('fmediaid',true);
			if($mediaIdList){
				$where['ttvissue.fmediaid'] = array('in',$mediaIdList);	
			}else{
				$where['ttvissue.fmediaid'] = 0;
			}							
		}
		
		if($fmediaid != ''){//判断是否查询媒介
		
			$mediaInfo = M('tmedia')->cache(true,600)
									->field('tmedia.fid,fregionid')
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->where(array('tmedia.fid'=>$fmediaid))
									->find();//查询媒介信息，用于按媒介查询
			$provinceId = substr($mediaInfo['fregionid'],0,2);	
			$where['ttvissue.fmediaid'] = $fmediaid;//媒体ID
			$_GET['region_fullname'] = '全国';
			$_GET['region_id'] = 100000;

		}elseif($fmedianame != ''){

			$mediaInfo = M('tmedia')->cache(true,600)
									->field('tmedia.fid,fregionid')
									->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
									->where(array('tmedia.fmedianame'=>$fmedianame))
									->find();//查询媒介信息，用于按媒介查询
			$provinceId = substr($mediaInfo['fregionid'],0,2);						
			$where['ttvissue.fmediaid'] = $mediaInfo['fid'];//媒体ID
			$_GET['region_fullname'] = '全国';
			$_GET['region_id'] = 100000;
		}
		
		
		
		 
		if(is_array($fillegaltypecode)){
			$where['ttvsample.fillegaltypecode'] = array('in',$fillegaltypecode);//违法类型代码
		}


		$where['ttvissue.fissuedate'] = array('between',$fstarttime_s.','.$fstarttime_e);//查询时间段
		

		
		$issue_table_name = 'ttvissue_' . date('Ym',$fstarttime_s) . '_' . $provinceId ;//发布表的名称
	
		try{//尝试查询符合条件的记录总数
			$count = M($issue_table_name)
				->alias('ttvissue')
				->cache(true,60)
				->join('ttvsample on ttvsample.fid = ttvissue.fsampleid')
				->join('tad on tad.fadid = ttvsample.fadid')
				->join('tadclass on tad.fadclasscode = tadclass.fcode')
				->join('tadowner on tad.fadowner = tadowner.fid')
				->join('tmedia on tmedia.fid = ttvissue.fmediaid')
				->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
				->join('tillegaltype on tillegaltype.fcode = ttvsample.fillegaltypecode')
				->where($where)->count();// 查询满足要求的总记录数
		}catch(Exception $error) { //如果创建失败
			$count = 0;
		} 
						
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数

		if($count > 0){
			$tvissueList = M($issue_table_name)
				->alias('ttvissue')
				->cache(true,60)
				->field('
					ttvissue.*,
					tad.fadname,
					ttvsample.fversion,
					ttvsample.favifilename,
					"'. $issue_table_name .'" as table_name,
					tmedia.fmedianame,
					tillegaltype.fillegaltype,
					tadclass.ffullname as fadclassfullname
				'.$addFields)
				->join('ttvsample on ttvsample.fid = ttvissue.fsampleid')
				->join('tad on tad.fadid = ttvsample.fadid')
				->join('tadclass on tad.fadclasscode = tadclass.fcode')
				->join('tadowner on tad.fadowner = tadowner.fid')
				->join('tmedia on tmedia.fid = ttvissue.fmediaid')
				->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
				->join('tillegaltype on tillegaltype.fcode = ttvsample.fillegaltypecode')
				->where($where)
				->order('ttvissue.fissuedate desc , ttvissue.fstarttime desc')
				->limit($Page->firstRow.','.$Page->listRows)->select();//查询广告样本列表
		}

		//数据导出
		if(!empty($outtype)){
			if(empty($tvissueList)){
				$this->ajaxReturn(array('code'=>1,'msg'=>'生成失败，暂无数据'));
			}
	        $ret = A('Issue')->outIssueXls($tvissueList,'01');
	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}				

		$tvissueList = list_k($tvissueList,$p,$pp);//为列表加上序号
		
		
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型

		$this->assign('tvissueList',$tvissueList);//广告发布列表
		$this->assign('illegaltype',$illegaltype);//违法类型
		$this->assign('page',$Page->show());//分页输出

	}

	
	
	/*广告发布详情*/
	public function ajax_tvissue_details(){
		
		$issueid = I('issueid');//获取发布ID
		$table_name = I('table_name');
		$tvissueDetails = M($table_name)
										->alias('ttvissue')
										->field(
												'ttvissue.*,
												tad.fadname,
												ttvsample.fversion,
												ttvsample.fillegalcontent,
												ttvsample.fexpressioncodes,
												ttvsample.fexpressions,
												ttvsample.fadmanuno,
												ttvsample.fmanuno,
												ttvsample.fadapprno,
												ttvsample.fapprno,
												ttvsample.fadent,
												ttvsample.fent,
												ttvsample.fentzone,
												
												tmedia.fmedianame,
												tillegaltype.fillegaltype,
												tadclass.ffullname as adclass_fullname
												')
										->join('ttvsample on ttvsample.fid = ttvissue.fsampleid')
										->join('tad on tad.fadid = ttvsample.fadid')
										->join('tadclass on tadclass.fcode = tad.fadclasscode')
										->join('tadowner on tadowner.fid = tad.fadowner')
										->join('tillegaltype on tillegaltype.fcode = ttvsample.fillegaltypecode')
										->join('tmedia on tmedia.fid = ttvissue.fmediaid')
										->where(array('ttvissue.fid'=>$issueid))
										->find();//查询样本详情

		$tvissueDetails['fstarttime'] = date('Y-m-d H:i:s',$tvissueDetails['fstarttime']);
		$tvissueDetails['fendtime'] = date('Y-m-d H:i:s',$tvissueDetails['fendtime']);
		
		$source_info = S('issue_m3u8_'.$table_name.'_'.$issueid);
		$source_url = $source_info['source_url'];
		$ad_part = array(
							'source_url'=>strval($source_url),

							);

		
		
		$this->ajaxReturn(array('code'=>0,'tvissueDetails'=>$tvissueDetails,'ad_part'=>$ad_part));
	}
	

	
	/*删除发布记录*/
	public function del_issue(){
		if(A('Admin/Authority','Model')->authority('200162') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));
		}
		M()->startTrans();//开启事务方法
		$issueid = I('issueid');//获取发布ID
		$table_name = I('table_name');
		
		
		$where['fid'] = $issueid;
		
		$issueInfo = M($table_name)->where($where)->find();
		

		$del_y_issue = M('ttvissue')->where(array('fmediaid'=>$issueInfo['fmediaid'],'fstarttime'=>date('Y-m-d H:i:s',$issueInfo['fstarttime'])))->delete();
		
	
		$rr = M($table_name)->where($where)->delete();
		M()->commit();//事务回滚方法
		if($rr){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功','del_y_issue'=>$del_y_issue));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败'));
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}