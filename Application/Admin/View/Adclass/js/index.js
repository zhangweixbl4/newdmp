var vm = new Vue({
    el: '#app',
    data: {
        adClassTree: adClassTree,
        adClassV2List: adClassV2List,
        adClass: []
    },
    computed:{
        ids: function () {
            return this.adClassV2List.map((item) => item.fcode)
        }
    },
    methods:{
        delItem: function (index) {
            this.adClassV2List.splice(index, 1)
        },
        add: function () {
            if (this.adClass.length !== 0){
                var fcode = this.adClass[this.adClass.length - 1]
                if (this.ids.indexOf(fcode) !== -1){
                    this.$message.error('该类型已关联')
                    return false
                }
                var ffullname = this.$refs.adClassSelector.currentLabels.join('>')
                var obj = {
                    ffullname: ffullname,
                    fcode: fcode
                }
                this.adClassV2List.push(obj)
            } else{
                this.$message.error('请选择商业类别, 然后点击添加')
            }
        },
        save: function () {
            this.$confirm('确认提交?').then(() => {
                $.post('', {ids: this.ids.join(',')}).then((res) => {
                    if (res.code == 0){
                        this.$message.success(res.msg)
                        parent.location.reload()
                        close()
                    } else {
                        this.$message.error(res.msg)
                    }
                })
            }).catch(() => {})
        }
    }
})