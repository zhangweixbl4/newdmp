	
	
	$(function () { $(".i-checks").iCheck({ checkboxClass: "icheckbox_square-green", radioClass: "iradio_square-green", }) });
	
	/*设备运行状态*/
	function device_run_param(run_param){
		
			$('#device_collect_time').html(run_param.frunparamupdatetime);//显示更新时间
			$('#run_param_device_name').html(run_param.fname);//显示设备名称
			$('#device_operating_system').html(run_param.foperatingsystem);//显示操作系统的名称
			
			
			var req = JSON.parse(run_param.frunparam);//json字符串转为json对象

			let diskName = [];
			let diskFree = [];
			let diskUse = [];
			let cpuUse = (req.cpu_use_ratio * (Math.random()*10+90)).toFixed(2);//cpu使用率数字计算
			let memoryUse = req.memory_use_ratio;//内存使用率数字计算
			if(memoryUse == 0) memoryUse = 0.1;
			memoryUse = (memoryUse * (Math.random()*10+90)).toFixed(2);//内存使用率数字计算
			
			for (let i = 0; i < req.disk.length; i++) {
				diskName.unshift(req.disk[i].name);//磁盘名称
				diskFree.unshift(req.disk[i].free);//磁盘可用空间
				diskUse.unshift(req.disk[i].use);//磁盘已使用空间

			}
			
			//以下是开始装载磁盘信息数据
			let disk = echarts.init(document.getElementById('disk'));
			let diskOption = {
				tooltip: {
					trigger: 'axis',
					axisPointer: {            // 坐标轴指示器，坐标轴触发有效
						type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
					}
				},
				color: ['#f8ac59', '#1ab394'],
				legend: {data: ['已使用(MB)', '未使用(MB)']},
				grid: {left: '3%',right: '4%',bottom: '3%',containLabel: true},
				xAxis: {type: 'value'},
				yAxis: {type: 'category',data: diskName},
				series: [
					{name: '已使用(MB)',type: 'bar',stack: '总量',label: {normal: {show: true,position: 'insideRight'}},data: diskUse},
					{name: '未使用(MB)',type: 'bar',stack: '总量',label: {normal: {show: true,position: 'insideRight'}},data: diskFree}
				]
			};
			//以上是装载磁盘信息数据
			
			//以下是开始装载CPU的数据
			let CPU = echarts.init(document.getElementById('CPU'));
			let CPUOption = {
				tooltip: {formatter: "{a} <br/>{b} : {c}%"},
				toolbox: {feature: {}},
				series: [
					{
						name: '占用率',
						type: 'gauge',
						detail: { formatter: '{value}%', textStyle: { fontSize: 30 } },
						data: [{ value: cpuUse, name: 'CPU占用率' }],
						radius: '90%',
						splitNumber: 20,
						axisLine: {lineStyle: { width: 15,color: [[0.2, '#1ab394'], [0.8, '#f8ac59'], [1, '#ed5565']]},},
						splitLine: {length: 15},
						axisTick: {length: 8},
						pointer: {width: 10,length:80},
						axisLabel: {textStyle: {fontSize: 12}},
					
					}
				]
			};
			//以上是装载CPU数据
			

			//以下开始装载内存的数据
			let memory = echarts.init(document.getElementById('memory'));
			let memoryOption = {
				tooltip: {formatter: "{a} <br/>{b} : {c}%"},
				toolbox: {feature: {}},

				series: [
					{
						name: '占用率',
						type: 'gauge',
						detail: { formatter: '{value}%', textStyle: { fontSize: 30 } },
						data: [{ value: memoryUse, name: '内存占用率' }],
						radius: '90%',
						splitNumber: 20,
						axisLine: {lineStyle: {width: 15,color: [[0.2, '#1ab394'], [0.8, '#f8ac59'], [1, '#ed5565']]},},
						splitLine: {length: 15},
						axisTick: {length: 8},
						pointer: { width: 10,length:80},
						axisLabel: {textStyle: {fontSize: 12}},
					}
				]
			};
			//以上是装载内存数据
			
			disk.setOption(diskOption);//执行磁盘数据柱状图显示
			CPU.setOption(CPUOption);//执行CPU数据仪表盘显示
			memory.setOption(memoryOption);//执行内存数据仪表盘显示
	}
	
	
	
	