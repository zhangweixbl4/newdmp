Vue.prototype.$ELEMENT = {
    size: 'mini',
    zIndex: 3000
}
var vm = new Vue({
    el: '#app',
    data: {
        where: {},
        adLoading: false,
        tableLoading: false,
        showAdInfo: false,
        adClassArr: adClassArr,
        adClassTree: adClassTree,
        searchAdClassArr: [],
        searchAdClassArr2: [],
        page: 1,
        total: 0,
        table: [],
        adInfo: {},
        searchAdOwnerLoading: false,
        searchAdOwnerRes: [],
    },
    created: function () {
        this.search()
    },
    methods: {
        search: function () {
            this.page = 1
            if (this.searchAdClassArr.length !== 0) {
                this.where.adClass = this.searchAdClassArr[this.searchAdClassArr.length - 1]
            }
            if (this.searchAdClassArr2.length !== 0) {
                this.where.adClass2 = this.searchAdClassArr2[this.searchAdClassArr2.length - 1]
            }

            this.searchReq()
        },
        searchReq: function () {
            this.tableLoading = true
            var vm = this
            $.post('', {
                where: this.where,
                page: this.page
            }, function (res) {
                if (res.code == 0) {
                    vm.table = res.data
                    vm.total = +res.total
                } else {
                    vm.$message.error('获取列表失败')
                }
                vm.tableLoading = false
            })
        },
        resetWhere: function () {
            this.where = {}
            this.searchAdClassArr = []
            this.searchAdClassArr2 = []
            this.page = 1
            this.searchReq()
        },
        pageChange: function (page) {
            this.page = page
            this.searchReq()
        },
        openAdInfo: function (row) {
            this.adLoading = true
            this.showAdInfo = true
            var vm = this
            $.post('/Api/Ad/ajax_ad_details', {
                fadid: row.fadid
            }, function (res) {
                if (res.code == 0) {
                    vm.adInfo = res.adDetails
                    vm.adInfo.adClass = []
                    vm.adInfo.adClass.push(vm.adInfo.fadclasscode.substr(0, 2))
                    vm.adInfo.adClass.push(vm.adInfo.fadclasscode)
                    vm.adInfo.adClass2 = []
                    for (var i = 2; i <= vm.adInfo.fadclasscode_v2.length; i += 2) {
                        vm.adInfo.adClass2.push(vm.adInfo.fadclasscode_v2.substr(0, i))
                    }

                } else {
                    vm.$message.error('获取详情失败')
                    vm.closeAdInfo()
                    vm.searchReq()
                }
                if (res.adDetails.is_sure != 2) {
                    vm.adInfo.is_sure = 0
                }

                vm.adLoading = false
            })
        },
        closeAdInfo: function () {
            this.adLoading = false
            this.showAdInfo = false
            this.adInfo = {}
        },
        save: function () {
            this.adLoading = true
            this.adInfo.adClassCode = this.adInfo.adClass[this.adInfo.adClass.length - 1]
            this.adInfo.adClassCodeV2 = this.adInfo.adClass2[this.adInfo.adClass2.length - 1]
            var vm = this;
            $.post('/Api/Ad/updateAdPartialInfo', {
                adInfo: this.adInfo
            }, function (res) {
                if (res.code == 0) {
                    vm.$message.success(res.msg)
                } else {
                    vm.$message.error(res.msg)
                }
                vm.adLoading = false
                vm.closeAdInfo();
                vm.searchReq()
            })

        },
        searchAdOwner: function (query, cb) {
            if (!query) {
                cb([]);
                return
            }
            $.post("/Api/Adowner/searchAdOwner", {
                name: query
            }, function (res) {
                cb(res.data);
            });
        },
        inputAdName: function () {
            this.adLoading = true
            $.post('/Api/Ad/checkAdIsSureByName', {
                adName: this.adInfo.fadname
            }).then((res) => {
                this.adInfo.is_sure = res.data.is_sure
                let tempAdId = this.adInfo.fadid
                // 只要找到广告信息, 就更新到任务中
                if (res.data.fadid) {
                    this.adInfo = Object.assign(this.adInfo, res.data)
                    this.adInfo.adClass = this._processAdClass(this.adInfo.fadclasscode)
                    this.adInfo.adClass2 = this._processAdClass(this.adInfo.fadclasscode_v2)
                    this.adInfo.adowner_name = this.adInfo.fname
                }
                this.adInfo.fadid = tempAdId
                this.$forceUpdate()
                this.adLoading = false
            })
        },
        _processAdClass: function (data) {
            let adClass = [];
            if (data.length === 6) {
                adClass.push(data.substr(0, 2))
                adClass.push(data.substr(0, 4))
                adClass.push(data)
            }
            if (data.length === 4) {
                adClass.push(data.substr(0, 2))
                adClass.push(data)
            }
            if (data.length === 2) {
                adClass.push(data)
            }
            return adClass
        },
        searchAdName: function (queryString, cb) {
            $.post("/Api/Ad/searchAdByName", {
                adName: queryString
            }, function (res) {
                cb(res.data);
            });
        },
        changeFormAdClass2: function (value) {
            let code = value[value.length - 1]
            $.post('/Api/Adclass/getAdClassA', {
                code: code
            }).then((res) => {
                if (res.code == 0) {
                    let adClass = [];
                    if (res.data.length === 4) {
                        adClass.push(res.data.substr(0, 2))
                        adClass.push(res.data)
                    }
                    if (res.data.length === 2) {
                        adClass.push(res.data)
                    }
                    Vue.set(this.adInfo, 'adClass', adClass)
                    this.$forceUpdate()
                } else {
                    this.$message('该分类未关联广告类别, 请手动选择')
                }
            })
        }

    }
})