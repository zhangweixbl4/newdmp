<?php
namespace Open\Controller;
use Think\Controller;
use Think\Exception;


class AdvertorialController extends BaseController {
	
	
	
	public function _initialize() {
		header('Access-Control-Allow-Origin:*');
	}
	
	/*软广告类型*/
	public function type(){
		$media_class = I('get.media_class');
		
		$dataList = M('tadvertorial_type','',C('ZIZHI_DB'))->field('fid,fpid,fname,ffull_name')->where(array('fstatus'=>1,'fmedia_class_id'=>$media_class))->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'','dataList'=>$dataList));
		
	}
	
	/*添加软广告*/
	public function add(){
		
		$postData = file_get_contents('php://input');
		$postArr = json_decode($postData,true);
		
		$mediaInfo = M('tmedia')->cache(true,600)->field('fid,left(fmediaclassid,2) as media_class')->where(array('fid'=>$postArr['fmedia_id']))->find();
		
		if(!$mediaInfo){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介id错误'));
		}
		
		if($mediaInfo['media_class'] == '01'){
			$tableName = 'ttv_advertorial';
		}elseif($mediaInfo['media_class'] == '02'){
			$tableName = 'tbc_advertorial';
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介类型错误'));
		}
		
		$addData = array();
		
		$addData['fmedia_id'] = $postArr['fmedia_id']; #'媒体ID',
		$addData['fissue_date'] = date('Y-m-d',strtotime($postArr['fstart_time']));  #'发布日期',
		$addData['fstart_time'] = $postArr['fstart_time'];  #'开始时间',
		$addData['fend_time'] = $postArr['fend_time'];  #'结束时间',
		$addData['fprog_name'] = $postArr['fprog_name'];  #'节目主题',
		$addData['fhost'] = $postArr['fhost'];  #'主持人',
		$addData['fad_name'] = $postArr['fad_name'];  #'广告主题',
		$addData['fadvertorial_type_id'] = $postArr['fadvertorial_type_id'];  #'软广类型',
		$addData['ftel'] = $postArr['ftel'];  #'电话',
		$addData['faddr'] = $postArr['faddr'];  #'地址',
		$addData['fother'] = $postArr['fother'];  #'其他',
		$addData['fcreator'] = $postArr['fcreator'] ? $postArr['fcreator'] : get_client_ip();  #'创建人',
		$addData['fcreate_time'] = date('Y-m-d H:i:s');  #'创建时间',
		$addData['fmodifier'] = $postArr['fcreator'] ? $postArr['fcreator'] : get_client_ip(); # '修改人',
		$addData['fmodify_time'] = date('Y-m-d H:i:s');  #'修改时间',
		
		$fid = M($tableName,'',C('ZIZHI_DB'))->add($addData);
		if($fid){
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功','fid'=>$fid));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败'));
		}
		
	}
	
	/*修改软广*/
	public function edit(){
		
		$postData = file_get_contents('php://input');
		$postArr = json_decode($postData,true);
		
		$mediaInfo = M('tmedia')->cache(true,600)->field('fid,left(fmediaclassid,2) as media_class')->where(array('fid'=>$postArr['fmedia_id']))->find();
		
		if(!$mediaInfo){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介id错误'));
		}
		
		if($mediaInfo['media_class'] == '01'){
			$tableName = 'ttv_advertorial';
		}elseif($mediaInfo['media_class'] == '02'){
			$tableName = 'tbc_advertorial';
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介类型错误'));
		}
		
		
		$dataInfo =  M($tableName,'',C('ZIZHI_DB'))->where(array('fid'=>$postArr['fid'],'fmedia_id'=>$postArr['fmedia_id']))->find();
		
		if(!$dataInfo){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'不存在的fid'));
		}


		if( array_key_exists('fstart_time',$postArr) && $dataInfo['fissue_date'] != date('Y-m-d',strtotime($postArr['fstart_time']))){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'不允许跨天修改软广时间'));
		}


		$editData = array();

		if(array_key_exists('fstart_time',$postArr))		$editData['fstart_time'] = $postArr['fstart_time'];
		if(array_key_exists('fend_time',$postArr))		$editData['fend_time'] = $postArr['fend_time'];
		if(array_key_exists('fprog_name',$postArr))		$editData['fprog_name'] = $postArr['fprog_name'];
		if(array_key_exists('fhost',$postArr))	$editData['fhost'] = $postArr['fhost'];
		if(array_key_exists('fad_name',$postArr))		$editData['fad_name'] = $postArr['fad_name'];
		if(array_key_exists('fadvertorial_type_id',$postArr))		$editData['fadvertorial_type_id'] = $postArr['fadvertorial_type_id'];
		if(array_key_exists('ftel',$postArr))		$editData['ftel'] = $postArr['ftel'];
		if(array_key_exists('faddr',$postArr))		$editData['faddr'] = $postArr['faddr'];
		if(array_key_exists('fother',$postArr))		$editData['fother'] = $postArr['fother'];

		$editData['fmodifier'] = $postArr['fmodifier'] ? $postArr['fmodifier'] : get_client_ip(); # '修改人',
		$editData['fmodify_time'] = date('Y-m-d H:i:s');  #'修改时间',
		#var_dump(array('fid'=>$postArr['fid'],'fmedia_id'=>$postArr['fmedia_id']));
		
		$eT =  M($tableName,'',C('ZIZHI_DB'))->where(array('fid'=>$postArr['fid'],'fmedia_id'=>$postArr['fmedia_id']))->save($editData);
		
		
		if($eT){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败'));
		}
		
		
		
		
	}
	
	/*查询软广列表*/
	public function get_list(){
		$fmedia_id = I('get.fmedia_id');
		$fissue_date = I('get.fissue_date');
		
		$mediaInfo = M('tmedia')->cache(true,600)->field('fid,left(fmediaclassid,2) as media_class')->where(array('fid'=>$fmedia_id))->find();
		
		if(!$mediaInfo){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介id错误'));
		}
		
		if($mediaInfo['media_class'] == '01'){
			$tableName = 'ttv_advertorial';
		}elseif($mediaInfo['media_class'] == '02'){
			$tableName = 'tbc_advertorial';
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介类型错误'));
		}
		
		$dataList = M($tableName,'',C('ZIZHI_DB'))->alias('advertorial')
								
								->field('
								
												
											(select fname from tadvertorial_type where fid = advertorial.fadvertorial_type_id) as type_name,
											(select ffull_name from tadvertorial_type where fid = advertorial.fadvertorial_type_id) as type_full_name,
											
											advertorial.fid,
											advertorial.fmedia_id,
											advertorial.fstart_time,
											advertorial.fend_time,
											advertorial.fprog_name,
											advertorial.fhost,
											advertorial.fad_name,
											advertorial.fadvertorial_type_id,
											advertorial.ftel,
											advertorial.faddr,
											advertorial.fother
											
										')
								
								->where(array('advertorial.fmedia_id'=>$fmedia_id,'advertorial.fissue_date'=>$fissue_date))->select();
		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','dataList'=>$dataList));
		
	
	}
	
	/*删除软广*/
	public function del(){
		$postData = file_get_contents('php://input');
		$postArr = json_decode($postData,true);
		
		$mediaInfo = M('tmedia')->cache(true,600)->field('fid,left(fmediaclassid,2) as media_class')->where(array('fid'=>$postArr['fmedia_id']))->find();
		
		if(!$mediaInfo){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介id错误'));
		}
		
		if($mediaInfo['media_class'] == '01'){
			$tableName = 'ttv_advertorial';
		}elseif($mediaInfo['media_class'] == '02'){
			$tableName = 'tbc_advertorial';
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'媒介类型错误'));
		}
		
		$delC = M($tableName,'',C('ZIZHI_DB'))->where(array('fid'=>$postArr['fid'],'fmedia_id'=>$postArr['fmedia_id']))->delete();
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功','delC'=>$delC));
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
}