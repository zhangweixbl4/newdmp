<?php
namespace Api\Model;
use Think\Exception;
use OpenSearch\Client\OpenSearchClient;
use OpenSearch\Client\DocumentClient;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;


class AdModel{



	public function searchAdByName($adName = '',$fcreator_id = '',$fpriority){
        header('Access-Control-Allow-Origin:*');//允许跨域
		$client = A('Common/OpenSearch','Model')->client();//获得OpenSearchClient
		$searchClient = new SearchClient($client);

		$params = new SearchParamsBuilder();
		$params->setStart(0);//起始位置

		$params->setHits(20);//返回数量

		$params->setAppName('ad_name_search');//设置应用名称
		
		$where = array();
		$whereFilter = array();
		
		
		$where['fadname'] = $adName;
		
		
		
		if($fcreator_id){ #有传入创建人ID的情况
			$where['fcreator_id'] = $fcreator_id; #创建人id等于自己
			$where['fstate'] = 0; #状态等于
		}else{ #没有传入创建人的情况
			$whereFilter['fstate'] = array('in',array('1','2','9')); #过滤状态
		}
		
		if($fpriority){
			$whereFilter['fpriority'] = array('in',explode('|',$fpriority));
		}
		
		if($whereFilter){
			$setFilter = A('Common/OpenSearch','Model')->setFilter($whereFilter);//组装过滤条件
		}
		
		
		$setQuery = A('Common/OpenSearch','Model')->setQuery($where);//组装搜索字句
		
		if($setFilter) $params->setFilter($setFilter);//设置文档过滤条件
		#var_dump($setQuery,$setFilter);
		
		$params->setQuery($setQuery);//设置搜索
		$params->setSecondRankName('sort1');// 指定精排表达式
		$params->setCustomConfig('rerank_size',2000);// 
		
		
		
	
		#$params->addSort('fpriority', SearchParamsBuilder::SORT_DECREASE);//排序
		#$params->addSort('field_match_ratio(fadname)');//排序


		$params->setFormat("json");//数据返回json格式
		
		$params->setFetchFields(array(
										'identify',//识别码（哈希）
										'fadname',
										#'fieldlen(identify)',
									
									));//设置需返回哪些字段
		
		
		$ret = $searchClient->execute($params->build());//执行查询并返回信息
		

		$result = json_decode($ret->result,true);//
		#var_dump($result);
		
		$adNameList = [];
		foreach($result['result']['items'] as $items){
			$adNameList[] = array('value'=>$items['fadname']);
			
		}
		
		return $adNameList;
		
	}
	
	#删除搜索联想的广告
	public function delOpenAd($fadid){
		$identify = md5($fadid);
		#var_dump($identify);
		$delete_open_datas = [];
		$delete_open_datas[] = array('cmd'=>'delete','fields'=>array('identify'=>$identify));//组装需要删除的数据
			
			
			
		$delete_state = A('Common/OpenSearch','Model')->push('tad',$delete_open_datas,'ad_name_search');  //删除数据


		return json_decode($delete_state->result,true)['result'];
			
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}



