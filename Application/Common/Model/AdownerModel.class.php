<?php
namespace Common\Model;
use Think\Exception;
class AdownerModel{
	/*通过广告主名称获取广告主ID*/
	public function get_ad_owner_id($adOwnerName,$creator){
		$adOwnerName = strval($adOwnerName);
		$creator = strval($creator);
		if($creator == ''){
			$creator = '未知';
		}
		if($adOwnerName == ''){
			return 0;
		}
		$adOwnerInfo = S('ad_owner_name_id_'.md5($adOwnerName));
		if(!$adOwnerInfo){
			$adOwnerInfo = M('tadowner')->field('fid')->where(array('fname'=>$adOwnerName))->find();
			if($adOwnerInfo){
				S('ad_owner_name_id_'.md5($adOwnerName),$adOwnerInfo,864000);
			}
		}
		if($adOwnerInfo){
			return $adOwnerInfo['fid'];
		}else{
			$a_data = array();
			$a_data['fregionid'] = 100000;
			$a_data['fname'] = $adOwnerName;
			$a_data['fcreator'] = $creator;
			$a_data['fcreatetime'] = date('Y-m-d H:i:s');
			$a_data['fmodifier'] = $creator;
			$a_data['fmodifytime'] = date('Y-m-d H:i:s');
			$a_data['fstate'] = 1;
			$adOwnerId = M('tadowner')->add($a_data);
			return $adOwnerId;
		}
	}
	/**
	 * @Des: 获取广告主列表
	 * @Edt: yuhou.wang
	 * @param {type} 
	 * @return: 
	 */
	public function getList(){
		$data = M('tadowner')
			->cache(true,86400*10)
			->field('fid,fname')
			->where(['fstate'=>1])
			->select();
		return $data;
	}
}