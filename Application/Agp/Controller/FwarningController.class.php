<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 预警任务管理
 */

class FwarningController extends BaseController
{
	/**
	 * 获取预警列表
	 * by zw
	 */
	public function index()
	{	
		$p = I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录
		$system_num = getconfig('system_num');

		$where_wtk['wtk_customer'] = $system_num;
		$where_wtk['wtk_status'] = array('gt',0);
		$where_wtk['_string'] = '(wtk_trepid='.session('regulatorpersonInfo.fregulatorpid').' and wtk_status>0) or (wtk_object=0 and wtk_status=10) or (wtk_object='.session('regulatorpersonInfo.fregulatorpid').' and wtk_status=10)';

		$count = M('warning_task')
			->where($where_wtk)
			->count();

		$Page 	= new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$do_wtk = M('warning_task')
			->field('ifnull(b.ffullname,"所有机构") as ffullname,warning_task.*')
			->join('tregulator b on warning_task.wtk_object=b.fid','left')
			->where($where_wtk)
			->order('wtk_id desc')
			->page($p,$pp)
			->select();
		foreach ($do_wtk as $key => $value) {
			$do_wtk[$key]['wtk_content'] = htmlspecialchars_decode($value['wtk_content']);
		}

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_wtk)));
	}

	/**
	 * 添加预警
	 * by zw
	 */
	public function add_warning(){
		$system_num = getconfig('system_num');

		$wtk_warningtime = I('wtk_warningtime');//预警时间
		$wtk_title 		= I('wtk_title');//预警提示
		$wtk_content 	= I('wtk_content');//预警内容
		$wtk_type 		= I('wtk_type');//预警类型
		$wtk_object 	= I('wtk_object');//预警对象
		$wtk_files 	= I('wtk_files');//预警附件

		$data_wtk['wtk_userid'] 	= session('regulatorpersonInfo.fid');
		$data_wtk['wtk_customer'] 	= $system_num;
		$data_wtk['wtk_trepid'] 	= session('regulatorpersonInfo.fregulatorpid');
		$data_wtk['wtk_createtime'] = date('Y-m-d H:i:s');
		$data_wtk['wtk_warningtime'] = $wtk_warningtime;
		$data_wtk['wtk_title'] 		= $wtk_title;
		$data_wtk['wtk_content'] 	= $wtk_content;
		$data_wtk['wtk_type'] 		= $wtk_type;
		$data_wtk['wtk_object'] 	= $wtk_object;
		$data_wtk['wtk_files'] 	= $wtk_files;
		$data_wtk['wtk_status'] 	= 10;

		$do_wtk = M('warning_task')->add($data_wtk);
		if(!empty($do_wtk)){
			D('Function')->write_log('预警管理',1,'添加成功','warning_task',$do_wtk,M('warning_task')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功','data'=>$do_wtk));
		}else{
			D('Function')->write_log('预警管理',1,'添加失败','warning_task',$do_wtk,M('warning_task')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'添加失败'));
		}
	}

	/**
	 * 编辑预警
	 * by zw
	 */
	public function edit_warning(){
		$fid 			= I('fid');
		$wtk_warningtime = I('wtk_warningtime');//预警时间
		$wtk_title 		= I('wtk_title');//预警提示
		$wtk_content 	= I('wtk_content');//预警内容
		$wtk_type 		= I('wtk_type');//预警类型
		$wtk_object 	= I('wtk_object');//预警对象
		$wtk_files 	= I('wtk_files');//预警附件

		$data_wtk['wtk_edittime'] 	= date('Y-m-d H:i:s');
		$data_wtk['wtk_edituserid'] = session('regulatorpersonInfo.fid');
		$data_wtk['wtk_warningtime'] = $wtk_warningtime;
		$data_wtk['wtk_title'] 		= $wtk_title;
		$data_wtk['wtk_content'] 	= $wtk_content;
		$data_wtk['wtk_type'] 		= $wtk_type;
		$data_wtk['wtk_object'] 	= $wtk_object;
		$data_wtk['wtk_files'] 	= $wtk_files;

		$do_wtk = M('warning_task')->where(['wtk_id'=>$fid,'wtk_trepid'=>session('regulatorpersonInfo.fregulatorpid')])->save($data_wtk);
		if(!empty($do_wtk)){
			D('Function')->write_log('预警管理',1,'修改成功','warning_task',$fid,M('warning_task')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			D('Function')->write_log('预警管理',1,'修改失败','warning_task',$fid,M('warning_task')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'修改失败'));
		}
	}

	/**
	 * 删除预警
	 * by zw
	 */
	public function delete_warning(){
		$fid = I('fid');

		$data_wtk['wtk_status'] = 0;
		$do_wtk = M('warning_task')->where(['wtk_id'=>$fid,'wtk_trepid'=>session('regulatorpersonInfo.fregulatorpid')])->save($data_wtk);
		if(!empty($do_wtk)){
			D('Function')->write_log('预警管理',1,'删除成功','warning_task',$fid,M('warning_task')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			D('Function')->write_log('预警管理',1,'删除失败','warning_task',$fid,M('warning_task')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败'));
		}
	}

	/**
	 * 查看预警
	 * by zw
	 */
	public function warning_view(){
		$fid = I('fid');

		$where_wtk['wtk_id'] = $fid;
		$where_wtk['wtk_customer'] = $system_num;
		$where_wtk['wtk_status'] = array('gt',0);
		$where_wtk['_string'] = '(wtk_trepid='.session('regulatorpersonInfo.fregulatorpid').' and wtk_status>0) or (wtk_object=0 and wtk_status=10) or (wtk_object='.session('regulatorpersonInfo.fregulatorpid').' and wtk_status=10)';

		$do_wtk = M('warning_task')
			->field('ifnull(b.ffullname,"所有机构") as ffullname,warning_task.*')
			->join('tregulator b on warning_task.wtk_object=b.fid','left')
			->where($where_wtk)
			->find();
		if(!empty($do_wtk)){
			$do_wtk['wtk_content'] = htmlspecialchars_decode($do_wtk['wtk_content']);
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('data'=>$do_wtk)));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据'));
		}
	}

}