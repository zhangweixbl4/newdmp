<?php
//公司内部人员控制器
namespace Admin\Controller;
use Think\Controller;
class PersonController extends BaseController {

    public function index(){
		 
		$personList = M('tperson')->where(array('fpid'=>0))->select();
		//var_dump(session('regulatorpersonInfo')); 
		$this->assign('personList',$personList);
		$this->display();
	}
	
	/*添加、修改组织人员*/
	public function add_edit_person(){
		$fid= I('fid');//人员ID
		$fid = intval($fid);//转为数字
		$authority_list = I('authority_list');
		$authority_list = implode(',', $authority_list);//转成字符串存储
		
		$region_authority = I('region_authority');
		$region_authority = implode(',', $region_authority);//转成字符串存储
		
		
		
		$role_list = I('role_list');
		$role_list = implode(',', $role_list);//转成字符串存储
		
		$a_e_data = array();
		$a_e_data['forgid'] = I('forgid');//机构ID
		$a_e_data['fcode'] = I('fcode');//人员编码
		$a_e_data['fname'] = I('fname');//人员名称
		$a_e_data['fduties'] = I('fduties');//人员职务
		$a_e_data['fmobile'] = I('fmobile');//联系手机
		$a_e_data['fdescribe'] = intval(I('fdescribe'));//说明	
		$a_e_data['fisadmin'] = I('fisadmin');//是否超级管理员 0=》不是，1=》是
		$a_e_data['fstate'] = I('fstate',1);//状态，-1删除，0-无效，1-有效
		$a_e_data['authority_list'] = $authority_list;//权限列表
		$a_e_data['region_authority'] = $region_authority;//权限列表
		
		$a_e_data['role_list'] = $role_list;
		
		if($fid == 0){//判断是修改还是新增
			if(A('Admin/Authority','Model')->authority('100134') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));
			}
			$is_repeat = M('tperson')->where(array('fcode'=>$a_e_data['fcode']))->count();//查询人员代码是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'人员代码重复'));//返回人员代码重复
			
			$fpassword = I('fpassword');
			if($fpassword == '') $fpassword = '123456';
			$a_e_data['fpassword'] = md5($fpassword);//密码
			$a_e_data['fcreator'] = session('personInfo.fid').'_'.session('personInfo.fname');//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间

			$rr = M('tperson')->add($a_e_data);//添加数据
			
			
			if($rr > 0){//判断是否添加成功
				
				$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));//返回失败
			}
			
		}else{//修改
			if(A('Admin/Authority','Model')->authority('100135') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));
			}
			$is_repeat = M('tperson')->where(array('fid'=>array('neq',$fid),'fcode'=>$a_e_data['fcode']))->count();//查询人员代码是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'人员代码重复'));//返回人员代码重复
			if(I('fpassword') != '') $a_e_data['fpassword'] = md5(I('fpassword'));//密码
			$a_e_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tperson')->where(array('fid'=>$fid))->save($a_e_data);//修改数据
			if($rr > 0){//判断是否修改成功
				
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));//返回失败
			}
			
		}
		
		
		
	}
	
	/*加载人员列表*/
	public function ajax_person_list(){
		$forgid = I('forgid',0);//机构ID
		$p = I('p',1);//分页的第几页
		$pp = 100;//每页显示多少条记录
		$where = array();
		if($forgid > 0) $where['forgid'] = $forgid;
		$where['fstate'] = array('neq',-1);
		$personList = M('tperson')->field('fname,fid,fcode')->where($where)->page($p.','.$pp)->select();//查询人员列表
		$this->ajaxReturn(array('code'=>0,'msg'=>'','personList'=>$personList));
	}
	
	/*人员详情*/
	public function ajax_person_details(){
		
		$fid = I('fid');//获取人员ID
		
		$personDetails = M('tperson')->where(array('fid'=>$fid))->find();
		$authority = explode(',',$personDetails['authority_list']);//转为数组
		$in_role_where = explode(',',$personDetails['role_list']);//转为数组
		$region_authority = explode(',',$personDetails['region_authority']);//转为数组
		
		
		if($in_role_where){

			$role_where = array('role_id'=>array('in',$in_role_where));
		}
		$role = M('admin_role')->field('role_id,role_name')->where($role_where)->select();
		$this->ajaxReturn(array('code'=>0,'personDetails'=>$personDetails,'authority' => $authority,'role'=>$role,'region_authority'=>$region_authority));
	}
	
	
	/*删除人员*/
	public function ajax_person_del(){
		if(A('Admin/Authority','Model')->authority('100136') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'没有权限'));
		}
		$fid = I('fid');//获取人员ID
		M('tperson')->where(array('fid'=>$fid))->save(array('fstate'=>-1));
		$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'.$fid));
		
	}
	
	/*角色管理*/
	public function role(){
		if(A('Admin/Authority','Model')->authority('200904') === 0){
			exit('没有权限');
		}
		$admin_role_list = M('admin_role')->select();//查询角色列表
		$authorityList = M('admin_authority')->where('p_id = 0')->select();//查询第一级权限列表

		$this->assign('authorityList',$authorityList);//权限列表（第一级）
		$this->assign('admin_role_list',$admin_role_list);//角色列表
		$this->display();
	}
	
	/*角色详情*/
	public function ajax_role_details(){
		
		$role_id = I('role_id');//获取角色ID
		
		$roleDetails = M('admin_role')->where(array('role_id'=>$role_id))->find();//查询角色详情
		$authority_code_list = array();
		$authority_list = explode(',',$roleDetails['authority_code_list']);//转为数组

		
		
		$this->ajaxReturn(array('code'=>0,'roleDetails'=>$roleDetails,'authority_list' => $authority_list));
	}
	

	/*添加、修改角色名称*/
	public function add_edit_role(){
		$role_id= I('role_id');//人员ID
		$role_id = intval($role_id);//转为数字
		$role_name = I('role_name');//角色名称
		$authority_code_list = I('authority_code_list');
		$authority_code_list = implode(',', $authority_code_list);//转成字符串存储
		$a_e_data = array();
		$a_e_data['role_id'] = I('role_id');//角色ID
		$a_e_data['authority_code_list'] = $authority_code_list;//权限编码
		$a_e_data['role_name'] = I('role_name');//角色名称
		
		if($role_id == 0){//判断是修改还是新增
			if(A('Admin/Authority','Model')->authority('200901') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'没有权限'));
			}
			$a_e_data['fcreator'] = session('personInfo.fid').'_'.session('personInfo.fname');//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('admin_role')->add($a_e_data);//添加数据
			if($rr > 0){//判断是否添加成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));//返回失败
			}
			
		}else{//修改
			if(A('Admin/Authority','Model')->authority('200902') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'没有权限'));
			}
			
			$a_e_data['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('admin_role')->where(array('role_id'=>$role_id))->save($a_e_data);//修改数据
			if($rr > 0){//判断是否修改成功
				
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));//返回失败
			}
			
		}
		
		
		
	}
	
	/*删除角色*/
	public function del_role(){
		if(A('Admin/Authority','Model')->authority('200903') === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'没有权限'));
		}
		$role_id= intval(I('role_id'));//人员ID
		if($role_id === 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'role_id错误'));
		}
		$del_state = M('admin_role')->where(array('role_id'=>$role_id))->delete();//删除数据
		if($del_state){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败,原因未知'));
		}
		
	}
	
	
	/*ajax角色列表*/
	public function get_role_list(){
		
		$role_name = I('role_name');

		$where = array();

		$where['role_name'] = array('like','%'.$role_name.'%');
		$roleList = M('admin_role')->field('role_id,role_name')->where($where)->limit(10)->select();//查询角色列表

		$this->ajaxReturn(array('code'=>0,'value'=>$roleList));
	}
	
	
	

}