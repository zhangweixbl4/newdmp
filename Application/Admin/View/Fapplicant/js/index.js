Vue.prototype.$ELEMENT = { size: "mini", zIndex: 3000 };
Vue.use(VueViewer.default)
const vm = new Vue({
  el: '#app',
  data() {
    return {
      form: {
        fstatus: '1',
      },
      tableData: [],
      page: {
        pageSize: 20,
        pageIndex: 1,
      },
      changeForm: {
        fapplicantid: '',
        fzkmoney: '',
        fpostusername: '',
        fpostaddress: '',
        fpostmobile: '',
        fremark: '',
      },
      details: {},
      detailData: [],
      tableHeight: '300px',
      loading: false,
      detailVisible: false,
      changeVisible: false,
      total: 0,
      sourceChangeForm: '',
      ftype: {
        '0': {
          label: '机构',
          type: 'primary'
        },
        '1': {
          label: '个人',
          type: 'info'
        },
      },
      fstatus: {
        '1': {
          label: '公证中',
          type: 'primary'
        },
        '2': {
          label: '已公证',
          type: 'info'
        },
      },
      fispost: {
        '0': {
          label: '邮寄',
          type: 'primary'
        },
        '1': {
          label: '不邮寄',
          type: 'info'
        },
      },
      cardBody: {
        'padding-top': '8px',
        'padding-bottom': '8px',
      },
    }
  },
  created() {
    this.sourceChangeForm = JSON.stringify(this.changeForm)
    this.getList()
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight()
      document.querySelector('#app').style.opacity = 1
    })
    window.onresize = () => {
      this.getTableHeight()
    }
  },
  methods: {
    changeInfo(row) {
      Object.keys(this.changeForm).forEach(i => {
        this.changeForm[i] = row[i]
      })
      this.changeVisible = true
    },
    onSubmit() {
      let data = {
        ...this.changeForm
      }
      fly.post('/Admin/Fapplicant/evidenceEdit', Qs.stringify(data)).then(({ data }) => {
        if (data.code === 0) {
          this.handleClose()
          $h.notify.success(data.msg, this)
          this.getList(this.page.pageIndex)
        } else {
          $h.notify.error(data.msg, this)
        }
      })
    },
    openDetail({fapplicantid}) {
      fly.post('/Admin/Fapplicant/evidenceView', Qs.stringify({ fapplicantid })).then(({ data }) => {
        if (data.code === 0) {
          this.details = data.data
          this.detailData = data.data2
          this.detailVisible = true
        } else {
          $h.notify.error(data.msg, this)
        }
      })
    },
    urlLink(url) {
      const a = document.createElement('a')
      a.href = url
      a.target = '_blank'
      a.click()
    },
    getList(page = 1) {
      this.page.pageIndex = page
      let data = {
        ...this.form,
        ...this.page
      }
      this.loading = true
      fly.post('/Admin/Fapplicant/finishEvidenceList', Qs.stringify(data)).then(({data}) => {
        // this.tableData = data.data.map(i => {
        //   i.flog = (i.flog || '').split('；')
        //   return i
        // })
        this.tableData = data.data
        this.total = data.count
        this.loading = false
      })
    },
    deleteEvidence({fapplicantid}) {
      this.$confirm('此操作将永久删除该公证记录, 是否继续?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        fly.post('/Admin/Fapplicant/evidenceDel', Qs.stringify({ fapplicantid })).then(({ data }) => {
          if (data.code === 0) {
            $h.notify.success(data.msg, this)
            this.getList(this.page.pageIndex)
          } else {
            $h.notify.error(data.msg, this)
          }
        })
      }).catch(() => {
      })
    },
    getTrack(id) {
      this.$prompt('请输入需要追加的跟踪日志', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
      }).then(({ value }) => {
        const data = {
          fapplicantid: id,
          content: value,
        }
        fly.post('/Admin/Fapplicant/evidencegenzong', Qs.stringify(data)).then(({ data }) => {
          if (data.code === 0) {
            $h.notify.success(data.msg, this)
            this.getList(this.page.pageIndex)
          } else {
            $h.notify.error(data.msg, this)
          }
        })
      }).catch(() => {
        this.$message({
          type: 'info',
          message: '取消输入'
        })
      })
    },
    splitObj(val) {
      let arr = val ? val.split('；') : ''
      arr = arr.filter(i=> {
       return i != ''
      })
      return arr
    },
    getEvidence({fapplicantid}) {
      this.$confirm('此操作不可撤销，是否确认？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        fly.post('/Admin/Fapplicant/evidenceFinish', Qs.stringify({fapplicantid})).then(({data}) => {
          if (data.code === 0) {
            $h.notify.success(data.msg, this)
            this.getList(this.page.pageIndex)
          } else {
            $h.notify.error(data.msg, this)
          }
        })
      }).catch(() => {
      })
    },
    handleClose() {
      this.detailVisible = false
      this.changeVisible = false
      this.resetChangeForm()
    },
    pageSizeChange(size) {
      this.page.pageSize = size
    },
    getTableHeight() {
      let formHeight = this.$refs.sform.$el.clientHeight
      this.tableHeight = document.body.clientHeight - formHeight - 98 + 'px'
    },
    resetChangeForm() {
      this.changeForm = JSON.parse(this.sourceChangeForm)
    },
    download(data){
      fly.post('/Api/Fapplicant/downEvidenceObjext',{ rid: data })
      .then(({data}) => {
        if(data.code === 0){
          const a = document.createElement('a')
          a.href = data.data
          a.target='_blank'
          a.click()
        }else{
          $h.notify.error(data.msg, this)
        }
      })
    },
    downloadPdf(data) {
      fly.post('/Api/Fapplicant/downEvidenceLetterPDF',{rid: data} )
      .then(res => {
        if(res.data.code === 0){
          const c = document.createElement('a')
          c.href = res.data.data
          c.target='_blank'
          c.click()
        }else{
          this.$func._error(res.data.msg)
        }
      })
    }
  },
})