<?php
namespace Agp\Controller;
use Think\Controller;
use Think\Exception;
use Agp\Model\StatisticalReportModel;
import('Vendor.PHPExcel');

/**
 * 淄博移动互联网监测数据
 */

class FzbnetmonitoringController extends BaseController
{
	
	/**
	 * 获取移动互联网监测数据
	 * by zw
	 */
	public function index() {
		header("Content-type:text/html;charset=utf-8");
		ini_set('memory_limit','1024M');
		ini_set('max_execution_time', '120');//设置超时时间
		session_write_close();

		$p = I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录

		$appname = I('appname');//app名称
		$title = I('title');//标题
		$types = I('types');//类型，视频flv,图片image
		$fissuedatest = I('fissuedatest');//发布时间起
		$fissuedateed = I('fissuedateed');//发布时间止

		$db_ad = M('addata2','','DB_CONFIG1');

		$where['machine_ip'] = '淄博';
		$where['platform'] = 2;
		$where['status'] = array('in',[1,3]);
		if(!empty($appname)){
			$where['attribute05'] = array('like','%'.$appname.'%');
		}
		
		if(!empty($title)){
			$where['title'] = array('like','%'.$title.'%');
		}

		if(!empty($types)){
			$where['type'] = $types;
		}

		if(!empty($fissuedatest) && !empty($fissuedateed)){
			$where['_string'] = ' from_unixtime(created_date/1000,"%Y-%m-%d") between "'.$fissuedatest.'" and "'.$fissuedateed.'"';
		}

		$count = $db_ad
			->where($where)
			->count();

		$data = $db_ad
			->alias('a')
			->field('id,title,attribute05,target_url,type,attribute08,FROM_UNIXTIME(created_date/1000,"%Y-%m-%d") as created_date,shape')
			->where($where)
			->page($p,$pp)
			->order('created_date desc')
			->select();
			
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	/**
     * 生成跟踪报告
     * by zw
     */
    public function genzongpresentation(){
        session_write_close();
        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isrelease = $ALL_CONFIG['isrelease'];
        
        $daytime = I('daytime')?I('daytime'):date("Y-m-01", strtotime("-1 month"));
        $pnname = session('regulatorpersonInfo.regionname').date('Y年m月',strtotime($daytime)).'跟踪报告';

	    //时间条件筛选
	    $years      = date('Y',strtotime($daytime));//选择年份
	    $timetypes  = 30;//选择时间段
	    $timeval    = date('m',strtotime($daytime));//选择时间
    	$where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date',0,$isrelease);

        $where_tia['a.fcustomer']  = $system_num;
        $area = session('regulatorpersonInfo.regionid');
        $tregion_len = get_tregionlevel($area);
		if($tregion_len == 1){//国家级
			$where_tia['_string'] = ' fregion_id ='.$area;  
		}elseif($tregion_len == 2){//省级
			$where_tia['_string'] = ' fregion_id like "'.substr($area,0,2).'%"';
		}elseif($tregion_len == 4){//市级
			$where_tia['_string'] = ' fregion_id like "'.substr($area,0,4).'%"';
		}elseif($tregion_len == 6){//县级
			$where_tia['_string'] = ' fregion_id like "'.substr($area,0,6).'%"';
		}

        $joins = '(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on a.fid=snd.illad_id';
        
        $isexamine = getconfig('isexamine');
        if(in_array($isexamine, [10,20,30,40,50])){
          $where_tia['a.fexamine'] = 10;
        }
        $do_tia = M('tbn_illegal_ad')
	      ->alias('a')
	      ->field('a.fid,b.ffullname as fadclass,a.fad_name,(case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fresult,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fstatus,ifnull(k.ntcount,0) as ntcount,k.kfstarttime,k.kfendtime,a.fresult_datetime')
	      ->join('tadclass b on a.fad_class_code=b.fcode')
	      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
	      ->join($joins)
	      ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
	      ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
          ->join('left join (select g.fid,count(1) as ntcount,h.fsample_id,DATE_FORMAT(min(h.fissue_date),"%Y-%m-%d") as kfstarttime,DATE_FORMAT(max(h.fissue_date),"%Y-%m-%d") as kfendtime from tbn_illegal_ad g inner join (select aa.fsample_id,bb.fissue_date from tbn_illegal_ad aa,tbn_illegal_ad_issue bb where aa.fid=bb.fillegal_ad_id) h on g.fsample_id=h.fsample_id and h.fissue_date between DATE_SUB(g.fresult_datetime,INTERVAL -1 DAY) and DATE_SUB(g.fresult_datetime,INTERVAL -30 DAY) group by h.fsample_id) k on a.fid=k.fid')
	      ->where($where_tia)
	      ->order('x.fstarttime desc')
	      ->select();

        foreach ($do_tia as $key => $value) {
            $addhtml .= '<tr><td align="center">'.($key+1).'</td>';
            $addhtml .= '<td align="center">'.$value['fmedianame'].'</td>';
            $addhtml .= '<td align="center">'.$value['fad_name'].'</td>';
            $addhtml .= '<td align="center">'.$value['fadclass'].'</td>';
            $addhtml .= '<td align="center">'.$value['fcount'].'</td>';
            $addhtml .= '<td align="center">'.$value['fstarttime'].'~'.$value['fendtime'].'</td>';
            if(!empty($value['fresult_datetime'])){
                $addhtml .= '<td align="center">'.date('Y-m-d',strtotime($value['fresult_datetime'])).'</td>';
            }else{
                $addhtml .= '<td align="center"> </td>';
            }
            $addhtml .= '<td align="center">'.$value['fresult'].'</td>';
            if(!empty($value['fresult'])){
                $addhtml .= '<td align="center">'.$value['ntcount'].'</td>';
            }else{
                $addhtml .= '<td align="center"> </td>';
            }
            if(!empty($value['kfstarttime'])){
                $addhtml .= '<td align="center">'.$value['kfstarttime'].'~'.$value['kfendtime'].'</td>';
            }else{
                $addhtml .= '<td align="center"> </td>';
            }
           
        }

        if(empty($addhtml)){
            $addhtml = '<tr><td align="center" colspan="10">暂无信息</td></tr>';
        }

        $html = '<body style="font-size:18px; line-height:24px;"><div align="center" style="font-size:30px; font-weight:bold; padding:10px;">'.date('Y年m月',strtotime($daytime)).'案件线索跟踪报告</div>
            <table style="border:0;margin:0;border-collapse:collapse;border-spacing:0;" width="100%">
                <tr>
                    <td align="left" style="font-size:18px;">监管机构：'.session('regulatorpersonInfo.regulatorpname').'</td>
                </tr>
                <tr>
                    <td style="font-size:18px;">'.date('Y年m月1日',strtotime($daytime)).'至'.date('d', strtotime(date('Y-m-01', strtotime($daytime)) . ' +1 month -1 day')).'日</td>
                </tr>
            </table>
            <table border="1" cellpadding="0" width="100%" cellspacing="0" style="border-collapse: collapse;">
                <tr>
                    <td align="center">序号</td>
                    <td align="center">媒体名称</td>
                    <td align="center">广告名称</td>
                    <td align="center">广告类别</td>
                    <td align="center">发布条次</td>
                    <td align="center">发布时间段</td>
                    <td align="center">处理日期</td>
                    <td align="center">处置结果</td>
                    <td align="center">整改后播出次数</td>
                    <td align="center">整改后播出时间段</td>
                </tr>
                '.$addhtml.'
            </table>
            </body>
        ';
        $date = date('Ymdhis');
        $savefilename = $date.'.doc';
        $savefile = './Public/Word/'.$date.'.doc';

        //将文档保存
        word_start();
        echo $html;
        word_save($savefile);
        ob_flush();//每次执行前刷新缓存
        flush();

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.doc',$savefile,array('Content-Disposition' => 'attachment;filename='.$pnname.'.doc'));//上传云
        unlink($savefile);//删除文件

        $system_num = getconfig('system_num');

        //将生成记录保存
        $data['pnname']             = $pnname;
        $data['pntype']             = 61;
        $data['pnfiletype']         = 10;
        $data['pnstarttime']        = date('Y-m-d',strtotime($daytime));
        $data['pncreatetime']       = date('Y-m-d H:i:s');
        $data['pnurl']              = $ret['url'];
        $data['pnhtml']             = $html;
        $data['pnfocus']            = 0;
        $data['pntrepid']           = session('regulatorpersonInfo.fregulatorpid');
        $data['pntreid']            = session('regulatorpersonInfo.fregulatorid');
        $data['pncreatepersonid']   = 0;
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
        }
	}

	/**
     * 生成复核报告
     * by zw
     */
    public function fuhepresentation(){
        session_write_close();
        ini_set('memory_limit','1024M');
        header("Content-type: text/html; charset=utf-8");
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $ALL_CONFIG = getconfig('ALL');
        $system_num = $ALL_CONFIG['system_num'];
        $isrelease = $ALL_CONFIG['isrelease'];
        
        $daytime = I('daytime')?I('daytime'):date("Y-m-01", strtotime("-1 month"));
        $pnname = session('regulatorpersonInfo.regionname').date('Y年m月',strtotime($daytime)).'复核报告';

	    //时间条件筛选
	    $years      = date('Y',strtotime($daytime));//选择年份
	    $timetypes  = 30;//选择时间段
	    $timeval    = date('m',strtotime($daytime));//选择时间
    	$where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date',0,$isrelease);

    	$area = session('regulatorpersonInfo.regionid');
        $tregion_len = get_tregionlevel($area);
		if($tregion_len == 1){//国家级
			$where_tia['_string'] = ' b.fregion_id ='.$area;  
		}elseif($tregion_len == 2){//省级
			$where_tia['_string'] = ' b.fregion_id like "'.substr($area,0,2).'%"';
		}elseif($tregion_len == 4){//市级
			$where_tia['_string'] = ' b.fregion_id like "'.substr($area,0,4).'%"';
		}elseif($tregion_len == 6){//县级
			$where_tia['_string'] = ' b.fregion_id like "'.substr($area,0,6).'%"';
		}

        $where_tia['a.fcustomer']  = $system_num;
	    $where_tia['b.fstatus3']   = array('egt',10);
	    $where_tia['e.fstate']     = 1;
        $where_tia['d.fstate']     = 1;
	    $where_tia['a.ftype']      = 0;

        $do_tia = M('tbn_data_check')
	      ->alias('a')
	      ->field('a.id as aid,a.tshij_result,a.tquj_result,b.fmedia_class,c.ffullname as fadclass,b.fad_name, (case when instr(d.fmedianame,"（") > 0 then left(d.fmedianame,instr(d.fmedianame,"（") -1) else d.fmedianame end) as fmedianame,b.fillegal_code,b.fillegal,b.fexpressions,b.favifilename,b.fjpgfilename,b.fh_reson')
	      ->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')
	      ->join('tadclass c on b.fad_class_code=c.fcode')
	      ->join('tmedia d on b.fmedia_id=d.fid and d.fid=d.main_media_id ')
	      ->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on b.fid=snd.illad_id')
	      ->join('tregion e on b.fregion_id=e.fid')
	      ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on b.fid=x.fillegal_ad_id')
	      ->where($where_tia)
	      ->order('a.id desc')
	      ->select();

      	$sheet = $objPHPExcel->setActiveSheetIndex(0);
      	$sheet ->setCellValue('A1','序号');
        $sheet ->setCellValue('B1','媒体类别');
        $sheet ->setCellValue('C1','广告类别');
        $sheet ->setCellValue('D1','广告名称');
        $sheet ->setCellValue('E1','发布媒体');
        $sheet ->setCellValue('F1','违法表现代码');
        $sheet ->setCellValue('G1','违法表现');
        $sheet ->setCellValue('H1','违法内容');
        $sheet ->setCellValue('I1','证据内容');
        $sheet ->setCellValue('J1','申请复核意见');
        $sheet ->setCellValue('K1','市局意见');
        $sheet ->setCellValue('L1','市局附件');
        $sheet ->setCellValue('M1','区县局意见');
        $sheet ->setCellValue('N1','区县局附件');

        foreach ($do_tia as $key => $value) {
            $sheet ->setCellValue('A'.($key+2),$key+1);
            if($value['fmedia_class'] == 1){
            	$sheet ->setCellValue('B'.($key+2),'电视');
            }elseif($value['fmedia_class'] == 2){
            	$sheet ->setCellValue('B'.($key+2),'广播');
            }elseif($value['fmedia_class'] == 3){
            	$sheet ->setCellValue('B'.($key+2),'报纸');
            }elseif($value['fmedia_class'] == 13){
            	$sheet ->setCellValue('B'.($key+2),'互联网');
            }
            $sheet ->setCellValue('C'.($key+2),$value['fadclass']);
            $sheet ->setCellValue('D'.($key+2),$value['fad_name']);
            $sheet ->setCellValue('E'.($key+2),$value['fmedianame']);
            $sheet ->setCellValue('F'.($key+2),$value['fillegal_code']);
            $sheet ->setCellValue('G'.($key+2),$value['fillegal']);
            $sheet ->setCellValue('H'.($key+2),$value['fexpressions']);
            if($value['fmedia_class'] == 1 || $value['fmedia_class'] == 2){
            	$sheet ->setCellValue('I'.($key+2),$value['favifilename']);
            }elseif($value['fmedia_class'] == 3|| $value['fmedia_class'] == 13){
            	$fjpgfilename = str_replace('http://mmbiz.qpic.cn','https://hz-weixin-img.oss-cn-hangzhou.aliyuncs.com',$value['fjpgfilename']);
            	$sheet ->setCellValue('I'.($key+2),$fjpgfilename);
            }
            $sheet ->setCellValue('J'.($key+2),$value['fh_reson']);
		    $where_ah['a.fcheck_id']         = $value['aid'];
		    $where_ah['a.fstate']            = 0;
		    $where_ah['a.fupload_trelevel']  = 10;
		    $do_ah = M('tbn_data_check_attach')->field('a.fattach_url')->alias('a')->where($where_ah)->order('a.fid asc')->select();
		    $ahfile = '';
		    foreach ($do_ah as $key2 => $value2) {
		    	if(!empty($ahfile)){
		    		$ahfile .= chr(10);
		    	}
		    	$ahfile .= $value2['fattach_url'];
		    }
            $sheet ->setCellValue('K'.($key+2),$value['tshij_result']);
            $sheet ->setCellValue('L'.($key+2),$ahfile);
            $where_ah['a.fcheck_id']         = $value['aid'];
		    $where_ah['a.fstate']            = 0;
		    $where_ah['a.fupload_trelevel']  = 0;
		    $do_ah = M('tbn_data_check_attach')->field('a.fattach_url')->alias('a')->where($where_ah)->order('a.fid asc')->select();
		    $ahfile = '';
		    foreach ($do_ah as $key2 => $value2) {
		    	if(!empty($ahfile)){
		    		$ahfile .= chr(10);
		    	}
		    	$ahfile .= $value2['fattach_url'];
		    }
            $sheet ->setCellValue('M'.($key+2),$value['tquj_result']);
            $sheet ->setCellValue('N'.($key+2),$ahfile);

        }
        $objActSheet =$objPHPExcel->getActiveSheet();

        $objActSheet->setTitle('Sheet1');

        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $date_now = $date;
        $date = date('Ymdhis');
        $savefile = './Public/Xls/'.$date.'.xlsx';
        $objWriter->save($savefile);

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='.$pnname.'.xlsx'));//上传云
        unlink($savefile);//删除文件

        $system_num = getconfig('system_num');

        //将生成记录保存
        $data['pnname']             = $pnname;
        $data['pntype']             = 60;
        $data['pnfiletype']         = 30;
        $data['pnstarttime']        = date('Y-m-d',strtotime($daytime));
        $data['pncreatetime']       = date('Y-m-d H:i:s');
        $data['pnurl']              = $ret['url'];
        $data['pnhtml']             = $html;
        $data['pnfocus']            = 0;
        $data['pntrepid']           = session('regulatorpersonInfo.fregulatorpid');
        $data['pntreid']            = session('regulatorpersonInfo.fregulatorid');
        $data['pncreatepersonid']   = 0;
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
        }
	}
}