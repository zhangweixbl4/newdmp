<?php
namespace Gongshang\Controller;
use Think\Controller;

/**
 * Class AdclueListController
 * @package Gongshang\Controller
 * 线索登记
 */

class AdclueListController extends BaseController
{
	public function index()
	{
		$p = I('p', 1);//当前第几页
		$pp = 20;//每页显示多少记录
//发布媒介、广告名称、广告内容类别、违法类型、涉嫌违法内容、发布次数、指导次数、状态、操作（详情）；

		$fadname = I('fadname');// 广告名称
		$fmediaclass = I('fmediaclass');//媒体类别
		$fcreatetime_s = I('fcreatetime_s');// 创建时间  开始
		$fcreatetime_e = I('fcreatetime_e');// 创建时间  结束
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID

		$fadclasscode = I('fadclasscode');// 广告内容类别
		$fmediaid = I('fmediaid');// 发布媒介

		if ($fcreatetime_s == '') {
			$fcreatetime_s = '2015-01-01';
		}
		if ($fcreatetime_e == '') {
			$fcreatetime_e = date('Y-m-d');
		}

		$where = array();
		$where['tillegalad.fdisposestyle'] = array('in', '0,4');
		$where['tillegalad.fstate'] = array('in', '0,1');
		$where['tillegalad.fcreatetime'] = array('between', $fcreatetime_s . ',' . $fcreatetime_e . ' 23:59:59'); //时间
		if ($fadname != '') {
			$where['tad.fadname'] = array('like', '%' . $fadname . '%');//广告名称
		}
		if ($fmediaclass != '') {
			$where['tillegalad.fmediaclassid'] = $fmediaclass;
		}
		if (is_array($fillegaltypecode)) {
			$where['tillegalad.fillegaltypecode'] = array('in', $fillegaltypecode);//违法类型代码
		}
		if ($fadclasscode != '') {
			$arr_code=$this->get_next_tadclasscode($fadclasscode,true);//获取下级广告类别代码
			if($arr_code){
				$where['tad.fadclasscode'] = array('in', $arr_code);
			}else{
				$where['tad.fadclasscode'] = $fadclasscode;
			}
		}
		if($fmediaid == ''){
			if(session('regulatorpersonInfo.special_media')){
				$media=json_decode(session('regulatorpersonInfo.special_media'),true);
			}else{
				$media=$this->get_regulator_mediaid(session('regulatorpersonInfo.fregulatorid'));//监管机构对应的媒体
			}
			$where['tillegalad.fmediaid'] = array('in', $media);//发布媒介
		}else{
			$where['tillegalad.fmediaid'] = $fmediaid;//发布媒介
		}
		$where['tmediaowner.fregionid'] = session('regulatorpersonInfo.regulator_regionid');//地区搜索条件

		$count = M('tillegalad')
			->join('tmediaclass on tmediaclass.fid = tillegalad.fmediaclassid', 'LEFT')//媒体列别
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on tadclass.fcode = tad.fadclasscode', 'LEFT')//广告内容列别
			->join('tmedia on tmedia.fid = tillegalad.fmediaid', 'LEFT')//媒介信息
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//媒介机构
			->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode', 'LEFT')//违法类型
			->join('(SELECT * FROM `tillegaladflow` where fbizname=\'线索处理\' and fflowname=\'线索登记\') as f  on  tillegalad.fillegaladid=f.fillegaladid ', 'LEFT')//违法表
			->where($where)->where('f.fstate IN (\'0\',\'1\') or f.fstate is NULL')
			->count();// 查询满足要求的总记录数

		$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$data = M('tillegalad')
			->field('
						f.fillegaladflowid,
						tillegalad.fillegaladid,
			            tad.fadname,
			            tmedia.fmedianame,
			            tadclass.ffullname as adclass_fullname,
			            tillegaltype.fillegaltype,
			            tillegalad.fillegalcontent,
			            tillegalad.fissuetimes,
			            tillegalad.ffirstissuetime,
			            tillegalad.fdisposetimes,
			            tillegalad.fstate
									')
			->join('tmediaclass on tillegalad.fmediaclassid = tmediaclass.fid ', 'LEFT')//媒体列别
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on  tad.fadclasscode=tadclass.fcode ', 'LEFT')//广告内容列别
			->join('tmedia on  tillegalad.fmediaid=tmedia.fid', 'LEFT')//媒介信息
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//媒介机构
			->join('tillegaltype on tillegalad.fillegaltypecode=tillegaltype.fcode', 'LEFT')//违法类型
			->join('(SELECT * FROM `tillegaladflow` where fbizname=\'线索处理\' and fflowname=\'线索登记\') as f  on  tillegalad.fillegaladid=f.fillegaladid ', 'LEFT')//违法表
			->where($where)->where('f.fstate IN (\'0\',\'1\') or f.fstate is NULL')
			->order('tillegalad.fstate asc')
			->limit($Page->firstRow . ',' . $Page->listRows)
			->select();//查询违法广告
/*		SELECT * FROM `tillegalad` LEFT JOIN (SELECT * FROM `tillegaladflow` where fbizname='线索处理' and fflowname='线索登记') as f
on tillegalad.fillegaladid=f.fillegaladid
where tillegalad.fdisposestyle in('0','4') AND tillegalad.fstate IN ('0','1')AND tillegalad.fmediaid IN ('24','29')
	AND (f.fstate IN ('0','1') or f.fstate is NULL)*/
		$data = list_k($data, $p, $pp);//为列表加上序号
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate' => 1))->select();//查询违法类型
		$this->assign('illegaltype', $illegaltype); //违法类型
		$this->assign('data', $data); //列表信息
		$this->assign('page', $Page->show());//分页输出
		$this->display('adclueReg');
	}

	/*线索详情*/
	public function adclueRegDetails()
	{
		$where = array();
		if(I('fillegaladid')){
			$fillegaladid = I('fillegaladid');//线索ID
		}else{
			$this->error('缺失fillegaladid!');
		}
		if(I('fillegaladflowid')){
			$fillegaladflowid = I('fillegaladflowid');
		}
		$arr['fstate'] = 1;
		$res = M("tillegalad")->where(array('fillegaladid' => $fillegaladid))->save($arr);//待处理改正处理 埋点

		$where['tillegalad.fillegaladid'] = $fillegaladid;
		$data = M('tillegalad')
			->field('
						tillegalad.fillegaladid,
						tillegalad.fmediaclassid,
						tillegalad.fadid,
						tillegalad.fmediaid,
			            tad.fadname,
			            tadclass.ffullname as adclass_fullname,
			            tillegalad.fissuetimes,
			            tillegalad.fdisposetimes,
			            tillegalad.ffirstissuetime,
			            tillegalad.flastissuetime,

			            tillegaltype.fillegaltype,
			            tillegalad.fillegalcontent,
			            tillegalad.fexpressions,
			            tillegalad.fconfirmations,
			            tillegalad.fsampleid,
			             tmediaowner. fcreditcode,
			            tmediaowner.fregaddr,
			            tmediaowner. flinkman,
			            tmediaowner.ftel,
			            tmediaowner.fname as mediaowner_name

									')//id，<<媒介类别>>,广告id. 广告名称，广告内容列别，发布次数。指导次数。首次发布日期，末次发布日期
			// 违法类型：违法内容：违法表现：认定依据,样本id：
			->join('tmediaclass on tmediaclass.fid = tillegalad.fmediaclassid', 'LEFT')//媒体列别
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on tadclass.fcode = tad.fadclasscode', 'LEFT')//广告内容列别
			->join('tmedia on tmedia.fid = tillegalad.fmediaid', 'LEFT')//媒介信息
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid', 'LEFT')
//			->join('taddirect on taddirect.fillegaladid = tillegalad.fillegaladid', 'LEFT')//广告指导
			->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode', 'LEFT')//违法类型
			->where($where)
			->find();//查询广告信息
		$data['fillegaladflowid']= $fillegaladflowid;
		if ($data['fmediaclassid']) {
			switch ($data['fmediaclassid']) {
				case 'bc':
					$adclueInfo = M('tbcsample')->field('fid,favifilename')->where(array('fadid' => $data['fadid']))->find();
					$list = M('tbcissue')
						->field('tmedia.fmedianame,tbcissue.fissuedate')
						->join('tmedia on  tbcissue.fmediaid=tmedia.fid', 'LEFT')//媒介信息
						->where(array('fbcsampleid'=>$data['fsampleid']))
						->select();//发布记录
					break;
				case 'tv':
					$adclueInfo = M('ttvsample')->field('fid,favifilename')->where(array('fadid' => $data['fadid']))->find();
					$list = M('ttvissue')
						->field('tmedia.fmedianame,ttvissue.fissuedate')
						->join('tmedia on  ttvissue.fmediaid=tmedia.fid', 'LEFT')//媒介信息
						->where(array('ftvsampleid'=>$data['fsampleid']))
						->select();//发布记录
					break;
				case 'paper':
					$adclueInfo = M('tpapersample')->field('fpapersampleid,fjpgfilename')->where(array('fadid' => $data['fadid']))->find();
					$list = M('tpaperissue')
						->field('tmedia.fmedianame,tpaperissue.fissuedate')
						->join('tmedia on  tpaperissue.fmediaid=tmedia.fid', 'LEFT')//媒介信息
						->where(array('fpapersampleid'=>$data['fsampleid']))
						->select();//发布记录
					break;
			}
		}
		//附件信息
		$attach = M('tillegaladattach')->where(array('fillegaladid' => $fillegaladid))->select();
		$attach_count = count($attach);
//		//合并线索
//		$merge_where['tadcase.fstate'] = array('in', '2,3');
//		$merge_where['tmediaowner.fname'] = $data['mediaowner_name'];
//		$merge_data = M('tadcase')
//			->field('
//			            tadcase.*,
//						tillegalad.fillegaladid,
//			            tad.fadname,
//			            tmediaowner. fcreditcode,
//			            tmediaowner.fregaddr,
//			            tmediaowner. flinkman,
//			            tmediaowner.ftel,
//			            tmediaowner.fname as mediaowner_name
//									')
//
//			->join('tillegalad on tadcase.fillegaladid = tillegalad.fillegaladid', 'LEFT')//广告信息
//			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
//			->join('tmedia on tmedia.fid = tadcase.fmediaid', 'LEFT')//媒介信息
//			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid', 'LEFT')
//			->where($merge_where)
//			->select();
		//下级单位
		$regulator_data = M('tregulator')->where(array('fpid'=>session('regulatorpersonInfo.fregulatorid'),'fkind'=>1))->select();


//		$fadcaseid = 60;//线索id
//		$tadcase = M("tadcase")->where(array('fadcaseid' => $fadcaseid))->find();
//		if()
//		$arr_fmergeid=explode(",",$tadcase['fmergeid']);//合并的违法广告id转数组
//		$arr_fmergeid[]=$fillegaladid;//新增当前违法广告id
//		aa($arr_fmergeid);
//		$fmergeid = implode(",",$arr_fmergeid);//把数据转成字符串
//		aa($fmergeid);
//		$reg = M("tadcase")->where(array('fadcaseid' => $fadcaseid))->save(array('fmergeid'=>$fmergeid));


		$this->assign('data', $data); //违法信息
		$this->assign('list', $list); //发布记录
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->assign('adclueInfo', $adclueInfo); //视频信息
		$this->assign('attach',$attach);//附件
		$this->assign('attach_count',$attach_count);//附件数量
//		$this->assign('merge_data', $merge_data); //合并线索
		$this->assign('regulator_data', $regulator_data); //下级单位
		$this->display();
	}

	/*反馈*/
	public function results()
	{
		if(I('fillegaladid')){
			$fillegaladid = I('fillegaladid');
		}else{
		    $this->ajaxReturn(array('code'=>-1,'msg'=>'fillegaladid不存在！'));
		}
		if(I('fillegaladflowid')){
			$fillegaladflowid = I('fillegaladflowid');//线索流程id
		}
		if(I('reg_type')){
			$reg_type = I('reg_type');//处理结果
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'请填写处理结果！'));
		}
		//是否已经操作过
		if($fillegaladflowid){
			$is_dj = M('tillegaladflow')
				->where(array('fillegaladflowid' => $fillegaladflowid,'fstate'=>2))
				->find();//查询违法广告信息
			if($is_dj){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'该广告已被操作过，请刷新~！'));
			}
		}else{
			$is_dj = M('tillegaladflow')
				->where(array('fillegaladid' => $fillegaladid,'fstate'=>1))
				->where('fflowname =\'线索审核\' or fflowname =\'申请复核\'or fflowname =\'广告指导\'or fflowname =\'指导签发\'or fflowname =\'指导反馈\'')
				->find();//查询违法广告信息
			if($is_dj){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'该广告已被操作过，请刷新~！'));
			}

		}

		$tillegalad = M('tillegalad')
			->field('
						tillegalad.*,
			            tmediaowner.fname as mediaowner_name,
			            tmediaowner.fregaddr,
			           tmediaowner. flinkman,
			            tmediaowner.ftel,
			           tmediaowner. fcreditcode
									')//mediaowner_name 企业名称、注册地址,联系人，电话，企业信用代码
			->join('tmedia on tmedia.fid = tillegalad.fmediaid', 'LEFT')//媒介信息
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid', 'LEFT')
			->where(array('fillegaladid' => $fillegaladid))->find();//查询违法广告信息
		switch ($reg_type) {
			case 1://线索登记
				/*添加线索记录*/
				if(I('fcaseno')){
					$fcaseno = I('fcaseno');//登记编号
				}else{
					$this->ajaxReturn(array('code'=>-1,'msg'=>'fcaseno不存在！'));
				}
//				if(I('flaw')){
//					$flaw = I('flaw');//登记理由
//				}else{
//					$this->ajaxReturn(array('code'=>-1,'msg'=>'flaw不存在！'));
//				}
				if(I('ftasktype')){
					$ftasktype = I('ftasktype');//是否转办
					if($ftasktype==1 && !empty(I('ftaskdepart'))&&!empty(I('ftaskdepartid'))){
						$tadcase_data['ftaskdepartid'] = I('ftaskdepartid');
						$tadcase_data['ftaskdepart'] = I('ftaskdepart');
					}
				}else{
					$this->ajaxReturn(array('code'=>-1,'msg'=>'ftasktype不存在！'));
				}
				$tadcase_data['fregisterunitid'] = session('regulatorpersonInfo.fregulatorid');//登记单位ID
				$tadcase_data['fregisterunit'] = session('regulatorpersonInfo.regulatorname');//登记单位
				$tadcase_data['fregister'] = session('regulatorpersonInfo.fname');//登记人
				$tadcase_data['fregistertime'] = date('Y-m-d H:i:s');//登记时间

				$tadcase_data['ftasktype'] = $ftasktype;//任务类型
				$tadcase_data['fcaseno'] = $fcaseno;//案件编号
				$tadcase_data['fentname'] = $tillegalad['mediaowner_name'];//企业名称
				$tadcase_data['fcreditid'] = I('fcreditid');//统一信用代码
				$tadcase_data['fhouse'] =I('fhouse');//企业住所
				$tadcase_data['flegalrep'] = I('flegalrep');//法定代表人
				$tadcase_data['fpenbasis'] = $tillegalad['fpunishments'];//处罚依据
				$tadcase_data['flaw'] = $tillegalad['fconfirmations'];//认定依据
				$tadcase_data['fstate'] = 2;//待审核
				$tadcase = M('tadcase')->where(array('fillegaladid' => $fillegaladid,'fstate' =>2))->find();//是否线索表已经有信息
				if(!$tadcase){
					$tadcase_data['fillegaladid'] = $fillegaladid;
					$tadcase_data['fmediaid'] = $tillegalad['fmediaid'];
					$tadcase_data['fsampleid'] = $tillegalad['fsampleid'];
					$reg = M("tadcase")->add($tadcase_data);
				}else{
					$reg = M("tadcase")->where(array('fillegaladid' => $fillegaladid,'fstate' =>2))->save($tadcase_data);
				}
				if(!$reg){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'线索登记失败！'));
				}


				if($fillegaladflowid){
					/*更改流程表_线索登记*/
					$save_list['fregulatorpersonid'] = session('regulatorpersonInfo.fid');//处理人ID
					$save_list['fregulatorpersonname'] = session('regulatorpersonInfo.fname');//处理人
					$save_list['ffinishtime'] = date('Y-m-d H:i:s');//完成时间
					$save_list['fstate'] = 2;
					$dj_res = M("tillegaladflow")->where(array('fillegaladflowid' => $fillegaladflowid))->save($save_list);
				}else{
					/*新增流程表_登记流程*/
					$dj_list['fillegaladid'] = $fillegaladid;
					$dj_list['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');
					$dj_list['fcreateregualtorname'] =  session('regulatorpersonInfo.regulatorname');
					$dj_list['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
					$dj_list['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
					$dj_list['fcreatetime'] = date('Y-m-d H:i:s');
					$dj_list['fbizname'] = '线索处理';
					$dj_list['fflowname'] = '线索登记';
					$dj_list['fregulatorid'] = session('regulatorpersonInfo.fregulatorid');
					$dj_list['fregulatorname'] =  session('regulatorpersonInfo.regulatorname');
					$dj_list['fregulatorpersonid'] = session('regulatorpersonInfo.fid');
					$dj_list['fregulatorpersonname'] = session('regulatorpersonInfo.fname');
					$dj_list['ffinishtime'] = date('Y-m-d H:i:s');
					$dj_list['fstate'] = 2;
					$dj_res = M("tillegaladflow")->add($dj_list);
				}
				if(!$dj_res){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'线索登记失败！！'));
				}
				/*新增流程表_审核流程*/
				$sh_list['fillegaladid'] = $fillegaladid;
				$sh_list['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');
				$sh_list['fcreateregualtorname'] =  session('regulatorpersonInfo.regulatorname');
				$sh_list['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
				$sh_list['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
				$sh_list['fcreatetime'] = date('Y-m-d H:i:s');
				$sh_list['fbizname'] = '线索处理';
				$sh_list['fflowname'] = '线索审核';
				$sh_list['fregulatorid'] = session('regulatorpersonInfo.fregulatorid');
				$sh_list['fregulatorname'] =  session('regulatorpersonInfo.regulatorname');
				$sh_list['fstate'] = 1;
				$sh_res = M("tillegaladflow")->add($sh_list);
				//附件参数
				if(I('attachinfo')){
					$attachinfo = I('attachinfo');

					foreach ($attachinfo as $key => $value) {
						if($value['id'] == '') $attachlist[] = $value;
					}
					if($attachlist){
						$attach_data['fillegaladid'] = $fillegaladid;
						$attach_data['fillegaladflowid'] = 0;
						$attach_data['ffilename'] = ' ';
						$attach_data['fuploadtime'] = date('Y-m-d H:i:s',time());
						$attach_data['fattachuser'] = 1;
						foreach ($attachlist as $key => $value) {
							$attach_data['fattachname'] = $value['name'];
							$attach_data['fattachurl'] = $value['url'];
							$attach_data['ffilename'] = preg_replace('/\..*/','',$value['url']);
							$attach_data['ffiletype'] = preg_replace('/.*\./','',$value['url']);
							$attach[$key] = $attach_data;
						}
						$attachid = M('tillegaladattach')->addAll($attach);
						if(empty($attachid)) $this->ajaxReturn(array('code'=>-1,'msg'=>'附件添加失败'));
					}
				}
				/*更改违法广告表*/
				$arr['fmodifier'] = session('regulatorpersonInfo.fname');
				$arr['fmodifytime'] = date('Y-m-d H:i:s');
				$arr['fdisposestyle'] = 4;//线索
				$arr['fstate'] = 1;//正处理
				$res = M("tillegalad")->where(array('fillegaladid' => $fillegaladid))->save($arr);
				if(!$res){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'线索登记失败！！！'));
				}
				$this->ajaxReturn(array('code'=>0,'msg'=>'线索登记成功！'));
				break;
			case 2:
				/*并入线索
				 * 新增流程表*/
//				tadcase.fadcaseid,
//						tillegalad.fillegaladid,
//			            tad.fadname

//				$fadcaseid = I('fadcaseid');//线索id
//				$tadcase = M("tadcase")->where(array('fadcaseid' => $fadcaseid))->find();
//				$arr_fmergeid=explode(",",$tadcase['fmergeid']);//合并的违法广告id转数组
//				$arr_fmergeid[]=$fillegaladid;//新增当前违法广告id
//				aa($arr_fmergeid);
//				$fmergeid = implode(",",$arr_fmergeid);//把数据转成字符串
//				aa($fmergeid);
//				$reg = M("tadcase")->where(array('fadcaseid' => $fadcaseid))->save(array('fmergeid'=>$fmergeid));





//				$tadcase = M("tadcase")->where(array('fadcaseid' => $fadcaseid))->find();
//				$arr_fmergeid=explode(",",$tadcase['fmergeid']);
//				$arr_fmergeid[]=
//				$adclass=array('01','02','06');
////		$special_adclass = implode(",",$adclass);
//				$fmergeid=$tadcase['fmergeid'];
//				$reg = M("tadcase")->where(array('fadcaseid' => $fadcaseid))->save(array('fmergeid'=>$tadcase));

				$data['fillegaladid'] = $fillegaladid;
				$data['fcreateinfo'] = I('fdescribe'); //暂时存这里
				$data['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');
				$data['fcreateregualtorname'] =  session('regulatorpersonInfo.regulatorname');
				$data['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
				$data['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
				$data['fcreatetime'] = date('Y-m-d H:i:s');
				$data['fbizname'] = '申请复核';
				$data['fstate'] = 1;
				$reg = M("tillegaladflow")->add($data);
				if(!$reg){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'申请复核失败！'));
				}
				/*更改违法广告表*/
				$arr['fmodifier'] = session('regulatorpersonInfo.fname');
				$arr['fmodifytime'] = date('Y-m-d H:i:s');
				$arr['fdisposestyle'] = 3;
				$arr['fstate'] = 0;
				$res = M("tillegalad")->where(array('fillegaladid' => $fillegaladid))->save($arr);
				if(!$res){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'申请复核失败！！'));
				}
				$this->ajaxReturn(array('code'=>0,'msg'=>'申请复核成功！'));
				break;
			case 3:
				/*复核
				 * 新增流程表*/
				$model=M("tillegaladflow");
				$model->startTrans();//开启事务
				if($fillegaladflowid){
					/*更改流程表_线索登记*/
					$save_list['fregulatorpersonid'] = session('regulatorpersonInfo.fid');//处理人ID
					$save_list['fregulatorpersonname'] = session('regulatorpersonInfo.fname');//处理人
					$save_list['ffinishtime'] = date('Y-m-d H:i:s');//完成时间
					$save_list['fstate'] = 2;
					$save_res = $model->where(array('fillegaladflowid' => $fillegaladflowid))->save($save_list);
				}
				$data['fillegaladid'] = $fillegaladid;
				$data['fcreateinfo'] = I('frecheck'); //暂时存这里
				$data['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');
				$data['fcreateregualtorname'] =  session('regulatorpersonInfo.regulatorname');
				$data['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
				$data['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
				$data['fcreatetime'] = date('Y-m-d H:i:s');
				$data['fbizname'] = '广告复核';
				$data['fflowname'] = '复核任务';
				$data['fstate'] = 1;
				$tregulator= M("tregulator")->where(array('fid' =>session('regulatorpersonInfo.fregulatorid')))->find();//当前机构信息
				if($tregulator['fpid']!= 0){
					$data['fregulatorid'] = $tregulator['fpid'];
					$up_tregulator= M("tregulator")->where(array('fid' =>$tregulator['fpid']))->find();
					$data['fregulatorname'] =  $up_tregulator['fname'];
					$reg = $model->add($data);
					if($reg){
						$model->commit();//成功则提交
					}else{
						$model->rollback();//不成功，则回滚
						$this->ajaxReturn(array('code'=>-1,'msg'=>'申请复核失败！'));
					}
				}else{
					$model->rollback();//不成功，则回滚
					$this->ajaxReturn(array('code'=>-1,'msg'=>'您已没有上级！！'));
				}
				/*更改违法广告表*/
				$arr['fmodifier'] = session('regulatorpersonInfo.fname');
				$arr['fmodifytime'] = date('Y-m-d H:i:s');
				$arr['fdisposestyle'] = 3;
				$arr['fstate'] = 0;
				$res = M("tillegalad")->where(array('fillegaladid' => $fillegaladid))->save($arr);
				if(!$res){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'申请复核失败！！'));
				}
				$check_data['fillegaladid'] = $fillegaladid;
				$check_data['fapplyregulatorid'] = session('regulatorpersonInfo.fregulatorid');
				$check_data['fapplyregulator'] = session('regulatorpersonInfo.regulatorname');
				$check_data['fapply'] =  session('regulatorpersonInfo.fname');
				$check_data['fapplytime'] = date('Y-m-d H:i:s');
				$check_data['fapplyreason'] = I('frecheck');
				$check_data['fcheckregulatorid'] = $tregulator['fpid'];
				$check_data['fcheckregulator'] = $up_tregulator['fname'];
				$check_data['fillegaltypecode1'] = $tillegalad['fillegaltypecode'];//复核前违法类型
				$check_data['fillegalcontent1'] = $tillegalad['fillegalcontent'];//复核前涉嫌违法内容
				$check_data['fexpressioncodes1'] = $tillegalad['fexpressioncodes'];//复核前违法表现代码
				$check_data['fexpressions1'] = $tillegalad['fexpressions'];//复核前违法表现
				$check_data['fconfirmations1'] = $tillegalad['fconfirmations'];//复核前认定依据
				$check_data['fpunishments1'] = $tillegalad['fpunishments'];//复核前处罚依据
				$check_data['fpunishmenttypes1'] = $tillegalad['fpunishmenttypes'];//复核前处罚种类及幅度
				$check_data['fstate'] = 0;
				$check_reg = M("tadcheck")->add($check_data);
				if(!$check_reg){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'申请复核失败！！'));
				}
				$this->ajaxReturn(array('code'=>0,'msg'=>'申请复核成功！'));
				break;
			case 4://转为指导
				/*转线索新增流程表*/
				if($fillegaladflowid){
					/*更改流程表_线索登记*/
					$save_list['fregulatorpersonid'] = session('regulatorpersonInfo.fid');//处理人ID
					$save_list['fregulatorpersonname'] = session('regulatorpersonInfo.fname');//处理人
					$save_list['ffinishtime'] = date('Y-m-d H:i:s');//完成时间
					$save_list['fstate'] = 2;
					$dj_res = M("tillegaladflow")->where(array('fillegaladflowid' => $fillegaladflowid))->save($save_list);
				}
				$data['fillegaladid'] = $fillegaladid;
				$data['fcreateinfo'] = I('ftoguide'); //暂时存这里
				$data['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');
				$data['fcreateregualtorname'] =  session('regulatorpersonInfo.regulatorname');
				$data['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
				$data['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
				$data['fcreatetime'] = date('Y-m-d H:i:s');
				$data['fbizname'] = '广告指导';
				$data['fflowname'] = '广告指导';
				$data['fregulatorid'] = session('regulatorpersonInfo.fregulatorid');
				$data['fregulatorname'] =  session('regulatorpersonInfo.regulatorname');
				$data['fstate'] = 1;
				$reg = M("tillegaladflow")->add($data);
				if(!$reg){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'转为指导失败！'));
				}

				/*更改违法广告表*/
				$arr['fmodifier'] = session('regulatorpersonInfo.fname');
				$arr['fmodifytime'] = date('Y-m-d H:i:s');
				$arr['fdisposestyle'] = 1;//指导
				$arr['fstate'] = 0;//待处理
				$res = M("tillegalad")->where(array('fillegaladid' => $fillegaladid))->save($arr);
				if(!$res){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'转为指导失败！'));
				}
				$this->ajaxReturn(array('code'=>0,'msg'=>'转为指导成功！'));
				break;
		}
	}

	//删除附件
	public function ajax_delete_attach(){

		$fillegaladattachid = I('fillegaladattachid');
		if($fillegaladattachid) M('tillegaladattach')->where(array('fillegaladattachid' => $fillegaladattachid))->delete();
		$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
	}

	/**
	 * ajax 请求并入线索或者广告名称列表
	 */
	public function ajax_merge_list(){
		if(I('fillegaladid')){
			$fillegaladid = I('fillegaladid');
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'fillegaladid不存在！'));
		}
		if(I('fmediaid')){
			$fmediaid = I('fmediaid');
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'fmediaid不存在！'));
		}
		$keyword  = I('keyword');//关键字
		$merge_where['tad.fadname'] = array('like','%'.$keyword.'%');
		$merge_where['tadcase.fstate'] = array('in', '2,3');
		$merge_where['tadcase.fmediaid'] =$fmediaid;
		$data = M('tadcase')
			->field('
			            tadcase.fadcaseid,
						tillegalad.fillegaladid,
			            tad.fadname
									')
			->join('tillegalad on tadcase.fillegaladid = tillegalad.fillegaladid', 'LEFT')//广告信息
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tmedia on tmedia.fid = tadcase.fmediaid', 'LEFT')//媒介信息
			->where($merge_where)
			->select();
		$this->ajaxReturn(array('code'=>0,'value'=>$data));
	}
}