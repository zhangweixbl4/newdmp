<?php
namespace Gongshang\Controller;
use Think\Controller;

/**
 * Class AdauditController
 * @package Gongshang\Controller
 * 线索审核
 */

class AdauditController extends BaseController
{
	public function index()
	{
		$p = I('p', 1);//当前第几页
		$pp = 20;//每页显示多少记录
//发布媒介、广告名称、广告内容类别、违法类型、涉嫌违法内容、发布次数、指导次数、状态、操作（详情）；

		$fadname = I('fadname');// 广告名称
		$fmediaclass = I('fmediaclass');//媒体类别
		$fcreatetime_s = I('fcreatetime_s');// 创建时间  开始
		$fcreatetime_e = I('fcreatetime_e');// 创建时间  结束
		$fillegaltypecode = I('fillegaltypecode');// 违法类型ID

		$fadclasscode = I('fadclasscode');// 广告内容类别
		$fmediaid = I('fmediaid');// 发布媒介

		if ($fcreatetime_s == '') {
			$fcreatetime_s = '2015-01-01';
		}
		if ($fcreatetime_e == '') {
			$fcreatetime_e = date('Y-m-d');
		}
		$where = array();
		$where['tillegalad.fcreatetime'] = array('between', $fcreatetime_s . ',' . $fcreatetime_e . ' 23:59:59'); //时间
		if ($fadname != '') {
			$where['tad.fadname'] = array('like', '%' . $fadname . '%');//广告名称
		}
		if ($fmediaclass != '') {
			$where['tillegalad.fmediaclassid'] = $fmediaclass;//媒体类别
		}
		if (is_array($fillegaltypecode)) {
			$where['tillegalad.fillegaltypecode'] = array('in', $fillegaltypecode);//违法类型代码
		}
		if ($fadclasscode != '') {
			$arr_code=$this->get_next_tadclasscode($fadclasscode,true);//获取下级广告类别代码
			if($arr_code){
				$where['tad.fadclasscode'] = array('in', $arr_code);
			}else{
				$where['tad.fadclasscode'] = $fadclasscode;
			}
		}
		if($fmediaid == ''){
			if(session('regulatorpersonInfo.special_media')){
				$media=json_decode(session('regulatorpersonInfo.special_media'),true);
			}else{
				$media=$this->get_regulator_mediaid(session('regulatorpersonInfo.fregulatorid'));//监管机构对应的媒体
			}
			$where['tillegalad.fmediaid'] = array('in', $media);//发布媒介
		}else{
			$where['tillegalad.fmediaid'] = $fmediaid;//发布媒介
		}


		$count = M('tillegaladflow')
			->join('tillegalad on tillegaladflow.fillegaladid=tillegalad.fillegaladid', 'LEFT')//违法类型
			->join('tmediaclass on tillegalad.fmediaclassid = tmediaclass.fid ', 'LEFT')//媒体列别
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on  tad.fadclasscode=tadclass.fcode ', 'LEFT')//广告内容列别
			->join('tmedia on  tillegalad.fmediaid=tmedia.fid', 'LEFT')//媒介信息
			->join('tillegaltype on tillegalad.fillegaltypecode=tillegaltype.fcode', 'LEFT')//违法类型
			->join('tadcase on tillegaladflow.fillegaladid=tadcase.fillegaladid', 'LEFT')//违法类型
			->where($where)
			->where(array('tillegaladflow.fbizname'=>'线索处理','tillegaladflow.fflowname'=>'线索审核','tillegaladflow.fstate'=>array('in', '0,1'),'tillegaladflow.fregulatorid'=>session('regulatorpersonInfo.fregulatorid')))
			->where(array('tadcase.fstate'=>2))
			->count();// 查询满足要求的总记录数

		$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$data = M('tillegaladflow')
			->field('
						tillegaladflow.*,
			            tad.fadname,
			            tmedia.fmedianame,
			            tadclass.ffullname as adclass_fullname,
			            tillegaltype.fillegaltype,
			            tillegalad.fillegalcontent,
			            tillegalad.fissuetimes,
			            tadcase.fregister,
			            tadcase.fregistertime
									')
			->join('tillegalad on tillegaladflow.fillegaladid=tillegalad.fillegaladid', 'LEFT')//违法类型
			->join('tmediaclass on tillegalad.fmediaclassid = tmediaclass.fid ', 'LEFT')//媒体列别
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on  tad.fadclasscode=tadclass.fcode ', 'LEFT')//广告内容列别
			->join('tmedia on  tillegalad.fmediaid=tmedia.fid', 'LEFT')//媒介信息
			->join('tillegaltype on tillegalad.fillegaltypecode=tillegaltype.fcode', 'LEFT')//违法类型
			->join('tadcase on tillegaladflow.fillegaladid=tadcase.fillegaladid', 'LEFT')//线索信息
			->where($where)
			->where(array('tillegaladflow.fbizname'=>'线索处理','tillegaladflow.fflowname'=>'线索审核','tillegaladflow.fstate'=>array('in', '0,1'),'tillegaladflow.fregulatorid'=>session('regulatorpersonInfo.fregulatorid'),'tillegaladflow.fregulatorname'=>session('regulatorpersonInfo.regulatorname')))
			->where(array('tadcase.fstate'=>2))
			->limit($Page->firstRow . ',' . $Page->listRows)
			->select();//查询违法广告
		$data = list_k($data, $p, $pp);//为列表加上序号
		$illegaltype = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate' => 1))->select();//查询违法类型
		$this->assign('illegaltype', $illegaltype); //违法类型
		$this->assign('data', $data); //列表信息
		$this->assign('page', $Page->show());//分页输出
		$this->display('adclueAudit');
	}

	/*线索详情*/
	public function details()
	{
		$where = array();
		if(I('fillegaladid')){
			$fillegaladid = I('fillegaladid');//线索ID
		}else{
			$this->error('缺失fillegaladid!');
		}
		if(I('fillegaladflowid')){
			$fillegaladflowid = I('fillegaladflowid');//线索ID
		}
		$where['tillegalad.fillegaladid'] = $fillegaladid;
		$data = M('tillegalad')
			->field('
						tillegalad.fillegaladid,
						tillegalad.fmediaclassid,
						tillegalad.fadid,
			            tad.fadname,
			            tadclass.ffullname as adclass_fullname,
			            tillegalad.fissuetimes,
			            tillegalad.fdisposetimes,
			            tillegalad.ffirstissuetime,
			            tillegalad.flastissuetime,

			            tillegaltype.fillegaltype,
			            tillegalad.fillegalcontent,
			            tillegalad.fexpressions,
			            tillegalad.fconfirmations,
			            tillegalad.fsampleid,
			            tmediaowner.fname as mediaowner_name

									')//id，<<媒介类别>>,广告id. 广告名称，广告内容列别，发布次数。指导次数。首次发布日期，末次发布日期
			// 违法类型：违法内容：违法表现：认定依据,样本id：
			->join('tmediaclass on tmediaclass.fid = tillegalad.fmediaclassid', 'LEFT')//媒体列别
			->join('tad on tillegalad.fadid = tad.fadid', 'LEFT')//广告信息
			->join('tadclass on tadclass.fcode = tad.fadclasscode', 'LEFT')//广告内容列别
			->join('tmedia on tmedia.fid = tillegalad.fmediaid', 'LEFT')//媒介信息
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid', 'LEFT')
			->join('tillegaltype on tillegaltype.fcode = tillegalad.fillegaltypecode', 'LEFT')//违法类型
			->where($where)
			->find();//查询广告信息
		$data['fillegaladflowid']= $fillegaladflowid;
		if ($data['fmediaclassid']) {
			switch ($data['fmediaclassid']) {
				case 'bc':
					$adclueInfo = M('tbcsample')->field('fid,favifilename')->where(array('fadid' => $data['fadid']))->find();
					break;
				case 'tv':
					$adclueInfo = M('ttvsample')->field('fid,favifilename')->where(array('fadid' => $data['fadid']))->find();
					break;
				case 'paper':
					$adclueInfo = M('tpapersample')->field('fpapersampleid,fjpgfilename')->where(array('fadid' => $data['fadid']))->find();
					break;
			}
		}
		//附件信息
		$attach = M('tillegaladattach')->where(array('fillegaladid' => $fillegaladid))->select();
		$attach_count = count($attach);
		//流转信息
		$flow = M('tillegaladflow')->where(array('fillegaladid' =>$fillegaladid))->select();
		//登记信息
		$dj_data = M('tadcase')->where(array('fillegaladid' =>$fillegaladid,'fstate' =>2))->find();
		//下级单位
		$regulator_data = M('tregulator')->where(array('fpid'=>session('regulatorpersonInfo.fregulatorid'),'fkind'=>1))->select();

		$this->assign('data', $data); //违法信息
		$this->assign('adclueInfo', $adclueInfo); //视频信息
		$this->assign('attach',$attach);//附件
		$this->assign('attach_count',$attach_count);//附件数量
		$this->assign('flow', $flow); //流转信息
		$this->assign('dj_data', $dj_data); //登记信息
		$this->assign('regulator_data', $regulator_data); //下级单位
		$this->display('adclueAuditDetails');
	}

	/*指导反馈*/
	public function results()
	{
		if(I('fillegaladid')){
			$fillegaladid = I('fillegaladid');
		}else{
		    $this->ajaxReturn(array('code'=>-1,'msg'=>'fillegaladid不存在！'));
		}
		if(I('fillegaladflowid')){
			$fillegaladflowid = I('fillegaladflowid');//线索流程id
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'fillegaladflowid不存在！'));
		}
		if(I('reg_type')){
			$reg_type = I('reg_type');//处理结果
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'请填写处理结果！'));
		}
		$is_dj = M('tillegaladflow')
			->where(array('fillegaladflowid' => $fillegaladflowid,'fstate'=>2))
			->find();//查询违法广告信息
		if($is_dj){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'该广告已被操作过，请刷新~！'));
		}
		switch ($reg_type) {
			case 1://审核同意
				/*更改流程表_线索审核*/
				$save_list['fregulatorpersonid'] = session('regulatorpersonInfo.fid');//处理人ID
				$save_list['fregulatorpersonname'] = session('regulatorpersonInfo.fname');//处理人
				$save_list['ffinishtime'] = date('Y-m-d H:i:s');//完成时间
				$save_list['fstate'] = 2;
				$save_reg = M("tillegaladflow")->where(array('fillegaladflowid' => $fillegaladflowid))->save($save_list);
				if(!$save_reg){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'线索审核失败！！'));
				}

				$case_data = M('tadcase')->where(array('fillegaladid' =>$fillegaladid,'fstate' =>2))->find();
				//是否是转办
				if($case_data['ftasktype']==1){
					/*新增流程表_转办审核流程*/
					$add_list['fillegaladid'] = $fillegaladid;
					$add_list['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');
					$add_list['fcreateregualtorname'] =  session('regulatorpersonInfo.regulatorname');
					$add_list['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
					$add_list['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
					$add_list['fcreatetime'] = date('Y-m-d H:i:s');
					$add_list['fbizname'] = '线索处理';
					$add_list['fflowname'] = '线索审核';
					$add_list['fregulatorid'] = $case_data['ftaskdepartid'];
					$add_list['fregulatorname'] =  $case_data['ftaskdepart'];
					$add_list['fstate'] = 1;
					//转办信息情况
					$tadcase_data['ftasktype']='';
					$tadcase_data['ftaskdepartid']=NULL;
					$tadcase_data['ftaskdepart']='';
					$tadcase_data['fstate'] = 2;//2待审核
				}else{
					/*新增流程表_办结流程*/
					$add_list['fillegaladid'] = $fillegaladid;
					$add_list['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');
					$add_list['fcreateregualtorname'] =  session('regulatorpersonInfo.regulatorname');
					$add_list['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
					$add_list['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
					$add_list['fcreatetime'] = date('Y-m-d H:i:s');
					$add_list['fbizname'] = '线索处理';
					$add_list['fflowname'] = '线索办结';
					$add_list['fregulatorid'] = session('regulatorpersonInfo.fregulatorid');
					$add_list['fregulatorname'] =  session('regulatorpersonInfo.regulatorname');
					$add_list['fstate'] = 1;

					$tadcase_data['fstate'] = 3;//带办结
				}
				$add_reg = M("tillegaladflow")->add($add_list);
				if(!$add_reg){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'线索审核失败！！!'));
				}
				//登记信息
				/*更改线索记录*/
				$tadcase_data['fauditor'] = session('regulatorpersonInfo.fname');//审核人
				$tadcase_data['faudittime'] = date('Y-m-d H:i:s');//登记时间
				$reg = M("tadcase")->where(array('fillegaladid' => $fillegaladid,'fstate' =>2))->save($tadcase_data);
				if(!$reg){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'线索审核失败！'));
				}
				$this->ajaxReturn(array('code'=>0,'msg'=>'线索审核成功！'));
				break;
			case 2://回退
				$model=M("tillegaladflow");
				$model->startTrans();//开启事务

				/*1，更改流程表,当前流程改成回退*/
				$save_list['fregulatorpersonid'] = session('regulatorpersonInfo.fid');//处理人ID
				$save_list['fregulatorpersonname'] = session('regulatorpersonInfo.fname');//处理人
				$save_list['ffinishtime'] = date('Y-m-d H:i:s');//完成时间
				$save_list['fstate'] = 10;//回退
				$save_reg = $model->where(array('fillegaladflowid' => $fillegaladflowid))->save($save_list);

				//最后一条状态是2的数据
				$before_data=$model->where(array('fillegaladid' => $fillegaladid,'fstate' => 2))->order('fillegaladflowid desc')->find();
				//2，新增一条状态是1的流程数据
				$add_list['fillegaladid'] = $fillegaladid;
				$add_list['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');
				$add_list['fcreateregualtorname'] =  session('regulatorpersonInfo.regulatorname');
				$add_list['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
				$add_list['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
				$add_list['fcreatetime'] = date('Y-m-d H:i:s');
				$add_list['fbizname'] = $before_data['fbizname'];
				$add_list['fflowname'] = $before_data['fflowname'];
				$add_list['fregulatorid'] = $before_data['fregulatorid'];
				$add_list['fregulatorname'] =  $before_data['fregulatorname'];
				$add_list['fstate'] = 1;
				$add_reg = $model->add($add_list);

				//3，更改流程表,把最后是2的数据的状态改成10回退
				$save_list['fstate'] = 10;//回退
				$save_data = $model->where(array('fillegaladflowid' => $before_data['fillegaladflowid']))->save($save_list);
				if($save_reg && $add_reg && $save_data){
					$model->commit();//成功则提交

				}else{
					$model->rollback();//不成功，则回滚
					$this->ajaxReturn(array('code'=>-1,'msg'=>'线索回退失败！！'));
				}
				/*更改线索记录*/
				$tadcase_data['fauditor'] = session('regulatorpersonInfo.fname');//审核人
				$tadcase_data['faudittime'] = date('Y-m-d H:i:s');//登记时间
				if($before_data['fflowname']=='线索登记'){
					$tadcase_data['fstate'] = 1;//待登记

				}elseif($before_data['fflowname']=='线索审核'){
					$tadcase_data['fstate'] = 2;//待审核
				}
				$reg = M("tadcase")->where(array('fillegaladid' => $fillegaladid,'fstate' =>2))->save($tadcase_data);
				if(!$reg){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'线索退回失败！'));
				}
				$this->ajaxReturn(array('code'=>0,'msg'=>'线索退回成功！'));
				break;
			case 3://转办

				if(I('ftaskdepartid')&&I('ftaskdepart')){
					$tadcase_data['ftaskdepartid'] = I('ftaskdepartid');//任务接收单位ID
					$tadcase_data['ftaskdepart'] = I('ftaskdepart');//任务接收单位
					$tadcase_data['ftasktype'] = 1;//是否转办
				}else{
					$this->ajaxReturn(array('code'=>-1,'msg'=>'转办信息不全面！'));
				}
				/*更改线索记录*/
				$case_data['ftasktype']='';
				$case_data['ftaskdepartid']=NULL;
				$case_data['ftaskdepart']='';
				$case_data['fauditor'] = session('regulatorpersonInfo.fname');//审核人
				$case_data['faudittime'] = date('Y-m-d H:i:s');//登记时间
				$case_data['fstate'] = 2;//待审核
				$reg = M("tadcase")->where(array('fillegaladid' => $fillegaladid,'fstate' =>2))->save($case_data);
				if(!$reg){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'线索转办失败！'));
				}

				/*更改流程表_线索审核*/
				$save_list['fregulatorpersonid'] = session('regulatorpersonInfo.fid');//处理人ID
				$save_list['fregulatorpersonname'] = session('regulatorpersonInfo.fname');//处理人
				$save_list['ffinishtime'] = date('Y-m-d H:i:s');//完成时间
				$save_list['fstate'] = 2;
				$save_reg = M("tillegaladflow")->where(array('fillegaladflowid' => $fillegaladflowid))->save($save_list);
				if(!$save_reg){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'线索转办失败！！'));
				}
				/*新增流程表_转办审核流程*/
				$add_list['fillegaladid'] = $fillegaladid;
				$add_list['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');
				$add_list['fcreateregualtorname'] =  session('regulatorpersonInfo.regulatorname');
				$add_list['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
				$add_list['fcreateregualtorpersonname'] = session('regulatorpersonInfo.fname');
				$add_list['fcreatetime'] = date('Y-m-d H:i:s');
				$add_list['fbizname'] = '线索处理';
				$add_list['fflowname'] = '线索审核';
				$add_list['fregulatorid'] = $tadcase_data['ftaskdepartid'];
				$add_list['fregulatorname'] =  $tadcase_data['ftaskdepart'];
				$add_list['fstate'] = 1;
				$add_reg = M("tillegaladflow")->add($add_list);
				if(!$add_reg){
					$this->ajaxReturn(array('code'=>-1,'msg'=>'线索转办失败！！!'));
				}
				$this->ajaxReturn(array('code'=>0,'msg'=>'线索转办成功！'));
				break;
		}
	}
}