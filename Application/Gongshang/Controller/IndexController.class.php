<?php
namespace Gongshang\Controller;
use Think\Controller;

class IndexController extends BaseController {
    public function index(){
		if(session('regulatorpersonInfo.fcode') == ''){
			header("Location:".U('Gongshang/Login/index'));
			exit;
		}
		if(session('regulatorpersonInfo.special_menu')){//如果有特殊的菜单
			$menu_list = json_decode(session('regulatorpersonInfo.special_menu'),true);
		}else{
			$menu_list=D('MenuList')->clist($menu_id = 0,session('regulatorpersonInfo.role'));//获取菜单
		}
		$this->assign('menu_list',$menu_list);//监管机构信息
		if(session('regulatorpersonInfo.role')=='gongshang') {
			$regulatorpersonInfo = M('tregulatorperson')->cache(true, 600)->where(array('fid' => session('regulatorpersonInfo.fid')))->find();//监管人员信息
			$regulatorInfo = M('tregulator')->cache(true, 600)->where(array('fid' => $regulatorpersonInfo['fregulatorid']))->find();//监管机构信息
			/*是部门登录的处理情况，*/
			while ($regulatorInfo['fkind'] == 2 && $regulatorInfo['fpid'] != 0) {
				$regulatorInfo = M('tregulator')->cache(true, 600)->where(array('fid' => $regulatorInfo['fpid']))->find();//找到属于的机构信息
			}
			session('regulatorpersonInfo.fregulatorid', $regulatorInfo['fid']);//机构id

			session('regulatorpersonInfo.regionid',$regulatorInfo["fregionid"]);//todo 临时
			if (!$_SESSION['regulatorpersonInfo']['regulator_regionid']) $_SESSION['regulatorpersonInfo']['regulator_regionid'] = $regulatorInfo["fregionid"];//机构 行政区划id
			if (!$_SESSION['regulatorpersonInfo']['regulatorname']) $_SESSION['regulatorpersonInfo']['regulatorname'] = $regulatorInfo["fname"];//机构名称
			if (!$_SESSION['regulatorpersonInfo']['regulator_type']) $_SESSION['regulatorpersonInfo']['regulator_type'] = $regulatorInfo["ftype"];//监管机构类型（20工商）
			if (!$_SESSION['regulatorpersonInfo']['regulator_code']) $_SESSION['regulatorpersonInfo']['regulator_code'] = $regulatorInfo["fcode"];//监管机构编码
			if (!$_SESSION['regulatorpersonInfo']['special_adclass']) $_SESSION['regulatorpersonInfo']['special_adclass'] = $regulatorInfo["special_adclass"];//个性化广告分类
			$regionid = session('regulatorpersonInfo.regionid');
			$this->assign('regionid', $regionid);//监管人员信息
			$this->assign('regulatorpersonInfo', $regulatorpersonInfo);//监管人员信息
			$this->assign('regulatorInfo', $regulatorInfo);//监管机构信息
		}elseif(session('regulatorpersonInfo.role')=='media'){
			alog(session());


		}

		$this->display();

	}

	public function home(){
		
		exit('首页调整中');
		session_write_close();
		$regulatorpersonInfo = M('tregulatorperson')->cache(true,600)->where(array('fid'=>session('regulatorpersonInfo.fid')))->find();//监管人员信息
		$regulatorInfo = M('tregulator')->cache(true,600)->where(array('fid'=>session('regulatorpersonInfo.fregulatorid')))->find();//监管机构信息
		$self_regionid = $regulatorInfo['fregionid'];//监管行政区划
		$now_date = strtotime(date('Y-m-d'));//当前日期（时间戳）

		$regionid_where = array();
		//$regionid_where['tmediaowner.fregionid']= $self_regionid;//只查自己的地区
		
		if($self_regionid > 0 && $self_regionid != 100000){
			
			$region_id_rtrim = A('Common/System','Model')->get_region_left($self_regionid);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$regionid_where['left(tmediaowner.fregionid,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		
		}

		$lately_15 = $now_date - (15 * 86400);//15天前日期（时间戳）

		$media=$this->get_access_media();//获取权限判断后的最终媒体列表

		/*监测数量*/
		$agp_count = M('tmedia')
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构
			->where(array('tmedia.fid'=>array('in', $media),'tmedia.fstate'=>array('neq',-1)))
			->group('left(tmedia.fmediaclassid,2)')
			->getField('left(tmedia.fmediaclassid,2) as class_id,count(tmedia.fid)','');//查找电视。广播。媒体 监测数量。1电视，2 广播 3 报纸
		if(!$agp_count){
			$agp_count = M('tmedia')
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构
			->where($regionid_where)
			->group('left(tmedia.fmediaclassid,2)')
			->getField('left(tmedia.fmediaclassid,2) as class_id,count(tmedia.fid)','');//查找电视。广播。媒体 监测数量。1电视，2 广播 3 报纸
		}

		/*-----------------------------------------------------------------------------------------------------------------------*/

		/*条形图 近15天新增的样本数，广告发布数。*/
		$today=date("Y-m-d");
		$date_15=date("Y-m-d",strtotime("-15 day")); //十五天前的日期

		$region_id_rtrim = rtrim($self_regionid, '00');//地区ID去掉末尾的0
		$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位

		//样本数据条件
		$sample_where = array();
		$sample_where['sample.fstate']= array('neq',-1);
		$sample_where['sample.fcreatetime']= array('between',$date_15.','.$today);

		//发布数据条件
		$issue_where = array();
		$issue_where['issue.fstate']= array('neq',-1);
		$issue_where['issue.fcreatetime']= array('between',$date_15.','.$today);

		if($media){
			$sample_where['sample.fmediaid'] = array('in', $media);//可以查到的媒体
			$issue_where['issue.fmediaid'] = array('in', $media);//发布媒介
		}else{
			if($self_regionid != '100000') {
				$sample_where['left(tmediaowner.fregionid,' . $region_id_strlen . ')'] = $region_id_rtrim;//地区搜索条件
				$issue_where['left(tmediaowner.fregionid,' . $region_id_strlen . ')'] = $region_id_rtrim;//地区搜索条件
			}
		}

		//电视样本量
		$ttvsample_no = M('ttvsample')
					->alias('sample')
					->join('tmedia on tmedia.fid = sample.fmediaid')
					->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
					->where($sample_where)
					->group('DATE_FORMAT(sample.fcreatetime,\'%Y-%m-%d\')')
					->cache(true)
					->getField('DATE_FORMAT(sample.fcreatetime,\'%Y-%m-%d\') as fday,count(sample.fid) as fcount','');
		//广播样本量
		$tbcsample_no = M('tbcsample')
					->alias('sample')
					->join('tmedia on tmedia.fid = sample.fmediaid')
					->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
					->where($sample_where)
					->group('DATE_FORMAT(sample.fcreatetime,\'%Y-%m-%d\')')
					->cache(true)
					->getField('DATE_FORMAT(sample.fcreatetime,\'%Y-%m-%d\') as fday,count(sample.fid) as fcount','');
		//报纸样本量
		$tpapersample_no = M('tpapersample')
					->alias('sample')
					->join('tmedia on tmedia.fid = sample.fmediaid')
					->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
					->where($sample_where)
					->group('DATE_FORMAT(sample.fcreatetime,\'%Y-%m-%d\')')
					->cache(true)
					->getField('DATE_FORMAT(sample.fcreatetime,\'%Y-%m-%d\') as fday,count(sample.fpapersampleid) as fcount','');

		/*-----------------------------------------------------------------------------------------------------------------*/
		//电视发布量
		$ttvissue_no = M('ttvissue')
			->alias('issue')
			->join('tmedia on tmedia.fid = issue.fmediaid')
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
			->where($issue_where)
			->group('DATE_FORMAT(issue.fcreatetime,\'%Y-%m-%d\')')
			->cache(true)
			->getField('DATE_FORMAT(issue.fcreatetime,\'%Y-%m-%d\') as fday,count(issue.ftvissueid) as fcount','');
		//广播发布量
		$tbcissue_no = M('tbcissue')
			->alias('issue')
			->join('tmedia on tmedia.fid = issue.fmediaid')
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
			->where($issue_where)
			->group('DATE_FORMAT(issue.fcreatetime,\'%Y-%m-%d\')')
			->cache(true)
			->getField('DATE_FORMAT(issue.fcreatetime,\'%Y-%m-%d\') as fday,count(issue.fbcissueid) as fcount','');
		//报纸发布量
		$tpaperissue_no = M('tpaperissue')
			->alias('issue')
			->join('tmedia on tmedia.fid = issue.fmediaid')
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
			->where($issue_where)
			->group('DATE_FORMAT(issue.fcreatetime,\'%Y-%m-%d\')')
			->cache(true)
			->getField('DATE_FORMAT(issue.fcreatetime,\'%Y-%m-%d\') as fday,count(issue.fpaperissueid) as fcount','');

		/*-----------------------------------------------------------------------------------------------------------------*/
		//电视违法量
		$ttvillegal_no = M('ttvissue')
			->alias('issue')
			->join('tmedia on tmedia.fid = issue.fmediaid')
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
			->join('ttvsample as sample on sample.fid = issue.ftvsampleid')
			->where($issue_where)
			->where(array('sample.fillegaltypecode'=>array('gt',0)))
			->group('DATE_FORMAT(issue.fcreatetime,\'%Y-%m-%d\')')
			->cache(true)
			->getField('DATE_FORMAT(issue.fcreatetime,\'%Y-%m-%d\') as fday,SUM(issue.fquantity)as fissue,count(distinct sample.fid) as fsample');
		//广播违法量
		$tbcillegal_no = M('tbcissue')
			->alias('issue')
			->join('tmedia on tmedia.fid = issue.fmediaid')
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
			->join('tbcsample  as sample  on sample.fid = issue.fbcsampleid')
			->where($issue_where)
			->where(array('sample.fillegaltypecode'=>array('gt',0)))
			->group('DATE_FORMAT(issue.fcreatetime,\'%Y-%m-%d\')')
			->cache(true)
			->getField('DATE_FORMAT(issue.fcreatetime,\'%Y-%m-%d\') as fday,SUM(issue.fquantity)as fissue,count(distinct sample.fid) as fsample');
		//报纸违法量
		$tpaperillegal_no = M('tpaperissue')
			->alias('issue')
			->join('tmedia on tmedia.fid = issue.fmediaid')
			->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
			->join('tpapersample as sample on sample.fpapersampleid = issue.fpapersampleid')
			->where($issue_where)
			->where(array('sample.fillegaltypecode'=>array('gt',0)))
			->group('DATE_FORMAT(issue.fcreatetime,\'%Y-%m-%d\')')
			->cache(true)
			->getField('DATE_FORMAT(issue.fcreatetime,\'%Y-%m-%d\') as fday,SUM(issue.fquantity)as fissue,count(distinct sample.fpapersampleid) as fsample');

		/*-----------------------------------------------------------------------------------------------------------------*/
		//把日期遍历出来。并赋值相加
		for($i=14;$i>=0;$i--){
			$date_i=date("Y-m-d",strtotime("-".$i."day")); //十五天前的日期
			$list[$date_i]['date'] = $date_i;
			$list[$date_i]['adsample_count']=$ttvsample_no[$date_i]+$tbcsample_no[$date_i]+$tpapersample_no[$date_i];//近15天广告样本总和
			$list[$date_i]['adissue_count']=$ttvissue_no[$date_i]+$tbcissue_no[$date_i]+$tpaperissue_no[$date_i];//近15天广告发布总和
			$list[$date_i]['adillegal_issue_count']=$ttvillegal_no[$date_i]['fissue']+$tbcillegal_no[$date_i]['fissue']+$tpaperillegal_no[$date_i]['fissue'];//近15天发布违法广告总和
			$list[$date_i]['adillegal_sample_count']=$ttvillegal_no[$date_i]['fsample']+$tbcillegal_no[$date_i]['fsample']+$tpaperillegal_no[$date_i]['fsample'];//近15天发布违法广告样本总和

			$ad_count['adissue_count_15'] +=$list[$date_i]['adissue_count']; //右侧绿色柱子15天广告发布数
			$ad_count['adsample_count_15'] +=$list[$date_i]['adsample_count']; //右侧绿色柱子15天广告样本数
			$ad_count['adillegal_issue_count_15'] +=$list[$date_i]['adillegal_issue_count']; //右侧绿色柱子15天违法广告发布数
			$ad_count['adillegal_sample_count_15'] +=$list[$date_i]['adillegal_sample_count']; //右侧绿色柱子15天违法广告样本数
		}
		$this->assign('list',$list);//监测数量
		/*-----------------------------------------------------------------------------------------------------------------*/




		/*原型的线索处理分析*/
		$adclue['adclue_count'] = M('tadclue')->cache(true,600)
											->join('tadcluesection on tadcluesection.fadclueid = tadclue.fadclueid and tadcluesection.fstate in (0,1,3)')
											->where(array('fcreatetime'=>array('between',date('Y-m-d H:i:s',$lately_15).','.date('Y-m-d H:i:s',$now_date)),'tadcluesection.fsendunit|tadcluesection.freceiveunit' => $regulatorInfo['fcode']))//发送单位，接收单位  查询条件
											->count();//查询线索数量

		$adclue['processed_adclue_count'] = M('tadclue')->cache(true,600)
													->join('tadcluesection on tadcluesection.fadclueid = tadclue.fadclueid and tadcluesection.fstate in (0,1,3)')
													->where(array('fcreatetime'=>array('between',date('Y-m-d H:i:s',$lately_15).','.date('Y-m-d H:i:s',$now_date)),'tadcluesection.fsendunit|tadcluesection.freceiveunit' => $regulatorInfo['fcode'],'tadclue.fstate'=>1))//发送单位，接收单位  查询条件
													->count();//查询已处理线索数量

		$adclue['untreated_adclue_count'] = M('tadclue')->cache(true,600)
													->join('tadcluesection on tadcluesection.fadclueid = tadclue.fadclueid and tadcluesection.fstate in (0,1,3)')
													->where(array('fcreatetime'=>array('between',date('Y-m-d H:i:s',$lately_15).','.date('Y-m-d H:i:s',$now_date + 86400)),'tadcluesection.fsendunit|tadcluesection.freceiveunit' => $regulatorInfo['fcode'],'tadclue.fstate'=>0))//发送单位，接收单位  查询条件
													->count();//查询未处理线索数量
		$adclue['processed_proportion'] = round($adclue['processed_adclue_count'] / $adclue['adclue_count'] * 100,2) .'%';//已处理占比
		$adclue['untreated_proportion'] = round($adclue['untreated_adclue_count'] / $adclue['adclue_count'] * 100,2) .'%';//未处理占比


//		$clueresulttypeList = M('tadclue')->cache(true,3600)
//										->field('tadclue.fresult,tclueresulttype.fadclueresult,tclueresulttype.fadclueresultid,count(tadclue.fresult) as count')
//										->join('tadcluesection on tadcluesection.fadclueid = tadclue.fadclueid and tadcluesection.fstate in (0,1,3)')
//										->join('tclueresulttype on tclueresulttype.fadclueresultid = tadclue.fresult')
//										->where(array('fhandletime'=>array('between',date('Y-m-d H:i:s',$lately_15).','.date('Y-m-d H:i:s',$now_date + 86400)),'tadcluesection.fsendunit|tadcluesection.freceiveunit' => $regulatorInfo['fcode'],'tadclue.fstate'=>1))
//										->group('tadclue.fresult')
//										->select();
//		$last_login = M('gongshang_login_log')->cache(true,600)
//											->join('ip on ip.ip = gongshang_login_log.login_ip','LEFT')
//											->where(array('person_id'=>$regulatorpersonInfo['fid']))->order('login_time desc')->limit(1,1)->select();
//		$regulatorpersonInfo['last_login'] = $last_login[0];//最近登录信息

		$adclassList =  M('tadclass')->cache(true,600)->where(array('fcode'=>array('gt',0),'fpcode'=>''))->select();//广告类别
//		foreach($adclassList as $key => $adclass){
//			$adclassList[$key]['fadclass'] = $adclass['fadclass'];
//			$adclassList[$key]['count'] = M('ttvissue')->cache(true,600)
//														->join('ttvsample on ttvsample.fid = ttvissue.ftvsampleid')
//														->join('tad on tad.fadid = ttvsample.fadid')
//														->join('tmedia on tmedia.fid = ttvsample.fmediaid')
//														->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
//														->where(array_merge($regionid_where,array('ttvissue.fstate'=>array('neq',-1),'ttvissue.fissuedate'=>array('between',date('Y-m-d H:i:s',$lately_30).','.date('Y-m-d H:i:s',$now_date)),'left(tad.fadclasscode,2)'=>$adclass['fcode'])))
//
//														->count()
//										+	 M('tbcissue')->cache(true,600)
//														->join('tbcsample on tbcsample.fid = tbcissue.fbcsampleid')
//														->join('tad on tad.fadid = tbcsample.fadid')
//														->join('tmedia on tmedia.fid = tbcsample.fmediaid')
//														->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
//														->where(array_merge($regionid_where,array('tbcissue.fstate'=>array('neq',-1),'tbcissue.fissuedate'=>array('between',date('Y-m-d H:i:s',$lately_30).','.date('Y-m-d H:i:s',$now_date)),'left(tad.fadclasscode,2)'=>$adclass['fcode'])))
//
//														->count()
//
//										+	 M('tpaperissue')->cache(true,600)
//														->join('tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
//														->join('tad on tad.fadid = tpapersample.fadid')
//														->join('tmedia on tmedia.fid = tpapersample.fmediaid')
//														->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
//														->where(array_merge($regionid_where,array('tpaperissue.fstate'=>array('neq',-1),'tpaperissue.fissuedate'=>array('between',date('Y-m-d H:i:s',$lately_30).','.date('Y-m-d H:i:s',$now_date)),'left(tad.fadclasscode,2)'=>$adclass['fcode'])))
//														->count();
//
//
//		}
		$this->assign('agp_count',$agp_count);//监测数量
		$this->assign('adclassList',$adclassList);					
//		$this->assign('clueresulttypeList',$clueresulttypeList);
//		$this->assign('tv_ad_list',$tv_ad_list);
//		$this->assign('bc_ad_list',$bc_ad_list);
		$this->assign('adclue',$adclue);//广告线索
//		$this->assign('count',$count);
		$this->assign('ad_count',$ad_count);
		$this->assign('regulatorpersonInfo',$regulatorpersonInfo);
//		$this->assign('monitorList',$monitorList);
		$this->display();
	}
	
	
	
}