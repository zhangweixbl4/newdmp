<?php
namespace Open\Controller;
use Think\Controller;

class BcissueController extends BaseController {
	


	public function add_edit_bcissue(){
		$bcissue_data = file_get_contents('php://input');//获取post数据
		$bcissue_data = json_decode($bcissue_data,true);//把json转化成数组
		//var_dump($bcissue_data);
		$id = $bcissue_data['id'];//广告发布ID
		
		$a_e_data = array();
		
		$a_e_data['fbcsampleid'] = $bcissue_data['fbcsampleid'];//样本ID
		$bcsample_count = M('tbcsample')->where(array('fid'=>$a_e_data['fbcsampleid']))->count();//查询广播样本
		if($bcsample_count == 0) $this->ajaxReturn(array('code'=>10016,'msg'=>'添加或修改数据失败,样本ID有错误'));
		
		$a_e_data['fbcmodelid'] = intval($bcissue_data['fbcmodelid']);//模型ID

		$a_e_data['fmediaid'] = $bcissue_data['fmediaid'];//媒体ID
		$media_count = M('tmedia')->where(array('fid'=>$a_e_data['fmediaid']))->count();
		if($media_count == 0) $this->ajaxReturn(array('code'=>10016,'msg'=>'添加或修改数据失败,媒体ID有错误'));
		
		$a_e_data['fissuedate'] = date("Y-m-d H:i:s",strtotime($bcissue_data['fissuedate']));//发布日期
		
		
		
		$a_e_data['fstarttime'] = date("Y-m-d H:i:s",strtotime($bcissue_data['fstarttime']));//发布开始时间
		$a_e_data['fendtime'] = date("Y-m-d H:i:s",strtotime($bcissue_data['fendtime']));//发布结束时间
		$a_e_data['flength'] = intval($bcissue_data['flength']);//时长
		$a_e_data['fissuetype'] = $bcissue_data['fissuetype'];//发布类型
		if($a_e_data['fissuetype'] == '') $this->ajaxReturn(array('code'=>10015,'msg'=>'添加或修改数据失败,发布类型不能为空'));
		
		$a_e_data['famounts'] = intval($bcissue_data['famounts']);//金额
		$a_e_data['fquantity'] = intval($bcissue_data['fquantity']);//数量
		if($a_e_data['fquantity'] == 0) $a_e_data['fquantity'] == 1;//如果数量为0，则默认为1
		
		$a_e_data['fissample'] = intval($bcissue_data['fissample']);//是否样本
		$a_e_data['faudiosimilar'] = intval($bcissue_data['faudiosimilar']);//音频相似度
		$a_e_data['fvideosimilar'] = intval($bcissue_data['fvideosimilar']);//视频相似度
		
		$a_e_data['fadsort'] = $bcissue_data['fadsort'];//广告段排序，非必填
		$a_e_data['fpriorad'] = $bcissue_data['fpriorad'];//前一广告，非必填
		$a_e_data['fnextad'] = $bcissue_data['fnextad'];//后一广告，非必填
		$a_e_data['fpriorprg'] = $bcissue_data['fpriorprg'];//前一节目，非必填
		$a_e_data['fnextprg'] = $bcissue_data['fnextprg'];//后一节目，非必填
		
		$a_e_data['fstate'] = $bcissue_data['fstate'];//状态
		$person_info = $GLOBALS['person_info'];
		
		if($id == '' ){
			$a_e_data['fcreator'] = $person_info['fname'];//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = $person_info['fname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			$rr = M('tbcissue')->add($a_e_data);
			if($rr > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'添加广告发布成功','id'=>$rr));
			if($rr == 0) $this->ajaxReturn(array('code'=>10011,'msg'=>'添加广告发布失败,原因未知'));
		}else{
			$a_e_data['fmodifier'] = $person_info['fname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tbcissue')->where(array('id'=>$id))->save($a_e_data);
			if($rr > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'修改广告发布成功'));
			if($rr == 0) $this->ajaxReturn(array('code'=>10012,'msg'=>'修改广告发布失败,原因未知'));
		}
		
		
	
	}
	
	/*获取广播发布记录*/
	public function get_bcissue_list(){
		
		$bcissue_data = file_get_contents('php://input');//获取post数据
		$bcissue_data = json_decode($bcissue_data,true);//把json转化成数组
		$fbcsampleid = intval($bcissue_data['fbcsampleid']);//广播广告样本ID
		$p = intval($bcissue_data['p']);//需要返回的页数
		if($p == 0) $p = 1;
		$pp = intval($bcissue_data['pp']);//需要返回的条数
		if($pp <= 0) $pp = 10;
		if($pp > 100) $pp = 100;
		
		if($fbcsampleid == '')  $this->ajaxReturn(array('code'=>10025,'msg'=>'查询失败,缺少样本ID'));
		
		$fissuedate_s = $bcissue_data['fissuedate_s'];//发布日期 开始
		if($fissuedate_s == '') $fissuedate_s = '2015-01-01';
		$fissuedate_e = $bcissue_data['fissuedate_e'];//发布日期 结束
		if($fissuedate_e == '') $fissuedate_e = date('Y-m-d H:i:s');
		
		$where = array();
		$where['fbcsampleid'] = $fbcsampleid;
		$where['fissuedate'] = array('between',$fissuedate_s.','.$fissuedate_e);
		
		$count = M('tbcissue')->where($where)->count();
		
		$bcissueList = M('tbcissue')->where($where)->page($p,$pp)->select();

		$this->ajaxReturn(array('code'=>0,'msg'=>'','bcissueList'=>$bcissueList,'count'=>$count));


		
	}	
	
}