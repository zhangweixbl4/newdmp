<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 数据复核
 */

class FfuheController extends BaseController
{
	/**
	 * 获取复核数据列表
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function index()
	{
		session_write_close();
		$system_num = getconfig('system_num');

		$p = I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录

		$fadname 			= I('fadname');// 广告名称
		$fadclasscode 		= I('fadclasscode');// 广告内容类别
		$fmediaclass 		= I('fmediaclass');//媒体类型
		$fillegaltypecode 	= I('fillegaltypecode');// 违法类型
		$fissuedatest 		= I('fissuedatest');//开始时间
		$fissuedateed 		= I('fissuedateed');//结束时间

		$where['a.freviewstate'] 	= 10;
		$where['a.freview_tregulator'] 	= session('regulatorpersonInfo.fregulatorpid');//复核机构ID
		//机构数据查看权限范围
		if (!empty($fadname)) {
			$where['b.fadname'] = array('like', '%' . $fadname . '%');//广告名称
		}
		if (!empty($fadclasscode)) {
			$arr_code=D('Function')->get_next_tadclasscode($fadclasscode,true);//获取下级广告类别代码
			if($arr_code){
				$where['b.fadclasscode'] = array('in', $arr_code);
			}else{
				$where['b.fadclasscode'] = $fadclasscode;
			}
		}
		if (!empty($fillegaltypecode)) {//违法类型代码
			$where['fillegaltypecode'] = $fillegaltypecode;
		}
		if(!empty($fissuedatest) && !empty($fissuedateed)){
			$where['fissuedate'] = array('between',array($fissuedatest,$fissuedateed));
		}

		$where['a.fadid'] = array('neq',0);

		if($fmediaclass == 'tv'){
			$count = M('ttvsample')
				->alias('a')
				->join('tad b on a.fadid = b.fadid and b.fadid<>0')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id')
				->join('tmedia_temp ttp on f.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->count();
			
			$data = M('ttvsample')
				->alias('a')
				->field('a.fid,"tv" as mclass,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,c.ffullname,(case when a.fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltypecode,a.fissuedate')
				->join('tad b on a.fadid = b.fadid and b.fadid<>0')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id')
				->join('tmedia_temp ttp on f.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->order('a.fissuedate desc')
				->page($p,$pp)
				->select();//查询复核数据
		}elseif($fmediaclass == 'bc'){
			$count = M('tbcsample')
				->alias('a')
				->join('tad b on a.fadid = b.fadid and b.fadid<>0')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id')
				->join('tmedia_temp ttp on f.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->count();
			
			$data = M('tbcsample')
				->alias('a')
				->field('a.fid,"bc" as mclass,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,c.ffullname,(case when a.fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltypecode,a.fissuedate')
				->join('tad b on a.fadid = b.fadid and b.fadid<>0')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id')
				->join('tmedia_temp ttp on f.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->order('a.fissuedate desc')
				->page($p,$pp)
				->select();//查询复核数据
		}elseif($fmediaclass == 'paper'){
			$count = M('tpapersample')
				->alias('a')
				->join('tad b on a.fadid = b.fadid and b.fadid<>0')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id')
				->join('tmedia_temp ttp on f.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->count();
			
			$data = M('tpapersample')
				->alias('a')
				->field('a.fpapersampleid as fid,"paper" as mclass,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,c.ffullname,(case when a.fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltypecode,a.fissuedate')
				->join('tad b on a.fadid = b.fadid and b.fadid<>0')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id')
				->join('tmedia_temp ttp on f.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->order('a.fissuedate desc')
				->page($p,$pp)
				->select();//查询复核数据
		}

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	/**
	 * 生成复核数据
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据（count-生成个数）
	 * by zw
	 */
	public function createfuhe(){
		session_write_close();
		$system_num = getconfig('system_num');
		
		$createtype			= I('createtype')?I('createtype'):'10';//生成类型
		$createcount		= I('createcount')?I('createcount'):3;//生成数值
		$fissuedatest 		= I('fissuedatest');//发布开始时间
		$fissuedateed 		= I('fissuedateed');//发布结束时间
		$fmediaclass 		= I('fmediaclass');//媒体类型
		$fillegaltypecode 	= I('fillegaltypecode');// 违法类型
		$fadclasscode 		= I('fadclasscode');// 广告内容类别

		$where['a.freviewstate'] = 0;//已被复核过的记录不参与随机

		//相关广告内容类别，包含子级，有1个或多个
		if (!empty($fadclasscode)) {
			$arr_code = D('Function')->get_next_tadclasscode($fadclasscode,true);//获取下级广告类别代码
			if($arr_code){
				$where['b.fadclasscode'] = array('in', $arr_code);
			}else{
				$where['b.fadclasscode'] = $fadclasscode;
			}
		}
		if(!empty($fissuedatest)||!empty($fissuedateed)){
			$fissuedatest = $fissuedatest?$fissuedatest:date('Y-m-d');
			$fissuedateed = $fissuedateed?$fissuedateed:date('Y-m-d');
			$where['a.fissuedate'] = array('between',$fissuedatest.','.$fissuedateed);
		}
		if (!empty($fillegaltypecode)) {//违法类型代码
			$where['a.fillegaltypecode'] = $fillegaltypecode;
		}

		$fiddata = [];

		$where['a.fadid'] = array('neq',0);

		if($fmediaclass == 'tv'){
			$count = M('ttvsample')
				->alias('a')
				->join('tad b on a.fadid = b.fadid  and b.fadid<>0')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tillegaltype d on a.fillegaltypecode=d.fcode')//违法类型
				->join('tmedia_temp ttp on a.fmediaid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->count();
			$do_te = M('ttvsample')
				->alias('a')
				->field('a.fid')
				->join('tad b on a.fadid = b.fadid  and b.fadid<>0')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tillegaltype d on a.fillegaltypecode=d.fcode')//违法类型
				->join('tmedia_temp ttp on a.fmediaid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->order('rand()')
				->limit('0,'.($createtype=='10'?(ceil($createcount/100*$count)):$createcount))
				->select();
			foreach ($do_te as $key => $value) {
				array_push($fiddata, $value['fid']);
			}
			if(!empty($fiddata)){
				M()->execute('update ttvsample set freviewstate=10,review_state=0,freview_tregulator='.session('regulatorpersonInfo.fregulatorpid').',freview_userid='.session('regulatorpersonInfo.fid').' where fid in ('.implode(',',$fiddata).')');
			}
		}elseif($fmediaclass == 'bc'){
			$count = M('tbcsample')
				->alias('a')
				->join('tad b on a.fadid = b.fadid  and b.fadid<>0')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tillegaltype d on a.fillegaltypecode=d.fcode')//违法类型
				->join('tmedia_temp ttp on a.fmediaid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->count();
			$do_te = M('tbcsample')
				->alias('a')
				->field('a.fid')
				->join('tad b on a.fadid = b.fadid  and b.fadid<>0')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tillegaltype d on a.fillegaltypecode=d.fcode')//违法类型
				->join('tmedia_temp ttp on a.fmediaid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->order('rand()')
				->limit('0,'.($createtype=='10'?(ceil($createcount/100*$count)):$createcount))
				->select();
			foreach ($do_te as $key => $value) {
				array_push($fiddata, $value['fid']);
			}
			if(!empty($do_te)){
				M()->execute('update tbcsample set freviewstate=10,review_state=0,freview_tregulator='.session('regulatorpersonInfo.fregulatorpid').',freview_userid='.session('regulatorpersonInfo.fid').' where fid in ('.implode(',',$fiddata).')');
			}
		}elseif($fmediaclass == 'paper'){
			$count = M('tpapersample')
				->alias('a')
				->join('tad b on a.fadid = b.fadid  and b.fadid<>0')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tillegaltype d on a.fillegaltypecode=d.fcode')//违法类型
				->join('tmedia_temp ttp on a.fmediaid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->count();
			$do_te = M('tpapersample')
				->alias('a')
				->field('a.fid')
				->join('tad b on a.fadid = b.fadid  and b.fadid<>0')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tillegaltype d on a.fillegaltypecode=d.fcode')//违法类型
				->join('tmedia_temp ttp on a.fmediaid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->order('rand()')
				->limit('0,'.($createtype=='10'?(ceil($createcount/100*$count)):$createcount))
				->select();
			foreach ($do_te as $key => $value) {
				array_push($fiddata, $value['fid']);
			}
			if(!empty($do_te)){
				M()->execute('update tpapersample set freviewstate=10,review_state=0,freview_tregulator='.session('regulatorpersonInfo.fregulatorpid').',freview_userid='.session('regulatorpersonInfo.fid').' where fid in ('.implode(',',$fiddata).')');
			}
		}

		D('Function')->write_log('数据复核',1,'生成成功');
		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功'));
	}

	/**
	 * 获取样本详情
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data 数据
	 * by zw
	 */
	public function getfuheview()
	{

		$fid 	=I('fid');//样本ID
		$mclass	=I('mclass');//媒体类型
		if(empty($fid) || empty($mclass)){
			$this->error('参数缺失!');
		}

		if($mclass == 'tv'){
			$where['a.fid']	= $fid;
			$data = M('ttvsample')
				->alias('a')
				->field('
					b.fadname,
					b.fbrand,
					c.ffullname,
					c.fcode,
					c.fpcode,
					e.fname,
					a.favifilepng,
					a.fillegaltypecode,
					a.favifilename,
					"tv" as mclass,
					a.fversion,
					a.fexpressioncodes,
					a.fexpressions,
					a.fillegalcontent,
					a.fconfirmations,
					a.fadmanuno,
					a.fmanuno,
					a.fadapprno,
					a.fapprno,
					a.fadent,
					a.fentzone,
					a.fent,
					d.fcode as tfcode,
					d.fillegaltype,
					a.fissuedate,
					(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame
				')
				->join('tad b on a.fadid = b.fadid and b.fadid<>0 ')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tillegaltype d on a.fillegaltypecode=d.fcode')//违法类型
				->join('tadowner e on b.fadowner=e.fid')
				->join('tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id')
				->where($where)
				->find();
		}elseif($mclass == 'bc'){
			$where['a.fid']	=$fid;
			$data = M('tbcsample')
				->alias('a')
				->field('
					b.fadname,
					b.fbrand,
					c.ffullname,
					c.fcode,
					c.fpcode,
					e.fname,
					a.favifilepng,
					a.fillegaltypecode,
					a.favifilename,
					"bc" as mclass,
					a.fversion,
					a.fexpressioncodes,
					a.fexpressions,
					a.fillegalcontent,
					a.fconfirmations,
					a.fadmanuno,
					a.fmanuno,
					a.fadapprno,
					a.fapprno,
					a.fadent,
					a.fentzone,
					a.fent,
					d.fcode as tfcode,
					d.fillegaltype,
					a.fissuedate,
					(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame
				')
				->join('tad b on a.fadid = b.fadid and b.fadid<>0 ')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tillegaltype d on a.fillegaltypecode=d.fcode')//违法类型
				->join('tadowner e on b.fadowner=e.fid')
				->join('tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id')
				->where($where)
				->find();
		}elseif($mclass == 'paper'){
			$where['a.fpapersampleid']	=$fid;
			$data = M('tpapersample')
				->alias('a')
				->field('
					b.fadname,
					b.fbrand,
					c.ffullname,
					c.fcode,
					c.fpcode,
					e.fname,
					a.fjpgfilename as favifilepng,
					a.fillegaltypecode,
					a.fjpgfilename favifilename,
					"paper" as mclass,
					a.fversion,
					a.fexpressioncodes,
					a.fexpressions,
					a.fillegalcontent,
					a.fconfirmations,
					a.fadmanuno,
					a.fmanuno,
					a.fadapprno,
					a.fapprno,
					a.fadent,
					a.fentzone,
					a.fent,
					d.fcode as tfcode,
					d.fillegaltype,
					a.fissuedate,
					(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame
				')
				->join('tad b on a.fadid = b.fadid and b.fadid<>0 ')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tillegaltype d on a.fillegaltypecode=d.fcode')//违法类型
				->join('tadowner e on b.fadowner=e.fid')
				->join('tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id')
				->where($where)
				->find();
		}
		
		if(!empty($data)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据'));
		}
	}

	/**
	 * 单条数据申请复核操作
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data 数据
	 * by zw
	 */
	public function updatefuhe(){

		$value     = I('fid');//样本ID
	    $attachinfo = I('attachinfo');//上传的文件信息
	    $result     = I('result');//复核原因
	    $mclass     = I('mclass');//媒体类型
	    $fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');//机构级别，0区，10市，20省，30国家
		$tregionlevel = getconfig('tregionlevel');//平台等级，10区，20市，30省，40国家
	    $fregulatorppid = session('regulatorpersonInfo.fregulatorppid');
	    if(strlen($fregulatorppid) == 8){
	      $fregulatorppregion = substr($fregulatorppid,2,6);
	      $tregion_len = get_tregionlevel($fregulatorppregion);
	      if($tregion_len == 1){//国家级
	        $fregulatorppregion = 30;
	      }elseif($tregion_len == 2){//省级
	        $fregulatorppregion = 20;
	      }elseif($tregion_len == 4){//市级
	        $fregulatorppregion = 10;
	      }elseif($tregion_len == 6){//县级
	        $fregulatorppregion = 0;
	      }
	    }else{
	      $fregulatorppregion = $fregulatorlevel + 10;
	    }

	    $attach     = [];
	    
    	if($fregulatorlevel == 20){
    		$data_fj['fsample_id'] 		= $value;
	    	$data_fj['fsample_type'] 	= $mclass;
	    	$data_fj['tshengj_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_fj['tshengj_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_fj['tshengj_username']  = session('regulatorpersonInfo.fname');
            $data_fj['tshengj_time']      = date('Y-m-d H:i:s');
            $data_fj['tshengj_result']    = '同意';
	    	$data_fj['tregionid'] 		= session('regulatorpersonInfo.regionid');
	    	$data_fj['tcheck_content'] 	= $result;
	    	$data_fj['tcheck_jingdu'] 	= $fregulatorppregion;
	    	$data_fj['tcheck_status'] 	= 10;
	    	$data_fj['tcheck_createtime'] 	= date('Y-m-d');
	    	if($mclass=='tv'){
	    		M()->execute('update ttvsample set freviewstate=30,review_state=10 where fid='.$value);
	    		$mclass = 1;
	    	}elseif($mclass=='bc'){
	    		M()->execute('update tbcsample set freviewstate=30,review_state=10 where fid='.$value);
	    		$mclass = 2;
	    	}elseif($mclass=='paper'){
	    		M()->execute('update tpapersample set freviewstate=30,review_state=10 where fpapersampleid='.$value);
	    		$mclass = 3;
	    	}
	    	M('tbn_illegal_ad')->where(['fsample_id'=>$value,'fstatus'=>0,'fmedia_class'=>$mclass])->save(['fstatus'=>30,'fstatus3'=>30]);
	    	$weihuphone = getconfig('fuhephone');
	    	$fhphone = S('fh'.$weihuphone);
			if(empty($fhphone)){
	          S('fh'.$weihuphone,date('Y-m-d H:i:s'),43200);//半天一提醒
	          $sms_return = A('Common/Alitongxin','Model')->usertask_sms($weihuphone,'复核任务');
	        }
    	}elseif($fregulatorlevel == 10){
    		$data_fj['fsample_id'] 		= $value;
	    	$data_fj['fsample_type'] 	= $mclass;
	    	$data_fj['tshij_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_fj['tshij_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_fj['tshij_username']  = session('regulatorpersonInfo.fname');
            $data_fj['tshij_time']      = date('Y-m-d H:i:s');
            $data_fj['tregionid'] 		= session('regulatorpersonInfo.regionid');
	    	$data_fj['tcheck_content'] 	= $result;
	    	$data_fj['tcheck_jingdu'] 	= $fregulatorppregion;
	    	$data_fj['tcheck_createtime'] 	= date('Y-m-d');
	    	if(((int)$fregulatorlevel+10) == (int)$tregionlevel){
	    		$data_fj['tshij_result']    = '同意';
	    		$data_fj['tcheck_status']   = 10;
		    	if($mclass=='tv'){
		    		M()->execute('update ttvsample set freviewstate=30,review_state=10 where fid='.$value);
		    		$mclass = 1;
		    	}elseif($mclass=='bc'){
		    		M()->execute('update tbcsample set freviewstate=30,review_state=10 where fid='.$value);
		    		$mclass = 2;
		    	}elseif($mclass=='paper'){
		    		M()->execute('update tpapersample set freviewstate=30,review_state=10 where fpapersampleid='.$value);
		    		$mclass = 3;
		    	}
		    	M('tbn_illegal_ad')->where(['fsample_id'=>$value,'fstatus'=>0,'fmedia_class'=>$mclass])->save(['fstatus'=>30,'fstatus3'=>30]);
		    }else{
            	$data_fj['tshij_result']    = '申请复核';
		    	if($mclass=='tv'){
		    		M()->execute('update ttvsample set freviewstate=20 where fid='.$value);
		    	}elseif($mclass=='bc'){
		    		M()->execute('update tbcsample set freviewstate=20 where fid='.$value);
		    	}elseif($mclass=='paper'){
		    		M()->execute('update tpapersample set freviewstate=20 where fpapersampleid='.$value);
		    	}
		    }
    	}elseif($fregulatorlevel == 0){
    		$data_fj['fsample_id'] 		= $value;
	    	$data_fj['fsample_type'] 	= $mclass;
	    	$data_fj['tquj_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_fj['tquj_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_fj['tquj_username']  = session('regulatorpersonInfo.fname');
            $data_fj['tquj_time']      = date('Y-m-d H:i:s');
            $data_fj['tquj_result']    = '申请复核';
            $data_fj['tregionid'] 		= session('regulatorpersonInfo.regionid');
	    	$data_fj['tcheck_content'] 	= $result;
	    	$data_fj['tcheck_jingdu'] 	= $fregulatorppregion;
	    	$data_fj['tcheck_createtime'] 	= date('Y-m-d');
	    	if($mclass=='tv'){
	    		M()->execute('update ttvsample set freviewstate=20 where fid='.$value);
	    	}elseif($mclass=='bc'){
	    		M()->execute('update tbcsample set freviewstate=20 where fid='.$value);
	    	}elseif($mclass=='paper'){
	    		M()->execute('update tpapersample set freviewstate=20 where fpapersampleid='.$value);
	    	}
    	}
    	
      	$do_fj = M('fj_data_check')->add($data_fj);
      	if(!empty($attachinfo)){
      		//上传附件
	        $attach_data['fsample_id']    		= $value;//样本ID
	        $attach_data['fsample_type']    	= $mclass;//媒体类型
	        $attach_data['fcheck_id']        	= $do_fj;//复核表Id
	        $attach_data['fupload_trelevel'] 	= $fregulatorlevel;//上传机构级别
	        $attach_data['ffile_type']       	= 0;//复核类型
	        $attach_data['fstate']        		= 0;//状态
	        $attach_data['fupload_treid']    	= session('regulatorpersonInfo.fregulatorpid');//机构ID
	        $attach_data['fupload_trename']  	= session('regulatorpersonInfo.regulatorpname');//机构名
	        $attach_data['fuploader']        	= session('regulatorpersonInfo.fname');//上传人
	        $attach_data['fupload_time']     	= date('Y-m-d H:i:s');//上传时间
	        foreach ($attachinfo as $key2 => $value2){
	          $attach_data['fattach']     = $value2['fattachname'];
	          $attach_data['fattach_url'] = $value2['fattachurl'];
	          array_push($attach,$attach_data);
	        }
	        if(!empty($attach)){
	     		M('fj_data_check_attach')->addAll($attach);
	        }
      	}
        
       D('Function')->write_log('数据复核',1,'复核提交成功','fj_data_check',$do_fj,M('fj_data_check')->getlastsql());
      $this->ajaxReturn(array('code'=>0,'msg'=>'提交成功'));
	}

	/**
	 * 获取复核任务列表
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function getfuhetask()
	{

		$p = (int)I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录
		$limitstr = ' limit '.($p-1)*$pp.','.$pp;

		$fadname 			= I('fadname');// 广告名称
		$fadclasscode 		= I('fadclasscode');// 广告内容类别
		$fmediaclass 		= I('fmediaclass');//媒体类型
		$fillegaltypecode 	= I('fillegaltypecode');// 违法类型
		$fissuedatest 		= I('fissuedatest');//开始时间
		$fissuedateed 		= I('fissuedateed');//结束时间

		$fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');
	    $where_tia = '1=1';
	    if($fregulatorlevel == 20){
			$where_tia .= ' and a.tregionid like "'.substr(session('regulatorpersonInfo.regionid'),0,2).'%"';
		}elseif($fregulatorlevel == 10){
			$where_tia .= ' and a.tregionid like "'.substr(session('regulatorpersonInfo.regionid'),0,4).'%"';
		}elseif($fregulatorlevel == 0){
			$where_tia .= ' and a.tregionid like "'.substr(session('regulatorpersonInfo.regionid'),0,6).'%"';
		}
	    $where_tia .= ' and a.tcheck_jingdu = '.$fregulatorlevel;//复核进度
	    $where_tia .= ' and a.tcheck_status = 0';//处理状态
	    if(!empty($fmediaclass)){
		    $where_tia .= ' and a.fsample_type = "'.$fmediaclass.'"';//媒体类型
	    }
	    if(!empty($fadname)){
		    $where_tia .= ' and b.fadname like "%'.$fadname.'%"';//广告名称
	    }

		if (!empty($fadclasscode)) {
			$arr_code=D('Function')->get_next_tadclasscode($fadclasscode,true);//获取下级广告类别代码
			if($arr_code){
				$where_tia .= ' and b.fadclasscode in ('.implode(',', $arr_code).')';
			}else{
				$where_tia .= ' and b.fadclasscode = '.$fadclasscode;
			}
		}
		if (!empty($fillegaltypecode)) {//违法类型代码
			$where_tia .= ' and g.fillegaltypecode = '.$fillegaltypecode;
		}
		if(!empty($fissuedatest) && !empty($fissuedateed)){
			$where_tia .= ' and a.tcheck_createtime between "'.$fissuedatest.'" and "'.$fissuedateed.'"';
		}

	    if($fmediaclass=='tv'){
	    	$countsql = 'select count(*) as acount 
	    		from fj_data_check a
				inner join ttvsample g on g.fid = a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "tv"';
			$count = M()->query($countsql);

			$datasql = 'select a.fid,a.fsample_id,"tv" as mclass,c.ffullname,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,g.fillegalcontent,a.tshengj_result,a.tshengj_time,a.tshengj_username,a.tshij_result,a.tshij_time,a.tshij_username,a.tquj_result,a.tquj_time,a.tquj_username 
	    		from fj_data_check a
				inner join ttvsample g on g.fid = a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "tv"
				order by a.fid desc '.$limitstr;
	    }elseif($fmediaclass=='bc'){
	    	$countsql = 'select count(*) as acount 
	    		from fj_data_check a
				inner join tbcsample g on g.fid = a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "bc"';
			$count = M()->query($countsql);

			$datasql = 'select a.fid,a.fsample_id,"bc" as mclass,c.ffullname,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,g.fillegalcontent,a.tshengj_result,a.tshengj_time,a.tshengj_username,a.tshij_result,a.tshij_time,a.tshij_username,a.tquj_result,a.tquj_time,a.tquj_username 
	    		from fj_data_check a
				inner join tbcsample g on g.fid = a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "bc"
				order by a.fid desc '.$limitstr;
	    }elseif($fmediaclass=='paper'){
	    	$countsql = 'select count(*) as acount 
	    		from fj_data_check a
				inner join tpapersample g on g.fpapersampleid=a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "paper"';
			$count = M()->query($countsql);

			$datasql = 'select a.fid,a.fsample_id,"paper" as mclass,c.ffullname,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,g.fillegalcontent,a.tshengj_result,a.tshengj_time,a.tshengj_username,a.tshij_result,a.tshij_time,a.tshij_username,a.tquj_result,a.tquj_time,a.tquj_username 
	    		from fj_data_check a
				inner join tpapersample g on g.fpapersampleid=a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "paper"
				order by a.fid desc '.$limitstr;
	    }else{
	    	$countsql = 'select sum(acount) as acount from (
	    	(select count(*) as acount 
	    		from fj_data_check a
				inner join ttvsample g on g.fid = a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "tv")
			union all (select count(*) as acount 
	    		from fj_data_check a
				inner join tbcsample g on g.fid = a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "bc")
			union all (select count(*) as acount 
	    		from fj_data_check a
				inner join tpapersample g on g.fpapersampleid=a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "paper")
			) as a';
			$count = M()->query($countsql);

			$datasql = 'select * from (
			(select a.fid,a.fsample_id,"tv" as mclass,c.ffullname,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,g.fillegalcontent,a.tshengj_result,a.tshengj_time,a.tshengj_username,a.tshij_result,a.tshij_time,a.tshij_username,a.tquj_result,a.tquj_time,a.tquj_username 
	    		from fj_data_check a
				inner join ttvsample g on g.fid = a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "tv") 
			union all (select a.fid,a.fsample_id,"bc" as mclass,c.ffullname,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,g.fillegalcontent,a.tshengj_result,a.tshengj_time,a.tshengj_username,a.tshij_result,a.tshij_time,a.tshij_username,a.tquj_result,a.tquj_time,a.tquj_username 
	    		from fj_data_check a
				inner join tbcsample g on g.fid = a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "bc") 
			union all (select a.fid,a.fsample_id,"paper" as mclass,c.ffullname,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,g.fillegalcontent,a.tshengj_result,a.tshengj_time,a.tshengj_username,a.tshij_result,a.tshij_time,a.tshij_username,a.tquj_result,a.tquj_time,a.tquj_username 
	    		from fj_data_check a
				inner join tpapersample g on g.fpapersampleid=a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "paper") 
			) as a ORDER BY a.fid desc'.$limitstr;
	    }
    	$data = M()->query($datasql);
	    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count[0]['acount'],'list'=>$data)));
	}

	/**
	 * 获取任务复核详情
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data 数据
	 * by zw
	 */
	public function getfuhetaskview()
	{

		$fid = I('fid');//复核表id

		$where['fid'] = $fid;
		$data = M('fj_data_check')
			->where($where)
			->find();
		if(!empty($data)){
			$where_d['fcheck_id'] = $fid;
			$where_d['fstate'] = 0;
			$data['flow']=M('fj_data_check_attach')
				->where($where_d)
				->order('ffile_type asc,fupload_trelevel desc,fid desc')
				->select();
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据','data'=>$data));
		}
	}

	/**
	 * 复核任务复核操作
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data 数据
	 * by zw
	 */
	public function updatefuhetask(){

		$fid 		= I('fid');//复核记录id
		$result     = I('result');//处理结果
		$attachinfo = I('attachinfo');//上传的文件信息
		$fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');//机构级别，0区，10市，20省，30国家
		$tregionlevel = getconfig('tregionlevel');//平台等级，10区，20市，30省，40国家

	    $where_tia['fid'] = $fid;
		$where_tia['tcheck_jingdu'] = $fregulatorlevel;//复核进度
		$where_tia['tcheck_status'] = 0;//处理状态
		$attach = [];

		$do_tia = M('fj_data_check')->field('fsample_id,fsample_type,tcheck_jingdu')->where($where_tia)->find();//是否有权限处理
		if(!empty($do_tia)){
			if($fregulatorlevel == 20){//省级
			    $data_tdc['tshengj_treid']     = session('regulatorpersonInfo.fregulatorpid');
			    $data_tdc['tshengj_trename']   = session('regulatorpersonInfo.regulatorpname');
			    $data_tdc['tshengj_username']  = session('regulatorpersonInfo.fname');
			    $data_tdc['tshengj_time']      = date('Y-m-d H:i:s');
			    $data_tdc['tshengj_result']    = $result;
			    $data_tdc['tcheck_jingdu']     = $do_tia['tcheck_jingdu']+10;
			    if($result == '同意'){
			    	if(((int)$fregulatorlevel+10) == (int)$tregionlevel){
			    		$data_tdc['tcheck_status']  = 10;
						if($do_tia['fsample_type']=='tv'){
							M()->execute('update ttvsample set freviewstate=30,review_state=10 where fid='.$do_tia['fsample_id']);
							$mclass = 1;
						}elseif($do_tia['fsample_type']=='bc'){
							M()->execute('update tbcsample set freviewstate=30,review_state=10 where fid='.$do_tia['fsample_id']);
							$mclass = 2;
						}elseif($do_tia['fsample_type']=='paper'){
							M()->execute('update tpapersample set freviewstate=30,review_state=10 where fpapersampleid='.$do_tia['fsample_id']);
							$mclass = 3;
						}
						M('tbn_illegal_ad')->where(['fsample_id'=>$do_tia['fsample_id'],'fstatus'=>0,'fmedia_class'=>$mclass])->save(['fstatus'=>30,'fstatus3'=>30]);
			    	}else{
			    		$data_tdc['tcheck_status']  = 0;
			    	}
			        
					$weihuphone = getconfig('fuhephone');
					$fhphone = S('fh'.$weihuphone);
					if(empty($fhphone)){
			          S('fh'.$weihuphone,date('Y-m-d H:i:s'),43200);//半天一提醒
			          $sms_return = A('Common/Alitongxin','Model')->usertask_sms($weihuphone,'复核任务');
			        }
			    }else{
			      $data_tdc['tcheck_status']  = 20;
					if($do_tia['fsample_type']=='tv'){
						M()->execute('update ttvsample set freviewstate=0,review_state=20 where fid='.$do_tia['fsample_id']);
					}elseif($do_tia['fsample_type']=='bc'){
						M()->execute('update tbcsample set freviewstate=0,review_state=20 where fid='.$do_tia['fsample_id']);
					}elseif($do_tia['fsample_type']=='paper'){
						M()->execute('update tpapersample set freviewstate=0,review_state=20 where fpapersampleid='.$do_tia['fsample_id']);
					}
			    }
			 
			  M('fj_data_check')->where($where_tia)->save($data_tdc);
			}elseif($fregulatorlevel == 10){//市级
			  $data_tdc['tshij_treid']     = session('regulatorpersonInfo.fregulatorpid');
			  $data_tdc['tshij_trename']   = session('regulatorpersonInfo.regulatorpname');
			  $data_tdc['tshij_username']  = session('regulatorpersonInfo.fname');
			  $data_tdc['tshij_time']      = date('Y-m-d H:i:s');
			  $data_tdc['tshij_result']    = $result;
			  $data_tdc['tcheck_jingdu']     = $do_tia['tcheck_jingdu']+10;
			  if($result == '同意'){
			  		if(((int)$fregulatorlevel+10) == (int)$tregionlevel){
			  			$data_tdc['tcheck_status']  = 10;
						if($do_tia['fsample_type']=='tv'){
							M()->execute('update ttvsample set freviewstate=30,review_state=10 where fid='.$do_tia['fsample_id']);
							$mclass = 1;
						}elseif($do_tia['fsample_type']=='bc'){
							M()->execute('update tbcsample set freviewstate=30,review_state=10 where fid='.$do_tia['fsample_id']);
							$mclass = 2;
						}elseif($do_tia['fsample_type']=='paper'){
							M()->execute('update tpapersample set freviewstate=30,review_state=10 where fpapersampleid='.$do_tia['fsample_id']);
							$mclass = 3;
						}
						M('tbn_illegal_ad')->where(['fsample_id'=>$do_tia['fsample_id'],'fstatus'=>0,'fmedia_class'=>$mclass])->save(['fstatus'=>30,'fstatus3'=>30]);
			  		}else{
			    		$data_tdc['tcheck_status']  = 0;
			    	}
			    }else{
			      $data_tdc['tcheck_status']  = 20;
					if($do_tia['fsample_type']=='tv'){
						M()->execute('update ttvsample set freviewstate=0,review_state=20 where fid='.$do_tia['fsample_id']);
					}elseif($do_tia['fsample_type']=='bc'){
						M()->execute('update tbcsample set freviewstate=0,review_state=20 where fid='.$do_tia['fsample_id']);
					}elseif($do_tia['fsample_type']=='paper'){
						M()->execute('update tpapersample set freviewstate=0,review_state=20 where fpapersampleid='.$do_tia['fsample_id']);
					}
			    }
			  M('fj_data_check')->where($where_tia)->save($data_tdc);
			}
			if(!empty($attachinfo)){
	      		//上传附件
		        $attach_data['fsample_id']    		= $do_tia['fsample_id'];//样本ID
		        $attach_data['fsample_type']    	= $do_tia['fsample_type'];//媒体类型
		        $attach_data['fcheck_id']        	= $fid;//复核表Id
		        $attach_data['fupload_trelevel'] 	= $fregulatorlevel;//上传机构级别
		        $attach_data['ffile_type']       	= 10;//复核类型
		        $attach_data['fstate']        		= 0;//状态
		        $attach_data['fupload_treid']    	= session('regulatorpersonInfo.fregulatorpid');//机构ID
		        $attach_data['fupload_trename']  	= session('regulatorpersonInfo.regulatorpname');//机构名
		        $attach_data['fuploader']        	= session('regulatorpersonInfo.fname');//上传人
		        $attach_data['fupload_time']     	= date('Y-m-d H:i:s');//上传时间
		        foreach ($attachinfo as $key2 => $value2){
		          $attach_data['fattach']     = $value2['fattachname'];
		          $attach_data['fattach_url'] = $value2['fattachurl'];
		          array_push($attach,$attach_data);
		        }
		        if(!empty($attach)){
		     		M('fj_data_check_attach')->addAll($attach);
		        }
	      	}
		}
		D('Function')->write_log('复核任务',1,'复核成功','fj_data_check',$fid,M('fj_data_check')->getlastsql());
		$this->ajaxReturn(array('code'=>0,'msg'=>'提交成功'));

	}

	/**
	 * 获取复核台账列表
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data 数据
	 * by zw
	 */
	public function getfuheledger(){
		session_write_close();
		
		$p = (int)I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录
		$limitstr = ' limit '.($p-1)*$pp.','.$pp;

		$fadname 			= I('fadname');// 广告名称
		$fadclasscode 		= I('fadclasscode');// 广告内容类别
		$fmediaclass 		= I('fmediaclass');//媒体类型
		$fillegaltypecode 	= I('fillegaltypecode');// 违法类型
		$fissuedatest 		= I('fissuedatest');//开始时间
		$fissuedateed 		= I('fissuedateed');//结束时间

		$where_tia = '1=1';
		if(!empty($fmediaclass)){
			$where_tia .= ' and a.fsample_type = "'.$fmediaclass.'"';//媒体类型
		}
		$fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');//机构级别
      	if($fregulatorlevel == 30){
	      $where_tia .= ' and a.tgj_treid ='.session('regulatorpersonInfo.fregulatorpid');//机构ID
	    }elseif($fregulatorlevel == 20){
	      $where_tia .= ' and a.tshengj_treid ='.session('regulatorpersonInfo.fregulatorpid');//机构ID
	    }elseif($fregulatorlevel == 10){
	      $where_tia .= ' and a.tshij_treid ='.session('regulatorpersonInfo.fregulatorpid');//机构ID
	    }elseif($fregulatorlevel == 0){
	      $where_tia .= ' and a.tquj_treid ='.session('regulatorpersonInfo.fregulatorpid');//机构ID
	    }

	    if(!empty($fadname)){
	    	$where_tia .= ' and b.fadname like "%'.$fadname.'%"';//媒体类型
	    }

		if (!empty($fadclasscode)) {
			$arr_code=D('Function')->get_next_tadclasscode($fadclasscode,true);//获取下级广告类别代码
			if($arr_code){
				$where_tia .= ' and b.fadclasscode in ('.implode(',', $arr_code).')';
			}else{
				$where_tia .= ' and b.fadclasscode = '.$fadclasscode;
			}
		}
		if (!empty($fillegaltypecode)) {//违法类型代码
			$where_tia .= ' and g.fillegaltypecode = '.$fillegaltypecode;
		}
		if(!empty($fissuedatest) && !empty($fissuedateed)){
			$where_tia .= ' and a.tcheck_createtime between "'.$fissuedatest.'" and "'.$fissuedateed.'"';
		}

	    if($fmediaclass=='tv'){
	    	$countsql = 'select count(*) as acount 
	    		from fj_data_check a
				inner join ttvsample g on g.fid = a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "tv"';
			$count = M()->query($countsql);

			$datasql = 'select a.fid,a.fsample_id,"tv" as mclass,c.ffullname,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,g.fillegalcontent,a.tshengj_result,a.tshengj_time,a.tshengj_username,a.tshij_result,a.tshij_time,a.tshij_username,a.tquj_result,a.tquj_time,a.tquj_username,a.tcheck_fhreson 
	    		from fj_data_check a
				inner join ttvsample g on g.fid = a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "tv"
				order by a.fid desc '.$limitstr;
	    }elseif($fmediaclass=='bc'){
	    	$countsql = 'select count(*) as acount 
	    		from fj_data_check a
				inner join tbcsample g on g.fid = a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "bc"';
			$count = M()->query($countsql);

			$datasql = 'select a.fid,a.fsample_id,"tv" as mclass,c.ffullname,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,g.fillegalcontent,a.tshengj_result,a.tshengj_time,a.tshengj_username,a.tshij_result,a.tshij_time,a.tshij_username,a.tquj_result,a.tquj_time,a.tquj_username,a.tcheck_fhreson  
	    		from fj_data_check a
				inner join tbcsample g on g.fid = a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "bc"
				order by a.fid desc '.$limitstr;
	    }elseif($fmediaclass=='paper'){
	    	$countsql = 'select count(*) as acount 
	    		from fj_data_check a
				inner join tpapersample g on g.fpapersampleid=a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "paper"';
			$count = M()->query($countsql);

			$datasql = 'select a.fid,a.fsample_id,"tv" as mclass,c.ffullname,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,g.fillegalcontent,a.tshengj_result,a.tshengj_time,a.tshengj_username,a.tshij_result,a.tshij_time,a.tshij_username,a.tquj_result,a.tquj_time,a.tquj_username,a.tcheck_fhreson 
	    		from fj_data_check a
				inner join tpapersample g on g.fpapersampleid=a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "paper"
				order by a.fid desc '.$limitstr;
	    }else{
	    	$countsql = 'select sum(acount) as acount from (
	    	(select count(*) as acount 
	    		from fj_data_check a
				inner join ttvsample g on g.fid = a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "tv")
			union all (select count(*) as acount 
	    		from fj_data_check a
				inner join tbcsample g on g.fid = a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "bc")
			union all (select count(*) as acount 
	    		from fj_data_check a
				inner join tpapersample g on g.fpapersampleid=a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "paper")
			) as a';
			$count = M()->query($countsql);

			$datasql = 'select * from (
			(select a.fid,a.fsample_id,"tv" as mclass,c.ffullname,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,g.fillegalcontent,a.tshengj_result,a.tshengj_time,a.tshengj_username,a.tshij_result,a.tshij_time,a.tshij_username,a.tquj_result,a.tquj_time,a.tquj_username,a.tcheck_fhreson 
	    		from fj_data_check a
				inner join ttvsample g on g.fid = a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "tv") 
			union all (select a.fid,a.fsample_id,"bc" as mclass,c.ffullname,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,g.fillegalcontent,a.tshengj_result,a.tshengj_time,a.tshengj_username,a.tshij_result,a.tshij_time,a.tshij_username,a.tquj_result,a.tquj_time,a.tquj_username,a.tcheck_fhreson 
	    		from fj_data_check a
				inner join tbcsample g on g.fid = a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "bc") 
			union all (select a.fid,a.fsample_id,"paper" as mclass,c.ffullname,b.fadname,(case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,g.fillegalcontent,a.tshengj_result,a.tshengj_time,a.tshengj_username,a.tshij_result,a.tshij_time,a.tshij_username,a.tquj_result,a.tquj_time,a.tquj_username,a.tcheck_fhreson 
	    		from fj_data_check a
				inner join tpapersample g on g.fpapersampleid=a.fsample_id 
				inner join tad b on g.fadid = b.fadid and b.fadid<>0
				inner join tadclass c on b.fadclasscode=c.fcode
				inner join tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id 
				where '.$where_tia.' and fsample_type = "paper") 
			) as a ORDER BY a.fid desc'.$limitstr;
	    }
    	$data = M()->query($datasql);
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count[0]['acount'],'list'=>$data)));
	}

	/**
  * 查看复核证据
  * by zw
  */
  public function fjjieguo(){
    $fid = I('fid');//复核表ID
    $fupload_trelevel = I('fupload_trelevel');//机构级别
    $where_tia['a.fcheck_id']         = $fid;
    $where_tia['a.fstate']            = 0;
    $where_tia['a.fupload_trelevel']  = $fupload_trelevel;

    //复核证据附件
    $do_tia = M('fj_data_check_attach')->field('a.fattach,a.fattach_url')->alias('a')->where($where_tia)->order('a.fid asc')->select();

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','zjfjdata'=>$do_tia));
  }

  //获取违法表现列表
	public function gettillegal($pid=''){
		$datatl=M('tillegal')->field('fid,fcode,fexpression,fconfirmation')->where(['fpcode'=>$pid,'fstate'=>1])->select();
		if(!empty($datatl)){
			if(empty($pid)){
				foreach ($datatl as $key => $value) {
					$datatl[$key]['list_2']=$this->gettillegal($value['fcode']);;
				}
			}else{
				return $datatl;
			}
		}
		return $datatl;
	}

	public function tillegal(){
		$this->ajaxReturn($this->gettillegal());
	}

}