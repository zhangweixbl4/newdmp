<?php

namespace Test\Controller;
use Think\Controller;
use Think\Exception;

class IllegalController extends Controller{
	
	
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}
	
	
	
	public function o_n_c($o_c){
		
		$o_n_c_arr = array(
		
							'201601027'=>'YP1',
							'201601001'=>'YP2',
							'201601002'=>'YP3',
							'201601035'=>'YP4',
							'201601009'=>'YP5',
							'201601004'=>'YP6',
							'201601013'=>'YP7',
							'201601014'=>'YP8',
							'201601036'=>'YP9',
							'201601026'=>'YP10',
							'201601012'=>'YP11',
							'201601037'=>'YP12',
							'201601034'=>'YP13',
							'201601030'=>'YP14',
							'201601003'=>'YP15',
							'201601005'=>'YP16',
							'201601006'=>'YP17',
							'201601007'=>'YP18',
							'201601008'=>'YP19',
							'201601010'=>'YP20',
							'201601011'=>'YP21',
							'201601015'=>'YP22',
							'201601016'=>'YP23',
							'201601017'=>'YP24',
							'201601018'=>'YP25',
							'201601019'=>'YP26',
							'201601020'=>'YP27',
							'201601021'=>'YP28',
							'201601022'=>'YP29',
							'201601023'=>'YP30',
							'201601024'=>'YP31',
							'201601025'=>'YP32',
							'201601028'=>'YP33',
							'201601029'=>'YP34',
							'201601031'=>'YP35',
							'201601032'=>'YP36',
							'201601033'=>'YP37',
							'201602022'=>'YL1',
							'201602007'=>'YL2',
							'201602008'=>'YL3',
							'201602020'=>'YL4',
							'201602021'=>'YL5',
							'201602011'=>'YL6',
							'201602015'=>'YL7',
							'201602023'=>'YL8',
							'201602001'=>'YL9',
							'201602018'=>'YL10',
							'201602002'=>'YL11',
							'201602003'=>'YL12',
							'201602004'=>'YL13',
							'201602005'=>'YL14',
							'201602006'=>'YL15',
							'201602009'=>'YL16',
							'201602010'=>'YL17',
							'201602012'=>'YL18',
							'201602013'=>'YL19',
							'201602014'=>'YL20',
							'201602016'=>'YL21',
							'201602017'=>'YL22',
							'201603010'=>'YLQX1',
							'201603011'=>'YLQX2',
							'201603012'=>'YLQX3',
							'201603032'=>'YLQX4',
							'201603023'=>'YLQX5',
							'201603004'=>'YLQX6',
							'201603007'=>'YLQX7',
							'201603033'=>'YLQX8',
							'201603027'=>'YLQX9',
							'201603030'=>'YLQX10',
							'201603031'=>'YLQX11',
							'201603001'=>'YLQX12',
							'201603002'=>'YLQX13',
							'201603003'=>'YLQX14',
							'201603005'=>'YLQX15',
							'201603006'=>'YLQX16',
							'201603008'=>'YLQX17',
							'201603009'=>'YLQX18',
							'201603013'=>'YLQX19',
							'201603014'=>'YLQX20',
							'201603015'=>'YLQX21',
							'201603016'=>'YLQX22',
							'201603017'=>'YLQX23',
							'201603018'=>'YLQX24',
							'201603019'=>'YLQX25',
							'201603020'=>'YLQX26',
							'201603021'=>'YLQX27',
							'201603022'=>'YLQX28',
							'201603024'=>'YLQX29',
							'201603025'=>'YLQX30',
							'201603026'=>'YLQX31',
							'201603028'=>'YLQX32',
							'201603029'=>'YLQX33',
							'201606003'=>'SP1',
							'201606012'=>'SP2',
							'201606013'=>'SP3',
							'201606014'=>'SP4',
							'201606007'=>'SP5',
							'201606015'=>'SP6',
							'201606016'=>'SP7',
							'201606017'=>'SP8',
							'201606018'=>'SP9',
							'201606019'=>'SP10',
							'201606020'=>'SP11',
							'201606011'=>'SP12',
							'201606001'=>'SP13',
							'201606002'=>'SP14',
							'201606004'=>'SP15',
							'201606005'=>'SP16',
							'201606006'=>'SP17',
							'201606008'=>'SP18',
							'201606009'=>'SP19',
							'201606010'=>'SP20',
							'201608001'=>'YC1',
							'201608002'=>'YC2',
							'201608003'=>'YC3',
							'201610015'=>'FDC1',
							'201610024'=>'FDC2',
							'201610025'=>'FDC3',
							'201610026'=>'FDC4',
							'201610027'=>'FDC5',
							'201610001'=>'FDC6',
							'201610002'=>'FDC7',
							'201610003'=>'FDC8',
							'201610004'=>'FDC9',
							'201610005'=>'FDC10',
							'201610006'=>'FDC11',
							'201610007'=>'FDC12',
							'201610008'=>'FDC13',
							'201610009'=>'FDC14',
							'201610010'=>'FDC15',
							'201610011'=>'FDC16',
							'201610012'=>'FDC17',
							'201610013'=>'FDC18',
							'201610014'=>'FDC19',
							'201610016'=>'FDC20',
							'201610017'=>'FDC21',
							'201610018'=>'FDC22',
							'201610019'=>'FDC23',
							'201610020'=>'FDC24',
							'201610021'=>'FDC25',
							'201610022'=>'FDC26',
							'201610023'=>'FDC27',
							'201611002'=>'ZH1',
							'201611001'=>'ZH2',
							'201611013'=>'ZH3',
							'201611014'=>'ZH4',
							'201611040'=>'ZH5',
							'201611003'=>'ZH6',
							'201611004'=>'ZH7',
							'201611005'=>'ZH8',
							'201611038'=>'ZH9',
							'201611006'=>'ZH10',
							'201611039'=>'ZH11',
							'201611007'=>'ZH12',
							'201611008'=>'ZH13',
							'201611009'=>'ZH14',
							'201611010'=>'ZH15',
							'201611011'=>'ZH16',
							'201611012'=>'ZH17',
							'201611041'=>'ZH18',
							'201611015'=>'ZH19',
							'201611015'=>'ZH20',
							'201611015'=>'ZH21',
							'201611018'=>'ZH22',
							'201611019'=>'ZH23',
							'201611020'=>'ZH24',
							'201611021'=>'ZH25',
							'201611022'=>'ZH26',
							'201611023'=>'ZH27',
							'201611024'=>'ZH28',
							'201611042'=>'ZH29',
							'201611043'=>'ZH30',
							'201611044'=>'ZH31',
							'201611045'=>'ZH32',
							'201611046'=>'ZH33',
							'201611047'=>'ZH34',
							'201611048'=>'ZH35',
							'201611049'=>'ZH36',
							'201611050'=>'ZH37',
							'201611051'=>'ZH38',
							'201611052'=>'ZH39',
							'201611053'=>'ZH40',
							'201611054'=>'ZH41',
							'201611055'=>'ZH42',
							'201611056'=>'ZH43',
							'201611057'=>'ZH44',
							'201611058'=>'ZH45',
							'201611059'=>'ZH46',
							'201611060'=>'ZH47',
							'201611061'=>'ZH48',
							'201611062'=>'ZH49',
							'201611063'=>'ZH50',
							'201611025'=>'ZH51',
							'201611064'=>'ZH52',
							'201611065'=>'ZH53',
							'201611066'=>'ZH54',
							'201611067'=>'ZH55',
							'201611068'=>'ZH56',
							'201611026'=>'ZH57',
							'201611027'=>'ZH58',
							'201611028'=>'ZH59',
							'201611029'=>'ZH60',
							'201611030'=>'ZH61',
							'201611031'=>'ZH62',
							'201611032'=>'ZH63',
							'201611033'=>'ZH64',
							'201611034'=>'ZH65',
							'201611035'=>'ZH66',
							'201611036'=>'ZH67',
							'201611037'=>'ZH68',

		
							);
		$n_c = 	$o_n_c_arr[$o_c];
		if(!$n_c){
			$n_c = $o_c;
		}
		return $n_c;					
		
	}





	public function sam(){
		
		$media_tab = I('mm');

		if($media_tab == ''){
			$media_tab = 'tv';
		}
		
		if($media_tab == 'tv' || $media_tab == 'bc'){
			$fidField = 'fid';
		}elseif($media_tab == 'paper'){
			$fidField = 'fpapersampleid';
		}

		$samListCount = M('t'.$media_tab.'sample')->where(array('freviewstate'=>0,'fillegaltypecode'=>array('gt',0)))->count();

		
		$samList = M('t'.$media_tab.'sample')->field($fidField.' as fid,fexpressioncodes')->where(array('freviewstate'=>0,'fillegaltypecode'=>array('gt',0)))->limit(500)->select();
		echo '剩余：'.$samListCount."\n";
		foreach($samList as $sam){

			$old_fexpressioncodes_arr = explode(';',$sam['fexpressioncodes']);
			$new_fexpressioncodes_arr = array();
			
			foreach($old_fexpressioncodes_arr as $old_fexpressioncodes){
				
				$new_fexpressioncodes_arr[] = $this->o_n_c($old_fexpressioncodes);
			}
			
			if($new_fexpressioncodes_arr){
				$illegalList = M('tillegal')->where(array('fcode'=>array('in',$new_fexpressioncodes_arr)))->select();
				$fexpressioncodes_array = array();
				$fexpressions_array = array();
				$fconfirmations_array = array();
				$fpunishments_array = array();
				$fpunishmenttypes_array = array();
				
				
				foreach($illegalList as $illegal){
					$fexpressioncodes_array[] = $illegal['fcode'];//违法表现代码
					$fexpressions_array[] = $illegal['fexpression'];//违法表现
					$fconfirmations_array[] = $illegal['fconfirmation'];//认定依据
					$fpunishments_array[] = $illegal['fpunishment'];//处罚依据
					$fpunishmenttypes_array[] = $illegal['fpunishmenttype'];//处罚种类及幅度
					
				}
				
				$newSamInfo['fillegaltypecode'] = 30;
				$newSamInfo['fexpressioncodes'] = implode(';',$fexpressioncodes_array);
				$newSamInfo['fexpressions'] = implode("\n",$fexpressions_array);
				$newSamInfo['fconfirmations'] = implode("\n",$fconfirmations_array);
				$newSamInfo['fpunishments'] = implode("\n",$fpunishments_array);
				$newSamInfo['fpunishmenttypes'] = implode("\n",$fpunishmenttypes_array);
				$newSamInfo['freviewstate'] = 100;
				
				M('t'.$media_tab.'sample')->where(array($fidField=>$sam['fid']))->save($newSamInfo);
			}
			
			
			
		}
		
		
	}
	
	public function task_input(){
		$illSamList = M('task_input')->field('taskid,fexpressioncodes')->where(array('fillegaltypecode'=>array('gt',0)))->select();
		foreach($illSamList as $illSam){
			$old_fexpressioncodes_arr = explode(';',$illSam['fexpressioncodes']);
			$new_fexpressioncodes_arr = array();
			
			foreach($old_fexpressioncodes_arr as $old_fexpressioncodes){
				
				$new_fexpressioncodes_arr[] = $this->o_n_c($old_fexpressioncodes);
			}
			
			$new_fexpressioncodes = implode(';',$new_fexpressioncodes_arr);
			M('task_input')->where(array('taskid'=>$illSam['taskid']))->save(array('fexpressioncodes'=>$new_fexpressioncodes,'fillegaltypecode'=>30));
		}
	}
	

}