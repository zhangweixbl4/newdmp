<?php
//电视样本录入控制器
namespace Admin\Controller;
use Think\Controller;
class BcinputController extends BaseController {

	public function index(){
		
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$keyword = I('keyword');//搜索关键词
		$task_state = I('task_state');//任务状态
		$receive_time_s = I('receive_time_s');//任务接收开始时间
		if($receive_time_s == '') $receive_time_s = '2015-01-01';
		$receive_time_e = I('receive_time_e');//任务接收结束时间
		if($receive_time_e == '') $receive_time_e = date('Y-m-d');
		$where = array();//查询条件
		$where['finputstate'] = array('gt',0);
		if($task_state == '0') $where['tbcsample.finputstate'] = 1;
		if($task_state == '1') $where['ad_input_task.task_state'] = 1;
		if($task_state == '2') $where['ad_input_task.task_state'] = 2;
		if($task_state == '3') $where['ad_input_task.task_state'] = 3;
		if($task_state == '4') $where['ad_input_task.task_state'] = 4;
		if($task_state == '5') $where['ad_input_task.task_state'] = 5;
		if($task_state == '6') $where['ad_input_task.task_state'] = 6;

		if($keyword != ''){
			$where['tbcsample.fversion|tad.fadname'] = array('like','%'.$keyword.'%');
		}
		if($receive_time_s != '' || $receive_time_e != ''){
			$where['ad_input_task.receive_time|tbcsample.fcreatetime'] = array('between',$receive_time_s.','.$receive_time_e.' 23:59:59');
		}

		$count = M('tbcsample')
								->join('tad on tad.fadid = tbcsample.fadid')
								->join('ad_input_task on ad_input_task.sample_id = tbcsample.fid and ad_input_task.mediaclass = "广播"','LEFT')
								->join('ad_input_user on ad_input_user.wx_id = ad_input_task.user_id','LEFT')
								->where($where)->count();// 查询满足要求的总记录数
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数

		
		$bcsampleList = M('tbcsample')
										->field('tbcsample.*,tad.fadname,ad_input_task.*,ad_input_user.nickname,ad_input_user.headimgurl,
												case 
													when tbcsample.finputstate = 1 then "未接收"
													when task_state = 1 then "已接收" 
													when task_state = 2 then "已录入" 
													when task_state = 3 then "审核通过" 
													when task_state = 4 then "审核未通过" 
													when task_state = 5 then "任务退回" 
													when task_state = 6 then "系统收回" 
													
												else "未知" end as task_state
												')
										->join('tad on tad.fadid = tbcsample.fadid')
										->join('ad_input_task on ad_input_task.sample_id = tbcsample.fid and ad_input_task.mediaclass = "广播"','LEFT')
										->join('ad_input_user on ad_input_user.wx_id = ad_input_task.user_id','LEFT')
										->where($where)
										->order('ad_input_task.task_id desc,tbcsample.fid desc')
										->limit($Page->firstRow.','.$Page->listRows)->select();//查询广告样本列表
		//var_dump( M('tbcsample')->getLastSql());							
		$bcsampleList = list_k($bcsampleList,$p,$pp);//为列表加上序号

		$this->assign('bcsampleList',$bcsampleList);//广告录入列表
		//var_dump($bcsampleList);
		$this->assign('page',$Page->show());//分页输出
		$this->display();
	}
	
	
		
	/*样本详情*/
	public function ajax_bcsample_details(){
		
		$fid = I('fid');//获取样本ID
		$bcsampleDetails= M('tbcsample')
										->field('tbcsample.*,tad.fadname,tad.fbrand,tadowner.fname as adowner_name,tillegaltype.fillegaltype,tadclass.ffullname as adclass_fullname')
										->join('tad on tad.fadid = tbcsample.fadid')
										->join('tadowner on tadowner.fid = tad.fadowner')
										->join('tillegaltype on tillegaltype.fcode = tbcsample.fillegaltypecode')
										->join('tadclass on tadclass.fcode = tad.fadclasscode')
										->where(array('tbcsample.fid'=>$fid))
										->find();//查询样本详情
		//var_dump($bcsampleDetails);
		$this->ajaxReturn(array('code'=>0,'bcsampleDetails'=>$bcsampleDetails));
	}
	
	/*任务审核*/
	public function task_audit(){
		$task_list = I('task_id');
		$task_state = I('task_state');
		//var_dump($task_list);
		foreach($task_list as $task_id){
			$taskInfo = M('ad_input_task')->where(array('task_id'=>$task_id))->find();//查询任务信息
			
			if($task_state == '3' && $taskInfo['task_state'] == 2){
				M('ad_input_task')->where(array('task_id'=>$task_id))->save(array('task_state'=>3));//任务改为已审核状态
				M('tbcsample')->where(array('fid'=>$taskInfo['sample_id']))->save(array('finputstate'=>4));//样本改为未录入状态
			}
			if($task_state == '6' && $taskInfo['task_state'] == 2){
				M('ad_input_task')->where(array('task_id'=>$task_id))->save(array('task_state'=>6));//任务改为系统退回状态
				M('tbcsample')->where(array('fid'=>$taskInfo['sample_id']))->save(array('finputstate'=>1));//样本改为未录入状态
				$money = 0 - $taskInfo['money'];//把金额变成负数
				A('Adinput/User','Model')->money($taskInfo['user_id'],$money,'录入广告退回,任务ID:'.$task_id);//修改余额
			}
			
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功'));
	}
	
	

}