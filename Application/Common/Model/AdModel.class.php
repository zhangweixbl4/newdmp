<?php
namespace Common\Model;
use Think\Exception;
class AdModel{
    public $notAllowedAdnames = ['社会公益'];
    /*获取广告ID*/
    // $type 来源类型 13-互联网 1-默认
    public function getAdId($adName,$brand,$adClassCode,$adOwnerName,$creator,$adClassCodeV2 = '',$type=1){
        //不允许的广告名称
        if(in_array($adName,$this->notAllowedAdnames)){
            return false;
        }
        if($adClassCode == '2202') $adClassCode='220210';//非商业类/公益广告 强制分到第三级 非商业类/公益广告/其它
        // 广告名称替换空格,被英文字符前后的空格不替换
        $adName = str_replace(array('  '),array(' '),$adName); #替换不合法字符
        $adName = strtoupper(trim($adName));
        $adName = preg_replace('/(\w)\s+(\w)/', '${1}|${2}', $adName);
        $adName = preg_replace('/\s+/', '', $adName);
        $adName = str_replace('|', ' ', $adName);
        if(strpos($adName,'公益') === 0){
            $adName = str_replace(')','）',str_replace('(','（',$adName));
        }
        $brand = strtoupper(trim($brand));//品牌名称
		lock('getAdId_'.$adName);//获取线程锁
		$adInfo = M('tad')->master(true)->where(array('fadname'=>$adName,'fstate'=>['GT',0]))->find();//通过广告名称查询广告信息
        if($type == 1 && $adInfo['fadid']){
            // 传统引用时，fpriority置为1,状态置为2待确认
            if($adInfo['fpriority'] == 13 || $adInfo['fstate'] == 9){
                $ret = M('tad')->where(['fadid'=>$adInfo['fadid']])->save(['fpriority'=>1,'fstate'=>2]);
            }
        }
		if ($adInfo['fstate'] == 1){//如果广告已确认,
			if($adInfo['fadowner'] == '0' && $adOwnerName && $adOwnerName != '广告主不详'){
				$adClassCode = $adInfo['fadclasscode'];//广告分类
                $adClassCodeV2 = $adInfo['fadclasscode_v2'];//商业分类
                $brand = $adInfo['fbrand'];//广告品牌
			}else{
				lock('getAdId_'.$adName,null);//释放线程锁
                return $adInfo['fadid'];//直接返回广告id
			}
        }
        $wx_id = session('wx_id');
		if($adInfo){
			$e_data = array();
            if(M('tadclass')->cache(true,3600)->where(array('fcode'=>$adClassCode))->count() == 0){
                $adClassCode = '2301';
            }
            if(M('hz_ad_class')->cache(true,3600)->where(array('fcode'=>$adClassCodeV2))->count() == 0){
                $adClassCodeV2 = '3203';
            }
            $e_data['fadclasscode'] = $adClassCode;//广告分类
            $e_data['fadclasscode_v2'] = $adClassCodeV2;//新广告分类
            $e_data['fbrand'] = $brand;//品牌
            $e_data['fmodifier'] = $creator;//修改人
			$e_data['fmodifytime'] = date('Y-m-d H:i:s');
            $e_data['fadowner'] = $this->getAdOwnerId($adOwnerName,$creator);//广告主
            if($type == 1){
                // 传统引用时，fpriority置为1,状态置为2待确认
                if($adInfo['fpriority'] == 13 || $adInfo['fstate'] == 9){
                    $ret = M('tad')->where(['fadid'=>$adInfo['fadid']])->save(['fpriority'=>1,'fstate'=>2]);
                    M('ad_record')->add([
                        'fadid'       => $adInfo['fadid'],
                        'wx_id'       => $wx_id,
                        'before'      => json_encode($adInfo),
                        'after'       => json_encode($e_data),
                        'update_time' => time(),
                        'user_name'   => $creator,
                        'type'        => 2,
                        'log'         => '引用修改',
                    ]);
                }
            }
			$res = M('tad')->where(array('fadid'=>$adInfo['fadid']))->save($e_data);
            if ($res){
                A('Common/AliyunLog','Model')->ad_log([
                    'type'   => 'save',
                    'msg'    => '广告信息被修改',
                    'fadid'  => $adInfo['fadid'],
                    'wx_id'  => $wx_id,
                    'before' => json_encode($adInfo),
                    'after'  => json_encode($e_data),
                ]);
            }
            $ad_id = $adInfo['fadid'];
        }else{//假如没有数据，就新增
			if(M('tadclass')->cache(true,3600)->where(array('fcode'=>$adClassCode))->count() == 0){
				$adClassCode = '2301';
            }
            if($type == 13){
                $fstate = 9;
            }else{
                $fstate = 2;
            }
            $addData = array(
                'fadname'			=> $adName,//广告名称
                'fadowner'			=> $this->getAdOwnerId($adOwnerName,$creator),//广告主ID
                'fbrand'			=> $brand,//品牌
                'fadclasscode'		=> $adClassCode,//广告类别代码
                'fadclasscode_v2'	=> $adClassCodeV2,// 新广告类别代码
                'fcreator' 			=> $creator,//创建人
                'fcreatetime' 		=> date('Y-m-d H:i:s'),//创建时间
                'fmodifier' 	    => $creator,//修改人
                'fmodifytime' 		=> date('Y-m-d H:i:s'),//修改时间
                'fstate'			=> $fstate,
                'fpriority'         => $type,
            );
			$ad_id = M('tad')->add($addData);//添加广告
            M('ad_record')->add([
                'fadid'       => $ad_id,
                'wx_id'       => $wx_id,
                'after'       => json_encode($addData),
                'update_time' => time(),
                'user_name'   => $creator,
                'type'        => 1,
                'log'         => '创建主题',
            ]);
            if ($ad_id){
                A('Common/AliyunLog','Model')->ad_log([
                    'type' => 'add',
                    'msg' => '新增广告',
                    'fadid' => $ad_id,
                    'wx_id' => $wx_id,
                    'add_data' => $addData,
                ]);
            }
		}
		lock('getAdId_'.$adName,null);//释放线程锁
		return $ad_id;
    }
	
	/*获取广告主ID*/
	public function getAdOwnerId($adOwnerName,$creator){
        $adOwnerName = strtoupper(trim($adOwnerName));
	    if (!$adOwnerName){
	        return '0';
        }
		lock('getAdOwnerId_'.$adOwnerName);//获取线程锁
		$adownerInfo = M('tadowner')->master(true)->where(array('fname'=>$adOwnerName))->find();//查询广告主信息
		if($adownerInfo){//如果有广告主信息
			$fadowner = $adownerInfo['fid'];//赋值广告主ID
		}else{//如果没有广告主ID
            $fadowner = M('tadowner')
                ->add(array(
                    'fname'			=> $adOwnerName,
                    'fregionid'		=> 100000,
                    'fcreator' 		=> $creator,//创建人
                    'fcreatetime' 	=> date('Y-m-d H:i:s'),//创建时间
                    'fmodifier' 	=> $creator,//修改人
                    'fmodifytime' 	=> date('Y-m-d H:i:s'),//修改时间
                    'fstate'		=> 1,
                ));//添加广告主
		}
		lock('getAdOwnerId_'.$adOwnerName,null);//释放线程锁
		return $fadowner;
	}
}