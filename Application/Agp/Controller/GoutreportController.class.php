<?php
namespace Agp\Controller;
use Think\Controller;

import('Vendor.PHPExcel');

class GoutreportController extends BaseController{

    /**
     * 获取报告列表
     * by zw
     */
    public function index() {
        session_write_close();
        header("Content-type:text/html;charset=utf-8");

        $p                  = I('page', 1);//当前第几页
        $pp                 = 20;//每页显示多少记录
        $pnname             = I('pnname');// 报告名称
        $pntype             = I('pntype');// 报告类型
        $pnfiletype         = I('pnfiletype');// 文件类型
        $pncreatetime       = I('pnstarttime');//报告时间
        $system_num = getconfig('system_num');

        if (!empty($pnname)) {
            $where['pnname'] = array('like', '%' . $pnname . '%');//报告名称
        }
        if (!empty($pntype)) {
            $where['pntype'] = $pntype;//报告类型
        }else{
            $where['pntype'] = array('in',array(50));//报告类型
        }
        if (!empty($pnfiletype)) {
            $where['pnfiletype'] = $pnfiletype;//文件类型
        }
        if(!empty($pncreatetime)){
            $where['pnstarttime'] = array('between',array($pncreatetime[0],$pncreatetime[1]));
        }   
        $where['pntrepid'] = session('regulatorpersonInfo.fregulatorpid');
        $where['fcustomer']  = $system_num;

        $count = M('tpresentation')
            ->where($where)
            ->count();

        
        $data = M('tpresentation')
            ->where($where)
            ->order('pnendtime desc')
            ->page($p,$pp)
            ->select();
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
    }

	public function gzhuanxiang(){
		$fadnames = I('fadnames');//需要查询的广告
		$mclass = strlen(I('mclass'))?(int)I('mclass'):0;//媒体类型
        $issuest = I('issuest')?I('issuest'):date('Y-m-d');//发布开始时间
        $issueed = I('issueed')?I('issueed'):date('Y-m-d');//发布结束时间
		$area  = I('area')?I('area'):0;//所属地域
		$iscontain  = I('iscontain')?I('iscontain'):0;//是否包含下属地区
		$fillegaltypecode = strlen(I('fillegaltypecode'))?(int)I('fillegaltypecode'):-1;//违法程度，-1全部，0不违法，>0违法
        $focus = I('focus',0);
        $this->gzhuanxiang2($fadnames,$mclass,$issuest,$issueed,$area,$iscontain,$fillegaltypecode,$focus,session('regulatorpersonInfo.fid'));
	}

	/**
	 * 导出专项报告，按广告名、时间范围（全表查询）
	 * by zw
	 */
	public function gzhuanxiang2($fadnames,$mclass,$issuest,$issueed,$area,$iscontain,$fillegaltypecode,$focus,$userid){
		session_write_close();
		header("Content-type: text/html; charset=utf-8");
        ini_set('memory_limit','1024M');
        ini_set('max_execution_time','86400');
        set_time_limit(0);

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

		$system_num = getconfig('system_num');//客户编号
        
        $regulatorpersonInfo = $this->getuser_view($userid);//获取用户信息

        $where1 = '1=1';
        $where2 = '1=1';
        if(!empty($fadnames)){
        	$where1 .= " and fadname in('".implode("','",$fadnames)."')";
        }
        if(!empty($area)){
        	if(!empty($iscontain)){
                $tregion_len = get_tregionlevel($area);
				if($tregion_len == 1){//国家级
					$where2 .= ' and fregionid ='.$area;	
				}elseif($tregion_len == 2){//省级
					$where2 .= ' and fregionid like "'.substr($area,0,2).'%"';
				}elseif($tregion_len == 4){//市级
					$where2 .= ' and fregionid like "'.substr($area,0,4).'%"';
				}elseif($tregion_len == 6){//县级
					$where2 .= ' and fregionid like "'.substr($area,0,6).'%"';
				}
        	}else{
        		$where2 .= ' and fregionid = '.$area;
        	}
        }
        if($fillegaltypecode == 0){
        	$where1 .= ' and b.fillegaltypecode = 0';
        }elseif($fillegaltypecode > 0){
        	$where1 .= ' and b.fillegaltypecode > 0';
        }

        $alldata = [];

        if($mclass == 0 || $mclass == 1){
            $issuest_bak = $issuest;
            $issuesql = '';
            while($issuest_bak<=$issueed && !empty($issuest_bak)){
                if(!empty($issuesql)){
                    $issuesql .= ' union all (select fsampleid,fissuedate,fregionid from '.gettable('tv',strtotime($issuest_bak)).')';
                }else{
                    $issuesql = '(select fsampleid,fissuedate,fregionid from '.gettable('tv',strtotime($issuest_bak)).')';
                }
                if(date('Y-m',strtotime($issueed))>date('Y-m',strtotime($issuest_bak))){
                    $issuest_bak = date('Y-m-d',strtotime(date('Y-m-01',strtotime($issuest_bak)).'+1 month'));
                }else{
                    $issuest_bak = '';
                }
            }

            $datasql = 'SELECT "电视" as mclass,from_unixtime(a.starttime) as starttime,from_unixtime(a.endtime) as endtime,a.issuecount,
                b.fadlen,b.fillegaltypecode,b.favifilename,b.fillegalcontent,b.fexpressioncodes,b.fexpressions,b.fconfirmations,
                c.fadname,
                d.fname as fadowner,
                "" as net_advertiser,
                e.fmedianame,
                f.ffullname as tregionname,
                h.ffullname as fadclassname
                FROM (select fsampleid,count(*) as issuecount,min(fissuedate) as starttime, max(fissuedate) as endtime from ('.$issuesql.') x where '.$where2.' and fissuedate BETWEEN '.strtotime($issuest).' AND '.strtotime($issueed).' group by fsampleid) a
                inner join ttvsample b on a.fsampleid=b.fid and b.fstate=1
                inner join tad c on b.fadid=c.fadid and c.fadid<>0
                inner join tadowner d on c.fadowner=d.fid and d.fstate=1
                inner join tmedia e on e.fid=b.fmediaid and e.fid=e.main_media_id and e.fstate=1
                inner join tregion f on f.fid=e.media_region_id and f.fstate=1
                inner join tadclass h on h.fcode=c.fadclasscode and h.fstate=1
                inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.$userid.' ) as ttp on e.fid=ttp.fmediaid
                where '.$where1.'
                order by f.fid asc';
            $do_ie = M()->query($datasql);
            if(!empty($do_ie)){
                $alldata = array_merge($alldata,$do_ie);
            }
        }
        if($mclass == 0 || $mclass == 2){
            $issuest_bak = $issuest;
            $issuesql = '';
            while($issuest_bak<=$issueed && !empty($issuest_bak)){
                if(!empty($issuesql)){
                    $issuesql .= ' union all (select fsampleid,fissuedate,fregionid from '.gettable('bc',strtotime($issuest_bak)).')';
                }else{
                    $issuesql = '(select fsampleid,fissuedate,fregionid from '.gettable('bc',strtotime($issuest_bak)).')';
                }
                if(date('Y-m',strtotime($issueed))>date('Y-m',strtotime($issuest_bak))){
                    $issuest_bak = date('Y-m-d',strtotime(date('Y-m-01',strtotime($issuest_bak)).'+1 month'));
                }else{
                    $issuest_bak = '';
                }
            }

        	$datasql = 'SELECT "广播" as mclass,from_unixtime(a.starttime) as starttime,from_unixtime(a.endtime) as endtime,a.issuecount,
        		b.fadlen,b.fillegaltypecode,b.favifilename,b.fillegalcontent,b.fexpressioncodes,b.fexpressions,b.fconfirmations,
        		c.fadname,
        		d.fname as fadowner,
        		"" as net_advertiser,
        		e.fmedianame,
        		f.ffullname as tregionname,
        		h.ffullname as fadclassname
				FROM (select fsampleid,count(*) as issuecount,min(fissuedate) as starttime, max(fissuedate) as endtime from ('.$issuesql.') x where '.$where2.' and fissuedate BETWEEN '.strtotime($issuest).' AND '.strtotime($issueed).' group by fsampleid) a
				inner join tbcsample b on a.fsampleid=b.fid and b.fstate=1
				inner join tad c on b.fadid=c.fadid and c.fadid<>0
				inner join tadowner d on c.fadowner=d.fid and d.fstate=1
				inner join tmedia e on e.fid=b.fmediaid and e.fid=e.main_media_id and e.fstate=1
				inner join tregion f on f.fid=e.media_region_id and f.fstate=1
				inner join tadclass h on h.fcode=c.fadclasscode and h.fstate=1
				inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.$userid.' ) as ttp on e.fid=ttp.fmediaid
				where '.$where1.'
				order by f.fid asc';
			$do_ie = M()->query($datasql);
            if(!empty($do_ie)){
                $alldata = array_merge($alldata,$do_ie);
            }
        }
        if($mclass == 0 || $mclass == 3){
        	$datasql = 'SELECT "报纸" as mclass,a.starttime,a.endtime,a.issuecount,
        		a.fpage as fadlen,b.fillegaltypecode,b.fjpgfilename as favifilename,b.fillegalcontent,b.fexpressioncodes,b.fexpressions,b.fconfirmations,
        		c.fadname,
        		d.fname as fadowner,
        		"" as net_advertiser,
        		e.fmedianame,
        		f.ffullname as tregionname,
        		h.ffullname as fadclassname
				FROM (select fpapersampleid,count(*) as issuecount,min(fissuedate) as starttime, max(fissuedate) as endtime,fpage from tpaperissue where '.$where2.' and fissuedate BETWEEN "'.$issuest.'" AND "'.$issueed.'" AND fstate=1 group by fpapersampleid) a
				inner join tpapersample b on a.fpapersampleid=b.fpapersampleid and b.fstate=1
				inner join tad c on b.fadid=c.fadid and c.fadid<>0
				inner join tadowner d on c.fadowner=d.fid and d.fstate=1
				inner join tmedia e on e.fid=b.fmediaid and e.fid=e.main_media_id and e.fstate=1
				inner join tregion f on f.fid=e.media_region_id and f.fstate=1
				inner join tadclass h on h.fcode=c.fadclasscode and h.fstate=1
				inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.$userid.' ) as ttp on e.fid=ttp.fmediaid
				where '.$where1.'
				order by f.fid asc';
			$do_ie = M()->query($datasql);
			if(!empty($do_ie)){
				$alldata = array_merge($alldata,$do_ie);
			}
        }
        if($mclass == 0 || $mclass == 13){
        	$datasql = 'SELECT 
        		"互联网" as mclass,a.starttime,a.endtime,a.issuecount, CONCAT(b.net_x,",",b.net_y) as fadlen,b.fillegaltypecode,a.net_old_url as favifilename,b.fillegalcontent,b.fexpressioncodes,b.fexpressions,b.fconfirmations,
        		b.fadname,
        		d.fname as fadowner,
        		b.net_advertiser,
        		e.fmedianame,
        		f.ffullname as tregionname,
        		h.ffullname as fadclassname
				FROM (select tid,count(*) as issuecount,min(date(FROM_UNIXTIME(net_created_date/1000))) as starttime, max(date(FROM_UNIXTIME(net_created_date/1000))) as endtime,net_old_url from tnetissueputlog where net_created_date between '.(strtotime($issuest)*1000).' and '.(strtotime($issueed)*1000).' group by tid) a
				inner join tnetissue b on a.tid=b.major_key  and finputstate=2 
				inner join tadowner d on b.fadowner=d.fid and d.fstate=1
				inner join tmedia e on e.fid=b.fmediaid and e.fid=e.main_media_id and e.fstate=1
				inner join tregion f on f.fid=e.media_region_id and f.fstate=1
				inner join tadclass h on h.fcode=b.fadclasscode and h.fstate=1
				inner join (select distinct(fmediaid) from tmedia_temp where tmedia_temp.fcustomer = "'.$system_num.'" and tmedia_temp.fuserid='.$userid.' ) as ttp on e.fid=ttp.fmediaid
				where '.$where1.'
				order by f.fid asc';
			$do_ie = M()->query($datasql);
			if(!empty($do_ie)){
				$alldata = array_merge($alldata,$do_ie);
			}
        }
        $countdata = count($alldata);
        if($countdata>30000){
            $this->ajaxReturn(array('code'=>-1,'msg'=>'数据量太多，无法生成，请缩小生成范围'));
        }

		$sheet = $objPHPExcel->setActiveSheetIndex(0);
        $sheet ->setCellValue('A1','序号');
        $sheet ->setCellValue('B1','所属地域');
        $sheet ->setCellValue('C1','媒体名称');
        $sheet ->setCellValue('D1','媒体类别');
        $sheet ->setCellValue('E1','广告名称');
        $sheet ->setCellValue('F1','广告类别名称');
        $sheet ->setCellValue('G1','广告主/域名');
        $sheet ->setCellValue('H1','发布开始日期');
        $sheet ->setCellValue('I1','发布截止日期');
        $sheet ->setCellValue('J1','总发布次数');
        $sheet ->setCellValue('K1','广告时长/版面');
        $sheet ->setCellValue('L1','违法类型');
        $sheet ->setCellValue('M1','违法内容');
        $sheet ->setCellValue('N1','违法表现代码');
        $sheet ->setCellValue('O1','违法表现');
        $sheet ->setCellValue('P1','认定依据');
        $sheet ->setCellValue('Q1','素材地址');
        foreach ($alldata as $key => $value) {
            $sheet ->setCellValue('A'.($key+2),$key+1);//序号
            $sheet ->setCellValue('B'.($key+2),$value['tregionname']);
            $sheet ->setCellValue('C'.($key+2),$value['fmedianame']);
            $sheet ->setCellValue('D'.($key+2),$value['mclass']);
            $sheet ->setCellValue('E'.($key+2),$value['fadname']);
            $sheet ->setCellValue('F'.($key+2),$value['fadclassname']);
            if(!empty($value['net_advertiser'])){
                $sheet ->setCellValue('G'.($key+2),$value['net_advertiser']);
            }else{
                $sheet ->setCellValue('G'.($key+2),$value['fadowner']);
            }
            $sheet ->setCellValue('H'.($key+2),date('Y-m-d',strtotime($value['starttime'])));
            $sheet ->setCellValue('I'.($key+2),date('Y-m-d',strtotime($value['endtime'])));
            $sheet ->setCellValue('J'.($key+2),$value['issuecount']);
            $sheet ->setCellValue('K'.($key+2),$value['fadlen']);
            if($value['fillegaltypecode']>0){
                $sheet ->setCellValue('L'.($key+2),'违法');
            }else{
                $sheet ->setCellValue('L'.($key+2),'不违法');
            }
            $sheet ->setCellValue('M'.($key+2),$value['fillegalcontent']);
            $sheet ->setCellValue('N'.($key+2),$value['fexpressioncodes']);
            $sheet ->setCellValue('O'.($key+2),$value['fexpressions']);
            $sheet ->setCellValue('P'.($key+2),$value['fconfirmations']);
            $sheet ->setCellValue('Q'.($key+2),$value['favifilename']);
        }
        $objActSheet =$objPHPExcel->getActiveSheet();

        //给当前活动的表设置名称
        $objActSheet->setTitle('Sheet1');
        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $date = date('Ymdhis');
        $savefile = './Public/Xls/'.$date.'.xlsx';
        $objWriter->save($savefile);
        if(!empty($area)){
            $pnname = $regulatorpersonInfo['regulatorpname'].'专项报告'.$date.'.xlsx';
        }else{
            $pnname = '全国专项报告'.$date.'.xlsx';
        }
        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='.$pnname));//上传云
        unlink($savefile);//删除文件

        //将生成记录保存
        $data['pnname'] 			= $pnname;
        $data['pntype'] 			= 50;
        $data['pnfiletype'] 		= 30;
        $data['pnstarttime'] 		= $issuest;
        $data['pnendtime'] 		    = $issueed;
        $data['pncreatetime'] 		= date('Y-m-d H:i:s');
        $data['pnurl'] 				= $ret['url'];
        $data['pnhtml'] 			= '';
        $data['pnfocus']            = $focus;
        $data['pntrepid']           = $regulatorpersonInfo['fregulatorpid'];
        $data['pncreatepersonid']   = $regulatorpersonInfo['fid'];
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'生成失败'));
        }

	}

}