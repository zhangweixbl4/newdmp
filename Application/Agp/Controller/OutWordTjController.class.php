<?php
namespace Agp\Controller;
use Think\Controller;
use Think\Exception;
use Agp\Model\StatisticalReportModel;
use Agp\Model\StatisticalModel;
use OpenSearch\Client\OpenSearchClient;
use OpenSearch\Client\DocumentClient;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;
import('Vendor.PHPExcel');

class OutWordTjController extends BaseController{

    public function get_owner_media_ids($level = -1,$media_type = '',$out_media_type = ''){
        $owner_media = get_owner_media_ids(-1,'',$out_media_type);//获取权限内媒体
/*        $owner_media_ids = M('tmedia')->where([
            'fid' => ['IN',$owner_media],
            'media_region_id' => ['NEQ',session('regulatorpersonInfo.regionid')]
        ])->getField('fid',true);*/
        //return $owner_media_ids;
        return $owner_media;
    }

    //天津报表列表
    public function index() {
        $p 					= I('page', 1);//当前第几页
        $pp 				= 20;//每页显示多少记录
        $pnname 			= I('pnname');// 报告名称
        $pntype 			= I('pntype');// 报告类型
        $pnfiletype 		= I('pnfiletype');// 文件类型
        $pncreatetime 		= I('pnendtime');//生成时间
        $system_num = getconfig('system_num');

        if (!empty($pnname)) {
            $where['pnname'] = array('like', '%' . $pnname . '%');//报告名称
        }
        if (!empty($pntype)) {
            $where['pntype'] = $pntype;//报告类型
        }else{
            $where['pntype'] = array('in',array(10,20,30,40,50,60,61,70,75,80));
        }
        if (!empty($pnfiletype)) {
            $where['pnfiletype'] = $pnfiletype;//文件类型
        }
        if(!empty($pncreatetime)){
            $where['_string'] = ' pnendtime between "'.$pncreatetime[0].'" and "'.$pncreatetime[1].'"';
        }

        $where['pntrepid'] = session('regulatorpersonInfo.fregulatorpid');
        $where['fcustomer']  = $system_num;

        $count = M('tpresentation')
            ->where($where)
            ->count();


        $data = M('tpresentation')
            ->where($where)
            ->order('pncreatetime desc')
            ->page($p,$pp)
            ->select();
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
    }

    //报告删除接口
    public function tpresentation_delete() {

        $pnid         = I('pnid');//报告ID
        $user = session('regulatorpersonInfo');//获取用户信息
        $system_num = getconfig('system_num');

        $where['pnid'] = $pnid;
        $where['pntrepid'] = $user['fregulatorpid'];
        $where['fcustomer']  = $system_num;

        $data = M('tpresentation')->where($where)->delete();

        if($data > 0){
            $this->ajaxReturn(array('code'=>0,'msg'=>'删除成功！'));
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'您无权删除此报告！'));
        }
    }

    public function upload_report(){
        $system_num = getconfig('system_num');
        $system_num = getconfig('system_num');
        $user = session('regulatorpersonInfo');//获取用户信息
        $ffiles = I('ffiles');
        $pnendtime = I('pnendtime');
        $pnfiletype = I('pnfiletype');
        $pnname = I('pnname');
        $pntype = I('pntype');
        $date_now = date('Y-m-d H:i:s');
        $data['pnname'] 			= $pnname;
        $data['pntype'] 			= $pntype;
        $data['pnfiletype'] 		= $pnfiletype;
        $data['pnstarttime'] 		= $date_now;
        $data['pnendtime'] 		    = $pnendtime;
        $data['pncreatetime'] 		= date('Y-m-d H:i:s');
        $data['pnurl'] 				= $ffiles[0]['url'];
        $data['pnhtml'] 			= '';
        $data['pnfocus']            = 0;
        $data['pntrepid']           = $user['fregulatorpid'];
        $data['pncreatepersonid']   = $user['fid'];
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'上传成功'));
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'上传失败'));
        }
    }

    //互联网报告接口
    public function create_netad(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport1 = New StatisticalReportModel();//实例化数据统计模型
        $StatisticalReport = New StatisticalModel();//实例化数据统计模型

        $report_start_date = strtotime(I('s_time',date('Y-m-d')));//开始时间
        $report_end_date = strtotime(I('e_time',date('Y-m-d')))+86399;//结束时间

        $user = session('regulatorpersonInfo');//获取用户信息
        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);//开始年月字符
        $date_ym = date('Y年m月',$report_end_date);//开始年月字符

        //查找全部媒体，筒体全部互联网媒体数据
        $net_media_ids = $StatisticalReport->net_media_type_ids('13');
        $net_media_customize = $StatisticalReport->report_ad_monitor('fmedia_class_code',$net_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','13');//当前客户全部互联网广告数据统计
        //如果没有互联网数据则返回
        if(empty($net_media_customize)){
            $this->ajaxReturn([
                'code'=>1,
                'msg'=>'暂无数据'
            ]);
        }
        /*
        * @互联网各媒体监测总情况ggslyzwfl
        *
        **/
        $every_net_media_data = [];
        $every_net_media_name = [];
        //$every_net_media_jczqk1 = $StatisticalReport1->net_media_customize($user['regionid'],false,'tn.fmediaid',$report_start_date,$report_end_date);//互联网各媒体各媒体监测总情况
        $every_net_media_jczqk = $StatisticalReport->report_ad_monitor('fmediaid',$net_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','13');//pc媒体发布情况

        foreach ($every_net_media_jczqk as $every_net_media_jczqk_key=>$every_net_media_jczqk_val){
            if(!$every_net_media_jczqk_val['titlename']) {
                if ($every_net_media_jczqk_val['fad_illegal_times'] != 0) {
                    $every_net_media_name[] = $every_net_media_jczqk_val['tmedia_name'];
                }
                $every_net_media_data[] = [
                    strval($every_net_media_jczqk_key + 1),
                    $every_net_media_jczqk_val['tmedia_name'],
                    $every_net_media_jczqk_val['fad_times'],
                    $every_net_media_jczqk_val['fad_illegal_times'],
                    $every_net_media_jczqk_val['times_illegal_rate'] . '%'
                ];
            }
        }
        $every_net_media_data = to_string($every_net_media_data);

        /*
        * @互联网各广告类别各媒体监测总情况
        *
        **/
        $every_ad_type_data = [];
        $every_ad_type_name = [];
        //$every_ad_type_jczqk1 = $StatisticalReport1->net_media_customize($user['regionid'],false,'ac.fcode',$report_start_date,$report_end_date);//互联网各广告类别各媒体监测总情况
        $every_ad_type_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$net_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','13');//pc媒体广告发布情况

        foreach ($every_ad_type_jczqk as $every_ad_type_jczqk_key=>$every_ad_type_jczqk_val){
            if(!$every_ad_type_jczqk_val['titlename']) {
                if ($every_ad_type_jczqk_val['fad_illegal_times'] != 0) {
                    $every_ad_type_name[] = $every_ad_type_jczqk_val['tadclass_name'];
                }
                $every_ad_type_data[] = [
                    strval($every_ad_type_jczqk_key + 1),
                    $every_ad_type_jczqk_val['tadclass_name'],
                    $every_ad_type_jczqk_val['fad_times'],
                    $every_ad_type_jczqk_val['fad_illegal_times'],
                    $every_ad_type_jczqk_val['times_illegal_rate'] . '%'
                ];
            }
        }
        $every_ad_type_data = to_string($every_ad_type_data);



        //定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];
        $data['content'][] = create_report_array('text','大标题','text',$user['regulatorpname']);
        $data['content'][] = create_report_array('text','黑体二号','text',$date_ym);


        if($net_media_customize[1]['fad_illegal_times'] == 0){
            $wf_des = "本次监测中，没有发现违法广告";
        }else{
            $wf_des = "本次监测中，共发现涉嫌违法广告".$net_media_customize[1]['fad_illegal_times']."条，共监测发现".$net_media_customize[1]['fad_times']."条；其中,";
        }

        $net_media_type_array = $StatisticalReport->get_media_type(13);//互联网媒体各大类

        $every_net_type_data[] = ['类别','总条','违法条','条违法率'];//
        $every_net_ill_data[] = ['','涉嫌违法广告条'];
        $media_des = '';
        $media_issue_data = [];
        $adclass_issue_data = [];
        $ill_media_data = [];
        $ill_detail_data = [];
        foreach ($net_media_type_array as $net_media_type_key=>$net_media_type_val){
            $media_count = $StatisticalReport->net_media_counts(substr($user['regionid'],0,2),$net_media_type_key);//媒体数量
            $net_media_count_str.= $media_count."家".$net_media_type_val."；";
            if($media_count >= 1){
                $medias = $StatisticalReport->net_media_type_ids($net_media_type_key);
                $media_customize = $StatisticalReport->report_ad_monitor('fmedia_class_code',$medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','13');//当前广告数据
                if(count($media_customize) != 1){
                    if($media_customize[1]['fad_illegal_times'] != 0){
                        $wf_des .= $net_media_type_val."发现涉嫌违法广告".$net_pc_media_customize[1]['fad_illegal_times']."条，共监测发现".$net_pc_media_customize[1]['fad_times']."条；";
                    }else{
                        $wf_des .= $net_media_type_val."未发现涉嫌违法广告；";
                    }
                    $every_net_ill_data[] =  [$net_media_type_val,$media_customize[1]['fad_illegal_times']?strval($media_customize[1]['fad_illegal_times']):'0'];
                    $every_net_type_data[] =  [
                        $net_media_type_val,
                        $media_customize[1]['fad_times']?strval($media_customize[1]['fad_times']):'0',
                        $media_customize[1]['fad_illegal_times']?strval($media_customize[1]['fad_illegal_times']):'0',
                        ($media_customize[1]['times_illegal_rate']?$media_customize[1]['times_illegal_rate']:'0.00').'%'
                    ];


                    $every_media_data = [];
                    $every_media_name = [];
                    $every_media_id = [];
                    $every_media_jczqk = $StatisticalReport->report_ad_monitor('fmediaid',$medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','13');//pc媒体发布情况

                    foreach ($every_media_jczqk as $every_media_jczqk_key=>$every_media_jczqk_val){
                        if(!$every_media_jczqk_val['titlename']){
                            $tmedia_name = $every_media_jczqk_val['tmedia_name'];
                            if($every_media_jczqk_val['fad_illegal_times'] != 0){
                                $every_media_name[] = $every_media_jczqk_val['tmedia_name'];
                            }
                            $every_media_id[$every_media_jczqk_val['fmediaid']] = $every_media_jczqk_val['tmedia_name'];
                        }else{
                            $tmedia_name = '合计';
                        }
                        $every_media_data[] = [
                            strval($every_media_jczqk_key+1),
                            $tmedia_name,
                            strval($every_media_jczqk_val['fad_times']),
                            strval($every_media_jczqk_val['fad_illegal_times']),
                            $every_media_jczqk_val['times_illegal_rate'].'%'
                        ];
                    }

                    $every_adclass_data = [];
                    $every_adclass_name = [];
                    $every_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','13');//pc媒体广告发布情况
                    foreach ($every_adclass_jczqk as $every_adclass_jczqk_key=>$every_adclass_jczqk_val){
                        if(!$every_adclass_jczqk_val['titlename']){
                            $tadclass_name = $every_adclass_jczqk_val['tadclass_name'];
                            if($every_adclass_jczqk_val['fad_illegal_times'] != 0){
                                $every_adclass_name[] = $every_adclass_jczqk_val['tadclass_name'];
                            }
                        }else{
                            $pc_tadclass_name = '合计';
                        }
                        $every_adclass_data[] = [
                            strval($every_adclass_jczqk_key+1),
                            $tadclass_name,
                            strval($every_adclass_jczqk_val['fad_times']),
                            strval($every_adclass_jczqk_val['fad_illegal_times']),
                            $every_adclass_jczqk_val['times_illegal_rate'].'%'
                        ];
                    }

                    //发布情况标题
                    $media_issue_data[] = create_report_array('text','宋体三号标题','text',$net_media_type_val."发布情况");

                    //判断是否有违法数据
                    $media_des .= $media_count."个".$net_media_type_val."，";//媒体数字符串

                    if($media_customize[1]['fad_illegal_times'] != 0){
                        $media_des .= '其中涉嫌违法广告'.$media_customize[1]['fad_illegal_times'].'条，违法率'.$media_customize[1]['times_illegal_rate'].'%。';
                    }else{
                        $media_des .= '未发现涉嫌违法广告。';
                    }
                    //发布情况描述
                    $media_issue_data[] = create_report_array('text','仿宋三号段落','text',$net_media_type_val."全部类别广告".$media_customize[1]['fad_times']."条，".$media_des);

                    $every_media_list = $this->create_net_chart_data($every_media_data);
                    if(!empty($every_media_list[1])){
                        $media_issue_data[] = create_report_array('chart','广告量条形图','data',$every_media_list[1]);
                    }
                    if(!empty($every_media_list[0])){
                        $media_issue_data[] = create_report_array('table','网站监测情况列表','data',$every_media_list[0]);
                    }


                    $adclass_issue_data[] = [
                        "type" => "text",
                        "bookmark" => "宋体三号标题",
                        "text" => $net_media_type_val."类别广告发布情况"
                    ];
                    if(empty($every_adclass_name)){
                        $every_adclass_name = '本次监测中'.$net_media_type_val.'各广告类别暂未发现涉嫌违法广告';
                        $adclass_issue_data[] = [
                            "type" => "text",
                            "bookmark" => "仿宋三号段落",
                            "text" => $every_adclass_name
                        ];
                    }else{
                        $every_adclass_name = "本次监测中".$net_media_type_val."涉嫌违法广告类别主要集中在".implode('、',$every_adclass_name)."。";
                        $adclass_issue_data[] = [
                            "type" => "text",
                            "bookmark" => "仿宋三号段落",
                            "text" => $every_adclass_name
                        ];
                    }
                    $every_adclass_list = $this->create_net_chart_data($every_adclass_data);//
                    if(!empty($every_adclass_name[1])){
                        $adclass_issue_data[] = [
                            "type" => "chart",
                            "bookmark" => "广告量条形图",
                            "data" => $every_adclass_list[1]
                        ];
                    }
                    if(!empty($every_adclass_name[0])){
                        $adclass_issue_data[] = [
                            "type" => "table",
                            "bookmark" => "广告类别监测情况列表",
                            "data" => $every_adclass_list[0]
                        ];
                    }

                    if(!empty($every_media_id)) {

                        $ill_media_data[] = [
                            "type" => "text",
                            "bookmark" => "宋体三号标题",
                            "text" => $net_media_type_val
                        ];
                        foreach ($every_media_id as $every_media_id_key => $every_media_id_val) {

                            $every_media = $StatisticalReport->report_ad_monitor('fmediaid',[$every_media_id_key],$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','13');//pc媒体广告发布情况
                            $every_media = to_string($every_media);

                            $every_adclass = $StatisticalReport->report_ad_monitor('fad_class_code',[$every_media_id_key],$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','13');//pc媒体广告发布情况
                            $every_adclass = to_string($every_adclass);
                            $illegal_situation = $StatisticalReport->net_illegal_situation_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'13',$every_media_id_key);//pc媒体广告发布情况

                            $class_name = [];
                            $fbxs_type = [];
                            $every_adclass_wfdata = [];
                            foreach ($every_adclass as $every_adclass_key => $every_adclass_val) {

                                $class_name[] = $every_adclass_val['tadclass_name'];
                                $every_adclass_wfdata[] = [
                                    strval($every_adclass_key + 1),
                                    $every_adclass_val['tadclass_name']?$every_adclass_val['tadclass_name']:$every_adclass_val['titlename'],
                                    $every_adclass_val['fad_times'],
                                    $every_adclass_val['fad_illegal_times'],
                                    $every_adclass_val['times_illegal_rate'] . '%'
                                ];

                            }
                            $illegal_situation_data = [];
                            foreach ($illegal_situation as $illegal_situation_key => $illegal_situation_val) {

                                $illegal_situation_data[] = [
                                    strval($illegal_situation_key + 1),
                                    $illegal_situation_val['fadclass'],
                                    $illegal_situation_val['fillegalcontent']
                                ];

                            }

                            $class_name = implode('、', $class_name);

                            //TODO::0717
                            $ill_media_data[] = [
                                "type" => "text",
                                "bookmark" => "宋体三号数字序号标题",
                                "text" => $every_media_id_val
                            ];

                            $ill_media_data[] = [
                                "type" => "text",
                                "bookmark" => "仿宋三号段落",
                                "text" => "本次监测【" . $every_media_id_val . "】涉嫌违法发布广告" . $every_media[1]['fad_illegal_times'] . "条，监测发现" . $every_media[1]['fad_times'] . "条，主要发布类别" . $class_name . "。"
                            ];

                            $ill_media_data[] = [
                                "type" => "table",
                                "bookmark" => "单媒体详情列表无广告形式",
                                "data" => $every_adclass_wfdata
                            ];
                            if(!empty($illegal_situation_data)) {
                                $ill_media_data[] = [
                                    "type" => "text",
                                    "bookmark" => "仿宋三号居中",
                                    "text" => "该".$net_media_type_val."广告主要违法情形"
                                ];
                                $ill_media_data[] = [
                                    "type" => "table",
                                    "bookmark" => "单媒体违法情形列表",
                                    "data" => $illegal_situation_data
                                ];
                            }
                        }
                    }


                    $illegal_detail_data = $StatisticalReport->report_illegal_ad_list($medias,date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),$user['regionid']);
                    $illegal_detail_data = to_string($illegal_detail_data);
                    if(!empty($illegal_detail_data)){
                        $ill_detail_data[] = [
                            "type" => "text",
                            "bookmark" => "宋体三号标题",
                            "text" => "$net_media_type_val"
                        ];
                        foreach ($illegal_detail_data as $illegal_detail_data_key => $illegal_detail_data_val){
                            $illegal_detail = $StatisticalReport->report_illegal_detail($illegal_detail_data_val['fsample_id']);
                            $img = getimages(htmlspecialchars_decode($illegal_detail['fillegalcontent']));
                            if($img){
                                if(is_img_link($img)){
                                    $fillegalcontent = ['index'=>12,'type'=>'image','content'=>$img];
                                }else{
                                    $fillegalcontent = ['index'=>12,'type'=>'text','content'=>''];
                                }
                            }else{
                                $fillegalcontent = ['index'=>12,'type'=>'text','content'=>str_replace("&nbsp;","",strip_tags(htmlspecialchars_decode($illegal_detail['fillegalcontent'])))];
                            }

                            $illegal_detail_list = [
                                ['index'=>2,'type'=>'text','content'=>$illegal_detail_data_val['fmedianame']],
                                ['index'=>4,'type'=>'text','content'=>$illegal_detail_data_val['ffullname']],
                                ['index'=>6,'type'=>'text','content'=>$illegal_detail_data_val['fad_ill_times'].'次'],
                                ['index'=>8,'type'=>'text','content'=>$illegal_detail['net_original_url']?$illegal_detail['net_original_url']:''],
                                ['index'=>10,'type'=>'text','content'=>$illegal_detail['net_target_url']?$illegal_detail['net_target_url']:''],
                                $fillegalcontent,
                                ['index'=>14,'type'=>'text','content'=>$illegal_detail['thumb_url_true']?$illegal_detail['thumb_url_true']:'']
                            ];
                            $ill_detail_data[] = [
                                "type" => "text",
                                "bookmark" => "宋体三号数字序号标题",
                                "text" => ($illegal_detail_data_key+1).'、'.$illegal_detail_data_val['fad_name']
                            ];
                            $ill_detail_data[] = [
                                "type" => "filltable",
                                "bookmark" => "填表数据",//{"index":2,"type":"text","content":"xm"},
                                "data" => $illegal_detail_list
                            ];
                        }
                    }

                }
            }
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号",
            "text" => "本期共监测".$media_des."全部类别广告".$net_media_customize[1]['fad_times']."条，其中违法广告".$net_media_customize[1]['fad_illegal_times']."条，违法率".$net_media_customize[1]['times_illegal_rate']."%。"
        ];

        if(empty($every_ad_type_name)){
            $every_ad_type_name = '本期各广告类别暂未发现违法广告';
        }else{
            $every_ad_type_name = "违法广告集中在".implode('、',$every_ad_type_name);
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号",
            "text" => $every_ad_type_name
        ];
        if(empty($every_net_media_name)){
            $every_net_media_name = '本期各互联网媒体暂未发现违法广告';
        }else{
            $every_net_media_name = "违法广告集中在".implode('、',$every_net_media_name)."中发布。";
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号",
            "text" => $every_net_media_name
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号加粗居中",
            "text" => $user['regulatorpname']."广告监测中心"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号加粗居中",
            "text" => $date_end_ymd
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号标题",
            "text" => "一、监测总体情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "此次共监测".$net_media_count_str
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "监测情况总条形图",
            "data" => $every_net_ill_data
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "监测情况总列表",
            "data" => $every_net_type_data
        ];

        //各媒体发布情况
        foreach ($media_issue_data as $media_issue_data_val){
            $data['content'][] = $media_issue_data_val;
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号标题",
            "text" => "二、主要类别发布情况"
        ];

        //主要类别发布情况
        foreach ($adclass_issue_data as $adclass_issue_data_val){
            $data['content'][] = $adclass_issue_data_val;
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号标题",
            "text" => "三、涉嫌违法广告发布情况"
        ];



        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $wf_des
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "违法广告量占比图",
            "data" => $every_net_ill_data
        ];

        foreach ($ill_media_data as $ill_media_data_val){
            $data['content'][] = $ill_media_data_val;
        }

        //五、违法广告明细
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体二号标题",
            "text" => '五、违法广告明细'
        ];

        foreach ($ill_detail_data as $ill_detail_data_val){
            $data['content'][] = $ill_detail_data_val;
        }


        $report_data = json_encode($data,true);
        //echo $report_data;exit;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
//将生成记录保存
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].date('Y年m月d日',$report_start_date).'至'.date('Y年m月d日',$report_end_date).'互联网报告';//$report_start_date.'至'.$report_end_date
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 40;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid'] 	= $user['fid'];
            $data['fcustomer'] 	= $user['system_num'];
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }
    }

    /*生成日报表XLS*/
    public  function create_day(){
        $illtype = I('illtype',-1);//违法类型
        $mclass = I('mclass',-1);//媒介类型
        //$daytime = I('daytime',date('Y-m-d'));//日期
        $daytime = I('daytime','2018-12-20');//日期
        $use_open_search = getconfig('use_open_search');//是否是用open_search的数据
        //为1则表示使用open_search的明细数据
        if($use_open_search == 1){
            $this->day_report_os($daytime,$illtype,$mclass);
        }else{
            $this->day_report_unos($daytime,$illtype,$mclass);
        }
    }
    //生成日报表XLS open_search明细数据导出方法
    public  function day_report_os($daytime='2019-01-01',$illtype=-1,$mclass=-1){
        ini_set('memory_limit','1024M');
        if($daytime == ''){
            $daytime =  date('Y-m-d');
        }
        $bdmedias = $this->get_owner_media_ids();//媒体权限组
        $user = session('regulatorpersonInfo');//获取用户信息
        //每次只取一个媒体的数据（为了防止满5000缺数据）
        foreach ($bdmedias as $bdmedia){
            $data[] = $this->day_data($illtype,$mclass,$daytime,[$bdmedia]);
        }
        $issueList = [];
        foreach ($data as $data_val){
            $issueList = array_merge($issueList,$data_val);
        }
        //判断是否是数据导出
        if(!$issueList || empty($issueList)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
        }
        $excel_url = $this->out_excel($issueList);
        if($illtype == 0){
            $illtype_name = '不违法';
        }elseif($illtype == 20){
            $illtype_name = '违法';
        }elseif($illtype == 30){
            $illtype_name = '违法';
        }else{
            $illtype_name = '全部';
        }
        $media_type_name = '';
        switch (intval($mclass)){
            case 1:
                $media_type_name = '电视';
                break;
            case 2:
                $media_type_name = '广播';
                break;
            case 3:
                $media_type_name = '报纸';
                break;
            case 13:
                $media_type_name = '互联网';
                break;
        }

        $system_num = getconfig('system_num');

        $pnname = date('Y年m月d',strtotime($daytime)).'日'.$media_type_name.$illtype_name.'广告发布情况表';
        $data['pnname'] 			= $pnname;
        $data['pntype'] 			= 10;
        $data['pnfiletype'] 		= 30;
        $data['pnstarttime'] 		= $daytime;
        $data['pnendtime'] 		    = $daytime;
        $data['pncreatetime'] 		= date('Y-m-d H:i:s');
        $data['pnurl'] 				= $excel_url;
        $data['pnhtml'] 			= '';
        $data['pnfocus']            = 0;
        $data['pntrepid']           = $user['fregulatorpid'];
        $data['pncreatepersonid']   = $user['fid'];
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$excel_url));
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'生成失败'));
        }

    }

    //生成日报表XLS 非open_search明细数据导出方法（发布数据）
    public function day_report_unos($date='2019-01-01',$illtype=-1,$mclass=-1){
        session_write_close();
        ini_set('memory_limit','1024M');
        header("Content-type: text/html; charset=utf-8");
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        if($date == ''){
            $date =  date('Y-m-d');
        }
        switch ($illtype){
            case -1:
                $ill_type = 'fillegaltypecode IN(0,30)';
                break;
            case 0:
                $ill_type = 'fillegaltypecode = 0';
                break;
            case 30:
                $ill_type = 'fillegaltypecode > 0';
                break;
        }

        $usermedia = $this->get_owner_media_ids();

        $regulatorpersonInfo = M('tregulatorperson')
            ->field('fregulatorid as bumen_id,fid')
            ->where('fid='.session('regulatorpersonInfo.fid'))->find();
        if(!empty($regulatorpersonInfo)){
            $regulatorInfo = M('tregulator')->where(array('fid' => $regulatorpersonInfo['bumen_id']))->find();//监管机构信息
            $regulatorpersonInfo['fregulatorid']        = $regulatorInfo['fid'];//部门ID
            $regulatorpersonInfo['fregulatorpid']       = D('Function')->get_tregulatoraction($regulatorInfo['fid']);//获取当前单位所属机构ID
            $do_tr = M('tregulator')->field('tregulator.fname,tregion.fname as regionname,tregion.fname1 as regionname1,tregulator.fregionid')->join('tregion on tregulator.fregionid=tregion.fid')->where(array('tregulator.fid' => $regulatorpersonInfo['fregulatorpid']))->find();//获取机构信息
            $regulatorpersonInfo['regulatorpname']      = $do_tr['fname'];//所属机构名称
            $regulatorpersonInfo['regionid']            = $do_tr['fregionid'];//机构行政区划ID
            $regulatorpersonInfo['regionname']          = $do_tr['regionname'];//机构行政区划名称
            $regulatorpersonInfo['regionname1']         = $do_tr['regionname1'];//机构行政区划全名称
            $regulatorpersonInfo['regulator_regionid']  = $regulatorInfo['fregionid'];//部门行政区划ID
            $regulatorpersonInfo['regulatorname']       = $regulatorInfo['fname'];//部门名称
//用户媒介权限
            $mediajurisdiction = D('Function')->get_mediajurisdiction($regulatorInfo['fid'],[],[],$regulatorpersonInfo['fid']);//获取媒体权限组
            $regulatorpersonInfo['mediajurisdiction']   = $mediajurisdiction?$mediajurisdiction:array('0');

            $system_num = getconfig('system_num');
            $where_tt['freg_id'] = session('regulatorpersonInfo.fregulatorpid');
            $where_tt['fcustomer'] = $system_num;
            $do_tt = M('tbn_media_grant')->field('fmedia_id')->where($where_tt)->select();
            foreach ($do_tt as $key => $value) {
                array_push($regulatorpersonInfo['mediajurisdiction'], $value['fmedia_id']);
            }
        }
        $regionid = $regulatorpersonInfo['regionid'];//机构行政区划ID
        $regionid = substr($regionid , 0 , 2);
        $arr = explode("-" ,$date);
        $now_date = $arr[0].$arr[1].'_'.$regionid;//当月
        /*序号	媒体类别	媒体名称	发行日期	广告名称	广告类别	品牌	生产厂家	发布位置	违法类型	涉嫌违法内容	违法表现代码	违法表现	建样日期*/
        if($mclass == 1){
            $mt_name = '电视';
            $tv_table = 'ttvissue_'.date('Y',strtotime($date)).'_'.$regulatorpersonInfo['regionid'];
            $ttvsample = 'ttvsample_'.$regulatorpersonInfo['regionid'];
            $issues = 'select tv.fmediaid,tv.fsampleid,tc.fclass, (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,FROM_UNIXTIME(tv.fissuedate, "%Y-%m-%d") AS fissuedate,tad.fadname,ac.ffullname as fadclass,
tad.fbrand,tvs.fadent,tvs.fillegaltypecode,tvs.fillegalcontent,tvs.fexpressioncodes,tvs.fexpressions,left(tvs.fcreatetime,11) as fcreatetime,
concat_ws("-",FROM_UNIXTIME(tv.fstarttime, "%h:%i:%s"),FROM_UNIXTIME(tv.fendtime, "%h:%i:%s")) as fplace
from '.$tv_table.' as tv
join '.$ttvsample.' tvs on tvs.fid=tv.fsampleid
join tad on tad.fadid=tvs.fadid and tad.fadid<>0
join tmedia as tm on tm.fid=tvs.fmediaid
join tmediaclass as tc on tc.fid = left(tm.fmediaclassid,2)
join tadclass as ac on ac.fcode = left(tad.fadclasscode,2)
where tv.fmediaid in ('.join(',',$usermedia).')
and datediff("'.$date.'",FROM_UNIXTIME(tv.fissuedate, "%Y-%m-%d"))=0
and tvs.'.$ill_type;
        }elseif($mclass == 2){
            $mt_name = '广播';
            $tb_table = 'tbcissue_'.date('Y',strtotime($date)).'_'.$regulatorpersonInfo['regionid'];
            $tbcsample = 'tbcsample_'.$regulatorpersonInfo['regionid'];
            $issues = 'select tb.fmediaid,tb.fsampleid,tc.fclass, (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,FROM_UNIXTIME(tb.fissuedate, "%Y-%m-%d") AS fissuedate,tad.fadname,ac.ffullname as fadclass,tad.fbrand,tbs.fadent,tbs.fillegaltypecode,tbs.fillegalcontent,tbs.fexpressioncodes,tbs.fexpressions,left(tbs.fcreatetime,11) as fcreatetime,
concat_ws("-",FROM_UNIXTIME(tb.fstarttime, "%h:%i:%s"),FROM_UNIXTIME(tb.fendtime, "%h:%i:%s")) as fplace
from '.$tb_table.' as tb
join '.$tbcsample.' tbs on tbs.fid=tb.fsampleid
join tad on tad.fadid=tbs.fadid and tad.fadid<>0
join tmedia as tm on tm.fid=tbs.fmediaid
join tmediaclass as tc on tc.fid = left(tm.fmediaclassid,2)
join tadclass as ac on ac.fcode = left(tad.fadclasscode,2)
where tb.fmediaid in ('.join(',',$usermedia).')
and datediff("'.$date.'",FROM_UNIXTIME(tb.fissuedate, "%Y-%m-%d"))=0
and tbs.'.$ill_type;
        }elseif($mclass == 3){
            $mt_name = '报纸';
            $tp_table = 'tpaperissue_'.$regulatorpersonInfo['regionid'];
            $tpapersample = 'tbcsample_'.$regulatorpersonInfo['regionid'];
            $issues = 'select tp.fmediaid,tp.fpapersampleid as fsampleid,tc.fclass, (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,left(tp.fissuedate,11)as fissuedate,tad.fadname,ac.ffullname as fadclass,tad.fbrand,tps.fadent,tps.fillegaltypecode,tps.fillegalcontent,tps.fexpressioncodes,tps.fexpressions,left(tps.fcreatetime,11) as fcreatetime,tp.fpage as fplace
from '.$tp_table.' as tp
join '.$tpapersample.' tps on tps.fpapersampleid=tp.fpapersampleid
join tad on tad.fadid=tps.fadid and tad.fadid<>0
join tmedia as tm on tm.fid=tps.fmediaid
join tmediaclass as tc on tc.fid = left(tm.fmediaclassid,2)
join tadclass as ac on ac.fcode = left(tad.fadclasscode,2)
where tp.fmediaid in ('.join(',',$usermedia).')
and datediff("'.$date.'",tp.fissuedate)=0
and tps.'.$ill_type;
        }elseif($mclass == 13){
            $mt_name = '互联网';
            $issues = 'select tn.fmediaid,tn.tid as fsampleid,tc.fclass, (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,FROM_UNIXTIME(tn.net_created_date/1000, "%Y-%m-%d") AS fissuedate,tns.fadname,ac.ffullname as fadclass,tns.fbrand,tns.fadent,tns.fillegaltypecode,tns.fillegalcontent,tns.fexpressioncodes,null as fexpressions,FROM_UNIXTIME(tn.net_created_date/1000, "%Y-%m-%d") as fcreatetime,
concat_ws("-",tn.net_x,tn.net_y) as fplace
from tnetissueputlog as tn
join tnetissue tns on tn.tid=tns.major_key  and finputstate=2
join tmedia as tm on tm.fid=tns.fmediaid
join tmediaclass as tc on tc.fid = left(tm.fmediaclassid,2)
join tadclass as ac on ac.fcode = left(tns.fadclasscode,2)
where tn.fmediaid in ('.join(',',$usermedia).')
and datediff("'.$date.'",FROM_UNIXTIME(tn.net_created_date/1000, "%Y-%m-%d"))=0
and tbs.'.$ill_type;
        }elseif($mclass == -1){
            $issues = 'select tv.fmediaid,tv.fsampleid,tc.fclass, (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,FROM_UNIXTIME(tv.fissuedate, "%Y-%m-%d") AS fissuedate,tad.fadname,ac.ffullname as fadclass,
        tad.fbrand,tvs.fadent,tvs.fillegaltypecode,tvs.fillegalcontent,tvs.fexpressioncodes,tvs.fexpressions,left(tvs.fcreatetime,11) as fcreatetime,
        concat_ws("-",FROM_UNIXTIME(tv.fstarttime, "%h:%i:%s"),FROM_UNIXTIME(tv.fendtime, "%h:%i:%s")) as fplace
        from ttvissue_'.$now_date.' as tv
        join ttvsample tvs on tvs.fid=tv.fsampleid
        join tad on tad.fadid=tvs.fadid and tad.fadid<>0
        join tmedia as tm on tm.fid=tvs.fmediaid
        join tmediaclass as tc on tc.fid = left(tm.fmediaclassid,2)
        join tadclass as ac on ac.fcode = left(tad.fadclasscode,2)
        where tv.fmediaid in ('.join(',',$usermedia).')
        and tvs.'.$ill_type.'
        and datediff("'.$date.'",FROM_UNIXTIME(tv.fissuedate, "%Y-%m-%d"))=0

        union all (
        select tb.fmediaid,tb.fsampleid,tc.fclass, (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,FROM_UNIXTIME(tb.fissuedate, "%Y-%m-%d") AS fissuedate,tad.fadname,ac.ffullname as fadclass,tad.fbrand,tbs.fadent,tbs.fillegaltypecode,tbs.fillegalcontent,tbs.fexpressioncodes,tbs.fexpressions,left(tbs.fcreatetime,11) as fcreatetime,
        concat_ws("-",FROM_UNIXTIME(tb.fstarttime, "%h:%i:%s"),FROM_UNIXTIME(tb.fendtime, "%h:%i:%s")) as fplace
        from tbcissue_'.$now_date.' as tb
        join tbcsample tbs on tbs.fid=tb.fsampleid
        join tad on tad.fadid=tbs.fadid and tad.fadid<>0
        join tmedia as tm on tm.fid=tbs.fmediaid
        join tmediaclass as tc on tc.fid = left(tm.fmediaclassid,2)
        join tadclass as ac on ac.fcode = left(tad.fadclasscode,2)
        where tb.fmediaid in ('.join(',',$usermedia).')
        and tbs.'.$ill_type.'
        and datediff("'.$date.'",FROM_UNIXTIME(tb.fissuedate, "%Y-%m-%d"))=0)
        union all (
        select tp.fmediaid,tp.fpapersampleid as fsampleid,tc.fclass, (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,left(tp.fissuedate,11)as fissuedate,tad.fadname,ac.ffullname as fadclass,tad.fbrand,tps.fadent,tps.fillegaltypecode,tps.fillegalcontent,tps.fexpressioncodes,tps.fexpressions,left(tps.fcreatetime,11) as fcreatetime,tp.fpage as fplace
        from tpaperissue as tp
        join tpapersample tps on tps.fpapersampleid=tp.fpapersampleid
        join tad on tad.fadid=tps.fadid and tad.fadid<>0
        join tmedia as tm on tm.fid=tps.fmediaid
        join tmediaclass as tc on tc.fid = left(tm.fmediaclassid,2)
        join tadclass as ac on ac.fcode = left(tad.fadclasscode,2)
        where tp.fmediaid in ('.join(',',$usermedia).')
        and tps.'.$ill_type.'
        and datediff("'.$date.'",tp.fissuedate)=0)
        union all (
        select tn.fmediaid,tn.tid as fsampleid,tc.fclass, (case when instr(tm.fmedianame,"（") > 0 then left(tm.fmedianame,instr(tm.fmedianame,"（") -1) else tm.fmedianame end) as fmedianame,FROM_UNIXTIME(tn.net_created_date/1000, "%Y-%m-%d") AS fissuedate,tns.fadname,ac.ffullname as fadclass,tns.fbrand,tns.fadent,tns.fillegaltypecode,tns.fillegalcontent,tns.fexpressioncodes,null as fexpressions,FROM_UNIXTIME(tn.net_created_date/1000, "%Y-%m-%d") as fcreatetime,
        concat_ws("-",tn.net_x,tn.net_y) as fplace
        from tnetissueputlog as tn
        join tnetissue tns on tn.tid=tns.major_key  and finputstate=2
        join tmedia as tm on tm.fid=tns.fmediaid
        join tmediaclass as tc on tc.fid = left(tm.fmediaclassid,2)
        join tadclass as ac on ac.fcode = left(tns.fadclasscode,2)
        where tn.fmediaid in ('.join(',',$usermedia).')
        and tns.'.$ill_type.'
        and datediff("'.$date.'",FROM_UNIXTIME(tn.net_created_date/1000, "%Y-%m-%d"))=0)
        ';
        }
        $data = M()->query($issues);//当月总发布量
        if(empty($data)){
            $this->ajaxReturn(array('code'=>-1,'msg'=>'当前选择条件下无数据'));
        }
        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        /*序号	媒体类别	媒体名称	发行日期	广告名称	广告类别	品牌	生产厂家	发布位置	违法类型	涉嫌违法内容	违法表现代码	违法表现	建样日期
        */
        $sheet ->setCellValue('A1','序号');
        $sheet ->setCellValue('B1','媒体类别');
        $sheet ->setCellValue('C1','媒体名称');
        $sheet ->setCellValue('D1','发行日期');
        $sheet ->setCellValue('E1','广告名称');
        $sheet ->setCellValue('F1','广告类别');
        $sheet ->setCellValue('G1','品牌');
        $sheet ->setCellValue('H1','生产厂家');
        $sheet ->setCellValue('I1','发布位置');
        $sheet ->setCellValue('J1','违法类型');
        $sheet ->setCellValue('K1','涉嫌违法内容');
        $sheet ->setCellValue('L1','违法表现代码');
        $sheet ->setCellValue('M1','违法表现');
        foreach ($data as $key => $value) {
            $sheet ->setCellValue('A'.($key+2) ,$key+1);//序号
            $sheet ->setCellValue('B'.($key+2) ,$value['fclass']);//媒体类别
            $sheet ->setCellValue('C'.($key+2) ,$value['fmedianame']);//媒体名称
            $sheet ->setCellValue('D'.($key+2) ,$value['fissuedate']);//发行日期
            $sheet ->setCellValue('E'.($key+2) ,$value['fadname']);//广告名称
            $sheet ->setCellValue('F'.($key+2) ,$value['fadclass']);//广告类别
            $sheet ->setCellValue('G'.($key+2) ,$value['fbrand']);//品牌
            $sheet ->setCellValue('H'.($key+2) ,$value['fadent']);//生产厂家
            $sheet ->setCellValue('I'.($key+2) ,$value['fplace']);//发布位置
            if($value['fillegaltypecode'] == 20){
                $fillegaltype = '违法';
            }elseif($value['fillegaltypecode'] == 30){
                $fillegaltype = '违法';
            }else{
                $fillegaltype = '不违法';
            }
            $sheet ->setCellValue('J'.($key+2) ,$fillegaltype);//违法类型
            $sheet ->setCellValue('K'.($key+2) ,$value['fillegalcontent']);//涉嫌违法内容
            $sheet ->setCellValue('L'.($key+2) ,$value['fexpressioncodes']);//违法表现代码
            $sheet ->setCellValue('M'.($key+2) ,$value['fexpressions']);//违法表现

        }
        $objActSheet =$objPHPExcel->getActiveSheet();

//给当前活动的表设置名称
        $objActSheet->setTitle('Sheet1');

        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $date_now = $date;
        $date = date('Ymdhis');
        $savefile = './Public/Xls/'.$date.'.xlsx';
        $objWriter->save($savefile);

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

        if($illtype == 0){
            $illtype_name = '不违法';
        }elseif($illtype == 20){
            $illtype_name = '违法';
        }elseif($illtype == 30){
            $illtype_name = '违法';
        }

        $system_num = getconfig('system_num');

//将生成记录保存
        $pnname = $regulatorpname.date('Y年m月d',strtotime($date_now)).'日'.$mt_name.$illtype_name.'广告发布情况表';
        $data['pnname'] 			= $pnname;
        $data['pntype'] 			= 10;
        $data['pnfiletype'] 		= 30;
        $data['pnstarttime'] 		= $date_now;
        $data['pnendtime'] 		    = $date_now;
        $data['pncreatetime'] 		= date('Y-m-d H:i:s');
        $data['pnurl'] 				= $ret['url'];
        $data['pnhtml'] 			= '';
        $data['pnfocus']            = 0;
        $data['pntrepid']           = $regulatorpersonInfo['fregulatorpid'];
        $data['pncreatepersonid']   = $regulatorpersonInfo['fid'];
        $data['fcustomer']  = $system_num;
        $do_tn = M('tpresentation')->add($data);
        if(!empty($do_tn)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'生成失败'));
        }
    }

    //导出日报excel表格
    public function out_excel($data)
    {
        ini_set('memory_limit','1024M');
        header("Content-type: text/html; charset=utf-8");
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        $sheet ->setCellValue('A1','序号');
        $sheet ->setCellValue('B1','广告名称');
        $sheet ->setCellValue('C1','广告内容类别');
        $sheet ->setCellValue('D1','发布媒体');
        $sheet ->setCellValue('E1','媒体类型');
        $sheet ->setCellValue('F1','违法类型');
        $sheet ->setCellValue('G1','发布时间');
        $sheet ->setCellValue('H1','开始时间');
        $sheet ->setCellValue('I1','结束时间');
        $sheet ->setCellValue('J1','时长/版面');
        $sheet ->setCellValue('K1','违法代码');
        $sheet ->setCellValue('L1','涉嫌违法内容');

        $arr = [1=>'电视',2=>'广播',3=>'报刊',13=>'互联网'];

        foreach ($data as $key => $value) {
            $sheet ->setCellValue('A'.($key+2) ,$key+1);
            $sheet ->setCellValue('B'.($key+2) ,$value['fadname']);
            $sheet ->setCellValue('C'.($key+2) ,$value['fadclass']);
            $sheet ->setCellValue('D'.($key+2) ,$value['fmedianame']);
            if($value['mclass'] == 1){
                $sheet ->setCellValue('E'.($key+2) ,$arr[1]);
            }elseif($value['mclass'] == 2){
                $sheet ->setCellValue('E'.($key+2) ,$arr[2]);
            }elseif($value['mclass'] == 3){
                $sheet ->setCellValue('E'.($key+2) ,$arr[3]);
            }elseif($value['mclass'] == 13){
                $sheet ->setCellValue('E'.($key+2) ,$arr[13]);
            }
            $sheet ->setCellValue('F'.($key+2) ,$value['fillegaltype']);
            $sheet ->setCellValue('G'.($key+2) ,$value['fissuedate']);
            if($value['mclass'] == 1 || $value['mclass'] == 2){
                $sheet ->setCellValue('H'.($key+2) ,$value['fstarttime']);
                $sheet ->setCellValue('I'.($key+2) ,$value['fendtime']);
            }else{
                $sheet ->setCellValue('H'.($key+2) ,' ');
                $sheet ->setCellValue('I'.($key+2) ,' ');
            }
            if($value['mclass'] == 1 || $value['mclass'] == 2){
                $sheet ->setCellValue('J'.($key+2) ,$value['flength']);
            }elseif($value['mclass'] == 3){
                $sheet ->setCellValue('J'.($key+2) ,$value['fpage']);
            }else{
                $sheet ->setCellValue('J'.($key+2) ,' ');
            }
            $sheet ->setCellValue('K'.($key+2) ,$value['fexpressioncodes']);
            $sheet ->setCellValue('L'.($key+2) ,$value['fillegalcontent']);

        }

        $objActSheet =$objPHPExcel->getActiveSheet();
        //给当前活动的表设置名称
        $objActSheet->setTitle('Sheet1');

        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $date = date('Ymdhis');
        $savefile = './Public/Xls/'.$date.'.xlsx';
        $objWriter->save($savefile);
        D('Function')->write_log('监测数据',1,'excel导出成功');

        $ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件
        return $ret['url'];
    }

    //日报数据，循环查询OPENSEARCH
    public function day_data($illtype = -1,$mclass=-1,$daytime='2018-11-23',$bdmedias = [])
    {
        session_write_close();
        header('Access-Control-Allow-Origin:*');//允许跨域
        $system_num = getconfig('system_num');

        $p = 1;//当前第几页
        $pp = 500;//每页显示多少记录
        $forcount = -1;


        $client = A('Common/OpenSearch','Model')->client();//获得OpenSearchClient
        $searchClient = new SearchClient($client);
        $params = new SearchParamsBuilder();

        $params->setAppName(C('OPEN_SEARCH_CUSTOMER'));//设置应用名称

        $where = array();

        if($illtype != -1) $where['fillegaltypecode'] = $illtype;//搜索违法类别

        if($mclass != -1) $where['fmediaclassid'] = intval($mclass);//搜索媒介类别
        
        $where['fissuedate'] = strtotime($daytime);

        $forgid = strlen($system_num) == 6 ? '20'.$system_num:$system_num;
        $where['forgid'] = $forgid;

        $setQuery = A('Common/OpenSearch','Model')->setQuery($where);//组装搜索字句

        $whereFilter = array();
        $params->setQuery($setQuery);//设置搜索
        $params->addSort('fissuedate', SearchParamsBuilder::SORT_DECREASE);//降序排序
        $params->setFormat("json");//数据返回json格式

        //获取数据的所有媒体
        $params->addDistinct(array('key' => 'fmediaid', 'distTimes' => 1, 'distCount' => 1, 'reserved' => 'false'));//设置排重字段
        $params->setStart(0);//起始位置
        $params->setHits(500);//返回数量
        $params->setFetchFields(
            array(
                'fmediaid',//媒介id
            )
        );//设置需返回哪些字段
        $ret = $searchClient->execute($params->build());
        $result = json_decode($ret->result,true);
        $medialist = $result['result']['items'];
        $medias = [];
        foreach ($medialist as $key => $value) {
            $medias[] = $value['fmediaid'];
        }
        if(!empty($medias)){
            $fmedias1 = array_diff($bdmedias,array_diff($bdmedias,$medias));//本地有的媒体权限
            $fmedias2 = array_diff($medias,$bdmedias);//opensearch所有数据的媒体，本地没有的媒体
            if(count($fmedias1)<=250){
                $whereFilter['fmediaid'] = array('in',$bdmedias);
            }elseif(count($fmedias2)<=250){
                $whereFilter['fmediaid'] = array('notin',$fmedias2);
            }else{
                $whereFilter['fmediaid'] = array('=',0);
            }
            $setFilter = A('Common/OpenSearch','Model')->setFilter($whereFilter);//组装过滤条件
            $params->setFilter($setFilter);//设置文档过滤条件
            $params->addDistinct(array('key' => 'fmediaid', 'distTimes' => 1, 'distCount' => 1, 'reserved' => 'true'));//清除排重字段
        }else{
            return [];
        }

        $params->setFetchFields(
            array(
                'identify',//识别码（哈希）
                'fmediaclassid',//媒体类型
                'fmediaid',//媒介id
                'fmedianame',//媒介名称
                'fadclasscode', //广告类别id
                'fadname',//广告名称
                'fissuedate', //发布日期
                'flength',//发布时长
                'fpage',//版面
                'fstarttime', //发布开始时间戳
                'fendtime', //发布开始时间戳
                'fillegaltypecode', //违法类型
                'fexpressioncodes', //违法代码
                'fillegalcontent', //违法内容
            )
        );//设置需返回哪些字段
        $params->addDistinct();//清除排重字段
        $issueList = array();
        $a = 1;
        while($forcount!=0){
            $params->setStart(($p-1)*$pp);//起始位置
            $params->setHits($pp);//返回数量
            $ret = $searchClient->execute($params->build());//执行查询并返回信息
            $result = json_decode($ret->result,true);
            $issueList0 = $result['result']['items'];
            foreach($issueList0 as $issu){

                $issu['fstarttime'] = date('H:i:s',$issu['fstarttime']);
                $issu['fendtime'] = date('H:i:s',$issu['fendtime']);
                $issu['fissuedate'] = date('Y-m-d',$issu['fissuedate']);

                $adclasscode_arr = explode(sprintf("%c", 9),$issu['fadclasscode']);
                $issu['fadclass'] = $adClass[$adclasscode_arr[0]]['ffullname'];

                if($issu['fillegaltypecode'] == '0'){
                    $issu['fillegaltype'] = '不违法';
                }else{
                    $issu['fillegaltype'] = '违法';
                }
                $issu['mclass'] = $issu['fmediaclassid'];
                $issu['sid'] = $issu['identify'];
                $issueList[] = $issu;
                //销毁无用字段
                unset($issu['fmediaclassid']);
                unset($issu['identify']);
            }
            $p++;
            //循环导出数据专用，上限5000条
            if($forcount<0){
                $forcount = ceil($result['result']['total']/$pp);
            }
            if(!empty($forcount)){
                $forcount--;
            }
            if($p>10){
                break;
            }

        }
        return $issueList;
    }



    /**
     * 生成天津周报word 20181218  by yjn create_sjz_week_report
     */
    public function create_week(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport = New StatisticalModel();//实例化数据统计模型
        $StatisticalReport1 = New StatisticalReportModel();//实例化数据统计模型
        $s_time = I('s_time','2018-12-24');//开始日期
        $e_time = I('e_time','2018-12-30');//结束日期
        $week_num = intval(date('W',strtotime($s_time)));
        $report_start_date = strtotime($s_time);//周初时间戳
        $report_end_date = strtotime($e_time)+84399;//周末时间戳

        $date_ym = date('Y年m月',$report_start_date);//年月字符
        $user = session('regulatorpersonInfo');//获取用户信息
        $zxs = ['110000','120000','310000','500000'];//直辖市
        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级

        //文字描述匹配
        switch ($re_level){
            case 2:
            case 3:
            case 4:
                $regionid_str = substr($user['regionid'],0,4);
                $dq_name = '全市';
                $xj_name = '区县';
                $self_name = '市属';
                break;
            case 1:
                $regionid_str = substr($user['regionid'],0,2);
                if(!in_array($user['regionid'],$zxs)){
                    $dq_name = '全省';
                    $xj_name = '市级';
                    $self_name = '省属';
                }else{
                    $dq_name = '全市';
                    $xj_name = '区级';
                    $self_name = '市属';
                }
                break;
            case 5:
                $regionid_str = $user['regionid'];
                $dq_name = '全县';
                break;
        }

        $date_str = date('Y年m月d日',strtotime($s_time)).'至'.date('Y年m月d日',strtotime($e_time)).'(第'.$week_num.'周)';//开始年月字符


        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);//结束时间
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数
        $owner_media_ids = $this->get_owner_media_ids();//获取权限内媒体

        $ct_media_tv_count = $StatisticalReport->ct_media_count('tv',$owner_media_ids);//电视媒体数量

        $ct_media_bc_count = $StatisticalReport->ct_media_count('bc',$owner_media_ids);//广播媒体数量

        $ct_media_paper_count = $StatisticalReport->ct_media_count('paper',$owner_media_ids);//报纸媒体数量

        $ct_media_count = $ct_media_tv_count + $ct_media_bc_count + $ct_media_paper_count;

        $pc_media_count = $StatisticalReport->net_media_counts($regionid_str,'1301');//pc媒体数量
        $gzh_media_count = $StatisticalReport->net_media_counts($regionid_str,'1303');//公众号数量

        $pc_illegal_situation = $StatisticalReport->net_illegal_situation_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));//pc违法情形列表

        $pc_medias = $StatisticalReport->net_media_type_ids('1301');
        $wx_medias = $StatisticalReport->net_media_type_ids('1303');
        /*
        * @各媒体监测总情况
        **/
        $net_media_customize = $StatisticalReport1->net_media_customize_sum($owner_media_ids,false,$report_start_date,$report_end_date);//互联网监测总情况
        $net_gzh_customize1 = $StatisticalReport1->net_media_customize_sum($owner_media_ids,['source_type',4],$report_start_date,$report_end_date);//微信广告数量
        $net_gzh_customize = $StatisticalReport->report_ad_monitor('fmedia_class_code',$wx_medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','13');//微信广告数量
        $net_pc_customize1 = $StatisticalReport1->net_media_customize_sum($owner_media_ids,['net_platform',1],$report_start_date,$report_end_date);//pc广告数量

        $net_pc_customize = $StatisticalReport->report_ad_monitor('fmedia_class_code',$pc_medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','13');//pc广告数量


        $ct_jczqk = $StatisticalReport->report_ad_monitor('fmedia_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));

        $ct_fad_times = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times = 0;//定义传统媒体发布违法广告条次
        $every_media_data = [];//定义媒体发布监测情况数组
//循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk as $ct_jczqk_key => $ct_jczqk_val){
            $every_media_data_array = [];
            if(!$ct_jczqk_val['titlename']){
                if($ct_jczqk_val['fmedia_class_code'] != '13'){
                    $ct_fad_times += $ct_jczqk_val['fad_times'];
                    $ct_fad_illegal_times += $ct_jczqk_val['fad_illegal_times'];
                    $ct_fad_play_len += $ct_jczqk_val['fad_play_len'];
                    $ct_fad_illegal_play_len += $ct_jczqk_val['fad_illegal_play_len'];
                }
                $fmedia_class_code = $ct_jczqk_val['fmedia_class_code'];
                $tmediaclass_name = $ct_jczqk_val['tmediaclass_name'];
            }else{
                $fmedia_class_code = 'all';
                $tmediaclass_name = '合计';
            }
            $every_media_data_array = [
                $tmediaclass_name,
                $ct_jczqk_val['fad_times'],
                $ct_jczqk_val['fad_illegal_times'],
                $ct_jczqk_val['times_illegal_rate'].'%',
                $ct_jczqk_val['fad_play_len'],
                $ct_jczqk_val['fad_illegal_play_len'],
                $ct_jczqk_val['lens_illegal_rate']?$ct_jczqk_val['lens_illegal_rate'].'%':'0.00'.'%'
            ];
            switch ($fmedia_class_code){
                case '01':
                    $every_media_data[0] = $every_media_data_array;
                    break;
                case '02':
                    $every_media_data[1] = $every_media_data_array;
                    break;
                case '03':
                    $every_media_data[2] = $every_media_data_array;
                    break;
                case '13':
                    $every_media_data[3] = $every_media_data_array;
                    break;
                case 'all':
                    $every_media_data[4] = $every_media_data_array;
                    break;
            }

        }
        $every_media_data = [
            $every_media_data[0],
            $every_media_data[1],
            $every_media_data[2],
            $every_media_data[3],
            $every_media_data[4],
        ];
        $every_media_data = to_string($every_media_data);
        foreach ($every_media_data as $every_media_k=>$every_media_v){
            if(empty($every_media_v)){
                unset($every_media_data[$every_media_k]);
            }
        }
        /*
        * @各地域监测总情况
        *
        **/
        $every_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));

        $every_region_data = [];//定义各地区发布监测情况数组
        $every_region_num = 0;
//循环赋值
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){
            if(!$every_region_jczqk_val['titlename']){
                if($every_region_jczqk_val['fregionid'] == $user['regionid']){
                    $every_region_jczqk_tregion_name = $self_name;
                }else{
                    $every_region_jczqk_tregion_name = $every_region_jczqk_val['tregion_name'];
                }
                if($every_region_jczqk_val['fad_illegal_times'] != 0){
                    $every_region_num++;
                    $every_region_name[] = $every_region_jczqk_tregion_name;
                }
                $every_region_data[] = [
                    strval($every_region_jczqk_key+1),
                    $every_region_jczqk_tregion_name,
                    $every_region_jczqk_val['fad_times'],
                    $every_region_jczqk_val['fad_illegal_times'],
                    $every_region_jczqk_val['times_illegal_rate'].'%',
                    $every_region_jczqk_val['fad_play_len'],
                    $every_region_jczqk_val['fad_illegal_play_len'],
                    $every_region_jczqk_val['lens_illegal_rate']?$every_region_jczqk_val['lens_illegal_rate'].'%':'0.00'.'%'
                ];
            }

        }
        $every_region_data = to_string($every_region_data);


        /*
        * @各广告类型监测总情况
        *
        **/
        $every_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));

        $every_adclass_data = [];//定义各大类发布监测情况数组
        $adclass_illegal_times = 0;//各违法地域数量
//循环赋值
        foreach ($every_adclass_jczqk as $every_adclass_jczqk_key => $every_adclass_jczqk_val){
            if(!$every_adclass_jczqk_val['titlename']){
                if($every_adclass_jczqk_val['fad_illegal_times'] > 0){
                    $adclass_illegal_times += $every_adclass_jczqk_val['fad_illegal_times'];
                }
                $every_adclass_data[] = [
                    strval($every_adclass_jczqk_key+1),
                    $every_adclass_jczqk_val['tadclass_name'],
                    $every_adclass_jczqk_val['fad_times'],
                    $every_adclass_jczqk_val['fad_illegal_times'],
                    $every_adclass_jczqk_val['times_illegal_rate'].'%',
                    $every_adclass_jczqk_val['fad_play_len'],
                    $every_adclass_jczqk_val['fad_illegal_play_len'],
                    $every_adclass_jczqk_val['lens_illegal_rate']?$every_adclass_jczqk_val['lens_illegal_rate'].'%':'0.00'.'%'
                ];
            }
        }
        $every_adclass_data = to_string($every_adclass_data);


        foreach ($every_adclass_data as $every_adclass_data_key=>$every_adclass_data_val){
            if($every_adclass_data_val[3] > 0){
                $every_adclass_name[] = $every_adclass_data_val[1];
                if($every_adclass_data_key == 0){
                    $every_adclass_rate_des[] = $every_adclass_data_val[1].'广告占涉嫌违法广告总量的'.round(($every_adclass_data_val[3]/$adclass_illegal_times)*100,2).'％';
                }else{
                    $every_adclass_rate_des[] .= $every_adclass_data_val[1].'广告占'.round(($every_adclass_data_val[3]/$adclass_illegal_times)*100,2).'％';
                }
            }
        }

        /*
        * @电视各地域监测总情况
        *
        **/
        $every_tv_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','01');

        $every_tv_region_data = [];//定义各地区发布监测情况数组
        $every_tv_region_num = 0;//电视各违法地域数量
        //循环赋值
        foreach ($every_tv_region_jczqk as $every_tv_region_jczqk_key => $every_tv_region_jczqk_val){
            if(!$every_tv_region_jczqk_val['titlename']){
                if($every_tv_region_jczqk_val['fregionid'] == $user['regionid']){
                    $every_tv_region_jczqk_tregion_name = $self_name;
                }else{
                    $every_tv_region_jczqk_tregion_name = $every_tv_region_jczqk_val['tregion_name'];
                }
                if($every_tv_region_jczqk_val['fad_illegal_times'] != 0){
                    $every_tv_region_num++;
                    $every_tv_region_name[] = $every_tv_region_jczqk_tregion_name;
                }
                $every_tv_region_data[] = [
                    strval($every_tv_region_jczqk_key+1),
                    $every_tv_region_jczqk_tregion_name,
                    $every_tv_region_jczqk_val['fad_times'],
                    $every_tv_region_jczqk_val['fad_illegal_times'],
                    $every_tv_region_jczqk_val['times_illegal_rate'].'%',
                    $every_tv_region_jczqk_val['fad_play_len'],
                    $every_tv_region_jczqk_val['fad_illegal_play_len'],
                    $every_tv_region_jczqk_val['lens_illegal_rate'].'%'
                ];
            }
        }
        $every_tv_region_data = to_string($every_tv_region_data);


        /*
        * @电视各广告类型监测总情况
        *
        **/
        $every_tv_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','01');

        $every_tv_adclass_data = [];//定义各地区发布监测情况数组
        //循环赋值
        foreach ($every_tv_adclass_jczqk as $every_tv_adclass_jczqk_key => $every_tv_adclass_jczqk_val){

            if(!$every_tv_adclass_jczqk_val['titlename']){
                $every_tv_adclass_data[] = [
                    strval($every_tv_adclass_jczqk_key+1),
                    $every_tv_adclass_jczqk_val['tadclass_name'],
                    $every_tv_adclass_jczqk_val['fad_times'],
                    $every_tv_adclass_jczqk_val['fad_illegal_times'],
                    $every_tv_adclass_jczqk_val['times_illegal_rate'].'%',
                    $every_tv_adclass_jczqk_val['fad_play_len'],
                    $every_tv_adclass_jczqk_val['fad_illegal_play_len'],
                    $every_tv_adclass_jczqk_val['lens_illegal_rate'].'%'
                ];
            }

        }
        $every_tv_adclass_data = to_string($every_tv_adclass_data);



        /*
        * @广播各地域监测总情况
        *
        **/
        $every_tb_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','02');

        $every_tb_region_data = [];//定义各地区发布监测情况数组
        $every_tb_region_num = 0;//广播各违法地域数量
//循环赋值
        foreach ($every_tb_region_jczqk as $every_tb_region_jczqk_key => $every_tb_region_jczqk_val){
            if(!$every_tb_region_jczqk_val['titlename']){
                if($every_tb_region_jczqk_val['fregionid'] == $user['regionid']){
                    $every_tb_region_jczqk_tregion_name = $self_name;
                }else{
                    $every_tb_region_jczqk_tregion_name = $every_tb_region_jczqk_val['tregion_name'];
                }

                if($every_tb_region_jczqk_val['fad_illegal_times'] != 0){
                    $every_tb_region_num++;
                    $every_tb_region_name[] = $every_tb_region_jczqk_tregion_name;
                }

                $every_tb_region_data[] = [
                    strval($every_tb_region_jczqk_key+1),
                    $every_tb_region_jczqk_tregion_name,
                    $every_tb_region_jczqk_val['fad_times'],
                    $every_tb_region_jczqk_val['fad_illegal_times'],
                    $every_tb_region_jczqk_val['times_illegal_rate'].'%',
                    $every_tb_region_jczqk_val['fad_play_len'],
                    $every_tb_region_jczqk_val['fad_illegal_play_len'],
                    $every_tb_region_jczqk_val['lens_illegal_rate'].'%'
                ];
            }
        }
        $every_tb_region_data = to_string($every_tb_region_data);



        /*
        * @广播各广告类型监测总情况
        *
        **/
        $every_tb_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','02');

        $every_tb_adclass_data = [];//定义各地区发布监测情况数组
//循环赋值
        foreach ($every_tb_adclass_jczqk as $every_tb_adclass_jczqk_key => $every_tb_adclass_jczqk_val){

            if(!$every_tb_adclass_jczqk_val['titlename']) {
                $every_tb_adclass_data[] = [
                    strval($every_tb_adclass_jczqk_key+1),
                    $every_tb_adclass_jczqk_val['tadclass_name'],
                    $every_tb_adclass_jczqk_val['fad_times'],
                    $every_tb_adclass_jczqk_val['fad_illegal_times'],
                    $every_tb_adclass_jczqk_val['times_illegal_rate'].'%',
                    $every_tb_adclass_jczqk_val['fad_play_len'],
                    $every_tb_adclass_jczqk_val['fad_illegal_play_len'],
                    $every_tb_adclass_jczqk_val['lens_illegal_rate'].'%'
                ];
            }

        }
        $every_tb_adclass_data = to_string($every_tb_adclass_data);

        /*
        * @报纸各地域监测总情况
        *
        **/
        $every_tp_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','03');

        $every_tp_region_data = [];//定义各地区发布监测情况数组
        $every_tp_region_num = 0;//报纸各违法地域数量
//循环赋值
        foreach ($every_tp_region_jczqk as $every_tp_region_jczqk_key => $every_tp_region_jczqk_val){

            if(!$every_tp_region_jczqk_val['titlename']) {

                if($every_tp_region_jczqk_val['fregionid'] == $user['regionid']){
                    $every_tp_region_jczqk_tregion_name = $self_name;
                }else{
                    $every_tp_region_jczqk_tregion_name = $every_tp_region_jczqk_val['tregion_name'];
                }
                if($every_tp_region_jczqk_val['fad_illegal_times'] != 0){
                    $every_tp_region_num++;
                    $every_tp_region_name[] = $every_tp_region_jczqk_tregion_name;
                }
                $every_tp_region_data[] = [
                    strval($every_tp_region_jczqk_key+1),
                    $every_tp_region_jczqk_tregion_name,
                    $every_tp_region_jczqk_val['fad_times'],
                    $every_tp_region_jczqk_val['fad_illegal_times'],
                    $every_tp_region_jczqk_val['times_illegal_rate'].'%'
                ];
            }
        }
        $every_tp_region_data = to_string($every_tp_region_data);


        /*
        * @报纸各广告类型监测总情况
        *
        **/
        $every_tp_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','03');

        $every_tp_adclass_data = [];//定义各地区发布监测情况数组
        //循环赋值
        foreach ($every_tp_adclass_jczqk as $every_tp_adclass_jczqk_key => $every_tp_adclass_jczqk_val){

            if(!$every_tp_adclass_jczqk_val['titlename']) {
                $every_tp_adclass_data[] = [
                    strval($every_tp_adclass_jczqk_key+1),
                    $every_tp_adclass_jczqk_val['tadclass_name'],
                    $every_tp_adclass_jczqk_val['fad_times'],
                    $every_tp_adclass_jczqk_val['fad_illegal_times'],
                    $every_tp_adclass_jczqk_val['times_illegal_rate'].'%'
                ];
            }
        }
        $every_tp_adclass_data = to_string($every_tp_adclass_data);

        /*
        * @PC各媒体监测总情况
        *
        **/
        $every_pc_media_data = [];

        $every_pc_media_jczqk = $StatisticalReport->report_ad_monitor('fmediaid',$pc_medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));
        foreach ($every_pc_media_jczqk as $every_pc_media_jczqk_key=>$every_pc_media_jczqk_val){
            if(!$every_pc_media_jczqk_val['titlename']) {
                $every_pc_media_data[] = [
                    strval($every_pc_media_jczqk_key+1),
                    $every_pc_media_jczqk_val['tmedia_name'],
                    $every_pc_media_jczqk_val['fad_times'],
                    $every_pc_media_jczqk_val['fad_illegal_times'],
                    $every_pc_media_jczqk_val['times_illegal_rate'].'%'
                ];
            }
        }

        $every_pc_media_data = to_string($every_pc_media_data);

        /*
        * @PC各广告类型监测总情况
        *
        **/
        $every_pc_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$pc_medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));
        $every_pc_adclass_data = [];

        foreach ($every_pc_adclass_jczqk as $every_pc_adclass_jczqk_key=>$every_pc_adclass_jczqk_val){
            if(!$every_pc_adclass_jczqk_val['titlename']) {
                $every_pc_adclass_data[] = [
                    strval($every_pc_adclass_jczqk_key+1),
                    $every_pc_adclass_jczqk_val['tadclass_name'],
                    $every_pc_adclass_jczqk_val['fad_times'],
                    $every_pc_adclass_jczqk_val['fad_illegal_times'],
                    $every_pc_adclass_jczqk_val['times_illegal_rate'].'%'
                ];

            }

        }


        $every_pc_adclass_data = to_string($every_pc_adclass_data);
        /*
        * @微信各媒体监测总情况
        **/

        $every_wx_media_data = [];

        $every_wx_media_jczqk = $StatisticalReport->report_ad_monitor('fmediaid',$wx_medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));
        foreach ($every_wx_media_jczqk as $every_wx_media_jczqk_key=>$every_wx_media_jczqk_val){
            if(!$every_wx_media_jczqk_val['titlename']) {
                $every_wx_media_data[] = [
                    strval($every_wx_media_jczqk_key+1),
                    $every_wx_media_jczqk_val['tmedia_name'],
                    $every_wx_media_jczqk_val['fad_times'],
                    $every_wx_media_jczqk_val['fad_illegal_times'],
                    $every_wx_media_jczqk_val['times_illegal_rate'].'%'
                ];
            }
        }

        $every_wx_media_data = to_string($every_wx_media_data);


//定义数据数组
        $data = [
            'dotfilename' => 'ReportTemplate.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报告标题一",
            "text" => $user['regulatorname'].""
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报告标题二",
            "text" => "广告监测周报"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "通报对象",
            "text" => $dq_name."工商行政管理局、市场监督管理部门："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报表通报描述",
            "text" => $date_str. $user['regulatorname']."广告监测中心对".$dq_name."所属的媒介广告发布情况进行了监测，现将有关情况通报如下："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "一、监测范围及对象",
            "text" => "一、监测范围及对象"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（一）传统媒体广告监测",
            "text" => "（一）传统媒体广告监测"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "传统媒体广告监测描述",
            "text" => "本期共对".$dq_name."所属的".$ct_media_count."家电视、广播、报纸等传统媒体广告发布情况进行了监测，其中电视媒体".$ct_media_tv_count."家、广播媒体".$ct_media_bc_count."家、报纸媒体".$ct_media_paper_count."家，广告监测范围为".$date_ymd."-".$date_md."（共".$days."天）"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（二）互联网媒体广告监测",
            "text" => "（二）互联网媒体广告监测"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "互联网媒体广告监测描述",
            "text" => "本期共对".$dq_name."所属的".$pc_media_count."家主要PC门户网及".$gzh_media_count."个微信公众号媒体，广告监测范围为".$date_ymd."-".$date_md."（共".$days."天）"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "二、监测总体情况",
            "text" => "二、监测总体情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（一）传统媒体广告监测情况",
            "text" => "（一）传统媒体广告监测情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "传统媒体广告监测情况描述",
            "text" => "本期共监测到电视、广播、报纸的各类广告".$ct_fad_times."条次，涉嫌违法广告".$ct_fad_illegal_times."条次，条次违法率为".round(($ct_fad_illegal_times/$ct_fad_times)*100,2)."％。"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（二）互联网媒体广告监测情况",
            "text" => "（二）互联网媒体广告监测情况"
        ];
        if(!empty($every_media_data[3])){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "互联网媒体广告监测情况描述",
                "text" => "本期共监测到互联网广告".$every_media_data[3][1]."条次，其中涉嫌违法广告".$every_media_data[3][2]."条次，违法率".$every_media_data[3][3]."。"
            ];
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "互联网媒体广告监测情况描述",
                "text" => "本期暂未监测到互联网广告。"
            ];
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表1各类媒体广告监测统计情况标题",
            "text" => "表1各类媒体广告监测统计情况"
        ];
        $every_media_data = array_values($every_media_data);
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表1各类媒体广告监测统计情况列表含时长",
            "data" => $every_media_data
        ];
        array_pop($every_media_data);
        $every_media_datas = $this->create_chart_data($every_media_data,0,1,2,3);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告监测情况条状图",
            "data" => $every_media_datas[0]
        ];
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告发布量饼状图",
            "data" => $every_media_datas[1]
        ];
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告违法发布量饼状图",
            "data" => $every_media_datas[2]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "三、传统媒体广告监测情况",
            "text" => "三、传统媒体广告监测情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（一）广告总体监测情况",
            "text" => "（一）广告总体监测情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "广告总体监测情况描述",
            "text" => "本期监测数据显示，发布涉嫌违法广告条次违法率最为严重的".$every_region_num."个".$xj_name."依次为：".implode('、',$every_region_name)."。"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表2本期各地区发布涉嫌违法广告情况标题",
            "text" => "表2 本期各地区发布涉嫌违法广告情况"
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表2本期各地区发布涉嫌违法广告情况列表含时长",
            "data" => $every_region_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表2注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];

        $every_region_data_chart = $this->create_chart_data($every_region_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告监测情况条状图",
            "data" => $every_region_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告发布量饼状图",
            "data" => $every_region_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告违法发布量饼状图",
            "data" => $every_region_data_chart[2]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表3本期各大类违法广告发布总情况标题",
            "text" => '表3本期各大类违法广告发布总情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表3本期各大类违法广告发布总情况列表含时长",
            "data" => $every_adclass_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表3注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];
        $every_adclass_data_chart = $this->create_chart_data($every_adclass_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告监测情况列条状图",
            "data" => $every_adclass_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告发布量饼状图",
            "data" => $every_adclass_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告违法发布量饼状图",
            "data" => $every_adclass_data_chart[2]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "各类媒体广告发布情况描述",
            "text" => '涉嫌违法广告类别主要集中在'.implode('、',$every_adclass_name).'，其中'.implode("、",$every_adclass_rate_des).'。'
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（二）电视发布涉嫌违法广告情况",
            "text" => '（二）电视发布涉嫌违法广告情况'
        ];
        if(!empty($every_tv_region_data)){
            if($every_tv_region_num > 0){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "电视发布涉嫌违法广告情况描述",
                    "text" => '本期电视发布涉嫌违法广告条次违法率最为严重的'.$every_tv_region_num.'个地区依次为：'.implode('、',$every_tv_region_name).'。'
                ];
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "电视发布涉嫌违法广告情况描述",
                    "text" => '本期电视暂未发现违法广告。'
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况标题",
                "text" => '表4 电视发布涉嫌广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况列表",
                "data" => $every_tv_region_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表4注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];
            $every_tv_region_data_chart = $this->create_chart_data($every_tv_region_data);
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "电视广告监测情况条状图",
                "data" => $every_tv_region_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "电视广告发布量饼状图",
                "data" => $every_tv_region_data_chart[1]
            ];
            if($every_tv_region_num > 0){
                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "电视广告违法发布量饼状图",
                    "data" => $every_tv_region_data_chart[2]
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表5电视各广告类型发布涉嫌违法广告情况标题",
                "text" => '表5 电视各广告类型发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表5电视各广告类型发布涉嫌违法广告情况列表",
                "data" => $every_tv_adclass_data
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表5注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];

            $every_tv_adclass_data_chart = $this->create_chart_data($every_tv_adclass_data);
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "电视各广告类型广告监测情况条状图",
                "data" => $every_tv_adclass_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "电视各广告类型广告发布量饼状图",
                "data" => $every_tv_adclass_data_chart[1]
            ];
            if($every_tv_region_num > 0){
                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "电视各广告类型广告违法发布量饼状图",
                    "data" => $every_tv_adclass_data_chart[2]
                ];
            }
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "电视发布涉嫌违法广告情况描述",
                "text" => '本期电视暂未监测到广告。'
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（三）广播发布涉嫌违法广告情况",
            "text" => '（三）广播发布涉嫌违法广告情况'
        ];
        if(!empty($every_tb_region_data)){
            if($every_tb_region_num > 0){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "广播发布涉嫌违法广告情况描述",
                    "text" => '本期广播发布涉嫌违法广告条次违法率最为严重的'.$every_tb_region_num.'个地区依次为：'.implode('、',$every_tb_region_name).'。'
                ];
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "广播发布涉嫌违法广告情况描述",
                    "text" => '本期广播暂未发现违法广告。'
                ];
            }
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表6广播发布涉嫌违法广告情况标题",
                "text" => '表6 广播发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表6广播发布涉嫌违法广告情况列表",
                "data" => $every_tb_region_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表6注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];

            $every_tb_region_data_chart = $this->create_chart_data($every_tb_region_data);
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广播广告监测情况条状图",
                "data" => $every_tb_region_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广播广告发布量饼状图",
                "data" => $every_tb_region_data_chart[1]
            ];
            if($every_tb_region_num > 0){

                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "广播广告违法发布量饼状图",
                    "data" => $every_tb_region_data_chart[2]
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表7广播各广告类别发布涉嫌违法广告情况标题",
                "text" => '表7 广播各广告类别发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表7广播各广告类别发布涉嫌违法广告情况列表",
                "data" => $every_tb_adclass_data
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表7注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];

            $every_tb_adclass_data_chart = $this->create_chart_data($every_tb_adclass_data);

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广播各广告类型广告监测情况条状图",
                "data" => $every_tb_adclass_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广播各广告类型广告发布量饼状图",
                "data" => $every_tb_adclass_data_chart[1]
            ];
            if($every_tb_region_num > 0){

                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "广播各广告类型广告违法发布量饼状图",
                    "data" => $every_tb_adclass_data_chart[2]
                ];
            }
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "广播发布涉嫌违法广告情况描述",
                "text" => '本期广播暂未监测到广告。'
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（四）报纸发布涉嫌违法广告情况",
            "text" => '（四）报纸发布涉嫌违法广告情况'
        ];
        if(!empty($every_tp_region_data)){
            if($every_tp_region_num > 0){
                $every_tp_region_str = '本期报纸发布涉嫌违法广告条次违法率最为严重的'.$every_tp_region_num.'个地区依次为：'.implode('、',$every_tp_region_name).'。';
            }else{
                $every_tp_region_str = '本期各地区报纸暂未发布违法广告。';
            }//1119e
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "报纸发布涉嫌违法广告情况描述",
                "text" => $every_tp_region_str
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表8报纸发布涉嫌违法广告情况标题",
                "text" => '表8 报纸发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表8报纸发布涉嫌违法广告情况列表",
                "data" => $every_tp_region_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表8注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];
            $every_tp_region_data_chart = $this->create_chart_data($every_tp_region_data);

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "报纸广告监测情况条状图",
                "data" => $every_tp_region_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "报纸广告发布量饼状图",
                "data" => $every_tp_region_data_chart[1]
            ];
            if($every_tp_region_num > 0){

                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "报纸广告违法发布量饼状图",
                    "data" => $every_tp_region_data_chart[2]
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表9报纸各广告类型发布涉嫌违法广告情况标题",
                "text" => '表9 报纸各广告类型发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表9报纸各广告类型发布涉嫌违法广告情况列表",
                "data" => $every_tp_adclass_data
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表9注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];
            $every_tp_adclass_data_chart = $this->create_chart_data($every_tp_adclass_data);

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "报纸各广告类型广告监测情况条状图",
                "data" => $every_tp_adclass_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "报纸各广告类型广告发布量饼状图",
                "data" => $every_tp_adclass_data_chart[1]
            ];
            if($every_tp_region_num > 0){

                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "报纸各广告类型广告违法发布量饼状图",
                    "data" => $every_tp_adclass_data_chart[2]
                ];
            }
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "报纸发布涉嫌违法广告情况描述",
                "text" => '本期报纸暂未监测到广告'
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "四、互联网广告监测情况",
            "text" => "四、互联网广告监测情况"
        ];
        if($every_media_data[3][1] != 0){
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "（一）总体发布情况",
                "text" => "（一）总体发布情况"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "总体发布情况描述",
                "text" => "本期共监测到互联网广告".$every_media_data[3][1]."条，其中违法广告".$every_media_data[3][2]."条，违法率".$every_media_data[3][3]."。微信公众号全部类别广告".$net_gzh_customize[1]['fad_times']."条。"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "PC门户网站广告发布情况标题",
                "text" => "1、PC门户网站广告发布情况"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "PC门户网站广告发布情况列表",
                "data" => $every_pc_media_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "PC门户网站广告发布情况列表注释",
                "text" => "（注：按发布涉嫌违法广告情况严重程度从高到低排列）"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "PC门户网站广告类别发布情况标题",
                "text" => "2、PC门户网站广告类别发布情况"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "PC门户网站广告类别发布情况列表",
                "data" => $every_pc_adclass_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "PC门户网站广告类别发布情况列表注释",
                "text" => "（注：按发布涉嫌违法广告情况严重程度从高到低排列）"
            ];
            $every_pc_adclass_name = implode('、',$every_pc_adclass_name);
            if(!empty($every_pc_adclass_name)){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "PC门户网站广告类别发布情况列表描述",
                    "text" => $every_pc_adclass_name."是本月PC网络违法广告主要类别。"
                ];
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "PC门户网站广告类别发布情况列表描述",
                    "text" => "本月PC网络所有类别暂未发现违法广告。"
                ];
            }
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "微信公众号发布情况",
                "text" => "3、微信公众号发布情况"
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "微信公众号发布情况列表",
                "data" => $every_wx_media_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "微信公众号发布情况列表注释",
                "text" => "（注：按发布涉嫌违法广告情况严重程度从高到低排列）"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "（二）涉嫌违法发布情况",
                "text" => "（二）涉嫌违法发布情况"
            ];

            if($net_pc_customize[1]['fad_illegal_times'] != 0){
                $ybwf = '其中涉嫌违法广告'.$net_pc_customize[1]['fad_illegal_times'].'条，违法率'.$net_pc_customize[1]['times_illegal_rate'].'%。';
                /*            if($net_pc_customize['yzwfggsl'] != 0){
                $yzwf = '涉嫌严重违法广告'.$net_pc_customize['yzwfggsl'].'条，严重违法率'.$net_pc_customize['ggslyzwfl'].'%，';
                }else{
                $yzwf = '未发现严重违法广告';
                }*/
                $pc_des = $ybwf;
            }else{
                $pc_des = '未发现涉嫌违法广告。';
            }

            if($net_gzh_customize[1]['fad_illegal_times'] != 0){
                $ybwf = '其中涉嫌违法广告'.$net_gzh_customize[1]['fad_illegal_times'].'条，违法率'.$net_gzh_customize[1]['times_illegal_rate'].'%。';
                /*            if($net_gzh_customize['yzwfggsl'] != 0){
                $yzwf = '涉嫌严重违法广告'.$net_gzh_customize['yzwfggsl'].'条，严重违法率'.$net_gzh_customize['ggslyzwfl'].'%，';
                }else{
                $yzwf = '未发现严重违法广告';
                }*/
                $wx_des = $ybwf;
            }else{
                $wx_des = '未发现涉嫌违法广告。';
            }
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "涉嫌违法发布情况描述一",
                "text" => "本月次监测PC门户网站全部类别广告".$net_pc_customize[1]['fad_times']."条，".$pc_des."微信公众号全部类别广告".$net_gzh_customize[1]['fad_times']."条，".$wx_des
            ];
            $every_pc_adclass_des = '';
            foreach ($every_pc_adclass_data as $every_pc_adclass_data_key=>$every_pc_adclass_data_val){
                if($every_pc_adclass_data_val['3'] > 0){
                    $every_pc_adclass_name .= $every_pc_adclass_data_val[1].'、';
                    if($every_pc_adclass_data_key == 0){
                        $every_pc_adclass_des .= $every_pc_adclass_data_val[1].'广告占涉嫌违法广告总量的'.round(($every_pc_adclass_data_val[3]/$net_pc_customize[1]['fad_illegal_times'])*100,2).'％、';
                    }else{
                        $every_pc_adclass_des .= $every_pc_adclass_data_val[1].'广告占'.round(($every_pc_adclass_data_val[3]/$net_pc_customize[1]['fad_illegal_times'])*100,2).'％、';
                    }
                }
            }
            $every_pc_media_des = '';
            $every_pc_media_name = [];
            foreach ($every_pc_media_data as $every_pc_media_data_key=>$every_pc_media_data_val){
                if($every_pc_media_data_val['3'] > 0){
                    $every_pc_media_name[] = $every_pc_media_data_val[1];
                    if($every_pc_media_data_key == 0){
                        $every_pc_media_des .= $every_pc_media_data_val[1].'发布的占涉嫌违法广告总量的'.round(($every_pc_media_data_val[3]/$net_pc_customize[1]['fad_illegal_times'])*100,2).'％、';
                    }else{
                        $every_pc_media_des .= $every_pc_media_data_val[1].'占'.round(($every_pc_media_data_val[3]/$net_pc_customize[1]['fad_illegal_times'])*100,2).'％、';
                    }
                }else{
                    $not_illegal_media_num++;
                    $not_illegal_media_name[] = $every_pc_media_data_val[1];
                }
            }
            if($every_pc_adclass_des == ''){
                $every_pc_adclass_des = '全部类别下无违法广告';
            }
            if($every_pc_media_des == ''){
                $every_pc_media_des = '所有PC门户网站未发布违法广告';
            }
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "涉嫌违法发布情况描述二",
                "text" => "其中PC门户网站违法违法分布情况如下：".$every_pc_adclass_des.";".$every_pc_media_des."。"
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "涉嫌违法发布情况描述三",
                "text" => $not_illegal_media_num."家网站本次监测没有发现违法广告，分别是：".implode('、',$not_illegal_media_name)."。"
            ];
            if(!empty($every_pc_media_name)){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "涉嫌违法发布情况描述四",
                    "text" => "PC门户网站中，".implode('、',$every_pc_media_name)."为主要的违法广告发布网站。"
                ];
                foreach ($every_pc_media_data as $every_pc_media_data_key=>$every_pc_media_data_val){
                    array_splice($every_pc_media_data[$every_pc_media_data_key],0,1);
                    array_splice($every_pc_media_data[$every_pc_media_data_key],1,1);
                    array_splice($every_pc_media_data[$every_pc_media_data_key],2,1);
                    array_splice($every_pc_media_data[$every_pc_media_data_key],2,1);
                    array_splice($every_pc_media_data[$every_pc_media_data_key],2,1);
                }
                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "PC网站违法发布量饼状图",
                    "data" => $every_pc_media_data
                ];
            }


            $data['content'][] = [
                "type" => "text",
                "bookmark" => "互联网主要违法情形",
                "text" => "1、互联网主要违法情形"
            ];
            $every_pc_illegal_situation_data = [];
            foreach ($pc_illegal_situation as $pc_illegal_situation_key => $pc_illegal_situation_val){
                $every_pc_illegal_situation_data[] = [
                    strval($pc_illegal_situation_key+1),
                    $pc_illegal_situation_val['fillegalcontent'],
                    $pc_illegal_situation_val['illegal_count']
                ];
            }
            if(!empty($every_pc_illegal_situation_data)){
                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "PC门户网站主要违法情形列表",
                    "data" => $every_pc_illegal_situation_data
                ];
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "微信公众号违法广告发布情况描述",
                    "text" => '本期互联网暂未发现违法情形'
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "微信公众号违法广告发布情况",
                "text" => "微信公众号违法广告发布情况"
            ];
            $every_wx_media_des = '';
            foreach ($every_wx_media_data as $every_wx_media_data_key=>$every_wx_media_data_val){
                if($every_wx_media_data_val['3'] > 0){
                    $every_wx_media_name .= $every_wx_media_data_val[1].'、';
                    if($every_wx_media_data_key == 0){
                        $every_wx_media_des .= $every_wx_media_data_val[1].'发布的占涉嫌违法广告总量的'.round(($every_wx_media_data_val[3]/$net_gzh_customize[1]['fad_illegal_times'])*100,2).'％、';
                    }else{
                        $every_wx_media_des .= $every_wx_media_data_val[1].'占'.round(($every_wx_media_data_val[3]/$net_gzh_customize[1]['fad_illegal_times'])*100,2).'％、';
                    }
                }else{
                    $not_illegal_wx_media_num++;
                    $not_illegal_wx_media_name .= $every_wx_media_data_val[1].'、';
                }
            }
            if($every_wx_media_des == ''){
                $every_wx_media_des = '本次监测微信公众号没有发布涉嫌违法广告数据。';
            }
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "微信公众号违法广告发布情况描述",
                "text" => $every_wx_media_des
            ];
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "微信公众号违法广告发布情况描述",
                "text" => '本期未检测到互联网数据'
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "落款",
            "text" => $user['regulatorname']
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "时间",
            "text" => $date_end_ymd
        ];

        $report_data = json_encode($data);
        //echo $report_data;exit;//20190103

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $system_num = getconfig('system_num');

//将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].date('Y年m月d日',$report_start_date).'至'.date('Y年m月d日',$report_end_date).'周报(第'.$week_num.'周)';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 20;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }

    /**
     * 生成天津月报word 20181218  by yjn    create_sjz_month_report
     */

    public function create_month_20190708(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport = New StatisticalModel();//实例化数据统计模型
        $StatisticalReport1 = New StatisticalReportModel();//实例化数据统计模型
        //$month = I('daytime',date('Y-m',time()));//接收时间  TODO::时间改
        $month = I('daytime','2018-12');//接收时间  TODO::时间改
        $report_start_date = strtotime($month);//指定月份月初时间戳
        $report_end_date = strtotime($month.'-'.date('t', strtotime($month)).' 23:59:59');
        //$report_end_date = mktime(23, 59, 59, date('m', strtotime($month))+1, 00);//指定月份月末时间戳
        if($report_end_date > time()){
            $report_end_date = time();

        }

        $date_ym = date('Y年m月',$report_start_date);//年月字符
        $user = session('regulatorpersonInfo');//获取用户信息
        $zxs = ['110000','120000','310000','500000'];//直辖市
        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级

        //文字描述匹配
        switch ($re_level){
            case 2:
            case 3:
            case 4:
                $regionid_str = substr($user['regionid'],0,4);
                $dq_name = '全市';
                $xj_name = '区县';
                $self_name = '市属';
                break;
            case 1:
                $regionid_str = substr($user['regionid'],0,2);
                if(!in_array($user['regionid'],$zxs)){
                    $dq_name = '全省';
                    $xj_name = '市级';
                    $self_name = '省属';
                }else{
                    $dq_name = '全市';
                    $xj_name = '区级';
                    $self_name = '市属';
                }
                break;
            case 5:
                $regionid_str = $user['regionid'];
                $dq_name = '全县';
                break;
        }

        $date_table = date('Y',strtotime($month)).'_'.$user['regionid'];//组合表

        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);//结束时间
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数

        $owner_media_ids = $this->get_owner_media_ids(-1,'','13');//获取权限内媒体

        $ct_media_tv_count = $StatisticalReport->ct_media_count('tv',$owner_media_ids);//电视媒体数量

        $ct_media_bc_count = $StatisticalReport->ct_media_count('bc',$owner_media_ids);//广播媒体数量

        $ct_media_paper_count = $StatisticalReport->ct_media_count('paper',$owner_media_ids);//报纸媒体数量

        $ct_media_count = $ct_media_tv_count + $ct_media_bc_count + $ct_media_paper_count;

        $pc_media_count = $StatisticalReport->net_media_counts($regionid_str,'1301');//pc媒体数量
        $gzh_media_count = $StatisticalReport->net_media_counts($regionid_str,'1303');//公众号数量

        $pc_illegal_situation = $StatisticalReport->net_illegal_situation_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));//pc违法情形列表
        $pc_medias = $StatisticalReport->net_media_type_ids('1301');
        $wx_medias = $StatisticalReport->net_media_type_ids('1303');
        /*
        * @各媒体监测总情况
        **/
        //$net_media_customize = $StatisticalReport1->net_media_customize_sum($owner_media_ids,false,$report_start_date,$report_end_date);//互联网监测总情况
        //$net_gzh_customize1 = $StatisticalReport1->net_media_customize_sum($owner_media_ids,['source_type',4],$report_start_date,$report_end_date);//微信广告数量
        //$net_gzh_customize = $StatisticalReport->report_ad_monitor('fmedia_class_code',$wx_medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','13');//微信广告数量
        //$net_pc_customize1 = $StatisticalReport1->net_media_customize_sum($owner_media_ids,['net_platform',1],$report_start_date,$report_end_date);//pc广告数量
        //$net_pc_customize = $StatisticalReport->report_ad_monitor('fmedia_class_code',$pc_medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','13');//pc广告数量

        $ct_jczqk = $StatisticalReport->report_ad_monitor('fmedia_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));

        $ct_fad_times = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times = 0;//定义传统媒体发布违法广告条次
        $every_media_data = [];//定义媒体发布监测情况数组
//循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk as $ct_jczqk_key => $ct_jczqk_val){
            $every_media_data_array = [];
            if(!$ct_jczqk_val['titlename']){
                if($ct_jczqk_val['fmedia_class_code'] != '13'){
                    $ct_fad_times += $ct_jczqk_val['fad_times'];
                    $ct_fad_illegal_times += $ct_jczqk_val['fad_illegal_times'];
                    $ct_fad_play_len += $ct_jczqk_val['fad_play_len'];
                    $ct_fad_illegal_play_len += $ct_jczqk_val['fad_illegal_play_len'];
                }
                $fmedia_class_code = $ct_jczqk_val['fmedia_class_code'];
                $tmediaclass_name = $ct_jczqk_val['tmediaclass_name'];
            }else{
                $fmedia_class_code = 'all';
                $tmediaclass_name = '合计';
            }
            $every_media_data_array = [
                $tmediaclass_name,
                $ct_jczqk_val['fad_times'],
                $ct_jczqk_val['fad_illegal_times'],
                $ct_jczqk_val['times_illegal_rate'].'%',
                $ct_jczqk_val['fad_play_len'],
                $ct_jczqk_val['fad_illegal_play_len'],
                $ct_jczqk_val['lens_illegal_rate']?$ct_jczqk_val['lens_illegal_rate'].'%':'0.00'.'%',
                $ct_jczqk_val['ck_count']?$ct_jczqk_val['ck_count']:'0',
                $ct_jczqk_val['ckl']?$ct_jczqk_val['ckl']:'0.00'.'%',
                $ct_jczqk_val['cl_count']?$ct_jczqk_val['cl_count']:'0',
                $ct_jczqk_val['cll']?$ct_jczqk_val['cll']:'0.00'.'%'
            ];
            switch ($fmedia_class_code){
                case '01':
                    $every_media_data[0] = $every_media_data_array;
                    break;
                case '02':
                    $every_media_data[1] = $every_media_data_array;
                    break;
                case '03':
                    $every_media_data[2] = $every_media_data_array;
                    break;
                case '13':
                    //$every_media_data[3] = $every_media_data_array;
                    break;
                case 'all':
                    $every_media_data[3] = $every_media_data_array;
                    break;
            }

        }
        $every_media_data = [
            $every_media_data[0],
            $every_media_data[1],
            $every_media_data[2],
            //$every_media_data[3],
            $every_media_data[3],
        ];
        $every_media_data = to_string($every_media_data);
        foreach ($every_media_data as $every_media_k=>$every_media_v){
            if(empty($every_media_v)){
                unset($every_media_data[$every_media_k]);
            }
        }


        /*
        * @各地域监测总情况
        *
        **/
        $every_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));

        $every_region_data = [];//定义各地区发布监测情况数组
        $every_region_num = 0;
//循环赋值
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){
            if(!$every_region_jczqk_val['titlename']){
                if($every_region_jczqk_val['fregionid'] == $user['regionid']){
                    $every_region_jczqk_tregion_name = $self_name;
                }else{
                    $every_region_jczqk_tregion_name = $every_region_jczqk_val['tregion_name'];
                }
                if($every_region_jczqk_val['fad_illegal_times'] != 0){
                    $every_region_num++;
                    $every_region_name[] = $every_region_jczqk_tregion_name;
                }
                $every_region_data[] = [
                    strval($every_region_jczqk_key+1),
                    $every_region_jczqk_tregion_name,
                    $every_region_jczqk_val['fad_times'],
                    $every_region_jczqk_val['fad_illegal_times'],
                    $every_region_jczqk_val['times_illegal_rate'].'%',
                    $every_region_jczqk_val['fad_play_len'],
                    $every_region_jczqk_val['fad_illegal_play_len'],
                    $every_region_jczqk_val['lens_illegal_rate']?$every_region_jczqk_val['lens_illegal_rate'].'%':'0.00'.'%',
                    $every_region_jczqk_val['ck_count']?$every_region_jczqk_val['ck_count']:'0',
                    $every_region_jczqk_val['ckl']?$every_region_jczqk_val['ckl']:'0.00'.'%',
                    $every_region_jczqk_val['cl_count']?$every_region_jczqk_val['cl_count']:'0',
                    $every_region_jczqk_val['cll']?$every_region_jczqk_val['cll']:'0.00'.'%'
                ];
            }

        }
        $every_region_data = to_string($every_region_data);


        /*
        * @各广告类型监测总情况
        *
        **/
        $every_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));

        $every_adclass_data = [];//定义各大类发布监测情况数组
        $adclass_illegal_times = 0;//各违法地域数量
//循环赋值
        foreach ($every_adclass_jczqk as $every_adclass_jczqk_key => $every_adclass_jczqk_val){
            if(!$every_adclass_jczqk_val['titlename']){
                if($every_adclass_jczqk_val['fad_illegal_times'] > 0){
                    $adclass_illegal_times += $every_adclass_jczqk_val['fad_illegal_times'];
                }
                $every_adclass_data[] = [
                    strval($every_adclass_jczqk_key+1),
                    $every_adclass_jczqk_val['tadclass_name'],
                    $every_adclass_jczqk_val['fad_times'],
                    $every_adclass_jczqk_val['fad_illegal_times'],
                    $every_adclass_jczqk_val['times_illegal_rate'].'%',
                    $every_adclass_jczqk_val['fad_play_len'],
                    $every_adclass_jczqk_val['fad_illegal_play_len'],
                    $every_adclass_jczqk_val['lens_illegal_rate']?$every_adclass_jczqk_val['lens_illegal_rate'].'%':'0.00'.'%',
                    $every_adclass_jczqk_val['ck_count']?$every_adclass_jczqk_val['ck_count']:'0',
                    $every_adclass_jczqk_val['ckl']?$every_adclass_jczqk_val['ckl']:'0.00'.'%',
                    $every_adclass_jczqk_val['cl_count']?$every_adclass_jczqk_val['cl_count']:'0',
                    $every_adclass_jczqk_val['cll']?$every_adclass_jczqk_val['cll']:'0.00'.'%'
                ];
            }
        }
        $every_adclass_data = to_string($every_adclass_data);


        foreach ($every_adclass_data as $every_adclass_data_key=>$every_adclass_data_val){
            if($every_adclass_data_val[3] > 0){
                $every_adclass_name[] = $every_adclass_data_val[1];
                if($every_adclass_data_key == 0){
                    $every_adclass_rate_des[] = $every_adclass_data_val[1].'广告占涉嫌违法广告总量的'.round(($every_adclass_data_val[3]/$adclass_illegal_times)*100,2).'％';
                }else{
                    $every_adclass_rate_des[] .= $every_adclass_data_val[1].'广告占'.round(($every_adclass_data_val[3]/$adclass_illegal_times)*100,2).'％';
                }
            }
        }

        /*
        * @电视各地域监测总情况
        *
        **/
        $every_tv_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','01');

        $every_tv_region_data = [];//定义各地区发布监测情况数组
        $every_tv_region_num = 0;//电视各违法地域数量
        //循环赋值
        foreach ($every_tv_region_jczqk as $every_tv_region_jczqk_key => $every_tv_region_jczqk_val){
            if(!$every_tv_region_jczqk_val['titlename']){
                if($every_tv_region_jczqk_val['fregionid'] == $user['regionid']){
                    $every_tv_region_jczqk_tregion_name = $self_name;
                }else{
                    $every_tv_region_jczqk_tregion_name = $every_tv_region_jczqk_val['tregion_name'];
                }
                if($every_tv_region_jczqk_val['fad_illegal_times'] != 0){
                    $every_tv_region_num++;
                    $every_tv_region_name[] = $every_tv_region_jczqk_tregion_name;
                }
                $every_tv_region_data[] = [
                    strval($every_tv_region_jczqk_key+1),
                    $every_tv_region_jczqk_tregion_name,
                    $every_tv_region_jczqk_val['fad_times'],
                    $every_tv_region_jczqk_val['fad_illegal_times'],
                    $every_tv_region_jczqk_val['times_illegal_rate'].'%',
                    $every_tv_region_jczqk_val['fad_play_len'],
                    $every_tv_region_jczqk_val['fad_illegal_play_len'],
                    $every_tv_region_jczqk_val['lens_illegal_rate'].'%',
                    $every_tv_region_jczqk_val['ck_count']?$every_tv_region_jczqk_val['ck_count']:'0',
                    $every_tv_region_jczqk_val['ckl']?$every_tv_region_jczqk_val['ckl']:'0.00'.'%',
                    $every_tv_region_jczqk_val['cl_count']?$every_tv_region_jczqk_val['cl_count']:'0',
                    $every_tv_region_jczqk_val['cll']?$every_tv_region_jczqk_val['cll']:'0.00'.'%'
                ];
            }
        }
        $every_tv_region_data = to_string($every_tv_region_data);


        /*
        * @电视各广告类型监测总情况
        *
        **/
        $every_tv_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','01');

        $every_tv_adclass_data = [];//定义各地区发布监测情况数组
        //循环赋值
        foreach ($every_tv_adclass_jczqk as $every_tv_adclass_jczqk_key => $every_tv_adclass_jczqk_val){

            if(!$every_tv_adclass_jczqk_val['titlename']){
                $every_tv_adclass_data[] = [
                    strval($every_tv_adclass_jczqk_key+1),
                    $every_tv_adclass_jczqk_val['tadclass_name'],
                    $every_tv_adclass_jczqk_val['fad_times'],
                    $every_tv_adclass_jczqk_val['fad_illegal_times'],
                    $every_tv_adclass_jczqk_val['times_illegal_rate'].'%',
                    $every_tv_adclass_jczqk_val['fad_play_len'],
                    $every_tv_adclass_jczqk_val['fad_illegal_play_len'],
                    $every_tv_adclass_jczqk_val['lens_illegal_rate'].'%',
                    $every_tv_adclass_jczqk_val['ck_count']?$every_tv_adclass_jczqk_val['ck_count']:'0',
                    $every_tv_adclass_jczqk_val['ckl']?$every_tv_adclass_jczqk_val['ckl']:'0.00'.'%',
                    $every_tv_adclass_jczqk_val['cl_count']?$every_tv_adclass_jczqk_val['cl_count']:'0',
                    $every_tv_adclass_jczqk_val['cll']?$every_tv_adclass_jczqk_val['cll']:'0.00'.'%'
                ];
            }

        }
        $every_tv_adclass_data = to_string($every_tv_adclass_data);



        /*
        * @广播各地域监测总情况
        *
        **/
        $every_tb_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','02');
        //$every_tb_region_jczqk1 = $StatisticalReport1->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'02','','fad_illegal_times_rate','fregionid',false,1,get_check_dates(),true);//广播各地域监测总情况
        $every_tb_region_data = [];//定义各地区发布监测情况数组
        $every_tb_region_num = 0;//广播各违法地域数量
//循环赋值
        foreach ($every_tb_region_jczqk as $every_tb_region_jczqk_key => $every_tb_region_jczqk_val){
            if(!$every_tb_region_jczqk_val['titlename']){
                if($every_tb_region_jczqk_val['fregionid'] == $user['regionid']){
                    $every_tb_region_jczqk_tregion_name = $self_name;
                }else{
                    $every_tb_region_jczqk_tregion_name = $every_tb_region_jczqk_val['tregion_name'];
                }

                if($every_tb_region_jczqk_val['fad_illegal_times'] != 0){
                    $every_tb_region_num++;
                    $every_tb_region_name[] = $every_tb_region_jczqk_tregion_name;
                }

                $every_tb_region_data[] = [
                    strval($every_tb_region_jczqk_key+1),
                    $every_tb_region_jczqk_tregion_name,
                    $every_tb_region_jczqk_val['fad_times'],
                    $every_tb_region_jczqk_val['fad_illegal_times'],
                    $every_tb_region_jczqk_val['times_illegal_rate'].'%',
                    $every_tb_region_jczqk_val['fad_play_len'],
                    $every_tb_region_jczqk_val['fad_illegal_play_len'],
                    $every_tb_region_jczqk_val['lens_illegal_rate'].'%',
                    $every_tb_region_jczqk_val['ck_count']?$every_tb_region_jczqk_val['ck_count']:'0',
                    $every_tb_region_jczqk_val['ckl']?$every_tb_region_jczqk_val['ckl']:'0.00'.'%',
                    $every_tb_region_jczqk_val['cl_count']?$every_tb_region_jczqk_val['cl_count']:'0',
                    $every_tb_region_jczqk_val['cll']?$every_tb_region_jczqk_val['cll']:'0.00'.'%'
                ];
            }
        }
        $every_tb_region_data = to_string($every_tb_region_data);



        /*
        * @广播各广告类型监测总情况
        *
        **/
        $every_tb_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','02');
        //$every_tb_adclass_jczqk1 = $StatisticalReport1->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'02','','fad_illegal_times_rate','fcode',false,1,get_check_dates(),true);//广播各广告类型监测总情况
        $every_tb_adclass_data = [];//定义各地区发布监测情况数组
//循环赋值
        foreach ($every_tb_adclass_jczqk as $every_tb_adclass_jczqk_key => $every_tb_adclass_jczqk_val){
            /*if($every_tb_adclass_jczqk_val['fad_illegal_times'] != 0){
            $every_tb_adclass_num++;
            $every_tb_adclass_name .= $every_tb_region_jczqk_val['tregion_name'].'、';
            }*/
            if(!$every_tb_adclass_jczqk_val['titlename']) {
                $every_tb_adclass_data[] = [
                    strval($every_tb_adclass_jczqk_key+1),
                    $every_tb_adclass_jczqk_val['tadclass_name'],
                    $every_tb_adclass_jczqk_val['fad_times'],
                    $every_tb_adclass_jczqk_val['fad_illegal_times'],
                    $every_tb_adclass_jczqk_val['times_illegal_rate'].'%',
                    $every_tb_adclass_jczqk_val['fad_play_len'],
                    $every_tb_adclass_jczqk_val['fad_illegal_play_len'],
                    $every_tb_adclass_jczqk_val['lens_illegal_rate'].'%',
                    $every_tb_adclass_jczqk_val['ck_count']?$every_tb_adclass_jczqk_val['ck_count']:'0',
                    $every_tb_adclass_jczqk_val['ckl']?$every_tb_adclass_jczqk_val['ckl']:'0.00'.'%',
                    $every_tb_adclass_jczqk_val['cl_count']?$every_tb_adclass_jczqk_val['cl_count']:'0',
                    $every_tb_adclass_jczqk_val['cll']?$every_tb_adclass_jczqk_val['cll']:'0.00'.'%'
                ];
            }

        }
        $every_tb_adclass_data = to_string($every_tb_adclass_data);

        /*
        * @报纸各地域监测总情况
        *
        **/
        $every_tp_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','03');
        //$every_tp_region_jczqk1 = $StatisticalReport1->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'03','','fad_illegal_times_rate','fregionid',false,1,get_check_dates(),true);//报纸各地域监测总情况
        $every_tp_region_data = [];//定义各地区发布监测情况数组
        $every_tp_region_num = 0;//报纸各违法地域数量
//循环赋值
        foreach ($every_tp_region_jczqk as $every_tp_region_jczqk_key => $every_tp_region_jczqk_val){

            if(!$every_tp_region_jczqk_val['titlename']) {

                if($every_tp_region_jczqk_val['fregionid'] == $user['regionid']){
                    $every_tp_region_jczqk_tregion_name = $self_name;
                }else{
                    $every_tp_region_jczqk_tregion_name = $every_tp_region_jczqk_val['tregion_name'];
                }
                if($every_tp_region_jczqk_val['fad_illegal_times'] != 0){
                    $every_tp_region_num++;
                    $every_tp_region_name[] = $every_tp_region_jczqk_tregion_name;
                }
                $every_tp_region_data[] = [
                    strval($every_tp_region_jczqk_key+1),
                    $every_tp_region_jczqk_tregion_name,
                    $every_tp_region_jczqk_val['fad_times'],
                    $every_tp_region_jczqk_val['fad_illegal_times'],
                    $every_tp_region_jczqk_val['times_illegal_rate'].'%',
                    $every_tp_region_jczqk_val['ck_count']?$every_tp_region_jczqk_val['ck_count']:'0',
                    $every_tp_region_jczqk_val['ckl']?$every_tp_region_jczqk_val['ckl']:'0.00'.'%',
                    $every_tp_region_jczqk_val['cl_count']?$every_tp_region_jczqk_val['cl_count']:'0',
                    $every_tp_region_jczqk_val['cll']?$every_tp_region_jczqk_val['cll']:'0.00'.'%'
                ];
            }
        }
        $every_tp_region_data = to_string($every_tp_region_data);


        /*
        * @报纸各广告类型监测总情况
        *
        **/
        $every_tp_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','03');
        //$every_tp_adclass_jczqk1 = $StatisticalReport1->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'03','','fad_illegal_times_rate','fcode',false,1,get_check_dates(),true);//报纸各广告类型监测总情况
        $every_tp_adclass_data = [];//定义各地区发布监测情况数组
//循环赋值
        foreach ($every_tp_adclass_jczqk as $every_tp_adclass_jczqk_key => $every_tp_adclass_jczqk_val){
            /*            if($every_tb_adclass_jczqk_val['fad_illegal_times'] != 0){
            $every_tb_adclass_num++;
            $every_tb_adclass_name .= $every_tb_region_jczqk_val['tregion_name'].'、';
            }*/
            if(!$every_tp_adclass_jczqk_val['titlename']) {
                $every_tp_adclass_data[] = [
                    strval($every_tp_adclass_jczqk_key+1),
                    $every_tp_adclass_jczqk_val['tadclass_name'],
                    $every_tp_adclass_jczqk_val['fad_times'],
                    $every_tp_adclass_jczqk_val['fad_illegal_times'],
                    $every_tp_adclass_jczqk_val['times_illegal_rate'].'%',
                    $every_tp_adclass_jczqk_val['ck_count']?$every_tp_adclass_jczqk_val['ck_count']:'0',
                    $every_tp_adclass_jczqk_val['ckl']?$every_tp_adclass_jczqk_val['ckl']:'0.00'.'%',
                    $every_tp_adclass_jczqk_val['cl_count']?$every_tp_adclass_jczqk_val['cl_count']:'0',
                    $every_tp_adclass_jczqk_val['cll']?$every_tp_adclass_jczqk_val['cll']:'0.00'.'%'
                ];
            }
        }
        $every_tp_adclass_data = to_string($every_tp_adclass_data);

        /*
        * @PC各媒体监测总情况
        *
        **/
        $every_pc_media_data = [];

        $every_pc_media_jczqk = $StatisticalReport->report_ad_monitor('fmediaid',$pc_medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));
        foreach ($every_pc_media_jczqk as $every_pc_media_jczqk_key=>$every_pc_media_jczqk_val){
            if(!$every_pc_media_jczqk_val['titlename']) {
                $every_pc_media_data[] = [
                    strval($every_pc_media_jczqk_key+1),
                    $every_pc_media_jczqk_val['tmedia_name'],
                    $every_pc_media_jczqk_val['fad_times'],
                    $every_pc_media_jczqk_val['fad_illegal_times'],
                    $every_pc_media_jczqk_val['times_illegal_rate'].'%',
                    $every_pc_media_jczqk_val['ck_count']?$every_pc_media_jczqk_val['ck_count']:'0',
                    $every_pc_media_jczqk_val['ckl']?$every_pc_media_jczqk_val['ckl']:'0.00'.'%',
                    $every_pc_media_jczqk_val['cl_count']?$every_pc_media_jczqk_val['cl_count']:'0',
                    $every_pc_media_jczqk_val['cll']?$every_pc_media_jczqk_val['cll']:'0.00'.'%'
                ];
            }
        }

        $every_pc_media_data = to_string($every_pc_media_data);

        /*
        * @PC各广告类型监测总情况
        *
        **/
        /*        $every_pc_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$pc_medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));
                $every_pc_adclass_data = [];

                foreach ($every_pc_adclass_jczqk as $every_pc_adclass_jczqk_key=>$every_pc_adclass_jczqk_val){
                    if(!$every_pc_adclass_jczqk_val['titlename']) {
                        $every_pc_adclass_data[] = [
                            strval($every_pc_adclass_jczqk_key+1),
                            $every_pc_adclass_jczqk_val['tadclass_name'],
                            $every_pc_adclass_jczqk_val['fad_times'],
                            $every_pc_adclass_jczqk_val['fad_illegal_times'],
                            $every_pc_adclass_jczqk_val['times_illegal_rate'].'%',
                            $every_pc_adclass_jczqk_val['ck_count']?$every_pc_adclass_jczqk_val['ck_count']:'0',
                            $every_pc_adclass_jczqk_val['ckl']?$every_pc_adclass_jczqk_val['ckl']:'0.00'.'%',
                            $every_pc_adclass_jczqk_val['cl_count']?$every_pc_adclass_jczqk_val['cl_count']:'0',
                            $every_pc_adclass_jczqk_val['cll']?$every_pc_adclass_jczqk_val['cll']:'0.00'.'%'
                        ];

                    }

                }


                $every_pc_adclass_data = to_string($every_pc_adclass_data);*/
        /*
        * @微信各媒体监测总情况
        **/

        /*        $every_wx_media_data = [];

                $every_wx_media_jczqk = $StatisticalReport->report_ad_monitor('fmediaid',$wx_medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));
                foreach ($every_wx_media_jczqk as $every_wx_media_jczqk_key=>$every_wx_media_jczqk_val){
                    if(!$every_wx_media_jczqk_val['titlename']) {
                        $every_wx_media_data[] = [
                            strval($every_wx_media_jczqk_key+1),
                            $every_wx_media_jczqk_val['tmedia_name'],
                            $every_wx_media_jczqk_val['fad_times'],
                            $every_wx_media_jczqk_val['fad_illegal_times'],
                            $every_wx_media_jczqk_val['times_illegal_rate'].'%',
                            $every_wx_media_jczqk_val['ck_count']?$every_wx_media_jczqk_val['ck_count']:'0',
                            $every_wx_media_jczqk_val['ckl']?$every_wx_media_jczqk_val['ckl']:'0.00'.'%',
                            $every_wx_media_jczqk_val['cl_count']?$every_wx_media_jczqk_val['cl_count']:'0',
                            $every_wx_media_jczqk_val['cll']?$every_wx_media_jczqk_val['cll']:'0.00'.'%'
                        ];
                    }
                }

                $every_wx_media_data = to_string($every_wx_media_data);*/


//定义数据数组
        $data = [
            'dotfilename' => 'ReportTemplate.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报告标题一",
            "text" => $user['regulatorname'].""
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报告标题二",
            "text" => $date_ym."广告监测通报"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "通报对象",
            "text" => $dq_name."工商行政管理局、市场监督管理部门："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报表通报描述",
            "text" => "（".$date_ymd."-".$date_md. "），". $user['regulatorname']."广告监测中心对".$dq_name."所属的媒介广告发布情况进行了监测，现将有关情况通报如下："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "一、监测范围及对象",
            "text" => "一、监测范围及对象"
        ];
        /*        $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "（一）传统媒体广告监测",
                    "text" => "（一）传统媒体广告监测"
                ];*/
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "传统媒体广告监测描述",
            "text" => "本期共对".$dq_name."所属的".$ct_media_count."家电视、广播、报纸等传统媒体广告发布情况进行了监测，其中电视媒体".$ct_media_tv_count."家、广播媒体".$ct_media_bc_count."家、报纸媒体".$ct_media_paper_count."家，广告监测范围为".$date_ymd."-".$date_md."（共".$days."天）"
        ];
        /*        $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "（二）互联网媒体广告监测",
                    "text" => "（二）互联网媒体广告监测"
                ];
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "互联网媒体广告监测描述",
                    "text" => "本期共对".$dq_name."所属的".$pc_media_count."家主要PC门户网及".$gzh_media_count."个微信公众号媒体，广告监测范围为".$date_ymd."-".$date_md."（共".$days."天）"
                ];*/
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "二、监测总体情况",
            "text" => "二、监测总体情况"
        ];
        /*        $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "（一）传统媒体广告监测情况",
                    "text" => "（一）传统媒体广告监测情况"
                ];*/
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "传统媒体广告监测情况描述",
            "text" => "本期共监测到电视、广播、报纸的各类广告".$ct_fad_times."条次，涉嫌违法广告".$ct_fad_illegal_times."条次，条次违法率为".round(($ct_fad_illegal_times/$ct_fad_times)*100,2)."％。"
        ];
        /*        $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "（二）互联网媒体广告监测情况",
                    "text" => "（二）互联网媒体广告监测情况"
                ];
                if(!empty($every_media_data[3])){
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "互联网媒体广告监测情况描述",
                        "text" => "本期共监测到互联网广告".$every_media_data[3][1]."条次，其中涉嫌违法广告".$every_media_data[3][2]."条次，违法率".$every_media_data[3][3]."。"
                    ];
                }else{
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "互联网媒体广告监测情况描述",
                        "text" => "本期暂未监测到互联网广告。"
                    ];
                }*/

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表1各类媒体广告监测统计情况标题",
            "text" => "表1各类媒体广告监测统计情况"
        ];
        $every_media_data = array_values($every_media_data);
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表1各类媒体广告监测统计情况列表含时长查处情况",
            "data" => $every_media_data
        ];
        array_pop($every_media_data);
        $every_media_datas = $this->create_chart_data($every_media_data,0,1,2,3);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告监测情况条状图",
            "data" => $every_media_datas[0]
        ];
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告发布量饼状图",
            "data" => $every_media_datas[1]
        ];
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告违法发布量饼状图",
            "data" => $every_media_datas[2]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "三、传统媒体广告监测情况",
            "text" => "三、传统媒体广告监测情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（一）广告总体监测情况",
            "text" => "（一）广告总体监测情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "广告总体监测情况描述",
            "text" => "本期监测数据显示，发布涉嫌违法广告条次违法率最为严重的".$every_region_num."个".$xj_name."依次为：".implode('、',$every_region_name)."。"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表2本期各地区发布涉嫌违法广告情况标题",
            "text" => "表2 本期各地区发布涉嫌违法广告情况"
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表2本期各地区发布涉嫌违法广告情况列表含时长查处情况",
            "data" => $every_region_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表2注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];

        $every_region_data_chart = $this->create_chart_data($every_region_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告监测情况条状图",
            "data" => $every_region_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告发布量饼状图",
            "data" => $every_region_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告违法发布量饼状图",
            "data" => $every_region_data_chart[2]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表3本期各大类违法广告发布总情况标题",
            "text" => '表3本期各大类违法广告发布总情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表3本期各大类违法广告发布总情况列表含时长查处情况",
            "data" => $every_adclass_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表3注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];
        $every_adclass_data_chart = $this->create_chart_data($every_adclass_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告监测情况列条状图",
            "data" => $every_adclass_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告发布量饼状图",
            "data" => $every_adclass_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告违法发布量饼状图",
            "data" => $every_adclass_data_chart[2]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "各类媒体广告发布情况描述",
            "text" => '涉嫌违法广告类别主要集中在'.implode('、',$every_adclass_name).'，其中'.implode("、",$every_adclass_rate_des).'。'
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（二）电视发布涉嫌违法广告情况",
            "text" => '（二）电视发布涉嫌违法广告情况'
        ];
        if(!empty($every_tv_region_data)){
            if($every_tv_region_num > 0){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "电视发布涉嫌违法广告情况描述",
                    "text" => '本期电视发布涉嫌违法广告条次违法率最为严重的'.$every_tv_region_num.'个地区依次为：'.implode('、',$every_tv_region_name).'。'
                ];
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "电视发布涉嫌违法广告情况描述",
                    "text" => '本期电视暂未发现违法广告。'
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况标题",
                "text" => '表4 电视发布涉嫌广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况列表带查处情况",
                "data" => $every_tv_region_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表4注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];
            $every_tv_region_data_chart = $this->create_chart_data($every_tv_region_data);
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "电视广告监测情况条状图",
                "data" => $every_tv_region_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "电视广告发布量饼状图",
                "data" => $every_tv_region_data_chart[1]
            ];
            if($every_tv_region_num > 0){
                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "电视广告违法发布量饼状图",
                    "data" => $every_tv_region_data_chart[2]
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表5电视各广告类型发布涉嫌违法广告情况标题",
                "text" => '表5 电视各广告类型发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况列表带查处情况",
                "data" => $every_tv_adclass_data
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表5注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];

            $every_tv_adclass_data_chart = $this->create_chart_data($every_tv_adclass_data);
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "电视各广告类型广告监测情况条状图",
                "data" => $every_tv_adclass_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "电视各广告类型广告发布量饼状图",
                "data" => $every_tv_adclass_data_chart[1]
            ];
            if($every_tv_region_num > 0){
                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "电视各广告类型广告违法发布量饼状图",
                    "data" => $every_tv_adclass_data_chart[2]
                ];
            }
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "电视发布涉嫌违法广告情况描述",
                "text" => '本期电视暂未监测到广告。'
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（三）广播发布涉嫌违法广告情况",
            "text" => '（三）广播发布涉嫌违法广告情况'
        ];
        if(!empty($every_tb_region_data)){
            if($every_tb_region_num > 0){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "广播发布涉嫌违法广告情况描述",
                    "text" => '本期广播发布涉嫌违法广告条次违法率最为严重的'.$every_tb_region_num.'个地区依次为：'.implode('、',$every_tb_region_name).'。'
                ];
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "广播发布涉嫌违法广告情况描述",
                    "text" => '本期广播暂未发现违法广告。'
                ];
            }
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表6广播发布涉嫌违法广告情况标题",
                "text" => '表6 广播发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况列表带查处情况",
                "data" => $every_tb_region_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表6注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];

            $every_tb_region_data_chart = $this->create_chart_data($every_tb_region_data);
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广播广告监测情况条状图",
                "data" => $every_tb_region_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广播广告发布量饼状图",
                "data" => $every_tb_region_data_chart[1]
            ];
            if($every_tb_region_num > 0){

                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "广播广告违法发布量饼状图",
                    "data" => $every_tb_region_data_chart[2]
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表7广播各广告类别发布涉嫌违法广告情况标题",
                "text" => '表7 广播各广告类别发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况列表带查处情况",
                "data" => $every_tb_adclass_data
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表7注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];

            $every_tb_adclass_data_chart = $this->create_chart_data($every_tb_adclass_data);

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广播各广告类型广告监测情况条状图",
                "data" => $every_tb_adclass_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广播各广告类型广告发布量饼状图",
                "data" => $every_tb_adclass_data_chart[1]
            ];
            if($every_tb_region_num > 0){

                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "广播各广告类型广告违法发布量饼状图",
                    "data" => $every_tb_adclass_data_chart[2]
                ];
            }
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "广播发布涉嫌违法广告情况描述",
                "text" => '本期广播暂未监测到广告。'
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（四）报纸发布涉嫌违法广告情况",
            "text" => '（四）报纸发布涉嫌违法广告情况'
        ];
        if(!empty($every_tp_region_data)){
            if($every_tp_region_num > 0){
                $every_tp_region_str = '本期报纸发布涉嫌违法广告条次违法率最为严重的'.$every_tp_region_num.'个地区依次为：'.implode('、',$every_tp_region_name).'。';
            }else{
                $every_tp_region_str = '本期各地区报纸暂未发布违法广告。';
            }//1119e
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "报纸发布涉嫌违法广告情况描述",
                "text" => $every_tp_region_str
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表8报纸发布涉嫌违法广告情况标题",
                "text" => '表8 报纸发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表8报纸发布涉嫌违法广告情况列表带查处情况",
                "data" => $every_tp_region_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表8注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];
            $every_tp_region_data_chart = $this->create_chart_data($every_tp_region_data);

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "报纸广告监测情况条状图",
                "data" => $every_tp_region_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "报纸广告发布量饼状图",
                "data" => $every_tp_region_data_chart[1]
            ];
            if($every_tp_region_num > 0){

                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "报纸广告违法发布量饼状图",
                    "data" => $every_tp_region_data_chart[2]
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表9报纸各广告类型发布涉嫌违法广告情况标题",
                "text" => '表9 报纸各广告类型发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表9报纸各广告类型发布涉嫌违法广告情况列表带查处情况",
                "data" => $every_tp_adclass_data
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表9注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];
            $every_tp_adclass_data_chart = $this->create_chart_data($every_tp_adclass_data);

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "报纸各广告类型广告监测情况条状图",
                "data" => $every_tp_adclass_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "报纸各广告类型广告发布量饼状图",
                "data" => $every_tp_adclass_data_chart[1]
            ];
            if($every_tp_region_num > 0){

                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "报纸各广告类型广告违法发布量饼状图",
                    "data" => $every_tp_adclass_data_chart[2]
                ];
            }
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "报纸发布涉嫌违法广告情况描述",
                "text" => '本期报纸暂未监测到广告'
            ];
        }



        /*        $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "四、互联网广告监测情况",
                    "text" => "四、互联网广告监测情况"
                ];
                if($every_media_data[3][1] != 0){
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "（一）总体发布情况",
                        "text" => "（一）总体发布情况"
                    ];
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "总体发布情况描述",
                        "text" => "本期共监测到互联网广告".$every_media_data[3][1]."条，其中违法广告".$every_media_data[3][2]."条，违法率".$every_media_data[3][3]."。微信公众号全部类别广告".$net_gzh_customize[1]['fad_times']."条。"
                    ];
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "PC门户网站广告发布情况标题",
                        "text" => "1、PC门户网站广告发布情况"
                    ];
                    $data['content'][] = [
                        "type" => "table",
                        "bookmark" => "PC门户网站广告发布情况列表带处理情况",
                        "data" => $every_pc_media_data
                    ];
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "PC门户网站广告发布情况列表注释",
                        "text" => "（注：按发布涉嫌违法广告情况严重程度从高到低排列）"
                    ];
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "PC门户网站广告类别发布情况标题",
                        "text" => "2、PC门户网站广告类别发布情况"
                    ];
                    $data['content'][] = [
                        "type" => "table",
                        "bookmark" => "PC门户网站广告类别发布情况列表带查处情况",
                        "data" => $every_pc_adclass_data
                    ];
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "PC门户网站广告类别发布情况列表注释",
                        "text" => "（注：按发布涉嫌违法广告情况严重程度从高到低排列）"
                    ];
                    $every_pc_adclass_name = implode('、',$every_pc_adclass_name);
                    if(!empty($every_pc_adclass_name)){
                        $data['content'][] = [
                            "type" => "text",
                            "bookmark" => "PC门户网站广告类别发布情况列表描述",
                            "text" => $every_pc_adclass_name."是本月PC网络违法广告主要类别。"
                        ];
                    }else{
                        $data['content'][] = [
                            "type" => "text",
                            "bookmark" => "PC门户网站广告类别发布情况列表描述",
                            "text" => "本月PC网络所有类别暂未发现违法广告。"
                        ];
                    }
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "微信公众号发布情况",
                        "text" => "3、微信公众号发布情况"
                    ];
                    $data['content'][] = [
                        "type" => "table",
                        "bookmark" => "微信公众号发布情况列表带查处情况",
                        "data" => $every_wx_media_data
                    ];
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "微信公众号发布情况列表注释",
                        "text" => "（注：按发布涉嫌违法广告情况严重程度从高到低排列）"
                    ];
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "（二）涉嫌违法发布情况",
                        "text" => "（二）涉嫌违法发布情况"
                    ];

                    if($net_pc_customize[1]['fad_illegal_times'] != 0){
                        $ybwf = '其中涉嫌违法广告'.$net_pc_customize[1]['fad_illegal_times'].'条，违法率'.$net_pc_customize[1]['times_illegal_rate'].'%。';

                        $pc_des = $ybwf;
                    }else{
                        $pc_des = '未发现涉嫌违法广告。';
                    }

                    if($net_gzh_customize[1]['fad_illegal_times'] != 0){
                        $ybwf = '其中涉嫌违法广告'.$net_gzh_customize[1]['fad_illegal_times'].'条，违法率'.$net_gzh_customize[1]['times_illegal_rate'].'%。';
                        $wx_des = $ybwf;
                    }else{
                        $wx_des = '未发现涉嫌违法广告。';
                    }
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "涉嫌违法发布情况描述一",
                        "text" => "本月次监测PC门户网站全部类别广告".$net_pc_customize[1]['fad_times']."条，".$pc_des."微信公众号全部类别广告".$net_gzh_customize[1]['fad_times']."条，".$wx_des
                    ];
                    $every_pc_adclass_des = '';
                    foreach ($every_pc_adclass_data as $every_pc_adclass_data_key=>$every_pc_adclass_data_val){
                        if($every_pc_adclass_data_val['3'] > 0){
                            $every_pc_adclass_name .= $every_pc_adclass_data_val[1].'、';
                            if($every_pc_adclass_data_key == 0){
                                $every_pc_adclass_des .= $every_pc_adclass_data_val[1].'广告占涉嫌违法广告总量的'.round(($every_pc_adclass_data_val[3]/$net_pc_customize[1]['fad_illegal_times'])*100,2).'％、';
                            }else{
                                $every_pc_adclass_des .= $every_pc_adclass_data_val[1].'广告占'.round(($every_pc_adclass_data_val[3]/$net_pc_customize[1]['fad_illegal_times'])*100,2).'％、';
                            }
                        }
                    }
                    $every_pc_media_des = '';
                    $every_pc_media_name = [];
                    foreach ($every_pc_media_data as $every_pc_media_data_key=>$every_pc_media_data_val){
                        if($every_pc_media_data_val['3'] > 0){
                            $every_pc_media_name[] = $every_pc_media_data_val[1];
                            if($every_pc_media_data_key == 0){
                                $every_pc_media_des .= $every_pc_media_data_val[1].'发布的占涉嫌违法广告总量的'.round(($every_pc_media_data_val[3]/$net_pc_customize[1]['fad_illegal_times'])*100,2).'％、';
                            }else{
                                $every_pc_media_des .= $every_pc_media_data_val[1].'占'.round(($every_pc_media_data_val[3]/$net_pc_customize[1]['fad_illegal_times'])*100,2).'％、';
                            }
                        }else{
                            $not_illegal_media_num++;
                            $not_illegal_media_name[] = $every_pc_media_data_val[1];
                        }
                    }
                    if($every_pc_adclass_des == ''){
                        $every_pc_adclass_des = '全部类别下无违法广告';
                    }
                    if($every_pc_media_des == ''){
                        $every_pc_media_des = '所有PC门户网站未发布违法广告';
                    }
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "涉嫌违法发布情况描述二",
                        "text" => "其中PC门户网站违法违法分布情况如下：".$every_pc_adclass_des.";".$every_pc_media_des."。"
                    ];
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "涉嫌违法发布情况描述三",
                        "text" => $not_illegal_media_num."家网站本次监测没有发现违法广告，分别是：".implode('、',$not_illegal_media_name)."。"
                    ];
                    if(!empty($every_pc_media_name)){
                        $data['content'][] = [
                            "type" => "text",
                            "bookmark" => "涉嫌违法发布情况描述四",
                            "text" => "PC门户网站中，".implode('、',$every_pc_media_name)."为主要的违法广告发布网站。"
                        ];
                        foreach ($every_pc_media_data as $every_pc_media_data_key=>$every_pc_media_data_val){
                            array_splice($every_pc_media_data[$every_pc_media_data_key],0,1);
                            array_splice($every_pc_media_data[$every_pc_media_data_key],1,1);
                            array_splice($every_pc_media_data[$every_pc_media_data_key],2,1);
                            array_splice($every_pc_media_data[$every_pc_media_data_key],2,1);
                            array_splice($every_pc_media_data[$every_pc_media_data_key],2,1);
                        }
                        $data['content'][] = [
                            "type" => "chart",
                            "bookmark" => "PC网站违法发布量饼状图",
                            "data" => $every_pc_media_data
                        ];
                    }


                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "互联网主要违法情形",
                        "text" => "1、互联网主要违法情形"
                    ];
                    $every_pc_illegal_situation_data = [];
                    foreach ($pc_illegal_situation as $pc_illegal_situation_key => $pc_illegal_situation_val){
                        $every_pc_illegal_situation_data[] = [
                            strval($pc_illegal_situation_key+1),
                            $pc_illegal_situation_val['fillegalcontent'],
                            $pc_illegal_situation_val['illegal_count']
                        ];
                    }
                    if(!empty($every_pc_illegal_situation_data)){
                        $data['content'][] = [
                            "type" => "table",
                            "bookmark" => "PC门户网站主要违法情形列表",
                            "data" => $every_pc_illegal_situation_data
                        ];
                    }else{
                        $data['content'][] = [
                            "type" => "text",
                            "bookmark" => "微信公众号违法广告发布情况描述",
                            "text" => '本期互联网暂未发现违法情形'
                        ];
                    }

                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "微信公众号违法广告发布情况",
                        "text" => "微信公众号违法广告发布情况"
                    ];
                    $every_wx_media_des = '';
                    foreach ($every_wx_media_data as $every_wx_media_data_key=>$every_wx_media_data_val){
                        if($every_wx_media_data_val['3'] > 0){
                            $every_wx_media_name .= $every_wx_media_data_val[1].'、';
                            if($every_wx_media_data_key == 0){
                                $every_wx_media_des .= $every_wx_media_data_val[1].'发布的占涉嫌违法广告总量的'.round(($every_wx_media_data_val[3]/$net_gzh_customize[1]['fad_illegal_times'])*100,2).'％、';
                            }else{
                                $every_wx_media_des .= $every_wx_media_data_val[1].'占'.round(($every_wx_media_data_val[3]/$net_gzh_customize[1]['fad_illegal_times'])*100,2).'％、';
                            }
                        }else{
                            $not_illegal_wx_media_num++;
                            $not_illegal_wx_media_name .= $every_wx_media_data_val[1].'、';
                        }
                    }
                    if($every_wx_media_des == ''){
                        $every_wx_media_des = '本次监测微信公众号没有发布涉嫌违法广告数据。';
                    }
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "微信公众号违法广告发布情况描述",
                        "text" => $every_wx_media_des
                    ];
                }else{
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "微信公众号违法广告发布情况描述",
                        "text" => '本期未检测到互联网数据'
                    ];
                }*/

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "落款",
            "text" => $user['regulatorname'].""
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "时间",
            "text" => $date_end_ymd
        ];

        $report_data = json_encode($data);
        //echo $report_data;exit;//20190103

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $system_num = getconfig('system_num');

//将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].date('Y年m月',strtotime($month)).'月报';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 30;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }


    /**
     * 生成天津月报word 20181218  by yjn    create_sjz_month_report
     */

    public function create_month(){

        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport = New StatisticalModel();//实例化数据统计模型
        $StatisticalReport1 = New StatisticalReportModel();//实例化数据统计模型
        $month = I('daytime','2019-06');//接收时间  TODO::时间改
        $report_start_date = strtotime($month);//指定月份月初时间戳
        $report_end_date = strtotime($month.'-'.date('t', strtotime($month)).' 23:59:59');
        if($report_end_date > time()){
            $report_end_date = time();

        }

        $date_ym = date('Y年m月',$report_start_date);//年月字符
        $user = session('regulatorpersonInfo');//获取用户信息
        $zxs = ['110000','120000','310000','500000'];//直辖市
        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级

        //文字描述匹配
        switch ($re_level){
            case 2:
            case 3:
            case 4:
                $regionid_str = substr($user['regionid'],0,4);
                $dq_name = '全市';
                $xj_name = '区县';
                $self_name = '市属';
                break;
            case 1:
                $regionid_str = substr($user['regionid'],0,2);
                if(!in_array($user['regionid'],$zxs)){
                    $dq_name = '全省';
                    $xj_name = '市级';
                    $self_name = '省属';
                }else{
                    $dq_name = '全市';
                    $xj_name = '区级';
                    $self_name = '市属';
                }
                break;
            case 5:
                $regionid_str = $user['regionid'];
                $dq_name = '全县';
                break;
        }

        $date_table = date('Y',strtotime($month)).'_'.$user['regionid'];//组合表

        $date_ymd = date('Y年m月d日',$report_start_date);//开始年月字符
        $date_end_ymd = date('Y年m月d日',$report_end_date);//结束时间
        $date_md = date('m月d日',$report_end_date);//结束月日字符

        $days = round(($report_end_date - $report_start_date + 1)/86400);//计算天数

        $owner_media_ids = $this->get_owner_media_ids(5,'','13');//获取权限内媒体

        $ct_media_tv_count = $StatisticalReport->ct_media_count('tv',$owner_media_ids);//电视媒体数量

        $ct_media_bc_count = $StatisticalReport->ct_media_count('bc',$owner_media_ids);//广播媒体数量

        $ct_media_paper_count = $StatisticalReport->ct_media_count('paper',$owner_media_ids);//报纸媒体数量

        $ct_media_count = $ct_media_tv_count + $ct_media_bc_count + $ct_media_paper_count;

        $pc_media_count = $StatisticalReport->net_media_counts($regionid_str,'1301');//pc媒体数量
        $gzh_media_count = $StatisticalReport->net_media_counts($regionid_str,'1303');//公众号数量

        $pc_illegal_situation = $StatisticalReport->net_illegal_situation_list(date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));//pc违法情形列表
        $pc_medias = $StatisticalReport->net_media_type_ids('1301');
        $wx_medias = $StatisticalReport->net_media_type_ids('1303');

        $ct_jczqk = $StatisticalReport->report_ad_monitor('fmedia_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));

        $ct_fad_times = 0;//定义传统媒体发布广告条次
        $ct_fad_illegal_times = 0;//定义传统媒体发布违法广告条次
        $every_media_data = [];//定义媒体发布监测情况数组

        //循环计算赋值  传统媒体监测总情况
        foreach ($ct_jczqk as $ct_jczqk_key => $ct_jczqk_val){
            $every_media_data_array = [];
            if(!$ct_jczqk_val['titlename']){
                if($ct_jczqk_val['fmedia_class_code'] != '13'){
                    $ct_fad_times += $ct_jczqk_val['fad_times'];
                    $ct_fad_illegal_times += $ct_jczqk_val['fad_illegal_times'];
                    $ct_fad_play_len += $ct_jczqk_val['fad_play_len'];
                    $ct_fad_illegal_play_len += $ct_jczqk_val['fad_illegal_play_len'];
                }
                $fmedia_class_code = $ct_jczqk_val['fmedia_class_code'];
                $tmediaclass_name = $ct_jczqk_val['tmediaclass_name'];
            }else{
                $fmedia_class_code = 'all';
                $tmediaclass_name = '合计';
            }
            $every_media_data_array = [
                $tmediaclass_name,
                $ct_jczqk_val['fad_times'],
                $ct_jczqk_val['fad_illegal_times'],
                $ct_jczqk_val['times_illegal_rate'].'%',
                $ct_jczqk_val['fad_play_len'],
                $ct_jczqk_val['fad_illegal_play_len'],
                $ct_jczqk_val['lens_illegal_rate']?$ct_jczqk_val['lens_illegal_rate'].'%':'0.00'.'%',
                $ct_jczqk_val['ck_count']?$ct_jczqk_val['ck_count']:'0',
                $ct_jczqk_val['ckl']?$ct_jczqk_val['ckl']:'0.00'.'%',
                $ct_jczqk_val['cl_count']?$ct_jczqk_val['cl_count']:'0',
                $ct_jczqk_val['cll']?$ct_jczqk_val['cll']:'0.00'.'%'
            ];
            switch ($fmedia_class_code){
                case '01':
                    $every_media_data[0] = $every_media_data_array;
                    break;
                case '02':
                    $every_media_data[1] = $every_media_data_array;
                    break;
                case '03':
                    $every_media_data[2] = $every_media_data_array;
                    break;
                case '13':
                    //$every_media_data[3] = $every_media_data_array;
                    break;
                case 'all':
                    $every_media_data[3] = $every_media_data_array;
                    break;
            }

        }
        $every_media_data = [
            $every_media_data[0],
            $every_media_data[1],
            $every_media_data[2],
            //$every_media_data[3],
            $every_media_data[3],
        ];
        $every_media_data = to_string($every_media_data);
        foreach ($every_media_data as $every_media_k=>$every_media_v){
            if(empty($every_media_v)){
                unset($every_media_data[$every_media_k]);
            }
        }


        /*
        * @各地域监测总情况
        *
        **/
        $every_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));


        $every_region_data = [];//定义各地区发布监测情况数组
        $ext_reg_ids = [];
        $every_region_num = 0;

        $reg_ids = M("tregion")->where([
            'fpid' => 120000,
            'flevel' => 5
        ])->getField("fid",true);
        //循环赋值
        foreach ($every_region_jczqk as $every_region_jczqk_key => $every_region_jczqk_val){


            if(!$every_region_jczqk_val['titlename']){
                if($every_region_jczqk_val['fregionid'] == $user['regionid']){
                    continue;
                    $every_region_jczqk_tregion_name = $self_name;
                }else{
                    $ext_reg_ids[] = $every_region_jczqk_val['fregionid'];
                    $every_region_jczqk_tregion_name = $every_region_jczqk_val['tregion_name'];
                }
                if($every_region_jczqk_val['fad_illegal_times'] != 0){
                    $every_region_num++;
                    $every_region_name[] = $every_region_jczqk_tregion_name;
                }
                $every_region_data[] = [
                    strval($every_region_jczqk_key+1),
                    $every_region_jczqk_tregion_name,
                    $every_region_jczqk_val['fad_times'],
                    $every_region_jczqk_val['fad_illegal_times'],
                    $every_region_jczqk_val['times_illegal_rate'].'%',
                    $every_region_jczqk_val['fad_play_len'],
                    $every_region_jczqk_val['fad_illegal_play_len'],
                    $every_region_jczqk_val['lens_illegal_rate']?$every_region_jczqk_val['lens_illegal_rate'].'%':'0%',
                    $every_region_jczqk_val['ck_count']?$every_region_jczqk_val['ck_count']:'0',
                    $every_region_jczqk_val['ckl']?$every_region_jczqk_val['ckl']:'0%',
                    $every_region_jczqk_val['cl_count']?$every_region_jczqk_val['cl_count']:'0',
                    $every_region_jczqk_val['cll']?$every_region_jczqk_val['cll']:'0%'
                ];
            }

        }

        $no_data_ids = array_diff($reg_ids,$ext_reg_ids);
        $has_count = count($every_region_data);
        if(!empty($no_data_ids)){
            $reg_names = M("tregion")->where([

                'fid' => ['IN',$no_data_ids],
                'flevel' => 5

            ])->getField("fname1",true);
            foreach ($reg_names as $rename){
                $every_region_data[] = [
                    strval($has_count++),
                    $rename,
                    "0",
                    "0",
                    "0%",
                    "0",
                    "0",
                    "0%",
                    "0",
                    "0%",
                    "0",
                    "0%"
                ];
            }

        }

        $every_region_data = to_string($every_region_data);

        /*
        * @各广告类型监测总情况
        *
        **/
        $every_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));

        $every_adclass_data = [];//定义各大类发布监测情况数组
        $adclass_illegal_times = 0;//各违法地域数量
        //循环赋值
        foreach ($every_adclass_jczqk as $every_adclass_jczqk_key => $every_adclass_jczqk_val){
            if(!$every_adclass_jczqk_val['titlename']){
                if($every_adclass_jczqk_val['fad_illegal_times'] > 0){
                    $adclass_illegal_times += $every_adclass_jczqk_val['fad_illegal_times'];
                }
                $every_adclass_data[] = [
                    strval($every_adclass_jczqk_key+1),
                    $every_adclass_jczqk_val['tadclass_name'],
                    $every_adclass_jczqk_val['fad_times'],
                    $every_adclass_jczqk_val['fad_illegal_times'],
                    $every_adclass_jczqk_val['times_illegal_rate'].'%',
                    $every_adclass_jczqk_val['fad_play_len'],
                    $every_adclass_jczqk_val['fad_illegal_play_len'],
                    $every_adclass_jczqk_val['lens_illegal_rate']?$every_adclass_jczqk_val['lens_illegal_rate'].'%':'0.00'.'%',
                    $every_adclass_jczqk_val['ck_count']?$every_adclass_jczqk_val['ck_count']:'0',
                    $every_adclass_jczqk_val['ckl']?$every_adclass_jczqk_val['ckl']:'0.00'.'%',
                    $every_adclass_jczqk_val['cl_count']?$every_adclass_jczqk_val['cl_count']:'0',
                    $every_adclass_jczqk_val['cll']?$every_adclass_jczqk_val['cll']:'0.00'.'%'
                ];
            }
        }
        $every_adclass_data = to_string($every_adclass_data);


        foreach ($every_adclass_data as $every_adclass_data_key=>$every_adclass_data_val){
            if($every_adclass_data_val[3] > 0){
                $every_adclass_name[] = $every_adclass_data_val[1];
                if($every_adclass_data_key == 0){
                    $every_adclass_rate_des[] = $every_adclass_data_val[1].'广告占涉嫌违法广告总量的'.round(($every_adclass_data_val[3]/$adclass_illegal_times)*100,2).'％';
                }else{
                    $every_adclass_rate_des[] .= $every_adclass_data_val[1].'广告占'.round(($every_adclass_data_val[3]/$adclass_illegal_times)*100,2).'％';
                }
            }
        }

        /*
        * @电视各地域监测总情况
        *
        **/
        $every_tv_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','01');

        $every_tv_region_data = [];//定义各地区发布监测情况数组
        $every_tv_region_num = 0;//电视各违法地域数量
        //循环赋值
        foreach ($every_tv_region_jczqk as $every_tv_region_jczqk_key => $every_tv_region_jczqk_val){
            if(!$every_tv_region_jczqk_val['titlename']){
                if($every_tv_region_jczqk_val['fregionid'] == $user['regionid']){
                    $every_tv_region_jczqk_tregion_name = $self_name;
                }else{
                    $every_tv_region_jczqk_tregion_name = $every_tv_region_jczqk_val['tregion_name'];
                }
                if($every_tv_region_jczqk_val['fad_illegal_times'] != 0){
                    $every_tv_region_num++;
                    $every_tv_region_name[] = $every_tv_region_jczqk_tregion_name;
                }
                $every_tv_region_data[] = [
                    strval($every_tv_region_jczqk_key+1),
                    $every_tv_region_jczqk_tregion_name,
                    $every_tv_region_jczqk_val['fad_times'],
                    $every_tv_region_jczqk_val['fad_illegal_times'],
                    $every_tv_region_jczqk_val['times_illegal_rate'].'%',
                    $every_tv_region_jczqk_val['fad_play_len'],
                    $every_tv_region_jczqk_val['fad_illegal_play_len'],
                    $every_tv_region_jczqk_val['lens_illegal_rate'].'%',
                    $every_tv_region_jczqk_val['ck_count']?$every_tv_region_jczqk_val['ck_count']:'0',
                    $every_tv_region_jczqk_val['ckl']?$every_tv_region_jczqk_val['ckl']:'0.00'.'%',
                    $every_tv_region_jczqk_val['cl_count']?$every_tv_region_jczqk_val['cl_count']:'0',
                    $every_tv_region_jczqk_val['cll']?$every_tv_region_jczqk_val['cll']:'0.00'.'%'
                ];
            }
        }
        $every_tv_region_data = to_string($every_tv_region_data);


        /*
        * @电视各广告类型监测总情况
        *
        **/
        $every_tv_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','01');

        $every_tv_adclass_data = [];//定义各地区发布监测情况数组
        //循环赋值
        foreach ($every_tv_adclass_jczqk as $every_tv_adclass_jczqk_key => $every_tv_adclass_jczqk_val){

            if(!$every_tv_adclass_jczqk_val['titlename']){
                $every_tv_adclass_data[] = [
                    strval($every_tv_adclass_jczqk_key+1),
                    $every_tv_adclass_jczqk_val['tadclass_name'],
                    $every_tv_adclass_jczqk_val['fad_times'],
                    $every_tv_adclass_jczqk_val['fad_illegal_times'],
                    $every_tv_adclass_jczqk_val['times_illegal_rate'].'%',
                    $every_tv_adclass_jczqk_val['fad_play_len'],
                    $every_tv_adclass_jczqk_val['fad_illegal_play_len'],
                    $every_tv_adclass_jczqk_val['lens_illegal_rate'].'%',
                    $every_tv_adclass_jczqk_val['ck_count']?$every_tv_adclass_jczqk_val['ck_count']:'0',
                    $every_tv_adclass_jczqk_val['ckl']?$every_tv_adclass_jczqk_val['ckl']:'0.00'.'%',
                    $every_tv_adclass_jczqk_val['cl_count']?$every_tv_adclass_jczqk_val['cl_count']:'0',
                    $every_tv_adclass_jczqk_val['cll']?$every_tv_adclass_jczqk_val['cll']:'0.00'.'%'
                ];
            }

        }
        $every_tv_adclass_data = to_string($every_tv_adclass_data);

        /*
        * @广播各地域监测总情况
        *
        **/
        $every_tb_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','02');
        //$every_tb_region_jczqk1 = $StatisticalReport1->ad_monitor_customize($date_table,$report_start_date,$report_end_date,$owner_media_ids,'02','','fad_illegal_times_rate','fregionid',false,1,get_check_dates(),true);//广播各地域监测总情况
        $every_tb_region_data = [];//定义各地区发布监测情况数组
        $every_tb_region_num = 0;//广播各违法地域数量
        //循环赋值
        foreach ($every_tb_region_jczqk as $every_tb_region_jczqk_key => $every_tb_region_jczqk_val){
            if(!$every_tb_region_jczqk_val['titlename']){
                if($every_tb_region_jczqk_val['fregionid'] == $user['regionid']){
                    $every_tb_region_jczqk_tregion_name = $self_name;
                }else{
                    $every_tb_region_jczqk_tregion_name = $every_tb_region_jczqk_val['tregion_name'];
                }

                if($every_tb_region_jczqk_val['fad_illegal_times'] != 0){
                    $every_tb_region_num++;
                    $every_tb_region_name[] = $every_tb_region_jczqk_tregion_name;
                }

                $every_tb_region_data[] = [
                    strval($every_tb_region_jczqk_key+1),
                    $every_tb_region_jczqk_tregion_name,
                    $every_tb_region_jczqk_val['fad_times'],
                    $every_tb_region_jczqk_val['fad_illegal_times'],
                    $every_tb_region_jczqk_val['times_illegal_rate'].'%',
                    $every_tb_region_jczqk_val['fad_play_len'],
                    $every_tb_region_jczqk_val['fad_illegal_play_len'],
                    $every_tb_region_jczqk_val['lens_illegal_rate'].'%',
                    $every_tb_region_jczqk_val['ck_count']?$every_tb_region_jczqk_val['ck_count']:'0',
                    $every_tb_region_jczqk_val['ckl']?$every_tb_region_jczqk_val['ckl']:'0.00'.'%',
                    $every_tb_region_jczqk_val['cl_count']?$every_tb_region_jczqk_val['cl_count']:'0',
                    $every_tb_region_jczqk_val['cll']?$every_tb_region_jczqk_val['cll']:'0.00'.'%'
                ];
            }
        }
        $every_tb_region_data = to_string($every_tb_region_data);

        /*
        * @广播各广告类型监测总情况
        *
        **/
        $every_tb_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','02');
        $every_tb_adclass_data = [];//定义各地区发布监测情况数组

        //循环赋值
        foreach ($every_tb_adclass_jczqk as $every_tb_adclass_jczqk_key => $every_tb_adclass_jczqk_val){

            if(!$every_tb_adclass_jczqk_val['titlename']) {
                $every_tb_adclass_data[] = [
                    strval($every_tb_adclass_jczqk_key+1),
                    $every_tb_adclass_jczqk_val['tadclass_name'],
                    $every_tb_adclass_jczqk_val['fad_times'],
                    $every_tb_adclass_jczqk_val['fad_illegal_times'],
                    $every_tb_adclass_jczqk_val['times_illegal_rate'].'%',
                    $every_tb_adclass_jczqk_val['fad_play_len'],
                    $every_tb_adclass_jczqk_val['fad_illegal_play_len'],
                    $every_tb_adclass_jczqk_val['lens_illegal_rate'].'%',
                    $every_tb_adclass_jczqk_val['ck_count']?$every_tb_adclass_jczqk_val['ck_count']:'0',
                    $every_tb_adclass_jczqk_val['ckl']?$every_tb_adclass_jczqk_val['ckl']:'0.00'.'%',
                    $every_tb_adclass_jczqk_val['cl_count']?$every_tb_adclass_jczqk_val['cl_count']:'0',
                    $every_tb_adclass_jczqk_val['cll']?$every_tb_adclass_jczqk_val['cll']:'0.00'.'%'
                ];
            }

        }
        $every_tb_adclass_data = to_string($every_tb_adclass_data);

        /*
        * @报纸各地域监测总情况
        *
        **/
        $every_tp_region_jczqk = $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','03');
        $every_tp_region_data = [];//定义各地区发布监测情况数组
        $every_tp_region_num = 0;//报纸各违法地域数量

        //循环赋值
        foreach ($every_tp_region_jczqk as $every_tp_region_jczqk_key => $every_tp_region_jczqk_val){

            if(!$every_tp_region_jczqk_val['titlename']) {

                if($every_tp_region_jczqk_val['fregionid'] == $user['regionid']){
                    $every_tp_region_jczqk_tregion_name = $self_name;
                }else{
                    $every_tp_region_jczqk_tregion_name = $every_tp_region_jczqk_val['tregion_name'];
                }
                if($every_tp_region_jczqk_val['fad_illegal_times'] != 0){
                    $every_tp_region_num++;
                    $every_tp_region_name[] = $every_tp_region_jczqk_tregion_name;
                }
                $every_tp_region_data[] = [
                    strval($every_tp_region_jczqk_key+1),
                    $every_tp_region_jczqk_tregion_name,
                    $every_tp_region_jczqk_val['fad_times'],
                    $every_tp_region_jczqk_val['fad_illegal_times'],
                    $every_tp_region_jczqk_val['times_illegal_rate'].'%',
                    $every_tp_region_jczqk_val['ck_count']?$every_tp_region_jczqk_val['ck_count']:'0',
                    $every_tp_region_jczqk_val['ckl']?$every_tp_region_jczqk_val['ckl']:'0.00'.'%',
                    $every_tp_region_jczqk_val['cl_count']?$every_tp_region_jczqk_val['cl_count']:'0',
                    $every_tp_region_jczqk_val['cll']?$every_tp_region_jczqk_val['cll']:'0.00'.'%'
                ];
            }
        }
        $every_tp_region_data = to_string($every_tp_region_data);


        /*
        * @报纸各广告类型监测总情况
        *
        **/
        $every_tp_adclass_jczqk = $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date),'times_illegal_rate','03');
        $every_tp_adclass_data = [];//定义各地区发布监测情况数组
        //循环赋值
        foreach ($every_tp_adclass_jczqk as $every_tp_adclass_jczqk_key => $every_tp_adclass_jczqk_val){

            if(!$every_tp_adclass_jczqk_val['titlename']) {
                $every_tp_adclass_data[] = [
                    strval($every_tp_adclass_jczqk_key+1),
                    $every_tp_adclass_jczqk_val['tadclass_name'],
                    $every_tp_adclass_jczqk_val['fad_times'],
                    $every_tp_adclass_jczqk_val['fad_illegal_times'],
                    $every_tp_adclass_jczqk_val['times_illegal_rate'].'%',
                    $every_tp_adclass_jczqk_val['ck_count']?$every_tp_adclass_jczqk_val['ck_count']:'0',
                    $every_tp_adclass_jczqk_val['ckl']?$every_tp_adclass_jczqk_val['ckl']:'0.00'.'%',
                    $every_tp_adclass_jczqk_val['cl_count']?$every_tp_adclass_jczqk_val['cl_count']:'0',
                    $every_tp_adclass_jczqk_val['cll']?$every_tp_adclass_jczqk_val['cll']:'0.00'.'%'
                ];
            }
        }
        $every_tp_adclass_data = to_string($every_tp_adclass_data);

        /*
        * @PC各媒体监测总情况
        *
        **/
        $every_pc_media_data = [];

        $every_pc_media_jczqk = $StatisticalReport->report_ad_monitor('fmediaid',$pc_medias,$user['regionid'],date('Y-m-d',$report_start_date),date('Y-m-d',$report_end_date));
        foreach ($every_pc_media_jczqk as $every_pc_media_jczqk_key=>$every_pc_media_jczqk_val){
            if(!$every_pc_media_jczqk_val['titlename']) {
                $every_pc_media_data[] = [
                    strval($every_pc_media_jczqk_key+1),
                    $every_pc_media_jczqk_val['tmedia_name'],
                    $every_pc_media_jczqk_val['fad_times'],
                    $every_pc_media_jczqk_val['fad_illegal_times'],
                    $every_pc_media_jczqk_val['times_illegal_rate'].'%',
                    $every_pc_media_jczqk_val['ck_count']?$every_pc_media_jczqk_val['ck_count']:'0',
                    $every_pc_media_jczqk_val['ckl']?$every_pc_media_jczqk_val['ckl']:'0.00'.'%',
                    $every_pc_media_jczqk_val['cl_count']?$every_pc_media_jczqk_val['cl_count']:'0',
                    $every_pc_media_jczqk_val['cll']?$every_pc_media_jczqk_val['cll']:'0.00'.'%'
                ];
            }
        }

        $every_pc_media_data = to_string($every_pc_media_data);


        //定义数据数组
        $data = [
            'dotfilename' => 'ReportTemplate.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报告标题一",
            "text" => $user['regulatorname'].""
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报告标题二",
            "text" => $date_ym."广告监测通报"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "通报对象",
            "text" => $dq_name."各市场监督管理部门："
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "报表通报描述",
            "text" => "天津市市场监管委委托第三方监测机构对全市主要媒体".$date_ym."的广告发布情况进行了监测，现将有关情况通报如下："
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "一、监测范围及对象",
            "text" => "一、监测范围及对象"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "传统媒体广告监测描述",
            "text" => "本期共对全市各区".$ct_media_count."家电视、广播、报纸等传统媒体广告发布情况进行了监测，其中电视媒体".$ct_media_tv_count."家、广播媒体".$ct_media_bc_count."家、报纸媒体".$ct_media_paper_count."家，广告监测范围为".$date_ymd."-".$date_md."（共30天）"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "二、监测总体情况",
            "text" => "二、监测总体情况"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "传统媒体广告监测情况描述",
            "text" => "本期共监测到电视、广播、报纸的各类广告".$ct_fad_times."条次，涉嫌违法广告".$ct_fad_illegal_times."条次，条次违法率为".round(($ct_fad_illegal_times/$ct_fad_times)*100,2)."％。"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表1各类媒体广告监测统计情况标题",
            "text" => "表1各类媒体广告监测统计情况"
        ];
        $every_media_data = array_values($every_media_data);
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表1各类媒体广告监测统计情况列表含时长查处情况",
            "data" => $every_media_data
        ];
        array_pop($every_media_data);
        $every_media_datas = $this->create_chart_data($every_media_data,0,1,2,3);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告监测情况条状图",
            "data" => $every_media_datas[0]
        ];
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告发布量饼状图",
            "data" => $every_media_datas[1]
        ];
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各类媒体广告违法发布量饼状图",
            "data" => $every_media_datas[2]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "三、传统媒体广告监测情况",
            "text" => "三、传统媒体广告监测情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（一）广告总体监测情况",
            "text" => "（一）广告总体监测情况"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "广告总体监测情况描述",
            "text" => "本期监测数据显示，发布涉嫌违法广告条次违法率最为严重的".$every_region_num."个".$xj_name."依次为：".implode('、',$every_region_name)."。"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表2本期各地区发布涉嫌违法广告情况标题",
            "text" => "表2 本期各地区发布涉嫌违法广告情况"
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表2本期各地区发布涉嫌违法广告情况列表含时长查处情况",
            "data" => $every_region_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表2注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];

        $every_region_data_chart = $this->create_chart_data($every_region_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告监测情况条状图",
            "data" => $every_region_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告发布量饼状图",
            "data" => $every_region_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各地区广告违法发布量饼状图",
            "data" => $every_region_data_chart[2]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表3本期各大类违法广告发布总情况标题",
            "text" => '表3本期各大类违法广告发布总情况'
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "表3本期各大类违法广告发布总情况列表含时长查处情况",
            "data" => $every_adclass_data
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "表3注释",
            "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
        ];
        $every_adclass_data_chart = $this->create_chart_data($every_adclass_data);
        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告监测情况列条状图",
            "data" => $every_adclass_data_chart[0]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告发布量饼状图",
            "data" => $every_adclass_data_chart[1]
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "本期各大类广告违法发布量饼状图",
            "data" => $every_adclass_data_chart[2]
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "各类媒体广告发布情况描述",
            "text" => '涉嫌违法广告类别主要集中在'.implode('、',$every_adclass_name).'，其中'.implode("、",$every_adclass_rate_des).'。'
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（二）电视发布涉嫌违法广告情况",
            "text" => '（二）电视发布涉嫌违法广告情况'
        ];
        if(!empty($every_tv_region_data)){
            if($every_tv_region_num > 0){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "电视发布涉嫌违法广告情况描述",
                    "text" => '本期电视发布涉嫌违法广告条次违法率最为严重的'.$every_tv_region_num.'个地区依次为：'.implode('、',$every_tv_region_name).'。'
                ];
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "电视发布涉嫌违法广告情况描述",
                    "text" => '本期电视暂未发现违法广告。'
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况标题",
                "text" => '表4 电视发布涉嫌广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况列表带查处情况",
                "data" => $every_tv_region_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表4注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];
            $every_tv_region_data_chart = $this->create_chart_data($every_tv_region_data);
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "电视广告监测情况条状图",
                "data" => $every_tv_region_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "电视广告发布量饼状图",
                "data" => $every_tv_region_data_chart[1]
            ];
            if($every_tv_region_num > 0){
                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "电视广告违法发布量饼状图",
                    "data" => $every_tv_region_data_chart[2]
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表5电视各广告类型发布涉嫌违法广告情况标题",
                "text" => '表5 电视各广告类型发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况列表带查处情况",
                "data" => $every_tv_adclass_data
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表5注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];

            $every_tv_adclass_data_chart = $this->create_chart_data($every_tv_adclass_data);
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "电视各广告类型广告监测情况条状图",
                "data" => $every_tv_adclass_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "电视各广告类型广告发布量饼状图",
                "data" => $every_tv_adclass_data_chart[1]
            ];
            if($every_tv_region_num > 0){
                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "电视各广告类型广告违法发布量饼状图",
                    "data" => $every_tv_adclass_data_chart[2]
                ];
            }
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "电视发布涉嫌违法广告情况描述",
                "text" => '本期电视暂未监测到广告。'
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（三）广播发布涉嫌违法广告情况",
            "text" => '（三）广播发布涉嫌违法广告情况'
        ];
        if(!empty($every_tb_region_data)){
            if($every_tb_region_num > 0){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "广播发布涉嫌违法广告情况描述",
                    "text" => '本期广播发布涉嫌违法广告条次违法率最为严重的'.$every_tb_region_num.'个地区依次为：'.implode('、',$every_tb_region_name).'。'
                ];
            }else{
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "广播发布涉嫌违法广告情况描述",
                    "text" => '本期广播暂未发现违法广告。'
                ];
            }
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表6广播发布涉嫌违法广告情况标题",
                "text" => '表6 广播发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况列表带查处情况",
                "data" => $every_tb_region_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表6注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];

            $every_tb_region_data_chart = $this->create_chart_data($every_tb_region_data);
            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广播广告监测情况条状图",
                "data" => $every_tb_region_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广播广告发布量饼状图",
                "data" => $every_tb_region_data_chart[1]
            ];
            if($every_tb_region_num > 0){

                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "广播广告违法发布量饼状图",
                    "data" => $every_tb_region_data_chart[2]
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表7广播各广告类别发布涉嫌违法广告情况标题",
                "text" => '表7 广播各广告类别发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表4电视发布涉嫌严重违法广告情况列表带查处情况",
                "data" => $every_tb_adclass_data
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表7注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];

            $every_tb_adclass_data_chart = $this->create_chart_data($every_tb_adclass_data);

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广播各广告类型广告监测情况条状图",
                "data" => $every_tb_adclass_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "广播各广告类型广告发布量饼状图",
                "data" => $every_tb_adclass_data_chart[1]
            ];
            if($every_tb_region_num > 0){

                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "广播各广告类型广告违法发布量饼状图",
                    "data" => $every_tb_adclass_data_chart[2]
                ];
            }
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "广播发布涉嫌违法广告情况描述",
                "text" => '本期广播暂未监测到广告。'
            ];
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "（四）报纸发布涉嫌违法广告情况",
            "text" => '（四）报纸发布涉嫌违法广告情况'
        ];
        if(!empty($every_tp_region_data)){
            if($every_tp_region_num > 0){
                $every_tp_region_str = '本期报纸发布涉嫌违法广告条次违法率最为严重的'.$every_tp_region_num.'个地区依次为：'.implode('、',$every_tp_region_name).'。';
            }else{
                $every_tp_region_str = '本期各地区报纸暂未发布违法广告。';
            }//1119e
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "报纸发布涉嫌违法广告情况描述",
                "text" => $every_tp_region_str
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表8报纸发布涉嫌违法广告情况标题",
                "text" => '表8 报纸发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表8报纸发布涉嫌违法广告情况列表带查处情况",
                "data" => $every_tp_region_data
            ];
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表8注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];
            $every_tp_region_data_chart = $this->create_chart_data($every_tp_region_data);

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "报纸广告监测情况条状图",
                "data" => $every_tp_region_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "报纸广告发布量饼状图",
                "data" => $every_tp_region_data_chart[1]
            ];
            if($every_tp_region_num > 0){

                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "报纸广告违法发布量饼状图",
                    "data" => $every_tp_region_data_chart[2]
                ];
            }

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表9报纸各广告类型发布涉嫌违法广告情况标题",
                "text" => '表9 报纸各广告类型发布涉嫌违法广告情况'
            ];
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "表9报纸各广告类型发布涉嫌违法广告情况列表带查处情况",
                "data" => $every_tp_adclass_data
            ];

            $data['content'][] = [
                "type" => "text",
                "bookmark" => "表9注释",
                "text" => '（注：按发布涉嫌违法广告情况严重程度从高到低排列）'
            ];
            $every_tp_adclass_data_chart = $this->create_chart_data($every_tp_adclass_data);

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "报纸各广告类型广告监测情况条状图",
                "data" => $every_tp_adclass_data_chart[0]
            ];

            $data['content'][] = [
                "type" => "chart",
                "bookmark" => "报纸各广告类型广告发布量饼状图",
                "data" => $every_tp_adclass_data_chart[1]
            ];
            if($every_tp_region_num > 0){

                $data['content'][] = [
                    "type" => "chart",
                    "bookmark" => "报纸各广告类型广告违法发布量饼状图",
                    "data" => $every_tp_adclass_data_chart[2]
                ];
            }
        }else{
            $data['content'][] = [
                "type" => "text",
                "bookmark" => "报纸发布涉嫌违法广告情况描述",
                "text" => '本期报纸暂未监测到广告'
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "落款",
            "text" => $user['regulatorname'].""
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "时间",
            "text" => $date_end_ymd
        ];

        $report_data = json_encode($data);
        //echo $report_data;exit;//20190103

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $system_num = getconfig('system_num');

            //将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].date('Y年m月',strtotime($month)).'月报';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 30;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',$report_start_date);
            $data['pnendtime'] 		    = date('Y-m-d',$report_end_date);
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }


    /**
     * 生成天津季报word 20181218  by yjn  create_sjz_quarter_report
     */
    public function create_quarter(){
        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport = New StatisticalModel();//实例化数据统计模型
        $StatisticalReport1 = New StatisticalReportModel();//实例化数据统计模型
        $quarter = I('quarter',4);//季度
        $year = I('year','2018');//季度
        $user = session('regulatorpersonInfo');//获取用户信息

        $zxs = ['110000','120000','310000','500000'];//直辖市
        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级
        switch ($quarter){
            case 1:
                $days_1 = date('t', strtotime($year."-01-01"));
                $days_3 = date('t', strtotime($year."-03-01"));
                $s_date = $year."-01-01";
                $e_date = $year."-03-".$days_3;
                break;
            case 2:
                $days_4 = date('t', strtotime($year."-04-01"));
                $days_6 = date('t', strtotime($year."-06-01"));
                $s_date = $year."-04-01";
                $e_date = $year."-06-".$days_6;
                break;
            case 3:
                $days_7 = date('t', strtotime($year."-07-01"));
                $days_9 = date('t', strtotime($year."-09-01"));
                $s_date = $year."-07-01";
                $e_date = $year."-09-".$days_9;
                break;
            case 4:
                $days_10 = date('t', strtotime($year."-10-01"));
                $days_12 = date('t', strtotime($year."-12-01"));
                $s_date = $year."-10-01";
                $e_date = $year."-12-".$days_12;
                break;
        }

        //季度中文
        switch ($quarter){
            case 1:
                break;
                $quarter_str = '一';
            case 2:
                $quarter_str = '二';
                break;
            case 3:
                $quarter_str = '三';
                break;
            case 4:
                $quarter_str = '四';
                break;
        }

        //文字描述匹配
        switch ($re_level){
            case 2:
            case 3:
            case 4:
                $regionid_str = substr($user['regionid'],0,4);
                $dq_name = '全市';
                $xj_name = '区县';
                $self_name = '市属';
                break;
            case 1:
                $regionid_str = substr($user['regionid'],0,2);
                if(!in_array($user['regionid'],$zxs)){
                    $dq_name = '全省';
                    $xj_name = '市级';
                    $self_name = '省属';
                }else{
                    $dq_name = '全市';
                    $xj_name = '区级';
                    $self_name = '市属';
                }
                break;
            case 5:
                $regionid_str = $user['regionid'];
                $dq_name = '全县';
                break;
        }

        $date_ymd = date('Y年m月d日',strtotime($s_date));//开始年月字符
        $date_end_ymd = date('Y年m月d日',strtotime($e_date));//结束时间
        $date_md = date('m月d日',strtotime($e_date));//结束月日字符
        $date_str = $year."年第".$quarter_str."季度";
        $days = round((strtotime($s_date) - strtotime($e_date) + 1)/86400);//计算天数
        $owner_media_ids = $this->get_owner_media_ids(-1,'','13');

        $ct_media_tv_count_hz = $StatisticalReport->ct_media_count('tv',$owner_media_ids,0);//市局电视媒体数量
        $ct_media_bc_count_hz = $StatisticalReport->ct_media_count('bc',$owner_media_ids,0);//市局广播媒体数量

        $ct_media_paper_count_hz = $StatisticalReport->ct_media_count('paper',$owner_media_ids,0);//市局报纸媒体数量

        $ct_media_tv_count_xj = $StatisticalReport->ct_media_count('tv',$owner_media_ids,2);//各区县电视媒体数量

        $ct_media_bc_count_xj = $StatisticalReport->ct_media_count('bc',$owner_media_ids,2);//各区县广播媒体数量

        $ct_media_paper_count_xj = $StatisticalReport->ct_media_count('paper',$owner_media_ids,2);//各区县报纸媒体数量

        $pc_media_count = $StatisticalReport->net_media_counts($regionid_str,'1301');//pc媒体数量
        $gzh_media_count = $StatisticalReport->net_media_counts($regionid_str,'1303');//公众号数量

        $all_media_count = $gzh_media_count + $pc_media_count+$ct_media_tv_count_hz + $ct_media_bc_count_hz + $ct_media_paper_count_hz + $ct_media_tv_count_xj + $ct_media_bc_count_xj + $ct_media_paper_count_xj;//全部有数据的媒体数量


        //开始设定json数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体初号hong",
            "text" => "第".$quarter_str."季度广告"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体初号hong",
            "text" => "监测报告"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $user['regulatorname']."广告监测中心对".$year."年第".$quarter_str."季度全市部分电视、广播、报刊、互联网等".$all_media_count."家媒体发布广告的情况进行了监测，现将监测情况通报如下："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "一、监测范围及对象"
        ];
        /*        if($ct_media_tv_count_hz != 0){
                    $hz_media_count_str .= $ct_media_tv_count_hz."个电视媒体；";
                }
                if($ct_media_bc_count_hz != 0){
                    $hz_media_count_str .= $ct_media_bc_count_hz."个广播媒体；";
                }
                if($ct_media_paper_count_hz != 0){
                    $hz_media_count_str .= $ct_media_paper_count_hz."个报纸媒体；";
                }
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => "（一）".$self_name."媒体：".$hz_media_count_str
                ];*/
        if($ct_media_tv_count_xj != 0){
            $xj_media_count_str .= $ct_media_tv_count_xj."个电视媒体；";
        }
        if($ct_media_bc_count_xj != 0){
            $xj_media_count_str .= $ct_media_bc_count_xj."个广播媒体；";
        }
        if($ct_media_paper_count_xj != 0){
            $xj_media_count_str .= $ct_media_paper_count_xj."个报纸媒体；";
        }
        $tregion_count = M('tregion')->where(['fpid'=>$user['regionid']])->count();

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "".$tregion_count."个区、县：".$xj_media_count_str
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）".$pc_media_count."家PC门户网站和".$gzh_media_count."个微信公众号"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "二、各类媒体广告发布情况"
        ];

        $every_media_class_tj =  $StatisticalReport->report_ad_monitor('fmedia_class_code',$owner_media_ids,$user['regionid'],$s_date,$e_date);
        $every_media_class_data = [];
        foreach ($every_media_class_tj as $every_media_class_tj_key => $ct_jczqk_val){

            if(!$ct_jczqk_val['titlename']){
                $tmediaclass_name = $ct_jczqk_val['tmediaclass_name'];
            }else{
                $tmediaclass_name = '合计';
            }
            $every_media_class_data[] = [
                $tmediaclass_name,
                $ct_jczqk_val['fad_times'],
                $ct_jczqk_val['fad_illegal_times'],
                $ct_jczqk_val['times_illegal_rate'].'%',
                $ct_jczqk_val['fad_play_len'],
                $ct_jczqk_val['fad_illegal_play_len'],
                $ct_jczqk_val['lens_illegal_rate']?$ct_jczqk_val['lens_illegal_rate'].'%':'0.00'.'%'
            ];
        }
        $every_media_class_data = to_string($every_media_class_data);
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "本季度共监测广告".$every_media_class_data[4][1]."条次，".$every_media_class_data[4][4]."条数，其中监测发现各类涉嫌违法广告".$every_media_class_data[4][2]."条次，".$every_media_class_data[4][5]."条数。具体情况详见下表："
        ];


        $data['content'][] = [
            "type" => "table",
            "bookmark" => "淄博季报媒介类型汇总表格时长版",
            "data" => $every_media_class_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "三、市（各区县）媒体广告发布总体情况（排名按条次违法率由高到底排列）"
        ];
        $every_region_tj =  $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],$s_date,$e_date,'times_illegal_rate');
        $every_region_tj_tcpm =  $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],$s_date,$e_date,'fad_illegal_times');
        $every_region_data = [];
        $every_region_illegal_times_data = [
            [''],
            ['']
        ];

        //各地区条次违法率排名json
        foreach ($every_region_tj as $every_region_tj_key => $every_region_tj_val){

            if(!$every_region_tj_val['titlename']){
                $every_region_data[] = [
                    $every_region_tj_key+1,
                    $every_region_tj_val['tregion_name'],
                    $every_region_tj_val['fad_times'],
                    $every_region_tj_val['fad_illegal_times'],
                    $every_region_tj_val['times_illegal_rate'].'%',
                    $every_region_tj_val['fad_play_len'],
                    $every_region_tj_val['fad_illegal_play_len'],
                    $every_region_tj_val['lens_illegal_rate']?$every_region_tj_val['lens_illegal_rate'].'%':'0.00'.'%',
                    $every_region_tj_val['ck_count']?$every_region_tj_val['ck_count']:'0',
                    $every_region_tj_val['ckl']?$every_region_tj_val['ckl']:'0.00'.'%',
                    $every_region_tj_val['cl_count']?$every_region_tj_val['cl_count']:'0',
                    $every_region_tj_val['cll']?$every_region_tj_val['cll']:'0.00'.'%'
                ];
            }
        }
        $every_region_data = to_string($every_region_data);
        foreach ($every_region_tj_tcpm as $every_region_tj_tcpm_key => $every_region_tj_tcpm_val){
            if(!$every_region_tj_tcpm_val['titlename']){
                if($every_region_tj_tcpm_val['fad_illegal_times'] > 0){
                    array_push($every_region_illegal_times_data[0],$every_region_tj_tcpm_val['tregion_name']);
                    array_push($every_region_illegal_times_data[1],strval($every_region_tj_tcpm_val['fad_illegal_times']));
                }
            }
        }
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号居中正文",
            "text" => "全市媒体发布涉嫌广告（条次违法率）从高到低排名"
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "通用各地区汇总表格列表",
            "data" => $every_region_data
        ];



        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各市区县违法广告发布量排名条形图hz",
            "data" => $every_region_illegal_times_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "四、全市电视、广播、报纸、互联网广告发布情况（排名按发布违法广告条次由高到底排列）"
        ];

        $zb_ds_media_count = $ct_media_tv_count_hz + $ct_media_tv_count_xj;
        $zb_gb_media_count = $ct_media_bc_count_hz + $ct_media_bc_count_xj;
        $zb_bz_media_count = $ct_media_paper_count_hz + $ct_media_paper_count_xj;
        $zb_hlw_media_count = $pc_media_count + $gzh_media_count;
        $media_class_array = ['01'=>$zb_dz_media_count,'02'=>$zb_gb_media_count,'03'=>$zb_bz_media_count,'13'=>$zb_hlw_media_count];
        foreach ($media_class_array as $media_class_key=>$media_class_val){
            $every_media_data = $this->create_tj_media_data($owner_media_ids,$user['regionid'],$s_date,$e_date,$media_class_key);
            switch ($media_class_key){
                case '01':
                    $this_mt_count = $zb_ds_media_count;
                    $this_mt_type = '电视';
                    break;
                case '02':
                    $this_mt_count = $zb_gb_media_count;
                    $this_mt_type = '广播';
                    break;
                case '03':
                    $this_mt_count = $zb_bz_media_count;
                    $this_mt_type = '报纸';
                    break;
                case '13':
                    $this_mt_count = $zb_hlw_media_count;
                    $this_mt_type = '互联网';
                    break;
            }

            if(!empty($every_media_data)){
                if(!empty($every_media_data['list'])){

                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "仿宋三号段落",
                        "text" => $year."年第".$quarter_str."季度广告监测中心对我市".$this_mt_count."家".$this_mt_type."媒体发布的广告进行了监测，共监测各类广告".$every_media_data['text']['fad_count']."条，其中涉嫌违法广告".$every_media_data['text']['fad_illegal_count']."条，条数违法率为".$every_media_data['text']['counts_illegal_rate']."；监测各类广告".$every_media_data['text']['fad_times']."条次，其中涉嫌违法广告".$every_media_data['text']['fad_illegal_times']."条次，条次违法率为".$every_media_data['text']['times_illegal_rate'].$every_media_data['no_data_media_str']."。"
                    ];
                    $data['content'][] = [
                        "type" => "table",
                        "bookmark" => "各媒体违法广告发布量排名列表不带条数",
                        "data" => $every_media_data['list']
                    ];

                    $data['content'][] = [
                        "type" => "chart",
                        "bookmark" => "违法广告发布量排名通用hz",
                        "data" => $every_media_data['chart']
                    ];
                }else{
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "仿宋三号段落",
                        "text" => "本期".$this_mt_type."媒体未监测到广告数据。"
                    ];
                }
            }
        }



        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、全市各类媒体发布的违法广告（全部类别）通报（排名按发布违法广告条次由高到底排列）"
        ];

        /*        $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => "（一）市级媒体"
                ];*/


        $mt_type_array = ['01'=>"电视",'02'=>"广播",'03'=>"报纸",'13'=>"互联网"];
        foreach ($mt_type_array as $m_key=>$m_val){
            $every_ill_data = [];
            $sj_ill_data = $StatisticalReport->report_illegal_ad_list($owner_media_ids,$s_date,$e_date,4,$m_key);
            if(!empty($sj_ill_data)){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => $m_val
                ];
                foreach ($sj_ill_data as $sj_ill_data_key=>$sj_ill_data_val){
                    if(!$sj_ill_data_val['titlename']){
                        $every_ill_data[] = [
                            $sj_ill_data_key+1,
                            $sj_ill_data_val['fmedianame'],
                            $sj_ill_data_val['fad_name'],
                            $sj_ill_data_val['ffullname'],
                            $sj_ill_data_val['fad_ill_times']
                        ];
                    }
                }
                $every_ill_data = to_string($every_ill_data);

                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "具体类别广告汇总列表hz",
                    "data" => $every_ill_data
                ];

            }

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "区（县）级媒体"
        ];

        foreach ($mt_type_array as $m_key=>$m_val){
            $every_ill_data = [];
            $xj_ill_data = $StatisticalReport->report_illegal_ad_list($owner_media_ids,$s_date,$e_date,5,$m_key,0);
            if(!empty($xj_ill_data)){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => $m_val
                ];
                foreach ($xj_ill_data as $xj_ill_data_key=>$xj_ill_data_val){
                    if(!$xj_ill_data_val['titlename']){
                        $every_ill_data[] = [
                            $xj_ill_data_key+1,
                            $xj_ill_data_val['fmedianame'],
                            $xj_ill_data_val['fad_name'],
                            $xj_ill_data_val['ffullname'],
                            $xj_ill_data_val['fad_ill_times']
                        ];
                    }
                }
                $every_ill_data = to_string($every_ill_data);

                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "具体类别广告汇总列表hz",
                    "data" => $every_ill_data
                ];

            }

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "六、全市各类违法广告比重结构图（排名按发布违法广告条次由高到底排列）"
        ];

        $every_adclass_tj1 =  $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],$s_date,$e_date,'fad_illegal_times');
        $every_adclass_tj2 =  $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],$s_date,$e_date,'times_illegal_rate');

        //按违法条次排序列表
        foreach ($every_adclass_tj1 as $every_adclass_tj1_key=>$every_adclass_tj1_val){
            if(!$every_adclass_tj1_val['titlename']){
                $every_ad_class_data[] = [
                    $every_adclass_tj1_key+1,
                    $every_adclass_tj1_val['tadclass_name'],
                    $every_adclass_tj1_val['fad_times'],
                    $every_adclass_tj1_val['fad_illegal_times'],
                    $every_adclass_tj1_val['times_illegal_rate'].'%',
                    $every_adclass_tj1_val['fad_play_len'],
                    $every_adclass_tj1_val['fad_illegal_play_len'],
                    $every_adclass_tj1_val['lens_illegal_rate']?$every_adclass_tj1_val['lens_illegal_rate'].'%':'0.00'.'%'
                ];
            }
        }
        $every_ad_class_data = to_string($every_ad_class_data);

        //按违法率排序描述
        $ill_tadclass_name = [];//前三违法广告类别
        $ill_tadclass_rate = [];//前三违法率
        foreach ($every_adclass_tj2 as $every_adclass_tj2_key=>$every_adclass_tj2_val){
            if(!$every_adclass_tj2_val['titlename']){
                if($every_adclass_tj2_key < 3 && $every_adclass_tj2_val['fad_illegal_times']){
                    $ill_tadclass_name[] = $every_adclass_tj2_val['tadclass_name'];
                    $ill_tadclass_rate[] = $every_adclass_tj2_val['times_illegal_rate'].'%';
                }else{
                    break;
                }
            }
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $year."年第".$quarter_str."季度监测情况看涉嫌违法广告类别主要发布在：".implode($ill_tadclass_name,'、')."的广告条次违法率较高，分别为".implode($ill_tadclass_rate,'、')."，其他类别条次违法率均低于".$ill_tadclass_rate[2]."。"
        ];

        if(!empty($every_ad_class_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表时长版",
                "data" => $every_ad_class_data
            ];

        }
        //[01,02,06,13]
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "七、药品、医疗服务、医疗器械、保健食品违法广告发布情况（排名按发布违法广告条次由高到底排列）"
        ];
        $zd_adclass_ill_data = $StatisticalReport->report_illegal_ad_list($owner_media_ids,$s_date,$e_date,5,'',1,['01','02','06','13']);
        foreach ($zd_adclass_ill_data as $zd_adclass_ill_data_key=>$zd_adclass_ill_data_val){
            if(!$zd_adclass_ill_data_val['titlename']){
                $zd_ill_adclass_data[] = [
                    $zd_adclass_ill_data_key+1,
                    $zd_adclass_ill_data_val['fmedianame'],
                    $zd_adclass_ill_data_val['fad_name'],
                    $zd_adclass_ill_data_val['ffullname'],
                    $zd_adclass_ill_data_val['fad_ill_times']
                ];
            }
        }
        $zd_ill_adclass_data = to_string($zd_ill_adclass_data);

        if(!empty($zd_ill_adclass_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $zd_ill_adclass_data
            ];
        }

        $report_data = json_encode($data);
        //echo $report_data;exit;//20190103

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $system_num = getconfig('system_num');

            //将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].$date_str.'报告';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 70;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',strtotime($s_date));
            $data['pnendtime'] 		    = date('Y-m-d',strtotime($e_date));
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }

    }

    /**
     * 生成天津年报word 20181218 * by yjn  create_sjz_year_report
     */
    public function create_year(){

        set_time_limit(0);
        session_write_close();//停止使用session
        header("Content-type:text/html;charset=utf-8");
        $StatisticalReport = New StatisticalModel();//实例化数据统计模型
        $StatisticalReport1 = New StatisticalReportModel();//实例化数据统计模型
        $year = I('year','2018');//季度
        $user = session('regulatorpersonInfo');//获取用户信息

        $zxs = ['110000','120000','310000','500000'];//直辖市
        $re_level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');//当前用户行政等级

        //文字描述匹配
        switch ($re_level){
            case 2:
            case 3:
            case 4:
                $regionid_str = substr($user['regionid'],0,4);
                $dq_name = '全市';
                $xj_name = '区县';
                $self_name = '市属';
                break;
            case 1:
                $regionid_str = substr($user['regionid'],0,2);
                if(!in_array($user['regionid'],$zxs)){
                    $dq_name = '全省';
                    $xj_name = '市级';
                    $self_name = '省属';
                }else{
                    $dq_name = '全市';
                    $xj_name = '区级';
                    $self_name = '市属';
                }
                break;
            case 5:
                $regionid_str = $user['regionid'];
                $dq_name = '全县';
                break;
        }
        $s_date = $year.'-07-01';
        $e_date = $year.'-12-31';
        $date_ymd = date('Y年m月d日',strtotime($s_date));//开始年月字符
        $date_end_ymd = date('Y年m月d日',strtotime($e_date));//结束时间
        $date_md = date('m月d日',strtotime($e_date));//结束月日字符
        $date_str = $year."年";

        $days = round((strtotime($s_date) - strtotime($e_date) + 1)/86400);//计算天数
        $owner_media_ids = $this->get_owner_media_ids();

        $ct_media_tv_count_hz = $StatisticalReport->ct_media_count('tv',$owner_media_ids,0);//市局电视媒体数量
        $ct_media_bc_count_hz = $StatisticalReport->ct_media_count('bc',$owner_media_ids,0);//市局广播媒体数量

        $ct_media_paper_count_hz = $StatisticalReport->ct_media_count('paper',$owner_media_ids,0);//市局报纸媒体数量

        $ct_media_tv_count_xj = $StatisticalReport->ct_media_count('tv',$owner_media_ids,2);//各区县电视媒体数量

        $ct_media_bc_count_xj = $StatisticalReport->ct_media_count('bc',$owner_media_ids,2);//各区县广播媒体数量

        $ct_media_paper_count_xj = $StatisticalReport->ct_media_count('paper',$owner_media_ids,2);//各区县报纸媒体数量

        $pc_media_count = $StatisticalReport->net_media_counts($regionid_str,'1301');//pc媒体数量
        $gzh_media_count = $StatisticalReport->net_media_counts($regionid_str,'1303');//公众号数量

        $all_media_count = $gzh_media_count + $pc_media_count+$ct_media_tv_count_hz + $ct_media_bc_count_hz + $ct_media_paper_count_hz + $ct_media_tv_count_xj + $ct_media_bc_count_xj + $ct_media_paper_count_xj;//全部有数据的媒体数量


        //开始设定json数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => $user['regionid'].'_'.time().'.docx',
            'content' => []
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体初号hong",
            "text" => $year."年广告"
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体初号hong",
            "text" => "监测报告"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $user['regulatorname']."广告监测中心对".$year."年全市部分电视、广播、报刊、互联网等".$all_media_count."家媒体发布广告的情况进行了监测，现将监测情况通报如下："
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "一、监测范围及对象"
        ];
        /*        if($ct_media_tv_count_hz != 0){
                    $hz_media_count_str .= $ct_media_tv_count_hz."个电视媒体；";
                }
                if($ct_media_bc_count_hz != 0){
                    $hz_media_count_str .= $ct_media_bc_count_hz."个广播媒体；";
                }
                if($ct_media_paper_count_hz != 0){
                    $hz_media_count_str .= $ct_media_paper_count_hz."个报纸媒体；";
                }
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => "（一）".$self_name."媒体：".$hz_media_count_str
                ];*/
        if($ct_media_tv_count_xj != 0){
            $xj_media_count_str .= $ct_media_tv_count_xj."个电视媒体；";
        }
        if($ct_media_bc_count_xj != 0){
            $xj_media_count_str .= $ct_media_bc_count_xj."个广播媒体；";
        }
        if($ct_media_paper_count_xj != 0){
            $xj_media_count_str .= $ct_media_paper_count_xj."个报纸媒体；";
        }
        $tregion_count = M('tregion')->where(['fpid'=>$user['regionid']])->count();

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（一）其他".$tregion_count."个区、县：".$xj_media_count_str
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "（二）".$pc_media_count."家PC门户网站和".$gzh_media_count."个微信公众号"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "二、各类媒体广告发布情况"
        ];

        $every_media_class_tj =  $StatisticalReport->report_ad_monitor('fmedia_class_code',$owner_media_ids,$user['regionid'],$s_date,$e_date);
        $every_media_class_data = [];
        foreach ($every_media_class_tj as $every_media_class_tj_key => $ct_jczqk_val){

            if(!$ct_jczqk_val['titlename']){
                $tmediaclass_name = $ct_jczqk_val['tmediaclass_name'];
            }else{
                $tmediaclass_name = '合计';
            }
            $every_media_class_data[] = [
                $tmediaclass_name,
                $ct_jczqk_val['fad_times'],
                $ct_jczqk_val['fad_illegal_times'],
                $ct_jczqk_val['times_illegal_rate'].'%',
                $ct_jczqk_val['fad_play_len'],
                $ct_jczqk_val['fad_illegal_play_len'],
                $ct_jczqk_val['lens_illegal_rate']?$ct_jczqk_val['lens_illegal_rate'].'%':'0.00'.'%'
            ];
        }
        $every_media_class_data = to_string($every_media_class_data);
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $year."年共监测广告".$every_media_class_data[4][1]."条次，".$every_media_class_data[4][4]."条数，其中监测发现各类涉嫌违法广告".$every_media_class_data[4][2]."条次，".$every_media_class_data[4][5]."条数。具体情况详见下表："
        ];


        $data['content'][] = [
            "type" => "table",
            "bookmark" => "淄博季报媒介类型汇总表格时长版",
            "data" => $every_media_class_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "三、市（各区县）媒体广告发布总体情况（排名按条次违法率由高到底排列）"
        ];
        $every_region_tj =  $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],$s_date,$e_date,'times_illegal_rate');
        $every_region_tj_tcpm =  $StatisticalReport->report_ad_monitor('fregionid',$owner_media_ids,$user['regionid'],$s_date,$e_date,'fad_illegal_times');
        $every_region_data = [];
        $every_region_illegal_times_data = [
            [''],
            ['']
        ];

        //各地区条次违法率排名json
        foreach ($every_region_tj as $every_region_tj_key => $every_region_tj_val){

            if(!$every_region_tj_val['titlename']){
                $every_region_data[] = [
                    $every_region_tj_key+1,
                    $every_region_tj_val['tregion_name'],
                    $every_region_tj_val['fad_times'],
                    $every_region_tj_val['fad_illegal_times'],
                    $every_region_tj_val['times_illegal_rate'].'%',
                    $every_region_tj_val['fad_play_len'],
                    $every_region_tj_val['fad_illegal_play_len'],
                    $every_region_tj_val['lens_illegal_rate']?$every_region_tj_val['lens_illegal_rate'].'%':'0.00'.'%',
                    $every_region_tj_val['ck_count']?$every_region_tj_val['ck_count']:'0',
                    $every_region_tj_val['ckl']?$every_region_tj_val['ckl']:'0.00'.'%',
                    $every_region_tj_val['cl_count']?$every_region_tj_val['cl_count']:'0',
                    $every_region_tj_val['cll']?$every_region_tj_val['cll']:'0.00'.'%'
                ];
            }
        }
        $every_region_data = to_string($every_region_data);
        foreach ($every_region_tj_tcpm as $every_region_tj_tcpm_key => $every_region_tj_tcpm_val){
            if(!$every_region_tj_tcpm_val['titlename']){
                if($every_region_tj_tcpm_val['fad_illegal_times'] > 0){
                    array_push($every_region_illegal_times_data[0],$every_region_tj_tcpm_val['tregion_name']);
                    array_push($every_region_illegal_times_data[1],strval($every_region_tj_tcpm_val['fad_illegal_times']));
                }
            }
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "宋体三号居中正文",
            "text" => "全市媒体发布涉嫌广告（条次违法率）从高到低排名"
        ];
        $data['content'][] = [
            "type" => "table",
            "bookmark" => "通用各地区汇总表格列表",
            "data" => $every_region_data
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "各市区县违法广告发布量排名条形图hz",
            "data" => $every_region_illegal_times_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "四、全市电视、广播、报纸、互联网广告发布情况（排名按发布违法广告条次由高到底排列）"
        ];

        $zb_ds_media_count = $ct_media_tv_count_hz + $ct_media_tv_count_xj;
        $zb_gb_media_count = $ct_media_bc_count_hz + $ct_media_bc_count_xj;
        $zb_bz_media_count = $ct_media_paper_count_hz + $ct_media_paper_count_xj;
        $zb_hlw_media_count = $pc_media_count + $gzh_media_count;
        $media_class_array = ['01'=>$zb_dz_media_count,'02'=>$zb_gb_media_count,'03'=>$zb_bz_media_count,'13'=>$zb_hlw_media_count];
        foreach ($media_class_array as $media_class_key=>$media_class_val){
            $every_media_data = $this->create_tj_media_data($owner_media_ids,$user['regionid'],$s_date,$e_date,$media_class_key);
            switch ($media_class_key){
                case '01':
                    $this_mt_count = $zb_ds_media_count;
                    $this_mt_type = '电视';
                    break;
                case '02':
                    $this_mt_count = $zb_gb_media_count;
                    $this_mt_type = '广播';
                    break;
                case '03':
                    $this_mt_count = $zb_bz_media_count;
                    $this_mt_type = '报纸';
                    break;
                case '13':
                    $this_mt_count = $zb_hlw_media_count;
                    $this_mt_type = '互联网';
                    break;
            }

            if(!empty($every_media_data)){
                if(!empty($every_media_data['list'])){
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "仿宋三号段落",
                        "text" => $year."年广告监测中心对我市".$this_mt_count."家".$this_mt_type."媒体发布的广告进行了监测，共监测各类广告".$every_media_data['text']['fad_count']."条，其中涉嫌违法广告".$every_media_data['text']['fad_illegal_count']."条，条数违法率为".$every_media_data['text']['counts_illegal_rate']."；监测各类广告".$every_media_data['text']['fad_times']."条次，其中涉嫌违法广告".$every_media_data['text']['fad_illegal_times']."条次，条次违法率为".$every_media_data['text']['times_illegal_rate'].$every_media_data['no_data_media_str']."。"
                    ];
                    $data['content'][] = [
                        "type" => "table",
                        "bookmark" => "各媒体违法广告发布量排名列表不带条数",
                        "data" => $every_media_data['list']
                    ];

                    $data['content'][] = [
                        "type" => "chart",
                        "bookmark" => "违法广告发布量排名通用hz",
                        "data" => $every_media_data['chart']
                    ];
                }else{
                    $data['content'][] = [
                        "type" => "text",
                        "bookmark" => "仿宋三号段落",
                        "text" => '本期'.$this_mt_type."媒体未监测到广告数据。"
                    ];
                }
            }
        }



        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "五、全市各类媒体发布的违法广告（全部类别）通报（排名按发布违法广告条次由高到底排列）"
        ];

        /*        $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => "（一）市级媒体"
                ];*/


        $mt_type_array = ['01'=>"电视",'02'=>"广播",'03'=>"报纸",'13'=>"互联网"];
        foreach ($mt_type_array as $m_key=>$m_val){
            $every_ill_data = [];
            $sj_ill_data = $StatisticalReport->report_illegal_ad_list($owner_media_ids,$s_date,$e_date,4,$m_key);
            if(!empty($sj_ill_data)){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => $m_val
                ];
                foreach ($sj_ill_data as $sj_ill_data_key=>$sj_ill_data_val){
                    if(!$sj_ill_data_val['titlename']){
                        $every_ill_data[] = [
                            $sj_ill_data_key+1,
                            $sj_ill_data_val['fmedianame'],
                            $sj_ill_data_val['fad_name'],
                            $sj_ill_data_val['ffullname'],
                            $sj_ill_data_val['fad_ill_times']
                        ];
                    }
                }
                $every_ill_data = to_string($every_ill_data);

                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "具体类别广告汇总列表hz",
                    "data" => $every_ill_data
                ];

            }

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => "区（县）级媒体"
        ];

        foreach ($mt_type_array as $m_key=>$m_val){
            $every_ill_data = [];
            $xj_ill_data = $StatisticalReport->report_illegal_ad_list($owner_media_ids,$s_date,$e_date,5,$m_key,0);
            if(!empty($xj_ill_data)){
                $data['content'][] = [
                    "type" => "text",
                    "bookmark" => "仿宋三号段落",
                    "text" => $m_val
                ];
                foreach ($xj_ill_data as $xj_ill_data_key=>$xj_ill_data_val){
                    if(!$xj_ill_data_val['titlename']){
                        $every_ill_data[] = [
                            $xj_ill_data_key+1,
                            $xj_ill_data_val['fmedianame'],
                            $xj_ill_data_val['fad_name'],
                            $xj_ill_data_val['ffullname'],
                            $xj_ill_data_val['fad_ill_times']
                        ];
                    }
                }
                $every_ill_data = to_string($every_ill_data);

                $data['content'][] = [
                    "type" => "table",
                    "bookmark" => "具体类别广告汇总列表hz",
                    "data" => $every_ill_data
                ];

            }

        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "六、全市各类违法广告比重结构图（排名按发布违法广告条次由高到底排列）"
        ];

        $every_adclass_tj1 =  $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],$s_date,$e_date,'fad_illegal_times');
        $every_adclass_tj2 =  $StatisticalReport->report_ad_monitor('fad_class_code',$owner_media_ids,$user['regionid'],$s_date,$e_date,'times_illegal_rate');

        //按违法条次排序列表
        foreach ($every_adclass_tj1 as $every_adclass_tj1_key=>$every_adclass_tj1_val){
            if(!$every_adclass_tj1_val['titlename']){
                $every_ad_class_data[] = [
                    $every_adclass_tj1_key+1,
                    $every_adclass_tj1_val['tadclass_name'],
                    $every_adclass_tj1_val['fad_times'],
                    $every_adclass_tj1_val['fad_illegal_times'],
                    $every_adclass_tj1_val['times_illegal_rate'].'%',
                    $every_adclass_tj1_val['fad_play_len'],
                    $every_adclass_tj1_val['fad_illegal_play_len'],
                    $every_adclass_tj1_val['lens_illegal_rate']?$every_adclass_tj1_val['lens_illegal_rate'].'%':'0.00'.'%'
                ];
            }
        }
        $every_ad_class_data = to_string($every_ad_class_data);

        //按违法率排序描述
        $ill_tadclass_name = [];//前三违法广告类别
        $ill_tadclass_rate = [];//前三违法率
        foreach ($every_adclass_tj2 as $every_adclass_tj2_key=>$every_adclass_tj2_val){
            if(!$every_adclass_tj2_val['titlename']){
                if($every_adclass_tj2_key < 3 && $every_adclass_tj2_val['fad_illegal_times']){
                    $ill_tadclass_name[] = $every_adclass_tj2_val['tadclass_name'];
                    $ill_tadclass_rate[] = $every_adclass_tj2_val['times_illegal_rate'].'%';
                }else{
                    break;
                }
            }
        }


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号段落",
            "text" => $year."年监测情况看涉嫌违法广告类别主要发布在：".implode($ill_tadclass_name,'、')."的广告条次违法率较高，分别为".implode($ill_tadclass_rate,'、')."，其他类别条次违法率均低于".$ill_tadclass_rate[2]."。"
        ];

        if(!empty($every_ad_class_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "各大类别广告汇总列表时长版",
                "data" => $every_ad_class_data
            ];

        }
        //[01,02,06,13]
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋三号标题",
            "text" => "七、药品、医疗服务、医疗器械、保健食品违法广告发布情况（排名按发布违法广告条次由高到底排列）"
        ];
        $zd_adclass_ill_data = $StatisticalReport->report_illegal_ad_list($owner_media_ids,$s_date,$e_date,5,'',1,['01','02','06','13']);
        foreach ($zd_adclass_ill_data as $zd_adclass_ill_data_key=>$zd_adclass_ill_data_val){
            if(!$zd_adclass_ill_data_val['titlename']){
                $zd_ill_adclass_data[] = [
                    $zd_adclass_ill_data_key+1,
                    $zd_adclass_ill_data_val['fmedianame'],
                    $zd_adclass_ill_data_val['fad_name'],
                    $zd_adclass_ill_data_val['ffullname'],
                    $zd_adclass_ill_data_val['fad_ill_times']
                ];
            }
        }
        $zd_ill_adclass_data = to_string($zd_ill_adclass_data);

        if(!empty($zd_ill_adclass_data)){
            $data['content'][] = [
                "type" => "table",
                "bookmark" => "具体类别广告汇总列表hz",
                "data" => $zd_ill_adclass_data
            ];
        }

        $report_data = json_encode($data);
        //echo $report_data;exit;//20190103

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }

            $system_num = getconfig('system_num');

//将生成记录保存
            $focus = I('focus',0);
            $pnname = $user['regulatorname'].$date_str.'报告';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 80;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= date('Y-m-d',strtotime($s_date));
            $data['pnendtime'] 		    = date('Y-m-d',strtotime($e_date));
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }


    }


    public function region_sort($arrays,$sort_key,$sort_order=SORT_ASC,$sort_type=SORT_NUMERIC ){
        if(is_array($arrays)){
            foreach ($arrays as $array){
                if(is_array($array)){
                    $key_arrays[] = $array[$sort_key];
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }

        array_multisort($key_arrays,$sort_order,$sort_type,$arrays);

        $sum = 0;
        foreach($arrays as $array_arr) {
            $sum += 1;
            $array_array[] = array_merge(array('pm' => (string)$sum),$array_arr);
        }
        return $array_array;
    }


    /*生成图表数据*/
    function create_chart_data($array,$index1 = 1,$index2 = 2,$index3 = 3,$index4 = 4){
        $array1[] = ["类型","发布量","违法发布量","违法率"];
        $array2[] = ["类型","发布量"];
        $array3[] = ["类型","违法发布量"];
        foreach ($array as $array_key=>$array_val){
            $array1[] = [
                $array_val[$index1],
                $array_val[$index2],
                $array_val[$index3],
                $array_val[$index4]
            ];
            $array2[] = [
                $array_val[$index1],
                $array_val[$index2]
            ];
            if($array_val[3] != 0){
                $array3[] = [
                    $array_val[$index1],
                    $array_val[$index3]
                ];
            }
        }
        return [$array1,$array2,$array3];
    }

    /*生成互联网图表数据*/
    function create_net_chart_data($array){
        if(empty($array)){
            return [[],[]];
        }
        $array_data2[] = ["网站","广告量"];

        foreach ($array as $array_key=>$array_val){
            $array_data1[] = [
                $array_val[0],
                $array_val[1],
                $array_val[2],
                $array_val[3],
                $array_val[4]
            ];
            $array_data2[] = [
                $array_val[1],
                $array_val[2]
            ];
        }
        return [$array_data1,$array_data2];
    }


    public function create_data_hzlist($ct_jczqk_quarter,$group){
        $ct_jczqk = [];
        foreach ($ct_jczqk_quarter as $ct_jczqk_quarter_val){
            foreach ($ct_jczqk_quarter_val as $ct_jczqk_quarter_val2){
                $fclass_md5 = '';
                $fclass_md5 = MD5($ct_jczqk_quarter_val2[$group]);
                if(isset($ct_jczqk[$fclass_md5])){
                    $ct_jczqk[$fclass_md5]['fad_times'] +=  $ct_jczqk_quarter_val2['fad_times'];
                    $ct_jczqk[$fclass_md5]['fad_times'] = strval($ct_jczqk[$fclass_md5]['fad_times']);
                    $ct_jczqk[$fclass_md5]['fad_count'] +=  $ct_jczqk_quarter_val2['fad_count'];
                    $ct_jczqk[$fclass_md5]['fad_count'] = strval($ct_jczqk[$fclass_md5]['fad_count']);
                    $ct_jczqk[$fclass_md5]['fad_illegal_times'] +=  $ct_jczqk_quarter_val2['fad_illegal_times'];
                    $ct_jczqk[$fclass_md5]['fad_illegal_times'] = strval($ct_jczqk[$fclass_md5]['fad_illegal_times']);
                    $ct_jczqk[$fclass_md5]['fad_illegal_count'] +=  $ct_jczqk_quarter_val2['fad_illegal_count'];
                    $ct_jczqk[$fclass_md5]['fad_illegal_count'] = strval($ct_jczqk[$fclass_md5]['fad_illegal_count']);
                    $ct_jczqk[$fclass_md5]['fad_illegal_times_rate'] =  round((($ct_jczqk[$fclass_md5]['fad_illegal_times'])/($ct_jczqk[$fclass_md5]['fad_times']))*100,2);
                    $ct_jczqk[$fclass_md5]['fad_illegal_count_rate'] =  round((($ct_jczqk[$fclass_md5]['fad_illegal_count'])/($ct_jczqk[$fclass_md5]['fad_count']))*100,2);
                }else{
                    $ct_jczqk[$fclass_md5] = $ct_jczqk_quarter_val2;
                }
            }
        }
        $ct_jczqk = array_values($ct_jczqk);
        return $ct_jczqk;
    }

    public function create_data_hzlist2($ct_jczqk_quarter){
        $ct_jczqk = [];
        foreach ($ct_jczqk_quarter as $ct_jczqk_quarter_val){
            foreach ($ct_jczqk_quarter_val as $ct_jczqk_quarter_val2){
                $fclass_md5 = '';
                $fclass_md5 = MD5($ct_jczqk_quarter_val2['fmedianame'].$ct_jczqk_quarter_val2['fadclass'].$ct_jczqk_quarter_val2['fad_name']);
                if(isset($ct_jczqk[$fclass_md5])){
                    $ct_jczqk[$fclass_md5]['illegal_times'] +=  $ct_jczqk_quarter_val2['illegal_times'];
                    $ct_jczqk[$fclass_md5]['illegal_times'] = strval($ct_jczqk[$fclass_md5]['illegal_times']);
                }else{
                    $ct_jczqk[$fclass_md5] = $ct_jczqk_quarter_val2;
                }
            }
        }
        $ct_jczqk = array_values($ct_jczqk);
        return $ct_jczqk;
    }

    public function get_table_data($data){
        $data_array = [];
        foreach ($data as $data_val){
            $data_array[] = array_values($data_val);
        }
        return $data_array;

    }

    public function pmaddkey($data,$px_num=3){
        if($px_num){
            $data = pxsf($data,$px_num);
        }
        $res = [];
        foreach ($data as $data_key=>$data_val){
            $array_res = [
                strval($data_key+1),
            ];
            foreach ($data_val as $data_val2){
                $array_res[] = $data_val2;
            }
            $res[] = $array_res;
        }
        return $res;
    }

    /**
     * 周预警报表生成
     * by zw
     */
    public function create_yujingreport(){
        session_write_close();
        header("Content-type: text/html; charset=utf-8");
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        $sheet ->setCellValue('A1','序号');
        $sheet ->setCellValue('B1','广告名称');
        $sheet ->setCellValue('C1','广告类别');
        $sheet ->setCellValue('D1','媒体类型');
        $sheet ->setCellValue('E1','违法表现');
        $sheet ->setCellValue('F1','涉嫌违法内容');

        $timestamp = time();
        $starttime = I('starttime')?I('starttime'):date('Y-m-d', strtotime("last week Monday", $timestamp));//周开始时间
        $endtime = I('endtime')?I('endtime'):date('Y-m-d', strtotime("last week Sunday", $timestamp));//周结束时间，默认上周

        $fadclasscode = ['01','02','03','04','05'];//生成的广告大类

        $arr_code = [];
        if(is_array($fadclasscode)){
            foreach ($fadclasscode as $key => $value) {
                $codes = D('Function')->get_next_tadclasscode($value,true);//获取下级广告类别代码
                if(!empty($codes)){
                    $arr_code = array_merge($arr_code,$codes);
                }else{
                    array_push($arr_code,$value);
                }
            }
            $where_tad = ' and c.fadclasscode  in("'.implode('","', $arr_code).'")';
        }

        //电视数据输出
        $tvsql = 'SELECT *
            FROM (
            SELECT c.fadname,b.fillegalcontent,b.fexpressions,d.ffullname
            FROM ttvissue a
            INNER JOIN ttvsample b ON a.ftvsampleid=b.fid
            INNER JOIN tad c ON b.fadid=c.fadid '.$where_tad.'
            inner join tadclass d on c.fadclasscode=d.fcode
            WHERE a.fissuedate BETWEEN "'.$starttime.'" AND "'.$endtime.'" AND fillegaltypecode>0
            GROUP BY a.ftvsampleid,c.fadname,b.fillegalcontent,b.fexpressions,d.ffullname) a
            GROUP BY fadname';
        $tvdata = M()->query($tvsql);

        $nowline = 0;
        foreach ($tvdata as $key => $value) {
            $sheet ->setCellValue('A'.($nowline+2) ,$nowline+1);
            $sheet ->setCellValue('B'.($nowline+2) ,$value['fadname']);
            $sheet ->setCellValue('C'.($nowline+2) ,$value['ffullname']);
            $sheet ->setCellValue('D'.($nowline+2) ,'电视');
            $sheet ->setCellValue('E'.($nowline+2) ,$value['fexpressions']);
            $sheet ->setCellValue('F'.($nowline+2) ,$value['fillegalcontent']);
            $nowline += 1;
        }

        //广播数据输出
        $bcsql = 'SELECT *
            FROM (
            SELECT c.fadname,b.fillegalcontent,b.fexpressions,d.ffullname
            FROM tbcissue a
            INNER JOIN tbcsample b ON a.fbcsampleid=b.fid
            INNER JOIN tad c ON b.fadid=c.fadid '.$where_tad.'
            inner join tadclass d on c.fadclasscode=d.fcode
            WHERE a.fissuedate BETWEEN "'.$starttime.'" AND "'.$endtime.'" AND fillegaltypecode>0
            GROUP BY a.fbcsampleid,c.fadname,b.fillegalcontent,b.fexpressions,d.ffullname) a
            GROUP BY fadname';
        $bcdata = M()->query($bcsql);

        foreach ($bcdata as $key => $value) {
            $sheet ->setCellValue('A'.($nowline+2) ,$nowline+1);
            $sheet ->setCellValue('B'.($nowline+2) ,$value['fadname']);
            $sheet ->setCellValue('C'.($nowline+2) ,$value['ffullname']);
            $sheet ->setCellValue('D'.($nowline+2) ,'广播');
            $sheet ->setCellValue('E'.($nowline+2) ,$value['fexpressions']);
            $sheet ->setCellValue('F'.($nowline+2) ,$value['fillegalcontent']);
            $nowline += 1;
        }

        //报纸数据输出
        $papersql = 'SELECT *
            FROM (
            SELECT c.fadname,b.fillegalcontent,b.fexpressions,d.ffullname
            FROM tpaperissue a
            INNER JOIN tpapersample b ON a.fpapersampleid=b.fpapersampleid
            INNER JOIN tad c ON b.fadid=c.fadid '.$where_tad.'
            inner join tadclass d on c.fadclasscode=d.fcode
            WHERE a.fissuedate BETWEEN "'.$starttime.'" AND "'.$endtime.'" AND fillegaltypecode>0
            GROUP BY a.fpapersampleid,c.fadname,b.fillegalcontent,b.fexpressions,d.ffullname) a
            GROUP BY fadname';
        $paperdata = M()->query($papersql);

        foreach ($paperdata as $key => $value) {
            $sheet ->setCellValue('A'.($nowline+2) ,$nowline+1);
            $sheet ->setCellValue('B'.($nowline+2) ,$value['fadname']);
            $sheet ->setCellValue('C'.($nowline+2) ,$value['ffullname']);
            $sheet ->setCellValue('D'.($nowline+2) ,'报纸');
            $sheet ->setCellValue('E'.($nowline+2) ,$value['fexpressions']);
            $sheet ->setCellValue('F'.($nowline+2) ,$value['fillegalcontent']);
            $nowline += 1;
        }

        $objActSheet =$objPHPExcel->getActiveSheet();

        //给当前活动的表设置名称
        $objActSheet->setTitle(session('regulatorpersonInfo.regionname1').'违法广告预警列表');

        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $date_now = $date;
        $date = date('Ymdhis');
        $savefile = './Public/Xls/'.$date.'.xlsx';
        $objWriter->save($savefile);

        $ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='.session('regulatorpersonInfo.regionname1').$starttime.'至'.$endtime.'违法广告预警报表.xlsx'));//上传云
        unlink($savefile);//删除文件
        echo "<a href='".$ret['url']."' target='_blank'>".session('regulatorpersonInfo.regionname1').$starttime.'至'.$endtime."违法广告预警报表.xlsx</a>";
    }

    //生成季报年报无条数含有时长版本
    public function create_tj_media_data($owner_media_ids,$user_regionid,$s_date,$e_date,$media_type){
        $StatisticalReport = New StatisticalModel();//实例化数据统计模型
        $every_media_tj =  $StatisticalReport->report_ad_monitor('fmediaid',$owner_media_ids,$user_regionid,$s_date,$e_date,'fad_illegal_times',$media_type);
        $every_media_data = [];
        $every_media_illegal_times_data = [
            [''],
            ['']
        ];

        //各地区条次违法率排名json
        foreach ($every_media_tj as $every_media_tj_key => $every_media_tj_val){
            if(!$every_media_tj_val['titlename']){
                $medias[] = $every_media_tj_val['fmediaid'];
                if($every_media_tj_val['fad_illegal_times'] > 0){
                    array_push($every_media_illegal_times_data[0],$every_media_tj_val['tmedia_name']);
                    array_push($every_media_illegal_times_data[1],strval($every_media_tj_val['fad_illegal_times']));
                }
                $every_media_data[] = [
                    $every_media_tj_key+1,
                    $every_media_tj_val['tregion_name'],
                    $every_media_tj_val['tmedia_name'],
                    $every_media_tj_val['fad_times'],
                    $every_media_tj_val['fad_illegal_times'],
                    $every_media_tj_val['times_illegal_rate'].'%',
                    $every_media_tj_val['fad_play_len'],
                    $every_media_tj_val['fad_illegal_play_len'],
                    $every_media_tj_val['lens_illegal_rate']?$every_media_tj_val['lens_illegal_rate'].'%':'0.00'.'%'
                ];
            }else{
                $hj_all = [
                    'fad_play_len' =>$every_media_tj_val['fad_count'],
                    'fad_illegal_play_len' =>$every_media_tj_val['fad_illegal_count'],
                    'lens_illegal_rate' =>$every_media_tj_val['lens_illegal_rate']?$every_media_tj_val['lens_illegal_rate'].'%':'0.00'.'%',
                    'fad_times' =>$every_media_tj_val['fad_times'],
                    'fad_illegal_times' =>$every_media_tj_val['fad_illegal_times'],
                    'times_illegal_rate' =>$every_media_tj_val['times_illegal_rate'].'%'
                ];
            }
        }
        $every_media_data = to_string($every_media_data);
        if(empty($every_media_tj)){
            return [];
        }else{
            $no_data_medias = array_diff($owner_media_ids,$medias);
            if(!empty($no_data_medias)){
                $media_names = M('tmedia')->where(['fid'=>['IN',$no_data_medias],'fmediaclassid'=>['like',$media_type.'%']])->getField('fmedianame',true);
            }else{
                $media_names = [];
            }
            if(!empty($media_names)){
                //$no_data_media_str = "(其中".implode($media_names,'、')."等媒体未监测到广告信息)";
            }else{
                $no_data_media_str = '';
            }
            return [
                'text'=>$hj_all,
                'list'=>$every_media_data,
                'chart'=>$every_media_illegal_times_data,
                'no_data_media_str' => $no_data_media_str
            ];
        }
    }


}
