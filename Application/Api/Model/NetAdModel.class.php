<?php
namespace Api\Model;
use Think\Exception;


class NetAdModel{


    public function copy_sam($sourceid,$targetid_list){
		
		$add_tnetsam_handle = [];
		foreach($targetid_list as $mj){
			$add_tnetsam_handle[] = array('major_key'=>$mj,'create_time'=>date('Y-m-d H:i:s'));
		}
		M('tnetsam_handle')->addAll($add_tnetsam_handle);
		
		$sourceInfo = M('tnetissue')
									->field('
											`fadid`	,
											`fadname`	,
											`fadtext`	,
											`fbrand`	,
											`fadowner`	,
											`fadclasscode`	,
											`fadclasscode_v2`	,
											`fspokesman`	,
											`fapprovalid`	,
											`fapprovalunit`	,
											`fillegaltypecode`	,
											`fillegalcontent`	,
											`fexpressioncodes`	,
											`fexpressions`	,
											`fconfirmations`	,
											`fadmanuno`	,
											`fmanuno`	,
											`fadapprno`	,
											`fapprno`	,
											`fadent`	,
											`fent`	,
											`fentzone`	,
											`isad`,
											`finputstate`	,
											`finspectstate`	,
											"自动继承_'.$sourceid.'" as fmodifier ,
											"32" as finputuser,
												
											`fmodifytime` 

											')
									->where(array('major_key'=>$sourceid))
									->find();
		#var_dump($sourceInfo);
		#file_put_contents('LOG/ABCDE.LOG',json_encode($sourceInfo) . "\n\n" .implode(',',$targetid_list) . "\n\n"); 
		if($sourceInfo){
			$ret = M('tnetissue')->where(array('major_key'=>array('in',$targetid_list)))->save($sourceInfo);
		}
		#file_put_contents('LOG/ABCDE.LOG','end',FILE_APPEND); 
		return $ret;

	}
	
	#获取联盟id
	public function get_trackerid($tracker){
		#return 0;
		$trackerid = S(md5('get_trackerid_'.$tracker));
		if($trackerid){
			#var_dump('命中联盟缓存');
			return $trackerid;
		}
		$trackerid = M('tracker')->master(true)->where(array('websitename'=>$tracker))->getField('trackerid');
		S(md5('get_trackerid_'.$tracker),$trackerid,3600);
		if($trackerid) return $trackerid; #如果查询到联盟，直接返回
		lock('get_trackerid_'.$tracker);//获取线程锁
		$trackerid = M('tracker')->add(array('websitename'=>$tracker));
		lock('get_trackerid_'.$tracker,null);//释放锁
		return $trackerid;
		
		
	}
	
	public function confirm_feat($data){
		$id = str_replace('adsource1_','',$data['id']);
		$identify_code = $data['identify_code'];
		
		if(strlen($identify_code) < 6) return false; #识别码太短
		if(is_numeric($id)){   
			$e_row = M('tnetissue_identify')
										->where(array('major_key'=>$id))
										->save(array(
														'result_type'=>$data['result_type'],
														'identify_code'=>$identify_code,
														'receive_time'=>time(),
														'i_id'=>$data['id']
														
													));
		}
		
		
		
		if($e_row){ #修改成功
			M('tnetissue')->where(array('major_key'=>$id))->save(array('identify_code'=>$identify_code)); #修改样本的识别码
		}else{
			#file_put_contents('LOG/confirm_feat_2',date('Y-m-d H:i:s').'	'.json_encode($data)."\n",FILE_APPEND);
			$fid = M('tnetissue_identify_2')
									->add(array(
														'result_type'=>$data['result_type'],
														'identify_code'=>$identify_code,
														'receive_time'=>time(),
														'i_id'=>$data['id']
														
													));
			return 'a_row';
		
		}
		
		
		if($data['result_type'] == 50) return false;
		
		$task_identify = M('tnetissue_identify')->where(array('identify_code'=>$identify_code,'is_task'=>1))->find(); #查询是否有任务
		
		
		if($task_identify){ #有任务的情况 $task_identify['major_key'] 同步到 $id
			$sam_priority = M('tnetissue')->join('tmedia on tmedia.fid = tnetissue.fmediaid')->where(array('tnetissue.major_key'=>$id))->getField('tmedia.priority');
			$tas_priority = M('tnettask')->where(array('major_key'=>$task_identify['major_key']))->getField('priority');
			
			#file_put_contents('LOG/confirm_feat_3',date('Y-m-d H:i:s').'	'.$sam_priority.'	'.$tas_priority."\n",FILE_APPEND);
			if($sam_priority > $tas_priority){ #如果样本所在媒体的优先级大于当前任务的优先级
				$se = M('tnettask')->where(array('major_key'=>$task_identify['major_key']))->save(array('priority'=>$sam_priority));
				#file_put_contents('LOG/confirm_feat_4',date('Y-m-d H:i:s').'	'.$sam_priority.'	'.$tas_priority.'	'.$se."\n",FILE_APPEND);
			}
			
			
			A('Api/NetAd','Model')->copy_sam($task_identify['major_key'],[$id]); #同步数据
			
		}else{ #没有任务的情况
			$samInfo = M('tnetissue')->field('major_key,fmediaid,net_id,net_created_date,"text" as ftype,"" as fadclasscode')->where(array('major_key'=>$id))->find();
		
			$task_array = [$samInfo]; #获取 任务 需要传递 的数据
			
			if($task_array){
				$addTaskRes = A('TaskInput/NetAd','Model')->addTask($task_array); #添加任务
				M('tnetissue_identify')->where(array('major_key'=>$id))->save(array('is_task'=>1)); #把这条记录修改为任务
				M('tnettask')->where(array('major_key'=>$id))->save(array('identify_code'=>$identify_code));
			}
		}
		
		
		return 'effect_row';
		
	}

}



