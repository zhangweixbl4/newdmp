<?php

namespace Test\Controller;
use Think\Controller;
use Think\Exception;

class TestController extends Controller{

	public function ff(){
		$sql_content = "123;456;789;1321;456456;fdsaf;";
		#var_dump($sql_content);	

		#var_dump(strpos($sql_content,';'));
		
		$wzs = []; #指定字符出现的位置
		$wz = 0; #从这个位置开始查找
		while(true){ #循环查找
			$wz = strpos($sql_content,';',$wz); #查找分号出现的位置
			if($wz === false){ #如果找不到，就结束
				break;
			}else{
				$wzs[] = $wz; 
			}
			$wz += 1; #从下一个位置开始查找
		}
		

		
		$s=0;
		$retStrList = [];
		foreach($wzs as $w){
			$is_fg = true;
			if((substr_count(str_replace(['\"',"\'"],['',''],$sql_content),'"',0,$w) % 2) == 1) $is_fg = false; 
			if((substr_count(str_replace(['\"',"\'"],['',''],$sql_content),"'",0,$w) % 2) == 1) $is_fg = false; 
		
			if($is_fg){
				$w += 1;
				var_dump($s.'_'.$w);
				$retStrList[] = substr($sql_content,$s,$w-$s);
				$s = $w;
			}
		}
		$retStrList[] = substr($sql_content,$s);
		
		var_dump($retStrList);

		
	}


	public function index(){
		header("Content-type: text/html; charset=utf-8"); 
		header('Access-Control-Allow-Origin:*');
		$mediaId = I('mediaId');//媒介ID
		$startDate = I('startDate');//开始日期
		$endDate = I('endDate');//结束日期
		
		$mediaInfo = M('tmedia')
								->cache(true,600)
								->field('
											fid,
											fmedianame,
											left(fmediaclassid,2) as media_class,
											media_region_id
										')
								->where(array('fid'=>$mediaId))
								->find();//查询媒体详情
		
		//var_dump($mediaInfo);
		if($mediaInfo['media_class'] == '01'){//电视
			$prefixTable = 'ttvissue_';
		}elseif($mediaInfo['media_class'] == '02'){//广播
			$prefixTable = 'tbcissue_';
		}else{
			
		}
		
		$dateList = array();
		for($i=strtotime($startDate);$i<=strtotime($endDate);$i+=86400){//循环日期
			
			$issueTable = $prefixTable.date('Ym',$i).'_'.substr($mediaInfo['media_region_id'],0,2);//发布记录表名称
			
			
			$issueList = M($issueTable)
									//->field('hour(FROM_UNIXTIME(fstarttime)) as hour,count(*) as count')
									->where(array('fmediaid'=>$mediaInfo['fid'],'fissuedate'=>$i))
									->group('hour(FROM_UNIXTIME(fstarttime))')
									->getField('hour(FROM_UNIXTIME(fstarttime)) as hour,count(*) as count');//查询发布记录，按小时分组
									
									
			
			$na_url = 'http://47.96.182.117/manage/channel/getChannelServiceTime';//查询闭台数据的地址
			$ret = http($na_url,array('channel'=>$mediaInfo['fid'],'date'=>date('Y-m-d',$i)));//去远程查询
			$ret = json_decode($ret,true);//查询结果转化为数组			
			$data = array();
			$data['date'] = date('Y-m-d',$i);//日期
			$data['abnormal'] = array();//异常数据
			$data['hours'] = array();
			for($h=0;$h<24;$h++){
				$data['hours'][$h] = intval($issueList[$h]);//设置每个小时的发布数量
			}
			
			foreach($ret['dataList'] as $rr){//循环查到的结果，判断是否包含查询时间
				//var_dump($rr); 
				$abnormal = array();
				$abnormal['start'] = $rr['start'];
				$abnormal['end'] = $rr['end'];
				$abnormal['start_time'] = $rr['start_time'];
				$abnormal['end_time'] = $rr['end_time'];
				
				if($rr['type'] == 1){
					$abnormal['title'] = '停台';
					$abnormal['color'] = '颜色代码';
				}elseif($rr['type'] == 2){
					$abnormal['title'] = '停机检修';
					$abnormal['color'] = '颜色代码';
				}	
				$data['abnormal'][] = $abnormal;
			}
			
			
			$dateList[] = $data;
			
			
			
			//var_dump($issueList);
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'','dataList'=>$dateList));
		
	}
	


	
	public function index2(){
		header("Content-type: text/html; charset=utf-8"); 
		$where = array();
		$where['right(tregion.fid,4)'] = '0000';
		$where['tregion.fid'] = array('in','440100,440300,320100,420100,210100,610100,510100,370100,330100,230100,220100,210200,370200,350200,330200');
		$where['_logic'] = 'or';
		$regionList = M('tregion')->field('fid,fname,ffullname')->where($where)->select();
		
		$regionList2 = array();
		
		foreach($regionList as $region){
			$regionList2[$region['fid']] = $region;
			$regionList2[$region['fid']]['tv'] = M('tmedia')->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')->where("tmedia.priority >= -100 and LEFT (tmedia.fmediaclassid, 2) = '01' and tmediaowner.fregionid = ".$region['fid'])->count();
			$regionList2[$region['fid']]['bc'] = M('tmedia')->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')->where("tmedia.priority >= -100 and LEFT (tmedia.fmediaclassid, 2) = '02' and tmediaowner.fregionid = ".$region['fid'])->count();
			$regionList2[$region['fid']]['paper'] = M('tmedia')->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')->where("tmedia.priority >= -100 and LEFT (tmedia.fmediaclassid, 2) = '03' and tmediaowner.fregionid = ".$region['fid'])->count();
			
		}
		
		foreach($regionList2 as $region){
			echo $region['fid'].",";
			echo $region['fname'].",";
			echo $region['ffullname'].",";
			echo $region['tv'].",";
			echo $region['bc'].",";
			echo $region['paper'].",\n";
			
			
		}
	}



	
	public function dbList(){
		header("Content-type:text/html;charset=utf-8");
		
		$tableList = M()->query("show table status where Engine = 'InnoDB'");
		//var_dump($tableList);
		//exit;

		foreach($tableList as $table){
			$tableName = $table['Name'];
			//echo $tableName."\n";
			
			
			$tableInfo = M()->query('SHOW FULL COLUMNS FROM '.$tableName);

			
			$sqlBody .= "CREATE TABLE ".$tableName." ("."\n";
			
			foreach($tableInfo as $table_info){
				//var_dump($table_info);
				$sqlBody .= '	`'.$table_info['Field'].'` ';
				if($table_info['Extra'] == 'auto_increment'){
					$sqlBody .= "bigint(20) ";
				}else{
					$sqlBody .= str_replace(array(''),array(''),$table_info['Type']).' ';
				}
				
				
				
				
				if($table_info['Null'] == 'NO'){
					//$sqlBody .= "NOT NULL ";
				}
				
				if($table_info['Default'] != ''){
					$sqlBody .= "DEFAULT '".str_replace(array(''),array(''),$table_info['Default'])."' ";
				}else{
					$sqlBody .= "DEFAULT NULL ";
				}
				
				if($table_info['Extra'] == 'auto_increment'){
					$sqlBody .= "AUTO_INCREMENT ";
				}
				
				if($table_info['Comment'] != ''){
					$sqlBody .= "COMMENT '".$table_info['Comment']."' ";
				}
				if($table_info['Key'] == 'PRI'){//判断是否主索引
					$pri_key = $table_info['Field'];
				}
				$sqlBody .= ",\n";
			}
			
		  
			$sqlBody .= "	PRIMARY KEY (`".$pri_key."`),\n";
			$sqlBody .= "	KEY idx_name(`".$pri_key."`)\n";
			$sqlBody .= ")	DEFAULT CHARSET=utf8 ";
			
			
			$sqlBody .= " DISTRIBUTE_KEY (`".$pri_key."`)";
			
			$sqlBody .= ";\n\n";
		}
		
		echo $sqlBody;
		
	}

}