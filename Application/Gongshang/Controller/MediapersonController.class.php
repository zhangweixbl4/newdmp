<?php
//媒介人员
namespace Gongshang\Controller;
use Think\Controller;
class MediapersonController extends BaseController {

	/*媒介人员列表*/
	public function index(){
		$p = I('p',1);//当前第几页
		$pp = 5;//每页显示多少记录
		$keyword = I('keyword');
		
		$where = array();
		if($keyword != '') $where['tmediaowner.fname|tmediaperson.fname|tmediaperson.fmobile'] = array('like','%'.$keyword.'%');
		
		$count = M('tmediaperson')
							->join('tmediaowner on tmediaowner.fid = tmediaperson.fmediaownerid')
							->where($where)->count();// 查询满足要求的总记录数
							
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数					
		$personList = M('tmediaperson')
							->field('tmediaperson.*,tmediaowner.fname as mediaowner_name')
							->join('tmediaowner on tmediaowner.fid = tmediaperson.fmediaownerid')
							->where($where)
							->limit($Page->firstRow.','.$Page->listRows)
							->select();
		$personList = list_k($personList,$p,$pp);//为列表加上序号
		//var_dump($personList);
		$this->assign('page',$Page->show());//分页输出
		$this->assign('personList',$personList);
		$this->display();

	}
	
	/*人员详情*/
	public function ajax_person_details(){
		
		$fid = I('fid');//获取人员ID
		
		$personDetails = M('tmediaperson')
										->field('tmediaperson.*,tmediaowner.fname as mediaowner_name')
										->join('tmediaowner on tmediaowner.fid = tmediaperson.fmediaownerid')
										->where(array('tmediaperson.fid'=>$fid))->find();
		$this->ajaxReturn(array('code'=>0,'personDetails'=>$personDetails));
	}
	
	/*删除人员*/
	public function ajax_person_del(){
		$fid = I('fid');//获取人员ID
		if(M('tmediaperson')->where(array('fid'=>$fid))->delete()){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'.$fid));
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除失败,原因未知'));
		}
		
		
	}

	
	/*添加、修改媒介人员*/
	public function add_edit_person(){
		$fid= I('fid');//人员ID
		$fid = intval($fid);//转为数字
		
		$a_e_data = array();
		$a_e_data['fmediaownerid'] = I('fmediaownerid');//媒介机构ID
		$a_e_data['fcode'] = I('fcode');//人员编码
		$a_e_data['fname'] = I('fname');//人员名称
		$a_e_data['fduties'] = I('fduties');//人员职务
		$a_e_data['fmobile'] = I('fmobile');//联系手机
		$a_e_data['fdescribe'] = I('fdescribe');//说明	
		$a_e_data['fisadmin'] = I('fisadmin');//是否管理员 0=》不是，1=》是
		$a_e_data['fstate'] = I('fstate',1);//状态，-1删除，0-无效，1-有效
		
		$fcreator = session('regulatorpersonInfo');//创建人
		
		if($fid == 0){//判断是修改还是新增
			$is_repeat = M('tmediaperson')->where(array('fcode'=>$a_e_data['fcode']))->count();//查询人员代码是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'人员代码重复'));//返回人员代码重复
			if(I('fpassword') == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'密码不能为空'));
			$a_e_data['fpassword'] = md5(I('fpassword'));//密码
			$a_e_data['fcreator'] = $fcreator['regulatorname'];//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = $fcreator['regulatorname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			
			$rr = M('tmediaperson')->add($a_e_data);//添加数据
			
			
			if($rr > 0){//判断是否添加成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));//返回失败
			}
			
		}else{//修改
			$is_repeat = M('tmediaperson')->where(array('fid'=>array('neq',$fid),'fcode'=>$a_e_data['fcode']))->count();//查询人员代码是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'人员代码重复'));//返回人员代码重复
			if(I('fpassword') != '') $a_e_data['fpassword'] = md5(I('fpassword'));//密码
			$a_e_data['fmodifier'] = $fcreator['regulatorname'];//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('tmediaperson')->where(array('fid'=>$fid))->save($a_e_data);//修改数据
			if($rr > 0){//判断是否修改成功
				
					
				//var_dump(M('ad_input_user')->where(array('person_id'=>$fid))->count());
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));//返回失败
			}
			
		}
		
		
		
	}
	
	
	

}