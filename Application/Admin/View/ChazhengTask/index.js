Vue.prototype.$ELEMENT = {
  size: 'mini',
  zIndex: 3000
}
var vm = new Vue({
  el: '#app',
  name: 'app',
  data() {
    return {
      files: [],
      search: {
        page: '1',
      },
      status: {
        '40': '待查证',
        '41': '重新查证',
        '42': '查证完成',
      },
      clcolor: {
        '1': 'dnager',
        '2': 'info',
        '3': 'success',
      },
      loading: false,
      tabledata: [],
      details: {},
      Details: {},
      searchDate: [],
      showDetails: false,
      filltype: {
        '0': '投诉举报',
        '20': '上级交办',
        '30': '部门移交',
        '40': '其他',
      },
      dealDialog: false,
      filedata: [],
      showFiles: false,
      Files: false
    }
  },
  created() {
    this._getList()
  },
  methods: {
    _getList(page) {
      this.loading = true
      let data = Object.assign({}, this.search)
      data.page = page || '1'
      data.fsttime = this.searchDate[0] || ''
      data.fendtime = this.searchDate[1] || ''
      $.post("/Admin/ChazhengTask/index_list", data, (res) => {
        if (res.code === 0) {
          this.tabledata = res.data.list
        }
        this.loading = false
      })
    },
    _showModal(obj) {
      this.details = obj
      this.dealDialog = true
    },
    _closeModal: function () {
      $('#changeModal').modal('hide');
      $(".send").modal('hide');
      this.files = [],
        this.rmark = ''
    },
    _save() {
      if (!this.files.length) {
        return false
      }
      var data = {
        fid: this.details.fid,
        dfileurl: this.files[0]['url'],
      }
      $.post("/Admin/ChazhengTask/report_process", data, (res) => {
        if (res.code === 0) {
          $('#changeModal').modal('hide')
          this.$message.success(res.msg)
          this._getList()
          this.files = []
        }
      })
    },
    _jbinfo(obj) {
      this.Details = {
        fid: obj.fid,
        type: 1
      }
      this.showDetails = true
    },
    _fujian(obj, type) {
      let data = {
        fid: obj.fid,
        types: type
      }
      $.post('/Admin/ChazhengTask/view_chazheng', data, res => {
        if (res.code === 0) {
          if (type) {
            this.Files = true
            if (res.data.files.czfiles) {
              this.files = res.data.files.czfiles.map(item => {
                if (item.fattach_url.indexOf('?attname=') === -1) {
                  item.fattach_url = item.fattach_url + '?attname=' + item.fattach
                }
                return item
              })
            }
            this.desc = res.data.fcxczcontent || ''
          } else {
            if (res.data.files.jbfiles.length) {
              this.showFiles = true
              this.filedata = res.data.files.jbfiles.map(item => {
                if (item.fattach_url.indexOf('?attname=') === -1) {
                  item.fattach_url = item.fattach_url + '?attname=' + item.fattach
                }
                return item
              })
              this.desc = res.data
            } else {
              this.$message.warning('无附件！')
            }
          }
        } else {
          this.$message.error(res.msg)
        }
      })
    }
  }
})