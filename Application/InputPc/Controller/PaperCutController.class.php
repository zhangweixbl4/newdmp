<?php
namespace InputPc\Controller;
use Think\Controller;
class PaperCutController extends BaseController {
    public function index(){
		$wx_id = intval(session('wx_id'));	
		$p = I('p',1);//当前第几页
		$pp = 10;//每页显示多少记录
		$keyword = I('keyword');
		$fstate = I('fstate');
		$fissuedate_s = I('fissuedate_s');
		if($fissuedate_s == ''){
			$fissuedate_s = date('Y-m-d',time()-7*86400);
			$_GET['fissuedate_s'] = $fissuedate_s;
		}	
		$fissuedate_s = date('Y-m-d',strtotime($fissuedate_s));
		
		$fissuedate_e = I('fissuedate_e');
		if($fissuedate_e == ''){
			$fissuedate_e = date('Y-m-d');
			$_GET['fissuedate_e'] = $fissuedate_e;
		}	
		$fissuedate_e = date('Y-m-d',strtotime($fissuedate_e));
		$fpage = I('fpage');//版面
		
		cookie('paper_cut_get_next_task',array('keyword'=>$keyword,'fissuedate_s'=>$fissuedate_s,'fissuedate_e'=>$fissuedate_e,'fpage'=>$fpage));

		$where = array();
		
		$where['_string'] .= '(tpapersource.fstate = 0 or ';//未剪辑的
		$where['_string'] .= 'tpapersource.fuserid = '.$wx_id.') ';//自己剪辑的
		$where['_string'] .= A('InputPc/Task','Model')->paper_cut_authority($wx_id);//数据权限控制
		$where['fissuedate'] = array('between',array($fissuedate_s,$fissuedate_e));
		if($keyword != ''){
			$where['tmedia.fmedianame'] = array('like','%'.$keyword.'%');
		}
		if($fstate != ''){
			$where['tpapersource.fstate'] = intval($fstate);
		}
		if($fpage != ''){
			$where['tpapersource.fpage'] = array('like','%'.$fpage.'%');
		}
		
		
		$count = M('tpapersource')
								->join('tmedia on tmedia.fid = tpapersource.fmediaid')
								//->join('tmedialabel on tmedialabel.fmediaid = tmedia.fid and tmedialabel.flabelid = 242')
								->where($where)->count();
		
		$Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
		$sourceList = M('tpapersource')
								->field('
										tpapersource.*,
										tmedia.fmedianame
										
										
										')
								->join('tmedia on tmedia.fid = tpapersource.fmediaid')
								//->join('tmedialabel on tmedialabel.fmediaid = tmedia.fid and tmedialabel.flabelid = 242')
								->order('( case when tpapersource.fstate=6 then 1 ELSE 4 END),tpapersource.fissuedate desc,rand(),tmedia.fmedianame')
								->limit($Page->firstRow.','.$Page->listRows)
								->where($where)->select();
		
		//var_dump(M('tpapersource')->getLastSql());
		
		
		
		
		$sourceList = list_k($sourceList,$p,$pp);//为列表加上序号
		
		
		$todayTaskCount = A('InputPc/PaperCut','Model')->get_today_paper_task_count();
		
		$this->assign('todayTaskCount',$todayTaskCount);
		$this->assign('sourceList',$sourceList);
		
		$this->assign('wx_id',$wx_id);
		
		
		$this->assign('page',$Page->show());//分页输出
		//exit;
		$this->display();
	}
	public function ad_cut(){
		if(	!A('InputPc/Task','Model')->user_other_authority('1003')){
			exit('无权限');
		}
		$fid = I('fid');//获取素材ID
		$wx_id = intval(session('wx_id'));

		$papersourceDetails = M('tpapersource')
										->field('tpapersource.*,tmedia.fmedianame')
										->join('tmedia on tmedia.fid = tpapersource.fmediaid')
										->where(array('tpapersource.fid'=>$fid))
										->find();//查询样本详情
		if(($papersourceDetails['fuserid'] == $wx_id || $papersourceDetails['fuserid'] == 0) && $papersourceDetails['fstate'] == 0){
			M('tpapersource')->where(array('tpapersource.fid'=>$fid))->save(array('fuserid'=>$wx_id,'fstate'=>1,'operation_time'=>time()));//修改处理人
		}
			
		if($papersourceDetails['fuserid'] != $wx_id){
			$taskUser = M('ad_input_user')->where(array('wx_id'=>$papersourceDetails['fuserid']))->find();
		}								
		
		
		$illegaltypeList = M('tillegaltype')->field('fcode,fillegaltype')->where(array('fstate'=>1))->select();//查询违法类型
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
		$papersizeList = M('tpapersize')->field('fcode,fsize')->select();//规格列表
		$issueList = M('tpaperissue')->field('
											tpaperissue.fpaperissueid,	
											tpaperissue.fsize,
											tpaperissue.fissuetype,
											tpapersample.fjpgfilename,
											tpapersample.fversion,
											tpapersample.cut_err_reason,
											tpapersample.finputstate,
											(select nickname from ad_input_user where wx_id = tpapersample.sub_cut_err_user) as sub_cut_err_username,
											
											
											tpapersample.tem_ad_name as fadname
											
											
												')
									->join('tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
									//->join('tad on tad.fadid = tpapersample.fadid')
									->where(array('tpaperissue.sourceid'=>$fid))
									->select();
		$this->assign('taskUser',$taskUser);//任务用户信息					
		$this->assign('issueList',$issueList);//广告列表	
		$this->assign('papersizeList',$papersizeList);//规格列表
		$this->assign('illegaltypeList',$illegaltypeList);//违法类型数据列表
		$this->assign('IllegalList',$IllegalList);//违法表现数据列表
		$this->assign('papersourceDetails',$papersourceDetails);
		$this->assign('qiniu_up_base64_img_token',A('Common/Qiniu','Model')->qiniu_up_base64_img_token());
		
		
		$this->display();
	}
	/*添加广告样本*/
	public function add_papersample(){
		$paper = '';
		$wx_id = intval(session('wx_id'));
		$wxInfo = M('ad_input_user')->field('wx_id,nickname,cutting_seniority')->where(array('wx_id'=>$wx_id))->find();
		$nickname = $wxInfo['nickname'];
		if(	!A('InputPc/Task','Model')->user_other_authority('1003')){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'无权限,请联系管理员'));
		}
		
		$fmediaid = I('fmediaid');
		$fadname = I('fadname');//广告名
		if($fadname == ''){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'请输入广告名称'));
		}
		
		$fbrand = I('fbrand');// 广告品牌
		$adclass_code = I('adclass_code');//广告分类code
		$adowner_name = I('adowner_name');//广告主
		$fversion = I('fversion');//版本
		$fspokesman = I('fspokesman');//代言人
		$fapprovalid = I('fapprovalid');//审批号
		$fapprovalunit = I('fapprovalunit');//审批单位
		
		$fillegaltypecode = intval(I('fillegaltypecode','-1'));//违法类型代码
		$fexpressioncodes = I('fexpressioncodes');//违法表现代码，逗号分隔
		$fillegalcontent = I('fillegalcontent');//违法内容
		$sourceId = I('sourceId');//原始素材编号
		$fissuetype = I('fissuetype');//发布类型
		$fsize = I('fsize');//发布规格
		$fjpgfilename = I('fjpgfilename');
		$uuid = MD5($fjpgfilename);
		$fadmanuno = I('fadmanuno');//广告中标识的生产批准文号
		$fmanuno = I('fmanuno');//生产批准文号
		$fadapprno = I('fadapprno');//广告中标识的广告批准文号
		$fapprno = I('fapprno');//广告批准文号
		$fadent = I('fadent');//广告中标识的生产企业（证件持有人）名称
		$fent = I('fent');//生产企业（证件持有人）名称
		$fentzone = I('fentzone');//生产企业（证件持有人）所在地区
		
		$sourceInfo = M('tpapersource')->where(array('fid'=>$sourceId))->find();//素材信息
		
		if($sourceInfo['fuserid'] != $wx_id){//判断该报纸版面是不是由自己处理
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,该报纸版面目前不是由您处理'));
		}
		if($sourceInfo['fstate'] != 1  && $sourceInfo['fstate'] != 6){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,当前状态不允许添加'));
		}
		
		
		if($adclass_code == '') $adclass_code = '2301';
		
		//$fadid = A('Adinput/InputTask','Model')->get_ad_id($fadname,$fbrand,$adclass_code,$adowner_name);//获取广告ID
		
		
		$a_e_data['fmediaid'] = $sourceInfo['fmediaid'];//媒介id
		$a_e_data['fissuedate'] = $sourceInfo['fissuedate'];//发布日期
		$a_e_data['last_issue_date'] = $sourceInfo['fissuedate'];//最后发布日期
		
		
		$a_e_data['fadid'] = 0;//广告ID
		$a_e_data['tem_ad_name'] = $fadname;//广告ID
		
		
		$a_e_data['fversion'] = $fversion;//版本说明
		$a_e_data['fspokesman'] = $fspokesman;//代言人

		$a_e_data['fapprovalid'] = $fapprovalid;//审批号
		$a_e_data['fapprovalunit'] = $fapprovalunit;//审批单位

		$a_e_data['fcreator'] = $nickname;//创建人
		$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$a_e_data['fmodifier'] = $nickname;//修改人
		$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$a_e_data['finspectstate'] = 1;//违法判定状态
		$a_e_data['fjpgfilename'] = $fjpgfilename;
		$a_e_data['uuid'] = $uuid;
		$a_e_data['sourceid'] = $sourceInfo['fid'];
		
		$a_e_data['fadmanuno'] = $fadmanuno;//广告中标识的生产批准文号
		$a_e_data['fmanuno'] = $fmanuno;//生产批准文号
		$a_e_data['fadapprno'] = $fadapprno;//广告中标识的广告批准文号
		$a_e_data['fapprno'] = $fapprno;//广告批准文号
		$a_e_data['fadent'] = $fadent;//广告中标识的生产企业（证件持有人）名称
		$a_e_data['fent'] = $fent;//生产企业（证件持有人）名称
		$a_e_data['fentzone'] = $fentzone;//生产企业（证件持有人）所在地区
		
		
		
		if(true || $fadname == '' || !A('InputPc/Task','Model')->user_other_authority('1001')){//如果广告名称为空 或者 没有广告录入权限
			$a_e_data['finputstate'] = 1;//需要众包录入广告信息
		}else{
			$a_e_data['finputstate'] = 2;
			$a_e_data['finputuser'] = $wx_id;
			A('InputPc/Task','Model')->change_task_count($wx_id,'paper_input',1);//
		}
		
		if(M('tadclass')->where(array('fcode'=>$adclass_code))->count() == 0){
			$this->ajaxReturn( array('code'=>-1,'msg'=>'广告类别错误'));
		} 

		M()->startTrans();//开启事务方法
		$papersampleid = M('tpapersample')->add($a_e_data);//添加数据
		//var_dump($papersampleid);

		if($papersampleid > 0){
			if(intval($fillegaltypecode) >= 0 && A('InputPc/Task','Model')->user_other_authority('1002')){//判断是否有违法判定 和 有广告初审权限
				$illegal = A('Open/Papersample','Model')->papersampleillegal($papersampleid,explode(',',$fexpressioncodes),$nickname);//添加电视广告违法表现对应表并获取冗余字段
				$illegal['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
				$illegal['fillegalcontent'] = $fillegalcontent;//违法内容
				$illegal['finspectstate'] = 2;//判定状态
				$illegal['finspectuser'] = $wx_id;//违法审查人
				$illegal['fmodifier'] = $wxInfo['nickname'];//修改人
				$illegal['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
				if($fillegaltypecode != '') $illegal['fillegaltypecode'] = $fillegaltypecode;//违法类型
				$rr_illegal = M('tpapersample')->where(array('fpapersampleid'=>$papersampleid))->save($illegal);//修改数据
				if(!$rr_illegal){
					M()->rollback();//事务回滚方法
					$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,违法数据写入失败'));
				}
				A('InputPc/Task','Model')->change_task_count($wx_id,'paper_inspect',1);//		
			}
			$issueId = M('tpaperissue')->add(array(
													'fpapersampleid' => $papersampleid,
													'fmediaid' => $sourceInfo['fmediaid'],
													'fissuedate' => $sourceInfo['fissuedate'],
													'fpage' => $sourceInfo['fpage'],
													'fpagetype' => $sourceInfo['fpagetype'],
													'fsize' => $fsize,//发布规格
													'fissuetype' => $fissuetype,//发布类型
													'famounts' => 0,//金额
													
													'fquantity' => 1,//数量
													'fissample' => 1,//是否样本
													'fcreator' => $nickname,//创建人
													'fcreatetime' => date('Y-m-d H:i:s'),//创建时间
													'fmodifier' => $nickname,//修改人
													'fmodifytime' => date('Y-m-d H:i:s'),//修改时间
													'sourceid' => $sourceInfo['fid'],
													'fregionid'=> M('tmedia')->cache(true,600)->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')->where(array('tmedia.fid'=>$sourceInfo['fmediaid']))->getField('tmediaowner.fregionid'),
													
														));
			//var_dump($issueId);
			if(!$issueId){
				M()->rollback();//事务回滚方法
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,发布记录表写入失败'));
			}
			//M()->rollback();//事务回滚方法
			M()->commit();//事务提交方法
			A('InputPc/Task','Model')->change_task_count($wx_id,'paper_ad_cut',1);//
			if(intval($fillegaltypecode) >= 0){//判断是否有违法判定
				set_tillegalad_data($papersampleid,'paper');//加入AGP违法广告逻辑
			}
			
            if(time() > strtotime('2018-07-11')){
                M('tpapersample')->where(array('fpapersampleid'=>$papersampleid))->save(array('finputstate'=>0,'finspectstate'=>0));
            }
			
			$taskid = A('Common/InputTask','Model')->add_task(3,$papersampleid);//添加到任务表
			
			//var_dump($papersampleid);
			
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
		}else{

			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));
		}

	}
	/*删除报纸发布*/
	public function del_issue(){
		$wx_id = intval(session('wx_id'));
		$fpaperissueid = intval(I('fpaperissueid'));//报纸样本ID

		
		$sourceUserId = M('tpaperissue')->join('tpapersource on tpapersource.fid = tpaperissue.sourceid')->where(array('tpaperissue.fpaperissueid'=>$fpaperissueid))->getField('tpapersource.fuserid');
		if($wx_id != $sourceUserId){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败,您无权删除该记录'));
		}
		
		M()->startTrans();//开启事务方法
		$fpapersampleid = M('tpaperissue')->where(array('fpaperissueid'=>$fpaperissueid))->getField('fpapersampleid');//查询样本id
		$del_sample_state = M('tpapersample')->where(array('fpapersampleid'=>$fpapersampleid))->delete();//删除样本
		if(intval($del_sample_state) == 0){
			M()->rollback();//事务回滚方法
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败,删除样本失败'));
		}
		$del_issue_state = M('tpaperissue')->where(array('fpaperissueid'=>$fpaperissueid))->delete();//删除发布记录
		if(intval($del_issue_state) == 0){
			M()->rollback();//事务回滚方法
			$this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败,删除记录失败'));
		}
		M()->commit();//事务提交方法
		A('InputPc/Task','Model')->change_task_count($wx_id,'paper_ad_cut',-1);//
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		
		
	}
	/*退回重新剪辑*/
	public function back_papersource(){
		$wx_id = intval(session('wx_id'));
		$fid = I('fid');//广告样本ID
		$fid = intval($fid);//转为数字
		$where = array();
		$where['fuserid'] = $wx_id;
		$where['fid'] = $fid;
		
		if(M('tpapersource')->where($where)->getField('fstate') == 2){//判断报纸状态是否等于2
			A('InputPc/Task','Model')->change_task_count($wx_id,'paper_page_cut',-1);//剪辑版面减1
		}
		$e_data = array();
		$e_data['fuserid'] = 0;
		$e_data['operation_time'] = 0;
		$e_data['fstate'] = 0;
		
		
		$rr = M('tpapersource')->where($where)->save($e_data);
		if($rr > 0){
				
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败'));
		}
		
	}
	/*提交版面*/
	public function submit_papersource(){
		$wx_id = intval(session('wx_id'));
		$fid = I('fid');//广告样本ID
		$fid = intval($fid);//转为数字
		$where = array();
		$where['fuserid'] = $wx_id;
		$where['fid'] = $fid;
		$where['fstate'] = array('neq',2);
		
		$e_data = array();
		
		$e_data['operation_time'] = 0;
		$e_data['fstate'] = 2;
		$e_data['fmodifytime'] = date('Y-m-d H:i:s');
		
		
		
		$rr = M('tpapersource')->where($where)->save($e_data);
		//var_dump(M('tpapersource')->getLastSql());
		
		
		

		
		if($rr > 0){
			A('InputPc/Task','Model')->change_task_count($wx_id,'paper_page_cut',1);//剪辑版面加1
			$next_task = $this->get_next_task();
			if($next_task){
				$next_url = U('InputPc/PaperCut/ad_cut',array('fid'=>$next_task['fid']));
			}
			$this->ajaxReturn(array('code'=>0,'msg'=>'执行成功','next_url'=>strval($next_url)));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'执行失败','next_url'=>''));
		}
	}
	/*获取下一个任务*/
    private function get_next_task(){
		
		$wx_id = intval(session('wx_id'));	
		
		$paper_cut_get_next_task = cookie('paper_cut_get_next_task');

		$keyword = $paper_cut_get_next_task['keyword'];

		$fissuedate_s = $paper_cut_get_next_task['fissuedate_s'];
		if($fissuedate_s == ''){
			$fissuedate_s = date('Y-m-d',time()-7*86400);
			$_GET['fissuedate_s'] = $fissuedate_s;
		}	
		$fissuedate_s = date('Y-m-d',strtotime($fissuedate_s));
		
		$fissuedate_e = $paper_cut_get_next_task['fissuedate_e'];
		if($fissuedate_e == ''){
			$fissuedate_e = date('Y-m-d');
			$_GET['fissuedate_e'] = $fissuedate_e;
		}	
		$fissuedate_e = date('Y-m-d',strtotime($fissuedate_e));
		$fpage = $paper_cut_get_next_task['fpage'];//版面
		

		$where = array();
		$where['_string'] .= '(tpapersource.fstate = 0 or ';//未剪辑的
		$where['_string'] .= 'tpapersource.fuserid = '.$wx_id.') ';//自己剪辑的
		$where['_string'] .= A('InputPc/Task','Model')->paper_cut_authority($wx_id);//数据权限控制
		$where['fissuedate'] = array('between',array($fissuedate_s,$fissuedate_e));
		if($keyword != ''){
			$where['tmedia.fmedianame'] = array('like','%'.$keyword.'%');
		}

		$where['tpapersource.fstate'] = 0;
		
		if($fpage != ''){
			$where['tpapersource.fpage'] = array('like','%'.$fpage.'%');
		}
		

		$sourceInfo = M('tpapersource')
								->field('
										tpapersource.fid
										')
								->join('tmedia on tmedia.fid = tpapersource.fmediaid')
								->where($where)->find();				
		return $sourceInfo;						
		

	}
    /**
     * 获取任务
     */
    public function paperCut(){
        if (IS_POST){
            // 获取任务
            $task = $this->getNextCutTask();
            if ($task === false){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '没有新的剪辑任务'
                ]);
            }else{
                // 如果状态是6, 就是众包提的剪辑错误
                if ($task['fstate'] == 6){
                    if (strpos($task['cut_err_reason'], ' ;; ') !== false){
                        $arr = explode(' ;; ', $task['cut_err_reason']);
                        $cutErrorType = M('cut_error_reason')->cache(true)->where(['id' =>  ['IN', $arr[0]]])->getField('reason', true);
                        $cutErrorType = implode(',', $cutErrorType);
                        $task['cut_err_reason'] = '错误类型:'.$cutErrorType . ' ; 错误描述: ' . $arr[1];
                    }
                }
                $task['media_name'] = M('tmedia')->cache(true, 60)->where(['fid' => ['EQ', $task['fmediaid']]])->getField('fmedianame');
                $task['issue_date'] = date('Y-m-d', strtotime($task['fissuedate']));
                $this->ajaxReturn([
                    'code' => 0,
                    'data' => $task,
                ]);
            }
        }else{
            $this->display();
        }
    }
    public function getIssueListBySourceId($fid)
    {
        $data = M('tpaperissue')->field('fpaperissueid id, fpapersampleid sample_id')->where(['sourceid' => ['EQ', $fid]])->select();
        foreach ($data as $key => $value) {
            $sampleInfo = M('tpapersample')->field('fjpgfilename, tem_ad_name')->find($value['sample_id']);
            $data[$key]['url'] = $sampleInfo['fjpgfilename'];
            $data[$key]['adName'] = $sampleInfo['tem_ad_name'];
        }
        $this->ajaxReturn([
            'data' => $data,
        ]);
    }
    /**
     * 添加发布记录,样本,任务
     * @param Array $issue 剪切出来广告的信息
     * @param Int $sourceId
     */
    public function addPaperIssue($issue, $sourceId)
    {
        $wx_id      = intval(session('wx_id'));
        $wxInfo     = M('ad_input_user')->cache(true,86400)->field('wx_id,nickname,cutting_seniority')->where(['wx_id' => $wx_id])->find();
        $nickname   = $wxInfo['nickname'];
        $sourceInfo = M('tpapersource')->cache(true,86400)->find($sourceId);
        // 判断素材信息
        if($sourceInfo['fuserid'] != $wx_id){//判断该报纸版面是不是由自己处理
            $this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,该报纸版面目前不是由您处理'));
        }
        if($sourceInfo['fstate'] != 1 && $sourceInfo['fstate'] != 6){
            $this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,当前状态不允许添加'));
        }
        // 添加样本
        $sampleData = [
            'tem_ad_name'  => $issue['adName'],
            'fjpgfilename' => $issue['url'],
            'uuid' => MD5($issue['url']),
            'fadid'        => 0,
        ];
        $sampleData['fmediaid']        = $sourceInfo['fmediaid'];//媒介id
        $sampleData['fissuedate']      = $sourceInfo['fissuedate'];//发布日期
        $sampleData['last_issue_date'] = $sourceInfo['fissuedate'];//最后发布日期
        $sampleData['sourceid']        = $sourceInfo['fid'];
        $sampleData['fcreator']        = $nickname;//创建人
        $sampleData['fcreatetime']     = date('Y-m-d H:i:s');//创建时间
        $sampleData['fmodifier']       = $nickname;//修改人
        $sampleData['fmodifytime']     = date('Y-m-d H:i:s');//修改时间
        $sampleData['finspectstate']   = 1;//违法判定状态
        $sampleData['finputstate']     = 1;//需要众包录入广告信息
        M()->startTrans();
        $sampleId = M('tpapersample')->add($sampleData);
        $sampleUpdate = $sampleId !== false;
        if ($sampleUpdate){
            $fregionid = M('tmedia')->cache(true,86400)->where(['fid'=>$sourceInfo['fmediaid']])->getField('media_region_id');
            $paperissueData = [
                'fpapersampleid' => $sampleId,
                'fmediaid'       => $sourceInfo['fmediaid'],
                'fissuedate'     => $sourceInfo['fissuedate'],
                'fpage'          => $sourceInfo['fpage'],
                'fpagetype'      => $sourceInfo['fpagetype'],
                'fsize'          => $issue['fsize'],
                'fissuetype'     => $issue['fissuetype'],
                'famounts'       => 0,//金额
                'fquantity'      => 1,//数量
                'fissample'      => 1,//是否样本
                'fcreator'       => $nickname,//创建人
                'fcreatetime'    => date('Y-m-d H:i:s'),//创建时间
                'fmodifier'      => $nickname,//修改人
                'fmodifytime'    => date('Y-m-d H:i:s'),//修改时间
                'sourceid'       => $sourceInfo['fid'],
                'fregionid'      => $fregionid,
            ];
            $issueId = M('tpaperissue')->add($paperissueData);
            if ($issueId){
                A('InputPc/Task','Model')->change_task_count($wx_id,'paper_ad_cut',1);
                M()->commit();
                $taskid = A('Common/InputTask','Model')->add_task(3,$sampleId);//添加到任务表
                if ($taskid){
                    $logData = [
                        'sampleData' => $sampleData,
                        'sourceId'   => $sourceId,
                        'type'       => 'add',
                        'msg'        => '报纸样本及发布记录添加成功'
                    ];
                    A('Common/AliyunLog','Model')->paper_sample_log($logData);
                    $this->ajaxReturn(array('code'=>0,'msg'=>'添加成功', 'issueId' => $issueId));
                }else{
                    $logData = [
                        'sampleData' => $sampleData,
                        'sourceId'   => $sourceId,
                        'type'       => 'add',
                        'msg'        => '报纸发布记录添加失败, 已回滚'
                    ];
                    A('Common/AliyunLog','Model')->paper_sample_log($logData);
                    $this->ajaxReturn(array('code'=>-1,'msg'=>'添加任务失败2', 'issueId' => $issueId));
                }
            }
        }
        M()->rollback();
        $logData = [
            'sampleData' => $sampleData,
            'sourceId'   => $sourceId,
            'type'       => 'add',
            'msg'        => '报纸样本添加失败, 已回滚'
        ];
        A('Common/AliyunLog','Model')->paper_sample_log($logData);
        $this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));
    }
    /**
     * 添加发布记录, 样本 任务
     * @param $issue    array    剪切出来广告的信息
     * @param $sourceId
     */
    public function addPaperIssue_20190725($issue, $sourceId)
    {
        $wx_id = intval(session('wx_id'));
        $wxInfo = M('ad_input_user')->field('wx_id,nickname,cutting_seniority')->where(array('wx_id'=>$wx_id))->find();
        $nickname = $wxInfo['nickname'];
        $sourceInfo = M('tpapersource')->find($sourceId);
        // 判断素材信息
        if($sourceInfo['fuserid'] != $wx_id){//判断该报纸版面是不是由自己处理
            $this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,该报纸版面目前不是由您处理'));
        }
        if($sourceInfo['fstate'] != 1 && $sourceInfo['fstate'] != 6){
            $this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,当前状态不允许添加'));
        }
        // 添加样本
        $sampleData = [
            'tem_ad_name' => $issue['adName'],
            'fjpgfilename' => $issue['url'],
            'fadid' => 0,
        ];
        $sampleData['fmediaid'] = $sourceInfo['fmediaid'];//媒介id
        $sampleData['fissuedate'] = $sourceInfo['fissuedate'];//发布日期
        $sampleData['last_issue_date'] = $sourceInfo['fissuedate'];//最后发布日期
        $sampleData['sourceid'] = $sourceInfo['fid'];
        $sampleData['fcreator'] = $nickname;//创建人
        $sampleData['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
        $sampleData['fmodifier'] = $nickname;//修改人
        $sampleData['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
        $sampleData['finspectstate'] = 1;//违法判定状态
        $sampleData['finputstate'] = 1;//需要众包录入广告信息
        M()->startTrans();
        $sampleId = M('tpapersample')->add($sampleData);
        $sampleUpdate = $sampleId !== false;
        if ($sampleUpdate){
            $fsize = $issue['fsize'];
            $fissuetype = $issue['fissuetype'];
            $issueId = M('tpaperissue')->add(array(
                'fpapersampleid' => $sampleId,
                'fmediaid' => $sourceInfo['fmediaid'],
                'fissuedate' => $sourceInfo['fissuedate'],
                'fpage' => $sourceInfo['fpage'],
                'fpagetype' => $sourceInfo['fpagetype'],
                'fsize' => $issue['fsize'],
                'fissuetype' => $issue['fissuetype'],
                'famounts' => 0,//金额
                'fquantity' => 1,//数量
                'fissample' => 1,//是否样本
                'fcreator' => $nickname,//创建人
                'fcreatetime' => date('Y-m-d H:i:s'),//创建时间
                'fmodifier' => $nickname,//修改人
                'fmodifytime' => date('Y-m-d H:i:s'),//修改时间
                'sourceid' => $sourceInfo['fid'],
                'fregionid'=> M('tmedia')->cache(true,600)->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')->where(array('tmedia.fid'=>$sourceInfo['fmediaid']))->getField('tmediaowner.fregionid'),
            ));
            if ($issueId){
                A('InputPc/Task','Model')->change_task_count($wx_id,'paper_ad_cut',1);
                M()->commit();
                $taskid = A('Common/InputTask','Model')->add_task(3,$sampleId);//添加到任务表
                if ($taskid){
                    $logData = [
                        'sampleData' => $sampleData,
                        'sourceId' => $sourceId,
                        'type' => 'add',
                        'msg' => '报纸样本及发布记录添加成功'
                    ];
                    A('Common/AliyunLog','Model')->paper_sample_log($logData);
                    $this->ajaxReturn(array('code'=>0,'msg'=>'添加成功', 'issueId' => $issueId));
                }else{
                    $logData = [
                        'sampleData' => $sampleData,
                        'sourceId' => $sourceId,
                        'type' => 'add',
                        'msg' => '报纸发布记录添加失败, 已回滚'
                    ];
                    A('Common/AliyunLog','Model')->paper_sample_log($logData);
                    $this->ajaxReturn(array('code'=>-1,'msg'=>'添加任务失败2', 'issueId' => $issueId));
                }
            }
        }
        M()->rollback();
        $logData = [
            'sampleData' => $sampleData,
            'sourceId' => $sourceId,
            'type' => 'add',
            'msg' => '报纸样本添加失败, 已回滚'
        ];
        A('Common/AliyunLog','Model')->paper_sample_log($logData);
        $this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));
    }
    /**
     * 删除剪辑好的报纸发布记录
     * @param $issueId
     */
    public function delPaperIssue($issueId)
    {
        $wx_id = intval(session('wx_id'));

        $sourceUserId = M('tpaperissue')->join('tpapersource on tpapersource.fid = tpaperissue.sourceid')->where(array('tpaperissue.fpaperissueid'=>$issueId))->getField('tpapersource.fuserid');
        if($wx_id != $sourceUserId){
            $this->ajaxReturn(array('code'=>-1,'msg'=>'删除失败,您无权删除该记录'));
        }
        M()->startTrans();
        // 删除样本
        $fpapersampleid = M('tpaperissue')->where(array('fpaperissueid'=>$issueId))->getField('fpapersampleid');//查询样本id
        $sampleRes = M('tpapersample')->where(array('fpapersampleid'=>$fpapersampleid))->delete() === false ? false : true;//删除样本
        // 删除发布记录
        $issueRes = M('tpaperissue')->where(array('fpaperissueid'=>$issueId))->delete() === false ? false : true;//删除发布记录
        // 任务改成剪辑错误
        $taskid = M('task_input')
            ->where(['sam_id' => ['EQ', $fpapersampleid]])
            ->getField('taskid');
        $taskRes = M('task_input')
            ->where(['taskid' => ['EQ', $taskid]])
            ->save([
                'cut_err_state' => 1,
                'cut_err_reason' => '剪辑人员删除样本'
            ]) === false ? false : true;
        M('task_input_flow')->add([
            'taskid' => $taskid,
            'flow_content' => '剪辑人员删除样本',
            'flow_time' => time(),
            'wx_id' => $wx_id,
        ]);
        if ($sampleRes && $issueRes && $taskRes){
            A('InputPc/Task','Model')->change_task_count($wx_id,'paper_ad_cut',-1);//
            M()->commit();
            $logData = [
                'issueId' => $issueId,
                'paperSampleId' => $fpapersampleid,
                'type' => 'delete',
                'msg' => '报纸样本及发布记录由剪辑人员删除成功'
            ];
            A('Common/AliyunLog','Model')->paper_sample_log($logData);
            $this->ajaxReturn([
                'code' => '0',
                'msg' => '删除成功'
            ]);
        }else{
            M()->rollback();
            $logData = [
                'issueId' => $issueId,
                'paperSampleId' => $fpapersampleid,
                'type' => 'delete',
                'msg' => '报纸样本及发布记录由剪辑人员删除失败, 已回滚'
            ];
            A('Common/AliyunLog','Model')->paper_sample_log($logData);
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '删除失败'
            ]);
        }
    }
    /**
     * 标记素材剪辑完成
     * @param $sourceId
     */
    public function submitCutTask($sourceId)
    {
        $wx_id = session('wx_id');
        $sourceInfo = M('tpapersource')->find($sourceId);
        if ($sourceInfo['fuserid'] != $wx_id){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '当前任务不是你的任务'
            ]);
        }
        if ($sourceInfo['fstate'] != 1 && $sourceInfo['fstate'] != 6){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '当前任务状态非法'
            ]);
        }
        $saveData = [
            'fstate' => 2,
            'fmodifier' => M('ad_input_user')->where(['wx_id' => ['EQ', $wx_id]])->getField('alias'),
            'fmodifytime' => date('Y-m-d')
        ];
        M()->startTrans();
        $res = M('tpapersource')
            ->where([
                'fid' => ['EQ', $sourceId],
                'fuserid' => ['EQ', $wx_id],
            ])->save($saveData)=== false ? false : true;
        if ($sourceInfo['fstate'] == 6){
            $wx_id = session('wx_id');
            $time = time();
            // 找到该版面的样本id
            $sampleWhere = [
                'finputstate' => ['EQ', -1],
                'finspectstate' => ['EQ', -1],
                'sourceid' => ['EQ', $sourceId],
                'fstate' => ['EQ', -1],
            ];
            $samIds = M('tpapersample')
                ->where($sampleWhere)
                ->getField('fpapersampleid', true);
            if (!$samIds){
                $samIds = ['0'];
            }
            // 将之前影响的样本修改回来
            $sampleUpdate = M('tpapersample')
                ->where([
                    'fpapersampleid' => ['IN', $samIds]
                ])
                ->save([
                    'finputstate' => 1,
                    'finspectstate' => 1,
                    'fstate' => 1,
                ]) !== false;
            // 找到之前受影响的任务
            $taskWhere = [
                'media_class' => ['EQ', 3],
                'sam_id' => ['IN', $samIds],
                'task_state' => ['EQ', 0],
                'cut_err_state' => ['EQ', 1]
            ];
            $taskIds = M('task_input')
                ->where($taskWhere)
                ->getField('taskid', true);
            // 修改所有的的任务状态为非剪辑错误
            if ($taskIds){
                $taskUpdate = M('task_input')
                    ->where([
                        'taskid' => ['IN', $taskIds],
                    ])
                    ->save([
                        'cut_err_state' => 0,
                    ]) !== false;
            }else{
                $taskUpdate = true;
            }
            // 将修改写入任务流程表
            if ($taskIds){
                $flowData = [];
                foreach ($taskIds as $taskId) {
                    $flowData[] = [
                        'taskid' => $taskId,
                        'flow_content' => '报纸版面剪辑错误已修改, 将之前批量修改的任务剪辑状态更正为0',
                        'flow_time' => $time,
                        'wx_id' => $wx_id,
                    ];
                }
                $taskFlowRes = M('task_input_flow')->addAll($flowData);
            }else{
                $taskFlowRes = true;
            }
            $res = $taskFlowRes && $sampleUpdate && $taskUpdate && $res;
        }

        if ($res){
            M()->commit();
            if ($sourceInfo['fstate'] == 6){
                $msg = '已退回版面的重新提交成功';
            }else{
                $msg = '版面提交成功';
            }
            $logData = [
                'sourceId' => $sourceId,
                'type' => 'save',
                'msg' => $msg,
            ];
            A('Common/AliyunLog','Model')->paper_source_log($logData);
            A('InputPc/Task','Model')->change_task_count($wx_id,'paper_page_cut',1);//
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '提交成功'
            ]);
        }else{
            M()->rollback();
            $str = '版面更新';
            if ($sourceInfo['fstate'] == 6){
                if (!$taskFlowRes){
                    $str = '任务记录更新';
                }
                if (!$sampleUpdate){
                    $str = '样本更新';
                }
                if (!$taskUpdate){
                    $str = '任务更新';
                }
            }
            $logData = [
                'sourceId' => $sourceId,
                'type' => 'save',
                'msg' => $str. '失败',
            ];
            A('Common/AliyunLog','Model')->paper_source_log($logData);
            $this->ajaxReturn([
                'code' => 0,
                'msg' => $str.'失败'
            ]);
        }

    }
    /**
     * 获取下一个剪辑任务
     */
    private function getNextCutTask()
    {
        $wx_id = session('wx_id');
        // 获取权限信息
        $userGroupInfo = M('ad_input_user_group')
            ->field('ad_input_user_group.other_authority, ad_input_user_group.mediaauthority, ad_input_user_group.regionauthority, ad_input_user_group.dateauthority, ad_input_user.prefer_list')
            ->join('ad_input_user on ad_input_user.group_id = ad_input_user_group.group_id')
            ->where(array('ad_input_user.wx_id'=>$wx_id))
            ->find();
        // 检查是否有报纸权限
        $authList = explode(',', $userGroupInfo['other_authority']);
        if (!in_array('1003', $authList)){
            return false;
        }
        // 如果有正在进行的任务, 就返回
        $task = M('tpapersource')->where(['fstate' => ['EQ', 1], ['fuserid' => ['EQ', $wx_id]]])->find();
        if ($task){
            return $task;
        }
        // 如果有被退回的任务, 优先分配
        $task = M('tpapersource')->where(['fstate' => ['EQ', 6], ['fuserid' => ['EQ', $wx_id]]])->find();
        if ($task){
            return $task;
        }
        // 处理用户任务组权限
        $searchWhere = [
            'fstate' => ['EQ', 0],
            'priority' => ['EGT',0],
        ];

        $mediaIds = [];
        // 生成搜索条件及子查询
        if ($userGroupInfo['mediaauthority'] !== 'unrestricted'){
            if (!$userGroupInfo['mediaauthority'] || $userGroupInfo['mediaauthority'] === '""'){
                return false;
            }
            $mediaIds = array_merge($mediaIds, json_decode($userGroupInfo['mediaauthority'], true));
            // $searchWhere['fmediaid'] = ['IN', $mediaIds];
        }
        // 地区限制的条件
        if ($userGroupInfo['regionauthority'] !== 'unrestricted'){
            if (!$userGroupInfo['regionauthority'] || $userGroupInfo['regionauthority'] === '""'){
                return false;
            }
            $userGroupInfo['regionauthority'] = json_decode($userGroupInfo['regionauthority'], true);
            // 区分并组合查找本级的和下属的条件, 生成mediaOwner的子查询搜索条件
            $inArr = [];
            $leftArr = [];
            foreach ($userGroupInfo['regionauthority'] as $item) {
                $len = strlen($item);
                if ($len == 6){
                    $inArr[] = $item;
                }else{
                    $leftArr[] = "(LEFT(fregionid, $len) = $item)";
                }
            }
            // 搜索条件有2个部分, 一个是全部地区id的本级媒体, 一个是部分地区id本级及下属媒体,互相之间的关系应该是OR
            $_mediaOwnerWhere = [
                '_logic' => 'OR',
            ];
            if (count($inArr) != 0){
                $_mediaOwnerWhere['fregionid'] = ['IN', $inArr];
            }
            if (count($leftArr) != 0){
                $_string = implode(' OR ', $leftArr);
                $_mediaOwnerWhere['_string'] = $_string;
            }
            // 生成mediaOwner子查询
            $subQuery = M('tmediaowner')->field('fid')->where($_mediaOwnerWhere)->buildSql();
            $_mediaWhere = [
                'fmediaownerid' => ['EXP', 'IN '. $subQuery]
            ];
            $res = M('tmedia')->where($_mediaWhere)->getField('fid', true);
            // 得到允许的媒体id
            $mediaIds = array_merge($mediaIds, $res);
            // $searchWhere['fmediaid'][] = ['IN', $res];
        }
        // 发布日期限制条件
        if ($userGroupInfo['dateauthority'] !== 'unrestricted'){
            if (!$userGroupInfo['dateauthority'] || $userGroupInfo['dateauthority'] === '""'){
                return false;
            }
            $dateauthority = json_decode($userGroupInfo['dateauthority'], true);
            $dateWhere = [];
            foreach ($dateauthority as $key => $value) {
                $dateWhere[] = ['BETWEEN', explode(' , ', $value)];
            }
            $dateWhere[] = 'OR';
            // $searchWhere['fissuedate'] = $dateWhere;
        }
        // 加入媒体偏好元素
        // 获取媒体偏好
        $mediaPrefer = $userGroupInfo['prefer_list'];
        if ($mediaPrefer){
            $mediaPrefer = explode(',', $mediaPrefer);
            if ($userGroupInfo['mediaauthority'] !== 'unrestricted') {
                $preferList = array_intersect($mediaPrefer, $mediaIds);
            }else{
                $preferList = $mediaPrefer;
            }
            $preferList = implode(',', $preferList);
        }
        if (!$preferList){
            $preferList = "''";
        }
        $while_num = 0;
        $e_task_state = false;
        while(!$e_task_state){
            $while_num++;
            // $task = M('tpapersource')->where($searchWhere)->order("field(fmediaid, $preferList) desc, priority desc, fissuedate desc")->find();
            $task = M('tpapersource')->where($searchWhere)->order("priority desc, fissuedate desc")->find();
            $task['fstate'] = 1;
            $task['fuserid'] = $wx_id;
            $task['operation_time'] = time();
            if(!$task) return false;//如果没有获取到任务，就返回false
            $e_task_state = M('tpapersource')
                ->where(array('fstate'=>0,'fid'=>$task['fid']))
                ->save(array(
                        'fuserid'=>$wx_id,
                        'fstate'=>1,
                        'operation_time'=> time())
                );
            if($e_task_state){
                $logData = [
                    'sourceId' => $task['fid'],
                    'type' => 'save',
                    'msg' => '领取任务成功',
                ];
                A('Common/AliyunLog','Model')->paper_source_log($logData);
                return $task;
            }
            if($while_num > 10) return false;//如果抢了10次还没有任务，那就返回false
        }
    }
    /**
     * 报纸素材列表
     */
    public function paperSourceList()
    {
        if (IS_POST){
            $page = I('page', 1);
            $where = I('where');
            $mediaWhere = [];
            $sourceWhere = [];
            if ($where['media_name'] != ''){
                $mediaWhere['fmedianame'] = ['LIKE', "%{$where['media_name']}%"];
            }
            if ($where['page'] != ''){
                $sourceWhere['fpage'] = ['LIKE', "%{$where['page']}%"];
            }
            if ($where['fstate'] != ''){
                $sourceWhere['fstate'] = ['EQ', $where['fstate']];
            }
            if ($where['timeRangeArr'] != ''){
                $sourceWhere['fissuedate'] = ['BETWEEN', $where['timeRangeArr']];
            }
            if ($mediaWhere){
                $mediaSubQuery = M('tmeida')
                    ->field('fid')
                    ->where($mediaWhere)
                    ->buildSql();
                $sourceWhere['fmediaid'] = ['EXP', ' IN ' . $mediaSubQuery];
            }
            $table = M('tpapersource')
                ->where($sourceWhere)
                ->page($page, 10)
                ->select();
            $stateArr = [
                '0' => '待处理',
                '1' => '正在剪辑',
                '2' => '已完成',
            ];
            foreach ($table as $key => $value) {
                $table[$key]['media_name'] = M('tmedia')->cache(true, 60)->where(['fid' => $value['fmediaid']])->getField('fmedianame');
                $table[$key]['media_name'] = r_highlight($table[$key]['media_name'], $where['media_name'], 'red');
                $table[$key]['issue_date'] = date('Y-m-d', strtotime($value['fissuedate']));
            }
            $total = M('tpapersource')
                ->where($sourceWhere)
                ->count();
            $this->ajaxReturn(compact('total', 'table'));
        }else{
            $this->display();
        }
    }
    /**
     * 提交报纸版面错误
     * @param $id
     */
    public function pageError($id)
    {
        $wx_id = session('wx_id');
        $sourceInfo = M('tpapersource')->field('fstate, fuserid')->find($id);
        if ($sourceInfo['fuserid'] != $wx_id){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '当前任务不是你的任务'
            ]);
        }
        if ($sourceInfo['fstate'] != 1 && $sourceInfo['fstate'] != 6){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '当前任务状态非法'
            ]);
        }

        $res = M('tpapersource')
            ->where([
                'fid' => ['EQ', $id],
                'fuserid' => ['EQ', $wx_id]
            ])
            ->save([
                'fstate' => 10,
                'fmodifier' => M('ad_input_user')->where(['wx_id' => ['EQ', $wx_id]])->getField('alias'),
                'fmodifytime' => date('Y-m-d H:i:s')
            ]) === false ? false : true;
        if ($res){
            $logData = [
                'sourceId' => $id,
                'type' => 'save',
                'msg' => '提交版面错误成功',
            ];
            A('Common/AliyunLog','Model')->paper_source_log($logData);
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '提交成功'
            ]);
        }else{
            $logData = [
                'sourceId' => $id,
                'type' => 'save',
                'msg' => '提交版面错误失败',
            ];
            A('Common/AliyunLog','Model')->paper_source_log($logData);
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '提交失败'
            ]);
        }
    }
    /**
     * 提交报纸图片错误
     * @param $id
     */
    public function imageError($id)
    {
        $wx_id = session('wx_id');
        $sourceInfo = M('tpapersource')->field('fstate, fuserid')->find($id);
        if ($sourceInfo['fuserid'] != $wx_id){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '当前任务不是你的任务'
            ]);
        }
        if ($sourceInfo['fstate'] != 1  && $sourceInfo['fstate'] != 6){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '当前任务状态非法'
            ]);
        }

        $res = M('tpapersource')
            ->where([
                'fid' => ['EQ', $id],
                'fuserid' => ['EQ', $wx_id]
            ])
            ->save([
                'fstate' => 11,
                'fmodifier' => M('ad_input_user')->where(['wx_id' => ['EQ', $wx_id]])->getField('alias'),
                'fmodifytime' => date('Y-m-d H:i:s')
            ]) === false ? false : true;
        if ($res){
            $logData = [
                'sourceId' => $id,
                'type' => 'save',
                'msg' => '提交图片错误成功',
            ];
            A('Common/AliyunLog','Model')->paper_source_log($logData);
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '提交成功'
            ]);
        }else{
            $logData = [
                'sourceId' => $id,
                'type' => 'save',
                'msg' => '提交图片错误失败',
            ];
            A('Common/AliyunLog','Model')->paper_source_log($logData);
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '提交失败'
            ]);
        }
    }
    /**
     * 获取版面规格列表
     */
    public function getPaperSizeList(){
        $list = A('InputPc/PaperCut','Model')->getPaperSizeList();
        $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$list]);
    }

}