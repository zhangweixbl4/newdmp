<?php
namespace Admin\Model;

class FunctionModel{
	/**
	 * 获取机构列表
	 * $pid 父id，$isbm是否列出部门
	 * @return array|string data-数据
	 * by zw
	 */
    public function m_alljglist($pid = 0, $isbm = 0){
    	if(empty($isbm)){
    		$wheres = ' and fkind=1';
    	}
		$datalist = M('tregulator')
			->field('tregulator.fid,tregulator.fname,a.acount')
			->join('(select fpid as pid,count(*) as acount from tregulator where 1=1 '.$wheres.' group by fpid) a on tregulator.fid=a.pid','left')
			->where('tregulator.fpid='.$pid.$wheres.' and tregulator.fstate=1')
			->order('tregulator.fkind desc')
			->select();
		foreach ($datalist as $key => $value) {
			$datas[$key] 	= $value;
			if(!empty($value['acount'])){
				$data 		= $this->m_alljglist($value['fid'],$isbm);
				if(!empty($data)){
					$datas[$key]['children'] = $data;
				}
			}
		}
		return $datas;
    }

    /**
	 * 获取当前单位所属机构
	 * $fid 机构id
	 * @return array|string $count 总数  $data 下级数据
	 * by zw
	 */
	public function get_tregulatoraction($fid){
		$datatl=M('tregulator')->field('fpid,fkind')->where('fid='.$fid.' and fstate=1')->find();
		if($datatl['fkind']==1){
			$data = $fid;
		}else{
			$data = $this->get_tregulatoraction($datatl['fpid']);
		}
		return $data;
	}

	/**
	 * 获取机构级别
	 * by zw
	 */
	function get_tregionlevel($area){
        //判断机构级别，0区级，10市级，20省级，30国家级
        $region_id_rtrim = rtrim($area,'00');//去掉地区后面两位0，判断区
        $region_id_rtrim = rtrim($region_id_rtrim,'000');//去掉后面三位0，判断全国
        $region_id_rtrim = rtrim($region_id_rtrim,'00');//去掉后面两位0，判断市
        $tregion_len = strlen($region_id_rtrim);//地区ID去掉00后还剩几位
        return $tregion_len != 3 ?$tregion_len:4;
    }
}