<?php
namespace Api\Controller;
use Think\Controller;
class AhgyController extends Controller {
	
    public function tv(){

		$tv = '{"msg":"成功","data":[{"wcl":464.42,"sjl":16.37,"rjl":0.53,"mtmc":"亳州都市频道","jhl":3.53},{"wcl":321.71,"sjl":9.53,"rjl":0.31,"mtmc":"阜阳新闻综合","jhl":2.96},{"wcl":279.45,"sjl":9.3,"rjl":0.3,"mtmc":"合肥文体博览","jhl":3.33},{"wcl":142.29,"sjl":8.1,"rjl":0.26,"mtmc":"安徽导视频道","jhl":5.69},{"wcl":729.13,"sjl":7.47,"rjl":0.24,"mtmc":"安徽公共频道","jhl":1.02},{"wcl":674.91,"sjl":6.92,"rjl":0.22,"mtmc":"安徽卫视","jhl":1.02},{"wcl":345.8,"sjl":6.68,"rjl":0.22,"mtmc":"安徽人物频道","jhl":1.93},{"wcl":78.02,"sjl":5.34,"rjl":0.17,"mtmc":"安徽公共频道","jhl":6.84},{"wcl":120.9,"sjl":4.22,"rjl":0.14,"mtmc":"滁州科教频道","jhl":3.49},{"wcl":91.12,"sjl":4.17,"rjl":0.13,"mtmc":"马鞍山新闻综合频道","jhl":4.57}],"isSuccess":true}';
		// var_dump(json_encode($data));
		echo $tv;
		
	}

	public function bc(){

		$bc = '{"msg":"成功","data":[{"wcl":597.76,"sjl":18.01,"rjl":0.58,"mtmc":"新城资讯广播FM88.1","jhl":3.01},{"wcl":566.47,"sjl":9.84,"rjl":0.32,"mtmc":"合肥故事广播FM98.8","jhl":1.74},{"wcl":74.46,"sjl":6.28,"rjl":0.2,"mtmc":"安徽交通广播FM90.8","jhl":8.44},{"wcl":548.68,"sjl":5.08,"rjl":0.16,"mtmc":"马鞍山交通音乐广播","jhl":0.93},{"wcl":148.95,"sjl":3.39,"rjl":0.11,"mtmc":"滁州综合广播","jhl":2.28},{"wcl":44.36,"sjl":3.34,"rjl":0.11,"mtmc":"宣城交通文艺FM106.1","jhl":7.53},{"wcl":105.16,"sjl":2.87,"rjl":0.09,"mtmc":"亳州新闻综合广播","jhl":2.73},{"wcl":204.44,"sjl":2.52,"rjl":0.08,"mtmc":"宿州交通广播","jhl":1.23},{"wcl":35.02,"sjl":2.48,"rjl":0.08,"mtmc":"蚌埠新闻广播FM107.9","jhl":7.08},{"wcl":30.05,"sjl":2.28,"rjl":0.07,"mtmc":"阜阳FM90","jhl":7.57}],"isSuccess":true}';

		echo $bc;

	}

	public function paper(){

		$paper = '{"msg":"成功","data":[{"wcl":282.06,"sjl":5.74,"rjl":0.19,"mtmc":"临泉报","jhl":2.04},{"wcl":29.03,"sjl":5.25,"rjl":0.17,"mtmc":"瑶海报","jhl":18.08},{"wcl":2573.53,"sjl":3.5,"rjl":0.11,"mtmc":"庐阳","jhl":0.14},{"wcl":70.7,"sjl":3.25,"rjl":0.1,"mtmc":"安庆日报","jhl":4.6},{"wcl":17.36,"sjl":2.25,"rjl":0.07,"mtmc":"太湖周刊","jhl":12.96},{"wcl":135.14,"sjl":1,"rjl":0.03,"mtmc":"黄山日报","jhl":0.74},{"wcl":100.2,"sjl":0.5,"rjl":0.02,"mtmc":"第二附属医院","jhl":0.5},{"wcl":0,"sjl":0,"rjl":0,"mtmc":"今日青阳","jhl":0.01},{"wcl":0,"sjl":0,"rjl":0,"mtmc":"亳州晚报","jhl":9.91},{"wcl":0,"sjl":0,"rjl":0,"mtmc":"肥东晨报","jhl":0.37},{"wcl":0,"sjl":0,"rjl":0,"mtmc":"金周刊","jhl":7.31},{"wcl":0,"sjl":0,"rjl":0,"mtmc":"亳州新报","jhl":0.4},{"wcl":0,"sjl":0,"rjl":0,"mtmc":"铜都晨刊","jhl":2.31},{"wcl":0,"sjl":0,"rjl":0,"mtmc":"铜陵日报","jhl":0.24}],"isSuccess":true}';

		echo $paper;

	}

}