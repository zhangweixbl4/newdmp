<?php
namespace InputPc\Model;



class MenuModel{


    /*获取菜单*/
    public function menu_list(){
		$menu_list = array();
		if(	A('InputPc/Task','Model')->user_other_authority('1001') || A('InputPc/Task','Model')->user_other_authority('1002')){	
			if(A('InputPc/Task','Model')->user_other_authority('2001')){
				$menu_list[] = array(
									'menu_name'=>'电视录入/初审',
									'menu_url'=>'InputPc/Tvsam/index',
									'icon'=>'0',
									);
			}	
			if(A('InputPc/Task','Model')->user_other_authority('2002')){
				$menu_list[] = array(
									'menu_name'=>'广播录入/初审',
									'menu_url'=>'InputPc/Bcsam/index',
									'icon'=>'0',
									);
			}
			if(A('InputPc/Task','Model')->user_other_authority('2003')){
				$menu_list[] = array(
									'menu_name'=>'报纸录入/初审',
									'menu_url'=>'InputPc/Papersam/index',
									'icon'=>'0',
									);
			}
			if(A('InputPc/Task','Model')->user_other_authority('2004')){	
				$menu_list[] = array(
									'menu_name'=>'网络广告录入/初审',
									'menu_url'=>'InputPc/Netsam/index',
									'icon'=>'0',
									);
			}					
								
		}



		
		
							
		if(	A('InputPc/Task','Model')->user_other_authority('1003')){					
			$menu_list[] = array(
								'menu_name'=>'报纸剪辑',
								'menu_url'=>'InputPc/PaperCut/paperCut',
								'icon'=>'0',
								);
		}					
		if(	A('InputPc/Task','Model')->user_other_authority('1004')){			
			$menu_list[] = array(
								'menu_name'=>'报纸上传',
								'menu_url'=>'InputPc/PaperSource/index',
								'icon'=>'0',
								);
		}

        $menu_list[] = array(
            'menu_name'=>'违法线索审核众包平台',
            'menu_url'=>'InputPc/index/skipNewPlatform',
            'icon'=>'0',
        );

        return $menu_list;
    }



}