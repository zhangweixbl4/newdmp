<?php
namespace Gongshang\Controller;
use Think\Controller;

class AdreviewController extends BaseController
{
	public function index()
	{
		$this->display();
	}
	public function retaskDetails()
	{
		$IllegalList = A('Common/Illegal','Model')->get_illegal_list();//违法表现数据列表
		$this->assign('IllegalList',$IllegalList);//违法表现数据列表
		$this->display();
	}
}