<?php
namespace Gongshang\Controller;
use Think\Controller;

/**
 * Class MonitorExchangeController
 * @package Gongshang\Controller
 * 监测交流
 */
class MonitorExchangeController extends BaseController
{
	/*接收通告列表*/
	public function cList()
	{
		//todo 咨询列表
		$p = I('p', 1);//当前第几页
		$pp = 20;//每页显示多少记录
		$report_type = I('report_type');//1，已收   2 已发
		$is_read = I('is_read');//1,全部  2 已读 3未读
		$data=array();
		if($report_type == 1){
			if($is_read==1) {
				$count = M('exchange_receive')
					->where(array('exchange_receive_gov_id' => session('regulatorpersonInfo.fregulatorid')))
					->count();// 查询满足要求的总记录数

				$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数

				$data = M('exchange_receive')
					->field('exchange.*,exchange_receive.id as receive_id ')
					->join('exchange on exchange.exchange_id = exchange_receive.exchange_receive_id', 'LEFT')//通知主表
					->where(array('exchange_receive_gov_id' => session('regulatorpersonInfo.fregulatorid')))
					->order('exchange_receive.id desc')
					->limit($Page->firstRow . ',' . $Page->listRows)
					->select();//查询违法广告
			}
			if($is_read==2) {

				$receive_data = M('exchange_receive')->where(array('exchange_receive_gov_id'=>session('regulatorpersonInfo.fregulatorid')))->select();//本部门的通告

				foreach($receive_data as $k=> $v){
					$arr_uid = explode(",",$v['exchange_read_user_ids']);
					if(in_array(session('regulatorpersonInfo.fid'),$arr_uid)){
						$receive_id[]=$v['id']; //已读
					}
				}
				$count=count($receive_id);
				$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数
				if($count){
					$data = M('exchange_receive')
						->field('exchange.*,exchange_receive.id as receive_id ')
						->join('exchange on exchange.exchange_id = exchange_receive.exchange_receive_id', 'LEFT')//通知主表
						->where(array('exchange_receive_gov_id' => session('regulatorpersonInfo.fregulatorid')))
						->where(array('id' => array('in', $receive_id)))
						->order('exchange_receive.id desc')
						->limit($Page->firstRow . ',' . $Page->listRows)
						->select();//查询违法广告
				}

			}
			//todo
			if($is_read==3){
				$receive_data = M('exchange_receive')->where(array('exchange_receive_gov_id'=>session('regulatorpersonInfo.fregulatorid')))->select();//本部门的通告

				foreach($receive_data as $k=> $v){
					$arr_uid = explode(",",$v['exchange_read_user_ids']);
					if(!in_array(session('regulatorpersonInfo.fid'),$arr_uid)){
						$receive_id[]=$v['id']; //未读的id
					}
				}
				$count=count($receive_id);
				$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数
				if($count){
					$data = M('exchange_receive')
						->field('exchange.*,exchange_receive.id as receive_id ')
						->join('exchange on exchange.exchange_id = exchange_receive.exchange_receive_id', 'LEFT')//通知主表
						->where(array('exchange_receive_gov_id' => session('regulatorpersonInfo.fregulatorid')))
						->where(array('id' => array('in', $receive_id)))
						->order('exchange_receive.id desc')
						->limit($Page->firstRow . ',' . $Page->listRows)
						->select();//查询违法广告
				}
			}

		}elseif($report_type == 2){
			$count = M('exchange')
				->where(array('exchange_gov_id' => session('regulatorpersonInfo.fregulatorid')))
				->count();// 查询满足要求的总记录数

			$Page = new \Think\Page($count, $pp);// 实例化分页类 传入总记录数和每页显示的记录数

			$data = M('exchange')
				->where(array('exchange_gov_id' => session('regulatorpersonInfo.fregulatorid')))
				->order('exchange_id desc')
				->limit($Page->firstRow . ',' . $Page->listRows)
				->select();//查询违法广告
		}
		$this->assign('page', $Page->show());//分页输出
		$this->assign('data', $data); //列表信息
		$this->display('MonitorExchangeList');
		// var_dump($_SESSION);
	}

	/*添加交流*/
	public function cadd()
	{
		//所有机构信息
		$list =M('tregulator')->where(array('fid'=>array('neq',session('regulatorpersonInfo.fregulatorid'))))->select();
		$this->assign('list',$list);//联席人员名单
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->display('MonitorExchangeEdit');
	}

	/*添加交流AJAX*/
	public function ajax_addexchange(){

		$model=M("exchange");
		$model->startTrans();//开启事务
		$receive_model=M("exchange_receive");
		$receive_model->startTrans();//开启事务
		$lx_uploads_file_model=M("lx_uploads_file");
		$lx_uploads_file_model->startTrans();//开启事务

		//1，添加通知
		$status = I('status');//1 公开 2，私有
		$data['exchange_content'] = I('exchange_content');
		$data['exchange_title'] = I('exchange_title');
		$data['exchange_gov_id'] = session('regulatorpersonInfo.fregulatorid');//通知发送部门id
		$data['exchange_gov'] = session('regulatorpersonInfo.regulatorname');//通知发送部门
		$data['exchange_post_time'] = date('Y-m-d H:i:s');
		$data['exchange_user_id'] = session('regulatorpersonInfo.fid');///建议发送人ID
		$data['exchange_username'] = session('regulatorpersonInfo.fname');///建议发送人
		if($status=='1'){
			$count =M('tregulator')->where(array('fid'=>array('neq',session('regulatorpersonInfo.fregulatorid'))))->count();

		}elseif($status=='2'){
			$arr_memberid=I('fmemberid');//选择发送的机构id
			$count=count($arr_memberid);
		}
		if(empty($count)){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有接收交流的机构！'));
		}
		$data['exchange_requie_times'] = $count;
		$reg =  $model->add($data);

		//2，添加通知接收
		if($status=='1'){
			$arr_memberid = M("tregulator")->where(array('fid'=>array('neq',session('regulatorpersonInfo.fregulatorid'))))->getField('fid',true);//接收对应人员
		}
		foreach ($arr_memberid as $k => $v) {
			$dataList[] = array('exchange_receive_id' => $reg,'exchange_receive_gov_id' =>$v);
		}
		$res = $receive_model->addAll($dataList);

		if($reg && $res){
			$model->commit();//成功则提交
			$receive_model->commit();//成功则提交
		}else{
			$model->rollback();
			$receive_model->rollback();
			$this->ajaxReturn(array('code'=>-1,'msg'=>'提交失败！'));

		}

		//附件
		if(I('attachinfo')){
			$attachinfo = I('attachinfo');
			foreach ($attachinfo as $key => $value) {
				if($value['id'] == '') $attachlist[] = $value;
			}
			if($attachlist){
				$attach_data['user_id'] = session('regulatorpersonInfo.fid');
				$attach_data['modelname'] ='交流';
				$attach_data['modelid'] =$reg;
				$attach_data['ffilename'] = ' ';
				$attach_data['fuploadtime'] = date('Y-m-d H:i:s',time());
				foreach ($attachlist as $key => $value) {
					$attach_data['fattachname'] = $value['name'];
					$attach_data['fattachurl'] = $value['url'];
					$attach_data['ffilename'] = preg_replace('/\..*/','',$value['url']);
					$attach_data['ffiletype'] = preg_replace('/.*\./','',$value['url']);
					$attach[$key] = $attach_data;
				}
				$attachid = $lx_uploads_file_model->addAll($attach);
				if($attachid){
					$lx_uploads_file_model->commit();//成功则提交
				}else{
					$lx_uploads_file_model->rollback();
					$this->ajaxReturn(array('code'=>-1,'msg'=>'附件添加失败！'));

				}
			}
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'发送成功！'));
	}

	/*已收详情*/
	public function details()
	{
		if(I('receive_id')){
			$receive_id = I('receive_id');//接收id
		}else{
			$this->error('缺失receive_id');
		}

		/*1,埋点 修改exchange_receive*/
		$receive_data = M('exchange_receive')->where(array('id'=>$receive_id))->find();//当前接收详情
		$exchange_id =$receive_data['exchange_receive_id'];//通知id

		$arr_uid = explode(",",$receive_data['exchange_read_user_ids']);
		//是否已经读过
		if(!in_array(session('regulatorpersonInfo.fid'),$arr_uid)){
			$arr['exchange_receive_status'] = 2;//已读
			if(empty($receive_data['exchange_read_user_ids'])){
				$arr['exchange_read_user_ids'] = session('regulatorpersonInfo.fid');//阅读人员id
			}else{
				$arr['exchange_read_user_ids'] = $receive_data['exchange_read_user_ids'].','.session('regulatorpersonInfo.fid');//阅读人员id ,拼接
			}
			$arr['exchange_read_user_num']=$receive_data['exchange_read_user_num']+1;//本部门已阅人总数
			if(empty($receive_data['exchange_read_log'])){
				$arr['exchange_read_log'] = date('Y-m-d H:i:s').'=>'.session('regulatorpersonInfo.fid');
			}else{
				$arr['exchange_read_log'] = $receive_data['exchange_read_log'].','.date('Y-m-d H:i:s').'=>'.session('regulatorpersonInfo.fid');//首次阅读时间拼接，与阅读人员id对应
			}
			$receive = M('exchange_receive')->where(array('id'=>$receive_id))->save($arr);

			/*2,埋点 修改exchange_receive*/
			$read_num = M('exchange_receive')
				->where(array('exchange_receive_id'=>$exchange_id,'exchange_receive_status'=>2))
				->count();// 查询满足要求的总记录数




			$exchange = M('exchange')->where(array('exchange_id'=>$exchange_id))->save(array('exchange_read_times'=>$read_num));
		}

		/*通告详情*/
		$data = M('exchange_receive')
			->field('exchange.*,
					 exchange_receive.id as receive_id,
					 exchange_receive.exchange_read_user_ids,
					 exchange_receive.exchange_read_user_num,
					 exchange_receive.exchange_read_log
					 ')
			->join('exchange on exchange.exchange_id = exchange_receive.exchange_receive_id', 'LEFT')//通知主表
			->where(array('exchange_receive.id'=>$receive_id))
			->find();//已收通告详情

		//附件信息
		$attach = M('lx_uploads_file')->where(array('modelname' =>'交流','modelid' =>$exchange_id))->select();
		$attach_count = count($attach);

		//评论
		$answer = M('exchange_answer')->where(array('exchange_id' =>$exchange_id))->select();

		$this->assign('data', $data); //违法信息
		$this->assign('attach',$attach);//附件
		$this->assign('attach_count',$attach_count);//附件数量
		$this->assign('answer',$answer);//评论
		$this->display('MonitorExchange');
	}

	/*已发详情*/
	public function sendDetails(){
		if(I('exchange_id')){
			$exchange_id = I('exchange_id');//主表id
		}else{
			$this->error('缺失exchange_id');
		}
		/*通告详情*/
		$data = M('exchange')->where(array('exchange_id'=>$exchange_id))->find();//通告详情

		//附件信息
		$attach = M('lx_uploads_file')->where(array('modelname' =>'交流','modelid' =>$exchange_id))->select();
		$attach_count = count($attach);
		//评论
		$answer = M('exchange_answer')->where(array('exchange_id' =>$exchange_id))->select();

		$this->assign('data', $data); //违法信息
		$this->assign('attach',$attach);//附件
		$this->assign('attach_count',$attach_count);//附件数量
		$this->assign('answer',$answer);//评论
		$this->display('MonitorExchange');

	}

	/*发布评论AJAX*/
	public function ajax_addComment(){
		if(I('exchange_id')){
			$exchange_id = I('exchange_id');//通知id
		}else{
			$this->error('缺失exchange_id');
		}
		$answer_model=M("exchange_answer");
		$answer_model->startTrans();//开启事务

		//1，添加评论表
		$data['exchange_id'] = $exchange_id;//建议主表id
		$data['exchange_answer_content'] = I('exchange_answer_content');//通知回复内容
		$data['exchange_answer_user_id'] = session('regulatorpersonInfo.fid');///通知评论人id
		$data['exchange_answer_user_name'] = session('regulatorpersonInfo.fname');///通知评论人姓名
		$data['exchange_answer_time'] = date('Y-m-d H:i:s');
		$data['exchange_answer_org_id'] = session('regulatorpersonInfo.regionid');//评论人机构id
		$data['exchange_answer_org_name'] = session('regulatorpersonInfo.regulatorname');//评论人机构名称
		$reg =  M("exchange_answer")->add($data);
		if($reg){
			$answer_model->commit();//成功则提交
			$this->ajaxReturn(array('code'=>0,'msg'=>'公告发送成功！'));
		}else{
			$answer_model->rollback();
			$this->ajaxReturn(array('code'=>-1,'msg'=>'提交复核失败！'));

		}

	}
	//删除附件
	public function ajax_delete_attach(){
		$file_id = I('file_id');
		if(file_id) M('lx_uploads_file')->where(array('file_id' => $file_id))->delete();
		$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
	}
}