<?php
namespace Api\Model;
use Think\Exception;
use Think\Log;

/**
* 鸿茅药酒发布清单数据
*/
class HmyjModel{

	/**
	* 根据日期媒介分批获取发布记录并写入鸿茅药酒发布清单表
	* @Param	String $month_date 日期 。如"2018-06-03"
	* @Param	Int media_id 媒体ID
	* @Return	void 返回的数据
	*/
    public function Hmyj($month_date,$media_id){
		$mediaInfo = M('tmedia')
			->cache(true,86400)
			->field('tmediaowner.fregionid,fmedianame,fmediaclassid')
			->join('tmediaowner ON tmediaowner.fid = tmedia.fmediaownerid')
			->where(['tmedia.fid'=>$media_id])
			->find();
		$mediaclassid = substr($mediaInfo['fmediaclassid'],0,2);
		if($mediaclassid == '01'){
			$media_tab = 'tv';
			$table_name = 'ttvissue_'.date('Ym',strtotime($month_date)).'_'.substr($mediaInfo['fregionid'],0,2);
		}elseif($mediaclassid == '02'){
			$media_tab = 'bc';
			$table_name = 'tbcissue_'.date('Ym',strtotime($month_date)).'_'.substr($mediaInfo['fregionid'],0,2);
		}elseif($mediaclassid == '03'){
			$media_tab = 'paper';
			$table_name = 'tpaperissue';
		}else{
			return false;
		}
		$tableName = 'hmyj_issue';
		$fields = '
			fid,
			fsampleid,
			fmediaid,
			fmedianame,
			fmediaclassid,
			fregionid,
			fadid,
			fadname,
			fadclasscode,
			fadowner,
			fadownername,
			fbrand,
			fspokesman,
			fstarttime,
			fendtime,
			fissuedate,
			flength,
			fillegaltypecode,
			fillegalcontent,
			fexpressioncodes,
			fexpressions,
			fconfirmations,
			sam_source_path';
		$dataSet = M($tableName)
			->field($fields)
			->where(['fmediaid'=>$media_id,'fissuedate'=>$month_date])
			->select();//用于排重
		//条件
		$whereAd = [
			'tad.fadowner' => ['IN','11000000016943'], //广告主：内蒙古鸿茅实业股份有限公司
			'tad.fadname'  => ['LIKE','鸿茅药酒%'], //广告名称：鸿茅药酒
			'_logic'       => 'OR'
		];
		$where = [
			'tissue.fmediaid'   => $media_id,
			'tissue.fissuedate' => strtotime($month_date),
			'_complex' => $whereAd
		];

		if($media_tab == 'tv' || $media_tab == 'bc'){
			foreach($dataSet as $dataRow){
				$dataRep[$media_id.'_'.$dataRow['fstarttime']] = $dataRow;//用于排重的数据
			}
			try{
				$data2List = M($table_name)
					->alias('tissue')
					->cache(true,1200)
					->field('
						tissue.fsampleid,
						tissue.fmediaid,
						tissue.fregionid,
						tad.fadid,
						tad.fadname,
						tad.fadclasscode,
						tad.fadowner,
						tad.fbrand,
						tsample.fspokesman,
						tissue.fstarttime,
						tissue.fendtime,
						tissue.fissuedate,
						tissue.flength,
						tsample.fillegaltypecode,
						tsample.fillegalcontent,
						tsample.fexpressioncodes,
						tsample.fexpressions,
						tsample.fconfirmations,
						tsample.favifilename as sam_source_path
						')
					->join('t'.$media_tab.'sample as tsample on tsample.fid = tissue.fsampleid')
					->join('tad on tad.fadid = tsample.fadid')
					->where($where)
					->select();	
			}catch(Exception $error){
				return false;
			}
			foreach($data2List as $data2){
				$e_a_data['fsampleid']        = $data2['fsampleid']; //样本id
				$e_a_data['fmediaid']         = $data2['fmediaid']; //媒介id
				$e_a_data['fmedianame']       = $mediaInfo['fmedianame'];//媒介名称
				$e_a_data['fmediaclassid']    = $mediaInfo['fmediaclassid'];//媒介类型
				$e_a_data['fregionid']        = $data2['fregionid']; //地区id
				$e_a_data['fadid']            = $data2['fadid'];//广告名称
				$e_a_data['fadname']          = $data2['fadname'];//广告名称
				$e_a_data['fadclasscode']     = $data2['fadclasscode'];//广告类别id
				$e_a_data['fadowner']         = $data2['fadowner'];//广告主
				$e_a_data['fadownername']     = M('tadowner')->where(['fid' => $data2['fadowner']])->getField('fname') ;//广告主名称
				$e_a_data['fbrand']           = $data2['fbrand']; //品牌
				$e_a_data['fspokesman']       = $data2['fspokesman']; //代言人
				$e_a_data['fstarttime']       = $data2['fstarttime']; //发布开始时间戳
				$e_a_data['fendtime']         = $data2['fendtime']; //发布结束时间戳
				$e_a_data['fissuedate']       = date('Y-m-d',$data2['fissuedate']); //发布日期
				$e_a_data['flength']          = $data2['fendtime'] - $data2['fstarttime']; //发布时长(秒)
				$e_a_data['fillegaltypecode'] = $data2['fillegaltypecode']; //违法类型
				$e_a_data['fillegalcontent']  = strval($data2['fillegalcontent']); //'涉嫌违法内容
				$e_a_data['fexpressioncodes'] = $data2['fexpressioncodes']; //'违法表现代码，用“;”分隔
				$e_a_data['fexpressions']     = $data2['fexpressions']; //违法表现，用“;”分隔
				$e_a_data['fconfirmations']   = $data2['fconfirmations']; //认定依据
				$e_a_data['sam_source_path']  = strval($data2['sam_source_path']); //素材路径
				if(!$dataRep[$media_id.'_'.$data2['fstarttime']]){
					$fid = M($tableName)->add($e_a_data);
				}else{
					$need_save = false;
					foreach($e_a_data as $data_field => $data_value){//循环判断数据字段是否有修改
						if($data_value != $dataRep[$media_id.'_'.$data2['fstarttime']][$data_field]){
							$e_data[$data_field] = $data_value;//赋值修改字段
							$need_save = 1;//把需要修改设为1
						}
					}
					if($need_save){
						$e_state = M($tableName)
							->where([
								'fstarttime' => $data2['fstarttime'],
								'fmediaid'   => $data2['fmediaid']
								])
							->save($e_data);
					}
				}
			}
		}elseif($media_tab == 'paper'){
			foreach($dataSet as $dataRow){
				$dataRep[$media_id.'_'.$dataRow['fissuedate'].'_'.md5($dataRow['sam_source_path'])] = $dataRow;//用于排重的数据
			}
			$data2List = M($table_name)
				->alias('tissue')
				->cache(true,1200)
				->field('
						tissue.fpapersampleid as fsampleid,
						tissue.fmediaid,
						tissue.fregionid,
						tad.fadid,
						tad.fadname,
						tad.fadclasscode,
						tad.fadowner,
						tad.fbrand,
						tsample.fspokesman,
						0 as fstarttime ,
						0 as fendtime,
						tissue.fissuedate,
						0 as flength,
						tsample.fillegaltypecode,
						tsample.fillegalcontent,
						tsample.fexpressioncodes,
						tsample.fexpressions,
						tsample.fconfirmations,
						tsample.fjpgfilename as sam_source_path
						')
				->join('tpapersample as tsample on tsample.fpapersampleid = tissue.fpapersampleid')
				->join('tad on tad.fadid = tsample.fadid')
				->where($where)
				->select();	
			foreach($data2List as $data2){
				$e_a_data['fsampleid']        = $data2['fsampleid']; //样本id
				$e_a_data['fmediaid']         = $data2['fmediaid']; //媒介id
				$e_a_data['fmedianame']       = $mediaInfo['fmedianame'];//媒介名称
				$e_a_data['fmediaclassid']    = $mediaInfo['fmediaclassid'];//媒介类型
				$e_a_data['fregionid']        = $data2['fregionid']; //地区id
				$e_a_data['fadid']            = $data2['fadid'];//广告名称
				$e_a_data['fadname']          = $data2['fadname'];//广告名称
				$e_a_data['fadclasscode']     = $data2['fadclasscode'];//广告类别id
				$e_a_data['fadowner']         = $data2['fadowner'];//广告主
				$e_a_data['fadownername']     = M('tadowner')->where(['fid' => $data2['fadowner']])->getField('fname') ;//广告主名称
				$e_a_data['fbrand']           = $data2['fbrand']; //品牌
				$e_a_data['fspokesman']       = $data2['fspokesman']; //代言人
				$e_a_data['fstarttime']       = $data2['fstarttime']; //发布开始时间戳
				$e_a_data['fendtime']         = $data2['fendtime']; //发布结束时间戳
				$e_a_data['fissuedate']       = date('Y-m-d',$data2['fissuedate']); //发布日期
				$e_a_data['flength']          = $data2['fendtime'] - $data2['fstarttime']; //发布时长(秒)
				$e_a_data['fillegaltypecode'] = $data2['fillegaltypecode']; //违法类型
				$e_a_data['fillegalcontent']  = strval($data2['fillegalcontent']); //'涉嫌违法内容
				$e_a_data['fexpressioncodes'] = $data2['fexpressioncodes']; //'违法表现代码，用“;”分隔
				$e_a_data['fexpressions']     = $data2['fexpressions']; //违法表现，用“;”分隔
				$e_a_data['fconfirmations']   = $data2['fconfirmations']; //认定依据
				$e_a_data['sam_source_path']  = strval($data2['sam_source_path']); //素材路径
				if(!$dataRep[$media_id.'_'.$month_date.'_'.md5($data2['sam_source_path'])]){
					$fid = M($tableName)->add($e_a_data);
				}else{
					$need_save = false;
					foreach($e_a_data as $data_field => $data_value){//循环判断数据字段是否有修改
						if($data_value != $dataRep[$media_id.'_'.$month_date.'_'.md5($data2['sam_source_path'])][$data_field]){
							$e_data[$data_field] = $data_value;//赋值修改字段
							$need_save = 1;//把需要修改设为1
						}
					}
					if($need_save){//需要修改
						$e_state = M($tableName)
							->where([
								'fstarttime' => strtotime($data2['fstarttime']),
								'fmediaid'   => $data2['fmediaid']
								])
							->save($e_data);
					}
				}
			}
		}
	}
}