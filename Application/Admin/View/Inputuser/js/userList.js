Vue.prototype.$ELEMENT = { size: 'mini', zIndex: 3000 }
var vm = new Vue({
    el:'#app',
    data: {
        table: [],
        total: 0,
        where: {
            user_tag: []
        },
        page: 1,
        pageSize: 10,
        userTags,
        showUserInfo: false,
        showBatchModify: false,
        currentWxID: null,
        groupList,
        selectedUser: [],
    },
    components: {
        userInfo,
        userGroupInfo,
    },
    created: function(){
        this.getTable()
    },
    computed: {
        ids: function () {
            return this.selectedUser.map(v => v.wx_id)
        }
    },
    methods: {
        getTable: function () {
            this.loading = true
            $.post('', {
                where: this.where,
                page: this.page,
                pageSize: this.pageSize,
            }).then((res) => {
                this.table = res.table
                this.total = +res.total
                this.loading = false
            })
        },
        editUser: function (id) {
            this.currentWxID = id
            this.showUserInfo = true
            this.$nextTick(() => {
                this.$refs.userInfo.dataInit()
            })
        },
        resetWhere: function () {
            this.where = {
                user_tag: []
            }
            this.page = 1
            this.getTable()
        },
        pageChange: function (page) {
            this.page = page
            this.getTable()
        },
        selectUser: function (val) {
            this.selectedUser = val
        },
        handleSizeChange: function (val) {
            this.pageSize = val
            this.getTable()
        },
        barchModify: function () {
            if (this.selectedUser.length === 0){
                this.$message.warning('请至少选择一人')
            } else {
                this.showBatchModify = true
                this.$nextTick(() => {
                    this.$refs.userGroupInfo.dataInit()
                })
            }
        },
        delUsers: function () {
            if(this.selectedUser.length === 0){
                this.$message.warning('请至少选择一人')
            }else{
                this.$confirm('确认删除选中的 '+this.selectedUser.length+' 人?').then(() => {
                    this.loading = true
                    $.post('delUsers', {
                            wxIds: this.ids,
                        }).then((res) => {
                            if (res.code == 0){
                                this.$message.success(res.msg)
                            } else{
                                this.$message.error(res.msg)
                            }
                            this.loading = false
                            this.getTable()
                        })
                }).catch(() => {})
            }
        }
    },
    filters: {
        sexFilter: function (value) {
            if (value == 0){
                return '未知'
            }
            if (value == 1){
                return '男'
            }
            if (value == 2){
                return '女'
            }
        },
        timestampFilter: function (value) {
            return moment.unix(value).format('YYYY-M-D HH:mm:ss')
        },
        userTagFilter: function (value) {
            if (value == 1){
                return '正式员工'
            }
            if (value == 0){
                return '未设置'
            }
            if (value == 2){
                return '实习员工'
            }
            let res = userTagArr.filter((v) => {
                return v.value == value
            })[0]
            return `<el-button type="${res.btnType}">${res.label}</el-button>`
        },
    }
})