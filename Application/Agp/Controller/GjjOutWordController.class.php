<?php
namespace Agp\Controller;
use Think\Controller;
use Agp\Model\StatisticalReportModel;
class GjjOutWordController extends BaseController{

    /**
     * 获取历史报告列表
     * @return array|string code-状态（0成功1失败），msg-提示信息，data-数据（count-记录总数，list-列表数据）
     * by yjn
     */
    public function index() {
        session_write_close();
        header("Content-type:text/html;charset=utf-8");

        $p                  = I('page', 1);//当前第几页
        $pp                 = 20;//每页显示多少记录
        $pnname             = I('pnname');// 报告名称
        $pntype             = I('pntype');// 报告类型
        $pnfiletype         = I('pnfiletype');// 文件类型
        $pncreatetime       = I('pnendtime');//报告时间
        $system_num = getconfig('system_num');

        if (!empty($pnname)) {
            $where['pnname'] = array('like', '%' . $pnname . '%');//报告名称
        }

        if (!empty($pntype)) {
            $where['pntype'] = $pntype;//报告类型
        }else{
            $where['pntype'] = array('in',array(10,20,30));//报告类型
        }

        if (!empty($pnfiletype)) {
            $where['pnfiletype'] = $pnfiletype;//文件类型
        }

        if(!empty($pncreatetime)){
            $where['pnendtime'] = array('between',array($pncreatetime[0],$pncreatetime[1]));
        }

        $where['pntrepid'] = session('regulatorpersonInfo.fregulatorpid');
        $where['fcustomer']  = $system_num;

        $count = M('tpresentation')
            ->where($where)
            ->count();

        
        $data = M('tpresentation')
            ->where($where)
            ->order('pnendtime desc')
            ->page($p,$pp)
            ->select();
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
    }

    //报告删除接口
    public function tpresentation_delete() {

        $pnid         = I('pnid');//报告ID
        $user = session('regulatorpersonInfo');//获取用户信息
        $system_num = getconfig('system_num');
        
        $where['pnid'] = $pnid;
        $where['pntrepid'] = $user['fregulatorpid'];
        $where['fcustomer']  = $system_num;

        $data = M('tpresentation')->where($where)->delete();

        if($data > 0){
            $this->ajaxReturn(array('code'=>0,'msg'=>'删除成功！'));
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'您无权删除此报告！'));
        }
    }

    //按月报告
    public function create_month_report(){
        header("Content-type:text/html;charset=utf-8");

        $system_num = getconfig('system_num');

        $date = I('daytime','2018-07-01');

        $isfixeddata = I('isfixeddata')?I('isfixeddata'):0;//是否实时数据

        $year = substr($date,0,4);

        $strmonth = date('Y年m月',strtotime($date));

        $month = intval(date('m',strtotime($date)));

        $days = date('t', strtotime($date));

        $e_date = $year.'-'.date('m',strtotime($date)).'-'.$days;

        $strendday = $strmonth.$days.'日';

        $table_condition = substr($date,5,10);
        $previous_cycles = $this->get_previous_cycles($times_table ='tbn_ad_summary_month',$year,$table_condition);

        if($month == 1){
            $previous_year = $year-1;
            $previous_month = 12;
        }else{
            $previous_month = intval(date('m',strtotime($date))) - 1;
            $previous_year = $year;
        }

        $StatisticalReport = New StatisticalReportModel();//实例化数据统计模型

        $gjj_label_media = M('tmedia_temp')->where('ftype=1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid'))->getField('fmediaid',true);

        $tv_count = M('tmedia')->where(['fid'=>['IN',$gjj_label_media],'fmediaclassid'=>['like','01%']])->count();
        $tb_count = M('tmedia')->where(['fid'=>['IN',$gjj_label_media],'fmediaclassid'=>['like','02%']])->count();
        $tp_count = M('tmedia')->where(['fid'=>['IN',$gjj_label_media],'fmediaclassid'=>['like','03%']])->count();

        $Sampling_date = M('zongju_data_confirm')->where(['fmonth'=>$date])->getField('condition');
        $Sampling_date = json_decode($Sampling_date,true);

        $tv_date = [];
        foreach ($Sampling_date['tv'] as $tv_val){
            $tv_date[] = intval($tv_val);
        }
        sort($tv_date);
        $bc_date = [];
        foreach ($Sampling_date['bc'] as $bc_val){
            $bc_date[] = intval($bc_val);

        }
        sort($bc_date);
        $paper_date = [];
        foreach ($Sampling_date['paper'] as $paper_val){
            $paper_date[] = intval($paper_val);

        }
        sort($paper_date);
        $tv_date = implode($tv_date,'、');
        $bc_date = implode($bc_date,'、');
        $paper_date = implode($paper_date,'、');

        $system_num = getconfig('system_num');
        if($system_num != '100000'){
            $this->ajaxReturn(array('code'=>-1,'msg'=>'访问受限！'));
        }
        $user = session('regulatorpersonInfo');//获取用户信息

        //定义数据数组
        $data = [
            'dotfilename' => 'Template.dotx',
            'ossupload' => 'yes',
            'reportfilename' => md5(time()).'.docx',
            'content' => []
        ];
        $data['content'][] = [
            "type" => "text",
            "bookmark" => "方正小标宋体二号标题",
            "text" => "关于".$strmonth."广告监测情况报告"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "方正小标宋四号居中",
            "text" => "（".date('m月d日')."由浙江华治数聚科技股份有限公司提供）"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号标题",
            "text" => "一、监测范围及对象"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋四号正文",
            "text" => "截止".$strendday."，我司根据国家总局广告司监测任务的要求，共对全国所属的".count($gjj_label_media)."家电视、广播、报纸等传统媒体广告发布情况进行了监测。其中电视".$tv_count."家监测范围".$month."月".$tv_date."日；广播".$tb_count."家监测范围".$month."月".$bc_date."日；报刊".$tp_count."家监测范围".$month."月".$paper_date."日。"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号标题",
            "text" => "二、监测总体情况"
        ];

        /* 总数据 */
        $MEDIA_CLASS_DATA = $StatisticalReport->gjj_date($times_table='tbn_ad_summary_month',$fztj='fmedia_class_code',false,$year,$table_condition,$is_show_longad=2,$is_show_fsend=2,$isfixeddata);
        $all_times = 0 ;
        $all_illegal_times = 0 ;
        $all_fad_play_len = 0 ;
        $all_fad_illegal_play_len = 0 ;
        foreach ($MEDIA_CLASS_DATA as $MEDIA_CLASS_DATA_KEY =>$MEDIA_CLASS_DATA_VAL){
            $all_times += $MEDIA_CLASS_DATA_VAL['fad_times'];
            $all_illegal_times += $MEDIA_CLASS_DATA_VAL['fad_illegal_times'];
            $all_fad_play_len += $MEDIA_CLASS_DATA_VAL['fad_play_len'];
            $all_fad_illegal_play_len += $MEDIA_CLASS_DATA_VAL['fad_illegal_play_len'];
        }
        $all_illegal_times_rate = (round($all_illegal_times/$all_times,4)*100);
        $all_fad_illegal_play_len_rate = (round($all_fad_illegal_play_len/$all_fad_play_len,4)*100);

//上月
/*        $PRV_MEDIA_CLASS_DATA = $StatisticalReport->gjj_date($times_table='tbn_ad_summary_month',$fztj='fmedia_class_code',false,$previous_year,$previous_cycles,$is_show_longad=2,$is_show_fsend=2,$isfixeddata);
        $prv_all_times = 0 ;
        $prv_all_illegal_times = 0 ;
        $prv_all_fad_play_len = 0 ;
        $prv_all_fad_illegal_play_len = 0 ;
        foreach ($PRV_MEDIA_CLASS_DATA as $PRV_MEDIA_CLASS_DATA_KEY =>$PRV_MEDIA_CLASS_DATA_VAL){
            $prv_all_times += $PRV_MEDIA_CLASS_DATA_VAL['fad_times'];
            $prv_all_illegal_times += $PRV_MEDIA_CLASS_DATA_VAL['fad_illegal_times'];
            $prv_all_fad_play_len += $PRV_MEDIA_CLASS_DATA_VAL['fad_play_len'];
            $prv_all_fad_illegal_play_len += $PRV_MEDIA_CLASS_DATA_VAL['fad_illegal_play_len'];
        }

        $prv_all_illegal_times_rate = (round($prv_all_illegal_times/$prv_all_times,4)*100);
        $prv_all_fad_illegal_play_len_rate = (round($prv_all_fad_illegal_play_len/$prv_all_fad_play_len,4)*100);

        $illegal_times_hb = $all_illegal_times_rate-$prv_all_illegal_times_rate;
        $times_hb = $all_times-$prv_all_times;

        if($illegal_times_hb > 0){
            $hb_times_rate = "增长".$illegal_times_hb;
        }else{
            $illegal_times_hb = $illegal_times_hb*-1;
            $hb_times_rate = "下降".$illegal_times_hb;
        }

        if($times_hb > 0){
            $hb_times = "涨幅".(round($times_hb/$prv_all_times,4)*100);
        }else{
            $times_hb = $times_hb*-1;
            $hb_times = "降幅".(round($times_hb/$prv_all_times,4)*100);
        }

        $illegal_play_len_hb = $all_fad_illegal_play_len-$prv_all_fad_illegal_play_len_rate;

        if($illegal_play_len_hb > 0){

        }else{

        }*/


        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋四号正文",
            "text" => $strmonth."，共监测到电视、广播、报纸发布广告".$all_times."条次，涉嫌违法广告".$all_illegal_times."条次，条次违法率为".$all_illegal_times_rate."%；电视和广播共发布广告时长".$all_fad_play_len."秒，涉嫌违法时长".$all_fad_illegal_play_len."秒，时长违法率".$all_fad_illegal_play_len_rate."%。"
        ];


        /*省级副省级计划市汇总*/
        $ALL_FLEVEL = $StatisticalReport->gjj_date($times_table='tbn_ad_summary_month',$fztj='flevel',$level = [1,2,3],$year,$table_condition,$is_show_longad=2,$is_show_fsend=2,$isfixeddata);

        $all_xzdj_data = [];
        $all_xz_fad_times = 0;
        $all_xz_fad_illegal_times = 0;
        $all_xz_fad_play_len = 0;
        $all_xz_fad_illegal_play_len = 0;
        foreach ($ALL_FLEVEL as $ALL_FLEVEL_key => $ALL_FLEVEL_VAL){
            $xz_level = [];
            $all_xz_fad_times += $ALL_FLEVEL_VAL['fad_times'];
            $all_xz_fad_illegal_times += $ALL_FLEVEL_VAL['fad_illegal_times'];
            $all_xz_fad_play_len += $ALL_FLEVEL_VAL['fad_play_len'];
            $all_xz_fad_illegal_play_len += $ALL_FLEVEL_VAL['fad_illegal_play_len'];

            switch ($ALL_FLEVEL_VAL['flevel']){
                case '1':
                    $xz_level = ['省级'];
                    break;
                case '2':
                    $xz_level = ['副省级'];
                    break;
                case '3':
                    $xz_level = ['计划单列市'];
                    break;
            }
            $all_xzdj_data[] = array_merge(
                $xz_level,
                 [
                    strval($ALL_FLEVEL_VAL['fad_times']),
                    strval($ALL_FLEVEL_VAL['fad_illegal_times']),
                    $ALL_FLEVEL_VAL['times_illegal_rate'].'%',
                    strval($ALL_FLEVEL_VAL['fad_play_len']),
                    strval($ALL_FLEVEL_VAL['fad_illegal_play_len']),
                    $ALL_FLEVEL_VAL['lens_illegal_rate'].'%'
                 ]
            );
        }
        $all_xzdj_data[] = [
            '合计',
            strval($all_xz_fad_times),
            strval($all_xz_fad_illegal_times),
            (round($all_xz_fad_illegal_times/$all_xz_fad_times,4)*100).'%',
            strval($all_xz_fad_play_len),
            strval($all_xz_fad_illegal_play_len),
            (round($all_xz_fad_illegal_play_len/$all_xz_fad_play_len,4)*100).'%'
        ];


        $data['content'][] = [
            "type" => "table",
            "bookmark" => "省副计合计",
            "data" => $all_xzdj_data
        ];

        /*        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋四号正文",
            "text" => "根据监测情况可以看出".$strmonth."涉嫌违法条次较".$previous_month."月大幅增长，".$previous_month."月涉嫌违法广告".$prv_all_illegal_times."条次，".$month."月".$all_illegal_times."条次，环比".$hb_times."%，涉嫌违法率环比".$hb_times_rate."个百分点（".$previous_month."月涉嫌违法率".$prv_all_illegal_times_rate."%），涉嫌违法广告增长的主要原因是："
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋四号正文",
            "text" => "1、根据广告司广告监测标准2018版的要求，违法广告不再区分一般违法和严重违法，统一为违法，因此在违法广告的统计数量上将会比原来的统计方式有所上升（原先只统计严重违法广告），故此涉及相关的违法率等指标也均有一定程度的上升。"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋四号正文",
            "text" => "2、医疗器械类、保健食品类、医疗服务类、房地产类、药品类违法率环比分别上升了3.11个百分点、2.56个百分点、1.94个百分点、1.59个百分点、0.65个百分点，主要违法表现为：保健食品广告未显著标明“本品不能代替药物”、《医疗广告审查证明》超过一年有效期后，未经审查仍继续发布医疗广告的。按规定必须在医疗器械广告中出现的内容，其字体和颜色不清晰可见、不易于辨认；在电视、互联网、显示屏等媒体发布时，出现时间少于5秒等；房地产广告中涉及面积的，没有表明为建筑面积或者套内面积；房地产预售、销售广告，没有载明开发企业名称、中介服务机构名称和预售或者销售许可证书号。像这类过去的一般违法表现，占现在违法的50%左右。"
        ];*/


        /*省级*/
        $SHENG = $StatisticalReport->gjj_date($times_table='tbn_ad_summary_month',$fztj='fregionid',$level = 1,$year,$table_condition,$is_show_longad=2,$is_show_fsend=2,$isfixeddata);
        $sheng_times_illegal_rate_data = $this->pxsf($SHENG,'times_illegal_rate');
        $sheng_lens_illegal_rate_data = $this->pxsf($SHENG,'lens_illegal_rate');

        $sheng_data = [];
        $sheng_data_chart = [['地域名称','总条次']];
        $sheng_all_fad_times = 0;
        $sheng_all_fad_illegal_times = 0;
        $sheng_fad_play_len = 0;
        $sheng_fad_illegal_play_len = 0;
        foreach ($sheng_times_illegal_rate_data as $sheng_data_key=>$sheng_data_val){
            //合计
            $sheng_all_fad_times += $sheng_data_val['fad_times'];
            $sheng_all_fad_illegal_times += $sheng_data_val['fad_illegal_times'];
            $sheng_fad_play_len += $sheng_lens_illegal_rate_data[$sheng_data_key]['fad_play_len'];
            $sheng_fad_illegal_play_len += $sheng_lens_illegal_rate_data[$sheng_data_key]['fad_illegal_play_len'];

            $sheng_data[] = [
                strval($sheng_data_key+1),
                $sheng_data_val['tregion_name'],
                strval($sheng_data_val['fad_times']),
                strval($sheng_data_val['fad_illegal_times']),
                $sheng_data_val['times_illegal_rate'].'%',
                $sheng_lens_illegal_rate_data[$sheng_data_key]['tregion_name'],
                strval($sheng_lens_illegal_rate_data[$sheng_data_key]['fad_play_len']),
                strval($sheng_lens_illegal_rate_data[$sheng_data_key]['fad_illegal_play_len']),
                $sheng_lens_illegal_rate_data[$sheng_data_key]['lens_illegal_rate'].'%',
            ];
            $sheng_data_chart[] = [$sheng_data_val['tregion_name'],$sheng_data_val['fad_times']];
        }
        $sheng_data[] = [
            strval(count($sheng_data)+1),
            '全部地域',
            strval($sheng_all_fad_times),
            strval($sheng_all_fad_illegal_times),
            (round($sheng_all_fad_illegal_times/$sheng_all_fad_times,4)*100).'%',
            '全部地域',
            strval($sheng_fad_play_len),
            strval($sheng_fad_illegal_play_len),
            (round($sheng_fad_illegal_play_len/$sheng_fad_play_len,4)*100).'%'
        ];

        /*副省级*/
        $FUSHENG = $StatisticalReport->gjj_date($times_table='tbn_ad_summary_month',$fztj='fregionid',$level = 2,$year,$table_condition,$is_show_longad=2,$is_show_fsend=2,$isfixeddata);
        $fusheng_times_illegal_rate_data = $this->pxsf($FUSHENG,'times_illegal_rate');
        $fusheng_lens_illegal_rate_data = $this->pxsf($FUSHENG,'lens_illegal_rate');

        $fusheng_data = [];
        $fusheng_data_chart = [['地域名称','总条次']];
        $fusheng_all_fad_times = 0;
        $fusheng_all_fad_illegal_times = 0;
        $fusheng_fad_play_len = 0;
        $fusheng_fad_illegal_play_len = 0;
        foreach ($fusheng_times_illegal_rate_data as $fusheng_data_key=>$fusheng_data_val){
            $fusheng_all_fad_times += $fusheng_data_val['fad_times'];
            $fusheng_all_fad_illegal_times += $fusheng_data_val['fad_illegal_times'];
            $fusheng_fad_play_len += $fusheng_lens_illegal_rate_data[$fusheng_data_key]['fad_play_len'];
            $fusheng_fad_illegal_play_len += $fusheng_lens_illegal_rate_data[$fusheng_data_key]['fad_illegal_play_len'];
            $fusheng_data[] = [
                strval($fusheng_data_key+1),
                $fusheng_data_val['tregion_name'],
                strval($fusheng_data_val['fad_times']),
                strval($fusheng_data_val['fad_illegal_times']),
                $fusheng_data_val['times_illegal_rate'].'%',
                $fusheng_lens_illegal_rate_data[$fusheng_data_key]['tregion_name'],
                strval($fusheng_lens_illegal_rate_data[$fusheng_data_key]['fad_play_len']),
                strval($fusheng_lens_illegal_rate_data[$fusheng_data_key]['fad_illegal_play_len']),
                $fusheng_lens_illegal_rate_data[$fusheng_data_key]['lens_illegal_rate'].'%',
            ];
            $fusheng_data_chart[] = [$fusheng_data_val['tregion_name'],$fusheng_data_val['fad_times']];
        }
        $fusheng_data[] = [
            strval(count($fusheng_data)+1),
            '全部地域',
            strval($fusheng_all_fad_times),
            strval($fusheng_all_fad_illegal_times),
            (round($fusheng_all_fad_illegal_times/$fusheng_all_fad_times,4)*100).'%',
            '全部地域',
            strval($fusheng_fad_play_len),
            strval($fusheng_fad_illegal_play_len),
            (round($fusheng_fad_illegal_play_len/$fusheng_fad_play_len,4)*100).'%'
        ];

        /*计划市*/
        $JIHUASHI = $StatisticalReport->gjj_date($times_table='tbn_ad_summary_month',$fztj='fregionid',$level = 3,$year,$table_condition,$is_show_longad=2,$is_show_fsend=2,$isfixeddata);

        $jihuashi_times_illegal_rate_data = $this->pxsf($JIHUASHI,'times_illegal_rate');
        $jihuashi_lens_illegal_rate_data = $this->pxsf($JIHUASHI,'lens_illegal_rate');

        $jihuashi_data = [];
        $jihuashi_data_chart = [['地域名称','总条次']];
        $jihuashi_all_fad_times = 0;
        $jihuashi_all_fad_illegal_times = 0;
        $jihuashi_fad_play_len = 0;
        $jihuashi_fad_illegal_play_len = 0;
        foreach ($jihuashi_times_illegal_rate_data as $jihuashi_data_key=>$jihuashi_data_val){
            $jihuashi_all_fad_times += $jihuashi_data_val['fad_times'];
            $jihuashi_all_fad_illegal_times += $jihuashi_data_val['fad_illegal_times'];
            $jihuashi_fad_play_len += $jihuashi_lens_illegal_rate_data[$jihuashi_data_key]['fad_play_len'];
            $jihuashi_fad_illegal_play_len += $jihuashi_lens_illegal_rate_data[$jihuashi_data_key]['fad_illegal_play_len'];
            $jihuashi_data[] = [
                strval($jihuashi_data_key+1),
                $jihuashi_data_val['tregion_name'],
                strval($jihuashi_data_val['fad_times']),
                strval($jihuashi_data_val['fad_illegal_times']),
                $jihuashi_data_val['times_illegal_rate'].'%',
                $jihuashi_lens_illegal_rate_data[$jihuashi_data_key]['tregion_name'],
                strval($jihuashi_lens_illegal_rate_data[$jihuashi_data_key]['fad_play_len']),
                strval($jihuashi_lens_illegal_rate_data[$jihuashi_data_key]['fad_illegal_play_len']),
                $jihuashi_lens_illegal_rate_data[$jihuashi_data_key]['lens_illegal_rate'].'%',
            ];
            $jihuashi_data_chart[] = [$jihuashi_data_val['tregion_name'],$jihuashi_data_val['fad_times']];
        }
        $jihuashi_data[] = [
            strval(count($jihuashi_data)+1),
            '全部地域',
            strval($jihuashi_all_fad_times),
            strval($jihuashi_all_fad_illegal_times),
            (round($jihuashi_all_fad_illegal_times/$jihuashi_all_fad_times,4)*100).'%',
            '全部地域',
            strval($jihuashi_fad_play_len),
            strval($jihuashi_fad_illegal_play_len),
            (round($jihuashi_fad_illegal_play_len/$jihuashi_fad_play_len,4)*100).'%'
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号居中",
            "text" => "各省级、副省级和计划单列市发布广告情况"
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号居中",
            "text" => "各省、直辖市、自治区"
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "各级单位汇总列表",
            "data" => $sheng_data
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "省级单位广告发布量占比图",
            "data" => $sheng_data_chart
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号居中",
            "text" => "各副省级"
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "各级单位汇总列表",
            "data" => $fusheng_data
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "副省级单位广告发布量占比图",
            "data" => $fusheng_data_chart
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "楷体三号居中",
            "text" => "各计划单列市"
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "各级单位汇总列表",
            "data" => $jihuashi_data
        ];

        $data['content'][] = [
            "type" => "chart",
            "bookmark" => "单列市广告发布量占比图",
            "data" => $jihuashi_data_chart
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号标题",
            "text" => "三、传统媒体广告类别监测情况"
        ];

        $AD_CLASS_DATA = $StatisticalReport->gjj_date($times_table='tbn_ad_summary_month',$fztj='fad_class_code',false,$year,$table_condition,$is_show_longad=2,$is_show_fsend=2,$isfixeddata);
        $AD_CLASS_DATA = $this->pxsf($AD_CLASS_DATA,'times_illegal_rate');
        $top_six_illegal_ad_class_names = [];
        $all_ad_class_data = [];
        foreach ($AD_CLASS_DATA as $AD_CLASS_KEY => $AD_CLASS_VAL){
            if($AD_CLASS_KEY < 6){
                $top_six_illegal_ad_class_names[] = $AD_CLASS_VAL['tadclass_name'];
            }
            $all_ad_class_data[] = [
                $AD_CLASS_VAL['tadclass_name'],
                strval($AD_CLASS_VAL['fad_times']),
                strval($AD_CLASS_VAL['fad_illegal_times']),
                $AD_CLASS_VAL['times_illegal_rate'].'%'
            ];
        }

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "仿宋四号正文",
            "text" => "根据7月监测情况看违法广告主要涉嫌违法的广告类别分别是：".implode($top_six_illegal_ad_class_names,'、')."。"
        ];

        $data['content'][] = [
            "type" => "table",
            "bookmark" => "传统媒体广告类别监测情况表",
            "data" => $all_ad_class_data
        ];

        /*省级*/
        $SHENG = $StatisticalReport->gjj_date($times_table='tbn_ad_summary_month',$fztj='fad_class_code',$level = 1,$year,$table_condition,$is_show_longad=2,$is_show_fsend=2,$isfixeddata);
        $sheng_times_illegal_rate_data = $this->pxsf($SHENG,'times_illegal_rate');
        $sheng_lens_illegal_rate_data = $this->pxsf($SHENG,'lens_illegal_rate');

        $sheng_data = [];
        $sheng_data_chart = [['类别名称','总条次']];
        $sheng_all_fad_times = 0;
        $sheng_all_fad_illegal_times = 0;
        $sheng_fad_play_len = 0;
        $sheng_fad_illegal_play_len = 0;
        foreach ($sheng_times_illegal_rate_data as $sheng_data_key=>$sheng_data_val){
            //合计
            $sheng_all_fad_times += $sheng_data_val['fad_times'];
            $sheng_all_fad_illegal_times += $sheng_data_val['fad_illegal_times'];
            $sheng_fad_play_len += $sheng_lens_illegal_rate_data[$sheng_data_key]['fad_play_len'];
            $sheng_fad_illegal_play_len += $sheng_lens_illegal_rate_data[$sheng_data_key]['fad_illegal_play_len'];

            $sheng_data[] = [
                strval($sheng_data_key+1),
                $sheng_data_val['tadclass_name'],
                strval($sheng_data_val['fad_times']),
                strval($sheng_data_val['fad_illegal_times']),
                $sheng_data_val['times_illegal_rate'].'%',
                $sheng_lens_illegal_rate_data[$sheng_data_key]['tadclass_name'],
                strval($sheng_lens_illegal_rate_data[$sheng_data_key]['fad_play_len']),
                strval($sheng_lens_illegal_rate_data[$sheng_data_key]['fad_illegal_play_len']),
                $sheng_lens_illegal_rate_data[$sheng_data_key]['lens_illegal_rate'].'%',
            ];
            $sheng_data_chart[] = [$sheng_data_val['tadclass_name'],$sheng_data_val['fad_times']];
        }
        $sheng_data[] = [
            strval(count($sheng_data)+1),
            '全部地域',
            strval($sheng_all_fad_times),
            strval($sheng_all_fad_illegal_times),
            (round($sheng_all_fad_illegal_times/$sheng_all_fad_times,4)*100).'%',
            '全部地域',
            strval($sheng_fad_play_len),
            strval($sheng_fad_illegal_play_len),
            (round($sheng_fad_illegal_play_len/$sheng_fad_play_len,4)*100).'%'
        ];

        /*副省级*/
        $FUSHENG = $StatisticalReport->gjj_date($times_table='tbn_ad_summary_month',$fztj='fad_class_code',$level = 2,$year,$table_condition,$is_show_longad=2,$is_show_fsend=2,$isfixeddata);
        $fusheng_times_illegal_rate_data = $this->pxsf($FUSHENG,'times_illegal_rate');
        $fusheng_lens_illegal_rate_data = $this->pxsf($FUSHENG,'lens_illegal_rate');

        $fusheng_data = [];
        $fusheng_data_chart = [['类别名称','总条次']];
        $fusheng_all_fad_times = 0;
        $fusheng_all_fad_illegal_times = 0;
        $fusheng_fad_play_len = 0;
        $fusheng_fad_illegal_play_len = 0;
        foreach ($fusheng_times_illegal_rate_data as $fusheng_data_key=>$fusheng_data_val){
            $fusheng_all_fad_times += $fusheng_data_val['fad_times'];
            $fusheng_all_fad_illegal_times += $fusheng_data_val['fad_illegal_times'];
            $fusheng_fad_play_len += $fusheng_lens_illegal_rate_data[$fusheng_data_key]['fad_play_len'];
            $fusheng_fad_illegal_play_len += $fusheng_lens_illegal_rate_data[$fusheng_data_key]['fad_illegal_play_len'];
            $fusheng_data[] = [
                strval($fusheng_data_key+1),
                $fusheng_data_val['tadclass_name'],
                strval($fusheng_data_val['fad_times']),
                strval($fusheng_data_val['fad_illegal_times']),
                $fusheng_data_val['times_illegal_rate'].'%',
                $fusheng_lens_illegal_rate_data[$fusheng_data_key]['tadclass_name'],
                strval($fusheng_lens_illegal_rate_data[$fusheng_data_key]['fad_play_len']),
                strval($fusheng_lens_illegal_rate_data[$fusheng_data_key]['fad_illegal_play_len']),
                $fusheng_lens_illegal_rate_data[$fusheng_data_key]['lens_illegal_rate'].'%',
            ];
            $fusheng_data_chart[] = [$fusheng_data_val['tadclass_name'],$fusheng_data_val['fad_times']];
        }
        $fusheng_data[] = [
            strval(count($fusheng_data)+1),
            '全部类别',
            strval($fusheng_all_fad_times),
            strval($fusheng_all_fad_illegal_times),
            (round($fusheng_all_fad_illegal_times/$fusheng_all_fad_times,4)*100).'%',
            '全部类别',
            strval($fusheng_fad_play_len),
            strval($fusheng_fad_illegal_play_len),
            (round($fusheng_fad_illegal_play_len/$fusheng_fad_play_len,4)*100).'%'
        ];

        /*计划市*/
        $JIHUASHI = $StatisticalReport->gjj_date($times_table='tbn_ad_summary_month',$fztj='fad_class_code',$level = 3,$year,$table_condition,$is_show_longad=2,$is_show_fsend=2,$isfixeddata);

        $jihuashi_times_illegal_rate_data = $this->pxsf($JIHUASHI,'times_illegal_rate');
        $jihuashi_lens_illegal_rate_data = $this->pxsf($JIHUASHI,'lens_illegal_rate');

        $jihuashi_data = [];
        $jihuashi_data_chart = [['类别名称','总条次']];
        $jihuashi_all_fad_times = 0;
        $jihuashi_all_fad_illegal_times = 0;
        $jihuashi_fad_play_len = 0;
        $jihuashi_fad_illegal_play_len = 0;
        foreach ($jihuashi_times_illegal_rate_data as $jihuashi_data_key=>$jihuashi_data_val){
            $jihuashi_all_fad_times += $jihuashi_data_val['fad_times'];
            $jihuashi_all_fad_illegal_times += $jihuashi_data_val['fad_illegal_times'];
            $jihuashi_fad_play_len += $jihuashi_lens_illegal_rate_data[$jihuashi_data_key]['fad_play_len'];
            $jihuashi_fad_illegal_play_len += $jihuashi_lens_illegal_rate_data[$jihuashi_data_key]['fad_illegal_play_len'];
            $jihuashi_data[] = [
                strval($jihuashi_data_key+1),
                $jihuashi_data_val['tadclass_name'],
                strval($jihuashi_data_val['fad_times']),
                strval($jihuashi_data_val['fad_illegal_times']),
                $jihuashi_data_val['times_illegal_rate'].'%',
                $jihuashi_lens_illegal_rate_data[$jihuashi_data_key]['tadclass_name'],
                strval($jihuashi_lens_illegal_rate_data[$jihuashi_data_key]['fad_play_len']),
                strval($jihuashi_lens_illegal_rate_data[$jihuashi_data_key]['fad_illegal_play_len']),
                $jihuashi_lens_illegal_rate_data[$jihuashi_data_key]['lens_illegal_rate'].'%',
            ];
            $jihuashi_data_chart[] = [$jihuashi_data_val['tadclass_name'],$jihuashi_data_val['fad_times']];
        }
        $jihuashi_data[] = [
            strval(count($jihuashi_data)+1),
            '全部类别',
            strval($jihuashi_all_fad_times),
            strval($jihuashi_all_fad_illegal_times),
            (round($jihuashi_all_fad_illegal_times/$jihuashi_all_fad_times,4)*100).'%',
            '全部类别',
            strval($jihuashi_fad_play_len),
            strval($jihuashi_fad_illegal_play_len),
            (round($jihuashi_fad_illegal_play_len/$jihuashi_fad_play_len,4)*100).'%'
        ];





        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号标题",
            "text" => "省级媒体广告类别情况表"
        ];


        $data['content'][] = [
            "type" => "table",
            "bookmark" => "广告类别情况表",
            "data" => $sheng_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号标题",
            "text" => "副省级媒体广告类别发布情况表"
        ];


        $data['content'][] = [
            "type" => "table",
            "bookmark" => "广告类别情况表",
            "data" => $fusheng_data
        ];

        $data['content'][] = [
            "type" => "text",
            "bookmark" => "黑体三号标题",
            "text" => "计划单列市媒体广告类别发布情况表"
        ];


        $data['content'][] = [
            "type" => "table",
            "bookmark" => "广告类别情况表",
            "data" => $jihuashi_data
        ];


        $report_data = json_encode($data);
        //echo $report_data;exit;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8081",
            CURLOPT_URL => C('REPORT_SERVER'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 6000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['reporttype'=>'commonreport','reportparam'=>$report_data]),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,错误信息：'.$err));
        } else {
            $response = json_decode($response,true);
            if(empty($response['ReportFileName'])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败,请重新尝试！'));
            }
            //将生成记录保存
            $focus = I('focus',0);
            $pnname = '关于'.$strmonth.'广告监测情况报告';
            $data['pnname'] 			= $pnname;
            $data['pntype'] 			= 30;
            $data['pnfiletype'] 		= 10;
            $data['pnstarttime'] 		= $date;
            $data['pnendtime'] 		    = $e_date;
            $data['pncreatetime'] 		= date('Y-m-d H:i:s');
            $data['pnurl'] 				= $response['ReportFileName'];
            $data['pnhtml'] 			= '';
            $data['pnfocus']            = $focus;
            $data['pntrepid']           = $user['fregulatorpid'];
            $data['pncreatepersonid']   = $user['fid'];
            $data['fcustomer']  = $system_num;
            $do_tn = M('tpresentation')->add($data);
            if(!empty($do_tn)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$response['ReportFileName']));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'生成失败'));
            }
        }




    }

    //获取上个周期
    public function get_previous_cycles($times_table ='tbn_ad_summary_week',$year='2018',$table_condition='17'){

        if($times_table == 'tbn_ad_summary_week'){
            $year_mod = M('tbn_ad_summary_week')->distinct(true)->getField('fweek',true);
        }else{
            $year_mod = M($times_table)->distinct(true)->getField('fdate',true);
        }
        $years = [];
        foreach ($year_mod as $year_date){
            $years[] = substr($year_date,0,4);
        }
        $years = array_unique($years);//选年
        $select_half_year = [];
        foreach ($years as $v_half_year){
            $select_half_year[] = $v_half_year.'-01-01';//$v_year.'上半年'
            $select_half_year[] = $v_half_year.'-07-01';//$v_year.'下半年';
        }

        $select_year = [];
        foreach ($years as $v_year){
            $select_year[] = $v_year.'-01-01';
        }
        switch ($times_table){
            case 'tbn_ad_summary_week':
                $cycles = $this->create_number(52);
                break;
            case 'tbn_ad_summary_half_month':
                $cycles = $this->create_month_date(true,false);
                break;
            case 'tbn_ad_summary_month':
                $cycles = $this->create_month_date(false,false);
                break;
            case 'tbn_ad_summary_quarter':
                $cycles = ['01-01','04-01','07-01','10-01'];
                break;
            case 'tbn_ad_summary_half_year':
                $cycles = $select_half_year;
                break;
            case 'tbn_ad_summary_year':
                $cycles = $select_year;
                break;
        }
        $year_array = explode('-',$table_condition);
        $cycles_count = count($cycles);
        foreach ($cycles as $cy_key => $cycle){
            if($cycle == $table_condition){
                if($times_table == 'tbn_ad_summary_half_year' || $times_table == 'tbn_ad_summary_year'){
                    if($cy_key == 0){
                        $year_array[0] -= 1;
                        $prv = explode('-',$cycles[$cycles_count-1]);
                        return $year_array[0].'-'.$prv[1].'-'.$prv[2];
                    }else{
                        return $cycles[$cy_key-1];
                    }
                }
                if($cy_key == 0){
                    $year -= 1;
                    $condition = $cycles[$cycles_count-1];
                }else{
                    $condition = $cycles[$cy_key-1];
                }
                if($times_table == 'tbn_ad_summary_week'){
                    if($condition < 10){
                        $condition = '0'.$condition;
                    }
                    return $condition;
                }else{
                    return $condition;
                }

            }
        }
    }

    public function create_month_date($half = true,$kv = true){
        $n = 0;
        $m = [];
        while ($n < 12){
            $n++;
            $str = $n;
            if($n<10){
                $str = '0'.$n;
            }
            if($kv){
                if($half){
                    $m[$str.'-01'] = $n.'月上半月';
                    if($n == 2){
                        $m[$str.'-15'] = $n.'月下半月';
                    }else{
                        $m[$str.'-16'] = $n.'月下半月';
                    }
                }else{
                    $m[$str.'-01'] = $n.'月';
                }
            }else{
                if($half){
                    $m[] = $str.'-01';
                    if($n == 2){
                        $m[] = $str.'-15';
                    }else{
                        $m[] = $str.'-16';
                    }
                }else{
                    $m[] = $str.'-01';
                }
            }

        }
        return $m;
    }

    /*
 * 排序
 * */
    public function pxsf($data = [],$score_rule = 'com_score',$is_desc = true){
        $data_count = count($data);
        for($k=0;$k<=$data_count;$k++)
        {
            for($j=$data_count-1;$j>$k;$j--){
                if($is_desc){
                    if($data[$j][$score_rule]>$data[$j-1][$score_rule]){
                        $temp = $data[$j];
                        $data[$j] = $data[$j-1];
                        $data[$j-1] = $temp;
                    }
                }else{
                    if($data[$j][$score_rule]<$data[$j-1][$score_rule]){
                        $temp = $data[$j];
                        $data[$j] = $data[$j-1];
                        $data[$j-1] = $temp;
                    }
                }

            }
        }
        return $data;
    }

}