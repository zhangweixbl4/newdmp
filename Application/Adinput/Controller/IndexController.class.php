<?php
namespace Adinput\Controller;
use Think\Controller;
class IndexController extends Controller {
	
    public function index(){
		
		 
		get_openid();
		$tvInputTask_count = M('ttvsample')->where(array('finputstate'=>1))->count();//电视录入任务数量
		$bcInputTask_count = M('tbcsample')->where(array('finputstate'=>1))->count();//广播录入任务数量
		$paperInputTask_count = M('tpapersample')->where(array('finputstate'=>1))->count();//报纸录入任务数量
		$inputTask_count = $tvInputTask_count + $bcInputTask_count + $paperInputTask_count;//录入任务数量
		
		
		$tvAuditTask_count = M('ttvsample')->where(array('finspectstate'=>1))->count();//电视审查任务数量
		$bcAuditTask_count = M('tbcsample')->where(array('finspectstate'=>1))->count();//广播审查任务数量
		$paperAuditTask_count = M('tpapersample')->where(array('finspectstate'=>1))->count();//报纸审查任务数量
		$AuditTask_count = $tvAuditTask_count + $bcAuditTask_count + $paperAuditTask_count;//审查任务数量
		
		$this->assign('inputTask_count',$inputTask_count);
		$this->assign('AuditTask_count',$AuditTask_count);
		
		$this->display();
	}

	
	
	
	

}