function initYear() {
  let now = new Date()
  let nowyear = now.getFullYear()
  const obj = {}
  for (let i = nowyear; i > 2014; i--) {
    obj[i] = `${i} 年`
  }
  return obj
}

function initWeek() {
  const tbn_ad_summary_week = {}
  for (let i = 1; i < 53; i++) {
    tbn_ad_summary_week[i] = `第 ${i} 周`
  }
  return tbn_ad_summary_week
}

const Gtime = {
  "times_table": {
    "tbn_ad_summary_day": "天",
    "tbn_ad_summary_week": "周",
    "tbn_ad_summary_half_month": "半月",
    "tbn_ad_summary_month": "月",
    "tbn_ad_summary_quarter": "季度",
    "tbn_ad_summary_half_year": "半年",
    "tbn_ad_summary_year": "年"
  },
  "year": initYear(),
  "tbn_ad_summary_week": initWeek(),
  "tbn_ad_summary_half_month": {
    "01-01": "1月上半月",
    "01-16": "1月下半月",
    "02-01": "2月上半月",
    "02-16": "2月下半月",
    "03-01": "3月上半月",
    "03-16": "3月下半月",
    "04-01": "4月上半月",
    "04-16": "4月下半月",
    "05-01": "5月上半月",
    "05-16": "5月下半月",
    "06-01": "6月上半月",
    "06-16": "6月下半月",
    "07-01": "7月上半月",
    "07-16": "7月下半月",
    "08-01": "8月上半月",
    "08-16": "8月下半月",
    "09-01": "9月上半月",
    "09-16": "9月下半月",
    "10-01": "10月上半月",
    "10-16": "10月下半月",
    "11-01": "11月上半月",
    "11-16": "11月下半月",
    "12-01": "12月上半月",
    "12-16": "12月下半月"
  },
  "tbn_ad_summary_month": {
    "01-01": "1月",
    "02-01": "2月",
    "03-01": "3月",
    "04-01": "4月",
    "05-01": "5月",
    "06-01": "6月",
    "07-01": "7月",
    "08-01": "8月",
    "09-01": "9月",
    "10-01": "10月",
    "11-01": "11月",
    "12-01": "12月"
  },
  "tbn_ad_summary_quarter": {
    "01-01": "第一季度",
    "04-01": "第二季度",
    "07-01": "第三季度",
    "10-01": "第四季度"
  },
  "tbn_ad_summary_half_year": {
    "2018-01-01": "2018上半年",
    "2018-07-01": "2018下半年",
    "2017-01-01": "2017上半年",
    "2017-07-01": "2017下半年",
    "2016-01-01": "2016上半年",
    "2016-07-01": "2016下半年",
    "2015-01-01": "2015上半年",
    "2015-07-01": "2015下半年"
  },
  "tbn_ad_summary_year": {
    "2018-01-01": "2018年",
    "2017-01-01": "2017年",
    "2016-01-01": "2016年",
    "2015-01-01": "2015年"
  },
}

const Adata = {
  "region_level": {
    "1": "省级",
    "2": "副省级市",
    "3": "计划单列市"
  },
  "region_level2": {
    "1": "省级",
    "2": "副省级市",
    "3": "计划单列市"
  },
  "region_level3": {
    "0": "全部",
    "1": "省级",
    "2": "副省级市",
    "3": "计划单列市"
  },
  "region": {
    "1": "省级",
    "2": "副省级市",
    "3": "计划单列市"
  },
  "region2": {
    "1": "各省",
    "2": "各副省级市",
    "3": "各计划单列市"
  },
  "search_type": {
    "0": "广告",
    "1": "地域",
    "2": "媒体"
  },
  "media_class": {
    "media_pclass": {
      "": "全部媒体",
      "01": "电视",
      "02": "广播",
      "03": "报纸"
    },
    "01": {
      "0101": "省级频道",
      "010101": "卫视频道",
      "010102": "其他频道",
      "0102": "市级频道",
      "010201": "新闻频道",
      "010202": "其他频道",
      "0103": "县级频道"
    },
    "02": null,
    "03": {
      "0301": "党报",
      "0302": "都市报",
      "0303": "专业报",
      "0304": "其他"
    }
  },
  "ad_class2": {
    "01": "药品类",
    "02": "保健食品类",
    "03": "医疗器械类",
    "06": "普通食品类",
    "07": "酒类",
    "08": "烟草类",
    "09": "化妆品类",
    "10": "房地产类",
    "13": "电器类",
    "14": "交通产品类",
    "15": "知识产品类",
    "16": "普通商品类",
    "17": "医疗服务类",
    "18": "金融服务类",
    "19": "信息服务类",
    "20": "互联网服务类",
    "21": "商业招商投资类",
    "22": "教育培训服务类",
    "23": "会展文体活动服务类",
    "24": "普通服务类",
    "25": "形象宣传类",
    "26": "非商业类",
    "27": "其它",
  },
  "ad_class": {
    "1": "药品类",
    "2": "医疗器械类",
    "3": "化妆品类",
    "4": "房地产类",
    "5": "普通食品类",
    "6": "保健食品类",
    "7": "烟草类",
    "8": "酒类",
    "9": "电器类",
    "10": "交通产品类",
    "11": "知识产品类",
    "12": "普通商品类",
    "13": "医疗服务类",
    "14": "金融服务类",
    "15": "信息服务类",
    "16": "互联网服务类",
    "17": "商业招商投资",
    "18": "教育培训服务类",
    "19": "会展文化活动服务类",
    "20": "普通服务类",
    "21": "形象宣传类",
    "22": "非商业类",
    "23": "其它类"
  },
  "onther_class": {
    "fad_count": "条数",
    "fad_illegal_count": "违法条数",
    "counts_illegal_rate": "条数违法率",
    "fad_times": "条次",
    "fad_illegal_times": "违法条次",
    "times_illegal_rate": "条次违法率",
    "fad_play_len": "时长",
    "fad_illegal_play_len": "违法时长",
    "lens_illegal_rate": "时长违法率",
    "comprehensive_score":"综合得分"
  },
  "onther_class2": {
    "fad_times": "条次",
    "fad_illegal_times": "违法条次",
    "times_illegal_rate": "条次违法率",
    "fad_play_len": "时长",
    "fad_illegal_play_len": "违法时长",
    "lens_illegal_rate": "时长违法率",
    "comprehensive_score":"综合得分"
  },
  "data_class": {
    "fad_count": "条数",
    "fad_illegal_count": "违法条数",
    "fad_times": "条次",
    "fad_illegal_times": "违法条次",
    "fad_play_len": "时长",
    "fad_illegal_play_len": "违法时长"
  },
  "data_class1": {
    "fad_times": "条次",
    "fad_illegal_times": "违法条次",
    "fad_play_len": "时长",
    "fad_illegal_play_len": "违法时长"
  },
  "data_class2": {
    "fad_times": "条次",
    "fad_illegal_times": "违法条次",
    "fad_play_len": "时长",
    "fad_illegal_play_len": "违法时长"
  },
  'permission_media': {
    '1': '权限内',
    '2': '权限外',
  }
}

const NJadType = [
  '药品',
  '医疗服务',
  '医疗器械',
  '普通食品',
  '保健食品',
  '农资',
  '烟草',
  '酒类',
  '教育、培训',
  '招商投资金融保险',
  '房地产',
  '化妆品',
  '美容服务',
  '信息服务',
  '家用电器',
  '旅游',
  '交通产品',
  '其他',
  '自定义'
]

export {
  Gtime,
  Adata,
  NJadType
}
