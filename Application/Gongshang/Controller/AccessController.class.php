<?php
namespace Gongshang\Controller;
use Think\Controller;

class AccessController extends BaseController
{
	/*展示信息*/
	public function index(){
		$list['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');
		$list['fcreateregualtorname'] =  session('regulatorpersonInfo.regulatorname');
		$data = $this->get_next_regulator(session('regulatorpersonInfo.fregulatorid'));//下级机构列表
		$this->assign('data', $data); //列表信息
		$this->assign('list', $list); //本工商局最高级
		$this->display();
	}
	/*增加组织人员*/
	public function cadd(){
		$data = array();
		$data['fregulatorid'] = I('fregulatorid');//机构ID
		$data['fcode'] = I('fcode');//人员编码
		$data['fname'] = I('fname');//人员名称
		$data['fduties'] = I('fduties');//人员职务
		$data['fmobile'] = I('fmobile');//联系手机
		$data['fisadmin'] =0;//是否超级管理员 0=》不是，1=》是
		$data['fdescribe'] = I('fdescribe');//说明
		$data['fstate'] = I('fstate',1);//状态，-1删除，0-无效，1-有效
		if(I('menu')){
			$data['special_menu'] = json_encode(I('menu')); //个性化菜单，json
		}
		else{
			$data['special_menu']='';
		}
		if(I('media_list')){
			$data['special_media'] = json_encode(I('media_list')); //个性化媒体，json
		}else{
			$data['special_media']='';
		}

		$count_fcode = M('tregulatorperson')->where(array('fcode'=>$data['fcode']))->count();//查询人员代码是否重复
		if($count_fcode > 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'人员代码重复'));//返回人员代码重复
		}
		$count_fmobile = M('tregulatorperson')->where(array('fmobile'=>$data['fmobile']))->count();//查询联系手机是否重复
		if($count_fmobile > 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'联系手机重复'));//返回联系手机重复
		}
		$data['fpassword'] = md5(I('fpassword'));//密码
		$data['fcreator'] = '系统最高管理员';//创建人
		$data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$data['fmodifier'] = '系统最高管理员';//修改人
		$data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$res = M('tregulatorperson')->add($data);//添加数据
		if($res){//判断是否添加成功
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));//返回成功
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));//返回失败
		}
	}
	/*修改组织人员*/
	public function edit()
	{
		$personid = I('fid');//人员ID
		$personid = intval($personid);//转为数字
		$data = array();
		$data['fregulatorid'] = I('fregulatorid');//机构ID
		$data['fcode'] = I('fcode');//人员编码
		$data['fname'] = I('fname');//人员名称
		$data['fduties'] = I('fduties');//人员职务
		$data['fmobile'] = I('fmobile');//联系手机
		$data['fdescribe'] = intval(I('fdescribe'));//说明
//		$data['fisadmin'] = I('fisadmin');//是否超级管理员 0=》不是，1=》是
		$data['fstate'] = I('fstate', 1);//状态，-1删除，0-无效，1-有效
		$count_fcode = M('tregulatorperson')->where(array('fid' => array('neq', $personid), 'fcode' => $data['fcode']))->count();//查询人员代码是否重复
		if ($count_fcode > 0) {
			$this->ajaxReturn(array('code' => -1, 'msg' => '人员代码重复'));
		}//返回人员代码重复
		$count_fmobile = M('tregulatorperson')->where(array('fid' => array('neq', $personid), 'fmobile' => $data['fmobile']))->count();//查询联系手机是否重复
		if ($count_fmobile > 0) {
			$this->ajaxReturn(array('code' => -1, 'msg' => '联系手机重复'));//返回联系手机重复
		}
		if (I('fpassword') != '') $data['fpassword'] = md5(I('fpassword'));//密码

		if (I('menu')) {
			$data['special_menu'] = json_encode(I('menu')); //个性化菜单，json
		}else{
			$data['special_menu']='';
		}
		if (I('media_list')) {
		$data['special_media'] = json_encode(I('media_list')); //个性化媒体，json
		}else{
			$data['special_media']='';
		}

		$data['fmodifier'] = '系统最高管理员';//修改人
		$data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$res = M('tregulatorperson')->where(array('fid'=>$personid))->save($data);//修改数据
		if($res){//判断是否修改成功
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));//返回成功
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败！'));//返回失败
		}
	}
	/*异步加载部门列表*/
	public function ajax_regulator_list(){
		$fid = I('fid');//获取机构ID
		$data = $this->get_next_regulator($fid);//下级机构列表
		if(!$data){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'无数据'));
		}
		$this->assign('regulatorList',$data);
		$this->display();
//		$this->ajaxReturn(array('code'=>0,'msg'=>'','data'=>$data));

	}
	/*异步加载部门or机构下的人员*/
	public function ajax_regulator_person() {
		$fid = I('fid');//获取机构ID
		$data = M('tregulatorperson')->where(array('fregulatorid'=>$fid,'fstate' =>1))->select();
		if(!$data){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'无数据'));
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'','data'=>$data));
	}
	/*人员详情*/
	public function details(){
		$fid = I('fid');//获取人员ID
		if(session('regulatorpersonInfo.special_menu')){
			$menu = json_decode(session('regulatorpersonInfo.special_menu'),true);
		}
		//暂定没有权限菜单的没有。
//		else{
//			$menu=D('MenuList')->clist();//获取菜单
//		}
		$media_list = M('tregulatormedia')
			->field('
			tregulatormedia.fregulatormediaid,
			tregulatormedia.fregulatorcode as fregulatorid ,
			tregulatormedia.fmediaid,
			tmedia.fmedianame
			')
			->join('tmedia on tmedia.fid = tregulatormedia.fmediaid','LEFT')
			->where(array('tregulatormedia.fregulatorcode'=>session('regulatorpersonInfo.fregulatorid'),'tregulatormedia.fstate'=>1))
			->limit(50)
			->select();//查询机构下的媒体列表
//		aa(M('tregulatormedia')->_sql());
		if($fid){
			$data = $this->get_tregulatorperson_details($fid);//人员详情
			$this->assign('data', $data);
		}
		$this->assign('menu', $menu); //菜单
		$this->assign('media_list', $media_list); //机构下的媒体列表
		$this->display();
	}
	/*删除人员*/
	public function ajax_regulatorperson_del(){
		$fid = I('fid');//获取人员ID
		M('tregulatorperson')->where(array('fid'=>$fid))->save(array('fstate'=>-1));
		$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));

	}
	/*增加组织机构*/
	public function cadd_regulator(){
		$data = array();
		$data['fpid'] = intval(I('fpid',session('regulatorpersonInfo.fregulatorid')));//上级ID
		$data['ftype'] = 20;//监管机构类型（20工商）
		$data['fname'] = I('fname');//部门名称
		$data['fcode'] = I('fcode');//机构编码
		$data['fdescribe'] = I('fdescribe');//部门说明
		$data['fkind'] = 2;//机构类型(0-根节点，1-机构，2-部门）
		$data['fcreator'] = '系统最高管理员';//创建人
		$data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
		$data['fmodifier'] = '系统最高管理员';//修改人
		$data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$data['fstate'] = 1;//状态，-1删除，0-无效，1-有效
		$data['fregionid'] = session('regulatorpersonInfo.regionid');//行政区划
		$count_fcode = M('tregulator')->where(array('fcode'=>$data['fcode']))->count();//查询人员代码是否重复
		if($count_fcode > 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'机构代码重复'));
		}
		$res = M('tregulator')->add($data);
		if($res){//判断是否添加成功
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));//返回成功
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败'));//返回失败
		}
	}
	/*修改组织机构*/
	public function edit_regulator(){
		$fid= I('fid');//部门ID
		$fid = intval($fid);//转为数字
		$data = array();
		$data['fname'] = I('fname');//部门名称
		$data['fcode'] = I('fcode');//机构编码
		$data['fdescribe'] = I('fdescribe');//部门说明
		$data['fmodifier'] = '系统最高管理员';//修改人
		$data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$count_fcode = M('tregulator')->where(array('fid'=>array('neq',$fid),'fcode'=>$data['fcode']))->count();//查询人员代码是否重复
		if($count_fcode > 0){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'机构代码重复'));
		}
		$res = M('tregulator')->where(array('fid'=>$fid))->save($data);//修改数据
		if($res){//判断是否修改成功
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));//返回成功
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败！'));//返回失败
		}
	}
	/*机构详情*/
	public function ajax_regulator_details(){
		$fid = I('fid');//获取机构ID
		$data = M('tregulator')->where(array('fid'=>$fid))->find();
		$this->ajaxReturn(array('code'=>0,'data'=>$data));
	}

}