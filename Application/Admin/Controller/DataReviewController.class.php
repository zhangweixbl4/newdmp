<?php
//数据复核模块
namespace Admin\Controller;
use TaskInput\Controller\NetAdController;
use TaskInput\Controller\TraditionAdController;
use Think\Controller;
class DataReviewController extends BaseController {

    public function index(){
        $this->display();
    }

    //样本复核fj_data_check
    public function index_fj_list(){
        header("Content-type: text/html; charset=utf-8");

        $mediaclass = I('mediaclass','tv');//媒介类别
        $join_field = 'ybb.fid';
        $map['tcheck_status'] = 10;//申请复核状态
        switch ($mediaclass){
            case 'tv':
                $sample_table = 'ttvsample';
                $res = M('fj_data_check')
                    ->field("
                    tshengj_trename,
                    tcheck_content,
                    favifilename,
                    favifilepng,
                    tad.fadname,
                    tshengj_trename,
                    tshengj_trename,
                    tshengj_trename
                    ")
                    ->alias('a')
                    ->join("$sample_table ybb on $join_field = a.fsample_id")
                    ->join('tad ON ybb.fadid = tad.fadid AND tad.fstate = 1 AND tad.fadid <> 0')
                    ->where($map)
                    ->select();
                dump($res);exit;
                break;
            case 'bc':
                $sample_table = 'tbcsample';
                break;
            case 'paper':
                $sample_table = 'tpapersample';
                $join_field = 'ybb.fpapersampleid';
                break;
            case 'net':
                $sample_table = 'tnetissue';
                $join_field = 'tnetissue.major_key';
                break;
        }
        if(!empty($mediaclass)){
            $map['fsample_type'] = $mediaclass;
        }

    }

    //违法广告复核  tbn_illegal_ad
    public function index_ill_list(){

        $this->display();
    }



}