<?php
//样本调整控制器

namespace Admin\Controller;
use Think\Controller;

class SampleAdjustController extends BaseController {

    public function index(){
		 
		
	}
	
	
	/*调整逻辑*/
	public function adjust(){
		
		

		$samId = I('samId');//样本id
		$samType = strval(I('samType'));//样本类型，tv电视，bc广播
		$fillegalcontent = I('fillegalcontent');//涉嫌违法内容
		$fillegaltype = I('fillegaltype');//违法程度
		$fexpressioncodes = I('fexpressioncodes');//违法表现代码，逗号“,”分隔
		$start_issue_time = date('Y-m-d H:i:s',strtotime(I('start_issue_time')));//调整开始时间
		$end_issue_time = date('Y-m-d H:i:s',strtotime(I('end_issue_time')));//调整结束时间
		
		if($samType != 'tv' && $samType != 'bc'){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'samType不正确'));
		}
		
		$oldSamInfo = M('t'.$samType.'sample')->where(array('fid'=>$samId))->find();//查询旧的样本数据
		
		$newSamInfo = $oldSamInfo;//新的样本数据赋值，使用原来的违法信息
		unset($newSamInfo['fid']);//清除主键id
		$newSamInfo['uuid'] = $oldSamInfo['uuid'].'-'.date('Ymd');//新样本的uuid，更改UUID
		$newSamInfo['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//新样本的修改人
		$newSamInfo['fmodifytime'] = date('Y-m-d H:i:s');//新样本的修改时间
			
		$newSamId = M('t'.$samType.'sample')->add($newSamInfo); //添加新记录，并获得新记录的id
		
		
		
		$fexpressioncodes_arr = explode(',',$fexpressioncodes);
		
	
		
		$illegalList = M('tillegal')->where(array('fcode'=>array('in',$fexpressioncodes_arr)))->select();
		
		
		$fexpressioncodes_array = array();
		$fexpressions_array = array();
		$fconfirmations_array = array();
		$fpunishments_array = array();
		$fpunishmenttypes_array = array();
		
		
		foreach($illegalList as $illegal){
			$fexpressioncodes_array[] = $illegal['fcode'];//违法表现代码
			$fexpressions_array[] = $illegal['fexpression'];//违法表现
			$fconfirmations_array[] = $illegal['fconfirmation'];//认定依据
			$fpunishments_array[] = $illegal['fpunishment'];//处罚依据
			$fpunishmenttypes_array[] = $illegal['fpunishmenttype'];//处罚种类及幅度
			
		}
		

		$upSamInfo['fexpressioncodes'] = implode(';',$fexpressioncodes_array);
		$upSamInfo['fexpressions'] = implode("\n",$fexpressions_array);
		$upSamInfo['fconfirmations'] = implode("\n",$fconfirmations_array);
		$upSamInfo['fpunishments'] = implode("\n",$fpunishments_array);
		$upSamInfo['fpunishmenttypes'] = implode("\n",$fpunishmenttypes_array);
		$upSamInfo['fillegaltypecode'] = $fillegaltype;
		$upSamInfo['fillegalcontent'] = $fillegalcontent;
		$upSamInfo['fmodifier'] = session('personInfo.fid').'_'.session('personInfo.fname');//新样本的修改人
		$upSamInfo['fmodifytime'] = date('Y-m-d H:i:s');//新样本的修改时间
		
		$issue_relation_arr = array_filter(explode(',',$oldSamInfo['issue_relation']));//关联的关系表
		

		

		foreach($issue_relation_arr as $issue_table){//循环关联关系表
			$e_f = M($issue_table)->where(array('fstarttime'=>array('not between',array(strtotime($start_issue_time),strtotime($end_issue_time))),'fsampleid'=>$samId))->save(array('fsampleid'=>$newSamId));//分表的发布记录

		}
		
		$e_d = M('t'.$samType.'issue')->where(array('fstarttime'=>array('not between',array($start_issue_time,$end_issue_time)),'f'.$samType.'sampleid'=>$samId))->save(array('f'.$samType.'sampleid'=>$newSamId));//大表的发布记录

		
		
		
		
		$e_old_uuid_state = M('t'.$samType.'sample')->where(array('fid'=>$oldSamInfo['fid']))->save($upSamInfo);//修改旧记录
		S($oldSamInfo['uuid'].'_'.$oldSamInfo['fmediaid'],null);//清除缓存，新增发布记录会用到这个缓存

		
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','e_f'=>$e_f,'e_d'=>$e_d));
		
		
	}
	
	

}