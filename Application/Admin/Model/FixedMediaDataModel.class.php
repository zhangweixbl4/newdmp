<?php
namespace Admin\Model;



class FixedMediaDataModel{

	#修改媒体数据归档状态
	public function fixed($mediaid,$day,$state,$value,$fperson=''){
		#return true;

		$formerInfo = M('tinspect_plan','',C('ZIZHI_DB'))->where(array('fmedia_id'=>$mediaid,'fissue_date'=>$day))->find(); #查询原归档信息
		if(!$formerInfo) return false;

		if(in_array($state,array('-99','-1','0','1','2','99'))){
			
			$teDat = array();
			if($state != $formerInfo['fstatus']){ #如果需要更新的归档状态不等于原归档状态
				$teDat['fstatus'] = $state; #归档状态
				$teDat['fixed_value'] = $value; #归档说明

				if($state == 2){ #如果归档状态是已归档
					$teDat['ffinish_time'] = date('Y-m-d H:i:s'); #归档时间
					$teDat['next_try_fixed_time'] = 0; #重置下次尝试归档时间
					$teDat['fixed_try_count'] = 0; #重置尝试归档次数
					$teDat['need_get_prog'] = 1;  #获取节目
					$teDat['need_gd'] = 1;  #归档到分析库
					$teDat['last_change_time'] = time();  
					
					
				}
				$rr = M('tinspect_plan','',C('ZIZHI_DB'))->where(array('fmedia_id'=>$mediaid,'fissue_date'=>$day))->save($teDat);
			
			}
			
			if ($state == 2 && $former_state != 2){ #如果状态是已归档 且原状态是其它状态 ，下面触发交付数据
					
				$mediaInfo = M('tmedia')->field('fid,left(fmediaclassid,2) as media_class')->where(array('fid'=>$mediaid))->find(); #查询媒体信息
				
				if(strtotime($day) < strtotime('2020-04-01')){ #判断4月1日前
				
					$regulatorcodeList = M('tregulatormedia')
												->cache(true,120)
												->where(array('fmediaid'=>$mediaid,'fstate'=>1,'fisoutmedia'=>0))
												->group('fregulatorcode')
												->getField('fregulatorcode',true);//
				}else{ #4月1日后
					
					$regulatorcodeList = M('tcustomer_inspect_plan','',C('ZIZHI_DB'))
												->cache(true,60)
												->where(array('fmedia_id'=>$mediaid,'fissue_date'=>$day,'fstatus'=>['egt',0]))
												->group('fcustomer_code')
												->getField('fcustomer_code',true);//
					
				}
				foreach($regulatorcodeList as $regulatorcode){
					if ($regulatorcode == '20100000') continue;
					if (substr($regulatorcode,0,2) != 20) continue;
					if (strlen($regulatorcode) != 8) continue;
					M('make_customer_ad_issue')->add(array('forgid'=>$regulatorcode,'fmediaclass'=>$mediaInfo['media_class'],'fmediaid'=>$mediaid,'fdate'=>$day,'fcreatetime'=>date('Y-m-d H:i:s')),array(),true);
				
				}
			}
			
			if ($state == 99 && $former_state == 2 && !C('IS_TEST')){ #如果状态是特殊 ，且原状态是已归档 ，触发退回孙涛
				$fk_url = 'http://192.168.0.153:8002/judge_back_day';
				$postToData = array();
				$postToData['channel'] = $mediaid;
				$postToData['day'] = $day;
				$postToData['status'] = -1;
				$postToData['reason'] = $value;
				
				$reqStr = http($fk_url,$postToData,'POST');
				
				M('media_data_back')->add(array(
												  
												  'fmediaid'=>$mediaid,
												  'fdate'=>$day,
												  'fcreatetime'=>date('Y-m-d H:i:s'),
												  'push_time'=>date('Y-m-d H:i:s'),
												  'push_req'=>$reqStr,
												  'push_state'=>1,
												  'back_reason'=>$value,
													));
			}
			

		}
		
		M('fixed_media_data_log')->add(array('fmediaid'=>$mediaid,'fdate'=>$day,'ftime'=>date('Y-m-d H:i:s'),'fvalue'=>$state.','.$value,'fperson'=>$fperson));
				
		return true;
		
		
		
		
		
	}
	
	#查询归档状态*
	public function get_state($mediaid,$day){
		/* $state = M('fixed_media_data')->where(array('fmediaid'=>$mediaid,'fdate'=>$day))->getField('fixed_state');
		if(!$state) $state = 0;
		return $state;
		 */
		$state = M('tinspect_plan','',C('ZIZHI_DB'))->where(array('fmedia_id'=>$mediaid,'fissue_date'=>$day))->getField('fstatus');

		return $state;
		 
		
	}
	
	#获取消息
	public function get_issue_count($fmediaid,$fdate){

		
		$mediaInfo = M('tmedia')->field('fid,fmedianame,left(fmediaclassid,2) as media_class,media_region_id')->where(array('fid'=>$fmediaid))->find();

		$issue_table_postfix = date('Ym',strtotime($fdate)).'_'.substr($mediaInfo['media_region_id'],0,2);
		
		try{//
			if($mediaInfo['media_class'] == '01'){
				$data = M('ttvissue_'.$issue_table_postfix)
														->alias('tiss')
														->field('
																	count(*) as tc ,
																	count(DISTINCT tiss.fsampleid) as ts,
																	count(case when tsam.fillegaltypecode = 30 then 1 else null end) as wftc ,
																	count(DISTINCT case when tsam.fillegaltypecode = 30 then tiss.fsampleid else null end) as wfts

																	')
														->join('ttvsample as tsam on tsam.fid = tiss.fsampleid')
														->where(array('tiss.fissuedate'=>strtotime($fdate),'tiss.fmediaid'=>$fmediaid))
														->find();
				try{//
					$eCount = M('ttvissue_'.$issue_table_postfix)
										->alias('iss_tab')
										->field('count(*) as wlrtc,count(DISTINCT sam_tab.uuid) as wlrts')
										->join('ttvsample as sam_tab on sam_tab.fid = iss_tab.fsampleid')
										->where(array('sam_tab.fadid'=>0,'iss_tab.fmediaid'=>$fmediaid,'iss_tab.fissuedate'=>strtotime($fdate)))
										->find();
				}catch( \Exception $e) { //
					$eCount = array('wlrtc'=>0,'wlrts'=>0);
				}
				$data['wlrtc'] = $eCount['wlrtc'];
				$data['wlrts'] = $eCount['wlrts'];
			}elseif($mediaInfo['media_class'] == '02'){
				$data = M('tbcissue_'.$issue_table_postfix)
														->alias('tiss')
														->field('
																	count(*) as tc ,
																	count(DISTINCT tiss.fsampleid) as ts,
																	count(case when tsam.fillegaltypecode = 30 then 1 else null end) as wftc ,
																	count(DISTINCT case when tsam.fillegaltypecode = 30 then tiss.fsampleid else null end) as wfts

																	')
														->join('tbcsample as tsam on tsam.fid = tiss.fsampleid')
														->where(array('tiss.fissuedate'=>strtotime($fdate),'tiss.fmediaid'=>$fmediaid))
														->find();
				try{//
					$eCount = M('tbcissue_'.$issue_table_postfix)
										->alias('iss_tab')
										->field('count(*) as wlrtc,count(DISTINCT sam_tab.uuid) as wlrts')
										->join('tbcsample as sam_tab on sam_tab.fid = iss_tab.fsampleid')
										->where(array('sam_tab.fadid'=>0,'iss_tab.fmediaid'=>$fmediaid,'iss_tab.fissuedate'=>strtotime($fdate)))
										->find();
				}catch( \Exception $e) { //
					$eCount = array('wlrtc'=>0,'wlrts'=>0);
				}
				$data['wlrtc'] = $eCount['wlrtc'];
				$data['wlrts'] = $eCount['wlrts'];
			}elseif($mediaInfo['media_class'] == '03'){
				$data = M('tpaperissue')
														->alias('tiss')
														->field('
																	count(*) as tc ,
																	count(DISTINCT tiss.fpapersampleid) as ts,
																	count(case when tsam.fadid = 0 then 1 else null end) as wlrtc ,
																	count(DISTINCT case when tsam.fadid = 0 then tsam.fpapersampleid else null end) as wlrts,
																	
																	count(case when tsam.fillegaltypecode = 30 then 1 else null end) as wftc ,
																	count(DISTINCT case when tsam.fillegaltypecode = 30 then tiss.fpapersampleid else null end) as wfts

																	')
														->join('tpapersample as tsam on tsam.fpapersampleid = tiss.fpapersampleid')
														->where(array('tiss.fissuedate'=>$fdate,'tiss.fmediaid'=>$fmediaid))
														->find();
				
				
				
			}elseif($mediaInfo['media_class'] == '13'){
				$data = M('tnetissueputlog')
														->alias('tiss')
														->field('
																	count(*) as tc ,
																	count(DISTINCT tiss.tid) as ts,
																	count(case when tsam.fillegaltypecode = 30 then 1 else null end) as wftc ,
																	count(DISTINCT case when tsam.fillegaltypecode = 30 then tiss.tid else null end) as wfts

																	')
														->join('tnetissue as tsam on tsam.major_key = tiss.tid')
														->where(array(
																		'tiss.fissuedate'=>strtotime($fdate),
																		'tiss.fmediaid'=>$fmediaid,
																		'tsam.isad'=>1,
																		))
														->find();
				$eCount = M('tnetissueputlog')
								->field('count(*) as wlrtc,count(DISTINCT tnetissueputlog.tid) as wlrts')
								->join('join tnetissue on tnetissue.major_key = tnetissueputlog.tid')
								->join('tnetissue_identify on tnetissue_identify.major_key = tnetissue.major_key')
								->where(array(
												'tnetissueputlog.fissuedate'=>strtotime($fdate),
												'tnetissueputlog.fmediaid'=>$fmediaid,
												'tnetissue.finputstate'=>1,
												'tnetissue.isad'=>1,
												'result_type'=>array('neq',50),
										))
								->find();
				$data['wlrtc'] = $eCount['wlrtc'];
				$data['wlrts'] = $eCount['wlrts'];
				
				#var_dump(M('tnetissueputlog')->getLastSql());
			}else{
				
			}
		}catch( \Exception $e) {
			$data = array('tc'=>0,'ts'=>0,'wftc'=>0,'wfts'=>0,'wlrtc'=>0,'wlrts'=>0);
		}
		

		return $data;
		
	}
    


}