Vue.prototype.$ELEMENT = { size: "mini", zIndex: 3000 }

const vm = new Vue({
  el: '#app',
  data() {
    return {
      form: {},
      tableData: [],
      tableHeight: '300px',
      page: {
        pageIndex: 1,
        pageSize: 20,
      },
      total: 0
    }
  },
  created() {
    let tableData = []
    for (let i = 0; i < 20; i++){
      let days = []
      for (let v = 1; v < 32; v++){
        days.push({
          day: v,
          state: Math.round(Math.random())
        })
      }
      tableData.push({
        id: i,
        name: '浙江卫视',
        days: days
      })
    }
    this.tableData = tableData
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight()
      document.querySelector('#app').style.opacity = 1
    })
    window.onresize = () => {
      this.getTableHeight()
    }
  },
  methods: {
    getList(page = 1) {
      this.page.pageIndex = page
    },
    pageSizeChange(pageSize) {
      this.page.pageSize = pageSize
      this.getList(this.page.pageIndex)
    },
    getTableHeight() {
      let formHeight = this.$refs.sform.$el.clientHeight
      this.tableHeight = document.body.clientHeight - formHeight - 62 + 'px'
    },
  },
})