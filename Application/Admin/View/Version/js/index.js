Vue.prototype.$ELEMENT = { size: "mini", zIndex: 3000 };
Vue.use(VueClipboard)

const search = {
  template: `
    <span>
      <a class="el-icon-search" style="color:#1AB394;font-size: 16px;vertical-align: text-bottom;font-weight: bold;" @click="() => $emit('click')" title="以该字段为条件进行搜索"></a>
      <slot></slot>
    </span>
  `
}

const vm = new Vue({
  el: "#app",
  components: {
    search
  },
  data() {
    return {
      adClassTree: adClass,
      loading: false,
      bdisabled: false,
      form:{
        fadname: '',
        fversion: '',
        fadclasscode: '',
        fadlen: '',
        fuuid: '',
        fmediaclass: '3'
      },
      fadlens: '',
      fadlene: '',
      fadclasscode: [],
      multipleSelection: [],
      page: {
        pageIndex: 1,
        pageSize: 20,
      },
      importForm: {
        fmediaid: '',
        fdate: '',
        fillegaltypecode: '',
      },
      fdate: [],
      fillegaltypecode: false,
      mainVersion: '',
      count: '0',
      tableHeight: '300px',
      tableData: [],
      showSampleList: false,
      showMerge: false,
      showImport: false,
      sourceForm: '',
      activeRow: {},
      activeSampleRow: {},
      mediaList: [],
      mediaClass: [
        {
          label: '电视',
          value: '1',
        },
        {
          label: '广播',
          value: '2',
        },
        {
          label: '报纸',
          value: '3',
        },
      ],
      importRules: {
        fmediaid: [
          { required: true, message: '选择导入媒体', trigger: 'blur' }
        ],
        fdate:[{ required: true, message: '请选择时间段', trigger: 'change' }],
      },
      preFormData: {},
      pickerOptions: {
        disabledDate(time) {
          return  +new Date() < time
        }
      }
    }
  },
  computed: {
    canMerge() {
      return this.multipleSelection.length >= 2
    },
    ispaper() {
      return this.preFormData.fmediaclass === '3'
    }
  },
  watch: {
    fadlens(val) {
      this.setLength()
    },
    fadlene(val) {
      this.setLength()
    },
    fdate(val) {
      this.importForm.fdate = val ? val.join(',') : ''
    },
    fillegaltypecode(val) {
      this.importForm.fillegaltypecode = val ? '30' : ''
    }
  },
  created() {
    this.sourceForm = JSON.stringify(this.form)
    this.getList()
  },
  mounted() {
    this.$nextTick(() => {
      this.getTableHeight()
      document.querySelector('#app').style.opacity = 1
    })
    window.onresize = () => {
      this.getTableHeight()
    }
  },
  methods: {
    getList(page = 1) {
      this.loading = true
      this.page.pageIndex = page
      let data = {
        ...this.form,
        ...this.page
      }
      this.preFormData = data
      fly.post('/Admin/Version/getList', data).then(({data}) => {
        if (data.code === 0) {
          this.tableData = data.data.map(i => {
            this.setAttributes(i)
            i._fadname = this.setHightColor(i.fadname, this.preFormData.fadname)
            i._fversion = this.setHightColor(i.fversion, this.preFormData.fversion)
            i._fuuid = this.setHightColor(i.fuuid, this.preFormData.fuuid)
            i.fadclasscode = $h.getAdClassCode(i.fadclasscode)
            i.source = JSON.stringify(i)
            return i
          })
          this.count = +data.count
        } else {
          $h.notify.error(data.msg, this)
        }
        this.loading = false
      }).catch(e => console.error(e))
    },
    setHightColor(str,query) {
      return str ? str.split(query).join(`<span style="color: #F56C6C;">${query}</span>`) : str
    },
    getSampleList(row) {
      return new Promise((resolve, reject) => {
        fly.post('/Admin/Version/getSamList', { fid: row.fid }).then(({data}) => {
          if (data.code === 0) {
            this.$set(row, 'samplelist', data.data.map(i => {
              i.visible = false
              return i
            }))
            resolve(data.data)
          } else {
            $h.notify.error(data.msg, this)
            reject()
          }
        }).catch(e => {
          $h.notify.error(data.msg, this)
          reject()
        })
      })
    },
    unbindVersion(row) {
      const h = this.$createElement
      this.$msgbox({
        title: '确 认',
        message: h('p', null, [
          h('span', null, '确定解除 '),
          h('span', { style: 'color: #F56C6C' }, row.fadname),
          h('span', null, ' 与 '),
          h('span', { style: 'color: #F56C6C' }, this.activeRow.fadname),
          h('span', null, ' 的关联？')
        ]),
        confirmButtonText: '确定',
        type: 'danger',
        closeOnClickModal: false,
        closeOnPressEscape: false,
      }).then(() => {
        let data = {
          fid: this.activeRow.fid,
          fsampleid: row.fid
        }
        fly.post('/Admin/Version/delSam', data).then(({data}) => {
          if (data.code === 0) {
            $h.notify.success(data.msg, this)
            this.getSampleList(this.activeRow)
          } else {
            $h.notify.error(data.msg, this)
          }
        }).catch(e => console.error(e))
      }).catch(() => {
      })
    },
    deleteVersion(row) {
      row.visible = false
      this.getSampleList(row).then(res => {
        if (res.length < 1) {
          fly.post('/Admin/Version/delVersion', { fid: row.fid }).then(({data}) => {
            if (data.code === 0) {
              $h.notify.success(res.msg, this)
              this.getList(this.page.pageIndex)
            } else {
              $h.notify.error(res.msg, this)
            }
          }).catch(e => console.error(e))
        } else {
          this.$set(row, 'disabled' ,true)
          $h.notify.error('该版本下任有关联样本，无法删除', this)
        }
      })
    },
    setAttributes(obj) {
      ['showvideo', 'isedit', 'visible', 'disabled'].forEach(i => {
        this.$set(obj, i, false)
      })
    },
    handleRowClick(row, column, event) {
      if (!row.fsourcefilename) {
        return $h.notify.error('资源不存在！', this)
      }
      this.activeSampleRow = row
    },
    handleAdclassChange(val) {
      this.$set(this, 'fadclasscode', val)
      this.$set(this.form, 'fadclasscode', [...val].pop())
    },
    handleSearch(name, val) {
      if (name === 'fadclasscode') {
        this.handleAdclassChange(val)
      } else {
        this.$set(this.form, name, val)
      }
      this.getList()
    },
    mergeVersion() {
      this.showMerge = true
    },
    submitMerge() {
      let data = {
        mainfid: this.mainVersion,
        fids: this.multipleSelection.map(i => i.fid).join(',')
      }
      fly.post('/Admin/Version/mergeVersion', data).then(({data}) => {
        if (data.code === 0) {
          $h.notify.success(data.msg, this)
          this.resetTable()
          this.beforeClose()
          this.getList(this.page.pageIndex)
        } else {
          $h.notify.error(data.msg, this)
        }
      }).catch(e => console.error(e))
    },
    resetTable() {
      this.mainVersion = ''
      this.$refs.table.clearSelection()
      this.multipleSelection = []
    },
    handleSelectionChange(val) {
      this.multipleSelection = val
    },
    save({fadname, fversion, fadclasscode, fid}) {
      let data = {
        fid,
        fadname,
        fversion,
        fadclasscode: [...fadclasscode].pop()
      }
      fly.post('/Admin/Version/editVersion', data).then(({data}) => {
        if (data.code === 0) {
          $h.notify.success(data.msg, this)
          this.getList(this.page.pageIndex)
        } else {
          $h.notify.error(data.msg, this)
        }
      }).catch(e => console.error(e))
    },
    saveCancel(row) {
      const source = JSON.parse(row.source);
      ['fadclasscode', 'fadname', 'fversion', 'isedit'].forEach(i => {
        this.$set(row, i, source[i])
      })
    },
    reset() {
      this.form = JSON.parse(this.sourceForm)
      this.fadclasscode = []
      this.fadlens = ''
      this.fadlene = ''
      this.getList()
    },
    beforeClose() {
      this.showSampleList = false
      this.showMerge = false
      this.showImport = false
      this.mediaList = []
      this.activeRow = {}
      this.activeSampleRow = {}
      this.fillegaltypecode = false
      this.fdate = []
      this.importForm.fmediaid = ''
    },
    showList(row) {
      if (!row.samplelist) {
        this.getSampleList(row)
      }
      this.activeRow = row
      this.showSampleList = true
    },
    getTableHeight() {
      let formHeight = this.$refs.sform.$el.clientHeight
      this.tableHeight = document.body.clientHeight - formHeight - 120 + 'px'
    },
    querySearchAsync(queryString, cb) {
      if (queryString) {
        fly.post('/Open/Ad/get_ad', { fadname: queryString }).then(({data}) => {
          cb(data.adList.map(i => {
            i.value = i.fadname
            return i
          }))
        })
      } else {
        cb([])
      }
    },
    handleAdSelect(obj, row) {
      this.$set(row, 'fadclasscode', $h.getAdClassCode(obj.fadclasscode))
    },
    sizeChange(size) {
      this.page.pageSize = size
      this.getList(this.page.pageIndex)
    },
    setLength() {
      if (this.fadlens && this.fadlene) {
        this.form.fadlen = `${this.fadlens},${this.fadlene}`
      } else if(this.fadlens && !this.fadlene) {
        this.form.fadlen = `${this.fadlens},86400`
      } else if (!this.fadlens && this.fadlene) {
        this.form.fadlen = `0,${this.fadlene}`
      } else {
        this.form.fadlen = ''
      }
    },
    lengthChange(type) {
      if (type === 'fadlens') {
        this.fadlens = Number(this.fadlens) <= Number(this.fadlene || Infinity) ? this.fadlens : this.fadlene
      } else {
        this.fadlene = Number(this.fadlens) <= Number(this.fadlene) ? this.fadlene : this.fadlens
      }
    },
    searchMedia(queryString) {
      fly.post('/Api/Media/searchMediaByName', Qs.stringify({ name: queryString })).then(({data}) => {
        this.mediaList = data.data
      })
    },
    importSample() {
      this.$refs.importForm.validate((valid) => {
        if (valid) {
          this.bdisabled = true
          fly.post('/Admin/Version/getAddVersionInfo', this.importForm).then(({ data }) => {
            if (data.code === 0 && data.count) {
              this.$confirm(`当前条件下该媒体下目前共有 ${data.count} 条记录，确认导入？`, '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
              }).then(() => {
                this.submit()
              }).catch(() => {
              })
            } else {
              $h.notify.error(data.msg, this)
            }
            this.bdisabled = false
          }).catch(e => {
            console.error(e)
            this.bdisabled = false
          })
        }
      })
    },
    submit() {
      fly.post('/Admin/Version/addVersion', this.importForm).then(({data}) => {
        if (data.code === 0) {
          $h.notify.success(data.msg, this)
          this.getList(1)
          this.beforeClose()
        } else {
          $h.notify.error(data.msg, this)
        }
      })
    }
  }
});
