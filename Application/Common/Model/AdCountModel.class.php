<?php
/*广告条数控制器*/
namespace Common\Model;
use Think\Exception;


class AdCountModel{
	
	/*查询广告条数*/
	public function queryAdCount($where){
		ini_set('memory_limit','256M');
		$cacheAdCountKey = md5(json_encode($where));
		
		$cacheAdCount = S($cacheAdCountKey);
		if($cacheAdCount){
			return $cacheAdCount;
		}
		
		$whereMediaList = $where;
		$mediaIdList = $samIdList0 = M('tbn_ad_summary_day')->cache(true,600)->where($whereMediaList)->group('fmediaid')->getField('fmediaid',true);//查询涉及的媒体id

		
		$AdCount['ad_count'] = 0;
		$AdCount['ad_ill_count'] = 0;
		
		foreach($mediaIdList as $mediaId){
			$where['fmediaid'] = $mediaId;
			$samIdList0 = M('tbn_ad_summary_day')->field('fad_sam_list,fad_sam_illegal_list')->cache(true,600)->where($where)->select();//查询数据库
			$fad_sam_list = array();
			$fad_sam_illegal_list = array();
					
			if($samIdList0){
				foreach($samIdList0 as $samIdList1){
					$fad_sam_list[] = $samIdList1['fad_sam_list'];
					$fad_sam_illegal_list[] = $samIdList1['fad_sam_illegal_list'];
				}
				$fad_sam_list = implode(',',$fad_sam_list);//把数组转为字符串
				$fad_sam_illegal_list = implode(',',$fad_sam_illegal_list);//把数组转为字符串
				
				$fad_sam_list = explode(',',$fad_sam_list);//把字符串转为数组
				$fad_sam_illegal_list = explode(',',$fad_sam_illegal_list);//把字符串转为数组
				
				$fad_sam_list = array_unique($fad_sam_list);//去除重复值
				$fad_sam_illegal_list = array_unique($fad_sam_illegal_list);//去除重复值
				
				$AdCount['ad_count'] += count($fad_sam_list);//广告条数相加
				$AdCount['ad_ill_count'] += count($fad_sam_illegal_list);//广告条数相加

			}
			
		}

		
		
		S($cacheAdCountKey,$AdCount,600);
		return $AdCount;
		
	}
	
	
}