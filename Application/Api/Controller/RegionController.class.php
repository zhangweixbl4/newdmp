<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;

class RegionController extends Controller {
	
	/*获取地区列表*/
    public function get_region_list(){
		
		$fid = I('fid',100000);//获取fid
		if($fid == 0) $fid = 100000;
		
		$regionList = M('tregion')->cache(true,60)->field('fid,fname')->where(array('fpid'=>$fid))->select();//查询地区列表
		$regionDetails = M('tregion')->cache(true,60)->field('fid,left(fname,2) as fname,ffullname')->where(array('fid'=>$fid))->find();//查询地区详情
		
		if(!$regionDetails) $regionDetails = array('fid'=>100000,'fname'=>'全国','ffullname'=>'全国');
		$this->ajaxReturn(array('code'=>0,'regionList'=>$regionList,'regionDetails'=>$regionDetails));
		
	}
	
	/*
	获取全国地区列表树
	by zw
	*/
    public function get_region_treelist(){
		$where['fstate'] = 1;
		$where['flevel'] = array('in',array(1,2,3,4,5));
		$data = M('tregion')
			->alias('a')
			->field('fid,fpid,fname')
			->cache(true,600)
			->where($where)
			->order('fid asc')
			->select();//查询地区

		$result = [];//定义索引数组，用于记录节点在目标数组的位置  
		$I = [];
		foreach($data as $val) {
			if($val['fpid'] == 100000) {
				$i = count($result);
				$result[$i] = $val;
				$I[$val['fid']] = & $result[$i];
			}else{
				$i = count($I[$val['fpid']]['regionList']);
				$I[$val['fpid']]['regionList'][$i] = $val;
				$I[$val['fid']] = & $I[$val['fpid']]['regionList'][$i];
			}
		}

	   return $result;
    }
	
	public function getRegionTree(){
		header('Access-Control-Allow-Origin:*');
		$this->ajaxReturn(array('code'=>0,'msg'=>'','region'=>$this->get_region_treelist()));
		
		
		
	}

	/*创建电视分表、广播分表*/
	public function create_issue_part_table(){
		
		$month = date("Ym", strtotime("+1 months", time()));//时间戳加一个月
		
		if(date("Ym", strtotime("+3 day", time())) != $month){
			exit;
		}

		
		$regionList = M('tregion')->field('left(fid,2) as region_prefix')->where(array('right(fid,4)'=>'0000'))->select();
		
		foreach($regionList as $region){//循环地区
			
			$table_name_list = array(
										'ttvissue_'.$month.'_'.$region['region_prefix'],
										'tbcissue_'.$month.'_'.$region['region_prefix']
										
									);//创建的表名
			
			foreach($table_name_list as $table_name){//循环创建表
				A('Common/SynIssue','Model')->create_issue_part_table($table_name);	//创建表
			}
			



		}
		

	}

	/**
     * 众包任务省市县搜索, 根据id搜索下级行政区
     * @param  $id  int  fid
     */
    public function searchChildren($id)
    {
        $data = M('tregion')->cache(true)->field('fid value, fname label, ffullname')->where([
            'fpid' => ['EQ', $id],
        ])->select();


        $this->ajaxReturn([
            'data' => $data,
        ]);
    }

    /**
     * 获取一级行政区
     * @return mixed
     */
    public function getRegionArr()
    {
        $data = M('tregion')->cache(true)->field('fid value, fname label, ffullname')->where([
            'fstate' => ['EQ', 1],
            'flevel' => ['EQ', 1],
        ])->select();
        foreach ($data as &$datum) {
            $datum['children'] = [];
        }
        $this->ajaxReturn([
            'data' => $data,
        ]);
    }
}