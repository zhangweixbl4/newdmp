<?php
//众包任务管理（新）
namespace Admin\Controller;
use TaskInput\Controller\NetAdController;
use TaskInput\Controller\TraditionAdController;
use Think\Controller;
class TaskInputController extends BaseController {
    // 接收参数
    protected $P;

    /**
     * 初始化
     */
    public function _initialize(){
        parent::_initialize();
        $this->P = json_decode(file_get_contents('php://input'),true);
    }

	public function index_bak(){
        if(A('Admin/Authority','Model')->authority('300931') === 0){
            exit('没有权限');//验证是否有新增权限
        }
        $p = I('p',1);//当前第几页
        $pp = 10;//每页显示多少记录
        $keyword = I('keyword');//搜索关键词
        $taskWhere = [];//查询条件
        $tadWhere = [];

        // 根据搜索条件生产子查询搜索条件
        if($keyword != ''){
            $tadWhere['fadname'] = ['like','%'.$keyword.'%'];
        }

        if (I('adclass_code') != ''){
            $tadWhere['fadclasscode'] = ['EQ', I('adclass_code')];
        }

        if (I('task_state') != ''){
            $taskWhere['task_state'] = ['EQ',I('task_state')];
        }

        if (I('quality_state') != ''){
            $taskWhere['quality_state'] = ['EQ',I('quality_state')];
        }

        if (I('illegalType') != ''){
            $taskWhere['fillegaltypecode'] = ['EQ',I('illegalType')];
        }

        if (I('date_s') != ''){
            $taskWhere['issue_date'] = ['EGT',I('date_s')];
        }else{
            $taskWhere['issue_date'] = ['EQ',date('Y-m-d')];
        }
        if (I('date_e') != ''){
            $taskWhere['issue_date'] = ['ELT',I('date_e')];
        }else{
            $taskWhere['issue_date'] = ['EQ',date('Y-m-d')];
        }
        if (I('date_s') != '' && I('date_e') != ''){
            $startTime = I('date_s');
            $endTime = I('date_e');
            if (strtotime($startTime) == strtotime($endTime)){
                $startTime = date('Y-m-d', strtotime('-1 day', strtotime($startTime)));
            }
            $taskWhere['issue_date'] = ['BETWEEN',[$startTime, $endTime]];
        }

        if (I('mediaType') != ''){
            $taskWhere['media_class'] = ['EQ',I('mediaType')];
        }

        if (I('pageSize') != ''){
            $pp = I('pageSize');
        }

        $mediaWhere = [];
        if (I('mediaName') != ''){
            $mediaWhere['fmedianame'] = ['LIKE', '%' . I('mediaName') . '%'];
        }

        if (I('region_id') != ''){
            if(I('this_region_id') == 'true'){
                $mediaOwnerSubQuery = M('tmediaowner')->field('fid')->where([
                    "fregionid" => ['EQ', I('region_id')]
                ])->buildSql();
                $mediaWhere['fmediaownerid'] = ['EXP', 'IN '. $mediaOwnerSubQuery];//地区搜索条件
            }elseif(I('region_id') != 100000){
                $region_id_rtrim = A('Common/System','Model')->get_region_left(I('region_id'));//地区ID去掉末尾的0
                $region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
                $mediaOwnerSubQuery = M('tmediaowner')->field('fid')->where([
                    "LEFT(fregionid, $region_id_strlen)" => ['EQ', $region_id_rtrim]
                ])->buildSql();
                $mediaWhere['fmediaownerid'] = ['EXP', 'IN '. $mediaOwnerSubQuery];//地区搜索条件
            }
        }

        // 组装子查询条件
        if ($tadWhere !== []){
            $tadSubQuery = M('tad')->field('fadid')->where($tadWhere)->buildSql();
            $taskWhere['fadid'] = ['EXP', 'IN ' . $tadSubQuery];
        }
        if ($mediaWhere !== []){
            $tmediaSubQuery = M('tmedia')->field('fid')->where($mediaWhere)->buildSql();
            $taskWhere['media_id'] = ['EXP', 'IN ' . $tmediaSubQuery];
        }
        $order = 'issue_date desc';
        $count = M('task_input')
            ->where($taskWhere)
            ->count();
        $Page = new \Think\Page($count,$pp);// 实例化分页类 传入总记录数和每页显示的记录数
        $this->assign('page',$Page->show());//分页输出
        $taskList = M('task_input')
            ->field("
            taskid,
            media_class,
            fadid as adid,
            (select fmedianame from tmedia where fid = media_id) as fmedianame,
            (select fadname from tad where fadid = adid) as fadname,
            (select fadclasscode from tad where fadid = adid) as fadclasscode,
            (select ffullname from tadclass where fcode = fadclasscode ) as ffullname,
            issue_date,
            task_state,
            quality_state,
            priority,
            (select fillegaltype from tillegaltype where fcode = fillegaltypecode) as fillegaltype
            ")
            ->where($taskWhere)
            ->order($order)
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();

        $mediaClassArr = ['', '电视', '广播', '报刊'];
        $taskStateArr = ['初始', '任务中', '已完成', '', '被退回', '等待撤回审核'];
        $qualityStateArr = ['未质检', '正在质检', '已质检'];
        foreach ($taskList as $key => &$value) {
            $value['media_type'] = $mediaClassArr[$value['media_class']];
            $value['task_state'] = $taskStateArr[$value['task_state']];
            $value['quality_state'] = $qualityStateArr[$value['quality_state']];
            $value['fadname_title'] = $value['fadname'];
            $value['fadname'] = r_highlight($value['fadname'], I('keyword'));
            $value['fmedianame'] = r_highlight($value['fmedianame'], I('mediaName'));
        }
        $taskList = list_k($taskList,$p,$pp);//为列表加上序号

        $illegalType = M('tillegaltype')->field('fcode, fillegaltype')->cache(true)->where(['fstate' => ['EQ', 1]])->select();
        $unlockAuth = A('Admin/Authority','Model')->authority('300933') !== 0 ? 1 : 0;
        $modifyPriorityAuth = A('Admin/Authority','Model')->authority('300932') !== 0 ? 1 : 0;
        $deleteAuth = A('Admin/Authority','Model')->authority('300934') !== 0 ? 1 : 0;
        $this->assign(compact('taskList', 'illegalType', 'unlockAuth', 'modifyPriorityAuth', 'deleteAuth'));

        $this->display();
	}

    /**
     * 解锁任务(任务中的任务)
     * @param $ids  需要解锁的任务id 用 , 间隔
     * @param $adType  string 1是网络广告, 0是传统媒体广告
     */
    public function unlockTask($ids, $adType)
    {
        if(A('Admin/Authority','Model')->authority('300933') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限解锁任务'
            ]);
        }
        if (!$ids){
            return false;
        }
        $table = $adType == 1 ? 'tnettask' : 'task_input';
        if ($adType != 1){
            $res = M($table)->where([
                'taskid' => ['IN', $ids],
                'task_state' => ['EQ', 1],
            ])->save(['task_state' => 0]) === false ? false : true;
        }else{
            $res = M($table)
                ->where([
                    'taskid' => ['IN', $ids]
                ])
                ->save([
                    'task_state' => 0,
                    'wx_id' => 0,
                    'isad' => 1,
                    'cut_err_reason' => '',
                    'cut_err_state' => 0,
                    'syn_state' => 0,
                    'notaduser' => 0,
                    'modifytime' => time()
                ]) === false ? false : true;
        }
        $logData = [
            'type' => 'save',
            'ids' => $ids,
            'adType' => $adType,
        ];
        if ($res){
            $logData['msg'] = '修改成功';
            if ($table === 'task_input'){
                A('Common/AliyunLog','Model')->task_input_log($logData);
            }else{
                A('Common/AliyunLog','Model')->net_task_log($logData);
            }
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        } else {
            $logData['msg'] = '修改失败';
            if ($table === 'task_input'){
                A('Common/AliyunLog','Model')->task_input_log($logData);
            }else{
                A('Common/AliyunLog','Model')->net_task_log($logData);
            }
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
	}

    /**
     * 修改任务优先级
     * @param $ids
     * @param $priority
     * @param $adType
     */
    public function changePriority($ids, $priority, $adType)
    {
        if(A('Admin/Authority','Model')->authority('300932') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限修改任务优先级'
            ]);
        }
        $data = [
            'priority' => $priority
        ];
        $table = $adType == 1 ? 'tnettask' : 'task_input';
        $res = M($table)->where([
            'taskid' => ['IN', $ids],
            'priority' => ['NEQ', 11],
        ])->save($data) === false ? false : true;
        $logData = [
            'type' => 'save',
            'ids' => $ids,
            'adType' => $adType,
            'priority' => $priority
        ];
        if ($res){
            $logData['msg'] = '修改成功';
            if ($table === 'task_input'){
                A('Common/AliyunLog','Model')->task_input_log($logData);
            }else{
                A('Common/AliyunLog','Model')->net_task_log($logData);
            }
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        } else {
            $logData['msg'] = '修改失败';
            if ($table === 'task_input'){
                A('Common/AliyunLog','Model')->task_input_log($logData);
            }else{
                A('Common/AliyunLog','Model')->net_task_log($logData);
            }
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
	}
    /**
     * 修改任务分组
     * @param $ids
     * @param $group_id
     * @param $adType 1-互联网 0-传统
     */
    public function setGroupId(){
        $ids = I('ids') ? I('ids') : $this->P['ids'];
        $group_id = (I('group_id') || I('group_id') === 0) ? I('group_id') : $this->P['group_id'];
        $adType = (I('adType') || I('adType') === 0) ? I('adType') : $this->P['adType'];
        $data = [
            'group_id' => $group_id
        ];
        $table = $adType == 1 ? 'tnettask' : 'task_input';
        $res = M($table)
            ->where([
                'taskid' => ['IN', $ids],
            ])
            ->save($data) === false ? false : true;
        $logData = [
            'type' => 'save',
            'ids' => $ids,
            'adType' => $adType,
            'group_id' => $group_id
        ];
        if ($res){
            $logData['msg'] = '修改成功';
            if ($table === 'task_input'){
                A('Common/AliyunLog','Model')->task_input_log($logData);
            }else{
                A('Common/AliyunLog','Model')->net_task_log($logData);
            }
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        } else {
            $logData['msg'] = '修改失败';
            if ($table === 'task_input'){
                A('Common/AliyunLog','Model')->task_input_log($logData);
            }else{
                A('Common/AliyunLog','Model')->net_task_log($logData);
            }
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
	}

    /**
     * 删除任务
     * @param $ids
     * @param $adType
     */
    public function deleteTask($ids, $adType)
    {
        if(A('Admin/Authority','Model')->authority('300934') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限删除任务'
            ]);
        }
        $table = $adType == 1 ? 'tnettask' : 'task_input';
        $taskDeleteRes = M($table)->where(['taskid' => ['IN', $ids]])->delete() === false ? false : true;
        $logData = [
            'type' => 'save',
            'ids' => $ids,
            'adType' => $adType,
        ];
        if ($taskDeleteRes){
            $logData['msg'] = '删除成功';
            if ($table === 'task_input'){
                A('Common/AliyunLog','Model')->task_input_log($logData);
            }else{
                A('Common/AliyunLog','Model')->net_task_log($logData);
            }
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '删除成功'
            ]);
        } else {
            $logData['msg'] = '删除失败';
            if ($table === 'task_input'){
                A('Common/AliyunLog','Model')->task_input_log($logData);
            }else{
                A('Common/AliyunLog','Model')->net_task_log($logData);
            }
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '删除失败'
            ]);
        }
	}

    /**
     * 查看任务流程
     * @param $taskid
     */
    public function checkTaskFlow($taskid, $adType)
    {
        $table = $adType == 1 ? 'tnettask_flowlog' : 'task_input_flow';
        if ($table === 'task_input_flow'){
            $flowList = M('task_input_flow')
                ->where(['taskid' => ['EQ', $taskid]])
                ->select();
            foreach ($flowList as $key => $value) {
                $flowList[$key]['nickname'] = $value['wx_id'] ? M('ad_input_user')->cache(true)->find($value['wx_id'])['alias'] : '';
                $flowList[$key]['flow_time'] = date('Y-m-d H:i:s', $value['flow_time']);
            }
        }else{
            $flowList = M('tnettask_flowlog')
                ->field('log flow_content, createtime flow_time, creator wx_id')
                ->where(['taskid' => ['EQ', $taskid]])
                ->select();
            foreach ($flowList as $key => $value) {
                $flowList[$key]['nickname'] = $value['wx_id'] ? M('ad_input_user')->cache(true)->find($value['wx_id'])['alias'] : '';
                $flowList[$key]['flow_time'] = date('Y-m-d H:i:s', $value['flow_time']);
            }
        }

        $this->assign(compact('flowList'));
        $this->display();
	}

    /**
     * 批量修改任务优先级
     */
    public function batchModifyTaskPriority()
    {
        if(A('Admin/Authority','Model')->authority('300932') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限修改任务优先级'
            ]);
        }
        $ids = I('ids');
        $priority = I('priority');
        $res = M('task_input')->where(['taskid' => ['IN', $ids]])->save(['priority' => $priority]) === false ? false : true;
        $logData = [
            'type' => 'save',
            'ids' => $ids,
            'priority' => $priority,
        ];
        if ($res){
            $logData['msg'] = '任务优先级修改成功';
            A('Common/AliyunLog','Model')->task_input_log($logData);
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        }else{
            $logData['msg'] = '任务优先级修改失败';
            A('Common/AliyunLog','Model')->task_input_log($logData);
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
    }

    /**
     * 众包任务管理
     */
    public function index()
    {
        if (IS_POST){
            // 1是网络广告 0 是传统广告
            $adType = I('isNetAd', '0');
            if (I('taskid') != ''){
                if ($adType === '1'){
                    $adController = new NetAdController();
                    $adController->index();
                }else{
                    $adController = new TraditionAdController();
                    $adController->index();
                }
            }
            $formData = I('where');
            $page = I('page', 1);
            $pageSize = I('pageSize', 10);
            $data = $this->baseTaskSearch($adType, $formData, $page, $pageSize);
            $this->ajaxReturn($data);
        }else{
            $illegalTypeArr = json_encode($this->getIllegalTypeArr());
            $adClassArr = json_encode($this->getAdClassArr());
            $adClassTree = json_encode($this->getAdClassTree());
            $regionTree = json_encode($this->getRegionTree());
            $illegalArr = json_encode($this->getIllegalArr());
            $this->assign(compact('illegalTypeArr', 'adClassArr', 'adClassTree', 'regionTree', 'illegalArr'));
            $this->display('taskList');
        }
    }
    /**
     * @Des: 管理员修改任务广告信息
     * @Edt: yuhou.wang
     * @Date: 2019-12-20 09:09:27
     * @param {type} 
     * @return: 
     */    
    public function submitAdInputTask(){
        $I = I('data');
        if(!empty($I)){
            $oriTaskInfo = M('task_input')->where(['taskid'=>$I['taskid']])->find();
            $personInfo = 'DMP_'.session('personInfo.fid').session('personInfo.fname');
            $fadid = A('Common/Ad','Model')->getAdId($I['fadname'],$I['fbrand'],$I['fadclasscode'],$I['fname'],$personInfo, $I['fadclasscode_v2']);//获取广告ID
            // 更新任务信息
            $data = [
                'fadid'       => $fadid,
                'fspokesman'  => $I['fspokesman'],
                'fversion'    => $I['fversion'],
                'is_long_ad'  => $I['is_long_ad'],
                'fadmanuno'   => $I['fadmanuno'],
                'fmanuno'     => $I['fmanuno'],
                'fadapprno'   => $I['fadapprno'],
                'fapprno'     => $I['fapprno'],
                'fadent'      => $I['fadent'],
                'fent'        => $I['fent'],
                'fentzone'    => $I['fentzone'],
                'fmodifier'   => $personInfo,
                'fmodifytime' => date('Y-m-d H:i:s'),
                'syn_state'   => 1,//需同步
            ];
            $resSave = M('task_input')->where(['taskid'=>$I['taskid']])->save($data);
            // 更新流程日志
            $changeTaskInfo = [
                'before'=>$oriTaskInfo,
                'submit'=>$data
            ];
            $flow = [
                'taskid'       => $I['taskid'],
                'flow_content' => 'DMP管理员修改任务广告信息_'.$personInfo,
                'flow_time'    => time(),
                'wx_id'        => session('personInfo.fid'),
                'task_info'    => json_encode($changeTaskInfo),
            ];
            $resFlow = M('task_input_flow')->add($flow);
            $this->ajaxReturn(['code'=>0,'msg'=>'提交成功']);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }
    /**
     * @Des: 管理员修改任务违法判定
     * @Edt: yuhou.wang
     * @Date: 2019-12-20 09:09:27
     * @param {type} 
     * @return: 
     */    
    public function submitJudgeTask(){
        $I = I('data');
        if(!empty($I)){
            $oriTaskInfo = M('task_input')->where(['taskid'=>$I['taskid']])->find();
            $personInfo = 'DMP_'.session('personInfo.fid').session('personInfo.fname');
            // 更新任务信息
            $data = [
                'fillegaltypecode' => $I['fillegaltypecode'],
                'fexpressioncodes' => $I['fexpressioncodes'],
                'fillegalcontent'  => $I['fillegalcontent'],
                'fadmanuno'        => $I['fadmanuno'],
                'fmanuno'          => $I['fmanuno'],
                'fadapprno'        => $I['fadapprno'],
                'fapprno'          => $I['fapprno'],
                'fadent'           => $I['fadent'],
                'fent'             => $I['fent'],
                'fentzone'         => $I['fentzone'],
                'fmodifier'        => $personInfo,
                'fmodifytime'      => date('Y-m-d H:i:s'),
                'syn_state'        => 1,//需同步
            ];
            $resSave = M('task_input')->where(['taskid'=>$I['taskid']])->save($data);
            // 更新流程日志
            $changeTaskInfo = [
                'before'=>$oriTaskInfo,
                'submit'=>$data
            ];
            $flow = [
                'taskid'       => $I['taskid'],
                'flow_content' => 'DMP管理员修改任务违法判定_'.$personInfo,
                'flow_time'    => time(),
                'wx_id'        => session('personInfo.fid'),
                'task_info'    => json_encode($changeTaskInfo),
            ];
            $resFlow = M('task_input_flow')->add($flow);
            $this->ajaxReturn(['code'=>0,'msg'=>'提交成功']);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
    }

    /**
     * 基础任务搜索方法
     * @param $type   string  任务的类别   0为传统媒体广告, 1为网络媒体广告
     * @param $formData
     * @param $page
     * @param int $pageSize
     * @return array
     */
    public function baseTaskSearch($type, $formData, $page, $pageSize=10)
    {
        $issueDateField = $type == '1' ? 'net_created_date' : 'issue_date';  // 发布时间字段不同
        $updateTimeField = $type == '1' ? 'modifytime' : 'lasttime';  // 发布时间字段不同
        $createTimeField = $type == '1' ? 'createtime' : 'fcreatetime';  // 创建时间字段不同
        $table = $type == '1' ? 'tnettask' : 'task_input';  // 表名不同
        $adId = $type == '1' ? 'major_key' : 'fadid';   // 广告主键不同
        $where = [];
        if ($formData['media_class'] != ''){
            $where['media_class'] = ['EQ', $formData['media_class']];
        }
        if ($formData['fillegaltypecode'] != ''){
            $where['fillegaltypecode'] = ['EQ', $formData['fillegaltypecode']];
        }
        if ($formData['task_state'] != ''){
            $where['task_state'] = ['EQ', $formData['task_state']];
        }
        if ($formData['cutError'] == 1){
            if ($type == '1'){
                $where['isad'] = ['EQ', 0];
            }else{
                $where['cut_err_state'] = ['EQ', 1];
            }
        }else{
            if ($type == '1'){
                $where['isad'] = ['EQ', 1];
            }else{
                $where['cut_err_state'] = ['EQ', 0];
            }
        }
        if ($formData['quality_state'] != ''){
            $where['quality_state'] = ['EQ', $formData['quality_state']];
        }
        if ($formData['timeRangeArr'] != ''){
            $where[$issueDateField] = ['BETWEEN', $formData['timeRangeArr']];
            if ($table == 'tnettask'){
                $where[$issueDateField] = ['BETWEEN', [strtotime($formData['timeRangeArr'][0]), strtotime("+1 day",strtotime($formData['timeRangeArr'][1])) - 1]];// 结束日期转时间戳 23：59：59
            }
        }
        if ($formData['timeRangeArr2'] != ''){
            $where[$updateTimeField] = ['BETWEEN', [strtotime($formData['timeRangeArr2'][0]), strtotime($formData['timeRangeArr2'][1])]];
            if ($table == 'tnettask'){
                $where[$updateTimeField] = ['BETWEEN', [strtotime($formData['timeRangeArr2'][0]), strtotime($formData['timeRangeArr2'][1])]];
            }
        }
        // 任务创建时间范围
        if ($formData['timeRangeArrCreate'] != ''){
            $where[$createTimeField] = ['BETWEEN', [date('Y-m-d H:i:s',strtotime($formData['timeRangeArrCreate'][0])), date('Y-m-d H:i:s',strtotime($formData['timeRangeArrCreate'][1]))]];
            if ($table == 'tnettask'){
                $where[$createTimeField] = ['BETWEEN', [strtotime($formData['timeRangeArrCreate'][0]), strtotime($formData['timeRangeArrCreate'][1])]];
            }
        }
        if (!empty($formData['fpriority'])){
            if(in_array('EGT',$formData['fpriority']) || in_array('ELT',$formData['fpriority'])){
                $min = ($formData['fpriority'][0]=='EGT' || $formData['fpriority'][0]=='ELT') ? $formData['fpriority'][0] : $formData['fpriority'][1];
                $max = ($formData['fpriority'][0]=='EGT' || $formData['fpriority'][0]=='ELT') ? $formData['fpriority'][1] : $formData['fpriority'][0];
                $formData['fpriority'][0] = $min;
                $formData['fpriority'][1] = $max;
                $where['priority'] = $formData['fpriority'];
            }else{
                $min = ($formData['fpriority'][0] <= $formData['fpriority'][1]) ? $formData['fpriority'][0] : $formData['fpriority'][1];
                $max = ($formData['fpriority'][0] <= $formData['fpriority'][1]) ? $formData['fpriority'][1] : $formData['fpriority'][0];
                $formData['fpriority'][0] = $min;
                $formData['fpriority'][1] = $max;
                $where['priority'] = ['BETWEEN', $formData['fpriority']];
            }
        }
        if (!empty($formData['group_id']) || $formData['group_id'] === 0){
            $where['group_id'] = $formData['group_id'];
        }
        if (!empty($formData['sam_uuid'])){
            if($table == 'tnettask'){
                $where['identify_code'] = $formData['sam_uuid'];
            }else{
                $where['sam_uuid'] = $formData['sam_uuid'];
            }
        }
        if (!empty($formData['taskid'])){
            $where['taskid'] = $formData['taskid'];
        }
        if (!empty($formData['major_key'])){
            $where['major_key'] = $formData['major_key'];
        }
        if (!empty($formData['fversion'])){
            $where['fversion'] = ['LIKE','%'.$formData['fversion'].'%'];
        }

        if ($formData['wx_id'] != ''){
            $wxSubQuery = M('ad_input_user')
                ->field('wx_id')
                ->where([
                    'alias|nickname' => ['LIKE', '%'.$formData['wx_id'].'%']
                ])
                ->buildSql();
            $where['wx_id'] = ['EXP', 'IN '.$wxSubQuery];
        }
        // tad表
        $adWhere = [];
        if ($formData['adClass'] != ''){
            $len = strlen($formData['adClass']);
            $adWhere["LEFT(fadclasscode, $len)"] = ['EQ', $formData['adClass']];
        }
        if ($formData['adClass2'] != ''){
            $len = strlen($formData['adClass2']);
            $adWhere["LEFT(fadclasscode_v2, $len)"] = ['EQ', $formData['adClass2']];
        }
        if ($formData['adName'] != ''){
            $adWhere['fadname'] = ['LIKE', '%'.$formData['adName'].'%'];
        }
        if ($formData['adOwner'] != ''){
            $adOwnerSubQuery = M('tadowner')
                ->field('fid')
                ->where([
                    'fname' => ['LIKE', '%'.$formData['adOwner'].'%']
                ])
                ->buildSql();
            $adWhere['fadowner'] = ['EXP', " IN ".$adOwnerSubQuery];
        }
        if ($formData['net_platform'] != '' && $table == 'tnettask'){
            $adWhere['net_platform'] = ['EQ', $formData['net_platform']];
        }
        // tmedia表
        $mediaWhere = [];
        if ($formData['mediaName'] != ''){
            $mediaWhere['fmedianame'] = ['LIKE', '%'.$formData['mediaName'].'%'];
        }
        // tmediaowner表
        $mediaOwnerWhere = [];
        if ($formData['regionId'] != ''){
            if ($formData['onlyThisLevel'] != 1){
                $region_id_rtrim = A('Common/System','Model')->get_region_left($formData['regionId']);//地区ID去掉末尾的0
                $region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
                $mediaOwnerWhere["LEFT(fregionid, $region_id_strlen)"] = ['EQ', $region_id_rtrim];
            }else{
                $mediaOwnerWhere['fregionid'] = ['LIKE', '%'.$formData['regionId'].'%'];
            }
            $mediaOwnerSubQuery = M('tmediaowner')
                ->field('fid')
                ->where($mediaOwnerWhere)
                ->buildSql();
            $mediaWhere['fmediaownerid'] = ['EXP', 'IN '. $mediaOwnerSubQuery];//地区搜索条件
        }
        // 媒介标签搜索
        if ($formData['mediaLabel'] != ''){
            $labelSubQuery = M('tlabel')
                ->field('flabelid')
                ->where([
                    'fstate' => ['EQ', 1],
                    'flabel' => ['EQ', $formData['mediaLabel']],
                ])
                ->buildSql();
            $mediaLabelSubQuery = M('tmedialabel')
                ->field('fmediaid')
                ->where([
                    'fstate' => ['EQ', 1],
                    'flabelid' => ['EXP', ' IN ' . $labelSubQuery]
                ])
                ->buildSql();

            $mediaWhere['fid'] = ['EXP', ' IN ' . $mediaLabelSubQuery];
        }
        if ($mediaWhere != []){
            $mediaSubQuery = M('tmedia')
                ->field('fid')
                ->where($mediaWhere)
                ->buildSql();
            $where['media_id'] = ['EXP', 'IN '. $mediaSubQuery]; // 媒体搜索条件
        }
        if ($adWhere != []){
            if ($table == 'tnettask'){
                $adSubQuery = M('tnetissue')
                    ->field('major_key')
                    ->where($adWhere)
                    ->buildSql();
                $where['major_key'] = ['EXP', 'IN '. $adSubQuery]; // 广告搜索条件
            }else{
                $adSubQuery = M('tad')
                    ->field('fadid')
                    ->where($adWhere)
                    ->buildSql();
                    $where['_string'] = "(fadid IN $adSubQuery OR tem_ad_name LIKE ('%{$formData['adName']}%') )";
            }

        }
        $fieldList = "taskid,$adId, media_class, fillegaltypecode, task_state, quality_state, $issueDateField, cut_err_state, wx_id, back_reason, media_id, priority,group_id,$createTimeField,$updateTimeField,sam_uuid uuid";
        if ($table === 'task_input'){
            $fieldList .= ', tem_ad_name';
        }else{
            $fieldList .= ", major_key, (select net_platform from tnetissue where major_key = $table.major_key) AS net_platform,identify_code uuid";
        }
        $res = M($table)
            ->field($fieldList)
            ->where($where)
            ->order("$updateTimeField desc")
            ->page($page, $pageSize)
            ->select();
        $sql = M($table)->getLastSql();
        $count = M($table)
            ->where($where)
            ->count();
        foreach ($res as $key => $value) {
            $res[$key]['alias'] = M('ad_input_user')->cache(true)->where(['wx_id' => ['EQ', $value['wx_id']]])->getField('alias');
            if ($table == 'tnettask'){
                $res[$key]['fmedianame'] = M('tmedia')->where(['fid' => ['EQ', $value['media_id']]])->getField('fmedianame');
                $adInfo = M('tnetissue')->field('fadname, fadclasscode,fadclasscode_v2, fadowner, fbrand')->find($value[$adId]);
                $res[$key][$issueDateField] = date('Y-m-d', $value[$issueDateField]);
                $res[$key][$createTimeField] = date('Y-m-d H:i:s', $value[$createTimeField]);
            }else{
                $res[$key]['fmedianame'] = M('tmedia')->cache(true)->where(['fid' => ['EQ', $value['media_id']]])->getField('fmedianame');
                $adInfo = M('tad')->field('fadname, fadclasscode,fadclasscode_v2, fadowner, fbrand')->find($value[$adId]);
                $res[$key]['tem_ad_name'] = r_highlight($res[$key]['tem_ad_name'], $formData['adName'], 'red');

            }
            $res[$key][$updateTimeField] = date('Y-m-d H:i:s', $value[$updateTimeField]);
            $res[$key]['fillegaltypecode'] = $res[$key]['fillegaltypecode'] ? $res[$key]['fillegaltypecode'] : '0';
            $res[$key]['fadname'] = $adInfo['fadname'];
            $res[$key]['fadclasscode'] = $adInfo['fadclasscode'];
            $res[$key]['fbrand'] = $adInfo['fbrand'];
            $res[$key]['ffullname'] = M('tadclass')->cache(true)->where(['fcode' => ['EQ', $adInfo['fadclasscode']]])->getField('ffullname');
            $res[$key]['adClassV2'] = M('hz_ad_class')->cache(true)->where(['fcode' => ['EQ', $adInfo['fadclasscode_v2']]])->getField('ffullname');
            $res[$key]['adOwnerName'] = M('tadowner')->cache(true)->where(['fid' => ['EQ', $adInfo['fadowner']]])->getField('fname');
            $res[$key]['fadname'] = r_highlight($res[$key]['fadname'], $formData['adName'], 'red');
            $res[$key]['fmedianame'] = r_highlight($res[$key]['fmedianame'], $formData['mediaName'], 'red');

        }
        $data = [
            'data' => $res,
            'total' => $count,
            'page' => $page,
//            'where' => $mediaWhere
        ];
        return $data;
    }

    /**
     * 违法类型
     * @return mixed
     */
    protected function getIllegalTypeArr()
    {
        $illegalType = M('tillegaltype')->field('fcode value, fillegaltype text')->cache(true)->where(['fstate' => ['EQ', 1]])->select();
        return $illegalType;
    }
    /**
     * 广告类别树(多级)
     */
    protected function getAdClassArr()
    {
        $data = M('tadclass')
            ->cache(true)
            ->field('fcode value, fpcode, fadclass label')
            ->where(['fstate' => ['EQ', 1], 'fcode' => ['NEQ', 2301]])
            ->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpcode']])){
                $items[$value['fpcode']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }

    /**
     * 返回广告类型树
     * @return array
     */
    protected function getAdClassArr_V20200310()
    {
        $data = M('tadclass')
            ->field('fcode, fpcode, fadclass')
            ->where(['fstate' => ['EQ', 1], 'fcode' => ['NEQ', 2301]])
            ->cache(true)
            ->select();
        $res = [];
        foreach ($data as $key => &$value) {
            if (strlen($value['fcode']) === 2){
                $res[] = [
                    'value' => $value['fcode'],
                    'label' => $value['fadclass'],
                ];
            }
        }
        foreach ($data as $key => &$value2) {
            $value2['value'] = $value2['fcode'];
            $value2['label'] = $value2['fadclass'];
            if (strlen($value2['fcode']) === 4){
                foreach ($res as &$item) {
                    if ($value2['fpcode'] === $item['value']){
                        $item['children'][] = $value2;
                        break;
                    }
                }
            }
        }
        return $res;
    }

    /**
     * 商业类型树
     */
    protected function getAdClassTree()
    {
        $data = M('hz_ad_class')->field('fcode value, fpcode, fname label')->where(['fstatus' => ['EQ', 1]])->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpcode']])){
                $items[$value['fpcode']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }

    /**
     * 获取行政区划树
     * @return array
     */
    protected function getRegionTree()
    {
        $data = M('tregion')->field('fid value, fpid, fname label')->cache(true)->where(['fstate' => ['EQ', 1], 'fid' => ['NEQ', 100000]])->select();
        $items = [];
        foreach($data as $value){
            $items[$value['value']] = $value;
        }
        $tree = [];
        foreach($items as $key => $value){
            if(isset($items[$value['fpid']])){
                $items[$value['fpid']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }

    /**
     * 返回违法表现树
     * @return array
     */
    protected function getIllegalArr()
    {
        $data = M('tillegal')->field('fcode, fpcode, fexpression, fconfirmation, fillegaltype')->where(['fstate' => ['EQ', 1]])->cache(true)->select();
        $res = [];
        foreach ($data as &$datum) {
            $datum['id'] = $datum['fcode'];
            $datum['label'] = $datum['fexpression'];
            if (!$datum['fpcode']){
                $res[] = $datum;
            }
        }
        foreach ($data as $item) {
            foreach ($res as &$re) {
                if ($item['fpcode'] === $re['fcode']){
                    $re['children'][] = $item;
                    break;
                }
            }
        }
        return $res;
    }
}