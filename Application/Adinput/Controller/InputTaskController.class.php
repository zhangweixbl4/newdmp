<?php
namespace Adinput\Controller;
use Think\Controller;
class InputTaskController extends Controller {
	
    public function index(){
		
	}
	
	
	/*任务分配*/
	public function task_assign(){
		$user_info = get_user_info();//获取用户信息
		$taskInfo = M('ad_input_task')->where(array('task_state'=>1,'user_id'=>$user_info['wx_id']))->find();//获取已接任务信息
		
		if($taskInfo){//如果有任务直接返回已接任务
		
			if($taskInfo['mediaclass'] == '电视') $url = U('Adinput/InputTask/tv_task');//电视任务跳转的链接
			if($taskInfo['mediaclass'] == '广播') $url = U('Adinput/InputTask/bc_task');//广播任务的跳转链接
			if($taskInfo['mediaclass'] == '报纸') $url = U('Adinput/InputTask/paper_task');//报纸任务的跳转链接
			$this->ajaxReturn(array('code'=>0,'msg'=>'','url'=>$url));
		}
		
		if($user_info['input_seniority'] != 1){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有广告录入资质'));//判断是否有广告录入资质 
		}
		
		$tvTaskInfo = M('ttvsample')->where(array('finputstate'=>1))->find();//电视任务信息
		$bcTaskInfo = M('tbcsample')->where(array('finputstate'=>1))->find();//广播任务信息
		$paperTaskInfo = M('tpapersample')->where(array('finputstate'=>1))->find();//报纸任务信息
		
		if($tvTaskInfo){//判断是否有电视任务
			$task_id = A('Adinput/InputTask','Model')->add_tv_task($tvTaskInfo);
			if($task_id > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'','url'=>U('Adinput/InputTask/tv_task')));
		}elseif($bcTaskInfo){//判断是否有广播任务
			$task_id = A('Adinput/InputTask','Model')->add_bc_task($bcTaskInfo);
			if($task_id > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'','url'=>U('Adinput/InputTask/bc_task')));
		}elseif($paperTaskInfo){//判断是否有报纸任务
			$task_id = A('Adinput/InputTask','Model')->add_paper_task($paperTaskInfo);
			if($task_id > 0) $this->ajaxReturn(array('code'=>0,'msg'=>'','url'=>U('Adinput/InputTask/paper_task')));
		}else{//都没有
			$this->ajaxReturn(array('code'=>-1,'msg'=>'当前已无任务'));
		}

	}
	
	/*电视广告任务*/
	public function tv_task(){
		
		$user_info = get_user_info();//获取用户信息
		$taskInfo = M('ad_input_task')
									->field('ad_input_task.*,ttvsample.favifilename,ttvsample.favifilepng')
									->join('ttvsample on ttvsample.fid = ad_input_task.sample_id')
									->where(array('task_state'=>1,'user_id'=>$user_info['wx_id'],'ad_input_task.mediaclass'=>'电视'))->find();
		//var_dump($taskInfo);
		$this->assign('taskInfo',$taskInfo);
		$this->display();
	}
	/*广播广告任务*/
	public function bc_task(){
		
		$user_info = get_user_info();//获取用户信息
		$taskInfo = M('ad_input_task')
									->field('ad_input_task.*,tbcsample.favifilename')
									->join('tbcsample on tbcsample.fid = ad_input_task.sample_id')
									->where(array('task_state'=>1,'user_id'=>$user_info['wx_id'],'ad_input_task.mediaclass'=>'广播'))->find();
		//var_dump($taskInfo);
		$this->assign('taskInfo',$taskInfo);
		$this->display();
	}
	/*报纸广告任务*/
	public function paper_task(){
		
		$user_info = get_user_info();//获取用户信息
		$taskInfo = M('ad_input_task')
									->field('ad_input_task.*,tpapersample.fjpgfilename')
									->join('tpapersample on tpapersample.fpapersampleid = ad_input_task.sample_id')
									->where(array('task_state'=>1,'user_id'=>$user_info['wx_id'],'ad_input_task.mediaclass'=>'报纸'))->find();
		//var_dump($taskInfo);
		$this->assign('taskInfo',$taskInfo);
		$this->display();
	}
	
	/*电视广告任务录入*/
	public function ajax_tv_task(){
		$user_info = get_user_info();
		$task_id = I('task_id');
		$fadname = I('fadname');//广告名称
		if ($fadname == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'广告名称不能为空'));
			
		$fadclasscode = I('fadclasscode');//广告类别
		if(M('tadclass')->where(array('fcode'=>$fadclasscode))->count() == 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'广告类别错误'));
		$fbrand = I('fbrand');//品牌
		$adowner_name = I('adowner_name');//广告主
		$fspokesman = I('fspokesman');//代言人
		$fversion = I('fversion');//版本描述
		$fapprovalid = I('fapprovalid');//审批号
		$fapprovalunit = I('fapprovalunit');//审批单位
		
		
		$ad_id = A('Adinput/InputTask','Model')->get_ad_id($fadname,$fbrand,$fadclasscode,$adowner_name);//获取广告ID
		$taskInfo = M('ad_input_task')->where(array('task_id'=>$task_id))->find();
		$sample_id = $taskInfo['sample_id'];
		$rr = M('ttvsample')->where(array('fid'=>$sample_id))->save(array(
																	'fspokesman'	=> $fspokesman,//代言人
																	'fversion'		=> $fversion,//版本描述
																	'fapprovalid'	=> $fapprovalid,//审批号
																	'fapprovalunit'	=> $fapprovalunit,//审批单位
																	
																	'fadid'			=> $ad_id,//广告ID
																	'finputstate'	=> 3,//录入状态改为3
																	'fmodifier' 	=> $user_info['nickname'],//修改人
																	'fmodifytime' 	=> date('Y-m-d H:i:s'),//修改时间
																));
		M('ad_input_task')->where(array('task_id'=>$task_id))->save(array('task_state'=>2,'complete_time'=> date('Y-m-d H:i:s')));//修改任务状态

		A('Adinput/User','Model')->money($user_info['wx_id'],$taskInfo['money'],'录入广告,任务ID:'.$task_id);//增加余额

		$this->ajaxReturn(array('code'=>0,'msg'=>'录入任务成功'));
		
	}
	
	/*广播广告任务录入*/
	public function ajax_bc_task(){
		$user_info = get_user_info();
		$task_id = I('task_id');
		$fadname = I('fadname');//广告名称
		if ($fadname == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'广告名称不能为空'));
			
		$fadclasscode = I('fadclasscode');//广告类别
		$fbrand = I('fbrand');//品牌
		$adowner_name = I('adowner_name');//广告主
		$fspokesman = I('fspokesman');//代言人
		$fversion = I('fversion');//版本描述
		$fapprovalid = I('fapprovalid');//审批号
		$fapprovalunit = I('fapprovalunit');//审批单位
		
		
		$ad_id = A('Adinput/InputTask','Model')->get_ad_id($fadname,$fbrand,$fadclasscode,$adowner_name);//获取广告ID
		$taskInfo = M('ad_input_task')->where(array('task_id'=>$task_id))->find();
		$sample_id = $taskInfo['sample_id'];
		$rr = M('tbcsample')->where(array('fid'=>$sample_id))->save(array(
																	'fspokesman'	=> $fspokesman,//代言人
																	'fversion'		=> $fversion,//版本描述
																	'fapprovalid'	=> $fapprovalid,//审批号
																	'fapprovalunit'	=> $fapprovalunit,//审批单位
																	
																	'fadid'			=> $ad_id,//广告ID
																	'finputstate'	=> 3,//录入状态改为3
																	'fmodifier' 	=> $user_info['nickname'],//修改人
																	'fmodifytime' 	=> date('Y-m-d H:i:s'),//修改时间
																));
		M('ad_input_task')->where(array('task_id'=>$task_id))->save(array('task_state'=>2,'complete_time'=> date('Y-m-d H:i:s')));//修改任务状态

		A('Adinput/User','Model')->money($user_info['wx_id'],$taskInfo['money'],'录入广告,任务ID:'.$task_id);//增加余额

		$this->ajaxReturn(array('code'=>0,'msg'=>'录入任务成功'));
		
	}
	
	/*报纸广告任务录入*/
	public function ajax_paper_task(){
		$user_info = get_user_info();
		$task_id = I('task_id');
		$fadname = I('fadname');//广告名称
		if ($fadname == '') $this->ajaxReturn(array('code'=>-1,'msg'=>'广告名称不能为空'));
			
		$fadclasscode = I('fadclasscode');//广告类别
		$fbrand = I('fbrand');//品牌
		$adowner_name = I('adowner_name');//广告主
		$fspokesman = I('fspokesman');//代言人
		$fversion = I('fversion');//版本描述
		$fapprovalid = I('fapprovalid');//审批号
		$fapprovalunit = I('fapprovalunit');//审批单位
		
		
		$ad_id = A('Adinput/InputTask','Model')->get_ad_id($fadname,$fbrand,$fadclasscode,$adowner_name);//获取广告ID
		$taskInfo = M('ad_input_task')->where(array('task_id'=>$task_id))->find();
		$sample_id = $taskInfo['sample_id'];
		$rr = M('tpapersample')->where(array('fpapersampleid'=>$sample_id))->save(array(
																	'fspokesman'	=> $fspokesman,//代言人
																	'fversion'		=> $fversion,//版本描述
																	'fapprovalid'	=> $fapprovalid,//审批号
																	'fapprovalunit'	=> $fapprovalunit,//审批单位
																	
																	'fadid'			=> $ad_id,//广告ID
																	'finputstate'	=> 3,//录入状态改为3
																	'fmodifier' 	=> $user_info['nickname'],//修改人
																	'fmodifytime' 	=> date('Y-m-d H:i:s'),//修改时间
																));
		M('ad_input_task')->where(array('task_id'=>$task_id))->save(array('task_state'=>2,'complete_time'=> date('Y-m-d H:i:s')));//修改任务状态
		A('Adinput/User','Model')->money($user_info['wx_id'],$taskInfo['money'],'录入广告,任务ID:'.$task_id);//增加余额
		$this->ajaxReturn(array('code'=>0,'msg'=>'录入任务成功'));
		
	}
	
	
	
	
	
	

	
	
	
	

}