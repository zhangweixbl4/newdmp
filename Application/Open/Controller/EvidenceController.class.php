<?php
/*
 * @Description: 存证公证控制器
 * @Author: yuhou.wang
 * @Date: 2019-10-10 15:41:49
 * @LastEditors: yuhou.wang
 * @LastEditTime: 2019-11-05 17:51:24
 */
namespace Open\Controller;
use Think\Controller;
use Open\Model;
class EvidenceController extends BaseController {
    // 接收参数
    protected $P;
    /**
     * 初始化
     */
    public function _initialize(){
        parent::_initialize();
        $this->P = json_decode(file_get_contents('php://input'),true);
    }
	/**
	 * @Des: 保存证据
	 * @Edt: yuhou.wang
	 * @param {Array} $evidences 证据,支持单条或批量
	 * @return: 
	 * @Date: 2019-10-10 14:31:18
	 */
	public function addEvidence(){
		ini_set('max_execution_time', 0);//设置超时时间
        $evidences = $this->P['evidences'] ? $this->P['evidences'] : I('evidences');
        if($evidences){
			$result = A('Open/Evidence','Model')->addEvidences($evidences);
			$this->ajaxReturn($result);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
	}
	/**
	 * @Des: 定时任务-互联网违法广告存证
	 * @Edt: yuhou.wang
	 * @param {String} $url 证据URL
	 * @return: 
	 * @Date: 2019-10-10 14:31:18
	 */
	public function autoAddEvidence(){
		ini_set('max_execution_time', 0);//设置超时时间
		$fregion_id = $this->P['fregion_id'];// ? $this->P['fregion_id'] : ['EQ',330100];
		$create_time = $this->P['create_time'] ? $this->P['create_time'] : ['GT',date('Y-m-d H:i:s',strtotime('-3 months'))];
		$limit = $this->P['limit'] ? $this->P['limit'] : 100;
		// 已存样本
		$sam_evi = M('tb_evidence_sam')->where(['fmediaclass'=>13])->getField('fsample_id',true);
		// 获取指定地区违法样本
		$where = [
			'fmedia_class'     => 13,
			'create_time'	   => $create_time,
			'fillegaltypecode' => ['GT',0],
		];
		if(!empty($sam_evi)){
			$where['fsample_id'] = ['NOT IN',$sam_evi];
		}
		if(!empty($fregion_id)){
			$where['fregion_id'] = $fregion_id;
		}
		$evidences = M('tbn_illegal_ad')
			->field('favifilename url,fad_name title,fsample_id,(CASE fmedia_class WHEN 1 THEN "01" WHEN 2 THEN "02" WHEN 3 THEN "03" WHEN 13 THEN "13" END) fmediaclass')
			->where($where)
			->order('create_time DESC')
			->limit($limit)
			->select();
		// 发送存证
		if(!empty($evidences)){
			$result = A('Open/Evidence','Model')->addEvidences($evidences);
			$this->ajaxReturn($result);
		}
		$this->ajaxReturn(['code'=>1,'msg'=>'无新数据']);
	}
	/**
	 * @Des: 定时任务-互联网违法广告存证
	 * @Edt: yuhou.wang
	 * @param {String} $url 证据URL
	 * @return: 
	 * @Date: 2019-10-10 14:31:18
	 */
	public function autoAddEvidenceIllegal(){
		ini_set('max_execution_time', 0);//设置超时时间
		$limit = $this->P['limit'] ? $this->P['limit'] : 100;
		// 已存样本
		$sam_evi = M('tb_evidence_sam')->cache(true)->where(['fmediaclass'=>13])->getField('fsample_id',true);
		$where = [
			'fillegaltypecode'=>['GT',0]
		];
		if(!empty($sam_evi)){
			$where['major_key'] = ['NOT IN',$sam_evi];
		}
		// 获取最新N条违法样本
		$evidences = M('tnetissue')
			->field('net_target_url url,fadname title,major_key fsample_id,"13" fmediaclass')
			->where($where)
			->order('fmodifytime DESC')
			->limit($limit)
			->select();
		// 发送存证
		$result = A('Open/Evidence','Model')->addEvidences($evidences);
		$this->ajaxReturn($result);
	}
	/**
	 * @Des: 定时任务-补全存证信息
	 * @Edt: yuhou.wang
	 * @param {String} $url 证据URL
	 * @return: 
	 * @Date: 2019-10-10 14:31:18
	 */
	public function autoAppendInfo(){
		ini_set('max_execution_time', 0);//设置超时时间
		$limit = $this->P['limit'] ? $this->P['limit'] : 10;
		$rids = $this->P['rids'] ? $this->P['rids'] : [];
		if(empty($rids)){
			// 获取待存证码
			$rids = M('tb_evidence')->where(['status'=>0])->limit($limit)->getField('rid',true);
		}
		// 补全
		if(!empty($rids)){
			$result = A('Open/Evidence','Model')->appendInfo($rids);
		}
		$this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$result]);
	}
	/**
	 * @Des: 定时任务-更新关联违法样本公证码
	 * @Edt: yuhou.wang
	 * @param {String} $url 证据URL
	 * @return: 
	 * @Date: 2019-10-10 14:31:18
	 */
	public function autoAppendIllegalRid(){
		ini_set('max_execution_time', 0);//设置超时时间
		// 获取待存证码
		$evidence = M('tb_evidence')->field('rid,url')->where(['status'=>1])->select();
		// 更新违法样本表存证码
		foreach($evidence as $row){
			$retIllegal = M('tbn_illegal_ad')->where(['favifilename'=>$row['url'],'ftb_evidence_rid'=>0])->save(['ftb_evidence_rid'=>$row['rid']]);
		}
		$this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$retIllegal]);
	}
	/**
	 * @Des: 定时任务-解压获取截图补全存证信息
	 * @Edt: yuhou.wang
	 * @param {String|Array} $objectIds 公证码,支持单条或批量
	 * @return: 
	 * @Date: 2019-10-10 14:31:18
	 */
	public function appendInfo(){
		ini_set('max_execution_time', 0);//设置超时时间
        $objectIds = $this->P['objectIds'] ? $this->P['objectIds'] : I('objectIds');
        if($objectIds){
			$result = A('Open/Evidence','Model')->appendInfo($objectIds);
			$this->ajaxReturn(['code'=>0,'msg'=>'成功','data'=>$result]);
        }else{
            $this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
        }
	}
	/**
	 * @Des: 定时任务-解压获取截图
	 * @Edt: yuhou.wang
	 * @param {String} $url 证据URL
	 * @return: 
	 * @Date: 2019-10-10 14:31:18
	 */
	public function getSnapshot(){
		// 下载解压
		
		// 上传截图
	}
	/**
	 * @Des: 定时任务-从OSS更新证据元信息
	 * @Edt: yuhou.wang
	 * @param {String} $url 证据URL
	 * @return: 
	 * @Date: 2019-10-10 14:31:18
	 */
	public function setInfo(){
		// 获取OSS对象元信息
		
		// 更新证据信息
	}
	/**
	 * @Des: 获取存管函(下载为PDF文件版)
	 * @Edt: yuhou.wang
	 * @Date: 2019-11-01 06:17:14
	 * @param {Int} $rid 存证码 
	 * @return: 
	 */	
	public function getEvidenceLetterPDF(){
		$rid = $this->P['rid'] ? $this->P['rid'] : I('rid');
		if(!empty($rid)){
			$ret = A('Open/Evidence','Model')->getEvidenceLetterPDF($rid);
			$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$ret]);
		}else{
			$this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
		}
	}
	/**
	 * @Des: 获取存管函(输出HTML源文件版)
	 * @Edt: yuhou.wang
	 * @Date: 2019-11-01 06:17:14
	 * @param {Int} $rid 存证码 
	 * @return: 
	 */	
    public function getEvidenceLetterHTML(){
		$rid = $this->P['rid'] ? $this->P['rid'] : I('rid');
		if(!empty($rid)){
			$ret = A('Open/Evidence','Model')->getEvidenceLetterHTML($rid);
			echo $ret;
		}else{
			$this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
		}
    }
	/**
	 * @Des: 获取存证资源链接
	 * @Edt: yuhou.wang
	 * @Date: 2019-11-01 06:51:22
	 * @param {Int} $rid 存证码
	 * @return: 
	 */	
    public function getEvidenceObjextOSS(){
		$rid = $this->P['rid'] ? $this->P['rid'] : I('rid');
        if (!empty($rid)) {
            $ret = A('Open/Evidence','Model')->getEvidenceObjext($rid);
			$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$ret]);
        }else{
			$this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
		}
	}
	/**
	 * @Des: 获取存证资源链接-Local
	 * @Edt: yuhou.wang
	 * @Date: 2019-11-01 06:51:22
	 * @param {Int} $rid 存证码
	 * @return: 
	 */	
    public function getEvidenceObjext(){
		$rid = $this->P['rid'] ? $this->P['rid'] : I('rid');
        if (!empty($rid)) {
            $ret = A('Open/Evidence','Model')->getEvidenceObjext($rid);
			$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$ret]);
        }else{
			$this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
		}
	}

	public function getObjectMeta(){
		$rid = $this->P['rid'] ? $this->P['rid'] : I('rid');
        if (!empty($rid)) {
            $ret = A('Open/Evidence','Model')->getObjectMeta($rid);
			$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$ret]);
        }else{
			$this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
		}
	}
}