<?php
namespace Admin\Controller;
use Think\Controller;
class ReportTaskController extends BaseController {
	//报告列表
	public function index()
	{
		$this->assign('file_up',file_up());//上传文件所需的参数
		$this->display();
	}

	//报告任务列表接口
    public function index_list()
    {
        $p = I('page',1);//分页
        $keyword = trim(I('keyword'));//关键词
        if($keyword != ''){
            $map['f_report_task.title|f_report_task.content'] = ['like','%'.$keyword.'%'];
        }
        $map['f_report_task.deleteuser'] = ['EQ',0];
        $map['f_report_task.status'] = ['NEQ',0];
        $data = M('f_report_task')
            ->field('
                f_report_task.*,
                CASE
		        WHEN f_report_task.douser = 0 THEN
			    "暂无"
		        ELSE
                tperson.fname
		        END as username
            ')
            ->join('tperson on tperson.fid = f_report_task.douser','left')
            ->where($map)
            ->order('f_report_task.ctime desc')
            ->page($p.',20')
            ->select();

        $count = M('f_report_task')
            ->field('
                f_report_task.fid,
            ')
            ->join('tperson on tperson.fid = f_report_task.douser','left')
            ->where($map)
            ->count();
        $this->ajaxReturn([
            'code' => 0,
            'data' => ['count'=>$count,'list'=>$data],
            'msg' => '获取数据成功'
        ]);

    }

    //报告处理接口
    public function report_process()
    {
        $user = session('personInfo');//
        $data['fid'] = I('fid');//主键
        $data['dfileurl'] = I('dfileurl');//处理后报告地址
        $data['description'] = I('description');//处理说明\
        $data['douser'] = $user['fid'];//处理人
        $data['status'] 	= 3;//处理完成状态
        $data['dotime'] = date('Y-m-d H:i:s');//处理时间\

        $res = M('f_report_task')->save($data);
        if($res !== false){
            $this->ajaxReturn([
                'code'=>0,
                'msg'=>'处理成功'
            ]);
        }
    }

    //报告任务删除
    public function delete_task(){

        $data['fid'] = I('fid');
        $user = session('personInfo');//
        $data['deleteuser'] = $user['fid'];//用户id
        $data['deletetime'] = date('Y-m-d H:i:s');//删除时间
        $res = M('f_report_task')->save($data);
        if($res){
            $this->ajaxReturn([
                'code'=>0,
                'msg'=>'删除成功'
            ]);
        }
    }

    /**
     * 获取发送的报告
     * by zw
     */
    public function send_reportlist(){

        $p = I('page',1);//分页
        $pnname = I('pnname');//报告名称
        $pntype = I('pntype');//报告类型
        $pnstarttime = I('pnstarttime');//开始时间
        $pnendtime = I('pnendtime');//结束时间
        $crname  = I('crname');//客户名

        if(!empty($pnname)){
            $where_tn['pnname'] = array('like','%'.$pnname.'%');
        }
        if(!empty($pntype)){
            $where_tn['pntype'] = $pntype;
        }
        if(!empty($pnstarttime) && !empty($pnendtime)){
            $where_tn['pnendtime'] = array('between',$pnstarttime.','.$pnendtime);
        }
        if(!empty($crname)){
            $where_tn['b.cr_name'] = array('like','%'.$crname.'%');
        }

        $where_tn['pnfocus'] = 2;
        $do_tn = M('tpresentation')
            ->field('pnid,pnname,pntype,pnfiletype,pnendtime,pnurl,b.cr_name,c.fname')
            ->alias('a')
            ->join('(select distinct(cr_num) as cr_num,cr_name from customer where cr_status = 1 group by cr_num) b on a.fcustomer = b.cr_num')
            ->join('tregulator c on a.pntrepid = c.fid')
            ->where($where_tn)
            ->order('pncreatetime desc')
            ->page($p.',20')
            ->select();

        $count = M('tpresentation')
            ->alias('a')
            ->join('(select distinct(cr_num) as cr_num,cr_name from customer where cr_status = 1 group by cr_num) b on a.fcustomer = b.cr_num')
            ->join('tregulator c on a.pntrepid = c.fid')
            ->where($where_tn)
            ->count();
        $this->ajaxReturn([
            'code' => 0,
            'data' => ['count'=>$count,'list'=>$do_tn],
            'msg' => '获取成功'
        ]);
    }

    /**
     * 获取客户列表
     * by zw
     */
    public function get_customer(){
        $do_cr = M('customer')
            ->field('cr_name,cr_num')
            ->where('cr_status = 1 and cr_num>="100000"')
            ->select();
        $this->ajaxReturn([
            'code'=>0,
            'msg'=>'获取成功',
            'data'=>$do_cr
        ]);
    }

    /**
     * 通过客户ID获取机构
     * by zw
     */
    public function customer_get_tre(){
        $cr_num = I('cr_num');//客户编号

        $where_tr['fstate']    = 1;
        $where_tr['flevel']    = array('in',array(1,2,3,4,5));
        $where_tr['fpid']      = array('neq',-1);
        $tregion_len = D('Function')->get_tregionlevel($cr_num);
        if($tregion_len == 1){//国家级
            $where_tr['flevel']    = array('in',array(1,2,3));
        }elseif($tregion_len == 2){//省级
            $where_tr['fid'] = array('like',substr($cr_num,0,2).'%');
        }elseif($tregion_len == 4){//市级
            $where_tr['fid'] = array('like',substr($cr_num,0,4).'%');
        }elseif($tregion_len == 6){//县级
            $where_tr['fid'] = array('like',substr($cr_num,0,6).'%');
        }

        $do_tr = M('tregion')
            ->field('fid,fpid,fname1 as fname,ffullname')
            ->cache(true,60)
            ->where($where_tr)
            ->order('fid asc')
            ->select();//查询地区

        $this->ajaxReturn([
            'code'=>0,
            'msg'=>'获取成功',
            'data'=>$do_tr
        ]);
    }

    /**
     * 发送报告
     * by zw
     */
    public function send_report(){
        $pnname = I('pnname');//报告名称
        $pntype = I('pntype');//报告类型
        $pnfiletype = I('pnfiletype');//文件类型
        $pnstarttime = I('pnstarttime');//开始时间
        $pnendtime = I('pnendtime');//结束时间
        $pnurl = I('pnurl');//报告下载URL
        $pntrepid = I('pntrepid');//机构ID
        $fcustomer = I('fcustomer');//客户编号

        if(empty($pnname) || empty($pntype) || empty($pnfiletype) || empty($pnstarttime) || empty($pnurl) || empty($fcustomer) || empty($pntrepid)){
            $this->ajaxReturn([
                'code'=>1,
                'msg'=>'请保证填写数据的完整',
            ]);
        }
        $pntrepid = M('tregulator') ->where('fregionid = '.$pntrepid.' and fstate = 1 and fkind = 1')->getfield('fid');
        if(empty($pntrepid)){
            $this->ajaxReturn([
                'code'=>1,
                'msg'=>'机构不存在',
            ]);
        }

        $data_pn['pnname'] = $pnname;
        $data_pn['pntype'] = $pntype;
        $data_pn['pnfiletype'] = $pnfiletype;
        $data_pn['pnstarttime'] = $pnstarttime;
        $data_pn['pnendtime'] = $pnendtime;
        $data_pn['pncreatetime'] = date('Y-m-d H:i:s');
        $data_pn['pnurl'] = $pnurl;
        $data_pn['pntrepid'] = $pntrepid;
        $data_pn['pnfocus'] = 2;
        $data_pn['fcustomer'] = $fcustomer;
        $do_pn = M('tpresentation')->add($data_pn);
        if(!empty($do_pn)){
            $this->ajaxReturn([
                'code'=>0,
                'msg'=>'添加成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code'=>1,
                'msg'=>'添加失败',
            ]);
        }
    }

    /**
     * 删除报告
     * by zw
     */
    public function del_report(){
        $pnid = I('pnid');//报告ID

        $where_pn['pnid'] = $pnid;
        $do_pn = M('tpresentation')
            ->where($where_pn)
            ->delete();
        if(!empty($do_pn)){
            $this->ajaxReturn([
                'code'=>0,
                'msg'=>'删除成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code'=>1,
                'msg'=>'删除失败',
            ]);
        }
    }

}