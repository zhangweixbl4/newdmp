<?php
namespace TaskInput\Controller;

class IssueConfirmController extends TraditionAdController {
    private $error_msg = '';
    public function _initialize()
    {
        parent::_initialize();
        if(!A('InputPc/Task','Model')->user_other_authority('4001')){
            die('<h1>无权限,请联系管理员</h1>');
        }
    }
    public function index()
    {
        if (IS_POST){
            $formData = I('where');
            $searchType = I('searchType', 'ad');
            // 违法广告发布记录搜索条件处理
            if ($searchType == 'ad'){
                $this->adSearchType($formData);
            }elseif ($searchType == 'media'){
                $this->mediaSearchType($formData);
            }
        }else{
            $illegalType = json_encode(parent::getIllegalTypeArr());
            $adClassArr = json_encode(parent::getAdClassArr());
            $regionTree = json_encode(parent::getRegionTree());
            $illegalArr = json_encode(parent::getIllegalArr());
            $this->assign(compact('illegalType', 'adClassArr', 'regionTree', 'illegalArr'));
            $this->display();
        }
    }

    // 违法广告发布记录按广告展示
    private function adSearchType($formData)
    {
        $page = I('page', 1);
        $issueWhere = $this->processIssueWhere($formData);
        $issueSubQuery = M('tbn_illegal_ad_issue')
            ->field('fillegal_ad_id, count(1) illegal_count')
            ->where($issueWhere)
            ->group('fillegal_ad_id')
            ->buildSql();
        $adWhere = [
            'ad.fstatus' => ['EQ', 0],
            'ad.fid' => ['EXP', " = issue.fillegal_ad_id"]
        ];
        // 违法广告搜搜条件处理
        if ($formData['adName'] != ''){
            $adWhere['ad.fad_name'] = ['LIKE', "%{$formData['adName']}%"];
        }
        if ($formData['adClass'] != ''){
            $adWhere['ad.fad_class_code'] = ['EQ', $formData['adClass']];
        }
        if ($formData['regionId'] != ''){
            if ($formData['onlyThisLevel'] == 1){
                $adWhere['ad.fregion_id'] = ['EQ', $formData['regionId']];
            }else{
                $tempRegionId = rtrim($formData['regionId'], '0');
                $len = strlen($tempRegionId);
                $adWhere["LEFT(ad.fregion_id, $len)"] = ['EQ', $tempRegionId];
            }
        }
        $table = M()
            ->table("tbn_illegal_ad ad, $issueSubQuery issue")
            ->where($adWhere)
            ->order('ad.last_syn_time desc')
            ->page($page, 10)
            ->select();
        $total = M()
            ->table("tbn_illegal_ad ad, $issueSubQuery issue")
            ->where($adWhere)
            ->order('ad.last_syn_time desc')
            ->count();
        foreach ($table as $key => $value) {
            $table[$key]['region'] = M('tregion')->cache(true)->where(['fid' => ['EQ', $value['fregion_id']]])->getField('ffullname');
            $table[$key]['media_name'] = M('tmedia')->cache(true)->where(['fid' => ['EQ', $value['fmedia_id']]])->getField('fmedianame');
            $table[$key]['ad_class'] = M('tadclass')->cache(true)->where(['fcode' => ['EQ', $value['fad_class_code']]])->getField('ffullname');
            $table[$key]['fexpressions'] = array_filter(explode(' ;; ', $table[$key]['fexpressions']));
        }
        $this->ajaxReturn(compact('table', 'total'));
    }

    // 按媒体搜索违法广告发布记录
    private function mediaSearchType($formData)
    {
        $page = I('page', 1);
        $issueWhere = $this->processIssueWhere($formData);
        $issueSubQuery = M('tbn_illegal_ad_issue')
            ->field('fillegal_ad_id, fmedia_id, count(1) illegal_count, fmedia_class')
            ->where($issueWhere)
            ->group('fmedia_id')
            ->buildSql();
        $table = M()
            ->table($issueSubQuery . ' AS res')
            ->page($page, 10)
            ->select();
        foreach ($table as $key => $value) {
            $table[$key]['media_name'] = M('tmedia')->cache(true, 60)->where(['fid' => ['EQ', $value['fmedia_id']]])->getField('fmedianame');
            $adInfo = M('tbn_illegal_ad')->where(['fstatus' => ['EQ', 0]])->find($value['fillegal_ad_id']);
            $table[$key]['region'] = M('tregion')->cache(true)->where(['fid' => ['EQ', $adInfo['fregion_id']]])->getField('ffullname');
        }
        $total = M()
            ->table($issueSubQuery . ' AS res')
            ->count();
        $this->ajaxReturn(compact('table', 'total'));

    }

    /**
     * 处理总局抽查计划的条件
     * @param $date
     */
    public function processZongjuCondition($date)
    {
        // 取出来该月的抽查计划
        $row = M('zongju_data_confirm')->where(['fmonth' => ['EQ', $date]])->find();
        if (!$row){
            return false;
        }
        $dateCondition = json_decode($row['condition'], true);
        if (!$dateCondition){
            return false;
        }
        $mediaClassArr = ['tv' => 1, 'bc' => 2, 'paper' => 3];
        $res = [
            '_logic' => 'OR'
        ];
        // 定义出
        foreach ($dateCondition as $key => $value) {
            if (!$value){
                continue;
            }
            $month = substr($date, 0, 8);
            $dateArr = [];
            foreach ($value as $item) {
                $dateArr[] = $month . $item;
            }
            $res[] = [
                'fmedia_class' => ['EQ', $mediaClassArr[$key]],
                'fissue_date' => ['IN', $dateArr]
            ];
        }
        return $res;
    }
    
    /**
     * 处理违法广告发布记录的搜索条件
     * @param $formData
     * @return array
     */
    private function processIssueWhere($formData)
    {
        // 媒体搜索条件处理
        $mediaWhere = [];
        if ($formData['mediaName'] != ''){
            $mediaWhere['fmedianame'] = ['LIKE', "%{$formData['mediaName']}%"];
        }
        if ($formData['customer'] == '国家局'){
            $mediaLabelSubQrery = M('tmedialabel')
                ->field('fmediaid')
                ->where([
                    'fstate' => ['EQ', 1],
                    'flabelid' => ['IN', '243,244,245'],
                ])
                ->buildSql();
            $mediaWhere['fid'] = ['EXP', ' IN '.$mediaLabelSubQrery];
        }
        if ($mediaWhere){
            $mediaSubQuery = M('tmedia')->field('fid')->where($mediaWhere)->buildSql();
        }
        $issueWhere = [];

        // 如果没有传时间, 默认是本月的月初
        $startTime = date('Y-m-') . '01';
        if ($formData['timeRangeArr'] != ''){
            if (substr($formData['timeRangeArr'][0], 5, 2) !== substr($formData['timeRangeArr'][0], 5, 2)){
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '搜索月份不一致'
                ]);
            }
            $issueWhere['fissue_date'] = ['BETWEEN', $formData['timeRangeArr']];
            $temp = $formData['timeRangeArr'][0];
            $temp = explode('-', $temp);
            array_pop($temp);
            $temp[] = '01';
            $startTime = implode('-', $temp);
        }else{
            $issueWhere['fissue_date'] = ['BETWEEN', date('Y-m-d'), date('Y-m-', strtotime('+1 month') - 1) . '01'];
        }
        if ($formData['customer'] == '国家局'){
            $zonjuConfitionDate = $this->processZongjuCondition($startTime);
            if ($zonjuConfitionDate){
                $issueWhere[] = $zonjuConfitionDate;
            }else{
                $issueWhere[] = ['false'];
            }

        }

        if ($formData['mediaClass'] != ''){
            $issueWhere['fmedia_class'] = ['EQ', $formData['mediaClass']];
        }
//        if ($formData['issueState'] != ''){
            $issueWhere['fsend_status'] = ['EQ', 0];
//        }
        if ($formData['regionId'] != ''){
            if ($formData['onlyThisLevel'] == 1){
                $adSubQuery = M('tbn_illegal_ad')->field('fid')->where(['fregion_id' => ['EQ', $formData['regionId']]])->buildSql();
                $issueWhere['fillegal_ad_id'] = ['EXP','IN ' . $adSubQuery];
            }else{
                $tempRegionId = rtrim($formData['regionId'], '0');
                $len = strlen($tempRegionId);
                $adSubQuery = M('tbn_illegal_ad')->field('fid')->where(["LEFT(fregion_id, $len)" => ['EQ', $tempRegionId]])->buildSql();
                $issueWhere['fillegal_ad_id'] = ['EXP','IN ' . $adSubQuery];
            }
        }

        if ($mediaWhere){
            $issueWhere['fmedia_id'] = ['EXP', " IN $mediaSubQuery"];
        }
        return $issueWhere;
    }

    // 获取发布记录和样本信息
    public function illegalIssueList()
    {
        $formData = I('where');
        $row = I('row');
        $issueWhere = $this->processIssueWhere($formData);
        $issueWhere['fillegal_ad_id'] = ['EQ', $row['fid']];
        $sampleTableArr = [
            '1' => 'ttvsample',
            '2' => 'tbcsample',
            '3' => 'tpapersample'
        ];
        $sampleTable = $sampleTableArr[$row['fmedia_class']];

        // 找到样本
        $data = M($sampleTable)->find($row['fsample_id']);
        if ($row['fmedia_class'] == 3){
            $data['paperUrl'] =  A('Common/Paper','Model')->get_slim_source($data['fpapersampleid']);
        }
        $adInfo = M('tad')->field('fbrand, fadowner')->find($data['fadid']);
        $data['fadowner'] = M('tadowner')->where(['fid' => ['EQ', $adInfo['fadowner']]])->getField('fname');
        $data['fbrand'] = $adInfo['fbrand'];
        $data['fconfirmations'] = array_filter(explode(' ;; ', $data['fconfirmations']));
        // 找到违法广告发布记录
        $data['issueList'] = M('tbn_illegal_ad_issue')
            ->field('fid, fissue_date issue_date, fstarttime start_time, fendtime end_time, fmedia_id media_id')
            ->where($issueWhere)
            ->order('start_time asc')
            ->select();
        foreach ($data['issueList'] as $key => $value) {
            $data['issueList'][$key]['media_name'] = M('tmedia')->cache(true)->where(['fid' => ['EQ', $value['media_id']]])->getField('fmedianame');
        }
        $adTypeArr = [
            '1' => 'tv',
            '2' => 'bc',
            '3' => 'paper'
        ];
        $data['adType'] = $adTypeArr[$row['fmedia_class']];
        $this->ajaxReturn([
            'code' => 0,
            'data' => $data
        ]);


    }

    /**
     * 检查该月的总局抽查计划是否确认, 允许变更返回true, 否则返回false
     * @param $ids
     * @return bool
     */
    private function checkThisMonthIsConfirm($ids)
    {
        if (!is_array($ids)){
            $ids = explode(',', $ids);
        }
        if (!$ids){
            $this->error_msg = '没有选中任何发布记录';
            return false;
        }elseif (count($ids) >= 2){ // 确认超过2项
            // 前面会尽量保证ids是按发布日期排序的, 所以检查第一个和最后一个是否同月
            $tempIds = [];
            $tempIds[] = array_shift($ids);
            $tempIds[] = array_pop($ids);
            $tempMonthArr = [];
            foreach ($tempIds as $tempId) {
                $issueDate = M('tbn_illegal_ad_issue')->where(['fid' => ['EQ', $tempId]])->getField('fissue_date');
                $tempMonthArr[] = substr($issueDate, 0, 7);
            }
            if ($tempMonthArr[0] != $tempMonthArr[1]){
                $this->error_msg = '搜索条件的起止年月必须相同';
                return false;
            }else{
                $res = M('zongju_data_confirm')->where(['fmonth' => ['EQ', $tempMonthArr[0] . '-01']])->getField('dmp_confirm_state');
                if ($res == 0){
                    return true;
                }elseif ($res == 1){
                    $this->error_msg = '该月违法广告数据已经提交至国家局, 不允许再更改';
                    return false;
                }else{
                    $this->error_msg = '抽查计划状态未知, 请联系管理员';
                    return false;
                }
            }

        }elseif(count($ids) == 1){ // 只确认一项
            $issueDate = substr(M('tbn_illegal_ad_issue')->where(['fid' => ['EQ', $ids[0]]])->getField('fissue_date'), 0, 7);
            $res = M('zongju_data_confirm')->where(['fmonth' => ['EQ', $issueDate . '-01']])->getField('dmp_confirm_state');
            if ($res == 0){
                return true;
            }elseif ($res == 1){
                $this->error_msg = '违法广告数据已经提交至国家局, 不允许再更改';
                return false;
            }else{
                $this->error_msg = '抽查计划状态未知, 请联系管理员';
                return false;
            }
        }


    }

    /**
     * 确认违法发布记录
     * @param $ids  string  字符串形式 逗号连接
     * @param int $res  1 是dmp确认 2 是退回
     */
    public function confirmIssue($ids, $res)
    {
        if (!$this->checkThisMonthIsConfirm($ids)){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => $this->error_msg,
            ]);
        }
        $ids = explode(',', $ids);
        $wx_id = session('wx_id');
        $userName = M('ad_input_user')->where(['wx_id' => ['EQ', $wx_id]])->getField('alias');
        $model = M('tbn_illegal_ad_issue');
        $hasErr = false;
        // res == -2 是因为广告样本错误
        if ($res == -2){
            $where = [
                'fsend_status' => ['EQ', 0],
            ];
        }else{
            $where = [
                'fsend_status' => ['EQ', 0],
            ];
        }
        foreach ($ids as $id) {
            $where['fid'] = ['EQ', $id];
            $row = $model->where($where)->find();
            if ($row){
                if ($res == 1){
                    $log =$row['fsend_log'] . '【众包任务】 '.$userName.'(ID:'.$wx_id.') 于'.date('Y-m-d H:i:s').'众包任务系统确认；';
                }elseif($res == -1){
                    $log =$row['fsend_log'] . '【众包任务】 '.$userName.'(ID:'.$wx_id.') 于'.date('Y-m-d H:i:s').'众包任务系统退回；';
                }elseif ($res == -2){
                    $log =$row['fsend_log'] . '【众包任务】 '.$userName.'(ID:'.$wx_id.') 于'.date('Y-m-d H:i:s').'广告样本有误,众包任务系统退回；';
                }
                $updateRes = $model
                    ->where($where)
                    ->save([
                    'fsend_status' => $res,
                    'fsend_log' => $log,
                ]) !== false ? true : false;
                if (!$updateRes){
                    $hasErr = false;
                }
            }
        }
        if (!$hasErr){
            $this->ajaxReturn([
                'code'=> 0,
                'msg' => '更新成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '有部分数据更新不成功'
            ]);
        }
    }

    /**
     * 批量确认违法广告发布记录
     * @param $ids
     * @param $where
     * @param $type
     */
    public function batchConfirm($ids, $type, $where)
    {
        $pk = $type === 'ad' ? 'fillegal_ad_id' : 'fmedia_id';
        $where = $this->processIssueWhere($where);
        $where[$pk] = ['IN', $ids];
        $ids = M('tbn_illegal_ad_issue')->where($where)->getField('fid', true);
        $ids = implode(',', $ids);
        $this->confirmIssue($ids, 9);
    }

    /**
     * 广告信息录入错误
     * @param $adId
     */
    public function submitAdError($adId)
    {

        // 发布记录id
        $issueIds = M('tbn_illegal_ad_issue')->where(['fillegal_ad_id' => ['EQ', $adId]])->getField('fid', true);
        $res = M('tbn_illegal_ad')
            ->where([
                'fid' => ['EQ', $adId],
                'fstatus' => ['EQ', 0]
            ])
            ->save([
                'fstatus' => -1
            ]);
        $this->confirmIssue($issueIds, -2);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
    }


}