<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 问题反馈
 * by zw
 */
class GfankuiController extends BaseController {
  
  /**
 * 反馈列表
 * by zw
 */
  public function fankuilist(){
    session_write_close();

    $p  = I('page', 1);//当前第几页
    $pp = 20;//每页显示多少记录
    $ftitle     = I('ftitle');// 标题
    $starttime  = I('starttime');//反馈时间起
    $endtime    = I('endtime');//反馈时间止
    $status     = I('status');//状态

    if (!empty($ftitle)) {
      $where_tia['ftitle'] = array('like', '%' . $ftitle . '%');//标题
    }

    if(!empty($starttime)&&!empty($endtime)){
      $where_tia['a.fcreatetime'] = array('between',array($starttime,$endtime));
    }

    if(!empty($status)){
      $where_tia['a.fstatus']     = $status;
    }
    $where_tia['a.fcreateuserid'] = session('regulatorpersonInfo.fid');

    $count = M('tbn_question')
      ->alias('a')
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    
    $do_tia = M('tbn_question')
      ->alias('a')
      ->field('a.fid,a.ftitle,a.fcontent,a.ffiles,a.ffilename,a.fcreateusername,a.fcreatetime,a.frecontent,a.frefiles,a.frefilename,a.freusername,a.fretime,(case when fstatus=0 then "未处理" when fstatus=10 then "已处理" else "已回复" end ) as fstatus')
      ->where($where_tia)
      ->order('a.fid desc')
      ->page($p,$pp)
      ->select();
  
    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));

  }

  /**
 * 添加反馈
 * by zw
 */
  public function fankuiadd(){
    $ftitle   = I('ftitle');//标题
    $fcontent = I('fcontent');//反馈内容
    $ffiles   = I('ffiles');//反馈文件

    if(empty($ftitle)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请填写标题'));
    }
    if(empty($fcontent) && empty($ffiles)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请提交需要反馈的问题'));
    }

    $data['ftitle']           = $ftitle;
    $data['fcontent']         = $fcontent;
    $data['ffiles']           = $ffiles[0]['url'];
    $data['ffilename']        = $ffiles[0]['name'];
    $data['fcreatetrepid']    = session('regulatorpersonInfo.fregulatorpid');
    $data['fcreatetreid']     = session('regulatorpersonInfo.fregulatorid');
    $data['fcreateuserid']    = session('regulatorpersonInfo.fid');
    $data['fcreatetrepname']  = session('regulatorpersonInfo.regulatorpname');
    $data['fcreatetrename']   = session('regulatorpersonInfo.regulatorname');
    $data['fcreateusername']  = session('regulatorpersonInfo.fname');
    $data['fcreatetime']      = date('Y-m-d H:i:s');
    $data['fstatus']          = 0;
    $do = M('tbn_question')->add($data);
    if($do){
      D('Function')->write_log('问题反馈',1,'反馈成功','tbn_question',$do,M('tbn_question')->getlastsql());
      $this->ajaxReturn(array('code'=>0,'msg'=>'反馈成功'));
    }else{
      D('Function')->write_log('问题反馈',0,'反馈失败','tbn_question',0,M('tbn_question')->getlastsql());
      $this->ajaxReturn(array('code'=>1,'msg'=>'反馈失败'));
    }
  }

  /**
 * 删除反馈
 * by zw
 */
  public function fankuidel(){
    $fid   = I('fid');//问题id

    $where['fid']           = $fid;
    $where['fcreateuserid'] = session('regulatorpersonInfo.fid');

    $do = M('tbn_question')->where($where)->delete();
    if($do){
      D('Function')->write_log('问题反馈',1,'删除成功','tbn_question',$fid,M('tbn_question')->getlastsql());
      $this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
    }else{
      D('Function')->write_log('问题反馈',0,'删除失败','tbn_question',$fid,M('tbn_question')->getlastsql());
      $this->ajaxReturn(array('code'=>1,'msg'=>'删除失败'));
    }
  }

  /**
 * 查看反馈
 * by zw
 */
  public function fankuiview(){
    $fid   = I('fid');//问题id

    $where['fid']           = $fid;
    $where['fcreateuserid'] = session('regulatorpersonInfo.fid');

    $do = M('tbn_question')->field('ftitle,fcontent,ffiles,ffilename,fcreateusername,fcreatetime,frecontent,frefiles,frefilename,freusername,fretime,(case when fstatus=0 then "未处理" when fstatus=10 then "已处理" else "已回复" end ) as fstatus')->where($where)->find();
    if($do){
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do));
    }else{
      $this->ajaxReturn(array('code'=>1,'msg'=>'信息不存在'));
    }
  }

}