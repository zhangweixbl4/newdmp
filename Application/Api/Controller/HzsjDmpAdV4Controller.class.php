<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;

/*

TRUNCATE table tbn_ad_summary_day_v4;
TRUNCATE table hzsj_dmp_ad_v4;
update extract_other_region_data set fstate = 0;
select * from tbn_illegal_ad_issue where fillegal_ad_id in (select fid from tbn_illegal_ad where identify = 'new_2');
select * from tbn_illegal_ad where identify = 'new_2';

*/



class HzsjDmpAdV4Controller extends Controller {
	
	
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
	}
	
	public function add_extract(){
		$forgid = I('forgid');
		$sd = I('sd');
		$ed = I('ed');
		$ssd = strtotime($sd);
		$sed = strtotime($ed);
		if(!$forgid) exit('forgid Error');
		for($sdate=$ssd;$sdate<=$sed;$sdate+=86400){
			$fdate = date('Y-m-d',$sdate);
			$addData = array();
			$addData['forgid'] = $forgid;
			$addData['fdate'] = $fdate;
			$addData['fstate'] = 0;
			$addData['fcreatetime'] = date('Y-m-d H:i:s');
			M('extract_other_region_data')->add($addData,[],true);
		}
		
		echo 'SUCCESS';
		
	}
	
	public function mt_add_extract(){
		$forgid = 62100000;
		$lately_day = intval(I('lately_day'));
		if($lately_day < 3 ) $lately_day = 3;
		if($lately_day > 31) $lately_day = 31;
		
		$sd = date('Y-m-d',time()-86400 * $lately_day);
		$ed = date('Y-m-d');
		$ssd = strtotime($sd);
		$sed = strtotime($ed);
		if(!$forgid) exit('forgid Error');
		$ad_sql = "select tad.fadid
					from tad 
					where (
							fadname like '%普天同庆%'
					or fadname like '%小豹子%'
					or fadname like '%国典%'
					or fadname like '%汉秘%'
					or fadname like '%茅台醇%'
					or fadname like '%贵州牌%'
					or fadname like '%祝尊富贵%'
					or fadname like '%富贵万%'
					or fadname like '%茅仙%'
					or fadname like '%老门牌%'
					or fadname like '%天朝上品%'
					or fadname like '%华福名酒%'
					or fadname like '%赤水老窖%'
					or fadname like '%酱门经典%'
					or fadname like '%茅乡%'
					or fadname like '%台源%'
					or fadname like '%播窖%'
					or fadname like '%华盛名酒%'
					or fadname like '%古源%'
					or fadname like '%百年故事%'
					or fadname like '%茅源%'
					or fadname like '%茅坛%'
					or fadname like '%茅台不老%'
					or fadname like '%原酒%'
					or fadname like '%白金酒%'
					or fadname like '%传奇天露%'
					or fadname like '%龙头马%'
					or fadname like '%元贵牌%'
					or fadname like '%匠中匠%'
					or fadname like '%黔品大匠%'
					or fadname like '%宝路喜%'
					or fadname like '%黔茅%'
					or fadname like '%茅鹿源%'
					or fadname like '%正和%'
					or fadname like '%茅韵%'
					or fadname like '%怀匠%'
					or fadname like '%厚礼%'
					or fadname like '%悠蜜%'
					or fadname like '%蓝神%'
					or fadname like '%桑圣王%'
					or fadname like '%蓝媚坊%'
					or fadname like '%丽彩华章%'
					or fadname like '%云上果园%'
					or fadname like '%U味%'
					or fadname like '%粲酒%'
					or fadname like '%馨雅%'
					or fadname like '%茅台%'
					or fadowner in (select fid from tadowner where fname like '%茅台%')
					)
					and fadname not like '%收购%'
					";
		
		for($sdate=$ssd;$sdate<=$sed;$sdate+=86400){
			$fdate = date('Y-m-d',$sdate);
			$addData = array();
			$addData['forgid'] = $forgid;
			$addData['fdate'] = $fdate;
			$addData['fstate'] = 0;
			$addData['fcreatetime'] = date('Y-m-d H:i:s');
			$addData['ad_sql'] = $ad_sql;
			$addData['need_net'] = 0;
			$addData['tissue_customer'] = 1;
			M('extract_other_region_data')->add($addData,[],true);
			echo $forgid.','.$fdate . "\n";
		}
		
		echo 'SUCCESS';
		
	}
	
	public function zxb_add_extract(){
		$forgid = 63100000;
		$lately_day = intval(I('lately_day'));
		if($lately_day < 3 ) $lately_day = 3;
		if($lately_day > 31) $lately_day = 31;
		
		$sd = date('Y-m-d',time()-86400 * $lately_day);
		$ed = date('Y-m-d');
		$ssd = strtotime($sd);
		$sed = strtotime($ed);
		if(!$forgid) exit('forgid Error');
		$ad_sql = "select fadid from tad where fadname like '%公益%' or left(fadclasscode,4) = '2202'";
		
		for($sdate=$ssd;$sdate<=$sed;$sdate+=86400){
			$fdate = date('Y-m-d',$sdate);
			$addData = array();
			$addData['forgid'] = $forgid;
			$addData['fdate'] = $fdate;
			$addData['fstate'] = 0;
			$addData['fcreatetime'] = date('Y-m-d H:i:s');
			$addData['ad_sql'] = $ad_sql;
			$addData['need_net'] = 1;
			$addData['tissue_customer'] = 0;
			
			M('extract_other_region_data')->add($addData,[],true);
			echo $forgid.','.$fdate . "\n";
		}
		
		echo 'SUCCESS';
		
	}
	
	
	


	/*抽取数据*/
	public function extract_other_region_data(){
		/* $sss = '{"fmediaid":"11000010003734","fmediaownerid":"1001229","fdate":"2019-03-06","fmedia_class_code":1,"fregionid":"130400","fad_class_code":"01","fcustomer":"130000","forgid":"20130000","fad_times":3,"fad_play_len":44,"fsam_list":"1287581","fsam_list_long_ad":null}';
		
		$addData = json_decode($sss,true);
		$rr = M('tbn_ad_summary_day_v4')->add($addData);
		
		var_dump($rr);
		exit; */

		session_write_close();
		set_time_limit(3600);
		ini_set('memory_limit',-1);
		
		
		M('extract_other_region_data')->where(array('fstate'=>1,'ffinishtime'=>array('lt',date('Y-m-d H:i:s',time()-1200))))->save(array('fstate'=>0));
		
		#var_dump(M('extract_other_region_data')->getLastSql());
		$extract_other_region_list = M('extract_other_region_data')->where(array('fstate'=>0))->limit(1)->select();
		#exit;
		
		#$extract_other_region_list = array(array('forgid'=>20130000,'fdate'=>'2019-07-06'));
		
		foreach($extract_other_region_list as $extract_other_region){

			
			$editStateRet = M('extract_other_region_data')->where(array('fstate'=>0,'fid'=>$extract_other_region['fid']))->save(array('fstate'=>1,'ffinishtime'=>date('Y-m-d H:i:s')));
			if(!$editStateRet) continue; #如果修改状态失败
			
			$openInputNum = 0;
			$mysqlInputNum = 0;
			$gInputNum = 0;
			
			
			$delNumArr = A('Api/HzsjDmpAdV4','Model')->delete_issue_data($extract_other_region['forgid'],$extract_other_region['fdate']); #清除这一天的数据
			
			
			if(strval($extract_other_region['ad_sql']) == ''){
				$HzsjDmpAdV4Model = A('Api/HzsjDmpAdV4','Model');
			}else{
				$HzsjDmpAdV4Model = A('Api/HzsjDmpAdV4KeyWord','Model');
			}
			
			if($extract_other_region['need_net']){
				$InputNum = $HzsjDmpAdV4Model->handle_13($extract_other_region['forgid'],$extract_other_region['fdate'],$extract_other_region['tissue_customer'],strval($extract_other_region['ad_sql'])); #抽取网络广告明细
				$mysqlInputNum += $InputNum['mysqlInputNum'];
				$openInputNum += $InputNum['openInputNum'];
			}
			
			$InputNum = $HzsjDmpAdV4Model->handle_03($extract_other_region['forgid'],$extract_other_region['fdate'],$extract_other_region['tissue_customer'],strval($extract_other_region['ad_sql']));#抽取报纸广告明细
			$mysqlInputNum += $InputNum['mysqlInputNum'];
			$openInputNum += $InputNum['openInputNum'];
			
			$InputNum = $HzsjDmpAdV4Model->handle_01($extract_other_region['forgid'],$extract_other_region['fdate'],$extract_other_region['tissue_customer'],strval($extract_other_region['ad_sql']));#抽取电视、广播明细
			$mysqlInputNum += $InputNum['mysqlInputNum'];
			$openInputNum += $InputNum['openInputNum'];
			
			
			$saveData = array();
			$remark = 'MySQL明细:'.$mysqlInputNum.'  搜索引擎明细:'.$openInputNum.' 汇总数:'.$gInputNum.' 删除MySQL:'.$delNumArr['sqlDelNum'].' 删除搜索引擎:'.$delNumArr['openDelNum'].' 删除违法记录:'.$delNumArr['illegalIssueDelNum'].' 删除汇总记录:'.$delNumArr['sumDelNum'];
			if( $openInputNum == $mysqlInputNum){
				$saveData = array('fstate'=>2,'ffinishtime'=>date('Y-m-d H:i:s'),'remark'=>'抽取完成SUCCESS '.$remark);
			
			}else{
				$saveData = array('fstate'=>0,'ffinishtime'=>date('Y-m-d H:i:s'),'remark'=>'抽取出错ERROR '.$remark);
			}	
				
			M('extract_other_region_data')->where(array('fid'=>$extract_other_region['fid']))->save($saveData);
			

		}
		
		
		echo 'SUCCESS';
		
		
		
		
	}
	
	
	public function tbn_ad_summary_day_v4(){
		session_write_close();
		set_time_limit(3600);
		ini_set('memory_limit',-1);
		$extract_other_region = M('extract_other_region_data')->where(array('fstate'=>2,'gathered'=>0))->find();
		
		$dataList = M('hzsj_dmp_ad_v4')->where(array('forgid'=>$extract_other_region['forgid'],'fissuedate'=>strtotime($extract_other_region['fdate'])))->select();
		#var_dump(M('hzsj_dmp_ad_v4')->getLastSql());
		$fad_times_long_ad = A('Api/HzsjDmpAdV4','Model')->tbn_ad_summary_day_v4($dataList);
		var_dump($extract_other_region['forgid'],$extract_other_region['fdate'],$fad_times_long_ad);
		$saveData = array('gathered'=>1);
		M('extract_other_region_data')->where(array('fid'=>$extract_other_region['fid']))->save($saveData);
	}
	
	
	
		

	
	
	
	
	
	
	
	
}