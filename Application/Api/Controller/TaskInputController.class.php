<?php
namespace Api\Controller;
use Think\Controller;
use Think\Exception;


class TaskInputController extends Controller {
    
    /**
    * 接口接收的JSON参数并解码为对象
    */
    protected $oParam;

    public function _initialize() {
        header("Content-type: text/html; charset=utf-8");
        //exit('暂停使用');
        if(I('auto') == '1'){
            echo '<meta http-equiv="refresh"content="0.1"/>';
        }
        
    }
	
	#同步汇总数据到分析型数据库
	public function syn_score(){
		
		$dataList = M('task_input_score')->where(array('syn_flag'=>0))->limit(1000)->select();
		$wait_write_list = [];
		$fids = [];
		foreach($dataList as $data){
			unset($data['syn_flag']);
			$wait_write_list[] = $data;
			$fids[] = $data['id'];
			
		}
		#var_dump(C('FENXI_DB'));
		$writeState = M('task_input_score','',C('FENXI_DB'))->addAll($wait_write_list,[],true);//写入分析型数据库汇总数据
		if($writeState){
			M('task_input_score')->where(array('id'=>array('in',$fids)))->save(array('syn_flag'=>1));
		}
		var_dump($writeState);
		
		#var_dump($fids);
		#var_dump($wait_write_list);
		
	}
    
    
    public function back_overtime_task(){
        $overtime = sys_c('task_overtime');
        $taskList = M('task_input')->where(array('lasttime'=>array('lt',time() - $overtime),'task_state'=>1))->getField('taskid',true);//查询超时未提交的任务
        foreach($taskList as $taskid){//循环任务
            M('task_input')->where(array('task_state'=>1,'taskid'=>$taskid))->save(array('wx_id'=>0,'task_state'=>0,'lasttime'=>time()));//修改任务状态为初始
            M('task_input_link')->where(['taskid' => ['EQ', $taskid], 'state' => ['EQ', 1], 'media_class' => ['NEQ', 13]])->save(['wx_id' => 0, 'state' => 0, 'update_time' => time()]);
            A('TaskInput/Data','Model')->add_flow($taskid,'任务长时间未提交,被系统收回');//写入流程
        }
        M('task_input_link')
            ->where([
                'update_time' => ['LT', time() - $overtime],
                'state' => ['EQ', 1],
            ])
            ->save([
                'state' => 0,
                'wx_id' => 0,
                'update_time' => time(),
            ]);
        //互联网广告超时任务回收
        $this->back_overtime_task_net();
        //互联网数据检查任务超时回收
        $this->back_overtime_task_net_check();
    }
    
    /**
     * 互联网广告任务超时处理
     */
    public function back_overtime_task_net(){
        $overtime = sys_c('task_overtime');
        $taskList = M('tnettask')
            ->field('taskid,task_state')
            ->where([
                'modifytime'=>['lt',time() - $overtime],
                'RIGHT(task_state,1)'=>1
                ])
            ->select();//查询超时未提交的任务
        foreach($taskList as $taskInfo){//循环任务
            $taskState = str_split($taskInfo['task_state']); //任务状态码，第一位表示环节，第二位表示状态
            $preLink = $taskState[0] - 1;//重置为上一环节或初始0状态
            if($preLink > 0){
                $preTaskState = (int)($preLink.'2');
                // 流程日志中查询上一状态负责人
                $wx_id = M('tnettask_flowlog')
                    ->where([
                        'taskid' => $taskInfo['taskid'],
                        'logtype' => $preTaskState
                        ])
                    ->order('createtime DESC')
                    ->getField('wx_id');
            }else{
                $preTaskState = 0;
                $wx_id = 0;
            }
            // 修改任务状态为初始或前一环节完成状态
            $task = M('tnettask')
                ->where([
                    'RIGHT(task_state,1)'=>1,
                    'taskid'=>$taskInfo['taskid']
                    ])
                ->save([
                    'wx_id'      => $wx_id,
                    'task_state' => $preTaskState,
                    'modifier'   => '超时重置', 
                    'modifytime' => time()
                    ]);
            // 写入流程日志
            $linkType = $taskState[0];
            $taskLogType = $linkType.'8';//退回环节状态
            $flowData = [
                'taskid' => $taskInfo['taskid'],
                'tasklink' => $linkType,
                'wx_id' => $wx_id,//任务环节最后负责人
                'logtype' => $taskLogType,
                'log' => A('TaskInput/NetAd','Model')->getLogType($taskLogType),
                'createtime' => time(),
                'creator' => $wx_id
            ];
            $flowUpdate = M('tnettask_flowlog')
                ->add($flowData);
            // 写入流程
            // $taskflow = A('TaskInput/Data','Model')->add_netad_flow($taskInfo['taskid'],'任务长时间未提交,被系统收回');
        }
    }

    /**
     * 户外广告任务超时、退回任务超时处理
     */
    public function back_overtime_task_od(){
        //任务处理超时回收，超时时间20分钟
        $taskList = M('todtask')
            ->field('taskid,task_state,wx_id')
            ->where([
                'modifytime'=>['lt',time() - 1200],
                'RIGHT(task_state,1)'=>1
                ])
            ->select();//查询超时未提交的任务
        foreach($taskList as $taskInfo){//循环任务
            $taskState = str_split($taskInfo['task_state']); //任务状态码，第一位表示环节，第二位表示状态
            if($taskState[0] == 1){
                $task_state = 0;
            }elseif($taskState[0] == 2){
                $task_state = 20;
            }
            // 修改任务状态为初始或前一环节完成状态
            $task = M('todtask')
                ->where(['taskid'=>$taskInfo['taskid']])
                ->save([
                    'wx_id'      => 0,
                    'task_state' => $task_state,
                    'modifier'   => '超时重置', 
                    'modifytime' => time()
                    ]);
            // 写入流程日志
            $linkType = $taskState[0];
            $taskLogType = $linkType.'8';//退回环节状态
            $flowData = [
                'taskid' => $taskInfo['taskid'],
                'tasklink' => $linkType,
                'wx_id' => $taskInfo['wx_id'],//任务环节最后负责人
                'logtype' => $taskLogType,
                'log' => A('TaskInput/OutAd','Model')->getLogType($taskLogType),
                'createtime' => time(),
                'creator' => $wx_id
            ];
            $flowUpdate = M('todtask_flowlog') ->add($flowData);// 写入流程
        }

        //户外广告退回后处理超时回收，超时时间为3天
        $taskList = M('todtask')
            ->field('taskid,task_state,wx_id')
            ->where([
                'modifytime'=>['lt',time() - 259200],
                'RIGHT(task_state,1)'=>3
                ])
            ->select();//查询超时未提交的任务
        foreach($taskList as $taskInfo){//循环任务
            $taskState = str_split($taskInfo['task_state']); //任务状态码，第一位表示环节，第二位表示状态
            if($taskState[0] == 1){
                $task_state = 0;
            }elseif($taskState[0] == 2){
                $task_state = 20;
            }
            // 修改任务状态为初始或前一环节完成状态
            $task = M('todtask')
                ->where(['taskid'=>$taskInfo['taskid']])
                ->save([
                    'wx_id'      => 0,
                    'task_state' => $task_state,
                    'modifier'   => '退回处理超时重置', 
                    'modifytime' => time()
                    ]);
            // 写入流程日志
            $linkType = $taskState[0];
            $taskLogType = $linkType.'7';//超时退回环节状态
            $flowData = [
                'taskid' => $taskInfo['taskid'],
                'tasklink' => $linkType,
                'wx_id' => $taskInfo['wx_id'],//任务环节最后负责人
                'logtype' => $taskLogType,
                'log' => A('TaskInput/OutAd','Model')->getLogType($taskLogType),
                'createtime' => time(),
                'creator' => $wx_id
            ];
            $flowUpdate = M('todtask_flowlog') ->add($flowData);// 写入流程
        }
    }

    /**
     * 互联网数据检查任务超时回收
     */
    public function back_overtime_task_net_check(){
        // 已分配但超时未处理任务
        $overtime = time()-sys_c('task_overtime')-2;//因定时任务执行频率60秒次，故最长可能有60秒误差，此处提前2秒回收
        $where = [
            'fstatus' => 5,
            'modifytime' => ['ELT',$overtime],
        ];
        $list = M('tnetissue_customer')
            ->field('id,title,brand,fstatus,modifytime,ftaskid,fnettaskid,wx_id,priority')
            ->where($where)
            ->select();
        if(!empty($list)){
            foreach($list as $key => $row){
                // 更新重置任务状态为初始
                $saveData = [
                    'wx_id' => 0,
                    'fstatus' => 2,
                    'modifytime' => time(),
                ];
                $saveRes = M('tnetissue_customer')
                    ->where(['id'=>$row['id']])
                    ->save($saveData);
                // 更新流程
                $flowTableName = 'tnetissue_customer_flowlog';
                $flowData = [
                    'taskid'     => $row['id'],
                    'wx_id'      => $row['wx_id'],
                    'logtype'    => 4,
                    'log'        => '超时重置任务',
                    'taskinfo'   => json_encode($row),
                    'creator'    => 'API',
                    'createtime' => time(),
                ];
                $flowRes = M($flowTableName)->add($flowData);
            }
        }
    }

    /**
     * 互联网广告任务超时处理
     */
    public function back_overtime_task_net_V20190301(){
        $taskList = M('tnettask')->where(array('modifytime'=>array('lt',time() - 1200),'task_state'=>1))->getField('taskid',true);//查询超时未提交的任务
        foreach($taskList as $taskid){//循环任务
            $task = M('tnettask')->where(array('task_state'=>1,'taskid'=>$taskid))->save(array('wx_id'=>0,'task_state'=>0,'modifier'=>'超时重置', 'modifytime'=>time()));//修改任务状态为初始
            M('task_input_link')->where(['taskid' => ['EQ', $taskid], 'state' => ['EQ', 1], 'media_class' => ['EQ', 13]])->save(['wx_id' => 0, 'state' => 0, 'update_time' => time()]);

            $taskflow = A('TaskInput/Data','Model')->add_netad_flow($taskid,'任务长时间未提交,被系统收回');//写入流程
        }
    }

    /**
     * 报纸版面剪辑任务超时处理
     */
    public function back_overtime_paperCutTask(){
        // 暂时设置, 普通在做任务过期时间20分钟, 被退回任务过期时间3天
        $state1TimeWhere = time() - 1200;
        $state6TimeWhere = time() - 60*60*24*3;
        $where = "(operation_time < $state1TimeWhere AND fstate = 1) OR (operation_time < $state6TimeWhere AND fstate = 6)";
        $res = M('tpapersource')
            ->where($where)
            ->save([
                'fstate' => 0,
                'fuserid' => 0
            ]);
        echo $res;
    }

    /**
     * @param $id
     */
    public function getErrorCountByQualityTaskId($id)
    {
        $qualityTask = M('task_input_quality')->find($id);
        // 已经抽查的列表
        $alreadyList = explode(',', $qualityTask['already_quality_list']);
        // 正确的列表
        $rightList = explode(',', $qualityTask['quality_right_list']);
        // 错误的列表
        $errorArr = [];
        foreach ($alreadyList as $item) {
            if (!in_array($item, $rightList)){
                $errorArr[] = $item;
            }
        }
        if ($errorArr == []){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '该质检任务暂时没有错误的录入'
            ]);
        }
        $user = M('task_input')
            ->field("count(*) count, nickname")
            ->join('ad_input_user on ad_input_user.wx_id = task_input.wx_id')
            ->where(['taskid' => ['IN', $errorArr]])
            ->group('task_input.wx_id')
            ->order('count desc')
            ->find();
        $class = M('task_input')
            ->field("count(*) count, fadclasscode")
            ->join('tad on tad.fadid = task_input.fadid')
            ->where(['taskid' => ['IN', $errorArr]])
            ->group('fadclasscode')
            ->order('count desc')
            ->find();
        $class['ffullname'] = M('tadclass')->where(['fcode' => ['EQ', $class['fadclasscode']]])->getField('ffullname');
        $media = M('task_input')
            ->field("count(*) count, fmedianame")
            ->join('tmedia on tmedia.fid = task_input.media_id')
            ->where(['taskid' => ['IN', $errorArr]])
            ->group('media_id')
            ->order('count desc')
            ->find();
        $data = compact('user', 'class', 'media');
        $this->ajaxReturn([
            'code' => 0,
            'msg' => '数据获取成功',
            'data' => $data,
        ]);
    }

    /**
     * 搜索同名任务
     * @param $adName
     * @param $mediaClass
     * @param $taskId
     * @param int $adLen
     */
    public function searchSameAdNameTask($adName, $mediaClass, $taskId, $adLen=0)
    {
        // 获取广告id
        $adId = M('tad')->where(['fadname' => ['EQ', $adName]])->getField('fadid');
        if ($adId){
            if ($mediaClass == 3){
                $res = $this->searchSameAdNameTaskPaper($adId, $taskId);
            }else{
                $res = $this->searchSameAdNameTaskTvBc($adId, $mediaClass, $taskId, $adLen);
            }
            if ($res){
                $this->ajaxReturn([
                    'code' => 0,
                    'data' => $res
                ]);
            }else{
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => '未找到相关任务'
                ]);
            }
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '未找到同名广告'
            ]);
        }
    }

    /**
     * 搜索广告同名的报纸任务
     * @param $adId
     * @param $taskId
     * @return mixed
     */
    private function searchSameAdNameTaskPaper($adId, $taskId)
    {
        $res = M('task_input')
            ->field('taskid, fillegaltypecode')
            ->where([
                'taskid' => ['NEQ', $taskId],
                'fadid' => ['EQ', $adId],
                'task_state' => ['EQ', 2],
                'cut_err_state' => ['EQ', 0],
                'media_class' => ['EQ', 3]
            ])
            ->order('fillegaltypecode desc, lasttime desc')
            ->limit(10)
            ->select();
        return $res;
    }


    /**
     * 搜索广告同名的电视广播任务
     * @param $adId
     * @param $mediaClass
     * @param $taskId
     * @param int $adLen
     * @return bool
     */
    private function searchSameAdNameTaskTvBc($adId, $mediaClass, $taskId, $adLen=0)
    {
        if(!$adLen){
            $adLen = 0;
        }
        if ($mediaClass == 1){
            $samTable = 'ttvsample';
        }elseif ($mediaClass == 2){
            $samTable = 'tbcsample';
        }else{
            return false;
        }
        $res = M('task_input')
            ->alias('task')
            ->field('task.taskid, task.fillegaltypecode, sample.fadlen')
            ->join("$samTable sample on task.sam_uuid = sample.uuid")
            ->where([
                'task.taskid' => ['NEQ', $taskId],
                'task.fadid' => ['EQ', $adId],
                'task.task_state' => ['EQ', 2],
                'task.cut_err_state' => ['EQ', 0],
                'task.media_class' => ['IN', '1,2']
            ])
            ->order("task.fillegaltypecode desc, ABS(sample.fadlen - $adLen)")
            ->limit(10)
            ->select();

        foreach ($res as $key => $value) {
            if (!$value['fadlen']){
                $res[$key]['fadlen'] = 0;
            }
        }
        return $res;
    }
    
    
    
    public function z_illegal_ad(){
    
        $dataList = M('illegal_ad_issue_review')->where('ad_name = ""')->order('id desc')->limit(50)->select();
        
        foreach($dataList as $data){
          $mediaInfo = M('tmedia')
                      ->field('tmedia.fid,left(tmedia.fmediaclassid,2) as media_class,tmediaowner.fregionid')
                      ->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
                      ->where(array('tmedia.fid'=>$data['media_id']))->find();
          if($mediaInfo['media_class'] == '01'){
            try{//
              $da = M('ttvissue_'.date('Ym',$data['start_time']).'_'.substr($mediaInfo['fregionid'],0,2))->alias('issue')
                ->join('ttvsample on ttvsample.fid = issue.fsampleid')
                ->join('tad on tad.fadid = ttvsample.fadid')
                ->where(array('issue.fstarttime'=>$data['start_time'],'issue.fmediaid'=>$data['media_id']))
                ->getField('tad.fadname');
            }catch( \Exception $e) { //
            
            } 
            
          }elseif($mediaInfo['media_class'] == '02'){
            try{//
              $da = M('tbcissue_'.date('Ym',$data['start_time']).'_'.substr($mediaInfo['fregionid'],0,2))->alias('issue')
                ->join('tbcsample on tbcsample.fid = issue.fsampleid')
                ->join('tad on tad.fadid = tbcsample.fadid')
                ->where(array('issue.fstarttime'=>$data['start_time'],'issue.fmediaid'=>$data['media_id']))
                ->getField('tad.fadname');
            }catch( \Exception $e) { //
            
            } 
            
          }elseif($mediaInfo['media_class'] == '03'){
            $da = M('tpapersample')
                  ->join('tad on tad.fadid = tpapersample.fadid')
                  ->where(array('tpapersample.fjpgfilename'=>$data['paper_jpg'],'tpapersample.fissuedate'=>$data['issue_date']))
                  ->getField('tad.fadname');
          }        
          if(!$da) $da = '未知';
          var_dump($mediaInfo['media_class']);
          var_dump($da);
          M('illegal_ad_issue_review')->where(array('id'=>$data['id']))->save(array('ad_name'=>$da));
          
        }
    
    }
    
    
}