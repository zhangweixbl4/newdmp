<?php
//机构管理控制器
namespace Admin\Controller;
use Think\Controller;
class OrgController extends BaseController {

    public function index(){
		
		if(A('Admin/Authority','Model')->authority('100131') === 0){
			exit('没有权限');
		}
		$orgList = M('torg')->where(array('fpid'=>0))->select();
		$authorityList = M('admin_authority')->where('p_id = 0')->select();
		//var_dump($orgList);
		
		$regionList = M('tregion')->field('fid,fname')->where(array('fpid'=>100000))->select();
		//var_dump($regionList);
		$this->assign('regionList',$regionList);
		
		$this->assign('orgList',$orgList);
		$this->assign('authorityList',$authorityList);
		$this->display();
	}
	
	/*添加、修改组织机构*/
	public function add_edit_org(){
		$fid= I('fid');//机构ID
		$fid = intval($fid);//转为数字
		$a_e_data = array();
		$a_e_data['fname'] = I('fname');//机构名称
		
		$a_e_data['ffullname'] = I('ffullname');//机构全称
		$a_e_data['fdescribe'] = I('fdescribe');//机构说明
		$a_e_data['fpid'] = intval(I('fpid',0));//上级ID
		$a_e_data['fcode'] = I('fcode');//机构编码
		$a_e_data['fkind'] = I('fkind');//机构类型(0-根节点，1-机构，2-部门）
		$a_e_data['fstate'] = I('fstate',1);//状态，-1删除，0-无效，1-有效
		
		if($fid == 0){//判断是修改还是新增
			if(A('Admin/Authority','Model')->authority('100132') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));
			}
			$is_repeat = M('torg')->where(array('fcode'=>$a_e_data['fcode']))->count();//查询机构代码是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'机构代码重复'));//返回机构代码重复
			
			//添加到torg表
			$a_e_data['fcreator'] = '系统最高管理员';//创建人
			$a_e_data['fcreatetime'] = date('Y-m-d H:i:s');//创建时间
			$a_e_data['fmodifier'] = '系统最高管理员';//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('torg')->add($a_e_data);//添加数据
			
			if($rr > 0){//判断是否添加成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败,原因未知'));//返回失败
			}
			
		}else{//修改
			if(A('Admin/Authority','Model')->authority('100133') === 0){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'您没有相对应的权限'));
			}
			$is_repeat = M('torg')->where(array('fid'=>array('neq',$fid),'fcode'=>$a_e_data['fcode']))->count();//查询机构代码是否重复
			if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'机构代码重复'));//返回机构代码重复
			
			$a_e_data['fmodifier'] = '系统最高管理员';//修改人
			$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
			$rr = M('torg')->where(array('fid'=>$fid))->save($a_e_data);//修改数据
			if($rr > 0){//判断是否修改成功
				$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));//返回成功
			}else{
				$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));//返回失败
			}
			
		}
		
		
		
	}
	
	/*异步加载机构列表*/
	public function ajax_org_list(){
		
		$fid = I('fid');//获取机构ID
		$orgList = M('torg')->where(array('fpid'=>$fid))->order('fkind asc')->select();
		if(!$orgList){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'无数据'));
		}
		$this->assign('orgList',$orgList);
		$this->display();
	}
	
	/*机构详情*/
	public function ajax_org_details(){
		
		$fid = I('fid');//获取机构ID
		
		$orgDetails = M('torg')->where(array('fid'=>$fid))->find();
		$this->ajaxReturn(array('code'=>0,'orgDetails'=>$orgDetails));
	}
	
	
	/*修正机构的全称*/
	public function adjust_full_name(){
		set_time_limit(0);
		$mList = M('torg')->where(array('fid'=>array('gt',0)))->select();//查询所有机构
		foreach($mList as $m){
			$ffullname = $this->adjust_full_name_2($m['fid'],$m['fname']);//计算全名，使用fid和fname
			M('torg')->where(array('fid'=>$m['fid']))->save(array('ffullname'=>$ffullname));
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>''));
	}
	/*修改机构的全称，计算全称*/
	private function adjust_full_name_2($fid = '',$fname = '',$num = 0){
		$v = M('torg')->where(array('fid'=>$fid))->find();//查询机构信息
		if ($v['fpid'] == 0 || $num > 20){
			return $fname;//如果上级ID等于0或者查询次数大于20次，直接返回
		}	
		if ($v['fpid'] != 0){
			$c_fname = M('torg')->where(array('fid'=>$v['fpid']))->getField('fname');
			return $this->adjust_full_name_2($v['fpid'],$c_fname . '>' . $fname, $num+1);
		}	
	}
	

	/*监管机构搜索接口*/
	public function get_person_list(){
		$fname = I('fname');//获取监管机构名称
		$where = array();
		$where['fstate'] = array('neq',-1);
		$where['fname'] = array('like','%'.$fname.'%');
		// $where['fcode'] = array('neq','0');
		// $where['fpcode'] = array('neq','');
		if($fname != ''){
			$personList = M('tperson')->field('forgid,fname')->where($where)->limit(10)->select();//查询监管机构列表
		}
		$this->ajaxReturn(array('code'=>0,'value'=>$personList));
		
	}

	/*获取上级监管机构列表*/
	public function get_pid_list(){
		$fid = I('fid');//获取点击的监管机构
		$num = 0;
		$fpid = array();
		while($fid != 0 && $num < 10){
			$info = M('torg')->where(array('fid'=>$fid))->find();
			if($info['fpid'] > 0 ){
				$fpid[] = $info['fpid'];
			}
			$fid = $info['fpid'];
			$num++;
		}
		sort($fpid);
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','fpid'=>$fpid));
	}
	
	
	
	

}