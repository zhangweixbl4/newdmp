<?php
namespace Adinput\Model;



class WeixinModel{


	
	public function task_back($wx_id,$back_reason,$ad_name){
		$openid = M('ad_input_user')->where(array('wx_id'=>$wx_id))->getField('openid');//查询用户信息
		$sms_url = 'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token='.wx_token();
		
		$sms_post = '
							{
							   "touser":"'.$openid.'",
							   "template_id":"'. C('WX_BACK_M') .'",
							   "url":"",  
									   
							   "data":{
									   "first": {
										   "value":"您录入的广告被审核人员退回",
										   "color":"#ff0000"
									   },
									   "keyword1":{
										   "value":"'.$ad_name.'",
										   "color":"#173177"
									   },
									   "keyword2": {
										   "value":"'.$back_reason.'",
										   "color":"#173177"
									   },
									   "remark":{
										   "value":"请及时登录任务系统查看详情！",
										   "color":"#173177"
									   }
							   }
						   }
						';
		$rr = http($sms_url,$sms_post,'POST');
		
	}
	

		/**
	 * 获取jssdk需要的ticket
	 *
	 */
	public function getJsapiTicket() {
		$ticket = S('ad_input_wx_ticket');
		if ($ticket == ''){//判断ticket是否过期
			$ticket_url='https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token='.wx_token().'&type=jsapi';//请求ticket的地址
			$result=http($ticket_url);//发起请求
			$result = json_decode($result,true);
			S('ad_input_wx_ticket',$result['ticket'],$result['expires_in'] - 10) ;//写入缓存
			$ticket = $result['ticket'];
		}
		
		return $ticket;
	}

	 /**
	 * 微信JSSDK配置数据
	 *
	 */
	public function wx_jssdk_config(){
		$param['noncestr'] 		= createNoncestr();// 必填，生成签名的随机串
		$param['jsapi_ticket'] 	= $this->getJsapiTicket();//获取ticket 用于签名

		$param['timestamp'] 	= time();//生成签名的时间戳
		$param['url'] 	= 'http://'.$_SERVER['HTTP_HOST'].$_SERVER["REQUEST_URI"];//当前地址
		
		$tmpArr = array('noncestr='.$param['noncestr'], 'jsapi_ticket='.$param['jsapi_ticket'], 'timestamp='.$param['timestamp'],'url='.$param['url']);
		sort($tmpArr, SORT_STRING);//按照字典序排序
		$tmpStr = implode( '&',$tmpArr );//数组转为字符串

		$param['signature']		= sha1( $tmpStr );//签名
		$param['appId']			= C('WX_APPID');// 必填，公众号的唯一标识
		return $param;
	}

    public function wx_captcha($wx_id,$code){
        $openid = M('ad_input_user')->where(array('wx_id'=>$wx_id))->getField('openid');//查询用户信息
        $sms_url = 'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token='.wx_token();

        $sms_post = '
							{
							   "touser":"'.$openid.'",
							   "template_id":"'. C('WX_VERIFY_M') .'",
							   "url":"",  
									   
							   "data":{
									   "first": {
										   "value":"登录验证码",
										   "color":"#ff0000"
									   },
									   "keyword1":{
										   "value":"'.$code.'",
										   "color":"#173177"
									   },
									   "keyword2": {
										   "value":"'.'1分钟内有效'.'",
										   "color":"#173177"
									   },
									   "remark":{
										   "value":"若非本人操作, 可能您的账号存在风险, 请及时联系管理员！",
										   "color":"#173177"
									   }
							   }
						   }
						';
        $rr = http($sms_url,$sms_post,'POST');

    }

}