<?php
//管理员个人中心
namespace Admin\Controller;
use Think\Controller;
use Think\Exception;

class AdminController extends BaseController {

	public function my(){
		
		$my_fid = session('personInfo.fid');
		$myInfo = M('tperson')
							->field('tperson.*,torg.ffullname as org_fullname,tregion.ffullname as region_fullname')
							->join('torg on torg.fid = tperson.forgid')
							->join('tregion on tregion.fid = tperson.fregionid','LEFT')
							->where(array('tperson.fid'=>$my_fid))->find();
		//var_dump($myInfo);
		
		$login_log_list = M('admin_login_log')
											->join('ip on ip.ip = admin_login_log.login_ip','LEFT')
											->where(array('person_id'=>$my_fid))->order('login_time desc')->limit(5)->select();
		//var_dump($login_log_list);
		$this->assign('login_log_list',$login_log_list);
		$this->assign('myInfo',$myInfo);
		$this->assign('file_up',file_up());//上传文件所需的参数
		//var_dump(file_up());
		$this->display();
	}
	
	public function edit_my_info(){
		$my_fid = session('personInfo.fid');
		$fregionid = I('fregionid');//行政区划
		$fduties = I('fduties');//职务
		$fmobile = I('fmobile');//手机号
		$fmail = I('fmail');//电子邮箱
		$is_repeat = M('tperson')->where(array('fid'=>array('neq',$my_fid),'fmobile'=>$fmobile))->count();//查询手机号是否重复
		if($is_repeat > 0) $this->ajaxReturn(array('code'=>-1,'msg'=>'手机号码重复,修改失败'));//返回手机号码重复
		$e_data = array();
		$e_data['fregionid'] = $fregionid;
		$e_data['fduties'] = $fduties;
		$e_data['fmobile'] = $fmobile;
		$e_data['fmail'] = $fmail;

		$e_data['fmodifier'] = session('personInfo.fname');//修改人
		$e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$rr = M('tperson')->where(array('fid'=>$my_fid))->save($e_data);
		//var_dump(M('tperson')->getLastSql());
		if($rr > 0){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));
		}

	}
	
	
	public function edit_my_password(){
		$my_fid = session('personInfo.fid'); 
		$myInfo = M('tperson')->where(array('tperson.fid'=>$my_fid))->find();
		
		$old_password = I('old_password');//旧密码
		$new_password = I('new_password');//新密码
		if($old_password == $new_password){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'原密码和新密码相同,没有做修改'));
		}
		
		if(md5($old_password) != $myInfo['fpassword']){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'原密码错误,修改失败'));
		}
		
		$e_data = array();
		$e_data['fmodifier'] = session('personInfo.fname');//修改人
		$e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$e_data['fpassword'] = md5($new_password);//密码
		
		$rr = M('tperson')->where(array('tperson.fid'=>$my_fid))->save($e_data);//修改密码
		
		if($rr > 0){
			//M('ad_input_user')->where(array('person_id'=>$my_fid))->save(array('password'=>md5($new_password)));
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));
		}
		
		
	}
	
	public function edit_my_avatar(){
		$my_fid = session('personInfo.fid');
		$favatar = I('favatar');//头像地址
		$e_data['favatar'] = $favatar;

		$e_data['fmodifier'] = session('personInfo.fname');//修改人
		$e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$rr = M('tperson')->where(array('fid'=>$my_fid))->save($e_data);
		if($rr > 0 ){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败,原因未知'));
		}
	}

    public function sqlList()
    {
        if(A('Admin/Authority','Model')->authority('300981') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限操作'
            ]);
        }
        if (IS_POST){
            $formData = I('where');
            $where = [];
            if ($formData['state'] != ''){
                $where['state'] = ['EQ', $formData['state']];
            }
            if ($formData['type'] != ''){
                $where['type'] = ['EQ', $formData['type']];
            }
            if ($formData['user'] != ''){
                $where['user'] = ['LIKE', '%'.$formData['user'].'%'];
            }
            if ($formData['reason'] != ''){
                $where['reason'] = ['LIKE', '%'.$formData['reason'].'%'];
            }
            $page = I('page', 1);
            $table = M('sql_list')
                ->where($where)
                ->page($page, 10)
                ->order('created_time desc')
                ->select();
            $stateTextArr = [
                '0' => '未执行',
                '1' => '正在执行',
                '2' => '已执行',
                '9' => '驳回',
                '10' => '语句报错',
            ];
            $typeTextArr = [
                '0' => '修改',
                '1' => '新增',
                '2' => '删除',
				'9' => '其它',
            ];
            foreach ($table as $key => $value) {
                $table[$key]['created_time'] = date('Y-m-d H:i:s', $value['created_time']);
                $table[$key]['state'] = $stateTextArr[$value['state']];
                $table[$key]['type'] = $typeTextArr[$value['type']];
            }
            $total = M('sql_list')
                ->where($where)
                ->count();
            $this->ajaxReturn(compact('table', 'total'));
        }else{
            $this->display();
        }
	}

    public function addSqlItem($sqlInfo)
    {
        if(A('Admin/Authority','Model')->authority('300982') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限操作'
            ]);
        }
        $userName = session('personInfo.fname');

        $sqlInfo['created_time'] = time();
        $sqlInfo['user'] = $userName;
        $sqlInfo['state'] = 0;

        $res = M('sql_list')
            ->add($sqlInfo);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '添加成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '添加失败'
            ]);
        }
	}

    public function getSqlInfo($id)
    {
        if(A('Admin/Authority','Model')->authority('300981') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限操作'
            ]);
        }
        $res = M('sql_list')->find($id);
        $this->ajaxReturn([
            'code' => 0,
            'data' => $res,
        ]);
	}

    public function rejectSqlItem($id)
    {
        if(A('Admin/Authority','Model')->authority('300983') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限操作'
            ]);
        }
        $userName = session('personInfo.fname');
        $time = time();
        $data = [
            'confirmer' => $userName,
            'exec_start_time' => $time,
            'exec_end_time' => $time,
            'state' => 9
        ];
        $res = M('sql_list')
            ->where([
                'id' => ['EQ', $id],
                'state' => ['EQ', 0]
            ])
            ->save($data);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '退回成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '退回失败'
            ]);
        }
    }

    public function execSqlItem($id)
    {
		
		
		$dbList = array(
						'0'=>'',
						'1'=>C('ZIZHI_DB'),
						'2'=>C('FENXI_DB'),
					);
		
		
        if(A('Admin/Authority','Model')->authority('300983') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限操作'
            ]);
        }
        $userName = session('personInfo.fname');
        $where = [
            'id' => ['EQ', $id]
        ];
        $row = M('sql_list')->find($id);
        if ($row['state'] != 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '任务状态非法'
            ]);
        }
        $res = M('sql_list')
            ->where($where)
            ->save([
                'state' => 1
            ]);
        if ($res){
            M('sql_list')
                ->where($where)
                ->save([
                    'exec_start_time' => time(),
                    'confirmer' => $userName,
                ]);
				
			$db = M('','',$dbList[$row['db_flag']]);
			$db->startTrans();
            try{
				$affected_rows = 0;

				$sql_content = $row['sql_content'];
				
				$retStrList = $this->fg_sql($row['sql_content']);
				foreach($retStrList as $sql){
					if(!trim($sql)) continue;
					$affected_rows += $db->execute($sql);

				}
				
				$db->commit();
				
				
            }catch (Exception $e){
				M()->rollback();
                M('sql_list')
                    ->where($where)
                    ->save([
                        'state' => 10,
                        'exec_end_time' => time(),
                    ]);
                $this->ajaxReturn([
                    'code' => -1,
                    'msg' => $e->getMessage()
                ]);
            }
            M('sql_list')
                ->where($where)
                ->save([
                    'state' => 2,
                    'exec_end_time' => time(),
                    'affected_rows' => $affected_rows,
                ]);
            $this->ajaxReturn([
                'code' => 0,
                'data' => $affected_rows
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '任务状态非法'
            ]);
        }
    }

    public function testAffectedRows($id)
    {
		
		$dbList = array(
						'0'=>'',
						'1'=>C('ZIZHI_DB'),
						'2'=>C('FENXI_DB'),
					);
					
					
        if(A('Admin/Authority','Model')->authority('300983') === 0){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '你没有权限操作'
            ]);
        }
        $sql_content = M('sql_list')->find($id)['sql_content'];
		
		
		$db = M('','',$dbList[$row['db_flag']]);
		

		$db->startTrans();
		$affected_rows = 0;
		try{
			$retStrList = $this->fg_sql($sql_content);
			foreach($retStrList as $sql){
				if(!trim($sql)) continue;
				$affected_rows += $db->execute($sql);

			}
		}catch (Exception $e){
			$db->rollback();
			$this->ajaxReturn([
				'code' => -1,
				'msg' => $e->getMessage()
			]);
		}
		$db->rollback();
		$this->ajaxReturn([
			'code' => 0,
			'data' => $affected_rows
		]);

    }
	
	private function fg_sql($sql_content){
		
		$wzs = []; #指定字符出现的位置
		$wz = 0; #从这个位置开始查找
		while(true){ #循环查找
			$wz = strpos($sql_content,';',$wz); #查找分号出现的位置
			if($wz === false){ #如果找不到，就结束
				break;
			}else{
				$wzs[] = $wz; 
			}
			$wz += 1; #从下一个位置开始查找
		}
		

		
		$s=0;
		$retStrList = [];
		foreach($wzs as $w){
			$is_fg = true;
			if((substr_count(str_replace(['\"',"\'"],['',''],$sql_content),'"',0,$w) % 2) == 1) $is_fg = false; 
			if((substr_count(str_replace(['\"',"\'"],['',''],$sql_content),"'",0,$w) % 2) == 1) $is_fg = false; 
		
			if($is_fg){
				$w += 1;
				#var_dump($s.'_'.$w);
				$retStrList[] = substr($sql_content,$s,$w-$s);
				$s = $w;
			}
		}
		$retStrList[] = substr($sql_content,$s);
		
		return $retStrList;

		
	}
	

}