<?php
namespace InputPc\Controller;
use Think\Controller;
class IndexController extends BaseController {
	

	

    public function index(){
		if(!session('wx_id')){
			header("Location:".U('Adinput/VideoCutting/login'));
			exit;
		}
		$wxInfo = M('ad_input_user')->where(array('wx_id'=>session('wx_id')))->find();
		$this->assign('wxInfo',$wxInfo);
		$this->assign('menu_list',A('InputPc/Menu','Model')->menu_list());//管理菜单
		
		
		$user_task_count = M('user_task_count')->where(array('user_id'=>session('wx_id'),'count_date'=>date('Y-m-d')))->find();
		
		
		$tv_input_count = $user_task_count['tv_input'];//今日电视录入
		$bc_input_count = $user_task_count['bc_input'];//今日广播录入
		$paper_input_count = $user_task_count['paper_input'];//今日报纸录入
		
		$tv_inspect_count = $user_task_count['tv_inspect'];//今日电视审核
		$bc_inspect_count = $user_task_count['bc_inspect'];//今日广播审核
		$paper_inspect_count = $user_task_count['paper_inspect'];//今日报纸审核
		
		$today['input_count'] = $tv_input_count + $bc_input_count + $paper_input_count;//今日录入数量
		$today['inspect_count'] = $tv_inspect_count + $bc_inspect_count + $paper_inspect_count;//今日审核数量
		
		$today['paper_page_cut'] = $user_task_count['paper_page_cut'] + 0;//今日报纸版面剪辑数量
		$today['paper_ad_cut'] = $user_task_count['paper_ad_cut'] + 0;//今日报纸广告剪辑数量
		
		
		
		$this->assign('today',$today);

		
		
		
		
		
		
		
		$this->display();
	}
	
	public function home(){
		header("Location:".U('InputPc/Tvsam/index'));
		exit;
		$this->display();
	}

    public function skipNewPlatform()
    {
        if(!session('wx_id')){
            header("Location:".U('Adinput/VideoCutting/login'));
            exit;
        }
        $wx_id = session('wx_id');
        $token = createNoncestr( 16 );
        S('task_urer_'.$token, $wx_id, 60);
        $urlData = [
            [
                'name' => '管理员审核页面',
                'url' => 'http://47.92.128.163:8070/admin/task/data_audit?token=' . $token,
            ],
            [
                'name' => '用户做题列表',
                'url' => 'http://47.92.128.163:8070/admin/task/person_center?token=' . $token,
            ],
            [
                'name' => '官网做题',
                'url' => 'http://47.92.128.163:8070/admin/task/task_detail?token=' . $token,
            ],
            [
                'name' => '电商做题',
                'url' => 'http://47.92.128.163:8070/admin/task/shop_task_detail?token=' . $token,
            ],
            [
                'name' => '判定说明修改',
                'url' => 'http://47.92.128.163:8070/admin/judgement/index?token=' . $token,
            ],
        ];
        $this->assign(compact('urlData'));
        $this->display();
    }
}