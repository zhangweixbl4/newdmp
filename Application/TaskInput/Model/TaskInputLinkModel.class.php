<?php
namespace TaskInput\Model;
use Think\Model;
class TaskInputLinkModel extends Model
{
    /**
     * 检查任务流程状态是否合法
     * @param $taskid
     * @param $type
     * @param $media_class
     * @return bool
     */
    public function checkAdInputValidate($taskid, $type, $media_class)
    {
        $wx_id = session('wx_id');
        // 找到环节表里面的对应行
        $row = $this
            ->where([
                'taskid' => ['EQ', $taskid],
                'type' => ['EQ', $type],
                'media_class' => ['EQ', $media_class]
            ])
            ->master(true)
            ->find();
        if (!$row){
            $this->error = '未找到任务';
            return false;
        }
        if ($row['wx_id'] != $wx_id){
            $this->error = '此任务不是你的任务';
            return false;
        }
        if ($row['state'] != 1 && $row['state'] != 2 && $row['state'] != 4){
            $this->error = '任务状态非法';
            return false;
        }
        return true;
    }

    /**
     * 新建任务时新建相关的任务环节
     * @param $taskid
     * @param $media_class
     * @param $typeArr 定义需要创建的环节类型 如：[1,2],[2]
     * @return bool|string
     */
    public function newTaskLink($taskid, $media_class,$typeArr=[1,2])
    {
        // 定义需要创建的环节类型
        if(!is_array($typeArr)){
            return false;
        }
        $dataArr = [];
        $data = [
            'taskid' => $taskid,
            'update_time' => time(),
            'media_class' => $media_class
            ];
        foreach ($typeArr as $item) {
            $data['type'] = $item;
            $dataArr[] = $data;
        }
        return $this->addAll($dataArr);

    }

    /**
     * 提交任务后,更新相关环节及任务的状态
     * @param $type
     * @param $taskid
     * @param $media_class
     * @param $wx_id
     * @return bool
     */
    public function submitTaskLink($type, $taskid, $media_class, $wx_id)
    {
        // 分为2种情况, 提交新任务和提交被退回的任务
        $linkId = $this
            ->where([
                'taskid' => ['EQ', $taskid],
                'type' => ['EQ', $type],
                'media_class' => ['EQ', $media_class],
            ])
            ->getField('id');
        $linkRes = $this
            ->where([
                'id' => ['EQ', $linkId]
            ])
            ->save([
                'wx_id' => $wx_id,
                'state' => 2,
                'update_time' => time()
            ]);
        if (!$linkRes){
            $this->error = '环节表更新失败';
            return false;
        }
        //查找所有环节的状态
        $userAuthArr = (new AdInputUserModel())->getTaskLinkAuth($wx_id);
        $links = $this
            ->where([
                'taskid' => ['EQ', $taskid],
                'media_class' => ['EQ', $media_class],
                'id' => ['NEQ', $linkId]
            ])
            ->select();
        $saveData = [
            'syn_state' => 1,
            'lasttime' => time(),
        ];
        $hasState4 = false; // 有被退回任务
        $hasState1 = false; // 有正在进行的任务任务
        $allState2 = true; // 所有环节全部完成
        foreach ($links as $link) {
            if ($link['state'] == 4){
                $hasState4 = true;
            }
            if ($link['state'] == 1){
                $hasState1 = true;
            }
            if ($link['state'] != 2){
                $allState2 = false;
            }
        }
        if ($hasState4){
            $saveData['task_state'] = 4;
        }elseif ($hasState1){
            $saveData['task_state'] = 1;
        }elseif ($allState2 && count($userAuthArr) == 2){
            $saveData['task_state'] = 2;
        }else{
            // 完成全部环节, 所以任务就是已经完成
            if ($allState2){
                $saveData['task_state'] = 2;
            }else{
                // 这种情况是只有录入权限, 所以需要将任务和剩余环节全部释放掉
                $saveData['task_state'] = 0;
                $this
                    ->where([
                        'taskid' => ['EQ', $taskid],
                        'state' => ['EQ', 1],
                        'media_class' => ['EQ', $media_class],
                    ])
                    ->save([
                        'state' => 0,
                        'wx_id' => 0,
                        'update_time' => time(),
                    ]);
            }
        }
        // 修改对应任务的状态信息
        if ($media_class == 13){
            $saveData['modifytime'] = $saveData['lasttime'];
            $taskRes = M('tnettask')
                ->where([
                    'taskid' => ['EQ', $taskid]
                ])
                ->save($saveData);
        }else{
            $taskRes = M('task_input')
                ->where([
                    'taskid' => ['EQ', $taskid]
                ])
                ->save($saveData);
        }
        if (!$taskRes){
            $this->error = '流程表更新后任务表更新失败';
            return false;
        }
        return true;
    }
}