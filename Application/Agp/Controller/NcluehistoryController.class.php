<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 线索台账
 */

class NcluehistoryController extends BaseController
{
	/**
	 * 获取线索台账列表
	 * by zw
	 */
	public function index() {
		$getData = $this->oParam;
		$pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
		$fnum = $getData['fnum'];//交办单号
		$fadname = $getData['fadname'];//广告名称
	    $finput_user = $getData['finput_user'];//登记人
	    $fstatus = $getData['fstatus'];//状态
	    $finput_date = $getData['finput_date'];//登记日期，数组
	    $fpush_date = $getData['fpush_date'];//下发日期，数组
	    $fendtime = $getData['fendtime'];//截止日期，数组
	    $fhandle_enddate = $getData['fhandle_enddate'];//办结日期，数组
	    $outtype = $getData['outtype'];//导出类别
	    $fview_look = $getData['fview_look'];//是否查看，-1全部，0未查看，1已查看
	    $fget_regulatorid = $getData['fget_regulatorid'];//接收机构

	    $where['_string'] = '1=1';
	    if(empty($outtype)){
        	$limitIndex = ($pageIndex-1)*$pageSize;
	    }else{
	    	$pageSize = 999999;
	    	$limitIndex = 0;
	    }
	    if(!empty($fnum)){
	    	$where['fnum'] = array('like','%'.$fnum.'%');
	    }
	    if(!empty($fadname)){
	    	$where['fadname'] = array('like','%'.$fadname.'%');
	    }
	    if(!empty($finput_user)){
	    	$where['finput_user'] = array('like','%'.$finput_user.'%');
	    }
	    if(!empty($finput_date)){
	    	$where['a.finput_date'] = ['between',[$finput_date[0],$finput_date[1]]];
	    }
	    if(!empty($fpush_date)){
	    	$where['a.fpush_date'] = ['between',[$fpush_date[0],$fpush_date[1]]];
	    }
	    if(!empty($fendtime)){
	    	$where['a.fendtime'] = ['between',[$fendtime[0],$fendtime[1]]];
	    }
	    if(!empty($fhandle_enddate)){
	    	$where['a.fhandle_enddate'] = ['between',[$fhandle_enddate[0],$fhandle_enddate[1]]];
	    }
	    if(!empty($fget_regulatorid)){
	    	$where['a.fget_regulatorid'] = $fget_regulatorid;
	    }
	    if(empty($fview_look)){
	    	$where['a.fview_look'] = 0;
	    }elseif($fview_look == 1){
	    	$where['a.fview_look'] = 1;
	    }
    	switch ($fstatus) {
    		case 20:
    			$where['a.fstatus'] = $fstatus;
    			break;
    		case 30:
    			$where['a.fstatus'] = $fstatus;
    			break;
    		case 40:
    			$where['a.fstatus'] = $fstatus;
    			break;
    		case 50:
    			$where['a.fstatus'] = $fstatus;
    			break;
    		case 21:
    			$where['a.fstatus'] = ['in',[20,30,40]];
    			$where['_string'] .= ' and DATEDIFF(a.fendtime,"'.date('Y-m-d').'")<0';
    			break;
    		default:
				$where['a.fstatus'] = ['in',[20,30,40,50]];
    			break;
    	}

		$count = M('nj_clue')
			->alias('a')
			->join('(select fclueid from nj_clueflow where fcreatetrepid = '.session('regulatorpersonInfo.fregulatorpid').' group by fclueid) b on a.fid = b.fclueid')
			->join('(select a.fclueid,a.fcreatetrepid,a.fcreatetrepname,fcreatepersonid,fcreateperson,fflowname from nj_clueflow a,(select max(fid) fid from nj_clueflow group by fclueid) b where a.fid = b.fid) c on a.fid = c.fclueid')
			->where($where)
			->count();
		
		$data = M('nj_clue')
			->alias('a')
			->field('a.*,c.*,(case when fstatus = 10 then "待登记" when fstatus = 20 then "待处理" when fstatus = 30 then "核查中" when fstatus = 40 then "立案调查中" when fstatus = 50 then "已办结" else "已作废" end) fstatuscn,(case when fstatus = 20 or fstatus = 30 or fstatus = 40 then ifnull(DATEDIFF(a.fendtime,"'.date('Y-m-d').'"),0) when fstatus = 50 then ifnull(DATEDIFF(a.fendtime,a.fhandle_enddate),0) else 9999 end) diffday,(case when finput_userid = '.session('regulatorpersonInfo.fid').' then 1 else 0 end) isdel,(case when a.fview_look = 1 then "已查看" else "未查看" end) fview_lookcn')
			->join('(select fclueid from nj_clueflow where fcreatetrepid = '.session('regulatorpersonInfo.fregulatorpid').' group by fclueid) b on a.fid = b.fclueid')
			->join('(select a.fclueid,a.fcreatetrepid,a.fcreatetrepname,fcreatepersonid,fcreateperson,fflowname from nj_clueflow a,(select max(fid) fid from nj_clueflow group by fclueid) b where a.fid = b.fid) c on a.fid = c.fclueid')
			->order('a.fstatus asc,diffday asc,a.fid asc')
			->where($where)
			->limit($limitIndex,$pageSize)
			// ->fetchsql(true)
			->select();
		if(!empty($outtype)){
			if(empty($data)){
				$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
			}
	        $outdata['datalie'] = [
	          '序号'=>'fid',
	          '交办单号'=>'fnum',
	          '交办日期'=>'fhandover_date',
	          '交办方式'=>'fhandover_types',
	          '线索来源'=>'fget_types',
	          '移送或提供线索单位'=>'ftipoffs_unit',
	          '投诉或举报方'=>'ftipoffs_user',
	          '投诉或举报方联系方式'=>'ftipoffs_phone',
	          '广告主'=>'fadowner_name',
	          '广告发布者（发布平台）'=>'fadsend_user',
	          '广告类型'=>'fadtypes',
	          '商品/服务类别'=>'fadclass_name',
	          '广告内容'=>'fadname',
	          '刊播时间'=>'fadissue_date',
	          '落地链接'=>'ftarget_url',
	          '发布条目数'=>'fadissue_times',
	          '反馈截止日期'=>'fendtime',
	          '登记机构'=>'finput_regulator',
	          '下发日期'=>'fpush_date',
	          '处理状态'=>'fstatuscn',
	          '查看状态'=>'fview_lookcn',
	          '接收机构'=>'fget_regulator',
	          '反馈机构'=>'fhandle_regulator',
	          '反馈人员'=>'fhandle_user',
	          '反馈结果'=>'fhandle_result',
	          '办结日期'=>'fhandle_enddate',
	        ];

	        $outdata['lists'] = $data;
	        $ret = A('Api/Function')->outdata_xls($outdata);

	        D('Function')->write_log('南京线索台账导出',1,'excel导出成功');
	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
		}
	}

	/*
	* 线索作废
	* zw
	*/
	public function clueDelete(){
		$getData = $this->oParam;
		$fid = $getData['fid'];//线索ID
		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
		}

		if(is_array($fid)){
			$whereNce['fid'] = ['in',$fid];
		}else{
			$whereNce['fid'] = $fid;
		}
		$whereNce['finput_userid'] = session('regulatorpersonInfo.fid');

		$isok = 0;//成功数量
		$listNce = M('nj_clue')->field('fid')->where($whereNce)->select();
		foreach ($listNce as $key => $value) {
			$delNce = M('nj_clue')->where(['fid'=>$value['fid']])->save(['fstatus'=>-1,'fmodify_time'=>date('Y-m-d H:i:s'),'fmodify_user'=>session('regulatorpersonInfo.fname').'（'.session('regulatorpersonInfo.fid').'）']);
			if(!empty($delNce)){
				$viewNcfw = M('nj_clueflow')->where(['fclueid'=>$value['fid']])->order('fid desc')->getField('fid');
				if(!empty($viewNcfw)){
					$dataNcfw['fcreateinfo'] = ['exp','CONCAT(fcreateinfo,"'.session('regulatorpersonInfo.fname').'（'.session('regulatorpersonInfo.fid').'）于'.date('Y-m-d H:i:s').'将线索作废")'];
					$dataNcfw['fchildstatus'] = 30;
					$dataNcfw['fsendstatus'] = 10;
					M('nj_clueflow')->where(['fid'=>$viewNcfw])->save($dataNcfw);
				}
				$isok += 1;
			}
		}
			
		$this->ajaxReturn(array('code'=>0,'msg'=>'成功操作'.$isok.'条。注意：非您本人登记的线索无法操作'));
		
	}

	/*
	* 线索查看
	* zw
	*/
	public function clueView(){
		$getData = $this->oParam;
		$fid = $getData['fid'];//线索ID
		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
		}

		$viewNce = M('nj_clue')
			->alias('a')
			->join('(select fclueid from nj_clueflow where fcreatetrepid = '.session('regulatorpersonInfo.fregulatorpid').' group by fclueid) b on a.fid = b.fclueid')
			->where(['a.fid'=>$fid])
			->find();
		if(!empty($viewNce)){
			$viewNce['fhandle_result'] = str_replace('（）', '', $viewNce['fhandle_result']);
			$viewNcw = M('nj_clueflow')->field('fcreatetrepname,fcreateperson')->where(['fsendstatus'=>0,'fclueid'=>$fid])->find();
			if(!empty($viewNcw)){
				$viewNce = array_merge($viewNce,$viewNcw);
			}

			//延期申请材料
			$yanqiData = M('nj_cluefile')
				->field('fattachname,fattachurl,fuploadtime')
				->where(['ftype'=>41,'fclueid'=>$fid,'fstatus'=>1])
				->select();
			$viewNce['yanqifile'] = $yanqiData?$yanqiData:[];

			$doNcfw = M('nj_clueflow')
				->field('a.fcreatetime,a.fcreateinfo,a.freason,(case when a.fstatus = 10 then "线索登记" when a.fstatus = 30 then "线索核查" when a.fstatus = 40 then "线索立案" when a.fstatus = 50 then "线索反馈" end) finfoname')
				->alias('a')
				->join('(select max(fid) bid from nj_clueflow where fclueid = '.$fid.' and fstatus in(10,30,40,50) group by fclueid,fstatus) b on a.fid = b.bid')
				->select();
			foreach ($doNcfw as $key => $value) {
				$value['cluefile'] = [];
				$viewNce[$value['finfoname']] = $value;
			}
			$doNcfe = M('nj_cluefile')
				->field('(case when ftype = 5 then "线索登记" when ftype = 30 then "线索核查" when ftype = 40 then "线索立案" when ftype = 50 then "线索反馈" end) finfoname,fattachname,fattachurl')
				->where(['fclueid'=>$fid,'fstatus'=>1,'ftype'=>['in',[5,30,40,50]]])
				->select();
			foreach ($doNcfe as $key => $value) {
				$viewNce[$value['finfoname']]['cluefile'][] = $value;
			}
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$viewNce));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无权获取'));
		}
	}

    
}