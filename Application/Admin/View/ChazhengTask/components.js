Vue.component('cz-deal', {
  props: {
    data: {
      default: {},
      type: Object
    }
  },
  data: function () {
    return {
      uploadOptions: Object.assign(UPLOAD_OPTION.val),
      upOptions: {
        up_server: UPLOAD_OPTION.up_server
      },
      form: {
        files: {
          czfiles: [],
          jbfiles: []
        },
      },
      from: '',
      show: true,
      czfiles: [],
      fczcontent: '',
      fill_type: {
        '0': '投诉举报',
        '20': '上级交办',
        '30': '部门移交',
        '40': '其他',
      }
    }
  },
  created() {
    this._getview(this.data.fid)
  },
  methods: {
    _getview(fid) {
      let data = {
        fid: fid
      }
      $.post('/Admin/ChazhengTask/view_chazheng', data)
        .then(res => {
          if (res.code === 0) {
            this.form = res.data
            this.fczcontent = res.data.fczcontent
            let files = res.data.files
            files.jbfiles.map(item => {
              if (item.fattach_url.indexOf('?attname=') === -1) {
                item.fattach_url = item.fattach_url + '?attname=' + item.fattach
              }
              return item
            })
            files.czfiles.forEach(item => {
              if (item.fattach_url.indexOf('?attname=') === -1) {
                item.fattach_url = item.fattach_url + '?attname=' + item.fattach
              }
              return item
            })
          }
        })
    },
    _tijiao() {
      let data = {
        fid: this.data.fid,
        fczcontent: this.fczcontent,
        attachinfo: []
      }
      this.form.files.czfiles.forEach(item => {
        data.attachinfo.push({
          fattachname: item.fattach,
          fattachurl: item.fattach_url
        })
      })
      $.post('/Admin/ChazhengTask/report_process', data)
        .then(res => {
          if (res.code === 0) {
            this.$message.success(res.msg)
            this.$emit('close')
          } else {
            this.$message.error(res.msg)
          }
        })
    },
    _removeTag(obj, e) {
      this.form.files.czfiles = this.form.files.czfiles.filter(item => {
        return obj.fattach_url !== item.fattach_url
      })
      e.preventDefault();
    },
    _setKey(file) {
      this.uploadOptions.key = file.uid
    },
    _uploadSuccess(response, file, fileList) {
      this.form.files.czfiles.push({
        fattach: file.name,
        fattach_url: [UPLOAD_OPTION.bucket_url, response.key, '?attname=', file.name].join('')
      })
    },
    _uploadError() {
      this.$message.error('上传失败')
    }
  },
  template: `
  <el-dialog title="案件处理"
    :visible.sync="show"
    width="1200px"
    :close-on-click-modal="false"
    :close-on-press-escape="false"
    center
    :before-close="() => $emit('close')">
      <el-form :model="form"
        ref="form"
        label-width="120px">
        <el-row :gutter="20">
          <el-col :span="8">
            <el-form-item label="案件名称: ">
              {{form.fad_name}}
            </el-form-item>
          </el-col>
          <el-col :span="8">
            <el-form-item label="案件类型: ">
            {{fill_type[form.fill_type]}}
            </el-form-item>
          </el-col>
          <el-col :span="8">
            <el-form-item label="广告类别：">
            {{form.ffullname}}
            </el-form-item>
          </el-col>
          <el-col :span="8">
            <el-form-item label="被投诉人/单位: ">
              {{form.fcplaintt_media}}
            </el-form-item>
          </el-col>
          <el-col :span="8">
            <el-form-item label="投诉人/单位: ">
              {{form.fplaint_person}}
            </el-form-item>
          </el-col>
          <el-col :span="8">
            <el-form-item label="投诉方联系方式: ">
              {{form.fplaint_phone}}
            </el-form-item>
          </el-col>
          <el-col :span="8">
            <el-form-item label="投诉日期: ">
              {{form.fplaint_time}}
            </el-form-item>
          </el-col>
          <el-col :span="24">
            <el-form-item label="案件附件: ">
              <a :href="item.fattach_url"
                v-for="(item,index) in form.files.jbfiles"
                :key="index"
                style="margin-right:10px">
                <el-tag class="right-margin"
                  size="small"
                  type="priamry">
                  {{item.fattach}}
                </el-tag>
              </a>
              <span v-if="!form.files.jbfiles.length">无附件</span>
            </el-form-item>
          </el-col>
          <el-col :span="24">
            <el-form-item label="涉嫌违法内容: ">
              {{form.fill_content}}
            </el-form-item>
          </el-col>
          <el-col :span="24">
            <el-form-item label="重新查证理由: " v-if="form.fstatus === '41'">
              {{form.fcxczcontent || '无'}}
            </el-form-item>
          </el-col>
          <el-col :span="24">
            <el-form-item label="查证说明: ">
              <el-input type="textarea" v-model="fczcontent"></el-input>
            </el-form-item>
          </el-col>
          <el-col :span="24">
            <el-form-item label="查证材料: ">
              <a :href="item.fattach_url"
                v-for="(item,index) in form.files.czfiles"
                :key="index"
                style="margin-right:10px">
                <el-tag class="right-margin"
                  size="small"
                  type="priamry"
                  closable
                  @close="_removeTag(item,$event)">
                  {{item.fattach}}
                </el-tag>
              </a>
              <div style="margin-top:8px;">
                <el-upload
                  :action="upOptions.up_server"
                  :data="uploadOptions"
                  :before-upload="_setKey"
                  auto-upload
                  :on-success="_uploadSuccess"
                  :show-file-list="false"
                  :on-error="_uploadError">
                  <el-button  type="primary">点击上传</el-button>
                </el-upload>
              </div>
            </el-form-item>
          </el-col>
        </el-row>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button
          type="primary"
          @click="_tijiao">提交</el-button>
      </div>
    </el-dialog>
  `
})

Vue.component('files-dialog', {
  props: {
    data: {
      default: () => [],
      type: Array
    },
    desc: {
      default: '',
      type: String
    }
  },
  data() {
    return {
      show: true
    }
  },
  template: `
    <el-dialog center
      :visible.sync="show"
      title="附件列表"
      :close-on-click-modal="false"
      :close-on-press-escape="false"
      :before-close="() => $emit('close')">
      <h3 v-if="desc">查证说明: {{desc}}</h3>
      <el-table :data="data"
        border>
        <el-table-column label="查证附件">
          <template slot-scope="scope">
            <a :href="scope.row.fattach_url">
              <el-button type="text">{{ scope.row.fattach }}</el-button>
            </a>
          </template>
        </el-table-column>
      </el-table>
    </el-dialog>
  `
})
Vue.component('file-dialog', {
  props: {
    data: {
      default: () => [],
      type: Array
    },
  },
  data() {
    return {
      show: true
    }
  },
  template: `
    <el-dialog center
      :visible.sync="show"
      title="附件列表"
      :close-on-click-modal="false"
      :close-on-press-escape="false"
      :before-close="() => $emit('close')">
      <el-table :data="data"
        border>
        <el-table-column label="附件名称">
          <template slot-scope="scope">
            <a :href="scope.row.fattach_url">
              <el-button type="text">{{ scope.row.fattach }}</el-button>
            </a>
          </template>
        </el-table-column>
      </el-table>
    </el-dialog>
  `
})
Vue.component('ajxsxq', {
  props: {
    data: {
      default: () => {},
      type: Object
    },
    iscz: {
      default: false,
      type: Boolean
    },
  },
  data() {
    return {
      infoData: {
        files: {
          czfiles: []
        }
      },
      filedata: [],
      filedata2: [],
      from: '',
      show: true,
      loading: true,
      fill_type: {
        '0': '投诉举报',
        '20': '上级交办',
        '30': '部门移交',
        '40': '其他',
      },
      czzt: {
        '0': '草稿箱',
        '20': '已交办',
        '30': '已上报',
        '40': '查证中',
        '41': '重新查证中',
        '42': '完成查证',
      }
    }
  },
  created() {
    this._getInfo(this.data)
  },
  methods: {
    _getInfo(obj) {
      this.loading = true
      let data = {
        fid: obj.fid
      }
      let url = ''
      let type = Number(obj.type)
      switch (type) {
        case 1:
          url = this.iscz ? '/Admin/ChazhengTask/view_chazheng' : '/Agp/Gckzdanjian/view_jszdanjian'
          aurl = '/Agp/Gckzdanjian/attach_fjanjian'
          break
        case 2:
          url = '/Agp/Gckzdanjian/view_jbzdanjian'
          aurl = '/Agp/Gckzdanjian/attach_fjanjian'
          break
        case 3:
          url = '/Agp/Gckkdyzdanjian/view_jbzdanjian'
          aurl = '/Agp/Gckzdanjian/attach_fjanjian'
          break
        case 4:
          url = '/Agp/Gkdyzdajjieguo/view_jszdanjian'
          aurl = '/Agp/Gckzdanjian/attach_fjanjian'
          break
      }
      $.post(url, data)
        .then(res => {
          if (res.code === 0) {
            this.infoData = res.data
          } else {
            this.$message.error(res.msg)
          }
          this.loading = false
        })
    }
  },
  template: `
    <el-dialog :title="infoData.fad_name"
      :visible.sync="show"
      width="1200px"
      center
      @close="$emit('close')">
      <div v-loading="loading">
        <el-row :gutter="10">
          <el-col :span="12">
            <el-card :body-style="{ padding: '20px' }"
              shadow="never">
              <div slot="header">
                <span>交办信息</span>
              </div>
              <el-form label-width="130px">
                <el-form-item label="交办案件工商局：">
                  {{infoData.fsend_regname || '无'}}
                </el-form-item>
                <el-form-item label="广告类别：">
                  {{infoData.ffullname || '无'}}
                </el-form-item>
                <el-form-item label="投诉人/单位：">
                  {{infoData.fplaint_person || '无'}}
                </el-form-item>
                <el-form-item label="投诉方联系方式：">
                  {{infoData.fplaint_phone || '无'}}
                </el-form-item>
                <el-form-item label="被投诉人/单位：">
                  {{infoData.fcplaintt_media || '无'}}
                </el-form-item>
                <el-form-item label="涉嫌违法内容：">
                  {{infoData.fill_content || '无'}}
                </el-form-item>
                <el-form-item label="投诉日期：">
                  {{infoData.fplaint_time || '无'}}
                </el-form-item>
                <el-form-item label="案件类型：">
                  {{fill_type[infoData.fill_type]}}
                </el-form-item>
                <el-form-item label="交办日期：">
                  {{infoData.fsend_time ? infoData.fsend_time.substring(0,10) : '' || '未交办'}}
                </el-form-item>
                <el-form-item label="附件：">
                  <div v-for="(item,index) in infoData.files.jbfiles"
                    :key="item.fattach_url">
                    <a :href="item.fattach_url"
                      target="_blank">
                      <el-button type="text">{{item.fattach}}</el-button>
                    </a>
                  </div>
                </el-form-item>
                <el-form-item label="查办案件工商局：">
                  {{infoData.fget_regname}}
                </el-form-item>
              </el-form>
            </el-card>
          </el-col>
          <el-col :span="12">
            <el-card :body-style="{ padding: '20px' }"
              shadow="never">
              <div slot="header">
                <span>处理情况</span>
              </div>
              <el-form label-width="120px">
                <el-form-item label="处理状态：">
                  {{czzt[infoData.fstatus]}}
                </el-form-item>
                <el-form-item label="重新查证理由：" v-if="infoData.fstatus === '41'">
                  {{infoData.fcxczcontent || '无'}}
                </el-form-item>
                <el-form-item v-if="iscz" label="查证材料：">
                  <div v-for="(item,index) in infoData.files.czfiles">
                    <a :href="item.fattach_url+'?attname='+ item.fattach" :key="index" target="_blank">
                      <el-button type="text">{{item.fattach}}</el-button>
                    </a>
                  </div>
                </el-form-item>
                <el-form-item v-if="iscz" label="查证说明：">
                  {{infoData.fczcontent || '无'}}
                </el-form-item>
                <el-form-item label="反馈日期：">
                  {{infoData.fresult_datetime ? infoData.fresult_datetime.substring(0,10) : '未反馈'}}
                </el-form-item>
                <el-form-item label="处理情况：">
                  {{infoData.fresult || '未处理'}}
                </el-form-item>
                <el-form-item label="处理凭证：">
                  <a :href="item.fattach_url + '?attname=' + item.fattach"
                    v-for="item in filedata2"
                    target="_blank"
                    :key="item.fattach_url">
                    <el-button type="text">{{item.fattach}}</el-button>
                  </a>
                </el-form-item>
              </el-form>
            </el-card>
          </el-col>
        </el-row>
      </div>
    </el-dialog>
  `
})