<?php
namespace Api\Controller;
use Think\Controller;

/**
 * 存公证公共接口
 * by zw
 */

class FapplicantController extends Controller {

	/**
     * 接收参数
     */
    protected $P;

    /**
     * 初始化
     */
    public function _initialize(){
        $this->P = json_decode(file_get_contents('php://input'),true);
    }

   	/**
     * 下载存证资源包
     * by zw
    */
    public function downEvidenceObjext(){
    	$rid = $this->P['rid'];
    	$ret = A('Open/Evidence','Model')->getEvidenceObjext($rid);
    	if(is_array($ret)){
    		$this->ajaxReturn($ret);
    	}else{
    		$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$ret]);
    	}
    }

    /**
     * 下载存管函(下载为PDF文件版)
     * by zw
    */
    public function downEvidenceLetterPDF(){
    	$rid = $this->P['rid'];
    	$ret = A('Open/Evidence','Model')->getEvidenceLetterPDF($rid);
    	if(is_array($ret)){
    		$this->ajaxReturn($ret);
    	}else{
    		$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$ret]);
    	}
    }

    /**
     * 下载存管函(输出HTML源文件版)
     * by zw
    */
    public function downEvidenceLetterHTML(){
    	$rid = $this->P['rid'];
    	$ret = A('Open/Evidence','Model')->getEvidenceLetterHTML($rid);
    	if(is_array($ret)){
    		$this->ajaxReturn($ret);
    	}else{
    		$this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$ret]);
    	}
    }
}