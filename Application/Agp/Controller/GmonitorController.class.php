<?php
namespace Agp\Controller;
use Think\Controller;

class GmonitorController extends BaseController
{
    /**
    *发送监管信息
    */
    public function sendMonitor()
    {
      $title = I('title','');//信息标题
      $content = I('content','');//消息内容
      $attachinfo = I('attachinfo');//上传的文件信息
      $bureau = I('bureau');//接收局

      $data['mon_title'] = $title;
      $data['mon_remark'] = $content;
      $data['mon_send_time'] = date('Y-m-d H:i:s');//添加时间
      $data['mon_sendtreid'] = session('regulatorpersonInfo.fregulatorpid');//发送机构ID
      $data['mon_sendtrename'] = session('regulatorpersonInfo.regulatorpname');//发送机构名
      $data['mon_sendusername'] = session('regulatorpersonInfo.fname');//上传人
      $data['mon_senduserid'] = session('regulatorpersonInfo.fid');//上传人id
      
      $monitor_id = M('tbn_monitor')->add($data);
      if($monitor_id){
        $this->addFileList($attachinfo ,$monitor_id);//添加附件信息
        $this->addMonBureau($bureau ,$monitor_id);//添加监管信息对应的接收局
        D('Function')->write_log('监测情况通报',1,'消息发送成功','tbn_monitor',$monitor_id,M('tbn_monitor')->getlastsql());
        $this->ajaxReturn(array('code'=>0,'msg'=>'消息发送成功'));
      }else{
        D('Function')->write_log('监测情况通报',0,'消息发送失败','tbn_monitor',0,M('tbn_monitor')->getlastsql());
        $this->ajaxReturn(array('code'=>-1,'msg'=>'消息发送失败'));
      }
    }
  
     //添加附件信息
    protected function addFileList($attachinfo ,$monitor_id)
    {

        $attach_data['monf_monid'] = $monitor_id;
        $attach_data['monf_upload_time'] = date('Y-m-d H:i:s');//上传时间
        $attach = [];
        foreach ($attachinfo as $key2 => $value2){
            $attach_data['monf_filename'] = $value2['fattachname'];
            $attach_data['monf_fileurl'] = $value2['fattachurl'];
            array_push($attach,$attach_data);
        }

        M('tbn_monitor_file')->addAll($attach);

    }
    
    //添加监管信息对应的接收局
    protected function addMonBureau($bureau ,$monitor_id)
    {
      $system_num = getconfig('system_num');
      $attach_data['monb_monid'] = $monitor_id;
      $attach_data['monb_customer'] = $system_num;
      $attach = [];
      foreach ($bureau as $key2 => $value2){
              $attach_data['monb_mon_bureau'] = $value2;
              array_push($attach,$attach_data);
          }
      
      M('tbn_monitor_bureau')->addAll($attach);
    }
    
    //接收监管信息列表
    public function getMonitorList()
    {
      Check_QuanXian(['tbqingkuanglb']);
      $system_num = getconfig('system_num');
      $user_bureau = session('regulatorpersonInfo.fregulatorlevel');//用户所属局

      $p  = I('page', 1);//当前第几页
      $pp = 10;//每页显示多少记录
      $where['monb_mon_bureau'] = $user_bureau;
      $where['monb_customer'] = $system_num;

      $count = M('tbn_monitor_bureau')
          ->alias('mb')
          ->join('tbn_monitor as m on mb.monb_monid = m.mon_id')//接收消息
          ->where($where)
          ->count();// 查询满足要求的总记录数

      

      $res = M('tbn_monitor_bureau')
          ->alias('mb')
          ->field('
            m.mon_id,
            m.mon_title,
            m.mon_remark,
            m.mon_sendtrename,
            m.mon_send_time
          ')
          ->join('tbn_monitor as m on mb.monb_monid = m.mon_id')
          ->where($where)
          ->order('mb.monb_id desc')
          ->page($p,$pp)
          ->select();
      foreach($res as &$v){
        $v['file_list'] = M('tbn_monitor_file')
                  ->field('monf_filename,monf_fileurl')
                  ->where(array('monf_monid'=>$v['mon_id']))
                  ->select();
      }
      //dump($res);
      //die;
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$res)));
    }
    
    //发送监管信息列表
    public function sendMonitorList()
    {
      $system_num = getconfig('system_num');
      $id = session('regulatorpersonInfo.fregulatorpid');//机构ID
      $use_id = session('regulatorpersonInfo.fid');//
      $p  = I('page', 1);//当前第几页
      $pp = 10;//每页显示多少记录
      $where['mon_sendtreid'] = $id;
      $where['mon_senduserid'] = $use_id;
      $where['monb_customer'] = $system_num;

      $count = M('tbn_monitor')
          ->alias('m')
          ->join('tbn_monitor_bureau as mb on m.mon_id = mb.monb_monid','LEFT')//发送消息
          ->where($where)
          ->count();// 查询满足要求的总记录数

      

      $res = M('tbn_monitor')
          ->alias('m')
          ->field('
            m.mon_id,
            m.mon_title,
            m.mon_remark,
            m.mon_sendtrename,
            m.mon_send_time,
            mb.monb_mon_bureau
          ')
          ->join('tbn_monitor_bureau as mb on m.mon_id = mb.monb_monid','LEFT')
          ->where($where)
          ->order('m.mon_id desc')
          ->page($p,$pp)
          ->select();
      foreach($res as &$v){
          $v['file_list'] = M('tbn_monitor_file')
              ->field('monf_filename,monf_fileurl')
              ->where(array('monf_monid'=>$v['mon_id']))
              ->select();
      }

      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$res)));
    }
}