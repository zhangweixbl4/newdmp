<?php
namespace Agp\Model;

class FnetadModel{

	/**
	 * 互联网违法广告自动添加
	 * by zw
	 * $sampleid 样本ID
	 */
	public function ad_addaction($sampleid){
		session_write_close();
		$where_sample['a.major_key'] = $sampleid;
		$where_sample['a.fillegaltypecode'] = array('gt',0);
		$do_sample = M('tnetissue')
			->field('b.fmediaownerid,c.fregionid,a.fmediaid,a.fadclasscode,a.fadname,a.fexpressioncodes,a.fillegalcontent,a.net_target_url,a.thumb_url_true,a.fexpressions,a.net_platform')
			->alias('a')
			->join('tmedia b on a.fmediaid=b.fid')
			->join('tmediaowner c on b.fmediaownerid = c.fid')
			->where($where_sample)
			->find();
		if(!empty($do_sample)){		
			//查询该违法广告的媒体关联哪些机构
			$regulatorcodeList = M('tregulatormedia')
				->where(array('fmediaid'=>$do_sample['fmediaid']))
				->group('fcustomer')
				->getField('fcustomer',true);

			foreach($regulatorcodeList as $regulatorcode){
				if(!in_array($regulatorcode,C('CUSTOMER_COM')))  continue;//退出本次循环

				//判断是否已存在相应未处理完成的违法广告，如果已不存在则添加新的违法广告
				$where_ad['fsample_id'] = $sampleid;
				$where_ad['tbn_illegal_ad.fcustomer'] = $regulatorcode;
				$where_ad['fmedia_class'] = 13;
				$where_ad_1['fstatus'] = 0;
				$where_ad_2['fstatus&fstatus3'] = [30,['lt',30],'_multi'=>true];
				$where_ad['_complex'] = [$where_ad_1,$where_ad_2,'_logic' => 'or'];
				$do_ad = M('tbn_illegal_ad')
					->field('fid,last_syn_time,fmedia_id,fmediaownerid')
					->where($where_ad)
					->find();
				
				if(empty($do_ad)){
					$data_ad['fregion_id'] = $do_sample['fregionid'];
					$data_ad['fmedia_class'] = 13;
					$data_ad['fplatform'] = $do_sample['net_platform'];
					$data_ad['fmedia_id'] = $do_sample['fmediaid'];
					$data_ad['fad_class_code'] = $do_sample['fadclasscode'];
					$data_ad['fsample_id'] = $sampleid;
					$data_ad['fad_name'] = $do_sample['fadname'];
					$data_ad['fillegal_code'] = $do_sample['fexpressioncodes'];
					$data_ad['fillegal'] = $do_sample['fillegalcontent'];
					$data_ad['favifilename'] = $do_sample['net_target_url'];
					$data_ad['fjpgfilename'] = $do_sample['thumb_url_true'];
					$data_ad['fmediaownerid'] = $do_sample['fmediaownerid'];
					$data_ad['create_time'] = date('Y-m-d H:i:s');
					$data_ad['fexpressions'] = $do_sample['fexpressions'];
					$data_ad['fcustomer'] = $regulatorcode;
					$data_ad['identify'] = time();
					$data_ad['fillegaltypecode'] = 30;

					//添加违法广告记录
					$do_ad = M('tbn_illegal_ad')->add($data_ad);

					D('Function')->write_log('违法广告信息添加',1,'执行成功','tbn_illegal_ad',$do_ad,M('tbn_illegal_ad')->getlastsql());

					//添加违法广告发布记录
					$this->adissue_addaction($do_ad,0,$do_sample['fmediaid'],$do_sample['fmediaownerid'],$sampleid);
				}else{
					D('Function')->write_log('违法广告信息更新',1,'执行成功','tbn_illegal_ad',$do_ad['fid'],M('tbn_illegal_ad')->getlastsql());
					$this->adissue_addaction($do_ad['fid'],$do_ad['last_syn_time'],$do_ad['fmedia_id'],$do_ad['fmediaownerid'],$sampleid);
				}
			}
		}

		if(!empty($do_ad)){
			return 1;
		}else{
			return 0;
		}

	}

	/**
	 * 互联网违法广告发布记录添加
	 * by zw
	 * $adid 违法广告ID
	 */
	public function adissue_addaction($adid,$last_syn_time,$fmedia_id,$fmediaownerid,$sampleid){
		session_write_close();
		if(!empty($last_syn_time)){
			$where_log['net_created_date'] = array('gt',$last_syn_time*1000);
		}
		$where_log['tid'] = $sampleid;
		$do_log = M('tnetissueputlog')
			->field('net_created_date,net_original_url')
			->where($where_log)
			->select();

		foreach ($do_log as $key => $value) {
			$data_issue[$key]['fillegal_ad_id'] = $adid;
			$data_issue[$key]['fmedia_id'] = $fmedia_id;
			$data_issue[$key]['fissue_date'] = date('Y-m-d',$value['net_created_date']/1000);
			$data_issue[$key]['ftarget_url'] = $value['net_original_url'];
			$data_issue[$key]['fmedia_class'] = 13;
			$data_issue[$key]['fmediaownerid'] = $fmediaownerid;
		}
		if(!empty($data_issue)){
			M('tbn_illegal_ad_issue')->addall($data_issue);
			M()->execute('update tbn_illegal_ad set last_syn_time='.time().' where fid = '.$adid);
		}
	}

}