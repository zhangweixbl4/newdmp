<?php
namespace Agp\Controller;
use Think\Controller;

class OuttaskfjController extends Controller{

    public function reporttask(){
        session_write_close();
        $nowtime    = date('Y-m-d H:i:s');
        $trepid     = I('trepid',20350000);//福建ID
        $system_num = getconfig('system_num');

        $regionid = substr($trepid, 2,6);
        $tregion_len = get_tregionlevel($regionid);
        $where_tr['_string'] = '1=0';
        if($tregion_len  == 2){//省级
            $where_tr['_string'] = ' a.fregionid like "'.substr($regionid,0,2).'%"';
        }elseif($tregion_len == 4){//市级
            $where_tr['_string'] = ' a.fregionid like "'.substr($regionid,0,4).'%"';
        }elseif($tregion_len == 6){//县级
            $where_tr['_string'] = ' a.fregionid like "'.substr($regionid,0,6).'%"';
        }

        $where_tr['a.fkind'] = 1;
        $do_tr = M('tregulator')
            ->field('a.fid')
            ->alias('a')
            ->join('tregion b on a.fregionid = b.fid and b.flevel <5')
            ->where($where_tr)
            ->order('a.fid asc')
            ->select();
        foreach ($do_tr as $key => $value) {
            //检索复核报告
            $where_tn1['pnfocus']   = 1;
            $where_tn1['pntype']    = 60;
            $where_tn1['pnfiletype']= 10;
            $where_tn1['pntrepid']  = $value['fid'];
            $where_tn1['fcustomer']  = $system_num;
            $do_tn1 = M('tpresentation')->field('pnstarttime')->where($where_tn1)->order('pnendtime desc')->find();
            if(!empty($do_tn1)){
                $month_tn1 = $this->get_month_diff(date('Y-m-01',strtotime($do_tn1['pnstarttime'])),date('Y-m-d'));
                if($month_tn1>2){
                    $daytime = date("Y-m-01",strtotime("+1 months",strtotime($do_tn1['pnstarttime'])));
                    A("OutWordFJ")->create_fuhepresentation($daytime,1,$value['fid']);
                }
            }else{
                $daytime = I('daytime')?I('daytime'):date("Y-m-d", strtotime("-3 month"));//报告时间
                A("OutWordFJ")->create_fuhepresentation($daytime,1,$value['fid']);
            }

            //检索跟踪报告
            $where_tn2['pnfocus']   = 1;
            $where_tn2['pntype']    = 61;
            $where_tn2['pnfiletype']= 10;
            $where_tn2['pntrepid']  = $value['fid'];
            $where_tn2['fcustomer']  = $system_num;
            $do_tn2 = M('tpresentation')->field('pnstarttime')->where($where_tn2)->order('pnendtime desc')->find();
            if(!empty($do_tn2)){
                $month_tn2 = $this->get_month_diff(date('Y-m-01',strtotime($do_tn2['pnstarttime'])),date('Y-m-d'));
                if($month_tn2>2){
                    $daytime = date("Y-m-01",strtotime("+1 months",strtotime($do_tn2['pnstarttime'])));
                    A("OutWordFJ")->create_genzongpresentation($daytime,1,$value['fid']);
                }
            }else{
                $daytime = I('daytime')?I('daytime'):date("Y-m-d", strtotime("-3 month"));//报告时间
                A("OutWordFJ")->create_genzongpresentation($daytime,1,$value['fid']);
            }

            // 检索日报
            $where_tn3['pnfocus']   = 1;
            $where_tn3['pntype']    = 10;
            $where_tn3['pnfiletype']= 10;
            $where_tn3['pntrepid']  = $value['fid'];
            $where_tn3['fcustomer']  = $system_num;
            $do_tn3 = M('tpresentation')->field('pnendtime')->where($where_tn3)->order('pnendtime desc')->find();
            if(!empty($do_tn3)){
                $month_tn3 = $this->get_day_diff(strtotime($do_tn3['pnendtime']),strtotime(date('Y-m-d')));
                if($month_tn3>2){//一天处理时间
                    $daytime = date("Y-m-d",strtotime("+1 day",strtotime($do_tn3['pnendtime'])));
                    A("OutWordFJ")->create_daypresentation2($daytime,1,$value['fid']);
                }
            }else{
                $daytime = I('daytime')?I('daytime'):date("Y-m-01", strtotime("-30 day"));//从上个月1号开始生成日报
                A("OutWordFJ")->create_daypresentation2($daytime,1,$value['fid']);
            }

            //检索周报
            $where_tn4['pnfocus']   = 1;
            $where_tn4['pntype']    = 20;
            $where_tn4['pnfiletype']= 10;
            $where_tn4['pntrepid']  = $value['fid'];
            $where_tn4['fcustomer']  = $system_num;
            $do_tn4 = M('tpresentation')->field('pnstarttime,pnendtime')->where($where_tn4)->order('pnendtime desc')->find();
            if(!empty($do_tn4)){
                $month_tn4 = $this->get_day_diff(strtotime($do_tn4['pnendtime']),strtotime(date('Y-m-d')));
                if($month_tn4>9){//一天处理时间
                    $daytime = date("Y-m-d",strtotime("+1 day",strtotime($do_tn4['pnendtime'])));
                    $do_dt = M()->query('select weekofyear("'.$daytime.'") as dt');
                    if($do_dt[0]['dt'] == 1 && date("m",strtotime($daytime)) == 12){
                        $do_dt = M()->query('select weekofyear("'.$do_tn4['pnstarttime'].'") as dt');
                        $do_dt[0]['dt'] += 1;
                    }
                    A("OutWordFJ")->create_weekpresentation2( date("Y年".$do_dt[0]['dt']."周",strtotime($daytime)),1,$value['fid']);
                }
            }else{
                $daytime = date("Y-m-d",mktime(0, 0 , 0,date("m"),date("d")-date("w")+1-28,date("Y")));//从前四周开始生成周报
                $do_dt = M()->query('select weekofyear("'.$daytime.'") as dt');
                A("OutWordFJ")->create_weekpresentation2( date("Y年".$do_dt[0]['dt']."周",strtotime($daytime)),1,$value['fid']);
            }

            //检索月报
            $where_tn5['pnfocus']   = 1;
            $where_tn5['pntype']    = 30;
            $where_tn5['pnfiletype']= 10;
            $where_tn5['pntrepid']  = $value['fid'];
            $where_tn5['fcustomer']  = $system_num;
            $do_tn5 = M('tpresentation')->field('pnstarttime')->where($where_tn5)->order('pnendtime desc')->find();
            if(!empty($do_tn5)){
                $month_tn5 = $this->get_month_diff($do_tn5['pnstarttime'],date('Y-m-d'));
                if($month_tn5>1){
                    $daytime = date('Y-m-d',strtotime(date('Y-m-01', strtotime($do_tn5['pnstarttime'])). ' +1 month'));
                    $daytime2 = date('Y-m-d',strtotime(date('Y-m-01', strtotime($do_tn5['pnstarttime'])). ' +2 month -1 day'));
                    $diffday = $this->get_day_diff(strtotime($daytime2),strtotime(date('Y-m-d')));
                    if($diffday>2){//一天处理时间
                        A("OutWordFJ")->public_presentation2($daytime,1,$value['fid'],'month');
                    }
                }
            }else{
                $daytime = I('daytime')?I('daytime'):date('Y-m-d',strtotime(date('Y-m-01'). ' -2 month -1 day'));//从前3个月生成月报
                A("OutWordFJ")->public_presentation2($daytime,1,$value['fid'],'month');
            }

            //检索季报
            $where_tn7['pnfocus']   = 1;
            $where_tn7['pntype']    = 70;
            $where_tn7['pnfiletype']= 10;
            $where_tn7['pntrepid']  = $value['fid'];
            $where_tn7['fcustomer']  = $system_num;
            $do_tn7 = M('tpresentation')->field('pnstarttime,pnendtime')->where($where_tn7)->order('pnendtime desc')->find();
            if(!empty($do_tn7)){
                $month_tn7 = $this->get_month_diff($do_tn7['pnendtime'],date('Y-m-d'));
                if($month_tn7>3){
                    $daytime = date('Y-m-d',strtotime(date('Y-m-01', strtotime($do_tn7['pnstarttime'])). ' +4 month'));
                    $daytime2 = date('Y-m-d',strtotime(date('Y-m-01', strtotime($do_tn7['pnstarttime'])). ' +5 month -1 day'));
                    $diffday = $this->get_day_diff(strtotime($daytime2),strtotime(date('Y-m-d')));
                    if($diffday>2){//一天处理时间
                        A("OutWordFJ")->public_presentation2($daytime,1,$value['fid'],'season');
                    }
                }
            }else{
                $diffmonth = $this->get_month_diff(date('Y').'-01-01',date('Y-m-d'));
                if($diffmonth>=6 && $diffmonth <12){
                    $daytime = I('daytime')?I('daytime'):date('Y-01-01');
                }else{
                    $daytime = I('daytime')?I('daytime'):date('Y-07-01',strtotime(date('Y-m-01'). ' -12 month'));
                }
                A("OutWordFJ")->public_presentation2($daytime,1,$value['fid'],'season');
            }

            //检索半年报
            $where_tn8['pnfocus']   = 1;
            $where_tn8['pntype']    = 75;
            $where_tn8['pnfiletype']= 10;
            $where_tn8['pntrepid']  = $value['fid'];
            $where_tn8['fcustomer']  = $system_num;
            $do_tn8 = M('tpresentation')->field('pnstarttime,pnendtime')->where($where_tn8)->order('pnendtime desc')->find();
            if(!empty($do_tn8)){
                $month_tn8 = $this->get_month_diff($do_tn8['pnendtime'],date('Y-m-d'));
                if($month_tn8>6){
                    $daytime = date('Y-m-d',strtotime(date('Y-m-01', strtotime($do_tn8['pnstarttime'])). ' +7 month'));
                    $daytime2 = date('Y-m-d',strtotime(date('Y-m-01', strtotime($do_tn8['pnstarttime'])). ' +8 month -1 day'));
                    $diffday = $this->get_day_diff(strtotime($daytime2),strtotime(date('Y-m-d')));
                    if($diffday>2){//一天处理时间
                        A("OutWordFJ")->public_presentation2($daytime,1,$value['fid'],'halfyear');
                    }
                }
            }else{
                $diffmonth = $this->get_month_diff(date('Y').'-01-01',date('Y-m-d'));
                if($diffmonth>=6 && $diffmonth <12){
                    $daytime = I('daytime')?I('daytime'):date('Y-01-01');
                }else{
                    $daytime = I('daytime')?I('daytime'):date('Y-07-01',strtotime(date('Y-m-01'). ' -12 month'));
                }
                A("OutWordFJ")->public_presentation2($daytime,1,$value['fid'],'halfyear');
            }

            //检索年报
            $where_tn9['pnfocus']   = 1;
            $where_tn9['pntype']    = 80;
            $where_tn9['pnfiletype']= 10;
            $where_tn9['pntrepid']  = $value['fid'];
            $where_tn9['fcustomer']  = $system_num;
            $do_tn9 = M('tpresentation')->field('pnstarttime,pnendtime')->where($where_tn9)->order('pnendtime desc')->find();
            if(!empty($do_tn9)){
                $month_tn9 = $this->get_month_diff($do_tn9['pnendtime'],date('Y-m-d'));
                if($month_tn9>12){
                    $daytime = date('Y-m-d',strtotime(date('Y-m-01', strtotime($do_tn9['pnstarttime'])). ' +13 month'));
                    $daytime2 = date('Y-m-d',strtotime(date('Y-m-01', strtotime($do_tn9['pnstarttime'])). ' +14 month -1 day'));
                    $diffday = $this->get_day_diff(strtotime($daytime2),strtotime(date('Y-m-d')));
                    if($diffday>2){//一天处理时间
                        A("OutWordFJ")->public_presentation2($daytime,1,$value['fid'],'year');
                    }
                }
            }else{
                $diffmonth = $this->get_month_diff(date('Y').'-01-01',date('Y-m-d'));
                if($diffmonth>12){
                    $daytime = I('daytime')?I('daytime'):date('Y-01-01');
                }else{
                    $daytime = I('daytime')?I('daytime'):date('Y-01-01',strtotime(date('Y-m-01'). ' -12 month'));
                }
                A("OutWordFJ")->public_presentation2($daytime,1,$value['fid'],'year');
            }

        }

        $this->ajaxReturn(array('code'=>0,'msg'=>'任务执行完成'));
        
    }

    //获取月份差
    function get_month_diff( $date1, $date2, $tags='-' ){
        $date1 = explode($tags,$date1);
        $date2 = explode($tags,$date2);
        return ($date2[0] - $date1[0]) * 12 + ($date2[1] - $date1[1]);
    }

    //获取天数差
    function get_day_diff($a,$b){
     $a_dt=getdate($a);
     $b_dt=getdate($b);
     $a_new=mktime(12,0,0,$a_dt['mon'],$a_dt['mday'],$a_dt['year']);
     $b_new=mktime(12,0,0,$b_dt['mon'],$b_dt['mday'],$b_dt['year']);
     return round(abs($a_new-$b_new)/86400);
    }

}